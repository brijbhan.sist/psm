﻿<%@ WebHandler Language="C#" Class="DriverUploaded" %>

using System;
using System.Web;
using System.IO;
using System.Data;
using Newtonsoft.Json;

using System.Drawing;
using System.Drawing.Drawing2D;
public class DriverUploaded : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        DataTable dt = new DataTable();
        dt.Clear();
        dt.Columns.Add("URL");

        if (context.Request.Files.Count > 0)
        {
            HttpFileCollection files = context.Request.Files;
            for (int i = 0; i < files.Count; i++)
            {
                HttpPostedFile file = files[i];
                string fname;
                if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE" || HttpContext.Current.Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                {
                    string[] testfiles = file.FileName.Split(new char[] { '\\' });
                    fname = testfiles[testfiles.Length - 1];
                }
                else
                {
                    fname = file.FileName;
                }
                string[] getextension = fname.Split('.');
                string dirFullPath = HttpContext.Current.Server.MapPath("~/DriverUploaded/");

                string timeStamp = context.Request.QueryString["timeStamp"].ToString();

                string fnamefull = dirFullPath + timeStamp + "." + getextension[1];

                file.SaveAs(fnamefull);
                context.Response.ContentType = "text/plain";
                context.Response.Write(JsonConvert.SerializeObject(dt));
            }
        }

    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}