﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="POS_CreditMemoList.aspx.cs" Inherits="POS_POS_CreditMemoList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
       #tblOrderLog th, .table td {
    padding: 6px !important;
}
       #tblOrderLog tbody td {
    padding: 6px !important;
}
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Credit Memo List</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Manage Credit Memo</a>
                        </li>
                        <li class="breadcrumb-item active">Credit Memo List
                        </li>
                    </ol>
                </div>

            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">

                <%
                    if (Session["EmpAutoId"] != null)
                    {
                        if (Session["EmpTypeNo"].ToString() == "2" || Session["EmpTypeNo"].ToString() == "10")
                        {
                %>
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <%
                        }
                    }
                %>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1" id="btnAddNewCredit" runat="server">
                    <button type="button" class="dropdown-item" onclick="location.href='/POS/POS_CreditMemo.aspx'">New Credit Memo</button>

                </div>
                <input type="hidden" id="hiddenPackerAutoId" runat="server" />
                <input type="hidden" id="hiddenEmpType" runat="server" />
            </div>
        </div>
    </div>

    <div class="content-body" style="min-height: 400px;">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="
        padding: 0rem 1rem;
"> From Credit Memo Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" onchange="setdatevalidation(1)" placeholder="From Credit Memo Date" id="txtSFromDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">To Credit Memo Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" onchange="setdatevalidation(2)" placeholder="To Credit Memo Date" id="txtSToDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <select class="form-control border-primary input-sm" id="ddlSalesPerson" onchange="bindCustomer();" style="width: 100%;">
                                            <option value="0">All Sales Person</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-4">
                                        <select class="form-control border-primary input-sm" id="ddlCustomer" style="width: 100%;">
                                            <option value="0">All Customer</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Credit Memo No" id="txtSOrderNo" onfocus="this.select()" />
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <select class="form-control border-primary input-sm" id="ddlSStatus">
                                            <option value="0">All</option>
                                        </select>
                                    </div>

                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-4">
                                        <select class="form-control border-primary input-sm" id="ddlCreditType">
                                            <option value="0">All Type</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" id="btnSearch">Search</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblOrderList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="action text-center width3per">Action</td>
                                                        <td class="orderNo text-center">Credit Memo No</td>
                                                        <td class="orderDt text-center width4per">Date</td>
                                                        <td class="CreditType text-center width3per">Credit Type</td>
                                                        <td class="cust">Customer</td>
                                                        <td class="SalesPerson">Sales Person</td>
                                                        <td class="value price width3per">Amount ($)</td>
                                                        <td class="product text-center width3per">Items</td>
                                                        <td class="status text-center width3per">Status</td>
                                                        <td class="referenceorderno  text-center">Ref. O. No</td>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>

                                </div>
                                <div class="row container-fluid">
                                    <div>
                                        <select class="form-control input-sm border-primary" id="ddlPageSize">
                                            <option selected="selected" value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="Pager"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div id="modalOrderLog" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row form-group" style="width:100%">
                        <div class="col-md-4 col-sm-12">
                            <h4 class="modal-title">Credit Memo Log</h4>
                        </div>
                        <div class="col-md-4 col-sm-12 text-right">
                            <span><b>Credit Memo No</b>&nbsp;:&nbsp;<span id="lblOrderNo"></span></span>
                        </div>
                        <div class="col-md-4 col-sm-12 text-right">
                            <span><b>Date</b>&nbsp;:&nbsp;<span id="lblOrderDate"></span></span>
                        </div>
                    </div>
                </div>
                <input type="hidden" value="" id="CreditLogAutoId" />
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="tblOrderLog">
                            <thead class="bg-blue white">
                                <tr>
                                    <td class="SrNo text-center">SN</td>
                                    <td class="ActionBy">Action By</td>
                                    <td class="Date  text-center">Date 
                                    <i class="la la-arrow-up" aria-hidden="true" style="display: none;" onclick="sortBy('1')"></i>
                                        <i class="la la-arrow-down" aria-hidden="true" style="display: none;" onclick="sortBy('2')"></i>
                                    </td>
                                    <td class="Action">Action</td>
                                    <td class="Remark">Remark</td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation round box-shadow-1  btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="JS/POS_CreditMemoList.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
</asp:Content>

