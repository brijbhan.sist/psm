﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class POS_POS_CreditMemo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["EmpTypeNo"] != null)
        {
            if (Session["EmpTypeNo"].ToString() != "10")
            {
                Response.Redirect("/");

            }
        }
        else
        {
            Response.Redirect("/");
        }

    }
}