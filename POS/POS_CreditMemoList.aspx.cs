﻿using System;
using Newtonsoft.Json;
using DLLPOS_CreditMemoMaster;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class POS_POS_CreditMemoList : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["EmpTypeNo"] != null)
        {
            if (Session["EmpTypeNo"].ToString() != "10")
            {
                Response.Redirect("/");

            }
        }
        else
        {
            Response.Redirect("/");
        }
    }
    [WebMethod(EnableSession = true)]
    public static string BarcodeReader(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_POSCreditMemoMaster pobj = new PL_POSCreditMemoMaster();
            pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"].ToString());
            pobj.barcodeNo = jdv["BarcodeNo"];
            pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
            BL_POSCreditMemoMaster.ReadBarcode(pobj);
            if (!pobj.isException)
            {
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string bindDropdown()
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_POSCreditMemoMaster pobj = new PL_POSCreditMemoMaster();
            pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"].ToString());
            BL_POSCreditMemoMaster.bindDropdown(pobj);
            string json = "";
            foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
            {
                json += dr[0];
            }

            return json;
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string bindDropdownList()
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_POSCreditMemoMaster pobj = new PL_POSCreditMemoMaster();
            pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"].ToString());
            BL_POSCreditMemoMaster.bindDropdownList(pobj);
            string json = "";
            foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
            {
                json += dr[0];
            }

            return json;
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string bindAllDropdown()
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_POSCreditMemoMaster pobj = new PL_POSCreditMemoMaster();
            pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"].ToString());
            BL_POSCreditMemoMaster.bindAllDropdown(pobj);
            if (!pobj.isException)
            {
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }

                return json;
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string bindAllCustomer(string SalesPerson)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_POSCreditMemoMaster pobj = new PL_POSCreditMemoMaster();
            pobj.SalesPerson = Convert.ToInt32(SalesPerson);
            BL_POSCreditMemoMaster.bindCustomer(pobj);
            if (!pobj.isException)
            {
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }

                return json;
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string BindProduct(string CustomerAutoId)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_POSCreditMemoMaster pobj = new PL_POSCreditMemoMaster();
            pobj.CustomerAutoId = Convert.ToInt32(CustomerAutoId);
            pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"].ToString());
            BL_POSCreditMemoMaster.BindProduct(pobj);
            if (!pobj.isException)
            {
                return pobj.Ds.GetXml();
            }
            else
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string bindUnitType(string ProductAutoId)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_POSCreditMemoMaster pobj = new PL_POSCreditMemoMaster();
            pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"].ToString());
            pobj.ProductAutoId = Convert.ToInt32(ProductAutoId);
            BL_POSCreditMemoMaster.SelectUnit(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string ValidateCreditMemo(string dataValue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_POSCreditMemoMaster pobj = new PL_POSCreditMemoMaster();
            pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"].ToString());
            pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
            pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]);
            pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
            pobj.Qty = Convert.ToInt32(jdv["Qty"]);
            BL_POSCreditMemoMaster.ValidateCreditMemo(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string insertOrderData(string TableValues, string dataValue)
    {
        DataTable dtOrder = new DataTable();
        dtOrder = JsonConvert.DeserializeObject<DataTable>(TableValues);

        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_POSCreditMemoMaster pobj = new PL_POSCreditMemoMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                if (dtOrder.Rows.Count > 0)
                {
                    pobj.TableValue = dtOrder;
                }
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"].ToString());
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.TotalAmount = Convert.ToDecimal(jdv["TotalAmount"]);
                pobj.OverallDisc = Convert.ToDecimal(jdv["OverallDisc"]);
                pobj.OverallDiscAmt = Convert.ToDecimal(jdv["OverallDiscAmt"]);
                pobj.TotalTax = Convert.ToDecimal(jdv["TotalTax"]);
                pobj.GrandTotal = Convert.ToDecimal(jdv["GrandTotal"]);
                pobj.TaxType = Convert.ToInt32(jdv["TaxType"]);
                pobj.TaxValue = Convert.ToDecimal(jdv["TaxValue"]);
                pobj.MLQty = Convert.ToDecimal(jdv["MLQty"]);
                pobj.MLTax = Convert.ToDecimal(jdv["MLTax"]);
                pobj.AdjustmentAmt = Convert.ToDecimal(jdv["AdjustmentAmt"]);
                pobj.CreditMemoType = Convert.ToInt32(jdv["CreditMemoType"]);
                pobj.ReferenceOrderAutoId = Convert.ToInt32(jdv["ReferenceOrderAutoId"]);
                pobj.Remarks = jdv["Remarks"];
                BL_POSCreditMemoMaster.insert(pobj);
                if (!pobj.isException)
                {

                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string editCredit(string CreditAutoId)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_POSCreditMemoMaster pobj = new PL_POSCreditMemoMaster();
            pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"].ToString());
            pobj.CreditAutoId = Convert.ToInt32(CreditAutoId);
            BL_POSCreditMemoMaster.EditCredit(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string updateOrderData(string TableValues, string dataValue, string CreditAutoId)
    {
        DataTable dtOrder = new DataTable();
        dtOrder = JsonConvert.DeserializeObject<DataTable>(TableValues);
        var jss = new JavaScriptSerializer();
        PL_POSCreditMemoMaster pobj = new PL_POSCreditMemoMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                if (dtOrder.Rows.Count > 0)
                {
                    pobj.TableValue = dtOrder;
                }

                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"].ToString());
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.TotalAmount = Convert.ToDecimal(jdv["TotalAmount"]);
                pobj.OverallDisc = Convert.ToDecimal(jdv["OverallDisc"]);
                pobj.OverallDiscAmt = Convert.ToDecimal(jdv["OverallDiscAmt"]);
                pobj.TotalTax = Convert.ToDecimal(jdv["TotalTax"]);
                pobj.TaxType = Convert.ToInt32(jdv["TaxType"]);
                pobj.TaxValue = Convert.ToDecimal(jdv["TaxValue"]);
                pobj.GrandTotal = Convert.ToDecimal(jdv["GrandTotal"]);

                pobj.MLQty = Convert.ToDecimal(jdv["MLQty"]);
                pobj.MLTax = Convert.ToDecimal(jdv["MLTax"]);
                pobj.AdjustmentAmt = Convert.ToDecimal(jdv["AdjustmentAmt"]);
                pobj.Remarks = jdv["Remarks"];
                pobj.CreditAutoId = Convert.ToInt32(CreditAutoId);
                BL_POSCreditMemoMaster.updateCredit(pobj);
                if (!pobj.isException)
                {

                    return "Success!!";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public static string updateOrderData1(string TableValues, string CreditAutoId)
    {
        DataTable dtOrder = new DataTable();
        dtOrder = JsonConvert.DeserializeObject<DataTable>(TableValues);
        var jss = new JavaScriptSerializer();
        PL_POSCreditMemoMaster pobj = new PL_POSCreditMemoMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                if (dtOrder.Rows.Count > 0)
                {
                    pobj.TableValue = dtOrder;
                }

                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"].ToString());
                pobj.CreditAutoId = Convert.ToInt32(CreditAutoId);
                BL_POSCreditMemoMaster.updateCredit1(pobj);
                if (!pobj.isException)
                {

                    return "Success!!";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public static string ApprovedCredit(string CreditAutoId, string Status)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_POSCreditMemoMaster pobj = new PL_POSCreditMemoMaster();
            pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"].ToString());
            pobj.CreditAutoId = Convert.ToInt32(CreditAutoId);
            pobj.Status = Convert.ToInt32(Status);
            BL_POSCreditMemoMaster.ApprovedCredit(pobj);
            if (!pobj.isException)
            {

                return "Success!!";
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string CompleteOrder(string CreditAutoId)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_POSCreditMemoMaster pobj = new PL_POSCreditMemoMaster();
            pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"].ToString());
            pobj.CreditAutoId = Convert.ToInt32(CreditAutoId);
            BL_POSCreditMemoMaster.CreditComplete(pobj);
            if (!pobj.isException)
            {

                return "Success!!";
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string PrintCredit(string CreditAutoId)
    {
        PL_POSCreditMemoMaster pobj = new PL_POSCreditMemoMaster();
        pobj.CreditAutoId = Convert.ToInt32(CreditAutoId);
        BL_POSCreditMemoMaster.PrintCredit(pobj);
        return pobj.Ds.GetXml();

    }
    [WebMethod(EnableSession = true)]
    public static string selectQtyPrice(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_POSCreditMemoMaster pobj = new PL_POSCreditMemoMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustAutoId"]);
                pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]);
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                BL_POSCreditMemoMaster.UnitDetails(pobj);
                return pobj.Ds.GetXml();
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string PrintBulkCredit(string CreditAutoId)
    {
        PL_POSCreditMemoMaster pobj = new PL_POSCreditMemoMaster();
        pobj.CreditAutoId = Convert.ToInt32(CreditAutoId);
        BL_POSCreditMemoMaster.PrintBulkCredit(pobj);
        string json = "";
        foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
        {
            json += dr[0];
        }
        return json;

    }
    [WebMethod(EnableSession = true)]
    public static string getCreditList(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_POSCreditMemoMaster pobj = new PL_POSCreditMemoMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.CreditNo = jdv["CreditNo"];
                if (jdv["FromDate"] != null && jdv["FromDate"] != "")
                {
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != null && jdv["ToDate"] != "")
                {
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                }
                pobj.PageIndex = Convert.ToInt32(jdv["pageIndex"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.SalesPerson = Convert.ToInt32(jdv["SalesPerson"]);
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.CreditType = Convert.ToInt32(jdv["CreditType"]);
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"].ToString());
                BL_POSCreditMemoMaster.select(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                return json;
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception)
        {
            return "false";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string bindStatus()
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {

            PL_POSCreditMemoMaster pobj = new PL_POSCreditMemoMaster();
            BL_POSCreditMemoMaster.bindStatus(pobj);
            string json = "";
            foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
            {
                json += dr[0];
            }
            return json;
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string deleteOrder(string CreditAutoId)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                PL_POSCreditMemoMaster pobj = new PL_POSCreditMemoMaster();
                pobj.CreditAutoId = Convert.ToInt32(CreditAutoId);
                BL_POSCreditMemoMaster.delete(pobj);
                if (!pobj.isException)
                {

                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {

                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string viewOrderLog(string CreditAutoId, int sortInCode)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_POSCreditMemoMaster pobj = new PL_POSCreditMemoMaster();
            pobj.CreditAutoId = Convert.ToInt32(CreditAutoId);
            pobj.sortInCode = Convert.ToInt32(sortInCode);
            BL_POSCreditMemoMaster.BindCreditLog(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Session Expired";
        }

    }
    [WebMethod(EnableSession = true)]
    public static string clickonSecurity(string CheckSecurity)
    {
        PL_POSCreditMemoMaster pobj = new PL_POSCreditMemoMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"].ToString() != null)
            {

                pobj.CheckSecurity = CheckSecurity.ToString();
                BL_POSCreditMemoMaster.clickonSecurity(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return "false";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string ConfirmSecurity(string CheckSecurity, string CreditNo, string MLTaxRemark)
    {
        PL_POSCreditMemoMaster pobj = new PL_POSCreditMemoMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"].ToString() != null)
            {

                pobj.CheckSecurity = CheckSecurity;
                pobj.CreditNo = CreditNo;
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"].ToString());
                pobj.MLTaxRemark = MLTaxRemark;
                BL_POSCreditMemoMaster.ConfirmSecurity(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "false";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string CheckSecurity(string datavalue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(datavalue);
        PL_POSCreditMemoMaster pobj = new PL_POSCreditMemoMaster();
        try
        {
            if (HttpContext.Current.Session["EmpTypeNo"] != null)
            {
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpTypeNo"].ToString());
                pobj.SecurityKey = jdv["Security"];
                BL_POSCreditMemoMaster.checkSecurity(pobj);
                if (!pobj.isException)
                {

                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "SessionExpired";

            }
        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string CancelCreditMomo(string datavalue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(datavalue);
            PL_POSCreditMemoMaster pobj = new PL_POSCreditMemoMaster();
            pobj.EmpAutoId = Convert.ToInt32((HttpContext.Current.Session["EmpAutoId"]).ToString());
            pobj.CreditAutoId = Convert.ToInt32(jdv["CreditAutoId"]);
            pobj.CancelRemarks = jdv["CancelRemark"];
            pobj.IPAddress = HttpContext.Current.Request.UserHostAddress;
            BL_POSCreditMemoMaster.CancelCreditMemo(pobj);
            if (!pobj.isException)
            {
                return "Success";
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}