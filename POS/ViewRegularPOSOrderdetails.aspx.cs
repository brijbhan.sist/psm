﻿using DLLRegularPOSOrder;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Warehouse_ViewRegularPOSOrderdetails : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/POS/JS/ViewRegularPOSOrderdetails.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredField'>" + text + "</script>"
                ));
        }
    }
    [WebMethod(EnableSession = true)]
    public static string bindOrderDetails(string OrderAutoId)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_RegularPOSOrder pobj = new PL_RegularPOSOrder();
            try
            {

                pobj.Opcode = 42;
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                BL_RegularPOSOrder.bindOrderDetails(pobj);
                return pobj.Ds.GetXml();

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}