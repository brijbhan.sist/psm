﻿$(document).ready(function () {
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    $("#txtSFromDate").val(month + '/' + day + '/' + year);
    $("#txtSToDate").val(month + '/' + day + '/' + year);
    bindStatus();
    bindDropdown();
    bindCustomer();

    $('#txtSFromDate').pickadate({
        min: new Date("01/01/2019"),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtSToDate').pickadate({
        min: new Date("01/01/2019"),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    if ($("#ddlPageSize").val() == null) {
        $("#ddlPageSize").val(10);
    }
    getCreditList(1);
})
function setdatevalidation(val) {
    var fdate = $("#txtSFromDate").val();
    var tdate = $("#txtSToDate").val();
    if (val == 1) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtSToDate").val(fdate);
        }
    }
    else if (val == 2) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtSFromDate").val(tdate);
        }
    }
}
function Pagevalue(e) {
    getCreditList(parseInt($(e).attr("page")));
};
function bindDropdown() {   
    $.ajax({
        type: "POST",
        url: "POS_CreditMemoList.aspx/bindDropdownList",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            
            if (response.d != "Session Expired") {
                var DropDown = $.parseJSON(response.d);
                for (var i = 0; i < DropDown.length; i++) {
                    var AllDropDownList = DropDown[i];

                    var CustomerList = AllDropDownList.Customer;
                    
                    var ddlCustomer = $("#ddlCustomer");
                    $("#ddlCustomer option:not(:first)").remove();
                    for (var k = 0; k < CustomerList.length; k++) {
                        var Customer = CustomerList[k];
                        var option = $("<option />");
                        option.html(Customer.Customer);
                        option.val(Customer.AutoId);
                        ddlCustomer.append(option);
                    }
                    ddlCustomer.select2();

                    var Status = AllDropDownList.CreditType;
                    for (var p = 0; p < Status.length; p++) {
                        var StatusList = Status[p];
                        $('#txtOrderDate').val(StatusList.CurrentDate);
                    }

                    var ddlCreditType = $("#ddlCreditType");
                    $("#ddlCreditType option:not(:first)").remove();
                    var CreditType = AllDropDownList.OrderType;
                    for (var k = 0; k < CreditType.length; k++) {
                        var Type = CreditType[k];
                        var option = $("<option />");
                        option.html(Type.OrderType);
                        option.val(Type.AutoId);
                        ddlCreditType.append(option);
                    }
                 
                    var ddlSalesPerson = $("#ddlSalesPerson");
                    $("#ddlSalesPerson option:not(:first)").remove();
                    var SalesPerson = AllDropDownList.SalesPerson;
                    for (var k = 0; k < SalesPerson.length; k++) {
                        var Type = SalesPerson[k];
                        var option = $("<option />");
                        option.html(Type.SalesPerson);
                        option.val(Type.AutoId);
                        ddlSalesPerson.append(option);
                    }
                    ddlSalesPerson.select2();
                }
            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function bindCustomer() {
    SalesPerson=$("#ddlSalesPerson").val();
    $.ajax({
        type: "POST",
        url: "POS_CreditMemoList.aspx/bindAllCustomer",
        data: "{'SalesPerson':'" + SalesPerson + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {

            if (response.d != "Session Expired") {
                var getData = $.parseJSON(response.d);
                $("#ddlCustomer option:not(:first)").remove();
                $.each(getData, function (index, item) {
                    $("#ddlCustomer").append("<option value='" + getData[index].AutoId + "'>" + getData[index].Customer + "</option>");
                });
                $("#ddlCustomer").select2();
            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function bindStatus() {
   
    $.ajax({
        type: "POST",
        url: "POS_CreditMemoList.aspx/bindStatus",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var status = $.parseJSON(response.d);
                var ddlStatus = $("#ddlSStatus");
                $("#ddlSStatus option:not(:first)").remove();
              
                for (var k = 0; k < status.length; k++) {
                    var StatusType = status[k];
                    var option = $("<option />");
                    option.html(StatusType.StatusType);
                    option.val(StatusType.AutoId);
                    ddlStatus.append(option);
                }
            }
            //else {
            //    location.href = '/';
            //}
        },
        failure: function (result) {
        },
        error: function (result) {
        }
    });
}
function getCreditList(pageIndex) {
    var data = {
        CustomerAutoId: $("#ddlCustomer").val(),
        CreditNo: $("#txtSOrderNo").val().trim(),
        FromDate: $("#txtSFromDate").val().trim(),
        ToDate: $("#txtSToDate").val().trim(),
        Status: $("#ddlSStatus").val(),
        CreditType: $("#ddlCreditType").val(),
        SalesPerson: $("#ddlSalesPerson").val(),
        pageIndex: pageIndex,
        PageSize: $("#ddlPageSize").val() || 10,
    };

    $.ajax({
        type: "POST",
        url: "POS_CreditMemoList.aspx/getCreditList",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
              
                if (response.d != "false") {
                  
                    var DropDown = $.parseJSON(response.d);
                   
                    var row = $("#tblOrderList thead tr").clone(true);

                    for (var i = 0; i < DropDown.length; i++) {
                        $("#tblOrderList tbody tr").remove();

                        var AllDropDownList = DropDown[i];
                        var OrderList = AllDropDownList.OrderList;
                        var EmpType = AllDropDownList.EmpType;
                        if (OrderList.length > 0) {
                            $("#EmptyTable").hide();

                            for (var j = 0; j < OrderList.length; j++) {
                                var List = OrderList[j];
                                $(".orderNo", row).html("<span OrderAutoId='" + List.AutoId + "'>" + List.OrderNo + "</span>");
                                $(".orderDt", row).text(List.OrderDate);
                                $(".cust", row).text(List.CustomerName);
                                $(".SalesPerson", row).text(List.SalesPerson);
                                $(".CreditType", row).text(List.CreditType);
                                $(".value", row).text(parseFloat(List.GrandTotal).toFixed(2));
                                $(".product", row).text(List.NoOfItems);
                                $(".referenceorderno", row).text(List.referenceorderno);

                                if (Number(List.StatusCode) == 1) {
                                    $(".status", row).html("<span class='badge badge badge-pill badge-info'>" + List.Status + "</span>");
                                } else if (Number(List.StatusCode) == 2) {
                                    $(".status", row).html("<span class='badge badge badge-pill badge-primary'>" + List.Status + "</span>");
                                } else if (Number(List.StatusCode) == 3) {
                                    $(".status", row).html("<span class='badge badge badge-pill badge-primary'>" + List.Status + "</span>");
                                } else if (Number(List.StatusCode) == 4) {
                                    $(".status", row).html("<span class='badge badge badge-pill badge-warning'>" + List.Status + "</span>");
                                } else if (Number(List.StatusCode) == 5) {
                                    $(".status", row).html("<span class='badge badge badge-pill badge-warning'>" + List.Status + "</span>");
                                } else if (Number(List.StatusCode) == 6) {
                                    $(".status", row).html("<span class='badge badge badge-pill badge-danger'>" + List.Status + "</span>");
                                } else if (Number(List.StatusCode) == 7) {
                                    $(".status", row).html("<span class='badge badge badge-pill badge-danger'>" + List.Status + "</span>");
                                } else if (Number(List.StatusCode) == 8) {
                                    $(".status", row).html("<span class='badge badge badge-pill badge-default'>" + List.Status + "</span>");
                                }
                                if (EmpType.EmpType == 2 || EmpType.EmpType == 1) {
                                    if (List.StatusCode == 1 || List.StatusCode == 6) {
                                        $(".action", row).html("<a title='Edit' href='/POS/POS_CreditMemo.aspx?PageId=" + List.AutoId + "'><span class='la la-edit'></span></a>&nbsp;<a title='Log' href='javascript:;' onclick='viewOrderLog(" + List.AutoId + ",1)'><span class='la la-history'></span></a>&nbsp<a title='Delete' href='javascript:;'><span class='la la-remove' onclick='deleterecord(\"" + List.AutoId + "\")'></span><b></a></b>");
                                    } else {
                                        $(".action", row).html("<a title='Edit' href='/POS/POS_CreditMemo.aspx?PageId=" + List.AutoId + "'><span class='la la-edit'></span></a>&nbsp;<a href='javascript:;' title='Log' onclick='viewOrderLog(" + List.AutoId + ",1)'><span class='la la-history'></span></a></b>");
                                    }
                                }
                                else if (EmpType.EmpType == 8) {
                                    $(".action", row).html("<a title='Edit' href='/Manager/ManagerCreditMemo.aspx?PageId=" + List.AutoId + "'><span class='la la-edit'></span></a>&nbsp;<a title='Log' href='javascript:;' onclick='viewOrderLog(" + List.AutoId + ",1)'><span class='la la-history'></span></a></b>");
                                }
                                else {
                                    $(".action", row).html("<a title='Edit' href='/POS/POS_CreditMemo.aspx?PageId=" + List.AutoId + "'><span class='la la-edit'></span></a>&nbsp;<a title='Log' href='javascript:;' onclick='viewOrderLog(" + List.AutoId + ",1)'><span class='la la-history'></span></a></b>");
                                }
                                $(".value", row).css('text-align', 'right');

                                $("#tblOrderList tbody").append(row);
                                row = $("#tblOrderList tbody tr:last").clone(true);
                            }
                        } else {
                            $("#EmptyTable").show();
                        }
                        var paging = AllDropDownList.Paging;
                        for (var k = 0; k < paging.length; k++) {
                            var pager = paging[k];
                            $(".Pager").ASPSnippets_Pager({
                                ActiveCssClass: "current",
                                PagerCssClass: "pager",
                                PageIndex: parseInt(pager.PageIndex),
                                PageSize: parseInt(pager.PageSize),
                                RecordCount: parseInt(pager.RecordCount)
                            });
                        }
                       
                    }
                }
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function print_NewOrder(e) {
    window.open("/Packer/OrderPrint.html?OrderAutoId=" + $(e).closest("tr").find(".orderNo span").attr("OrderAutoId"), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    setTimeout(function () {
        getOrderList(1);
    }, 2000);
}
$("#ddlPageSize").change(function () {
    getCreditList(1);
});
$("#btnSearch").click(function () {
    getCreditList(1);
})
function deleteOrder(CreditAutoId) {

    $.ajax({
        type: "POST",
        url: "POS_CreditMemoList.aspx/deleteOrder",
        data: "{'CreditAutoId':" + CreditAutoId + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == 'Session Expired') {
                location.href = '/';
            } else if (response.d == 'true') {
                swal("", "Credit memo deleted successfully.", "success");
                getCreditList(1);
            } else {
                swal("Error !", response.d, "error");
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function deleterecord(CreditAutoId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete credit memo.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: false,
            },
            confirm: {
                text: "Yes, delete it.",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            deleteOrder(CreditAutoId);

        }
        else {
            swal("", "Credit Memo is safe.", "error");
        }
    })
}
function sortBy(sortCode) {
    CreditAutoId = $("#CreditLogAutoId").val();
    viewOrderLog(CreditAutoId, sortCode);
}
function viewOrderLog(CreditAutoId, sortInCode) {
    $("#CreditLogAutoId").val(CreditAutoId);
    if (sortInCode == 1) {
        $(".la-arrow-down").show();
        $(".la-arrow-up").hide();
    } else if (sortInCode == 2) {
        $(".la-arrow-down").hide();
        $(".la-arrow-up").show();
    }
    $.ajax({
        type: "POST",
        url: "POS_CreditMemoList.aspx/viewOrderLog",
        data: "{'CreditAutoId':" + CreditAutoId + ",'sortInCode':" + sortInCode + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == 'Session Expired') {
                location.href = '/';
            } else {
                var xmldoc = $.parseXML(response.d);
                var order = $(xmldoc).find("Table");
                var orderlog = $(xmldoc).find("Table1");

                $("#lblOrderNo").text(order.find("CreditNo").text());
                $("#lblOrderDate").text(order.find("CreditDate").text());

                $("#tblOrderLog tbody tr").remove();
                var row = $("#tblOrderLog thead tr").clone(true);
                if (orderlog.length > 0) {
                    $("#EmptyTable").hide();
                    $.each(orderlog, function (index) {
                        $(".SrNo", row).html(index + 1);
                        $(".ActionBy", row).html($(this).find("EmpName").text());
                        $(".Date", row).html($(this).find("LogDate").text());
                        $(".Action", row).html($(this).find("Action").text());
                        $(".Remark", row).html($(this).find("Remarks").text());

                        $("#tblOrderLog tbody").append(row);
                        row = $("#tblOrderLog tbody tr:last").clone(true);
                    });
                }
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
    $("#modalOrderLog").modal('show');
}