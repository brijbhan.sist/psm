﻿var getid;
var Stock, AvailableAllocatedQty = 0, RequiredQty = 0, PrdtId = 0, j = 0;
$(document).ready(function () {
    $('#panelProduct input').attr('disabled', true);
    $('#panelProduct select').attr('disabled', true);
    $('#panelProduct button').attr('disabled', true);
    $('#TxtCheckDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    var d = new Date();
    CurrentDate = (d.getMonth() + 1) + "/" + d.getDate() + "/" + d.getFullYear();
    $('#txtOrderDate').val(CurrentDate);
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    var DraftAutoId = getQueryString('DraftAutoId');
    drtAutoId = DraftAutoId;
    bindDropdown();
    readCurrencyDetails();
 if (DraftAutoId != null) {
        $('#DraftAutoId').val(DraftAutoId);
        DraftOrder(DraftAutoId);
    }
    $('#txtScanBarcode').focus();
});

var MLQtyRate = 0.00, MLTaxType = 0, WeightOzTex = 0;
function bindDropdown() {
    $.ajax({
        type: "POST",
        url: "RegularPOSOrder.aspx/bindDropdown",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            if (response.d != "Session Expired") {
                var getData = $.parseJSON(response.d);
                $("#ddlCustomer option:not(:first)").remove();
                $.each(getData[0].CustomerList, function (index, item) {
                    $("#ddlCustomer").append("<option value='" + item.A + "'>" + item.C + "</option>");
                });
                $("#ddlCustomer").select2();
                $("#ddlProduct option:not(:first)").remove();
                $.each(getData[0].ProductList, function (index, item) {
                    $("#ddlProduct").append("<option MLQty='" + item.ML + "' WeightOz='" + item.Oz + "' value='" + item.AutoId + "'>" + item.PN + "</option>");
                });
                $("#ddlProduct").select2();
                $("#ddlShippingType option:not(:first)").remove();

                $.each(getData[0].ShippingType, function (index, item) {
                    $("#ddlShippingType").append("<option taxEnabled='" + item.EnabledTax + "' value='" + item.AutoId + "'>" + item.ST + "</option>");

                });
                $("#ddlShippingType").val('8');

                $("#ddlPaymentMenthod option:not(:first)").remove();
                $.each(getData[0].PaymentList, function (index, item) {
                    $("#ddlPaymentMenthod").append("<option value='" + item.AutoId + "'>" + item.PM + "</option>");
                });
            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(result.d);
        },
        error: function (result) {
            console.log(result.d);
        }
    });
}
function DraftOrder(DraftAutoId) {

    $.ajax({
        type: "POST",
        url: "RegularPOSOrder.aspx/EditDraftOrder",
        data: "{'DraftAutoId':'" + DraftAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var Order = $(xmldoc).find("Table");
                $("#txtOrderDate").val($(Order).find('OrderDate').text());
                $("#txtDeliveryDate").val($(Order).find('DeliveryDate').text());
                $("#txtOrderRemarks").val($(Order).find('Remarks').text());
                if ($(Order).find('ShippingType').text() != '' && $(Order).find('ShippingType').text() != '0') {
                    $("#ddlShippingType").val($(Order).find('ShippingType').text());
                }
                else {
                    $("#ddlShippingType").val('9');
                }
                $("#txtOrderStatus").val($(Order).find('Status').text());
                if ($(Order).find('CustomerAutoId').text().length > 0) {
                    $("#ddlCustomer").val($(Order).find('CustomerAutoId').text()).change();
                    $("#ddlCustomer").attr('disabled', true);
                }
                var items = $(xmldoc).find("Table1");
                $("#tblProductDetail body tr").remove();
                var row = $("#tblProductDetail thead tr").clone(true);
                if (items.length > 0) {
                    $('#emptyTable').hide();
                }

                $.each(items, function () {
                    $("#tblProductDetail tbody").append(row);
                    MLQty = parseFloat($(this).find("UnitMLQty").text()).toFixed(2) || 0.00;
                    WeightOz = parseFloat($(this).find("WeightOz").text()).toFixed(2) || 0.00;
                    $(".ProId", row).html($(this).find("ProductId").text() + "<span isfreeitem=" + $(this).find("isFreeItem").text() + "></span>");
                    if (Number($(this).find("isFreeItem").text()) == 1) {
                        $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span>" + " <product class='badge badge badge-pill badge-success'>Free</product>");
                        MLQty = 0.00;
                        WeightOz = 0.00;
                    } else if (Number($(this).find("IsExchange").text()) == 1) {
                        $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span>" + " <product class='badge badge badge-pill badge-primary'>Exchange</product>");
                        MLQty = 0.00;
                        WeightOz = 0.00;
                    }
                    else if (Number($(this).find("IsTaxable").text()) == 1) {
                        $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span>" + " <product class='badge badge badge-pill badge-danger'>Taxable</product>");
                    } else {
                        $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span>");
                    }


                    $(".UnitType", row).html("<span UnitAutoId='" + $(this).find("UnitAutoId").text() + "' QtyPerUnit='" + $(this).find("QtyPerUnit").text() + "'>" + $(this).find("UnitType").text() + " (" + $(this).find("QtyPerUnit").text() + " pcs" + ")</span>");
                    $(".ReqQty", row).html("<input type='text' maxlength='6' onchange='CheckAllocatedQty(this)' class='form-control input-sm border-primary text-center' onkeypress='return isNumberKey(event)' onkeyup='rowCal(this,1)' style='width:70px;' findoldqty='" + $(this).find("ReqQty").text() + "' value='" + $(this).find("ReqQty").text() + "'/>");
                    $(".Barcode", row).text("");
                    $(".QtyShip", row).html("<input type='text' class='form-control input-sm' style='width:70px;' value='0' onkeyup='rowCal(this,1);shipVsReq(this)' />");
                    $(".TtlPcs", row).text(Number($(this).find('QtyPerUnit').text()) * Number($(this).find('ReqQty').text()));
                    $(".SRP", row).text($(this).find("SRP").text());
                    $(".GP", row).text($(this).find("GP").text());
                    if (Number($(this).find("TaxRate").text()) == 1) {
                        $(".TaxRate", row).html('<span typetax="1" MLQty="' + MLQty + '" weightoz="' + WeightOz + '"></span>' + '<TaxRate style="font-weight: bold" class="la la-check-circle success center"></TaxRate>');
                    } else {
                        $(".TaxRate", row).html('<span typetax="0" MLQty="' + MLQty + '" weightoz="' + WeightOz + '"></span>');
                    }

                    if (Number($(this).find("IsExchange").text()) == 1) {
                        $(".UnitPrice", row).html("<input type='text' class='form-control input-sm border-primary' maxlength='6' onkeyup='changePrice(this)' onkeypress='return isNumberDecimalKey(event,this)'  style='width:70px;text-align:right;' value='0.00' minprice='" + $(this).find("minprice").text() + "' BasePrice='" + $(this).find("UnitPrice").text() + "' disabled />");
                        $(".IsExchange", row).html('<span isexchange="1"></span>' + '<IsExchange style="font-weight: bold" class="la la-check-circle success center"></IsExchange>');
                    } else if (Number($(this).find("isFreeItem").text()) == 1) {
                        $(".UnitPrice", row).html("<input type='text' class='form-control input-sm border-primary' maxlength='6' onkeyup='changePrice(this)' onkeypress='return isNumberDecimalKey(event,this)'  style='width:70px;text-align:right;' value='0.00' minprice='" + $(this).find("minprice").text() + "' BasePrice='" + $(this).find("UnitPrice").text() + "' disabled />");
                        $(".IsExchange", row).html('<span isexchange="0"></span>');
                    }
                    else {
                        $(".UnitPrice", row).html("<input type='text' class='form-control input-sm border-primary' maxlength='6' onkeyup='changePrice(this)' onkeypress='return isNumberDecimalKey(event,this)'  style='width:70px;text-align:right;' value='" + $(this).find("UnitPrice").text() + "' minprice='" + $(this).find("minprice").text() + "' BasePrice='" + $(this).find("UnitPrice").text() + "' />");
                        $(".IsExchange", row).html('<span isexchange="0"></span>');
                    }
                    $(".OM_MinPrice", row).text($(this).find("minprice").text());
                    $(".OM_CostPrice", row).text($(this).find("CostPrice").text());
                    $(".NetPrice", row).text($(this).find("NetPrice").text());
                    $(".Action", row).html("<a href='javascript:;' id='deleterow' onclick='deleterow(this)'><span class='ft-x'></span></a>");
                    $('#tblProductDetail tbody').append(row);
                    row = $("#tblProductDetail tbody tr:last").clone(true);

                });

                calTotalAmount();
                calGrandTotal();
                calOverallDisc();
                calTotalTax();
                $("#txtShipping").val($(Order).find('ShippingCharges').text());
                $("#txtDiscAmt").val($(Order).find('OverallDiscAmt').text());
                $("#txtOverallDisc").val($(Order).find('OverallDisc').text());

            } else {
                location.href = '/';
            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

var BUnitAutoId = 0;
var validcheck = 0;
function readBarcode() {
    $("#txtBarcode").focus();
    var Barcode = $("#txtBarcode").val();
    if (Barcode != "") {
        var chkIsTaxable = $('#chkIsTaxable').prop('checked');
        var chkExchange = $('#chkExchange').prop('checked');
        var chkFreeItem = $('#chkFreeItem').prop('checked');
        var IsExchange = 0, IsTaxable = 0, IsFreeItem = 0, chkcount = 0;

        if (chkIsTaxable == true) {
            IsTaxable = 1;
            chkcount = 1;
        }
        if (chkExchange == true) {
            IsExchange = 1;
            chkcount = chkcount + 1;
        }
        if (chkFreeItem == true) {
            IsFreeItem = 1;
            chkcount = chkcount + 1;
        }
        if (chkcount > 1) {
            toastr.error('Please check only one checkbox.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            $("#txtBarcode").val('');
            $("#txtBarcode").focus();
            return;
        }
        var data = {
            CustomerAutoId: $("#ddlCustomer").val(),
            Barcode: Barcode,
            DraftAutoId: $("#DraftAutoId").val(),
            ShippingType: $('#ddlShippingType  option:selected').val(),
            DeliveryDate: "",
            ReqQty: $("#txtReqQty").val(),
            IsTaxable: IsTaxable,
            IsExchange: IsExchange,
            IsFreeItem: IsFreeItem,
            Remarks: $('#txtOrderRemarks').val(),
        }
        $.ajax({
            type: "POST",
            url: "RegularPOSOrder.aspx/GetBarCodeDetails",
            data: "{'dataValues':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                $('#txtBarcode').attr('onchange', 'invalidBarCode()');
                if (response.d == 'false') {
                    $("#Stock")[0].play();
                    swal("Error!", "Stock not available.", "error").then(function () {
                        $("#txtBarcode").val("");
                        $("#txtBarcode").focus();
                        $("#txtBarcode").attr('onchange', 'readBarcode()');
                    });
                    $("#txtBarcode").focus();
                }
                else {
                    var xmldoc = $.parseXML(response.d);
                    var product = $(xmldoc).find("Table");
                    if (product.length > 0) {
                        if ($(product).find('Message').text() == "Less") {
                            $("#mdAllowQtyConfirmation").modal('show');
                            AvailableAllocatedQty = $(product).find('AvailableQty').text();
                            RequiredQty = $(product).find('RequiredQty').text();
                            PrdtId = $(product).find('ProductAutoId').text();
                            $("#txtBarcode").val("");
                            $("#txtBarcode").focus();
                            $("#txtBarcode").attr('onchange', 'readBarcode()');
                        }
                        else {
                            var productAutoId = $(product).find('ProductAutoId').text();
                            var unitAutoId = $(product).find('UnitType').text();
                            var TotalProductStock = $(product).find('ProductStock').text();
                            $("#DraftAutoId").val($(product).find('DraftAutoId').text());
                            var flag1 = false; var showmsg = 0;
                            $("#tblProductDetail tbody tr").each(function () {
                                if ($(this).find(".ProName > span").attr("productautoid") == productAutoId && $(this).find(".UnitType > span").attr("unitautoid") == unitAutoId
                                    && $(this).find(".IsExchange > span").attr("IsExchange") == IsExchange && $(this).find(".ProId > span").attr("IsFreeItem") == IsFreeItem) {
                                    var Qtyreq = $("#txtReqQty").val() || 0;
                                    var reqQty = Number($(this).find(".ReqQty input").val()) + Number(Qtyreq);
                                    var TQty = parseInt($(this).find(".UnitType > span").attr("qtyperunit")) * reqQty;
                                    if (TotalProductStock >= TQty) {
                                        $(this).find(".ReqQty input").val(reqQty);
                                        $(this).find(".ReqQty input").attr("FindOldQty", reqQty);
                                    } else {
                                        showmsg = 1;
                                        $("#Stock")[0].play();
                                        swal("Error!", "Stock not available.", "error").then(function () {
                                            $("#txtBarcode").val("");
                                            $("#txtBarcode").focus();
                                            $("#txtBarcode").attr('onchange', 'readBarcode()');
                                        });
                                        $("#txtBarcode").focus();

                                    }
                                    flag1 = true;
                                    rowCal($(this).find(".ReqQty > input"), 0);
                                    $('#tblProductDetail tbody tr:first').before($(this));
                                }

                                if ($(this).find(".ProName span").attr('ProductAutoId') == productAutoId && $(this).find(".UnitType span").attr('UnitAutoId') != unitAutoId
                                    && $(this).find(".ProId > span").attr("isfreeitem") == IsFreeItem && $(this).find(".ProId > span").attr("IsExchange") == '0' && IsExchange == 0) {

                                    showmsg = 4;
                                }
                                if ($(this).find(".ProName span").attr('ProductAutoId') == productAutoId && $(this).find(".UnitType span").attr('UnitAutoId') != unitAutoId
                                    && $(this).find(".IsExchange > span").attr("IsExchange") == IsExchange && $(this).find(".ProId > span").attr("isfreeitem") == '0' && IsFreeItem == 0) {
                                    showmsg = 4;
                                }
                            });

                            if (showmsg == 4) {
                                swal("", "You can't add different unit of added product.", "error");
                                $("#txtBarcode").val("");
                                $("#txtBarcode").focus();
                                $("#txtBarcode").attr('onchange', 'readBarcode()');
                                return;
                            }
                            if (!flag1) {

                                var row = $("#tblProductDetail thead tr").clone(true);
                                $(".ProId", row).html($(product).find('ProductId').text() + "<span IsFreeItem='" + IsFreeItem + "'></span>");
                                if (chkFreeItem == true) {
                                    $(".ProName", row).html("<span ProductAutoId='" + productAutoId + "'>" + $(product).find('ProductName').text() + "</span>" + " <product class='badge badge badge-pill badge-success'>Free</product>");

                                } else if (IsExchange == true) {
                                    $(".ProName", row).html("<span ProductAutoId='" + productAutoId + "'>" + $(product).find('ProductName').text() + "</span>" + " <product class='badge badge badge-pill badge-primary'>Exchange</product>");

                                } else if (IsTaxable == true) {
                                    $(".ProName", row).html("<span ProductAutoId='" + productAutoId + "'>" + $(product).find('ProductName').text() + "</span>" + " <product class='badge badge badge-pill badge-danger'>Taxable</product>");

                                } else {
                                    $(".ProName", row).html("<span ProductAutoId='" + productAutoId + "'>" + $(product).find('ProductName').text() + "</span>");
                                }
                                $(".UnitType", row).html("<span UnitAutoId='" + unitAutoId + "' QtyPerUnit='" + $(product).find('UnitQty').text() + "'>" + $(product).find('UnitName').text() + "</span>");
                                $(".ReqQty", row).html("<input type='text' onchange='CheckAllocatedQty(this)' maxlength='6' onkeypress='return isNumberKey(event)' class='form-control input-sm border-primary text-center' onkeyup='rowCal(this,1)' style='width:70px;' value='" + $("#txtReqQty").val() + "' FindOldQty='" + $("#txtReqQty").val() + "'/>");
                                $(".Barcode", row).text("");
                                $(".QtyShip", row).html("<input type='text' class='form-control input-sm border-primary' onkeypress='return isNumberKey(event)' style='width:70px;' disabled value='0' onkeyup='rowCal(this,1);shipVsReq(this)' />");
                                $(".TtlPcs", row).text(parseInt($("#txtReqQty").val()) * $(product).find('UnitQty').text());
                                if ($('#chkExchange').prop('checked') == false && $('#chkFreeItem').prop('checked') == false) {
                                    if ($(product).find('CustomPrice').text() == "") {
                                        $(".UnitPrice", row).html("<input type='text' maxlength='6' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm border-primary' onkeyup='changePrice(this)' style='width:70px;text-align:right;' value='" + $(product).find('Price').text() + "' minprice='" + $(product).find('MinPrice').text() + "' BasePrice='" + $(product).find('Price').text() + "' />");
                                    } else {
                                        $(".UnitPrice", row).html("<input type='text' maxlength='6' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm border-primary' onkeyup='changePrice(this)' style='width:70px;text-align:right;' value='" + (parseFloat($(product).find('CustomPrice').text()) >= parseFloat($(product).find('MinPrice').text()) ? $(product).find('CustomPrice').text() : $(product).find('Price').text()) + "' minprice='" + $(product).find('MinPrice').text() + "' BasePrice='" + $(product).find('Price').text() + "' />");
                                    }
                                } else {
                                    $(".UnitPrice", row).html("<input type='text' maxlength='6' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm border-primary' onkeyup='changePrice(this)' style='width:70px;text-align:right;' value='0.00' minprice='0.00' BasePrice='0.00' disabled/>");
                                }
                                $(".SRP", row).text($(product).find('SRP').text());
                                $(".GP", row).text($(product).find('GP').text());
                                $(".OM_MinPrice", row).text($(product).find('MinPrice').text());
                                $(".OM_CostPrice", row).text($(product).find('CostPrice').text());
                                $(".OM_BasePrice", row).text($(product).find('BasePrice').text());
                                var taxType = '', TypeTax = 0;
                                var MLQty = $(product).find('MLQty').text();
                                if ($('#chkExchange').prop('checked') == true || $('#chkFreeItem').prop('checked') == true) {
                                    MLQty = 0.00;
                                }
                                if ($('#chkIsTaxable').prop('checked') == true) {
                                    taxType = 'Taxable';
                                    TypeTax = 1;
                                }
                                var IsExchange1 = '';
                                if ($('#chkExchange').prop('checked') == true) {
                                    IsExchange1 = 'Exchange';
                                }


                                if (TypeTax == 1) {
                                    $(".TaxRate", row).html("<span TypeTax='" + TypeTax + "' MLQty='" + MLQty + "' WeightOz='" + $(product).find('WeightOz').text() + "'> </span>" + '<TaxRate style="font-weight: bold" class="la la-check-circle success center"></TaxRate>');
                                } else {
                                    $(".TaxRate", row).html("<span TypeTax='" + TypeTax + "' MLQty='" + MLQty + "' WeightOz='" + $(product).find('WeightOz').text() + "'> </span>");
                                }
                                if (IsExchange == 1) {
                                    $(".IsExchange", row).html("<span IsExchange='" + IsExchange + "'> </span>" + '<IsExchange style="font-weight: bold" class="la la-check-circle success center"></IsExchange>');
                                } else {
                                    $(".IsExchange", row).html("<span IsExchange='" + IsExchange + "'> </span>");
                                }
                                $(".NetPrice", row).text("0.00");
                                $(".QtyRemain", row).text("0");
                                $(".Action", row).html("<a href='javascript:;' id='deleterow' onclick='deleterow(this)'><span class='ft-x'></span></a>");
                                if ($('#tblProductDetail tbody tr').length > 0) {
                                    $('#tblProductDetail tbody tr:first').before(row);
                                }
                                else {
                                    $('#tblProductDetail tbody').append(row);
                                }

                                rowCal(row.find(".ReqQty > input"), 0);
                                $("#txtQuantity, #txtTotalPieces").val("0");
                                if ($('#tblProductDetail tbody tr').length > 0) {
                                    $('#emptyTable').hide();
                                    $("#ddlCustomer").attr('disabled', true);
                                }
                                $("#txtBarcode").val('');
                                //$('#chkIsTaxable').prop('checked', false);
                                //$('#chkExchange').prop('checked', false);
                                //$('#chkFreeItem').prop('checked', false);
                                $('#chkFreeItem').removeAttr('disabled');
                                $("#txtBarcode").val('');
                                $("#txtBarcode").focus();
                                $("#txtBarcode").attr('onchange', 'readBarcode()');
                                //$("#txtBarcode").attr('onchange', 'invalidBarCode()');
                            } else {
                                $("#txtBarcode").attr('onchange', 'readBarcode()');
                            }
                            $("#txtBarcode").val('');
                            $("#txtBarcode").focus();
                            var msg = true
                        }
                    }
                    else {
                        msg = false
                        $("#yes_audio")[0].play();
                        swal("Error!", "Barcode does not exists.", "error").then(function () {
                            $("#txtBarcode").val("");
                            $("#txtBarcode").focus();
                            $("#txtBarcode").attr('onchange', 'readBarcode()');
                        });
                        $("#panelProduct select").val(0);
                        $("#txtQuantity, #txtTotalPieces").val("0");
                        $("#alertBarcodeCount").hide();
                        $("#txtBarcode").val('');
                        $("#txtBarcode").focus();
                    }
                    if (msg == true && showmsg == 0) {
                        $("#txtReqQty").val(1);
                        toastr.success('Product added successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    }
                    else {
                        $("#txtReqQty").val(1);
                    }
                }
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    } else {
        $("#ddlProduct").val(0).change();
        $("#alertBarcodeCount").hide();
        $("#ddlProduct").attr('disabled', false);
        $("#ddlUnitType").attr('disabled', false);
    }
}

function invalidBarCode() {
    $("#txtBarcode").val('');
    $("#txtBarcode").focus();
}
var price, taxRate, SRP, minPrice, CustomPrice, GP; // ---------- Global Variables ---------------

function ChangeUnit() {


    $('#chkExchange').prop('checked', false);
    $('#chkIsTaxable').prop('checked', false);
    $('#chkFreeItem').prop('checked', false);
    if ($("#ddlUnitType option:selected").attr('eligibleforfree') == 1) {
        $('#chkFreeItem').attr('disabled', false);
    } else {
        $('#chkFreeItem').attr('disabled', true);
    }
    $("#alertStockQty").hide();
    var data = {
        ProductAutoId: $("#ddlProduct").val(),
        UnitAutoId: $("#ddlUnitType").val(),
        CustAutoId: $("#ddlCustomer").val()
    };

    if (data.UnitAutoId != 0) {
        $.ajax({
            type: "POST",
            url: "RegularPOSOrder.aspx/selectQtyPrice",
            data: "{'dataValues':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d != "Session Expired") {
                    var xmldoc = $.parseXML(response.d);
                    var stockAndPrice = $(xmldoc).find("Table");
                    price = stockAndPrice.find("Price").text();
                    minPrice = stockAndPrice.find("MinPrice").text();
                    CostPrice = stockAndPrice.find("CostPrice").text();
                    CustomPrice = stockAndPrice.find("CustomPrice").text();
                    taxRate = stockAndPrice.find("TaxRate").text();
                    SRP = stockAndPrice.find("SRP").text();
                    GP = stockAndPrice.find("GP").text();
                    Stock = stockAndPrice.find("Stock").text();
                    if ($("#ddlUnitType").val() != 0) {
                        $("#alertStockQty").text('');
                        if (Number($("#ddlUnitType").val()) == 1) {
                            $("#alertStockQty").append(" [ Stock : " + $(stockAndPrice).find("Stock").text() + " case ] ," +
                                " [ Base Price : $" + price + " ]");
                        } else if (Number($("#ddlUnitType").val()) == 2) {
                            $("#alertStockQty").append(" [ Stock : " + $(stockAndPrice).find("Stock").text() + " box ] ," +
                                " [ Base Price : $" + price + " ]");
                        } else {
                            $("#alertStockQty").append(" [ Stock : " + $(stockAndPrice).find("Stock").text() + " pcs ] ," +
                                " [ Base Price : $" + price + " ]");
                        }
                        $("#alertStockQty").show();
                        validcheck = 0;

                    } else {
                        $("#alertStockQty").hide();
                    }
                } else {
                    location.href = '/';
                }

            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
};                                                 //Check Stock 

/*-------------------------------------------------------------------------------------------------------------------------------*/

//                                                          Add Row to table
/*-------------------------------------------------------------------------------------------------------------------------------*/
function AddItemList() {
    debugger;
    var chkIsTaxable = $('#chkIsTaxable').prop('checked');
    var chkExchange = $('#chkExchange').prop('checked');
    var chkFreeItem = $('#chkFreeItem').prop('checked');
    var MLQty = $("#ddlProduct option:selected").attr('MLQty');
    var weightoz = $("#ddlProduct option:selected").attr('weightoz');
    var reqQty = parseInt($("#txtReqQty").val()) || 0;
    var IsExchange = 0, IsTaxable = 0, IsFreeItem = 0;
    var chkcount = 0;

    if (reqQty == 0) {
        toastr.error('Please enter a valid quantity.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $("#txtReqQty").addClass('border-warning');
        return;
    }
    else {
        $("#txtReqQty").removeClass('border-warning');
    }
    if (chkIsTaxable == true) {
        IsTaxable = 1;
        chkcount = 1;
    }
    if (chkExchange == true) {
        IsExchange = 1;
        chkcount = chkcount + 1;
        MLQty = 0.00;
        weightoz = 0.00;
    }
    if (chkFreeItem == true) {
        IsFreeItem = 1;
        chkcount = chkcount + 1;
        MLQty = 0.00;
        weightoz = 0.00;
    }
    if (MLTaxType == "0") {
        MLQty = 0.00;
    }

    if (chkcount > 1) {
        toastr.error('Please check only one checkbox.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }

    var flag1 = false, validatecheck = true;

    if ($("#ddlProduct option:selected").val() == '0' && $("#ddlUnitType option:selected").val() == '0') {
        validatecheck = false;
    }

    var productAutoId = $("#ddlProduct option:selected").val();
    var unitAutoId = $("#ddlUnitType option:selected").val();
    var shwmsg = 0, oldqty = 0, freenotifi = 0;
    $("#tblProductDetail tbody tr").each(function () {
        if ($(this).find(".ProName > span").attr("productautoid") == productAutoId && $(this).find(".UnitType > span").attr("unitautoid") == unitAutoId && $(this).find(".ProId > span").attr("isfreeitem") == IsFreeItem
            && $(this).find(".IsExchange > span").attr("isexchange") == IsExchange) {
            var reqQty = Number($(this).find(".ReqQty input").val()) + Number($("#txtReqQty").val());
            var TQty = parseInt($("#ddlUnitType option:selected").attr("qtyperunit")) * reqQty;
            oldqty = $(this).find(".ReqQty input").attr("FindOldQty");
            if (parseInt($("#ddlUnitType option:selected").attr("prostock")) >= TQty) {
                $(this).find(".ReqQty input").val(reqQty);
                $(this).find(".ReqQty input").attr("FindOldQty", reqQty);
            } else {
                shwmsg = 1;
                $("#Stock")[0].play();
            }
            flag1 = true;
            $('#tblProductDetail tbody tr:first').before($(this));
        }

        if ($(this).find(".ProName span").attr('ProductAutoId') == productAutoId && $(this).find(".UnitType span").attr('UnitAutoId') != unitAutoId
            && $(this).find(".ProId > span").attr("isfreeitem") == IsFreeItem && $(this).find(".IsExchange > span").attr("isexchange") == '0' && IsExchange == 0) {
            shwmsg = 2;

        }
        if ($(this).find(".ProName span").attr('ProductAutoId') == productAutoId && $(this).find(".UnitType span").attr('UnitAutoId') != unitAutoId
            && $(this).find(".IsExchange > span").attr("isexchange") == IsExchange && $(this).find(".ProId > span").attr("isfreeitem") == '0' && IsFreeItem == 0) {
            shwmsg = 2;
        }
    });

    if (shwmsg == 1) {
        swal("Error!", "Stock not available.", "error");
        return;
    } else if (shwmsg == 2) {
        swal("", "You can't add different unit of added product.", "error");
        return;
    }
    if (validatecheck) {
        var addStock = 0;
        $("#tblProductDetail tbody tr").each(function () {
            if ($(this).find(".ProName > span").attr("productautoid") == $("#ddlProduct option:selected").val()) {
                addStock += parseInt($(this).find(".ReqQty > input").val()) * parseInt($(this).find(".UnitType > span").attr("qtyperunit"))
            }
        });

        if ((parseInt($("#ddlUnitType option:selected").attr("prostock")) - parseInt(addStock)) < (parseInt($("#txtReqQty").val()) * parseInt($("#ddlUnitType option:selected").attr("qtyperunit")))) {
            $("#Stock")[0].play();
            swal("Error!", "Stock not available.", "error");
        }
        else {
            var Draftdata = {
                DraftAutoId: $('#DraftAutoId').val(),
                CustomerAutoId: $('#ddlCustomer').val(),
                ShippingType: $('#ddlShippingType  option:selected').val(),
                productAutoId: productAutoId,
                unitAutoId: unitAutoId,
                ReqQty: $("#txtReqQty").val(),
                IsTaxable: chkIsTaxable,
                IsExchange: IsExchange,
                IsFreeItem: IsFreeItem,
                MLQty: MLQty,
                Remarks: $('#txtOrderRemarks').val()
            }
            $.ajax({
                type: "POST",
                url: "RegularPOSOrder.aspx/SaveDraftOrder",
                data: "{'dataValues':'" + JSON.stringify(Draftdata) + "'}",
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                async: true,
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    debugger;
                    if (response.d != "Session Expired") {
                        if (response.d == "false") {
                            return;
                        }
                        else {
                            var xmldoc = $.parseXML(response.d);
                            var ItemDetails = $(xmldoc).find("Table");
                            if (ItemDetails.length > 0) {
                                if ($(ItemDetails).find('Message').text() == "Less") {
                                    $("#tblProductDetail tbody tr").each(function () {
                                        if ($(this).find(".ProName > span").attr("productautoid") == $("#ddlProduct option:selected").val()) {
                                            $(this).find(".ReqQty > input").val(oldqty);
                                            $(this).find(".ReqQty > input").attr("findoldqty", oldqty);
                                            // rowCal($(this).find(".ReqQty > input"), oldqty);
                                            $("#ddlProduct").select2('val', '0');
                                            $("#ddlUnitType").val(0);
                                            $("#txtReqQty").val("1");
                                        }
                                    });
                                    $("#mdAllowQtyConfirmation").modal('show');
                                    AvailableAllocatedQty = $(ItemDetails).find('AvailableQty').text();
                                    RequiredQty = $(ItemDetails).find('RequiredQty').text();
                                    PrdtId = $(ItemDetails).find('ProductAutoId').text();
                                }
                                else {
                                    $('#DraftAutoId').val($(ItemDetails).find('DraftAutoId').text());
                                    if (!flag1) {
                                        var product = $("#ddlProduct option:selected").text().split("--");
                                        var row = $("#tblProductDetail thead tr").clone(true);
                                        $(".ProId", row).html(product[0] + "<span IsFreeItem='" + IsFreeItem + "'></span>");
                                        if (IsFreeItem == 1) {
                                            $(".ProName", row).html("<span ProductAutoId='" + $("#ddlProduct option:selected").val() + "'>" + product[1] + "</span>" + " <product class='badge badge badge-pill badge-success'>Free</product>");

                                        } else if (IsExchange == 1) {
                                            $(".ProName", row).html("<span ProductAutoId='" + $("#ddlProduct option:selected").val() + "'>" + product[1] + "</span>" + " <product class='badge badge badge-pill badge-primary'>Exchange</product>");
                                        }
                                        else if (IsTaxable == 1) {
                                            $(".ProName", row).html("<span ProductAutoId='" + $("#ddlProduct option:selected").val() + "'>" + product[1] + "</span>" + " <product class='badge badge badge-pill badge-danger'>Taxable</product>");
                                        } else {
                                            $(".ProName", row).html("<span ProductAutoId='" + $("#ddlProduct option:selected").val() + "'>" + product[1] + "</span>");
                                        }


                                        $(".UnitType", row).html("<span UnitAutoId='" + $("#ddlUnitType option:selected").val() + "' QtyPerUnit='" + $("#ddlUnitType option:selected").attr("QtyPerUnit") + "'>" + $("#ddlUnitType option:selected").text() + "</span>");
                                        if ($("#hiddenOrderStatus").val() == "3") {
                                            $(".ReqQty", row).html('<input type="text" maxlength="6"  onchange="CheckAllocatedQty(this)" onkeypress="return isNumberKey(event)" class="form-control input-sm border-primary text-center" value="' + $("#txtReqQty").val() + '"  FindOldQty="' + $("#txtReqQty").val() + '"/>');
                                        }
                                        else {
                                            $(".ReqQty", row).html('<input type="text" maxlength="6"  onchange="CheckAllocatedQty(this)" onkeypress="return isNumberKey(event)" class="form-control input-sm border-primary text-center"  onkeyup="rowCal(this,1)" style="width:70px;" value="' + $("#txtReqQty").val() + '"   FindOldQty="' + $("#txtReqQty").val() + '"/>');
                                        }
                                        $(".TtlPcs", row).text(parseInt($("#txtReqQty").val()) * parseInt($("#ddlUnitType option:selected").attr("QtyPerUnit")));
                                        if ($('#chkExchange').prop('checked') == false && $('#chkFreeItem').prop('checked') == false) {
                                            $(".UnitPrice", row).html("<input type='text' maxlength='6' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm border-primary' onkeyup='changePrice(this)' style='width:70px;text-align:right;' value=" + $(ItemDetails).find('UnitPrice').text() + " minprice=" + $(ItemDetails).find('minPrice').text() + " BasePrice=" + $(ItemDetails).find('BasePrice').text() + " />");
                                        } else {
                                            $(".UnitPrice", row).html("<input type='text' maxlength='6' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm border-primary' onkeyup='changePrice(this)' style='width:70px;text-align:right;' value='0.00' minprice='0.00' BasePrice='0.00' disabled/>");
                                        }
                                        $(".SRP", row).text(SRP);
                                        $(".GP", row).text(GP);
                                        $(".OM_MinPrice", row).text($(ItemDetails).find('minPrice').text());
                                        $(".OM_CostPrice", row).text(CostPrice);
                                        $(".OM_BasePrice", row).text(price);
                                        var taxType = '', TypeTax = 0;
                                        if ($('#chkIsTaxable').prop('checked') == true) {
                                            taxType = 'Taxable';
                                            TypeTax = 1;
                                        }
                                        var IsExchange1 = '';
                                        if ($('#chkExchange').prop('checked') == true) {
                                            IsExchange1 = 'Exchange'
                                        }

                                        if (TypeTax == 1) {
                                            $(".TaxRate", row).html("<span TypeTax='" + TypeTax + "'  MLQty='" + MLQty + "' weightoz='" + weightoz + "'></span>" + '<TaxRate style="font-weight: bold" class="la la-check-circle success center"></TaxRate>');
                                        } else {
                                            $(".TaxRate", row).html("<span TypeTax='" + TypeTax + "'  MLQty='" + MLQty + "' weightoz='" + weightoz + "'></span>");
                                        }
                                        if (IsExchange == 1) {
                                            $(".IsExchange", row).html("<span IsExchange='" + IsExchange + "'> </span>" + '<IsExchange style="font-weight: bold" class="la la-check-circle success center"></IsExchange>');
                                        } else {
                                            $(".IsExchange", row).html("<span IsExchange='" + IsExchange + "'> </span>");
                                        }
                                        $(".NetPrice", row).text("0.00");
                                        $(".Action", row).html("<a href='javascript:;' id='deleterow' onclick='deleterow(this)'><span class='ft-x'></span></a>");
                                        if ($('#tblProductDetail tbody tr').length > 0) {
                                            $('#tblProductDetail tbody tr:first').before(row);
                                        }
                                        else {
                                            $('#tblProductDetail tbody').append(row);
                                        }
                                        rowCal(row.find(".ReqQty > input"), 0);
                                    }
                                    if ($('#tblProductDetail tbody tr').length > 0) {
                                        $("#ddlCustomer").attr('disabled', true);
                                    }
                                    $('#chkIsTaxable').prop('checked', false);
                                    $('#chkExchange').prop('checked', false);
                                    $('#chkFreeItem').prop('checked', false);
                                    $("#txtBarcode").val('');
                                    $("#txtBarcode").focus();
                                    $("#ddlProduct").select2('val', '0');
                                    $("#ddlUnitType").val(0);
                                    $("#txtReqQty").val("1");
                                    $("#emptyTable").hide();
                                    toastr.success('Product added successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                                }
                                $("#tblProductDetail tbody tr").each(function () {
                                    if ($(this).find(".ProName > span").attr("productautoid") == productAutoId && $(this).find(".UnitType > span").attr("unitautoid") == unitAutoId && $(this).find(".ProId > span").attr("isfreeitem") == IsFreeItem && $(this).find(".IsExchange > span").attr("isexchange") == IsExchange) {
                                        rowCal($(this).find(".ReqQty > input"), 0);
                                    }
                                });

                                calTotalAmount();
                            }
                        }
                    } else {
                        location.href = '/';
                    }

                },
                error: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                },
                failure: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                }
            });
        }
    }
    else {
        toastr.error('All * fields are mandatory', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}

/*----------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------------------*/


//                                                          Calculations
/*-------------------------------------------------------------------------------------------------------------------------------*/
function rowCal(e, i) {
    var row = $(e).closest("tr");
    $('#txtStoreCreditAmount').val('0.00');
    var dataValue = {
        DraftAutoId: $('#DraftAutoId').val(),
        ProductAutoId: $(row).find('.ProName span').attr('productautoid'),
        UnitAutoId: $(row).find('.UnitType span').attr('unitautoid'),
        ReqQty: $(row).find('.ReqQty input').val(),
        UnitPrice: $(row).find('.UnitPrice input').val(),
        IsExchange: $(row).find('.IsExchange span').attr('IsExchange'),
        IsFreeItem: $(row).find('.ProId span').attr('IsFreeItem'),
        TotalReqQnty: ($(row).find('.UnitType span').attr('qtyperunit') * $(row).find('.ReqQty input').val()),
        Remarks: $("#txtOrderRemarks").val()

    }

    $.ajax({
        type: "POST",
        url: "RegularPOSOrder.aspx/EditQty",
        data: "{'dataValues':'" + JSON.stringify(dataValue) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == 'false') {
                var OldReqQty = Number(row.find(".ReqQty input").attr("findoldqty"));
                var OldTotalPiece = Number(row.find(".TtlPcs").val());
                if (i == 1) {
                    row.find(".ReqQty input").val(OldReqQty);
                    $("#Stock")[0].play();
                    swal("Error!", "Stock not available.", "error");
                    calTotalAmount();
                }

            } else {
                var totalPcs = (Number($(e).val()) * Number(row.find(".UnitType span").attr("QtyPerUnit")));
                row.find(".TtlPcs").text(totalPcs);
                row.find(".QtyRemain").text(Number(row.find(".ReqQty input").val()) - Number($(e).val()));
                price = Number(row.find(".UnitPrice input").val());
                var netPrice = (price * Number(row.find(".ReqQty input").val()));
                row.find(".NetPrice").text(netPrice.toFixed(2));
                calTotalAmount();

            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}
function changePrice(e) {
    var row = $(e).closest("tr");
    var dataValue = {
        DraftAutoId: $('#DraftAutoId').val(),
        ProductAutoId: $(row).find('.ProName span').attr('productautoid'),
        UnitAutoId: $(row).find('.UnitType span').attr('unitautoid'),
        ReqQty: $(row).find('.ReqQty input').val(),
        UnitPrice: $(row).find('.UnitPrice input').val(),
        IsExchange: $(row).find('.IsExchange span').attr('IsExchange'),
        TotalReqQnty: $(row).find('.TtlPcs ').text(),
        Remarks: $("#txtOrderRemarks").val()
    }

    $.ajax({
        type: "POST",
        url: "RegularPOSOrder.aspx/EditQty",
        data: "{'dataValues':'" + JSON.stringify(dataValue) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) { },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

    rowCal(row.find(".ReqQty > input"), 0);
    if (Number($(e).val()) < Number($(e).attr("minprice"))) {
        $(e).css("background-color", "#FF9149").addClass("PriceNotOk");
    } else {
        $(e).css("background-color", "#FFF").removeClass("PriceNotOk");
    }
}
function calTotalAmount() {
    var total = 0.00;
    $("#tblProductDetail tbody tr").each(function () {
        total += Number($(this).find(".UnitPrice input").val()) * Number($(this).find(".ReqQty input").val());
    });
    $("#txtTotalAmount").val(total.toFixed(2));
    discountataddItem();
}
function discountataddItem() {

    var factor = 0.00, discountamount = 0.00, totalamt = 0.00;
    if ($("#txtDiscAmt").val() == "") {
        $("#txtDiscAmt").val('0.00');
        $("#txtOverallDisc").val('0.00');
    }
    else if (parseFloat($("#txtDiscAmt").val()) > (parseFloat($("#txtTotalAmount").val()))) {
        $("#txtDiscAmt").val('0.00');
        $("#txtOverallDisc").val('0.00');
    }
    discountamount = Number($("#txtDiscAmt").val()) || 0.00;
    totalamt = Number($("#txtTotalAmount").val()) || 0.00;
    if (totalamt > 0) {
        factor = parseFloat((parseFloat(discountamount) / parseFloat(totalamt)) * 100).toFixed(2) || 0.00;
    }
    $("#txtOverallDisc").val(parseFloat(factor).toFixed(2));
    calTotalTax();
}
function calTotalTax() {

    var totalTax = 0.00, TaxValue = 0, qty;
    var MLQty = 0;
    var WeightOz = 0;
    $("#tblProductDetail tbody tr").each(function () {
        qty = Number($(this).find(".ReqQty input").val());
        if ($(this).find('.TaxRate span').attr('typetax') == 1) {
            var totalPrice = Number($(this).find(".UnitPrice input").val()) * qty;
            var Disc = totalPrice * Number($("#txtOverallDisc").val()) * 0.01;
            var priceAfterDisc = totalPrice - Disc;
            if ($('#ddlTaxType').val() != null && $('#ddlTaxType').val() != "") {
                TaxValue = $('#ddlTaxType option:selected').attr("TaxValue");
            }

            totalTax += priceAfterDisc * Number(TaxValue) * 0.01;
        }
        MLQty += parseFloat($(this).find('.TtlPcs').text()) * parseFloat($(this).find('.TaxRate span').attr('MLQty'));
        WeightOz += parseFloat($(this).find('.TtlPcs').text()) * parseFloat($(this).find('.TaxRate span').attr('weightoz'));
    });

    var taxenabledval = $("#ddlShippingType option:selected").attr('taxenabled');
    if (taxenabledval == 1) {
        $("#txtMLQty").val(MLQty.toFixed(2));
        $("#txtMLTax").val((parseFloat(MLQty) * parseFloat(MLQtyRate)).toFixed(2));
    } else {
        $("#txtMLQty").val("0");
        $("#txtMLTax").val("0.00");
    }
    $("#txtWeightQty").val(WeightOz.toFixed(2));
    $("#txtWeightTex").val((parseFloat(WeightOz) * parseFloat(WeightOzTex)).toFixed(2));
    $("#txtTotalTax").val(totalTax.toFixed(2));
    calGrandTotal();
}
function calOverallDisc1() {
    var DiscAmt = parseFloat($("#txtDiscAmt").val()) || 0.00;
    var TotalAmount = (Number($("#txtTotalAmount").val())) || 0.00;
    if (DiscAmt > TotalAmount) {
        $("#txtOverallDisc").val('0.00');
        $("#txtDiscAmt").val('0.00');
        toastr.error('Discount Amount should less or equal to order amount.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    } else {
        var per = (DiscAmt / TotalAmount) * 100;
        $("#txtOverallDisc").val(per.toFixed(2));

    }


    calTotalTax();
}
function calOverallDisc() {

    var factor = 0.00;
    if ($("#txtOverallDisc").val() == "") {
        $("#txtOverallDisc").val('0.00');
    }
    else if (parseFloat($("#txtOverallDisc").val()) > 100) {
        $("#txtOverallDisc").val('0.00');
        toastr.error('Discount % cannot be greater than 100.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
    factor = Number($("#txtOverallDisc").val()) * 0.01 || 0.00;

    $("#txtDiscAmt").val((Number($("#txtTotalAmount").val()) * factor).toFixed(2));
    calTotalTax();
}
function calGrandTotal() {

    var DiscAmt = 0.00;
    if ($("#txtDiscAmt").val() != "") {
        DiscAmt = parseFloat($("#txtDiscAmt").val())
    }
    var grandTotal = Number($("#txtTotalAmount").val()) - Number(DiscAmt) + Number($("#txtShipping").val()) + Number($("#txtTotalTax").val()) + Number($("#txtWeightTex").val()) + Number($("#txtMLTax").val());
    var round = Math.round(grandTotal);
    $("#txtAdjustment").val((round - grandTotal).toFixed(2));
    $("#txtGrandTotal").val(round.toFixed(2));
    paidAmount();
}

function PaidAmountUsed() {
    var paidamount = 0.00;
    if ($("#hdnPaidAmount").val() != '') {
        paidamount = $("#hdnPaidAmount").val();
    }
    var Balanceamount = 0.00;
    if ($("#txtOpenBalance").val() != '') {
        Balanceamount = $("#txtOpenBalance").val();
    }
    if (parseFloat($("#txtPaybleAmount").val()) < parseFloat($("#txtPaidAmount").val())) {
        toastr.error('Paid amount should be less than or equal to payable amount.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $("#txtPaidAmount").val(paidamount);
        $("#txtBalanceAmount").val(Balanceamount);
    }
    else {
        paidAmount();
    }
}

function paidAmount() {

    if ($("#txtPaidAmount").val() != "") {
        if (parseFloat($("#txtPaidAmount").val()) > 0) {
            $('#ddlPaymentMenthod').attr('disabled', false);
            $('#txtReferenceNo').attr('disabled', false);
        } else {
            $('#ddlPaymentMenthod').val('0');
            $('#txtReferenceNo').val('');
            $('#ddlPaymentMenthod').attr('disabled', true);
            $('#txtReferenceNo').attr('disabled', true);
            $("#SecCurrency").hide();
            $("#SecCheck").hide();
        }
    } else {
        $('#ddlPaymentMenthod').val('0');
        $('#txtReferenceNo').val('');
        $('#ddlPaymentMenthod').attr('disabled', true);
        $('#txtReferenceNo').attr('disabled', true);
        $("#SecCurrency").hide();
        $("#SecCheck").hide();
    }
    var grandTotal = parseFloat($("#txtGrandTotal").val());
    var DeductionAmount = 0.00;
    if ($("#txtDeductionAmount").val() != "") {
        DeductionAmount = parseFloat($("#txtDeductionAmount").val());
    }
    var StoreCreditAmount = 0.00;
    if ($("#txtStoreCreditAmount").val() != "") {
        if (parseFloat($("#txtStoreCreditAmount").val()) > 0) {
            StoreCreditAmount = parseFloat($("#txtStoreCreditAmount").val());
        }
    }
    var balanceAmount = 0.00;

    if (parseFloat(DeductionAmount) > parseFloat(grandTotal)) {
        payable = 0.00;
        $("#txtStoreCreditAmount").attr('disabled', true);
        $("#txtStoreCreditAmount").val(parseFloat(grandTotal).toFixed(2) - parseFloat(DeductionAmount).toFixed(2));
    } else {
        payable = parseFloat(grandTotal) - parseFloat(DeductionAmount);
        if (parseFloat(StoreCreditAmount) > 0) {
            payable = payable - parseFloat(StoreCreditAmount).toFixed(2);
        }
        if (parseFloat($("#CreditMemoAmount").html()).toFixed(2) > 0) {
            $("#txtStoreCreditAmount").attr('disabled', false);
        }
        else {
            $("#txtStoreCreditAmount").attr('disabled', true);
        }
    }
    $("#txtPaybleAmount").val(payable.toFixed(2));
    var DueAmount = 0.00;
    if ($('#txtPastDue').val() != "") {
        PastDue = $('#txtPastDue').val();
    }
    var OpenBalance = 0.00;
    OpenBalance = parseFloat(parseFloat(PastDue) + parseFloat(payable)).toFixed(2);
    $('#txtOpenBalance').val(parseFloat(OpenBalance).toFixed(2));
    var PaidAmount = 0.00;
    if ($("#txtPaidAmount").val() != "") {
        PaidAmount = parseFloat($("#txtPaidAmount").val()).toFixed(2);
    }
    $('#txtBalanceAmount').val((parseFloat(OpenBalance) - parseFloat(PaidAmount)).toFixed(2));
}
function changecredit() {
    var StoreCredit = $('#CreditMemoAmount').text();
    var StoreCreditAmount = 0.00;
    if ($('#txtStoreCreditAmount').val() != "") {
        StoreCreditAmount = $('#txtStoreCreditAmount').val()
    }
    var StoreCreditAmountUsed = 0.00;
    if ($("#hdnStoreCreditAmount").val() != "") {
        StoreCreditAmountUsed = $("#hdnStoreCreditAmount").val();
    }
    var GrandTotal = $('#txtGrandTotal').val();
    var DeductionAmount = $('#txtDeductionAmount').val();
    if (parseFloat(GrandTotal) < parseFloat(StoreCreditAmount)) {
        toastr.error('You can use maximum order amount.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $('#txtStoreCreditAmount').val('0.00');
        paidAmount();
    }
    else if (parseFloat(GrandTotal) >= parseFloat(DeductionAmount)) {
        if (parseFloat(StoreCreditAmount) > parseFloat(StoreCredit)) {
            toastr.error('You can use maximum "' + parseFloat(StoreCredit).toFixed(2) + '" Store Credit .', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            $('#txtStoreCreditAmount').val(parseFloat(StoreCredit).toFixed(2));
        }
        var checkamount = parseFloat(GrandTotal) - parseFloat(DeductionAmount);
        if (checkamount < StoreCreditAmount) {
            $('#txtStoreCreditAmount').val('0.00');
            toastr.error('You can use maximum payable Amount.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        }
        paidAmount();

    }

}

function CheckAllocatedQty(e) {
    debugger;
    var row = $(e).closest('tr');
    var Draftdata = {
        CustomerAutoId: $('#ddlCustomer').val(),
        productAutoId: $(row).find(".ProName span").attr('productautoid'),
        unitAutoId: $(row).find(".UnitType span").attr('unitautoid'),
        ReqQty: $(e).val()
    }
    $.ajax({
        type: "POST",
        url: "RegularPOSOrder.aspx/checkAllocationQty",
        data: "{'dataValue':'" + JSON.stringify(Draftdata) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            debugger;
            if (response.d != "Session Expired") {
                if (response.d == "false") {
                    swal("", "Oops,Something went wrong.Please try later.", "error");
                }
                else if (response.d == "Available") {
                    toastr.success('Product added successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                }
                else {
                    var xmldoc = $.parseXML(response.d);
                    var product = $(xmldoc).find("Table");
                    if (product.length > 0) {
                        if ($(product).find('Message').text() == "Less") {
                            $(e).val($(row).find(".ReqQty input").attr('findoldqty'));
                            rowCal($(row).find(".ReqQty > input"), $(row).find(".ReqQty input").attr('findoldqty'));
                            $("#mdAllowQtyConfirmation").modal('show');
                            AvailableAllocatedQty = $(product).find('AvailableQty').text();
                            RequiredQty = $(product).find('RequiredQty').text();
                            PrdtId = $(product).find('ProductAutoId').text();
                        }
                    }
                }
            } else {
                location.href = '/';
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function bindUnitType() {
    $("#alertStockQty").hide();
    if ($("#ddlProduct option:selected").attr('eligibleforfree') == 1) {
        $('#chkFreeItem').attr('disabled', false);
    } else {
        $('#chkFreeItem').attr('disabled', true);
    }
    if ($("#ddlProduct").val() !== '0') {
        $.ajax({
            type: "POST",
            url: "RegularPOSOrder.aspx/bindUnitType",
            data: "{'productAutoId':" + $("#ddlProduct").val() + "}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d != "Session Expired") {
                    var xmldoc = $.parseXML(response.d);
                    var unitType = $(xmldoc).find("Table");
                    var unitDefault = $(xmldoc).find("Table1");
                    var count = 0;
                    $("#ddlUnitType option:not(:first)").remove();
                    $.each(unitType, function () {
                        $("#ddlUnitType").append("<option EligibleforFree='" + $(this).find("EligibleforFree").text() + "' value='" + $(this).find("AutoId").text() + "' QtyPerUnit='" + $(this).find("Qty").text() + "' prostock='" + $(this).find("PStock").text() + "'>" + $(this).find("UnitType").text() + " (" + $(this).find("Qty").text() + " pcs" + ")</option>");
                    });
                    if (unitDefault.length > 0) {
                        if (BUnitAutoId == 0) {
                            $('#ddlUnitType option').each(function () {
                                if (this.value == $(unitDefault).find('AutoId').text()) {
                                    $("#ddlUnitType").val($(unitDefault).find('AutoId').text()).change();
                                }
                            });
                        } else {
                            $('#ddlUnitType option').each(function () {
                                if (this.value == BUnitAutoId) {
                                    $("#ddlUnitType").val(BUnitAutoId).change();
                                }
                            });
                            BUnitAutoId = 0;
                        }
                    } else {
                        $("#ddlUnitType").val(0).change();
                    }
                } else {
                    location.href = '/';
                }

            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });

    } else {
        $('#chkFreeItem').removeAttr('disabled');
        $("#ddlUnitType option:not(:first)").remove();
        $("#txtReqQty").val(1);
    }
}

function deleteItemrecord(e) {
    var tr = $(e).closest('tr');
    var data = {
        DraftAutoId: $('#DraftAutoId').val(),
        ProductAutoId: $(tr).find('.ProName span').attr('ProductAutoId'),
        UnitAutoId: $(tr).find('.UnitType span').attr('UnitAutoId'),
        isfreeitem: $(tr).find('.ProId span').attr('isfreeitem'),
        IsExchange: $(tr).find('.IsExchange span').attr('IsExchange')
    }
    if ($('#DraftAutoId').val() != '') {
        $.ajax({
            type: "POST",
            url: "RegularPOSOrder.aspx/DeleteItem",
            data: "{'DataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json;charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == "true") {
                    swal("", "Product deleted successfully.", "success");
                    $(e).closest('tr').remove();

                    calTotalAmount();
                    if ($("#tblProductDetail tbody tr").length == 0) {
                        $("#ddlCustomer").attr('disabled', false);
                        $("#emptyTable").show();
                    } else {
                        $("#ddlCustomer").attr('disabled', true);
                        $("#emptyTable").hide();
                    }
                }
                else if (result.d == "false") {
                    swal("", "Oops! Something went wrong.Please try later.", "success");
                }
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        })
    } else {
        $(e).closest('tr').remove();
        swal("", "Item deleted successfully.", "success");
    }



}

function deleterow(e) {
    swal({
        title: "Are you sure?",
        text: "You want to delete Product!",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: false,
            },
            confirm: {
                text: "Yes, delete it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            deleteItemrecord(e);

        } else {
            swal("", "Your Product is safe.", "error");
        }
    })
}

function BindAddressCustomer() {
    if ($("#ddlCustomer").val() == 0) {
        $('#panelProduct input').attr('disabled', true);
        $('#panelProduct select').attr('disabled', true);
        $('#panelProduct button').attr('disabled', true);
    } else {
        $('#panelProduct input').attr('disabled', false);
        $('#panelProduct select').attr('disabled', false);
        $('#panelProduct button').attr('disabled', false);
    }
    $.ajax({
        type: "POST",
        url: "RegularPOSOrder.aspx/BindAddressCustomer",
        data: "{'CustomerAutoId':" + $("#ddlCustomer").val() + ",'OrderAutoId':" + 0 + "}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var BillingAddress = $(xmldoc).find("Table");
                var ShippingAddress = $(xmldoc).find("Table1");
                var StoreCredit = $(xmldoc).find("Table2");
                var salesPerson = $(xmldoc).find("Table3");
                var CreditDetails = $(xmldoc).find("Table4");
                var CreditAmountDetails = $(xmldoc).find("Table5");
                var DuePayment = $(xmldoc).find("Table6");
                var TaxTypeMaster = $(xmldoc).find("Table7");
                var MLTaxMaster = $(xmldoc).find("Table8");
                var WeightOz = $(xmldoc).find("Table9");
                if ($(WeightOz).find("Value").text() != '')
                    WeightOzTex = $(WeightOz).find("Value").text();
                if ($(MLTaxMaster).find("TaxRate").text() != '')
                    MLQtyRate = $(MLTaxMaster).find("TaxRate").text();
                MLTaxType = $(MLTaxMaster).find("MLTaxType").text();
                $('#CustomerType').val($(salesPerson).find('CustomerType').text());
                if ($(salesPerson).find('CustomerType').text() == 3) {
                    $('.UnitPrice').hide();
                    $('.SRP').hide();
                    $('.GP').hide();
                    $('.NetPrice').hide();
                    $('#orderSummary').hide();
                    $("#drag-area11").hide();
                } else {
                    $('.UnitPrice').show();
                    $('.SRP').show();
                    $('.GP').show();
                    $('.NetPrice').show();
                    $('#orderSummary').show();
                    $("#drag-area11").show();
                }
                $("#ddlTaxType option").remove();
                $.each(TaxTypeMaster, function () {
                    $("#ddlTaxType").append("<option TaxValue='" + $(this).find("Value").text() + "' value='" + $(this).find("AutoId").text() + "'>" + $(this).find("TaxableType").text() + "-(" + $(this).find("Value").text() + ")</option>");
                });
                $("#tblduePayment tbody tr").remove();
                var rowdue = $("#tblduePayment thead tr").clone(true);
                var check = 0;
                var DueAmount = 0;
                if (DuePayment.length > 0) {
                    $(DuePayment).each(function (index) {
                        if (parseFloat($(this).find('AmtDue').text()) > 0) {
                            $(".SrNo", rowdue).text(Number(check) + 1);
                            $(".OrderNo", rowdue).text($(this).find('OrderNo').text());
                            $(".OrderDate", rowdue).text($(this).find('OrderDate').text());
                            $(".AmtDue", rowdue).html($(this).find('AmtDue').text());
                            DueAmount += parseFloat($(this).find('AmtDue').text());
                            $('#tblduePayment tbody').append(rowdue);
                            rowdue = $("#tblduePayment tbody tr:last").clone(true);
                            check = check + 1;
                        }
                    })
                    $('#CustomerDueAmount').modal('show');
                }
                $("#TotalDueAmount").html(parseFloat(DueAmount).toFixed(2));
                $('#StoreCredit').text(parseFloat(DueAmount).toFixed(2));
                $('#txtPastDue').val(parseFloat(DueAmount).toFixed(2));
                $('#txtOpenBalance').val(parseFloat(DueAmount).toFixed(2));
                $("#ddlBillingAddress option:not(:first)").remove();
                $.each(BillingAddress, function () {
                    if ($(this).find("IsDefault").text() == 0) {
                        $("#ddlBillingAddress").append("<option value='" + $(this).find("BillAddrAutoId").text() + "' >" + $(this).find("BillingAddress").text() + "</option>");
                    } else {
                        $("#ddlBillingAddress").append("<option selected value='" + $(this).find("BillAddrAutoId").text() + "' >" + $(this).find("BillingAddress").text() + "</option>");
                    }
                });
                $("#ddlShippingAddress option:not(:first)").remove();
                $.each(ShippingAddress, function () {
                    if ($(this).find("IsDefault").text() == 0) {
                        $("#ddlShippingAddress").append("<option value='" + $(this).find("ShipAddrAutoId").text() + "' >" + $(this).find("ShippingAddress").text() + "</option>");
                    } else {
                        $("#ddlShippingAddress").append("<option selected value='" + $(this).find("ShipAddrAutoId").text() + "' >" + $(this).find("ShippingAddress").text() + "</option>");
                    }
                });
                if (StoreCredit.length) {
                    if (parseFloat($(StoreCredit).find('CreditAmount').text()) > 0) {
                        $('#CreditMemoAmount').text($(StoreCredit).find('CreditAmount').text());
                        $('#txtStoreCreditAmount').attr('readonly', false);
                    } else {
                        $('#txtStoreCreditAmount').attr('readonly', true);
                    }

                } else {
                    $('#txtStoreCreditAmount').attr('readonly', true);
                }
                if (salesPerson.length) {
                    $('#salesPerson').val($(salesPerson).find('EMPNAME').text());
                } else {
                    $('#salesPerson').val('');
                }
                $("#tblCreditMemoList tbody tr").remove();
                if (CreditDetails.length > 0) {
                    var TotalDue = 0.00;
                    var rowc1 = $("#tblCreditMemoList thead tr").clone(true);
                    $.each(CreditDetails, function (index) {
                        $(".SRNO", rowc1).text(Number(index) + 1);
                        if ($(this).find('OrderAutoId').text() != '') {
                            $(".Action", rowc1).html("<input type='checkbox' OrderAutoId='" + $(this).find('OrderAutoId').text() + "' checked name='creditMemo' onclick='funcheckprop(this)' value=" + $(this).find("CreditAutoId").text() + "> ");
                        } else {
                            $(".Action", rowc1).html("<input type='checkbox' OrderAutoId='" + $(this).find('OrderAutoId').text() + "' name='creditMemo' onclick='funcheckprop(this)' value=" + $(this).find("CreditAutoId").text() + "> ");
                        }
                        $(".CreditNo", rowc1).html("<a target='_blank' href='/Sales/CreditMemo.aspx?PageId=" + $(this).find("CreditAutoId").text() + "'>" + $(this).find("CreditNo").text() + "</a>");
                        $(".CreditDate", rowc1).text($(this).find("CreditDate").text());
                        $(".ReturnValue", rowc1).text($(this).find("ReturnValue").text());
                        $(".CreditType", rowc1).text($(this).find("CreditType").text());
                        TotalDue = parseFloat(TotalDue) + parseFloat($(this).find("ReturnValue").text());
                        $("#tblCreditMemoList tbody").append(rowc1)
                        rowc1 = $("#tblCreditMemoList tbody tr:last").clone(true);
                    })
                    $("#TotalDue").text(TotalDue.toFixed(2));
                    $(".Action").show();
                    $('#CusCreditMemo').show();
                    $('#dCuCreditMemo').show();
                } else {
                    $('#CusCreditMemo').hide();
                    $('#dCuCreditMemo').hide();
                }
                $('#txtBarcode').focus();
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
/**************************************Attached Credit Memo***********************************/
function funcheckprop(e) {
    if ($("#txtStoreCreditAmount").val() == '') {
        $("#txtStoreCreditAmount").val('0.00');
    }
    var deductionAmount = 0.00;
    var GrandTotal = $("#txtGrandTotal").val();
    var payable = 0.00;

    $("#tblCreditMemoList tbody tr").each(function () {

        if ($(this).find('.Action input').prop('checked') == true) {
            deductionAmount += parseFloat($(this).find('.ReturnValue').text());
        }
    });

    $("#txtStoreCreditAmount").val('0.00');
    $("#txtDeductionAmount").val(parseFloat(deductionAmount).toFixed(2));

    if (parseFloat(deductionAmount) > parseFloat(GrandTotal)) {
        payable = 0.00;
        $("#txtStoreCreditAmount").attr('disabled', true);
        $("#txtStoreCreditAmount").val(parseFloat(GrandTotal).toFixed(2) - parseFloat(deductionAmount).toFixed(2));
    } else {
        payable = parseFloat(GrandTotal) - parseFloat(deductionAmount);
        if (parseFloat($("#CreditMemoAmount").html()).toFixed(2) > 0) {
            $("#txtStoreCreditAmount").attr('disabled', false);
        }
        else {
            $("#txtStoreCreditAmount").attr('disabled', true);
        }
    }

    $("#txtPaybleAmount").val(payable.toFixed(2));
    calTotalAmount();
}
/**************************************End Attached Cedit Memo********************************/


/*-------------------------------------------------------------------------------------------------------------------------------*/
//                                                          Generate Order
/*-------------------------------------------------------------------------------------------------------------------------------*/
function PrintInvoice(i) {
    $("#SavedMassege").modal('hide');
    printOrder_CustCopyNew(i);
}
function printOrder_CustCopy() {
    window.open("/Manager/OrderPrintCC.html?OrderAutoId=" + $("#txtHOrderAutoId").val() + "&Print=1", "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");

}
function PrintPackingSlip(i) {
    window.open("/Manager/OrderPrintCCF5.html?OrderAutoId=" + $("#txtHOrderAutoId").val() + "&Print=1", "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    if (i == 0)
        location.href = "/Warehouse/RegularPOSOrder.aspx";
}

function printOrder_CustCopyNew(i) {
    /// 
    if ($('#HDDomain').val() == 'psmpa') {


        $("#divPSMPADefault").show();
        $("#PSMPADefault").prop('checked', true);
    }
    else if ($('#HDDomain').val() == 'psmnpa') {//PSMNPA Default changed on 11/01/2019
        $("#divPSMNPADefault").show();
        $("#divPSMPADefault").hide();
        $("#rPSMNPADefault").prop('checked', true);
    }
    else {
        $("#chkdueamount").prop('checked', true);
    }
    $('#PopPrintTemplate input').attr('disabled', false);
    $('#PopPrintTemplate').modal('show');
    j = i;
}
function saveOrder() {
    if ($("#ddlShippingAddress").val() != '0' && $("#ddlBillingAddress").val() != '0') {
        $('#txtOverallDisc').removeClass('border-warning');
        $('#ddlCustomer').closest('div').find('.select2-selection--single').removeAttr('style');
        var flag1 = false, flag2 = false, flag3 = false;
        var grandTotal = parseFloat($("#txtGrandTotal").val());
        var DeductionAmount = 0.00;
        if ($("#txtDeductionAmount").val() != "") {
            DeductionAmount = parseFloat($("#txtDeductionAmount").val());
        }

        var StoreCredit = $('#CreditMemoAmount').text();
        var StoreCreditAmount = 0.00;
        var PaidAmount = parseInt($('#txtPaidAmount').val())
        var PaybleAmount = parseInt($('#txtPaybleAmount').val())      
        if ($('#txtStoreCreditAmount').val() != "") {
            StoreCreditAmount = $('#txtStoreCreditAmount').val()
        }
        if ($('#txtPaidAmount').val() == "" || Number($('#txtPaidAmount').val())==0) {
            toastr.error('Paid amount is required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            $("#txtPaidAmount").addClass('border-warning');
        }
        else if (parseFloat(PaidAmount) <  parseFloat(PaybleAmount))
        {
            $("#txtPaidAmount").addClass('border-warning');
            toastr.error('Paid amount should be equal to payable amount.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        }
        else if (parseFloat(StoreCreditAmount) > parseFloat(StoreCredit)) {
            toastr.error('You can use maximum "' + StoreCredit + '" Store Credit !!', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        }
        else if (Number($('#ddlCustomer').val()) != 0) {
            if ($('#tblProductDetail tbody tr').length > 0) {
                $("#tblProductDetail tbody tr").each(function () {
                    if ($(this).find(".ReqQty input").val() == "" || $(this).find(".ReqQty input").val() == null || parseInt($(this).find(".ReqQty input").val()) == 0) {
                        $(this).find(".ReqQty input").focus().addClass('border-warning');
                        flag1 = true;
                    } else {
                        $(this).find(".ReqQty input").blur().removeClass('border-warning');
                    }
                    if ($(this).find(".ProId span").attr('isfreeitem') == '0' && $(this).find(".IsExchange span").attr('isexchange') == '0') {
                        if (Number($(this).find(".UnitPrice input").val()) < Number($(this).find(".UnitPrice input").attr('minprice'))) {
                            flag2 = true;
                        }
                    }
                });

                if (!flag1 && !flag2 && !flag3) {
                    $("#alertDOrder").hide();
                    if (parseFloat($("#txtPaidAmount").val()) > 0) {
                        if ($("#ddlPaymentMenthod").val() == 0) {
                            toastr.error('Payment Method Required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                            return;
                        }
                        else {
                            OSave()
                        }
                    } else {
                        OSave();
                    }

                } else {
                    if (flag1) {
                        toastr.error('Required Quantity cannot be  left empty or Zero .', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

                    } else {
                        toastr.error('Unit price can not be less than min price.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    }
                }

            }
            else {
                toastr.error('No Product Added into the Table !!', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

            }
        } else {
            $('#ddlCustomer').closest('div').find('.select2-selection--single').attr('style', 'border:1px solid #FF9149  !important');
            toastr.error('Please select customer !!', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

        }
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
function OSave() {
    var TotalCurrencyAmount = parseFloat($("#txtTotalCurrencyAmount").val());
    var PaybleAmount = parseFloat($("#txtPaybleAmount").val());
    var PaymentCurrencyXml = '';
    var SortAmount = 0, curTotal = 0;
    if (parseFloat($('#txtOverallDisc').val()) > 100) {
        $('#txtOverallDisc').addClass('border-warning');
        toastr.error('Discount Amount should be less than 100%.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    var PayMenthod = parseInt($('#ddlPaymentMenthod').val())
    if (PayMenthod == 2) {
        if ($('#TxtCheckNo').val()=="") {
            $('#TxtCheckNo').addClass('border-warning');
            toastr.error('Check no are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            return;
        }
        if ($('#TxtCheckDate').val() == "") {
            $('#TxtCheckDate').addClass('border-warning');
            toastr.error('Check date are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            return;
        }
    }
    var flag1 = false, flag2 = false, flag3 = false;
    var grandTotal = parseFloat($("#txtGrandTotal").val());
    var DeductionAmount = 0.00;
    if ($("#txtDeductionAmount").val() != "") {
        DeductionAmount = parseFloat($("#txtDeductionAmount").val());
    }
    var CreditNo = '';
    $("#tblCreditMemoList tbody tr").each(function () {
        if ($(this).find('.Action input').prop('checked') == true) {
            CreditNo += $(this).find('.Action input').val() + ',';

        }
    })
    var StoreCredit = $('#CreditMemoAmount').text();
    var StoreCreditAmount = 0.00;
    if ((parseInt($('#ddlPaymentMenthod').val()) == '1')) {
        $("#tblCurrencyList tbody tr").each(function () {
            if ($(this).find(".NoofRupee input").val() != 0 && $(this).find(".NoofRupee input").val() != '') {
                PaymentCurrencyXml += '<PaymentCurrencyXml>';
                PaymentCurrencyXml += '<CurrencyAutoId><![CDATA[' + $(this).find('.CurrencyName span').attr('CurrencyAutoid') + ']]></CurrencyAutoId>';
                PaymentCurrencyXml += '<NoOfValue><![CDATA[' + $(this).find(".NoofRupee input").val() + ']]></NoOfValue>';
                PaymentCurrencyXml += '<TotalAmount><![CDATA[' + $(this).find(".Total").text() + ']]></TotalAmount>';
                PaymentCurrencyXml += '</PaymentCurrencyXml>';
                curTotal += parseFloat($(this).find(".CurrencyName span").attr("currencyvalue")) * parseFloat($(this).find(".NoofRupee input").val());
            }
        });
    }
    if ($('#txtStoreCreditAmount').val() != "") {
        StoreCreditAmount = $('#txtStoreCreditAmount').val()
    }
    if ($("#txtTotalCurrencyAmount").val() != "") {
        TotalCurrencyAmount = $("#txtTotalCurrencyAmount").val();
    }
    var orderData = {
        DraftAutoId: $('#DraftAutoId').val(),
        CustomerAutoId: $('#ddlCustomer').val(),
        ShippingType: $('#ddlShippingType').val(),
        TotalAmount: $("#txtTotalAmount").val(),
        OverallDisc: $("#txtOverallDisc").val(),
        OverallDiscAmt: $("#txtDiscAmt").val(),
        ShippingCharges: $("#txtShipping").val(),
        TotalTax: $("#txtTotalTax").val(),
        GrandTotal: $("#txtGrandTotal").val(),
        TaxType: $("#ddlTaxType").val(),
        PaidAmount: $("#txtPaidAmount").val(),
        CreditMemo: $("#txtDeductionAmount").val(),
        StoreCredit: $("#txtStoreCreditAmount").val(),
        PayableAmount: $("#txtPaybleAmount").val(),
        BalanceAmount: $("#txtBalanceAmount").val(),
        PaymentMenthod: $("#ddlPaymentMenthod").val(),
        ShippingAutoId: $("#ddlShippingAddress").val(),
        BillingAutoId: $("#ddlBillingAddress").val(),
        ReferenceNo: $("#txtReferenceNo").val(),
        Remarks: $("#txtOrderRemarks").val(),
        PastDue: $("#txtPastDue").val(),
        Adjustment: $("#txtAdjustment").val(),
        CheckNo: $("#TxtCheckNo").val(),
        CheckDate: $("#TxtCheckDate").val(),
        ChequeRemarks: $("#txtRemarks").val(),
        CreditNo: CreditNo,
        TotalCurrencyAmount: TotalCurrencyAmount,
        PaymentCurrencyXml: PaymentCurrencyXml
    };
    var Product = [];
    $("#tblProductDetail tbody tr").each(function () {

        Product.push({
            ProductAutoId: $(this).find('.ProName').find('span').attr('ProductAutoId'),
            UnitAutoId: $(this).find('.UnitType').find('span').attr('UnitAutoId'),
            QtyPerUnit: $(this).find('.UnitType').find('span').attr('QtyPerUnit'),
            QtyReq: $(this).find('.ReqQty input').val(),
            TotalPieces: $(this).find('.TtlPcs').text(),
            UnitPrice: $(this).find('.UnitPrice input').val(),
            SRP: $(this).find('.SRP').text() || 0,
            GP: $(this).find('.GP').text() || 0,
            Tax: $(this).find('.TaxRate span').attr('typetax'),
            IsExchange: $(this).find('.IsExchange').find('span').attr('IsExchange'),
            isFreeItem: $(this).find('.ProId').find('span').attr('isFreeItem'),
            NetPrice: $(this).find('.NetPrice').text(),
            OM_MinPrice: $(this).find('.OM_MinPrice').text(),
            OM_CostPrice: $(this).find('.OM_CostPrice').text(),
            OM_BasePrice: $(this).find('.OM_BasePrice').text()
        });
    });

    $.ajax({
        type: "Post",
        url: "RegularPOSOrder.aspx/insertOrderData",
        data: JSON.stringify({ orderData: JSON.stringify(orderData), ProductDetails: JSON.stringify(Product) }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (data) {
            if (data.d != "Session Expired") {
                if (data.d != "False") {
                    if ($('#CustomerType').val() == '3') {
                        $('#NoPrint').attr('onclick', 'PrintPackingSlip(0)');
                    } else {
                        $('#NoPrint').attr('onclick', 'PrintInvoice(1)');
                    }
                    var xmldoc = $.parseXML(data.d);
                    var tbl = $(xmldoc).find("Table");
                    if ($(tbl).find("Type").text() == "Available") {
                        resetOrder();
                        $("#txtHOrderAutoId").val($(tbl).find('OrderAutoId').text());
                        $("#SavedMassege").modal('show');
                    }
                    else if ($(tbl).find("Message").text() == "InvalidCRMemo") {
                        $("#tblCRMStatusList tbody tr").remove();
                        var row = $("#tblCRMStatusList thead tr").clone(true);
                        $.each(tbl, function () {
                            $(".CreditNo", row).text($(this).find("CreditNo").text());
                            $(".SalesAmount", row).text($(this).find("SalesAmount").text());
                            $(".Status", row).html('<span class="badge badge-sm badge-danger">' + $(this).find("Status").text() + '</span>');
                            $('#tblCRMStatusList tbody').append(row);
                            row = $("#tblCRMStatusList tbody tr:last").clone(true);
                        });
                        $("#CheckCreditMemo").modal('show');
                    }
                    else {
                        $("#tblProductList tbody tr").remove();
                        var row = $("#tblProductList thead tr").clone(true);
                        $.each(tbl, function () {
                            $(".Pop_ProId", row).text($(this).find("ProductId").text());
                            $(".Pop_ProName", row).text($(this).find("ProductName").text());
                            $(".Pop_RequiredQty", row).text($(this).find("TotalPieces").text());
                            $(".Pop_AvilStock", row).text($(this).find("Stock").text());
                            $('#tblProductList tbody').append(row);
                            row = $("#tblProductList tbody tr:last").clone(true);
                        });
                        $("#CheckStock").modal('show');
                    }

                } else {
                    swal("Error !", "Oops! Something went wrong.Please try later.", "error")
                }


            } else {
                location.href = '/';
            }
        },
        error: function (result) {
            swal("Error !", "Oops! Something went wrong.Please try later.", "error")
        },
        failure: function (result) {
            swal("Error !", "Oops! Something went wrong.Please try later.", "error")
        }
    });
}
function resetOrder() {
    $("input[type='text']").val('');
    var d = new Date();
    CurrentDate = (d.getMonth() + 1) + "/" + d.getDate() + "/" + d.getFullYear();
    $('#txtOrderDate').val(CurrentDate);
    $("#DraftAutoId").val(0);
    $("#btnaddCustomer").show();
    $("#btnChangeCustomer").hide();
    $("#ddlCustomer").removeAttr('disabled');
    $("#popBillAddr button").attr('disabled', true);
    $("#Small1 button").attr('disabled', true);
    $("#tblProductDetail tbody tr").remove();
    $("#tblCustDues tbody tr").remove();
    $("#tblTop25Products tbody tr").remove();
    $("#sumOrderValue").text('0.00');
    $("#sumPaid").text('0.00');
    $("#sumDue").text('0.00');
    $('#orderSummary input').val('0.00');
    $("#ddlCustomer").select2("val", "0");
    $("#ddlShippingAddress").val('0');
    $("#ddlBillingAddress").val('0');
    $("#ddlShippingType").val('0');
    $("#txtOrderRemarks").val('');
    $("#ddlProduct").val('0').change();
}
function nocreateorder() {
    location.href = '/POS/RegularPOSOrderList.aspx';
}
function createorder() {
    location.href = '/POS/RegularPOSOrder.aspx';
}
function updatedraftOrder() {

    if ($("#DraftAutoId").val() != '') {
        var Draftdata = {
            CustomerAutoId: $("#ddlCustomer").val(),
            DraftAutoId: $("#DraftAutoId").val(),
            ShippingType: $("#ddlShippingType").val(),
            Remark: $("#txtOrderRemarks").val(),
            ShippingCharge: $("#txtShipping").val() || 0,
            OverallDisc: $("#txtOverallDisc").val() || 0,
            DiscAmt: $("#txtDiscAmt").val() || 0
        }
        $.ajax({
            type: "POST",
            url: "RegularPOSOrder.aspx/updateDraftOrder",
            data: "{'dataValues':'" + JSON.stringify(Draftdata) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                //alert("Draft Order has been updated successfully.");
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
}

$('#SavedMassege').modal('show');
function OpenSecurity() {
    $("#mdSecurityCheck").modal('show');
    $("#mdAllowQtyConfirmation").modal('hide');
}
function CloseAllowConfirm() {
    $("#mdAllowQtyConfirmation").modal('hide');
}
function CheckManagerSecurity() {
    checkmanagersecuritykey()
}
function closeMnagerSecurity() {
}
function checkmanagersecuritykey() {
    $.ajax({
        type: "Post",
        url: "RegularPOSOrder.aspx/CheckManagerSecurity",
        data: "{'SecurityKey':" + $("#txtManagerSecurityCheck").val() + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d == 'true') {
                    $("#mdSecurityCheck").modal('hide');
                    $("#txtManagerSecurityCheck").val('');
                    $("#modalOverrideQty").modal('show');
                    $("#txtAvailQty").val(AvailableAllocatedQty);
                    $("#txtRequiredQty").val(RequiredQty);
                } else {
                    toastr.error('Access Denied.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                }
            } else {
                location.href = '/';
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function OverrideQty() {
    debugger;
    var ProductAutoId = 0
    if ($("#ddlProduct option:selected").val() == '0') {
        ProductAutoId = PrdtId;
    }
    else {
        ProductAutoId = $("#ddlProduct option:selected").val();
    }
    var Data = {
        CustomerAutoId: $('#ddlCustomer').val(),
        ProductAutoId: ProductAutoId,
        RequiredQuantity: $("#txtRequiredQty").val()
    }
    $.ajax({
        type: "Post",
        url: "RegularPOSOrder.aspx/AddAllocationQty",
        data: "{'dataValue':'" + JSON.stringify(Data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d == 'true') {
                    toastr.success('Qauntity allocated successfully', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    $("#modalOverrideQty").modal('hide')
                } else {
                    toastr.error('Oops,Something went wrong.Please try later', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                }
            } else {
                location.href = '/';
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function PrintOrder() {

    $('#PopPrintTemplate').modal('hide');
    if ($("#chkdueamount").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF1.html?OrderAutoId=" + $("#txtHOrderAutoId").val() + "", "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    } else if ($("#chkdefault").prop('checked') == true) {
        window.open("/Manager/OrderPrintCC.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#Template1").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF2.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#Template2").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF3.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    } else if ($("#PSMPADefault").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF4.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#PackingSlip").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF5.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#WithoutCategorybreakdown").prop('checked') == true) {
        window.open("/Manager/WithoutCategorybreakdown.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#rPSMNPADefault").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF4.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#Templ1").prop('checked') == true) {
        window.open("/Manager/PrintOrderItem.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#Templ2").prop('checked') == true) {
        window.open("/Manager/PrintOrderItemTemplate2.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    if (j != 0)
        location.href = "/Warehouse/OrderNew.aspx";

}
function readCurrencyDetails() {
    var PaymentMethod = parseInt( $("#ddlPaymentMenthod").val());
    if (PaymentMethod == 1) {
        $.ajax({
            type: "POST",
            url: "RegularPOSOrder.aspx/readCurrencyDetails",
            contentType: "application/json;charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d != "Session Expired") {
                    var xmldoc = $.parseXML(response.d);
                    var Currency = $(xmldoc).find('Table');

                    $("#tblCurrencyList tbody tr").remove();
                    var row = $("#tblCurrencyList thead tr").clone(true);
                    $.each(Currency, function (index) {
                        $(".CurrencyName", row).html("<span CurrencyValue='" + $(this).find('CurrencyValue').text() + "'  CurrencyAutoid='" + $(this).find('Autoid').text() + "'></span>" + $(this).find("Currencyname").text());
                        $(".NoofRupee", row).html("<input type='Text' id='txtTopCurrency' maxlength='6' class='form-control input-sm text-center' onkeypress='return isNumberKey(event)' onchange='PaymentCal(this)' onfocus='this.select()' />");
                        $(".Total", row).text('0.00');
                        $("#tblCurrencyList tbody").append(row);
                        row = $("#tblCurrencyList tbody tr:last").clone(true);
                    });

                } else {
                    location.href = '/';
                }

            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
        $("#SecCurrency").show();
        $("#SecCheck").hide();
    }
    else if (PaymentMethod == 2) {
        $("#SecCheck").show();
        $("#SecCurrency").hide();
    }
    else {
        $("#SecCheck").hide();
        $("#SecCurrency").hide();
    }
}
function PaymentCal(e) {
    var row = $(e).closest('tr');
    var Total = 0.00, TotalAmount = 0.00, noofRupee1 = 0, sortAmt = 0.00, ReceiveAmount = 0.00;
    var Currencyvalue = $(row).find(".CurrencyName span").attr('CurrencyValue');
    if ($(row).find(".NoofRupee input").val() != '') {
        noofRupee1 = $(row).find(".NoofRupee input").val();
        Total = parseFloat(parseFloat(Currencyvalue) * parseFloat(noofRupee1)).toFixed(2);
    }
    $(row).find(".Total").text(parseFloat(Total).toFixed(2) || 0.00);
    $("#tblCurrencyList tbody tr").each(function () {
        TotalAmount += parseFloat($(this).find('.Total').text());
    });
    ReceiveAmount = parseFloat($("#txtPaybleAmount").val());
    if (TotalAmount > parseFloat(ReceiveAmount)) {
        $(row).find(".NoofRupee input").val('');
        $(row).find(".Total").html('0.00');
        $(row).find(".NoofRupee input").focus();
        $(row).find(".NoofRupee input").addClass('border-warning');
        TotalAmount = TotalAmount - Total;
    }
    $("#TotalAmount").text(parseFloat(TotalAmount).toFixed(2));
    $("#txtTotalCurrencyAmount").val((TotalAmount).toFixed(2));
}