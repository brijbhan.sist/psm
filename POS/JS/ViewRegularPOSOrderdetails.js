﻿var getid;
var Stock, AvailableAllocatedQty = 0, RequiredQty = 0, PrdtId = 0, j = 0;
$(document).ready(function () {
   
    var d = new Date();
    CurrentDate = (d.getMonth() + 1) + "/" + d.getDate() + "/" + d.getFullYear();
    $('#txtOrderDate').val(CurrentDate);
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    var getid = getQueryString('OrderNo');
    //bindDropdown();
    if (getid != null) {
        $('#OrderAutoId').val(getid);
        $("#txtHOrderAutoId").val(getid);
        bindOrderDetails(getid);
    }
    $('#txtScanBarcode').focus();
});

var MLQtyRate = 0.00, MLTaxType = 0, WeightOzTex = 0;


function bindOrderDetails(OrderAutoId) {
    $.ajax({
        type: "POST",
        url: "ViewRegularPOSOrderdetails.aspx/bindOrderDetails",
        data: "{'OrderAutoId':'" + OrderAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {

            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var BillingAddress = $(xmldoc).find("Table");
                var ShippingAddress = $(xmldoc).find("Table1");
                var OrderDetails = $(xmldoc).find("Table2");
                var OrderItemDetails = $(xmldoc).find("Table3");
                var CreditDetails = $(xmldoc).find("Table4");
                var MLTax = $(xmldoc).find("Table5");
                var WeightOzTexValue = $(xmldoc).find("Table6");
                if ($(WeightOzTex).find('Value').text() != '')
                    WeightOzTex = $(WeightOzTex).find('Value').text();
                if ($(MLTax).find('TaxRate').text() != '')
                    MLQtyRate = $(MLTax).find('TaxRate').text();
                console.log(OrderDetails);

                $("#ddlShippingType").text($(OrderDetails).find("PaymentType").text());

                $("#txtOrderId").text($(OrderDetails).find("OrderNo").text());
                $("#ddlPaymentMenthod").val($(OrderDetails).find("PaymentType").text());
                $("#txtReferenceNo").val($(OrderDetails).find("referNo").text());
                $("#salesPerson").text($(OrderDetails).find("SALESNAME").text());
                $("#txtOrderDate").text($(OrderDetails).find("OrderDate").text());
                $("#txtTotalAmount").val($(OrderDetails).find("TotalAmount").text());
                $("#txtOverallDisc").val($(OrderDetails).find("OverallDisc").text());
                $("#txtShipping").val($(OrderDetails).find("ShippingCharges").text());
                $("#txtDiscAmt").val($(OrderDetails).find("OverallDiscAmt").text());
                $("#txtMLTax").val($(OrderDetails).find("MLTax").text());
                $("#txtMLQty").val($(OrderDetails).find("MLQty").text());
                $("#txtWeightQty").val($(OrderDetails).find("Weigth_OZQty").text());
                $("#txtWeightTex").val($(OrderDetails).find("Weigth_OZTaxAmount").text());

                $("#txtTotalTax").val($(OrderDetails).find("TotalTax").text());
                $("#txtGrandTotal").val($(OrderDetails).find("GrandTotal").text());
                $("#txtPaidAmount").val($(OrderDetails).find("paidAmount").text());
                $("#hdnPaidAmount").val($(OrderDetails).find("paidAmount").text());
                $("#txtDeductionAmount").val($(OrderDetails).find("DeductionAmount").text());
                $("#txtPastDue").val($(OrderDetails).find("PastDue").text());
                $("#txtAdjustment").val($(OrderDetails).find("AdjustmentAmt").text());

                var balancAmts = parseFloat(Number($(OrderDetails).find("PastDue").text()) + Number($(OrderDetails).find("PayableAmount").text())).toFixed(2);
                var balancAmt = parseFloat(balancAmts - $(OrderDetails).find("paidAmount").text()).toFixed(2);
                $("#txtBalanceAmount").val(balancAmt);


                if (parseFloat($(OrderDetails).find("GrandTotal").text()) > parseFloat($(OrderDetails).find("DeductionAmount").text())) {
                    if (parseFloat($("#CreditMemoAmount").html()) > 0)//txtStoreCreditAmount
                    {
                        $("#txtStoreCreditAmount").removeAttr("disabled");
                    }
                }
                else {
                    $("#txtStoreCreditAmount").attr("disabled", true);
                }
                $("#txtStoreCreditAmount").val($(OrderDetails).find("CreditAmount").text());
                $("#hdnStoreCreditAmount").val($(OrderDetails).find("CreditAmount").text());
                $("#txtOrderRemarks").val($(OrderDetails).find("OrderRemarks").text());
                $("#txtPaybleAmount").val($(OrderDetails).find("PayableAmount").text());
                if ($(OrderDetails).find('CustomerType').text() == 3) {
                    $('.UnitPrice').hide();
                    $('.SRP').hide();
                    $('.GP').hide();
                    $('.NetPrice').hide();
                    $('#orderSummary').hide();
                } else {
                    $('.UnitPrice').show();
                    $('.SRP').show();
                    $('.GP').show();
                    $('.NetPrice').show();
                    $('#orderSummary').show();
                }
                if (Number($(OrderDetails).find('StatusCode').text()) == 6) {
                    $('#btnEditOdrer').show();
                } else {
                    $('#btnEditOdrer').hide();
                }
                if (parseFloat($(OrderDetails).find('StoreCredit').text()) > 0) {
                    $('#StoreCredit').text(parseFloat($(OrderDetails).find('StoreCredit').text()).toFixed(2));
                }
                $("#ddlCustomer").text($(OrderDetails).find('CustomerName').text()).change();
                $("#ddlTaxType").val($(OrderDetails).find("TaxType").text());

                $("#ddlBillingAddress").text($(BillingAddress).find("BillingAddress").text());
                $("#ddlShippingAddress").text($(ShippingAddress).find("ShippingAddress").text());
               
                $("#tblProductDetail tbody tr").remove();
                var row = $("#tblProductDetail thead tr").clone(true);
                $.each(OrderItemDetails, function () {

                    $(".ProId", row).html($(this).find("ProductId").text() + "<span isFreeItem='" + $(this).find("isFreeItem").text() + "'></span>");

                    if (Number($(this).find("isFreeItem").text()) == 1) {
                        $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span>" + " <product class='badge badge badge-pill badge-success'>Free</product>");

                    } else if (Number($(this).find("IsExchange").text()) == 1) {
                        $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span>" + " <product class='badge badge badge-pill badge-primary'>Exchange</product>");
                    }
                    else if (Number($(this).find("Tax").text()) == 1) {
                        $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span>" + " <product class='badge badge badge-pill badge-danger'>Taxable</product>");
                    } else {
                        $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span>");
                    }
                    $(".UnitType", row).html("<span UnitAutoId='" + $(this).find("UnitAutoId").text() + "' QtyPerUnit='" + $(this).find("QtyPerUnit").text() + "'>" + $(this).find("UnitType").text() + " (" + $(this).find("QtyPerUnit").text() + " pcs" + ")</span>");
                    $(".ReqQty", row).html("<input type='text' maxlength='6' onchange='CheckAllocatedQty(this)' class='form-control input-sm border-primary text-center' onkeypress='return isNumberKey(event)' onkeyup='rowCal(this,1)' style='width:70px;' value='" + $(this).find("RequiredQty").text() + "' findoldqty='" + $(this).find("RequiredQty").text() + "'/>");
                    $(".PerPrice", row).text($(this).find("PerpiecePrice").text());
                    $(".QtyRemain", row).text($(this).find("RequiredQty").text());
                    $(".TtlPcs", row).text($(this).find("TotalPieces").text());
                    $(".NetPrice", row).text($(this).find("NetPrice").text());
                    if (Number($(this).find("IsExchange").text()) == 1 && Number($(this).find("isFreeItem").text()) == 1) {
                        $(".UnitPrice", row).html("<input type='text' maxlength='6' class='form-control input-sm border-primary' onkeypress='return isNumberDecimalKey(event,this)' style='text-align:right;width:70px;' value='" + $(this).find("UnitPrice").text() + "' onkeyup='changePrice(this)' minprice=" + $(this).find("MinPrice").text() + " baseprice=" + $(this).find("Price").text() + "/>");
                    } else {
                        $(".UnitPrice", row).html("<input type='text' maxlength='6' class='form-control input-sm border-primary' onkeypress='return isNumberDecimalKey(event,this)' style='text-align:right;width:70px;' value='" + $(this).find("UnitPrice").text() + "' onkeyup='changePrice(this)' minprice=" + $(this).find("MinPrice").text() + " baseprice=" + $(this).find("Price").text() + " disabled/>");
                    }

                    $(".SRP", row).text($(this).find("SRP").text());
                    $(".GP", row).text($(this).find("GP").text());
                    $(".OM_MinPrice", row).text($(this).find("OM_MinPrice").text());
                    $(".OM_CostPrice", row).text($(this).find("OM_CostPrice").text());
                    $(".OM_BasePrice", row).text($(this).find("BasePrice").text());
                    if (Number($(this).find("Tax").text()) == 1) {
                        $(".TaxRate", row).html('<span typetax="1" MLQty="' + $(this).find("UnitMLQty").text() + '" WeightOz="' + $(this).find("WeightOz").text() + '"></span>' + '<TaxRate style="font-weight: bold" class="la la-check-circle success center"></TaxRate>');
                    } else {
                        $(".TaxRate", row).html('<span typetax="0" MLQty="' + $(this).find("UnitMLQty").text() + '" WeightOz="' + $(this).find("WeightOz").text() + '"></span>');
                    }

                    if (Number($(this).find("IsExchange").text()) == 1) {
                        $(".IsExchange", row).html('<span IsExchange="1"></span>' + '<IsExchange style="font-weight: bold" class="la la-check-circle success center"></IsExchange>');
                    } else {
                        $(".IsExchange", row).html('<span IsExchange="0"></span>');
                    }



                    $(".Barcode", row).text($(this).find("Barcode").text());

                    $(".Action", row).html("<a href='javascript:;' id='deleterow' onclick='deleterow(this)'><span class='ft-x'></span></a>");
                    $('#tblProductDetail tbody').append(row);
                    row = $("#tblProductDetail tbody tr:last").clone(true);

                });
                $('#emptyTable').hide();
                $('input').attr('disabled', true);
                $('select').attr('disabled', true);
                if ($(OrderDetails).find('CustomerType').text() == 3) {
                    $('#btnGenOrderCCNew').attr('onclick', 'PrintPackingSlip(1)');
                }
                $('#btnGenOrderCCNew').show();

                $('#btnAdd').closest('.card-body').hide();
                $('#btnSave').hide();
                $('#btnReset').hide();
                $('.Action').hide();
                $('textarea').attr('disabled', true);
                $('#btnBackOdrer').hide();
                $('#btnUpdateOrder').hide();
                $("#tblCreditMemoList tbody tr").remove();
                if (CreditDetails.length > 0) {
                    var TotalDue = 0.00;
                    var rowc1 = $("#tblCreditMemoList thead tr").clone(true);
                    $.each(CreditDetails, function (index) {
                        $(".SRNO", rowc1).text(Number(index) + 1);
                        if ($(this).find('OrderAutoId').text() != '') {
                            $(".Action", rowc1).html("<input type='checkbox' OrderAutoId='" + $(this).find('OrderAutoId').text() + "' disabled checked name='creditMemo' onclick='funcheckprop(this)' value=" + $(this).find("CreditAutoId").text() + "> ");
                        } else {
                            $(".Action", rowc1).html("<input type='checkbox' OrderAutoId='" + $(this).find('OrderAutoId').text() + "' name='creditMemo' onclick='funcheckprop(this)' value=" + $(this).find("CreditAutoId").text() + "> ");
                        }
                        $(".CreditNo", rowc1).html("<a target='_blank' href='/Sales/CreditMemo.aspx?PageId=" + $(this).find("CreditAutoId").text() + "'>" + $(this).find("CreditNo").text() + "</a>");
                        $(".CreditDate", rowc1).text($(this).find("CreditDate").text());
                        $(".ReturnValue", rowc1).text($(this).find("ReturnValue").text());
                        $(".CreditType", rowc1).text($(this).find("CreditType").text());
                        TotalDue = parseFloat(TotalDue) + parseFloat($(this).find("ReturnValue").text());

                        $("#tblCreditMemoList tbody").append(rowc1)
                        rowc1 = $("#tblCreditMemoList tbody tr:last").clone(true);
                    })
                    $("#TotalDue").text(TotalDue.toFixed(2));
                    $(".Action").show();
                    $('#CusCreditMemo').show();
                    $('#dCuCreditMemo').show();
                } else {
                    $('#CusCreditMemo').hide();
                    $('#dCuCreditMemo').hide();
                }
            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(result.d);
        },
        error: function (result) {
            console.log(result.d);
        }
    });
}
