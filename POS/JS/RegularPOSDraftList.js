﻿$(document).ready(function () {
    $(document).ready(function () {
        $('#txtSFromDate').pickadate({
            format: 'mm/dd/yyyy',
            formatSubmit: 'mm/dd/yyyy',
            selectYears: true,
            selectMonths: true
        });
        $('#txtSToDate').pickadate({
            format: 'mm/dd/yyyy',
            formatSubmit: 'mm/dd/yyyy',
            selectYears: true,
            selectMonths: true
        });
        var d = new Date();
        var month = d.getMonth() + 1;
        var day = d.getDate();
        var year = d.getFullYear();
        $("#txtSFromDate").val(month + '/' + day + '/' + year);
        $("#txtSToDate").val(month + '/' + day + '/' + year);

        bindDropdown();
    })
})

function bindDropdown() {
    $.ajax({
        type: "POST",
        url: "RegularPOSOrderList.aspx/bindDropdown",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            if (response.d != "Session Expired") {
                var getData = $.parseJSON(response.d);
                $("#ddlCustomer option:not(:first)").remove();
                $.each(getData[0].CustomerList, function (index, item) {
                    $("#ddlCustomer").append("<option value='" + item.A + "'>" + item.C + "</option>");
                });
                $("#ddlCustomer").select2();

            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(result.d);
        },
        error: function (result) {
            console.log(result.d);
        }
    });
}
function Pagevalue(e) {
    getOrderList(parseInt($(e).attr("page")));
};
function getOrderList(pageIndex) {
    var data = {
        CustomerAutoId: $('#ddlCustomer').val(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        pageIndex: pageIndex,
        PageSize: $("#ddlPageSize").val()
    };

    $.ajax({
        type: "POST",
        url: "RegularPOSDraftList.aspx/getDraftOrderList",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var orderList = $(xmldoc).find("Table1");
                $("#tblOrderList tbody tr").remove();
                var row = $("#tblOrderList thead tr").clone(true);
                if (orderList.length > 0) {
                    $("#EmptyTable").hide();
                    $.each(orderList, function () {
                        $(".orderDt", row).text($(this).find("OrderDate").text());
                        $(".cust", row).text($(this).find("CustomerName").text());
                        $(".DeliveryDate", row).text($(this).find("DeliveryDate").text());
                        $(".product", row).text($(this).find("NoOfItems").text());
                        $(".EmpName", row).text($(this).find("EmpName").text());
                        $(".ShippingType", row).text($(this).find("ShippingType").text());
                        $(".status", row).html("<span class='badge badge badge-pill badge-warning'>" + $(this).find("Status").text() + "</span>");
                        $(".action", row).html("<a href='/POS/RegularPOSOrder.aspx?DraftAutoId=" + $(this).find("DraftAutoId").text() + "'><span class='ft-edit' title='Edit'></span></a> | <a href='javascript:;'><span class='ft-x' title='Delete' onclick='deleterecord(\"" + $(this).find("DraftAutoId").text() + "\")'></span><b></a>&nbsp;&nbsp;</b>");
                        $("#tblOrderList tbody").append(row);
                        row = $("#tblOrderList tbody tr:last").clone(true);
                    });
                } else {
                    $("#EmptyTable").show();
                }
                var EmpType = $(xmldoc).find("Table2");
                if ($(EmpType).find('EmpType').text() != 1) {
                    $(".EmpName").hide();
                }

                var pager = $(xmldoc).find("Table");
                $(".Pager").ASPSnippets_Pager({
                    ActiveCssClass: "current",
                    PagerCssClass: "pager",
                    PageIndex: parseInt(pager.find("PageIndex").text()),
                    PageSize: parseInt(pager.find("PageSize").text()),
                    RecordCount: parseInt(pager.find("RecordCount").text())
                });

                if ($("#ddlPageSize").val() == '0') {
                    $(".Pager").hide();
                }
                else {
                    $(".Pager").show();
                }
            } else {
                location.href = "/";
            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function deleterecord(DraftAutoId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete order !",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: false,
            },
            confirm: {
                text: "Yes, delete it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            deleteDraftOrder(DraftAutoId);

        } else {
            swal("", "Your data is safe.", "error");
        }
    })
}

function deleteDraftOrder(DraftAutoId) {
    $.ajax({
        type: "POST",
        url: "RegularPOSDraftList.aspx/deleteDraftOrder",
        data: "{'DraftAutoId':" + DraftAutoId + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == 'Session Expired') {
                location.href = '/';
            } else if (response.d == 'true') {
                swal("", "Draft order deleted successfully.", "success")
                getOrderList(1);
            } else {
                swal('Error !', "Oops! Something went wrong.Please try later.", "error");

            }
        },
        failure: function (result) {
            swal('Error !', "Oops! Something went wrong.Please try later.", "error");

        },
        error: function (result) {
            swal('Error !', "Oops! Something went wrong.Please try later.", "error");
        }
    });
}