﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DLLRegularPOSOrderList;

public partial class Warehouse_RegularPOSDraftList : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/POS/JS/RegularPOSDraftList.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredField'>" + text + "</script>"
                ));
        }

    }
    [WebMethod(EnableSession = true)]
    public static string getDraftOrderList(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_RegularPOSOrderList pobj = new PL_RegularPOSOrderList();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.Opcode = 43;
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                if (jdv["FromDate"] != null && jdv["FromDate"] != "")
                {
                    pobj.Fromdate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != null && jdv["ToDate"] != "")
                {
                    pobj.Todate = Convert.ToDateTime(jdv["ToDate"]);
                }
                pobj.PageIndex = jdv["pageIndex"];
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                BL_RegularPOSOrderList.getDraftOrderList(pobj);
                if (!pobj.isException)
                {

                    return pobj.Ds.GetXml();
                }
                else
                {

                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public static string deleteDraftOrder(int DraftAutoId)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_RegularPOSOrderList pobj = new PL_RegularPOSOrderList();
            try
            {
                pobj.DraftAutoId = Convert.ToInt32(DraftAutoId);
                BL_RegularPOSOrderList.deleteDraft(pobj);
                if (!pobj.isException)
                {

                    return "true";
                }
                else
                {

                    return "Oops! Something went wrong.Please try later.";
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}