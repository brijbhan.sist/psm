﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="ViewRegularPOSOrderdetails.aspx.cs" Inherits="Warehouse_ViewRegularPOSOrderdetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input id="OrderAutoId" type="hidden" />
    <input type="hidden" id="hiddenEmpTypeVal" runat="server" />
    <input type="hidden" id="hiddenStatusCode" runat="server" />
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Manage Order</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage Order</a></li>
                        <li class="breadcrumb-item">View Order</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" id="Button5" class="dropdown-item" onclick="location.href='/POS/RegularPOSOrderList.aspx'">Order List</button>

                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <input type="hidden" id="CustomerType" />
                        <div class="card-content collapse show">
                            <div class="card-body" id="panelBill">
                                <div class="row">
                                    <div class="col-md-2  col-sm-3">
                                        <label class="control-label text-bold-600">Order No</label>
                                        <input type="hidden" id="txtHOrderAutoId" class="form-control input-sm" />
                                        <input type="hidden" id="DraftAutoId" value="0" class="form-control input-sm" />
                                        <div class="form-group">
                                            <span id="txtOrderId"></span>                                           
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-3">
                                        <label class="control-label text-bold-600">Order Date</label>
                                        <div class="form-group">
                                            <span id="txtOrderDate"></span>                                           
                                        </div>
                                    </div>
                                     <div class="col-md-2 col-sm-6">
                                        <label class="control-label text-bold-600"">
                                            Shipping Type 
                                        </label>
                                        <div class="form-group">
                                            <span id="ddlShippingType"></span>                                          
                                        </div>
                                    </div>
                                     <div class="col-md-3 col-sm-2">
                                        <label class="control-label text-bold-600"">
                                            Sales Person 
                                        </label>
                                        <div class="form-group">
                                            <span id="salesPerson"></span>                                           
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-4">
                                        <label class="control-label text-bold-600">
                                            Customer 

                                        </label>
                                        <div class="form-group">
                                            <span id="ddlCustomer"></span>                                           
                                        </div>
                                    </div>
                                   
                                   
                                    <div class="col-sm-2 col-sm-6">
                                        <label class="control-label text-bold-600"">
                                            Shipping Address 

                                        </label>
                                        <div class="form-group">
                                            <span id="ddlShippingAddress"></span>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-2 col-sm-6">
                                        <label class="control-label text-bold-600"">
                                            Billing Address 

                                        </label>
                                        <div class="form-group">
                                            <span id="ddlBillingAddress"></span>                                         
                                        </div>
                                    </div>
                                    <input type="hidden" id="taxEnabled" />
                                    <div class="clearfix"></div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="drag-area4">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body" id="panelBill4">

                                <style>
                                    .table th, .table td {
                                        padding: 0.75rem 1rem;
                                    }
                                </style>
                                <div class="table-responsive">
                                    <table id="tblProductDetail" class="table table-striped table-bordered">
                                        <thead class="bg-blue white">
                                            <tr>
                                                <td class="Action text-center" title="Delete" style="width: 4%">Action</td>
                                                <td class="ProId  text-center" style="width: 4%">ID</td>
                                                <td class="ProName">Product Name</td>
                                                <td class="UnitType  text-center" style="width: 4%">Unit</td>
                                                <td class="OM_MinPrice  text-center" style="display: none;">Min Price</td>
                                                <td class="OM_CostPrice  text-center" style="display: none;">Cost Price</td>
                                                <td class="OM_BasePrice  text-center" style="display: none;">Base Price</td>
                                                <td class="ReqQty  text-center" style="width: 4%">Qty</td>
                                                <td class="TtlPcs  text-center" style="width: 4%">Total Pieces</td>
                                                <td class="UnitPrice  price" style="width: 4%">Unit Price </td>
                                                <td class="SRP  price" style="width: 4%">SRP</td>
                                                <td class="GP price" style="width: 4%">GP (%)</td>
                                                <td class="TaxRate text-center" style="width: 4%; display: none">Tax</td>
                                                <td class="IsExchange text-center" style="width: 4%; display: none">Exchange</td>
                                                <td class="NetPrice price" style="width: 4%">Net Price</td>

                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                    <h5 class="well text-center" id="emptyTable">No Product Selected.</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="row">
            <div class="col-md-5 col-sm-5">
                <section id="drag-area3">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-content collapse show">
                                    <div class="card-body" id="panelBill3">

                                        <div id="Div1" class="panel panel-default">

                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-sm-12 form-group">
                                                        <label>Remark</label><textarea id="txtOrderRemarks" onchange="updatedraftOrder()" class="form-control input-sm border-primary"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12" id="CusCreditMemo" style="display: none">
                                                <div class="panel panel-default">
                                                    <h4 class="form-section"><i class="la la-eye"></i>Customer Credit Memo</h4>

                                                    <div class="panel-body" id="dCuCreditMemo" style="display: none">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="table-responsive">
                                                                    <table id="tblCreditMemoList" class="table table-striped table-bordered ">
                                                                        <thead class="bg-blue white">
                                                                            <tr>
                                                                                <td class="SRNO">Sr No.</td>
                                                                                <td class="Action rowspan" style="display: none">Action</td>
                                                                                <td class="CreditNo">Credit No.</td>
                                                                                <td class="CreditType">Credit Type</td>
                                                                                <td class="CreditDate">Credit Date</td>
                                                                                <td class="ReturnValue" style="text-align: right;">Total Value ($)</td>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        </tbody>
                                                                        <tfoot style="font-weight: 700; background: oldlace;">
                                                                            <tr style="text-align: right;">
                                                                                <td style="display: none" class="rowspan"><b></b></td>
                                                                                <td colspan="5"><b>Total</b></td>
                                                                                <td id="TotalDue"></td>
                                                                            </tr>
                                                                        </tfoot>
                                                                    </table>

                                                                </div>


                                                            </div>


                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="table-responsive">
                                                    <table id="tblMemoList" class="table table-striped table-bordered " style="display: none">
                                                        <thead class="bg-blue white">
                                                            <tr>
                                                                <td class="Action" style="width: 10%">Action</td>
                                                                <td class="CreditNo" style="width: 10%">Credit No.</td>
                                                                <td class="CreditType" style="width: 10%">Credit Type</td>
                                                                <td class="empName" style="width: 10%">deduction By</td>
                                                                <td class="PayDate" style="width: 20%">Deduction Date</td>
                                                                <td class="DeductionAmount" style="text-align: right; width: 20%">Deducted Amount&nbsp;(&nbsp;$&nbsp;)</td>
                                                                <td class="dueRemarks">Remark</td>

                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                        <tfoot style="font-weight: 700; background: oldlace;">
                                                            <tr style="text-align: right;">
                                                                <td colspan="5"><b>TOTAL</b></td>
                                                                <td id="Td4"></td>
                                                                <td id="Td1"></td>

                                                            </tr>
                                                        </tfoot>
                                                    </table>

                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>

            <div class="col-md-7 col-sm-7">
                <section id="drag-area11">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-content collapse show">
                                    <div class="card-body" id="panelBil13">

                                        <div style="text-align: right" id="orderSummary">
                                            <div class="row">
                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">Sub Total</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtTotalAmount" value="0.00" readonly="readonly" />
                                                    </div>
                                                </div>


                                                <div class="col-sm-6 form-group">
                                                    <b>
                                                        <label style="display: flex;">Order Total</label></b>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right; font-size: 14px; font-weight: 700;" readonly="readonly" id="txtGrandTotal" value="0.00" />
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="row">
                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">Overall Discount</label>
                                                    <div style="display: flex;">
                                                        <div class="input-group" style="width: 145px">

                                                            <input type="text" class="form-control input-sm border-primary" style="text-align: right;" onchange="updatedraftOrder()" id="txtOverallDisc" maxlength="6" value="0.00" onkeyup="calOverallDisc()" onkeypress="return isNumberDecimalKey(event,this)" />
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" style="padding: 0rem 1rem;"><span>%</span></span>
                                                            </div>
                                                        </div>
                                                        &nbsp;&nbsp;&nbsp;
                                                    <div class="input-group" style="width: 145px">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right;" onfocus="this.select()" id="txtDiscAmt" value="0.00" onchange="updatedraftOrder()" onkeyup="calOverallDisc1()"
                                                            onkeypress="return isNumberDecimalKey(event,this)" />
                                                    </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">Credit Memo Amount</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtDeductionAmount" value="0.00" readonly="readonly" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>
                                            <div class="row">
                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">Shipping Charges</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">

                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" onchange="updatedraftOrder()" id="txtShipping" value="0.00" onkeyup="calGrandTotal()" onkeypress="return isNumberDecimalKey(event,this)" onfocus="this.select()" />

                                                    </div>
                                                </div>

                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex; white-space: nowrap">Store Credit Amount&nbsp;<span id="creditShow">(max <span id="CreditMemoAmount" class="text-bold-600">0.00</span> Available)</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtStoreCreditAmount" onkeypress="return isNumberDecimalKey(event,this)" onkeyup="changecredit()" onfocus="this.select()" value="0.00" readonly="readonly" />
                                                        <input type="hidden" id="hdnStoreCreditAmount" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>
                                            <div class="row">
                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">
                                                        Tax Type<span class="required"></span>

                                                    </label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>%</span></span>
                                                        </div>
                                                        <select class="form-control input-sm border-primary" id="ddlTaxType" onchange="calTotalTax()" runat="server">
                                                            <option value="0" taxvalue="0.0">-Select Tax Type-</option>
                                                        </select>
                                                    </div>

                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <b>
                                                        <label style="display: flex;">Payable Amount</label></b>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtPaybleAmount" readonly value="0.00" />
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="row">
                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">Total Tax</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtTotalTax" value="0.00" readonly="readonly" />
                                                    </div>
                                                </div>
                                                <div class="col-sm-6  form-group">
                                                    <label style="display: flex;">Past Due Amount</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right; font-size: 14px; font-weight: 700;" readonly id="txtPastDue" onkeypress="return isNumberDecimalKey(event,this)" value="0.00" onkeyup="paidAmount()" onchange="paidAmount()" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>
                                            <div class="row">
                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">ML Quantity</label>

                                                    <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtMLQty" value="0.00" readonly="readonly" />

                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <b>
                                                        <label style="display: flex;">Open Balance</label></b>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right; font-size: 14px; font-weight: 700;" readonly id="txtOpenBalance" onkeypress="return isNumberDecimalKey(event,this)" value="0.00" onkeyup="paidAmount()" onchange="paidAmount()" />
                                                    </div>
                                                </div>

                                            </div>


                                            <div class="clearfix"></div>
                                            <div class="row">
                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">ML Tax</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtMLTax" value="0.00" readonly="readonly" />
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">Paid Amount</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">

                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary req" style="text-align: right; font-size: 14px; font-weight: 700;" id="txtPaidAmount" onkeypress="return isNumberDecimalKey(event,this)" value="0.00" onkeyup="PaidAmountUsed()" onchange="PaidAmountUsed()" />
                                                        <input type="hidden" id="hdnPaidAmount" />
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">Weight Quantity</label>
                                                    <%-- <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>--%>
                                                    <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtWeightQty" value="0" readonly="readonly" />
                                                    <%--</div>--%>
                                                </div>

                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">Balance Amount</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtBalanceAmount" value="0.00" readonly="true" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">Weight Tax</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtWeightTex" value="0.00" readonly="readonly" />
                                                    </div>
                                                </div>

                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">Payment Method</label>
                                                    <input type="text"  class="form-control input-sm border-primary" id="ddlPaymentMenthod"/>                                               
                                                </div>

                                            </div>

                                            <div class="row">


                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">Adjustment</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtAdjustment" value="0.00" readonly="readonly" />
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">Reference No </label>
                                                    <input type="text" class="form-control input-sm border-primary" id="txtReferenceNo" runat="server" disabled />
                                                </div>

                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>
            </div>
        </div>
    </div>


    <div id="CustomerDueAmount" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="display: block">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Customer Due Amount</h4>

                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered " id="tblduePayment">
                            <thead class="bg-blue white">
                                <tr>
                                    <td class="SrNo text-center">SN</td>
                                    <td class="OrderNo  text-center">Order No</td>
                                    <td class="OrderDate  text-center">Order Date</td>
                                    <td class="AmtDue price">Due Amount</td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="3" class="text-right text-bold-600">Total Due Amount</td>
                                    <td id="TotalDueAmount" class="text-right text-bold-600"></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation  round box-shadow-1 btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <style>
        ::-webkit-scrollbar {
            width: 5px;
            height: 5px;
            background: none;
        }
    </style>

    <input type="hidden" id="HDDomain" runat="server" />
</asp:Content>
