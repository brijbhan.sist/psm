﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" ClientIDMode="Static" CodeFile="RegularPOSOrder.aspx.cs" Inherits="Warehouse_OrderNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        #tblduePayment {
            margin-bottom: 0 !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input id="OrderAutoId" type="hidden" />
    <input type="hidden" id="hiddenEmpTypeVal" runat="server" />
    <input type="hidden" id="hiddenStatusCode" runat="server" />
    <div id="CheckStock" class="modal" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content ">
                <div class="modal-header">
                    <h4 class="modal-title"><b>Stock Details</b></h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <br />

                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table id="tblProductList" class="table table-striped table-bordered">
                                    <thead class="bg-blue white">
                                        <tr>
                                            <td class="Pop_ProId text-center">Product ID</td>
                                            <td class="Pop_ProName">Product Name</td>
                                            <td class="Pop_AvilStock text-center" style="width: 10px">Available Qty</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="ok" class="btn btn-primary buttonAnimation round box-shadow-1  btn-sm" data-dismiss="modal"><b>OK</b></button>
                </div>
            </div>
        </div>
    </div>

    <div id="CheckCreditMemo" class="modal" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content ">
                <div class="modal-header">
                    <h4 class="modal-title"><b>Attached Credit Memo details</b></h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <br />
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table id="tblCRMStatusList" class="table table-striped table-bordered">
                                    <thead class="bg-blue white">
                                        <tr>
                                            <td class="CreditNo text-center">Credit No</td>
                                            <td class="SalesAmount text-center">Sales Amount</td>
                                            <td class="Status text-center" style="width: 10px">Status</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row" style="width: 100%;">
                        <div class="col-md-10">
                            <span style="color: red;">Above Credit Memo has been cancelled.</span>
                        </div>
                        <div class="col-md-2">
                            <button type="button" id="OK" class="btn btn-primary buttonAnimation round box-shadow-1  btn-sm" data-dismiss="modal"><b>OK</b></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Manage Order</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">

                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage Order</a></li>
                        <li class="breadcrumb-item">New Order</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" id="Button5" class="dropdown-item" onclick="location.href='/POS/RegularPOSOrderList.aspx'">Order List</button>

                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <input type="hidden" id="CustomerType" />
                        <div class="card-content collapse show">
                            <div class="card-body" id="panelBill">
                                <div class="row">
                                    <div class="col-sm-2  col-sm-3">
                                        <label class="control-label">Order No</label>
                                        <input type="hidden" id="txtHOrderAutoId" class="form-control input-sm" />
                                        <input type="hidden" id="DraftAutoId" value="0" class="form-control input-sm" />
                                        <div class="form-group">
                                            <input type="text" id="txtOrderId" class="form-control input-sm border-primary" readonly="readonly" />
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-sm-3">
                                        <label class="control-label">Order Date</label>
                                        <div class="form-group">
                                            <input type="text" id="txtOrderDate" class="form-control input-sm border-primary" readonly="readonly" />
                                        </div>
                                    </div>

                                    <div class="col-sm-3 col-sm-4">
                                        <label class="control-label">
                                            Select Customer <span class="required">*</span>

                                        </label>
                                        <div class="form-group">
                                            <select class="form-control input-sm selectvalidate border-primary" id="ddlCustomer" runat="server" onchange="BindAddressCustomer()">
                                                <option value="0">- Select Customer -</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-sm-2">
                                        <label class="control-label">
                                            Sales Person 
                                        </label>
                                        <div class="form-group">
                                            <input type="text" id="salesPerson" disabled="disabled" class="form-control input-sm    border-primary" />
                                        </div>
                                    </div>

                                    <div class="col-sm-2 col-sm-6">
                                        <label class="control-label">
                                            Shipping Address <span class="required">*</span>

                                        </label>
                                        <div class="form-group">
                                            <select class="form-control input-sm selectvalidate border-primary" id="ddlShippingAddress" runat="server">
                                                <option value="0">- Select -</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-2 col-sm-6">
                                        <label class="control-label">
                                            Billing Address <span class="required">*</span>

                                        </label>
                                        <div class="form-group">
                                            <select class="form-control input-sm selectvalidate border-primary" id="ddlBillingAddress" runat="server">
                                                <option value="0">- Select -</option>
                                            </select>
                                        </div>
                                    </div>
                                    <input type="hidden" id="taxEnabled" />
                                    <div class="clearfix"></div>
                                    <div class="col-sm-2 col-sm-6">
                                        <label class="control-label">
                                            Shipping Type <span class="required">*</span>

                                        </label>
                                        <div class="form-group">
                                            <select class="form-control input-sm selectvalidate border-primary" onchange="updatedraftOrder();calTotalAmount();" id="ddlShippingType" runat="server">
                                                <option value="0">- Select -</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section id="drag-area1">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body" id="panelProduct">

                                <div class="row">
                                    <label class="col-sm-1 control-label">Barcode</label>
                                    <div class="col-sm-3 form-group">
                                        <input type="text" class="form-control input-sm border-primary" id="txtBarcode" runat="server" onfocus="this.select()" placeholder="Enter Barcode here" onchange="readBarcode()" />
                                    </div>

                                </div>
                                <div class="clearfix"></div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label class="control-label">
                                            Product <span class="required">*</span>

                                        </label>
                                        <div class="form-group">
                                            <select class="form-control input-sm selectvalidate border-primary" id="ddlProduct" runat="server" style="width: 100% !important" onchange="bindUnitType()">
                                                <option value="0">-Select-</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-md-2">
                                        <label class="control-label">
                                            Unit Type <span class="required">*</span>
                                        </label>
                                        <div class="form-group">
                                            <select class="form-control input-sm Areq border-primary" id="ddlUnitType" runat="server" onchange="ChangeUnit()">
                                                <option value="0">-Select-</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-1">
                                        <label class="control-label" style="white-space: nowrap">
                                            Quantity <span class="required">*</span>

                                        </label>
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm Areq border-primary" id="txtReqQty" value="1" maxlength="5" runat="server" onkeypress="return isNumberKey(event)" />
                                        </div>
                                    </div>

                                    <div class="col-sm-1">
                                        <label class="control-label" style="white-space: nowrap">
                                            Is Exchange 
                                        </label>
                                        <div class="form-group">
                                            <input type="checkbox" id="chkExchange" onchange="changetype(this,'chkIsTaxable','chkFreeItem')" />
                                        </div>
                                    </div>
                                    <div class="col-sm-1">
                                        <label class="control-label" style="white-space: nowrap">
                                            Is Taxable 
                                        </label>
                                        <div class="form-group">
                                            <input type="checkbox" id="chkIsTaxable" onchange="changetype(this,'chkExchange','chkFreeItem')" />
                                        </div>
                                    </div>
                                    <div class="col-md-1 col-sm-1">
                                        <label class="control-label" style="white-space: nowrap">Free Item</label>
                                        <div class="form-group">
                                            <input type="checkbox" id="chkFreeItem" onchange="changetype(this,'chkExchange','chkIsTaxable')" />
                                        </div>
                                    </div>
                                    <script>
                                        function changetype(e, idhtml1, idhtml2) {

                                            $('#' + idhtml1).prop('checked', false);
                                            $('#' + idhtml2).prop('checked', false);
                                        }
                                    </script>
                                    <div class="col-sm-2">
                                        <div class="pull-left">
                                            <button type="button" class="btn btn-md btn-success buttonAnimation pull-right round box-shadow-1 btn-sm" onclick="AddItemList()" id="btnAdd" style="margin-top: 22px;">&nbsp;&nbsp;&nbsp;Add&nbsp;&nbsp;&nbsp;</button>
                                        </div>
                                    </div>

                                </div>
                                <div class="clearfix"></div>
                                <div class="row">
                                    <div class="col-sm-6 form-group">
                                        <div class="alert alert-danger alertSmall" id="alertStockQty" style="text-align: center; display: none; color: white !important;"></div>
                                    </div>
                                    <div class="col-sm-6 form-group">
                                        <div class="alert alert-danger alertSmall" id="Div2" style="text-align: center; display: none"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="drag-area4">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body" id="panelBill4">

                                <style>
                                    .table th, .table td {
                                        padding: 0.75rem 1rem;
                                    }
                                </style>
                                <div class="table-responsive">
                                    <table id="tblProductDetail" class="table table-striped table-bordered">
                                        <thead class="bg-blue white">
                                            <tr>
                                                <td class="Action text-center" title="Delete" style="width: 4%">Action</td>
                                                <td class="ProId  text-center" style="width: 4%">ID</td>
                                                <td class="ProName">Product Name</td>
                                                <td class="UnitType  text-center" style="width: 4%">Unit</td>
                                                <td class="OM_MinPrice  text-center" style="display: none;">Min Price</td>
                                                <td class="OM_CostPrice  text-center" style="display: none;">Cost Price</td>
                                                <td class="OM_BasePrice  text-center" style="display: none;">Base Price</td>
                                                <td class="ReqQty  text-center" style="width: 4%">Qty</td>
                                                <td class="TtlPcs  text-center" style="width: 4%">Total Pieces</td>
                                                <td class="UnitPrice  price" style="width: 4%">Unit Price </td>
                                                <td class="SRP  price" style="width: 4%">SRP</td>
                                                <td class="GP price" style="width: 4%">GP (%)</td>
                                                <td class="TaxRate text-center" style="width: 4%; display: none">Tax</td>
                                                <td class="IsExchange text-center" style="width: 4%; display: none">Exchange</td>
                                                <td class="NetPrice price" style="width: 4%">Net Price</td>

                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                    <h5 class="well text-center" id="emptyTable">No Product Selected.</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="row">
            <div class="col-md-5 col-sm-5">
                <section id="drag-area3">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-content collapse show">
                                    <div class="card-body" id="panelBill3">

                                        <div id="Div1" class="panel panel-default">

                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-sm-12 form-group">
                                                        <label>Remark</label><textarea id="txtOrderRemarks" onchange="updatedraftOrder()" class="form-control input-sm border-primary"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12" id="CusCreditMemo" style="display: none">
                                                <div class="panel panel-default">
                                                    <h4 class="form-section"><i class="la la-eye"></i>Customer Credit Memo</h4>

                                                    <div class="panel-body" id="dCuCreditMemo" style="display: none">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="table-responsive">
                                                                    <table id="tblCreditMemoList" class="table table-striped table-bordered ">
                                                                        <thead class="bg-blue white">
                                                                            <tr>
                                                                                <td class="SRNO">SN</td>
                                                                                <td class="Action rowspan" style="display: none">Action</td>
                                                                                <td class="CreditNo">Credit No.</td>
                                                                                <td class="CreditType">Credit Type</td>
                                                                                <td class="CreditDate">Credit Date</td>
                                                                                <td class="ReturnValue" style="text-align: right;">Total Value ($)</td>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        </tbody>
                                                                        <tfoot style="font-weight: 700; background: oldlace;">
                                                                            <tr style="text-align: right;">
                                                                                <td style="display: none" class="rowspan"><b></b></td>
                                                                                <td colspan="5"><b>Total</b></td>
                                                                                <td id="TotalDue"></td>
                                                                            </tr>
                                                                        </tfoot>
                                                                    </table>

                                                                </div>


                                                            </div>


                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="table-responsive">
                                                    <table id="tblMemoList" class="table table-striped table-bordered " style="display: none">
                                                        <thead class="bg-blue white">
                                                            <tr>
                                                                <td class="Action" style="width: 10%">Action</td>
                                                                <td class="CreditNo" style="width: 10%">Credit No.</td>
                                                                <td class="CreditType" style="width: 10%">Credit Type</td>
                                                                <td class="empName" style="width: 10%">deduction By</td>
                                                                <td class="PayDate" style="width: 20%">Deduction Date</td>
                                                                <td class="DeductionAmount" style="text-align: right; width: 20%">Deducted Amount&nbsp;(&nbsp;$&nbsp;)</td>
                                                                <td class="dueRemarks">Remark</td>

                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                        <tfoot style="font-weight: 700; background: oldlace;">
                                                            <tr style="text-align: right;">
                                                                <td colspan="5"><b>TOTAL</b></td>
                                                                <td id="Td4"></td>
                                                                <td id="Td1"></td>

                                                            </tr>
                                                        </tfoot>
                                                    </table>

                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="SecCurrency" style="display:none">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered" id="tblCurrencyList">
                                                        <thead class="bg-blue white">
                                                            <tr>
                                                                <td class="CurrencyName text-center">Bill</td>
                                                                <td class="NoofRupee text-center" style="width: 150px;">Total Count</td>
                                                                <td class="Total" style="text-align: right; width: 150px">Total Amount</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr style="font-weight: bold">
                                                                <td colspan="2" style="text-align: right">Total</td>
                                                                <td id="TotalAmount" style="text-align: right; font-weight: bold">0.00</td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4 col-sm-2">
                                                        <label class="control-label">
                                                            <span style="white-space: nowrap">Total Amount :</span>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-8 col-sm-12">
                                                        <div class="form-group">
                                                            <input type="text" id="txtTotalCurrencyAmount" readonly="true" onkeypress="return isNumberDecimalKey(event,this)" 
                                                                class="form-control border-primary input-sm" value="0.00" style="text-align: right;font-weight:bold;font-size: 17px;;" />
                                                        </div>
                                                    </div>
                                                </div>                                                
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="SecCheck" style="display:none">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-content collapse show">
                                    <div class="card-body">                                              
                                                 <div class="row">
                                                    <div class="col-md-4 col-sm-12">
                                                        <label class="control-label">
                                                            <span style="white-space: nowrap">Check No :</span>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-8 col-sm-12">
                                                        <div class="form-group">
                                                            <input type="text" id="TxtCheckNo" maxlength="20"  onkeypress="return isNumberKey(event)" class="form-control border-primary input-sm"  />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4 col-sm-2">
                                                        <label class="control-label">
                                                            <span style="white-space: nowrap">Date :</span>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-8 col-sm-4 form-group" >
                                                         <div class="input-group">
                                                             <div class="input-group-prepend">
                                                                 <span class="input-group-text" style="height:28px">
                                                                     <span class="la la-calendar-o"></span>
                                                                 </span>
                                                             </div>
                                                             <input type="text" id="TxtCheckDate" readonly="true"  class="form-control border-primary input-sm"  />
                                                         </div>                                                        
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4 col-sm-2">
                                                        <label class="control-label">
                                                            <span style="white-space: nowrap">Remarks :</span>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-8 col-sm-4">
                                                        <div class="form-group">
                                                        <textarea class="form-control border-primary input-sm" id="txtRemarks" maxlength="400"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

            <div class="col-md-7 col-sm-7">
                <section id="drag-area11">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-content collapse show">
                                    <div class="card-body" id="panelBil13">

                                        <div style="text-align: right" id="orderSummary">
                                            <div class="row">
                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">Sub Total</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtTotalAmount" value="0.00" readonly="readonly" />
                                                    </div>
                                                </div>


                                                <div class="col-sm-6 form-group">
                                                    <b>
                                                        <label style="display: flex;">Order Total</label></b>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right; font-size: 14px; font-weight: 700;" readonly="readonly" id="txtGrandTotal" value="0.00" />
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="row">
                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">Overall Discount</label>
                                                    <div style="display: flex;">
                                                        <div class="input-group" style="width: 145px">

                                                            <input type="text" class="form-control input-sm border-primary" style="text-align: right;" onchange="updatedraftOrder()" id="txtOverallDisc" maxlength="6" value="0.00" onkeyup="calOverallDisc()" onkeypress="return isNumberDecimalKey(event,this)" />
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" style="padding: 0rem 1rem;"><span>%</span></span>
                                                            </div>
                                                        </div>
                                                        &nbsp;&nbsp;&nbsp;
                                                    <div class="input-group" style="width: 145px">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right;" onfocus="this.select()" id="txtDiscAmt" value="0.00" onchange="updatedraftOrder()" onkeyup="calOverallDisc1()"
                                                            onkeypress="return isNumberDecimalKey(event,this)" />
                                                    </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">Credit Memo Amount</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtDeductionAmount" value="0.00" readonly="readonly" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>
                                            <div class="row">
                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">Shipping Charges</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">

                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" onchange="updatedraftOrder()" id="txtShipping" value="0.00" onkeyup="calGrandTotal()" onkeypress="return isNumberDecimalKey(event,this)" onfocus="this.select()" />

                                                    </div>
                                                </div>

                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex; white-space: nowrap">Store Credit Amount&nbsp;<span id="creditShow">(max <span id="CreditMemoAmount" class="text-bold-600">0.00</span> Available)</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtStoreCreditAmount" onkeypress="return isNumberDecimalKey(event,this)" onkeyup="changecredit()" onfocus="this.select()" value="0.00" readonly="readonly" />
                                                        <input type="hidden" id="hdnStoreCreditAmount" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>
                                            <div class="row">
                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">
                                                        Tax Type<span class="required"></span>

                                                    </label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>%</span></span>
                                                        </div>
                                                        <select class="form-control input-sm border-primary" id="ddlTaxType" onchange="calTotalTax()" runat="server">
                                                            <option value="0" taxvalue="0.0">-Select Tax Type-</option>
                                                        </select>
                                                    </div>

                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <b>
                                                        <label style="display: flex;">Payable Amount</label></b>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtPaybleAmount" readonly value="0.00" />
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="row">
                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">Total Tax</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtTotalTax" value="0.00" readonly="readonly" />
                                                    </div>
                                                </div>
                                                <div class="col-sm-6  form-group">
                                                    <label style="display: flex;">Past Due Amount</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right; font-size: 14px; font-weight: 700;" readonly id="txtPastDue" onkeypress="return isNumberDecimalKey(event,this)" value="0.00" onkeyup="paidAmount()" onchange="paidAmount()" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>
                                            <div class="row">
                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">ML Quantity</label>

                                                    <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtMLQty" value="0.00" readonly="readonly" />

                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <b>
                                                        <label style="display: flex;">Open Balance</label></b>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right; font-size: 14px; font-weight: 700;" readonly id="txtOpenBalance" onkeypress="return isNumberDecimalKey(event,this)" value="0.00" onkeyup="paidAmount()" onchange="paidAmount()" />
                                                    </div>
                                                </div>

                                            </div>


                                            <div class="clearfix"></div>
                                            <div class="row">
                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">ML Tax</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtMLTax" value="0.00" readonly="readonly" />
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">Paid Amount</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">

                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary req" style="text-align: right; font-size: 14px; font-weight: 700;" id="txtPaidAmount" onkeypress="return isNumberDecimalKey(event,this)" value="0.00" onkeyup="PaidAmountUsed()" onchange="PaidAmountUsed()" />
                                                        <input type="hidden" id="hdnPaidAmount" />
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">Weight Quantity</label>
                                                    <%-- <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>--%>
                                                    <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtWeightQty" value="0" readonly="readonly" />
                                                    <%--</div>--%>
                                                </div>

                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">Balance Amount</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtBalanceAmount" value="0.00" readonly="true" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">Weight Tax</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtWeightTex" value="0.00" readonly="readonly" />
                                                    </div>
                                                </div>

                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">Payment Method</label>
                                                    <select class="form-control input-sm border-primary" id="ddlPaymentMenthod" disabled="disabled" onchange="readCurrencyDetails();">
                                                        <option value="0">-Select Payment Mode-</option>
                                                    </select>
                                                </div>

                                            </div>

                                            <div class="row">


                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">Adjustment</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtAdjustment" value="0.00" readonly="readonly" />
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">Reference No </label>
                                                    <input type="text" class="form-control input-sm border-primary" id="txtReferenceNo" onkeypress="return isNumberKey(event)" runat="server" disabled />
                                                </div>

                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>
            </div>
        </div>

        <div class="card-footer">
            <div class="row">

                <div class="col-sm-12">
                    <div class="btn-group mr-1 pull-right">
                        <button type="button" id="btnSave" class="btn btn-success buttonAnimation pull-right round box-shadow-1 btn-sm animated undefined" onclick="saveOrder()"><b>Submit</b></button>
                    </div>
                    <div class="btn-group mr-1 pull-right">
                        <button type="button" id="btnReset" class=" btn btn-secondary buttonAnimation pull-right round box-shadow-1 btn-sm pulse" onclick="resetOrder()"><b>Reset</b></button>
                    </div>                    

                </div>
            </div>
        </div>
    </div>
    <audio class="audios" id="yes_audio" controls preload="none" style="display: none">
        <source src="/Audio/NOExists.mp3" type="audio/mpeg" />
    </audio>
    <audio class="audios" id="No_audio" controls preload="none" style="display: none">
        <source src="/Audio/WrongProduct.mp3" type="audio/mpeg" />
    </audio>
    <audio class="audios" id="Stock" controls preload="none" style="display: none">
        <source src="/Audio/Stock.mp3" type="audio/mpeg" />
    </audio>
    <div id="SavedMassege" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-md">

            <!-- Modal content-->
            <div class="modal-content">
                <br />
                <div class="modal-header">
                    <center style="width: 100%;"><h4>Order Generated.</h4><br /> <p>Do you want to Create Another Order ?</p></center>
                </div>

                <div class="modal-body">
                    <center>
                       <button type="button" class="btn btn-primary buttonAnimation round box-shadow-1 btn-sm" onclick="createorder()">Yes</button>
                       <button type="button" class="btn btn-danger buttonAnimation round box-shadow-1 btn-sm" onclick="nocreateorder()" >No</button>          
                    </center>
                </div>
                <div class="modal-body">
                    <center>
                       <p> Do you want to Print Invoice ?</p>                      
                       <button type="button" id="NoPrint" class="btn btn-success buttonAnimation  round box-shadow-1 btn-sm" onclick="PrintInvoice(1)" >Print</button>          
                    </center>
                </div>
            </div>
        </div>
    </div>

    <div id="CustomerDueAmount" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="display: block">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Customer Due Amount</h4>

                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered " id="tblduePayment">
                            <thead class="bg-blue white">
                                <tr>
                                    <td class="SrNo text-center">SN</td>
                                    <td class="OrderNo  text-center">Order No</td>
                                    <td class="OrderDate  text-center">Order Date</td>
                                    <td class="AmtDue price">Due Amount</td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="3" class="text-right text-bold-600">Total Due Amount</td>
                                    <td id="TotalDueAmount" class="text-right text-bold-600"></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation  round box-shadow-1 btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div id="msgMassage" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">
            <!-- Modal content-->

            <div class="modal-content">
                <div class="modal-header">
                    <h4>Message</h4>
                </div>
                <div class="modal-body">
                    <div>
                        <strong style="border-color: #ac2925; border: 1px solid; padding: 8px 29px; color: #ac2925;" id="barcodemsg">Barcode does not exists</strong>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" onclick="focusonBarcode()">OK</button>
                </div>
            </div>
        </div>
    </div>
    <div id="modalOverrideQty" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">
            <!-- Modal content-->

            <div class="modal-content">
                <div class="modal-header">
                    <h4>Add Allocation Quantity</h4>
                </div>
                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col-md-6">
                            <label for="as">Available quantity</label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" disabled id="txtAvailQty" class="form-control input-sm border-primary" />
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6">
                            <label for="as">Required quantity</label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" maxlength="6" onkeypress="return isNumberKey(event)" id="txtRequiredQty" class="form-control input-sm border-primary" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success buttonAnimation  round box-shadow-1 btn-sm" onclick="OverrideQty()">Add</button>
                    <button type="button" class="btn btn-danger  buttonAnimation  round box-shadow-1 btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        function focusonBarcode() {
            $("#msgMassage").modal('hide');
            $("#txtBarcode").focus();

        }
    </script>

    <style>
        ::-webkit-scrollbar {
            width: 5px;
            height: 5px;
            background: none;
        }
    </style>
    <div id="PopPrintTemplate" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="display: block">
                    <h4 class="modal-title">Select Invoice Template</h4>

                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <input type="radio" checked name="Template" id="chkdefault" /><label>&nbsp; PSM Default  - Invoice Only</label>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <input type="radio" name="Template" id="chkdueamount" /><label>&nbsp; PSM Default</label>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <input type="radio" name="Template" id="PackingSlip" /><label>&nbsp; Packing Slip</label>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <input type="radio" name="Template" id="WithoutCategorybreakdown" /><label>&nbsp; Packing Slip - Without Category Breakdown</label>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row" id="divPSMPADefault" style="display: none">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <input type="radio" name="Template" id="PSMPADefault" /><label>&nbsp; PSM PA Default</label>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row" id="divPSMNPADefault" style="display: none">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <input type="radio" name="Template" id="rPSMNPADefault" />
                                    PSM NPA Default
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <input type="radio" name="Template" id="Template1" /><label>&nbsp; 7 Eleven (Type 1)</label>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <input type="radio" name="Template" id="Template2" /><label>&nbsp; 7 Eleven (Type 2)</label>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row form-group">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <input type="radio" name="Template" id="Templ1" />
                                    Template 1
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row form-group">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <input type="radio" name="Template" id="Templ2" />
                                    Template 2
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1 btn-sm" onclick="PrintOrder()">Print</button>
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm" onclick="location.reload()" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mdAllowQtyConfirmation" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-md">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Message</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div style="border-color: #ac2925; border: 1px solid; color: #ac2925; min-height: 45px; line-height: 20px; padding: 9px;">
                                <strong>Product stock is not available for this sales person.<br />
                                    Do you want to override existing quantity with required quantity.</strong>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" onclick="OpenSecurity()">Yes</button>
                    <button type="button" class="btn btn-danger" onclick="CloseAllowConfirm()">No</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mdSecurityCheck" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Manager security check </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">Security </div>
                        <div class="col-md-9">
                            <input type="password" id="txtManagerSecurityCheck" class="form-control input-sm border-primary" />
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-12">
                            <span id="errormsgVoid"></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1 btn-sm" id="btnMulipleCheck" onclick="CheckManagerSecurity()">OK</button>
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm" data-dismiss="modal" onclick="closeMnagerSecurity()">Close</button>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="HDDomain" runat="server" />
</asp:Content>

