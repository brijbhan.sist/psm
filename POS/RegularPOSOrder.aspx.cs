﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DLLRegularPOSOrder;
using System.Web.Script.Serialization;
using System.Data;
using Newtonsoft.Json;
using System.IO;

public partial class Warehouse_OrderNew : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/POS/JS/RegularPOSOrder.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredField'>" + text + "</script>"
                ));
        }
        try
        {
            hiddenEmpTypeVal.Value = Session["EmpTypeNo"].ToString();
        }
        catch
        {
            Session.Abandon();
            Response.Redirect("~/Default.aspx", false);
        }

        try
        {
            if (Session["EmpTypeNo"] != null)
            {
                if (Session["EmpTypeNo"].ToString() != "10")
                {
                    Session.Abandon();
                    Response.Redirect("~/Default.aspx", false);
                }
            }
            else
            {
                Session.Abandon();
                Response.Redirect("~/Default.aspx", false);
            }
        }
        catch
        {
            Session.Abandon();
            Response.Redirect("~/Default.aspx", false);
        }

        string[] GtUrl = Request.Url.Host.ToString().Split('.');
        HDDomain.Value = GtUrl[0].ToLower();
    }
    [WebMethod(EnableSession = true)]
    public static string bindDropdown()
    {
        PL_RegularPOSOrder pobj = new PL_RegularPOSOrder();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.Opcode = 44;
                pobj.SalesPersonAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"].ToString());
                pobj.LoginEmpType = Convert.ToInt32(HttpContext.Current.Session["EmpTypeNo"]);
                BL_RegularPOSOrder.bindDropdown(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0].ToString();
                }
                if (json == "")
                {
                    json = "[]";
                }
                return json;
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public static string bindUnitType(int productAutoId)
    {
        PL_RegularPOSOrder pobj = new PL_RegularPOSOrder();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.Opcode = 404;
                pobj.ProductAutoId = productAutoId;
                BL_RegularPOSOrder.bindUnitType(pobj);
                return pobj.Ds.GetXml();
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public static string selectQtyPrice(string dataValues)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValues);
            PL_RegularPOSOrder pobj = new PL_RegularPOSOrder();
            try
            {

                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustAutoId"]);
                pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]);
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.Opcode = 403;
                BL_RegularPOSOrder.selectQtyPrice(pobj);
                return pobj.Ds.GetXml();

            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string SaveDraftOrder(string dataValues)
    {
        PL_RegularPOSOrder pobj = new PL_RegularPOSOrder();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValues);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.ProductAutoId = Convert.ToInt32(jdv["productAutoId"]);
                pobj.DraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
                pobj.UnitAutoId = Convert.ToInt32(jdv["unitAutoId"]);
                pobj.ShippingType = Convert.ToInt32(jdv["ShippingType"]);
                if (jdv["Remarks"] != "")
                    pobj.Remarks = jdv["Remarks"];
                else
                    pobj.Remarks = "";
                pobj.ReqQty = Convert.ToInt32(jdv["ReqQty"]);
                pobj.IsExchange = Convert.ToInt32(jdv["IsExchange"]);
                pobj.IsFreeItem = Convert.ToInt32(jdv["IsFreeItem"]);
                pobj.IsTaxable = Convert.ToInt32(jdv["IsTaxable"]);
                pobj.MLQty = Convert.ToDecimal(jdv["MLQty"]);
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                pobj.Opcode = 108;
                BL_RegularPOSOrder.SaveDraftOrder(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {

            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public static string EditQty(string dataValues)
    {
        PL_RegularPOSOrder pobj = new PL_RegularPOSOrder();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValues);
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.DraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
                pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]);
                pobj.ReqQty = Convert.ToInt32(jdv["ReqQty"]);
                pobj.QtyPerUnit = Convert.ToInt32(jdv["TotalReqQnty"]);
                pobj.UnitPrice = Convert.ToDecimal(jdv["UnitPrice"]);
                pobj.IsExchange = Convert.ToInt32(jdv["IsExchange"]);
                pobj.Remarks = jdv["Remarks"];

                try
                {
                    pobj.IsFreeItem = Convert.ToInt32(jdv["IsFreeItem"]);
                }
                catch { }
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                pobj.Opcode = 206;
                BL_RegularPOSOrder.EditQty(pobj);
                if (!pobj.isException)
                {

                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {

            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public static string insertOrderData(string orderData, string ProductDetails)
    {

        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(orderData);
        PL_RegularPOSOrder pobj = new PL_RegularPOSOrder();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                DataTable dtOrder = new DataTable();
                dtOrder = JsonConvert.DeserializeObject<DataTable>(ProductDetails);
                if (dtOrder.Rows.Count > 0)
                {
                    pobj.TableValue = dtOrder;
                }
                pobj.Opcode = 101;
                pobj.PaymentCurrencyXml= jdv["PaymentCurrencyXml"];
                pobj.TotalCurrencyAmount = Convert.ToDecimal(jdv["TotalCurrencyAmount"]);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.ShippingType = Convert.ToInt32(jdv["ShippingType"]);
                pobj.DraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
                pobj.ShippingAutoId = Convert.ToInt32(jdv["ShippingAutoId"]);
                pobj.BillingAutoId = Convert.ToInt32(jdv["BillingAutoId"]);
                pobj.TaxType = Convert.ToInt32(jdv["TaxType"]);
                pobj.PaymentMenthod = Convert.ToInt32(jdv["PaymentMenthod"]);
                pobj.ReferenceNo = jdv["ReferenceNo"];
                pobj.CreditNo = jdv["CreditNo"];
                pobj.Remarks = jdv["Remarks"];
                pobj.CheckNo = jdv["CheckNo"];
                if (jdv["CheckDate"] != "")
                    pobj.CheckDate = Convert.ToDateTime(jdv["CheckDate"]);
                pobj.PaymentRemarks = jdv["ChequeRemarks"];
                if (HttpContext.Current.Session["EmpType"].ToString() != "Admin" || HttpContext.Current.Session["EmpType"].ToString() != "POS")
                {
                    pobj.SalesPersonAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                }
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                pobj.TotalAmount = Convert.ToDecimal(jdv["TotalAmount"]);
                if (jdv["OverallDisc"] != "")
                    pobj.OverallDisc = Convert.ToDecimal(jdv["OverallDisc"]);
                if (jdv["OverallDiscAmt"] != "")
                    pobj.OverallDiscAmt = Convert.ToDecimal(jdv["OverallDiscAmt"]);
                if (jdv["ShippingCharges"] != "")
                    pobj.ShippingCharges = Convert.ToDecimal(jdv["ShippingCharges"]);
                pobj.TotalTax = Convert.ToDecimal(jdv["TotalTax"]);
                pobj.GrandTotal = Convert.ToDecimal(jdv["GrandTotal"]);
                if (jdv["PaidAmount"] != "")
                    pobj.PaidAmount = Convert.ToDecimal(jdv["PaidAmount"]);
                if (jdv["BalanceAmount"] != "")
                    pobj.BalanceAmount = Convert.ToDecimal(jdv["BalanceAmount"]);
                if (jdv["CreditMemo"] != "")
                    pobj.CreditMemo = Convert.ToDecimal(jdv["CreditMemo"]);
                if (jdv["StoreCredit"] != "")
                    pobj.StoreCredit = Convert.ToDecimal(jdv["StoreCredit"]);
                if (jdv["PayableAmount"] != "")
                    pobj.PayableAmount = Convert.ToDecimal(jdv["PayableAmount"]);
                if (jdv["PastDue"] != "")
                    pobj.PastDue = Convert.ToDecimal(jdv["PastDue"]);
                if (jdv["Adjustment"] != "")
                    pobj.Adjustment = Convert.ToDecimal(jdv["Adjustment"]);


                BL_RegularPOSOrder.insert(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {

            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public static string BindAddressCustomer(string CustomerAutoId, string OrderAutoId)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_RegularPOSOrder pobj = new PL_RegularPOSOrder();
            try
            {

                pobj.Opcode = 41;
                pobj.CustomerAutoId = Convert.ToInt32(CustomerAutoId);
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                BL_RegularPOSOrder.SaveDraftOrder(pobj);
                return pobj.Ds.GetXml();

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }    
    [WebMethod(EnableSession = true)]
    public static string EditDraftOrder(string DraftAutoId)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_RegularPOSOrder pobj = new PL_RegularPOSOrder();
            try
            {

                pobj.DraftAutoId = Convert.ToInt32(DraftAutoId);
                BL_RegularPOSOrder.EditDraftOrder(pobj);
                return pobj.Ds.GetXml();

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string DeleteItem(string DataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(DataValue);
        PL_RegularPOSOrder pobj = new PL_RegularPOSOrder();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.DraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]);
                pobj.IsFreeItem = Convert.ToInt32(jdv["isfreeitem"]);
                pobj.IsExchange = Convert.ToInt32(jdv["IsExchange"]);
                BL_RegularPOSOrder.DeleteItem(pobj);
                if (!pobj.isException)
                {

                    return "true";
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {

            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public static string updateDraftOrder(string dataValues)
    {
        PL_RegularPOSOrder pobj = new PL_RegularPOSOrder();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValues);
                pobj.DraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.ShippingType = Convert.ToInt32(jdv["ShippingType"]);
                //if (jdv["DeliveryDate"] != "")
                //    pobj.DeliveryDate = Convert.ToDateTime(jdv["DeliveryDate"]);
                pobj.Remarks = jdv["Remark"];
                pobj.ShippingCharges = Convert.ToDecimal(jdv["ShippingCharge"]);
                pobj.OverallDisc = Convert.ToDecimal(jdv["OverallDisc"]);
                pobj.OverallDiscAmt = Convert.ToDecimal(jdv["DiscAmt"]);
                BL_RegularPOSOrder.UpdateDraftOrder(pobj);
                if (!pobj.isException)
                {
                    return pobj.DraftAutoId.ToString();
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string checkAllocationQty(string dataValue)
    {
        PL_RegularPOSOrder pobj = new PL_RegularPOSOrder();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.ProductAutoId = Convert.ToInt32(jdv["productAutoId"]);
                pobj.UnitAutoId = Convert.ToInt32(jdv["unitAutoId"]);
                pobj.ReqQty = Convert.ToInt32(jdv["ReqQty"]);
                pobj.Opcode = 109;
                BL_RegularPOSOrder.checkallocationqty(pobj);
                if (!pobj.isException)
                {
                    if (pobj.exceptionMessage == "Available")
                    {
                        return "Available";
                    }
                    else
                    {
                        return pobj.Ds.GetXml();
                    }
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return "false";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string CheckManagerSecurity(string SecurityKey)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_RegularPOSOrder pobj = new PL_RegularPOSOrder();
            try
            {

                pobj.Opcode = 45;
                pobj.SecurityKey = SecurityKey;
                BL_RegularPOSOrder.CheckMangerSecurity(pobj);
                if (pobj.exceptionMessage == "success")
                {
                    return "true";
                }
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string AddAllocationQty(string dataValue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_RegularPOSOrder pobj = new PL_RegularPOSOrder();
            try
            {

                pobj.Opcode = 46;
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.ReqQty = Convert.ToInt32(jdv["RequiredQuantity"]);
                BL_RegularPOSOrder.AddAllocatedQuantity(pobj);
                if (pobj.exceptionMessage == "NotExist")
                {
                    return "false";
                }
                else
                {
                    return "true";
                }
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string GetBarCodeDetails(string dataValues)
    {
        PL_RegularPOSOrder pobj = new PL_RegularPOSOrder();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValues);
                pobj.Opcode = 47;
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.Barcode = jdv["Barcode"];
                if (jdv["DraftAutoId"] != "")
                    pobj.DraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
                pobj.ShippingType = Convert.ToInt32(jdv["ShippingType"]);
                pobj.Remarks = jdv["Remarks"];
                if (jdv["DeliveryDate"] != "")
                    pobj.DeliveryDate = Convert.ToDateTime(jdv["DeliveryDate"]);
                if (jdv["ReqQty"] != "")
                    pobj.ReqQty = Convert.ToInt32(jdv["ReqQty"]);
                else
                    pobj.ReqQty = 1;
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                pobj.IsTaxable = Convert.ToInt32(jdv["IsTaxable"]);
                pobj.IsExchange = Convert.ToInt32(jdv["IsExchange"]);
                pobj.IsFreeItem = Convert.ToInt32(jdv["IsFreeItem"]);
                BL_RegularPOSOrder.readBarcode(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string readCurrencyDetails()
    {
        PL_RegularPOSOrder pobj = new PL_RegularPOSOrder();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.Opcode = 48;
                BL_RegularPOSOrder.readCurrencyDetails(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
}