﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="POS_CreditMemo.aspx.cs" Inherits="POS_POS_CreditMemo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Credit Memo</h3>
            <input type="hidden" id="CreditAutoId" />
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">


                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Credit Memo</a></li>
                        <li class="breadcrumb-item">New Credit Memo</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a href="/POS/POS_CreditMemoList.aspx" class="dropdown-item" id="linktoOrderList">Go to List</a>
                </div>
            </div>
        </div>
    </div>


    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <input type="hidden" id="hiddenEmpTypeVal" runat="server" />
                            <h4 class="card-title">Credit Memo Detail</h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        </div>

                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 col-sm-3">
                                        <label class="control-label">Credit Memo No</label>
                                        <input type="hidden" id="txtHCreditNo" class="form-control input-sm" />
                                        <div class="form-group">
                                            <input type="text" id="txtOrderId" class="form-control border-primary input-sm" readonly="readonly" />
                                        </div>
                                    </div>
                                    <div class="col-md-3  col-sm-3">
                                        <label class="control-label">Date</label>
                                        <div class="form-group">
                                            <input type="text" id="txtOrderDate" class="form-control border-primary input-sm" readonly="readonly" />
                                        </div>
                                    </div>

                                    <div class="col-md-3  col-sm-3">
                                        <label class="control-label">Status</label>
                                        <input type="hidden" id="hiddenOrderStatus" />
                                        <div class="form-group">
                                            <input type="text" id="txtOrderStatus" class="form-control border-primary input-sm" value="New" readonly="readonly" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label class="control-label">Credit Type</label>

                                        <div class="form-group">
                                            <input type="text" id="CreditType" class="form-control border-primary input-sm" value="" readonly="readonly" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-sm-3">
                                        <label class="control-label">
                                            Customer<span class="required">&nbsp;*</span>
                                        </label>
                                        <div class="form-group">
                                            <select class="form-control input-sm border-primary ddlreq" id="ddlCustomer" runat="server" onchange="changeCustomer();" style="width: 100% !important">
                                                <option value="0">- Select Customer -</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label class="control-label">
                                            Credit Memo Type<span class="required">&nbsp;*</span>
                                        </label>
                                        <div class="form-group">
                                            <select class="form-control input-sm border-primary ddlreq" onchange="calTotalAmount()" id="ddlCreditMemoType" runat="server"  style="width: 100% !important">
                                                
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <label class="control-label">Remark</label>
                                        <div class="form-group">
                                            <textarea id="txtRemarks" maxlength="495" class="form-control border-primary input-sm" rows="2"></textarea>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" id="panelDueAmount" style="display:none">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-header">
                                <h4 class="card-title">Customer Due Amount</h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                       <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered " id="tblduePayment">
                                            <thead class="bg-blue white">
                                                <tr>
                                                    <td class="SrNo text-center">SN</td>
                                                    <td class="Actions text-center">Action</td>
                                                    <td class="OrderNo  text-center">Order No</td>
                                                    <td class="OrderDate  text-center">Order Date</td>
                                                    <td class="AmtDue price">Due Amount ($)</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="4" class="text-right text-bold-600">Total Due Amount</td>
                                                    <td id="TotalDueAmount" class="text-right text-bold-600"></td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" id="Productpannel">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-1 col-sm-3">
                                        <label class="control-label">
                                            Barcode
                                        </label>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <input type="text" class="form-control border-primary input-sm" autocomplete="off" id="txtBarcode" disabled="disabled" runat="server" onchange="readBarcode()" placeholder="Enter Barcode here" />
                                    </div>

                                </div>
                                <br />
                                <div class="row form-group">
                                    <div class="col-md-4 col-sm-4">
                                        <label class="control-label">
                                            Product<span class="required">&nbsp;*</span>

                                        </label>

                                        <select class="form-control border-primary input-sm ddlreq" id="ddlProduct" runat="server" onchange="ChangeProduct();">
                                            <option value="0">-Select-</option>
                                        </select>

                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label class="control-label">
                                            Unit Type<span class="required">&nbsp;*</span>

                                        </label>

                                        <select class="form-control border-primary input-sm ddlsreq" onchange="UnitDetails()" id="ddlUnitType" runat="server">
                                            <option value="0">-Select-</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2 col-sm-2">
                                        <label class="control-label">
                                            Return Qty<span class="required">&nbsp;*</span>

                                        </label>
                                        <input type="text" class="form-control border-primary input-sm req" value="1" id="txtReqQty" maxlength="4" runat="server" onkeypress="return isNumberKey(event)" />

                                    </div>
                                    <div class="col-md-2 col-sm-2">
                                        <label class="control-label">
                                            Is Taxable
                                        </label>
                                        <br />
                                        <input type="checkbox" style="margin-top: 7px;" class="input-sm border-primary" id="chkIsTaxable" />
                                    </div>
                                    <div class="col-md-1 col-sm-1">
                                        <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" id="btnAdd" disabled="disabled" onclick="AddinList()" style="margin-top: 22px; color: white">&nbsp;&nbsp;&nbsp;Add&nbsp;&nbsp;&nbsp;</button>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="row form-group">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="alert alert-danger alertSmall" id="alertStockQty" style="display: none; text-align: center; color: white !important">
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" id="tblProductDetails">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="table-responsive">
                                            <table id="tblProductDetail" class="table table-striped table-bordered">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="Action text-center" style="width: 5%">Action</td>
                                                        <td class="ProId text-center" style="width: 5%">ID</td>
                                                        <td class="ProName">Product Name</td>
                                                        <td class="UnitType text-center" style="width: 5%">Unit</td>
                                                        <td class="ReturnQty text-center" style="width: 5%">Return Qty</td>
                                                        <td class="AcceptedQty text-center" style="display: none; width: 5%">Accepted<br />
                                                            Qty</td>
                                                        <td class="FreshReturn text-center">Fresh Return</td>
                                                        <td class="DemageReturn text-center">Damage Return</td>
                                                        <td class="MissingItem text-center" style="display:none">Missing Item</td>
                                                        <td class="UnitPrice price" style="width: 5%">Unit                                                       
                                                            Price</td>
                                                        <td class="OM_MinPrice  text-center" style="display: none;">Min Price</td>
                                                        <td class="OM_CostPrice  text-center" style="display: none;">Cost Price</td>
                                                        <td class="OM_BasePrice  text-center" style="display: none;">Base Price</td>
                                                        <td class="SRP price" style="width: 5%; display: none">SRP</td>
                                                        <td class="TaxRate text-center" style="width: 5%; display: none">Tax</td>
                                                        <td class="NetPrice price" style="width: 5%">Net Price</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="emptyTable" style="display: none">No Product Selected.</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7  col-sm-7">
                    <div id="hideRemark" style="display: none">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12">
                                                <label class="control-label">
                                                    Manager Remark                              
                                                </label>
                                                <div class="form-group">
                                                    <textarea id="txtManagerRemarks" runat="server" class="form-control input-sm border-primary reqdremark" rows="3"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12">
                                                <label class="control-label">
                                                    ML Tax Remark                              
                                                </label>
                                                <div class="form-group">
                                                    <textarea id="MLTaxRemark" runat="server" class="form-control input-sm border-primary" rows="3" disabled></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5  col-sm-5" id="orderSummary">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <div class="row">
                                            <label class="col-md-4 col-sm-4 form-group text-right">Total Amount</label>
                                            <div class="col-md-8  col-sm-4 form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                                            <span>$</span>
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtTotalAmount" value="0.00" readonly="readonly" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-sm-4 form-group text-right" style="white-space: nowrap">Overall Discount</label>
                                            <div class="col-sm-8">
                                                <div style="display: flex">
                                                    <%--  <div class="col-sm-6 col-md-6">--%>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <input type="text" maxlength="6" class="form-control input-sm border-primary" style="text-align: right;" onkeypress="return isNumberDecimalKey(event,this)" id="txtOverallDisc" value="0.00"
                                                                onkeyup="calOverallDisc()" />
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text border-radius" style="padding: 0rem 1rem;">
                                                                    <span>%</span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    &nbsp;  
                                                  <%--  </div>--%>
                                                    <%--<div class="col-sm-6 col-md-6">--%>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                                    <span>$</span>
                                                                </span>
                                                            </div>
                                                            <input type="text" class="form-control input-sm border-primary" style="text-align: right;" onchange="calOverallDisc1();"
                                                                onkeypress="return isNumberDecimalKey(event,this)" id="txtDiscAmt" value="0.00" />
                                                        </div>
                                                    </div>
                                                    <%-- </div>--%>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-sm-4 form-group text-right">
                                                Tax Type<span class="required"></span>
                                            </label>
                                            <div class="col-sm-8 form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                                            <span>%</span>
                                                        </span>
                                                    </div>
                                                    <select class="form-control input-sm border-primary" id="ddlTaxType"  onchange="calTotalTax()" runat="server">
                                                    </select>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-4  col-sm-4 form-group text-right">Total Tax</label>
                                            <div class="col-md-8  col-sm-8 form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                                            <span>$</span>
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control input-sm border-primary" disabled style="text-align: right" id="txtTotalTax" value="0.00" onkeypress="return isNumberDecimalKey(event,this)" onkeyup="calTotalAmount()" onkeydown="calTotalAmount()" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-sm-4 form-group text-right">ML Quantity</label>
                                            <div class="col-sm-8 form-group">
                                                <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtMLQty" value="0.00" readonly="readonly" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-sm-4 form-group text-right">ML Tax</label>
                                            <div class="col-sm-8 form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                                            <span>$</span>
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtMLTax" value="0.00" readonly="readonly" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-sm-4 form-group text-right" style="white-space: nowrap">Weight Quantity</label>
                                            <div class="col-sm-8 form-group">
                                                <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtWeightQty" value="0.00" readonly="readonly" />
                                            </div>
                                        </div>
                                        <div class="row">

                                            <label class="col-sm-4 form-group text-right">Weight Tax</label>
                                            <div class="col-sm-8 form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                                            <span>$</span>
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtWeightTax" value="0.00" readonly="readonly" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-sm-4 form-group text-right">Adjustment</label>
                                            <div class="col-sm-8 form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                                            <span>$</span>
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtAdjustment" value="0.00" readonly="readonly" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-4  col-sm-4 form-group text-right"><b>Grand Total</b></label>
                                            <div class="col-md-8  col-sm-8 form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                                            <span>$</span>
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control input-sm border-primary" style="text-align: right; font-size: 14px; font-weight: 700;" readonly="readonly" id="txtGrandTotal" value="0.00" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="row pull-right">
                                        <div class="col-md-12">

                                            <div class="btn-group mr-1 pull-right">
                                                <button type="button" id="btnSave" class="btn btn-success buttonAnimation round box-shadow-1 btn-sm" data-animation="pulse" style="display: none;">Save</button>
                                            </div>
                                            <div class="btn-group mr-1 pull-right">
                                                <button type="button" id="btnReset" onclick="Reset()" class="btn btn-secondary buttonAnimation round box-shadow-1 btn-sm " data-animation="pulse" style="display: none;">Reset</button>
                                            </div>
                                          
                                            <div class="btn-group mr-1 pull-right">
                                                <button type="button" id="btnPrintOrder" class="btn btn-info buttonAnimation round box-shadow-1 btn-sm" data-animation="pulse" style="display: none;" onclick="print_NewOrder()">Print</button>
                                            </div>
                                            <div class="btn-group mr-1 pull-right">
                                                <button type="button" id="btnBackOrder" class="btn btn-primary buttonAnimation round box-shadow-1 btn-sm" data-animation="pulse" style="display: none;"><b>Edit</b></button>
                                            </div>
                                            <div class="btn-group mr-1 pull-right">
                                                <button type="button" id="btnApproved" class="btn btn-purple buttonAnimation  round box-shadow-1 btn-sm" data-animation="pulse" style="display: none;" onclick="ApprovedOrder()">Approved</button>
                                            </div>
                                            <div class="btn-group mr-1 pull-right">
                                                <button type="button" id="btnCancel" class="btn btn-danger buttonAnimation  round box-shadow-1 btn-sm" data-animation="pulse" style="display: none;" onclick="Cancelcreditmemo()">Decline</button>
                                            </div>
                                            <div class="btn-group mr-1 pull-right">
                                                <button type="button" id="btnBackupdate" class="btn btn-primary buttonAnimationround box-shadow-1 btn-sm" data-animation="pulse" style="display: none;" onclick="Backupdate()">Back</button>
                                            </div>
                                            <div class="btn-group mr-1 pull-right">
                                                <button type="button" id="btnComplete" class="btn btn-primary buttonAnimation  round box-shadow-1 btn-sm" data-animation="pulse" style="display: none;" onclick="CompleteOrder()">Complete</button>
                                            </div>
                                            <div class="btn-group mr-1 pull-right" id="btnRemoveMlTax" style="display: none;">
                                                <button type="button" class="btn btn-secondary buttonAnimation round box-shadow-1 btn-sm " data-animation="pulse" onclick="RemoveMlTax()">Remove ML Tax</button>
                                            </div>
                                            <div class="btn-group mr-1 pull-right">
                                                <button type="button" id="btnCancelCreditMemo" class="btn btn-warning buttonAnimation round box-shadow-1 btn-sm" data-animation="pulse" onclick="SecurityEnabledVoid()" style="display: none;"><b>Cancel</b></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <input type="hidden" id="hfMLTaxStatus" />

    <div class="modal fade" id="SecurityEnabled" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Check Security </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">Security </div>
                        <div class="col-md-9">
                            <input type="password" id="txtSecurity" class="form-control border-primary reqseq input-sm" onpaste="return false;" oncopy="return false;" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <span id="errormsg"></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning buttonAnimation pull-right round box-shadow-1  btn-sm" onclick="clickonSecurity()">OK</button>
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1  btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalMLTaxConfirmation" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Remark Details </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">Remark <span class="required">*</span></div>
                        <div class="col-md-9">
                            <textarea id="txtMLRemark" maxlength="500" class="form-control border-primary reqML input-sm" rows="4"></textarea>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning buttonAnimation pull-right round box-shadow-1  btn-sm" onclick="ConfirmMLTax()">OK</button>
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1  btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <%------------------------------Security Check------------------------------------------------%>
    <div class="modal fade" id="SecurityEnabledVoid" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Security Check </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">
                            <label class="control-label">Security</label>
                            <span class="required">*</span>
                        </div>
                        <div class="col-md-9">
                            <input type="password" id="txtSecurityVoid" class="form-control input-sm border-primary" />
                        </div>
                    </div>

                    <br />
                    <div class="row">
                        <div class="col-md-12">
                            <span id="errormsgVoid"></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1 btn-sm" onclick="clickonSecurityVoid()">OK</button>
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="ModelCancelRemark" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Cancel Remark </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">
                            <label class="control-label">Remark</label>
                            <span class="required">*</span>
                        </div>
                        <div class="col-md-9">
                            <%--<input type="text" id="ReasonCancel" class="form-control input-sm border-primary" />--%>
                            <textarea name="message" id="ReasonCancel" class="form-control input-sm border-primary" rows="2" cols="30"></textarea>
                        </div>
                    </div>
                    <br />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1 btn-sm" onclick="SaveCancelReason()">Save</button>
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
      <audio class="audios" id="yes_audio" controls preload="none" style="display: none">
        <source src="/Audio/NOExists.mp3" type="audio/mpeg" />
    </audio>


    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="JS/POS_CreditMemoMaster.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
</asp:Content>

