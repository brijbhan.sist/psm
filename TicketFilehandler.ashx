﻿<%@ WebHandler Language="C#" Class="ImageUploadHandler" %>

using System;
using System.Web;
using System.IO;
using System.Data;
using Newtonsoft.Json;
public class ImageUploadHandler: IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        DataTable dt = new DataTable();
        dt.Clear();
        dt.Columns.Add("URL");
        if (context.Request.Files.Count > 0)
        {
            HttpFileCollection files = context.Request.Files;
            for (int i = 0; i < files.Count; i++)
            {
                HttpPostedFile file = files[i];
                string fname;
                if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE" || HttpContext.Current.Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                {
                    string[] testfiles = file.FileName.Split(new char[] { '\\' });
                    fname = testfiles[testfiles.Length - 1];
                }
                else
                {
                    fname = file.FileName;
                }
                string dirFullPath = HttpContext.Current.Server.MapPath("~/Attachments/");

                string timeStamp = context.Request.QueryString["timeStamp"].ToString();//DateTime.Now.ToString("yyyyMMddHHmm");
                string fnamefull = dirFullPath + timeStamp + "_" + fname;

                file.SaveAs(fnamefull);              
                
                DataRow row = dt.NewRow();
                row["URL"] = timeStamp + "_" + fname;

                dt.Rows.Add(row);
            }
        }
        context.Response.ContentType = "text/plain";
        context.Response.Write(JsonConvert.SerializeObject(dt));
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}