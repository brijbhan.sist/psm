﻿var PayAutoId; PaymentAutoId = 0; var VendorId = 0; var checkId = 0;
$(document).ready(function () {
    var d = new Date();
    CurrentDate = (d.getMonth() + 1) + "/" + d.getDate() + "/" + d.getFullYear();
    $('#txtPaymentDate').val(CurrentDate);

    $('#txtPaymentDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtSFromDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtSToDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    bindDdlList();
    Binddropdown();

    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    var getids = getQueryString('PaymentAutoId');


    VendorId = getQueryString('VendorId');
    var PaymentAutoIdss = getids;
    if (getids != null) {
        fnEdit(getids);
        $('#btnGroupDrop1').show();
    }

    if (getQueryString('PaymentId') != null || getQueryString('PaymentId') == '') {
        PaymentAutoId = getQueryString('PaymentId');
        fnEdit(PaymentAutoId);
        $('#btnGroupDrop1').show();
    } else {
        PaymentAutoId = 0;
    }
})
function Binddropdown() {
    $.ajax({
        type: "POST",
        url: "VendorPayMaster.aspx/Binddropdown",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: false,
        cache: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var getData = $.parseJSON(response.d);

                $("#ddlPaymentType option:not(:first)").remove();
                $.each(getData, function (index, item) {
                    $("#ddlPaymentType").append("<option value='" + getData[index].AutoId + "'>" + getData[index].Type + "</option>");
                });

            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}
function bindDdlList() {
    $.ajax({
        type: "POST",
        url: "/Vendor/WebAPI/WvendorPayMaster.asmx/bindDdlList",
        data: "{}",
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },

        success: function (response) {

            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var Vendor = $(xmldoc).find("Table");
                var PaymentMode = $(xmldoc).find("Table1");
                $("#ddlVendor option:not(:first)").remove();
                $.each(Vendor, function () {
                    $("#ddlVendor").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("VendorName").text() + "</option>");
                });
                $("#ddlVendor").select2();
                // var customer = $(xmldoc).find("Table1");
                $("#ddlSVendor option:not(:first)").remove();
                $.each(Vendor, function () {
                    $("#ddlSVendor").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("VendorName").text() + "</option>");
                });
                $("#ddlSVendor").select2();

                $("#ddlPaymentMode option:not(:first)").remove();
                $.each(PaymentMode, function () {
                    $("#ddlPaymentMode").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("PaymentMode").text() + "</option>");
                });
                // $("#ddlPaymentMode").val('0');

            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function paycheckRequiredField() {
    var boolcheck = true;
    $('.req').each(function () {
        if ($(this).val() == '' || parseInt($(this).val()) == 0) {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });
    $('.ddlsreq').each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == 'undefined' || $(this).val() == 'Select') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });

    $('.ddlreq').each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == 'undefined' || $(this).val() == 'Select') {
            boolcheck = false;
            $(this).closest('div').find('.select2-selection--single').attr('style', 'border:1px solid #FF9149  !important');
        } else {
            $(this).closest('div').find('.select2-selection--single').removeAttr('style');
        }
    });
    return boolcheck;
}


function DeletePay(PaymentAutoId) {
    $.ajax({
        type: "POST",
        url: "/Vendor/WebAPI/WvendorPayMaster.asmx/DeletePay",
        data: "{'PaymentAutoId':'" + PaymentAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == 'Session Expired') {
                location.href = '/';
            } else {
                if (response.d == 'true') {
                    getPayList(1);
                    swal("", "Payment deleted successfully.", "success");
                } else {
                    swal("Error !", response.d, "error");
                }
            }
        },
        failure: function (result) {
            swal("Error !", "Oops! Something went wrong.Please try later.", "error");
        },
        error: function (result) {
            swal("Error !", "Oops! Something went wrong.Please try later.", "error");
        }
    });
}
function deleterecord(PaymentAutoId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this payment!",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: false,
            },
            confirm: {
                text: "Yes, delete it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            DeletePay(PaymentAutoId);

        } else {
            swal("", "Your payment is safe.", "error");
        }
    })
}

function resetData() {
    debugger
    $('#ddlVendor').val(0).change();
    $('#ddlPaymentMode').val(0);
    $('#ddlPaymentType').val(0);
    $('#txtPaymentAmount').val('0.00');

    $('#ReferenceId').val('');
    $('#txtRemarks').val('');
    $('#txtPayId').val('');
    $('#txtDueAmount').val('0.00');
    $('#btnCancel').hide();
    $('#btnUpdate').hide();
    $('#btnReset').show();
    $('#btnSave').show();
    $("#hideCheck").hide();
}

function BindPOSList() {
    $("#ddlPaymentMode").val('0');
    $("#ddlCheckNo option:not(:first)").remove();
    $("#txtPaymentAmount").attr('disabled', false);
    $("#txtPaymentAmount").val('0.00');
    $("#txtTotalCurrencyAmount").val('0.00');
    $("#TotalAmount").text('0.00');
    $.ajax({
        type: "POST",
        url: "/Vendor/WebAPI/WvendorPayMaster.asmx/BindPOSLists",
        data: "{'VendorAutoId':'" + $("#ddlVendor").val() + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successGetCarList,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function successGetCarList(response) {
    var xmldoc = $.parseXML(response.d);
    var POList = $(xmldoc).find("Table");
    var Currency = $(xmldoc).find('Table1');
    var TotalDuesAmt = $(xmldoc).find('Table2');
    var TotalOrderAmount = 0.00, TotalPaidAmount = 0.00, DueAmount = 0.00, TotalPayAmount = 0.00, TotalpayDueAmount = 0.00;
    $("#tblPOSOrderList tbody tr").remove();

    var row = $("#tblPOSOrderList thead tr").clone(true);
    if (POList.length > 0) {
        $("#EmptyTable").hide();
        $.each(POList, function (index) {
            $(".OrderNo", row).html("<span OrderAutoId='" + $(this).find('OrderAutoId').text() + "'></span><a target='_blank' href='/Admin/stockEntry.aspx?BillAutoId=" + $(this).find("OrderNo").text() + "'>" + $(this).find("OrderNo").text() + "</a>");
            $(".OrderDate", row).text($(this).find("PODate").text());
            $(".OrderAmount", row).text($(this).find("OrderAmount").text());
            TotalOrderAmount += parseFloat($(this).find("OrderAmount").text());
            TotalPaidAmount += parseFloat($(this).find("PaidAmount").text());
            $(".PaidAmount", row).text($(this).find("PaidAmount").text());
            $(".DueAmount", row).text($(this).find("DueAmount").text());
            DueAmount += parseFloat($(this).find("DueAmount").text());


            $(".CheckBoc", row).html("<input type='checkbox' class='chkDue' onclick='funCheckBoc(this)' />");
            //if (parseFloat($(this).find("DueAmount").text()) >= parseFloat($(this).find('OrderAmount').text())) {
            $(".PayAmount", row).text('0.00').text();
            // TotalPayAmount = parseFloat($(this).find('PaidAmount').text());
            $(".payDueAmount", row).text('0.00');
            //}
            //else {
            //    $(".PayAmount", row).text($(this).find("DueAmount").text());
            //    TotalPayAmount += parseFloat($(this).find("DueAmount").text());
            //        $(".payDueAmount", row).text('0.00');
            //    }

            $("#tblPOSOrderList tbody").append(row);
            row = $("#tblPOSOrderList tbody tr:last").clone(true);
        });

    }

    $("#TotalOrderAmount").text(TotalOrderAmount.toFixed(2));
    $("#TotalPaidAmount").text(TotalPaidAmount.toFixed(2));
    $("#DueAmount").text(DueAmount.toFixed(2));
    $("#TotalPayAmount").text(TotalPayAmount.toFixed(2));
    $("#TotalpayDueAmount").text(TotalpayDueAmount.toFixed(2));

    $("#txtDueAmount").val($(TotalDuesAmt).find("TotalDueAmount").text())
    $("#tblCurrencyList tbody tr").remove();
    var row = $("#tblCurrencyList thead tr").clone(true);
    $.each(Currency, function (index) {
        $(".CurrencyName", row).html("<span CurrencyValue='" + $(this).find('CurrencyValue').text() + "'  CurrencyAutoid='" + $(this).find('Autoid').text() + "'></span>" + $(this).find("Currencyname").text());
        $(".NoofRupee", row).html("<input type='Text' id='txtTopCurrency' class='form-control input-sm text-center' onkeypress='return isNumberKey(event)' onkeyup='PaymentCal(this)' onfocus='this.select()' />");
        $(".Total", row).text('0.00');
        $("#tblCurrencyList tbody").append(row);
        row = $("#tblCurrencyList tbody tr:last").clone(true);
    });
}

function PaymentMode() {
    $("#tblPOSOrderList tbody tr .CheckBoc input").prop('checked', false);
    $("#txtPaymentAmount").val('0.00');
    if ($("#ddlPaymentMode").val() == "2") {
        $("#txtPaymentAmount").attr('disabled', true);
        $("#hideCheck").show();
        $("#hidePayment").hide();
        $("#ddlPaymentType").removeClass('ddlreq');
        $(".DuehideCurrency").hide();
        var VenderAutoId = $("#ddlVendor").val();
        $.ajax({
            type: "POST",
            url: "/Vendor/WebAPI/WvendorPayMaster.asmx/bindCheckNo",
            data: "{'VenderAutoId':'" + VenderAutoId + "','PayAutoId':'" + PaymentAutoId + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            async: false,
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                var xmldoc = $.parseXML(response.d);
                var CheckDetails = $(xmldoc).find("Table");
                $("#ddlCheckNo option:not(:first)").remove();
                $.each(CheckDetails, function () {
                    $("#ddlCheckNo").append("<option value=' " + $(this).find("AutoId").text() + "'checkamount='" + $(this).find("CheckAmount").text() + "'>" + $(this).find("CheckNO").text() + "</option>");
                });

            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    } else if ($("#ddlPaymentMode").val() == "1") {
        $(".DuehideCurrency").show();
        $("#hideCheck").hide();
        $("#hidePayment").show();
        $("#ddlPaymentType").addClass('ddlreq');
        $("#txtPaymentAmount").attr('disabled', false);
        $("#tblCurrencyList tbody input").val('');
        $("#tblCurrencyList tbody .Total").html('0.00');
        $("#tblCurrencyList tfoot tr").find("#TotalAmount").html('0.00');
        $("#txtTotalCurrencyAmount").val('0.00');

    } else {
        $(".DuehideCurrency").hide();
        $("#hideCheck").hide();
        $("#hidePayment").hide();
        $("#ddlPaymentType").removeClass('ddlreq');
        $("#txtPaymentAmount").attr('disabled', false);
        $("#txtPaymentAmount").val('0.00');
    }
    Revert();
}
$("#ddlCheckNo").change(function () {
    $("#txtPaymentAmount").val($("#ddlCheckNo option:selected").attr("checkamount"));
    $("#txtPaymentAmount").attr('disabled', true);

});

function FillPayAmount() {
    if ($("#txtPaymentAmount").val() > 0) {
        var PayAmt = $("#txtPaymentAmount").val();
        $("#txtSortAmount").val(PayAmt);
    } else {
        $("#txtSortAmount").val('0.00');
    }
    $("#txtTotalStoreCredit").attr('TotalAmount', PayAmt);
    if (parseFloat($('#txtPaymentAmount').val()).toFixed(2) < parseFloat(TotalPayAmount).toFixed(2)) {
        $('#txtStoreCredit').val('0.00');
        $('#txtTotalStoreCredit').val((parseFloat($('#txtTotalStoreCredit').attr('TotalAmount'))).toFixed(2));
    } else {
        $('#txtStoreCredit').val((parseFloat($('#txtPaymentAmount').val()) - parseFloat(TotalPayAmount)).toFixed(2))
        $('#txtTotalStoreCredit').val(((parseFloat($('#txtTotalStoreCredit').attr('TotalAmount'))) + (parseFloat($('#txtStoreCredit').val()))).toFixed(2));
    }
}

function PaymentCal(e) {
    debugger;
    var row = $(e).closest('tr');
    var Total = 0;
    var Currencyvalue = $(row).find(".CurrencyName span").attr('CurrencyValue');
    var noofRupee1 = 0;

    if ($(row).find(".NoofRupee input").val() != '') {
        noofRupee1 = $(row).find(".NoofRupee input").val();
    }
    Total = parseFloat(parseFloat(Currencyvalue) * parseFloat(noofRupee1)).toFixed(2);
    $(row).find(".Total").text(Total);
    var TotalCurrencyAmount = 0;
    if ($("#txtTotalCurrencyAmount").val() != "") {
        TotalCurrencyAmount = $("#txtTotalCurrencyAmount").val();
    }
    var SortAmount = 0
    if ($("#txtSortAmount").val() != "") {
        SortAmount = $("#txtSortAmount").val();
    }
    var TotalAmount = 0;
    $("#tblCurrencyList tbody tr").each(function () {
        TotalAmount += parseFloat($(this).find('.Total').text());
    });
    // Start Change
    var ReceiveAmount = parseFloat($("#txtPaymentAmount").val());

    if (TotalAmount > parseFloat(ReceiveAmount)) {
        $(row).find(".NoofRupee input").val('');
        $(row).find(".Total").html('0.00');
        $(row).find(".NoofRupee input").focus();
        $(row).find(".NoofRupee input").addClass('border-warning');
    }
    else {
        $(row).find(".NoofRupee input").removeClass('border-warning');
    }
    TotalAmount = 0;
    $("#tblCurrencyList tbody tr").each(function () {
        TotalAmount += parseFloat($(this).find('.Total').text());
    });
    $("#TotalAmount").text(parseFloat(TotalAmount).toFixed(2));

    if (TotalAmount == parseFloat(ReceiveAmount)) {
        $(row).find(".NoofRupee input").removeClass('border-warning');
    }

    $("#txtSortAmount").val(parseFloat($("#txtPaymentAmount").val()) - TotalAmount);
    $("#txtTotalCurrencyAmount").val((parseFloat($("#txtPaymentAmount").val()) - parseFloat($("#txtSortAmount").val())).toFixed(2));
    if ($('#txtPaymentAmount').val() > 0) {
        $("#tblPOSOrderList tbody tr .CheckBoc input").prop('checked', false);
        $('#tblPOSOrderList tbody tr .PayAmount').text('0.00');
        $('#tblPOSOrderList tbody tr .payDueAmount').text('0.00');
        $('#TotalPayAmount').text('0.00');
        $('#TotalpayDueAmount').text('0.00');
    }
    //End 24-08-2019 By Rizwan Ahmad
}
function ReceivedPay() {
    $("#txtPaymentAmount").css('border-color', '#ccc');
    var ReceiveAmount = 0.00;

    if ($('#ddlPaymentMode').val() == 5) {
        var StoreCredit = parseFloat($('#CreditAmount').text());
        if ($("#txtPaymentAmount").val() != '') {
            ReceiveAmount = parseFloat($("#txtPaymentAmount").val());
        }

        if (parseFloat(StoreCredit) < ReceiveAmount) {
            $("#txtPaymentAmount").css('border-color', 'red')
        }
        $('#txtStoreCredit').val((parseFloat(StoreCredit) - parseFloat(ReceiveAmount)).toFixed(2));
        CusStoreCredit = -1 * ReceiveAmount;
        StoreCredit = parseFloat($('#txtTotalStoreCredit').attr('TotalAmount'));
    } else {
        if ($('#txtPaymentAmount').val() != "") {
            ReceiveAmount = parseFloat($('#txtPaymentAmount').val());
        }
        $('#txtStoreCredit').val(parseFloat(ReceiveAmount).toFixed(2));
        CusStoreCredit = $('#txtStoreCredit').val();
        StoreCredit = parseFloat($('#txtTotalStoreCredit').attr('TotalAmount'));
    }


    $("#tblPOSOrderList tbody tr .CheckBoc input").prop('checked', false);
    $('#tblPOSOrderList tbody tr .PayAmount').text('0.00');
    $('#tblPOSOrderList tbody tr .payDueAmount').text('0.00');
    $('#TotalPayAmount').text('0.00');
    $('#TotalpayDueAmount').text('0.00');


    $('#txtTotalStoreCredit').val((parseFloat(StoreCredit) + parseFloat(CusStoreCredit)).toFixed(2));

}
function funCheckBoc(e) {
    if ($("#ddlPaymentMode").val() != "0") {
        debugger;
        var ReceiveAmount = 0.00;
        var PaidAmount = 0.00;
        //Start
        if ($("#ddlPaymentMode").val() == "1") {

            if ($("#txtTotalCurrencyAmount").val() == "0.00" || $("#txtTotalCurrencyAmount").val() == "") {
                swal("Alert", "Please fill currency denomination.", "error");
                $("#txtTopCurrency").addClass("border-warning");
                $("#txtTopCurrency").focus();
                $("#tblPOSOrderList tbody tr .CheckBoc input").prop('checked', false);


                debugger;
                $("#tblCurrencyList tbody tr ").each(function () {
                    if ($(this).find('.NoofRupee input').val() == "") {
                        debugger;
                        $("#txtTopCurrency").addClass("border-warning");
                    } else {
                        $("#txtTopCurrency").removeClass("border-warning");
                    }
                })
                return;
            }
            ReceiveAmount = $("#txtTotalCurrencyAmount").val();
        }
        else {
            if ($("#txtPaymentAmount").val() != "") {
                ReceiveAmount = $("#txtPaymentAmount").val();
            }
        }
        //End 24-08-2019 By Rizwan Ahmad
        var tr = $(e).closest('tr');
        var DueAmount = 0.00;
        DueAmount = parseFloat(tr.find('.DueAmount').text());
        if ($(e).prop('checked') == true) {

            if (parseFloat(ReceiveAmount) > 0) {

                var PayAmount = 0.00;
                $("#tblPOSOrderList tbody tr").each(function () {
                    PayAmount += parseFloat($(this).find('.PayAmount').text());
                });

                var Amount = parseFloat(ReceiveAmount) - parseFloat(PayAmount);
                if (parseFloat(DueAmount) > parseFloat(Amount)) {
                    if (parseFloat(Amount) == 0) {
                        tr.find('.PayAmount').text('0.00');
                        tr.find('.payDueAmount').text('0.00');
                        $(e).prop('checked', false);
                    } else {
                        tr.find('.PayAmount').text(Amount.toFixed(2));
                        tr.find('.payDueAmount').text((parseFloat(DueAmount) - parseFloat(Amount)).toFixed(2));
                    }

                }
                else {
                    PaidAmount = parseFloat(DueAmount) - parseFloat(Amount);
                    tr.find('.PayAmount').text(DueAmount.toFixed(2));
                    tr.find('.payDueAmount').text('0.00');
                }

            } else {
                $("#txtPaymentAmount").focus();
                $(e).prop('checked', false);
                $("#txtPaymentAmount").css('border-color', 'Red');
            }
        } else {
            tr.find('.PayAmount').text('0.00');
            tr.find('.payDueAmount').text('0.00');
        }
        var PayAmount = 0.00, payDueAmount = 0.00;
        $("#tblPOSOrderList tbody tr").each(function () {
            if ($(this).find('.CheckBoc input').prop('checked') == true) {
                PayAmount += parseFloat($(this).find('.PayAmount').text());
                payDueAmount += parseFloat($(this).find('.payDueAmount').text());
            }
        });
        PayAmount = parseFloat(PayAmount).toFixed(2);
        $('#TotalPayAmount').text(parseFloat(PayAmount).toFixed(2))
        $('#TotalpayDueAmount').text(payDueAmount.toFixed(2));
        if ($('#ddlPaymentMode').val() == 5) {
            //$('#txtStoreCredit').val((parseFloat($('#CreditAmount').text()) - parseFloat($('#txtPaymentAmount').val())).toFixed(2));
            ReceiveAmount = ((0 - parseFloat($('#txtPaymentAmount').val()))).toFixed(2);
        } else {
            //$('#txtStoreCredit').val((parseFloat(ReceiveAmount) - parseFloat(PayAmount)).toFixed(2));
            //var TotalStoreCredit = $('#txtTotalStoreCredit').attr('TotalAmount');
            var TOTAL = parseFloat((parseFloat(ReceiveAmount) - parseFloat(PayAmount)));
        }
    } else {
        swal("Alert", "Please select any Payment Mode.", "error");
    }
}

//-----------------------------------------------------------Save Payment-------------------------------------------
function SavePayAmount() {
    $("#ddlCheckNo").removeClass('border-warning');
    if ($("#ddlVendor").val() != 0) {
        if (checkVRequiredField()) {
            if ($("#ddlPaymentMode").val() == 2) {
                if ($("#ddlCheckNo").val() == 0) {
                    toastr.error('Please select check', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    $("#ddlCheckNo").addClass('border-warning');
                    return;
                }
            }
            if ($("#ddlPaymentMode").val() != 0) {
                if ($('#tblPOSOrderList tbody tr .CheckBoc input[type="checkbox"]:checked').length == 0) {
                    toastr.error('Please select atleast one bill no.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    return;
                }
                if ((parseFloat($('#txtPaymentAmount').val())) == (parseFloat($('#TotalPayAmount').text()))) {

                    var PaymentCurrencyXml = '';
                    var SortAmount = 0;
                    if ($("#txtSortAmount").val() != '') {
                        SortAmount = $("#txtSortAmount").val();
                    }
                    if (dynamicALL('Save')) {
                        if ((parseInt($('#ddlPaymentMode').val()) == '1')) {
                            $("#tblCurrencyList tbody tr").each(function () {
                                if ($(this).find(".NoofRupee input").val() != 0 && $(this).find(".NoofRupee input").val() != '') {
                                    PaymentCurrencyXml += '<PaymentCurrencyXml>';
                                    PaymentCurrencyXml += '<CurrencyAutoId><![CDATA[' + $(this).find('.CurrencyName span').attr('CurrencyAutoid') + ']]></CurrencyAutoId>';
                                    PaymentCurrencyXml += '<NoOfValue><![CDATA[' + $(this).find(".NoofRupee input").val() + ']]></NoOfValue>';
                                    PaymentCurrencyXml += '<TotalAmount><![CDATA[' + $(this).find(".Total").text() + ']]></TotalAmount>';
                                    PaymentCurrencyXml += '</PaymentCurrencyXml>';
                                }
                            });
                        }
                        var TotalCurrencyAmount = 0;

                        if ($("#txtTotalCurrencyAmount").val() != "") {
                            TotalCurrencyAmount = $("#txtTotalCurrencyAmount").val();
                        }

                        var data = {
                            VendorAutoId: $("#ddlVendor").val(),
                            PaymentAmount: $("#txtPaymentAmount").val(),
                            PaymentDate: $("#txtPaymentDate").val(),
                            PaymentMethod: $("#ddlPaymentMode").val(),
                            PaymentType: $("#ddlPaymentType").val(),
                            ReferenceNo: $("#ReferenceId").val(),
                            PaymentRemarks: $("#txtRemarks").val(),
                            CheckAutoId: $("#ddlCheckNo").val(),
                            SortAmount: SortAmount,
                            TotalCurrencyAmount: TotalCurrencyAmount,
                            PaymentCurrencyXml: PaymentCurrencyXml
                        }

                        var ReceivedAmount = 0.00;
                        if ((parseInt($('#ddlPaymentMode').val()) == 1)) {
                            ReceivedAmount = $("#txtTotalCurrencyAmount").val();
                        } else {
                            ReceivedAmount = $("#txtPaymentAmount").val();
                        }

                        var ListReceivedAmount = 0.00;
                        var tblReceivedOrder = [];
                        $("#tblPOSOrderList tbody tr").each(function () {
                            if ($(this).find('.CheckBoc input').prop('checked') == true) {
                                tblReceivedOrder.push({
                                    OrderAutoId: $(this).find('.OrderNo span').attr('OrderAutoId'),
                                    PayAmount: parseFloat($(this).find('.PayAmount').text())
                                });
                                ListReceivedAmount += parseFloat($(this).find('.PayAmount').text());
                            }
                        });
                        if ((parseInt($('#ddlPaymentMode').val()) == 5) && (parseFloat(ReceivedAmount) != parseFloat(ListReceivedAmount))) {
                            toastr.error('Payment Amount and Order Amount should be equal.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                        }
                        else if (parseInt($('#ddlPaymentMode').val()) == 0) {
                            toastr.error('Please select any Payment mode', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                        }

                        //else if (parseFloat($('#txtPaymentAmount').val()) != parseFloat($('#txtTotalCurrencyAmount').val())) {
                        //    toastr.error('.Payment amount and Bill settled amount should be equal.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                        //}
                        else if ((parseInt($('#ddlPaymentMode').val()) == 5) && (parseFloat(ReceivedAmount) > parseFloat($('#CreditAmount').text()))) {
                            toastr.error('Payment Amount should be equal or less than Total Store Credit.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                        }
                        else if (ReceivedAmount == 0) {
                            toastr.error('Payment amount should be greater than zero.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                        }
                        else if ((parseFloat(ListReceivedAmount) > parseFloat(TotalCurrencyAmount)) && (parseInt($('#ddlPaymentMode').val()) == 1) && $('#ddlPaymentMode').val() == '') {
                            toastr.error('Total amount should be equal to payment amount.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                        }

                        else {

                            if (ListReceivedAmount < ReceivedAmount) {
                                swal({
                                    title: "Are you sure?",
                                    text: "You want to payment settlement.",
                                    icon: "warning",
                                    showCancelButton: true,
                                    buttons: {
                                        cancel: {
                                            text: "No, cancel!",
                                            value: null,
                                            visible: true,
                                            className: "btn-warning",
                                            closeModal: false,
                                        },
                                        confirm: {
                                            text: "Yes, Receive it!",
                                            value: true,
                                            visible: true,
                                            className: "",
                                            closeModal: false
                                        }
                                    }
                                }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        settlepayment(tblReceivedOrder, data);
                                    } else {
                                        swal("Thanks you", "Process aborted.", "error");
                                    }
                                })

                            } else {
                                settlepayment(tblReceivedOrder, data);
                            }

                        }
                    } else {
                        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    }

                }

                else {
                    toastr.error('Payment amount and Bill settled amount should be equal.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                }
            }
            else {
                toastr.error('Please select any Payment Mode.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            }
        }
        else {
            toastr.error('Please select any Payment Type.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        }

    } else {
        toastr.error('Please select any vendor', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}

function settlepayment(tblReceivedOrder, data) {

    $.ajax({
        type: "Post",
        url: "/Vendor/WebAPI/WvendorPayMaster.asmx/savePayment",
        data: "{'TableValues':'" + JSON.stringify(tblReceivedOrder) + "','dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (data) {
            if (data.d == 'Session Expired') {
                location.href = '/';
            } else if (data.d == 'true') {
                swal("", "Payment settled successfully.", "success").then(function () {
                    location.reload();
                });
            } else {
                swal("Error", data.d, "error");
            }
        },
        error: function (result) {
        },
        failure: function (result) {
        }
    });
}
//----------------------------------------------------Edit Vendor Payment settlement---------------------------------------
function fnEdit(PaymentAutoid) {
    debugger;
    PayAutoId = PaymentAutoid;
    $.ajax({
        type: "POST",
        url: "/Vendor/WebAPI/WvendorPayMaster.asmx/Editpay",
        data: "{'PaymentAutoid':'" + PayAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var PayDetails = $(xmldoc).find("Table");
                var PaymentDetails = $(xmldoc).find('Table2');
                var TotalDuesAmt = $(xmldoc).find('Table3');
                var CheckDetailss = $(xmldoc).find('Table4');

                if (CheckDetailss.length > 0) {
                    $.each(CheckDetailss, function () {
                        $("#ddlCheckNo").append("<option value='" + $(this).find("AutoId").text() + "'checkamount='" + $(this).find("CheckAmount").text() + "'>" + $(this).find("CheckNO").text() + "</option>");
                    });
                }

                $('#ddlCheckNo').val($(PayDetails).find('CheckAutoId').text());

                $('#btnSave').hide();
                $('#btnReset').hide();
                $('#btnUpdate').show();
                $('#btnCancel').show();
                $('.nav-tabs').find('a:eq(1)').attr('class', 'nav-link');
                $('.nav-tabs').find('a:eq(0)').attr('class', 'nav-link active');
                $('.nav-tabs').find('a:eq(0)').attr('aria-expanded', 'true');
                $('#DueBills').attr('class', 'tab-pane fade active in show');
                $('#PaymentHistory').attr('class', 'tab-pane fade');
                $('#txtPayId').val($(PayDetails).find('PayId').text());
                $('#ddlVendor').val($(PayDetails).find('VendorAutoId').text()).select2();
                $('#ddlVendor').attr('disabled', 'disabled');
                $('#txtPaymentDate').val($(PayDetails).find('PaymentDate').text());

                $('#ddlPaymentMode').val($(PayDetails).find('PaymentMode').text()).change();
                $('#ddlPaymentType').val($(PayDetails).find('PaymentType').text()).change();

                if ($(PayDetails).find('PaymentMode').text() == '2') {
                    $("#hideCheck").show();
                    $(".DuehideCurrency").hide();
                    $("#txtPaymentAmount").attr('disabled', 'disabled');
                }

                $('#txtPaymentAmount').val($(PayDetails).find('PaymentAmount').text());
                $('#ReferenceId').val($(PayDetails).find('ReferenceID').text());
                $('#txtRemarks').val($(PayDetails).find('Remark').text());

                $("#txtDueAmount").val($(TotalDuesAmt).find("TotalDueAmount").text());


                if (PaymentDetails.length > 0) {

                    var TotalOrderAmount1 = 0.00, TotalPaidAmount1 = 0.00, DueAmount1 = 0.00, TotalPayAmount1 = 0.00, TotalpayDueAmount1 = 0.00;
                    $("#tblPOSOrderList tbody tr").remove();
                    var row = $("#tblPOSOrderList thead tr").clone();
                    console.log(PaymentDetails);
                    $.each(PaymentDetails, function (index) {
                        $(".OrderNo", row).html("<span OrderAutoId='" + $(this).find('OrderAutoId').text() + "'></span><a target='_blank' href='/sales/OrderMaster.aspx?OrderNo=" + $(this).find("OrderNo").text() + "'>" + $(this).find("OrderNo").text() + "</a>");
                        $(".OrderDate", row).text($(this).find("PODate").text());
                        $(".OrderAmount", row).text($(this).find("OrderAmount").text());
                        TotalOrderAmount1 += parseFloat($(this).find("OrderAmount").text());
                        $(".PaidAmount", row).text($(this).find("PaidAmount").text());
                        $(".DueAmount", row).text($(this).find("DueAmount").text());
                        if ($(this).find('Pay').text() == 0) {
                            $(".CheckBoc", row).html("<input type='checkbox' onclick='funCheckBoc(this)' checked />");
                            var paysAmount = (parseFloat($(this).find("PaidAmount").text())) - (parseFloat($(this).find('PaymentAmount').text()).toFixed(2));
                            $(".PaidAmount", row).text(parseFloat(paysAmount).toFixed(2));
                            TotalPaidAmount1 += paysAmount;
                            var MainDueAmount = parseFloat($(this).find("OrderAmount").text()) - paysAmount
                            $(".DueAmount", row).text(parseFloat(($(this).find("OrderAmount").text()) - paysAmount).toFixed(2));
                            $(".PayAmount", row).text($(this).find('PaymentAmount').text());
                            $(".payDueAmount", row).text($(this).find("DueAmount").text());
                            DueAmount1 += parseFloat(MainDueAmount);
                            TotalPayAmount1 += parseFloat($(this).find('PaymentAmount').text());
                            TotalpayDueAmount1 += parseFloat($(this).find("DueAmount").text());

                        } else {
                            $(".PaidAmount", row).text($(this).find("PaidAmount").text());
                            TotalPaidAmount1 += parseFloat($(this).find("PaidAmount").text());
                            $(".CheckBoc", row).html("<input type='checkbox' onclick='funCheckBoc(this)' />");
                            $(".DueAmount", row).text($(this).find("DueAmount").text());
                            $(".PayAmount", row).text('0.00');
                            $(".payDueAmount", row).text('0.00');
                            DueAmount1 += parseFloat($(this).find("DueAmount").text());
                        }
                        $("#TotalOrderAmount").text(TotalOrderAmount1.toFixed(2));
                        $("#TotalPaidAmount").text(TotalPaidAmount1.toFixed(2));
                        $("#DueAmount").text(DueAmount1.toFixed(2));
                        $("#TotalPayAmount").text(TotalPayAmount1.toFixed(2));
                        $("#TotalpayDueAmount").text(TotalpayDueAmount1.toFixed(2));
                        $("#txtDueAmount").val($(TotalDuesAmt).find("TotalDueAmount").text());

                        $("#tblPOSOrderList tbody").append(row);
                        row = $("#tblPOSOrderList tbody tr:last").clone(true);
                    });

                }

                var PaymentHistoryCurrencyList = $(xmldoc).find('Table1');
                var Total = 0.00;
                $("#tblCurrencyList tbody tr").remove();
                var row = $("#tblCurrencyList thead tr").clone(true);
                if (PaymentHistoryCurrencyList.length > 0) {

                    $.each(PaymentHistoryCurrencyList, function (index) {
                        $(".CurrencyName", row).html("<span CurrencyValue='" + $(this).find('CurrencyValue').text() + "'  CurrencyAutoid='" + $(this).find('AutoId').text() + "'></span>" + $(this).find("CurrencyName").text());
                        $(".NoofRupee", row).html("<input type='Text'  value='" + $(this).find("NoofValue").text() + "' class='form-control input-sm text-center' onkeypress='return isNumberKey(event)' onkeyup='PaymentHostoryPaymentCal(this)' onfocus='this.select()'/>");
                        $(".Total", row).html($(this).find("TotalAmount").text());
                        $("#tblCurrencyList tbody").append(row);
                        row = $("#tblCurrencyList tbody tr:last").clone(true);
                        Total = parseFloat(Total) + parseFloat($(this).find("TotalAmount").text());
                    });
                }
                $("#TotalAmount").text(parseFloat(Total).toFixed(2));
                $("#txtTotalCurrencyAmount").val($("#TotalAmount").text());


            }
            else {
                location.href = '/';
            }

        },

        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

//-------------------------------------------------------------Update Payment settlement---------------------------------------------------
function UpdatePayAmount() {
    if ((parseFloat($('#txtPaymentAmount').val())) == (parseFloat($('#TotalPayAmount').text()))) {
        var PaymentCurrencyXml = '';
        var SortAmount = 0;
        if ($("#txtSortAmount").val() != '') {
            SortAmount = $("#txtSortAmount").val();
        }
        if (dynamicALL('Update')) {
            if (checkVRequiredField()) {
                if ((parseInt($('#ddlPaymentMode').val()) == '1')) {
                    $("#tblCurrencyList tbody tr").each(function () {
                        if ($(this).find(".NoofRupee input").val() != 0 && $(this).find(".NoofRupee input").val() != '') {
                            PaymentCurrencyXml += '<PaymentCurrencyXml>';
                            PaymentCurrencyXml += '<CurrencyAutoId><![CDATA[' + $(this).find('.CurrencyName span').attr('CurrencyAutoid') + ']]></CurrencyAutoId>';
                            PaymentCurrencyXml += '<NoOfValue><![CDATA[' + $(this).find(".NoofRupee input").val() + ']]></NoOfValue>';
                            PaymentCurrencyXml += '<TotalAmount><![CDATA[' + $(this).find(".Total").text() + ']]></TotalAmount>';
                            PaymentCurrencyXml += '</PaymentCurrencyXml>';
                        }
                    });
                }
                var TotalCurrencyAmount = 0;

                if ($("#txtTotalCurrencyAmount").val() != "") {
                    TotalCurrencyAmount = $("#txtTotalCurrencyAmount").val();
                }

                var data = {
                    VendorAutoId: $("#ddlVendor").val(),
                    PaymentAmount: $("#txtPaymentAmount").val(),
                    PaymentDate: $("#txtPaymentDate").val(),
                    PaymentMethod: $("#ddlPaymentMode").val(),
                    PaymentType: $("#ddlPaymentType").val(),
                    ReferenceNo: $("#ReferenceId").val(),
                    PaymentRemarks: $("#txtRemarks").val(),
                    CheckAutoId: $("#ddlCheckNo").val(),
                    SortAmount: SortAmount,
                    TotalCurrencyAmount: TotalCurrencyAmount,
                    PaymentCurrencyXml: PaymentCurrencyXml,
                    PaymentAutoId: PaymentAutoId
                }

                var ReceivedAmount = 0.00;
                if ((parseInt($('#ddlPaymentMode').val()) == 1)) {
                    ReceivedAmount = $("#txtTotalCurrencyAmount").val();
                } else {
                    ReceivedAmount = $("#txtPaymentAmount").val();
                }

                var ListReceivedAmount = 0.00;
                var tblReceivedOrder = [];
                $("#tblPOSOrderList tbody tr").each(function () {
                    if ($(this).find('.CheckBoc input').prop('checked') == true) {
                        tblReceivedOrder.push({
                            OrderAutoId: $(this).find('.OrderNo span').attr('OrderAutoId'),
                            PayAmount: parseFloat($(this).find('.PayAmount').text())
                        });
                        ListReceivedAmount += parseFloat($(this).find('.PayAmount').text());
                    }
                });
                if ((parseInt($('#ddlPaymentMode').val()) == 5) && (parseFloat(ReceivedAmount) != parseFloat(ListReceivedAmount))) {
                    toastr.error('Payment Amount and Order Amount should be equal.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                }
                else if (parseInt($('#ddlPaymentMode').val()) == 0) {
                    toastr.error('Please select any Payment mode', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                }
                else if (parseInt($('#ddlPaymentType').val()) == 0) {
                    toastr.error('Please select Any Payment type.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    $("#ddlPaymentType").addClass('border-warning');
                    return;
                }

                //else if (parseFloat($('#txtPaymentAmount').val()) != parseFloat($('#txtTotalCurrencyAmount').val())) {
                //    toastr.error('.Payment amount and Bill settled amount should be equal.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                //}
                else if ((parseInt($('#ddlPaymentMode').val()) == 5) && (parseFloat(ReceivedAmount) > parseFloat($('#CreditAmount').text()))) {
                    toastr.error('Payment Amount should be equal or less than Total Store Credit.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                }
                else if (ReceivedAmount == 0) {
                    toastr.error('Payment amount should be greater than zero.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                }
                else if ((parseFloat(ListReceivedAmount) > parseFloat(TotalCurrencyAmount)) && (parseInt($('#ddlPaymentMode').val()) == 1) && $('#ddlPaymentMode').val() == '') {
                    toastr.error('Total amount should be equal to payment amount.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                }
                else {

                    if (ListReceivedAmount < ReceivedAmount) {
                        swal({
                            title: "Are you sure?",
                            text: "You want to payment settlement.",
                            icon: "warning",
                            showCancelButton: true,
                            buttons: {
                                cancel: {
                                    text: "No, cancel!",
                                    value: null,
                                    visible: true,
                                    className: "btn-warning",
                                    closeModal: false,
                                },
                                confirm: {
                                    text: "Yes, Receive it!",
                                    value: true,
                                    visible: true,
                                    className: "",
                                    closeModal: false
                                }
                            }
                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                Updatesettlepayment(tblReceivedOrder, data);
                            } else {
                                swal("Thanks you", "Process aborted.", "error");
                            }
                        })

                    } else {
                        Updatesettlepayment(tblReceivedOrder, data);
                    }

                }
            }
            else {
                toastr.error('Please select any Payment Type.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            }
        }
        else {
            toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        }

    } else {
        toastr.error('Payment amount and Bill settled amount should be equal.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}

function Updatesettlepayment(tblReceivedOrder, data) {
    $.ajax({
        type: "Post",
        url: "/Vendor/WebAPI/WvendorPayMaster.asmx/UpdatePaymentsettle",
        data: "{'TableValues':'" + JSON.stringify(tblReceivedOrder) + "','dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (data) {
            if (data.d == 'Session Expired') {
                location.href = '/';
            } else if (data.d == 'true') {
                swal("", "Payment updated successfully.", "success").then(function () {
                    fnEdit(PaymentAutoid);
                });
            } else {
                swal("Error", data.d, "error");
            }
        },
        error: function (result) {
        },
        failure: function (result) {
        }
    });
}

function PaymentHostoryPaymentCal(e) {


    var row = $(e).closest('tr');
    var TotalAmount = 0;
    var Total = 0;
    var Currencyvalue = $(row).find(".CurrencyName span").attr('CurrencyValue');
    var noofRupee = 0;
    if ($(row).find(".NoofRupee input").val() != '') {
        noofRupee = $(row).find(".NoofRupee input").val();
    }

    Total = parseFloat(parseFloat(Currencyvalue) * parseFloat(noofRupee)).toFixed(2);
    $(row).find(".Total").text(Total);
    var TotalCurrencyAmount = 0;
    if ($("#txtTotalCurrencyAmount").val() != "") {
        TotalCurrencyAmount = $("#txtTotalCurrencyAmount").val();
    }
    var SortAmount = 0
    if ($("#txtSortAmount").val() != "") {
        SortAmount = $("#txtSortAmount").val();
    }


    $("#tblCurrencyList tbody tr").each(function () {
        TotalAmount += parseFloat($(this).find('.Total').text());
    });
    ;
    // Start Change
    var ReceiveAmount = $("#txtPaymentAmount").val();

    if (TotalAmount > parseFloat(ReceiveAmount)) {
        $(row).find(".NoofRupee input").val('');
        $(row).find(".Total").text('0.00');
        $(row).find(".NoofRupee input").focus();
        $(row).find(".NoofRupee input").addClass('border-warning');

    }
    TotalAmount = 0;
    $("#tblCurrencyList tbody tr").each(function () {
        TotalAmount += parseFloat($(this).find('.Total').text());
    });

    $("#TotalAmount").text(parseFloat(TotalAmount).toFixed(2));
    $("#txtSortAmount").val(parseFloat($("#txtPaymentAmount").val()) - TotalAmount);
    $("#txtTotalCurrencyAmount").val(parseFloat(TotalAmount).toFixed(2));

    $("#tblPOSOrderList tbody tr .CheckBoc input").prop('checked', false);
    $('#tblPOSOrderList tbody tr .PayAmount').text('0.00');
    $('#tblPOSOrderList tbody tr .payDueAmount').text('0.00');
    $('#TotalPayAmount').text('0.00');
    $('#TotalpayDueAmount').text('0.00');
    //End 24-08-2019 By Rizwan Ahmad

}


function Back() {
    location.href = '/Admin/ViewVenderDetails.aspx?vendorId=' + VendorId;
}

function Newpayment() {
    location.href = "/Vendor/VendorPayMaster.aspx";
}
function Revert() {
    $("#tblPOSOrderList tbody tr .CheckBoc input").prop('checked', false);
    $('#tblPOSOrderList tbody tr .PayAmount').text('0.00');
    $('#tblPOSOrderList tbody tr .payDueAmount').text('0.00');
    $('#TotalPayAmount').text('0.00');
    $('#TotalpayDueAmount').text('0.00');
    $("#tblCurrencyList tbody tr .NoofRupee input").val('');
    $("#tblCurrencyList tbody tr .Total").text('0.00');
    $("#TotalAmount").text('0.00');
    $("#txtSortAmount").val('0.00');
    $("#txtTotalCurrencyAmount").val('0.00');
}

function checkVRequiredField() {
    var boolcheck = true; 
    $('.ddlreq').each(function () {
        if ($(this).val().trim() == '' || $(this).val().trim() == '0' || $(this).val().trim() == '-Select-' || $(this).val().trim() == null) {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });
    return boolcheck;
}