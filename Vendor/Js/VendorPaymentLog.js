﻿$(document).ready(function () {
    $('#txtSFromDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtSToDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    }); 
    var d = new Date();
    var month = d.getMonth() + 1;
    if (month.toString().length == 1) {
        month = '0' + month;
    }
    var day = d.getDate();
    if (day.toString().length == 1) {
        day = '0' + day;
    }
    var year = d.getFullYear();
    $("#txtSFromDate").val(month + '/' + day + '/' + year);
    $("#txtSToDate").val(month + '/' + day + '/' + year);

    bindPaymentType();
    GetVendorPaymentLog(1);
});

function setdatevalidation(val) {
    var fdate = $("#txtSFromDate").val();
    var tdate = $("#txtSToDate").val();
    if (val == 1) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtSToDate").val(fdate);
        }
    }
    else if (val == 2) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtSFromDate").val(tdate);
        }
    }
}
function Pagevalue(e) {
    GetVendorPaymentLog(parseInt($(e).attr("page")));
}
function bindPaymentType() {
    $.ajax({
        type: "POST",
        url: "VendorPaymentLog.aspx/bindPaymentType",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var PaymentType = $(xmldoc).find("Table");

            $("#ddlPaymentType option:not(:first)").remove();
            $.each(PaymentType, function () {
                $("#ddlPaymentType").append($("<option value='" + $(this).find("AutoID").text() + "'>" + $(this).find("PaymentMode").text() + "</option>"));
            });

        },
        failure: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        },
        error: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        }
    });
};

function GetVendorPaymentLog(pageIndex) {
    debugger;
    var data = {
        VendorName: $("#txtSVendorName").val().trim(),
        PaymentType: $("#ddlPaymentType").val(),
        pageIndex: pageIndex,
        PageSize: $("#ddlPageSize").val(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
    };

    $.ajax({
        type: "POST",
        async: false,
        url: "VendorPaymentLog.aspx/GetVendorPaymentLog",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },

        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var PayDetails = $(xmldoc).find("Table1");
                var OverallTotal = $(xmldoc).find("Table2");
               
                $("#tblPayDetails tbody tr").remove();
                var row = $("#tblPayDetails thead tr").clone(true);
                var TotalAmount = 0;
                if (PayDetails.length > 0) {
                    $("#EmptyTable").hide();
                    $.each(PayDetails, function () { 
                        $(".action", row).html("<a title='Edit' href='/Vendor/ViewVendorPaymentLog.aspx?PaymentId=" + $(this).find("PaymentAutoId").text() + "&VendorId=" + $(this).find("AutoId").text()+"'><span class='la la-eye'></span></a></b>");
                        $(".PayID", row).text($(this).find("PayId").text());
                        $(".payby", row).text($(this).find("payby").text());
                        $(".Vendor", row).text($(this).find("VendorName").text());
                        $(".PaymentDate", row).text($(this).find("PaymentDate").text());
                        $(".PaymentAmount", row).text($(this).find("PaymentAmount").text());
                        $(".PaymentMode", row).text($(this).find("PaymentMode").text());
                        $(".PaymentType", row).text($(this).find("Type").text());
                        $(".ReferenceId", row).text($(this).find("NoOfBill").text());
                        $(".Status", row).text($(this).find("Status").text());
                        $(".Remark", row).text($(this).find("Remark").text());
                        if ($(this).find("Status").text() == '1') {
                            $(".Status", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("StatusType").text() + "</span>");
                        } else {
                            $(".Status", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("StatusType").text() + "</span>");
                        }
                        TotalAmount += parseFloat($(this).find("PaymentAmount").text());
                        $("#tblPayDetails tbody").append(row);
                        row = $("#tblPayDetails tbody tr:last").clone(true);
                    });
                    $("#TotalAmount").html(parseFloat(TotalAmount).toFixed(2));
                } else {
                    $("#EmptyTable").show();
                    $("#TotalAmount").html('0.00');
                }

                if (OverallTotal.length > 0) {
                    if ($(OverallTotal).find("TotalAmount").text() == "") {
                        $("#TotalAmounts").html('0.00');
                    }
                    else {
                        var TotalAmt = parseFloat($(OverallTotal).find("TotalAmount").text());
                        $("#TotalAmounts").html(TotalAmt.toFixed(2));
                    }
                }

                var pager = $(xmldoc).find("Table");
                $(".Pager").ASPSnippets_Pager({
                    ActiveCssClass: "current",
                    PagerCssClass: "pager",
                    PageIndex: parseInt(pager.find("PageIndex").text()),
                    PageSize: parseInt(pager.find("PageSize").text()),
                    RecordCount: parseInt(pager.find("RecordCount").text())
                });
                if ($("#ddlPageSize").val() == 0) {
                    $(".Pager").hide();
                }
                else {
                    $(".Pager").show();
                }

            } else {
                location.href = "/";
            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}



function PrintOrder(ReportAutoId) {
    window.open("/Reports/PaymentLog_BankReportLog.html?dt=" + ReportAutoId, "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
}

function CreateTable(us) {
    debugger
    row1 = "";
    var image = $("#imgName").val();
    var data = {
        VendorName: $("#txtSVendorName").val(),
        PaymentType: $("#ddlPaymentType").val(),
      
        PageSize: $("#ddlPageSize").val(),
        pageIndex: 1,
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
    };
    $.ajax({
        type: "POST",
        url: "VendorPaymentLog.aspx/GetVendorPaymentLog",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',

                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var PrintDate = $(xmldoc).find("Table");
                    var Report = $(xmldoc).find("Table1");
                    var TotalAmount = 0;
                    $("#PrintTable tbody tr").remove();
                    var row = $("#PrintTable thead tr:last-child").clone(true);
                    if (Report.length > 0) {
                       
                        $.each(Report, function () {
                            $(".P_PayID", row).text($(this).find("PayId").text());
                          
                            $(".P_VendorName", row).text($(this).find("VendorName").text());
                            $(".P_Vendor", row).text($(this).find("VendorName").text());
                            $(".P_PaymentDate", row).text($(this).find("PaymentDate").text());
                            $(".P_PaymentAmount", row).text($(this).find("PaymentAmount").text());
                            $(".P_PaymentMode", row).text($(this).find("PaymentMode").text());
                            $(".P_ReferenceId", row).text($(this).find("NoOfBill").text());
                            $(".P_Status", row).text($(this).find("Status").text());
                            $(".P_Remark", row).text($(this).find("Remark").text());
                            if ($(this).find("Status").text() == '1') {
                                $(".P_Status", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("StatusType").text() + "</span>");
                            } else {
                                $(".P_Status", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("StatusType").text() + "</span>");
                            }
                            TotalAmount += parseFloat($(this).find("PaymentAmount").text());
                            $("#PrintTable tbody").append(row);
                            row = $("#PrintTable tbody tr:last").clone(true);

                        });
                       
                        $("#OTotalAmounts").html(parseFloat(TotalAmount).toFixed(2));
                    }
                    if (us == 1) {
                        var image = $("#imgName").val();
                        var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                        mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                        $("#PrintDate").text("Print Date : " + PrintDate.find('PrintDate').text());
                        $("#PrintLogo").attr("src", "/Img/logo/" + image);
                        if ($("#txtSFromDate").val() != "") {
                            $("#DateRange").html("Date Range : " + $("#txtSFromDate").val() + " To " + $("#txtSToDate").val());
                        } 
                        mywindow.document.write($(PrintTable1).clone().html());
                        mywindow.document.write('</body></html>');
                        setTimeout(function () {
                            mywindow.print();
                        }, 2000);
                    }
                    if (us == 2) {
                        $("#PrintTable").table2excel({
                            exclude: ".noExl",
                            name: "Excel Document Name",
                            filename: "VendorPaymentLog" + (new Date()).format("MM/dd/yyyy hh:mm tt"),
                            fileext: ".xls",
                            exclude_img: true,
                            exclude_links: true,
                            exclude_inputs: true
                        });
                    }
                }
            }
            else {
                location.href = "/";
            }
        }

    });
}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblPayDetails tbody tr').length > 0) {
        CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
function ExportReport() {
    if ($('#tblPayDetails tbody tr').length > 0) {
        CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
//function fnEdit(PaymentAutoId, VendorId) {
//    location.href = '/Vendor/ViewVendorPaymentLog.aspx?PaymentId=' + PaymentAutoId + '&VendorId=' + VendorId;
//}

function NewPayment() {
    location.href = "/Vendor/VendorPayMaster.aspx";
}
