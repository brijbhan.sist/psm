﻿var PayAutoId; PaymentAutoId = 0; var VendorId = 0; var checkId = 0;
$(document).ready(function () {
    debugger
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    }; 

    if (getQueryString('PaymentId') != null || getQueryString('PaymentId') == '') {
        PaymentAutoId = getQueryString('PaymentId');
        fnEdit(PaymentAutoId); 
    } else { 
        location.href = '/Vendor/VendorPaymentLog.aspx';
    }
})
 

function fnEdit(PaymentAutoid) { 
    PayAutoId = PaymentAutoid;
    $.ajax({
        type: "POST",
        url: "ViewVendorPaymentLog.aspx/ViewPayment",
        data: "{'PaymentAutoid':'" + PayAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var PayDetails = $(xmldoc).find("Table");
                var PaymentDetails = $(xmldoc).find('Table2'); 
                $('#txtPayIds').text($(PayDetails).find('PayId').text());
                $('#ddlVendors').text($(PayDetails).find('VendorName').text());
                $('#txtPaymentDates').text($(PayDetails).find('PaymentDate').text());
                $('#ddlPaymentModes').text($(PayDetails).find('PaymentMode').text()); 
                $('#txtPaymentAmounts').text($(PayDetails).find('PaymentAmount').text());
                $('#ReferenceIds').text($(PayDetails).find('ReferenceID').text());
                $('#txtRemarkss').text($(PayDetails).find('Remark').text()); 
                $('#txtPayId').val($(PayDetails).find('PayId').text()); 
                $('#txtPaymentDate').val($(PayDetails).find('PaymentDate').text());
                 

                if ($(PayDetails).find('PaymentMode').text() == '2') {
                    $("#hideCheck").show(); 
                }

                $('#txtPaymentAmount').val($(PayDetails).find('PaymentAmount').text());
                $('#ReferenceId').val($(PayDetails).find('ReferenceID').text());
                $('#txtRemarks').val($(PayDetails).find('Remark').text());

                if (PaymentDetails.length > 0) {
                    $("#tblPOSOrderList tbody tr").remove();
                    var row = $("#tblPOSOrderList thead tr").clone();
                    var billAmt = 0;
                    $.each(PaymentDetails, function (index) {
                        $(".OrderNo", row).html($(this).find("BillNo").text());
                        $(".OrderDate", row).text($(this).find("BillDate").text());
                        $(".OrderAmount", row).text($(this).find("OrderAmount").text());
                        billAmt = billAmt + parseFloat($(this).find("OrderAmount").text());
                        $("#tblPOSOrderList tbody").append(row);
                        row = $("#tblPOSOrderList tbody tr:last").clone(true);
                    });
                    $("#TotalOrderAmount").html(parseFloat(billAmt).toFixed(2));
                }

                var PaymentHistoryCurrencyList = $(xmldoc).find('Table1');
                var Total = 0.00;
                $("#tblCurrencyList tbody tr").remove();
                var row = $("#tblCurrencyList thead tr").clone(true);
                if (PaymentHistoryCurrencyList.length > 0) {
                    if ($(PayDetails).find('PaymentMode').text() == 'Cash') {
                        $("#divCurrencySection").show();
                        $.each(PaymentHistoryCurrencyList, function (index) {
                            $(".CurrencyName", row).html("<span CurrencyValue='" + $(this).find('CurrencyValue').text() + "'  CurrencyAutoid='" + $(this).find('AutoId').text() + "'></span>" + $(this).find("CurrencyName").text());
                            $(".NoofRupee", row).html("<input type='Text'  value='" + $(this).find("NoofValue").text() + "' class='form-control input-sm text-center'  onfocus='this.select()' readonly='readonly'/>");
                            $(".Total", row).html($(this).find("TotalAmount").text());
                            if (Number($(this).find("NoofValue").text()) > 0) {
                                $("#tblCurrencyList tbody").append(row);
                                row = $("#tblCurrencyList tbody tr:last").clone(true);
                            }
                            Total = parseFloat(Total) + parseFloat($(this).find("TotalAmount").text());
                        });
                    }
                }
                else {
                    $("#divCurrencySection").hide();
                }
                $("#TotalAmount").text(parseFloat(Total).toFixed(2));
                $("#txtTotalCurrencyAmount").val($("#TotalAmount").text());

            }
            else {
                location.href = '/';
            }

        },

        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function Back() {
    location.href = '/Vendor/VendorPaymentLog.aspx';
}
 