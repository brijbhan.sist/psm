﻿using DLLVendorPaymentLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_ViewVendorPaymentLog : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod(EnableSession = true)]
    public static string ViewPayment(string PaymentAutoid)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                PL_VendorPaymentLog pobj = new PL_VendorPaymentLog();
                pobj.PayAutoId = Convert.ToInt32(PaymentAutoid);
                BL_VendorPaymentLog.editPay(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            catch (Exception)
            {

                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

}