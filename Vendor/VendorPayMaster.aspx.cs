﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DLLVendorPayMaster;
public partial class Vendor_VendorPayMaster : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod(EnableSession = true)]
    public static string Binddropdown()
    {
        PL_VendorPayMaster pobj = new PL_VendorPayMaster();
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {

                BL_VendorPayMaster.binddropdown(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                return json;


            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}