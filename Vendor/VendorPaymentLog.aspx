﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="VendorPaymentLog.aspx.cs" Inherits="Reports_VendorPaymentLog" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <input type="hidden" id="hiddenPackerAutoId" runat="server" />
            <input type="hidden" id="hiddenEmpType" runat="server" />
            <h3 class="content-header-title">Vendor Payment Log</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Vendor</a></li>
                        <li class="breadcrumb-item">Vendor Payment Log</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="NewPayment()">New Payment</button>
                    <button type="button" class="dropdown-item" onclick="PrintElem()">Print</button>
                    <button type="button" class="dropdown-item" id="btnExport" onclick="ExportReport();">Export</button>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row ">
                                    <div class="col-md-3 ">
                                        <input type="text" class="form-control input-sm border-primary" placeholder=" Vendor Name" id="txtSVendorName"/>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <select id="ddlPaymentType" class="form-control input-sm border-primary">
                                            <option value="0">ALL Payment Mode</option>
                                        </select>
                                    </div>                                  
                                    <div class="col-md-3 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                 <span class="input-group-text" style="padding: 0rem 1rem;">From Payment Date<span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" onchange="setdatevalidation(1)" placeholder="Payment From Date" id="txtSFromDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                 <span class="input-group-text" style="padding: 0rem 1rem;">To Payment Date<span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" onchange="setdatevalidation(2)" placeholder="Payment To Date" id="txtSToDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                      <div class="col-md-12 form-group">
                                        <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnSearch" onclick=" GetVendorPaymentLog(1);">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">

                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="MyTableHeader" id="tblPayDetails">
                                        <thead class="bg-blue white">
                                            <tr>
                                                <td class="action text-center width4per">Action</td>
                                                <td class="PayID  text-center" style="width:79px">Payment ID</td>
                                                <td class="PaymentDate  text-center" style="width:89px">Payment Date</td>
                                                <td class="Vendor" style="text-align:left;width:189px">Vendor Name</td>
                                                 <td class="PaymentType  text-center" style="width:89px">Payment Type</td>
                                                <td class="PaymentMode  text-center"  style="width:95px">Payment Mode</td>                                               
                                                <td class="PaymentAmount price" style="text-align:right;width:99px">Settled Amount</td>
                                                <%--<td class="ReferenceId text-center">No. of Bill</td>
                                                <td class="Status  text-center">Status</td>--%>
                                                <td class="payby text-center" style="width:89px">Paid By</td>
                                                <td class="Remark">Remark</td> 
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                          <tfoot>
                                            <tr style="font-weight: bold;">
                                                <td colspan="6" class="text-right">Total</td>
                                                <td id="TotalAmount" class="text-right">0.00</td>
                                                <td colspan="6" class="text-center"></td>
                                            </tr>
                                            <tr style="font-weight: bold;">
                                                <td colspan="6" class="text-right">Overall Total</td>
                                                <td id="TotalAmounts" style="text-align: right;">0.00</td>
                                                 <td colspan="6" class="text-center"></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                </div>
                                <br />
                                <div class="row container-fluid">
                                    <div>
                                        <select id="ddlPageSize" class="form-control input-sm border-primary" onchange=" GetVendorPaymentLog(1);">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="table-responsive" style="display: none;" id="PrintTable1">
            <div class="row" style="margin-bottom: 5px;">
                <div class="col-md-4 col-lg-4 col-sm-4 text-left">
                    <img src="" id="PrintLogo" height="40" />
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-center" style="margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;">
                    Vendor Payment Log
                    <br />
                    <span class="text-center DateRangeCSS" style="font-size: 9px; color: black;" id="DateRange"></span>

                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-right" id="PrintDate" style="line-height: 3; font-size: 12px; color: black; font-weight: bold;">Print Date</div>
            </div>
            <table class="MyTableHeader PrintMyTableHeader" id="PrintTable">
                <thead class="bg-blue white">
                    <tr>
                        
                        <td class="P_Vendor" style="text-align: left">Vendor Name</td>
                        <td class="P_PayID  text-center">Payment ID</td>
                        <td class="P_PaymentDate  text-center">Payment Date</td>
                        <td class="P_PaymentMode  text-center">Payment Mode</td>
                        <td class="P_PaymentAmount price" style="text-align: right">Payment Amount</td>
                        <%--<td class="P_ReferenceId text-center">No. of Bill</td>
                        <td class="P_Status  text-center">Status</td>--%>
                        <td class="P_VendorName  text-center ">Paid By</td>
                        <td class="P_Remark">Remark</td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="4" class="text-center" style="font-weight: bold;">Overall Total</td>
                        <td id="OTotalAmounts" style="font-weight: bold;" class="text-right">0.00</td>
                        <td colspan="4" class="text-center"></td>
                    </tr>
                </tfoot>

            </table>
        </div>
    </div>


    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>

