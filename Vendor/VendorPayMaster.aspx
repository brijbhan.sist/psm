﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="VendorPayMaster.aspx.cs" Inherits="Vendor_VendorPayMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Vendor Payment</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Manage Vendor</a></li>
                        <li class="breadcrumb-item active">Vendor Payment
                        </li>
                    </ol>
                </div>
            </div>
        </div>
         <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-menu-right box-shadow-2 px-2" id="btnGroupDrop1" style="display:none" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onclick="Back()">
                 Back</button>
            </div>
        </div>
    </div>

    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Payment Details</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Pay ID</label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <input type="text" class="form-control border-primary input-sm" id="txtPayId" readonly="readonly" />
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Vendor Name<span class="required"> *</span> </label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12">                                              
                                                <select id="ddlVendor" class="form-control border-primary input-sm ddlreq" runat="server" onchange="BindPOSList()"> <%-- onchange="BindPOSList()"--%>
                                                    <option value="0">-Select Vendor-</option>
                                                </select>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Due Amount</label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <div class="input-group">
                                                    <input type="text" id="txtDueAmount" class="form-control border-primary input-sm" readonly="true" value="0.00" onkeypress="return isNumberDecimalKey(event,this)" runat="server" onfocus="this.select()" style="text-align:right"/>
                                                    <div class="input-group-prepend">                                                      
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Payment Date <span class="required">*</span></label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                                            <span class="la la-calendar-o"></span>
                                                        </span>
                                                    </div>
                                                    <input type="text" id="txtPaymentDate" class="form-control border-primary input-sm req" runat="server" onfocus="this.select()" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                     
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Payment Mode <span class="required">*</span></label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <select id="ddlPaymentMode" class="form-control input-sm border-primary ddlsreq" runat="server" onchange="PaymentMode()">
                                                    <option value="0">-Select-</option>                                                 
                                                </select>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-6" id="hidePayment" style="display:none">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Payment Source <span class="required">*</span></label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <select id="ddlPaymentType" class="form-control input-sm border-primary " runat="server">
                                                    <option value="0">-Select-</option>                                                 
                                                </select>
                                            </div>

                                        </div>
                                    </div>
                                     <div class="col-md-6" id="hideCheck" style="display:none">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Check No.<span class="required">*</span></label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <select id="ddlCheckNo" class="form-control input-sm border-primary ddlsreq" runat="server" onchange="Revert();">
                                                    <option value="0">-Select Check No.-</option>                                                 
                                                </select>
                                            </div>

                                        </div>
                                    </div>
                                   
                                </div>
                                <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">

                                                <label class="control-label">Payment Amount<span class="required"> *</span> </label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                                            <span>$</span>
                                                        </span>
                                                    </div>
                                                    <input type="text" id="txtPaymentAmount" class="form-control border-primary input-sm req" style="text-align: right" onkeypress="return isNumberDecimalKey(event,this)" runat="server" value="0.00" onfocus="this.select()" onkeyup="FillPayAmount()" onkeydown="Revert()"/>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Reference ID</label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <input type="text" id="ReferenceId" class="form-control border-primary input-sm" />
                                            </div>

                                        </div>
                                    </div>
                                   
                                </div>
                                <div class="row">
                                     <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Remark</label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <textarea id="txtRemarks" class="form-control border-primary input-sm"></textarea>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                              <div class="card-header">
                            <h4 class="card-title">Due Bills</h4>
                        </div>
                            <div class="card-body"  id="navbar">
                              <%--  <div class="row">
                                    <div class="col-md-12 col-sm12">
                                        <ul class="nav nav-tabs nav-topline">
                                            <li class="nav-item">
                                                <a class="nav-link active" id="base-DueBills" data-toggle="tab" aria-controls="tab21" href="#DueBills" aria-expanded="true">Due Bills</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="base-PaymentHistory" data-toggle="tab" aria-controls="tab22" href="#PaymentHistory" aria-expanded="false">Payment History</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>--%>
                               


               <div class="tab-content px-1 border-grey border-lighten-2 border-0-top">
                  <div role="tabpanel" class="row tab-pane active" id="DueBills" aria-expanded="true" aria-labelledby="base-DueBills">              
                                <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered" id="tblPOSOrderList">
                                            <thead class="bg-blue white">
                                                <tr>
                                                    <td class="OrderNo text-center">PO No.</td>
                                                    <td class="OrderDate text-center">Bill Date</td>
                                                    <td class="OrderAmount" style="text-align: right">Bill Amount</td>
                                                    <td class="PaidAmount" style="text-align: right">Paid Amount</td>
                                                    <td class="DueAmount" style="text-align: right">Due Amount</td>
                                                    <td class="CheckBoc paynow text-center">Select Check Box</td>
                                                    <td class="PayAmount paynow" style="text-align: right">Pay Amount</td>
                                                    <td class="payDueAmount paynow" style="text-align: right">Due Balance</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="2">Total</td>
                                                    <td id="TotalOrderAmount" style="text-align: right; font-weight: bold">0.00</td>
                                                    <td id="TotalPaidAmount" style="text-align: right; font-weight: bold">0.00</td>
                                                    <td id="DueAmount" style="text-align: right; font-weight: bold">0.00</td>
                                                    <td id="Td3" class="paynow" style="text-align: right"></td>
                                                    <td id="TotalPayAmount" class="paynow" style="text-align: right; font-weight: bold">0.00</td>
                                                    <td id="TotalpayDueAmount" class="paynow" style="text-align: right; font-weight: bold">0.00</td>

                                                </tr>
                                            </tfoot>
                                        </table>
                                        <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                    </div>
                                </div>
                                </div>
                                                    <div class="row DuehideCurrency" style="display: none">
                                            <div class="col-md-6">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered" id="tblCurrencyList">
                                                        <thead class="bg-blue white">
                                                            <tr>
                                                                <td class="CurrencyName text-center">Bill</td>
                                                                <td class="NoofRupee text-center" style="width: 150px;">Total Count</td>
                                                                <td class="Total" style="text-align: right; width: 150px">Total Amount</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <td colspan="2"style="text-align: right">Total</td>
                                                                <td id="TotalAmount" style="text-align: right;font-weight:bold">0.00</td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="row" style="display:none;">
                                                    <div class="col-md-3 col-sm-12">
                                                        <label class="control-label">
                                                            <span style="white-space: nowrap">Short Amount :</span>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-4 col-sm-12">
                                                        <div class="form-group">
                                                            <input type="text" id="txtSortAmount" readonly="true"  onkeypress="return isNumberDecimalKey(event,this)" class="form-control border-primary input-sm" value="0.00" style="text-align: right" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-2">
                                                        <label class="control-label">
                                                            <span style="white-space: nowrap">Total Amount :</span>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-4 col-sm-4">
                                                        <div class="form-group">
                                                            <input type="text" id="txtTotalCurrencyAmount" readonly="true" onkeypress="return isNumberDecimalKey(event,this)" 
                                                                class="form-control border-primary input-sm" value="0.00" style="text-align: right;font-weight:bold;font-size: 17px;;" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnSave" onclick="SavePayAmount()">Save</button>
                                    </div>
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" class="btn btn-secondary  buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnReset" onclick="resetData()">Reset</button>
                                    </div> 
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnUpdate" style="display: none" onclick="UpdatePayAmount()">Update</button>
                                    </div>
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" class="btn btn-warning buttonAnimation pull-right round box-shadow-1 btn-sm" data-animation="pulse" id="btnCancel" style="display: none" onclick="Newpayment()">Cancel</button>
                                    </div>
                                </div>
                            </div>
                      <%--  </div>--%>
                      </div>


                   <div class="row tab-pane" id="PaymentHistory" aria-labelledby="base-PaymentHistory">
                       <div class="col-md-12">
                           <div class="card">

                               <div class="card-content collapse show">
                                   <div class="card-body">
                                       <div class="row">

                                           <div class="col-md-4 form-group">
                                               <select id="ddlSVendor" class="form-control border-primary input-sm" runat="server">
                                                   <option value="0">All Vendor</option>
                                               </select>
                                           </div>
                                           <div class="col-md-4 form-group" style="display: none">
                                               <select class="form-control border-primary input-sm" id="ddlSStatus">
                                                   <option value="0">All</option>
                                               </select>
                                           </div>
                                           <div class="col-md-4 form-group">
                                               <input type="text" class="form-control border-primary input-sm" placeholder="Payment Id" id="txtSPayId" />
                                           </div>
                                       </div>
                                       <div class="row">

                                           <div class="col-md-4 form-group">
                                               <div class="input-group">
                                                   <div class="input-group-prepend">
                                                       <span class="input-group-text" style="padding: 0rem 1rem;">
                                                           <span class="la la-calendar-o"></span>
                                                       </span>
                                                   </div>
                                                   <input type="text" class="form-control border-primary input-sm" placeholder="Vendor Payment From Date" id="txtSFromDate" onfocus="this.select()" />
                                               </div>
                                           </div>
                                           <div class="col-md-4 form-group">
                                               <div class="input-group">
                                                   <div class="input-group-prepend">
                                                       <span class="input-group-text" style="padding: 0rem 1rem;">
                                                           <span class="la la-calendar-o"></span>
                                                       </span>
                                                   </div>
                                                   <input type="text" class="form-control border-primary input-sm" placeholder="Vendor Payment To Date" id="txtSToDate" onfocus="this.select()" />
                                               </div>
                                           </div>

                                           <div class="col-md-2 form-group">
                                               <button type="button" class="btn btn-info buttonAnimation pull-left round box-shadow-1  btn-sm" data-animation="pulse" id="btnSearch" onclick="getPayList(1)">Search</button>
                                           </div>
                                       </div>

                                       <%--<input type="hidden" id="PaymentAutoId" />--%>
                                       <div class="row">
                                           <div class="table-responsive">
                                               <table class="table table-striped table-bordered" id="tblPayDetails">
                                                   <thead class="bg-blue white">
                                                       <tr>
                                                           <td class="action text-center">Action</td>
                                                           <td class="PayID  text-center">Payment ID</td>
                                                           <td class="PaymentDate  text-center">Payment Date</td>
                                                           <td class="VendorName">Vendor  Name</td>
                                                           <td class="PaymentAmount price">Payment Amount</td>
                                                           <td class="PaymentMode  text-center">Payment Mode</td>
                                                           <td class="ReferenceId">Reference ID</td>
                                                           <td class="Remark">Remark</td>
                                                           <td class="Status  text-center">Status</td>

                                                       </tr>
                                                   </thead>
                                                   <tbody>
                                                   </tbody>
                                               </table>
                                               <h5 class="well text-center" id="EmptyTable1" style="display: none">No data available.</h5>
                                           </div>
                                       </div>

                                   </div>
                               </div>

                           </div>
                       </div>
                   </div>
                     </div>
                            </div>
                    </div>
                       
                    </div>
                </div>
            </div>

        </section>
    </div>
    <div id="CustomerDueAmount" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Customer Due Amount</h4>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered" id="tblduePayment">
                                            <thead>
                                                <tr>
                                                    <td class="SrNo">SN</td>
                                                    <td class="OrderNo">Order No</td>
                                                    <td class="OrderDate">Order Date</td>
                                                    <td class="AmtDue">Amt Due</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="btn-group mr-1 pull-right">
                                            <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1  btn-sm" data-dismiss="modal">Close</button>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="js/VendorPayMaster.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
</asp:Content>

