﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="ViewVendorPaymentLog.aspx.cs" Inherits="Admin_ViewVendorPaymentLog" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <h3 class="content-header-title">Vendor Payment Log Details</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Vendor</a></li>
                         <li class="breadcrumb-item"><a href="#">Vendor Payment Log</a></li>
                        <li class="breadcrumb-item">Vendor Payment Log Details</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-menu-right box-shadow-2 px-2" id="btnGroupDrop1" style="display: none" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onclick="Back()">
                    Back</button>
            </div>
        </div>
    </div>

    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Payment Details</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-2">
                                        <label class="control-label"><b>Pay ID :</b></label>
                                    </div>
                                    <div class="col-sm-12 col-md-2">
                                        <div class="form-group">
                                            <span id="txtPayIds"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-2">
                                        <label class="control-label"><b>Payment Date :</b></label>
                                    </div>
                                    <div class="col-sm-12 col-md-2">
                                        <div class="form-group">
                                            <span id="txtPaymentDates"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-2">
                                        <label class="control-label">
                                            <b>Vendor Name : </b>
                                        </label>
                                    </div>
                                    <div class="col-sm-12 col-md-2">
                                        <div class="form-group">
                                            <span id="ddlVendors"></span>
                                        </div>
                                    </div>
                                   
                                </div>
                                <div class="row form-group">
                                    
                                    <div class="col-sm-12 col-md-2">
                                        <label class="control-label">
                                            <b>Payment Mode : </b>
                                        </label>
                                    </div>
                                    <div class="col-sm-12 col-md-2">
                                        <div class="form-group">
                                            <span id="ddlPaymentModes"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-2">
                                        <label class="control-label"><b>Payment Amount :</b></label>
                                    </div>
                                    <div class="col-sm-12 col-md-2">
                                        <div class="form-group">
                                            <span id="txtPaymentAmounts"></span>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-12 col-md-2">
                                        <label class="control-label">
                                            <b>Reference ID : </b>
                                        </label>
                                    </div>
                                    <div class="col-sm-12 col-md-2">
                                        <div class="form-group">
                                            <span id="ReferenceIds"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group" id="hideCheck" style="display:none">

                                    <div class="col-md-2 col-sm-12">
                                        <label class="control-label">
                                            <b>Check No :  </b>
                                        </label>
                                    </div>
                                    <div class="col-md-2 col-sm-12">
                                        <div class="form-group">
                                            <span id="ddlCheckNos"></span>
                                        </div>
                                    </div>


                                </div>
                                <div class="row">
                                    <div class="col-md-2 col-sm-2">
                                        <label class="control-label">
                                            <b>Remark :  </b>
                                        </label>
                                    </div>
                                    <div class="col-md-10 col-sm-10">
                                        <div class="form-group">
                                            <span id="txtRemarkss"></span>
                                        </div>
                                    </div>
                                </div>

                                <input type="hidden" id="ddlPaymentMode" onchange="PaymentMode()" />
                                <input type="hidden" id="ddlCheckNo" onchange="Revert();" />

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-md-3">
                                        <h4 class="card-title">Settled Bills</h4>
                                    </div>
                                </div>

                            </div>
                            <div class="card-body" id="navbar">

                                <div class="tab-content px-1 pt-1 border-grey border-lighten-2 border-0-top">
                                    <div role="tabpanel" class="row tab-pane active" id="DueBills" aria-expanded="true" aria-labelledby="base-DueBills">
                                        <div class="row">
                                            <div class="col-md-12">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered" id="tblPOSOrderList">
                                                    <thead class="bg-blue white">
                                                        <tr>
                                                            <td class="OrderNo text-center">Bill No.</td>
                                                            <td class="OrderDate text-center">Bill Date</td>
                                                            <td class="OrderAmount" style="text-align: right">Bill Amount</td>
                                                         <%--   <td class="PaidAmount" style="text-align: right">Settled Amount</td>
                                                            <td class="DueAmount" style="text-align: right">Due Amount</td>--%>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td colspan="2" style="text-align: right;font-weight: bold">Total</td>
                                                            <td id="TotalOrderAmount" style="text-align: right; font-weight: bold">0.00</td>
                                                           <%-- <td id="TotalPaidAmount" style="text-align: right; font-weight: bold">0.00</td>
                                                            <td id="DueAmount" style="text-align: right; font-weight: bold">0.00</td>--%>


                                                        </tr>
                                                    </tfoot>
                                                </table>
                                                <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                            </div>
                                                </div>
                                        </div><br /><br />
                                        <div class="row DuehideCurrency"  id="divCurrencySection">
                                            <div class="col-md-6">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered" id="tblCurrencyList">
                                                        <thead class="bg-blue white">
                                                            <tr>
                                                                <td class="CurrencyName text-center">Currency</td>
                                                                <td class="NoofRupee text-center" style="width: 150px;">Total Count</td>
                                                                <td class="Total" style="text-align: right; width: 150px">Total Amount</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <td colspan="2" style="text-align: right;font-weight: bold">Total</td>
                                                                <td id="TotalAmount" style="text-align: right; font-weight: bold">0.00</td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="row" style="display: none;">
                                                    <div class="col-md-3 col-sm-12">
                                                        <label class="control-label">
                                                            <span style="white-space: nowrap">Short Amount :</span>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-4 col-sm-12">
                                                        <div class="form-group">
                                                            <input type="text" id="txtSortAmount" readonly="true" onkeypress="return isNumberDecimalKey(event,this)" class="form-control border-primary input-sm" value="0.00" style="text-align: right" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-2">
                                                        <label class="control-label">
                                                            <span style="white-space: nowrap">Total Amount :</span>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-4 col-sm-4">
                                                        <div class="form-group">
                                                            <input type="text" id="txtTotalCurrencyAmount" readonly="true" onkeypress="return isNumberDecimalKey(event,this)" class="form-control border-primary input-sm" value="0.00" style="text-align: right; font-weight: bold; font-size: 17px;" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>


                                    <div class="row tab-pane" id="PaymentHistory" aria-labelledby="base-PaymentHistory">
                                        <div class="col-md-12">
                                            <div class="card">

                                                <div class="card-content collapse show">
                                                    <div class="card-body">
                                                        <div class="row">

                                                            <div class="col-md-4 form-group">
                                                                <select id="ddlSVendor" class="form-control border-primary input-sm" runat="server">
                                                                    <option value="0">All Vendor</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-4 form-group" style="display: none">
                                                                <select class="form-control border-primary input-sm" id="ddlSStatus">
                                                                    <option value="0">All</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-4 form-group">
                                                                <input type="text" class="form-control border-primary input-sm" placeholder="Payment Id" id="txtSPayId" />
                                                            </div>
                                                        </div>
                                                        <div class="row">

                                                            <div class="col-md-4 form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                                                            <span class="la la-calendar-o"></span>
                                                                        </span>
                                                                    </div>
                                                                    <input type="text" class="form-control border-primary input-sm" placeholder="Vendor Payment From Date" id="txtSFromDate" onfocus="this.select()" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                                                            <span class="la la-calendar-o"></span>
                                                                        </span>
                                                                    </div>
                                                                    <input type="text" class="form-control border-primary input-sm" placeholder="Vendor Payment To Date" id="txtSToDate" onfocus="this.select()" />
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2 form-group">
                                                                <button type="button" class="btn btn-info buttonAnimation pull-left round box-shadow-1  btn-sm" data-animation="pulse" id="btnSearch" onclick="getPayList(1)">Search</button>
                                                            </div>
                                                        </div>

                                                        <%--<input type="hidden" id="PaymentAutoId" />--%>
                                                        <div class="row">
                                                            <div class="table-responsive">
                                                                <table class="table table-striped table-bordered" id="tblPayDetails">
                                                                    <thead class="bg-blue white">
                                                                        <tr>
                                                                            <td class="action text-center">Action</td>
                                                                            <td class="PayID  text-center">Payment ID</td>
                                                                            <td class="PaymentDate  text-center">Payment Date</td>
                                                                            <td class="VendorName">Vendor  Name</td>
                                                                            <td class="PaymentAmount price">Payment Amount</td>
                                                                            <td class="PaymentMode  text-center">Payment Mode</td>
                                                                            <td class="ReferenceId">Reference ID</td>
                                                                            <td class="Remark">Remark</td>
                                                                            <td class="Status  text-center">Status</td>

                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    </tbody>
                                                                </table>
                                                                <h5 class="well text-center" id="EmptyTable1" style="display: none">No data available.</h5>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>


    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="js/ViewVendorPaymentLog.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
</asp:Content>

