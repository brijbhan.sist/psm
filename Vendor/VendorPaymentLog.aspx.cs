﻿using DLLVendorPaymentLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_VendorPaymentLog : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Vendor/JS/VendorPaymentLog.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredField'>" + text + "</script>"
                ));
        }
    }
    [WebMethod(EnableSession = true)]
    public static string bindPaymentType()
    {
       
        PL_VendorPaymentLog pobj = new PL_VendorPaymentLog();

        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                BL_VendorPaymentLog.bindPaymentType(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }

    }
    [WebMethod(EnableSession = true)]
    public static string GetVendorPaymentLog(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_VendorPaymentLog pobj = new PL_VendorPaymentLog();

        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                pobj.PaymentType = Convert.ToInt32(jdv["PaymentType"]);
                pobj.VendorName = jdv["VendorName"];
                if (jdv["FromDate"] != "")
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                if (jdv["ToDate"] != "")
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                pobj.PageSize = Convert.ToInt16(jdv["PageSize"]);
                pobj.PageIndex = Convert.ToInt16(jdv["pageIndex"]);
                BL_VendorPaymentLog.GetVendorPaymentLog(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }

    }
    [WebMethod(EnableSession = true)]
    public static string Editpay(string PaymentAutoid)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                PL_VendorPaymentLog pobj = new PL_VendorPaymentLog();
                pobj.PayAutoId = Convert.ToInt32(PaymentAutoid);
                BL_VendorPaymentLog.editPay(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            catch (Exception)
            {

                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}