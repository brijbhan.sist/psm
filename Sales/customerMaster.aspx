﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/MasterPage.master" AutoEventWireup="true" CodeFile="customerMaster.aspx.cs" Inherits="Sales_customerMaster" ClientIDMode="Static" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8Y7FRVyPLlIEvF11qdFiD-ZWsf5OVIjs&libraries=places"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Manage Customer</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage Customer</a></li>
                        <li class="breadcrumb-item">New Customer</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a class="dropdown-item" href="/Sales/NewCustomerList.aspx" id="linkAddNewProduct" runat="server">Go to Customer List</a>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Customer Details</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3  col-sm-3">
                                        <label class="control-label">Customer ID</label>
                                        <div class="form-group">
                                            <input ktype="text" class="form-control border-primary input-sm" readonly="readonly" id="txtCustomerId" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-md-3  col-sm-3">
                                        <label class="control-label">Customer Name<span class="required">&nbsp;*</span></label>
                                        <div class="form-group">
                                            <input type="text" class="form-control border-primary input-sm req" id="txtCustomerName" runat="server" />

                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label class="control-label">Customer Type<span class="required">&nbsp;*</span></label>
                                        <div class="form-group">
                                            <select class="form-control border-primary input-sm ddlreq" id="ddlCustType" runat="server" style="width: 100%;" onchange="ShowlocationBind()">
                                                <option value="0">-Select-</option>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label class="control-label">Status</label>
                                        <div class="form-group">
                                            <select class="form-control border-primary input-sm" id="ddlStatus" runat="server" style="width: 100%;">
                                                <option value="0">-Select-</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="hiddenShipAddress" style="display: none;"></div>
                                <div class="hiddenBillAddress" style="display: none;"></div>
                                <div class="row">
                                    <div class="col-md-3 col-sm-3">
                                        <label class="control-label">Price Level<span class="required">&nbsp;*</span> </label>
                                        <div class="form-group">
                                            <select class="form-control border-primary input-sm ddlreq" id="ddlPriceLevel" runat="server" style="width: 100%;">
                                                <option value="0">-Select-</option>
                                                <option value="-1">Auto Generate Price Level</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3" id="rowSalesPerson" runat="server">
                                        <label class="control-label">Sales Person<span class="required">&nbsp;*</span> </label>
                                        <div class="form-group">
                                            <select class="form-control border-primary input-sm ddlreq" id="ddlSalesPerson" runat="server">
                                                <option value="0">-Select-</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label class="control-label">Tax ID</label>
                                        <div class="form-group">
                                            <input type="text" class="form-control border-primary input-sm" id="txtTaxId" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label class="control-label">Terms<span class="required">&nbsp;*</span>   </label>

                                        <div class="form-group">
                                            <select class="form-control border-primary input-sm ddlreq" id="ddlTerms" runat="server" style="width: 100%;">
                                                <option value="0">-Select-</option>
                                            </select>

                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3 col-sm-3">
                                        <label class="control-label">Business Name</label>
                                        <div class="form-group">
                                            <input type="text" id="txtBusinessName" class="form-control border-primary input-sm" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3">

                                        <label class="control-label">OPT Licence</label>

                                        <div class="form-group">
                                            <input type="text" id="txtOPTLicence" class="form-control border-primary input-sm" onkeypress="return isNumberKey(event)" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label class="control-label">Store Open Time</label>

                                        <div class="form-group">
                                            <select class="form-control border-primary input-sm ddlreq" id="StoreOpenTime" runat="server" style="width: 100%;">
                                            </select>

                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label class="control-label">Store Close Time</label>

                                        <div class="form-group">
                                            <select class="form-control border-primary input-sm ddlreq" id="StoreCloseTime" runat="server" style="width: 100%;">
                                            </select>

                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3" style="display: none">
                                        <label class="control-label">Zone</label>
                                        <div class="form-group">
                                            <input type="text" class="form-control border-primary input-sm" id="txtZone1" readonly />
                                        </div>
                                    </div>

                                </div>
                                <div class="row">

                                    <div class="col-md-6 col-sm-6">
                                        <label class="control-label">Remark</label>

                                        <div class="form-group">
                                            <textarea maxlength="1000" id="txtCustomerRemark" class="form-control border-primary input-sm"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3" id="RowLocation" style="display: none">
                                        <label class="control-label">Other Location <span class="required">&nbsp;*</span> </label>
                                        <div class="form-group">
                                            <select class="form-control border-primary input-sm" id="ddlLocation" runat="server" style="width: 100%;">
                                                <option value="0">-Select-</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div id="DivBillingAddresssection" style="display: none">
                                    <br />
                                    <br />
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h4 class="card-title"><span class="la la-eye"></span>&nbsp;&nbsp;Billing Details</h4>

                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-md-3 col-sm-3">
                                            <label class="control-label">Address 1<span class="required">&nbsp;*</span></label>
                                            <input type="hidden" id="hiddenBillAutoId" />
                                            <div class="form-group">
                                                <input type="text" class="form-control border-primary input-sm req" id="txtBillAdd" runat="server" onchange="validateAddress(1);" />

                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                            <label class="control-label">Address 2</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control border-primary input-sm" placeholder="Suite/Apartment" id="txtBillAdd2" runat="server" />
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                            <label class="control-label">Zip Code </label>
                                            <div class="form-group">
                                               <%-- <select id="ddlZipCode1" runat="server" class="form-control border-primary input-sm ddlreq" disabled="true" style="width: 100%;">
                                                    <option value="0">-Select-</option>
                                                </select>--%>
                                                 <input type="text" class="form-control border-primary input-sm " id="txtZipcode1" runat="server" readonly />
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-3">


                                            <label class="control-label">State</label>

                                            <div class="form-group">
                                                <input type="text" class="form-control border-primary input-sm " id="txtState1" runat="server" readonly />
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                            <label class="control-label">City</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control border-primary input-sm " id="txtCity1" readonly />
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                            <label class="control-label">Latitude</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control border-primary input-sm " id="txtLat1" runat="server" readonly />
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                            <label class="control-label">Longitude</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control border-primary input-sm " id="txtLong1" runat="server" readonly />

                                            </div>
                                        </div>
                                    </div>


                                    <div class="clearfix"></div>
                                    <hr />
                                </div>
                                <div id="DivShippingAddresssection" style="display: none">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input type="checkbox" id="chkSame" onchange="fnChkSame()" />
                                            Shipping Address Same as Billing Address
                                        <input type="hidden" id="hiddenShipAutoId" />
                                        </div>
                                    </div>
                                    <br />
                                    <br />

                                    <div class="row" id="divShippingAddr">
                                        <div class="col-md-3 col-sm-3">
                                            <label class="control-label">Address 1<span class="required">&nbsp;*</span> </label>
                                            <div class="form-group">
                                                <input type="text" class="form-control border-primary input-sm req" id="txtShipAdd" runat="server" onblur="resetField(this.value)" onchange="validateAddress(2);" />
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                            <label class="control-label">Address 2</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control border-primary input-sm" id="txtShipAdd2" placeholder="Suite/Apartment" runat="server" onblur="resetField(this.value)" />
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                            <label class="control-label">Zip Code</label>
                                            <div class="form-group">
                                               <%-- <select id="ddlZipCode2" runat="server" class="form-control border-primary input-sm ddlreq" disabled="true" style="width: 100%;">
                                                    <option value="0">-Select-</option>
                                                </select>--%>
                                                <input type="text" class="form-control border-primary input-sm " id="txtZipcode2" runat="server" readonly />
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                            <label class="control-label">State</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control border-primary input-sm " id="txtState2" runat="server" readonly />


                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                            <label class="control-label">City</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control border-primary input-sm " id="txtCity2" runat="server" readonly />

                                            </div>
                                        </div>


                                        <div class="col-md-3 col-sm-3">
                                            <label class="control-label">Latitude</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control border-primary input-sm " id="txtLat" runat="server" readonly />

                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                            <label class="control-label">Longitude</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control border-primary input-sm " id="txtLong" runat="server" readonly />

                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="contactPart" style="display: none">
                                    <br />
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h4 class="card-title"><span class="la la-eye"></span>&nbsp;&nbsp;Contact Details</h4>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <hr />
                                    <div class="row">

                                        <div class="col-md-3 col-sm-3">
                                            <label class="control-label">Type</label>
                                            <span class="required">*</span>
                                            <div class="form-group">
                                                <select id="ddlType" class="form-control border-primary input-sm ddlcreq" runat="server">
                                                    <option value="0">-Select Type-</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                            <label class="control-label">Contact Person</label>
                                            <span class="required">*</span>
                                            <div class="form-group">
                                                <input type="text" class="form-control border-primary input-sm creq" maxlength="50"  id="txtContactPersonName" runat="server" />
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-3">
                                            <label class="control-label">Email </label>
                                            <span class="required">*</span>
                                            <div class="form-group">
                                                <input type="text" class="form-control border-primary input-sm creq" id="txtEmail" runat="server" onfocus="this.select()" onchange="validateEmail(this);" />
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                            <label class="control-label">Alternate Email  </label>
                                            <div class="form-group">
                                                <input type="text" class="form-control border-primary input-sm " id="txtAltEmail" runat="server" onfocus="this.select()" onchange="validateEmail1(this);" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-3">
                                            <label class="control-label"><span class="la la-phone"></span>&nbsp;&nbsp; Landline 1</label>
                                            <span class="required">*</span>
                                            <div class="form-group">
                                                <input type="text" class="form-control border-primary input-sm creq" onchange="checkLength(1)" minlength="10" maxlength="10" id="txtLandline" runat="server" onkeypress='return isNumberKey(event)' onfocus="this.select()" />
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                            <label class="control-label"><span class="la la-phone"></span>&nbsp;&nbsp; Landline 2</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control border-primary input-sm" id="txtLandline2" onchange="checkLength(2)" minlength="10" maxlength="10" runat="server" onkeypress='return isNumberKey(event)' onfocus="this.select()" />

                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                            <label class="control-label"><span class="la la-mobile"></span>&nbsp;&nbsp; Mobile No</label>
                                            <span class="required">*</span>
                                            <div class="form-group">
                                                <input type="text" class="form-control border-primary input-sm creq" minlength="10" onchange="checkLength(3)" maxlength="10" onkeypress='return isNumberKey(event)' id="txtMobileNo" runat="server" />

                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                            <label class="control-label">Fax No</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control border-primary input-sm" id="txtFaxNo" runat="server" onkeypress='return isNumberKey(event)' maxlength="20" />
                                            </div>

                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                            <label class="control-label">Is Default</label>
                                            <div class="form-group">
                                                <input type="checkbox" class="checkbox border-primary" runat="server" id="chkIsDefault" />
                                            </div>
                                        </div>
                                        <div class="col-md-9 pull-right">
                                            <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnAdd" onclick="addItemToList();">Add</button>
                                        </div>
                                    </div>

                                </div>
                                <div class="row" style="display: none;" id="tblContact">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblContactDetails">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="Action text-center">Action</td>
                                                        <td class="TypeAutoId text-center" style="display: none;">TypeAutoId</td>
                                                        <td class="Type text-center">Type</td>
                                                        <td class="ContactPerson text-center">Contact Person</td>
                                                        <td class="Mobile text-center">Mobile</td>
                                                        <td class="Landline">Landline</td>
                                                        <td class="Landline2">Landline2</td>
                                                        <td class="Fax">Fax</td>
                                                        <td class="Email">Email</td>
                                                        <td class="AltEmail">Alternate Email</td>
                                                        <td class="IsDefault text-center">Is Default</td>
                                                    </tr>
                                                </thead>
                                                <tbody id="tbodyContactPerson">
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnSave">Save</button>
                                    </div>
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" class="btn btn-secondary  buttonAnimation pull-right round box-shadow-1  btn-sm" onclick="resetCustomer()" data-animation="pulse" id="btnReset">Reset</button>
                                    </div>
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnUpdate" style="display: none" runat="server">Update</button>
                                    </div>
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" class="btn btn-warning buttonAnimation pull-right round box-shadow-1 btn-sm" id="btnCancel" data-animation="pulse" style="display: none">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="modal fade" id="zipcodeselect" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-md">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Zipcode</h4>
                </div>
                <div class="modal-body" style="padding: 3rem!important;">
                    <div class="row" id="zipcodelist">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success btn-sm" onclick="getSelectedZip()">OK</button>
                    <button type="button" class="btn btn-danger btn-sm" onclick="hideclosemodal()">Close</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="zipcodeselect1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-md">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Zipcode</h4>
                </div>
                <div class="modal-body" style="padding: 3rem!important;">
                    <div class="row" id="zipcodelist1">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success btn-sm" onclick="getSelectedBillingZip()">OK</button>
                    <button type="button" class="btn btn-danger btn-sm" onclick="hideclosemodal1()">Close</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="/Sales/JS/CustomerMaster.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
    <script>
        google.maps.event.addDomListener(window, 'load', function () {

            var places = new google.maps.places.Autocomplete(document.getElementById('txtShipAdd'));
            google.maps.event.addListener(places, 'place_changed', function () {

                var place = places.getPlace();

                var address = place.formatted_address;
                var latitude = place.geometry.location.lat;
                var longitude = place.geometry.location.lng;
                var mesg = "Address: " + address;
                mesg += "\nLatitude: " + latitude;
                mesg += "\nLongitude: " + longitude;
                //$("#txtShipAdd").val(address);
                setData(place);

            });
        });

        google.maps.event.addDomListener(window, 'load', function () {
            var places = new google.maps.places.Autocomplete(document.getElementById('txtBillAdd'));
            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                console.log(place);

                var address = place.formatted_address;
                var latitude = place.geometry.location.A;
                var longitude = place.geometry.location.F;
                var mesg = "Address: " + address;
                mesg += "\nLatitude: " + latitude;
                mesg += "\nLongitude: " + longitude;
                //$("#txtBillAdd").val(address);
                setBillingData(place);

            });
        });

    </script>
</asp:Content>

