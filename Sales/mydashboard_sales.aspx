﻿<%@ Page Title="Dashboard" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" ClientIDMode="Static" CodeFile="mydashboard_sales.aspx.cs" Inherits="Sales_mydashboard_sales" %>

<script runat="server">

</script>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .table th, .table td {
            padding: 0.75rem !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <div class="alert alert-warning alert-dismissable fade in" id="alertMessage" style="font-size: 14px; display: none;">
                <a href="javascript:;" class="close" aria-label="close" onclick="$('#alertMessage').hide();">&times;</a>
                <center>
                    <strong style="text-decoration:underline;">Message</strong><br /><span id="message"></span>
                </center>
            </div>
        </div>
        <div class="col-md-1"></div>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-10">
                                        <h4 class="card-title">Welcome to Dashboard</h4>
                                    </div>
                                    <div class="col-md-2">
                                        <button type="button" onclick="LoadDashboard()" class="pull-right btn btn-sm btn-success" id="linktoOrderList"><b>Load Dashboard</b></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" id="top" style="display: none">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-11">
                                    <h4 class="card-title">Top 25 Selling Product (Overall)</h4>
                                </div>
                                <div class="col-md-1">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card-content collapse">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered" id="topProduct">
                                        <thead class="bg-blue white">
                                            <tr>
                                                <td class="SN text-center">S.N.</td>
                                                <td class="ID  text-center">Product ID</td>
                                                <td class="Name">Product Name</td>
                                                <td class="Qty  text-center">Qty in Stock</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                    <h5 class="well text-center" id="EmptyTable" style="display: none">No Dues.</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" id="due" style="display: none">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-header">
                                <h4 class="card-title">Due Payment Orders List</h4>
                            </div>
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="duePaymentOrdersList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="action text-center">Action</td>
                                                        <td class="custId  text-center">Customer ID</td>
                                                        <td class="custName">Customer Name</td>
                                                        <td class="orderNo  text-center">Order No</td>
                                                        <td class="orderDate  text-center">Order Date</td>
                                                        <td class="value price">Payable Amount</td>
                                                        <td class="amtPaid price">Paid Amount</td>
                                                        <td class="amtDue price">Due Amount</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot style="font-weight: 700; background: oldlace;">
                                                    <tr style="text-align: right;">
                                                        <td colspan="5"><b>Total</b></td>
                                                        <td id="sumOrderValue">0.00</td>
                                                        <td id="sumPaid">0.00</td>
                                                        <td id="sumDue">0.00</td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            <h5 class="well text-center" id="noDues" style="display: none">No Dues.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-1">
                                        <select class="form-control border-primary input-sm" id="ddlPageSize" onchange="bindDueList(1)">
                                            <option value="10">10 </option>
                                            <option value="50">50</option>
                                            <option value="100">100 </option>
                                            <option value="500">500</option>
                                            <option value="1000">1000 </option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager col-md-8 form-group" id="Pager"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>    
    <script type="text/javascript" src="/js/ShowMessage.js"></script>
    <script type="text/javascript">

        function LoadDashboard() {
            bindDueList(1);
            bindSalesDashboard();
          
            $("#top").show();
            $("#due").show();
        }
        $(document).ready(function () {
            //bindCustomer();
        })
        function Pagevalue(e) {
            bindDueList(parseInt($(e).attr("page")));
        }
        function bindDueList(PageIndex) {
            var data = {
                PageSize: $("#ddlPageSize").val(),
                pageIndex: PageIndex
            };
            $.ajax({
                type: "POST",
                url: "/Sales/mydashboard_sales.aspx/bindDueList",
                data: JSON.stringify({ 'dataValue': JSON.stringify(data) }),
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                asynch:true,
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.d != "Session Expired") {
                        var xmldoc = $.parseXML(response.d);
                        var duepaging = $(xmldoc).find("Table");
                        var duePaymentOrderList = $(xmldoc).find("Table1");
                        $("#duePaymentOrdersList tbody tr").remove();
                        var row = $("#duePaymentOrdersList thead tr").clone(true);

                        var sumOrderValue = 0, sumPaid = 0, sumDue = 0;
                        if (duePaymentOrderList.length > 0) {
                            $.each(duePaymentOrderList, function () {
                                $("#noDues").hide();
                                $(".custId", row).text($(this).find("CustomerId").text());
                                $(".custName", row).text($(this).find("CustomerName").text());
                                $(".orderNo", row).html($(this).find("OrderNo").text());
                                $(".orderDate", row).text($(this).find("OrderDate").text());
                                $(".value", row).text($(this).find("GrandTotal").text());
                                $(".amtPaid", row).text($(this).find("AmtPaid").text()).css("color", "#8bc548");
                                $(".amtDue", row).text($(this).find("AmtDue").text()).css("color", "red");
                                $(".action", row).html("<a title='View' href='/Sales/orderMaster.aspx?OrderNo=" + $(this).find("OrderNo").text() + "'><span class='la la-eye'></span></a>");

                                sumOrderValue += Number($(this).find("GrandTotal").text());
                                sumPaid += Number($(this).find("AmtPaid").text());
                                sumDue += Number($(this).find("AmtDue").text());

                                $('#duePaymentOrdersList').find("tbody").append(row).end()
                                    .find("tfoot").show().find("#sumOrderValue").text(sumOrderValue.toFixed(2)).end()
                                    .find("#sumPaid").text(sumPaid.toFixed(2)).css("color", "#8bc548").end()
                                    .find("#sumDue").text(sumDue.toFixed(2)).css("color", "red").end();

                                row = $("#duePaymentOrdersList tbody tr:last").clone(true);
                            });
                        } else {
                            $("#duePaymentOrdersList").find("tbody tr").remove().end().find("tfoot").hide();
                            $("#noDues").show();
                        }
                        var pager = duepaging;
                        $("#Pager").ASPSnippets_Pager({
                            ActiveCssClass: "current",
                            PagerCssClass: "pager",
                            PageIndex: parseInt(pager.find("PageIndex").text()),
                            PageSize: parseInt(pager.find("PageSize").text()),
                            RecordCount: parseInt(pager.find("RecordCount").text())
                        });
                    } else {
                        location.href = '/';
                    }
                },
                error: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                },
                failure: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                }
            });
        }
        function bindSalesDashboard() {

            $.ajax({
                type: "POST",
                url: "/Sales/mydashboard_sales.aspx/bindSalesDashboard",
                data: {},
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                asynch: true,
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                   
                },
                success: function (response) {
                    if (response.d != "Session Expired") {
                        var xmldoc = $.parseXML(response.d);
                        var topProduct = $(xmldoc).find("Table");
                        var companyLogo = $(xmldoc).find("Table1");
                        $("#imgPreview").attr("src", "../Img/logo/" + $(companyLogo).find("Logo").text())
                        BindProduct(topProduct);
                    } else {
                        location.href = '/';
                    }
                },
                error: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                },
                failure: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                }
            });
        }
        function BindProduct(topProduct) {
            $("#EmptyTable").hide();
            $("#topProduct tbody tr").remove();
            var row = $("#topProduct thead tr").clone(true);

            if (topProduct.length > 0) {
                var i = 1;
                $.each(topProduct, function () {
                    $(".SN", row).html(i);
                    $(".ID", row).html($(this).find("ProductId").text());
                    $(".Name", row).html($(this).find("ProductName").text());
                    $(".Qty", row).html($(this).find("Stock").text());

                    $("#topProduct tbody").append(row);
                    row = $("#topProduct tbody tr:last-child").clone(true);
                    i++;
                });
            } else {
                $("#topProduct tbody tr").remove();
                $("#EmptyTable").show();
            }
            $.unblockUI();
        }
       
    </script>
</asp:Content>

