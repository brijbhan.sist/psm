﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DLLDraftCustomerList;
public partial class Sales_DraftCustomerList : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Sales/JS/DraftCustomerList.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredFields'>" + text + "</script>"
                ));
        }
    }
    [WebMethod(EnableSession = true)]
    public static string bindDropDowns()
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_DraftCustomerList pobj = new PL_DraftCustomerList();

            BL_DraftCustomerList.bindDropDowns(pobj);
            if (!pobj.isException)
            {
                return pobj.Ds.GetXml();
            }
            else
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string getCustomerList(string dataValue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_DraftCustomerList pobj = new PL_DraftCustomerList();
            try
            {
                pobj.CustomerName = jdv["CustomerName"];
                pobj.CustomerType = Convert.ToInt32(jdv["CustomerType"]);
                pobj.State = jdv["StateName"];
                pobj.City =jdv["CityName"];
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.PageIndex = jdv["pageIndex"];
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);

                if (HttpContext.Current.Session["EmpTypeNo"].ToString() == "2")
                {
                    if (Convert.ToInt32(jdv["SalesPersonAutoId"]) == 0)
                    {
                        pobj.SalesPersonAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                    }
                    else
                    {
                        pobj.SalesPersonAutoId = Convert.ToInt32(jdv["SalesPersonAutoId"]);
                    }
                }
                else
                {
                    pobj.SalesPersonAutoId = Convert.ToInt32(jdv["SalesPersonAutoId"]);
                }

                BL_DraftCustomerList.select(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string deleteCustomer(int CustomerId)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_DraftCustomerList pobj = new PL_DraftCustomerList();
            try
            {
                pobj.CustomerAutoId = CustomerId;
                BL_DraftCustomerList.deleteCustomer(pobj);
                if (!pobj.isException)
                {                    
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}