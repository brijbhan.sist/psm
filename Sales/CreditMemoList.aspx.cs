﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Sales_CreditMemoList : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["EmpTypeNo"] != null)
        {
            if (Session["EmpTypeNo"].ToString() == "2" || Session["EmpTypeNo"].ToString() == "10")
            {
                btnAddNewCredit.Visible = true;
            }
            else
            {
                btnAddNewCredit.Visible = false;
            }
        }
    }
}