﻿using DLLDashboard;
using System;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

public partial class Sales_mydashboard_sales : System.Web.UI.Page
{

   
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
    [WebMethod(EnableSession = true)]
    public static string bindDueList(string dataValue)
    {
        try
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_Dashboard pobj = new PL_Dashboard();
            pobj.PageIndex = Convert.ToInt32(jdv["pageIndex"]);
            pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
            pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"].ToString());
            BL_Dashboard.bindDueList(pobj);
            if (!pobj.isException)
            {
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public static string bindSalesDashboard()
    {
        try
        {
          
            PL_Dashboard pobj = new PL_Dashboard();
            pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"].ToString());
            BL_Dashboard.bindSalesDashboard(pobj);
            if (!pobj.isException)
            {
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
  
}