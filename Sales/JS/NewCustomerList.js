﻿$(document).ready(function () {
    bindDropDowns();
    if ($("#hEmpTypeNo").val() != '2') {
        $("#divAction").show();
    }
});

function Pagevalue(e) {
    getCustomerList(parseInt($(e).attr("page")));
};
/*------------------------------------------------------Bind SALES PERSON------------------------------------------------------------*/
function bindDropDowns() {
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/WCustomerList.asmx/bindDropDowns",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },

        success: function (response) {
            if (response.d == 'Session Expired') {
                location.href = '/';
            } else if (response.d != 'false') {
                var xmldoc = $.parseXML(response.d);
                var salesPerson = $(xmldoc).find('Table');
                var CustoemerType = $(xmldoc).find('Table1');
                var State = $(xmldoc).find('Table2');
                var City = $(xmldoc).find('Table3');

                $("#ddlSSalesPerson option:not(:first)").remove();  //-------------------------------For Search Field
                $.each(salesPerson, function () {
                    $("#ddlSSalesPerson").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("Name").text() + "</option>");
                });
                $("#ddlSSalesPerson").select2();

                $("#ddlCustomerType option:not(:first)").remove();  //-------------------------------For Search Field
                $.each(CustoemerType, function () {
                    $("#ddlCustomerType").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("CustomerType").text() + "</option>");
                });
                $("#ddlCustomerType").select2();

                $("#ddlState option:not(:first)").remove();  //-------------------------------For Search Field
                $.each(State, function () {
                    $("#ddlState").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("StateName").text() + "</option>");
                });
                $("#ddlState").select2();

                $("#ddlCity option:not(:first)").remove();  //-------------------------------For Search Field
                $.each(City, function () {
                    $("#ddlCity").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("CityName").text() + "</option>");
                });
                $("#ddlCity").select2();
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
$(document).ready(function () {
    $("#ddlState").change(function () {
        var data = {
            StateName: $("#ddlState").val(),
        };
        console.log(data);

        $.ajax({
            type: "POST",
            url: "/Sales/WebAPI/WCustomerList.asmx/StateChagedBindCity",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },

            success: function (response) {
                if (response.d == 'Session Expired') {
                    location.href = '/';
                } else if (response.d != 'false') {
                    var xmldoc = $.parseXML(response.d);
                    var City = $(xmldoc).find('Table');

                    $("#ddlCity option:not(:first)").remove();  //-------------------------------For Search Field
                    $.each(City, function () {
                        $("#ddlCity").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("CityName").text() + "</option>");
                    });
                    $("#ddlCity").select2();
                }
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    });
});



$("#ddlPageSize").change(function () {
    getCustomerList(1);
})
/*------------------------------------------------------Get Customer List (bind Table)--------------------------------------------------*/
function getCustomerList(PageIndex) {
    var data = {
        CustomerId: $("#txtSCustomerId").val().trim(),
        CustomerName: $("#txtSCustomerName").val().trim(),
        SalesPersonAutoId: $('#ddlSSalesPerson').val(),
        CustomerType: $("#ddlCustomerType").val(),
        StateName: $("#ddlState").val(),
        CityName: $("#ddlCity").val(),
        Status: $("#ddlStatus").val(),
        OrderNo: $("#txtOrderNo").val().trim(),
        pageIndex: PageIndex,
        PageSize: $("#ddlPageSize").val() || 10
    };
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/WCustomerList.asmx/getNewCustomerList",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == 'Session Expired') {
                location.href = '/';
            } else if (response.d != 'false') {
                var xmldoc = $.parseXML(response.d);
                var customer = $(xmldoc).find('Table1');
                $('#tblCustomerList tbody tr').remove();
                if (customer.length > 0) {
                    $('#EmptyTable').hide();
                    var row = $('#tblCustomerList thead tr').clone(true);
                    $.each(customer, function () {
                        $(".CustId", row).text($(this).find("CustomerId").text());
                        $(".CustName", row).text($(this).find("CustomerName").text());
                        $(".SalesPerson", row).text($(this).find("SalesPerson").text());
                        $(".OrderAmount", row).text($(this).find("OrderAmount").text());
                        $(".OrderAmount", row).css('text-align', 'right');
                        $(".PaidAmount", row).text($(this).find("PaidAmount").text());
                        $(".PaidAmount", row).css('text-align', 'right');
                        if ($(this).find("DueAmount").text() <= 0) {
                            $(".DueAmount", row).html($(this).find("DueAmount").text());
                        } else {
                            $(".DueAmount", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("DueAmount").text() + "</span>");
                        }
                        $(".DueAmount", row).css('text-align', 'right');
                        $(".TotalOrder", row).text($(this).find("TotalOrder").text());
                        $(".LastOrderDate", row).text($(this).find("LastOrderDate").text());
                        $(".ContactPersonName", row).text($(this).find("ContactPersonName").text());
                        if ($(this).find("StoreCreditAmount").text() > 0) {
                            $(".StoreCreditAmount", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("StoreCreditAmount").text() + "</span>");
                        } else {
                            $(".StoreCreditAmount", row).html($(this).find("StoreCreditAmount").text());
                        }
                        $(".StoreCreditAmount", row).css('text-align', 'right');
                        $(".Email", row).html($(this).find("Email").text());
                        $(".Contact", row).html($(this).find("Contact1").text());
                        $(".PriceLevel", row).html($(this).find("PriceLevelName").text());
                        $(".SalesPerson", row).text($(this).find("SalesPerson").text());
                        if ($(this).find("Status").text() == '1') {
                            $(".Status", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("StatusType").text() + "</span>");
                        } else {
                            $(".Status", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("StatusType").text() + "</span>");
                        }
                        if ($("#hEmpTypeNo").val() == "1") {
                            $(".Action", row).html("<a title='Edit' href='/Sales/ViewCustomerDetails.aspx?PageId=" + $(this).find("custAutoId").text() +"'><span class='ft-edit'></span></a>&nbsp;&nbsp;<a title='Delete' href='#'><span class='ft-x' onclick= 'deleterecord(\"" + $(this).find("custAutoId").text() + "\")' /></a>");
                        }
                        else {
                            $(".Action", row).html("<a title='Edit' href='/Sales/ViewCustomerDetails.aspx?PageId=" + $(this).find('custAutoId').text() +"'><span class='ft-edit'></span></a>");
                        }

                        $("#tblCustomerList tbody").append(row);
                        row = $("#tblCustomerList tbody tr:last-child").clone(true);
                    });

                    if ($("#hidnEmpType").val() != "Sales Person") {
                        $("#tblCustomerList").find(".SalesPerson").show();
                    } else {
                        $("#tblCustomerList").find(".SalesPerson").hide();
                    }
                }
                else {
                    $('#EmptyTable').show();
                }

                var pager = $(xmldoc).find("Table");
                $(".Pager").ASPSnippets_Pager({
                    ActiveCssClass: "current",
                    PagerCssClass: "pager",
                    PageIndex: parseInt(pager.find("PageIndex").text()),
                    PageSize: parseInt(pager.find("PageSize").text()),
                    RecordCount: parseInt(pager.find("RecordCount").text())
                });
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
    });
}

function editCustomer(AutoId) {
    window.location.href = "/Sales/ViewCustomerDetails.aspx?PageId=" + AutoId;
}
function reset() {
    $("#txtSCustomerId").val('');
    $("#txtSCustomerName").val('');
    $("#ddlSSalesPerson").val(0).change();
    $("#ddlCustomerType").val(0).change();
    $("#ddlState").val(0).change();
    $("#ddlCity").val(0).change();
    $("#ddlStatus").val(2).change();
}

$("#btnSearch").click(function () {

    getCustomerList(1);
});
$("#btnCancel").click(function () {

    $("#txtSCustomerName").val('');
    $("#txtSCustomerId").val('');
    $("#ddlSSalesPerson").val('0').change();
    $("#ddlCustomerType").val('0').change();
    $("#ddlState").val('0').change();
    $("#ddlCity").val('0').change();
    $("#ddlStatus").val('0');
});
function deleteCustomer(AutoId) {

    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/WCustomerList.asmx/deleteCustomer",
        data: "{'AutoId':'" + AutoId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (result) {
            if (result.d == "Session Expired") {
                window.location = "/";
            }
            else if (result.d == 'Success') {
                swal("", "Customer details deleted successfully.", "success");
                getCustomerList(1)
            } else {
                swal("Error!", result.d, "error");
            }
        },
        error: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later", "error");

        },
        failure: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later", "error");
        }
    })
}

function deleterecord(AutoId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this customer.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, cancel.",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, delete it.",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            deleteCustomer(AutoId);
        }
    })
}