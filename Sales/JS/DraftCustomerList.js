﻿$(document).ready(function () {
    getCustomerList(1);
    bindDropDowns();
});

function Pagevalue(e) {
    getCustomerList(parseInt($(e).attr("page")));
};


/*------------------------------------------------------Bind SALES PERSON------------------------------------------------------------*/
function bindDropDowns() {
    $.ajax({
        type: "POST",
        url: "DraftCustomerList.aspx/bindDropDowns",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        cache: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var salesPerson = $(xmldoc).find('Table');

            $("#ddlSSalesPerson option:not(:first)").remove();  //-------------------------------For Search Field
            $.each(salesPerson, function () {
                $("#ddlSSalesPerson").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("Name").text() + "</option>");
            });

            var Customertype = $(xmldoc).find('Table1');

            $("#ddlCustomerType option:not(:first)").remove();  //-------------------------------For Search Field
            $.each(Customertype, function () {
                $("#ddlCustomerType").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("CustomerType").text() + "</option>");
            });
            var State = $(xmldoc).find('Table2');

            $("#ddlState option:not(:first)").remove();  //-------------------------------For Search Field
            $.each(State, function () {
                $("#ddlState").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("StateName").text() + "</option>");
            });
            $("#ddlState").select2();
            var City = $(xmldoc).find('Table3');

            $("#ddlCity option:not(:first)").remove();  //-------------------------------For Search Field
            $.each(City, function () {
                $("#ddlCity").append("<option City='" + $(this).find("City").text() + "' value='" + $(this).find("AutoId").text() + "'>" + $(this).find("CityName").text() + "</option>");
            });
            $("#ddlCity").select2();


        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
/*------------------------------------------------------Get Customer List (bind Table)--------------------------------------------------*/
function getCustomerList(PageIndex) {
    debugger
    var CityName = '',StateName='';
    if ($("#ddlCity option:selected").text() == 'All City') {
        CityName = '';
    }
    else {
        CityName=$("#ddlCity option:selected").attr('city');
    }
    if ($("#ddlState option:selected").text() == 'All State') {
        StateName = '';
    }
    else {
        StateName=$("#ddlState option:selected").text();
    }
    var data = {
        CustomerName: $("#txtSCustomerName").val().trim(),
        SalesPersonAutoId: $("#ddlSSalesPerson").val(),
        CustomerType: $("#ddlCustomerType").val(),
        StateName: StateName,
        CityName:CityName,
        Status: $("#ddlStatus").val(),
        PageSize: $("#ddlPageSize").val(),
        pageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "DraftCustomerList.aspx/getCustomerList",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var customer = $(xmldoc).find('Table1');

            $('#tblCustomerList tbody tr').remove();
            if (customer.length > 0) {
                $('#EmptyTable').hide();
                var row = $('#tblCustomerList thead tr').clone(true);
                $.each(customer, function () {
                    $(".CustName", row).text($(this).find("CustomerName").text());
                    $(".CustomerType", row).text($(this).find("CustomerType").text());
                    $(".BusinessName", row).text($(this).find("BusinessName").text());
                    $(".OPTLicence", row).text($(this).find("OPTLicence").text());
                    $(".SalesPerson", row).text($(this).find("SalesPerson").text());
                    $(".Terms", row).text($(this).find("TermsDesc").text());
                    $(".CreatedBy", row).text($(this).find("CreatedBy").text());
                    $(".CreatedDate", row).text($(this).find("CreatedDate").text());
                    $(".ShippingAddress", row).html("<p>" + $(this).find("CompleteShippingAddress").text()+"</p>");
                    $(".BillingAddress", row).html("<p>" + $(this).find("CompleteBillingAddress").text()+"</p>");
                     if ($(this).find("Status").text() == '1') {
                         $(".Status", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("StatusType").text() + "</span>");
                    } else {
                         $(".Status", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("StatusType").text() + "</span>");
                    }

                    $(".Action", row).html("<a href='javascript:;'><span class='la la-edit' onclick='editCustomer(\"" + $(this).find("CustomerId").text() + "\")'></span></a>&nbsp&nbsp<a href='javascript:;'><span class='ft-x' onclick='deleterecord(\"" + $(this).find("CustomerId").text() + "\")'></span></a>");

                    $("#tblCustomerList tbody").append(row);
                    row = $("#tblCustomerList tbody tr:last-child").clone(true);
                });

                if ($("#hidnEmpType").val() == "Admin") {
                    $("#tblCustomerList").find(".SalesPerson").show();
                }
            }
            else {
                $('#EmptyTable').show();
            }

            var pager = $(xmldoc).find("Table");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
    });
}

function editCustomer(CustomerId) {
    window.location.href = "/Sales/DraftCustomerMaster.aspx?customerId=" + CustomerId;
}
function deleteCustomer(CustomerId) {

    $.ajax({
        type: "POST",
        url: "DraftCustomerList.aspx/deleteCustomer",
        data: "{'CustomerId':'" + CustomerId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (result) {
            if (result.d == "Session Expired") {
                window.location = "/";
            }
            else if (result.d == 'Success') {
                swal("", "Customer details deleted successfully.", "success");
                getCustomerList(1)
            } else {
                swal("Error!", result.d, "error");
            }
        },
        error: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later", "error");

        },
        failure: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later", "error");
        }
    })
}

function deleterecord(CustomerId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this draft customer.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, cancel.",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, delete it.",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            deleteCustomer(CustomerId);
        }
    })
}