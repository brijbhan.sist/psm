﻿var AutoId = 0; var getType, PaymentId = 0;
var c1 = 0, c2 = 0, c3 = 0, c4 = 0, c5 = 0, c6 = 0;
$(document).ready(function () {
    bindCardType();
    $('.date').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });

    // bindCustomerdocumnets();
    var getid = getQueryString('PageId');
    AutoId = getid;
    if (getid != null) {
        editCustomer(getid);
        CustomerOrderList(1);
        $('#EditCustomer').attr('href', '/Sales/customerMaster.aspx?PageId=' + AutoId)
    }
    getType = getQueryString('Type');
    if (getType != null) {
        $('.nav-tabs').find('a:eq(0)').removeClass('active');
        $('.nav-tabs').find('a:eq(4)').attr('class', 'nav-link active')
        $('.nav-tabs').find('a:eq(4)').attr('aria-expanded', 'true')
        $('#CollectionDetails').attr('class', 'tab-pane fade active in show')
        $('#OrderList').attr('class', 'tab-pane fade');
        $('.paynow').hide();
        $('#btnPayNow').hide();
        $('#btnCancel').show();
        $("#txtReceiveAmount").val('');
        $("#ddlPaymentMenthod").val('0');
        $("#txtReferenceNo").val('');
        $("#txtPaymentRemarks").val('');
        $("#ddlReceivedPaymentBy").val('0');
        $("#txtEmployeeRemarks").val('');
        CollectionDetailsSearch();
    }

    PaymentId = getQueryString('PaymentId');
    if (PaymentId != 0 && PaymentId != null) {
        $('.nav-tabs').find('a:eq(0)').removeClass('active');
        $('.nav-tabs').find('a:eq(4)').attr('class', 'nav-link active')
        $('.nav-tabs').find('a:eq(4)').attr('aria-expanded', 'true')
        $('#CollectionDetails').attr('class', 'tab-pane fade active in show')
        $('#OrderList').attr('class', 'tab-pane fade');
        CollectionDetailsSearch();
    }

    if ($('#hiddenEmpTypeVal').val() == 6) {
        $("#Button5").show();
    } else {
        $("#Button5").hide();
    }
});

var getQueryString = function (field, url) {
    var href = url ? url : window.location.href;
    var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
    var string = reg.exec(href);
    return string ? string[1] : null;
};
/*------------------------------------------------------Set Billing Address------------------------------------------------------------*/
function bindCustomerdocumnets() {
    $.ajax({
        type: "POST",
        url: "ViewCustomerDetails.aspx/binddocumnets",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            if (response.d != "Session Expired") {
                var getData = $.parseJSON(response.d);
                $("#ddlDocumentName option:not(:first)").remove();
                $.each(getData, function (index, item) {
                    $("#ddlDocumentName").append("<option value='" + getData[index].DocumentName + "'>" + getData[index].Document + "</option>");
                });

            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(result.d);
        },
        error: function (result) {
            console.log(result.d);
        }
    });
}

/*------------------------------------------------------Edit Customer------------------------------------------------------------*/
function editCustomer(AutoId) {
    $.ajax({
        type: "POST",
        url: "ViewCustomerDetails.aspx/GetCustomerDetails",
        data: "{'CustomerAutoId':'" + AutoId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var html = "";

                var customer = $(xmldoc).find('Table');
                var EmpDetails = $(xmldoc).find('Table1');
                var TotalDueAmount = $(xmldoc).find('Table2');
                if ($(TotalDueAmount).find("TotalDueAmount").text() != '')
                    $("#TotalDueAmount").text($(TotalDueAmount).find("TotalDueAmount").text());
                $("#txtCustomerId").text($(customer).find("CustomerId").text());
                $("#hiddenCustomerAutoId").val(AutoId);
                $("#lblZone").text($(customer).find("Zone1").text());
                $("#txtCustomerName").text($(customer).find("CustomerName").text());
                $("#spanContactPerson").text($(customer).find("ContactPersonName").text());
                $("#spanCustomerType").text($(customer).find("CustomerType").text());
                $("#spanSalesPerson").text($(customer).find("EmpName").text());
                $("#txtPricelevel").text($(customer).find("PriceLevelName").text());
                $("#txtTotalStoreCredit").attr('TotalAmount', $(customer).find("CreditAmount").text());
                $("#divStatus").html('');
                if ($(customer).find("Status").text() == '1') {
                    $("#lblStatus").text($(customer).find("StatusType").text());
                    $("#lblStatus").addClass("badge-success")
                    $("#lblStatus").removeClass("badge-danger")
                } else {
                    $("#lblStatus").text($(customer).find("StatusType").text());
                    $("#lblStatus").removeClass("badge-success")
                    $("#lblStatus").addClass("badge-danger")
                }


                if ($(customer).find("CreatedBy").text() == '' || $(customer).find("CreatedBy").text() == null) {
                    $("#createdBy").text('NA');
                } else {
                    $("#createdBy").text($(customer).find("CreatedBy").text());
                }
                if ($(customer).find("CreatedOn").text() == '' || $(customer).find("CreatedOn").text() == null) {
                    $("#createdOn").text('NA');
                } else {
                    $("#createdOn").text($(customer).find("CreatedOn").text());
                }
                if ($(customer).find("UpdatedBy").text() == '' || $(customer).find("UpdatedBy").text() == null) {
                    $("#updatedBy").text('NA');
                } else {
                    $("#updatedBy").text($(customer).find("UpdatedBy").text());
                }
                if ($(customer).find("UpdateOn").text() == '' || $(customer).find("UpdateOn").text() == null) {
                    $("#updatedOn").text('NA');
                } else {
                    $("#updatedOn").text($(customer).find("UpdateOn").text());
                }

                if (parseFloat($(customer).find("CreditAmount").text()) <= 0) {
                    $('#btnPaywithCredit').hide();
                } else {
                    $('#btnPaywithCredit').show();
                }
                $("#CreditAmount").text($(customer).find("CreditAmount").text());
                $("#txtStoreCreditTotal").attr('TotalAmount', $(customer).find("CreditAmount").text());
                $("#txtTotalStoreCredit").val((customer).find("CreditAmount").text());
                $("#txtStoreCreditTotal").val($(customer).find("CreditAmount").text());
                if (EmpDetails.length > 0) {
                    $("#ddlReceivedPaymentBy option:not(:first)").remove();
                    $.each(EmpDetails, function () {
                        $("#ddlReceivedPaymentBy").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("EmpName").text() + "</option>");
                    });
                    $("#EmpName1 option:not(:first)").remove();
                    $.each(EmpDetails, function () {
                        $("#EmpName1").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("EmpName").text() + "</option>");
                    });
                }

            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function PayNow() {
    $('.paynow').show();
    $('#btnPayNow').hide();
    $('#btnCancel').show();
    $("#PaymentAutoId").val('0')
    $("#txtReceiveAmount").val('');
    $("#ddlPaymentMenthod").val('0');
    $("#txtReferenceNo").val('');
    $("#txtPaymentRemarks").val('');
    $("#ddlReceivedPaymentBy").val('0');
    $("#txtEmployeeRemarks").val('');
}
function PayCancel() {
    if ($("#ddlPaymentMenthod").val() == "1") {
        $(".DuehideCurrency").hide();
        $("#base-DueOrderList").removeClass("active");
        $("#base-CollectionDetails").addClass("active");
        $("#DueOrderList").removeClass("active in show");
        $("#CollectionDetails").addClass("active in show");
    }
    $("#PaymentAutoId").val('0')
    $('.paynow').hide();
    $('#btnPayNow').show();
    $('#btnCancel').hide();
    $("#btnpaymentCancel").hide();
    $('#txtStoreCredit').val('0.00');
    $('#txtTotalStoreCredit').val($('#txtTotalStoreCredit').attr('totalamount'));
    $("#pay").show();
    $('#ddlPaymentMenthod').attr('disabled', false);
    $('#txtReferenceNo').attr('disabled', false);
    $('#ddlReceivedPaymentBy').attr('disabled', false);
    $('#txtReceiveAmount').attr('disabled', false);
    $('#txtPaymentRemarks').attr('disabled', true);
    $('#txtPaymentId').val('');
    c2 = 0;
    CustomerDuePayment();
}
function CustomerOrderList(PageIndex) {
    $.ajax({
        type: "POST",
        url: "ViewCustomerDetails.aspx/CustomerOrderList",
        data: "{'CustomerAutoId':'" + AutoId + "','PageIndex':'" + PageIndex + "','PageSize':'" + $("#ddlPageSize").val() + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var customerOrderList = $(xmldoc).find('Table1');
                var customerCreditList = $(xmldoc).find('Table2');
                $("#Table1 tbody tr").remove();
                var row = $("#Table1 thead tr").clone(true);
                var PayableAmount = 0.00; AmtPaid = 0.00, AmtDue = 0.00;
                if (customerOrderList.length > 0) {
                    $.each(customerOrderList, function (index) {

                        if ($("#hiddenEmpTypeVal").val() == '6') {
                            $(".OrderNo", row).html("<span OrderAutoId='" + $(this).find("AutoId").text() + "'><a target='_blank' href='/Account/Acccount_viewOrder.aspx?OrderAutoId=" + $(this).find("AutoId").text() + "'>" + $(this).find("OrderNo").text() + "</a></span>");
                        } else if ($("#hiddenEmpTypeVal").val() == '8') {
                            $(".OrderNo", row).html("<span OrderAutoId='" + $(this).find("AutoId").text() + "'><a target='_blank' href='/Manager/Manager_viewOrder.aspx?OrderAutoId=" + $(this).find("AutoId").text() + "'>" + $(this).find("OrderNo").text() + "</a></span>");
                        } else {
                            $(".OrderNo", row).html("<a target='_blank' href='/sales/OrderMaster.aspx?OrderNo=" + $(this).find("OrderNo").text() + "'>" + $(this).find("OrderNo").text() + "</a>");
                        }



                        $(".Date", row).text($(this).find("OrderDate").text());
                        $(".OrderType", row).text($(this).find("OrderType").text());
                        if (Number($(this).find("StatusCode").text()) == 1) {
                            $(".Status", row).html("<span class='badge badge badge-pill Status_New'>" + $(this).find("Status").text() + "</span>");
                        } else if (Number($(this).find("StatusCode").text()) == 2) {
                            $(".Status", row).html("<span class='badge badge badge-pill Status_Processed'>" + $(this).find("Status").text() + "</span>");
                        } else if (Number($(this).find("StatusCode").text()) == 3) {
                            $(".Status", row).html("<span class='badge badge badge-pill Status_Packed' style='background-color:#66ff99'>" + $(this).find("Status").text() + "</span>");
                        } else if (Number($(this).find("StatusCode").text()) == 4) {
                            $(".Status", row).html("<span class='badge badge badge-pill Status_Ready_to_Ship' style='background-color:#669900'>" + $(this).find("Status").text() + "</span>");
                        } else if (Number($(this).find("StatusCode").text()) == 5) {
                            $(".Status", row).html("<span class='badge badge badge-pill Status_Shipped' style='background-color:#33cc33'>" + $(this).find("Status").text() + "</span>");
                        } else if (Number($(this).find("StatusCode").text()) == 6) {
                            $(".Status", row).html("<span class='badge badge badge-pill Status_Delivered'>" + $(this).find("Status").text() + "</span>");
                        } else if (Number($(this).find("StatusCode").text()) == 7) {
                            $(".Status", row).html("<span class='badge badge badge-pill Status_Undelivered' style='background-color:#660000'>" + $(this).find("Status").text() + "</span>");
                        } else if (Number($(this).find("StatusCode").text()) == 8) {
                            $(".Status", row).html("<span class='badge badge badge-pill Status_cancelled' style='background-color:#ff9933'>" + $(this).find("Status").text() + "</span>");
                        }
                        else if (Number($(this).find("StatusCode").text()) == 9) {
                            $(".Status", row).html("<span class='badge badge badge-pill Status_Add_On' style='background-color:green'>" + $(this).find("Status").text() + "</span>");
                        }
                        else if (Number($(this).find("StatusCode").text()) == 10) {
                            $(".Status", row).html("<span class='badge badge badge-pill Status_Add_On_Packed' style='background-color:#61b960;'>" + $(this).find("Status").text() + "</span>");
                        }
                        else if (Number($(this).find("StatusCode").text()) == 11) {
                            $(".Status", row).html("<span class='badge badge badge-pill Status_Close'>" + $(this).find("Status").text() + "</span>");
                        }
                        $(".OrderItems", row).text($(this).find("OrderItems").text());
                        $(".OrderAmount", row).text($(this).find("PayableAmount").text());
                        PayableAmount += parseFloat($(this).find("PayableAmount").text());
                        AmtPaid += parseFloat($(this).find("AmtPaid").text());
                        if (parseFloat($(this).find("AmtDue").text()) > 0) {
                            AmtDue += parseFloat($(this).find("AmtDue").text());
                        }
                        if ($(this).find("AmtPaid").text() != 0) {
                            $(".PaidAmount", row).html("<a href='#' onclick='getPaymentDetail(" + $(this).find("AutoId").text() + ")'>" + $(this).find("AmtPaid").text() + "</a>");
                        } else {
                            $(".PaidAmount", row).html($(this).find("AmtPaid").text());

                        }
                        $(".DueAmount", row).text($(this).find("AmtDue").text());
                        $(".DeliveryDate", row).text($(this).find("DeliveryDate").text());
                        $(".SalesMan", row).text($(this).find("SalesMan").text());
                        $(".Packer", row).text($(this).find("PackerName").text());
                        $(".SalesManager", row).text($(this).find("SalesManager").text());
                        $(".Account", row).text($(this).find("Account").text());
                        $(".ShippingType", row).text($(this).find("ShippingType").text());

                        $("#Table1 tbody").append(row);
                        row = $("#Table1 tbody tr:last").clone(true);
                    });
                    $('#OrderListTotal').text(PayableAmount.toFixed(2));
                    $('#OrderListPaid').text(AmtPaid.toFixed(2));
                    $('#OrderListDue').text(AmtDue.toFixed(2));
                }
            } else {
                location.href = '/';
            }
            var pager = $(xmldoc).find("Table");
            $("#OrdL").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
        },
        error: function (result) {
        },
        failure: function (result) {
        }
    });
}


function getPaymentDetail(orderNo) {
    $.ajax({
        type: "POST",
        url: "ViewCustomerDetails.aspx/OrderPaymentDetails",
        data: "{'OrderAutoId':'" + orderNo + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var orderpDetails = $(xmldoc).find('Table');
                $("#tblOrderPayment tbody tr").remove();
                var row = $("#tblOrderPayment thead tr").clone(true);
                var paidTotal = 0.0;
                $.each(orderpDetails, function (index) {

                    $(".PaymentId_r", row).text($(this).find("PaymentId").text());
                    $(".PaymentDate_r", row).text($(this).find("PaymentDate").text());
                    $(".RecievedAmount_r", row).text($(this).find("ReceivedAmount").text());
                    $(".SettledBy", row).text($(this).find("SettledBy").text());
                    $(".PaymentMode", row).text($(this).find("PaymentMode").text());
                    paidTotal = paidTotal + parseFloat($(this).find("ReceivedAmount").text());
                    $("#tblOrderPayment tbody").append(row);
                    row = $("#tblOrderPayment tbody tr:last").clone(true);
                });
                $("#paidTotal").html(parseFloat(paidTotal).toFixed(2))
                $("#OrderPaymentDetails").modal();

            } else {
                location.href = '/';
            }
        },
        error: function (result) {
        },
        failure: function (result) {
        }
    });
}


function CustomerCreditMemoList(PageIndex) {
    $.ajax({
        type: "POST",
        url: "ViewCustomerDetails.aspx/CustomerCreditMemoList",
        data: "{'CustomerAutoId':'" + AutoId + "','PageIndex':'" + PageIndex + "','PageSize':'" + $("#ddlPageCMSize").val() + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var customerCreditList = $(xmldoc).find('Table1');
                $("#Table2 tbody tr").remove();
                var row = $("#Table2 thead tr").clone(true);
                var CreditListAmount = 0.00, CreditListTotalAmount = 0.00;
                if (customerCreditList.length > 0) {
                    $.each(customerCreditList, function (index) {
                        if ($("#hiddenEmpTypeVal").val() == '8') {
                            $(".CreditNo", row).html("<a target='_blank' href='/Manager/ManagerCreditMemo.aspx?PageId=" + $(this).find("CreditAutoId").text() + "'>" + $(this).find("CreditNo").text() + "</a></b>");
                        } else if ($("#hiddenEmpTypeVal").val() == '6') {
                            $(".CreditNo", row).html("<a target='_blank' href='/Account/AccountCreditMemoView.aspx?PageId=" + $(this).find("CreditAutoId").text() + "'>" + $(this).find("CreditNo").text() + "</a></b>");
                        } else {
                            $(".CreditNo", row).html("<a target='_blank' href='/Sales/CreditMemo.aspx?PageId=" + $(this).find("CreditAutoId").text() + "'>" + $(this).find("CreditNo").text() + "</a>");
                        }

                        $(".CreditDate", row).text($(this).find("CreditDate").text());
                        ;
                        if ($(this).find("Status").text() == "1") {
                            $(".StatusType", row).html("<span class='badge badge badge-pill badge-info'>" + $(this).find("StatusType").text() + "</span>");
                        } else if ($(this).find("Status").text() == "3") {
                            $(".StatusType", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("StatusType").text() + "</span>");
                        }
                        else {
                            $(".StatusType", row).html("<span class='badge badge badge-pill badge-warning'>" + $(this).find("StatusType").text() + "</span>");
                        }

                        $(".CreditType", row).text($(this).find("CreditType").text());
                        if ($(this).find("OrderAutoId").text() == '' && $(this).find("Status").text() == '3') {
                            if ($("#hiddenEmpTypeVal").val() == '6') {
                                $(".action", row).html("<a href='javascript:void(0)' class='btn btn-info buttonAnimation round box-shadow-1  btn-sm' onclick='getCreditMemo(" + $(this).find("CreditAutoId").text() + ")'>Settle</a>");
                                $(".Sr", row).html("<input type='checkbox' class='checkbox' CreditAutoId='" + $(this).find("CreditAutoId").text() + "'/> ");
                            } else {
                                $(".action", row).html('');
                                $(".Sr", row).html("");
                            }


                        } else {
                            $(".action", row).html("");
                            $(".Sr", row).html('');

                        }

                        $(".SalesAmount", row).text($(this).find("SalesAmount").text());
                        $(".TotalAmount", row).text($(this).find("SalesAmount").text());

                        $(".AppliedOrder", row).text($(this).find("AppliedOrder").text());
                        $(".CreatedBy", row).text($(this).find("CreatedBy").text());
                        $(".ApprovedBy", row).text($(this).find("ApprovedBy").text());
                        $(".SettledBy", row).text($(this).find("SettledBy").text());
                        $(".ApplyDate", row).text($(this).find("ApplyDate").text());

                        if ($(this).find("Status").text() == "1" || $(this).find("Status").text() == "3") {
                            CreditListAmount += parseFloat($(this).find("TotalAmount").text());
                            CreditListTotalAmount += parseFloat($(this).find("TotalAmount").text());
                        }

                        $("#Table2 tbody").append(row);
                        row = $("#Table2 tbody tr:last").clone(true);
                    });
                    if ($("#hiddenEmpTypeVal").val() != '6') {
                        $("#Table2 .action").hide('');
                        $("#Table2 .Sr").hide();
                        $("#btnSettleCredit").hide();
                        $("#CreditList").attr('colspan', '5');
                    }
                    $('#CreditListAmount').text(parseFloat(CreditListAmount).toFixed(2));
                    $('#CreditListTotalAmount').text(parseFloat(CreditListTotalAmount).toFixed(2));
                }

            } else {
                location.href = '/';
            }
            var pager = $(xmldoc).find("Table");
            $("#CRList").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
            $("#base-CreditMemo").removeAttr('onclick');
        },
        error: function (result) {
        },
        failure: function (result) {
        }
    });
}
function CustomerDuePaymentSearch() {
    c2 = 0;
    CustomerDuePayment()
}

function CustomerDuePayment() {
    $("#pay").show();
    $(".paynow").hide();
    $(".DuehideCurrency").hide();
    if (c2 == 0) {
        var data = {
            CustomerAutoId: AutoId,
            OrderNo: $("#txtSCustomerId").val().trim(),
            FromDate: $("#txtFromDate").val(),
            ToDate: $("#txtToDate").val(),
        }
        $.ajax({
            type: "POST",
            url: "ViewCustomerDetails.aspx/CustomerDuePayment",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json;charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d != "Session Expired") {
                    var xmldoc = $.parseXML(response.d);
                    var customerOrderList = $(xmldoc).find('Table');
                    var TotalDueAmount = $(xmldoc).find('Table1');
                    var Currency = $(xmldoc).find('Table2');
                    $("#Table4 tbody tr").remove();
                    var row = $("#Table4 thead tr").clone(true);
                    var TotalOrderAmount = 0.00, TotalPaidAmount = 0.00, DueAmount = 0.00, TotalPayAmount = 0.00, TotalpayDueAmount = 0.00;
                    if (customerOrderList.length > 0) {
                        $.each(customerOrderList, function (index) {
                            if ($("#hiddenEmpTypeVal").val() == '6') {
                                $(".OrderNo", row).html("<span OrderAutoId='" + $(this).find("OrderAutoId").text() + "'><a target='_blank' href='/Account/Acccount_viewOrder.aspx?OrderAutoId=" + $(this).find("OrderAutoId").text() + "'>" + $(this).find("OrderNo").text() + "</a></span>");
                            } else if ($("#hiddenEmpTypeVal").val() == '8') {
                                $(".OrderNo", row).html("<span OrderAutoId='" + $(this).find("OrderAutoId").text() + "'><a target='_blank' href='/Manager/Manager_viewOrder.aspx?OrderAutoId=" + $(this).find("OrderAutoId").text() + "'>" + $(this).find("OrderNo").text() + "</a></span>");
                            } else {
                                $(".OrderNo", row).html("<span OrderAutoId='" + $(this).find('OrderAutoId').text() + "'></span><a target='_blank' href='/sales/OrderMaster.aspx?OrderNo=" + $(this).find("OrderNo").text() + "'>" + $(this).find("OrderNo").text() + "</a>");
                            }

                            $(".OrderDate", row).text($(this).find("OrderDate").text());
                            $(".DaysOld", row).text($(this).find("DaysOld").text());
                            $(".OrderType", row).text($(this).find("OrderType").text());
                            $(".OrderAmount", row).text(parseFloat($(this).find("PayableAmount").text()).toFixed(2));
                            TotalOrderAmount += parseFloat($(this).find("PayableAmount").text());
                            TotalPaidAmount += parseFloat($(this).find("AmtPaid").text());
                            $(".PaidAmount", row).text(parseFloat($(this).find("AmtPaid").text()).toFixed(2));
                            $(".DueAmount", row).text(parseFloat($(this).find("AmtDue").text()).toFixed(2));
                            DueAmount += parseFloat($(this).find("AmtDue").text());
                            $(".CheckBoc", row).html("<input type='checkbox' class='chkDue' onclick='funCheckBoc(this)' />");
                            $(".PayAmount", row).text('0.00');
                            $(".payDueAmount", row).text('0.00');
                            $("#Table4 tbody").append(row);
                            row = $("#Table4 tbody tr:last").clone(true);
                        });

                        if ($('#hiddenEmpTypeVal').val() == 6) {

                            if (getType == null) {
                                $("#btnPayNow").show();
                            }
                            // $("#btnPayNow").hide();
                        } else {
                            $("#btnPayNow").hide();
                        }
                    } else {
                        if ($('#hiddenEmpTypeVal').val() == 6) {
                            $("#btnPayNow").show();
                        } else {
                            $("#btnPayNow").hide();
                        }
                    }
                    $("#TotalOrderAmount").text(TotalOrderAmount.toFixed(2));
                    $("#TotalPaidAmount").text(TotalPaidAmount.toFixed(2));
                    $("#DueAmount").text(DueAmount.toFixed(2));
                    $("#TotalPayAmount").text(TotalPayAmount.toFixed(2));
                    $("#TotalpayDueAmount").text(TotalpayDueAmount.toFixed(2));

                    $("#tblCurrencyList tbody tr").remove();
                    var row = $("#tblCurrencyList thead tr").clone(true);
                    $.each(Currency, function (index) {
                        $(".CurrencyName", row).html("<span CurrencyValue='" + $(this).find('CurrencyValue').text() + "'  CurrencyAutoid='" + $(this).find('Autoid').text() + "'></span>" + $(this).find("Currencyname").text());
                        $(".NoofRupee", row).html("<input type='Text' id='txtTopCurrency' maxlength='6' class='form-control input-sm text-center' onkeypress='return isNumberKey(event)' onchange='PaymentCal(this)' onfocus='this.select()' />");
                        $(".Total", row).text('0.00');
                        $("#tblCurrencyList tbody").append(row);
                        row = $("#tblCurrencyList tbody tr:last").clone(true);
                    });
                    c2 = c2 + 1;

                } else {
                    location.href = '/';
                }

            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
}
function PaymentCal(e) {

    var row = $(e).closest('tr');
    var Total = 0.00, TotalAmount = 0.00, noofRupee1 = 0, sortAmt = 0.00, ReceiveAmount = 0.00;
    var Currencyvalue = $(row).find(".CurrencyName span").attr('CurrencyValue');
    if ($(row).find(".NoofRupee input").val() != '') {
        noofRupee1 = $(row).find(".NoofRupee input").val();
        Total = parseFloat(parseFloat(Currencyvalue) * parseFloat(noofRupee1)).toFixed(2);
    }
    $(row).find(".Total").text(parseFloat(Total).toFixed(2) || 0.00);
    $("#tblCurrencyList tbody tr").each(function () {
        TotalAmount += parseFloat($(this).find('.Total').text());
    });
    ReceiveAmount = parseFloat($("#txtReceiveAmount").val());
    if (TotalAmount > parseFloat(ReceiveAmount)) {
        $(row).find(".NoofRupee input").val('');
        $(row).find(".Total").html('0.00');
        $(row).find(".NoofRupee input").focus();
        $(row).find(".NoofRupee input").addClass('border-warning');
        TotalAmount = TotalAmount - Total;
    }
    //TotalAmount = 0;
    //$("#tblCurrencyList tbody tr").each(function () {
    //    TotalAmount += parseFloat($(this).find('.Total').text());
    //});
    $("#TotalAmount").text(parseFloat(TotalAmount).toFixed(2));
    sortAmt = parseFloat(ReceiveAmount - TotalAmount).toFixed(2);
    $("#txtSortAmount").val(parseFloat(sortAmt).toFixed(2));
    $("#txtTotalCurrencyAmount").val((ReceiveAmount - sortAmt).toFixed(2));
    if ($('#OrderAutoId').val() == '') {
        $("#Table4 tbody tr .CheckBoc input").prop('checked', false);
        $('#Table4 tbody tr .PayAmount').text('0.00');
        $('#Table4 tbody tr .payDueAmount').text('0.00');
        $('#TotalPayAmount').text('0.00');
        $('#TotalpayDueAmount').text('0.00');
    }
}
function SortAmountCal() {

    var TotalCurrencyAmount = 0;
    var SortAmount = 0
    if ($("#txtTotalCurrencyAmount").val() != "") {
        TotalCurrencyAmount = $("#txtTotalCurrencyAmount").val();
    }
    if ($("#txtSortAmount").val() != "") {
        SortAmount = $("#txtSortAmount").val();
    }
    var TotalAmount = 0;
    if ($("#TotalAmount").text() != '') {
        TotalAmount = $("#TotalAmount").text();
    }

    TotalCurrencyAmount = parseFloat(TotalAmount) + parseFloat(SortAmount)
    $("#txtTotalCurrencyAmount").val(parseFloat(TotalCurrencyAmount).toFixed(2));

}
function funCheckBoc(e) {
    var ReceiveAmount = 0.00;
    var PaidAmount = 0.00;
    if ($("#ddlPaymentMenthod").val() == "1") {
        if ($("#txtTotalCurrencyAmount").val() == "0.00" || $("#txtTotalCurrencyAmount").val() == "") {
            swal({
                text: "Please fill currency denomination.",
                allowOutsideClick: false,
                closeOnClickOutside: false,
                icon: "warning",
                buttons: {
                    confirm: {
                        text: "Ok",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: true
                    }
                }
            }).then(function (ok) {
                if (ok) {
                    $("#txtTopCurrency").addClass("border-warning");
                    $("#txtTopCurrency").focus();
                    var PayAmount = 0.00, payDueAmount = 0.00;
                    $("#Table4 tbody tr").each(function () {
                        if ($(this).find('.CheckBoc input').prop('checked') == true) {
                            PayAmount += parseFloat($(this).find('.PayAmount').text());
                            payDueAmount += parseFloat($(this).find('.payDueAmount').text());
                        }
                    });
                    PayAmount = parseFloat(PayAmount).toFixed(2);
                    $('#TotalPayAmount').text(parseFloat(PayAmount).toFixed(2))
                    $('#TotalpayDueAmount').text(payDueAmount.toFixed(2));
                    if ($('#ddlPaymentMenthod').val() == 5) {
                        $('#txtStoreCredit').val((parseFloat($('#CreditAmount').text()) - parseFloat($('#txtReceiveAmount').val())).toFixed(2));
                    } else {
                        ReceiveAmount = $("#txtTotalCurrencyAmount").val();
                        $('#txtStoreCredit').val((parseFloat(ReceiveAmount) - parseFloat(PayAmount)).toFixed(2));
                        var TotalStoreCredit = $('#txtTotalStoreCredit').attr('TotalAmount');
                        var TOTAL = parseFloat(TotalStoreCredit) + parseFloat((parseFloat(ReceiveAmount) - parseFloat(PayAmount)));
                        $('#txtTotalStoreCredit').val(parseFloat(TOTAL).toFixed(2));
                    }

                    return;
                }
            })
        }
        ReceiveAmount = $("#txtTotalCurrencyAmount").val();
    }
    else {
        if ($("#txtReceiveAmount").val() != "") {
            ReceiveAmount = $("#txtReceiveAmount").val();
        }
    }
    var tr = $(e).closest('tr');
    var DueAmount = 0.00;
    DueAmount = parseFloat(tr.find('.DueAmount').text());
    if ($(e).prop('checked') == true) {

        if (parseFloat(ReceiveAmount) > 0) {

            var PayAmount = 0.00;
            $("#Table4 tbody tr").each(function () {
                PayAmount += parseFloat($(this).find('.PayAmount').text());
            });

            var Amount = parseFloat(ReceiveAmount) - parseFloat(PayAmount);
            if (parseFloat(DueAmount) > parseFloat(Amount)) {
                if (parseFloat(Amount) == 0) {
                    tr.find('.PayAmount').text('0.00');
                    tr.find('.payDueAmount').text('0.00');
                    $(e).prop('checked', false);
                } else {
                    tr.find('.PayAmount').text(Amount.toFixed(2));
                    tr.find('.payDueAmount').text((parseFloat(DueAmount) - parseFloat(Amount)).toFixed(2));
                }

            }
            else {
                PaidAmount = parseFloat(DueAmount) - parseFloat(Amount);
                tr.find('.PayAmount').text(DueAmount.toFixed(2));
                tr.find('.payDueAmount').text('0.00');
            }

        } else {
            $("#txtReceiveAmount").focus();
            $(e).prop('checked', false);
            $("#txtReceiveAmount").css('border-color', 'Red');
        }
    } else {
        tr.find('.PayAmount').text('0.00');
        tr.find('.payDueAmount').text('0.00');
    }
    var PayAmount = 0.00, payDueAmount = 0.00;
    $("#Table4 tbody tr").each(function () {
        if ($(this).find('.CheckBoc input').prop('checked') == true) {
            PayAmount += parseFloat($(this).find('.PayAmount').text());
            payDueAmount += parseFloat($(this).find('.payDueAmount').text());
        }
    });
    PayAmount = parseFloat(PayAmount).toFixed(2);
    $('#TotalPayAmount').text(parseFloat(PayAmount).toFixed(2))
    $('#TotalpayDueAmount').text(payDueAmount.toFixed(2));
    if ($('#ddlPaymentMenthod').val() == 5) {
        $('#txtStoreCredit').val((parseFloat($('#CreditAmount').text()) - parseFloat($('#txtReceiveAmount').val())).toFixed(2));
        ReceiveAmount = ((0 - parseFloat($('#txtReceiveAmount').val()))).toFixed(2);
    } else {
        $('#txtStoreCredit').val((parseFloat(ReceiveAmount) - parseFloat(PayAmount)).toFixed(2));
        var TotalStoreCredit = $('#txtTotalStoreCredit').attr('TotalAmount');
        var TOTAL = parseFloat(TotalStoreCredit) + parseFloat((parseFloat(ReceiveAmount) - parseFloat(PayAmount)));
        $('#txtTotalStoreCredit').val(parseFloat(TOTAL).toFixed(2));
    }

}
function ReceivedPay() {
    if ($("#txtReceiveAmount").val() == '') {
        $("#txtReceiveAmount").val("0.00");
    }
    $("#txtReceiveAmount").css('border-color', '#ccc');
    var ReceiveAmount = 0.00;
    if ($('#ddlPaymentMenthod').val() == 5) {
        var StoreCredit = parseFloat($('#CreditAmount').text());
        if ($("#txtReceiveAmount").val() != '') {
            ReceiveAmount = parseFloat($("#txtReceiveAmount").val());
        }

        if (parseFloat(StoreCredit) < ReceiveAmount) {
            $("#txtReceiveAmount").css('border-color', 'red')
        }
        $('#txtStoreCredit').val((parseFloat(StoreCredit) - parseFloat(ReceiveAmount)).toFixed(2));
        CusStoreCredit = -1 * ReceiveAmount;
        StoreCredit = parseFloat($('#txtTotalStoreCredit').attr('TotalAmount'));
    } else {
        if ($('#txtReceiveAmount').val() != "") {
            ReceiveAmount = parseFloat($('#txtReceiveAmount').val());
        }
        $('#txtStoreCredit').val(parseFloat(ReceiveAmount).toFixed(2));
        CusStoreCredit = $('#txtStoreCredit').val();
        StoreCredit = parseFloat($('#txtTotalStoreCredit').attr('TotalAmount'));
    }
    $("#Table4 tbody tr .CheckBoc input").prop('checked', false);
    $('#Table4 tbody tr .PayAmount').text('0.00');
    $('#Table4 tbody tr .payDueAmount').text('0.00');
    $('#TotalPayAmount').text('0.00');
    $('#TotalpayDueAmount').text('0.00');
    $('#txtTotalStoreCredit').val((parseFloat(StoreCredit) + parseFloat(CusStoreCredit)).toFixed(2));
}
$('#ddlPaymentMenthod').change(function () {
    $("#txtReceiveAmount").css('border-color', '#ccc');
    $("#txtReceiveAmount").val('0.00');
    ReceivedPay();

});
function SavePayment() {
    var PaymentCurrencyXml = '';
    var SortAmount = 0, curTotal = 0;
    if ($("#txtSortAmount").val() != '') {
        SortAmount = $("#txtSortAmount").val();
    }
    if (dynamicALL('Save')) {
        if ((parseInt($('#ddlPaymentMenthod').val()) == '1')) {
            $("#tblCurrencyList tbody tr").each(function () {
                if ($(this).find(".NoofRupee input").val() != 0 && $(this).find(".NoofRupee input").val() != '') {
                    PaymentCurrencyXml += '<PaymentCurrencyXml>';
                    PaymentCurrencyXml += '<CurrencyAutoId><![CDATA[' + $(this).find('.CurrencyName span').attr('CurrencyAutoid') + ']]></CurrencyAutoId>';
                    PaymentCurrencyXml += '<NoOfValue><![CDATA[' + $(this).find(".NoofRupee input").val() + ']]></NoOfValue>';
                    PaymentCurrencyXml += '<TotalAmount><![CDATA[' + $(this).find(".Total").text() + ']]></TotalAmount>';
                    PaymentCurrencyXml += '</PaymentCurrencyXml>';
                    curTotal += parseFloat($(this).find(".CurrencyName span").attr("currencyvalue")) * parseFloat($(this).find(".NoofRupee input").val());
                }
            });
        }
        var TotalCurrencyAmount = 0;

        if ($("#txtTotalCurrencyAmount").val() != "") {
            TotalCurrencyAmount = $("#txtTotalCurrencyAmount").val();
        }
        var data = {
            CustomerAutoId: AutoId,
            ReceivedAmount: $("#txtReceiveAmount").val(),
            PaymentMethod: $("#ddlPaymentMenthod").val(),
            ReferenceNo: $("#txtReferenceNo").val(),
            PaymentRemarks: $("#txtPaymentRemarks").val(),
            ReceivedPaymentBy: $("#ddlReceivedPaymentBy").val(),
            EmployeeRemarks: $("#txtEmployeeRemarks").val(),
            StoreCredit: $("#txtStoreCredit").val(),
            PaymentAutoId: $("#PaymentAutoId").val() || 0,
            SortAmount: SortAmount,
            TotalCurrencyAmount: TotalCurrencyAmount,
            PaymentCurrencyXml: PaymentCurrencyXml
        }

        var ReceivedAmount = 0.00;
        if ((parseInt($('#ddlPaymentMenthod').val()) == 1)) {
            ReceivedAmount = $("#txtTotalCurrencyAmount").val();
        }
        else {
            ReceivedAmount = $("#txtReceiveAmount").val();
        }

        var ListReceivedAmount = 0.00;
        var tblReceivedOrder = [];
        $("#Table4 tbody tr").each(function () {
            if ($(this).find('.CheckBoc input').prop('checked') == true) {
                tblReceivedOrder.push({
                    OrderAutoId: $(this).find('.OrderNo span').attr('OrderAutoId'),
                    PayAmount: parseFloat($(this).find('.PayAmount').text())
                });
                ListReceivedAmount += parseFloat($(this).find('.PayAmount').text());
            }
        });
        if ((parseInt($('#ddlPaymentMenthod').val()) == 5) && (parseFloat(ReceivedAmount) != parseFloat(ListReceivedAmount))) {
            toastr.error('Received Amount and Order Amount should be equal.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        }
        else if ((parseInt($('#ddlPaymentMenthod').val()) == 5) && (parseFloat(ReceivedAmount) > parseFloat($('#CreditAmount').text()))) {
            toastr.error('Received Amount should be equal or less than Total Store Credit.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        }
        else if (ReceivedAmount == 0) {
            toastr.error('Received amount should be greater than zero.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        }
        else if ((parseFloat(ListReceivedAmount) > parseFloat(TotalCurrencyAmount)) && (parseInt($('#ddlPaymentMenthod').val()) == 1) && $('#ddlPaymentMenthod').val() == '') {
            toastr.error('Total amount should be equal to received amount.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        }
        else if ((parseFloat(curTotal) != parseFloat(TotalCurrencyAmount)) && (parseInt($('#ddlPaymentMenthod').val()) == 1)) {
            toastr.error('Total amount should be equal to received amount.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        }
        else {

            if (ListReceivedAmount < ReceivedAmount) {
                swal({
                    title: "Are you sure?",
                    text: "You want to receive payment with Store Credit.",
                    icon: "warning",
                    showCancelButton: true,
                    allowOutsideClick: false,
                    closeOnClickOutside: false,
                    buttons: {
                        cancel: {
                            text: "No, cancel!",
                            value: null,
                            visible: true,
                            className: "btn-warning",
                            closeModal: true,
                        },
                        confirm: {
                            text: "Yes, Receive it!",
                            value: true,
                            visible: true,
                            className: "",
                            closeModal: false
                        }
                    }
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        settlepayment(tblReceivedOrder, data);
                    }
                })

            } else {
                settlepayment(tblReceivedOrder, data);
            }

        }
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}


function settlepayment(tblReceivedOrder, data) {
    $.ajax({
        type: "Post",
        url: "ViewCustomerDetails.aspx/savePayment",
        data: JSON.stringify({ TableValues: JSON.stringify(tblReceivedOrder), dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (data) {
            if (data.d == 'Session Expired') {
                location.href = '/';
            } else if (data.d == 'true') {
                swal("", "Payment settled successfully.", "success").then(function () {
                    location.reload();
                });
            } else {
                swal("Error", "Oops, Something went wrong.Please try later.", "error");
            }
        },
        error: function (result) {
        },
        failure: function (result) {
        }
    });
}
function PaymentCustomerList() {
    c1 = 0;
    CustomerPaymentList(1);
}
function CustomerPaymentList(PageIndex) {
    if (c1 == 0) {
        var data = {
            CustomerAutoId: AutoId,
            Status: $("#ddlPStatus").val(),
            PaymentId: $("#txtSPaymentId").val().trim(),
            FromDate: $("#txtSFromDate").val(),
            ToDate: $("#txtSToDate").val(),
            PageIndex: PageIndex,
            PageSize: $("#ddlPageSizeInPayHis").val(),
        }
        $.ajax({
            type: "POST",
            url: "ViewCustomerDetails.aspx/CustomerPaymentList",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json;charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {

                if (response.d != "Session Expired") {
                    var xmldoc = $.parseXML(response.d);
                    var customerOrderList = $(xmldoc).find('Table1');
                    $("#Table3 tbody tr").remove();
                    var row = $("#Table3 thead tr").clone(true);
                    var Table3Total = 0.00;
                    if (customerOrderList.length > 0) {
                        $.each(customerOrderList, function (index) {
                            if ($(this).find("CreditAmount").text() != "0.00" && $(this).find("CreditAmount").text() != "0") {
                                $(".PaymentId", row).html("<a onclick='ViewPaymentDetails(" + $(this).find('PaymentAutoId').text() + ")' href='javascript:void(0)'>" + $(this).find("PaymentId").text() + "</a>&nbsp;<span style='color:green'>*</span>");
                            }
                            else {
                                $(".PaymentId", row).html("<a onclick='ViewPaymentDetails(" + $(this).find('PaymentAutoId').text() + ")' href='javascript:void(0)'>" + $(this).find("PaymentId").text() + "</a>");
                            }
                            $(".PaymentDate", row).text($(this).find("PaymentDate").text());
                            $(".PaymentMode", row).text($(this).find("PaymentMethod").text());
                            $(".TotalOrders", row).text($(this).find("Totalordes").text());
                            $(".ReceivedEmpName", row).text($(this).find("EmpName").text());
                            $(".TotalPaidAmount", row).html($(this).find("ReceivedAmount").text());

                            if ($(this).find("Status").text() == "Settled") {
                                $(".Status", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("Status").text() + "</span>");
                            }
                            else {
                                $(".Status", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("Status").text() + "</span>");
                            }
                            Table3Total += parseFloat($(this).find("ReceivedAmount").text());
                            $(".Action", row).html("<a title='Payment Log' href='javascript:void(0)' onclick='ViewPaymentLog(" + $(this).find('PaymentAutoId').text() + ")'><span class='la la-history'></span></a>&nbsp;&nbsp;<a title='View' href='javascript:void(0)' onclick='ViewPaymentDetails(" + $(this).find('PaymentAutoId').text() + ")'><span class='la la-eye'></span></a>&nbsp;&nbsp;<a title='Print' href='javascript:void(0)' onclick='PrintSlip(" + $(this).find('PaymentAutoId').text() + ")'><span class='la la-print'></span></a>");
                            $("#Table3 tbody").append(row);
                            row = $("#Table3 tbody tr:last").clone(true);
                        });
                        $('#Table3Total').text(Table3Total.toFixed(2))
                        c1 = c1 + 1;
                    }
                    $('#Table3Total').text(Table3Total.toFixed(2))
                } else {
                    location.href = '/';
                }
                var pager = $(xmldoc).find("Table");
                $("#PayHis").ASPSnippets_Pager({
                    ActiveCssClass: "current",
                    PagerCssClass: "pager",
                    PageIndex: parseInt(pager.find("PageIndex").text()),
                    PageSize: parseInt(pager.find("PageSize").text()),
                    RecordCount: parseInt(pager.find("RecordCount").text())
                });
            },
            error: function (result) {
            },
            failure: function (result) {
            }
        });
    }
}
function ViewPaymentLog(PaymentAutoId) {
    $("#modalPaymentLog").modal('show');
    $.ajax({
        type: "POST",
        url: "ViewCustomerDetails.aspx/CustomerPaymentLog",
        data: "{'PaymentAutoId':" + PaymentAutoId + "}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var customerDocument = $(xmldoc).find('Table');
                $("#tblPaymentLog tbody tr").remove();
                var row = $("#tblPaymentLog thead tr").clone(true);
                if (customerDocument.length > 0) {
                    $.each(customerDocument, function (index) {
                        $(".SrNo", row).html((Number(index) + 1));
                        $(".PaymentId", row).html($(this).find("PaymentId").text());
                        $(".PaymentDate", row).html($(this).find("PaymentDate").text());
                        $(".EmpName", row).html($(this).find("EmpName").text());
                        $(".PaymentRemarks", row).html($(this).find("PaymentRemarks").text());
                        $("#tblPaymentLog tbody").append(row);
                        row = $("#tblPaymentLog tbody tr:last").clone(true);
                    });
                    $("#PaymentLogEmptyTable").hide();
                    $("#tblPaymentLog").show();
                }
                else {
                    $("#PaymentLogEmptyTable").show();
                    $("#tblPaymentLog").hide();
                }
            } else {
                location.href = '/';
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function ViewPaymentDetails(PaymentAutoId) {
    $('#PaymentAutoId').val(PaymentAutoId);
    $('#ReceivedAmount').prop('disabled', true);
    $('#PaymentMethod').prop('disabled', true);
    $('#ReferenceNo').prop('disabled', true);
    $('#PaymentRemarks').prop('disabled', true);
    $('#EmpName1').prop('disabled', true);
    $('#EmployeeRemarks').prop('disabled', true);
    $('#editPayment').show();
    $('#UpdatePayment').hide();
    $("#ReceivedAmount").css('border-color', '#ccc');
    $('#btnCancelpayment').hide();
    $('#Td2').text('0.00');
    $('#Td4').text('0.00');
    $('#Td5').text('0.00');
    $('#Td6').text('0.00');
    $('#Td7').text('0.00');
    $('#tblduePayment .OrderAmount').hide();
    $('#tblduePayment .PaidAmount').hide();
    $('#tblduePayment .DueAmount').hide();
    $('#tblduePayment .CheckBoc').hide();
    $('#tblduePayment .PayAmount').hide();
    $('#tblduePayment .payDueAmount').hide();
    $('#tblduePayment .Amount').show();
    $('#Td1').show();
    $('#Td2').hide();
    $('#Td3').hide();
    $('#Td4').hide();
    $('#Td5').hide();
    $('#Td6').hide();
    $('#Td7').hide();
    $('#Td8').hide();
    $.ajax({
        type: "POST",
        url: "ViewCustomerDetails.aspx/CustomerPaymentDetails",
        data: "{'PaymentAutoId':'" + PaymentAutoId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var PaymentDetails = $(xmldoc).find('Table');
                var PaymentDDL = $(xmldoc).find('Table3');

                $("#ddlPaymentMenthod option:not(:first)").remove();
                $.each(PaymentDDL, function () {
                    $("#PaymentMethod").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("PaymentMode").text() + "</option>");
                });

                $("#PaymentId").val($(PaymentDetails).find('PaymentId').text());
                $("#PaymentDate").val($(PaymentDetails).find('PaymentDate').text());
                $("#ReceivedAmount").val($(PaymentDetails).find('ReceivedAmount').text());
                $("#ReferenceNo").val($(PaymentDetails).find('ReferenceNo').text());
                $("#PaymentMethod").val($(PaymentDetails).find('PaymentMethod').text());
                $("#PaymentRemarks").val($(PaymentDetails).find('PaymentRemarks').text());
                $("#EmployeeRemarks").val($(PaymentDetails).find('EmployeeRemarks').text());
                $("#EmpName").val($(PaymentDetails).find('EmpName').text());
                $("#EmpName1").val($(PaymentDetails).find('ProcessedBy').text());
                $("#txtCreditStore").val($(PaymentDetails).find('CreditAmount').text());
                $("#txtChequeNo").val($(PaymentDetails).find('ChequeNo').text());
                $("#txtChequeDate").val($(PaymentDetails).find('ChequeDate').text());
                $("#txtPaymentHistorySortAmount").val($(PaymentDetails).find('SortAmount').text());
                $("#txtPaymentHistoryTotalCurrencyAmount").val($(PaymentDetails).find('TotalCurrencyAmount').text());
                $('#StatusName').html(' ( ' + $(PaymentDetails).find('StatusName').text() + ' )');
                ;
                if ($(PaymentDetails).find('Status').text() == '1') {

                }
                if ($(PaymentDetails).find('PaymentMethod').text() == '1') {
                    $(".hideCurrency").show();
                }
                else {
                    $(".hideCurrency").hide();
                }
                $("#txtStoreCreditTotal").val($(PaymentDetails).find('TotalCreditAmount').text());
                var TotalCreditAmount = parseFloat($(PaymentDetails).find('TotalCreditAmount').text()) - parseFloat($(PaymentDetails).find('CreditAmount').text());
                $("#txtStoreCreditTotal").attr('TotalAmount', TotalCreditAmount.toFixed(2));
                $("#EmpName1").prop('disabled', true);
                var customerOrderList = $(xmldoc).find('Table1');
                $("#tblduePayment tbody tr").remove();
                var row = $("#tblduePayment thead tr").clone(true);
                if (customerOrderList.length > 0) {
                    var Td1 = 0.00;
                    $.each(customerOrderList, function (index) {
                        $(".SrNo", row).text(Number(index) + 1);
                        $(".OrderDate", row).text($(this).find("OrderDate").text());
                        $(".OrderType", row).text($(this).find("OrderType").text());
                        $(".OrderNo", row).text($(this).find("OrderNo").text());
                        $(".Amount", row).html($(this).find("ReceivedAmount").text());
                        Td1 += parseFloat($(this).find("ReceivedAmount").text());
                        $("#tblduePayment tbody").append(row);
                        row = $("#tblduePayment tbody tr:last").clone(true);
                    });
                    $("#Td1").text(Td1.toFixed(2));
                }

                var PaymentHistoryCurrencyList = $(xmldoc).find('Table2');
                var Total = 0.00;
                $("#tblPaymentHistoryCurrencyList tbody tr").remove();
                var row = $("#tblPaymentHistoryCurrencyList thead tr").clone(true);
                if (PaymentHistoryCurrencyList.length > 0) {

                    $.each(PaymentHistoryCurrencyList, function (index) {
                        $(".CurrencyName", row).html($(this).find("CurrencyName").text());
                        $(".NoofRupee", row).html("<input type='Text' readonly='true' maxlength='6' value='" + $(this).find("NoofValue").text() + "' class='form-control input-sm text-center' onkeypress='return isNumberKey(event)' onchange='PaymentCal(this)' onfocus='this.select()' />");
                        $(".Total", row).text($(this).find("TotalAmount").text() || 0.00);
                        $("#tblPaymentHistoryCurrencyList tbody").append(row);
                        row = $("#tblPaymentHistoryCurrencyList tbody tr:last").clone(true);
                        Total = parseFloat(Total) + parseFloat($(this).find("TotalAmount").text());
                    });
                }
                $("#PaymentHistpryTotalAmount").text(parseFloat(Total).toFixed(2));

                if ($('#hiddenEmpTypeVal').val() == 6) {
                    $("#editPayment").show();
                    if ($(PaymentDetails).find('Status').text() == 1) {
                        $("#editPayment").hide();
                        $("#cancelbtn").hide();
                    }
                } else {
                    $("#editPayment").hide();
                }


            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
    $("#CustomerDueAmount").modal('show');
}
function CheckPaymentEdit() {
    $.ajax({
        type: "POST",
        url: "ViewCustomerDetails.aspx/CheckPaymentEdit",
        data: "{'PaymentAutoId':'" + $('#PaymentAutoId').val() + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                console.log(response.d);
                if (response.d == "Valid") {
                    Paymentedit();
                }
                else if (response.d == "Not Valid") {
                    SecurityPoPPaymentEdit();
                }
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function Paymentedit() {
    $('#editPayment').hide();
    $('#UpdatePayment').show();
    $('#btnCancelpayment').show();
    $("#ReceivedAmount").css('border-color', '#ccc');
    $.ajax({
        type: "POST",
        url: "ViewCustomerDetails.aspx/EditPaymentDetails",
        data: "{'PaymentAutoId':'" + $('#PaymentAutoId').val() + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var PaymentDetails = $(xmldoc).find('Table');
                if (PaymentDetails.length > 0) {
                    var TotalOrderAmount = 0.00, TotalPaidAmount = 0.00, DueAmount = 0.00, PayAmount = 0.00, AmtDue = 0.00;
                    $("#tblduePayment tbody tr").remove();
                    var row = $("#tblduePayment thead tr").clone();
                    $.each(PaymentDetails, function (index) {
                        $('.SrNo', row).html(Number(index) + 1)
                        $(".OrderNo", row).html("<span OrderAutoId='" + $(this).find('OrderAutoId').text() + "'></span>" + $(this).find("OrderNo").text());
                        $(".OrderDate", row).text($(this).find("OrderDate").text());
                        $(".OrderType", row).text($(this).find("OrderType").text());
                        $(".OrderAmount", row).text($(this).find("PayableAmount").text());
                        TotalOrderAmount += parseFloat($(this).find("PayableAmount").text());
                        if ($(this).find('Pay').text() == 0) {
                            $(".CheckBoc", row).html("<input type='checkbox' onclick='EditfunCheckBoc(this)' checked />");
                            $(".PaidAmount", row).text(parseFloat(parseFloat($(this).find("AmtPaid").text()) - parseFloat($(this).find("ReceivedAmount").text())).toFixed(2));
                            TotalPaidAmount += parseFloat(parseFloat(parseFloat($(this).find("AmtPaid").text()) - parseFloat($(this).find("ReceivedAmount").text())).toFixed(2));
                            var MainDueAmount = parseFloat($(this).find("AmtDue").text()) + parseFloat($(this).find("ReceivedAmount").text());
                            $(".DueAmount", row).text(parseFloat(MainDueAmount).toFixed(2));
                            $(".PayAmount", row).text($(this).find("ReceivedAmount").text());
                            $(".payDueAmount", row).text($(this).find("AmtDue").text());
                            DueAmount += parseFloat(MainDueAmount);
                            PayAmount += parseFloat($(this).find("ReceivedAmount").text());
                            AmtDue += parseFloat($(this).find("AmtDue").text());

                        } else {
                            $(".PaidAmount", row).text($(this).find("AmtPaid").text());
                            TotalPaidAmount += parseFloat($(this).find("AmtPaid").text());
                            $(".CheckBoc", row).html("<input type='checkbox' onclick='EditfunCheckBoc(this)' />");
                            $(".DueAmount", row).text($(this).find("AmtDue").text());
                            $(".PayAmount", row).text('0.00');
                            $(".payDueAmount", row).text('0.00');
                            DueAmount += parseFloat($(this).find("AmtDue").text());
                        }

                        $("#tblduePayment tbody").append(row);
                        row = $("#tblduePayment tbody tr:last").clone(true);
                    });
                    $('#Td2').text(parseFloat(TotalOrderAmount).toFixed(2));
                    $('#Td4').text(parseFloat(TotalPaidAmount).toFixed(2));
                    $('#Td5').text(parseFloat(DueAmount).toFixed(2));
                    $('#Td6').text(parseFloat(PayAmount).toFixed(2));
                    $('#Td7').text(parseFloat(AmtDue).toFixed(2));
                    ;
                    var ReceivedAmount = $('#ReceivedAmount').val();
                    if ($('#PaymentMethod').val() == 5) {
                        $('#ReceivedAmount').prop('disabled', false);
                        $('#EmployeeRemarks').prop('disabled', false);
                        $('#txtCreditStore').val('0.00');//remark
                        $('#txtStoreCreditTotal').attr('totalamount', (parseFloat($('#txtStoreCreditTotal').val()) + parseFloat(PayAmount)));
                        $('#txtStoreCreditTotal').val((parseFloat($('#txtStoreCreditTotal').val()) + parseFloat(PayAmount)).toFixed(2));
                    } else if (parseFloat(PayAmount) < ReceivedAmount) {
                        $('#txtCreditStore').val((parseFloat(ReceivedAmount) - parseFloat(PayAmount) - parseFloat($('#txtPaymentHistorySortAmount').val())).toFixed(2));
                        $('#txtStoreCreditTotal').attr('totalamount', (parseFloat($('#txtStoreCreditTotal').val())));// - parseFloat($('#txtCreditStore').val())));
                        $('#txtStoreCreditTotal').val((parseFloat($('#txtStoreCreditTotal').val())));// - parseFloat($('#txtCreditStore').val())).toFixed(2));
                    }

                    $('#tblduePayment .OrderAmount').show();
                    $('#tblduePayment .PaidAmount').show();
                    $('#tblduePayment .DueAmount').show();
                    $('#tblduePayment .CheckBoc').show();
                    $('#tblduePayment .PayAmount').show();
                    $('#tblduePayment .payDueAmount').show();
                    $('#tblduePayment .Amount').hide();
                    $('#Td1').hide();
                    $('#Td2').show();
                    $('#Td3').show();
                    $('#Td4').show();
                    $('#Td5').show();
                    $('#Td6').show();
                    $('#Td7').show();
                    $('#Td8').show();
                }
                var PaymentHistoryCurrencyList = $(xmldoc).find('Table1');
                var Total = 0.00;
                $("#tblPaymentHistoryCurrencyList tbody tr").remove();
                var row = $("#tblPaymentHistoryCurrencyList thead tr").clone(true);
                if (PaymentHistoryCurrencyList.length > 0) {
                    $.each(PaymentHistoryCurrencyList, function (index) {
                        $(".CurrencyName", row).html("<span CurrencyValue='" + $(this).find('CurrencyValue').text() + "'  CurrencyAutoid='" + $(this).find('AutoId').text() + "'></span>" + $(this).find("CurrencyName").text());
                        $(".NoofRupee", row).html("<input type='Text' maxlength='6'  value='" + $(this).find("NoofValue").text() + "' class='form-control input-sm text-center' onkeypress='return isNumberKey(event)' onchange='PaymentHostoryPaymentCal(this)' onfocus='this.select()' />");
                        $(".Total", row).text($(this).find("TotalAmount").text() || 0.00);
                        $("#tblPaymentHistoryCurrencyList tbody").append(row);
                        row = $("#tblPaymentHistoryCurrencyList tbody tr:last").clone(true);
                        Total = parseFloat(Total) + parseFloat($(this).find("TotalAmount").text());
                    });
                }
                $("#PaymentHistpryTotalAmount").text(parseFloat(Total).toFixed(2));

            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function PaymentHostoryPaymentCal(e) {
    var row = $(e).closest('tr');
    var TotalAmount = 0;
    var Total = 0;
    var Currencyvalue = $(row).find(".CurrencyName span").attr('CurrencyValue');
    var noofRupee = 0;
    if ($(row).find(".NoofRupee input").val() != '') {
        noofRupee = $(row).find(".NoofRupee input").val();
    }

    Total = parseFloat(parseFloat(Currencyvalue) * parseFloat(noofRupee)).toFixed(2);
    $(row).find(".Total").text(Total);
    var TotalCurrencyAmount = 0;
    if ($("#txtPaymentHistoryTotalCurrencyAmount").val() != "") {
        TotalCurrencyAmount = $("#txtPaymentHistoryTotalCurrencyAmount").val();
    }
    var SortAmount = 0
    if ($("#txtPaymentHistorySortAmount").val() != "") {
        SortAmount = $("#txtPaymentHistorySortAmount").val();
    }


    $("#tblPaymentHistoryCurrencyList tbody tr").each(function () {
        TotalAmount += parseFloat($(this).find('.Total').text());
    });
    ;
    // Start Change
    var ReceiveAmount = $("#ReceivedAmount").val();

    if (TotalAmount > parseFloat(ReceiveAmount)) {
        $(row).find(".NoofRupee input").val('');
        $(row).find(".Total").text('0.00');
        $(row).find(".NoofRupee input").focus();
        $(row).find(".NoofRupee input").addClass('border-warning');

    }
    TotalAmount = 0;
    $("#tblPaymentHistoryCurrencyList tbody tr").each(function () {
        TotalAmount += parseFloat($(this).find('.Total').text());
    });

    $("#PaymentHistpryTotalAmount").text(parseFloat(TotalAmount).toFixed(2));
    $("#txtPaymentHistorySortAmount").val(parseFloat(parseFloat($("#ReceivedAmount").val()) - TotalAmount).toFixed(2));
    $("#txtPaymentHistoryTotalCurrencyAmount").val(parseFloat(TotalAmount).toFixed(2));

    $("#tblduePayment tbody tr .CheckBoc input").prop('checked', false);
    $('#Table4 tbody tr .PayAmount').text('0.00');
    $('#Table4 tbody tr .payDueAmount').text('0.00');
    $('#Td6').text('0.00');
    $('#Td7').text('0.00');
    //End 24-08-2019 By Rizwan Ahmad


}

function PaymentHistorySortAmountCal() {

    var TotalCurrencyAmount = 0;
    var SortAmount = 0
    if ($("#txtPaymentHistoryTotalCurrencyAmount").val() != "") {
        TotalCurrencyAmount = $("#txtPaymentHistoryTotalCurrencyAmount").val();
    }
    if ($("#txtPaymentHistorySortAmount").val() != "") {
        SortAmount = $("#txtPaymentHistorySortAmount").val();
    }
    var TotalAmount = 0;
    if ($("#PaymentHistpryTotalAmount").text() != '') {
        TotalAmount = $("#PaymentHistpryTotalAmount").text();
    }

    TotalCurrencyAmount = parseFloat(TotalAmount) + parseFloat(SortAmount)
    $("#txtPaymentHistoryTotalCurrencyAmount").val(parseFloat(TotalCurrencyAmount).toFixed(2));

}

function Pagevalue(e) {
    var divid = $(e).parent().attr('id');
    if (divid == 'CustLog') {
        c4 = 0;
        CustomerLogReport(parseInt($(e).attr("page")));
    }
    else if (divid == 'CollDet') {
        c3 = 0;
        CollectionDetails(parseInt($(e).attr("page")));
    }
    else if (divid == 'OrdL') {
        CustomerOrderList(parseInt($(e).attr("page")));
    }
    else if (divid == 'CRList') {
        CustomerCreditMemoList(parseInt($(e).attr("page")));
    }
    else {
        c1 = 0;
        CustomerPaymentList(parseInt($(e).attr("page")));
    }



};
function PagingCustomerLogReport(pageIndex) {
    c4 = 0;
    CustomerLogReport(1);
}
function CustomerLogReportSortInOrder(SortInOrder) {
    c4 = 0;
    $("#DateSort").val(SortInOrder);
    CustomerLogReport(1);
}
function CustomerLogReport(pageIndex) {
    if (c4 == 0) {
        var DateSortIn = $("#DateSort").val();
        if (DateSortIn == 1) {
            $(".la-arrow-down").hide();
            $(".la-arrow-up").show();
        } else if (DateSortIn == 2) {
            $(".la-arrow-down").show();
            $(".la-arrow-up").hide();
        }
        var data = {
            CustomerAutoId: AutoId,
            DateSortIn: DateSortIn,
            PageIndex: pageIndex,
            PageSize: $("#ddlPageSizeOrdLog").val(),
        }
        $.ajax({
            type: "POST",
            url: "ViewCustomerDetails.aspx/CustomerLogReport",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json;charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d != "Session Expired") {

                    var xmldoc = $.parseXML(response.d);
                    var customerLOG = $(xmldoc).find('Table1');
                    $("#tblOrderLog tbody tr").remove();
                    var row = $("#tblOrderLog thead tr").clone(true);
                    if (customerLOG.length > 0) {
                        $("#EmptyTable").hide();
                        $.each(customerLOG, function (index) {
                            $(".SrNo", row).text(index + 1);
                            $(".ActionBy", row).text($(this).find("EmpName").text());
                            $(".Date", row).text($(this).find("ActionDate").text());
                            $(".Action", row).text($(this).find("Action").text());
                            $(".Remark", row).text($(this).find("Remarks").text());

                            $("#tblOrderLog tbody").append(row);
                            row = $("#tblOrderLog tbody tr:last").clone(true);
                        });
                    }

                    var pager = $(xmldoc).find("Table");
                    $("#CustLog").ASPSnippets_Pager({
                        ActiveCssClass: "current",
                        PagerCssClass: "pager",
                        PageIndex: parseInt(pager.find("PageIndex").text()),
                        PageSize: parseInt(pager.find("PageSize").text()),
                        RecordCount: parseInt(pager.find("RecordCount").text())
                    });
                } else {
                    location.href = '/';
                }
                c4 = c4 + 1;
            },
            error: function (result) {

            },
            failure: function (result) {
            }
        });
    }
}
function EditfunCheckBoc(e) {
    var ReceiveAmount = 0.00;
    var PaidAmount = 0.00;
    if ($("#PaymentMethod").val() == '1') {
        if ($("#PaymentHistpryTotalAmount").text() == "0.00" || $("#PaymentHistpryTotalAmount").text() == "") {
            swal({
                text: "Please fill currency denomination.",
                allowOutsideClick: false,
                closeOnClickOutside: false,
                icon: "warning",
                buttons: {
                    confirm: {
                        text: "Ok",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: true
                    }
                }
            }).then(function (ok) {
                if (ok) {
                    $(".NoofRupee input").addClass("border-warning");
                    $(".NoofRupee input").focus();
                    return;
                }
            })
        }
        ReceiveAmount = $("#txtPaymentHistoryTotalCurrencyAmount").val();
    }
    else {
        if ($("#ReceivedAmount").val() != "") {
            ReceiveAmount = $("#ReceivedAmount").val();
        }
    }
    var tr = $(e).closest('tr');
    var DueAmount = 0.00;
    DueAmount = parseFloat(tr.find('.DueAmount').text());
    if ($(e).prop('checked') == true) {
        if (parseFloat(ReceiveAmount) > 0) {
            var PayAmount = 0.00;
            $("#tblduePayment tbody tr").each(function () {
                PayAmount += parseFloat($(this).find('.PayAmount').text());
            });
            ;
            var Amount = parseFloat(ReceiveAmount) - parseFloat(PayAmount);
            if (parseFloat(DueAmount) > parseFloat(Amount)) {
                if (parseFloat(Amount) == 0) {
                    tr.find('.PayAmount').text('0.00');
                    tr.find('.payDueAmount').text('0.00');
                    $(e).prop('checked', false);
                } else {
                    tr.find('.PayAmount').text(Amount.toFixed(2));
                    tr.find('.payDueAmount').text((parseFloat(DueAmount) - parseFloat(Amount)).toFixed(2));
                }

            }
            else {
                PaidAmount = parseFloat(DueAmount) - parseFloat(Amount);
                tr.find('.PayAmount').text(DueAmount.toFixed(2));
                tr.find('.payDueAmount').text('0.00');
            }
        } else {
            $("#ReceiveAmount").focus();
            $(e).prop('checked', false);
            $("#ReceiveAmount").css('border-color', 'Red');
        }
    } else {
        tr.find('.PayAmount').text('0.00');
        tr.find('.payDueAmount').text('0.00');
    }
    var PayAmount = 0.00, payDueAmount = 0.00;
    $("#tblduePayment tbody tr").each(function () {
        if ($(this).find('.CheckBoc input').prop('checked') == true) {
            PayAmount += parseFloat($(this).find('.PayAmount').text());
            payDueAmount += parseFloat($(this).find('.payDueAmount').text());
        }
    });
    $('#Td6').text(PayAmount.toFixed(2));
    $('#Td7').text(payDueAmount.toFixed(2));
    // Start Change

    if ($("#PaymentMethod").val() == 5) {
        var a = $("#txtStoreCreditTotal").attr('totalamount');
        var b = $("#ReceivedAmount").val();
        $('#txtStoreCreditTotal').val(parseFloat(a) - parseFloat(b));
    }
    else {
        $('#txtCreditStore').val((parseFloat(ReceiveAmount) - parseFloat(PayAmount)).toFixed(2));
        var TotalStoreCredit = $('#txtStoreCreditTotal').attr('TotalAmount');
        //var TOTAL = parseFloat(TotalStoreCredit) + parseFloat((parseFloat(ReceiveAmount) - parseFloat(PayAmount)));
        var TOTAL = parseFloat(TotalStoreCredit) - parseFloat(parseFloat(PayAmount));

        $('#txtStoreCreditTotal').val(parseFloat(TOTAL).toFixed(2));
    }
    //End 24-08-2019 By Rizwan Ahmad
}
function EditReceivedPay() {
    $("#ReceivedAmount").css('border-color', '#ccc');
    $("#tblduePayment tbody tr .CheckBoc input").prop('checked', false);
    $('#tblduePayment tbody tr .PayAmount').text('0.00');
    $('#tblduePayment tbody tr .payDueAmount').text('0.00');
    $('#Td6').text('0.00')
    $('#Td7').text('0.00')

    $("#ReceivedAmount").css('border-color', '#ccc');
    var ReceiveAmount = 0.00;

    if ($('#PaymentMethod').val() == 5) {
        var StoreCredit = parseFloat($('#txtStoreCreditTotal').attr('totalamount'));
        if ($("#ReceivedAmount").val() != '') {
            ReceiveAmount = parseFloat($("#ReceivedAmount").val());
        }

        if (parseFloat(StoreCredit) < ReceiveAmount) {
            $("#ReceivedAmount").css('border-color', 'red')
        }
        $('#txtCreditStore').val('0.00');
    } else {
        if ($('#ReceivedAmount').val() != "") {
            ReceiveAmount = parseFloat($('#ReceivedAmount').val());
        }
        $('#txtCreditStore').val((ReceiveAmount).toFixed(2));
    }


    var CusCreditStore = 0.00;
    if ($('#txtCreditStore').val() != "") {
        CusCreditStore = parseFloat($('#txtCreditStore').val());
    }

    StoreCredit = parseFloat($('#txtStoreCreditTotal').attr('TotalAmount'));
    $('#txtStoreCreditTotal').val((parseFloat(StoreCredit) + parseFloat(CusCreditStore)).toFixed(2));


}
function CancelPayment() {

    ViewPaymentDetails($('#PaymentAutoId').val());
}

function PaymentUpdate() {
    if (dynamicALL('Update')) {
        var PaymentCurrencyXml = '';
        var TotalCurrencyAmount = 0;
        var SortAmount = 0, curTotal = 0;

        if ($("#txtPaymentHistorySortAmount").val() != '') {
            SortAmount = $("#txtPaymentHistorySortAmount").val();
        }
        if ((parseInt($('#PaymentMethod').val()) == 1)) {


            $("#tblPaymentHistoryCurrencyList tbody tr").each(function () {

                if ($(this).find(".NoofRupee input").val() != 0 && $(this).find(".NoofRupee input").val() != '') {
                    PaymentCurrencyXml += '<PaymentCurrencyXml>';
                    PaymentCurrencyXml += '<CurrencyAutoId><![CDATA[' + $(this).find('.CurrencyName span').attr('CurrencyAutoid') + ']]></CurrencyAutoId>';
                    PaymentCurrencyXml += '<NoOfValue><![CDATA[' + $(this).find(".NoofRupee input").val() + ']]></NoOfValue>';
                    PaymentCurrencyXml += '<TotalAmount><![CDATA[' + $(this).find(".Total").text() + ']]></TotalAmount>';
                    PaymentCurrencyXml += '</PaymentCurrencyXml>';
                    curTotal += parseFloat($(this).find(".CurrencyName span").attr("currencyvalue")) * parseFloat($(this).find(".NoofRupee input").val());
                }
            });
        }
        var TotalCurrencyAmount = 0;

        if ($("#txtPaymentHistoryTotalCurrencyAmount").val() != "") {
            TotalCurrencyAmount = $("#txtPaymentHistoryTotalCurrencyAmount").val();
        }

        var data = {
            PaymentAutoId: $('#PaymentAutoId').val(),
            CustomerAutoId: AutoId,
            ReceivedAmount: $("#ReceivedAmount").val(),
            PaymentMethod: $("#PaymentMethod").val(),
            ReferenceNo: $("#ReferenceNo").val(),
            PaymentRemarks: $("#PaymentRemarks").val(),
            ReceivedPaymentBy: $("#EmpName1").val(),
            EmployeeRemarks: $("#EmployeeRemarks").val(),
            CreditStore: $("#txtCreditStore").val(),
            SortAmount: SortAmount,
            TotalCurrencyAmount: TotalCurrencyAmount,
            PaymentCurrencyXml: PaymentCurrencyXml
        }
        var ReceivedAmount = $("#ReceivedAmount").val();
        var ListReceivedAmount = 0.00;
        var tblReceivedOrder = [];
        $("#tblduePayment tbody tr").each(function () {
            if ($(this).find('.CheckBoc input').prop('checked') == true) {
                tblReceivedOrder.push({
                    OrderAutoId: $(this).find('.OrderNo span').attr('OrderAutoId'),
                    PayAmount: parseFloat($(this).find('.PayAmount').text())
                });
                ListReceivedAmount += parseFloat($(this).find('.PayAmount').text());
            }
        });
        var StoreCreditTotal = parseFloat($('#txtStoreCreditTotal').attr('totalamount'));

        if ((parseInt($('#PaymentMethod').val()) == 5) && (parseFloat(ReceivedAmount) != parseFloat(ListReceivedAmount))) {

            toastr.error('Received amount and Order amount should be equal.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

        } else if ((parseInt($('#PaymentMethod').val()) == 5) && (parseFloat(ReceivedAmount) > parseFloat($('#txtStoreCreditTotal').attr('totalamount')))) {
            toastr.error('Received amount should be equal or less than Total Store Credit.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

        }
        else if (ReceivedAmount == 0) {
            toastr.error('Received amount should be greater than zero.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        }
        else if ((parseFloat(ListReceivedAmount) > parseFloat(TotalCurrencyAmount)) && (parseInt($('#PaymentMethod').val()) == '1')) {
            toastr.error('Settlement amount should be equal to received amount.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        }

        else if ((parseFloat(curTotal) != parseFloat(TotalCurrencyAmount)) && (parseInt($('#ddlPaymentMenthod').val()) == 1)) {
            toastr.error('Total amount should be equal to received amount.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        }
        else {

            if (ListReceivedAmount < ReceivedAmount) {
                swal({
                    title: "Are you sure?",
                    text: "You want to update payment with Store Credit.",
                    icon: "warning",
                    showCancelButton: true,
                    allowOutsideClick: false,
                    closeOnClickOutside: false,
                    buttons: {
                        cancel: {
                            text: "No, cancel!",
                            value: null,
                            visible: true,
                            className: "btn-warning",
                            closeModal: true,
                        },
                        confirm: {
                            text: "Yes, Receive it!",
                            value: true,
                            visible: true,
                            className: "",
                            closeModal: false
                        }
                    }
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        updatePay(tblReceivedOrder, data);
                    }
                })

            } else {
                updatePay(tblReceivedOrder, data);
            }

        }
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}

function updatePay(tblReceivedOrder, data) {
    $.ajax({
        type: "Post",
        url: "ViewCustomerDetails.aspx/UpdatePayment",
        data: JSON.stringify({ TableValues: JSON.stringify(tblReceivedOrder), dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (data) {
            if (data.d == 'Session Expired') {
                location.href = '/';
            } else if (data.d == 'true') {
                swal("", "Payment details updated successfully.", "success").then(function () {
                    location.reload();
                });
            } else {
                swal("Error", "Oops! Something went wrong.Please try later.", "error");
            }
        },
        error: function (result) {
        },
        failure: function (result) {
        }
    });
}

$('#PaymentMethod').change(function () {
    $("#ReceivedAmount").css('border-color', '#ccc');
    $("#ReceivedAmount").val('0.00');
    EditReceivedPay();

});
function uploaddocument() {
    $('#txtDocumentName').val('');
    $('#uploadedFile').val('');
    $('#Button3').hide();
    $('#Button2').show();
    $("#ahrefViewDocument").hide();
    $('#PopCustomerDocuments').modal('show');
    $("#Button2").attr('disabled', false);
}

function DocumentSave() { //using ValidationGroup document
    if (dynamicALL('document1')) {
        var ext = $('#uploadedFile').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg', 'doc', 'pdf', 'jfif']) == -1) {
            toastr.error('Invalid file.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            return;
        }
        var fileUpload = $("#uploadedFile").get(0);
        var files = fileUpload.files;
        var test = new FormData();
        for (var i = 0; i < files.length; i++) {
            test.append(files[i].name, files[i]);
        }
        $("#Button2").attr('disabled', true);
        $.ajax({
            url: "/UploadDocument.ashx?Id=" + $("#txtCustomerId").html(),
            type: "POST",
            contentType: false,
            processData: false,
            data: test,
            success: function (resultupload) {
                if (resultupload != '' || resultupload != null) {
                    var data = {
                        CustomerAutoId: AutoId,
                        DocumentName: $('#ddlDocumentName').val(),
                        DocumentURL: resultupload
                    }

                    if ($('#txtDocumentName').val() == "") {
                        return;
                    }
                    $.ajax({
                        type: "Post",
                        url: "ViewCustomerDetails.aspx/DocumentSave",
                        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function () {

                            $.blockUI({
                                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                                overlayCSS: {
                                    backgroundColor: '#FFF',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'transparent'
                                }
                            });
                        },
                        complete: function () {
                            $.unblockUI();
                            $("#Button2").attr('disabled', false);
                        },
                        success: function (data) {
                            if (data.d == 'Session Expired') {
                                location.href = '/';
                            } else if (data.d == 'true') {
                                $('#txtDocumentName').val('');
                                $('#uploadedFile').val('');
                                swal("", "Document details saved successfully.", "success");
                                $('#PopCustomerDocuments').modal('hide');
                                c5 = 0;
                                CustomerDocument();
                                clearBorder();
                            } else {
                                swal("Error !", "Oops! Something went wrong.Please try later.", "error");
                                $("#Button2").attr('disabled', false);
                            }
                        },
                        error: function (result) {
                        },
                        failure: function (result) {
                        }
                    });
                }
            },
            error: function (err) {
                console.log(err);
            }
        });

    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
function CustomerDocument() {
    if (c5 == 0) {
        var data = {
            CustomerAutoId: AutoId
        }
        $.ajax({
            type: "POST",
            url: "ViewCustomerDetails.aspx/CustomerDocument",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json;charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d != "Session Expired") {
                    var xmldoc = $.parseXML(response.d);
                    var customerDocument = $(xmldoc).find('Table');
                    $("#tblCustomerDocuments tbody tr").remove();
                    var row = $("#tblCustomerDocuments thead tr").clone(true);
                    if (customerDocument.length > 0) {
                        $.each(customerDocument, function (index) {
                            $(".SrNo", row).html((Number(index) + 1) + "<span FileAutoId=" + $(this).find("FileAutoId").text() + "></span>");
                            $(".Document", row).html("<a title='Download' href='javascript:void(0)' onclick='downloadfile(\"" + $(this).find("DocumentURL").text() + "\")'><span class='la la-download'></span></a>&nbsp;&nbsp;<a title='View' target='_default'   href='/UploadDocument/" + $(this).find("DocumentURL").text() + "'><span class='la la-eye'></span></a>");
                            $(".ViewDownload", row).html("<a title='Edit' href='javascript:void(0)' onclick='Editdownloadfile(this)'><span class='la la-edit'><span></a>&nbsp;&nbsp;<a title='Delete' href='javascript:void(0)' onclick='deleterecordDocument(\"" + $(this).find("FileAutoId").text() + "\")'><span class='la la-remove'></span></a>");
                            $(".DocumentName", row).text($(this).find("DocumentName").text());
                            $("#tblCustomerDocuments tbody").append(row);
                            row = $("#tblCustomerDocuments tbody tr:last").clone(true);
                        });
                    }
                } else {
                    location.href = '/';
                }
                c5 = c5 + 1;
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
}
function downloadfile(string) {
    var a = document.createElement('a');
    var url = '/UploadDocument/' + string;
    a.href = url;
    a.download = string;
    a.click();
    window.URL.revokeObjectURL(url);
}
function Editdownloadfile(e) {
    var tr = $(e).closest('tr');
    $('#hdnuploadAutoId').val(tr.find('.SrNo span').attr('FileAutoId'));
    $('#ddlDocumentName').val(tr.find('.DocumentName').text());
    $('#PopCustomerDocuments').modal('show');
    if (tr.find('.Document a:last-child').attr('href') != "") {
        $("#ahrefViewDocument").show();
        $("#ahrefViewDocument").attr('href', tr.find('.Document a:last-child').attr('href'))
    }
    else {
        $("#ahrefViewDocument").hide();
    }
    $("#Button2").attr('disabled', false);
    $('#Button2').hide();
    $('#Button3').show();
}
function UpdateDocument() {
    if (dynamicALL('documents')) {
        var ext = $('#uploadedFile').val().split('.').pop().toLowerCase();
        if (ext != "") {
            if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg', 'doc', 'pdf', 'jfif']) == -1) {
                toastr.error('Invalid file.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                return;
            }
        }
        //ENd
        var fileUpload = $("#uploadedFile").get(0);
        var files = fileUpload.files;
        var test = new FormData();
        for (var i = 0; i < files.length; i++) {
            test.append(files[i].name, files[i]);
        }
        $.ajax({
            url: "/UploadDocument.ashx?Id=" + $("#txtCustomerId").html(),
            type: "POST",
            contentType: false,
            processData: false,
            data: test,
            success: function (resultupload) {
                if (resultupload != '' || resultupload != null) {
                    var data = {
                        FileAutoId: $('#hdnuploadAutoId').val(),
                        CustomerAutoId: AutoId,
                        DocumentName: $('#ddlDocumentName').val(),
                        DocumentURL: resultupload
                    }
                    $.ajax({
                        type: "Post",
                        url: "ViewCustomerDetails.aspx/UpdateDocument",
                        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function () {
                            $.blockUI({
                                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                                overlayCSS: {
                                    backgroundColor: '#FFF',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'transparent'
                                }
                            });
                        },
                        complete: function () {
                            $.unblockUI();
                        },
                        success: function (data) {
                            if (data.d == 'Session Expired') {
                                location.href = '/';
                            } else if (data.d == 'true') {
                                $('#txtDocumentName').val('');
                                $('#uploadedFile').val('');
                                swal("", "Document details updated successfully", "success");
                                $('#PopCustomerDocuments').modal('hide');
                                c5 = 0;
                                CustomerDocument();
                                clearBorder();
                            } else {
                                swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                            }
                        },
                        error: function (result) {
                        },
                        failure: function (result) {
                        }
                    });
                }
            },
            error: function (err) {
                console.log(err);
            }
        });

    } else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
function deleterecordDocument(FileAutoId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete document!",
        icon: "warning",
        showCancelButton: true,
        allowOutsideClick: false,
        closeOnClickOutside: false,
        buttons: {
            cancel: {
                text: "No, cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, delete it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            DeleteDocument(FileAutoId);
        }
    })
}
function clearBorder() {
    $("#Button2").attr('disabled', false);
    $("#txtDocumentName").removeClass('border-warning');
    $("#uploadedFile").removeClass('border-warning');
    $("#txtSaddress1").removeClass('border-warning');
    $("#txtBaddress1").removeClass('border-warning');
}
function clearBordertwo() {

    $("#txtCheckNo").removeClass('border-warning');
    $("#txtCheckDate").removeClass('border-warning');
}
function clearBorder3() {

    $("#txtPopRemarks").removeClass('border-warning');
    $("#txtBankFeeCharge").removeClass('border-warning');
}

function DeleteDocument(FileAutoId) {
    $.ajax({
        type: "Post",
        url: "ViewCustomerDetails.aspx/DeleteDocument",
        data: "{'FileAutoId':'" + FileAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (data) {
            if (data.d == 'Session Expired') {
                location.href = '/';
            } else if (data.d == 'true') {
                swal("", "Document details deleted successfully", "success");

            } else {
                swal("Error", "Oops! Something went wrong.Please try later.", "error");
            }
            c5 = 0;
            CustomerDocument();
        },
        error: function (result) {
        },
        failure: function (result) {
        }
    });
}
function CollectionDetailsSearch() {
    c3 = 0;
    CollectionDetails(1);

}

function CollectionDetails(pageIndex) {
    if (c3 == 0) {
        var data = {
            CustomerAutoId: AutoId,
            FromDate: $("#FromDate").val(),
            ToDate: $("#ToDate").val(),
            Status: $("#ddlStatus").val(),
            PageIndex: pageIndex,
            PageSize: $("#ddlPageSizeinCollDet").val(),
        }
        $.ajax({
            type: "POST",
            url: "ViewCustomerDetails.aspx/CollectionDetails",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json;charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d != "Session Expired") {
                    var xmldoc = $.parseXML(response.d);
                    var customerOrderList = $(xmldoc).find('Table1');
                    $("#tblCollectionDetails tbody tr").remove();
                    var row = $("#tblCollectionDetails thead tr").clone(true);
                    var Table3Total = 0.00;
                    if (customerOrderList.length > 0) {
                        $.each(customerOrderList, function (index) {
                            $(".PayId", row).html($(this).find("PayId").text());
                            $(".ReceiveDate", row).html($(this).find("ReceivedDate").text() + "<span PaymentAutoId=" + $(this).find('PaymentAutoId').text() + "></span>");
                            $(".PaymentType", row).html($(this).find("PayType").text() + "<span PayCode=" + $(this).find('PayCode').text() + "></span>");
                            $(".ReceivedBy", row).html($(this).find("ReceivedBy").text() + "<span ProcessStatus=" + $(this).find('ProcessStatus').text() + "></span>");
                            $(".PaymentMode", row).html($(this).find("PaymentMode").text() + "<span PaymentMode=" + $(this).find('Pmode').text() + "></span>");
                            $(".ReceivedAmount", row).text($(this).find("ReceivedAmount").text());
                            $(".ReferenceId", row).html($(this).find("ReferenceId").text() + "<span StatusCode=" + $(this).find('StatusCode').text() + "></span>");
                            $(".ChequeDate", row).html($(this).find("ChequeDate").text() + "<span ChequeDate=" + $(this).find('ChequeDate').text() + "></span>");
                            $(".Remarks", row).html($(this).find("Remarks").text());
                            if (Number($(this).find("StatusCode").text()) == 1) {
                                $(".Status", row).html("<span class='badge badge badge-pill badge-info'>" + $(this).find("Status").text() + "</span>");
                            } else if (Number($(this).find("StatusCode").text()) == 2) {
                                $(".Status", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("Status").text() + "</span>");
                            } else if (Number($(this).find("StatusCode").text()) == 3) {
                                $(".Status", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("Status").text() + "</span>");
                            } else if (Number($(this).find("StatusCode").text()) == 4) {
                                $(".Status", row).html("<span class='badge badge badge-pill badge-warning'>" + $(this).find("Status").text() + "</span>");
                            } else if (Number($(this).find("StatusCode").text()) == 5) {
                                $(".Status", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("Status").text() + "</span>");
                            }
                            var html = '';
                            if ($(this).find('Pmode').text() == 2) {
                                if ($(this).find('StatusCode').text() == 2 && $("#hiddenEmpTypeVal").val() == 6) {
                                    html = "<span PaymentAutoId=" + $(this).find("PaymentAutoId").text() + " ChequeNo='" + $(this).find('ChequeNo').text() + "' ChequeDate='" + $(this).find('ChequeDate').text() + "'></span>&nbsp;&nbsp;<a title='Cancel Payment' href='javascript:void(0)' onclick='PaymentCancel(this)'><span class='la la-eye'></span></a>";
                                }
                            }
                            if ($(this).find("PayId").text() == PaymentId && (Number($(this).find("StatusCode").text()) == 1 || Number($(this).find("StatusCode").text()) == 4)) {
                                if (html != '') {
                                    $(".action", row).html(html);
                                } else {
                                    $(".action", row).html("<input type='radio' name='name' onclick='funcheck(this)' checked='checked'/> ");
                                }
                                funcheck(row)
                            }
                            else if (Number($(this).find("StatusCode").text()) == 1 || Number($(this).find("StatusCode").text()) == 4) {
                                if (html != '') {
                                    $(".action", row).html(html);
                                } else {
                                    $(".action", row).html("<input type='radio' name='name' onclick='funcheck(this)' />");
                                }
                            } else {
                                if (html != '') {
                                    $(".action", row).html(html);
                                } else {
                                    $(".action", row).html("<input type='radio' name='name' disabled='disabled'  />" + html);
                                }
                            }
                            if ($("#hiddenEmpTypeVal").val() == 1) {
                                if (Number($(this).find("StatusCode").text()) == 1 || Number($(this).find("StatusCode").text()) == 4) {
                                    $(".action", row).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='radio' name='name' onclick='funcheck(this)'/>" + html + "&nbsp;&nbsp;<a title='Delete payment' href='javascript:;'><span class='ft-x' onclick='deleterecord(\"" + $(this).find("PaymentAutoId").text() + "\")' style='color:red'></span></a>");
                                }
                            }
                            if ($("#hiddenEmpTypeVal").val() == 6) {
                                if (Number($(this).find("StatusCode").text()) == 1 ) {
                                    $(".action", row).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='radio' name='name' onclick='funcheck(this)'/>" + html + "&nbsp;&nbsp;<a title='Delete payment' href='javascript:;'><span class='ft-x' onclick='deleterecord(\"" + $(this).find("PaymentAutoId").text() + "\")' style='color:red'></span></a>");
                                }
                            }
                            $("#tblCollectionDetails tbody").append(row);
                            row = $("#tblCollectionDetails tbody tr:last").clone(true);
                        });
                    }
                    if ($("#hiddenEmpTypeVal").val() != 6) {
                        $("#tblCollectionDetails .cancelled").hide();
                    }
                } else {
                    location.href = '/';
                }
                var pager = $(xmldoc).find("Table");
                $("#CollDet").ASPSnippets_Pager({
                    ActiveCssClass: "current",
                    PagerCssClass: "pager",
                    PageIndex: parseInt(pager.find("PageIndex").text()),
                    PageSize: parseInt(pager.find("PageSize").text()),
                    RecordCount: parseInt(pager.find("RecordCount").text())
                })
                c3 = c3 + 1;
            },
            error: function (result) {
            },
            failure: function (result) {
            }
        });
    }
}

function deleterecord(PaymentAutoId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this payment.",
        icon: "warning",
        showCancelButton: true,
        allowOutsideClick: false,
        closeOnClickOutside: false,
        buttons: {
            cancel: {
                text: "No, cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, delete it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            deleteCollectionDetails(PaymentAutoId);
        }
    })
}

function deleteCollectionDetails(PaymentAutoId) {

    $.ajax({
        type: "POST",
        url: "ViewCustomerDetails.aspx/deleteCollectionDetails",
        data: "{'PaymentAutoId':" + PaymentAutoId + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            swal("", "Payment has been deleted successfully.", "success");
            c3 = 0;
            CollectionDetails(1);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function SettledcreditAmt() {
    $("#txtCreditAmount").val('0.00');
    $("#txtsettledRemarks").val('');
    $("#SettledCreditPopUp").modal('show');
}
function ValidateionforCredit() {
    var boolcheck = true;
    try {
        $('.creditstoreCredit').each(function () {
            if ($(this).val().trim() == '') {
                boolcheck = false;
                $(this).addClass('border-warning');
            } else {
                $(this).removeClass('border-warning');
            }
        });
    } catch (e) {

    }
    return boolcheck;
}
function UpdateCreaditAmt() {

    if (ValidateionforCredit()) {
        var data = {
            CustomerAutoId: AutoId,
            CreditAmount: $("#txtCreditAmount").val(),
            Remarks: $("#txtsettledRemarks").val()
        }
        $.ajax({
            type: "POST",
            url: "ViewCustomerDetails.aspx/UpdateCreaditAmt",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d == 'true') {
                    swal("", "Store Credit Settled successfully.", "success");
                    $("#SettledCreditPopUp").modal('hide');
                    c6 = 0;
                    StoreCreditLog(1);
                } else {
                    swal("Error!", "Oops! Something went wrong.Please try later.", "error");

                }

            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    } else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}


var paymentAutoid2;
function funcheck(e) {
    var tr = $(e).closest('tr');
    $('#btnOnHold').hide();
    $('#btnProcess').hide();
    paymentAutoid2 = $(tr).find('.ReceiveDate span').attr('PaymentAutoId');

    if ($(tr).find('.PaymentMode span').attr('PaymentMode') == 2 && $(tr).find('.ReferenceId span').attr('StatusCode') == 1) {
        $('#btnCancelCheck').show();
        $('#btnOnHold').show();
        $('#btnProcess').hide();
    }
    
    else if ($(tr).find('.ReferenceId span').attr('StatusCode') == 1 || $(tr).find('.ReferenceId span').attr('StatusCode') == 4) {
        $('#btnCancelCheck').show(); $('#btnProcess').show();
    }
}
var check = false;
function CancelCheckSecurity() {
    if (check == false) {
        SecurityPoP();
    } else {
        $("#ModelCancelRemark").modal('show');
    }
}

function clickonSecurityVoid() {
    var data = {
        Security: $("#txtSecurityVoid").val()
    }
    $.ajax({
        type: "POST",
        url: "ViewCustomerDetails.aspx/CheckSecurity",
        data: "{'datavalue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == 'SessionExpired') {
                location.href = '/';
            } else {
                if (check == false) {
                    var xmldoc = $.parseXML(response.d);
                    var orderList = $(xmldoc).find("Table");
                    if (orderList.length > 0) {
                        $.each(orderList, function () {
                            if ($("#txtSecurityVoid").val() == $(orderList).find("SecurityValue").text()) {
                                check = true
                                $("#SecurityEnabledVoid").modal('hide');
                                $("#ModelCancelRemark").modal('show');
                            }
                        });
                    }
                    else {
                        swal("Warning!", "Access denied.", "warning", {
                            button: "OK",
                        }).then(function () {
                            $("#txtSecurityVoid").val('');
                            $("#txtSecurityVoid").focus();
                        })
                    }
                }
                else {
                    $("#SecurityEnabledVoid").modal('hide');
                    // getCheckDetail();
                }
            }
        },
        failure: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        },
        error: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        }
    });
}

function SaveCancelReason() {
    if ($("#ReasonCancel").val() == "") {
        $("#ReasonCancel").addClass('border-warning');
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    var data = {
        PaymentAutoId: paymentAutoid2,
        Remark: $("#ReasonCancel").val()
    }
    $.ajax({
        type: "POST",
        url: "ViewCustomerDetails.aspx/CancelPayment",
        data: JSON.stringify({ datavalue: JSON.stringify(data) }),
        //data: "{'datavalue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d == 'SessionExpired') {
                location.href = '/';
            } else {
                if (check == true) {
                    $("#ReasonCancel").val('');
                    $("#ModelCancelRemark").modal('hide');
                    swal("", "Payment cancelled successfully.", "success").then(function () {
                        location.reload();
                    });
                }
                else {
                    $("#ReasonCancel").val('');
                    $("#txtSecurityVoid").focus();
                    $("#ModelCancelRemark").modal('show');
                }
            }
        },
        failure: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        },
        error: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        }
    });
}

function SecurityPoP() {
    $("#SecurityEnabledVoid").modal('show');
    $("#txtSecurityVoid").val('');
    $("#txtSecurityVoid").focus();
}

function SecurityPoPPaymentEdit() {
    $("#SecurityEnabledPaymentEdit").modal('show');
    $("#txtSecurityPaymentEdit").val('');
    $("#txtSecurityPaymentEdit").focus();
}


function clickonSecurityPaymentEdit() {
    if ($("#txtSecurityPaymentEdit").val().trim() != '') {
        var Security = $("#txtSecurityPaymentEdit").val()

        $.ajax({
            type: "POST",
            url: "ViewCustomerDetails.aspx/CheckSecurityPaymentEdit",
            data: "{'Security':'" + Security + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {

            },
            complete: function () {

            },
            success: function (response) {
                if (response.d == 'SessionExpired') {
                    location.href = '/';
                } else {

                    var xmldoc = $.parseXML(response.d);
                    var PaymentEdit = $(xmldoc).find("Table");
                    if (PaymentEdit.length > 0) {
                        $.each(PaymentEdit, function () {
                            if ($("#txtSecurityPaymentEdit").val() == $(PaymentEdit).find("SecurityValue").text()) {

                                check = true

                                $("#SecurityEnabledPaymentEdit").modal('hide');
                                setTimeout(function () {

                                    $("body").addClass('modal-open');
                                }, 1000)
                                $("#CustomerDueAmount").modal('show');
                                Paymentedit();
                            }
                        });
                    }
                    else {
                        swal("Warning!", "Access denied.", "warning", {
                            button: "OK",
                        }).then(function () {
                            $("#txtSecurityPaymentEdit").val('');
                            $("#txtSecurityPaymentEdit").focus();
                        })
                    }

                }
            },
            failure: function (result) {
                swal("Error!", "Oops! Something went wrong.Please try later.", "error");
            },
            error: function (result) {
                swal("Error!", "Oops! Something went wrong.Please try later.", "error");
            }
        });
    }
    else {
        $("#txtSecurityPaymentEdit").addClass('border-warning');
        toastr.error('Security can not be empty.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
function paymentCancellation() {
    if ($("#txtEmployeeRemarks").val() != "") {
        swal({
            title: "Are you sure?",
            text: "You want to cancel this payment!",
            icon: "warning",
            showCancelButton: true,
            allowOutsideClick: false,
            closeOnClickOutside: false,
            buttons: {
                cancel: {
                    text: "No, later!",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: true,
                },
                confirm: {
                    text: "Yes, cancel it!",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
                }
            }
        }).then(function (isConfirm) {
            if (isConfirm) {
                CancelledPayment();
            }
        })
    } else {
        swal("Error!", "Account Remarks is required when payment cancel.", "error");
    }
}

function CancelledPayment() {
    if ($("#txtEmployeeRemarks").val() != "") {
        var data = {
            CustomerAutoId: AutoId,
            EmployeeRemarks: $("#txtEmployeeRemarks").val(),
            PaymentAutoId: $("#PaymentAutoId").val()
        }
        $.ajax({
            type: "Post",
            url: "ViewCustomerDetails.aspx/CancelledPayment",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (data) {
                if (data.d == 'Session Expired') {
                    location.href = '/';
                } else if (data.d == 'true') {
                    swal("", "Payment cancelled successfully.", "success");
                    c2 = 0;
                    c1 = 0;
                    c3 = 0;
                    CustomerDuePayment();
                    CustomerPaymentList(1);
                    PayCancel();
                    CollectionDetails(1);
                } else {
                    swal("Error!", data.d, "error");
                }
            },
            error: function (result) {
            },
            failure: function (result) {
            }
        });
    } else {
        swal("Error!", "Account Remarks is required when payment cancel.", "error");
    }
}

function OnHoldCheck() {
    var PaymentAutoId;
    $("#tblCollectionDetails tbody tr").each(function () {
        if ($(this).find('.action input').prop('checked') == true) {
            PaymentAutoId = parseInt($(this).find('.ReceiveDate span').attr('paymentautoid'));
            $("#PaymentAutoId").val(PaymentAutoId);
        }
    });
    $('#txtCheckNo').val('');
    $('#txtCheckDate').val('');
    $('#txtRemarks').val('');
    $('#modelClearance').modal({ backdrop: 'static', keyboard: false })
}
function PaymentCancel(e) {
    var tr = $(e).closest('tr');
    $("#PaymentAutoId").val(tr.find('.action span').attr('paymentautoid'));
    $('#txtChequeNo').val(tr.find('.action span').attr('chequeno'));
    $('#txtChequeDate').val(tr.find('.action span').attr('ChequeDate'));
    $("#modalPopDuePayment").modal('show');
}
function CheckCancelDetails() {

    if (dynamicALL('CheckSave')) {
        {
            var data = {
                PaymentAutoId: $('#PaymentAutoId').val(),
                BankFeeCharge: $('#txtBankFeeCharge').val(),
                Remarks: $('#txtPopRemarks').val(),
            }
            $.ajax({
                type: "Post",
                url: "ViewCustomerDetails.aspx/CancelcheckDetails",
                data: "{'dataValue':'" + JSON.stringify(data) + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (data) {
                    if (data.d == 'Session Expired') {
                        location.href = '/';
                    } else
                        if (data.d == 'true') {
                            c3 = 0;
                            CollectionDetails(1);
                            $('#modalPopDuePayment').modal('hide');
                            swal("", "Check cancelled successfully.", "success").then(function () {
                                location.href = '/Sales/ViewCustomerDetails.aspx?PageId=' + AutoId;
                            });

                        } else {
                            swal("Error", data.d, "error");
                        }
                },
                error: function (result) {
                },
                failure: function (result) {
                }
            });

        }
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
function PrintSlip(PaymentAutoId) {
    window.open("/Warehouse/ReceiptVoucher.html?PId=" + PaymentAutoId + "", "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
}
function printPaymentSlip() {
    PrintSlip($('#PaymentAutoId').val());
}
function ProcessStoreCredit() {
    $('.DuehideCurrency').hide();
    $("#PaymentAutoId").val('0')
    var data = {
        CustomerId: AutoId
    }
    $.ajax({
        type: "POST",
        url: "ViewCustomerDetails.aspx/StoreCreditDetails",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var PaymentDetails = $(xmldoc).find('Table');
                var ddlPaymentMode = $(xmldoc).find('Table2');
                var EmployeeList = $(xmldoc).find('Table3');
                $("#ddlPaymentMenthod option:not(:first)").remove();
                $.each(ddlPaymentMode, function () {
                    $("#ddlPaymentMenthod").append("<option value='" + $(this).find("AutoID").text() + "'>" + $(this).find("PaymentMode").text() + "</option>");
                });
                $("#ddlReceivedPaymentBy option:not(:first)").remove();
                $.each(EmployeeList, function () {
                    $("#ddlReceivedPaymentBy").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("EmpName").text() + "</option>");
                });

                $('#txtPaymentId').val($(PaymentDetails).find('PayId').text());
                $('#ddlPaymentMenthod').val($(PaymentDetails).find('PaymentMode').text());
                $('#txtReferenceNo').val($(PaymentDetails).find('ReferenceId').text());
                $('#txtReceiveAmount').val($(PaymentDetails).find('CreditAmount').text());
                $('#txtPaymentRemarks').val($(PaymentDetails).find('Remarks').text());
                $('#txtChequeNo').val($(PaymentDetails).find('ChequeNo').text());
                $('#txtChequeDate').val($(PaymentDetails).find('ChequeDate').text());
                $('#ddlPaymentMenthod').attr('disabled', true);
                $('#btnpaymentCancel').show();
                if ($(PaymentDetails).find('PaymentMode').text() != '5') {
                    $('#txtReferenceNo').attr('disabled', true);
                    $('#ddlReceivedPaymentBy').attr('disabled', true);
                    $('#txtReceiveAmount').attr('disabled', true);
                    $('#txtPaymentRemarks').attr('disabled', true);
                } else {
                    $('#btnpaymentCancel').hide();
                    $('#txtReferenceNo').attr('disabled', false);
                    $('#ddlReceivedPaymentBy').attr('disabled', false);
                    $('#txtReceiveAmount').attr('disabled', false);
                    $('#txtPaymentRemarks').attr('disabled', false);
                }
                var customerOrderList = $(xmldoc).find('Table1');
                $('.nav-tabs').find('a:eq(4)').attr('class', 'nav-link');
                $('.nav-tabs').find('a:eq(3)').attr('class', 'nav-link active')
                $('.nav-tabs').find('a:eq(3)').attr('aria-expanded', 'true')
                $('.paynow').show();
                $('#btnCancel').show();

                $('#CollectionDetails').attr('class', 'tab-pane fade');
                $('#DueOrderList').attr('class', 'tab-pane fade active in show');
                $('#pay').hide();
                $("#Table4 tbody tr").remove();
                var row = $("#Table4 thead tr").clone(true);
                var TotalOrderAmount = 0.00, TotalPaidAmount = 0.00, DueAmount = 0.00, TotalPayAmount = 0.00, TotalpayDueAmount = 0.00;
                if (customerOrderList.length > 0) {
                    $.each(customerOrderList, function (index) {
                        $(".OrderNo", row).html("<span OrderAutoId='" + $(this).find('OrderAutoId').text() + "'></span><a target='_blank' href='/sales/OrderMaster.aspx?OrderNo=" + $(this).find("OrderNo").text() + "'>" + $(this).find("OrderNo").text() + "</a>");
                        $(".OrderDate", row).text($(this).find("OrderDate").text());
                        $(".OrderType", row).text($(this).find("OrderType").text());
                        $(".DaysOld", row).text($(this).find("DaysOld").text());
                        $(".OrderAmount", row).text($(this).find("PayableAmount").text());
                        TotalOrderAmount += parseFloat($(this).find("PayableAmount").text());
                        TotalPaidAmount += parseFloat($(this).find("AmtPaid").text());
                        $(".PaidAmount", row).text($(this).find("AmtPaid").text());
                        $(".DueAmount", row).text($(this).find("AmtDue").text());
                        $(".DueAmount", row).text($(this).find("AmtDue").text());
                        DueAmount += parseFloat($(this).find("AmtDue").text());
                        if ($(PaymentDetails).find('OrderAutoId').text() == $(this).find('OrderAutoId').text()) {
                            $(".CheckBoc", row).html("<input type='checkbox' class='chkDue' onclick='funCheckBoc(this)' checked disabled />");
                            if (parseFloat($(this).find("AmtDue").text()) >= parseFloat($(PaymentDetails).find('ReceivedAmount').text())) {
                                $(".PayAmount", row).text($(PaymentDetails).find('ReceivedAmount').text());
                                TotalPayAmount += parseFloat($(this).find("AmtDue").text());
                                $(".payDueAmount", row).text((parseFloat($(this).find("AmtDue").text()) - parseFloat($(PaymentDetails).find('ReceivedAmount').text())).toFixed(2));
                            } else {
                                $(".PayAmount", row).text($(this).find("AmtDue").text());
                                TotalPayAmount += parseFloat($(this).find("AmtDue").text());
                                $(".payDueAmount", row).text('0.00');
                            }


                        } else {
                            $(".CheckBoc", row).html("<input type='checkbox' class='chkDue' onclick='funCheckBoc(this)' />");
                            $(".PayAmount", row).text('0.00');
                            $(".payDueAmount", row).text('0.00');
                        }
                        $("#Table4 tbody").append(row);
                        row = $("#Table4 tbody tr:last").clone(true);
                    });
                }
                $("#TotalOrderAmount").text(TotalOrderAmount.toFixed(2));
                $("#TotalPaidAmount").text(TotalPaidAmount.toFixed(2));
                $("#DueAmount").text(DueAmount.toFixed(2));
                $("#TotalPayAmount").text(TotalPayAmount.toFixed(2));
                $("#TotalpayDueAmount").text(TotalpayDueAmount.toFixed(2));
                $('#txtStoreCredit').val('0.00')
                $('#txtTotalStoreCredit').val('0.00');

            } else {
                location.href = '/';
            }

        },
        error: function (result) {
        },
        failure: function (result) {
        }
    });

}


function StoreCreditLog(pageIndex) {
    if (c6 == 0) {
        var data = {
            CustomerAutoId: AutoId,
            PageIndex: pageIndex
        }
        $.ajax({
            type: "POST",
            url: "ViewCustomerDetails.aspx/StoreCreditLog",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json;charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d != "Session Expired") {

                    var xmldoc = $.parseXML(response.d);
                    var customerLOG = $(xmldoc).find('Table');
                    $("#tblStoreCreditlog tbody tr").remove();
                    var row = $("#tblStoreCreditlog thead tr").clone(true);

                    if (customerLOG.length > 0) {
                        $("#EmptyTable").hide();
                        $.each(customerLOG, function (index) {
                            $(".CreatedDate", row).text($(this).find("CreatedDate").text());
                            $(".Receivedby", row).text($(this).find("Receivedby").text());
                            $(".Amount", row).text($(this).find("Amount").text());
                            $(".ReferenceId", row).text($(this).find("ReferenceId").text());
                            $("#tblStoreCreditlog tbody").append(row);
                            row = $("#tblStoreCreditlog tbody tr:last").clone(true);
                        });
                    }
                } else {
                    location.href = '/';
                }
                c6 = c6 + 1;
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
}
function SettleCreditAmount() {
    var ReferenceNo = '';
    var check = false;
    $('#Table2 tbody tr').each(function () {
        if ($(this).find('.Sr input').prop('checked')) {
            ReferenceNo += $(this).find('.Sr input').attr('CreditAutoId') + ',';
            check = true;
        }
    });

    if (!check) {
        swal("Error !", "Please select at least one item in List.", "error");
        return;
    }

    $.ajax({
        type: "POST",
        url: "ViewCustomerDetails.aspx/orderList",
        data: "{'ReferenceNo':'" + ReferenceNo + "','CustomerId':'" + AutoId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var OrderList = $(xmldoc).find('Table');

                $("#ddlOrder option:not(:first)").remove();
                $.each(OrderList, function () {
                    $("#ddlOrder").append("<option  DueAmount='" + $(this).find("DueAmount").text() + "' value='" + $(this).find("OrderAutoId").text() + "'>" + $(this).find("OrderNo").text() + "</option>");
                });
                $("#ddlOrder").select2();


                var customerOrderList = $(xmldoc).find('Table1');
                $("#tblMemoOrderList tbody tr").remove();
                var row = $("#tblMemoOrderList thead tr").clone(true);
                var PayableAmount = 0.00, AmtDue = 0.00, AmtPaid = 0.00;
                if (customerOrderList.length > 0) {
                    $.each(customerOrderList, function (index) {
                        $(".SrNo", row).text((Number(index) + 1));
                        $(".CreditDate", row).html($(this).find("CreditDate").text());
                        $(".CreditNo", row).html($(this).find("CreditNo").text());
                        $(".TotalAmount", row).html($(this).find("TotalAmount").text());
                        $(".action", row).html('<input type="checkbox" checked CreditAutoId="' + $(this).find('CreditAutoId').text() + '"/>');
                        PayableAmount += parseFloat($(this).find("TotalAmount").text());
                        $("#tblMemoOrderList tbody").append(row);
                        row = $("#tblMemoOrderList tbody tr:last").clone(true);
                    });
                }
                $('#PTd1').html(PayableAmount.toFixed(2));
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
    $('#CreditOrderPop').modal('show');
}
function SettlledCredit() {

    var ReferenceNo = '';
    var check = false;
    $('#ddlOrder').closest('div').find('.select2-selection--single').removeAttr('style');
    $('#tblMemoOrderList tbody tr').each(function () {
        if ($(this).find('.action input').prop('checked')) {
            ReferenceNo += $(this).find('.action input').attr('CreditAutoId') + ',';
            check = true;
        }
    });
    if (!check) {
        swal("Error !", "Please select at least one item in List.", "error");
        return;
    }
    if ($('#ddlOrder').val() == '0' || $('#ddlOrder').val() == '' || $('#ddlOrder').val() == undefined) {
        swal("Error !", "Please select order.", "error");
        $('#ddlOrder').closest('div').find('.select2-selection--single').attr('style', 'border:1px solid #FF9149  !important');
        return;
    }
    var data = {
        OrderAutoId: $('#ddlOrder').val(),
        ReferenceNo: ReferenceNo,
        Remarks: $('#txtCreditRemarks').val()
    }

    $.ajax({
        type: "POST",
        url: "ViewCustomerDetails.aspx/CreditMemoSettled",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                $('#CreditOrderPop').modal('hide');
                if (response.d == 'true') {
                    swal("", "Order attached successfully in Credit Memo.", "success").then(function () {
                        location.reload();
                    });
                } else {
                    swal("Message!", response.d, "success");
                }
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function getamount() {
    $("#txtCreditMemoAmount").val($("#ddlOrder option:selected").attr('DueAmount'));
}
function getCreditMemo(CreditAutoId) {
    $.ajax({
        type: "POST",
        url: "ViewCustomerDetails.aspx/orderList",
        data: "{'ReferenceNo':'" + CreditAutoId + "','CustomerId':'" + AutoId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var OrderList = $(xmldoc).find('Table');

                $("#ddlOrder option:not(:first)").remove();
                $.each(OrderList, function () {
                    $("#ddlOrder").append("<option  DueAmount='" + $(this).find("DueAmount").text() + "' value='" + $(this).find("OrderAutoId").text() + "'>" + $(this).find("OrderNo").text() + "</option>");
                });
                $("#ddlOrder").select2();


                var customerOrderList = $(xmldoc).find('Table1');
                $("#tblMemoOrderList tbody tr").remove();
                var row = $("#tblMemoOrderList thead tr").clone(true);
                var PayableAmount = 0.00, AmtDue = 0.00, AmtPaid = 0.00;
                if (customerOrderList.length > 0) {
                    $.each(customerOrderList, function (index) {
                        $(".SrNo", row).text((Number(index) + 1));
                        $(".CreditDate", row).html($(this).find("CreditDate").text());
                        $(".CreditNo", row).html($(this).find("CreditNo").text());
                        $(".TotalAmount", row).html($(this).find("TotalAmount").text());
                        $(".action", row).html('<input type="checkbox" checked CreditAutoId="' + $(this).find('CreditAutoId').text() + '"/>');
                        PayableAmount += parseFloat($(this).find("TotalAmount").text());
                        $("#tblMemoOrderList tbody").append(row);
                        row = $("#tblMemoOrderList tbody tr:last").clone(true);
                    });
                }
                $('#PTd1').html(PayableAmount.toFixed(2));
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
        },
        failure: function (result) {
        }
    });
    $('#CreditOrderPop').modal('show');
}
function OpenPopup() {
    $("#txtBankName").val('');
    $("#txtACName").val('');
    $("#ddlCardType").val('0');
    $("#txtCardNo").val('');
    $("#txtCVV").val('');
    $("#txtExpiryDate").val('');
    $("#txtRoutingNo").val('');
    $("#txtBankRemarks").val('');
    $('#btnUpdates').hide();
    $('#btnSaves').show();
    $('#PopBankDetails').modal('show');
    // bindCardType();
}
function OpenPopup1() {
    $("#txtBankName").val('');
    $("#txtACName").val('');
    $("#ddlCardType").val('0');
    $("#txtCardNo").val('');
    $("#txtCVV").val('');
    $("#txtZipcode").val('');
    $("#txtExpiryDate").val('');
    $("#txtCardRemarks").val('');
    $('#btnUpdates1').hide();
    $('#btnSaves1').show();
    $('#PopCardDetails').modal('show');
    bindCardType();
}

//test svn log
//--------------------------------------------------------------Bind Card Type------------------------------------------
function bindCardType() {
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/customerMaster.asmx/bindCardType",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },

        success: function (response) {
            if (response.d == 'Session Expired') {
                location.href = '/';
            } else if (response.d != 'false') {
                var xmldoc = $.parseXML(response.d);
                var CardType = $(xmldoc).find('Table');
                $("#ddlCardType option:not(:first)").remove();  //-------------------------------For Search Field
                $.each(CardType, function () {
                    $("#ddlCardType").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("CardType").text() + "</option>");
                });
                $("#ddlCardType").select2();
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
//-----------------------------------------------------Save Bank Details--------------------------------------------
function SaveBankDetails() {
    if (dynamicALL('Breq')) {
        var data = {
            BankName: $("#txtBankName").val(),
            ACName: $("#txtACName").val(),
            RoutingNo: $("#txtRoutingNo").val(),
            Remarks: $("#txtBankRemarks").val(),
            CustomerAutoId: AutoId,
            Status: '1'
        }
        $.ajax({
            type: "POST",
            url: "ViewCustomerDetails.aspx/SaveBankDetails",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $("#btnSaves").attr('disabled', true);
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
                $("#btnSaves").attr('disabled', false);
            },
            success: function (result) {
                if (result.d == 'false') {
                    swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                }
                else if (result.d == 'Unauthorized access.') {
                    location.href = "/";
                }
                else {
                    swal("", "Bank details saved successfully.", "success");
                    $('#PopBankDetails').modal('hide');
                    $("#txtBankName").val('');
                    $("#txtACName").val('');
                    $("#ddlCardType").val('0');
                    $("#txtCardNo").val('');
                    $("#txtCVV").val('');
                    $("#txtExpiryDate").val('');
                    GetBankDetailsList();
                }
            },
            error: function (result) {
                swal("Error!", result.d, "error");
            },
            failure: function (result) {
                swal("Error!", result.d, "error");
            }
        });
    } else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
function SaveBankDetails1() {
    if (dynamicALL('ddlCardreq')) {
        var data = {
            CardType: $("#ddlCardType").val(),
            CardNo: $("#txtCardNo").val(),
            ExpiryDate: $("#txtExpiryDate").val(),
            CVV: $("#txtCVV").val(),
            Zipcode: $("#txtZipcode").val(),
            Remarks: $("#txtCardRemarks").val(),
            CustomerAutoId: AutoId,
            Status: '2'
        }
        $.ajax({
            type: "POST",
            url: "ViewCustomerDetails.aspx/SaveBankDetails",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $("#btnSaves1").attr('disabled', true);
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
                $("#btnSaves1").attr('disabled', false);
            },
            success: function (result) {
                if (result.d == 'false') {
                    swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                }
                else if (result.d == 'Unauthorized access.') {
                    location.href = "/";
                }
                else {
                    swal("", "Card details saved successfully.", "success");
                    $('#PopCardDetails').modal('hide');
                    $("#txtBankName").val('');
                    $("#txtACName").val('');
                    $("#ddlCardType").val('0');
                    $("#txtCardNo").val('');
                    $("#txtCVV").val('');
                    $("#txtExpiryDate").val('');
                    GetBankDetailsList();
                }
            },
            error: function (result) {
                swal("Error!", result.d, "error");
            },
            failure: function (result) {
                swal("Error!", result.d, "error");
            }
        });
    } else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
//------------------------------------------------Bind Bank Details List-------------------------------------
function GetBankDetailsList() {
    var data = {
        CustomerAutoId: AutoId,
    };
    $.ajax({
        type: "POST",
        url: "ViewCustomerDetails.aspx/GetBankDetailsList",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successGetBankDetailsList,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function successGetBankDetailsList(response) {
    var xmldoc = $.parseXML(response.d);
    var BaknDetailsList = $(xmldoc).find("Table");
    var CardDetailsList = $(xmldoc).find("Table1");
    $("#tblBankDetails tbody tr").remove();

    var row = $("#tblBankDetails thead tr").clone(true);
    if (BaknDetailsList.length > 0) {
        $("#EmptyTable").hide();
        $.each(BaknDetailsList, function (index) {
            $(".BankName", row).text($(this).find("BankName").text());
            $(".BankACC", row).text($(this).find("BankACC").text());
            $(".Routing", row).text($(this).find("RoutingNo").text());
            $(".BankRemarks", row).text($(this).find("BankRemark").text());
            $(".Action", row).html("<a title='Edit' href='#'><span class='ft-edit' onclick='EditBankDetails(" + $(this).find("AutoId").text() + ")'></span></a>&nbsp;<a title='Delete' href='#'><span class='ft-x' onclick='DeleteBankDetails(" + $(this).find("AutoId").text() + ",0)'></span></a>");
            $("#tblBankDetails tbody").append(row);
            row = $("#tblBankDetails tbody tr:last").clone(true);
        });

    } else {
        $("#EmptyTable").show();
    }

    $("#tblCardDetails tbody tr").remove();

    var row = $("#tblCardDetails thead tr").clone(true);
    if (CardDetailsList.length > 0) {
        $("#EmptyTable").hide();
        $.each(CardDetailsList, function (index) {
            $(".CardType", row).text($(this).find("CardType").text());
            $(".CardNo", row).text($(this).find("CardNo").text());
            $(".ExpiryDate", row).text($(this).find("ExpiryDate").text());
            $(".CVV", row).text($(this).find("CVV").text());
            $(".Zipcode", row).text($(this).find("Zipcode").text());
            $(".CardRemarks", row).text($(this).find("CardRemark").text());
            $(".Action", row).html("<a title='Edit' href='#'><span class='ft-edit' onclick='EditBankDetails1(" + $(this).find("AutoId").text() + ")'></span></a>&nbsp;<a title='Delete' href='#'><span class='ft-x' onclick='DeleteBankDetails(" + $(this).find("AutoId").text() + ",1)'></span></a>");
            $("#tblCardDetails tbody").append(row);
            row = $("#tblCardDetails tbody tr:last").clone(true);
        });

    } else {
        $("#EmptyTable").show();
    }
}
function EditBankDetails(AutoId) {
    $("#hdnAutoId").val(AutoId);
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/customerMaster.asmx/EditBankDetails",
        data: "{'AutoId':'" + AutoId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: onSuccessOfEditBankDetails,
        error: function (response) {
            console.log(response.d)
        }
    })
}
function onSuccessOfEditBankDetails(response) {
    if (response.d == 'Session Expired') {
        location.href = '/';
    } else {
        $('#PopBankDetails').modal('show');

        var xmldoc = $.parseXML(response.d);
        var Details = $(xmldoc).find('Table');
        $("#txtBankName").val($(Details).find("BankName").text());
        $("#txtACName").val($(Details).find("BankACC").text());
        $("#txtRoutingNo").val($(Details).find("RoutingNo").text());
        $("#txtBankRemarks").val($(Details).find("BankRemark").text());
        $("#txtBankRemarks").val($(Details).find("BankRemark").text());
        $("#btnSaves").hide();
        $("#btnReset").hide();
        $("#btnUpdates").show();
        $("#btnCancel").show();
    }
}

function EditBankDetails1(AutoId) {
    $("#hdnAutoId").val(AutoId);
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/customerMaster.asmx/EditBankDetails",
        data: "{'AutoId':'" + AutoId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: onSuccessOfEditBankDetails1,
        error: function (response) {
            console.log(response.d)
        }
    })
}
function onSuccessOfEditBankDetails1(response) {
    if (response.d == 'Session Expired') {
        location.href = '/';
    } else {
        $('#PopCardDetails').modal('show');

        var xmldoc = $.parseXML(response.d);
        var Details = $(xmldoc).find('Table');
        $("#ddlCardType").val($(Details).find("CardTypeAutoId").text()).change();
        $("#txtExpiryDate").val($(Details).find("ExpiryDate").text());
        $("#txtCVV").val($(Details).find("CVV").text());
        $("#txtZipcode").val($(Details).find("Zipcode").text());
        $("#txtCardNo").val($(Details).find("CardNo").text());
        $("#txtCardRemarks").val($(Details).find("CardRemark").text());

        $("#btnSaves1").hide();
        $("#btnReset").hide();
        $("#btnUpdates1").show();
        $("#btnCancel").show();
    }
}

function UpdateBankDetails() {
    if (dynamicALL('Breq')) {
        var data = {
            BankName: $("#txtBankName").val(),
            ACName: $("#txtACName").val(),
            RoutingNo: $("#txtRoutingNo").val(),
            AutoId: $("#hdnAutoId").val(),
            Remarks: $("#txtBankRemarks").val(),
            Status: '1'
        }
        $.ajax({
            type: "POST",
            url: "/Sales/WebAPI/customerMaster.asmx/UpdateBankDetails",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == 'false') {
                    swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                }
                else if (result.d == 'Unauthorized access.') {
                    location.href = "/";
                }
                else {
                    swal("", "Bank details updated successfully.", "success");
                    $('#PopBankDetails').modal('hide');
                    $("#txtBankName").val('');
                    $("#txtACName").val('');
                    $("#ddlCardType").val('0');
                    $("#txtCardNo").val('');
                    $("#txtCVV").val('');
                    $("#txtExpiryDate").val('');
                    GetBankDetailsList();
                }
            },
            error: function (result) {
                swal("Error!", result.d, "error");
            },
            failure: function (result) {
                swal("Error!", result.d, "error");
            }
        });
    } else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
function UpdateBankDetails1() {
    if (dynamicALL('ddlCardreq')) {
        var data = {
            CardType: $("#ddlCardType").val(),
            CardNo: $("#txtCardNo").val(),
            ExpiryDate: $("#txtExpiryDate").val(),
            CVV: $("#txtCVV").val(),
            Zipcode: $("#txtZipcode").val(),
            AutoId: $("#hdnAutoId").val(),
            Remarks: $("#txtCardRemarks").val(),
            Status: '2'
        }
        $.ajax({
            type: "POST",
            url: "/Sales/WebAPI/customerMaster.asmx/UpdateBankDetails",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == 'false') {
                    swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                }
                else if (result.d == 'Unauthorized access.') {
                    location.href = "/";
                }
                else {
                    swal("", "Card details updated successfully.", "success");
                    $('#PopCardDetails').modal('hide');
                    $("#txtBankName").val('');
                    $("#txtACName").val('');
                    $("#ddlCardType").val('0');
                    $("#txtCardNo").val('');
                    $("#txtCVV").val('');
                    $("#txtExpiryDate").val('');
                    GetBankDetailsList();
                }
            },
            error: function (result) {
                swal("Error!", result.d, "error");
            },
            failure: function (result) {
                swal("Error!", result.d, "error");
            }
        });
    } else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
function DeleteBankDetails(AutoId, type) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this Details.",
        icon: "warning",
        showCancelButton: true,
        allowOutsideClick: false,
        closeOnClickOutside: false,
        buttons: {
            cancel: {
                text: "No, cancel.",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, delete it.",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            DeleteDetail(AutoId, type);
        }
    })
}
/*--------------------------------------------------------Delete Tax ----------------------------------------------------------*/
function DeleteDetail(AutoId, type) {
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/customerMaster.asmx/DeleteBankDetails",
        data: "{'AutoId':'" + AutoId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },

        success: function (result) {
            if (type == 0) {
                swal("", "Bank details deleted successfully.", "success");
            } else {
                swal("", "Card details deleted successfully.", "success");
            }
            var table = $('#tblBankDetails').DataTable();
            table.destroy();
            GetBankDetailsList();
        },
        error: function (result) {
            swal("error", "Bank details already assigned to Orders.", "error");
        },
        failure: function (result) {
            swal("error", "Bank details already assigned to Orders.", "error");
        }
    })
}
function dtval(d, e) {
    if (d.value.length > 2) {


        var pK = e ? e.which : window.event.keyCode;
        if (pK == 8) { d.value = substr(0, d.value.length - 1); return; }
        var dt = d.value;

        var da = dt.split('/');
        for (var a = 0; a < da.length; a++) { if (da[a] != +da[a]) da[a] = da[a].substr(0, da[a].length - 1); }

        if (da[0] > 12) { da[1] = da[0].substr(da[0].length - 1, 1); da[0] = '0' + da[0].substr(0, da[0].length - 1); }

        if (da[1] > 99) da[0] = da[1].substr(0, da[1].length - 1);

        dt = da.join('/');
        if (dt.length == 2) dt += '/';
        d.value = dt;
    } else {
        d.value = d.value
    }

}
function dtexp(d) {

    var month = new Date().getMonth() + 1;
    var year = new Date().getFullYear();
    year = year.toString().substr(2, 2);
    var dt = d.value;
    if (d.value.substr(2, 1) != '/') {
        d.value = '';
        toastr.error('Date is not valid.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
    var da = dt.split('/');
    if (da[1] < year) {
        d.value = '';
        toastr.error('Date is not valid.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' })
    }
    if (da[1] == year & da[0] < month) {

        d.value = '';
        toastr.error('Date is not valid.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' })
    }
}
function checkCVVLength() {
    if ($("#txtCVV").val().length > 2) {
        $("#txtCVV").attr('style', 'border:1px solid #666EE8  !important');
    } else {
        $("#txtCVV").attr('style', 'border:1px solid #FF9149  !important');
        $("#txtCVV").val('');
    }
}

function SaveCheckDetails() { //using Validation Group SaveCheck
    if (dynamicALL('SaveCheck')) {
        {
            var data = {
                PaymentAutoId: $('#PaymentAutoId').val(),
                CheckNo: $('#txtCheckNo').val(),
                CheckDate: $('#txtCheckDate').val(),
                Remarks: $('#txtRemarks').val(),
            }

            $("#SecurityEnabledHold").modal('hide');
            $.ajax({
                type: "Post",
                url: "ViewCustomerDetails.aspx/Savecheck",
                data: JSON.stringify({ dataValue: JSON.stringify(data) }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (data) {
                    if (data.d == 'Session Expired') {
                        location.href = '/';
                    } else if (data.d == 'true') {
                        c3 = 0;
                        CollectionDetails(1);
                        $('#modelClearance').modal('hide');
                        $('#btnOnHold').hide();
                        swal("", "Check details saved successfully.", "success");
                    } else {
                        swal("Error", "Oops! Something went wrong.Please try later.", "error");
                    }
                },
                error: function (result) {
                },
                failure: function (result) {
                }
            });
        }
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}

//function HoldCheckSecurity() {
//    if (Holdcheck == false) {
//        SecurityHoldPoP();
//    } else {
//         Holdcheck = true;
//        $("#SecurityEnabledHold").modal('hide');
//    }
//}
var Holdcheck = false;

function SecurityHoldPoP() {
    $("#SecurityEnabledHold").modal('show');
    $("#txtSecurityHold").val('');
    $("#txtSecurityHold").focus();
}

function clickonSecurityHold() {
    var data = {
        Security: $("#txtSecurityHold").val()
    }
    $.ajax({
        type: "POST",
        url: "ViewCustomerDetails.aspx/CheckHoldSecurity",
        data: "{'datavalue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == 'SessionExpired') {
                location.href = '/';
            } else {
                if (check == false) {
                    var xmldoc = $.parseXML(response.d);
                    var orderList = $(xmldoc).find("Table");
                    if (orderList.length > 0) {
                        Holdcheck = true;
                        ProcessPayment();
                    } else {
                        swal("Warning!", "Access denied.", "warning", {
                            button: "OK",
                        }).then(function () {
                            $("#txtSecurityHold").val('');
                            $("#txtSecurityHold").focus();
                        });
                        Holdcheck = false;
                    }
                }
                else {
                    $("#SecurityEnabledHold").modal('hide');
                }
            }
        },
        failure: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        },
        error: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        }
    });
}



function ProcessPayment() {
    $("#txtSortAmount").val('0.00');
    $("#txtTotalCurrencyAmount").val('0.00');
    $("#TotalAmount").html('0.00');

    var PaymentAutoId = 0, PaymentType = 0, Count = 0, ProcessStatus, CheckDate;
    $("#tblCollectionDetails tbody tr").each(function () {
        if ($(this).find('.action input').prop('checked') == true) {
            CheckDate = $(this).find('.ChequeDate span').attr('ChequeDate');
            Count = Number(Count) + 1;
            PaymentAutoId = parseInt($(this).find('.ReceiveDate span').attr('paymentautoid'));
            PaymentType = parseInt($(this).find('.PaymentType span').attr('PayCode'));
            ProcessStatus = parseInt($(this).find('.ReceivedBy span').attr('ProcessStatus'));
            $("#PaymentAutoId").val(PaymentAutoId);
        }
    });

    var d = new Date();

    var month = d.getMonth() + 1;
    var day = d.getDate();

    var output =
        (('' + month).length < 2 ? '0' : '') + month + '/' +
        (('' + day).length < 2 ? '0' : '') + day + '/'
        + d.getFullYear();

    if (CheckDate > output) {
        SecurityHoldPoP();
    } else {
        Holdcheck = true;

    }

    if (Holdcheck == true) {
        $("#SecurityEnabledHold").modal('hide');

        if (ProcessStatus == 1) {
            swal("Warning!", "This order is not processed. Please process order first.", "warning");
        } else if (Count == 0) {
            swal("Error!", "Please check any of the one payment.", "error");
        } else {
            var data = {
                CustomerAutoId: AutoId,
                PaymentAutoId: $("#PaymentAutoId").val()
            }
            $.ajax({
                type: "POST",
                url: "ViewCustomerDetails.aspx/PaymentDetails",
                data: JSON.stringify({ dataValue: JSON.stringify(data) }),
                contentType: "application/json;charset=utf-8",
                datatype: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.d == "Settled") {
                        swal("", "Payment is already settled.", "warning");
                    }
                    else if (response.d == "Cancelled") {
                        swal("", "Payment is cancelled.", "warning");
                    }
                    else if (response.d != "Session Expired") {
                        var responsedetails = $.parseJSON(response.d);
                        var PaymentDetails = responsedetails[0].PayList;
                        var customerOrderList = responsedetails[0].DList;
                        var ddlPaymentMode = responsedetails[0].PM;
                        var EmployeeList = responsedetails[0].EList;
                        var currencydetails = responsedetails[0].CList;
                        Holdcheck = false;

                        $("#tblCurrencyList tbody tr").remove();
                        var row = $("#tblCurrencyList thead tr").clone(true);
                        $.each(currencydetails, function (index, items) {
                            $(".CurrencyName", row).html("<span CurrencyValue='" + items.CV + "'  CurrencyAutoid='" + items.Autoid + "'></span>" + items.CN);
                            $(".NoofRupee", row).html("<input type='Text' maxlength='6' id='txtTopCurrency' class='form-control input-sm text-center' onkeypress='return isNumberKey(event)' onchange='PaymentCal(this)' onfocus='this.select()' />");
                            $(".Total", row).text('0.00');
                            $("#tblCurrencyList tbody").append(row);
                            row = $("#tblCurrencyList tbody tr:last").clone(true);
                        });

                        $("#ddlPaymentMenthod option:not(:first)").remove();
                        $.each(ddlPaymentMode, function (i, items) {
                            $("#ddlPaymentMenthod").append("<option value='" + items.AutoID + "'>" + items.PM + "</option>");
                        });
                        $("#ddlReceivedPaymentBy option:not(:first)").remove();
                        $.each(EmployeeList, function (index, items) {
                            $("#ddlReceivedPaymentBy").append("<option value='" + items.AutoId + "'>" + items.EmpName + "</option>");
                        });

                        $('#txtPaymentId').val(PaymentDetails[0].PayId);
                        $('#ddlPaymentMenthod').val(PaymentDetails[0].PaymentMode);
                        $('#txtReferenceNo').val(PaymentDetails[0].ReferenceId);
                        $('#ddlReceivedPaymentBy').val(PaymentDetails[0].ReceiveBy);
                        $('#txtReceiveAmount').val(PaymentDetails[0].ReceivedAmount);
                        $('#txtPaymentRemarks').val(PaymentDetails[0].Remarks);
                        $('#txtChequeNo').val(PaymentDetails[0].ChequeNo);
                        $('#txtChequeDate').val(PaymentDetails[0].ChequeDate);
                        $('#ddlPaymentMenthod').attr('disabled', true);
                        $('#txtReferenceNo').attr('disabled', true);
                        $('#ddlReceivedPaymentBy').attr('disabled', true);
                        $('#txtReceiveAmount').attr('disabled', true);
                        $('#txtPaymentRemarks').attr('disabled', true);
                        $('.nav-tabs').find('a:eq(4)').attr('class', 'nav-link');
                        $('.nav-tabs').find('a:eq(3)').attr('class', 'nav-link active')
                        $('.nav-tabs').find('a:eq(3)').attr('aria-expanded', 'true')
                        $('#DueOrderList').attr('class', 'tab-pane fade active in show')
                        $('.paynow').show();
                        $('#btnCancel').show();
                        $('#btnpaymentCancel').show();
                        $('#CollectionDetails').attr('class', 'tab-pane fade');
                        $('#pay').hide();
                        $("#Table4 tbody tr").remove();
                        $("#OrderAutoId").val('');
                        var row = $("#Table4 thead tr").clone(true);
                        var TotalOrderAmount = 0.00, TotalPaidAmount = 0.00, DueAmount = 0.00, TotalPayAmount = 0.00, TotalpayDueAmount = 0.00, dAmt = 0.00, rAmt = 0.00;
                        if (customerOrderList.length > 0) {
                            $.each(customerOrderList, function (index, items) {
                                $(".OrderNo", row).html("<span OrderAutoId='" + items.AId + "'></span>" + items.ONO);
                                $(".OrderDate", row).text(items.OD);
                                $(".DaysOld", row).text(items.Dd);
                                $(".OrderType", row).text(items.OT);
                                $(".OrderAmount", row).text(parseFloat(items.Pay).toFixed(2));
                                TotalOrderAmount += parseFloat(items.Pay);
                                TotalPaidAmount += parseFloat(items.AmtPaid);
                                $(".PaidAmount", row).text(parseFloat(items.AmtPaid).toFixed(2));
                                $(".DueAmount", row).text(parseFloat(items.AmtDue).toFixed(2));

                                DueAmount += parseFloat(items.AmtDue);
                                if (PaymentDetails[0].OrderAutoId == items.AId) {
                                    $("#OrderAutoId").val(PaymentDetails[0].OrderAutoId);
                                    $(".CheckBoc", row).html("<input type='checkbox' class='chkDue' onclick='funCheckBoc(this)' checked disabled />");
                                    if (parseFloat(items.AmtDue) >= parseFloat(PaymentDetails[0].ReceivedAmount)) {
                                        $(".PayAmount", row).text(PaymentDetails[0].ReceivedAmount);
                                        TotalPayAmount = parseFloat(PaymentDetails[0].ReceivedAmount);
                                        dAmt = parseFloat(items.AmtDue);
                                        rAmt = PaymentDetails[0].ReceivedAmount;
                                        $(".payDueAmount", row).text(parseFloat(dAmt - rAmt).toFixed(2));
                                    } else {
                                        $(".PayAmount", row).text(items.AmtDue);
                                        TotalPayAmount += parseFloat(items.AmtDue);
                                        $(".payDueAmount", row).text('0.00');
                                    }
                                } else {
                                    $(".CheckBoc", row).html("<input type='checkbox' class='chkDue' onclick='funCheckBoc(this)' />");
                                    $(".PayAmount", row).text('0.00');
                                    $(".payDueAmount", row).text('0.00');
                                }
                                $("#Table4 tbody").append(row);
                                row = $("#Table4 tbody tr:last").clone(true);
                            });
                        }
                        if ($("#OrderAutoId").val() != '') {
                            $("#Table4 tbody tr .CheckBoc input").attr('disabled', 'disabled');
                        }
                        $("#TotalOrderAmount").text(TotalOrderAmount.toFixed(2));
                        $("#TotalPaidAmount").text(TotalPaidAmount.toFixed(2));
                        $("#DueAmount").text(DueAmount.toFixed(2));
                        $("#TotalPayAmount").text(TotalPayAmount.toFixed(2));
                        $("#TotalpayDueAmount").text(TotalpayDueAmount.toFixed(2));
                        if (parseFloat($('#txtReceiveAmount').val()).toFixed(2) < parseFloat(TotalPayAmount).toFixed(2)) {
                            $('#txtStoreCredit').val('0.00');
                            $('#txtTotalStoreCredit').val((parseFloat($('#txtTotalStoreCredit').attr('TotalAmount'))).toFixed(2));
                        } else {
                            $('#txtStoreCredit').val((parseFloat($('#txtReceiveAmount').val()) - parseFloat(TotalPayAmount)).toFixed(2))
                            $('#txtTotalStoreCredit').val(((parseFloat($('#txtTotalStoreCredit').attr('TotalAmount'))) + (parseFloat($('#txtStoreCredit').val()))).toFixed(2));
                        }
                        if (PaymentDetails[0].PaymentMode == 1) {
                            $(".DuehideCurrency").show();
                        }
                        else {
                            $(".DuehideCurrency").hide();
                        }
                        if ($("#ddlPaymentMenthod").val() == '2') {
                            $("#btnpaymentCancel").hide();
                        }
                        else {
                            $("#btnpaymentCancel").hide();
                        }
                    } else {
                        location.href = '/';
                    }
                },
                error: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                },
                failure: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                }
            });
        }
    }
}
function validateEmail(elementValue) {
    var EmailCodePattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var check = EmailCodePattern.test($(elementValue).val());
    if (!check) {
        toastr.error('Invalid email id.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $(elementValue).val('');
    }
    return;
}
function OpenContactPopup() {
    $("#ddlType").val(0).change();
    $("#txtContactName").val('');
    $("#txtContactMobile").val('');
    $("#txtLandline").val('');
    $("#txtLandline2").val('');
    $("#txtFax").val('');
    $("#txtEmail").val('');
    $("#txtAlternateEmail").val('');
    $('#btnContactUpdates').hide();
    $('#btnContactSaves1').show();
    $('#PopContactDetails').modal('show');
    bindContactPersonType();
}

function bindContactPersonType() {
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/customerMaster.asmx/bindContactPersonType",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },

        success: function (response) {
            console.log(response);
            if (response.d == 'Session Expired') {
                location.href = '/';
            } else if (response.d != 'false') {
                var xmldoc = $.parseXML(response.d);
                var ContactPersonType = $(xmldoc).find('Table');
                $("#ddlType option:not(:first)").remove();  //-------------------------------For Search Field
                $.each(ContactPersonType, function () {
                    $("#ddlType").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("TypeName").text() + "</option>");
                });
                //$("#ddlType").select2();
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}


function GetContactPersonList() {
    var data = {
        AutoId: $("#hiddenCustomerAutoId").val()
    }
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/customerMaster.asmx/GetContactPersonList",
        data: "{'datavalue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            console.log(response);
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var ContactPersonList = $(xmldoc).find('Table');
                $("#ContactDetails tbody tr").remove();
                var row = $("#ContactDetails thead tr").clone(true);
                if (ContactPersonList.length > 0) {
                    $.each(ContactPersonList, function (index) {

                        $(".AutoId", row).text($(this).find("AutoId").text());
                        $(".TypeAutoId", row).text($(this).find("Type").text());
                        $(".Type", row).text($(this).find("TypeName").text());
                        $(".ContactPerson", row).text($(this).find("ContactPerson").text());
                        $(".Mobile", row).text($(this).find("MobileNo").text());
                        $(".Landline", row).text($(this).find("Landline").text());
                        $(".Landline2", row).text($(this).find("Landline2").text());
                        $(".Fax", row).text($(this).find("Fax").text());
                        $(".Email", row).text($(this).find("Email").text());
                        $(".AltEmail", row).text($(this).find("AlternateEmail").text());
                        if ($(this).find("ISDefault").text() == 'Yes') {
                            $(".IsDefault", row).html('<span class="badge badge badge-pill badge-success mr-2" style="text-transform: capitalize;">' + $(this).find("ISDefault").text() + '</span>');
                            $(".Action", row).html("<a title='Edit' href='javascript:void(0)' onclick='EditContactPerson(this)'><span class='la la-edit'><span></a>");

                        }
                        else {
                            $(".IsDefault", row).html('<span class="badge badge badge-pill badge-danger mr-2" style="text-transform: capitalize;">' + $(this).find("ISDefault").text() + '</span>');
                            $(".Action", row).html("<a title='Edit' href='javascript:void(0)' onclick='EditContactPerson(this)'><span class='la la-edit'><span></a>&nbsp;&nbsp;<a title='Delete' href='javascript:void(0)' onclick='deleterecordContact(\"" + $(this).find("AutoId").text() + "\")'><span class='la la-remove'></span></a>");

                        }

                        $("#ContactDetails tbody").append(row);
                        row = $("#ContactDetails tbody tr:last").clone(true);
                    });
                }
                bindContactPersonType();

            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}
function saveContactDetail() {

    var IsDefault = 0;
    if ($("#chkIsDefault").prop("checked") == true) {
        IsDefault = 1;
    }
    if (ContactcheckRequiredField()) {
        if ($("#txtLandline").val().length < 10) {
            toastr.error('Please enter a valid 10 digit Landline No.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            $("#txtLandline").addClass('border-warning');
            return;
        }
        else if ($("#txtLandline2").val() != "") {
            if ($("#txtLandline2").val().length < 10) {
                $("#txtLandline").removeClass('border-warning');
                toastr.error('Please enter a valid 10 digit Landline No.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
                $("#txtLandline2").addClass('border-warning');
                return;
            }
        }
        else if ($("#txtContactMobile").val().length < 10) {
            $("#txtLandline2").removeClass('border-warning');
            toastr.error('Please enter a valid 10 digit Mobile No.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            $("#txtContactMobile").addClass('border-warning');
            return;
        }
        if ($("#txtLandline").val() == $("#txtContactMobile").val()) {
            if ($("#txtLandline2").val() != '') {
                if ($("#txtLandline2").val() == $("#txtContactMobile").val()) {
                    $("#txtLandline").addClass('border-warning');
                    $("#txtLandline2").addClass('border-warning');
                    $("#txtContactMobile").addClass('border-warning');
                    toastr.error("Landline 1, Landline 2 and Mobile No can't be same.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    return;
                }
                else if ($("#txtLandline").val() == $("#txtContactMobile").val()) {
                    $("#txtLandline").addClass('border-warning');
                    $("#txtContactMobile").addClass('border-warning');
                    $("#txtLandline2").removeClass('border-warning');
                    toastr.error("Landline 1 and Mobile No can't be same.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    return;
                }
            }
            else {
                $("#txtLandline").addClass('border-warning');
                $("#txtContactMobile").addClass('border-warning');
                toastr.error("Landline 2 and Mobile No can't be same.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                return;
            }
        }
        if ($("#txtLandline2").val() != '') {
            if ($("#txtLandline").val() == $("#txtLandline2").val()) {
                $("#txtLandline").addClass('border-warning');
                $("#txtLandline2").addClass('border-warning');
                toastr.error("Landline 1 and Landline 2 can't be same.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                return;
            }
            else if ($("#txtLandline2").val() == $("#txtContactMobile").val()) {
                $("#txtLandline2").addClass('border-warning');
                $("#txtContactMobile").addClass('border-warning');
                toastr.error("Landline 2 and Mobile No can't be same.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                return;
            }
            else {
                $("#txtLandline").removeClass('border-warning');
                $("#txtLandline2").removeClass('border-warning');
                $("#txtContactMobile").removeClass('border-warning');
            }
        }
        else {
            $("#txtLandline").removeClass('border-warning');
            $("#txtLandline2").removeClass('border-warning');
            $("#txtContactMobile").removeClass('border-warning');
        }
        if ($("#txtAlternateEmail").val() != '') {
            if ($("#txtAlternateEmail").val() == $("#txtEmail").val()) {
                $("#txtAlternateEmail").addClass('border-warning');
                $("#txtEmail").addClass('border-warning');
                toastr.error("Email and Alternate Email can't be same.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                return;
            }
            else {
                $("#txtAlternateEmail").removeClass('border-warning');
                $("#txtEmail").removeClass('border-warning');
            }
        }
        else {
            if ($("#txtEmail").val() == '') {
                $("#txtEmail").addClass('border-warning');
            }
            else {
                $("#txtAlternateEmail").removeClass('border-warning');
                $("#txtEmail").removeClass('border-warning');
            }
        }
        var data = {
            AutoId: $("#hiddenCustomerAutoId").val(),
            Type: $("#ddlType").val(),
            ContactPersonName: $("#txtContactName").val(),
            Mobile: $("#txtContactMobile").val(),
            Landline: $("#txtLandline").val(),
            Landline2: $("#txtLandline2").val(),
            Fax: $("#txtFax").val(),
            Email: $("#txtEmail").val(),
            AlternateEmail: $("#txtAlternateEmail").val(),
            IsDefault: IsDefault
        }
        $.ajax({
            type: "POST",
            url: "/Sales/WebAPI/customerMaster.asmx/saveContactDetail",
            data: "{'datavalue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json;charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d == "true") {
                    swal("", "Contact person detail saved successfully", "success");
                    $('#PopContactDetails').modal('hide');
                    GetContactPersonList();
                    $("#chkIsDefault").prop("checked", false);
                }
                else {
                    swal("Error", "Oops, Something went wrong.Please try later.", "error");
                }

            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
}

function EditContactPerson(e) {
    var tr = $(e).closest('tr');
    $('#hdnContactAutoId').val(tr.find('.AutoId').text());
    $('#ddlType').val(tr.find('.TypeAutoId').text()).change();
    $('#txtContactName').val(tr.find('.ContactPerson').text());
    $('#txtContactMobile').val(tr.find('.Mobile').text());
    $('#txtLandline').val(tr.find('.Landline').text());
    $('#txtLandline2').val(tr.find('.Landline2').text());

    if (tr.find('.IsDefault').text() == "Yes") {
        $("#chkIsDefault").prop("checked", true);
        $("#chkIsDefault").attr("disabled", 'disabled');
    }
    else {
        $("#chkIsDefault").prop("checked", false);
        $("#chkIsDefault").removeAttr("disabled");
    }

    $('#txtFax').val(tr.find('.Fax').text());
    $('#txtEmail').val(tr.find('.Email').text());
    $('#txtAlternateEmail').val(tr.find('.AltEmail').text());
    $('#PopContactDetails').modal('show');
    $('#btnContactUpdates').show();
    $('#btnContactSaves1').hide();
}


function checkSpecialCharacter(e) {
    debugger;
    //var $regexname = /^([a-zA-Z]{3,50})$/;
    //if (!$(e).val().match($regexname)) {
    //    toastr.error('Please enter valid name.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    //    $(e).focus();
    //    return 1;
    //}
    //else {
    //    return 0;
    //}
}

function MobileLength(e) {
    if ($(e).val().trim() != '') {
        if ($(e).val().length < 10) {
            toastr.error('Please enter a valid 10 digit number.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            $(e).focus();
        }
    }
}

function UpdateContactPersonDetails() {

    var IsDefault = 0;
    if ($("#chkIsDefault").prop("checked") == true) {
        IsDefault = 1;
    }
    if (ContactcheckRequiredField()) {

        if ($("#txtLandline").val().length < 10) {
            toastr.error('Please enter a valid 10 digit Landline No.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            $("#txtLandline").addClass('border-warning');
            return;
        }
        else if ($("#txtLandline2").val() != "") {
            if ($("#txtLandline2").val().length < 10) {
                $("#txtLandline").removeClass('border-warning');
                toastr.error('Please enter a valid 10 digit Landline No.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
                $("#txtLandline2").addClass('border-warning');
                return;
            }
        }
        else if ($("#txtContactMobile").val().length < 10) {
            $("#txtLandline2").removeClass('border-warning');
            toastr.error('Please enter a valid 10 digit Mobile No.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            $("#txtContactMobile").addClass('border-warning');
            return;
        }

        if ($("#txtLandline").val() == $("#txtContactMobile").val()) {
            if ($("#txtLandline2").val() != '') {
                if ($("#txtLandline2").val() == $("#txtContactMobile").val()) {
                    $("#txtLandline").addClass('border-warning');
                    $("#txtLandline2").addClass('border-warning');
                    $("#txtContactMobile").addClass('border-warning');
                    toastr.error("Landline 1, Landline 2 and Mobile No can't be same.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    return;
                }
                else if ($("#txtLandline").val() == $("#txtContactMobile").val()) {
                    $("#txtLandline").addClass('border-warning');
                    $("#txtContactMobile").addClass('border-warning');
                    $("#txtLandline2").removeClass('border-warning');
                    toastr.error("Landline 1 and Mobile No can't be same.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    return;
                }
            }
            else {
                $("#txtLandline").addClass('border-warning');
                $("#txtContactMobile").addClass('border-warning');
                toastr.error("Landline 2 and Mobile No can't be same.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                return;
            }
        }
        if ($("#txtLandline2").val() != '') {
            if ($("#txtLandline").val() == $("#txtLandline2").val()) {
                $("#txtLandline").addClass('border-warning');
                $("#txtLandline2").addClass('border-warning');
                toastr.error("Landline 1 and Landline 2 can't be same.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                return;
            }
            else if ($("#txtLandline2").val() == $("#txtContactMobile").val()) {
                $("#txtLandline2").addClass('border-warning');
                $("#txtContactMobile").addClass('border-warning');
                toastr.error("Landline 2 and Mobile No can't be same.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                return;
            }
            else {
                $("#txtLandline").removeClass('border-warning');
                $("#txtLandline2").removeClass('border-warning');
                $("#txtContactMobile").removeClass('border-warning');
            }
        }
        else {
            $("#txtLandline").removeClass('border-warning');
            $("#txtLandline2").removeClass('border-warning');
            $("#txtContactMobile").removeClass('border-warning');
        } if ($("#txtAlternateEmail").val() != '') {
            if ($("#txtAlternateEmail").val() == $("#txtEmail").val()) {
                $("#txtAlternateEmail").addClass('border-warning');
                $("#txtEmail").addClass('border-warning');
                toastr.error("Email and Alternate Email can't be same.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                return;
            }
            else {
                $("#txtAlternateEmail").removeClass('border-warning');
                $("#txtEmail").removeClass('border-warning');
            }
        }
        else {
            $("#txtAlternateEmail").removeClass('border-warning');
            $("#txtEmail").removeClass('border-warning');
        }
        var data = {
            AutoId: $("#hdnContactAutoId").val(),
            Type: $("#ddlType").val(),
            ContactPersonName: $("#txtContactName").val(),
            Mobile: $("#txtContactMobile").val(),
            Landline: $("#txtLandline").val(),
            Landline2: $("#txtLandline2").val(),
            Fax: $("#txtFax").val(),
            Email: $("#txtEmail").val(),
            AlternateEmail: $("#txtAlternateEmail").val(),
            IsDefault: IsDefault
        }
        console.log(data);
        $.ajax({
            type: "POST",
            url: "/Sales/WebAPI/customerMaster.asmx/updateContactDetail",
            data: "{'datavalue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json;charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d == "true") {
                    swal("", "Contact person details updated successfully", "success");
                    $('#PopContactDetails').modal('hide');
                    GetContactPersonList();
                }
                else {
                    swal("Error", "Oops, Something went wrong.Please try later.", "error");
                }

            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
}

function deleterecordContact(autoid) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this contact person",
        icon: "warning",
        showCancelButton: true,
        allowOutsideClick: false,
        closeOnClickOutside: false,
        buttons: {
            cancel: {
                text: "No, cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, delete it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(isConfirm => {
        if (isConfirm) {
            deleteCustomerContactPerson(autoid);
        }
    })
}

function deleteCustomerContactPerson(autoid) {
    var data = {
        AutoId: autoid
    }
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/customerMaster.asmx/deleteCustomerContactPerson",
        data: "{'datavalue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            console.log(response);
            if (response.d == "true") {
                swal("", "Contact person deleted successfully", "success");
                GetContactPersonList();
            }
            else {
                swal("", "Oops, Something went wrong.Please try later.", "error");
            }


        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}

function ContactcheckRequiredField() {
    debugger
    var boolcheck = true;
    $('.creq').each(function () {

        if ($(this).val().trim() == '') {
            boolcheck = false;
            $(this).addClass('border-warning');

        } else {
            $(this).removeClass('border-warning');
        }
    });
    $('.ddlcreq').each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == '-Select-' || $(this).val() == null) {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });
    if ($("#txtLandline").val().length < 10) {
        $("#txtLandline").addClass('border-warning');
    }
    if ($("#txtLandline2").val() != "") {
        if ($("#txtLandline2").val().length < 10) {
            $("#txtLandline2").addClass('border-warning');
        }
    }
    if ($("#txtContactMobile").val().length < 10) {
        $("#txtContactMobile").addClass('border-warning');
    }
    if ($("#txtLandline").val() == $("#txtContactMobile").val()) {
        if ($("#txtLandline2").val() != '') {
            if ($("#txtLandline2").val() == $("#txtContactMobile").val()) {
                $("#txtLandline").addClass('border-warning');
                $("#txtLandline2").addClass('border-warning');
                $("#txtContactMobile").addClass('border-warning');
            }
            else {
                $("#txtLandline").addClass('border-warning');
                $("#txtLandline2").addClass('border-warning');
            }
        }
        else {
            $("#txtLandline").addClass('border-warning');
            $("#txtContactMobile").addClass('border-warning');
        }
    }
    if ($("#txtLandline2").val() != '') {
        if ($("#txtLandline").val() == $("#txtLandline2").val()) {
            $("#txtLandline").addClass('border-warning');
            $("#txtLandline2").addClass('border-warning');
        }
        else if ($("#txtLandline2").val() == $("#txtContactMobile").val()) {
            $("#txtLandline2").addClass('border-warning');
            $("#txtContactMobile").addClass('border-warning');
        }
    }
    if ($("#txtAlternateEmail").val() != '') {
        if ($("#txtAlternateEmail").val() == $("#txtEmail").val()) {
            $("#txtAlternateEmail").addClass('border-warning');
            $("#txtEmail").addClass('border-warning');
        }
        else {

            if ($("#txtEmail").val() == '') {
                $("#txtEmail").addClass('border-warning');
            }
            else {
                $("#txtAlternateEmail").removeClass('border-warning');
                $("#txtEmail").removeClass('border-warning');
            }
        }
    }
    else {
        if ($("#txtEmail").val() == '') {
            $("#txtEmail").addClass('border-warning');
        }
        else {
            $("#txtAlternateEmail").removeClass('border-warning');
            $("#txtEmail").removeClass('border-warning');
        }
    }
    if ($("#txtLandline").val().length == 10 && $("#txtLandline2").val() == 10 && $("#txtContactMobile").val().length == 10) {
        $("#txtLandline").removeClass('border-warning');
        $("#txtLandline2").removeClass('border-warning');
        $("#txtContactMobile").removeClass('border-warning');
    }
    return boolcheck;
}
