﻿var getQueryString = function (field, url) {
    var href = url ? url : window.location.href;
    var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
    var string = reg.exec(href);
    return string ? string[1] : null;
};

$(document).ready(function () {
    var d = new Date();
    $('#txtReceiveDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtSFromDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtSToDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    var month = d.getMonth() + 1;
    if (month.toString().length == 1) {
        month = '0' + month;
    }
    var day = d.getDate();
    if (day.toString().length == 1) {
        day = '0' + day;
    }
    var year = d.getFullYear();
    $("#txtReceiveDate").val(month + '/' + day + '/' + year);
    $("#txtSFromDate").val(month + '/' + day + '/' + year);
    $("#txtSToDate").val(month + '/' + day + '/' + year);
    bindStatus();
    getPayList(1);
    if (getQueryString('PageId') != null) {
        $("#ddlCustomer").val(getQueryString('PageId')).change();
        $("#ddlCustomer").attr('disabled', true)
    }


})
function SavePayAmount() {
  var OrderAutoId = 0;
    var RetailerId = [];
  $('input:checkbox:checked', '#tblOrderList tbody').each(function (index, item) {
        var row = $(item).closest('tr');
        OrderAutoId = row.find('.AutoId').text(),
            RetailerId.push(OrderAutoId);
    });
    var AutoId = RetailerId.join(",");

        
    var amt = $("#txtReceiveAmount").val();
    amt = amt.split('.');
    if (Number(amt[1]) > 0) {
        toastr.error('Please enter a valid amount. Cents not allowed.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $("#txtReceiveAmount").addClass('border-warning');
    }
    else {
debugger;
        if (paycheckRequiredField()) {
            var data = {
                OrderAutoId: AutoId,
                CustomerAutoId: $('#ddlCustomer').val(),
                ReceiveDate: $("#txtReceiveDate").val(),
                ReceiveAmount: $("#txtReceiveAmount").val(),
                PaymentMode: $("#ddlPaymentMode").val(),
                ReferenceId: $("#ReferenceId").val(),
                Remarks: $("#txtRemarks").val()
            }
            $.ajax({
                type: "Post",
                url: "/Sales/WebAPI/WPayReceiveMaster.asmx/savePayment",
                data: JSON.stringify({ dataValue: JSON.stringify(data) }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.d == 'Session Expired') {
                        location.href = '/';
                    } else if (response.d == 'true') {
                        getPayList(1);

                        swal("", "Payment received successfully.", "success").then(function () {
                            resetData();
                            
                        });

                    } else {
                        swal("Error!", response.d, "error");
                    }
                },
                error: function (result) {
                    swal("Error!", result.d, "error");
                },
                failure: function (result) {
                    swal("Error!", result.d, "error");
                }
            });
        }
        else {
            toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        }
    }
}
function resetData() {
    $("#check-all").prop('checked', false);
    $("#hdnorderlist").hide();
    $('#ddlCustomer').val(0).change();
    $("#txtReceiveDate").val('');
    $("#txtPayId").val('');
    $("#txtReceiveAmount").val('0.00');
    $("#ddlPaymentMode").val(0);
    $("#ReferenceId").val('');
    $("#txtRemarks").val('');
    $('#btnSave').show();
    $('#btnReset').show();
    $('#btnUpdate').hide();
    $('#btnCancel').hide();
    if (getQueryString('PageId') != null) {
        location.href = '/Sales/PayMaster.aspx';
    }
    $('#ddlCustomer').closest('div').find('.select2-selection--single').removeAttr('style');
    $(".req").removeClass('border-warning');
    $(".ddlsreq").removeClass('border-warning');
    var d = new Date();
    CurrentDate = (d.getMonth() + 1) + "/" + d.getDate() + "/" + d.getFullYear();
    $('#txtReceiveDate').val(CurrentDate);
}

function Pagevalue(e) {
    getOrderList(parseInt($(e).attr("page")));
};
function bindStatus() {
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/WPayReceiveMaster.asmx/bindStatus",
        data: "{}",
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },

        success: function (response) {

            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var SStatus = $(xmldoc).find("Table");
                $("#ddlSStatus option:not(:first)").remove();
                $.each(SStatus, function () {
                    $("#ddlSStatus").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("StatusType").text() + "</option>");
                });
                var customer = $(xmldoc).find("Table1");
                $("#ddlCustomer option:not(:first)").remove();
                $.each(customer, function () {
                    $("#ddlCustomer").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("Customer").text() + "</option>");
                });
                $("#ddlCustomer").select2();
                $("#ddlSCustomer option:not(:first)").remove();
                $.each(customer, function () {
                    $("#ddlSCustomer").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("Customer").text() + "</option>");
                });
                $("#ddlSCustomer").select2();
                var PaymentType = $(xmldoc).find("Table2");
                $("#ddlPaymentMode option:not(:first)").remove();
                $.each(PaymentType, function () {
                    $("#ddlPaymentMode").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("PaymentMode").text() + "</option>");
                });
            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function getPayList(pageIndex) {
    var data = {
        PayId: $("#txtSPayId").val().trim(),
        CustomerAutoId: $("#ddlSCustomer").val().trim(),
        PaymentMode: $("#ddlPaymentMode").val(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        Status: $("#ddlSStatus").val(),
        PageSize: $("#ddlPagesize").val(),
        pageIndex: pageIndex
    };

    $.ajax({
        type: "POST",
        async: false,
        url: "/Sales/WebAPI/WPayReceiveMaster.asmx/getPayList",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }), 
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            }); 
        },

        complete: function () {
            $.unblockUI(); 
        },
        success: function (response) {

            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var PayDetails = $(xmldoc).find("Table1");

                $("#tblPayDetails tbody tr").remove();
                var row = $("#tblPayDetails thead tr").clone(true);

                if (PayDetails.length > 0) {
                    $("#EmptyTable").hide();
                    $.each(PayDetails, function () {
                        if ($(this).find("StatusCode").text() == 1) {
                            $(".action", row).html("<a title='Edit' href='#' onclick='fnEdit(" + $(this).find("PaymentAutoId").text() + ")'><span class='ft-edit'></span></a>&nbsp;&nbsp;&nbsp;&nbsp;<a title='Delete' href='#;'><span class='ft-x' onclick='deleterecord(\"" + $(this).find("PaymentAutoId").text() + "\")'></span><b></a></b>");
                        } else {
                            $(".action", row).html("");
                        }
                        $(".PayID", row).html("<span PaymentAutoId='" + $(this).find("PaymentAutoId").text() + "'>" + $(this).find("PayId").text() + "</span>");
                        $(".ReceiveDate", row).text($(this).find("ReceivedDate").text());
                        $(".CustomerName", row).text($(this).find("CustomerName").text());
                        $(".ReceivedAmount", row).text($(this).find("ReceivedAmount").text());
                        $(".PaymentMode", row).text($(this).find("PaymentMode").text());
                        $(".ReferenceId", row).html('<p style="width:150px !Important;word-break: break-all;;white-space: normal;text-align:justify">' +$(this).find("ReferenceId").text()+ '</p>');
                        $(".Status", row).text($(this).find("Status").text());
                        $(".Remarks", row).html('<p style="width:200px !Important;word-break: break-all;;white-space: normal;text-align:justify">' + $(this).find("Remarks").text() + '</p>');
                        if ($(this).find("StatusCode").text() == '1') {
                            $(".Status", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("Status").text() + "</span>");
                        } else {
                            $(".Status", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("Status").text() + "</span>");
                        }
                        $("#tblPayDetails tbody").append(row);
                        row = $("#tblPayDetails tbody tr:last").clone(true);
                    });
                }

                var pager = $(xmldoc).find("Table");
                $(".Pager").ASPSnippets_Pager({
                    ActiveCssClass: "current",
                    PagerCssClass: "pager",
                    PageIndex: parseInt(pager.find("PageIndex").text()),
                    PageSize: parseInt(pager.find("PageSize").text()),
                    RecordCount: parseInt(pager.find("RecordCount").text())
                });
            } else {
                location.href = "/";
            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function fnEdit(PaymentAutoid) {
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/WPayReceiveMaster.asmx/Editpay",
        data: "{'PaymentAutoid':'" + PaymentAutoid + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == 'Session Expired') {
                location.href = '/';
            } else {debugger;


                var xmldoc = $.parseXML(response.d);
                var PayDetails = $(xmldoc).find("Table"); 


                $('#PaymentAutoId').val($(PayDetails).find('PaymentAutoId').text());
                $('#txtPayId').val($(PayDetails).find('PayId').text());
                $('#ddlCustomer').val($(PayDetails).find('CustomerAutoId').text()).select2();
                $('#txtReceiveDate').val($(PayDetails).find('ReceivedDate').text());
                $('#txtReceiveAmount').val($(PayDetails).find('ReceivedAmount').text());
                $('#ddlPaymentMode').val($(PayDetails).find('PaymentMode').text());
                $('#ReferenceId').val($(PayDetails).find('ReferenceId').text());
                $('#txtRemarks').val($(PayDetails).find('Remarks').text());
                $('#btnSave').hide();
                $('#btnReset').hide();
                $('#btnUpdate').show();
                $('#btnCancel').show();



 var OrderList = $(xmldoc).find("Table1");
       var OrderList1 = $(xmldoc).find("Table2");
                
                $("#tblOrderList tbody tr").remove();
                var rowdue = $("#tblOrderList thead tr").clone(true);
                var check = 0;
               
               if (OrderList.length > 0) {
                    $(OrderList).each(function (index) {
                        if (parseFloat($(this).find('AmtDue').text()) > 0) {
                               $("#hdnorderlist").show();
debugger;
var checked='';
var ord=$(this).find("AutoId").text();
$(OrderList1).each(function (index) {debugger;
if($(this).find("AutoId").text()==ord){
checked='checked';
}
});

                            $(".SrNo", rowdue).html("<input type='checkbox' "+checked+" name='table_recordas' id='chk1Product'/>");
                            $(".AutoId", rowdue).text($(this).find("AutoId").text());
                            $(".OrderNo", rowdue).text($(this).find('OrderNo').text());
                            $(".PaidAmount", rowdue).text($(this).find('AmtPaid').text());
                            $(".Status", rowdue).text($(this).find('StatusType').text());
                            $(".OrderDate", rowdue).text($(this).find('OrderDate').text());
                            $(".AmtDue", rowdue).html($(this).find('AmtDue').text());
                            $(".OrderAmount", rowdue).html($(this).find('GrandTotal').text());
                          
                            $('#tblOrderList tbody').append(rowdue);
                            rowdue = $("#tblOrderList tbody tr:last").clone(true);
                            check = check + 1;
                        }else{
                         $("#hdnorderlist").hide();
                           }
                    })
                   
                }
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function UpdatePayAmount() {

     var OrderAutoId = 0;
    var RetailerId = [];
  $('input:checkbox:checked', '#tblOrderList tbody').each(function (index, item) {
        var row = $(item).closest('tr');
        OrderAutoId = row.find('.AutoId').text(),
            RetailerId.push(OrderAutoId);
    });
    var AutoId = RetailerId.join(",");
var amt = $("#txtReceiveAmount").val();
    amt = amt.split('.');
    if (Number(amt[1]) > 0) {
        toastr.error('Please enter a valid amount.Cents not allowed.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $("#txtReceiveAmount").addClass('border-warning');
    }
    else {
        if (paycheckRequiredField()) {
            var data = {
                OrderAutoId: AutoId,
                PaymentAutoId: $('#PaymentAutoId').val(),
                CustomerAutoId: $('#ddlCustomer').val(),
                ReceiveDate: $("#txtReceiveDate").val(),
                ReceiveAmount: $("#txtReceiveAmount").val(),
                PaymentMode: $("#ddlPaymentMode").val(),
                ReferenceId: $("#ReferenceId").val(),
                Remarks: $("#txtRemarks").val()
            }
            $.ajax({
                type: "Post",
                url: "/Sales/WebAPI/WPayReceiveMaster.asmx/updatePayment",
                data: JSON.stringify({ dataValue: JSON.stringify(data) }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.d == 'Session Expired') {
                        location.href = '/';
                    } else {
                        if (response.d == 'true') {
                            resetData();
                            getPayList(1);
                            swal("", "Payment update successfully.", "success");

                        } else {
                            swal("Error!", response.d, "error");
                        }
                    }
                },
                error: function (result) {
                    swal("Error!", result.d, "error");
                },
                failure: function (result) {
                    swal("Error!", result.d, "error");
                }
            });
        }
        else {
            toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        }
    }
}

function deleteOrder(PaymentAutoId) {


    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/WPayReceiveMaster.asmx/DeletePay",
        data: "{'PaymentAutoId':'" + PaymentAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == 'Session Expired') {
                location.href = '/';
            } else {
                if (response.d == 'true') {
                    getPayList(1);
                    swal("", "Payment deleted successfully.", "success");
                } else {
                    swal("Error !", response.d, "error");
                }
            }
        },
        failure: function (result) {
            swal("Error !", "Oops! Something went wrong.Please try later.", "error");
        },
        error: function (result) {
            swal("Error !", "Oops! Something went wrong.Please try later.", "error");
        }
    });
}

$("#check-all").change(function () {
    if ($(this).prop("checked")) {
        $("#tblOrderList input:checkbox:not(:disabled)").prop('checked', $(this).prop("checked"));
    }
    else {
        $("#tblOrderList input:checkbox").prop('checked', $(this).prop("checked"));
    }
 })

function Pagevalue(e) {
    getPayList(parseInt($(e).attr("page")));
};

function ViewDueAmount() {
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/WPayReceiveMaster.asmx/ViewDueAmount",
        data: "{'CustomerAutoId':'" + $("#ddlCustomer").val().trim() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == 'Session Expired') {
                location.href = '/';
            } else {
                var xmldoc = $.parseXML(response.d);
                var DuePayment = $(xmldoc).find("Table");
                var OrderList = $(xmldoc).find("Table1");
                $("#tblduePayment tbody tr").remove();
                var rowdue = $("#tblduePayment thead tr").clone(true); 
                $("#tblOrderList tbody tr").remove();
                var rowdue = $("#tblOrderList thead tr").clone(true);
                var check = 0;
                var DueAmount = 0;
                //var T_OrderAmt = 0;
                var T_DueAmt = 0;
                //var T_PaidAmt = 0;

               $("#hdnorderlist").hide();
               if (OrderList.length > 0) {
                    $(OrderList).each(function (index) {
                        if (parseFloat($(this).find('AmtDue').text()) > 0) {
                               $("#hdnorderlist").show();
                            $(".SrNo", rowdue).html("<input type='checkbox' name='table_recordas' id='chk1Product'/>");
                            $(".AutoId", rowdue).text($(this).find("AutoId").text());
                            $(".OrderNo", rowdue).text($(this).find('OrderNo').text());
                            $(".PaidAmount", rowdue).text($(this).find('AmtPaid').text());
                            $(".Status", rowdue).text($(this).find('StatusType').text());
                            $(".OrderDate", rowdue).text($(this).find('OrderDate').text());
                            $(".AmtDue", rowdue).html($(this).find('AmtDue').text());
                            $(".OrderAmount", rowdue).html($(this).find('GrandTotal').text());
                          
                            $('#tblOrderList tbody').append(rowdue);
                            rowdue = $("#tblOrderList tbody tr:last").clone(true);
                            check = check + 1;
                             //T_OrderAmt = T_OrderAmt + parseInt($(this).find("GrandTotal").text());
                             T_DueAmt = T_DueAmt + parseInt($(this).find("AmtDue").text());
                             //T_PaidAmt = T_PaidAmt + parseInt($(this).find("AmtPaid").text());
                             //$('#T_OrderAmt').html(T_OrderAmt.toFixed(2));
                             $('#T_DueAmt').html(T_DueAmt.toFixed(2));
                             //$('#T_PaidAmt').html(T_PaidAmt.toFixed(2));
                             
                        }else{
                         $("#hdnorderlist").hide();
                           }
                    })
                   
                }
                if (DuePayment.length > 0) {
                    $(DuePayment).each(function (index) {
                        if (parseFloat($(this).find('AmtDue').text()) > 0) {
                            $(".SrNo", rowdue).text(Number(check) + 1);
                            $(".OrderNo", rowdue).text($(this).find('OrderNo').text());
                            $(".OrderDate", rowdue).text($(this).find('OrderDate').text());
                            $(".AmtDue", rowdue).html($(this).find('AmtDue').text());
                            DueAmount += parseFloat($(this).find('AmtDue').text());
                            $('#tblduePayment tbody').append(rowdue);
                            rowdue = $("#tblduePayment tbody tr:last").clone(true);
                            check = check + 1;
                        }
                    })
                    $("#spDueAmount").attr('onclick', 'ViewDue()');
                }
                else {
                    $("#spDueAmount").removeAttr('onclick');
                }
                $('#txtDueAmount').val(parseFloat(DueAmount).toFixed(2))
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function ViewDue() {
    $('#CustomerDueAmount').modal('show');
    $("#spCustomerName").html($("#ddlCustomer option:selected").text());
}

function paycheckRequiredField() {
    var boolcheck = true;
    $('.req').each(function () {
        if ($(this).val() == '' || parseFloat($(this).val()) == 0) {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });
    $('.ddlsreq').each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == 'undefined' || $(this).val() == 'Select') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });

    $('.ddlreq').each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == 'undefined' || $(this).val() == 'Select') {
            boolcheck = false;
            $(this).closest('div').find('.select2-selection--single').attr('style', 'border:1px solid #FF9149  !important');
        } else {
            $(this).closest('div').find('.select2-selection--single').removeAttr('style');
        }
    });
    return boolcheck;
}
function deleterecord(PaymentAutoId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this payment.",
        icon: "warning",
        showCancelButton: true,
        allowOutsideClick: false,
        closeOnClickOutside: false,

        buttons: {
            cancel: {
                text: "No, cancel.",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, delete it.",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            deleteOrder(PaymentAutoId);
        } 
    })
}

 