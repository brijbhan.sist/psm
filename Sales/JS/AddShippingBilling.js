﻿var CusAutoId
$(document).ready(function () {
    $('.date').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    var getid = getQueryString('PageId');
    CusAutoId = getid;
    if (getid != null) {
    }


});

var getQueryString = function (field, url) {
    var href = url ? url : window.location.href;
    var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
    var string = reg.exec(href);
    return string ? string[1] : null;
};
function OpenBilling() {
    $("#IsDefault").prop('checked', false);
    $("#txtBaddress1").val('');
    $("#txtBaddress2").val('');
    $("#txtBcity").val('');
    $("#txtBstate").val('');
    $("#txtBzipcode").val('');
    $("#txtBLat").val('');
    $("#txtBLong").val('');
    $("#btnBUpdates").hide();
    $("#btnBSaves").show();

    $('#PopBillingDetails').modal('show');
}
function resetBilling() {
    $("#IsDefault").prop('checked', false);
    $("#txtBaddress1").val('');
    $("#txtBaddress2").val('');
    $("#txtBcity").val('');
    $("#txtBstate").val('');
    $("#txtBzipcode").val('');
    $("#txtBLat").val('');
    $("#txtBLong").val('');
    $("#btnSUpdates").hide();
    $("#btnSSaves").show();
  //  $('#PopBillingDetails').modal('hide');
}
function OpenShiping() {
    $("#IsSDefault").prop('checked', false);
    $("#txtSaddress1").val('');
    $("#txtSaddress2").val('');
    $("#txtScity").val('');
    $("#txtSstate").val('');
    $("#txtSZipcode").val('');
    $("#txtSLat").val('');
    $("#txtSLong").val('');
    $("#btnSUpdates").hide();
    $("#btnSSaves").show();
    $('#PopShippingDetails').modal('show');
}

function resetShipping() {
    $("#IsSDefault").prop('checked', false);
    $("#txtSaddress1").val('');
    $("#txtSaddress2").val('');
    $("#txtScity").val('');
    $("#txtSstate").val('');
    $("#txtSZipcode").val('');
    $("#txtSLat").val('');
    $("#txtSLong").val('');
   // $('#PopShippingDetails').modal('hide');
}
function setBillingData(place) {
    debugger
    $(".hiddenBillAddress").html(place.adr_address);
    $("#txtBaddress1").val($(".hiddenBillAddress").find(".street-address").text());
    var m = 0, check=0;
    for (var i = 0; i < place.address_components.length; i++) {
        for (var j = 0; j < place.address_components[i].types.length; j++) {
            if (place.address_components[i].types[j] == "postal_code") {
                $("#txtBzipcode").val(place.address_components[i].long_name);
                zcode = place.address_components[i].long_name;
            } 
            else if (place.address_components[i].types[j] == "administrative_area_level_1") {
                $("#txtBstate").val(place.address_components[i].long_name);
                state = place.address_components[i].long_name;
            }
            else if (place.address_components[i].types[j] == "locality") {
                $("#txtBcity").val(place.address_components[i].long_name);
                city = place.address_components[i].long_name;
                check = 1;
            }
            if (place.address_components[i].types[j] == "neighborhood" && check == 0) {
                $("#txtBcity").val(place.address_components[i].long_name);
                city = place.address_components[i].long_name;
                check = 1;
            }
            if (place.address_components[i].types[j] == "administrative_area_level_3" && check == 0) {
                $("#txtBcity").val(place.address_components[i].long_name);
                city = place.address_components[i].long_name;
            }
            //if (check == 0) {
            //    if (place.address_components[i].types[j] == "locality") {
            //        $("#txtBcity").val(place.address_components[i].long_name);
            //        city = place.address_components[i].long_name;
            //    }
            //    else if (place.address_components[i].types[j] == "administrative_area_level_3") {
            //        $("#txtBcity").val(place.address_components[i].long_name);
            //        city = place.address_components[i].long_name;
            //    }
            //}
        }
        m++;
    }
    if (m > 4) {
        checkState(state, city, zcode, 1);
        $("#txtBLat").val(place.geometry.location.lat);
        $("#txtBLong").val(place.geometry.location.lng);
    }
    else {
        swal("", "Invalid address !", "error");
        resetBilling();
    }

}
/*------------------------------------------------------Set Shipping Address------------------------------------------------------------*/

function setData(place) {
    debugger
    $(".hiddenShipAddress").html(place.adr_address);
    var textdata = $(".hiddenShipAddress").find(".street-address").text();
    $("#txtSaddress1").val(textdata);
    var m = 0;
    for (var i = 0; i < place.address_components.length; i++) {
        for (var j = 0; j < place.address_components[i].types.length; j++) {
            if (place.address_components[i].types[j] == "postal_code") {
                $("#txtSZipcode").val(place.address_components[i].long_name);
                zcode = place.address_components[i].long_name;
            } 
            else if (place.address_components[i].types[j] == "administrative_area_level_1") {
                $("#txtSstate").val(place.address_components[i].long_name);
                state = place.address_components[i].long_name;
            } else if (place.address_components[i].types[j] == "locality") {
                $("#txtScity").val(place.address_components[i].long_name);
                city = place.address_components[i].long_name;
                check = 1;
            }
            if (place.address_components[i].types[j] == "neighborhood" && check == 0) {
                $("#txtScity").val(place.address_components[i].long_name);
                city = place.address_components[i].long_name;
                check = 1;
            }
            if (place.address_components[i].types[j] == "administrative_area_level_3" && check == 0) {
                $("#txtScity").val(place.address_components[i].long_name);
                city = place.address_components[i].long_name;
            }
            //if (check == 0) {
            //    if (place.address_components[i].types[j] == "locality") {
            //        $("#txtScity").val(place.address_components[i].long_name);
            //        city = place.address_components[i].long_name;
            //    }
            //    else if (place.address_components[i].types[j] == "administrative_area_level_3") {
            //        $("#txtScity").val(place.address_components[i].long_name);
            //        city = place.address_components[i].long_name;
            //    }
            //}
        }
        m++;
    }
    if (m > 4) {
        checkState(state, city, zcode, 2);
        $("#txtSLat").val(place.geometry.location.lat);
        $("#txtSLong").val(place.geometry.location.lng);
    }
    else {
        swal("", "Invalid address !", "error");
    }

}

function SaveBilling() {
    var latitudeship = $("#txtBLat").val();
    var longitudeship = $("#txtBLong").val();
    var IsDefault = "";
    if (($("#IsDefault").prop("checked"))) {
        IsDefault = 1;
    }
    else {
        IsDefault = 0;
    }
   

    if (BcheckRequiredField()) {
        if (latitudeship == "" || longitudeship == "") {
            toastr.error('Invalid address.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            return false;
        }
        else if ($("#txtLat1").val() == "" || $("#txtLong1").val() == "" || $("#txtBzipcode").val() == "" || $("#txtBcity").val() == "" || $("#txtBstate").val()=="") {
            toastr.error('Invalid address..', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            return false;
        }
        var data = {
            CustomerAutoId: AutoId,
            BillAdd: $("#txtBaddress1").val(),
            BillAdd2: $("#txtBaddress2").val(),
            Zipcode: $("#txtBzipcode").val(),
            City: $("#txtBcity").val(),
            IsDefault: IsDefault,
            State: $("#txtBstate").val(),
            latitude: $("#txtBLat").val(),
            longitude: $("#txtBLong").val()
        };
        $.ajax({
            type: "POST",
            url: "ViewCustomerDetails.aspx/InsertBilling",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d != "Session Expired") {
                    if (result.d == "Success") {
                        swal("", "Address details saved successfully.", "success")
                        resetBilling();
                        CustomerBillingDetails();
                        $('#PopBillingDetails').modal('hide');
                    }
                    else if (result.d == "invalid state")
                    {
                        swal("", "Invalid state (" + $("#txtBstate").val()+")", "error");
                    }
                    else if (result.d == "ZipCode") {
                        swal("", "Invalid Zip Code : " + $('#txtBzipcode').val(), "error");
                    } else if (result.d == "City") {
                        swal("", "Invalid Zip Code : " + $('#txtBcity').val(), "error");

                    }
                   
                    else {
                        swal("Error!", result.d, "error");
                    }

                } else {
                    location.href = '/';
                }

            },
            error: function (result) {
                swal("Error!", "Oops! Something went wrong.Please try later.", "error");
            },
            failure: function (result) {
                swal("Error!", "Oops! Something went wrong.Please try later.", "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
}

function CustomerBillingDetails() {
    var data = {
        CustomerAutoId: AutoId,
    }
    $.ajax({
        type: "POST",
        url: "ViewCustomerDetails.aspx/GetBilling",
        data: "{'datavalue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var Billing = $(xmldoc).find('Table');

                if (Billing.length > 0) {
                    $('#tblBillingDetails tbody tr').remove();
                    var row = $('#tblBillingDetails thead tr').clone(true);
                    $.each(Billing, function () {
                        $(".Id", row).html($(this).find("AutoId").text());
                        $(".Address", row).html($(this).find("CompleteBillingAddress").text());
                        if ($(this).find("IsDefault").text() == "1") {
                            $(".IsBDefault", row).html('<span class="badge badge badge-pill badge-success mr-2" style="text-transform: capitalize;">yes</span>');

                        }
                        else {
                            $(".IsBDefault", row).html('<span class="badge badge badge-pill badge-success mr-2" style="text-transform: capitalize;">no</span>');

                        }

                        if ($(this).find("IsDefault").text() == "1") {
                            $(".BAction", row).html("<a title='Edit' href='#'><span class='ft-edit' onclick='editbilling(\"" + $(this).find("AutoId").text() + "\")' /></a>");

                        }
                        else {
                            $(".BAction", row).html("<a title='Edit' href='#'><span class='ft-edit' onclick='editbilling(\"" + $(this).find("AutoId").text() + "\")' /></a>&nbsp;&nbsp;<a title='Delete' href='#'><span class='ft-x' onclick= 'DeleteBillingAddress(\"" + $(this).find("AutoId").text() + "\")' /></a>");

                        }
                        $("#tblBillingDetails tbody").append(row);
                        row = $("#tblBillingDetails tbody tr:last-child").clone(true);
                    });
                }
            }
        }
    });
}

function editbilling(AutoId) {

    $("#hdnBAutoId").val(AutoId);
    $.ajax({
        type: "POST",
        url: "ViewCustomerDetails.aspx/EditBilling",
        data: "{'AutoId':'" + AutoId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: onSuccessOfEdit,
        error: function (response) {
            console.log(response.d)
        }
    })
}
function onSuccessOfEdit(response) {
    if (response.d == 'Session Expired') {
        location.href = '/';
    } else {
        var xmldoc = $.parseXML(response.d);
        var billing = $(xmldoc).find('Table');
       
        $("#txtBaddress1").val($(billing).find("Address").text());
        $("#txtBaddress2").val($(billing).find("Address2").text());
        $("#txtBcity").val($(billing).find("City").text());
        $("#txtBstate").val($(billing).find("StateName").text());
        $("#txtBzipcode").val($(billing).find("Zipcode").text());
        $("#txtBLat").val($(billing).find("BillSA_Lat").text());
        $("#txtBLong").val($(billing).find("BillSA_Long").text());
        if ($(billing).find("IsDefault").text() == "1") {
            $("#IsDefault").prop('checked', true);
            $("#IsDefault").attr('disabled','disabled');
        }
        else {
            $("#IsDefault").removeAttr('disabled');
            $("#IsDefault").prop('checked', false);
        }
        $("#PopBillingDetails").modal("show")
        $("#btnBSaves").hide();       
        $("#btnBUpdates").show();
      
    }
}

function deleteBilling(AutoId) {

    $.ajax({
        type: "POST",
        url: "ViewCustomerDetails.aspx/DeleteBilling",
        data: "{'AutoId':'" + AutoId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (result) {
            if (result.d == "Session Expired") {
                window.location = "/";
            }
            else if (result.d == 'Success') {
                swal("", "Billing Address deleted successfully.", "success");
                CustomerBillingDetails();
            } else {
                swal("Error!", result.d, "error");
            }
        },
        error: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later", "error");

        },
        failure: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later", "error");
        }
    })
}

function DeleteBillingAddress(AutoId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this Billing Address!",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: false,
            },
            confirm: {
                text: "Yes, delete it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(isConfirm => {
        if (isConfirm) {
            deleteBilling(AutoId);

        } else {
            swal("", "Your Address is safe.", "error");
        }
    })
}
function UpdateBilling() {
    var latitudeship = $("#txtBLat").val();
    var longitudeship = $("#txtBLong").val();
    var IsDefault = "";
    if (($("#IsDefault").prop("checked"))) {
        IsDefault = 1;
    }
    else {
        IsDefault = 0;
    }    

    if (BcheckRequiredField()) {
        if (latitudeship == "" || longitudeship == "") {
            toastr.error('Invalid address.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            return false;
        } else if ($("#txtLat1").val() == "" || $("#txtLong1").val() == "" || $("#txtBzipcode").val() == "" || $("#txtBcity").val() == "" || $("#txtBstate").val() == "") {
            toastr.error('Invalid address..', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            return false;
        }
        var data = {
            AutoId: $("#hdnBAutoId").val(),
            CustomerAutoId: AutoId,
            BillAdd: $("#txtBaddress1").val(),
            BillAdd2: $("#txtBaddress2").val(),
            Zipcode: $("#txtBzipcode").val(),
            City: $("#txtBcity").val(),
            IsDefault: IsDefault,
            State: $("#txtBstate").val(),
            latitude: $("#txtBLat").val(),
            longitude: $("#txtBLong").val()
        };
        $.ajax({
            type: "POST",
            url: "ViewCustomerDetails.aspx/UpdateBilling",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d != "Session Expired") {
                    if (result.d == "Success") {
                        swal("", "Address details updated successfully.", "success")
                        resetBilling();
                        CustomerBillingDetails();
                        $('#PopBillingDetails').modal('hide');
                    }
                    else if (result.d == "Invalid state") {
                      swal("", "Invalid state (" + $("#txtBstate").val() + ")", "error");
                    } else if (result.d == "ZipCode") {
                        swal("Error!", "Invalid Zip Code : " + $('#txtBzipcode').val(), "error");
                    } else if (result.d == "City") {
                        swal("Error!", "Invalid Zip Code : " + $('#txtBcity').val(), "error");

                    }
                    
                    else {
                        swal("Error!", result.d, "error");
                    }

                } else {
                    location.href = '/';
                }

            },
            error: function (result) {
                swal("Error!", "Oops! Something went wrong.Please try later.", "error");
            },
            failure: function (result) {
                swal("Error!", "Oops! Something went wrong.Please try later.", "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
}
function BcheckRequiredField() {
    var boolcheck = true;
    $('.Bireq').each(function () {
        if ($(this).val().trim() == '' || $(this).val().trim() == '0' || $(this).val().trim() == '0.00') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });

    return boolcheck;
}
function SaveShipping() {
    var latitudeship = $("#txtSLat").val();
    var longitudeship = $("#txtSLong").val();
    var IsDefault = "";
    if (($("#IsSDefault").prop("checked"))) {
        IsDefault = 1;
    }
    else {
        IsDefault = 0;
    }   

    if (ScheckRequiredField()) {
        if (latitudeship == "" || longitudeship == "") {
            toastr.error('Invalid address.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            return false;
        }
        else if ($("#txtSLat").val() == "" || $("#txtSLong").val() == "" || $("#txtSstate").val() == "" || $("#txtScity").val() == "" || $("#txtSZipcode").val() == "") {
            toastr.error('Invalid address..', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            return false;
        }
        var data = {
            CustomerAutoId: AutoId,
            BillAdd: $("#txtSaddress1").val(),
            BillAdd2: $("#txtSaddress2").val(),
            Zipcode: $("#txtSZipcode").val(),
            City: $("#txtScity").val(),
            IsDefault: IsDefault,
            State: $("#txtSstate").val(),
            latitude: $("#txtSLat").val(),
            longitude: $("#txtSLong").val()
        };
        $.ajax({
            type: "POST",
            url: "ViewCustomerDetails.aspx/InsertShipping",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d != "Session Expired") {
                    if (result.d == "Success") {
                        swal("", "Address details saved successfully.", "success")
                        resetShipping();
                        CustomerShippingDetails();
                        $('#PopShippingDetails').modal('hide');
                    } else if (result.d == "invalid state") {
                     swal("", "Invalid state (" + $("#txtSstate").val() + ")", "error");
                    } else if (result.d == "ZipCode") {
                        swal("", "Invalid Zip Code : " + $('#txtBzipcode').val(), "error");
                    } else if (result.d == "City") {
                        swal("", "Invalid Zip Code : " + $('#txtBcity').val(), "error");

                    }
                    
                    else {
                        swal("Error!", result.d, "error");
                    }

                } else {
                    location.href = '/';
                }

            },
            error: function (result) {
                swal("Error!", "Oops! Something went wrong.Please try later.", "error");
            },
            failure: function (result) {
                swal("Error!", "Oops! Something went wrong.Please try later.", "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
}

function CustomerShippingDetails() {
    var data = {
        CustomerAutoId: AutoId,
    }
    $.ajax({
        type: "POST",
        url: "ViewCustomerDetails.aspx/GetShipping",
        data: "{'datavalue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var Shipping = $(xmldoc).find('Table');

                if (Shipping.length > 0) {
                    $('#tblShippingDetails tbody tr').remove();
                    var row = $('#tblShippingDetails thead tr').clone(true);
                    $.each(Shipping, function () {
                        $(".Id", row).html($(this).find("AutoId").text());
                        $(".Address", row).html($(this).find("CompleteShippingAddress").text());
                        if ($(this).find("IsDefault").text() == "1") {
                            $(".IsSDefault", row).html('<span class="badge badge badge-pill badge-success mr-2" style="text-transform: capitalize;">yes</span>');
                        }
                        else {
                            $(".IsSDefault", row).html('<span class="badge badge badge-pill badge-success mr-2" style="text-transform: capitalize;">no</span>');

                        }
                        if ($(this).find("IsDefault").text() == "1") {
                            $(".SAction", row).html("<a title='Edit' href='#'><span class='ft-edit' onclick='editShipping(\"" + $(this).find("AutoId").text() + "\")' /></a>");

                        }
                        else {
                            $(".SAction", row).html("<a title='Edit' href='#'><span class='ft-edit' onclick='editShipping(\"" + $(this).find("AutoId").text() + "\")' /></a>&nbsp;&nbsp;<a title='Delete' href='#'><span class='ft-x' onclick= 'DeleteShippingAddress(\"" + $(this).find("AutoId").text() + "\")' /></a>");
                        }
                        $("#tblShippingDetails tbody").append(row);
                        row = $("#tblShippingDetails tbody tr:last-child").clone(true);
                    });
                }
            }
        }
    });
}

function editShipping(AutoId) {
    $.ajax({
        type: "POST",
        url: "ViewCustomerDetails.aspx/EditShipping",
        data: "{'AutoId':'" + AutoId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: onSuccessOfEditShipping,
        error: function (response) {
            console.log(response.d)
        }
    })
}
function onSuccessOfEditShipping(response) {
    if (response.d == 'Session Expired') {
        location.href = '/';
    } else {
        var xmldoc = $.parseXML(response.d);
        var Shiiping = $(xmldoc).find('Table');
        $("#hdnSAutoId").val($(Shiiping).find("AutoId").text());
        $("#txtSaddress1").val($(Shiiping).find("Address").text());
        $("#txtSaddress2").val($(Shiiping).find("Address2").text());
        $("#txtScity").val($(Shiiping).find("City").text());
        $("#txtSstate").val($(Shiiping).find("StateName").text());
        $("#txtSZipcode").val($(Shiiping).find("Zipcode").text());
        $("#txtSLat").val($(Shiiping).find("SA_Lat").text());
        $("#txtSLong").val($(Shiiping).find("SA_Long").text());
        if ($(Shiiping).find("IsDefault").text() == "1") {
            $("#IsSDefault").prop('checked', true);
            $("#IsSDefault").attr('disabled', 'disabled');
        }
        else {
            $("#IsSDefault").removeAttr('disabled');
            $("#IsSDefault").prop('checked', false);
        }
        $("#PopShippingDetails").modal("show")
        $("#btnSSaves").hide();
        $("#btnSUpdates").show();

    }
}

function deleteShipping(AutoId) {

    $.ajax({
        type: "POST",
        url: "ViewCustomerDetails.aspx/DeleteShipping",
        data: "{'AutoId':'" + AutoId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (result) {
            if (result.d == "Session Expired") {
                window.location = "/";
            }
            else if (result.d == 'Success') {
                swal("", "Shipping Address deleted successfully.", "success");
                CustomerShippingDetails();
            } else {
                swal("Error!", result.d, "error");
            }
        },
        error: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later", "error");

        },
        failure: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later", "error");
        }
    })
}

function DeleteShippingAddress(AutoId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this Shipping Address!",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: false,
            },
            confirm: {
                text: "Yes, delete it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(isConfirm => {
        if (isConfirm) {
            deleteShipping(AutoId);

        } else {
            swal("", "Your Address is safe.", "error");
        }
    })
}
function UpdateShipping() {
    var latitudeship = $("#txtSLat").val();
    var longitudeship = $("#txtSLong").val();
    var IsDefault = "";
    if (($("#IsSDefault").prop("checked"))) {
        IsDefault = 1;
    }
    else {
        IsDefault = 0;
    }
   

    if (ScheckRequiredField()) {
        if (latitudeship == "" || longitudeship == "") {
            toastr.error('Invalid address.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            return false;
        }
        else if ($("#txtSLat").val() == "" || $("#txtSLong").val() == "" || $("#txtSstate").val() == "" || $("#txtScity").val() == "" || $("#txtSZipcode").val() == "") {
           toastr.error('Invalid address..', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            return false;
        }
        var data = {
            AutoId: $("#hdnSAutoId").val(),
            CustomerAutoId: AutoId,
            BillAdd: $("#txtSaddress1").val(),
            BillAdd2: $("#txtSaddress2").val(),
            Zipcode: $("#txtSZipcode").val(),
            City: $("#txtScity").val(),
            IsDefault: IsDefault,
            State: $("#txtSstate").val(),
            latitude: $("#txtSLat").val(),
            longitude: $("#txtSLong").val()
        };
        $.ajax({
            type: "POST",
            url: "ViewCustomerDetails.aspx/UpdateShipping",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d != "Session Expired") {
                    if (result.d == "Success") {
                        swal("", "Address details updated successfully.", "success")
                        resetShipping();
                        CustomerShippingDetails();
                        $('#PopShippingDetails').modal('hide');
                    }
                    else if (result.d == "Invalid state")
                    {
                        swal("", "Invalid state (" + $("#txtSstate").val() + ")", "error");
                    }
                    else if (result.d == "ZipCode") {
                        swal("Error!", "Invalid Zip Code : " + $('#txtBzipcode').val(), "error");
                    } else if (result.d == "City") {
                        swal("Error!", "Invalid Zip Code : " + $('#txtBcity').val(), "error");

                    }
                    else if (result.d == "IsDefault") {
                        swal("Error!", "IsDefault allready exist", "error");

                    }
                    else {
                        swal("Error!", result.d, "error");
                    }

                } else {
                    location.href = '/';
                }

            },
            error: function (result) {
                swal("Error!", "Oops! Something went wrong.Please try later.", "error");
            },
            failure: function (result) {
                swal("Error!", "Oops! Something went wrong.Please try later.", "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
}
function ScheckRequiredField() {
    var boolcheck = true;
    $('.Sreq').each(function () {
        if ($(this).val().trim() == '' || $(this).val().trim() == '0' || $(this).val().trim() == '0.00') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });

    return boolcheck;
}

function checkState(state, city, zcode,val) {
    if (state != undefined && city != undefined && zcode != undefined) {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/Sales/WebAPI/customerMaster.asmx/checkState",
            dataType: "json",
            data: "{'state':'" + state.trim() + "','city':'" + city.trim() + "','zcode':'" + zcode.trim() + "'}",
            success: function (response) {
                if (response.d == 'false') {
                    swal("", "Oops, Something went wrong. Please try later.", "warning");
                }
                else if (response.d == 'Invalidstate') {
                    swal("", "Invalid State (" + state + ')',"warning");
                    if (val == 1) {
                        resetBilling();
                    }
                    else {
                        resetShipping();
                    }
                }

            }
        })
    }
    else {
        swal("", "Invalid address.", "warning");
        resetField('');
    }
}