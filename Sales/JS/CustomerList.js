﻿$(document).ready(function () {
    getCustomerList(1);
    bindDropDowns();
});

function Pagevalue(e) {
    getCustomerList(parseInt($(e).attr("page")));
};


/*------------------------------------------------------Bind SALES PERSON------------------------------------------------------------*/
function bindDropDowns() {
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/WCustomerList.asmx/bindDropDowns",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $("#fade").show();
        },
        complete: function () {
            $("#fade").hide();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var salesPerson = $(xmldoc).find('Table');

            $("#ddlSSalesPerson option:not(:first)").remove();  //-------------------------------For Search Field
            $.each(salesPerson, function () {
                $("#ddlSSalesPerson").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("Name").text() + "</option>");
            });

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
/*------------------------------------------------------Get Customer List (bind Table)--------------------------------------------------*/
function getCustomerList(PageIndex) {
    //var salesPerson;
    //if ($("#hiddenSalesPerson").val() == "") {
    //    salesPerson = $("#ddlSSalesPerson").val();
    //} else {
    //    salesPerson = $("#hiddenSalesPerson").val();
    //    $('#tblCustomerList tr').find("td:eq(5)").hide();
    //}

    var data = {
        CustomerId: $("#txtSCustomerId").val(),
        CustomerName: $("#txtSCustomerName").val(),
        SalesPersonAutoId: $("#ddlSSalesPerson").val(),
        //CustomerEmail: $("#txtSCustomerEmail").val(),
        pageIndex: PageIndex
    };

    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/WCustomerList.asmx/getCustomerList",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var customer = $(xmldoc).find('Table1');

            $('#tblCustomerList tbody tr').remove();
            if (customer.length > 0) {
                $('#EmptyTable').hide();
                var row = $('#tblCustomerList thead tr').clone(true);
                $.each(customer, function () {
                    $(".CustId", row).text($(this).find("CustomerId").text());
                    $(".CustName", row).text($(this).find("CustomerName").text());
                    $(".ContactPersonName", row).text($(this).find("ContactPersonName").text());
                    $(".Email", row).html("<ul class='list-unstyled' ><li>" + $(this).find("Email").text() + "</li><li>" + $(this).find("AltEmail").text() + "</li></ul>");
                    $(".Contact", row).html("<ul class='list-unstyled' ><li>" + $(this).find("Contact1").text() + "</li><li>" + $(this).find("Contact2").text() + "</li></ul>");
                    $(".PriceLevel", row).html($(this).find("PriceLevelName").text());
                    $(".SalesPerson", row).text($(this).find("SalesPerson").text());                  
                    if ($(this).find("Status").text() == '1') {
                        $(".Status", row).html("<span class='label label-success'>" + $(this).find("StatusType").text() + "</span>");
                    } else {
                        $(".Status", row).html("<span class='label label-danger'>" + $(this).find("StatusType").text() + "</span>");
                    }
                    
                    $(".Action", row).html("<a href='javascript:;'><span class='glyphicon glyphicon-edit' onclick='editCustomer(\"" + $(this).find("CustomerId").text() + "\")' style='font-size:22px;'></span></a>");

                    $("#tblCustomerList tbody").append(row);
                    row = $("#tblCustomerList tbody tr:last-child").clone(true);
                });
                
                if ($("#hidnEmpType").val() == "Admin") {
                    $("#tblCustomerList").find(".SalesPerson").show();
                } 
            }
            else {
                $('#EmptyTable').show();
            }

            var pager = $(xmldoc).find("Table");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
    });
}

function editCustomer(CustomerId) {
    window.location.href = "/Sales/customerMaster.aspx?customerId=" + CustomerId;
}