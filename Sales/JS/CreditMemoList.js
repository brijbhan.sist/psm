﻿$(document).ready(function () {
    bindStatus(); bindDropdown();

    $('#txtSFromDate').pickadate({
        min: new Date("01/01/2019"),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtSToDate').pickadate({
        min: new Date("01/01/2019"),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    var d = new Date();
    var month = d.getMonth() + 1;
    if (month.toString().length == 1) {
        month = '0' + month;
    }
    var day = d.getDate();
    if (day.toString().length == 1) {
        day = '0' + day;
    }
    var year = d.getFullYear();
    $("#txtSFromDate").val(month + '/' + day + '/' + year);
    $("#txtSToDate").val(month + '/' + day + '/' + year);
    getCreditList(1);
})

function setdatevalidation(val) {
    var fdate = $("#txtSFromDate").val();
    var tdate = $("#txtSToDate").val();
    if (val == 1) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtSToDate").val(fdate);
        }
    }
    else if (val == 2) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtSFromDate").val(tdate);
        }
    }
}
function Pagevalue(e) {
    getCreditList(parseInt($(e).attr("page")));
};
function bindDropdown() {

    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/WCreditMemo.asmx/bindDropdown",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var customer = $(xmldoc).find("Table");
                var Credit = $(xmldoc).find("Table3"); 
                $("#ddlCustomer option:not(:first)").remove();
                $.each(customer, function () {
                    $("#ddlCustomer").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("Customer").text() + "</option>");
                });
                $("#ddlCustomer").select2();
                var CurrentDate = $(xmldoc).find("Table2");
                $("#txtOrderDate").val($(CurrentDate).find('CurrentDate').text());

                $("#ddlCreditType option:not(:first)").remove();
                $.each(Credit, function () {
                    $("#ddlCreditType").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("OrderType").text() + "</option>");
                });
            }
            //else {
            //    location.href = '/';
            //}
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function bindStatus() {
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/WCreditMemoList.asmx/bindStatus",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var status = $(xmldoc).find("Table");

                $("#ddlSStatus option:not(:first)").remove();
                $.each(status, function () {
                    $("#ddlSStatus").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("StatusType").text() + "</option>");
                });
            }
            //else {
            //    location.href = '/';
            //}
        },
        failure: function (result) {
        },
        error: function (result) {
        }
    });
}
function getCreditList(pageIndex) {
    var data = {
        CustomerAutoId: $("#ddlCustomer").val(),
        CreditNo: $("#txtSOrderNo").val().trim(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        Status: $("#ddlSStatus").val(),
        CreditType: $("#ddlCreditType").val(), 
        pageIndex: pageIndex,
        PageSize: $("#ddlPageSize").val() || 10,
    };

    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/WCreditMemoList.asmx/getCreditList",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var orderList = $(xmldoc).find("Table1");
                    var EmpType = $(xmldoc).find("Table2");

                    $("#tblOrderList tbody tr").remove();
                    var row = $("#tblOrderList thead tr").clone(true);

                    if (orderList.length > 0) {
                        $("#EmptyTable").hide();
                        $.each(orderList, function () {
                            $(".orderNo", row).html("<span OrderAutoId='" + $(this).find("AutoId").text() + "'>" + $(this).find("OrderNo").text() + "</span>");
                            $(".orderDt", row).text($(this).find("OrderDate").text());
                            $(".cust", row).text($(this).find("CustomerName").text());
                            $(".SalesPerson", row).text($(this).find("SalesPerson").text());
                            $(".CreditType", row).text($(this).find("CreditType").text());
                            $(".value", row).text($(this).find("GrandTotal").text());
                            $(".product", row).text($(this).find("NoOfItems").text());
                            $(".referenceorderno", row).text($(this).find("referenceorderno").text());

                            if (Number($(this).find("StatusCode").text()) == 1) {
                                $(".status", row).html("<span class='badge badge badge-pill badge-info'>" + $(this).find("Status").text() + "</span>");
                            } else if (Number($(this).find("StatusCode").text()) == 2) {
                                $(".status", row).html("<span class='badge badge badge-pill badge-primary'>" + $(this).find("Status").text() + "</span>");
                            } else if (Number($(this).find("StatusCode").text()) == 3) {
                                $(".status", row).html("<span class='badge badge badge-pill badge-primary'>" + $(this).find("Status").text() + "</span>");
                            } else if (Number($(this).find("StatusCode").text()) == 4) {
                                $(".status", row).html("<span class='badge badge badge-pill badge-warning'>" + $(this).find("Status").text() + "</span>");
                            } else if (Number($(this).find("StatusCode").text()) == 5) {
                                $(".status", row).html("<span class='badge badge badge-pill badge-warning'>" + $(this).find("Status").text() + "</span>");
                            } else if (Number($(this).find("StatusCode").text()) == 6) {
                                $(".status", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("Status").text() + "</span>");
                            } else if (Number($(this).find("StatusCode").text()) == 7) {
                                $(".status", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("Status").text() + "</span>");
                            } else if (Number($(this).find("StatusCode").text()) == 8) {
                                $(".status", row).html("<span class='badge badge badge-pill badge-default'>" + $(this).find("Status").text() + "</span>");
                            }
                            if ($(EmpType).find('EmpType').text() == 2 || $(EmpType).find('EmpType').text() == 1) {
                                if ($(this).find('StatusCode').text() == 1 || $(this).find('StatusCode').text() == 6) {
                                    $(".action", row).html("<a title='Edit' href='/Sales/CreditMemo.aspx?PageId=" + $(this).find("AutoId").text() + "'><span class='la la-edit'></span></a>&nbsp;<a title='Log' href='javascript:;' onclick='viewOrderLog(" + $(this).find("AutoId").text() + ",1)'><span class='la la-history'></span></a>&nbsp<a title='Delete' href='javascript:;'><span class='la la-remove' onclick='deleterecord(\"" + $(this).find("AutoId").text() + "\")'></span><b></a></b>");
                                } else {
                                    $(".action", row).html("<a title='Edit' href='/Sales/CreditMemo.aspx?PageId=" + $(this).find("AutoId").text() + "'><span class='la la-edit'></span></a>&nbsp;<a href='javascript:;' title='Log' onclick='viewOrderLog(" + $(this).find("AutoId").text() + ",1)'><span class='la la-history'></span></a></b>");
                                }
                            }
                            else if ($(EmpType).find('EmpType').text() == 8) {
                                $(".action", row).html("<a title='Edit' href='/Manager/ManagerCreditMemo.aspx?PageId=" + $(this).find("AutoId").text() + "'><span class='la la-edit'></span></a>&nbsp;<a title='Log' href='javascript:;' onclick='viewOrderLog(" + $(this).find("AutoId").text() + ",1)'><span class='la la-history'></span></a></b>");
                            }
                            else {
                                $(".action", row).html("<a title='Edit' href='/Sales/CreditMemo.aspx?PageId=" + $(this).find("AutoId").text() + "'><span class='la la-edit'></span></a>&nbsp;<a title='Log' href='javascript:;' onclick='viewOrderLog(" + $(this).find("AutoId").text() + ",1)'><span class='la la-history'></span></a></b>");
                            }
                            $(".value", row).css('text-align', 'right');
                            $("#tblOrderList tbody").append(row);
                            row = $("#tblOrderList tbody tr:last").clone(true);
                        });
                    } else {
                        $("#EmptyTable").show();
                    }

                    var pager = $(xmldoc).find("Table");
                    $(".Pager").ASPSnippets_Pager({
                        ActiveCssClass: "current",
                        PagerCssClass: "pager",
                        PageIndex: parseInt(pager.find("PageIndex").text()),
                        PageSize: parseInt(pager.find("PageSize").text()),
                        RecordCount: parseInt(pager.find("RecordCount").text())
                    });

                }
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function print_NewOrder(e) {
    window.open("/Packer/OrderPrint.html?OrderAutoId=" + $(e).closest("tr").find(".orderNo span").attr("OrderAutoId"), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    setTimeout(function () {
        getOrderList(1);
    }, 2000);
}
$("#ddlPageSize").change(function () {
    getCreditList(1);
});
$("#btnSearch").click(function () {
    getCreditList(1);
})
function deleteOrder(CreditAutoId) {

    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/WCreditMemoList.asmx/deleteOrder",
        data: "{'CreditAutoId':" + CreditAutoId + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == 'Session Expired') {
                location.href = '/';
            } else if (response.d == 'true') {
                swal("", "Credit memo deleted successfully.", "success");
                getCreditList(1);
            } else {
                swal("", response.d, "error");
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function deleterecord(CreditAutoId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete credit memo.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, Cancel.",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, Delete it.",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            deleteOrder(CreditAutoId);
        }
    })
}
function sortBy(sortCode) {
    CreditAutoId = $("#CreditLogAutoId").val();
    viewOrderLog(CreditAutoId, sortCode);
}
function viewOrderLog(CreditAutoId, sortInCode) {
    $("#CreditLogAutoId").val(CreditAutoId);
    if (sortInCode == 1) {
        $(".la-arrow-down").show();
        $(".la-arrow-up").hide();
    } else if (sortInCode == 2) {
        $(".la-arrow-down").hide();
        $(".la-arrow-up").show();
    }
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/WCreditMemoList.asmx/viewOrderLog",
        data: "{'CreditAutoId':" + CreditAutoId + ",'sortInCode':" + sortInCode + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == 'Session Expired') {
                location.href = '/';
            } else {
                var xmldoc = $.parseXML(response.d);
                var order = $(xmldoc).find("Table");
                var orderlog = $(xmldoc).find("Table1");

                $("#lblOrderNo").text(order.find("CreditNo").text());
                $("#lblOrderDate").text(order.find("CreditDate").text());

                $("#tblOrderLog tbody tr").remove();
                var row = $("#tblOrderLog thead tr").clone(true);
                if (orderlog.length > 0) {
                    $("#EmptyTable").hide();
                    $.each(orderlog, function (index) {
                        $(".SrNo", row).html(index + 1);
                        $(".ActionBy", row).html($(this).find("EmpName").text());
                        $(".Date", row).html($(this).find("LogDate").text());
                        $(".Action", row).html($(this).find("Action").text());
                        $(".Remark", row).html($(this).find("Remarks").text());

                        $("#tblOrderLog tbody").append(row);
                        row = $("#tblOrderLog tbody tr:last").clone(true);
                    });
                }
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
    $("#modalOrderLog").modal('show');
}