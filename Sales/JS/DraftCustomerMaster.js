﻿var ContactPerson = [];
$(document).ready(function () {
    bindDropDowns();
    bindContactPersonType();
    var getid = getQueryString('customerId');
    if (getid != null) {
        editCustomer(getid);
        EditContactPerson(getid);
    }
});
var getQueryString = function (field, url) {
    var href = url ? url : window.location.href;
    var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
    var string = reg.exec(href);
    return string ? string[1] : null;
};
/*------------------------------------------------------Bind State , SALES PERSON , Status------------------------------------------------------------*/
function bindDropDowns() {
    $.ajax({
        type: "POST",
        url: "DraftCustomerMaster.aspx/bindDropDownsList",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            if (response.d != "Session Expired") {
                var DropDown = $.parseJSON(response.d);
                for (var i = 0; i < DropDown.length; i++) {
                    var AllDropDownList = DropDown[i];
                    var CustomerType = AllDropDownList.CustomerType;
                    var ddlCustomerType = $("#ddlCustType");
                    $("#ddlCustType option:not(:first)").remove();
                    for (var j = 0; j < CustomerType.length; j++) {
                        var CustomerDropList = CustomerType[j];
                        var option = $("<option />");
                        option.html(CustomerDropList.CustomerType);
                        option.val(CustomerDropList.AutoId);
                        ddlCustomerType.append(option);
                    }
                    ddlCustomerType.select2();
                    var ddlStatus = $("#ddlStatus");
                    $("#ddlStatus option").remove();
                    var Status = AllDropDownList.Status;
                    for (var k = 0; k < Status.length; k++) {
                        var StatusDropList = Status[k];
                        var option = $("<option />");
                        option.html(StatusDropList.StatusType);
                        option.val(StatusDropList.AutoId);
                        ddlStatus.append(option);
                    }
                    ddlStatus.select2();
                    var ddlEmployee = $("#ddlSalesPerson");
                    $("#ddlSalesPerson option:not(:first)").remove();
                    var Employee = AllDropDownList.Employee;
                    for (var l = 0; l < Employee.length; l++) {
                        var EmployeeDropList = Employee[l];
                        var option = $("<option />");
                        option.html(EmployeeDropList.Name);
                        option.val(EmployeeDropList.AutoId);
                        ddlEmployee.append(option);
                    }
                    ddlEmployee.select2();
                    var ddlTerms = $("#ddlTerms");
                    $("#ddlTerms option:not(:first)").remove();
                    var Terms = AllDropDownList.Terms;
                    for (var m = 0; m < Terms.length; m++) {
                        var TermsDropList = Terms[m];
                        var option = $("<option />");
                        option.html(TermsDropList.TermsDesc);
                        option.val(TermsDropList.TermsId);
                        ddlTerms.append(option);
                    }
                    ddlTerms.select2();
                    var ddlZipCode1 = $("#ddlZipCode1");
                    $("#ddlZipCode1 option:not(:first)").remove();
                    $("#ddlZipCode2 option:not(:first)").remove();
                    var ddlZipCode2 = $("#ddlZipCode2");
                    var ZipCode1 = AllDropDownList.ZipCode1;
                    for (var n = 0; n < ZipCode1.length; n++) {
                        var ZipCode1DropList = ZipCode1[n];
                        var option = $("<option />");
                        option.html(ZipCode1DropList.ZipCode);
                        option.val(ZipCode1DropList.AutoId);
                        option.attr('zm', ZipCode1DropList.ZM);
                        option.attr('CityId', ZipCode1DropList.CityId);
                        var option1 = option.clone();
                        ddlZipCode1.append(option);
                        ddlZipCode2.append(option1);
                    }
                    ddlZipCode1.select2();
                    ddlZipCode2.select2();
                    var CompanyDetails = AllDropDownList.CompanyDetails;
                    for (var p = 0; p < CompanyDetails.length; p++) {
                        var CompanyDetailsList = CompanyDetails[p];
                        $('#CompanyId').text('@' + CompanyDetailsList.CompanyId);
                    }


                    var StoreOpenTime = $("#StoreOpenTime");
                    $("#StoreOpenTime option:not(:first)").remove();
                    var StoreAllOpenTime = AllDropDownList.StoreTime;
                    for (var op = 0; op < StoreAllOpenTime.length; op++) {
                        var StoreAllOpenTimeList = StoreAllOpenTime[op];
                        var option = $("<option />");
                        option.html(StoreAllOpenTimeList.TimeFromLabel);
                        option.val(StoreAllOpenTimeList.TimeFrom);
                        StoreOpenTime.append(option);
                    }
                    StoreOpenTime.select2();
                    StoreOpenTime.val('06:00:00').change();


                    var StoreCloseTime = $("#StoreCloseTime");
                    $("#StoreCloseTime option:not(:first)").remove();
                    var StoreAllCloseTime = AllDropDownList.StoreTime;
                    for (var cl = 0; cl < StoreAllCloseTime.length; cl++) {
                        var StoreAllCloseTimeList = StoreAllCloseTime[cl];
                        var option = $("<option />");
                        option.html(StoreAllCloseTimeList.TimeToLabel);
                        option.val(StoreAllCloseTimeList.TimeTo);
                        StoreCloseTime.append(option);
                    }
                    StoreCloseTime.select2();
                    StoreCloseTime.val('22:00:00').change();


                }
            } else {
                location.href = '/';
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function priceLebelByCustemerType() {
    var CustemerTypeAutoId = $('#ddlCustType').val();

    $.ajax({
        type: "POST",
        url: "DraftCustomerMaster.aspx/BindpriceLevel",
        data: "{'DataValues':'" + CustemerTypeAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            if (response.d != "Session Expired") {
                var PriceLevelList = $.parseJSON(response.d);
                var ddlPriceLevel = $("#ddlPriceLevel");
                $("#ddlPriceLevel option:not(:first)").remove();
                if (getQueryString('PageId') == null)
                    $("#ddlPriceLevel").append("<option value = '-1' >Auto Generate Price Level</option >");
                for (var i = 0; i < PriceLevelList.length; i++) {
                    var PriceLevel = PriceLevelList[i];
                    var option = $("<option />");
                    option.html(PriceLevel.PL);
                    option.val(PriceLevel.AutoId);
                    ddlPriceLevel.append(option);
                }
                ddlPriceLevel.select2();
            } else {
                location.href = '/';
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function setData(place) {
    $(".hiddenShipAddress").html(place.adr_address);
    var textdata = $(".hiddenShipAddress").find(".street-address").text();
    $("#txtShipAdd").val(textdata);
    var m = 0, check = 0;
    for (var i = 0; i < place.address_components.length; i++) {
        for (var j = 0; j < place.address_components[i].types.length; j++) {

            if (place.address_components[i].types[j] == "postal_code") {
                $("#txtZipcode2").val(place.address_components[i].long_name);
                var zcode = place.address_components[i].long_name;
            }
            else if (place.address_components[i].types[j] == "administrative_area_level_1") {
                $("#txtState2").val(place.address_components[i].long_name);
                var state = place.address_components[i].long_name;
            }
            if (place.address_components[i].types[j] == "locality") {
                $("#txtCity2").val(place.address_components[i].long_name);
                city = place.address_components[i].long_name;
                check = 1;
            }
            if (check == 0) {

                if (place.address_components[i].types[j] == "neighborhood" && check ==0) {
                    $("#txtCity2").val(place.address_components[i].long_name);
                    city = place.address_components[i].long_name;
                    check = 1;
                }
                else if (place.address_components[i].types[j] == "administrative_area_level_3" && check == 0) {
                    $("#txtCity2").val(place.address_components[i].long_name);
                    city = place.address_components[i].long_name;
                }
            }
        }
        m++;
    }
    if (m > 4) {
        checkState(state, city, zcode, 2);
        $("#txtLat").val(place.geometry.location.lat);
        $("#txtLong").val(place.geometry.location.lng);
    }
    else {
        swal("", " Invalid address.", "error");
        resetField('');
    }
}
function checkState(state, city, zcode, val) {
    if (state != undefined && city != undefined && zcode != undefined) {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/Sales/WebAPI/customerMaster.asmx/checkState",
            dataType: "json",
            data: "{'state':'" + state.trim() + "','city':'" + city.trim() + "','zcode':'" + zcode.trim() + "'}",
            success: function (response) {
                if (response.d == 'false') {
                    swal("", "Oops, Something went wrong. Please try later.", "warning");
                }
                else if (response.d == 'Invalidstate') {
                    swal("", "Invalid State (" + state + ')', "warning");
                    if (val == 2) {
                        resetField('');
                    }
                    else {
                        resetBillingField();
                    }
                }

            }
        })
    }
    else {
        swal("", "Invalid address.", "warning");
        resetField('');
    }
}
function getSelectedZip() {
    var autoid = $('input[name="selectedzip"]:checked').val();
    if ($('input[name="selectedzip"]:checked').length == 0) {
        toastr.error('Please select zipcode.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return false;
    } else {
        $("#ddlZipCode2").val(autoid).change();
        $("#ddlZipCode2").attr('disabled', true);
        $("#zipcodeselect").modal("hide");

    }

}
function hideclosemodal() {
    $("#zipcodeselect").modal("hide");
}

function hideclosemodal1() {
    $("#zipcodeselect1").modal("hide");
}
function resetField(val) {
    if (val == '') {
        $("#txtShipAdd").val('');
        $("#txtCity2").val('');
        $("#txtState2").val('');
        $("#txtLat").val('');
        $("#txtLong").val('');
        $("#txtZipcode2").val('');
        $("#txtShipAdd2").val('');
    }
}
function setBillingData(place) {
    $(".hiddenBillAddress").html(place.adr_address);
    $("#txtBillAdd").val($(".hiddenBillAddress").find(".street-address").text());
    var m = 0, check = 0;
    for (var i = 0; i < place.address_components.length; i++) {
        for (var j = 0; j < place.address_components[i].types.length; j++) {

            if (place.address_components[i].types[j] == "postal_code") {
                $("#txtZipcode1").val(place.address_components[i].long_name);
                var zcode = place.address_components[i].long_name;
            }
            else if (place.address_components[i].types[j] == "administrative_area_level_1") {
                $("#txtState1").val(place.address_components[i].long_name);
                var state = place.address_components[i].long_name;
            }
            if (place.address_components[i].types[j] == "locality") {
                $("#txtCity1").val(place.address_components[i].long_name);
                var city = place.address_components[i].long_name;
                check = 1;
            }

            if (check == 0) {
                if (place.address_components[i].types[j] == "neighborhood" && check == 0) {
                    $("#txtCity1").val(place.address_components[i].long_name);
                    var city = place.address_components[i].long_name;
                    check = 1;
                }
                else if (place.address_components[i].types[j] == "administrative_area_level_3" && check == 0) {
                    $("#txtCity1").val(place.address_components[i].long_name);
                    var city = place.address_components[i].long_name;
                }
            }
        }
        m++;
    }
    if (m > 4) {
        checkState(state, city, zcode, 1);
        $("#txtLat1").val(place.geometry.location.lat);
        $("#txtLong1").val(place.geometry.location.lng);
    }
    else {
        swal("", "Invalid address.", "error");
        resetBillingField('');
    }

}
function checkBillingZipcode(zipcode) {
    var zipcodeHtml = ``;
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "DraftCustomerMaster.aspx/checkZipcode",
        dataType: "json",//$('option:selected', a).val();
        data: "{'ZipCode':'" + zipcode + "'}",
        success: function (response) {
            var xmldoc = $.parseXML(response.d);

            var zipcodeData = $(xmldoc).find('Table');
            if (zipcodeData.length == 0) {
                if (zipcode != undefined) {
                    swal("", "Zipcode (" + zipcode + ") not registered !", "error");
                } else {
                    swal("", "Zipcode not found !", "error");
                }
                resetBillingField('');
                $("#ddlZipCode1").attr('disabled', true);

            } else if (zipcodeData.length == 1) {
                $.each(zipcodeData, function () {
                    $("#ddlZipCode1").val($(zipcodeData).find('AutoId').text()).change();
                });
                $("#ddlZipCode1").attr('disabled', true);
            } else if (zipcodeData.length >= 2) {
                zipcodeHtml += `<div class="table-responsive"><table class="table table-striped table-bordered">
                                    <thead class="bg-blue white">
                                      <tr>
                                        <th></th>
                                        <th>Zipcode</th>
                                        <th>City</th>
                                        <th>State</th>
                                      </tr>
                                    </thead>
                                    <tbody>`;
                $.each(zipcodeData, function () {
                    zipcodeHtml += `<tr>
                                        <td style="padding: 8px 1rem !important;"><input type="radio" name="selectedzip" class="req" value="`+ $(this).find('AutoId').text() + `"></td>
                                        <td>`+ $(this).find('Zipcode').text() + `</td>
                                        <td>`+ $(this).find('CityName').text() + `</td>
                                        <td>`+ $(this).find('StateName').text() + `</td>
                                      </tr>`;
                });
                zipcodeHtml += `</tbody>
                                  </table></div>`;
                $("#zipcodelist1").html(zipcodeHtml);
                $("#zipcodeselect1").modal("show");
            }


        }
    });
}

function getSelectedBillingZip() {
    var autoid = $('input[name="selectedzip"]:checked').val();
    if ($('input[name="selectedzip"]:checked').length == 0) {
        toastr.error('Please select zipcode.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return false;
    } else {
        $("#ddlZipCode1").val(autoid).change();
        $("#ddlZipCode1").attr('disabled', true);
        $("#zipcodeselect1").modal("hide");

    }

}

function resetBillingField() {
    $("#txtBillAdd").val('');
    $("#txtBillAdd2").val('');
    $("#txtZipcode1").val('');
    $("#txtState1").val('');
    $("#txtCity1").val('');
    $("#txtLat1").val('');
    $("#txtLong1").val('');
}

function validateAddress(val) {
    if (val == 1) {
        $("#txtLat1").val('');
        $("#txtLong1").val('');
    } else if (val == 2) {
        $("#txtLat").val('');
        $("#txtLong").val('');
    }
}
/*------------------------------------------------------------------*/
function fnChkSame() {
    if ($("#chkSame").prop("checked") == false) {
        $('#ddlZipCode2').val('0').change();
    }
    if ($("#chkSame").is(':checked')) {
        $('#divShippingAddr').hide();
        $('#txtShipAdd').removeClass('req');
        $('#ddlZipCode2').removeClass('ddlreq');
        $('#txtLat').val('');
        $('#txtLong').val('');
    }
    else {
        $('#divShippingAddr').show();
        $('#txtShipAdd').addClass('req');
        $('#ddlZipCode2').addClass('ddlreq');
        $("#divShippingAddr input[type='text']").val('');
        $("#divShippingAddr select").val(0);

    }

}
function bindContactPersonType() {
    $.ajax({
        type: "POST",
        url: "DraftCustomerMaster.aspx/bindContactPersonType",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },

        success: function (response) {
            console.log(response);
            if (response.d == 'Session Expired') {
                location.href = '/';
            } else if (response.d != 'false') {
                var xmldoc = $.parseXML(response.d);
                var ContactPersonType = $(xmldoc).find('Table');
                $("#ddlType option:not(:first)").remove();  //-------------------------------For Search Field
                $.each(ContactPersonType, function () {
                    $("#ddlType").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("TypeName").text() + "</option>");
                });
                $("#ddlType").select2();
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function addItemToList() {
    var IsDefault = 0;
    if ($("#chkIsDefault").prop("checked") == true) {
        IsDefault = "<input type='radio' name='default' class='radio border-primary' checked>";
        if ($("#tblContactDetails tbody tr").length > 0) {
            $("#tblContactDetails tbody tr").each(function (i) {
                if ($(this).find('.IsDefault input').prop('checked') == true) {
                    $(this).find('.IsDefault').html("<input name='default'  type='radio'  class='radio border-primary'>");
                }
            })
        }
    }
    else {
        IsDefault = "<input type='radio' name='default'  class='radio border-primary'>";
    }

    if (ContactPerson.length == 0) {
        $("#tblContact").val();
    }
    debugger;
    if (ContactcheckRequiredField()) {
        if ($("#txtAltEmail").val() != '') {
            if ($("#txtAltEmail").val() == $("#txtEmail").val()) {
                $("#txtAltEmail").addClass('border-warning');
                $("#txtEmail").addClass('border-warning');
                toastr.error("Email and Alternate Email can't be same.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                return;
            }
            else {
                $("#txtAltEmail").removeClass('border-warning');
                $("#txtEmail").removeClass('border-warning');
            }
        }
        else if ($("#txtEmail").val() != '') {
            $("#txtAltEmail").removeClass('border-warning');
            $("#txtEmail").removeClass('border-warning');
        }
        if ($("#txtLandline").val().length < 10) {
            toastr.error('Please enter a valid 10 digit Landline No.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            $("#txtLandline").addClass('border-warning');
            return;
        }
        else if ($("#txtLandline2").val() != "") {
            if ($("#txtLandline2").val().length < 10) {
                $("#txtLandline").removeClass('border-warning');
                toastr.error('Please enter a valid 10 digit Landline No.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
                $("#txtLandline2").addClass('border-warning');
                return;
            }
        }
        else if ($("#txtMobileNo").val().length < 10) {
            $("#txtLandline2").removeClass('border-warning');
            toastr.error('Please enter a valid 10 digit Mobile No.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            $("#txtMobileNo").addClass('border-warning');
            return;
        }
        if ($("#txtLandline").val() == $("#txtMobileNo").val()) {
            if ($("#txtLandline2").val() != '') {
                if ($("#txtLandline2").val() == $("#txtMobileNo").val()) {
                    $("#txtLandline").addClass('border-warning');
                    $("#txtLandline2").addClass('border-warning');
                    $("#txtMobileNo").addClass('border-warning');
                    toastr.error("Landline 1, Landline 2 and Mobile No can't be same.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    return;
                }
                else if ($("#txtLandline").val() == $("#txtMobileNo").val()) {
                    $("#txtLandline").addClass('border-warning');
                    $("#txtMobileNo").addClass('border-warning');
                    $("#txtLandline2").removeClass('border-warning');
                    toastr.error("Landline 1 and Mobile No can't be same.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    return;
                }
            }
            else {
                $("#txtLandline").addClass('border-warning');
                $("#txtMobileNo").addClass('border-warning');
                toastr.error("Landline 2 and Mobile No can't be same.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                return;
            }
        }
        if ($("#txtLandline2").val() != '') {
            if ($("#txtLandline").val() == $("#txtLandline2").val()) {
                $("#txtLandline").addClass('border-warning');
                $("#txtLandline2").addClass('border-warning');
                toastr.error("Landline 1 and Landline 2 can't be same.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                return;
            }
            else if ($("#txtLandline2").val() == $("#txtMobileNo").val()) {
                $("#txtLandline2").addClass('border-warning');
                $("#txtMobileNo").addClass('border-warning');
                toastr.error("Landline 2 and Mobile No can't be same.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                return;
            }
            else {
                $("#txtLandline").removeClass('border-warning');
                $("#txtLandline2").removeClass('border-warning');
                $("#txtMobileNo").removeClass('border-warning');
            }
        }
        else {
            $("#txtLandline").removeClass('border-warning');
            $("#txtLandline2").removeClass('border-warning');
            $("#txtMobileNo").removeClass('border-warning');
        }


        if ($("#tbodyContactPerson tr").length == 0) {
            if (!$("#chkIsDefault").prop("checked")) {
                IsDefault = "<input type='radio' name='default' class='radio border-primary' checked>";
            }
        }
        var ContactPersonData = {
            "Type": $("#ddlType option:selected").text(),
            "TypeAutoId": $("#ddlType").val(),
            "ContactPerson": $("#txtContactPersonName").val(),
            "Email": $("#txtEmail").val(),
            "AltEmail": $("#txtAltEmail").val(),
            "Landline": $("#txtLandline").val(),
            "Landline2": $("#txtLandline2").val(),
            "Mobile": $("#txtMobileNo").val(),
            "Fax": $("#txtFaxNo").val(),
            "IsDefault": IsDefault
        };
        ContactPerson = [];
        ContactPerson.push(ContactPersonData);
        contactPersonHtml();
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });

    }
}
function contactPersonHtml() {
    var i = 0;
    var trHtml = ``;


    ContactPerson.forEach(function (e) {
        trHtml += `<tr>
                    <td class="Action text-center"><a title="Delete" href="javascript:void(0)" onclick="deleterecord(this)"><span class="la la-remove"></span></a></td>
                    <td class="TypeAutoId text-center" style="display:none;">`+ e.TypeAutoId + `</td>
                    <td class="Type text-center">`+ e.Type + `</td>
                    <td class="ContactPerson text-center">`+ e.ContactPerson + `</td>
                    <td class="Mobile text-center">`+ e.Mobile + `</td>
                    <td class="Landline">`+ e.Landline + `</td>
                    <td class="Landline2">`+ e.Landline2 + `</td>
                    <td class="Fax">`+ e.Fax + `</td>
                    <td class="Email">`+ e.Email + `</td>
                    <td class="AltEmail">`+ e.AltEmail + `</td>
                    <td class="IsDefault text-center">`+ e.IsDefault + `</td>
                </tr>`;
        i++;
    });
    $("#tbodyContactPerson").append(trHtml);
    $("#tblContact").show();
    clearField();
}

function ContactcheckRequiredField() {
    var boolcheck = true;
    $('.creq').each(function () {

        if ($(this).val().trim() == '') {
            boolcheck = false;
            $(this).addClass('border-warning');

        } else {
            $(this).removeClass('border-warning');
        }
    });

    $('.ddlcreq').each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == 'undefined' || $(this).val() == 'Select') {
            boolcheck = false;
            $(this).closest('div').find('.select2-selection--single').attr('style', 'border:1px solid #FF9149  !important');
        } else {
            $(this).removeClass('border-warning');
            $(this).closest('div').find('.select2-selection--single').removeAttr('style');
        }
    });
    if ($("#txtLandline").val().length < 10) {
        $("#txtLandline").addClass('border-warning');
    }
    if ($("#txtLandline2").val() != "") {
        if ($("#txtLandline2").val().length < 10) {
            $("#txtLandline2").addClass('border-warning');
        }
    }
    if ($("#txtMobileNo").val().length < 10) {
        $("#txtMobileNo").addClass('border-warning');
    }
    if ($("#txtLandline").val() == $("#txtMobileNo").val()) {
        if ($("#txtLandline2").val() != '') {
            if ($("#txtLandline2").val() == $("#txtMobileNo").val()) {
                $("#txtLandline").addClass('border-warning');
                $("#txtLandline2").addClass('border-warning');
                $("#txtMobileNo").addClass('border-warning');
            }
            else {
                $("#txtLandline").addClass('border-warning');
                $("#txtLandline2").addClass('border-warning');
            }
        }
        else {
            $("#txtLandline").addClass('border-warning');
            $("#txtMobileNo").addClass('border-warning');
        }
    }
    if ($("#txtLandline2").val() != '') {
        if ($("#txtLandline").val() == $("#txtLandline2").val()) {
            $("#txtLandline").addClass('border-warning');
            $("#txtLandline2").addClass('border-warning');
        }
        else if ($("#txtLandline2").val() == $("#txtMobileNo").val()) {
            $("#txtLandline2").addClass('border-warning');
            $("#txtMobileNo").addClass('border-warning');
        }
    }
    if ($("#txtAltEmail").val() != '') {
        if ($("#txtAltEmail").val() == $("#txtEmail").val()) {
            $("#txtAltEmail").addClass('border-warning');
            $("#txtEmail").addClass('border-warning');
        }
        else if ($("#txtAltEmail").val() != $("#txtEmail").val()) {
            $("#txtAltEmail").removeClass('border-warning');
            $("#txtEmail").removeClass('border-warning');
        }
    }
    else if ($("#txtEmail").val() != '') {
        $("#txtEmail").addClass('border-warning');
    }
    if ($("#txtLandline").val().length == 10 && $("#txtLandline2").val() == 10 && $("#txtMobileNo").val().length == 10) {
        $("#txtLandline").removeClass('border-warning');
        $("#txtLandline2").removeClass('border-warning');
        $("#txtMobileNo").removeClass('border-warning');
    }
    return boolcheck;
}
/*------------------------------------------------------Insert Customer------------------------------------------------------------*/
function Save() {
    if ($("#ddlPriceLevel").val() == '0') {
        $("#ddlPriceLevel").addClass('border-warning');
    }
    else {
        $("#ddlPriceLevel").removeClass('border-warning');
    }
    if (CustcheckRequiredField()) {
        if ($("#chkSame").is(':checked') == true) {
            var latitudeship = $("#txtLat1").val();
            var longitudeship = $("#txtLong1").val();
        } else {
            var latitudeship = $("#txtLat").val();
            var longitudeship = $("#txtLong").val();
        }
        if (latitudeship == "" || longitudeship == "") {
            toastr.error('Invalid address.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            return false;

        }
        else if ($("#txtLat1").val() == "" || $("#txtLong1").val() == "") {
            toastr.error('Invalid address.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            return false;

        }
        if (ContactPerson.length == 0) {
            toastr.error('Please add atleast one contact person', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            return false;
        }
        var flage = false;
        if ($('#tblContactDetails tbody tr').length > 0) {
            $("#tblContactDetails tbody tr").each(function () {
                if ($(this).find('.IsDefault input').prop('checked') == true) {
                    flage = true;
                }
            })
            if (!flage) {
                toastr.error('Atleast one contact person default.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            }
            else {
                var Xmlcontactperson = '', def = 0;
                $("#tblContactDetails tbody tr").each(function () {
                    Xmlcontactperson += '<contactpersonXML>';
                    Xmlcontactperson += '<TypeAutoId><![CDATA[' + $(this).find('.TypeAutoId').html() + ']]></TypeAutoId>';
                    Xmlcontactperson += '<ContactPerson><![CDATA[' + $(this).find('.ContactPerson').html() + ']]></ContactPerson>';
                    Xmlcontactperson += '<Mobile><![CDATA[' + $(this).find('.Mobile').html() + ']]></Mobile>';
                    Xmlcontactperson += '<Landline><![CDATA[' + $(this).find('.Landline').html() + ']]></Landline>';
                    Xmlcontactperson += '<Landline2><![CDATA[' + $(this).find('.Landline2').html() + ']]></Landline2>';
                    Xmlcontactperson += '<Fax><![CDATA[' + $(this).find('.Fax').html() + ']]></Fax>';
                    Xmlcontactperson += '<Email><![CDATA[' + $(this).find('.Email').html() + ']]></Email>';
                    Xmlcontactperson += '<AltEmail><![CDATA[' + $(this).find('.AltEmail').html() + ']]></AltEmail>';
                    if ($(this).find('.IsDefault input').prop("checked") == true) {
                        def = 1;
                    }
                    else {
                        def = 0;
                    }
                    Xmlcontactperson += '<IsDefault><![CDATA[' + def + ']]></IsDefault>';
                    Xmlcontactperson += '</contactpersonXML>';
                })

                var data = {
                    CustomerName: $("#txtCustomerName").val().trim(),
                    CustType: parseInt($("#ddlCustType").val()),
                    BillAdd: $("#txtBillAdd").val().trim(),
                    BillAdd2: $("#txtBillAdd2").val().trim(),
                    Zipcode1: $('#txtZipcode1').val().trim(),
                    City1: $("#txtCity1").val().trim(),
                    State1: $("#txtState1").val().trim(),

                    ShipAdd: ($("#chkSame").is(':checked') ? $("#txtBillAdd").val() : $("#txtShipAdd").val()),
                    ShipAdd2: ($("#chkSame").is(':checked') ? $("#txtBillAdd2").val() : $("#txtShipAdd2").val()),
                    Zipcode2: ($("#chkSame").is(':checked') ? $("#txtZipcode1").val() : $("#txtZipcode2").val()),
                    State2: ($("#chkSame").is(':checked') ? $("#txtState1").val() : $("#txtState2").val()),
                    City2: ($("#chkSame").is(':checked') ? $("#txtCity1").val() : $("#txtCity2").val()),

                    TaxId: $("#txtTaxId").val(),
                    Terms: parseInt($("#ddlTerms").val()),
                    PriceLevelAutoId: parseInt($("#ddlPriceLevel").val()),
                    Status: parseInt($("#ddlStatus").val()),
                    SalesPersonAutoId: parseInt($("#ddlSalesPerson").val()),
                    BusinessName: $("#txtBusinessName").val(),
                    OPTLicence: $("#txtOPTLicence").val(),
                    latitude: latitudeship,
                    longitude: longitudeship,
                    latitude1: $("#txtLat1").val(),
                    longitude1: $("#txtLong1").val(),
                    StoreOpenTime: $("#StoreOpenTime").val(),
                    StoreCloseTime: $("#StoreCloseTime").val(),
                    CustomerRemarks: $("#txtCustomerRemark").val(),
                    Xmlcontperson: Xmlcontactperson
                };
                $.ajax({
                    type: "POST",
                    url: "DraftCustomerMaster.aspx/insertCustomer",
                    data: JSON.stringify({ dataValue: JSON.stringify(data) }),
                    contentType: "application/json; charset=utf-8",
                    datatype: "json",
                    beforeSend: function () {
                        $.blockUI({
                            message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                            overlayCSS: {
                                backgroundColor: '#FFF',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (result) {
                        if (result.d != "Session Expired") {
                            if (result.d == "true") {
                                swal("", "Draft customer created successfully.", "success").then(function () {
                                    location.href = '/Sales/DraftCustomerList.aspx';
                                });
                            }
                            else if (result.d == "State1") {
                                swal("", "Invalid State : " + $('#txtState1').val(), "error");
                            }
                            else if (result.d == "State2") {
                                swal("", "Invalid State : " + $('#txtState2').val(), "error");
                            }
                            else if (result.d == "ZipCode1") {
                                swal("Error!", "Invalid Zip Code : " + $('#txtZipcode1').val(), "error");
                            } else if (result.d == "ZipCode2") {
                                swal("Error!", "Invalid Zip Code : " + $('#txtZipcode2').val(), "error");

                            } else if (result.d == "Mobileexists") {
                                swal("Error!", "Mobile No. already exists.", "error");

                            }
                            else if (result.d == "Emailexists") {
                                swal("Error!", "Email already exists.", "error");
                            }
                            else {
                                swal("Error!", result.d, "error");
                            }

                        } else {
                            location.href = '/';
                        }

                    },
                    error: function (result) {
                        swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                    },
                    failure: function (result) {
                        swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                    }
                });
            }
        }
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
}

function MobileLength(e) {

    if ($(e).val().trim() != '') {
        if ($(e).val().length < 10) {
            toastr.error('Please enter a valid 10 digit number.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            $(e).focus();
        }
    }
}
function CustcheckRequiredField() {
    var boolcheck = true;
    $('.req').each(function () {

        if ($(this).val().trim() == '') {
            boolcheck = false;
            $(this).addClass('border-warning');

        } else {
            $(this).removeClass('border-warning');
        }
    });

    $('.ddlreq').each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == 'undefined' || $(this).val() == 'Select') {
            boolcheck = false;
            $(this).closest('div').find('.select2-selection--single').attr('style', 'border:1px solid #FF9149  !important');
        }
        else {
            $(this).removeClass('border-warning');
            $(this).closest('div').find('.select2-selection--single').removeAttr('style');
        }
    });
    return boolcheck;
}
function validateEmail(elementValue) {
    var EmailCodePattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var check = EmailCodePattern.test($(elementValue).val());
    if (!check) {
        toastr.error('Invalid email id.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $(elementValue).val('');
    }
    return;
}
function clearField() {
    $("#ddlType").val('0').change();
    $("#txtContactPersonName").val('');
    $("#txtEmail").val('');
    $("#txtAltEmail").val('');
    $("#txtLandline").val('');
    $("#txtLandline2").val('');
    $("#txtMobileNo").val('');
    $("#txtFaxNo").val('');
    $("#chkIsDefault").prop("checked", false);
}
function editCustomer(CustomerId) {
    $.ajax({
        type: "POST",
        url: "DraftCustomerMaster.aspx/editCustomer",
        data: "{'CustomerId':'" + CustomerId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var CustomerDetails = $.parseJSON(response.d);

                var Details = CustomerDetails[0];

                $("#lblAutoId").text(Details.CustomerAutoId);
                $("#txtCustomerName").val(Details.CustomerName);
                $("#ddlCustType").val(Details.CustomerType).change();
                $("#txtBusinessName").val(Details.BusinessName);
                $("#txtOPTLicence").val(Details.OPTLicence);

                $("#hiddenBillAutoId").val(Details.BillingAutoId);
                $("#txtBillAdd").val(Details.BAddress);
                $("#txtBillAdd2").val(Details.BAddress2);
                $("#txtZipcode1").val(Details.BZipcode);
                $("#txtState1").val(Details.BStateName);
                $("#txtCity1").val(Details.BCity);
                $("#txtLat1").val(Details.BillSA_Lat);
                $("#txtLong1").val(Details.BillSA_Long);
                $("#txtCustomerRemark").val(Details.CustomerRemarks);


                $("#hiddenShipAutoId").val(Details.ShippingAutoId);
                $("#txtShipAdd").val(Details.SAddress);
                $("#txtShipAdd2").val(Details.SAddress2);
                $("#txtZipcode2").val(Details.SZipcode);
                $("#txtState2").val(Details.SStateName);
                $("#txtCity2").val(Details.SCity);
                $("#txtLat").val(Details.SA_Lat);
                $("#txtLong").val(Details.SA_Long);
                if (Details.priceLevelId) {
                    $("#ddlPriceLevel").val(Details.priceLevelId).change();
                }
                $("#txtTaxId").val(Details.TaxId);
                $("#ddlTerms").val(Details.Terms).change();
                $("#ddlStatus").val(Details.Status).change();



                if (Details.SalesPersonAutoId != '')
                    $("#ddlSalesPerson").val(Details.SalesPersonAutoId).change();

                $("#StoreOpenTime").val(Details.OptimotwFrom).change();
                $("#StoreCloseTime").val(Details.OptimotwTo).change();

                $("#btnSave").hide();
                $("#btnReset").hide();
                $("#btnUpdate").show();
                $("#btnCreateCustomer").show();
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function EditContactPerson(CustomerId) {
    $.ajax({
        type: "POST",
        async: false,
        url: "DraftCustomerMaster.aspx/EditContactPerson",
        data: "{'CustomerId':'" + CustomerId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var ContactPerson = $(xmldoc).find("Table");

                $("#tblContactDetails tbody tr").remove();
                var row = $("#tblContactDetails thead tr").clone(true);
                $.each(ContactPerson, function () {

                    $(".TypeAutoId", row).text($(this).find("Type").text());
                    $(".Type", row).text($(this).find("TypeName").text());
                    $(".ContactPerson", row).text($(this).find("ContactPerson").text());
                    $(".Mobile", row).text($(this).find("MobileNo").text());
                    $(".Landline", row).text($(this).find("Landline").text());
                    $(".Landline2", row).text($(this).find("Landline2").text());
                    $(".Fax", row).text($(this).find("Fax").text());
                    $(".Email", row).text($(this).find("Email").text());
                    $(".AltEmail", row).text($(this).find("AlternateEmail").text());
                    if ($(this).find("ISDefault").text() == 1) {
                        $(".IsDefault", row).html("<input type='radio' name='default' class='radio border-primary' checked>");
                    }
                    else {
                        $(".IsDefault", row).html("<input type='radio' name='default' class='radio border-primary' >");
                    }
                    $(".Action", row).html('<a title="Delete" href="javascript: void (0)" onclick="deleterecord(this)"><span class="ft-x"></span></a>');
                    $("#tblContactDetails tbody").append(row);
                    row = $("#tblContactDetails tbody tr:last").clone(true);
                    $("#tblContact").show();
                });
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function deleterecord(e) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this contact person",
        icon: "warning",
        showCancelButton: true,
        allowOutsideClick: false,
        closeOnClickOutside: false,
        buttons: {
            cancel: {
                text: "No, Cancel",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, Delete it",
                value: true,
                visible: true,
                className: "",
                closeModal: true
            }
        }
    }).then(isConfirm => {
        if (isConfirm) {
            swal("", "Contact person removed from list", "success");
            $(e).closest('tr').remove();
        }
    })
}
/*------------------------------------------------------Insert Customer------------------------------------------------------------*/
function CreateNewCustomer(GenStatus) {
    if (CustcheckRequiredField()) {
        if ($("#chkSame").is(':checked') == true) {
            var latitudeship = $("#txtLat1").val();
            var longitudeship = $("#txtLong1").val();


        } else {
            var latitudeship = $("#txtLat").val();
            var longitudeship = $("#txtLong").val();
        }
        if (latitudeship == "" || longitudeship == "") {
            toastr.error('Required Shipping lat & long.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            return false;

        }
        else if ($("#txtLat1").val() == "" || $("#txtLong1").val() == "") {
            toastr.error('Required Billing lat & long.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            return false;

        }
        if ($("#tbodyContactPerson tr").length == 0) {
            toastr.error('Please select atleast one contact person', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            return false;
        }
        var flage = false;
        if ($('#tblContactDetails tbody tr').length > 0) {
            $("#tblContactDetails tbody tr").each(function () {
                if ($(this).find('.IsDefault input').prop('checked') == true) {
                    flage = true;
                }
            })
            if (!flage) {
                toastr.error('Atleast one contact person default.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            }
            else {
                var Xmlcontactperson = '', def = 0;
                $("#tblContactDetails tbody tr").each(function () {
                    Xmlcontactperson += '<contactpersonXML>';
                    Xmlcontactperson += '<TypeAutoId><![CDATA[' + $(this).find('.TypeAutoId').html() + ']]></TypeAutoId>';
                    Xmlcontactperson += '<ContactPerson><![CDATA[' + $(this).find('.ContactPerson').html() + ']]></ContactPerson>';
                    Xmlcontactperson += '<Mobile><![CDATA[' + $(this).find('.Mobile').html() + ']]></Mobile>';
                    Xmlcontactperson += '<Landline><![CDATA[' + $(this).find('.Landline').html() + ']]></Landline>';
                    Xmlcontactperson += '<Landline2><![CDATA[' + $(this).find('.Landline2').html() + ']]></Landline2>';
                    Xmlcontactperson += '<Fax><![CDATA[' + $(this).find('.Fax').html() + ']]></Fax>';
                    Xmlcontactperson += '<Email><![CDATA[' + $(this).find('.Email').html() + ']]></Email>';
                    Xmlcontactperson += '<AltEmail><![CDATA[' + $(this).find('.AltEmail').html() + ']]></AltEmail>';
                    if ($(this).find('.IsDefault input').prop("checked") == true) {
                        def = 1;
                    }
                    else {
                        def = 0;
                    }
                    Xmlcontactperson += '<IsDefault><![CDATA[' + def + ']]></IsDefault>';
                    Xmlcontactperson += '</contactpersonXML>';
                })

                var data = {
                    GenStatus: GenStatus,
                    CustomerAutoId: $("#lblAutoId").text(),
                    BillAutoId: $("#hiddenBillAutoId").val(),
                    ShipAutoId: $("#hiddenShipAutoId").val(),
                    CustomerName: $("#txtCustomerName").val().trim(),
                    CustType: parseInt($("#ddlCustType").val()),

                    BillAdd: $("#txtBillAdd").val().trim(),
                    BillAdd2: $("#txtBillAdd2").val().trim(),
                    Zipcode1: $('#txtZipcode1').val().trim(),
                    City1: $("#txtCity1").val().trim(),
                    State1: $("#txtState1").val().trim(),

                    ShipAdd: ($("#chkSame").is(':checked') ? $("#txtBillAdd").val() : $("#txtShipAdd").val()),
                    ShipAdd2: ($("#chkSame").is(':checked') ? $("#txtBillAdd2").val() : $("#txtShipAdd2").val()),
                    Zipcode2: ($("#chkSame").is(':checked') ? $("#txtZipcode1").val() : $("#txtZipcode2").val()),
                    State2: ($("#chkSame").is(':checked') ? $("#txtState1").val() : $("#txtState2").val()),
                    City2: ($("#chkSame").is(':checked') ? $("#txtCity1").val() : $("#txtCity2").val()),


                    TaxId: $("#txtTaxId").val(),
                    Terms: parseInt($("#ddlTerms").val()),
                    PriceLevelAutoId: parseInt($("#ddlPriceLevel").val()),
                    Status: parseInt($("#ddlStatus").val()),
                    SalesPersonAutoId: parseInt($("#ddlSalesPerson").val()),
                    BusinessName: $("#txtBusinessName").val(),
                    OPTLicence: $("#txtOPTLicence").val(),
                    latitude: latitudeship,
                    longitude: longitudeship,
                    latitude1: $("#txtLat1").val(),
                    longitude1: $("#txtLong1").val(),
                    StoreOpenTime: $("#StoreOpenTime").val(),
                    StoreCloseTime: $("#StoreCloseTime").val(),
                    CustomerRemarks: $("#txtCustomerRemark").val(),

                    Xmlcontperson: Xmlcontactperson
                };
                $.ajax({
                    type: "POST",
                    url: "DraftCustomerMaster.aspx/UpdateCustomer",
                    data: JSON.stringify({ dataValue: JSON.stringify(data) }),
                    contentType: "application/json; charset=utf-8",
                    datatype: "json",
                    async: false,
                    beforeSend: function () {
                        $.blockUI({
                            message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                            overlayCSS: {
                                backgroundColor: '#FFF',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (result) {
                        if (result.d != "Session Expired") {
                            if (result.d == "true") {
                                if (GenStatus == 0) {
                                    swal("", "Draft customer updated successfully.", "success").then(function () {
                                        window.location.href = "/Sales/DraftCustomerList.aspx";

                                    });
                                } else {
                                    swal("", "Customer created successfully.", "success").then(function () {
                                        location.href = '/Sales/DraftCustomerList.aspx';
                                    });
                                }
                            } else if (result.d == "Mobileexists") {
                                swal("Error!", "Mobile No. already exists.", "error");

                            }
                            else if (result.d == "State1") {
                                swal("", "Invalid State : " + $('#txtState1').val(), "error");
                            }
                            else if (result.d == "State2") {
                                swal("", "Invalid State : " + $('#txtState2').val(), "error");
                            }
                            else if (result.d == "Emailexists") {
                                swal("Error!", "Email already exists.", "error");
                            }
                            else {
                                swal("Error!", result.d, "error");
                            }

                        } else {
                            location.href = '/';
                        }

                    },
                    error: function (result) {
                        swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                    },
                    failure: function (result) {
                        swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                    }
                });
            }
        }
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
}
