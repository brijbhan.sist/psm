﻿var GweighTax = 0.00, ReferenceOrderAutoId = 0;  // Global variable
$(document).ready(function () {
    blockCopyPaste();
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    var CreditAutoId = getQueryString('PageId');
    bindDropdown();
    if (CreditAutoId != null) {
        $("#btnSave").hide();
        $("#btnReset").hide();
        $("#btnUpdate").show();
        $("#btnPrintOrder").show();
        $("#CreditAutoId").val(CreditAutoId);
        editCredit(CreditAutoId);
    } else {
        $("#btnSave").show();
        $("#btnReset").show();
        $("#btnUpdate").hide();
        $("#btnPrintOrder").hide();
    }

    $("#ddlProduct").select2().on("select2:select", function (e) {
    });

    $("#ddlCustomer").select2().on("select2:select", function (e) {
    });
});
function bindDropdown() {

    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/WCreditMemo.asmx/bindAllDropdown",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            if (response.d != "Session Expired") {

                var DropDown = $.parseJSON(response.d);
                for (var i = 0; i < DropDown.length; i++) {
                    var AllDropDownList = DropDown[i];

                    var CustomerList = AllDropDownList.Customer;
                    var ddlCustomer = $("#ddlCustomer");
                    $("#ddlCustomer option:not(:first)").remove();
                    for (var k = 0; k < CustomerList.length; k++) {
                        var Customer = CustomerList[k];
                        var option = $("<option />");
                        option.html(Customer.Customer);
                        option.val(Customer.AutoId);
                        ddlCustomer.append(option);
                    }
                    ddlCustomer.select2();

                    var CreditTypeList = AllDropDownList.CreditType;
                    for (var p = 0; p < CreditTypeList.length; p++) {
                        var CreditType = CreditTypeList[p];
                        $('#txtOrderDate').val(CreditType.CurrentDate);
                        $('#CreditType').val(CreditType.CreditType);
                    }



                    var Product = AllDropDownList.Product;
                    var ddlProduct = $("#ddlProduct");
                    $("#ddlProduct option:not(:first)").remove();
                    for (var j = 0; j < Product.length; j++) {
                        var ProductList = Product[j];
                        var option = $("<option />");
                        option.html(ProductList.ProductName);
                        option.val(ProductList.AutoId);
                        option.attr('WeightOz', ProductList.WeightOz);
                        option.attr('MLQty', ProductList.MLQty);
                        ddlProduct.append(option);
                    }
                    ddlProduct.select2();

                    var CreditMemoType = AllDropDownList.CreditMemoType;
                    var ddlCredit = $("#ddlCreditMemoType");
                    $("#ddlCreditMemoType option:not(:first)").remove();
                    for (var j = 0; j < CreditMemoType.length; j++) {
                        var TypeList = CreditMemoType[j];
                        var option = $("<option />");
                        option.html(TypeList.CreditType);
                        option.val(TypeList.AutoId);
                        ddlCredit.append(option);
                    }
                    $("#ddlCreditMemoType").val(1).change();
                }
            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
var MLTaxRate = 0.00;
function changeCreditType() {
    if ($("#ddlCustomer").val() != "0" && $("#ddlCreditMemoType").val() != "0") {
        $("#txtBarcode").removeAttr('disabled')
        $("#btnAdd").removeAttr('disabled')
    } else {
        $("#btnAdd").attr('disabled', 'disabled')
        $("#txtBarcode").prop('disabled', true)
    }
}
function changeCustomer() {

    if ($("#ddlCustomer").val() != "0" && $("#ddlCreditMemoType").val() != "0") {
        $("#txtBarcode").removeAttr('disabled')
        $("#btnAdd").removeAttr('disabled')
    } else {
        $("#btnAdd").attr('disabled', 'disabled')
        $("#txtBarcode").prop('disabled', true)
    }
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/WCreditMemo.asmx/BindProduct",
        data: "{'CustomerAutoId':'" + $("#ddlCustomer").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                //var product = $(xmldoc).find("Table");
                var MLTaxDetails = $(xmldoc).find("Table1");
                var TaxDetails = $(xmldoc).find("Table2");
                var WeighTax = $(xmldoc).find("Table3");
                var DueAmount = $(xmldoc).find("Table4");

                if (WeighTax.length > 0) {
                    if ($(WeighTax).find('Value').text() != '') {
                        GweighTax = $(WeighTax).find("Value").text();
                    }
                }
                if (MLTaxDetails.length > 0) {
                    MLTaxRate = $(MLTaxDetails).find('TaxRate').text();
                }

                $("#ddlTaxType option").remove();
                $.each(TaxDetails, function () {
                    $("#ddlTaxType").append("<option taxvalue='" + $(this).find("Value").text() + "' value='" + $(this).find("AutoId").text() + "'>" + $(this).find("TaxableType").text() + '(' + $(this).find("Value").text() + ')' + "</option>");
                });
                if (Number($("#ddlCustomer").val()) > 0) {
                    $("#ddlTaxType").removeAttr('disabled');
                    $("#txtReqQty").removeAttr('disabled');
                    $("#ddlUnitType").removeAttr('disabled');
                    $("#ddlProduct").removeAttr('disabled');
                }
                else {
                    $("#ddlTaxType").attr('disabled', true);
                    $("#txtReqQty").attr('disabled', true);
                    $("#ddlUnitType").attr('disabled', true);
                    $("#ddlProduct").attr('disabled', true);
                }
                var amtDue = 0.00, sn = 0;
                if (DueAmount.length > 0) {
                    $("#tblduePayment tbody tr").remove();
                    var row = $("#tblduePayment thead tr").clone(true);
                    $.each(DueAmount, function () {
                        sn = Number(sn) + 1;
                        $(".OrderNo", row).html('<span OrderAutoId=' + $(this).find("AutoId").text() + '>' + $(this).find("OrderNo").text() + '</span>');
                        $(".OrderDate", row).text($(this).find("OrderDate").text());
                        $(".AmtDue", row).text($(this).find("AmtDue").text());
                        $(".SrNo", row).html(sn);
                        if ($(this).find("AutoId").text() == ReferenceOrderAutoId) {
                            $(".Actions", row).html('<input type="checkbox" onclick="checkItem(this)" name="Action">');
                        }
                        else {
                            $(".Actions", row).html('<input type="checkbox" onclick="checkItem(this)" name="Action">');
                        }

                        amtDue += parseFloat($(this).find("AmtDue").text());
                        $('#tblduePayment tbody').append(row);
                        row = $("#tblduePayment tbody tr:last").clone(true);
                    });
                    $("#panelDueAmount").show();
                    $("#TotalDueAmount").html(parseFloat(amtDue).toFixed(2));
                }
                else {
                    $("#panelDueAmount").hide();
                    $("#tblduePayment tbody tr").remove();
                }
            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function checkItem(e) {
    debugger
    var i = 1;
    if ($(e).prop('checked')) {
        i = 0;
    }
    $("#tblduePayment tbody tr").each(function () {
        $(this).find(".Actions input").prop('checked', false)
    });
    var row = $(e).closest('tr');
    if (i == 0)
        $(row).find(".Actions input").prop('checked', true);
}
function ChangeProduct() {
    var productAutoId = $("#ddlProduct option:selected").val();
    if (productAutoId == '0') {
        $("#txtReqQty").val(1);
        $("#alertStockQty").hide();
        $("#ddlUnitType option:not(:first)").remove();
        $("#ddlUnitType").val(0);
    }
    else {
        $.ajax({
            type: "POST",
            url: "/Sales/WebAPI/WCreditMemo.asmx/bindUnitType",
            data: "{'ProductAutoId':" + productAutoId + "}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },

            success: function (response) {
                if (response.d != "Session Expired") {
                    var xmldoc = $.parseXML(response.d);
                    var unitType = $(xmldoc).find("Table");
                    var unitDefault = $(xmldoc).find("Table1");
                    var count = 0;

                    $("#ddlUnitType option:not(:first)").remove();
                    $.each(unitType, function () {
                        $("#ddlUnitType").append("<option value='" + $(this).find("AutoId").text() + "' QtyPerUnit='" + $(this).find("Qty").text() + "' >" + $(this).find("UnitType").text() + " (" + $(this).find("Qty").text() + " pcs" + ")</option>");
                    });

                    if (unitDefault.length > 0) {
                        $("#ddlUnitType").val($(unitDefault).find('AutoId').text()).change();
                    } else {
                        $("#ddlUnitType option:not(first)").remove();
                        $("#ddlUnitType").val(0);
                    }
                    // BindunitDetails();// New function Added by Rizwan Ahmad on 11/14/2019
                } else {
                    location.href = '/';
                }

            },
            error: function (result) {
                console.log(result.d);
            },
            failure: function (result) {
                console.log(result.d);
            }
        });
    }
}
function readBarcode() {
    debugger
    blockCopyPaste();
    if ($("#txtBarcode").val() != '') {
        var chkIsTaxable = $('#chkIsTaxable').prop('checked');
        var IsTaxable = 0;
        var taxType = ''
        if (chkIsTaxable == true) {
            IsTaxable = 1;

            taxType = '<tax class="la la-check-circle success"></tax>';
        }
        var data = {
            CustomerAutoId: $("#ddlCustomer").val(),
            BarcodeNo: $("#txtBarcode").val(),
        }
        $.ajax({
            type: "POST",
            url: "/Sales/WebAPI/WCreditMemo.asmx/BarcodeReader",
            data: "{'dataValues':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d != "Session Expired") {
                    if (response.d == 'InvalidBarcode') {
                        swal({
                            title: "",
                            text: "Barcode does not exists.",
                            icon: "error",
                            closeOnClickOutside: false
                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                $("#txtBarcode").val('');
                                $("#txtBarcode").focus();
                            }
                        });
                        $("#yes_audio")[0].play();
                        $("#txtReqQty").val("0");
                        $("#txtBarcode").val('');
                        $("#txtBarcode").focus();
                        $("#txtBarcode").blur();
                    }
                    else if (response.d == 'InaciveProduct') {
                        $('#txtBarcode').removeAttr('onchange');
                        $('#txtBarcode').attr('onchange', 'invalidbarcodechnage()');
                        swal({
                            title: "",
                            text: "Inactive product can't be sold.",
                            icon: "error",
                            allowOutsideClick: false,
                            closeOnClickOutside: false,
                            allowEscapeKey: false
                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                $("#txtBarcode").val('');
                                $("#txtBarcode").focus();
                                $("#txtBarcode").attr('onchange', 'readBarcode()');
                            }
                        });
                        invalidbarcodechnage();
                    }
                    else {
                        var flag = false;
                        var xmldoc = $.parseXML(response.d);
                        var items = $(xmldoc).find("Table");
                        var WeighTax = $(xmldoc).find("Table1");
                        var Unit = $(xmldoc).find("Table1");
                        var UnitDetails = [];

                        $.each(Unit, function () {
                            UnitDetails.push({
                                AutoId: $(this).find("AutoId").text(),
                                UnitType: $(this).find("UnitType").text(),
                            });
                        });
                        localStorage.setItem("UnitItems", JSON.stringify(UnitDetails))
                        if (WeighTax.length > 0) {
                            if ($(WeighTax).find('Value').text() != '') {
                                GweighTax = $(WeighTax).find("Value").text();
                            }
                        }

                        $("#emptyTable").hide();
                        if (items.length > 0) {
                            if (Number($(items).find('ReturnQty').text()) <= Number($(items).find('Qty').text())) {
                                $("#tblProductDetail tbody tr").each(function () {

                                    if ($(this).find(".ProName span").attr('ProductAutoId') == $(items).find('ProductAutoId').text() &&
                                        $(this).find(".UnitType span").attr('UnitAutoId') == $(items).find('UnitAutoId').text()) {
                                        var qty = (parseInt($(items).find('ReturnQty').text()) + parseInt($(this).find('.ReturnQty input').val())) || 1;
                                        $(this).find('.ReturnQty input').val(qty);
                                        $(this).find('.FreshReturn input').val(qty);
                                        var NetPrice = parseFloat($(items).find('NetPrice').text()) * parseInt(qty);
                                        $(this).find('.NetPrice').text(NetPrice.toFixed(2));
                                        flag = true;
                                        $('#tblProductDetail tbody tr:first').before($(this));
                                    }
                                });
                                if (!flag) {
                                    var row = $("#tblProductDetail thead tr").clone(true);
                                    $.each(items, function () {

                                        $(".ProId", row).text($(this).find("ProductId").text());
                                        if (IsTaxable == 1) {
                                            $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span>" + " <product class='badge badge badge-pill badge-danger'>Taxable</product>");
                                        }
                                        else {
                                            $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span>");
                                        }
                                        $(".UnitType", row).html("<span UnitAutoId='" + $(this).find("UnitAutoId").text() + "' QtyPerUnit='" + $(this).find("QtyPerUnit").text() + "'>" + $(this).find("UnitType").text() + " (" + $(this).find("QtyPerUnit").text() + " pcs" + ")</span>");
                                        $(".TaxRate", row).html(taxType + "<span WeightOz='" + $(this).find("WeightOz").text() + "' MLQty='" + $(this).find("MLQty").text() + "' IsTaxable='" + IsTaxable + "'> </span>");
                                        $(".ReturnQty", row).html("<span MaxQty ='" + $(this).find('Qty').text() + "'></span><input type='text'  maxlength='5' class='form-control input-sm  border-primary text-center' onkeyup='getcalCulation(this)' onkeypress='return isNumberKey(event)' style='width:100px;' value='" + $(this).find("ReturnQty").text() + "' />");
                                        $(".UnitPrice", row).html("<input type='text' class='form-control input-sm border-primary' maxlength='6' onkeyup='changePrice(this)' onkeypress='return isNumberDecimalKey(event,this)' style='width:100px;text-align:right;' value='" + $(this).find("CostPrice").text() + "' />");
                                        $(".SRP", row).text($(this).find("SRP").text());
                                        $(".AcceptedQty", row).text($(this).find("ReturnQty").text());
                                        $(".NetPrice", row).text($(this).find("NetPrice").text());

                                        $(".OM_MinPrice", row).text($(this).find("OM_MinPrice").text());
                                        $(".OM_CostPrice", row).text($(this).find("OM_CostPrice").text());
                                        $(".OM_BasePrice", row).text($(this).find("OM_BasePrice").text());

                                        $(".FreshReturn", row).html("<div class='input-group'><div class='input-group-prepend'><input type='text' onkeyup='calculateReturnQty(this)' class='form-control input-sm border-primary text-center' style='width:50px;padding:0.25rem 0.25rem;margin-right: 2px;' value='" + $(this).find("ReturnQty").text() + "'  onkeypress='return isNumberKey(event)'/><select style='width:58px;padding:0' class='freshunit form-control input-sm border-primary'  disabled='disabled' ></select></div></div>")
                                        $(".DemageReturn", row).html("<div class='input-group'><div class='input-group-prepend'><input type='text' onkeyup='calculateReturnQty(this)' class='form-control input-sm border-primary text-center' style='width:50px;padding:0.25rem 0.25rem;margin-right: 2px;' value='0'  onkeypress='return isNumberKey(event)'/><select style='width:58px;padding:0' class='freshunit form-control input-sm border-primary'  disabled='disabled' ></select></div></div>")


                                        $(".Action", row).html("<a href='javascript:;' id='deleterow' onclick='deleterows(this)'><span class='ft-x'></span></a>");
                                        if ($('#tblProductDetail tbody tr').length > 0) {
                                            $('#tblProductDetail tbody tr:first').before(row);
                                        }
                                        else {
                                            $('#tblProductDetail tbody').append(row);
                                        }
                                        var getUnit = JSON.parse(localStorage.getItem('UnitItems'));
                                        $.grep(getUnit, function (e) {

                                            row.find('.freshunit').append($("<option value=" + e.AutoId + " selected='selected'>" + e.UnitType + "</option>"));
                                            row.find('.demageunit').append($("<option value=" + e.AutoId + " selected='selected'>" + e.UnitType + "</option>"));

                                        });
                                        row = $("#tblProductDetail tbody tr:last").clone(true);

                                    });
                                }
                                $("#txtReqQty").val('');
                                $("#txtBarcode").val('');
                                $("#txtBarcode").focus();
                                $("#ddlProduct").val('0').change();
                            } else {
                                var msg = "";
                                if (Number($(items).find('UnitAutoId').text()) == 1) {
                                    msg = "Return qty should be less or equal to Order Qty : " + $(items).find('Qty').text() + '[case]';
                                    swal("", msg, "error");
                                } else if (Number($(items).find('UnitAutoId').text()) == 2) {
                                    msg = "Return qty should be less or equal to Order Qty : " + $(items).find('Qty').text() + '[box]';
                                    swal("", msg, "error");
                                } else {
                                    msg = "Return qty should be less or equal to Order Qty : " + $(items).find('Qty').text() + '[pcs]';
                                    swal("", msg, "error");
                                }
                            }
                            $("#txtReqQty").val(1);
                            $("#txtBarcode").val('');
                            $("#txtBarcode").focus();
                            calTotalAmount();
                            //$('#chkIsTaxable').prop('checked', false);
                            blockCopyPaste();
                            toastr.success('Product added successfully .', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                            if ($("#hEmpTypeNo").val() != "10") {
                                $(".FreshReturn").hide();
                                $(".DemageReturn").hide();
                            }
                            else {
                                $(".FreshReturn").show();
                                $(".DemageReturn").show();
                            }
                        } else {

                            $("#txtBarcode").val('');
                        }
                    }
                } else {
                    location.href = '/';
                }
            },
            failure: function (result) {
                swal("", result.d, "error");
            },
            error: function (result) {
                swal("", result.d, "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
}
function invalidbarcodechnage() {
    $("#txtBarcode").val('');
    $("#txtBarcode").focus();
}

function AddinList() {
    $("#alertStockQty").hide();
    var chkIsTaxable = $('#chkIsTaxable').prop('checked');
    var IsTaxable = 0;
    var taxType = ''
    if (chkIsTaxable == true) {
        IsTaxable = 1;
        taxType = '<tax class="la la-check-circle success"></tax>';
    }

    //var MLQty = $("#ddlProduct option:selected").attr('MLQty');
    //var WeightOz = $('#ddlProduct option:selected').attr('WeightOz');
    var FreshReturnUnitAutoId = $("#ddlUnitType").val();
    var DamageReturnUnitAutoId = $("#ddlUnitType").val();
    if (CustCreditmemoRequiredField()) {
        var data = {
            CustomerAutoId: $("#ddlCustomer").val(),
            ProductAutoId: $("#ddlProduct").val(),
            UnitAutoId: $("#ddlUnitType").val(),
            Qty: $("#txtReqQty").val(),
        }
        $.ajax({
            type: "POST",
            url: "/Sales/WebAPI/WCreditMemo.asmx/ValidateCreditMemo",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            async: false,
            success: function (response) {
                if (response.d != "Session Expired") {
                    $("#ddlCustomer").attr('disabled', true);
                    var flag = false;
                    var xmldoc = $.parseXML(response.d);
                    var items = $(xmldoc).find("Table");
                    var Unit = $(xmldoc).find("Table1");
                    var UnitDetails = [];
                    $.each(Unit, function () {
                        UnitDetails.push({
                            AutoId: $(this).find("AutoId").text(),
                            UnitType: $(this).find("UnitType").text(),
                        });
                    });
                    localStorage.setItem("UnitItems", JSON.stringify(UnitDetails))
                    $("#emptyTable").hide();
                    if (items.length > 0) {
                        if (Number($(items).find('ReturnQty').text()) <= Number($(items).find('Qty').text())) {
                            $("#tblProductDetail tbody tr").each(function () {
                                if ($(this).find(".ProName span").attr('ProductAutoId') == $(items).find('ProductAutoId').text() &&
                                    $(this).find(".UnitType span").attr('UnitAutoId') == $(items).find('UnitAutoId').text()) {
                                    var qty = (parseInt($(items).find('ReturnQty').text()) + parseInt($(this).find('.ReturnQty input').val())) || 1;
                                    $(this).find('.ReturnQty input').val(qty);
                                    var NetPrice = parseFloat($(this).find('.UnitPrice input').val()) * parseInt(qty);
                                    $(this).find('.NetPrice').text(NetPrice.toFixed(2));
                                    flag = true;
                                    $('#tblProductDetail tbody tr:first').before($(this));
                                    toastr.success('Product added successfully .', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                                    if ($("#hEmpTypeNo").val() != "10") {
                                        $(".FreshReturn").hide();
                                        $(".DemageReturn").hide();
                                    }
                                    else {
                                        $(".FreshReturn").show();
                                        $(".DemageReturn").show();
                                    }
                                }
                                else if ($(this).find(".ProName span").attr('ProductAutoId') == $(items).find('ProductAutoId').text() &&
                                    $(this).find(".UnitType span").attr('UnitAutoId') != $(items).find('UnitAutoId').text()) {
                                    swal("", "You can't add different unit of added product.", "error");
                                    flag = true;
                                }
                            });
                            if (!flag) {
                                var row = $("#tblProductDetail thead tr").clone(true);
                                $.each(items, function () {
                                    $(".ProId", row).text($(this).find("ProductId").text());
                                    if (IsTaxable == 1) {
                                        $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span>" + " <product class='badge badge badge-pill badge-danger'>Taxable</product>");
                                    }
                                    else {
                                        $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span>");
                                    }
                                    $(".UnitType", row).html("<span UnitAutoId='" + $(this).find("UnitAutoId").text() + "' QtyPerUnit='" + $(this).find("QtyPerUnit").text() + "'>" + $(this).find("UnitType").text() + " (" + $(this).find("QtyPerUnit").text() + " pcs" + ")</span>");
                                    $(".TaxRate", row).html(taxType + "<span MLQty='" + $(this).find("MLQty").text() + "' IsTaxable='" + IsTaxable + "' WeightOz='" + $(this).find("WeightOz").text() + "'> </span>");
                                    $(".ReturnQty", row).html("<span MaxQty ='" + $(this).find('Qty').text() + "'></span><input type='text' class='form-control input-sm  border-primary text-center' maxlength='5' onkeyup='getcalCulation(this)' onkeypress='return isNumberKey(event)' style='width:100px;' value='" + $(this).find("ReturnQty").text() + "' />");
                                    $(".UnitPrice", row).html("<input type='text' class='form-control input-sm border-primary' maxlength='6' onkeyup='changePrice(this)' onpaste='return isNumberDecimalKey(event,this)' onkeypress='return isNumberDecimalKey(event,this)' style='width:100px;text-align:right;' value='" + $(this).find("CostPrice").text() + "' />");
                                    $(".SRP", row).text($(this).find("SRP").text());
                                    $(".AcceptedQty", row).text($(this).find("ReturnQty").text());
                                    $(".NetPrice", row).text($(this).find("NetPrice").text());
                                    $(".OM_MinPrice", row).text($(this).find("OM_MinPrice").text());
                                    $(".OM_CostPrice", row).text($(this).find("OM_CostPrice").text());
                                    $(".OM_BasePrice", row).text($(this).find("OM_BasePrice").text());
                                    $(".FreshReturn", row).html("<div class='input-group'><div class='input-group-prepend'><input type='text' onkeyup='calculateReturnQty(this)' class='form-control input-sm border-primary text-center' style='width:50px;padding:0.25rem 0.25rem;margin-right: 2px;' value='" + $(this).find("ReturnQty").text() + "'  onkeypress='return isNumberKey(event)'/><select style='width:58px;padding:0' class='freshunit form-control input-sm border-primary'  disabled='disabled' ></select></div></div>")
                                    $(".DemageReturn", row).html("<div class='input-group'><div class='input-group-prepend'><input type='text'  onkeyup='calculateReturnQty(this)' class='form-control input-sm border-primary text-center' style='width:50px;padding:0.25rem 0.25rem;margin-right: 2px;' value='0'  onkeypress='return isNumberKey(event)'/><select style='width:58px;padding:0' class='freshunit form-control input-sm border-primary'  disabled='disabled' ></select></div></div>")

                                    $(".Action", row).html("<a href='javascript:;' id='deleterow' onclick='deleterows(this)'><span class='ft-x'></span></a>");
                                    if ($('#tblProductDetail tbody tr').length > 0) {
                                        $('#tblProductDetail tbody tr:first').before(row);
                                    }
                                    else {
                                        $('#tblProductDetail tbody').append(row);
                                    }
                                    if ($("#hEmpTypeNo").val() != "10") {
                                        $(".FreshReturn").hide();
                                        $(".DemageReturn").hide();
                                    }
                                    else {
                                        $(".FreshReturn").show();
                                        $(".DemageReturn").show();
                                    }
                                    var getUnit = JSON.parse(localStorage.getItem('UnitItems'));
                                    $.grep(getUnit, function (e) {
                                        if (FreshReturnUnitAutoId == e.AutoId) {
                                            row.find('.freshunit').append($("<option value=" + e.AutoId + " selected='selected'>" + e.UnitType + "</option>"));
                                        }

                                        if (DamageReturnUnitAutoId == e.AutoId) {
                                            row.find('.demageunit').append($("<option value=" + e.AutoId + " selected='selected'>" + e.UnitType + "</option>"));
                                        }
                                    });
                                    row = $("#tblProductDetail tbody tr:last").clone(true);

                                });
                                toastr.success('Product added successfully .', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                            }
                            $("#txtReqQty").val('');
                            $("#ddlProduct").val('0').change();
                        } else {

                            if (Number($(items).find('UnitAutoId').text()) == 1) {
                                swal("", "Return qty should be less or equal to Order Qty : " + $(items).find('Qty').text() + '[case]', "error");
                            } else if (Number($(items).find('UnitAutoId').text()) == 2) {
                                swal("", "Return qty should be less or equal to Order Qty : " + $(items).find('Qty').text() + '[box]', "error");
                            } else {
                                swal("", "Return qty should be less or equal to Order Qty : " + $(items).find('Qty').text() + '[pcs]', "error");
                            }
                        }

                        $("#txtReqQty").val(1);
                        $("#ddlUnitType").val(0);
                        calTotalAmount();
                        $('#chkIsTaxable').prop('checked', false);
                        blockCopyPaste();
                    } else {
                        swal("", "No order qty ramining for credit memo", "error");
                    }
                } else {
                    location.href = '/';
                }
            },
            failure: function (result) {
                swal("", result.d, "error");
            },
            error: function (result) {
                swal("", result.d, "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
}

function calculateReturnQty(e) {
    var row = $(e).closest('tr');
    var ReturnQty = $(row).find('.FreshReturn input').val();
    var DamageQty = $(row).find('.DemageReturn input').val();
    var AcceptedQty = $(row).find('.ReturnQty   input').val();
    ReturnFm = parseInt(ReturnQty) + parseInt(DamageQty);
    if (ReturnQty == "") {
        $(row).find('.FreshReturn input').val(0)
    }
    else if (DamageQty == "") {
        $(row).find('.DemageReturn input').val(0)
    }
    else if (ReturnFm > AcceptedQty) {
        toastr.error('Fresh+Damage quantity should be equal to accepted quantity.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $(row).find('.FreshReturn input').val(0);
        $(row).find('.DemageReturn input').val(0);
    }
}
function getcalCulation(e) {
    var tr = $(e).closest('tr');
    //var maxReturnQty = 0.00;
    //maxReturnQty = tr.find('.ReturnQty span').attr('maxqty');
    //var ReturnQty = 0;
    //if (tr.find('.ReturnQty input').val() != '') {
    //    ReturnQty = tr.find('.ReturnQty input').val();
    //}
    //tr.find('.FreshReturn input').val(ReturnQty);
    //tr.find('.AcceptedQty').text(ReturnQty);
    //var UnitType = tr.find('.UnitType span').attr('unitautoid');
    //if (Number(ReturnQty) > Number(maxReturnQty)) {

    //if (Number(UnitType) == 1) {
    //    swal("", "Return qty should be less or equal to Order Qty : " + maxReturnQty + '[case]', "error");

    //} else if (Number(UnitType) == 3) {
    //    swal("Warning!", "Return qty should be less or equal to Order Qty : " + maxReturnQty + '[Box]', "error");

    //} else {
    //    swal("Warning!", "Return qty should be less or equal to Order Qty : " + maxReturnQty + '[pcs]', "error");
    //}
    //    $(e).val(maxReturnQty);
    //}
    changePrice(e);
}

function deleterow(e) {
    $(e).closest('tr').remove();
    swal("", "Item deleted successfully.", "success");
    if ($("#tblProductDetail tbody tr").length == 0) {
        $("#ddlCustomer").attr('disabled', false);
        $("#emptyTable").show();
        $("#txtWeightQty").val('0.00'); //Added line 519 to 521 for reset these three field. when we delete all list item then these field was not resetting.
        $("#txtWeightTax").val('0.00');
        $("#txtGrandTotal").val('0.00');
        if ($("#ddlCustomer").val() == '0') {
            $("#ddlTaxType").val('');//Added on 11/12/2019 By Rizwan Ahmad
        }
    } else {
        $("#emptyTable").hide();

    }
    calTotalAmount();
}

function deleterows(e) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this item.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, Cancel.",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, Delete it.",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            deleterow(e);
        }
    })
}


function calTotalAmount() {
    debugger;
    if ($("#ddlCreditMemoType").val() == '0') {
        $("#ddlCreditMemoType").addClass('border-warning');
    }
    else {
        $("#ddlCreditMemoType").removeClass('border-warning');
    }
    var total = 0.00;
    $("#tblProductDetail tbody tr").each(function () {
        total += Number($(this).find(".NetPrice").text());
    });
    $("#txtTotalAmount").val(total.toFixed(2));
    if (parseFloat(total) > 0) {
        $("#txtOverallDisc").removeAttr('disabled');
        $("#txtDiscAmt").removeAttr('disabled');
    }
    else {
        $("#txtOverallDisc").attr('disabled', true);
        $("#txtDiscAmt").attr('disabled', true);
    }
    calTotalTax();
}
function calTotalTax() {
    debugger;
    var totalTax = 0.00, qty, weightoz = 0;
    var MLQty = 0.00;
    $("#tblProductDetail tbody tr").each(function () {
        qty = Number($(this).find(".ReturnQty input").val());
        if (Number($("#hfMLTaxStatus").val()) == '0') {
            MLQty = 0;
        }
        else {
            MLQty = parseFloat(MLQty) + (parseFloat($(this).find('.TaxRate span').attr('mlqty')) * qty * parseFloat($(this).find('.UnitType span').attr('qtyperunit')));
        }
        if ($(this).find('.TaxRate span').attr('istaxable') == 1) {
            var totalPrice = Number($(this).find(".UnitPrice input").val()) * qty;
            var Disc = totalPrice * Number($("#txtOverallDisc").val()) * 0.01;
            var priceAfterDisc = totalPrice - Disc;
            if ($("#ddlTaxType").val() != null) {
                totalTax += (priceAfterDisc * Number($('#ddlTaxType option:selected').attr("TaxValue")) * 0.01) || 0;
            }
        }
        if ($(this).find('.TaxRate span').attr('WeightOz') != '') {

            weightoz += (parseFloat($(this).find('.TaxRate span').attr('WeightOz')) * qty * parseFloat($(this).find('.UnitType span').attr('qtyperunit')));
            $('#txtWeightQty').val(parseFloat(weightoz).toFixed(2));
            $('#txtWeightTax').val(parseFloat(weightoz * GweighTax).toFixed(2));
        }
    });

    if ($("#ddlCreditMemoType").val() == '1') {
        $("#txtMLQty").val('0.00');
        $("#txtMLTax").val('0.00');
        $('#txtWeightQty').val('0.00');
        $("#txtWeightTax").val('0.00');
        $("#txtTotalTax").val('0.00');
    }
    else {
        $("#txtMLQty").val(MLQty.toFixed(2));
        $("#txtMLTax").val((MLQty * MLTaxRate).toFixed(2));
        $("#txtTotalTax").val(totalTax.toFixed(2));
    }
    calGrandTotal();
}

function calOverallDisc1() {

    var DiscAmt = parseFloat($("#txtDiscAmt").val()).toFixed(2) || 0.00;  //toFixed(2) was not added Modified by Rizwan Ahmad on 11/13/2019 at 23:44 PM
    var TotalAmount = parseFloat(Number($("#txtTotalAmount").val())).toFixed(2) || 0.00;//toFixed(2) was not added Modified by Rizwan Ahmad on 11/13/2019 at 23:44 PM
    var per = 0.00;
    if (parseFloat(DiscAmt) > parseFloat(TotalAmount)) {
        toastr.error('Discount Amount should be less than Total Amount.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $("#txtDiscAmt").val('0.00');
        $("#txtOverallDisc").val('0.00');
        $("#txtDiscAmt").select();
    } else {
        if (parseFloat(TotalAmount) > 0) {
            per = ((DiscAmt / TotalAmount) * 100) || 0;
            $("#txtOverallDisc").val(per.toFixed(2));
        }
        else {
            $("#txtDiscAmt").val('0.00');
            $("#txtOverallDisc").val('0.00');
            $("#txtDiscAmt").select();
        }
    }
    calTotalTax();
}


function calOverallDisc() {
    var factor = 0.00, disc = 0;
    if ($("#txtOverallDisc").val() == "") {
        $("#txtOverallDisc").val('0.00');
    }
    else if (parseFloat($("#txtOverallDisc").val()) > 100) {
        $("#txtOverallDisc").val('0.00');
        toastr.error('Discount % cannot be greater than 100.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
    else {
        disc = parseFloat($("#txtOverallDisc").val()).toFixed(2);
    }
    factor = disc * 0.01 || 0.00;

    $("#txtDiscAmt").val((Number($("#txtTotalAmount").val()) * factor).toFixed(2));//Fix problem here Discount amount was not fixed for 2 digit
    calTotalTax();
}
function calGrandTotal() {
    var DiscPercent = 0.00, totAmt = 0.00, totDiscAmt = 0.00;//Added for discount calculation from line no. 625 to 633
    if ($("#txtOverallDisc").val() != "") {
        DiscPercent = parseFloat($("#txtOverallDisc").val()).toFixed(2);
    }
    if ($("#txtTotalAmount").val() != "") {
        totAmt = parseFloat($("#txtTotalAmount").val()).toFixed(2);
        totDiscAmt = parseFloat((totAmt * DiscPercent) / 100).toFixed(2) || 0;
        parseFloat($("#txtDiscAmt").val(totDiscAmt)).toFixed(2);
    }
    var grandTotal = Number($("#txtTotalAmount").val()) - parseFloat(totDiscAmt) + Number($("#txtTotalTax").val()) + parseFloat($('#txtWeightTax').val()) + parseFloat($("#txtMLTax").val());
    var round = Math.round(grandTotal);
    $("#txtAdjustment").val((round - grandTotal).toFixed(2));
    $("#txtGrandTotal").val(round.toFixed(2));
}

function changePrice(e) {
    debugger;
    var DiscountPercent = parseFloat($("#txtOverallDisc").val());// Added because of when we change Unit Price the discount amount was not updating
    var row = $(e).closest("tr");
    $(row).find(".UnitPrice input").css("border-color", "#eee");
    var NetPrice = 0.0;
    var UnitPrice = Number(row.find(".UnitPrice > input").val());
    if (UnitPrice != 0) {
        row.find('.UnitPrice input').removeClass('border-warning');
    }
    var ReturnQty = Number(row.find(".ReturnQty input").val());
    if (ReturnQty != 0) {
        row.find(".ReturnQty input").removeClass('border-warning');
    }
    NetPrice = parseFloat(Number(ReturnQty) * parseFloat(UnitPrice));
    $(row).find('.NetPrice').text(parseFloat(NetPrice).toFixed(2));
    $(row).find('.UnitPrice input').val(parseFloat(UnitPrice).toFixed(2));
    parseFloat($("#txtDiscAmt").val((NetPrice * DiscountPercent) / 100)).toFixed(2);// Added because of when we change Unit Price the discount amount was not updating

    calTotalAmount();
}

function refresh() {
    $("#tblProductDetail tbody tr").remove();
    calTotalAmount();

}

function Reset() {
    refresh();
    $("#ddlCustomer").val('0').change();
    $("#txtRemarks").val('');
    $("#txtOverallDisc").val('0.00');
    $("#txtDiscAmt").val('0.00');
    $("#txtAdjustment").val('0.00');
    $("#txtGrandTotal").val('0.00');
    $("#ddlCustomer").attr('disabled', false);
    $("#txtWeightQty").val('0.00'); //Added line 519 to 521 for reset these three field. when we delete all list item then these field was resetting.
    $("#txtWeightTax").val('0.00');
}

$("#btnSave").click(function () {
    var flag1 = false, flag2 = false, flag3 = 0;
    if ($("#ddlCustomer").val() == '0') {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        $("#ddlCustomer").closest('div').find('.select2-selection--single').attr('style', 'border:1px solid #FF9149  !important');
        return;
    }
    else {
        $("#ddlCustomer").closest('div').find('.select2-selection--single').removeAttr('style')
    }
    if ($("#ddlCreditMemoType").val() == '0') {
        $("#ddlCreditMemoType").addClass('border-warning');
        toastr.error("All * fields are mandatory ", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        return;
    }
    if ($('#tblProductDetail tbody tr').length > 0) {
        $("#tblProductDetail tbody tr").each(function () {
            if ($(this).find(".ReturnQty input").val() == "" || $(this).find(".ReturnQty input").val() == null || Number($(this).find(".ReturnQty input").val()) == 0) {
                $(this).find(".ReturnQty input").focus().addClass("border-warning");
                flag1 = true;
            }
            if ($(this).find(".UnitPrice input").val() == "" || $(this).find(".UnitPrice input").val() == null || Number($(this).find(".UnitPrice input").val()) == 0) {
                $(this).find(".UnitPrice input").focus().addClass("border-warning");
                flag3 = 1;
            }
            //var ReturnQty = $(this).find('.FreshReturn input').val();
            //var DamageQty = $(this).find('.DemageReturn input').val();
            //var AcceptedQty = $(this).find('.ReturnQty  input').val();
            //ReturnFm = parseInt(ReturnQty) + parseInt(DamageQty);
            //if (ReturnFm != AcceptedQty) {
            //    $(this).find(".FreshReturn input").focus().addClass("border-warning");
            //    flag2 = true
            //}
        });
        if (flag3 != 0) {
            toastr.error("Unit Price can't be empty or zero.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            return;
        }
        if (flag1) {
            toastr.error("Required quantity can't be empty or zero.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            return;
        }
        if (!flag2) {
            if (!flag1) {
                var taxvalue = 0;
                var overallDisc = 0.00;
                var overallDiscAmt = 0.00;
                if ($("#ddlTaxType").val() != null) {
                    taxvalue = $('#ddlTaxType option:selected').attr("TaxValue");
                }
                if ($("#txtOverallDisc").val() != "") {
                    overallDisc = $("#txtOverallDisc").val();
                }
                if ($("#txtDiscAmt").val() != "") {
                    overallDiscAmt = parseFloat($("#txtDiscAmt").val()).toFixed(2);//Modified toFixed was not added by Rizwan Ahmad 11/13/2019 23:38 PM.
                }
                var ReferenceOrderAutoId = 0;
                $("#tblduePayment tbody tr").each(function () {
                    var row = $(this);
                    if (row.find('.Actions input').prop('checked') == true) {
                        if (row.find(".OrderNo span").attr("OrderAutoId") != '') {
                            ReferenceOrderAutoId = Number(row.find(".OrderNo span").attr("OrderAutoId")) || 0;
                        }
                    }
                })
                var orderData = {
                    CustomerAutoId: $("#ddlCustomer").val(),
                    Remarks: $("#txtRemarks").val(),
                    TotalAmount: $("#txtTotalAmount").val(),
                    OverallDisc: overallDisc,
                    OverallDiscAmt: overallDiscAmt,
                    TotalTax: $("#txtTotalTax").val(),
                    GrandTotal: $("#txtGrandTotal").val(),
                    TaxType: $("#ddlTaxType").val(),
                    TaxValue: taxvalue,
                    MLQty: parseFloat($("#txtMLQty").val()),
                    MLTax: parseFloat($("#txtMLTax").val()),
                    AdjustmentAmt: parseFloat($("#txtAdjustment").val()),
                    CreditMemoType: $("#ddlCreditMemoType").val(),
                    ReferenceOrderAutoId: ReferenceOrderAutoId
                };
                var Product = [];
                var SRP = 0;
                $("#tblProductDetail tbody tr").each(function () {
                    if ($(this).find('.SRP').text() != '') {
                        SRP = $(this).find('.SRP').text();
                    } else {
                        SRP = 0;
                    }

                    Product.push({
                        'ProductAutoId': $(this).find('.ProName').find('span').attr('ProductAutoId'),
                        'UnitAutoId': $(this).find('.UnitType').find('span').attr('UnitAutoId'),
                        'RequiredQty ': $(this).find('.ReturnQty input').val(),
                        'QtyPerUnit': $(this).find('.UnitType').find('span').attr('qtyperunit'),
                        'UnitPrice': $(this).find('.UnitPrice input').val(),
                        'SRP': SRP,
                        'Tax': $(this).find('.TaxRate').find('span').attr('istaxable'),
                        'NetPrice': $(this).find('.NetPrice').text(),
                        'OM_MinPrice': $(this).find('.OM_MinPrice').text(),
                        'OM_CostPrice': $(this).find('.OM_CostPrice').text(),
                        'OM_BasePrice': $(this).find('.OM_BasePrice').text()
                    });
                });
                $.ajax({
                    type: "Post",
                    url: "/Sales/WebAPI/WCreditMemo.asmx/insertOrderData",
                    //data: "{'TableValues':'" + JSON.stringify(Product) + "','dataValue':'" + JSON.stringify(orderData) + "'}",
                    data: JSON.stringify({ TableValues: JSON.stringify(Product), dataValue: JSON.stringify(orderData) }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $.blockUI({
                            message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                            overlayCSS: {
                                backgroundColor: '#FFF',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (data) {
                        if (data.d != "Session Expired") {
                            if (data.d == 'true') {
                                swal("", "Credit memo generated successfully.", "success").then(function () {
                                    location.reload(true);
                                });
                            } else {
                                swal("", data.d, "error");
                            }
                        } else {
                            location.href = '/';
                        }
                    },
                    error: function (result) {
                        swal("", "Oops!, Something went wrong.", "error");

                    },
                    failure: function (result) {
                        swal("", result.d, "error");
                    }
                });
            } else {
                toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            }
        }
    }
    else {
        toastr.error('No Product Added into the Table.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
})

$('.close').click(function () {
    $(this).closest('div').hide();

});


function editCredit(CreditAutoId) {
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/WCreditMemo.asmx/editCredit",
        data: "{'CreditAutoId':'" + CreditAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var CreditOrder = $(xmldoc).find("Table");
                var items = $(xmldoc).find("Table1");
                var EmpType = $(xmldoc).find("Table2");
                var Unit = $(xmldoc).find("Table4");
                var CStatus = "";
                MLTaxRate = parseFloat($(CreditOrder).find("MLTaxPer").text());
                $("#txtOrderId").val($(CreditOrder).find("CreditNo").text());
                $("#txtHCreditNo").val($(CreditOrder).find("CreditNo").text());
                if ($(CreditOrder).find('IsMLTaxApply').text() == '1') {
                    $('#btnRemoveMlTax button').html('Remove ML Tax');
                } else {
                    $('#btnRemoveMlTax button').html('Add ML Tax')
                }
                $("#hfMLTaxStatus").val($(CreditOrder).find('IsMLTaxApply').text());



                $("#txtOrderDate").val($(CreditOrder).find("CreditDate").text());
                $("#txtOrderStatus").val($(CreditOrder).find("STATUS").text());
                CStatus = $(CreditOrder).find("STATUS").text();
                $("#txtRemarks").val($(CreditOrder).find("Remarks").text()).attr('disabled', true)
                $("#ddlCustomer").val($(CreditOrder).find("CustomerAutoId").text()).change();

                $("#txtManagerRemarks").val($(CreditOrder).find("ManagerRemark").text());
                $("#CreditType").val($(CreditOrder).find("CreditType").text());//CreditType
                $("#ddlCustomer").attr('disabled', true);

                //$("#ddlCreditMemoType").attr('disabled', true);
                $("#hiddenOrderStatus").val($(CreditOrder).find("StatusCode").text());
                $("#MLTaxRemark").val($(CreditOrder).find("MLTaxRemark").text());
                var UnitDetails = [];

                $.each(Unit, function () {
                    UnitDetails.push({
                        AutoId: $(this).find("UnitAutoId").text(),
                        UnitType: $(this).find("UnitType").text(),
                        ProductId: $(this).find("ProductId").text(),
                        Qty: $(this).find("Qty").text(),
                    });
                });
                localStorage.setItem("UnitItems", JSON.stringify(UnitDetails))
                var row = $("#tblProductDetail thead tr").clone(true);
                var FreshReturnUnitAutoId = 0;
                var DamageReturnUnitAutoId = 0;
                var MissingItemUnitAutoId = 0;
                $.each(items, function () {
                    if ($(this).find("QtyPerUnit_Fresh").text() != '') {
                        FreshReturnUnitAutoId = $(this).find("QtyPerUnit_Fresh").text();
                    } else {
                        FreshReturnUnitAutoId = $(this).find("UnitAutoId").text();
                    }
                    if ($(this).find("QtyPerUnit_Damage").text() != '') {
                        DamageReturnUnitAutoId = $(this).find("QtyPerUnit_Damage").text();
                    } else {
                        DamageReturnUnitAutoId = $(this).find("UnitAutoId").text();
                    }
                    // sachin changess start
                    if ($(this).find("QtyPerUnit_Missing").text() != '') {
                        MissingItemUnitAutoId = $(this).find("QtyPerUnit_Missing").text();
                    } else {
                        MissingItemUnitAutoId = $(this).find("UnitAutoId").text();
                    }
                    $(".ProId", row).text($(this).find("ProductId").text());
                    if ($(this).find("TaxRate").text() == 1) {
                        $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span>" + " <product class='badge badge badge-pill badge-danger'>Taxable</product>");
                    }
                    else {
                        $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span>");
                    }
                    $(".UnitType", row).html("<span UnitAutoId='" + $(this).find("UnitAutoId").text() + "' QtyPerUnit='" + $(this).find("QtyPerUnit").text() + "'>" + $(this).find("UnitType").text() + " (" + $(this).find("QtyPerUnit").text() + " pcs" + ")</span>");
                    $(".AcceptedQty", row).html("<span MAXQty='" + $(this).find("MAXQty ").text() + "'></span><input disabled='disabled' type='text' class='form-control input-sm text-center' onkeyup='getcalCulation1(this)' onkeypress='return isNumberKey(event)' style='width:100px;' value='" + $(this).find("AcceptedQty").text() + "' />");
                    if ($(EmpType).find('EmpType').text() != 8) {
                        if ($(CreditOrder).find("StatusCode").text() == 1) {
                            $(".ReturnQty", row).html("<span MAXQty='" + $(this).find("MAXQty ").text() + "'></span><input type='text'  maxlength='5' class='form-control input-sm border-primary text-center' onkeyup='getcalCulation(this)' onkeypress='return isNumberKey(event)' style='width:100px;' value='" + $(this).find("RequiredQty").text() + "' />");
                        } else {
                            $(".ReturnQty", row).html($(this).find("RequiredQty").text());
                        }
                    } else {

                        $(".ReturnQty", row).html($(this).find("RequiredQty").text());
                    }

                    if ($(CreditOrder).find("StatusCode").text() == 1) {
                        if ($(EmpType).find('EmpType').text() == 8) {
                            $(".UnitPrice", row).html("<input type='text' class='form-control input-sm border-primary' maxlength='10'  onkeyup='changePrice1(this)' style='width:100px;text-align:right;border:1px solid #3b4781;'  onkeypress='return isNumberDecimalKey(event,this)' value='" + $(this).find("ManagerUnitPrice").text() + "' />");

                        } else {
                            $(".UnitPrice", row).html("<input type='text' class='form-control input-sm border-primary' maxlength='10' onkeyup='changePrice(this)' style='width:100px;text-align:right;border:1px solid #3b4781;'  onkeypress='return isNumberDecimalKey(event,this)' value='" + $(this).find("ManagerUnitPrice").text() + "' />");
                        }
                    } else {
                        $(".UnitPrice", row).html($(this).find("UnitPrice").text());
                    }
                    $(".SRP", row).text($(this).find("SRP").text());

                    $(".TaxRate", row).html('<span WeightOz="' + $(this).find("WeightOz").text() + '" mlqty="' + $(this).find("UnitMLQty").text() + '" istaxable=' + $(this).find("TaxRate").text() + '>' + (($(this).find("TaxRate").text() == 1) ? '<tax class="la la-check-circle success"></span>' : '') + '</tax>');
                    $(".NetPrice", row).text($(this).find("NetAmount").text());

                    if ($(this).find("QtyReturn_Fresh").text() != "") {
                        $(".FreshReturn", row).html("<div class='input-group'><div class='input-group-prepend'><input type='text'  disabled='disabled'  class='form-control input-sm border-primary text-center' style='width:50px;padding:0.25rem 0.25rem;margin-right: 2px;' value='" + $(this).find("QtyReturn_Fresh").text() + "' onkeypress='return isNumberKey(event)'/><select style='width:58px;padding:0' class='freshunit form-control input-sm border-primary'  disabled='disabled' ></select></div></div>");
                    } else {
                        $(".FreshReturn", row).html("<div class='input-group'><div class='input-group-prepend'><input type='text'  disabled='disabled'  class='form-control input-sm border-primary  text-center' style='width:50px;padding:0.25rem 0.25rem;margin-right: 2px;' value='0' onkeypress='return isNumberKey(event)'/><select style='width:58px;padding:0' class='freshunit form-control input-sm border-primary'  disabled='disabled'></select></div></div>");
                    }
                    if ($(this).find("QtyReturn_Damage").text() != "") {
                        $(".DemageReturn", row).html("<div class='input-group-prepend'><input type='text'  disabled='disabled'  class='form-control input-sm border-primary onkeyup='calculateReturnQty(this)' text-center' style='width:50px;padding:0.25rem 0.25rem;margin-right: 2px;' value='" + $(this).find("QtyReturn_Damage").text() + "'  onkeypress='return isNumberKey(event)'/><select style='width:58px;padding:0' class='demageunit form-control input-sm border-primary'   disabled='disabled' ></select></div>");
                    } else {
                        $(".DemageReturn", row).html("<div class='input-group-prepend'><input type='text'  disabled='disabled'  class='form-control input-sm border-primary onkeyup='calculateReturnQty(this)' text-center' style='width:50px;padding:0.25rem 0.25rem;margin-right: 2px;' value='0'  onkeypress='return isNumberKey(event)'/><select style='width:58px;padding:0' class='demageunit form-control input-sm border-primary'   disabled='disabled'></select></div>");
                    }

                    if ($(this).find("QtyReturn_Missing").text() != "") {
                        $(".MissingItem", row).html("<div class='input-group-prepend'><input type='text'  disabled='disabled'  class='form-control input-sm border-primary onkeyup='calculateReturnQty(this)' text-center' style='width:50px;padding:0.25rem 0.25rem;margin-right: 2px;' value='" + $(this).find("QtyReturn_Missing").text() + "'  onkeypress='return isNumberKey(event)'/><select style='width:58px;padding:0' class='missingunit form-control input-sm border-primary'   disabled='disabled'></select></div>");
                    } else {
                        $(".MissingItem", row).html("<div class='input-group-prepend'><input type='text'  disabled='disabled'   class='form-control input-sm border-primary onkeyup='calculateReturnQty(this)' text-center' style='width:50px;padding:0.25rem 0.25rem;margin-right: 2px;' value='0'  onkeypress='return isNumberKey(event)'/><select style='width:58px;padding:0' class='missingunit form-control input-sm border-primary'   disabled='disabled'></select></div>");
                    }
                    $(".OM_MinPrice", row).text($(this).find("OM_MinPrice").text());
                    $(".OM_CostPrice", row).text($(this).find("OM_CostPrice").text());
                    $(".OM_BasePrice", row).text($(this).find("OM_BasePrice").text());
                    $(".Action", row).html("<a href='javascript:;' id='deleterow' onclick='deleterows(this)'><span class='ft-x'></span></a>");
                    $('#tblProductDetail tbody').append(row);
                    if (CStatus == "Approved") {
                        $(".FreshReturn").show();
                        $(".DemageReturn").show();
                    }
                    else {

                        if ($("#hEmpTypeNo").val() != "10") {
                            $(".FreshReturn").hide();
                            $(".DemageReturn").hide();
                        }

                    }

                    var getUnit = JSON.parse(localStorage.getItem('UnitItems'));
                    $.grep(getUnit, function (e) {
                        if (e.ProductId == $(row).find(".ProId").text()) {
                            if ($(this).find("FreshReturnUnitAutoId").text() != 0) {
                                if ($(row).find(".UnitType span").attr('UnitAutoId') == e.AutoId) {

                                    row.find('.freshunit').append($("<option value=" + e.AutoId + " Qty=" + e.Qty + " selected='selected'>" + e.UnitType + "</option>"));
                                    row.find('.demageunit').append($("<option value=" + e.AutoId + " Qty=" + e.Qty + " selected='selected'>" + e.UnitType + "</option>"));
                                    // sachin changess start
                                    row.find('.missingunit').append($("<option value=" + e.AutoId + " Qty=" + e.Qty + " selected='selected'>" + e.UnitType + "</option>"));
                                    // sachin changess end
                                }
                                else {
                                    row.find('.freshunit').append($("<option value=" + e.AutoId + " Qty=" + e.Qty + ">" + e.UnitType + "</option>"));
                                    row.find('.demageunit').append($("<option value=" + e.AutoId + " Qty=" + e.Qty + ">" + e.UnitType + "</option>"));
                                    // sachin changess start
                                    row.find('.missingunit').append($("<option value=" + e.AutoId + " Qty=" + e.Qty + ">" + e.UnitType + "</option>"));
                                    // sachin changess end
                                }
                            } else {
                                if (FreshReturnUnitAutoId == e.AutoId) {

                                    row.find('.freshunit').append($("<option value=" + e.AutoId + " Qty=" + e.Qty + " selected='selected'>" + e.UnitType + "</option>"));

                                }
                                else {
                                    row.find('.freshunit').append($("<option value=" + e.AutoId + " Qty=" + e.Qty + ">" + e.UnitType + "</option>"));


                                }
                                if (DamageReturnUnitAutoId == e.AutoId) {


                                    row.find('.demageunit').append($("<option value=" + e.AutoId + " Qty=" + e.Qty + " selected='selected'>" + e.UnitType + "</option>"));
                                }
                                else {

                                    row.find('.demageunit').append($("<option value=" + e.AutoId + " Qty=" + e.Qty + ">" + e.UnitType + "</option>"));
                                }
                                // sachin changess start
                                if (MissingItemUnitAutoId == e.AutoId) {
                                    row.find('.missingunit').append($("<option value=" + e.AutoId + " Qty=" + e.Qty + " selected='selected'>" + e.UnitType + "</option>"));
                                }
                                else {
                                    row.find('.missingunit').append($("<option value=" + e.AutoId + " Qty=" + e.Qty + ">" + e.UnitType + "</option>"));
                                }
                                // sachin changess end
                            }
                        }
                    });
                    row = $("#tblProductDetail tbody tr:last").clone(true);

                });
                $("#ddlCreditMemoType").val($(CreditOrder).find("CreditMemoType").text()).change();
                if ($(EmpType).find('EmpType').text() == 8) {
                    $(".AcceptedQty").show();
                }
                if ($(CreditOrder).find("StatusCode").text() == 6 && $(CreditOrder).find("referenceorderno").text() == '') {

                }
                else {
                    if ($(CreditOrder).find('CreditMemo_CheckMLTax').text() == '1' && $(EmpType).find('EmpType').text() == 1 && $(CreditOrder).find("OrderAutoId").text() == '') {
                        $("#btnRemoveMlTax").show();
                    } else {
                        $("#btnRemoveMlTax").hide();
                    }
                }
                if (($(CreditOrder).find("StatusCode").text() == 1 || $(CreditOrder).find("StatusCode").text() == 3) && $(CreditOrder).find("OrderAutoId").text().trim() == '' || $(CreditOrder).find("OrderAutoId").text() == '0' || $(CreditOrder).find("OrderAutoId").text() == null) {
                    $('#btnCancelCreditMemo').show();
                } else {
                    $('#btnCancelCreditMemo').hide();
                }

                $("#btnPrintOrder").show();
                $("#txtTotalAmount").val($(CreditOrder).find("GrandTotal").text());
                $("#txtOverallDisc").val($(CreditOrder).find("OverallDisc").text());
                $("#txtDiscAmt").val($(CreditOrder).find("OverallDiscAmt").text());

                $("#txtGrandTotal").val($(CreditOrder).find("TotalAmount").text());
                if ($("#ddlCreditMemoType").val() == '1') {
                    $("#txtMLQty").val('0.00');
                    $("#txtMLTax").val('0.00');
                    $("#txtWeightQty").val('0.00');
                    $("#txtWeightTax").val('0.00');
                    $("#txtTotalTax").val('0.00');
                }
                else {
                    $("#txtMLQty").val($(CreditOrder).find("MLQty").text());
                    $("#txtMLTax").val($(CreditOrder).find("MLTax").text());
                    $("#txtWeightQty").val($(CreditOrder).find("WeightTotalQuantity").text());
                    $("#txtWeightTax").val($(CreditOrder).find("WeightTaxAmount").text());
                    $("#txtTotalTax").val($(CreditOrder).find("TotalTax").text());
                }
                $("#txtAdjustment").val($(CreditOrder).find("AdjustmentAmt").text());
                ReferenceOrderAutoId = $(CreditOrder).find("ReferenceOrderAutoId").text();

                if ($(EmpType).find('EmpType').text() != 6) {
                    if ($(EmpType).find('EmpType').text() == 2 || $(EmpType).find('EmpType').text() == 1) {
                        if ($(CreditOrder).find("StatusCode").text() == 1) {
                            $("#btnUpdate").show();
                            $(".Action").show();
                            $('#btnAdd').closest('#Productpannel').show();
                            $("#txtDiscAmt").attr('disabled', false);
                            $("#txtRemarks").attr('disabled', false);
                            $("#txtOverallDisc").attr('disabled', false);
                            $("#ddlTaxType").attr('disabled', false);
                        } else {
                            $("#btnUpdate").hide();
                            $(".AcceptedQty").show();
                            $(".Action").hide();
                            $('#btnAdd').closest('#Productpannel').hide();
                            $("#txtDiscAmt").attr('disabled', true);
                            $("#txtOverallDisc").attr('disabled', true);
                            $("#ddlTaxType").attr('disabled', true);
                            $("#txtRemarks").attr('disabled', true);
                        }
                    } else {
                        if ($(CreditOrder).find("StatusCode").text() == 1) {
                            $("#btnUpdate").hide();
                            $("#btnBackOrder").show();
                            $('.ReturnQty input').attr('disabled', true);
                            $('.UnitPrice input').attr('disabled', true);
                            $("#btnApproved").show();
                            $("#btnCancel").show();
                            $(".Action").hide();
                            $('#btnAdd').closest('#Productpannel').hide();
                        } else {

                            if ($(EmpType).find('EmpType').text() == 6 && $(CreditOrder).find("StatusCode").text() == 2) {
                                $("#btnComplete").show();
                            } else {
                                $("#btnComplete").hide();
                            }

                            $("#btnUpdate").hide();
                            $("#btnApproved").hide();
                            $("#btnCancel").hide();
                            $(".Action").hide();
                            $("#Productpannel").hide();
                            $('#btnAdd').closest('#Productpannel').hide();

                        }
                        //if ($(EmpType).find('EmpType').text() == 10) {
                        //    $('#btnSave').closest('.row').hide();
                        //}
                    }



                } else {
                    $("#btnUpdate").hide();
                    $("#btnApproved").hide();
                    $("#btnCancel").hide();
                    $(".Action").hide();
                    $(".AcceptedQty").show();
                    $(".ReturnQty input").attr('disabled', true);
                    $(".AcceptedQty input").attr('disabled', true);
                    $(".UnitPrice input").attr('disabled', true);
                    $('#btnAdd').closest('#Productpannel').hide();
                    $("#btnComplete").hide();
                }

                if ($(EmpType).find('EmpType').text() == 8) {
                    if ($(CreditOrder).find("StatusCode").text() == 1) {
                        $("#hideRemark").show();
                    }
                    else {
                        $("#hideRemark").show()
                        $("#txtManagerRemarks").attr('disabled', true);
                    }
                }
                if ($(EmpType).find('EmpType').text() == 8) {
                    if ($(CreditOrder).find("StatusCode").text() == 1) {
                        $("#txtDiscAmt").attr('disabled', false);
                        $("#txtOverallDisc").attr('disabled', false);
                        $("#ddlTaxType").attr('disabled', false);
                    }
                    else {
                        $("#txtDiscAmt").attr('disabled', true);
                        $("#txtOverallDisc").attr('disabled', true);
                        $("#ddlTaxType").attr('disabled', true);
                    }
                }
                else {
                    if ($(CreditOrder).find("StatusCode").text() == 3) {
                        if ($(CreditOrder).find("ManagerRemark").text() == "") {
                            $("#hideRemark").hide();
                        }
                        else {
                            $("#hideRemark").show()
                            $("#txtManagerRemarks").attr('disabled', true);
                        }
                    }
                    else if ($(CreditOrder).find("StatusCode").text() == 5) {
                        if ($(CreditOrder).find("ManagerRemark").text() == "") {
                            $("#hideRemark").hide();
                        }
                        else {
                            $("#hideRemark").show();
                            $("#txtManagerRemarks").attr('disabled', true);
                        }
                    }
                    else {
                        $("#hideRemark").hide();
                    }

                }

                if ($(CreditOrder).find("StatusCode").text() == '3' || $(CreditOrder).find("StatusCode").text() == '6') {
                    $('input[type="text"]').attr('readonly');
                    $('select').attr('disabled');
                    $("#ddlCreditMemoType").attr('disabled', true);
                }

                $("#tblduePayment tbody tr").each(function (j, item) {
                    if ($(item).find(".OrderNo span").attr('OrderAutoId') == ReferenceOrderAutoId) {
                        $(item).find(".Actions input").prop('checked', true);
                    }
                    if ($(CreditOrder).find("StatusCode").text() == '3' || $(CreditOrder).find("StatusCode").text() == '6') {
                        $(item).find(".Actions input").attr('disabled', true);
                    }
                });
                blockCopyPaste();
            } else {
                location.href = '/';
            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
$("#btnUpdate").click(function () {
    var flag1 = false, flag2 = 0;
    if ($('#tblProductDetail tbody tr').length > 0) {
        $("#tblProductDetail tbody tr").each(function () {
            if ($(this).find(".ReturnQty input").val() == "" || $(this).find(".ReturnQty input").val() == null || Number($(this).find(".ReturnQty input").val()) == 0) {
                $(this).find(".ReturnQty input").focus().addClass("border-warning");
                flag1 = true;
            }
            if ($(this).find(".UnitPrice input").val() == "" || $(this).find(".UnitPrice input").val() == null || Number($(this).find(".UnitPrice input").val()) == 0) {
                $(this).find(".UnitPrice input").focus().addClass("border-warning");
                flag2 = 1;
            }
        });
        if (flag1) {
            toastr.error("Required quantity can't be zero or empty", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            return;
        }
        if (flag2 > 0) {
            toastr.error("Unit Price can't be zero or empty", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            return;
        }
        if ($("#ddlCreditMemoType").val() == '0') {
            $("#ddlCreditMemoType").addClass('border-warning');
            toastr.error("All * fields are mandatory ", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            return;
        }
        if (!flag1) {
            $("#ddlCreditMemoType").removeClass('border-warning');
            var Product = [];
            var SRP = 0;
            var taxvalue = 0;
            var overallDisc = 0.00;
            var overallDiscAmt = 0.00;
            if ($("#ddlTaxType").val() != null) {
                taxvalue = $('#ddlTaxType option:selected').attr("TaxValue");
            }
            if ($("#txtOverallDisc").val() != "" && $("#txtOverallDisc").val() != null) {
                overallDisc = $("#txtOverallDisc").val();
            }
            if ($("#txtDiscAmt").val() != "") {
                overallDiscAmt = parseFloat($("#txtDiscAmt").val()).toFixed(2);//Modified toFixed() was not added by Rizwan Ahmad 11/13/2019 23:38 PM.
            }
            $("#tblProductDetail tbody tr").each(function () {
                if ($(this).find('.SRP').text() != '') {
                    SRP = $(this).find('.SRP').text();
                } else {
                    SRP: 0;
                }
                Product.push({
                    'ProductAutoId': $(this).find('.ProName').find('span').attr('ProductAutoId'),
                    'UnitAutoId': $(this).find('.UnitType').find('span').attr('UnitAutoId'),
                    'RequiredQty ': $(this).find('.ReturnQty input').val(),
                    'QtyPerUnit': $(this).find('.UnitType').find('span').attr('qtyperunit'),
                    'UnitPrice': $(this).find('.UnitPrice input').val(),
                    'SRP': SRP,
                    'Tax': $(this).find('.TaxRate').find('span').attr('istaxable'),
                    'NetPrice': $(this).find('.NetPrice').text(),
                    'OM_MinPrice': $(this).find('.OM_MinPrice').text(),
                    'OM_CostPrice': $(this).find('.OM_CostPrice').text(),
                    'OM_BasePrice': $(this).find('.OM_BasePrice').text()
                });
            });
            var ReferenceOrderAutoId = 0;
            $("#tblduePayment tbody tr").each(function () {
                var row = $(this);
                if (row.find('.Actions input').prop('checked') == true) {
                    if (row.find(".OrderNo span").attr("OrderAutoId") != '') {
                        ReferenceOrderAutoId = row.find(".OrderNo span").attr("OrderAutoId");
                    }
                }
            })
            var orderData = {
                CustomerAutoId: $("#ddlCustomer").val(),
                Remarks: $("#txtRemarks").val(),
                TotalAmount: $("#txtTotalAmount").val(),
                OverallDisc: overallDisc,
                OverallDiscAmt: overallDiscAmt,
                TotalTax: $("#txtTotalTax").val(),
                GrandTotal: $("#txtGrandTotal").val(),
                TaxType: $("#ddlTaxType").val(),
                TaxValue: taxvalue,
                MLQty: parseFloat($("#txtMLQty").val()),
                MLTax: parseFloat($("#txtMLTax").val()),
                AdjustmentAmt: parseFloat($("#txtAdjustment").val()),
                CreditMemoType: $("#ddlCreditMemoType").val(),
                ReferenceOrderAutoId: ReferenceOrderAutoId,
                CreditAutoId: $("#CreditAutoId").val(),
            };
            $.ajax({
                type: "Post",
                url: "/Sales/WebAPI/WCreditMemo.asmx/updateOrderData",
                data: JSON.stringify({ TableValues: JSON.stringify(Product), dataValue: JSON.stringify(orderData) }),
                //data: "{'TableValues':'" + JSON.stringify(Product) + "','dataValue':'" + JSON.stringify(orderData) + "','CreditAutoId':'" + $("#CreditAutoId").val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },

                success: function (data) {
                    if (data.d != "Session Expired") {
                        swal("", "Credit Memo updated successfully.", "success");
                    } else {
                        location.href = '/';
                    }
                },
                error: function (result) {
                    swal("", "Oops!, Something went wrong.", "error");
                },
                failure: function (result) {
                    swal("", result.d, "error");
                }
            });
        } else {
            toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        }
    }
    else {
        swal("", "No product added into the table.", "error");
    }

})
function ApprovedOrders(Status) {

    var confirmtest = '';
    var message = '';
    if (Status == 3) {
        message = 'Approved successfully';

    } else {
        message = 'Credit cancel successfully'

    }

    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/WCreditMemo.asmx/ApprovedCredit",
        data: "{'CreditAutoId':" + $('#CreditAutoId').val() + ",Status:'" + Status + "',ManagerRemark:'" + $("#txtManagerRemarks").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },

        success: function (response) {
            swal("", message, "success");
            $("#btnBackOrder").hide();
            editCredit($('#CreditAutoId').val());

        },
        failure: function (result) {
            swal("", result.d, "error");
        },
        error: function (result) {

            swal("", result.d, "error");

        }
    });
}

function ApprovedOrder() {
    swal({
        title: "Are you sure?",
        text: "You want to approved this credit memo.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, Cancel.",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, Approved it.",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            ApprovedOrders(3);

        } else {
            //  swal("Success", "Thanks You:)", "success");
        }
    })
}

function Cancelcreditmemo() {
    if (reqdremarkCreditmemoRequiredField()) {

        swal({
            title: "Are you sure?",
            text: "You want to decline this credit memo.",
            icon: "warning",
            showCancelButton: true,
            buttons: {
                cancel: {
                    text: "No, Cancel.",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: true,
                },
                confirm:
                {
                    text: "Yes, Decline it.",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
                }
            }
        }).then(function (isConfirm) {
            if (isConfirm) {
                ApprovedOrders(5);
            }
            else {
                //  swal("", "Thanks You:)", "error");

            }
        })
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
}
function CompleteOrders() {
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/WCreditMemo.asmx/CompleteOrder",
        data: "{'CreditAutoId':" + $('#CreditAutoId').val() + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },

        success: function (response) {
            swal("", "Credit Memo has been completed successfully.", "success");
            editCredit($('#CreditAutoId').val());
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function CompleteOrder() {
    swal({
        title: "Are you sure?",
        text: "You want to Complete? This Credit Memo.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, Cancel.",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm:
            {
                text: "Yes, Complete it.",
                value: true,
                visible: true,
                className: "",
                closeModal: true
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            CompleteOrders();
        }
        else {
            // swal("", "Thanks You:)", "error");
        }
    })
}
function getcalCulation1(e) {

    var tr = $(e).closest('tr');
    var maxReturnQty = 0.00;
    maxReturnQty = tr.find('.ReturnQty').text()
    var AcceptedQty = 0;
    if (tr.find('.AcceptedQty input').val() != '') {
        AcceptedQty = tr.find('.AcceptedQty input').val();
    }
    if (Number(AcceptedQty) > Number(maxReturnQty)) {
        $(e).val(maxReturnQty);
    }
    changePrice1(e);
}
$("#btnBackOrder").click(function () {
    $("#btnUpdate1").show();
    $("#hideRemark").show();
    $(this).hide();
    $("#btnApproved").hide();
    $("#btnCancel").hide();
    $("#btnBackupdate").show();
    $("#ddlTaxType").attr('disabled', true);
    $(".AcceptedQty input").attr('disabled', false);
    $(".UnitPrice input").attr('disabled', false);
});
$("#btnBackupdate").click(function () {
    $("#btnUpdate1").hide();
    $("#hideRemark").show();
    $(this).hide();
    $("#btnApproved").show();
    $("#btnCancel").show();
    $("#btnBackOrder").show();
    $(".AcceptedQty input").attr('disabled', true);
    $(".UnitPrice input").attr('disabled', true);
});
function changePrice1(e) {
    debugger
    var tr = $(e).closest('tr');
    var AcceptedQty = 0.00;
    var UnitPrice = 0.00;
    if (tr.find('.AcceptedQty input').val() != '') {
        AcceptedQty = parseFloat(tr.find('.AcceptedQty input').val());
    }
    if (tr.find('.UnitPrice input').val() != '') {
        UnitPrice = parseFloat(tr.find('.UnitPrice input').val());
        tr.find('.UnitPrice input').removeClass('border-warning');
    }
    var NetAmount = parseFloat(AcceptedQty) * parseFloat(UnitPrice);
    tr.find('.NetPrice').text(NetAmount.toFixed(2));
    calTotalAmount1();
}

function calTotalAmount1() {
    var total = 0.00;
    $("#tblProductDetail tbody tr").each(function () {
        total += Number($(this).find(".NetPrice").text());
    });
    $("#txtTotalAmount").val(total.toFixed(2));
    calTotalTax1();
}
function calTotalTax1() {
    debugger;
    var totalTax = 0.00, qty;
    var MLQty = 0.00;
    $("#tblProductDetail tbody tr").each(function () {
        qty = Number($(this).find(".AcceptedQty input").val()) || 0;
        MLQty = parseFloat(MLQty) + (parseFloat($(this).find('.TaxRate span').attr('MLQty')) * qty * parseFloat($(this).find('.UnitType span').attr('qtyperunit')));

        if ($(this).find('.TaxRate span').attr('istaxable') == 1) {
            var totalPrice = Number($(this).find(".UnitPrice input").val()) * qty;
            var Disc = totalPrice * Number($("#txtOverallDisc").val()) * 0.01;
            var priceAfterDisc = totalPrice - Disc;
            totalTax += priceAfterDisc * Number($('#ddlTaxType option:selected').attr("TaxValue")) * 0.01;
        }
    });
    $("#txtTotalTax").val(totalTax.toFixed(2));

    if ($("#ddlCreditMemoType").val() == '1') {
        $("#txtMLQty").val('0.00');
        $("#txtMLTax").val('0.00');
        $("#txtWeightQty").val('0.00');
        $("#txtWeightTax").val('0.00');
    }
    else {
        $("#txtMLQty").val(MLQty.toFixed(2));
        $("#txtMLTax").val((MLQty * MLTaxRate).toFixed(2));
    }
    //if (Number($("#hfMLTaxStatus").val()) == '0') {
    //    $("#txtMLTax").val('0.00');
    //}
    calGrandTotal();
}
$("#btnUpdate1").click(function () {
    var Product = [];
    $("#tblProductDetail tbody tr").each(function () {

        Product.push({
            'ProductAutoId': $(this).find('.ProName').find('span').attr('ProductAutoId'),
            'UnitAutoId': $(this).find('.UnitType').find('span').attr('UnitAutoId'),
            'RequiredQty ': $(this).find('.AcceptedQty input').val(),
            'QtyPerUnit': $(this).find('.UnitType').find('span').attr('qtyperunit'),
            'UnitPrice': $(this).find('.UnitPrice input').val(),
            'SRP': $(this).find('.SRP').text() || 0,
            'Tax': $(this).find('.TaxRate span').attr('istaxable'),
            'NetPrice': $(this).find('.NetPrice').text()
        });
    });
    $.ajax({
        type: "Post",
        url: "/Sales/WebAPI/WCreditMemo.asmx/updateOrderData1",
        data: "{'TableValues':'" + JSON.stringify(Product) + "','CreditAutoId':'" + $("#CreditAutoId").val() + "','ManagerRemark':'" + $("#txtManagerRemarks").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },

        success: function (data) {
            if (data.d != "Session Expired") {
                swal("", "Details has been updated successfully.", "success");
            } else {
                location.href = '/';
            }
        },
        error: function (result) {
            swal("", "Oops! Something went wrong. Please try later.", "error");

        },
        failure: function (result) {
            swal("", result.d, "error");
        }
    });

});

function print_NewOrder() {
    window.open("/manager/CreditMemoPrint.html?PrintAutoId=" + $("#CreditAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
}

function CustCreditmemoRequiredField() {
    var boolcheck = true;
    $('.req').each(function () {
        if ($(this).val() == '') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });

    $('.ddlsreq').each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == 'undefined' || $(this).val() == 'Select') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });
    $('.ddlreq').each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == 'undefined' || $(this).val() == 'Select') {
            boolcheck = false;
            $(this).closest('div').find('.select2-selection--single').attr('style', 'border:1px solid #FF9149  !important');
        } else {

            $(this).closest('div').find('.select2-selection--single').removeAttr('style');
        }
    });
    return boolcheck;
}

function reqdremarkCreditmemoRequiredField() {
    var boolcheck = true;
    $('.reqdremark').each(function () {
        if ($(this).val() == '') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });
    return boolcheck;
}
function UnitDetails() { // New function Added by Rizwan Ahmad on 12/15/2019
    if ($("#ddlUnitType option:selected").attr('eligibleforfree') == 1) {
        $('#chkFreeItem').attr('disabled', false);
    } else {
        $('#chkFreeItem').attr('disabled', true);
    }
    $('#chkFreeItem').prop('checked', false);
    var data = {
        ProductAutoId: $("#ddlProduct").val(),
        UnitAutoId: $("#ddlUnitType").val(),
        CustAutoId: $("#ddlCustomer").val()
    };

    if (data.UnitAutoId != 0) {
        $.ajax({
            type: "POST",
            url: "/Sales/WebAPI/WCreditMemo.asmx/selectQtyPrice",
            data: "{'dataValues':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d != "Session Expired") {
                    var xmldoc = $.parseXML(response.d);
                    var stockAndPrice = $(xmldoc).find("Table");
                    price = parseFloat(stockAndPrice.find("Price").text()).toFixed(2);
                    minPrice = stockAndPrice.find("MinPrice").text();
                    CustomPrice = stockAndPrice.find("CustomPrice").text();
                    taxRate = stockAndPrice.find("TaxRate").text();
                    SRP = stockAndPrice.find("SRP").text();
                    GP = stockAndPrice.find("GP").text();
                    if ($("#ddlUnitType").val() != 0) {
                        $("#alertStockQty").text('');
                        if (Number($("#ddlUnitType").val()) == 1) {
                            $("#alertStockQty").append(" [ Stock : " + $(stockAndPrice).find("Stock").text() + " case ] ," +
                                " [ Base Price : $" + price + " ]");
                        } else if (Number($("#ddlUnitType").val()) == 2) {
                            $("#alertStockQty").append(" [ Stock : " + $(stockAndPrice).find("Stock").text() + " box ] ," +
                                " [ Base Price : $" + price + " ]");
                        } else {
                            $("#alertStockQty").append(" [ Stock : " + $(stockAndPrice).find("Stock").text() + " pcs ] ," +
                                " [ Base Price : $" + price + " ]");
                        }
                        $("#alertStockQty").show();
                    } else {
                        $("#alertStockQty").hide();
                    }
                } else {
                    location.href = '/';
                }

            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
};



// remove ml tax code start
function RemoveMlTax() {
    $('#SecurityEnabled').modal('show');
}

var i = 0;
function clickonSecurity() {
    if (checksecRequiredField()) {
        i = 0;
        $.ajax({
            type: "Post",
            url: "/Sales/CreditMemo.aspx/clickonSecurity",
            data: "{'CheckSecurity':" + $('#txtSecurity').val() + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            async: false,
            success: function (response) {
                console.log(response);
                if (response.d != "Session Expired") {
                    if (response.d == 'true') {
                        $('#SecurityEnabled').modal('hide');
                        $('#modalMLTaxConfirmation').modal('show');
                    } else {
                        toastr.error('Access Denied.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
                    }
                } else {
                    location.href = '/';
                }
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
    else {
        toastr.error('Security Required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
}
function ConfirmMLTax() {
    var Status = Number($("#hfMLTaxStatus").val());
    var text = "", btnText = "";
    if (Status == 1) {
        text = "You want to remove ML Tax.";
        btnText = "Yes, Remove It.";
    }
    else {
        text = "You want to add ML Tax.";
        btnText = "Yes, Add It.";
    }
    if (checksecRequiredFieldML()) {
        swal({
            title: "Are you sure?",
            text: text,
            icon: "warning",
            showCancelButton: true,
            buttons: {
                cancel: {
                    text: "No, Cancel.",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: true,
                },
                confirm: {
                    text: btnText,
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
                }
            }
        }).then(function (isConfirm) {
            if (isConfirm) {
                $("#modalMLTaxConfirmation").modal('hide');
                ConfirmSecurity();
            } else {
                closeModal: true
            }
        })
    }
    else {
        toastr.error('Security Required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
}
function ConfirmSecurity() {
    i = 0;
    $.ajax({
        type: "Post",
        url: "/Sales/CreditMemo.aspx/ConfirmSecurity",
        data: "{'CheckSecurity':'" + $("#txtSecurity").val() + "','CreditNo':'" + $("#txtHCreditNo").val() + "','MLTaxRemark':'" + $("#txtMLRemark").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,

        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d == 'true') {
                    $('#btnpkd').removeAttr('disabled');
                    if (Number($("#hfMLTaxStatus").val()) == 1) {
                        swal("", "ML Tax removed successfully.", "success"
                        ).then(function () {
                            location.href = '/Sales/CreditMemo.aspx?PageId=' + $("#CreditAutoId").val();
                        });
                    }
                    else {
                        swal("", "ML Tax added successfully.", "success"
                        ).then(function () {
                            location.href = '/Sales/CreditMemo.aspx?PageId=' + $("#CreditAutoId").val();
                        });
                    }
                    i = 1;
                } else {
                    toastr.error('Access Denied.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
                }
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function ClosePop() {
    if (i == 1) {
        editCredit($("#CreditAutoId").val());
    }
    $('#SecurityEnvalid').modal('hide');
}
function checksecRequiredField() {
    var boolcheck = true;
    $('.reqseq').each(function () {
        if ($(this).val() == '') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });

    return boolcheck;
}
function checksecRequiredFieldML() {
    var boolcheck = true;
    $('.reqML').each(function () {
        if ($(this).val() == '') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });

    return boolcheck;
}
var check = false;
function SecurityEnabledVoid() {

    if (check == false) {
        $("#SecurityEnabledVoid").modal('show');
    } else {
        $("#ModelCancelRemark").modal('show');
    }
}
function clickonSecurityVoid() {
    if ($("#txtSecurityVoid").val() == "") {
        $("#txtSecurityVoid").addClass('border-warning');
        toastr.error('Security key is required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    var data = {
        Security: $("#txtSecurityVoid").val()
    }
    $.ajax({
        type: "POST",
        url: "/Sales/CreditMemo.aspx/CheckSecurity",
        data: "{'datavalue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == 'SessionExpired') {
                location.href = '/';
            } else {
                if (check == false) {
                    var xmldoc = $.parseXML(response.d);
                    var orderList = $(xmldoc).find("Table");
                    if (orderList.length > 0) {
                        $.each(orderList, function () {
                            if ($("#txtSecurityVoid").val() == $(orderList).find("SecurityValue").text()) {
                                check = true
                                $("#SecurityEnabledVoid").modal('hide');
                                $("#ModelCancelRemark").modal('show');
                            }
                        });
                    }
                    else {
                        swal("", "Access denied.", "warning", {
                            button: "OK",
                        }).then(function () {
                            $("#txtSecurityVoid").val('');
                            $("#txtSecurityVoid").focus();
                        })
                    }
                }
                else {
                    $("#SecurityEnabledVoid").modal('hide');
                }
            }
        },
        failure: function (result) {
            swal("", "Oops! Something went wrong. Please try later.", "error");
        },
        error: function (result) {
            swal("", "Oops! Something went wrong. Please try later.", "error");
        }
    });
}
function SaveCancelReason() {
    if ($("#ReasonCancel").val() == "") {
        $("#ReasonCancel").addClass('border-warning');
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    cancelRemark();
}
function cancelRemark() {
    swal({
        title: "Are you sure?",
        text: "You want to cancel this credit memo.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, Cancel.",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: false,
            },
            confirm: {
                text: "Yes, Cancel it.",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            Cancelcreditmemo2();

        } else {
            swal("", "Your credit memo is safe.", "error");
        }
    })
}

function Cancelcreditmemo2() {
    var data = {
        CancelRemark: $("#ReasonCancel").val(),
        CreditAutoId: $('#CreditAutoId').val()
    }
    $.ajax({
        type: "POST",
        url: "/Sales/CreditMemo.aspx/CancelCreditMomo",
        data: "{'datavalue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {

            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },

        success: function (response) {
            if (response.d != 'Session Expired') {
                console.log(response.d)
                if (response.d == 'Success') {
                    swal("", "Credit memo cancelled successfully.", "success");
                    $("#ModelCancelRemark").modal('hide');
                    $("#ReasonCancel").val('');
                    $("#btnRemoveMlTax").hide();
                    editCredit($('#CreditAutoId').val())

                } else {
                    swal("", response.d, "error");
                    $("#ModelCancelRemark").modal('hide');
                    $("#ReasonCancel").val('');
                }
            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function blockCopyPaste() {
    debugger
    $('#txtReqQty').on("cut copy paste", function (e) {
        e.preventDefault();
    });
    $('#txtOverallDisc').on("cut copy paste", function (e) {
        e.preventDefault();
    });
    $('#txtDiscAmt').on("cut copy paste", function (e) {
        e.preventDefault();
    });
    $("#tblProductDetail input").on("cut copy paste", function (e) {
        e.preventDefault();
    });
}