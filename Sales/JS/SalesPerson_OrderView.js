﻿var rowupdate = "";
var CustomerId; var MLQtyRate = 0.00;
$(document).ready(function () {
    var getid = getQueryString('OrderAutoId');
    if (getid != null) {
        getOrderData(getid);
        $("#txtHOrderAutoId").val(getid);
    }
});
var getQueryString = function (field, url) {
    var href = url ? url : window.location.href;
    var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
    var string = reg.exec(href);
    return string ? string[1] : null;
};
function getOrderData(OrderAutoId) {
    $.ajax({
        type: "POST",
        async: false,
        url: "SalesPerson_OrderView.aspx/getOrderData",
        data: "{'OrderAutoId':'" + OrderAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var order = $(xmldoc).find("Table");
            var product = $(xmldoc).find("Table1");
            var duePayment = $(xmldoc).find("Table2");
            var CreditMemoDetails = $(xmldoc).find("Table3");
            var RemarksDetails = $(xmldoc).find("Table4");
            var pkg = $(xmldoc).find("Table5");
            var CreditDetails = $(xmldoc).find("Table6");
            if ($(order).find("StatusCode").text() == 4 || $(order).find("StatusCode").text() == 5 || $(order).find("StatusCode").text() == 7) {
                $("#showedit").show();
            } else {
                $("#showedit").hide();
            }
            $("#shippingtype").val($(order).find("ShipAutoId").text());
            if (Number(CreditDetails.length) > 0 && $(order).find("StatusCode").text() <= 4) {

                $("#tblCreditMemo tbody tr").remove();
                var rowc = $("#tblCreditMemo thead tr:last").clone(true);
                $.each(CreditDetails, function (index) {
                    $(".SRNO", rowc).text(Number(index) + 1);
                    $(".CreditNo", rowc).html($(this).find("CreditNo").text());
                    $(".CreditDate", rowc).text($(this).find("CreditDate").text());
                    $(".NoofItem", rowc).text($(this).find("NoofItem").text());
                    $(".NetAmount", rowc).text($(this).find("NetAmount").text());
                    $("#tblCreditMemo tbody").append(rowc);
                    rowc = $("#tblCreditMemo tbody tr:last").clone(true);
                });
            }
            $("#tblPacked tbody tr").remove();
            var rowP = $("#tblPacked thead tr:last").clone(true);
            if (pkg.length > 0) {
                $("#tblPackedDetails").show();
                $.each(pkg, function (index) {
                    $(".SRNO", rowP).text(Number(index) + 1);
                    $(".PackedId", rowP).text($(this).find("PackingId").text());
                    $(".PackedDate", rowP).text($(this).find("PkgDate").text());
                    $(".PackedBy", rowP).text($(this).find("Packer").text());
                    $('#tblPacked').find("tbody").append(rowP);
                    rowP = $("#tblPacked tbody tr:last").clone(true);
                });
            } else {
                $("#tblPackedDetails").hide();
            }

            $("#Table2 tbody tr").remove();
            if ($(RemarksDetails).length > 0) {
                $("#OrderRemarksDetails").show();
                var rowtest = $("#Table2 thead tr").clone();
                $.each(RemarksDetails, function (index) {
                    $(".SRNO", rowtest).html((Number(index) + 1));
                    $(".EmployeeName", rowtest).html($(this).find("EmpName").text());
                    $(".EmployeeType", rowtest).html($(this).find("EmpType").text());
                    $(".Remarks", rowtest).html($(this).find("Remarks").text());
                    $("#Table2 tbody").append(rowtest);
                    rowtest = $("#Table2 tbody tr:last").clone(true);
                });
            }
            var totalCredit = 0.00;
            if (Number(CreditMemoDetails.length) > 0) {
                $("#CusCreditMemo").show();
                $("#tblCreditMemoListDetails").show();
                $("#tblCreditMemoList tbody tr").remove();
                var rowc = $("#tblCreditMemoList thead tr:last").clone(true);
                $.each(CreditMemoDetails, function (index) {

                    $(".SRNO", rowc).text(Number(index) + 1);
                    $(".Action", rowc).html("<input type='radio' name='credit' value=" + $(this).find("CreditAutoId").text() + "> ");
                    $(".CreditNo", rowc).text($(this).find("CreditNo").text());
                    $(".CreditDate", rowc).text($(this).find("CreditDate").text());
                    $(".ReturnValue", rowc).text($(this).find("ReturnValue").text());
                    totalCredit += parseFloat($(this).find("ReturnValue").text())
                    $("#tblCreditMemoList tbody").append(rowc)
                    rowc = $("#tblCreditMemoList tbody tr:last").clone(true);
                });
            } else {
                $("#CusCreditMemo").hide();
            }
            $("#TotalDue").text(totalCredit.toFixed(2));

            CustomerId = $(order).find("CustomerId").text();
           
            $("#txtRemarks").val($(order).find("DrvRemarks").text());
            if ($(order).find("PaymentRecev").text() == null || $(order).find("PaymentRecev").text() == "") {
                $("#AccountDeliveryInfo").hide();
            } else {
                $("#AccountDeliveryInfo").show();
            }
            $('#txtRecievedPayment').val($(order).find("PaymentRecev").text());
            $("#txtAmtPaid").text($(order).find("AmtPaid").text());
            $("#txtAmtDue").val($(order).find("AmtDue").text());
            $("#txtAcctRemarks").val($(order).find("AcctRemarks").text());
            $("#txtTaxType").val($(order).find("TaxType").text());
            $("#txtHOrderAutoId").val($(order).find("AutoId").text());
            $("#txtOrderId").val($(order).find("OrderNo").text());
            $("#lblOrderno").text($(order).find("OrderNo").text());
            $("#txtOrderType").val($(order).find("OrderType").text());
            $("#hfShippingAutoId").val($(order).find("ShipAutoId").text());
            $("#txtShippingType").val($(order).find("ShippingType").text());

            if (Number($(order).find("StatusCode").text()) > 5) {
                $('#DrvDeliveryInfo').show();
            }
            $("#txtOrderDate").val($(order).find("OrderDate").text());
            $('#txtAssignDate').pickadate({
                format: 'mm/dd/yyyy',
                formatSubmit: 'mm/dd/yyyy',
                selectYears: true,
                selectMonths: true,
                min: -7,
                firstDay: 0
            });
            if ($(order).find("CommentType").text() != '')
                $("#ddlCommentType").val($(order).find("CommentType").text());
            $("#txtComment").val($(order).find("Comment").text());
            $("#txtDeliveryDate").val($(order).find("DeliveryDate").text());
            $("#txtOrderStatus").val($(order).find("Status").text());
            $("#txtCustomer").val($(order).find("CustomerName").text());
            $("#hiddenCustAutoId").val($(order).find("CustAutoId").text());
            $("#txtTerms").val($(order).find("Terms").text());
            $("#txtSalesperson").val($(order).find("SalesPerson").text());
            $("#txtCustomerType").val($(order).find("CustomerType").text());
            $("#txtTerms").val($(order).find("TermsDesc").text());
            $("#OrderRemarks").val($(order).find("OrderRemarks").text());
            $("#txtAdjustment").val($(order).find("AdjustmentAmt").text());
            $("#txtMLQty").val($(order).find("MLQty").text());
            $("#txtWeightQty").val($(order).find("WeightOzQty").text());

            $("#txtMLTax").val($(order).find("MLTax").text());
            $("#txtWeightTax").val($(order).find("WeightOzTotalTax").text());

            if ($(order).find("CreditAmount").text() != "") {
                $("#txtStoreCreditAmount").val($(order).find("CreditAmount").text());
            }
            if ($(order).find('PayableAmount').text() != '') {
                $("#txtPaybleAmount").val($(order).find("PayableAmount").text());
            } else {
                $("#txtPaybleAmount").val($(order).find("GrandTotal").text());
            }
            if ($(order).find("DeductionAmount").text() != "")
                $("#txtDeductionAmount").val($(order).find("DeductionAmount").text());
            $("#CreditMemoAmount").text($(order).find("CustomerCredit").text());

            $("#txtBillAddress").val($(order).find("BillAddr").text());
          
            if ($(order).find('isMLManualyApply').text() == '1') {
                $('#btnRemoveMlTax button').html('Remove ML Tax');
            } else {
                $('#btnRemoveMlTax button').html('Add ML Tax')
            }
            $("#hfMLTaxStatus").val($(order).find('isMLManualyApply').text());
            if ($(order).find("StatusCode").text() != 8 && $(order).find("EnabledTax").text() == 1) {
                $('#btnRemoveMlTax').show();
            } else {
                $('#btnRemoveMlTax').hide();
            }

            $("#txtShipAddress").val($(order).find("ShipAddr").text());
           
            $("#txtTotalAmount").val($(order).find("TotalAmount").text());
            $("#txtOverallDisc").val($(order).find("OverallDisc").text());
            $("#txtDiscAmt").val($(order).find("OverallDiscAmt").text());
            $("#txtShipping").val($(order).find("ShippingCharges").text());
            $("#txtTotalTax").val($(order).find("TotalTax").text());
            $("#txtGrandTotal").val($(order).find("GrandTotal").text());
            $("#hiddenGrandTotal").val($(order).find("GrandTotal").text());
            $("#txtPackedBoxes").val($(order).find("PackedBoxes").text());
            $("#lblNoOfbox").text($(order).find("PackedBoxes").text());
            showDuePayments(duePayment);
            $("#tblProductDetail tbody tr").remove();
            var row = $("#tblProductDetail thead tr").clone(true);
            $.each(product, function () {
                $(".ProId", row).html($(this).find("ProductId").text());
                if (Number($(this).find("isFreeItem").text()) == 1) {
                    $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span> <product class='badge badge badge-pill badge-success'>Free</product>");

                } else if (Number($(this).find("IsExchange").text()) == 1) {
                    $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span> <product class='badge badge badge-pill badge-primary'>Exchange</product>");
                }
                else if (Number($(this).find("Tax").text()) == 1) {
                    $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span> <product class='badge badge badge-pill badge-danger'>Taxable</product>");
                } else {
                    $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span>");
                }
                $(".UnitType", row).html($(this).find("UnitType").text() + " (" + $(this).find("QtyPerUnit").text() + " pcs" + ")");
                $(".TtlPcs", row).text($(this).find("TotalPieces").text());
                $(".ItemTotal", row).text($(this).find("ItemTotal").text());
                $(".Discount", row).text(parseFloat($(this).find("Discount").text()).toFixed(2));
                $(".NetPrice", row).text($(this).find("NetPrice").text());

                $(".UnitPrice", row).html($(this).find("UnitPrice").text());

                $(".RequiredQty", row).text($(this).find("RequiredQty").text());

                $(".SRP", row).text($(this).find("SRP").text());
                $(".GP", row).text($(this).find("GP").text());
                if (Number($(this).find("Tax").text()) == 1) {
                    $(".TaxRate", row).html('<span class="la la-check-circle success"></span>');
                } else {
                    $(".TaxRate", row).html('');
                }
                if (Number($(this).find("IsExchange").text()) == 1) {
                    $(".IsExchange", row).html('<span class="la la-check-circle success"></span>');
                } else {
                    $(".IsExchange", row).html('');
                }
                $(".Barcode", row).text($(this).find("Barcode").text());

                $(".QtyShip", row).text($(this).find("QtyShip").text());

                if ($(order).find("Status").text() == "Close") {
                    $(".PerPrice", row).text($(this).find("PerpiecePrice").text());
                    if ($(this).find("QtyDel").text() != "") {
                        $(".QtyDel", row).html($(this).find("QtyDel").text());
                    } else {
                        $(".QtyDel", row).html($(this).find("TotalPieces").text());
                    }
                    $(".FreshReturn", row).html($(this).find("FreshReturnQty").text());
                    $(".DemageReturn", row).html($(this).find("DamageReturnQty").text());
                    $(".MissingItem", row).html($(this).find("MissingItemQty").text());
                    if (Number($(this).find("FreshReturnQty").text()) > 0 || Number($(this).find("DamageReturnQty").text()) > 0) {
                        $(row).css('background-color', '#efcccc');
                    } else if (Number($(this).find("MissingItemQty").text()) > 0) {
                        $(row).css('background-color', '#edb3b3');
                    } else {
                        $(row).css('background-color', '#fff');
                    }

                } else {
                    $('#tblProductDetail td:nth-child(9)').hide();
                    $('#tblProductDetail td:nth-child(10)').hide();
                    $('#tblProductDetail td:nth-child(11)').hide();
                    $('#tblProductDetail td:nth-child(12)').hide();
                    $('#tblProductDetail td:nth-child(13)').hide();
                    $(".MissingItem", row).hide();
                    $(".FreshReturn", row).hide();
                    $(".DemageReturn", row).hide();
                    $(".QtyDel", row).hide();
                    $(".PerPrice", row).hide();
                }

                if (Number($(this).find("RequiredQty").text()) != Number($(this).find("QtyShip").text())) {
                    $(row).css('background-color', '#87dabc');  
                }
                else {
                    $(row).css('background-color', '#fff');
                }
                $('#tblProductDetail tbody').append(row);
                row = $("#tblProductDetail tbody tr:last").clone(true);
            });
            if (Number($(order).find("StatusCode").text()) < 3) {
                $('#PackingSlip').closest('.row').hide();
                $('#WithoutCategorybreakdown').closest('.row').hide();
            } else {
                $('#PackingSlip').closest('.row').show();
                $('#WithoutCategorybreakdown').closest('.row').show();
            }
            //uuu
            if (Number($(order).find("StatusCode").text()) == 9) {
                $("#btnAsgnDrv").hide();
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}


function showDuePayments(duePayment) {
    $("#tblCustDues tbody tr").remove();
    var row = $("#tblCustDues thead tr").clone(true);

    var sumOrderValue = 0,
        sumPaid = 0,
        sumDue = 0;

    if (duePayment.length > 0) {
        $("#noDues").hide();
        $("#CustomerPaymentsHistory").show();
        $.each(duePayment, function () {
            $(".orderNo", row).html("<span orderautoid='" + $(this).find("AutoId").text() + "'><a href='#'>" + $(this).find("OrderNo").text() + "</a></span>");
            $(".orderDate", row).text($(this).find("OrderDate").text());
            $(".value", row).text($(this).find("GrandTotal").text());
            $(".OrderType", row).text($(this).find("OrderType").text());
            $(".amtPaid", row).text($(this).find("AmtPaid").text()).css("color", "#8bc548");
            $(".amtDue", row).text($(this).find("AmtDue").text()).css("color", "red");
            $(".pay", row).html("<input type='text' class='form-control input-sm' style='width:100px;float:right;text-align:right;' value='0.00' placeholder='0.00' onkeyup='sumPay()' />");
            $(".remarks", row).html("<input type='text' class='form-control input-sm' placeholder='Enter payment details here' />");

            sumOrderValue += Number($(this).find("GrandTotal").text());
            sumPaid += Number($(this).find("AmtPaid").text());
            sumDue += Number($(this).find("AmtDue").text());

            $('#tblCustDues').find("tbody").append(row).end()
                .find("tfoot").show().find("#sumOrderValue").text(sumOrderValue.toFixed(2)).end()
                .find("#sumPaid").text(sumPaid.toFixed(2)).css("color", "#8bc548").end()
                .find("#sumDue").text(sumDue.toFixed(2)).css("color", "red").end()
                .find("#sumPay").text("0.00").end();

            row = $("#tblCustDues tbody tr:last").clone(true);
        });
        $("#CustDues").show();
        if ($("#hiddenEmpTypeVal").val() != "5") {
            $("#CustDues").find("#btnPay_Dues").show().end()
                .find("#tblCustDues").find(".pay").show().end()
                .find(".remarks").show().end()
                .find("tfoot > tr > td").show();
        }
    } else {
        $("#tblCustDues").find("tbody tr").remove().end().find("tfoot").hide();
        $("#noDues").show();
        $("#CustDues").show();
        $("#btnPay_Dues, #btnClose_Dues").hide();
        $("CustomerPaymentsHistory").hide();
    }
}
/*-------------------------------------------------------------------------------------------------------------------------------*/
//                                            Summation of Pay Amount against Order Dues

function printOrder_CustCopy() {
    $("#SavedMassege").modal('hide');
    if ($('#HDDomain').val() == 'psmpa') {
        $("#PSMPADefault").show();
        $("#rPSMPADefault").prop('checked', true);
    }
    else if ($('#HDDomain').val() == 'psmwpa') {
        $("#PSMWPADefault").show();
        $("#chkdueamount").prop('checked', true);
    }
    else if ($('#HDDomain').val() == 'psmnpa') {//PSMNPA Default changed on 11/01/2019
        $("#divPSMNPADefault").show();
        $("#PSMPADefault").hide();
        $("#rCheckNPADefault").prop('checked', true);
    }
    else {
        $("#chkdueamount").prop('checked', true);
        $("#PSMPADefault").closest('.row').hide();
    }
    $('#PopPrintTemplate').modal('show');
    $("#SavedMassege").modal('hide');
}
function PrintOrder() {

    $('#PopPrintTemplate').modal('hide');
    if ($("#chkdueamount").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF1.html?OrderAutoId=" + $("#txtHOrderAutoId").val() + "", "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    } else if ($("#chkdefault").prop('checked') == true) {
        window.open("/Manager/OrderPrintCC.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#Template1").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF2.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#Template2").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF3.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }

    else if ($("#rPSMWPADefault").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF6.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#rPSMPADefault").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF4.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#PackingSlip").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF5.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#WithoutCategorybreakdown").prop('checked') == true) {
        window.open("/Manager/WithoutCategorybreakdown.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#rCheckNPADefault").prop('checked') == true) { //PSMNPA Default changed on 11/01/2019
        window.open("/Manager/OrderPrintCCF4.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#printOrderItemList").prop('checked') == true) {
        window.open("/Manager/PrintOrderItem.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#Templ1").prop('checked') == true) {
        window.open("/Manager/PrintOrderItem.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#Templ2").prop('checked') == true) {
        window.open("/Manager/PrintOrderItemTemplate2.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    closePrintPop();
}
function viewOrderLog() {
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/WOrderLog.asmx/viewOrderLog",
        data: "{'OrderAutoId':" + $("#txtHOrderAutoId").val() + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var orderlog = $(xmldoc).find("Table");
            var order = $(xmldoc).find("Table1");

            $("#Span1").text(order.find("OrderNo").text());
            $("#lblOrderDate").text(order.find("OrderDate").text());

            $("#tblOrderLog tbody tr").remove();
            var row = $("#tblOrderLog thead tr").clone(true);
            if (orderlog.length > 0) {
                $("#EmptyTable").hide();
                $.each(orderlog, function (index) {
                    $(".SrNo", row).text(index + 1);
                    $(".ActionBy", row).text($(this).find("EmpName").text());
                    $(".Date", row).text($(this).find("ActionDate").text());
                    $(".Action", row).text($(this).find("Action").text());
                    $(".Remark", row).text($(this).find("Remarks").text());

                    $("#tblOrderLog tbody").append(row);
                    row = $("#tblOrderLog tbody tr:last").clone(true);
                });
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
    $("#modalOrderLog").modal('show');
}