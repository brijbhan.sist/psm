﻿$(document).ready(function () {

    if ($("#hiddenEmpType").val() == 2 || $("#hiddenEmpType").val() == 8) {
        $("#linktoOrderList").show();
    }
    if ($("#hiddenEmpType").val() == 2) {
        $("#col4").show();
    }
    else {
        $("#col4").hide();
    }
    if ($("#hiddenEmpType").val() == 7) {
        $("#btnExport").show();
    }

    bindStatus();
    $('#txtSFromDate').pickadate({
        min: new Date("01/01/2019"),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtSToDate').pickadate({
        min: new Date("01/01/2019"),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });

    var d = new Date();
    var month = d.getMonth() + 1;
    if (month.toString().length == 1) {
        month = '0' + month;
    }
    var day = d.getDate();
    if (day.toString().length == 1) {
        day = '0' + day;
    }
    var year = d.getFullYear();
    $("#txtSFromDate").val(month + '/' + day + '/' + year);
    $("#txtSToDate").val(month + '/' + day + '/' + year);

    if ($("#hiddenEmpType").val() == 2) {
        $('.SalesPerson').hide();
        $('#SalesPerson').hide();
    }
    else {
        $('#SalesPerson').show();
    }
    if ($("#hiddenEmpType").val() == 8) {
        $('.CreditMemo').show();
    }
})

function setdatevalidation(val) {
    var fdate = $("#txtSFromDate").val();
    var tdate = $("#txtSToDate").val();
    if (val == 1) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtSToDate").val(fdate);
        }
    }
    else if (val == 2) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtSFromDate").val(tdate);
        }
    }
}
function Pagevalue(e) {
    getOrderList(parseInt($(e).attr("page")));
};

function bindStatus() {
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/orderList.asmx/bindStatus",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var status = $(xmldoc).find("Table");
                var customer = $(xmldoc).find("Table1");
                var SalesPerson = $(xmldoc).find("Table2");
                var ShippingType = $(xmldoc).find("Table4");

                $("#ddlSStatus option:not(:first)").remove();
                $.each(status, function () {
                    $("#ddlSStatus").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("StatusType").text() + "</option>");
                });

                if (Number($("#hiddenEmpType").val()) == 7) {
                    $("#ddlSStatus").val('1');
                }
                $.each(customer, function () {
                    $("#ddlCustomer").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("Customer").text() + "</option>");
                });
                $("#ddlCustomer").select2();
                $("#ddlSalesPerson option:not(:first)").remove();
                $.each(SalesPerson, function () {
                    $("#ddlSalesPerson").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("EmpName").text() + "</option>");
                });
                $("#ddlSalesPerson").select2();

                $("#ddlShippingType option:not(:first)").remove();
                $.each(ShippingType, function () {
                    $("#ddlShippingType").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("ShippingType").text() + "</option>");
                }); $("#ddlShippingType").attr('multiple', 'multiple');
                $("#ddlShippingType").select2({
                    placeholder: 'All Shipping Type',
                    allowClear: true
                });


            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function getOrderList(pageIndex) {

    var ShippingType = '0';
    $("#ddlShippingType option:selected").each(function (i) {
        if (i == 0) {
            ShippingType = $(this).val() + ',';
        } else {
            ShippingType += $(this).val() + ',';
        }
    });

    var data = {
        OrderNo: $("#txtSOrderNo").val().trim(),
        CustomerAutoid: $("#ddlCustomer").val(),
        SalesPersonAutoId: $("#ddlSalesPerson").val(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        Status: $("#ddlSStatus").val(),
        pageIndex: pageIndex,
        PageSize: $("#ddlPageSize").val(),
        ShippingType: ShippingType.toString()
    };

    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/orderList.asmx/getOrderList",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var orderList = $(xmldoc).find("Table1");

                $("#tblOrderList tbody tr").remove();
                var row = $("#tblOrderList thead tr").clone(true);

                if (orderList.length > 0) {
                    $("#EmptyTable").hide();
                    $.each(orderList, function () {
                        if ($(this).find("ShipId").text() == '2' || $(this).find("ShipId").text() == '7' || $(this).find("ShipId").text() == '4') {
                            $(row).css('background-color', '#ffe6e6');
                        }
                        else if ($(this).find("ShipId").text() == '8') {
                            $(row).css('background-color', '#eb99ff');
                        }
                        else if ($(this).find("ShipId").text() == '9') {
                            $(row).css('background-color', '#e6fff2');
                        }
                        else if ($(this).find("ShipId").text() == '10') {
                            $(row).css('background-color', '#ccff99');
                        }
                        else {
                            $(row).css('background-color', 'white');
                        }
                        $(".orderNo", row).html("<span OrderAutoId='" + $(this).find("AutoId").text() + "'>" + $(this).find("OrderNo").text() + "</span>");
                        $(".orderDt", row).text($(this).find("OrderDate").text());
                        $(".Shipping", row).text($(this).find("ShippingType").text());
                        $(".cust", row).text($(this).find("CustomerName").text());
                        $(".value", row).text($(this).find("GrandTotal").text());
                        $(".product", row).text($(this).find("NoOfItems").text());
                        $(".SalesPerson", row).text($(this).find("SalesPerson").text());
                        $(".CreditMemo", row).text($(this).find("CreditMemo").text());
                        if (Number($(this).find("StatusCode").text()) == 1) {
                            $(".status", row).html("<span class='badge badge badge-pill Status_New'>" + $(this).find("Status").text() + "</span>");
                        } else if (Number($(this).find("StatusCode").text()) == 2) {
                            $(".status", row).html("<span class='badge badge badge-pill Status_Processed'>" + $(this).find("Status").text() + "</span>");
                        } else if (Number($(this).find("StatusCode").text()) == 3) {
                            $(".status", row).html("<span class='badge badge badge-pill Status_Packed'>" + $(this).find("Status").text() + "</span>");
                        } else if (Number($(this).find("StatusCode").text()) == 4) {
                            $(".status", row).html("<span class='badge badge badge-pill Status_Ready_to_Ship '>" + $(this).find("Status").text() + "</span>");
                        } else if (Number($(this).find("StatusCode").text()) == 5) {
                            $(".status", row).html("<span class='badge badge badge-pill Status_Shipped'>" + $(this).find("Status").text() + "</span>");
                        } else if (Number($(this).find("StatusCode").text()) == 6) {
                            $(".status", row).html("<span class='badge badge badge-pill Status_Delivered'>" + $(this).find("Status").text() + "</span>");
                        } else if (Number($(this).find("StatusCode").text()) == 7) {
                            $(".status", row).html("<span class='badge badge badge-pill Status_Undelivered'>" + $(this).find("Status").text() + "</span>");
                        } else if (Number($(this).find("StatusCode").text()) == 8) {
                            $(".status", row).html("<span class='badge badge badge-pill Status_cancelled' >" + $(this).find("Status").text() + "</span>");
                        }
                        else if (Number($(this).find("StatusCode").text()) == 9) {
                            $(".status", row).html("<span class='badge badge badge-pill Status_Add_On'>" + $(this).find("Status").text() + "</span>");
                        }
                        else if (Number($(this).find("StatusCode").text()) == 10) {
                            $(".status", row).html("<span class='badge badge badge-pill Status_Add_On_Packed'>" + $(this).find("Status").text() + "</span>");
                        }
                        else if (Number($(this).find("StatusCode").text()) == 11) {
                            $(".status", row).html("<span class='badge badge badge-pill Status_Close'>" + $(this).find("Status").text() + "</span>");
                        }
                        console.log($(this).find("Status").text());
                        if ($(this).find("Status").text() != 'New') {
                            $(".action", row).html("<b><a title='View' href='/Sales/SalesPerson_OrderView.aspx?OrderAutoId=" + $(this).find("AutoId").text() + "'><span class='la la-eye'></span></a>&nbsp;<a href='javascript:;' title='History' onclick='viewOrderLog(" + $(this).find("AutoId").text() + ")'><span class='la la-history'></span></a></b>");
                        }
                        else {
                            $(".action", row).html("<a title='Edit' href='/Sales/orderMaster.aspx?OrderNo=" + $(this).find("OrderNo").text() + "'><span class='la la-edit'></span></a>&nbsp;<a href='javascript:;' title='History' onclick='viewOrderLog(" + $(this).find("AutoId").text() + ")'><span class='la la-history'></span></a>&nbsp;<a href='javascript:;' title='Delete'><span class='la la-remove' onclick='deleterecord(\"" + $(this).find("AutoId").text() + "\")'></span><b></a></b>");
                        }

                        $("#tblOrderList tbody").append(row);
                        row = $("#tblOrderList tbody tr:last").clone(true);
                    });
                } else {
                    $("#EmptyTable").show();
                }

                var pager = $(xmldoc).find("Table");
                $(".Pager").ASPSnippets_Pager({
                    ActiveCssClass: "current",
                    PagerCssClass: "pager",
                    PageIndex: parseInt(pager.find("PageIndex").text()),
                    PageSize: parseInt(pager.find("PageSize").text()),
                    RecordCount: parseInt(pager.find("RecordCount").text())
                });
            } else {
                location.href = "/";
            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function print_NewOrder(e) {
    window.open("/Packer/OrderPrint.html?OrderAutoId=" + $(e).closest("tr").find(".orderNo span").attr("OrderAutoId"), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    setTimeout(function () {
        getOrderList(1);
    }, 2000);
}

$("#btnSearch").click(function () {
    getOrderList(1);
})

$("#ddlPageSize").change(function () {
    getOrderList(1);
})

function deleterecord(orderAutoId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this Order.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, Cancel",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, Delete It",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            deleteOrder(orderAutoId);
        } 
    })
}

function deleteOrder(orderAutoId) {
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/orderList.asmx/deleteOrder",
        data: "{'OrderAutoId':" + orderAutoId + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == "Session Expired") {
                location.href = "/";
            }
            else {
                swal("", "Order details deleted successfully.", "success")
                getOrderList(1);
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function viewOrderLog(OrderAutoId) {
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/WOrderLog.asmx/viewOrderLog",
        data: "{'OrderAutoId':" + OrderAutoId + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var orderlog = $(xmldoc).find("Table");
            var order = $(xmldoc).find("Table1");

            $("#lblOrderNo").text(order.find("OrderNo").text());
            $("#lblOrderDate").text(order.find("OrderDate").text());

            $("#tblOrderLog tbody tr").remove();
            var row = $("#tblOrderLog thead tr").clone(true);
            if (orderlog.length > 0) {
                $("#EmptyTable").hide();
                $.each(orderlog, function (index) {
                    $(".SrNo", row).text(index + 1);
                    $(".ActionBy", row).text($(this).find("EmpName").text());
                    $(".Date", row).text($(this).find("ActionDate").text());
                    $(".Action", row).text($(this).find("Action").text());
                    $(".Remark", row).text($(this).find("Remarks").text());

                    $("#tblOrderLog tbody").append(row);
                    row = $("#tblOrderLog tbody tr:last").clone(true);
                });
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
    $("#modalOrderLog").modal('show');
}
$("#btnExport").click(function () {
    var ShippingType = '0';
    $("#ddlShippingType option:selected").each(function (i) {
        if (i == 0) {
            ShippingType = $(this).val() + ',';
        } else {
            ShippingType += $(this).val() + ',';
        }
    });
    var data = {
        OrderNo: $("#txtSOrderNo").val(),
        CustomerAutoid: $("#ddlCustomer").val(),
        SalesPersonAutoId: $("#ddlSalesPerson").val(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        Status: $("#ddlSStatus").val(),
        pageIndex: 1,
        ShippingType: ShippingType.toString(),
        PageSize: 0,
    };

    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/orderList.asmx/getOrderList",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('#fade').show();
        },
        complete: function () {
            $('#fade').hide();
        },
        success: function (response) {

            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var orderList = $(xmldoc).find("Table1");
                $("#tblExport tbody tr").remove();
                var row = $("#tblExport thead tr").clone(true);
                if (orderList.length > 0) {
                    $.each(orderList, function () {
                        $(".orderNo", row).html("<span OrderAutoId='" + $(this).find("AutoId").text() + "'>" + $(this).find("OrderNo").text() + "</span>");
                        $(".orderDt", row).text($(this).find("OrderDate").text());
                        $(".cust", row).text($(this).find("CustomerName").text());
                        $(".value", row).text($(this).find("GrandTotal").text());
                        $(".product", row).text($(this).find("NoOfItems").text());
                        $(".SalesPerson", row).text($(this).find("SalesPerson").text());
                        $(".status", row).html($(this).find("Status").text());
                        $("#tblExport tbody").append(row);
                        row = $("#tblExport tbody tr:last").clone(true);
                    });
                }
                $("#tblExport").table2excel({

                    exclude: ".noExl",
                    name: "Excel Document Name",
                    filename: "Order List",
                    fileext: ".xls",
                    exclude_img: true,
                    exclude_links: true,
                    exclude_inputs: true
                });

            } else {
                location.href = "/";
            }

        },
        failure: function (result) {
        },
        error: function (result) {
        }
    });
});
