﻿var ContactPerson = [];
$(document).ready(function () {
    $('#txtUserId').removeClass('req');
    $('#txtPassword').removeClass('req');
    $('#txtConfirmPassword').removeClass('req');
    bindDropDowns();
    bindContactPersonType();
    var getid = getQueryString('PageId');
    if (getid != null) {
        editCustomer(getid);
        $('#DivBillingAddresssection input').removeClass('req');
        $('#DivShippingAddresssection input').removeClass('req');
    } else {
        $('.contactPart').show();
        $('#DivShippingAddresssection').show();
        $('#DivBillingAddresssection').show();

    }
});
var getQueryString = function (field, url) {
    var href = url ? url : window.location.href;
    var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
    var string = reg.exec(href);
    return string ? string[1] : null;
};
/*------------------------------------------------------Bind State , SALES PERSON , Status------------------------------------------------------------*/
function bindDropDowns() {
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/customerMaster.asmx/bindDropDownsList",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            if (response.d != "Session Expired") {
                var DropDown = $.parseJSON(response.d);
                for (var i = 0; i < DropDown.length; i++) {
                    var AllDropDownList = DropDown[i];
                    var CustomerType = AllDropDownList.CustomerType;
                    var ddlCustomerType = $("#ddlCustType");
                    $("#ddlCustType option:not(:first)").remove();
                    for (var j = 0; j < CustomerType.length; j++) {
                        var CustomerDropList = CustomerType[j];
                        var option = $("<option />");
                        option.html(CustomerDropList.CustomerType);
                        option.val(CustomerDropList.AutoId);
                        ddlCustomerType.append(option);
                    }
                    ddlCustomerType.select2();
                    var ddlStatus = $("#ddlStatus");
                    $("#ddlStatus option").remove();
                    var Status = AllDropDownList.Status;
                    for (var k = 0; k < Status.length; k++) {
                        var StatusDropList = Status[k];
                        var option = $("<option />");
                        option.html(StatusDropList.StatusType);
                        option.val(StatusDropList.AutoId);
                        ddlStatus.append(option);
                    }
                    ddlStatus.select2();
                    var ddlEmployee = $("#ddlSalesPerson");
                    $("#ddlSalesPerson option:not(:first)").remove();
                    var Employee = AllDropDownList.Employee;
                    for (var l = 0; l < Employee.length; l++) {
                        var EmployeeDropList = Employee[l];
                        var option = $("<option />");
                        option.html(EmployeeDropList.Name);
                        option.val(EmployeeDropList.AutoId);
                        ddlEmployee.append(option);
                    }
                    ddlEmployee.select2();
                    var ddlTerms = $("#ddlTerms");
                    $("#ddlTerms option:not(:first)").remove();
                    var Terms = AllDropDownList.Terms;
                    for (var m = 0; m < Terms.length; m++) {
                        var TermsDropList = Terms[m];
                        var option = $("<option />");
                        option.html(TermsDropList.TermsDesc);
                        option.val(TermsDropList.TermsId);
                        ddlTerms.append(option);
                    }
                    ddlTerms.select2();
                    var ddlZipCode1 = $("#ddlZipCode1");
                    $("#ddlZipCode1 option:not(:first)").remove();
                    $("#ddlZipCode2 option:not(:first)").remove();
                    var ddlZipCode2 = $("#ddlZipCode2");
                    var ZipCode1 = AllDropDownList.ZipCode1;
                    for (var n = 0; n < ZipCode1.length; n++) {
                        var ZipCode1DropList = ZipCode1[n];
                        var option = $("<option />");
                        option.html(ZipCode1DropList.ZipCode);
                        option.val(ZipCode1DropList.AutoId);
                        option.attr('zm', ZipCode1DropList.ZM);
                        option.attr('CityId', ZipCode1DropList.CityId);
                        var option1 = option.clone();
                        ddlZipCode1.append(option);
                        ddlZipCode2.append(option1);
                    }
                    ddlZipCode1.select2();
                    ddlZipCode2.select2();
                    var CompanyDetails = AllDropDownList.CompanyDetails;
                    for (var p = 0; p < CompanyDetails.length; p++) {
                        var CompanyDetailsList = CompanyDetails[p];
                        $('#CompanyId').text('@' + CompanyDetailsList.CompanyId);
                    }


                    var StoreOpenTime = $("#StoreOpenTime");
                    $("#StoreOpenTime option:not(:first)").remove();
                    var StoreAllOpenTime = AllDropDownList.StoreTime;
                    for (var op = 0; op < StoreAllOpenTime.length; op++) {
                        var StoreAllOpenTimeList = StoreAllOpenTime[op];
                        var option = $("<option />");
                        option.html(StoreAllOpenTimeList.TimeFromLabel);
                        option.val(StoreAllOpenTimeList.TimeFrom);
                        StoreOpenTime.append(option);
                    }
                    StoreOpenTime.select2();
                    StoreOpenTime.val('06:00:00').change();


                    var StoreCloseTime = $("#StoreCloseTime");
                    $("#StoreCloseTime option:not(:first)").remove();
                    var StoreAllCloseTime = AllDropDownList.StoreTime;
                    for (var cl = 0; cl < StoreAllCloseTime.length; cl++) {
                        var StoreAllCloseTimeList = StoreAllCloseTime[cl];
                        var option = $("<option />");
                        option.html(StoreAllCloseTimeList.TimeToLabel);
                        option.val(StoreAllCloseTimeList.TimeTo);
                        StoreCloseTime.append(option);
                    }
                    StoreCloseTime.select2();
                    StoreCloseTime.val('22:00:00').change();


                }
            } else {
                location.href = '/';
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
/*------------------------------------------------------Insert Customer------------------------------------------------------------*/
$("#btnSave").click(function () {

    if ($("#ddlPriceLevel").val() == '0') {
        $("#ddlPriceLevel").addClass('border-warning');
    }
    else {
        $("#ddlPriceLevel").removeClass('border-warning');
    }
    if (CustcheckRequiredField()) {
        if ($("#chkSame").is(':checked') == true) {
            var latitudeship = $("#txtLat1").val();
            var longitudeship = $("#txtLong1").val();
        } else {
            var latitudeship = $("#txtLat").val();
            var longitudeship = $("#txtLong").val();
        }
        if (latitudeship == "" || longitudeship == "") {
            toastr.error('Invalid address.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            return false;
        }
        else if ($("#txtLat1").val() == "" || $("#txtLong1").val() == "") {
            toastr.error('Invalid address..', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            return false;
        }
        if (ContactPerson.length == 0) {
            toastr.error('Please add atleast one contact person', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            return false;
        }


        var Xmlcontactperson = '', def = 0;
        $("#tblContactDetails tbody tr").each(function () {
            Xmlcontactperson += '<contactpersonXML>';
            Xmlcontactperson += '<TypeAutoId><![CDATA[' + $(this).find('.TypeAutoId').html() + ']]></TypeAutoId>';
            Xmlcontactperson += '<ContactPerson><![CDATA[' + $(this).find('.ContactPerson').html() + ']]></ContactPerson>';
            Xmlcontactperson += '<Mobile><![CDATA[' + $(this).find('.Mobile').html() + ']]></Mobile>';
            Xmlcontactperson += '<Landline><![CDATA[' + $(this).find('.Landline').html() + ']]></Landline>';
            Xmlcontactperson += '<Landline2><![CDATA[' + $(this).find('.Landline2').html() + ']]></Landline2>';
            Xmlcontactperson += '<Fax><![CDATA[' + $(this).find('.Fax').html() + ']]></Fax>';
            Xmlcontactperson += '<Email><![CDATA[' + $(this).find('.Email').html() + ']]></Email>';
            Xmlcontactperson += '<AltEmail><![CDATA[' + $(this).find('.AltEmail').html() + ']]></AltEmail>';
            if ($(this).find('.IsDefault input').prop("checked") == true) {
                def = 1;
            }
            else {
                def = 0;
            }
            Xmlcontactperson += '<IsDefault><![CDATA[' + def + ']]></IsDefault>';
            Xmlcontactperson += '</contactpersonXML>';
        })
        var data = {
            CustomerName: $("#txtCustomerName").val().trim(),
            CustType: parseInt($("#ddlCustType").val()),
            LocatioAutoId: $("#ddlLocation").val() || 0,
            BillAdd: $("#txtBillAdd").val().trim(),
            Zipcode1: $('#txtZipcode1').val(),
            ShipAdd: ($("#chkSame").is(':checked') ? $("#txtBillAdd").val().trim() : $("#txtShipAdd").val().trim()),
            Zipcode2: $('#txtZipcode2').val(),
            CityName1: $('#txtCity1').val(),
            CityName2: $('#txtCity2').val(),
            State1: $('#txtState1').val(),
            State2: $('#txtState2').val(),
            BillAdd2: $("#txtBillAdd2").val().trim(),
            ShipAdd2: $("#txtShipAdd2").val().trim(),
            TaxId: $("#txtTaxId").val().trim(),
            Terms: parseInt($("#ddlTerms").val()),
            PriceLevelAutoId: parseInt($("#ddlPriceLevel").val()),
            Status: parseInt($("#ddlStatus").val()),
            SalesPersonAutoId: parseInt($("#ddlSalesPerson").val()),
            BusinessName: $("#txtBusinessName").val().trim(),
            OPTLicence: $("#txtOPTLicence").val().trim(),
            latitude: latitudeship,
            longitude: longitudeship,
            latitude1: $("#txtLat1").val(),
            longitude1: $("#txtLong1").val(),
            StoreOpenTime: $("#StoreOpenTime").val(),
            StoreCloseTime: $("#StoreCloseTime").val(),
            CustomerRemarks: $("#txtCustomerRemark").val().trim(),
            Xmlcontperson: Xmlcontactperson
        };
        $.ajax({
            type: "POST",
            url: "/Sales/WebAPI/customerMaster.asmx/insertCustomer",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d != "Session Expired") {
                    if (result.d == "true") {
                        swal("", "Customer details saved successfully.", "success").then(function () {
                            location.href = '/Sales/NewCustomerList.aspx';
                        });
                    }
                    else if (result.d == "ZipCode1") {
                        swal("", "Invalid Zip Code : " + $('#txtZipcode1').val(), "error");
                    }
                    else if (result.d == "State1") {
                        swal("", "Invalid State : " + $('#txtState1').val(), "error");
                    }

                    else if (result.d == "State2") {
                        swal("", "Invalid State : " + $('#txtState2').val(), "error");
                    }
                    else if (result.d == "ZipCode2") {
                        swal("", "Invalid Zip Code : " + $('#txtZipcode2').val(), "error");

                    }
                    else if (result.d == "Mobileexists") {
                        swal("", "Mobile No. already exists.", "error");
                    }
                    else if (result.d == "Emailexists") {
                        swal("", "Email already exists.", "error");
                    }
                    else {
                        swal("", result.d, "error");
                    }
                } else {
                    location.href = '/';
                }
            },
            error: function (result) {
                swal("Error!", "Oops! Something went wrong.Please try later.", "error");
            },
            failure: function (result) {
                swal("Error!", "Oops! Something went wrong.Please try later.", "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
});

function editCustomer(CustomerId) {
    $(".contactPart").hide();
    $("#DivBillingAddresssection").hide();
    $("#DivShippingAddresssection").hide();
    //  $("#txtShipAdd").removeClass('reqMobileLength(this)
    $("#txtBillAdd").removeClass('req');
    $("#ddlZipCode1").removeClass('ddlreq');
    $("#ddlZipCode2").removeClass('ddlreq');

    $.ajax({
        type: "POST",
        async: false,
        url: "/Sales/WebAPI/customerMaster.asmx/editCustomer",
        data: "{'CustomerId':'" + CustomerId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var CustomerDetails = $.parseJSON(response.d);

                var Details = CustomerDetails[0];

                $("#txtCustomerId").val(Details.CustomerId);
                $("#txtZone1").val(Details.Zone1);
                $("#txtCustomerName").val(Details.CustomerName);
                $("#ddlCustType").val(Details.CustomerType).change();

                if (Details.CustomerType == '3') {
                    $("#ddlLocation").val(Details.LocationAutoId).change();
                    $('#RowLocation').show();
                    $('#ddlLocation').addClass('ddlreq ');
                } else {
                    $('#ddlLocation').removeClass('ddlreq ');
                    $("#RowLocation").hide();
                }
                $("#hiddenBillAutoId").val(Details.BillAutoId);

                $("#txtBusinessName").val(Details.BusinessName);
                $("#txtOPTLicence").val(Details.OPTLicence);

                $("#txtCustomerRemark").val(Details.CustomerRemarks);

                if (Details.PriceLevelAutoId) {
                    $("#ddlPriceLevel").val(Details.PriceLevelAutoId).change();
                }

                $("#txtTaxId").val(Details.TaxId);
                $("#ddlTerms").val(Details.Terms).change();
                $("#ddlStatus").val(Details.Status).change();

                if (Details.UserName != null) {
                    var TEST = Details.UserName;
                    var UserName = TEST.split('@');
                    $("#txtUserId").val(UserName[0]);
                    $("#txtPassword").val(Details.Password);
                    $("#txtConfirmPassword").val(Details.Password);
                    localStorage.setItem('PSW', Details.Password);
                    var b = Details.IsAppLogin;

                    UserName1 = Details.UserName;
                    Password = Details.Password;
                }
                if (b == "1") {
                    $("#chkAppLogin").prop("checked", true);
                    $('#txtUserId').addClass('req');
                    $('#txtPassword').addClass('req');
                    $('#txtConfirmPassword').addClass('req');
                }
                else {
                    $("#chkAppLogin").prop("checked", false);
                    $('#txtUserId').removeClass('req');
                    $('#txtPassword').removeClass('req');
                    $('#txtConfirmPassword').removeClass('req');
                }

                if (Details.SalesPersonAutoId != '')
                    $("#ddlSalesPerson").val(Details.SalesPersonAutoId).change();

                $("#StoreOpenTime").val(Details.OptimotwFrom).change();
                $("#StoreCloseTime").val(Details.OptimotwTo).change();

                $("#btnSave").hide();
                $("#btnReset").hide();
                $("#btnUpdate").show();
                $("#ddlCustType").attr('disabled', true);
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

$("#btnUpdate").click(function () {


    if ($("#ddlSalesPerson").val() == null) {
        $("#ddlSalesPerson").val(0).change();
    }
    if (CustcheckRequiredField()) {
        var data = {
            CustomerId: $("#txtCustomerId").val(),
            LocationAutoId: $("#ddlLocation").val(),
            CustomerName: $("#txtCustomerName").val().trim(),
            CustType: parseInt($("#ddlCustType").val()),
            TaxId: $("#txtTaxId").val().trim(),
            Terms: parseInt($("#ddlTerms").val()),
            BusinessName: $("#txtBusinessName").val().trim(),
            PriceLevelAutoId: parseInt($("#ddlPriceLevel").val()),
            Status: parseInt($("#ddlStatus").val()),
            SalesPersonAutoId: parseInt($("#ddlSalesPerson").val()),
            OPTLicence: $("#txtOPTLicence").val().trim(),
            StoreOpenTime: $("#StoreOpenTime").val(),
            StoreCloseTime: $("#StoreCloseTime").val(),
            CustomerRemarks: $("#txtCustomerRemark").val().trim()
        };

        $.ajax({
            type: "POST",
            url: "/Sales/WebAPI/customerMaster.asmx/updateCustomer",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json;charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d != "Session Expired") {
                    if (result.d == "true") {
                        swal("", "Customer details updated successfully.", "success");
                    } else if (result.d == "ZipCode1") {
                        swal("", "Invalid Zip Code : " + $('#txtZipcode1').val(), "error");
                    } else if (result.d == "ZipCode2") {
                        swal("", "Invalid Zip Code : " + $('#txtZipcode2').val(), "error");
                    }
                    else if (result.d == "Customer details already exist.") {
                        swal("", "Customer already exist.", "error");
                    }
                    else {
                        swal("", "Oops! Something went wrong.Please try later.", "error");
                    }
                } else {
                    location.href = '/';
                }

            },
            error: function (result) {
                swal("Error!", "Email Already exists.!", "error");
            },
            failure: function (result) {
                swal("Error!", result.d, "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
});

$("#btnReset").click(function () {
    resetCustomer();
});
/*------------------------------------------------------Delete Customer------------------------------------------------------------*/
function deleteCustomer(CustomerId) {

    alert("***** Delete Customer option is not enabled. *****")

}

/*------------------------------------------------------Reset Customer Input Fields------------------------------------------------------------*/
function resetCustomer() {
    $("input[type='text']").val('');
    if ($("#chkSame").prop("checked") == false) {
        $("select").val(0);
        $("select").val(0).change();
    }
    $("#ddlStatus").val(1).change();
    $("textarea").val('');
    $("#btnSave").show();
    $("#btnReset").show();
    $("#ddlLocation").val(0).change();
    $("#RowLocation").hide();
    $("#btnUpdate").hide();
    $("input[type='text']").removeClass('border-warning');
    $(".select2-selection,.select2-selection--single").attr('style', '');
    $('#StoreOpenTime').val('06:00:00').change();
    $('#StoreCloseTime').val('22:00:00').change();
}
/*------------------------------------------------------------------*/
function fnChkSame() {
    if ($("#chkSame").prop("checked") == false) {
        $('#ddlZipCode2').val('0').change();
    }
    if ($("#chkSame").is(':checked')) {
        $('#divShippingAddr').hide();
        $('#txtShipAdd').removeClass('req');
        $('#txtLat').val('');
        $('#txtLong').val('');

        $('#txtShipAdd').val($("#txtBillAdd").val());
        $('#txtShipAdd2').val($("#txtBillAdd2").val());
        $('#txtZipcode2').val($("#txtZipcode1").val());
        $('#txtState2').val($("#txtState1").val());
        $('#txtCity2').val($("#txtCity1").val());
        $('#txtLat').val($("#txtLat1").val());
        $('#txtLong').val($("#txtLong1").val());
    }
    else {
        $('#divShippingAddr').show();
        $('#txtShipAdd').addClass('req');
        $("#divShippingAddr input[type='text']").val('');
        $("#divShippingAddr select").val(0);
        $('#txtShipAdd').val('');
        $('#txtShipAdd2').val('');
        $('#txtZipcode2').val('');
        $('#txtState2').val('');
        $('#txtCity2').val('');
        $('#txtLat').val('');
        $('#txtLong').val('');
    }

}

function getCityState(No, e) {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Sales/WebAPI/customerMaster.asmx/BindPinCodeForCity",
        dataType: "json",//$('option:selected', a).val();
        data: "{'ZipCode':'" + $('option:selected', e).val() + "'}",
        success: function (response) {
            var xmldoc = $.parseXML(response.d);

            var CityDetails = $(xmldoc).find('Table');
            $.each(CityDetails, function () {

                $('#txtState' + No).val($(this).find('StateName').text());
                $('#txtHstate' + No).val($(this).find('StateId').text());
                if (No == 1) {
                    $('#txtZone1').val($(this).find('ZoneName').text());
                }

                $("#txtCity" + No).val($(this).find("CityName").text());
                $("#txtHcity" + No).val($(this).find("CityId").text());
            });

        }
    });
}
function BindStateCity(No, e) {

    $('#txtState' + No).val('');
    $('#txtHstate' + No).val('');
    $("#txtCity" + No).val('');
    $("#txtHcity" + No).val('');
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Sales/WebAPI/customerMaster.asmx/BindStateCity",
        dataType: "json",//$('option:selected', a).val();
        data: "{'ZipCode':'" + $('option:selected', e).attr("zm") + "',CityId:'" + $('option:selected', e).attr("CityId") + "'}",
        success: function (response) {
            var xmldoc = $.parseXML(response.d);

            var CityDetails = $(xmldoc).find('Table');
            $.each(CityDetails, function () {

                $('#txtState' + No).val($(this).find('StateName').text());
                $('#txtHstate' + No).val($(this).find('StateId').text());
                if (No == 1) {
                    $('#txtZone1').val($(this).find('ZoneName').text());
                }

                $("#txtCity" + No).val($(this).find("CityName").text());
                $("#txtHcity" + No).val($(this).find("CityId").text());
            });

        }
    });
}
function CustcheckRequiredField() {
    var boolcheck = true;
    $('.req').each(function () {

        if ($(this).val() == '') {
            boolcheck = false;
            $(this).addClass('border-warning');

        } else {
            $(this).removeClass('border-warning');
        }
    });

    $('.ddlreq').each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == 'undefined' || $(this).val() == 'Select') {
            boolcheck = false;
            $(this).closest('div').find('.select2-selection--single').attr('style', 'border:1px solid #FF9149  !important');
        }
        else {
            $(this).removeClass('border-warning');
            $(this).closest('div').find('.select2-selection--single').removeAttr('style');
        }
    });
    return boolcheck;
}
function validateEmail(elementValue) {
    var EmailCodePattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var check = EmailCodePattern.test($(elementValue).val());
    if (!check) {
        toastr.error('Invalid email id.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $(elementValue).val('');
        $("#txtEmail").addClass('border-warning');
    }
    else {
        $("#txtEmail").removeClass('border-warning');
    }
    return;
}
function validateEmail1(elementValue) {
    var EmailCodePattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var check = EmailCodePattern.test($(elementValue).val());
    if (!check) {
        toastr.error('Invalid email id.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $(elementValue).val('');
        $("#txtAltEmail").addClass('border-warning');
    }
    else {
        $("#txtAltEmail").removeClass('border-warning');
    }
    return;
}
//$('#ddlCustType').change(function () {
function ShowlocationBind() {
    if ($('#ddlCustType').val() == 3) {
        $('#RowLocation').show();
        $('#ddlLocation').addClass('ddlreq');
        BindLocation();
    } else {
        $('#ddlLocation').val(0);
        $('#RowLocation').hide();
        $('#ddlLocation').removeClass('ddlreq');
    }
    priceLebelByCustemerType();
}

function BindLocation() {
    var Location = $('#Hd_Domain').val();

    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/customerMaster.asmx/BindLocation",
        data: "{'DataValues':'" + Location + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var pricelevel = $(xmldoc).find('Table');

                $("#ddlLocation option:not(:first)").remove();  //-------------------------------For Search Field
                $.each(pricelevel, function () {
                    $("#ddlLocation").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("Location").text() + "</option>");
                });
                $("#ddlLocation").select2();

            } else {
                location.href = '/';
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}

function priceLebelByCustemerType() {
    var CustemerTypeAutoId = $('#ddlCustType').val();

    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/customerMaster.asmx/BindpriceLevel",
        data: "{'DataValues':'" + CustemerTypeAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            if (response.d != "Session Expired") {
                var PriceLevelList = $.parseJSON(response.d);
                var ddlPriceLevel = $("#ddlPriceLevel");
                $("#ddlPriceLevel option:not(:first)").remove();
                if (getQueryString('PageId') == null)
                    $("#ddlPriceLevel").append("<option value = '-1' >Auto Generate Price Level</option >");
                for (var i = 0; i < PriceLevelList.length; i++) {
                    var PriceLevel = PriceLevelList[i];
                    if (getQueryString('customerId') == null)
                        var option = $("<option />");
                    option.html(PriceLevel.PL);
                    option.val(PriceLevel.AutoId);
                    ddlPriceLevel.append(option);
                }
                ddlPriceLevel.select2();
            } else {
                location.href = '/';
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
//----------------------------------------------------------------------------------Allow App Login-----------------------------------------------------------------------
function myFunction() {
    var x = document.getElementById("txtPassword");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}
$("#txtConfirmPassword").change(function () {
    if ($("#txtPassword").val() != $("#txtConfirmPassword").val()) {
        toastr.error('Password not matched.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        //$("#txtConfirmPassword").val('');//added on 11/30/2019 By Rizwan Ahmad
    }
})

$("#txtPassword").change(function () {//added on 11/30/2019 By Rizwan Ahmad
    if ($("#txtConfirmPassword").val() != $("#txtPassword").val()) {
        toastr.error('Password not matched.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        //$("#txtPassword").val('');
    }
})
function UserLog(e) {
    $("#txtUserId").val('');
    $("#txtPassword").val('');
    $("#txtConfirmPassword").val('');
    if ($(e).prop('checked') == true) {
        $('#txtUserId').addClass('req');
        $('#txtPassword').addClass('req');
        $('#txtConfirmPassword').addClass('req');
        if (UserName1 != "") {
            $("#txtUserId").val(UserName1);
        }
        if (Password != "") {
            $("#txtPassword").val(Password);
            $("#txtConfirmPassword").val(Password);
        }
    } else {
        $('#txtUserId').removeClass('req');
        $('#txtPassword').removeClass('req');
        $('#txtConfirmPassword').removeClass('req');
    }
}


function setData(place) {
    debugger
    $(".hiddenShipAddress").html(place.adr_address);
    var textdata = $(".hiddenShipAddress").find(".street-address").text();
    $("#txtShipAdd").val(textdata);
    var m = 0, check = 0;
    for (var i = 0; i < place.address_components.length; i++) {
        for (var j = 0; j < place.address_components[i].types.length; j++) {
            if (place.address_components[i].types[j] == "postal_code") {
                var zcode = place.address_components[i].long_name;
                $("#txtZipcode2").val(zcode);
            }
            else if (place.address_components[i].types[j] == "administrative_area_level_1") {
                var state = place.address_components[i].long_name;
                $("#txtState2").val(state);
            } else if (place.address_components[i].types[j] == "locality") {
                $("#txtCity2").val(place.address_components[i].long_name);
                city = place.address_components[i].long_name;
                check = 1;
            }
            if (place.address_components[i].types[j] == "neighborhood" && check == 0) {
                $("#txtCity2").val(place.address_components[i].long_name);
                city = place.address_components[i].long_name;
                check = 1;
            }
            else if (place.address_components[i].types[j] == "administrative_area_level_3" && check == 0) {
                $("#txtCity2").val(place.address_components[i].long_name);
                city = place.address_components[i].long_name;
            }

        }
        m++;
    }
    if (m > 4) {
        checkState(state, city, zcode);
        $("#txtLat").val(place.geometry.location.lat);
        $("#txtLong").val(place.geometry.location.lng);
    }
    else {
        swal("", "Invalid address !", "error");
        resetField('');
    }

}
function checkState(state, city, zcode) {
    if (state != undefined && city != undefined && zcode != undefined) {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/Sales/WebAPI/customerMaster.asmx/checkState",
            dataType: "json",
            data: "{'state':'" + state.trim() + "','city':'" + city.trim() + "','zcode':'" + zcode.trim() + "'}",
            success: function (response) {
                if (response.d == 'false') {
                    swal("", "Oops, Something went wrong. Please try later.", "warning");
                }
                else if (response.d == 'Invalidstate') {
                    swal("", "Invalid State (" + state + ')', "warning");
                    resetField('');
                }

            }
        })
    }
    else {
        swal("", "Invalid address.", "warning");
        resetField('');
    }
}
function getSelectedZip() {
    var autoid = $('input[name="selectedzip"]:checked').val();
    if ($('input[name="selectedzip"]:checked').length == 0) {
        toastr.error('Please select zipcode.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return false;
    } else {
        $("#ddlZipCode2").val(autoid).change();
        $("#ddlZipCode2").attr('disabled', true);
        $("#zipcodeselect").modal("hide");

    }

}
function hideclosemodal() {
    $("#zipcodeselect").modal("hide");
}

function hideclosemodal1() {
    $("#zipcodeselect1").modal("hide");
}
function resetField(val) {
    if (val == '') {
        $("#txtShipAdd").val('');
        $("#txtCity2").val('');
        $("#txtState2").val('');
        $("#txtLat").val('');
        $("#txtLong").val('');
        $("#txtZipcode2").val('');
    }
}


function setBillingData(place) {
    $(".hiddenBillAddress").html(place.adr_address);
    $("#txtBillAdd").val($(".hiddenBillAddress").find(".street-address").text());
    var m = 0, check = 0;
    for (var i = 0; i < place.address_components.length; i++) {
        for (var j = 0; j < place.address_components[i].types.length; j++) {
            if (place.address_components[i].types[j] == "postal_code") {
                var zcode = place.address_components[i].long_name;
                $("#txtZipcode1").val(zcode);
            } else if (place.address_components[i].types[j] == "locality") {
                var city = place.address_components[i].long_name;
                $("#txtCity1").val(city);
                check = 1;
            }
            else if (place.address_components[i].types[j] == "administrative_area_level_1") {
                var state = place.address_components[i].long_name;
                $("#txtState1").val(state);
            }

            if (place.address_components[i].types[j] == "neighborhood" && check == 0) {
                $("#txtCity1").val(place.address_components[i].long_name);
                city = place.address_components[i].long_name;
                check = 1;

            }
            if (place.address_components[i].types[j] == "administrative_area_level_3" && check == 0) {
                $("#txtCity1").val(place.address_components[i].long_name);
                city = place.address_components[i].long_name;
            }

        }
        m++;
    }
    if (m > 4) {
        checkBillingState(state, city, zcode);
        $("#txtLat1").val(place.geometry.location.lat);
        $("#txtLong1").val(place.geometry.location.lng);
    }
    else {
        swal("", "Invalid address !", "error");
        resetBillingField('');
    }

}

function checkBillingState(state, city, zcode) {
    if (state != undefined && city != undefined && zcode != undefined) {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/Sales/WebAPI/customerMaster.asmx/checkState",
            dataType: "json",
            data: "{'state':'" + state.trim() + "','city':'" + city.trim() + "','zcode':'" + zcode.trim() + "'}",
            success: function (response) {
                if (response.d == 'false') {
                    swal("", "Oops, Something went wrong. Please try later.", "warning");
                }
                else if (response.d == 'Invalidstate') {
                    swal("", "Invalid State (" + state + ')', "warning");
                    resetBillingField('');
                }
            }
        });
    }
    else {
        swal("", "Invalid address.", "warning");
        resetBillingField('');
    }
}

function getSelectedBillingZip() {
    var autoid = $('input[name="selectedzip"]:checked').val();
    if ($('input[name="selectedzip"]:checked').length == 0) {
        toastr.error('Please select zipcode.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return false;
    } else {
        $("#ddlZipCode1").val(autoid).change();
        $("#ddlZipCode1").attr('disabled', true);
        $("#zipcodeselect1").modal("hide");
    }
}

function resetBillingField(val) {
    if (val == '') {
        $("#txtBillAdd").val('');
        $("#txtCity1").val('');
        $("#txtState1").val('');
        $("#txtLat1").val('');
        $("#txtLong1").val('');
        $("#txtZipcode1").val('');
    }
}

function validateAddress(val) {
    if (val == 1) {
        $("#txtLat1").val('');
        $("#txtLong1").val('');
    } else if (val == 2) {
        $("#txtLat").val('');
        $("#txtLong").val('');
    }
}

function bindContactPersonType() {
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/customerMaster.asmx/bindContactPersonType",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },

        success: function (response) {
            console.log(response);
            if (response.d == 'Session Expired') {
                location.href = '/';
            } else if (response.d != 'false') {
                var xmldoc = $.parseXML(response.d);
                var ContactPersonType = $(xmldoc).find('Table');
                $("#ddlType option:not(:first)").remove();  //-------------------------------For Search Field
                $.each(ContactPersonType, function () {
                    $("#ddlType").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("TypeName").text() + "</option>");
                });
                $("#ddlType").select2();
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function addItemToList() {

    var IsDefault = 0;
    if ($("#chkIsDefault").prop("checked") == true) {
        IsDefault = "<input type='radio' name='default' class='radio border-primary' checked>";
        if ($("#tblContactDetails tbody tr").length > 0) {
            $("#tblContactDetails tbody tr").each(function (i) {
                if ($(this).find('.IsDefault input').prop('checked') == true) {
                    $(this).find('.IsDefault').html("<input name='default'  type='radio'  class='radio border-primary'>");
                }
            })
        }
    }
    else {
        IsDefault = "<input type='radio' name='default'  class='radio border-primary'>";
    }

    if (ContactPerson.length == 0) {
        $("#tblContact").val();
    }
    if (ContactcheckRequiredField()) {
        if ($("#txtLandline").val().length < 10) {
            toastr.error('Please enter a valid 10 digit Landline No.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            $("#txtLandline").addClass('border-warning');
            return;
        }
        else if ($("#txtLandline2").val() != "") {
            if ($("#txtLandline2").val().length < 10) {
                $("#txtLandline").removeClass('border-warning');
                toastr.error('Please enter a valid 10 digit Landline No.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
                $("#txtLandline2").addClass('border-warning');
                return;
            }
        }
        else if ($("#txtMobileNo").val().length < 10) {
            $("#txtLandline2").removeClass('border-warning');
            toastr.error('Please enter a valid 10 digit Mobile No.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            $("#txtMobileNo").addClass('border-warning');
            return;
        }

        if ($("#txtLandline").val() == $("#txtMobileNo").val()) {
            if ($("#txtLandline2").val() != '') {
                if ($("#txtLandline2").val() == $("#txtMobileNo").val()) {
                    $("#txtLandline").addClass('border-warning');
                    $("#txtLandline2").addClass('border-warning');
                    $("#txtMobileNo").addClass('border-warning');
                    toastr.error("Landline 1, Landline 2 and Mobile No can't be same.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    return;
                }
                else if ($("#txtLandline").val() == $("#txtMobileNo").val()) {
                    $("#txtLandline").addClass('border-warning');
                    $("#txtMobileNo").addClass('border-warning');
                    $("#txtLandline2").removeClass('border-warning');
                    toastr.error("Landline 1 and Mobile No can't be same.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    return;
                }
            }
            else {
                $("#txtLandline").addClass('border-warning');
                $("#txtMobileNo").addClass('border-warning');
                toastr.error("Landline 2 and Mobile No can't be same.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                return;
            }
        }
        if ($("#txtLandline2").val() != '') {
            if ($("#txtLandline").val() == $("#txtLandline2").val()) {
                $("#txtLandline").addClass('border-warning');
                $("#txtLandline2").addClass('border-warning');
                toastr.error("Landline 1 and Landline 2 can't be same.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                return;
            }
            else if ($("#txtLandline2").val() == $("#txtMobileNo").val()) {
                $("#txtLandline2").addClass('border-warning');
                $("#txtMobileNo").addClass('border-warning');
                toastr.error("Landline 2 and Mobile No can't be same.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                return;
            }
            else {
                $("#txtLandline").removeClass('border-warning');
                $("#txtLandline2").removeClass('border-warning');
                $("#txtMobileNo").removeClass('border-warning');
            }
        }
        else {
            $("#txtLandline").removeClass('border-warning');
            $("#txtLandline2").removeClass('border-warning');
            $("#txtMobileNo").removeClass('border-warning');
        }
        if ($("#txtAltEmail").val() != '') {
            if ($("#txtAltEmail").val() == $("#txtEmail").val()) {
                $("#txtAltEmail").addClass('border-warning');
                $("#txtEmail").addClass('border-warning');
                toastr.error("Email and Alternate Email can't be same.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                return;
            }
            else {
                $("#txtAltEmail").removeClass('border-warning');
                $("#txtEmail").removeClass('border-warning');
            }
        }
        if ($("#tbodyContactPerson tr").length == 0) {
            if (!$("#chkIsDefault").prop("checked")) {
                IsDefault = "<input type='radio' name='default' class='radio border-primary' checked>";
            }
        }
        var ContactPersonData = {
            "Type": $("#ddlType option:selected").text(),
            "TypeAutoId": $("#ddlType").val(),
            "ContactPerson": $("#txtContactPersonName").val().trim(),
            "Email": $("#txtEmail").val(),
            "AltEmail": $("#txtAltEmail").val(),
            "Landline": $("#txtLandline").val(),
            "Landline2": $("#txtLandline2").val(),
            "Mobile": $("#txtMobileNo").val(),
            "Fax": $("#txtFaxNo").val(),
            "IsDefault": IsDefault
        };
        ContactPerson = [];
        ContactPerson.push(ContactPersonData);
        contactPersonHtml();
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });

    }
}
function contactPersonHtml() {
    var i = 0;
    var trHtml = ``;
    ContactPerson.forEach(function (e) {
        trHtml += `<tr>
                    <td class="Action text-center"><a title="Delete" href="javascript:void(0)" onclick="deleterecord(this)"><span class="la la-remove"></span></a></td>
                    <td class="TypeAutoId text-center" style="display:none;">`+ e.TypeAutoId + `</td>
                    <td class="Type text-center">`+ e.Type + `</td>
                    <td class="ContactPerson text-center">`+ e.ContactPerson + `</td>
                    <td class="Mobile text-center">`+ e.Mobile + `</td>
                    <td class="Landline">`+ e.Landline + `</td>
                    <td class="Landline2">`+ e.Landline2 + `</td>
                    <td class="Fax">`+ e.Fax + `</td>
                    <td class="Email">`+ e.Email + `</td>
                    <td class="AltEmail">`+ e.AltEmail + `</td>
                    <td class="IsDefault text-center">`+ e.IsDefault + `</td>
                </tr>`;
        i++;
    });
    $("#tbodyContactPerson").append(trHtml);
    $("#tblContact").show();
    clearField();
}

function clearField() {
    $("#ddlType").val('0').change();
    $("#txtContactPersonName").val('');
    $("#txtEmail").val('');
    $("#txtAltEmail").val('');
    $("#txtLandline").val('');
    $("#txtLandline2").val('');
    $("#txtMobileNo").val('');
    $("#txtFaxNo").val('');
    $("#chkIsDefault").prop("checked", false);
}

function ContactcheckRequiredField() {
    var boolcheck = true;
    $('.creq').each(function () {

        if ($(this).val().trim() == '' || $(this).val().trim() == '.') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else if (parseFloat($(this).val()) <= 0) {
            boolcheck = false;
            $(this).addClass('border-warning');
        }
        else {
            $(this).removeClass('border-warning');
        }
    });

    $('.ddlcreq').each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == 'undefined' || $(this).val() == 'Select') {
            boolcheck = false;
            $(this).closest('div').find('.select2-selection--single').attr('style', 'border:1px solid #FF9149  !important');
        } else {
            $(this).removeClass('border-warning');
            $(this).closest('div').find('.select2-selection--single').removeAttr('style');
        }
    });
    if ($("#txtLandline").val().length < 10) {
        $("#txtLandline").addClass('border-warning');
    }
    if ($("#txtLandline2").val() != "") {
        if ($("#txtLandline2").val().length < 10) {
            $("#txtLandline2").addClass('border-warning');
        }
    }
    if ($("#txtMobileNo").val().length < 10) {
        $("#txtMobileNo").addClass('border-warning');
    }
    if ($("#txtLandline").val() == $("#txtMobileNo").val()) {
        if ($("#txtLandline2").val() != '') {
            if ($("#txtLandline2").val() == $("#txtMobileNo").val()) {
                $("#txtLandline").addClass('border-warning');
                $("#txtLandline2").addClass('border-warning');
                $("#txtMobileNo").addClass('border-warning');
            }
            else {
                $("#txtLandline").addClass('border-warning');
                $("#txtLandline2").addClass('border-warning');
            }
        }
        else {
            $("#txtLandline").addClass('border-warning');
            $("#txtMobileNo").addClass('border-warning');
        }
    }
    if ($("#txtLandline2").val() != '') {
        if ($("#txtLandline").val() == $("#txtLandline2").val()) {
            $("#txtLandline").addClass('border-warning');
            $("#txtLandline2").addClass('border-warning');
        }
        else if ($("#txtLandline2").val() == $("#txtMobileNo").val()) {
            $("#txtLandline2").addClass('border-warning');
            $("#txtMobileNo").addClass('border-warning');
        }
    }
    if ($("#txtAltEmail").val() != '') {
        if ($("#txtAltEmail").val() == $("#txtEmail").val()) {
            $("#txtAltEmail").addClass('border-warning');
            $("#txtEmail").addClass('border-warning');
        }
        else {
            $("#txtAltEmail").removeClass('border-warning');
            $("#txtEmail").removeClass('border-warning');
        }
    }
    if ($("#txtLandline").val().length == 10 && $("#txtLandline2").val() == 10 && $("#txtMobileNo").val().length == 10) {
        $("#txtLandline").removeClass('border-warning');
        $("#txtLandline2").removeClass('border-warning');
        $("#txtMobileNo").removeClass('border-warning');
    }
    return boolcheck;
}

function deleterecord(e) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this contact person",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, delete it!",
                value: true,
                visible: true,
                className: "",
                closeModal: true
            }
        }
    }).then(isConfirm => {
        if (isConfirm) {
            swal("", "Contact person removed from list", "error");
            $(e).closest('tr').remove();
        } else {
            swal("", "Your data is safe.", "error");


        }
    })
}
function checkLength(num) {

    if (num == 1) {
        if ($("#txtLandline").val().length < 10) {
            toastr.error('Please enter a valid 10 digit Landline No.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            $("#txtLandline").addClass('border-warning');
            return;
        }
        else {
            $("#txtLandline").removeClass('border-warning');
        }
    }
    else if (num == 2 && $("#txtLandline2").val() != "") {
        if ($("#txtLandline2").val().length < 10) {
            toastr.error('Please enter a valid 10 digit Landline No.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            $("#txtLandline2").addClass('border-warning');
            return;
        }
        else {
            $("#txtLandline2").removeClass('border-warning');
        }
    }
    else if (num == 3) {
        if ($("#txtMobileNo").val().length < 10) {
            toastr.error('Please enter a valid 10 digit Mobile No.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            $("#txtMobileNo").addClass('border-warning');
            return;
        }
        else {
            $("#txtMobileNo").removeClass('border-warning');
        }
    }
    else {
        $("#txtLandline").removeClass('border-warning');
        $("#txtLandline2").removeClass('border-warning');
        $("#txtMobileNo").removeClass('border-warning');
    }
}