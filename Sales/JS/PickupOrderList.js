﻿

function Pagevalue(e) {
    getOrderList(parseInt($(e).attr("page")));
};

function bindStatus() {
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/WShipVia.asmx/bindStatus",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var status = $(xmldoc).find("Table");

            $("#ddlSStatus option:not(:first)").remove();
            $.each(status, function () {
                $("#ddlSStatus").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("StatusType").text() + "</option>");
            });
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function getOrderList(pageIndex,ShipVia) {
    
    var data = {
        OrderNo: $("#txtSOrderNo").val(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        Status: $("#ddlSStatus").val(),
        ShippingType: ShipVia,
        pageIndex: pageIndex
    };

    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/WShipVia.asmx/getOrderList",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var orderList = $(xmldoc).find("Table1");

                $("#tblOrderList tbody tr").remove();
                var row = $("#tblOrderList thead tr").clone(true);

                if (orderList.length > 0) {
                    $("#EmptyTable").hide();
                    $.each(orderList, function () {
                        $(".orderNo", row).html("<span OrderAutoId='" + $(this).find("AutoId").text() + "'>" + $(this).find("OrderNo").text() + "</span>");
                        $(".orderDt", row).text($(this).find("OrderDate").text());
                        $(".cust", row).text($(this).find("CustomerName").text());
                        $(".value", row).text($(this).find("GrandTotal").text());
                        $(".product", row).text($(this).find("NoOfItems").text());

                        if (Number($(this).find("StatusCode").text()) == 1) {
                            $(".status", row).html("<span class='label label-info'>" + $(this).find("Status").text() + "</span>");
                        } else if (Number($(this).find("StatusCode").text()) == 2) {
                            $(".status", row).html("<span class='label label-primary'>" + $(this).find("Status").text() + "</span>");
                        } else if (Number($(this).find("StatusCode").text()) == 3) {
                            $(".status", row).html("<span class='StatusPacked'>" + $(this).find("Status").text() + "</span>");
                        } else if (Number($(this).find("StatusCode").text()) == 4) {
                            $(".status", row).html("<span class='StatusRTS'>" + $(this).find("Status").text() + "</span>");
                        } else if (Number($(this).find("StatusCode").text()) == 5) {
                            $(".status", row).html("<span class='label label-warning'>" + $(this).find("Status").text() + "</span>");
                        } else if (Number($(this).find("StatusCode").text()) == 6) {
                            $(".status", row).html("<span class='label label-success'>" + $(this).find("Status").text() + "</span>");
                        } else if (Number($(this).find("StatusCode").text()) == 7) {
                            $(".status", row).html("<span class='label label-danger'>" + $(this).find("Status").text() + "</span>");
                        } else if (Number($(this).find("StatusCode").text()) == 8) {
                            $(".status", row).html("<span class='label label-default'>" + $(this).find("Status").text() + "</span>");
                        }

                        if ($(this).find("Status").text() != 'New' || Number($("#hiddenEmpType").val()) > 2) {
                            if (Number($("#hiddenEmpType").val()) == 3 && (Number($(this).find("StatusCode").text()) <= 2)) {
                                $(".action", row).html("<b><a href='/Sales/orderMaster.aspx?OrderNo=" + $(this).find("OrderNo").text() + "'>View</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href='javascript:;' onclick='print_NewOrder(this)'>Print</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href='javascript:;' onclick='viewOrderLog(" + $(this).find("AutoId").text() + ")'>Log</a></b>");
                            } else {
                                //alert($("#hiddenEmpType").val())
                                if (Number($("#hiddenEmpType").val()) == 4 && (Number($(this).find("StatusCode").text()) <= 3)) {
                                    $(".action", row).html("<b><a href='/Sales/orderMaster.aspx?OrderNo=" + $(this).find("OrderNo").text() + "'>View</a>&nbsp;&nbsp;|<a href='javascript:;'><span class='glyphicon glyphicon-remove' onclick='deleteOrder(\"" + $(this).find("AutoId").text() + "\")'></span><b></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href='javascript:;' onclick='viewOrderLog(" + $(this).find("AutoId").text() + ")'>Log</a></b>");

                                } else {
                                    $(".action", row).html("<b><a href='/Sales/orderMaster.aspx?OrderNo=" + $(this).find("OrderNo").text() + "'>View</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href='javascript:;' onclick='viewOrderLog(" + $(this).find("AutoId").text() + ")'>Log</a></b>");
                                }
                            }
                        } else {

                            $(".action", row).html("<a href='/Sales/orderMaster.aspx?OrderNo=" + $(this).find("OrderNo").text() + "'><span class='glyphicon glyphicon-edit'></span></a><a href='javascript:;'><span class='glyphicon glyphicon-remove' onclick='deleteOrder(\"" + $(this).find("AutoId").text() + "\")'></span><b></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href='javascript:;' onclick='viewOrderLog(" + $(this).find("AutoId").text() + ")'>Log</a></b>");
                        }
                        $("#tblOrderList tbody").append(row);
                        row = $("#tblOrderList tbody tr:last").clone(true);
                    });
                } else {
                    $("#EmptyTable").show();
                }

                var pager = $(xmldoc).find("Table");
                $(".Pager").ASPSnippets_Pager({
                    ActiveCssClass: "current",
                    PagerCssClass: "pager",
                    PageIndex: parseInt(pager.find("PageIndex").text()),
                    PageSize: parseInt(pager.find("PageSize").text()),
                    RecordCount: parseInt(pager.find("RecordCount").text())
                });
            } else {
                location.href = "/";
            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function print_NewOrder(e) {
    window.open("/Packer/OrderPrint.html?OrderAutoId=" + $(e).closest("tr").find(".orderNo span").attr("OrderAutoId"), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    setTimeout(function () {
        getOrderList(1);
    }, 2000);
}

$("#btnSearch").click(function () {
    getOrderList(1,5);
})
$("#btnCSearch").click(function () {
    getOrderList(1, 6);
})
function deleteOrder(orderAutoId) {
    if (confirm('Are you sure you want to delete this entry from list?')) {
        $.ajax({
            type: "POST",
            url: "/Sales/WebAPI/WShipVia.asmx/deleteOrder",
            data: "{'OrderAutoId':" + orderAutoId + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                $("#alertSuccessDelete").show();
                $("#alertSuccessDelete span").text("Order deleted Successfully!!");
                $(".close").click(function () {
                    $("#alertSuccessDelete").hide();
                })
                getOrderList(1);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
}

function viewOrderLog(OrderAutoId) {
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/WOrderLog.asmx/viewOrderLog",
        data: "{'OrderAutoId':" + OrderAutoId + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var orderlog = $(xmldoc).find("Table");
            var order = $(xmldoc).find("Table1");

            $("#lblOrderNo").text(order.find("OrderNo").text());
            $("#lblOrderDate").text(order.find("OrderDate").text());

            $("#tblOrderLog tbody tr").remove();
            var row = $("#tblOrderLog thead tr").clone(true);
            if (orderlog.length > 0) {
                $("#EmptyTable").hide();
                $.each(orderlog, function (index) {
                    $(".SrNo", row).text(index + 1);
                    $(".ActionBy", row).text($(this).find("EmpName").text());
                    $(".Date", row).text($(this).find("ActionDate").text());
                    $(".Action", row).text($(this).find("Action").text());
                    $(".Remark", row).text($(this).find("Remarks").text());

                    $("#tblOrderLog tbody").append(row);
                    row = $("#tblOrderLog tbody tr:last").clone(true);
                });
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
    $("#modalOrderLog").modal('show');
}