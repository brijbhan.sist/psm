﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/MasterPage.master" AutoEventWireup="true" CodeFile="ViewCustomerDetails.aspx.cs" Inherits="Sales_ViewCustomerDetails" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .nav-tabs {
            border-bottom: 1px solid #ddd;
        }

        #Table4 tr td {
            white-space: normal;
        }

        #tblduePayment tr td {
            white-space: normal;
        }

        .input-group-text {
            padding: 0 1rem;
        }

        .la-arrow-up {
            cursor: pointer;
        }

        .la-arrow-down {
            cursor: pointer;
        }

        .pac-container {
            z-index: 10000 !important;
        }

        table tbody td {
            padding: 3px !important;
        }

        .table th, .table td {
            padding: 3px;
        }

        table thead td {
            padding: 5px;
        }

        .value-lbl {
            float: left;
            margin-right: 30px;
        }

            .value-lbl label {
                float: left;
                margin-right: 10px;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input type="hidden" id="OrderAutoId" />
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Customer Details</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="/Sales/NewCustomerList.aspx">Customer List</a>
                        </li>
                        <li class="breadcrumb-item">Customer Details
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a href="/Sales/NewCustomerList.aspx" class="dropdown-item">Go to Customer List</a>
                    <input type="hidden" id="hiddenEmpTypeVal" runat="server" />
                    <%
                        if (Session["EmpType"] != null)
                        {
                            if (Session["EmpType"].ToString() != "Driver" && Session["EmpType"].ToString() != "Packer" && Session["EmpType"].ToString() != "Warehouse Manager")
                            {
                    %>
                    <a id="EditCustomer" class="dropdown-item">Edit</a>
                    <%
                            }
                        }%>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" value="" id="hiddenCustomerAutoId" />
    <div class="content-body">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body" style="padding-bottom: 0.5rem !important">
                                <div class="row">
                                    <div class="col-2">
                                        <label class="control-label"><b>Customer ID :</b></label>
                                    </div>
                                    <div class="col-2">
                                        <span id="txtCustomerId"></span>
                                    </div>
                                    <div class="col-2">
                                        <label class="control-label">
                                            <b>Customer Name : </b>
                                        </label>
                                    </div>
                                    <div class="col-2">
                                        <span id="txtCustomerName"></span>
                                    </div>
                                    <div class="col-2">
                                        <label class="control-label"><b>Status :</b></label>
                                    </div>
                                    <div class="col-2">
                                        <span id="lblStatus" class='badge badge badge-pill'></span>
                                    </div>
                                    <div class="col-2">
                                        <label class="control-label">
                                            <b>Price level : </b>
                                        </label>
                                    </div>
                                    <div class="col-2">
                                        <span id="txtPricelevel"></span>
                                    </div>
                                    <div class="col-2">
                                        <label class="control-label">
                                            <b>Store Contact Person :  </b>
                                        </label>
                                    </div>
                                    <div class="col-2">
                                        <span id="spanContactPerson"></span>
                                    </div>

                                    <div class="col-2">
                                        <label class="control-label">
                                            <b>Customer Type :</b>
                                        </label>
                                    </div>
                                    <div class="col-2">
                                        <span id="spanCustomerType"></span>
                                    </div>
                                    <div class="col-2">
                                        <label class="control-label">
                                            <b>Store Credit :</b>
                                        </label>
                                    </div>
                                    <div class="col-2">
                                        <span id="CreditAmount" class="badge badge badge-pill badge-success mr-2" style="text-transform: capitalize;"></span>
                                    </div>
                                    <div class="col-2">
                                        <label class="control-label">
                                            <b>Sales Person :</b>
                                        </label>
                                    </div>
                                    <div class="col-2">
                                        <span id="spanSalesPerson" style="text-transform: capitalize;"></span>
                                    </div>
                                    <div class="col-2">
                                        <label class="control-label">
                                            <b>Due Amount :</b>
                                        </label>
                                    </div>
                                    <div class="col-2">
                                        <span id="TotalDueAmount" class="badge badge badge-pill badge-danger mr-2" style="text-transform: capitalize;">0.00</span>
                                    </div>
                                </div>
                                <div class="row form-group" style="display: none">
                                    <div class="col-2">
                                        <label class="control-label">
                                            <b>Created By :</b>
                                        </label>
                                    </div>
                                    <div class="col-2">
                                        <span id="createdBy" style="text-transform: capitalize;"></span>
                                    </div>
                                    <div class="col-2">
                                        <label class="control-label">
                                            <b>Last Updated By :</b>
                                        </label>
                                    </div>
                                    <div class="col-2">
                                        <span id="updatedBy" style="text-transform: capitalize;"></span>
                                    </div>
                                    <div class="col-2">
                                        <label class="control-label">
                                            <b>Created On :</b>
                                        </label>
                                    </div>
                                    <div class="col-2">
                                        <span id="createdOn" style="text-transform: capitalize;"></span>
                                    </div>
                                    <div class="col-2">
                                        <label class="control-label">
                                            <b>Last Updated On :</b>
                                        </label>
                                        <span id="updatedOn" style="text-transform: capitalize;"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="content-body">
        <section id="drag-area1">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body" id="navbar">
                                <div class="row">
                                    <div class="col-md-12 col-sm12">
                                        <ul class="nav nav-tabs nav-topline">
                                            <li class="nav-item">
                                                <a class="nav-link active" id="base-OrderList" data-toggle="tab" aria-controls="tab21" href="#OrderList" aria-expanded="true">Order List</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="base-CreditMemo" data-toggle="tab" aria-controls="tab22" href="#CreditMemo" onclick="CustomerCreditMemoList(1)" aria-expanded="false">Credit Memo</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="base-PaymentHistory" data-toggle="tab" aria-controls="tab23" href="#PaymentHistory" onclick="CustomerPaymentList(1)" aria-expanded="false">Payment History</a>
                                            </li>

                                            <li class="nav-item">
                                                <a class="nav-link" id="base-DueOrderList" data-toggle="tab" aria-controls="tab21" href="#DueOrderList" onclick="CustomerDuePayment()" aria-expanded="true">Due Payment</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="base-CollectionDetails" data-toggle="tab" aria-controls="tab22" href="#CollectionDetails" onclick="CollectionDetails(1)" aria-expanded="false">Collection Details</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="base-CustomerLog" data-toggle="tab" aria-controls="tab23" href="#CustomerLog" onclick="CustomerLogReport(1)" aria-expanded="false">Customer Log</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="base-CustomerDocuments" data-toggle="tab" aria-controls="tab23" href="#CustomerDocuments" onclick="CustomerDocument();bindCustomerdocumnets()" aria-expanded="false" style="font-weight">Documents</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="base-StoreCreditlog" data-toggle="tab" aria-controls="tab23" href="#StoreCreditlog" onclick="StoreCreditLog(1)" aria-expanded="false">Store Credit </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="base-BankDetails" data-toggle="tab" aria-controls="tab23" href="#BankDetails" onclick="GetBankDetailsList()" aria-expanded="false">Bank Details</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="base-ContactDetails" data-toggle="tab" aria-controls="tab23" href="#ContactDetails" aria-expanded="false" onclick="GetContactPersonList();">Contacts</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="base-AddressDetails" data-toggle="tab" aria-controls="tab23" href="#AddressDetails" onclick="CustomerBillingDetails();CustomerShippingDetails();" aria-expanded="false">Address Info</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tab-content px-1 pt-1 border-grey border-lighten-2 border-0-top">
                                    <div role="tabpanel" class="row tab-pane active" id="OrderList" aria-expanded="true" aria-labelledby="base-OrderList">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered" id="Table1">
                                                        <thead class="bg-blue white">
                                                            <tr>
                                                                <td class="OrderNo text-center">Order No</td>
                                                                <td class="OrderType text-center" style="display: none">Order Type</td>
                                                                <td class="Date text-center">Date</td>
                                                                <td class="Status text-center">Status</td>
                                                                <td class="OrderItems text-center">Items</td>
                                                                <td class="OrderAmount price">Payable Amount</td>
                                                                <td class="PaidAmount  price">Paid Amount</td>
                                                                <td class="DueAmount price">Due Amount</td>
                                                                <td class="DeliveryDate text-center">Delivery Date</td>
                                                                <td class="SalesMan text-center">Sales Person</td>
                                                                <td class="Packer text-center">Packer</td>
                                                                <td class="SalesManager text-center">Sales Manager</td>
                                                                <td class="Account text-center">Account</td>
                                                                <td class="ShippingType text-center">Shipping Type</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr style="font-weight: bold">
                                                                <td colspan="4" class="text-right">Total</td>
                                                                <td id="OrderListTotal" style="text-align: right">0.00</td>
                                                                <td id="OrderListPaid" style="text-align: right">0.00</td>
                                                                <td id="OrderListDue" style="text-align: right">0.00</td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                                <%-- <div class="Pager" id="OrdL"></div>--%>
                                            </div>

                                        </div>
                                        <br />
                                        <div class="d-flex">
                                            <div class="form-group">
                                                <select class="form-control border-primary input-sm" id="ddlPageSize" onchange="CustomerOrderList(1);">
                                                    <option value="10">10</option>
                                                    <option value="50">50</option>
                                                    <option value="100">100</option>
                                                    <option value="500">500</option>
                                                    <option value="1000">1000</option>
                                                    <option value="0">All</option>
                                                </select>
                                            </div>
                                            <div class="ml-auto">
                                                <div class="Pager" id="OrdL"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row tab-pane" id="CreditMemo" aria-labelledby="base-CreditMemo">
                                        <div class="row form-group">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered" id="Table2">
                                                        <thead class="bg-blue white">
                                                            <tr>
                                                                <td class="Sr"></td>
                                                                <td class="action text-center">Action</td>
                                                                <td class="CreditNo text-center">Credit Memo No</td>
                                                                <td class="CreditType text-center">Credit Type</td>
                                                                <td class="CreditDate text-center">Date</td>
                                                                <td class="ApplyDate text-center">Apply Date</td>
                                                                <td class="StatusType text-center">Status</td>
                                                                <td class="SalesAmount price">Amount</td>
                                                                <td class="TotalAmount price">Approved Amount</td>
                                                                <td class="AppliedOrder text-center">Applied Order No</td>
                                                                <td class="CreatedBy text-center">Created By</td>
                                                                <td class="ApprovedBy text-center">Approved By</td>
                                                                <td class="SettledBy text-center">Settled by</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr style="font-weight: bold">
                                                                <td colspan="7" id="CreditList" class="text-center">Total</td>
                                                                <td id="CreditListAmount" style="text-align: right">0.00</td>
                                                                <td id="CreditListTotalAmount" style="text-align: right">0.00</td>
                                                                <td colspan="4"></td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="d-flex">
                                            <div class="form-group">
                                                <select class="form-control border-primary input-sm" id="ddlPageCMSize" onchange="CustomerCreditMemoList(1);">
                                                    <option value="10">10</option>
                                                    <option value="50">50</option>
                                                    <option value="100">100</option>
                                                    <option value="500">500</option>
                                                    <option value="1000">1000</option>
                                                    <option value="0">All</option>
                                                </select>
                                            </div>
                                            <div class="ml-auto">
                                                <div class="Pager" id="CRList"></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1  btn-sm" id="btnSettleCredit" onclick="SettleCreditAmount()">
                                                        Settle</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row tab-pane" id="PaymentHistory" aria-labelledby="base-PaymentHistory">
                                        <div class="row form-group">
                                            <div class="col-md-3 col-sm-3">
                                                <div class="form-group">
                                                    <input type="text" class="form-control border-primary input-sm" placeholder="Payment Id" id="txtSPaymentId" />
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-3">
                                                <div class="form-group">
                                                    <select id="ddlPStatus" class="form-control border-primary input-sm">
                                                        <option value="-1">All</option>
                                                        <option value="0">Settled</option>
                                                        <option value="1">Cancelled</option>
                                                    </select>
                                                </div>
                                            </div>


                                            <div class="col-md-3 col-sm-3">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                <span class="la la-calendar-o"></span>
                                                            </span>
                                                        </div>
                                                        <input type="text" class="form-control border-primary input-sm date" placeholder="From Date" id="txtSFromDate" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                <span class="la la-calendar-o"></span>
                                                            </span>
                                                        </div>
                                                        <input type="text" class="form-control border-primary input-sm date" placeholder="To Date" id="txtSToDate" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <div class="form-group">
                                                    <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1  btn-sm" id="Button1" onclick="PaymentCustomerList()">Search</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered" id="Table3">
                                                        <thead class="bg-blue white">
                                                            <tr>
                                                                <td class="Action text-center width3per">Action</td>
                                                                <td class="PaymentId text-center width4per">Payment ID</td>
                                                                <td class="PaymentDate text-center width4per">Payment Date</td>
                                                                <td class="PaymentMode text-center width4per">Payment Mode</td>
                                                                <td class="TotalOrders text-center width4per">Total Orders</td>
                                                                <td class="Status text-center width3per">Status</td>
                                                                <td class="ReceivedEmpName">Received Payment by</td>
                                                                <td class="TotalPaidAmount price width4per">Total Paid Amount</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr style="font-weight: bold">
                                                                <td colspan="7" class="text-right" style="font-weight: bold !important">Total</td>
                                                                <td id="Table3Total" style="text-align: right; font-weight: bold !important">0.00</td>

                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>

                                            </div>
                                        </div>
                                        <br />
                                        <div class="d-flex">
                                            <div class="form-group">
                                                <select class="form-control border-primary input-sm" id="ddlPageSizeInPayHis" onchange="PaymentCustomerList();">
                                                    <option value="10">10</option>
                                                    <option value="50">50</option>
                                                    <option value="100">100</option>
                                                    <option value="500">500</option>
                                                    <option value="1000">1000</option>
                                                    <option value="0">All</option>
                                                </select>
                                            </div>
                                            <div class="ml-auto">
                                                <div class="Pager" id="PayHis"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row tab-pane" id="DueOrderList" aria-labelledby="base-DueOrderList">
                                        <div id="pay">
                                            <div class="row form-group">
                                                <div class="col-md-3 col-sm-3">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control border-primary input-sm" placeholder="Order No" id="txtSCustomerId" />
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-3">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">From Order Date <span class="la la-calendar-o"></span>
                                                                </span>
                                                            </div>
                                                            <input type="text" class="form-control border-primary input-sm date" placeholder="From Date" id="txtFromDate" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-3">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">To Order Date <span class="la la-calendar-o"></span>
                                                                </span>
                                                            </div>
                                                            <input type="text" class="form-control border-primary input-sm date" placeholder="To Date" id="txtToDate" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-sm-3">
                                                    <div class="form-group">
                                                        <button type="button" class="btn btn-primary buttonAnimation round box-shadow-1  btn-sm" id="btnSearch" onclick="CustomerDuePaymentSearch()">Search</button>
                                                        <button type="button" class="btn btn-success buttonAnimation round box-shadow-1  btn-sm" id="btnPayNow1" style="display: none" onclick="PayNow()">Pay Now</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="display: none" class="paynow">
                                            <div class="row">
                                                <div class="col-md-3 col-sm-2">
                                                    <label class="control-label">
                                                        Payment ID :
                                                    </label>
                                                </div>
                                                <div class="col-md-3 col-sm-2">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control border-primary input-sm" disabled placeholder="Payment Id" id="txtPaymentId" />
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-2">
                                                    <label class="control-label">
                                                        <span style="white-space: nowrap">Payment Method 
                                                    </label>
                                                </div>
                                                <div class="col-md-3 col-sm-2">
                                                    <div class="form-group">
                                                        <select class="form-control border-primary input-sm Save" id="ddlPaymentMenthod" runat="server">
                                                            <option value="0">-Select Payment Mode-</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 col-sm-2">
                                                    <label class="control-label">
                                                        <span style="white-space: nowrap">Reference No :</span>
                                                    </label>
                                                </div>
                                                <div class="col-md-3 col-sm-2">
                                                    <div class="form-group">
                                                        <input type="text" id="txtReferenceNo" maxlength="20" class="form-control border-primary input-sm" />
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-2">
                                                    <label class="control-label">
                                                        <span style="white-space: nowrap">Received Payment By <span class="required">*</span>
                                                    </label>
                                                </div>
                                                <div class="col-md-3 col-sm-2">
                                                    <div class="form-group">
                                                        <select class="form-control border-primary input-sm Save" id="ddlReceivedPaymentBy" runat="server">
                                                            <option value="0">-Select-</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 col-sm-2">
                                                    <label class="control-label">
                                                        <span style="white-space: nowrap">Receive Payment Amount <span class="required">*</span>
                                                    </label>
                                                </div>
                                                <div class="col-md-3 col-sm-2">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control border-primary input-sm Save" runat="server" placeholder="0.00" id="txtReceiveAmount" style="text-align: right" onkeyup="ReceivedPay()" onkeypress="return isNumberDecimalKey(event,this)" />
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-2">
                                                    <label class="control-label">
                                                        <span style="white-space: nowrap">Store Credit :</span>
                                                    </label>
                                                </div>
                                                <div class="col-md-3 col-sm-2">
                                                    <div class="form-group">
                                                        <input type="text" id="txtStoreCredit" class="form-control border-primary input-sm" readonly="true" value="0.00" style="text-align: right" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 col-sm-2">
                                                    <label class="control-label">
                                                        <span style="white-space: nowrap">Payment Remark :</span>
                                                    </label>
                                                </div>
                                                <div class="col-md-3 col-sm-2">
                                                    <div class="form-group">
                                                        <textarea id="txtPaymentRemarks" class="form-control border-primary input-sm"></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-2">
                                                    <label class="control-label">
                                                        <span style="white-space: nowrap">Account Remark :</span>
                                                    </label>
                                                </div>
                                                <div class="col-md-3 col-sm-2">
                                                    <div class="form-group">
                                                        <textarea id="txtEmployeeRemarks" class="form-control border-primary input-sm"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 col-sm-2">
                                                    <label class="control-label">
                                                        <b style="white-space: nowrap">Total Store Credit :</b>
                                                    </label>
                                                </div>
                                                <div class="col-md-3 col-sm-2">
                                                    <div class="form-group">
                                                        <input type="text" id="txtTotalStoreCredit" class="form-control border-primary input-sm" readonly="true" value="0.00" style="text-align: right" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered" id="Table4">
                                                        <thead class="bg-blue white">
                                                            <tr>
                                                                <td class="OrderNo text-center">Order No</td>
                                                                <td class="OrderType text-center">Order Type</td>
                                                                <td class="OrderDate text-center">Order Date</td>
                                                                <td class="DaysOld text-center">Days</td>
                                                                <td class="OrderAmount" style="text-align: right">Order Amount</td>
                                                                <td class="PaidAmount" style="text-align: right">Paid Amount</td>
                                                                <td class="DueAmount" style="text-align: right">Due Amount</td>
                                                                <td class="CheckBoc paynow text-center" style="display: none;">Select Check Box</td>
                                                                <td class="PayAmount paynow" style="display: none; text-align: right">Pay Amount</td>
                                                                <td class="payDueAmount paynow" style="display: none; text-align: right">Due Balance</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr style="font-weight: bold">
                                                                <td colspan="4">Total</td>
                                                                <td id="TotalOrderAmount" style="text-align: right; font-weight: bold">0.00</td>
                                                                <td id="TotalPaidAmount" style="text-align: right; font-weight: bold">0.00</td>
                                                                <td id="DueAmount" style="text-align: right; font-weight: bold">0.00</td>
                                                                <td id="Td3" class="paynow" style="display: none; text-align: right"></td>
                                                                <td id="TotalPayAmount" class="paynow" style="display: none; text-align: right; font-weight: bold">0.00</td>
                                                                <td id="TotalpayDueAmount" class="paynow" style="display: none; text-align: right; font-weight: bold">0.00</td>

                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row DuehideCurrency" style="display: none">
                                            <div class="col-md-6">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered" id="tblCurrencyList">
                                                        <thead class="bg-blue white">
                                                            <tr>
                                                                <td class="CurrencyName text-center">Bill</td>
                                                                <td class="NoofRupee text-center" style="width: 150px;">Total Count</td>
                                                                <td class="Total" style="text-align: right; width: 150px">Total Amount</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr style="font-weight: bold">
                                                                <td colspan="2" style="text-align: right">Total</td>
                                                                <td id="TotalAmount" style="text-align: right; font-weight: bold">0.00</td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12">
                                                        <label class="control-label">
                                                            <span style="white-space: nowrap">Short Amount :</span>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-4 col-sm-12">
                                                        <div class="form-group">
                                                            <input type="text" id="txtSortAmount" readonly="true" onkeypress="return isNumberDecimalKey(event,this)" class="form-control border-primary input-sm" value="0.00" style="text-align: right" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-2">
                                                        <label class="control-label">
                                                            <span style="white-space: nowrap">Total Amount :</span>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-4 col-sm-4">
                                                        <div class="form-group">
                                                            <input type="text" id="txtTotalCurrencyAmount" readonly="true" onkeypress="return isNumberDecimalKey(event,this)"
                                                                class="form-control border-primary input-sm" value="0.00" style="text-align: right; font-weight: bold; font-size: 17px;" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-12 col-sm-12 full-right">
                                                <button type="button" class="btn btn-danger buttonAnimation round box-shadow-1  btn-sm pull-right" id="btnCancel" onclick="PayCancel()" style="display: none">Cancel</button>
                                                <button type="button" class="btn btn-warning buttonAnimation round box-shadow-1  btn-sm pull-right" id="btnpaymentCancel" onclick="paymentCancellation()" style="display: none; margin-right: 10px;">Cancel Payment</button>
                                                <button type="button" id="btnSubmit" onclick="SavePayment()" class="btn btn-success buttonAnimation round box-shadow-1  btn-sm pull-right paynow" style="display: none; margin-right: 10px;">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row tab-pane" id="CollectionDetails" aria-labelledby="base-CollectionDetails">
                                        <div class="row form-group">
                                            <div class="col-md-3 col-sm-3">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                <span class="la la-calendar-o"></span>
                                                            </span>
                                                        </div>
                                                        <input type="text" class="form-control border-primary input-sm date" placeholder="From Date" id="FromDate" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                <span class="la la-calendar-o"></span>
                                                            </span>
                                                        </div>
                                                        <input type="text" class="form-control border-primary input-sm date" placeholder="To Date" id="ToDate" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-3 col-sm-3">
                                                <div class="form-group">
                                                    <select class="form-control border-primary input-sm" id="ddlStatus">
                                                        <option value="0">All</option>
                                                        <option value="1">New</option>
                                                        <option value="2">Settled</option>
                                                        <option value="3">Cancelled</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-9 col-sm-9">
                                                <div class="form-group">
                                                    <button type="button" class="btn btn-primary buttonAnimation round box-shadow-1  btn-sm" id="Button4" onclick="CollectionDetailsSearch()">Search</button>

                                                    <%
                                                        if (Session["EmpTypeNo"] != null)
                                                        {
                                                            if (Session["EmpTypeNo"].ToString() == "6")
                                                            {
                                                    %>

                                                    <button type="button" class="btn btn-warning buttonAnimation round box-shadow-1  btn-sm" id="btnProcess" onclick="ProcessPayment()" style="display: none">Process</button>
                                                    <button type="button" class="btn btn-success buttonAnimation round box-shadow-1  btn-sm" id="btnPaywithCredit" style="display: none;" onclick="ProcessStoreCredit()">Pay with Store Credit</button>
                                                    <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" id="btnOnHold" style="display: none" onclick="OnHoldCheck()">On Hold</button>
                                                    <button type="button" class="btn btn-danger buttonAnimation round box-shadow-1  btn-sm" id="btnCancelCheck" style="display: none" onclick="CancelCheckSecurity()">Cancel Payment</button>
                                                    <%
                                                            }
                                                        }
                                                    %>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered" id="tblCollectionDetails">
                                                        <thead class="bg-blue white">
                                                            <tr>
                                                                <td class="action text-center">Action</td>
                                                                <td class="ChequeDate text-center" style="display: none;">CheckDate</td>
                                                                <td class="PayId text-center">Pay ID</td>
                                                                <td class="ReceiveDate text-center">Receive Date</td>
                                                                <td class="Status text-center">Status</td>
                                                                <td class="ReceivedBy">Collect By</td>
                                                                <td class="ReceivedAmount price">Receive Amount</td>
                                                                <td class="PaymentMode text-center">Payment Mode</td>
                                                                <td class="ReferenceId">Reference ID</td>
                                                                <td class="Remarks">Remark</td>
                                                                <td class="PaymentType text-center">Payment Type</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>

                                            </div>
                                        </div>
                                        <br />
                                        <div class="d-flex">
                                            <div class="form-group">
                                                <select class="form-control border-primary input-sm" id="ddlPageSizeinCollDet" onchange="CollectionDetailsSearch();">
                                                    <option value="10">10</option>
                                                    <option value="50">50</option>
                                                    <option value="100">100</option>
                                                    <option value="500">500</option>
                                                    <option value="1000">1000</option>
                                                    <option value="0">All</option>
                                                </select>
                                            </div>
                                            <div class="ml-auto">
                                                <div class="Pager" id="CollDet"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" id="DateSort" value="2" />
                                    <div class="row tab-pane" id="CustomerLog" aria-labelledby="base-CustomerLog">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered" id="tblOrderLog">
                                                        <thead class="bg-blue white">
                                                            <tr>
                                                                <td class="SrNo text-center">SN</td>
                                                                <td class="ActionBy text-center">Action By</td>
                                                                <td class="Date text-center">Date
                                                                <span class="la la-arrow-down" onclick="CustomerLogReportSortInOrder(1)" style="display: none;"></span>
                                                                    <span class="la la-arrow-up" onclick="CustomerLogReportSortInOrder(2)"></span>
                                                                </td>
                                                                <td class="Action">Action</td>
                                                                <td class="Remark">Remark</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>

                                            </div>
                                        </div>
                                        <br />
                                        <div class="d-flex">
                                            <div class="form-group">
                                                <select class="form-control border-primary input-sm" id="ddlPageSizeOrdLog" onchange="PagingCustomerLogReport(1);">
                                                    <option value="10">10</option>
                                                    <option value="50">50</option>
                                                    <option value="100">100</option>
                                                    <option value="500">500</option>
                                                    <option value="1000">1000</option>
                                                    <option value="0">All</option>
                                                </select>
                                            </div>
                                            <div class="ml-auto">
                                                <div class="Pager" id="CustLog"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row tab-pane" id="CustomerDocuments" aria-labelledby="base-CustomerDocuments">
                                        <div class="row form-group">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered" id="tblCustomerDocuments">
                                                        <thead class="bg-blue white">
                                                            <tr>
                                                                <td class="SrNo text-center">Sr. No</td>
                                                                <td class="DocumentName">Document Name</td>
                                                                <td class="Document text-center">Download/View</td>
                                                                <td class="ViewDownload text-center">Action</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td class="SrNo">1</td>
                                                                <td class="DocumentName"></td>
                                                                <td class="Document"></td>
                                                                <td class="ViewDownload"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-12">
                                                <div class="form-group  pull-right">
                                                    <button type="button" id="btnbrowse" onclick="uploaddocument()" class="btn btn-sm btn-success">Upload Document</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="row tab-pane" id="StoreCreditlog" aria-labelledby="base-StoreCreditlog">
                                        <%
                                            if (Session["EmpTypeNo"] != null)
                                            {
                                                if (Session["EmpTypeNo"].ToString() == "1")
                                                {%>



                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group  pull-right">
                                                    <button type="button" id="btnsettledcreditAmt" onclick="SettledcreditAmt()" class="btn btn-sm btn-success">Settle Credit Amount</button>
                                                </div>
                                            </div>
                                        </div>
                                        <% }
                                            }
                                        %>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered" id="tblStoreCreditlog">
                                                        <thead class="bg-blue white">
                                                            <tr>
                                                                <td class="CreatedDate text-center">Receive Date</td>
                                                                <td class="ReferenceId text-center">Reference No</td>
                                                                <td class="Receivedby">Collect By</td>
                                                                <td class="Amount price">Amount</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row tab-pane" id="BankDetails" aria-labelledby="base-BankDetails">
                                        <div class="row form-group">
                                            <div class="col-md-6">
                                                <div class="form-group  pull-left">
                                                    <button type="button" id="btnSaveBankDetails" onclick="OpenPopup()" class="btn btn-sm btn-success">Add Bank Details</button>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group  pull-left">
                                                    <button type="button" id="btnSaveCardDetails" onclick="OpenPopup1()" class="btn btn-sm btn-success">Add Card Details</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered" id="tblBankDetails">
                                                        <thead class="bg-blue white">
                                                            <tr>
                                                                <td class="Action text-center">Action</td>
                                                                <td class="BankName text-center">Bank Name</td>
                                                                <td class="Routing text-center">Routing No.</td>
                                                                <td class="BankACC text-center">Bank A/C</td>
                                                                <td class="BankRemarks">Remarks</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered" id="tblCardDetails">
                                                        <thead class="bg-blue white">
                                                            <tr>
                                                                <td class="Action text-center">Action</td>
                                                                <%--  <td class="BankName text-center">Bank Name</td>
                                                                <td class="BankACC text-center">Bank A/C</td>--%>
                                                                <td class="CardType">Card Type</td>
                                                                <td class="CardNo text-center">Card No</td>
                                                                <td class="ExpiryDate text-center">Expiry Date</td>
                                                                <td class="CVV text-center">CVV</td>
                                                                <td class="Zipcode text-center">Zip Code</td>
                                                                <td class="CardRemarks">Remarks</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                    <div class="row tab-pane" id="ContactDetails" aria-labelledby="base-ContactDetails">
                                        <div class="row form-group">
                                            <div class="col-md-12">
                                                <div class="form-group  pull-left">
                                                    <button type="button" id="btnSaveContactDetails" onclick="OpenContactPopup()" class="btn btn-sm btn-success">Add Contact Details</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered" id="tblContactDetails">
                                                        <thead class="bg-blue white">
                                                            <tr>
                                                                <td class="Action text-center">Action</td>
                                                                <td class="AutoId text-center" style="display: none;">AutoId</td>
                                                                <td class="TypeAutoId text-center" style="display: none;">TypeAutoId</td>
                                                                <td class="Type text-center">Type</td>
                                                                <td class="ContactPerson text-center">Contact Person</td>
                                                                <td class="Mobile text-center">Mobile</td>
                                                                <td class="Landline">Landline 1</td>
                                                                <td class="Landline2">Landline 2</td>
                                                                <td class="Fax">Fax</td>
                                                                <td class="Email">Email</td>
                                                                <td class="AltEmail">Alternate Email</td>
                                                                <td class="IsDefault text-center">Is Default</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row tab-pane" id="AddressDetails" aria-labelledby="base-AddressDetails">
                                        <div class="row form-group">
                                            <div class="col-md-6">
                                                <div class="form-group  pull-left">
                                                    <button type="button" id="btnSaveBilling" onclick="OpenBilling()" class="btn btn-sm btn-success">Add New Billing Address</button>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group  pull-left">
                                                    <button type="button" id="btnSaveShippng" onclick="OpenShiping()" class="btn btn-sm btn-success">Add New Shipping Address</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered" id="tblBillingDetails">
                                                        <thead class="bg-blue white">
                                                            <tr>
                                                                <td class="BAction text-center" style="width: 50px;">Action</td>
                                                                <td class="Id text-center" style="display: none">Id</td>
                                                                <td class="Address" style="max-width: 200px !important; white-space: normal !important;">Billing Address</td>
                                                                <td class="IsBDefault text-center" style="width: 35px;">Default</td>

                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered" id="tblShippingDetails">
                                                        <thead class="bg-blue white">
                                                            <tr>
                                                                <td class="SAction text-center" style="width: 50px;">Action</td>
                                                                <td class="Address" style="max-width: 200px !important; white-space: normal !important;">Shipping Address </td>
                                                                <td class="IsSDefault text-center" style="width: 35px;">Default</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div id="PopContactDetails" class="modal fade" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" style="max-width: 611px!important;">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 style="margin-bottom: 0 !important">Contact Person Details</h4>
                    <input type="hidden" id="hdnContactAutoId" />
                </div>
                <div class="modal-body" style="padding-bottom: 0 !important;">
                    <div class="row form-group">

                        <div class="col-md-4">
                            <label class="control-label">
                                Type  <span class="required">*</span>
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <select id="ddlType" class="form-control border-primary input-sm ddlcreq" runat="server">
                                    <option value="0">-Select Type-</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">
                                Contact Person Name  <span class="required">*</span>
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm document creq" onblur="checkSpecialCharacter(this)" placeholder="Contact Person Name" id="txtContactName" runat="server" maxlength="50" />

                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">
                                Mobile No. <span class="required">*</span>
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm document creq" onblur="MobileLength(this)" placeholder="Mobile No" id="txtContactMobile" runat="server" minlength="10" maxlength="10" onkeypress="return isNumberKey(event)" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">
                                Landline 1 <span class="required">*</span>
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm document creq" onblur="MobileLength(this)" placeholder="Landline 1" id="txtLandline" runat="server" minlength="10" maxlength="10" onkeypress="return isNumberKey(event)" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">
                                Landline 2
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm document" onblur="MobileLength(this)" placeholder="Landline 2" id="txtLandline2" runat="server" minlength="10" maxlength="10" onkeypress="return isNumberKey(event)" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">
                                Fax 
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm document" placeholder="Fax" id="txtFax" runat="server" maxlength="10" onkeypress="return isNumberKey(event)" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">
                                Email <span class="required">*</span>
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm document creq" placeholder="Email" id="txtEmail" runat="server" maxlength="50" onchange="validateEmail(this);" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">
                                Alternate Email 
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm document" placeholder="Alternate Email" id="txtAlternateEmail" maxlength="50" runat="server" onchange="validateEmail(this);" />
                            </div>
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">
                                Is Default 
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="checkbox" class="checkbox border-primary" runat="server" id="chkIsDefault" />
                            </div>
                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-success pull-right  buttonAnimation round box-shadow-1  btn-sm" id="btnContactSaves1" onclick="saveContactDetail()" style="margin: 0 5px 0 5px">Save</button>
                            <button type="button" class="btn btn-warning pull-right  buttonAnimation round box-shadow-1  btn-sm" id="btnContactUpdates" onclick="UpdateContactPersonDetails()" style="display: none; margin: 0 5px 0 5px">Update</button>
                            <button type="button" class="btn btn-danger pull-right  buttonAnimation round box-shadow-1  btn-sm" onclick="clearBorder()" data-dismiss="modal" style="margin: 0 5px 0 5px">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="CustomerDueAmount" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-xl">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Payment Details<span id="StatusName"></span></h4>
                    <input type="hidden" id="PaymentAutoId" value="0" />
                </div>
                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col-md-3 col-sm-3">
                            <label class="control-label">
                                Payment ID :
                            </label>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm" disabled placeholder="Payment Id" id="PaymentId" />
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label class="control-label">
                                Payment Date :
                            </label>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <span class="la la-calendar-o"></span>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control border-primary input-sm date" disabled placeholder="Payment Id" id="PaymentDate" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-3 col-sm-3">
                            <label class="control-label">
                                Payment Method : <span class="required">*</span>
                            </label>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <div class="form-group">
                                <select class="form-control border-primary input-sm Update" id="PaymentMethod" runat="server" disabled>
                                    <option value="0">-Select Payment Mode-</option>
                                    <%--<option value="1">Cash</option>
                                    <option value="2">Check</option>
                                    <option value="3">Money Order</option>
                                    <option value="4">Credit Card</option>
                                    <option value="5">Store Credit</option>--%>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label class="control-label">
                                Received  Amount : <span class="required">*</span>
                            </label>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm Update" disabled runat="server" placeholder="Received Amount" onfocus="EditReceivedPay()"
                                    onkeydown="EditReceivedPay()" onkeyup="EditReceivedPay()" id="ReceivedAmount" style="text-align: right" />
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-3 col-sm-3">
                            <label class="control-label">
                                Reference No :
                            </label>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm" disabled placeholder="Reference No" id="ReferenceNo" />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">
                                Store Credit:
                            </label>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" id="txtCreditStore" class="form-control border-primary input-sm" readonly="true" value="0.00" style="text-align: right" />
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-3">
                            <label class="control-label">
                                Received Payment By  : <span class="required">*</span>
                            </label>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <select id="EmpName1" class="form-control border-primary input-sm Update" runat="server">
                                    <option value="0">Select Employee</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">
                                Processed By : 
                            </label>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm" disabled placeholder="Received  By " id="EmpName" />
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-3">
                            <label class="control-label">
                                Account Remark:
                            </label>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <textarea id="EmployeeRemarks" disabled class="form-control border-primary input-sm"></textarea>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label class="control-label">
                                Payment Remark :
                            </label>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <div class="form-group">
                                <textarea id="PaymentRemarks" disabled class="form-control border-primary input-sm"></textarea>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <label class="control-label">
                                Total Store Credit:
                            </label>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" id="txtStoreCreditTotal" class="form-control border-primary input-sm" readonly="true" value="0.00" style="text-align: right" />
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered" id="tblduePayment">
                                    <thead class="bg-blue white">
                                        <tr>
                                            <td class="SrNo text-center">SN</td>
                                            <td class="OrderType text-center">Order Type</td>
                                            <td class="OrderDate text-center">Order Date</td>
                                            <td class="OrderNo text-center">Order No</td>
                                            <td class="Amount" style="text-align: right">Paid Amount</td>
                                            <td class="OrderAmount" style="display: none; text-align: right">Order Amount</td>
                                            <td class="PaidAmount" style="text-align: right; display: none">Paid Amount</td>
                                            <td class="DueAmount" style="text-align: right; display: none">Due Amount</td>
                                            <td class="CheckBoc paynow text-center" style="display: none">Select Check Box</td>
                                            <td class="PayAmount paynow" style="display: none; text-align: right">Pay Amount</td>
                                            <td class="payDueAmount paynow" style="display: none; text-align: right">Due Balance</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr style="font-weight: bold">
                                            <td colspan="4">Total</td>
                                            <td id="Td1" style="text-align: right">0.00</td>
                                            <td id="Td2" style="display: none; text-align: right;">0.00</td>
                                            <td id="Td4" style="display: none; text-align: right;">0.00</td>
                                            <td id="Td5" style="display: none; text-align: right;">0.00</td>
                                            <td id="Td8" style="display: none; text-align: right;">0.00</td>
                                            <td id="Td6" style="display: none; text-align: right;">0.00</td>
                                            <td id="Td7" style="display: none; text-align: right;">0.00</td>

                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="form-group hideCurrency" style="display: none">
                        <div class="col-md-8">
                            <h4 class="card-title">Cash Details</h4>
                        </div>
                    </div>
                    <div class="row hideCurrency" style="display: none; margin: 0">
                        <div class="col-md-8">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered" id="tblPaymentHistoryCurrencyList">
                                    <thead class="bg-blue white">
                                        <tr>
                                            <td class="CurrencyName text-center">Bill</td>
                                            <td class="NoofRupee" style="width: 150px;">Total Count</td>
                                            <td class="Total" style="text-align: right; width: 150px">Total Amount</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr style="font-weight: bold">
                                            <td colspan="2" style="text-align: right">Total</td>
                                            <td id="PaymentHistpryTotalAmount" style="text-align: right">0.00</td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">
                                    <b style="white-space: nowrap">Short Amount :</b>
                                </label>
                                <input type="text" id="txtPaymentHistorySortAmount" onkeyup="PaymentHistorySortAmountCal()" readonly="true" onkeypress="return isNumberDecimalKey(event,this)" class="form-control border-primary input-sm" value="0.00" style="text-align: right" />
                            </div>
                            <div class="form-group">
                                <label class="control-label">
                                    <b style="white-space: nowrap">Total Amount :</b>
                                </label>
                                <input type="text" id="txtPaymentHistoryTotalCurrencyAmount" readonly="true" onkeypress="return isNumberDecimalKey(event,this)" class="form-control border-primary input-sm" value="0.00" style="text-align: right; font-size: 19px; font-weight: bold;" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row form-group" style="margin: 0">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-danger buttonAnimation round box-shadow-1  btn-sm pull-right" style="margin: 0 5px 0 5px" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm pull-right" style="margin: 0 5px 0 5px" id="Print" onclick="printPaymentSlip()">Print</button>
                            <button type="button" class="btn btn-success buttonAnimation round box-shadow-1  btn-sm pull-right" style="margin: 0 5px 0 5px" id="editPayment" onclick="CheckPaymentEdit()">Edit</button>
                            <button type="button" class="btn btn-primary buttonAnimation round box-shadow-1  btn-sm pull-right" style="margin: 0 5px 0 5px; display: none" id="UpdatePayment" onclick="PaymentUpdate()">Update</button>
                            <button type="button" class="btn btn-warning buttonAnimation round box-shadow-1  btn-sm pull-right" id="btnCancelpayment" onclick="CancelPayment()" style="display: none">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="PopCustomerDocuments" class="modal fade" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 style="margin-bottom: 0 !important" class="card-title">Upload Document</h4>
                    <input type="hidden" id="hdnuploadAutoId" />
                </div>
                <div class="modal-body" style="padding-bottom: 0 !important;">
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label class="control-label">
                                Document Name : <span class="required">*</span>
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <select id="ddlDocumentName" class="form-control border-primary input-sm document1">
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">
                                Document : <span class="required">*</span>
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="file" class="form-control border-primary input-sm document1" style="height: 4rem" id="uploadedFile" runat="server" />
                                Allowed file type : <span class="required">gif,png,jpg,jpeg,doc,pdf</span>
                            </div>
                        </div>
                        <div class="col-md-4"></div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <a href="#" id="ahrefViewDocument" target="_blank" class="btn btn-sm btn-success">View</a>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-success pull-right  buttonAnimation round box-shadow-1  btn-sm" id="Button2" onclick="DocumentSave()" style="margin: 0 5px 0 5px">Save</button>
                            <button type="button" class="btn btn-warning pull-right  buttonAnimation round box-shadow-1  btn-sm" id="Button3" onclick="UpdateDocument()" style="display: none; margin: 0 5px 0 5px">Update</button>
                            <button type="button" class="btn btn-danger pull-right  buttonAnimation round box-shadow-1  btn-sm" onclick="clearBorder()" data-dismiss="modal" style="margin: 0 5px 0 5px">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="PopBankDetails" class="modal fade" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 style="margin-bottom: 0 !important">Add Bank Details</h4>
                    <input type="hidden" id="hdnAutoId" />
                </div>
                <div class="modal-body" style="padding-bottom: 0 !important;">
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label class="control-label">
                                Bank Name  <span class="required">*</span>
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm document Breq" maxlength="40" placeholder="Bank Name" id="txtBankName" runat="server" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">
                                Routing No.
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm document" placeholder="Routing No" onkeypress='return isNumberKey(event)' id="txtRoutingNo" runat="server" maxlength="25" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">
                                Bank A/C  <span class="required">*</span>
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm document Breq" placeholder="Bank A/C" id="txtACName" runat="server" onkeypress='return isNumberKey(event)' maxlength="20" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">
                                Remarks
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <textarea class="form-control border-primary input-sm" id="txtBankRemarks"></textarea>
                            </div>
                        </div>
                        <br />
                        <%-- <div class="col-md-12">
                            <h5><u>Credit Card Details</u></h5>
                        </div><br />--%>
                        <%--                       <div class="col-md-4">
                            <label class="control-label">
                                Card Type  <span class="required">*</span>
                            </label>
                        </div>
                       <div class="col-md-8">
                            <div class="form-group">
                                <select id="ddlCardType" class="form-control border-primary input-sm ddlreq" runat="server">
                                    <option value="0">-Select Card Type-</option>
                                </select>
                            </div>
                        </div>
                         <div class="col-md-4">
                            <label class="control-label">
                                Card No.  <span class="required">*</span>
                            </label>
                        </div>
                       <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm document req" placeholder="Card No." id="txtCardNo" runat="server" />
                            </div>
                        </div>
                         <div class="col-md-4">
                            <label class="control-label">
                                Expiry Date  <span class="required">*</span>
                            </label>
                        </div>
                       <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm date req" runat="server" id="txtExpiryDate" placeholder="Expiry Date" />
                            </div>
                        </div>
                         <div class="col-md-4">
                            <label class="control-label">
                                CVV  <span class="required">*</span>
                            </label>
                        </div>
                       <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm document req" placeholder="CVV" id="txtCVV" runat="server" />
                            </div>
                        </div>--%>
                    </div>

                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-success pull-right  buttonAnimation round box-shadow-1  btn-sm" id="btnSaves" onclick="SaveBankDetails()" style="margin: 0 5px 0 5px">Save</button>
                            <button type="button" class="btn btn-warning pull-right  buttonAnimation round box-shadow-1  btn-sm" id="btnUpdates" onclick="UpdateBankDetails()" style="display: none; margin: 0 5px 0 5px">Update</button>
                            <button type="button" class="btn btn-danger pull-right  buttonAnimation round box-shadow-1  btn-sm" onclick="clearBorder()" data-dismiss="modal" style="margin: 0 5px 0 5px">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="PopCardDetails" class="modal fade" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 style="margin-bottom: 0 !important">Add Card Details</h4>
                    <input type="hidden" id="hdnCardAutoId" />
                </div>
                <div class="modal-body" style="padding-bottom: 0 !important;">
                    <div class="row form-group">

                        <div class="col-md-4">
                            <label class="control-label">
                                Card Type  <span class="required">*</span>
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <select id="ddlCardType" class="form-control border-primary input-sm ddlCardreq" runat="server">
                                    <option value="0">-Select Card Type-</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">
                                Card No.  <span class="required">*</span>
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm document ddlCardreq" placeholder="Card No." id="txtCardNo" runat="server" onkeypress='return isNumberKey(event)' maxlength="16" />

                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">
                                Expiry Date  <span class="required">*</span>
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm ddlCardreq" runat="server" id="txtExpiryDate" placeholder="MM/YY" onkeyup="dtval(this,event)" onchange="dtexp(this)" maxlength="5" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">
                                CVV  <%--<span class="required">*</span>--%>
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm document" placeholder="CVV" id="txtCVV" runat="server" onkeypress='return isNumberKey(event)' maxlength="4" onblur="checkCVVLength()" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">
                                Zip Code 
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm document" placeholder="Zip Code" id="txtZipcode" runat="server" onkeypress='return isNumberKey(event)' maxlength="5" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">
                                Remarks
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <textarea class="form-control border-primary input-sm" id="txtCardRemarks"></textarea>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-success pull-right  buttonAnimation round box-shadow-1  btn-sm" id="btnSaves1" onclick="SaveBankDetails1()" style="margin: 0 5px 0 5px">Save</button>
                            <button type="button" class="btn btn-warning pull-right  buttonAnimation round box-shadow-1  btn-sm" id="btnUpdates1" onclick="UpdateBankDetails1()" style="display: none; margin: 0 5px 0 5px">Update</button>
                            <button type="button" class="btn btn-danger pull-right  buttonAnimation round box-shadow-1  btn-sm" onclick="clearBorder()" data-dismiss="modal" style="margin: 0 5px 0 5px">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="modelClearance" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Check Details</h4>
                </div>
                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col-sm-4 col-md-4">
                            <label>Check No <span class="required">*</span></label>
                        </div>
                        <div class="col-sm-6 col-md-8">
                            <input type="text" class="form-control border-primary input-sm SaveCheck" id="txtCheckNo" maxlength="10" runat="server" />
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-4 col-md-4">
                            <label style="white-space: nowrap">Check Deposit Date <span class="required">*</span></label>
                        </div>
                        <div class="col-sm-6 col-md-8">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <span class="la la-calendar-o"></span>
                                    </span>
                                </div>
                                <input type="text" class="form-control border-primary input-sm date SaveCheck" id="txtCheckDate" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-4 col-md-4">
                            <label>Remark</label>
                        </div>
                        <div class="col-sm-6 col-md-8">
                            <textarea class="form-control border-primary input-sm" id="txtRemarks" maxlength="400"></textarea>
                        </div>
                    </div>
                </div>
                <div class="model-footer">
                    <div class="row form-group">
                        <div class="col-sm-12 col-md-12">

                            <button type="button" class="btn btn-success buttonAnimation round box-shadow-1 pull-right  btn-sm" style="margin: 0 5px" onclick="SaveCheckDetails()">Save</button>
                            <button type="button" class="btn btn-danger buttonAnimation round box-shadow-1 pull-right  btn-sm" onclick="clearBordertwo()" style="margin: 0 5px" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="modalPopDuePayment" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" style="color: red">Cancel Check Details</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <label>Check No</label>
                        </div>
                        <div class="col-sm-6 form-group">
                            <input type="text" class="form-control border-primary input-sm" id="txtChequeNo" readonly />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <label>Check Date</label>
                        </div>
                        <div class="col-sm-6 form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <span class="la la-calendar-o"></span>
                                    </span>
                                </div>
                                <input type="text" class="form-control border-primary input-sm date" id="txtChequeDate" readonly />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <label>Remark <span class="required">*</span></label>
                        </div>
                        <div class="col-sm-6 form-group">
                            <textarea class="form-control border-primary input-sm CheckSave" id="txtPopRemarks" runat="server"></textarea>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-6">
                            <label>Bank Fee Charge for Customer <span class="required">*</span></label>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">$
                                    </span>
                                </div>
                                <input type="text" class="form-control border-primary input-sm CheckSave text-right" id="txtBankFeeCharge" maxlength="10" runat="server" onkeypress="return isNumberDecimalKey(event,this)" />

                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-12 form-group">
                            <button type="button" class="btn btn-warning buttonAnimation pull-right round box-shadow-1  btn-sm" style="margin: 0 5px" onclick="CheckCancelDetails()">Cancel Payment</button>
                            <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1  btn-sm" onclick="clearBorder3()" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="PopPaymentDetails" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Payment Details</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-3">
                                <label class="control-label">
                                    Payment ID :
                                </label>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input type="text" class="form-control border-primary input-sm" disabled placeholder="Payment Id" id="PPaymentId" />
                                </div>
                            </div>

                            <div class="col-md-3">
                                <label class="control-label">
                                    Payment Date :
                                </label>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <span class="la la-calendar-o"></span>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control border-primary date input-sm" disabled placeholder="Payment Id" id="PPaymentDate" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <div class="col-md-3">
                                <label class="control-label">
                                    <span style="white-space: nowrap">Payment Method : </span>
                                </label>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select class="form-control border-primary input-sm" id="PPaymentMethod" runat="server" disabled>
                                        <option value="0">-Select Payment Mode-</option>
                                        <option value="1">Cash</option>
                                        <option value="2">Check</option>
                                        <option value="3">Money Order</option>
                                        <option value="4">Credit Card</option>
                                        <option value="5">Store Credit</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label class="control-label">
                                    <span style="white-space: nowrap">Received Payment Amount : </span>
                                </label>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input type="text" class="form-control border-primary input-sm" disabled runat="server" placeholder="Received Amount"
                                        onfocus="EditReceivedPay()" onkeydown="EditReceivedPay()" onkeyup="EditReceivedPay()" id="PReceivedAmount" style="text-align: right" />
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <div class="col-md-3">
                                <label class="control-label">
                                    <sapn style="white-space: nowrap">Reference No :</sapn>
                                </label>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input type="text" class="form-control border-primary input-sm" disabled placeholder="Reference No" id="PReferenceNo" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label class="control-label">
                                    <span style="white-space: nowrap">Payment Remark :</span>
                                </label>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <textarea id="PPaymentRemarks" disabled class="form-control border-primary input-sm"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <div class="col-md-3">
                                <label class="control-label">
                                    <span style="white-space: nowrap">Account Remark:</span>
                                </label>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <textarea id="PEmployeeRemarks" disabled class="form-control border-primary input-sm"></textarea>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="tblPduePayment">
                            <thead class="bg-blue white">
                                <tr>
                                    <td class="OrderType">Order Type</td>
                                    <td class="OrderDate">Order Date</td>
                                    <td class="OrderNo">Order No</td>
                                    <td class="OrderAmount" style="text-align: right;">Order Amount</td>
                                    <td class="PaidAmount" style="text-align: right">Paid Amount</td>
                                    <td class="DueAmount" style="text-align: right;">Due Amount</td>
                                    <td class="CheckBoc">Select Check Box</td>
                                    <td class="PayAmount paynow" style="text-align: right">Pay Amount</td>
                                    <td class="payDueAmount paynow" style="text-align: right">Due Balance</td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr style="font-weight: bold">
                                    <td colspan="3">Total</td>
                                    <td id="PT1">0.00</td>
                                    <td id="PT2">0.00</td>
                                    <td id="PT3">0.00</td>
                                    <td id="PT5"></td>
                                    <td id="PT4">0.00</td>
                                    <td id="PT6">0.00</td>

                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-5">
                            <button type="button" class="btn btn-success buttonAnimation round box-shadow-1  btn-sm" id="PeditPayment" onclick="Paymentedit()">Edit</button>
                            <button type="button" class="btn btn-success buttonAnimation round box-shadow-1  btn-sm" id="PUpdatePayment" onclick="PaymentUpdate()" style="display: none; margin: 0 5px">Update</button>
                            <button type="button" class="btn btn-danger buttonAnimation round box-shadow-1  btn-sm" id="PbtnCancelpayment" onclick="CancelPayment()" style="display: none; margin: 0 5px">Cancel</button>
                            <button type="button" class="btn btn-default buttonAnimation round box-shadow-1  btn-sm" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="CreditOrderPop" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 style="margin-bottom: 0 !important" class="modal-title">Settle Credit Memo</h4>
                </div>
                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col-md-3">
                            Order No<span class="required"> *</span>
                        </div>
                        <div class="col-md-4">
                            <select id="ddlOrder" class="form-control border-primary input-sm" style="width: 100% !important" onchange="getamount()">
                                <option value="0">-Select Order-</option>
                            </select>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <label class="control-label">
                                <span style="white-space: nowrap">Total Order Amount : </span>
                            </label>
                        </div>
                        <div class="col-md-4 col-sm-3">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm" disabled placeholder="0.00"
                                    id="txtCreditMemoAmount" style="text-align: right" />
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered" id="tblMemoOrderList">
                                    <thead class="bg-blue white">
                                        <tr>
                                            <td class="SrNo">SN</td>
                                            <td class="action"></td>
                                            <td class="CreditDate">Credit Date</td>
                                            <td class="CreditNo">Credit No</td>
                                            <td class="TotalAmount" style="text-align: right">Total Amount</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr style="font-weight: bold">
                                            <td colspan="4">Total</td>
                                            <td id="PTd1" style="text-align: right">0.00</td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-2">
                            <label class="control-label">
                                <b style="white-space: nowrap">Remark : </b>
                            </label>
                        </div>
                        <div class="col-md-8 col-sm-8">
                            <div class="form-group">
                                <textarea class="form-control border-primary input-sm" placeholder="Enter Remark" maxlength="500"
                                    id="txtCreditRemarks"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation round box-shadow-1  btn-sm" style="margin: 0 5px" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-success buttonAnimation round box-shadow-1  btn-sm" style="margin: 0 5px" onclick="SettlledCredit()">Submit</button>
                </div>
            </div>
        </div>
    </div>

    <%------------------------------Security Check------------------------------------------------%>
    <div class="modal fade" id="SecurityEnabledVoid" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Security Check </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">Security </div>
                        <div class="col-md-9">
                            <input type="password" id="txtSecurityVoid" class="form-control input-sm border-primary" />
                        </div>
                    </div>

                    <br />
                    <div class="row">
                        <div class="col-md-12">
                            <span id="errormsgVoid"></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1 btn-sm" onclick="clickonSecurityVoid()">OK</button>
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ModelCancelRemark" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Cancel Remark </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">
                            <label class="control-label">Remark</label>
                            <span class="required">*</span>
                        </div>
                        <div class="col-md-9">
                            <textarea id="ReasonCancel" class="form-control input-sm border-primary"></textarea>
                        </div>
                    </div>

                    <br />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1 btn-sm" onclick="SaveCancelReason()">Save</button>
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div id="SettledCreditPopUp" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="modal-title">Settled Credit Amount</h4>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4 form-group">
                            <label class="control-label">Settled Amount <span class="required">*</span></label>

                        </div>
                        <div class="col-md-8 col-sm-8 form-group">
                            <input id="txtCreditAmount" class="form-control input-sm border-primary creditstoreCredit" value="0.00" maxlength="8" style="text-align: right" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="control-label">Remark <span class="required">*</span></label>
                        </div>
                        <div class="col-md-8 col-sm-8">
                            <textarea id="txtsettledRemarks" placeholder="Please Enter Remarks" class="form-control border-primary creditstoreCredit" row="2" maxlength="500"></textarea>
                            <span style="color: red;"><i>Note : Max length 500 character</i></span>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" onclick="UpdateCreaditAmt()">Update</button>
                </div>
            </div>
        </div>
    </div>
    <div id="modalPaymentLog" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="modal-title">Payment Log</h4>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered" id="tblPaymentLog">
                                    <thead class="bg-blue white">
                                        <tr>
                                            <td class="SrNo text-center">SN</td>
                                            <td class="PaymentId text-center">Payment Id</td>
                                            <td class="PaymentDate text-center">Payment Date</td>
                                            <td class="EmpName">Update By</td>
                                            <td class="PaymentRemarks">Payment Remarks</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                                <h5 class="well text-center" id="PaymentLogEmptyTable" style="display: none">No data available.</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div id="OrderPaymentDetails" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Payment Details</h4>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="tblOrderPayment">
                            <thead class="bg-blue white">
                                <tr>
                                    <td class="PaymentId_r text-center">Payment Id</td>
                                    <td class="PaymentDate_r text-center">Payment Date</td>
                                    <td class="PaymentMode text-center">Payment Mode</td>
                                    <td class="RecievedAmount_r text-right">Settled Amount</td>
                                    <td class="SettledBy text-right">Settled By</td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr style="font-weight: bold">
                                    <td colspan="3">Total</td>
                                    <td id="paidTotal" class="text-right"></td>
                                    <td></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="SecurityEnabledHold" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Security Check </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">Security </div>
                        <div class="col-md-9">
                            <input type="password" id="txtSecurityHold" class="form-control input-sm border-primary SaveHoldCheck" />
                        </div>
                    </div>

                    <br />
                    <div class="row">
                        <div class="col-md-12">
                            <span id="errormsgHold"></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1 btn-sm" onclick="clickonSecurityHold()">OK</button>
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="PopBillingDetails" class="modal fade" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 style="margin-bottom: 0 !important" class="card-title">Add Billing Details</h4>
                    <input type="hidden" id="hdnBAutoId" />
                </div>
                <div class="modal-body" style="padding-bottom: 0 !important;">
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label class="control-label">
                                Address 1  <span class="required">*</span>
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <textarea class="form-control border-primary input-sm document Bireq" autocomplete="chrome-off" placeholder="Address 1" id="txtBaddress1" rows="2"></textarea>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">
                                Address 2  
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <textarea class="form-control border-primary input-sm document " autocomplete="chrome-off" placeholder="Address 2" id="txtBaddress2" rows="2"></textarea>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">
                                City
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm document" placeholder="City" id="txtBcity" readonly maxlength="25" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">
                                State  
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm document" placeholder="State" id="txtBstate" readonly />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">
                                Zip Code 
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm document" placeholder="Zip Code" id="txtBzipcode" readonly />
                            </div>
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">Latitude</label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm " placeholder="Latitude" id="txtBLat" readonly />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">Longitude</label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm " placeholder="Longitude" id="txtBLong" readonly />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">
                                Is Default  
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="checkbox" class="Default" id="IsDefault" />
                            </div>
                        </div>
                        <br />
                    </div>


                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-success pull-right  buttonAnimation round box-shadow-1  btn-sm" id="btnBSaves" onclick="SaveBilling()" style="margin: 0 5px 0 5px">Save</button>
                            <button type="button" class="btn btn-warning pull-right  buttonAnimation round box-shadow-1  btn-sm" id="btnBUpdates" onclick="UpdateBilling()" style="display: none; margin: 0 5px 0 5px">Update</button>
                            <button type="button" class="btn btn-danger pull-right  buttonAnimation round box-shadow-1  btn-sm" onclick="clearBorder()" data-dismiss="modal" style="margin: 0 5px 0 5px">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="PopShippingDetails" class="modal fade" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 style="margin-bottom: 0 !important" class="card-title">Add Shipping Details</h4>
                    <input type="hidden" id="hdnSAutoId" />
                </div>
                <div class="modal-body" style="padding-bottom: 0 !important;">
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label class="control-label">
                                Address 1  <span class="required">*</span>
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <textarea class="form-control border-primary input-sm document Sreq" placeholder="Address 1" id="txtSaddress1" rows="2"></textarea>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">
                                Address2  
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <textarea class="form-control border-primary input-sm document " placeholder="Address 2" id="txtSaddress2" rows="2"></textarea>

                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">
                                City 
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm document" placeholder="City" id="txtScity" readonly maxlength="25" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">
                                State  
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm document" placeholder="State" id="txtSstate" readonly />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">
                                Zip Code 
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm document" placeholder="Zip Code" id="txtSZipcode" readonly />
                            </div>
                        </div>
                        <br />
                        <div class="col-md-4">
                            <label class="control-label">Latitude</label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm " id="txtSLat" placeholder="Latitude" runat="server" readonly />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">Longitude</label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm " id="txtSLong" placeholder="Longitude" readonly />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">
                                Is Default  
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="checkbox" class="SDefault" id="IsSDefault" />
                            </div>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-success pull-right  buttonAnimation round box-shadow-1  btn-sm" id="btnSSaves" onclick="SaveShipping();" style="margin: 0 5px 0 5px">Save</button>
                            <button type="button" class="btn btn-warning pull-right  buttonAnimation round box-shadow-1  btn-sm" id="btnSUpdates" onclick="UpdateShipping()" style="display: none; margin: 0 5px 0 5px">Update</button>
                            <button type="button" class="btn btn-danger pull-right  buttonAnimation round box-shadow-1  btn-sm" onclick="clearBorder()" data-dismiss="modal" style="margin: 0 5px 0 5px">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="hiddenShipAddress" style="display: none;"></div>
    <div class="hiddenBillAddress" style="display: none;"></div>

    <div class="modal fade" id="SecurityEnabledPaymentEdit" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Security Check </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">Security </div>
                        <div class="col-md-9">
                            <input type="password" id="txtSecurityPaymentEdit" class="form-control input-sm border-primary" />
                        </div>
                    </div>

                    <br />
                    <div class="row">
                        <div class="col-md-12">
                            <span id="ErrormsgPaymentEdit"></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1 btn-sm" onclick="clickonSecurityPaymentEdit()">OK</button>
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <script>

        google.maps.event.addDomListener(window, 'load', function () {
            var places = new google.maps.places.Autocomplete(document.getElementById('txtBaddress1'));
            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                console.log(place);

                var address = place.formatted_address;
                var latitude = place.geometry.location.lat;
                var longitude = place.geometry.location.lng;
                var mesg = "Address: " + address;
                mesg += "\nLatitude: " + latitude;
                mesg += "\nLongitude: " + longitude;

                setBillingData(place);

            });
        });

        google.maps.event.addDomListener(window, 'load', function () {

            var places = new google.maps.places.Autocomplete(document.getElementById('txtSaddress1'));
            google.maps.event.addListener(places, 'place_changed', function () {

                var place = places.getPlace();

                var address = place.formatted_address;
                var latitude = place.geometry.location.lat;
                var longitude = place.geometry.location.lng;
                var mesg = "Address: " + address;
                mesg += "\nLatitude: " + latitude;
                mesg += "\nLongitude: " + longitude;
                setData(place);

            });
        });

    </script>



</asp:Content>


