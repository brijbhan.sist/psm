﻿using DLLCreditMemoMaster;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Sales_CreditMemo : System.Web.UI.Page
{

  
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["EmpType"] == null)
        {
            Session.Abandon();
            Response.Redirect("~/Default.aspx", false);
        }
        else if (Request.QueryString["PageId"] == null && Session["EmpTypeNo"].ToString() != "2" && Session["EmpTypeNo"].ToString() != "1" && Session["EmpTypeNo"].ToString() != "10" && Session["EmpTypeNo"].ToString() != "12")
        {
            Response.Redirect("/Sales/CreditMemoList.aspx");
        }
    }

    [WebMethod(EnableSession = true)]
    public static string clickonSecurity(string CheckSecurity)
    {
        PL_CreditMemoMaster pobj = new PL_CreditMemoMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"].ToString() != null)
            {

                pobj.CheckSecurity = CheckSecurity.ToString();
                BL_CreditMemoMaster.clickonSecurity(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return "false";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string ConfirmSecurity(string CheckSecurity, string CreditNo, string MLTaxRemark)
    {
        PL_CreditMemoMaster pobj = new PL_CreditMemoMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"].ToString() != null)
            {

                pobj.CheckSecurity = CheckSecurity;
                pobj.CreditNo = CreditNo;
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"].ToString());
                pobj.MLTaxRemark = MLTaxRemark;
                BL_CreditMemoMaster.ConfirmSecurity(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "false";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string  CheckSecurity(string datavalue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(datavalue);
        PL_CreditMemoMaster pobj = new PL_CreditMemoMaster();
        try
        {
            if (HttpContext.Current.Session["EmpTypeNo"] != null)
            {
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpTypeNo"].ToString());
                pobj.SecurityKey = jdv["Security"];
                BL_CreditMemoMaster.checkSecurity(pobj);
                if (!pobj.isException)
                {
                    
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "SessionExpired";

            }
        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string CancelCreditMomo(string datavalue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(datavalue);
            PL_CreditMemoMaster pobj = new PL_CreditMemoMaster();
            pobj.EmpAutoId = Convert.ToInt32((HttpContext.Current.Session["EmpAutoId"]).ToString());
            pobj.CreditAutoId = Convert.ToInt32(jdv["CreditAutoId"]);
            pobj.CancelRemarks = jdv["CancelRemark"];
            pobj.IPAddress = HttpContext.Current.Request.UserHostAddress;
            BL_CreditMemoMaster.CancelCreditMemo(pobj);
            if (!pobj.isException)
            {
                return "Success";
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}