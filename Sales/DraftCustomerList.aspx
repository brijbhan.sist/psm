﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="DraftCustomerList.aspx.cs" Inherits="Sales_DraftCustomerList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Draft Customer List</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage Draft Customer</a></li>
                        <li class="breadcrumb-item">Draft Customer List</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" id="btnAddNewCust"  onclick="location.href='/Sales/DraftCustomerMaster.aspx'" runat="server">Add Draft Customer</button> 
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row ">
                                    <div class="col-md-3 form-group">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Customer Name" id="txtSCustomerName" />
                                    </div>
                                    <div class="col-md-3 form-group" id="dlSalesPerson" runat="server">
                                        <select class="form-control border-primary input-sm" id="ddlSSalesPerson" runat="server" style="width: 100%;">
                                            <option value="0">All Sales Person</option>
                                        </select>
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlCustomerType" style="width: 100%;">
                                            <option selected="selected" value="0">All Customer Type</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlState" style="width: 100%;">
                                            <option selected="selected" value="0">All State</option>
                                        </select>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-3 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlCity" style="width: 100%;">
                                            <option selected="selected" value="0">All City</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlStatus" style="width: 100%;display:none">
                                            <option selected="selected" value="2">All Status</option>
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                    </div>
                                    <div id="Div1" class="form-group" runat="server">
                                        <input type="hidden" id="hidnEmpType" runat="server" />

                                    </div>
                                    <div class="col-md-1 form-group">
                                        <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1  btn-sm" id="btnSearch" onclick="getCustomerList(1);">Search</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblCustomerList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="Action text-center width4per">Action</td>
                                                        <td class="CustName">Customer Name</td>
                                                        <td class="CustomerType">Customer Type</td>
                                                        <td class="BusinessName text-center">Business Name</td>
                                                        <td class="OPTLicence price">OPT Licence</td>
                                                        <td class="SalesPerson text-center width3per">Sales Person</td>
                                                        <td class="Terms price width3per">Terms</td>
                                                        <td class="ShippingAddress">Shipping Address</td>
                                                        <td class="BillingAddress">Billing Address</td>
                                                        <td class="CreatedBy">Created By</td>
                                                        <td class="CreatedDate">Created Date</td>
                                                        <td class="Status text-center" style="display:none">Status</td>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex">
                                    <div class="form-group">
                                        <select class="form-control border-primary input-sm" id="ddlPageSize" onchange="getCustomerList(1)">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="ml-auto">
                                        <div class="Pager"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

</asp:Content>

