﻿using DllCustomerNew;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Sales_ViewCustomerDetails : System.Web.UI.Page
{
    protected void Page_load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Sales/JS/ViewCustomerDetails.js"));
            string text1 = File.ReadAllText(Server.MapPath("/Sales/JS/AddShippingBilling.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredFields'>" + text + text1+ "</script>"
                ));
        }


        if (Session["EmpTypeNo"] == null)
        {
            Session.Abandon();
            Response.Redirect("~/Default.aspx", false);
        }
        try
        {
            hiddenEmpTypeVal.Value = Session["EmpTypeNo"].ToString();
        }
        catch (Exception)
        {
            Session.Abandon();
            Response.Redirect("~/Default.aspx", false);
        }
    }
    [WebMethod(EnableSession = true)]
    public static string binddocumnets()
    {
        PL_CustomerNew pobj = new PL_CustomerNew();
        pobj.Opcode = 12;
        DL_CustomerNew.CustomerDetails(pobj);
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            if (!pobj.isException)
            {
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                return json;
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string GetCustomerDetails(string CustomerAutoId)
    {
        PL_CustomerNew pobj = new PL_CustomerNew();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.Opcode = 41;
                pobj.CustomerAutoId = Convert.ToInt32(CustomerAutoId);
                DL_CustomerNew.CustomerDetails(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public static string CustomerOrderList(string CustomerAutoId, string PageIndex, string PageSize)
    {
        PL_CustomerNew pobj = new PL_CustomerNew();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.Opcode = 41;
                pobj.CustomerAutoId = Convert.ToInt32(CustomerAutoId);
                pobj.PageIndex = Convert.ToInt32(PageIndex);
                pobj.PageSize = Convert.ToInt32(PageSize);
                DL_CustomerNew.CustomerOrder(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod(EnableSession = true)]
    public static string CustomerPaymentList(string dataValue)
    {
        PL_CustomerNew pobj = new PL_CustomerNew();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {

                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.Opcode = 41;
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.PaymentId = jdv["PaymentId"];
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                if (jdv["FromDate"] != "")
                {
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != "")
                {
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);

                }
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                DL_CustomerNew.CustomerPayment(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public static string CustomerDuePayment(string dataValue)
    {
        PL_CustomerNew pobj = new PL_CustomerNew();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.Opcode = 42;
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.OrderNo = jdv["OrderNo"];
                if (jdv["FromDate"] != "")
                {
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != "")
                {
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);

                }
                DL_CustomerNew.CustomerPayment(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod(EnableSession = true)]
    public static string CustomerPaymentDetails(string PaymentAutoId)
    {
        PL_CustomerNew pobj = new PL_CustomerNew();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.Opcode = 43;
                pobj.PaymentAutoId = Convert.ToInt32(PaymentAutoId);
                DL_CustomerNew.CustomerPayment(pobj);
                if (!pobj.isException)
                {

                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public static string EditPaymentDetails(string PaymentAutoId)
    {
        PL_CustomerNew pobj = new PL_CustomerNew();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.Opcode = 44;
                pobj.PaymentAutoId = Convert.ToInt32(PaymentAutoId);
                DL_CustomerNew.CustomerPayment(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod(EnableSession = true)]
    public static string CheckPaymentEdit(string PaymentAutoId)
    {
        PL_CustomerNew pobj = new PL_CustomerNew();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.Opcode = 51;
                pobj.PaymentAutoId = Convert.ToInt32(PaymentAutoId);
                DL_CustomerNew.CustomerPayment(pobj);
                if (!pobj.isException)
                {
                    return pobj.exceptionMessage; ;
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod(EnableSession = true)]
    public static string UpdatePayment(string TableValues, string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        DataTable dtOrder = new DataTable();
        dtOrder = JsonConvert.DeserializeObject<DataTable>(TableValues);
        PL_CustomerNew pobj = new PL_CustomerNew();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.Opcode = 21;
                pobj.PaymentAutoId = Convert.ToInt32(jdv["PaymentAutoId"]);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                if (jdv["ReceivedAmount"] != "")
                {
                    pobj.ReceivedAmount = Convert.ToDecimal(jdv["ReceivedAmount"]);
                }
                if (jdv["CreditStore"] != "")
                {
                    pobj.StoreCredit = Convert.ToDecimal(jdv["CreditStore"]);
                }
                pobj.PaymentMethod = Convert.ToInt32(jdv["PaymentMethod"]);
                pobj.ReferenceNo = jdv["ReferenceNo"];
                pobj.PaymentRemarks = jdv["PaymentRemarks"];
                pobj.ReceivedPaymentBy = Convert.ToInt32(jdv["ReceivedPaymentBy"]);
                pobj.EmployeeRemarks = jdv["EmployeeRemarks"];
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                pobj.SortAmount = Convert.ToDecimal(jdv["SortAmount"]);
                pobj.TotalCurrencyAmount = Convert.ToDecimal(jdv["TotalCurrencyAmount"]);
                pobj.PaymentCurrencyXml = jdv["PaymentCurrencyXml"];
                if (dtOrder.Rows.Count > 0)
                {
                    pobj.TableValue = dtOrder;
                }

                DL_CustomerNew.CustomerPayment(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public static string CustomerLogReport(string dataValue)
    {
        PL_CustomerNew pobj = new PL_CustomerNew();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {

                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.Opcode = 42;
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.DateSortIn = Convert.ToInt32(jdv["DateSortIn"]);
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                DL_CustomerNew.CustomerDetails(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public static string StoreCreditLog(string dataValue)
    {
        PL_CustomerNew pobj = new PL_CustomerNew();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {

                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.Opcode = 43;
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                DL_CustomerNew.CustomerDetails(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod(EnableSession = true)]
    public static string savePayment(string TableValues, string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        DataTable dtOrder = JsonConvert.DeserializeObject<DataTable>(TableValues);
        PL_CustomerNew pobj = new PL_CustomerNew();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.Opcode = 11;
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                if (jdv["ReceivedAmount"] != "")
                {
                    pobj.ReceivedAmount = Convert.ToDecimal(jdv["ReceivedAmount"]);
                }
                pobj.PaymentMethod = Convert.ToInt32(jdv["PaymentMethod"]);
                pobj.BillAddAutoId = Convert.ToInt32(jdv["PaymentAutoId"]);
                pobj.ReferenceNo = jdv["ReferenceNo"];
                pobj.PaymentRemarks = jdv["PaymentRemarks"];
                pobj.ReceivedPaymentBy = Convert.ToInt32(jdv["ReceivedPaymentBy"]);
                if (jdv["StoreCredit"] != "")
                    pobj.StoreCredit = Convert.ToDecimal(jdv["StoreCredit"]);
                pobj.EmployeeRemarks = jdv["EmployeeRemarks"];
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                pobj.SortAmount = Convert.ToDecimal(jdv["SortAmount"]);
                pobj.TotalCurrencyAmount = Convert.ToDecimal(jdv["TotalCurrencyAmount"]);
                pobj.PaymentCurrencyXml = jdv["PaymentCurrencyXml"];
                if (dtOrder.Rows.Count > 0)
                {
                    pobj.TableValue = dtOrder;
                }
                DL_CustomerNew.CustomerPayment(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string DocumentSave(string dataValue)
    {
        PL_CustomerNew pobj = new PL_CustomerNew();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {

                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.Opcode = 11;
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.DocumentName = jdv["DocumentName"];
                pobj.DocumentURL = jdv["DocumentURL"];
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                DL_CustomerNew.CustomerDetails(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public static string CustomerDocument(string dataValue)
    {
        PL_CustomerNew pobj = new PL_CustomerNew();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {

                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.Opcode = 44;
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                DL_CustomerNew.CustomerDetails(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public static string UpdateDocument(string dataValue)
    {
        PL_CustomerNew pobj = new PL_CustomerNew();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {

                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.Opcode = 21;
                pobj.FileAutoId = Convert.ToInt32(jdv["FileAutoId"]);
                pobj.DocumentName = jdv["DocumentName"];
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.DocumentURL = jdv["DocumentURL"];
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                DL_CustomerNew.CustomerDetails(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public static string DeleteDocument(string FileAutoId)
    {
        PL_CustomerNew pobj = new PL_CustomerNew();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.Opcode = 31;
                pobj.FileAutoId = Convert.ToInt32(FileAutoId);
                DL_CustomerNew.CustomerDetails(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public static string CollectionDetails(string dataValue)
    {
        PL_CustomerNew pobj = new PL_CustomerNew();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {

                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.Opcode = 45;
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                if (jdv["FromDate"] != "")
                {
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != "")
                {
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);

                }
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                DL_CustomerNew.CustomerPayment(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod(EnableSession = true)]
    public static string CancelledPayment(string dataValue)
    {
        PL_CustomerNew pobj = new PL_CustomerNew();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {

                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.Opcode = 22;
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.EmployeeRemarks = jdv["EmployeeRemarks"];
                pobj.PaymentAutoId = Convert.ToInt32(jdv["PaymentAutoId"]);
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                DL_CustomerNew.CustomerPayment(pobj);
                if (!pobj.isException)
                {

                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string CancelcheckDetails(string dataValue)
    {
        PL_CustomerNew pobj = new PL_CustomerNew();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {

                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.Opcode = 23;
                pobj.EmployeeRemarks = jdv["Remarks"];
                pobj.PaymentAutoId = Convert.ToInt32(jdv["PaymentAutoId"]);
                if (jdv["BankFeeCharge"] != "")
                    pobj.ReceivedAmount = Convert.ToDecimal(jdv["BankFeeCharge"]);
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                DL_CustomerNew.CustomerPayment(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public static string CancelPayment(string datavalue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(datavalue);
        PL_CustomerNew pobj = new PL_CustomerNew();
        try
        {
            if (HttpContext.Current.Session["EmpTypeNo"] != null)
            {
                pobj.Opcode = 24;
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"].ToString());
                pobj.PaymentAutoId = Convert.ToInt32(jdv["PaymentAutoId"]);
                pobj.CancelRemark = jdv["Remark"];
                DL_CustomerNew.CustomerPayment(pobj);
                if (!pobj.isException)
                {

                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "SessionExpired";

            }
        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string Savecheck(string dataValue)
    {
        PL_CustomerNew pobj = new PL_CustomerNew();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {

                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.Opcode = 25;
                pobj.CheckNo = jdv["CheckNo"];
                if (jdv["CheckDate"] != "")
                    pobj.CheckDate = Convert.ToDateTime(jdv["CheckDate"]);
                pobj.PaymentRemarks = jdv["Remarks"];
                pobj.PaymentAutoId = Convert.ToInt32(jdv["PaymentAutoId"]);
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"].ToString());
                DL_CustomerNew.CustomerPayment(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public static string PaymentDetails(string dataValue)
    {
        PL_CustomerNew pobj = new PL_CustomerNew();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {

                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.Opcode = 47;
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.PaymentAutoId = Convert.ToInt32(jdv["PaymentAutoId"]);
                DL_CustomerNew.CustomerPayment(pobj);
                if (!pobj.isException)
                {
                    string json = "";
                    foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                    {
                        json += dr[0];
                    }
                    if (json == "")
                    {
                        json = "[]";
                    }
                    return json;
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod(EnableSession = true)]
    public static string SaveBankDetails(string dataValue)
    {
        if (HttpContext.Current.Session["EmpTypeNo"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_CustomerNew pobj = new PL_CustomerNew();
                pobj.Opcode = 13;
                if (Convert.ToInt32(jdv["Status"]) == 1)
                {
                    pobj.BankName = jdv["BankName"];
                    pobj.BankAcc = jdv["ACName"];
                    pobj.RoutingNo = jdv["RoutingNo"];
                    pobj.PaymentRemarks = jdv["Remarks"];
                }
                else
                {
                    pobj.CardType = Convert.ToInt32(jdv["CardType"]);
                    pobj.CardNo = jdv["CardNo"];
                    if (jdv["ExpiryDate"] != "")
                        pobj.ExpiryDate = jdv["ExpiryDate"];
                    if (jdv["CVV"] != "")
                    {
                        pobj.CVV = jdv["CVV"];
                    }
                    pobj.Zipcode = jdv["Zipcode"];
                    pobj.PaymentRemarks = jdv["Remarks"];
                }
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                DL_CustomerNew.CustomerDetails(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string GetBankDetailsList(string dataValue)
    {
        if (HttpContext.Current.Session["EmpTypeNo"] != null)
        {
            PL_CustomerNew pobj = new PL_CustomerNew();
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            pobj.Opcode = 48;
            pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
            DL_CustomerNew.CustomerDetails(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Unauthorized access.";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string UpdateBankDetails(string dataValue)
    {
        if (HttpContext.Current.Session["EmpTypeNo"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_CustomerNew pobj = new PL_CustomerNew();
                if (Convert.ToInt32(jdv["Status"]) == 1)
                {
                    pobj.BankName = jdv["BankName"];
                    pobj.BankAcc = jdv["ACName"];
                    pobj.RoutingNo = jdv["RoutingNo"];
                    //pobj.PaymentRemarks = jdv["Remarks"];
                }
                else
                {
                    pobj.CardType = Convert.ToInt32(jdv["CardType"]);

                    pobj.CardNo = jdv["CardNo"];
                    if (jdv["ExpiryDate"] != "")
                        pobj.ExpiryDate = jdv["ExpiryDate"];
                    pobj.CVV = jdv["CVV"];
                    //pobj.PaymentRemarks = jdv["Remarks"];
                }
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.AutoId = Convert.ToInt32(jdv["AutoId"]);
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                DL_CustomerNew.CustomerDetails(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string DeleteBankDetails(string AutoId)
    {
        PL_CustomerNew pobj = new PL_CustomerNew();
        pobj.AutoId = Convert.ToInt32(AutoId);
        DL_CustomerNew.CustomerDetails(pobj);
        if (!pobj.isException)
        {
            return pobj.Ds.GetXml();
        }
        else
        {
            return pobj.exceptionMessage;
        }

    }
    [WebMethod(EnableSession = true)]
    public static string CheckSecurity(string datavalue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(datavalue);
        PL_CustomerNew pobj = new PL_CustomerNew();
        try
        {
            if (HttpContext.Current.Session["EmpTypeNo"] != null)
            {
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpTypeNo"].ToString());
                pobj.SecurityKey = jdv["Security"];
                pobj.Opcode = 49;
                DL_CustomerNew.checkSecurity(pobj);
                if (!pobj.isException)
                {

                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "SessionExpired";

            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string CheckSecurityPaymentEdit(string Security)
    {
       
        PL_CustomerNew pobj = new PL_CustomerNew();
        try
        {
            if (HttpContext.Current.Session["EmpTypeNo"] != null)
            {
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpTypeNo"].ToString());
                pobj.SecurityKey = Security;
                pobj.Opcode = 52;
                DL_CustomerNew.checkSecurity(pobj);
                if (!pobj.isException)
                {

                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "SessionExpired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string orderList(string ReferenceNo, string CustomerId)
    {
        PL_CustomerNew pobj = new PL_CustomerNew();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.ReferenceNo = ReferenceNo;
                pobj.CustomerAutoId = Convert.ToInt32(CustomerId);
                pobj.Opcode = 42;
                DL_CustomerNew.CustomerOrder(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public static string CreditMemoSettled(string dataValue)
    {
        PL_CustomerNew pobj = new PL_CustomerNew();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.Opcode = 21;
                pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
                pobj.ReferenceNo = jdv["ReferenceNo"];
                pobj.EmployeeRemarks = jdv["Remarks"];
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"].ToString());
                DL_CustomerNew.CreditMemoSettled(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string StoreCreditDetails(string dataValue)
    {
        PL_CustomerNew pobj = new PL_CustomerNew();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {

                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.Opcode = 43;
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerId"]);
                DL_CustomerNew.CustomerOrder(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public static string deleteCollectionDetails(string PaymentAutoId)
    {
        PL_CustomerNew pobj = new PL_CustomerNew();
        pobj.Opcode = 431;
        pobj.PaymentAutoId = Convert.ToInt32(PaymentAutoId);
        DL_CustomerNew.CustomerPayment(pobj);
        if (!pobj.isException)
        {
            return pobj.Ds.GetXml();
        }
        else
        {
            return pobj.exceptionMessage;
        }

    }
    [WebMethod(EnableSession = true)]
    public static string UpdateCreaditAmt(string dataValue)
    {
        PL_CustomerNew pobj = new PL_CustomerNew();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.Opcode = 221;
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.StoreCredit = Convert.ToDecimal(jdv["CreditAmount"]);
                pobj.PaymentRemarks = jdv["Remarks"];
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"].ToString());
                DL_CustomerNew.CustomerDetails(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod(EnableSession = true)]
    public static string CustomerCreditMemoList(string CustomerAutoId, string PageIndex, string PageSize)
    {
        PL_CustomerNew pobj = new PL_CustomerNew();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.Opcode = 40;
                pobj.CustomerAutoId = Convert.ToInt32(CustomerAutoId);
                pobj.PageIndex = Convert.ToInt32(PageIndex);
                pobj.PageSize = Convert.ToInt32(PageSize);
                DL_CustomerNew.CustomerCreditMemoList(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public static string CustomerPaymentLog(string PaymentAutoId)
    {
        PL_CustomerNew pobj = new PL_CustomerNew();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.Opcode = 50;
                pobj.PaymentAutoId = Convert.ToInt32(PaymentAutoId);
                DL_CustomerNew.CustomerPaymentLog(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public static string OrderPaymentDetails(string OrderAutoId)
    {
        PL_CustomerNew pobj = new PL_CustomerNew();
        try
        {
            pobj.Opcode = 44;
            pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
            DL_CustomerNew.CustomerorderPayment(pobj);
            if (!pobj.isException)
            {
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string CheckHoldSecurity(string datavalue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(datavalue);
        PL_CustomerNew pobj = new PL_CustomerNew();
        try
        {
            if (HttpContext.Current.Session["EmpTypeNo"] != null)
            {
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpTypeNo"].ToString());
                pobj.SecurityKey = jdv["Security"];
                pobj.Opcode = 59;
                DL_CustomerNew.checkHoldSecurity(pobj);
                if (!pobj.isException)
                {

                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "SessionExpired";

            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string InsertBilling(string dataValue)
    {
        PL_CustomerNew pobj = new PL_CustomerNew();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.Opcode = 101;
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.Address = jdv["BillAdd"];
                pobj.Address2 = jdv["BillAdd2"];
                pobj.IsDefault = Convert.ToInt32(jdv["IsDefault"]);
                pobj.City = jdv["City"];
                pobj.State = jdv["State"];
                pobj.Zipcode1 = jdv["Zipcode"];
                pobj.latitude = jdv["latitude"];
                pobj.longitude = jdv["longitude"];
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                DL_CustomerNew.BillingDetails(pobj);
                if(!pobj.isException)
                {
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }

            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }    
    }

    [WebMethod(EnableSession = true)]
    public static string GetBilling(string datavalue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(datavalue);
        PL_CustomerNew pobj = new PL_CustomerNew();
        try
        {
            if (HttpContext.Current.Session["EmpTypeNo"] != null)
            {
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.Opcode = 403;
                DL_CustomerNew.BillingDetails(pobj);
                if (!pobj.isException)
                {

                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "SessionExpired";

            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string EditBilling(string AutoId)
    {       
        PL_CustomerNew pobj = new PL_CustomerNew();
        try
        {
            if (HttpContext.Current.Session["EmpTypeNo"] != null)
            {
                pobj.AutoId = Convert.ToInt32(AutoId);               
                pobj.Opcode = 401;
                DL_CustomerNew.BillingDetails(pobj);
                if (!pobj.isException)
                {

                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "SessionExpired";

            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string UpdateBilling(string dataValue)
    {
        PL_CustomerNew pobj = new PL_CustomerNew();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.Opcode = 201;
                pobj.AutoId = Convert.ToInt32(jdv["AutoId"]);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.Address = jdv["BillAdd"];
                pobj.Address2 = jdv["BillAdd2"];
                pobj.City = jdv["City"];
                pobj.IsDefault = Convert.ToInt32(jdv["IsDefault"]);
                pobj.State = jdv["State"];
                pobj.Zipcode1 = jdv["Zipcode"];
                pobj.latitude = jdv["latitude"];
                pobj.longitude = jdv["longitude"];
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                DL_CustomerNew.BillingDetails(pobj);
                if (!pobj.isException)
                {
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }

            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public static string DeleteBilling(string AutoId)
    {
        PL_CustomerNew pobj = new PL_CustomerNew();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
               
                pobj.Opcode = 301;
                pobj.AutoId = Convert.ToInt32(AutoId);
                DL_CustomerNew.BillingDetails(pobj);
                if (!pobj.isException)
                {
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }

            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod(EnableSession = true)]
    public static string InsertShipping(string dataValue)
    {
        PL_CustomerNew pobj = new PL_CustomerNew();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.Opcode = 102;
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.Address = jdv["BillAdd"];
                pobj.Address2 = jdv["BillAdd2"];
                pobj.IsDefault = Convert.ToInt32(jdv["IsDefault"]);
                pobj.City = jdv["City"];
                pobj.State = jdv["State"];
                pobj.Zipcode1 = jdv["Zipcode"];
                pobj.latitude = jdv["latitude"];
                pobj.longitude = jdv["longitude"];
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                DL_CustomerNew.ShippingDetails(pobj);
                if (!pobj.isException)
                {
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }

            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod(EnableSession = true)]
    public static string GetShipping(string datavalue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(datavalue);
        PL_CustomerNew pobj = new PL_CustomerNew();
        try
        {
            if (HttpContext.Current.Session["EmpTypeNo"] != null)
            {
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.Opcode = 404;
                DL_CustomerNew.ShippingDetails(pobj);
                if (!pobj.isException)
                {

                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "SessionExpired";

            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string EditShipping(string AutoId)
    {
        PL_CustomerNew pobj = new PL_CustomerNew();
        try
        {
            if (HttpContext.Current.Session["EmpTypeNo"] != null)
            {
                pobj.AutoId = Convert.ToInt32(AutoId);
                pobj.Opcode = 402;
                DL_CustomerNew.ShippingDetails(pobj);
                if (!pobj.isException)
                {

                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "SessionExpired";

            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string UpdateShipping(string dataValue)
    {
        PL_CustomerNew pobj = new PL_CustomerNew();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.Opcode = 202;
                pobj.AutoId = Convert.ToInt32(jdv["AutoId"]);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.Address = jdv["BillAdd"];
                pobj.Address2 = jdv["BillAdd2"];
                pobj.IsDefault = Convert.ToInt32(jdv["IsDefault"]);
                pobj.City = jdv["City"];
                pobj.State = jdv["State"];
                pobj.Zipcode1 = jdv["Zipcode"];
                pobj.latitude = jdv["latitude"];
                pobj.longitude = jdv["longitude"];
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                DL_CustomerNew.ShippingDetails(pobj);
                if (!pobj.isException)
                {
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }

            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public static string DeleteShipping(string AutoId)
    {
        PL_CustomerNew pobj = new PL_CustomerNew();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {

                pobj.Opcode = 302;
                pobj.AutoId = Convert.ToInt32(AutoId);
                DL_CustomerNew.ShippingDetails(pobj);
                if (!pobj.isException)
                {
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }

            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
}