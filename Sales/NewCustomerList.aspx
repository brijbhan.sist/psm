﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/MasterPage.master" AutoEventWireup="true" CodeFile="NewCustomerList.aspx.cs" Inherits="Sales_NewCustomerList" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <style>
      .Pager{
          text-align:right;
      }
  </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Customer List</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage Customer</a></li>
                        <li class="breadcrumb-item">Customer List</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12" id="divAction" style="display:none">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" id="btnAddNewCust" visible="false" onclick="location.href='/Sales/customerMaster.aspx'" runat="server">Add Customer</button> 
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area">


            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row ">
                                       <div class="col-md-3  form-group">
                                            <input type="text" id="txtOrderNo" class="form-control border-primary input-sm" placeholder="Order No."/>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Customer ID" id="txtSCustomerId" />
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Customer Name" id="txtSCustomerName" />
                                    </div>
                                    <div class="col-md-3 form-group" id="dlSalesPerson" runat="server">
                                        <select class="form-control border-primary input-sm" id="ddlSSalesPerson" runat="server" style="width:100%;">
                                            <option value="0">All Sales Person</option>
                                        </select>
                                    </div>
                                
                                    
                                </div>
                                <div class="row">
                                        <div class="col-md-3 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlCustomerType" style="width:100%;">
                                            <option selected="selected" value="0">All Customer Type</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlState" style="width: 100%;">
                                            <option selected="selected" value="0">All State</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlCity" style="width:100%;">
                                            <option selected="selected" value="0">All City</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlStatus" style="width:100%;">
                                            <option selected="selected" value="2">All Status</option>
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                    </div>
                                    <%--   <div class="col-md-3  form-group">
                                            <input type="text" id="txtOrderNo" class="form-control border-primary input-sm" placeholder="Order No."/>
                                    </div>--%>
                                    <div id="Div1" class="form-group" runat="server">
                                        <input type="hidden" id="hidnEmpType" runat="server" />

                                    </div>
                                      </div>
                                   <div class="row">
                                        <div class="col-md-12 form-group">
                                        <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1  btn-sm" id="btnSearch">Search</button>
                                    </div>
                                   </div>
                              
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblCustomerList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="Action text-center width3per">Action</td>
                                                        <td class="Status text-center width3per">Status</td>
                                                        <td class="CustId text-center width3per">Customer ID</td>
                                                        <td class="CustName">Customer Name</td>
                                                        <td class="ContactPersonName">Contact Person</td>
                                                        <td class="Email">Email</td>
                                                        <td class="Contact text-center">Mobile</td>
                                                        <td class="StoreCreditAmount price">Store Credit</td>
                                                        <td class="TotalOrder text-center width3per">Total Order</td>
                                                        <td class="OrderAmount price width3per">Order Amount</td>
                                                        <td class="PaidAmount price width3per">Paid Amount</td>
                                                        <td class="DueAmount price width3per">Due Amount</td>
                                                        <td class="SalesPerson">Sales Person</td>
                                                        <td class="LastOrderDate text-center">Last Order Date</td>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex">
                                    <div class="form-group">
                                        <select class="form-control border-primary input-sm" id="ddlPageSize">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="ml-auto">
                                        <div class="Pager"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
     <script type="text/javascript">
         document.write('<scr' + 'ipt type="text/javascript" src="/Sales/JS/NewCustomerList.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
   
</asp:Content>

