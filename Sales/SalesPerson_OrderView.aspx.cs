﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DLLSalesPerson_OrderView;

public partial class Sales_SalesPerson_OrderView : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
            string text = File.ReadAllText(Server.MapPath("/Sales/JS/SalesPerson_OrderView.js"));
            Page.Header.Controls.Add(
                new LiteralControl(
                     "<script id='checksdrivRequiredFields'>" + text + "</script>"
                    ));
        }
    }
    [WebMethod(EnableSession = true)]
    public static string getOrderData(string OrderAutoId)
    {
        PL_SalesPerson_OrderMaster pobj = new PL_SalesPerson_OrderMaster();
        try
        {
            pobj.OrderAutoId =Convert.ToInt32(OrderAutoId) ;
            BL_SalesPerson_OrderMaster.getOrderData(pobj);
            if (!pobj.isException)
            {
                
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
}