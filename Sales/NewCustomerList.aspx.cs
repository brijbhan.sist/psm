﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Sales_NewCustomerList : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["EmpTypeNo"].ToString() != "2")
            {
                dlSalesPerson.Visible = true;
            }
            else
            {
                dlSalesPerson.Visible = false;
            }
            hidnEmpType.Value = Session["EmpType"].ToString();

            if (Session["EmpTypeNo"].ToString() == "8" || Session["EmpTypeNo"].ToString() == "1")
            {
                btnAddNewCust.Visible = true;
            } 
            
             
            
        }
        catch
        {
            Session.Abandon();
            Response.Redirect("~/Default.aspx", false);
        }
    }
}