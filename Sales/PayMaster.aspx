﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/MasterPage.master" ClientIDMode="Static" CodeFile="~/Sales/PayMaster.aspx.cs" Inherits="Sales_Paymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <style>
        .zoom:hover {
            -ms-transform: scale(3); /* IE 9 */
            -webkit-transform: scale(3); /* Safari 3-8 */
            transform: scale(3); 
        }
        .table th, .table td {
            padding: 4px !important;
        }
        .table tbody td {
    padding: 4px !important;
    vertical-align: middle !important;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Receive Payment</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Receive Payment
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body" style="min-height:750px">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header" style="padding-left: 14px !important;">
                            <h4 class="card-title">Payment Details</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Payment ID</label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <input type="text" class="form-control border-primary input-sm" id="txtPayId" readonly="readonly" />
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Customer Name<span class="required"> *</span> </label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <select id="ddlCustomer" class="form-control border-primary input-sm ddlreq" runat="server" onchange="ViewDueAmount()">
                                                    <option value="0">-Select Customer-</option>
                                                </select>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Due Amount</label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding: 0rem .75rem;">
                                                            <span id="spDueAmount" class="la la-eye"></span>
                                                        </span>
                                                    </div>
                                                    <input type="text" id="txtDueAmount" class="form-control border-primary input-sm" style="text-align:right" readonly="true" value="0.00" onkeypress="return isNumberDecimalKey(event,this)" runat="server" onfocus="this.select()" />

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Receive Date <span class="required">*</span></label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                                            <span class="la la-calendar-o"></span>
                                                        </span>
                                                    </div>
                                                    <input type="text" id="txtReceiveDate" class="form-control border-primary input-sm req" runat="server" onfocus="this.select()" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">

                                                <label class="control-label">Receive Amount<span class="required"> *</span> </label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                                            <span>$</span>
                                                        </span>
                                                    </div>
                                                    <input type="text" id="txtReceiveAmount" onkeypress="return isNumberKey(event)" class="form-control border-primary input-sm req" maxlength="8" style="text-align: right" runat="server" value="0.00" onfocus="this.select()" />
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Payment Mode <span class="required">*</span></label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <select id="ddlPaymentMode" class="form-control input-sm border-primary ddlsreq" runat="server">
                                                    <option value="0">-Select Payment Mode-</option>
                                                </select>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Reference ID</label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <input type="text" id="ReferenceId" maxlength="50" class="form-control border-primary input-sm" />
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Remark</label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <textarea id="txtRemarks" maxlength="500" class="form-control border-primary input-sm"></textarea>
                                                <span style="color:red;"><i>[Note : Remark upto 500 characters]</i></span>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                 <div class="row" id="hdnorderlist" style="display:none">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered" id="tblOrderList">
                                            <thead class="bg-blue white">
                                                <tr>
                                                    <td class="SrNo text-center"><input type="checkbox" id="check-all" /> Action</td>
                                                    <td class="AutoId text-center" style="display:none">AutoId</td>
                                                    <td class="OrderNo  text-center">Order No.</td>
                                                    <td class="OrderDate  text-center">Order Date</td>
                                                    <td class="Status  text-center">Status</td>
                                                    <td class="OrderAmount price">Order Amount</td>
                                                    <td class="PaidAmount price">Paid Amount</td>
                                                    <td class="AmtDue price">Due Amount</td>
                                                    
                                                   
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                            <tfoot>
                                                <tr style="font-weight: bold;">
                                                    <td colspan="6" style="text-align: right">Total</td>
                                                    <td id="T_DueAmt" class="right" style="text-align: right">0.00</td>
                                                </tr>

                                            </tfoot>
                                        </table>
                                      
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnSave" onclick="SavePayAmount()">Save</button>
                                    </div>
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" class="btn btn-secondary  buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnReset" onclick="resetData()">Reset</button>
                                    </div>
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnUpdate" style="display: none" onclick="UpdatePayAmount()">Update</button>
                                    </div>
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" class="btn btn-warning buttonAnimation pull-right round box-shadow-1 btn-sm" data-animation="pulse" id="btnCancel" style="display: none" onclick="resetData()">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header" style="padding-left: 14px !important;">
                            <h4 class="card-title">Payment List</h4>
                        </div>

                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">

                                    <div class="col-md-3 form-group">
                                        <select id="ddlSCustomer" class="form-control border-primary input-sm" runat="server">
                                            <option value="0">All Customer</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group" style="display: none">
                                        <select class="form-control border-primary input-sm" id="ddlSStatus">
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Payment ID" id="txtSPayId" />
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    Receive From Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" placeholder="From Date" id="txtSFromDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    Receive To Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" placeholder="To Date" id="txtSToDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnSearch" onclick="getPayList(1)">Search</button>
                                    </div>
                                </div>

                            <input type="hidden" id="PaymentAutoId" />
                                <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered" id="tblPayDetails">
                                            <thead class="bg-blue white">
                                                <tr>
                                                    <td class="action text-center">Action</td>
                                                    <td class="PayID  text-center">Payment ID</td>
                                                    <td class="ReceiveDate  text-center">Receive Date</td>
                                                    <td class="CustomerName">Customer  Name</td>
                                                    <td class="ReceivedAmount price">Receive Amount ($)</td>
                                                    <td class="PaymentMode  text-center">Payment Mode</td>
                                                    <td class="ReferenceId width6per">Reference ID</td>
                                                    <td class="Remarks width6per">Remark</td>
                                                    <td class="Status  text-center">Status</td>

                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>

                                        </table>
                                        <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                    </div>
                                </div>
                                </div>
                                  <div class="row">
                                    <div class="col-md-2">
                                        <select class="form-control input-sm border-primary pagesize" id="ddlPagesize" onchange="getPayList(1);">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager text-right"></div>
                                    </div>
                                </div>
                            </div>
                    </div>

                </div>
            </div>
    </div>

        </section>
    </div>
    <div id="CustomerDueAmount" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-md-4">
                                <h4 class="card-title">Customer Due Amount</h4></div>
                                      <div class="col-md-8">
                                            [ <span id="spCustomerName"></span> ]  
                                      </div>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered" id="tblduePayment">
                                            <thead class="bg-blue white">
                                                <tr>
                                                    <td class="SrNo text-center">SN</td>
                                                    <td class="OrderNo text-center">Order No</td>
                                                    <td class="OrderDate text-center">Order Date</td>
                                                    <td class="AmtDue text-center">Amt Due ($)</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                            
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="btn-group mr-1 pull-right">
                                            <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1  btn-sm" data-dismiss="modal">Close</button>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="js/PayReceivedMaster.js?v=' + new Date() + '"></scr' + 'ipt>');
        $('#txtReceiveAmount').on('paste', function (event) {
            if (event.originalEvent.clipboardData.getData('Text').match(/[^\d]/)) {
                event.preventDefault();
            }
        });
    </script>
</asp:Content>
