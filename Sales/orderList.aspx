﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/MasterPage.master" AutoEventWireup="true" CodeFile="orderList.aspx.cs" Inherits="Sales_orderList" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .StatusRTS {
            BACKGROUND-COLOR: #1166b1;
            COLOR: WHITE;
            PADDING: 3PX;
            BORDER-RADIUS: 5PX;
        }

        .addon {
            background-color: green;
            color: white;
            padding: 5px;
            border-radius: 5px;
        }

        .input-group-text {
            padding: 0 1rem;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Order List</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Orders</a>
                        </li>
                        <li class="breadcrumb-item">Order List
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12" id="Action">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <%
                    if (Session["EmpAutoId"] != null)
                    {
                        if (Session["EmpTypeNo"].ToString() == "2")
                        {
                %>
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <%
                        }
                    }
                %>

                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a href="/sales/orderMaster.aspx" class="dropdown-item" id="linktoOrderList" style="display: none">Add new Order </a>
                    <button type="button" class="dropdown-item" id="btnExport" style="display: none">Export</button>
                    <input type="hidden" id="hiddenPackerAutoId" runat="server" />
                    <input type="hidden" id="hiddenEmpType" runat="server" />
                </div>
            </div>
        </div>
    </div>

    <div class="content-body" style="min-height: 400px">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-3">
                                        <select class="form-control border-primary input-sm" id="ddlCustomer" runat="server">
                                            <option value="0">All Customer</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-3" style="display: none" id="SalesPerson">
                                        <select class="form-control border-primary input-sm" id="ddlSalesPerson" runat="server">
                                            <option value="0">All Sales Person</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-3 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlSStatus">
                                            <option value="0">All Status</option>
                                        </select>
                                    </div>
                                    <%--<div class="col-sm-12 col-md-3" id="col3"></div>--%>

                                    <div class="col-sm-12 col-md-3">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Order No" id="txtSOrderNo" onfocus="this.select()" />
                                    </div>
                                    <div class="col-sm-12 col-md-3 form-group" id="col4" style="display: none"></div>
                                    <div class="col-md-3 col-sm-12">
                                        <select class="form-control border-primary input-sm" id="ddlShippingType">
                                            <option value="0">All Shipping Type</option>
                                        </select>
                                    </div>

                                    <div class="col-sm-12 col-md-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">From Order Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" placeholder="From Date" id="txtSFromDate" onfocus="this.select()" onchange="setdatevalidation(1);" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">To Order Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" placeholder="To Date" id="txtSToDate" onfocus="this.select()" onchange="setdatevalidation(2);" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" id="btnSearch">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblOrderList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="action text-center">Action</td>
                                                        <td class="status  text-center">Status</td>
                                                        <td class="orderNo  text-center">Order No</td>
                                                        <td class="orderDt  text-center">Order Date</td>
                                                        <td class="SalesPerson">Sales Person</td>
                                                        <td class="cust">Customer</td>
                                                        <td class="value price">Order Amount</td>
                                                        <td class="product  text-center">Products</td>
                                                        <td class="Shipping">Shipping Type</td>
                                                        <td class="CreditMemo" style="display: none">Credit Memo</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex">
                                    <div class="form-group">
                                        <select id="ddlPageSize" class="form-control border-primary input-sm">
                                            <option selected="selected" value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="ml-auto">
                                        <div class="Pager"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <table class="table table-striped table-bordered" id="tblExport" style="display: none">
        <thead>
            <tr>
                <td class="orderNo">Order No</td>
                <td class="orderDt">Order Date</td>
                <td class="SalesPerson">Sales Person</td>
                <td class="cust">Customer</td>
                <td class="value">Order Amount</td>
                <td class="product">Products(Nos.)</td>
                <td class="status">Status</td>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <!-- Modal -->
    <div id="modalOrderLog" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row" style="width: 100% !important">
                        <div class="col-md-6">
                            <h4 class="modal-title">Order Log</h4>
                        </div>
                        <div class="col-md-6">
                            <label><b>Order No </b>&nbsp;:&nbsp;<span id="lblOrderNo"></span>&nbsp;&nbsp;<b>Order Date</b>&nbsp;:&nbsp;<span id="lblOrderDate"></span></label>
                        </div>
                    </div>

                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="tblOrderLog">
                            <thead class="bg-blue white">
                                <tr>
                                    <td class="SrNo text-center">SN</td>
                                    <td class="ActionBy">Action By</td>
                                    <td class="Date text-center">Date</td>
                                    <td class="Action">Action</td>
                                    <td class="Remark" style="white-space: normal;">Remark</td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="/Sales/JS/OrderList.js?v=' + new Date() + '"></sc' + 'ript>');
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>

