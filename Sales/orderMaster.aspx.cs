﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Sales_orderMaster : System.Web.UI.Page
{


    protected void Page_load(object sender, EventArgs e)
    {
        try
        {
            if (Request.QueryString["OrderNo"] == null && (Session["EmpTypeNo"].ToString() != "2"))
            {
                Session.Abandon();
                Response.Redirect("~/Default.aspx", false);
            }

            if (Session["EmpTypeNo"].ToString() == "8")
            {
                linktoOrderList.Attributes.Add("href", "/Manager/Manager_orderList.aspx");
            }
            if (Session["EmpTypeNo"].ToString() == "3")
            {
                Response.Redirect("/Packer/Packer_orderList.aspx");
            }
            if (Session["EmpTypeNo"].ToString() == "3")
            {
                Response.Redirect("/Packer/Packer_orderList.aspx");
            }
            if (Session["EmpTypeNo"].ToString() == "7")
            {
                btnGenBar.Visible = false;
                btnSetAsProcess.Visible = false;
                btnGenOrderCC.Visible = false;  
            }
            hiddenEmpTypeVal.Value = Session["EmpTypeNo"].ToString();
        }
        catch
        {
            Session.Abandon();
            Response.Redirect("~/Default.aspx", false);
        }


        if (Session["DBLocation"] != null)
        {

            HDDomain.Value = Session["DBLocation"].ToString();
        }
        else
        {
            Response.Redirect("/");
        } 
    }
}