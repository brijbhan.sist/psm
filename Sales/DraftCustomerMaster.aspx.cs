﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DLLDraftCustomerMaster;
public partial class Sales_DraftCustomerMaster : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Sales/JS/DraftCustomerMaster.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredFields'>" + text + "</script>"
                ));
        }
    }
    [WebMethod(EnableSession = true)]
    public static string bindDropDownsList()
    {
        PL_DraftCustomerMaster pobj = new PL_DraftCustomerMaster();

        BL_DraftCustomerMaster.bindDropDownsList(pobj);
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            if (!pobj.isException)
            {
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                return json;
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string BindpriceLevel(string DataValues)
    {
        PL_DraftCustomerMaster pobj = new PL_DraftCustomerMaster();
        pobj.CustType = Convert.ToInt32(DataValues);
        BL_DraftCustomerMaster.bindPriceLevel(pobj);
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            if (!pobj.isException)
            {
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                if (json == "")
                {
                    json = "[]";
                }
                return json;
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string checkZipcode(string ZipCode)
    {
        PL_DraftCustomerMaster pobj = new PL_DraftCustomerMaster();
        pobj.ZipCode = ZipCode;
        BL_DraftCustomerMaster.checkZipcode(pobj);
        if (!pobj.isException)
        {
            return pobj.Ds.GetXml();
        }
        else
        {
            return pobj.exceptionMessage;
        }

    }
    [WebMethod(EnableSession = true)]
    public static string bindContactPersonType()
    {
        PL_DraftCustomerMaster pobj = new PL_DraftCustomerMaster();

        BL_DraftCustomerMaster.bindContactPersonType(pobj);
        if (HttpContext.Current.Session["EmpTypeNo"] != null)
        {
            if (!pobj.isException)
            {
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string insertCustomer(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_DraftCustomerMaster pobj = new PL_DraftCustomerMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.XmlContactPerson = jdv["Xmlcontperson"];
                pobj.CustomerName = jdv["CustomerName"];
                pobj.BusinessName = jdv["BusinessName"];
                pobj.OPTLicence = jdv["OPTLicence"];
                pobj.CustType = jdv["CustType"];

                pobj.BillAdd = jdv["BillAdd"];
                pobj.BillAdd2 = jdv["BillAdd2"];
                pobj.Zipcode1 = jdv["Zipcode1"];
                pobj.State1= jdv["State1"];
                pobj.City1= jdv["City1"];
                pobj.latitude = jdv["latitude"];
                pobj.longitude = jdv["longitude"];

                pobj.ShipAdd = jdv["ShipAdd"];
                pobj.ShipAdd2 = jdv["ShipAdd2"];
                pobj.Zipcode2 = jdv["Zipcode2"];
                pobj.State2 = jdv["State2"];
                pobj.City2 = jdv["City2"];
                pobj.latitude1 = jdv["latitude1"];
                pobj.longitude1 = jdv["longitude1"];

                pobj.TaxId = jdv["TaxId"];
                pobj.Terms = jdv["Terms"];
                pobj.PriceLevelAutoId = jdv["PriceLevelAutoId"];
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                pobj.Status = jdv["Status"];
                pobj.StoreOpenTime = jdv["StoreOpenTime"];
                pobj.StoreCloseTime = jdv["StoreCloseTime"];
                pobj.CustomerRemark = jdv["CustomerRemarks"];
                if (HttpContext.Current.Session["EmpTypeNo"].ToString() != "2")
                {
                    pobj.SalesPersonAutoId = jdv["SalesPersonAutoId"];
                }
                else
                {
                    pobj.SalesPersonAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                }

                BL_DraftCustomerMaster.insert(pobj);
                if (!pobj.isException)
                {                  
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public static string editCustomer(string CustomerId)
    {
        PL_DraftCustomerMaster pobj = new PL_DraftCustomerMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.CustomerAutoId = Convert.ToInt32(CustomerId);
                BL_DraftCustomerMaster.Edit(pobj);
                if (!pobj.isException)
                {
                    string json = "";
                    foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                    {
                        json += dr[0];
                    }
                    return json;
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public static string EditContactPerson(string CustomerId)
    {
        PL_DraftCustomerMaster pobj = new PL_DraftCustomerMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.CustomerAutoId = Convert.ToInt32(CustomerId);
                BL_DraftCustomerMaster.EditContactPerson(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public static string UpdateCustomer(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_DraftCustomerMaster pobj = new PL_DraftCustomerMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.XmlContactPerson = jdv["Xmlcontperson"];
                pobj.GenStatus= Convert.ToInt32(jdv["GenStatus"]);
                pobj.BillAddAutoId= Convert.ToInt32(jdv["BillAutoId"]);
                pobj.ShipAddAutoId= Convert.ToInt32(jdv["ShipAutoId"]);
                pobj.CustomerAutoId= Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.CustomerName = jdv["CustomerName"];
                pobj.BusinessName = jdv["BusinessName"];
                pobj.OPTLicence = jdv["OPTLicence"];
                if (jdv["CustType"] == null)
                {
                    return "Customer Type required";
                }
                pobj.CustType = jdv["CustType"];
                pobj.BillAdd = jdv["BillAdd"];
                pobj.BillAdd2 = jdv["BillAdd2"];
                pobj.Zipcode1 = jdv["Zipcode1"];
                pobj.State1 = jdv["State1"];
                pobj.City1 = jdv["City1"];
                pobj.latitude = jdv["latitude"];
                pobj.longitude = jdv["longitude"];

                pobj.ShipAdd = jdv["ShipAdd"];
                pobj.ShipAdd2 = jdv["ShipAdd2"];
                pobj.Zipcode2 = jdv["Zipcode2"];
                pobj.State2 = jdv["State2"];
                pobj.City2 = jdv["City2"];
                pobj.latitude1 = jdv["latitude1"];
                pobj.longitude1 = jdv["longitude1"];
                pobj.TaxId = jdv["TaxId"];
                if (jdv["Terms"] == null)
                {
                    return "Terms required";
                }
                pobj.Terms = jdv["Terms"];
                
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                if (jdv["Status"] == null)
                {
                    return "Status required";
                }
                pobj.Status = jdv["Status"];
                if (jdv["StoreOpenTime"] == null)
                {
                    return "Store Open Time required";
                }
                pobj.StoreOpenTime = jdv["StoreOpenTime"];
                if (jdv["StoreCloseTime"] == null)
                {
                    return "Store Close Time required";
                }
                pobj.StoreCloseTime = jdv["StoreCloseTime"];
                pobj.CustomerRemark = jdv["CustomerRemarks"];
                if(jdv["SalesPersonAutoId"]==null)
                {
                    return "Sales Person required";
                }
                if (jdv["PriceLevelAutoId"] == null)
                {
                    return "Price Level required";
                }
                pobj.PriceLevelAutoId = jdv["PriceLevelAutoId"];
                if (HttpContext.Current.Session["EmpTypeNo"].ToString() != "2")
                {
                    pobj.SalesPersonAutoId = jdv["SalesPersonAutoId"];
                }
                else
                {
                    pobj.SalesPersonAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                }

                BL_DraftCustomerMaster.update(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
}
