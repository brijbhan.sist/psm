﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Sales_customerMaster : System.Web.UI.Page
{
    protected void Page_load(object sender, EventArgs e)
    {
        try
        {
            if (Session["EmpTypeNo"].ToString() == "1" || Session["EmpTypeNo"].ToString() == "6" || Session["EmpTypeNo"].ToString() == "8")
            {
                rowSalesPerson.Visible = true;
            }
            else
            {
                rowSalesPerson.Visible = false;
            }
        }
        catch
        {
            Session.Abandon();
            Response.Redirect("~/Default.aspx", false);
        }
    }
}