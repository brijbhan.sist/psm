﻿<%@ WebHandler Language="C#" Class="UploadDocument" %>

using System;
using System.Web;
using System.IO;
using System.Data;
using Newtonsoft.Json;
public class UploadDocument : IHttpHandler {

    public void ProcessRequest (HttpContext context)
    {
        DataTable dt = new DataTable();
        dt.Clear();
        dt.Columns.Add("URL");

        if (context.Request.Files.Count > 0)
        {
            HttpFileCollection files = context.Request.Files;
            for (int i = 0; i < files.Count; i++)
            {
                HttpPostedFile file = files[i];
                string fname;
                if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE" || HttpContext.Current.Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                {
                    string[] testfiles = file.FileName.Split(new char[] { '\\' });
                    fname = testfiles[testfiles.Length - 1];
                }
                else
                {
                    fname = file.FileName;
                }
                string[] getextension = fname.Split('.');
                string dirFullPath = HttpContext.Current.Server.MapPath("~/UploadDocument/");

                string date = DateTime.Now.ToString("yyyyMMddHHmm");
                string timeStamp = context.Request.QueryString["Id"].ToString();

                string fnamefull = dirFullPath + timeStamp + date + "." + getextension[1];

                file.SaveAs(fnamefull);
                context.Response.ContentType = "text/plain";
                context.Response.Write(timeStamp + date + "." + getextension[1]);
            }
        }

    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}