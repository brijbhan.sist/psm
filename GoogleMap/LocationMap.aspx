﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LocationMap.aspx.cs" Inherits="GoogleMap_LocationMap" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.2.0/dist/leaflet.css" />

    <style>
        .map {
            position: absolute;
            width: 100%;
            height: 100%;
        }

        .leaflet-popup-content {
            width: 200px;
        }

        .leaflet-popup {
            bottom: 53px !important;
        }

        .leaflet-popup-content img {
            width: 80px;
        }

        .leaflet-marker-icon img {
            width: 60px;
        }

        .leaflet-marker-icon span {
            position: absolute;
            font-weight: bold;
            font-size: 1.2rem;
            top: 7px;
            left: -5px;
        }

        .leaflet-marker-icon {
            margin-left: -6px;
            margin-top: -68px !important;
            width: 12px;
            height: 12px;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <div id="map" class="map"></div>


        <script>
            function initMap() { 
                var service = new google.maps.DirectionsService;
                var map = new google.maps.Map(document.getElementById('map'));
                window.gMap = map;
                wp = JSON.parse(localStorage.getItem('GoogleMapTest'));
                var St_Lat = Number(localStorage.getItem('CompanyStart_Lat'));
                var St_Long = Number(localStorage.getItem('CompanyStart_Long'));
                var lt = { lat: St_Lat, lng: St_Long, stopNumber: "Start", orderNo: "Start Location" };
                wp.unshift(lt);
                wp.push(lt); 
                // Zoom and center map automatically by stations (each station will be in visible map area)
                var lngs = wp.map(function (station) { return Number(station.lng); });
                var lats = wp.map(function (station) { return Number(station.lat); });
                map.fitBounds({
                    west: Math.min.apply(null, lngs),
                    east: Math.max.apply(null, lngs),
                    north: Math.min.apply(null, lats),
                    south: Math.max.apply(null, lats),
                });
                var marker;
                // Show stations on the map as markers
                var infowindow;
                for (var i = 0; i < wp.length; i++) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(Number(wp[i].lat), Number(wp[i].lng)),
                        map: map,
                        label: wp[i].stopNumber + '',
                        //title: wp[i].orderNo
                    });
                    infowindow = new google.maps.InfoWindow({});
                    google.maps.event.addListener(marker, 'click', (function (marker, i) {
                        return function () {
                            var content = wp[i].orderNo;
                            infowindow.setContent(content);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                }


                // Divide route to several parts because max stations limit is 25 (23 waypoints + 1 origin + 1 destination)
                for (var i = 0, parts = [], max = 8 - 1; i < wp.length; i = i + max) {
                    parts.push(wp.slice(i, i + max + 1));
                } 
                // Service callback to process service results
                var service_callback = function (response, status) {
                    if (status != 'OK') {
                        console.log('Directions request failed due to ' + status);
                        return;
                    }
                    var renderer = new google.maps.DirectionsRenderer;
                    if (!window.gRenderers)
                        window.gRenderers = [];
                    window.gRenderers.push(renderer);
                    renderer.setMap(map);
                    renderer.setOptions({ suppressMarkers: true, preserveViewport: true });
                    renderer.setDirections(response);
                };

                // Send requests to service to get route (for stations count <= 25 only one request will be sent)
                for (var i = 0; i < parts.length; i++) {
                    // Waypoints does not include first station (origin) and last station (destination)
                    var waypoints = [];
                    for (var j = 1; j < parts[i].length - 1; j++)
                        waypoints.push({ location: parts[i][j], stopover: false });
                    // Service options

                    var service_options = {
                        origin: parts[i][0],
                        destination: parts[i][parts[i].length - 1],
                        waypoints: waypoints,
                        provideRouteAlternatives: false,
                        travelMode: 'DRIVING',
                        unitSystem: google.maps.UnitSystem.IMPERIAL
                    };
                    // Send request
                    service.route(service_options, service_callback);
                }
            }
        </script>
        <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyA8Y7FRVyPLlIEvF11qdFiD-ZWsf5OVIjs&callback=initMap"></script>

    </form>
</body>
</html>


