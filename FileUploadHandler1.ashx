﻿<%@ WebHandler Language="C#" Class="FileUploadHandler" %>

using System;
using System.Web;
using System.IO;
using System.Data;
using Newtonsoft.Json;

using System.Drawing;
using System.Drawing.Drawing2D;
public class FileUploadHandler : IHttpHandler
{


    public void ProcessRequest(HttpContext context)
    {
        try
        {
            string timeStamp = context.Request.QueryString["timestamp"].ToString();
            string fname = "";

            DataTable dt = new DataTable();
            dt.Columns.Add("URL");
            if (context.Request.Files.Count > 0)
            {
                #region image
                HttpFileCollection files = context.Request.Files;
                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFile file = files[i];
                    if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE" || HttpContext.Current.Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                    {
                        string[] testfiles = file.FileName.Split(new char[] { '\\' });
                        fname = testfiles[testfiles.Length - 1];
                    }
                    else
                    {
                        fname = file.FileName;
                    }
                    fname = timeStamp + "_" + fname;
                    string[] getextension = fname.Split('.');
                    fname = Path.Combine(context.Server.MapPath("~/MessageImageAttachments"), fname);
                    file.SaveAs(fname);

                    string[] GtUrl = HttpContext.Current.Request.Url.Host.ToString().Split('.');                   
                 
                    DataRow row = dt.NewRow();
                    row["URL"] = getextension[0] + "." + getextension[1]; dt.Rows.Add(row);

                }
                #endregion
            }
            context.Response.ContentType = "text/plain";
            context.Response.Write(JsonConvert.SerializeObject(dt));
            HttpContext.Current.Response.StatusCode = 200;
        }
        catch (Exception ex)
        {
            context.Response.ContentType = "text/plain";
            context.Response.Write(ex.Message);
            HttpContext.Current.Response.StatusCode = 400;
        }
    }
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }


}