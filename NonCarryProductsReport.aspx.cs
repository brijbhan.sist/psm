﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DLLNonCarryProductsReport;
public partial class Reports_NonCarryProductsReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Reports/Js/NonCarryProductsReport.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredField'>" + text + "</script>"
                ));
        }
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static string GetReport(PL_NonCarryProductsReport pobj)
    {
        try
        {                      
            BL_NonCarryProductsReport.Getreport(pobj);
            return pobj.Ds.GetXml();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
}