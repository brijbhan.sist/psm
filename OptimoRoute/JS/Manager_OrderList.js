﻿var OrdAutoId = 0; Driver = 0; OrderStatus = 0;
localStorage.setItem('Driver', '0');
localStorage.setItem('Orderstatus', '0');
var planningDate;
$(document).ready(function () {    
    bindStatus();
    BindDriverDDL();
    getOrderList(1);
    $('#txtSFromDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: false,
        selectMonths: false,
    });
    $('#txtSToDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: false,
        selectMonths: false,
    });
    $('#txtAssignDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: false,
        selectMonths: false,
    });
    $('#txtPlanningDate').pickadate({
        format: 'yyyy-mm-dd',
        formatSubmit: 'yyyy-mm-dd',
        selectYears: false,
        selectMonths: false,
    });
})
function Pagevalue(e) {
    getOrderList(parseInt($(e).attr("page")));
};
function bindStatus() {
    $.ajax({
        type: "POST",
        url: "/Manager/WebAPI/WManager_OrderList.asmx/bindStatus",
        data: "{}",
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {

            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var status = $(xmldoc).find("Table");
                var customer = $(xmldoc).find("Table1");
                var SalesPerson = $(xmldoc).find("Table2");
                var ShippingType = $(xmldoc).find("Table4");
                $("#ddlSStatus option:not(:first)").remove();
                $.each(status, function () {
                    $("#ddlSStatus").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("StatusType").text() + "</option>");
                });
                $("#ddlCustomer option:not(:first)").remove();
                $.each(customer, function () {
                    $("#ddlCustomer").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("Customer").text() + "</option>");
                });
                $("#ddlCustomer").select2();
                $("#ddlSalesPerson option:not(:first)").remove();
                $.each(SalesPerson, function () {
                    $("#ddlSalesPerson").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("EmpName").text() + "</option>");
                });
                $("#ddlSalesPerson").select2();
                $("#ddlSStatus").val('3');

                $("#ddlShippingType option:not(:first)").remove();
                $.each(ShippingType, function () {
                    $("#ddlShippingType").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("ShippingType").text() + "</option>");
                });
                $("#ddlShippingType").attr('multiple', 'multiple')
                $("#ddlShippingType").select2({
                    placeholder: 'All Shipping Type',
                    allowClear: true
                });

            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
$("#ddlPageSize").change(function () {
    getOrderList(1);
});
function getOrderList(pageIndex) {
    $("#check-all").prop('checked', false);
    var ShippingType = '0';
    $("#ddlShippingType option:selected").each(function (i) {
        if (i == 0) {
            ShippingType = $(this).val() + ',';
        } else {
            ShippingType += $(this).val() + ',';
        }
    });

    var data = {
        OrderNo: $("#txtSOrderNo").val(),
        CustomerAutoid: $("#ddlCustomer").val(),
        SalesPersonAutoId: $("#ddlSalesPerson").val(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        Status: $("#ddlSStatus").val(),
        pageIndex: pageIndex,
        PageSize: $("#ddlPageSize").val(),
        ShippingType: ShippingType.toString(),
        Driver: $("#ddlDriver").val(),
    };
    $.ajax({
        type: "POST",
        url: "/Manager/WebAPI/WManager_OrderList.asmx/getOrderList",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var orderList = $(xmldoc).find("Table1");
                if (orderList.length > 0) {
                    $("#EmptyTable").hide();
                    $("#tblOrderList tbody tr").remove();
                    var row = $("#tblOrderList thead tr").clone(true);
                    $.each(orderList, function () {
                        if ($(this).find("ShipId").text() == '2' || $(this).find("ShipId").text() == '7' || $(this).find("ShipId").text() == '4') {
                            $(row).css('background-color', '#ffe6e6');
                        }
                        else if ($(this).find("ShipId").text() == '8') {
                            $(row).css('background-color', '#eb99ff');
                        }
                        else if ($(this).find("ShipId").text() == '9') {
                            $(row).css('background-color', '#e6fff2');
                        }
                        else if ($(this).find("ShipId").text() == '10') {
                            $(row).css('background-color', '#ccff99');
                        }
                        else {
                            $(row).css('background-color', 'white');
                        }
                        $(".Shipping", row).text($(this).find("ShippingType").text());
                        $(".orderNo", row).html("<span OrderAutoId='" + $(this).find("AutoId").text() + "'>" + $(this).find("OrderNo").text() + "</span>");
                        $(".orderDt", row).text($(this).find("OrderDate").text());
                        $(".cust", row).text($(this).find("CustomerName").text());
                        $(".value", row).text($(this).find("GrandTotal").text());
                        $(".product", row).text($(this).find("NoOfItems").text());
                        $(".payableAmt", row).text(parseFloat($(this).find("PayableAmount").text()).toFixed(2));
                        $(".SalesPerson", row).text($(this).find("SalesPerson").text());
                        $(".CreditMemo", row).text($(this).find("CreditMemo").text());
                        $(".OrderAutoId", row).text($(this).find("AutoId").text());
                        $(".Stoppage", row).text($(this).find("StopNo").text());
                        var logView = '';
                        if (Number($(this).find("StatusCode").text()) == 1) {
                            $(".status", row).html("<span class='badge mr-2 badge badge-pill Status_New'>" + $(this).find("Status").text() + "</span>");
                        } else if (Number($(this).find("StatusCode").text()) == 2) {
                            $(".status", row).html("<span class='badge mr-2 badge badge-pill Status_Processed'>" + $(this).find("Status").text() + "</span>");
                        } else if (Number($(this).find("StatusCode").text()) == 3) {
                            logView = '&nbsp;&nbsp;<a href="javascript:void(0)" onclick="BindDriver(this)" class="la la-truck" title="Asign to driver"></a>';
                            $(".status", row).html("<span class='badge badge-pill Status_Packed' style='background-color:#66ff99'>" + $(this).find("Status").text() + "</span>");
                        } else if (Number($(this).find("StatusCode").text()) == 4) {
                            logView = '&nbsp;&nbsp;<a href="javascript:void(0)" onclick="BindDriver(this)" class="la la-truck" title="Change driver"></a>';
                            $(".status", row).html("<span class='badge mr-2 badge badge-pill Status_Ready_to_Ship' style='background-color:#AB47BC'>" + $(this).find("Status").text() + "</span>");
                        } else if (Number($(this).find("StatusCode").text()) == 5) {
                            $(".status", row).html("<span class='badge mr-2 badge badge-pill Status_Shipped'>" + $(this).find("Status").text() + "</span>");
                        } else if (Number($(this).find("StatusCode").text()) == 6) {
                            $(".status", row).html("<span class='badge mr-2 badge badge-pill Status_Delivered'>" + $(this).find("Status").text() + "</span>");
                        } else if (Number($(this).find("StatusCode").text()) == 7) {
                            $(".status", row).html("<span class='badge mr-2 badge badge-pill Status_Undelivered'>" + $(this).find("Status").text() + "</span>");
                        } else if (Number($(this).find("StatusCode").text()) == 8) {
                            $(".status", row).html("<span class='badge mr-2 badge badge-pill Status_cancelled' >" + $(this).find("Status").text() + "</span>");
                        }
                        else if (Number($(this).find("StatusCode").text()) == 9) {
                            logView = '&nbsp;&nbsp;<a href="javascript:void(0)" onclick="BindDriver(this)" class="la la-truck" title="Asign to driver"></a>';
                            $(".status", row).html("<span class='badge mr-2 badge badge-pill Status_Add_On' style='background-color:green'>" + $(this).find("Status").text() + "</span>");
                        }
                        else if (Number($(this).find("StatusCode").text()) == 10) {
                            logView = '&nbsp;&nbsp;<a href="javascript:void(0)" onclick="BindDriver(this)" class="la la-truck" title="Asign to driver"></a>';
                            $(".status", row).html("<span class='badge mr-2 badge badge-pill Status_Add_On_Packed' style='background-color:#61b960'>" + $(this).find("Status").text() + "</span>");
                        }
                        else if (Number($(this).find("StatusCode").text()) == 11) {
                            $(".status", row).html("<span class='badge mr-2 badge badge-pill Status_Close'>" + $(this).find("Status").text() + "</span>");
                        }
                        if (Number($(this).find("StatusCode").text()) == 8) {
                            $(".action", row).html("<input type='checkbox' name='table_records' id='chkProduct' onclick='chkProduct_OnClick(" + $(this).find("AutoId").text() + ");' >&nbsp;&nbsp;<b><a href='/Manager/Manager_viewOrder.aspx?OrderAutoId=" + $(this).find("AutoId").text() + "'><span class='la la-eye' title='View detail'></span></a>&nbsp;<a href='javascript:;' onclick='viewOrderLog(" + $(this).find("AutoId").text() + ")'><span class='la la-history' title='Log'></span></a>&nbsp;'" + logView + "&nbsp;<a href='javascript:;' onclick='SendMail(" + $(this).find("AutoId").text() + ")'><span class='ft-mail' title='Send Mail'></span></a> &nbsp;<a href='javascript:;'><span class='ft-edit' title='Edit'></span></a></b>");
                        } else {
                            $(".action", row).html("<input type='checkbox' name='table_records' id='chkProduct' onclick='chkProduct_OnClick(" + $(this).find("AutoId").text() + ");' >&nbsp;&nbsp;<b><a href='/Manager/Manager_viewOrder.aspx?OrderAutoId=" + $(this).find("AutoId").text() + "'><span class='la la-eye' title='View detail'></span></a>&nbsp;<a href='javascript:;' onclick='viewOrderLog(" + $(this).find("AutoId").text() + ")'><span class='la la-history' title='Log'></span></a>&nbsp;<a href='javascript:;' onclick='PrintOrderPop(" + $(this).find("AutoId").text() + "," + Number($(this).find("StatusCode").text()) + ")'><span class='icon-printer' title='Print'></span></a>" + logView + " &nbsp;<a href='javascript:;' onclick='SendMail(" + $(this).find("AutoId").text() + ")'><span class='ft-mail' title='Send Mail'></span></a> &nbsp;<a href='javascript:;' onclick='showpopupUpdateShipAdd(" + $(this).find("AutoId").text() + ")'><span class='ft-edit' title='Edit'></span></a></b>");
                        }

                        $("#tblOrderList tbody").append(row);
                        row = $("#tblOrderList tbody tr:last").clone(true);
                    });
                } else {
                    $("#EmptyTable").show();
                    $("#tblOrderList tbody tr").remove();
                }

                var pager = $(xmldoc).find("Table");
                $(".Pager").ASPSnippets_Pager({
                    ActiveCssClass: "current",
                    PagerCssClass: "pager",
                    PageIndex: parseInt(pager.find("PageIndex").text()),
                    PageSize: parseInt(pager.find("PageSize").text()),
                    RecordCount: parseInt(pager.find("RecordCount").text())
                });
            } else {
                location.href = "/";
            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function showpopupUpdateShipAdd(OrderAutoId) {
    $("#hdnordAutoId").val(OrderAutoId);
    $("#ShipAddressPopup").modal('show');
    BindshippingAddress();
}
function UpdateShipAdd() {
    if ($('#txtshippingaddress').val() == "") {
        toastr.error('Shipping address is mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        return;
    }
    var data = {
        OrderAutoId: $("#hdnordAutoId").val(),
        ShippingAddress: $('#txtshippingaddress').val(),
    };
    $.ajax({
        type: "POST",
        url: "/Manager/WebAPI/WManager_OrderList.asmx/UpdateShippingAddress",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == 'true') {
                swal('', 'Updated successfully.', 'success');
                $("#ShipAddressPopup").modal('hide');
            }
            else {
                swal('', 'Opps something went wrong.', 'error');
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function BindshippingAddress() {
    $.ajax({
        type: "POST",
        url: "/Manager/WebAPI/WManager_OrderList.asmx/BindshippingAddress",
        data: "{'OrderAutoId':'" + $("#hdnordAutoId").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var shipdetails = $(xmldoc).find("Table");
            debugger;
            if (($(shipdetails).find('OrdShippingAddress').text()) == null || ($(shipdetails).find('OrdShippingAddress').text()) == '') {
                $("#txtshippingaddress").val($(shipdetails).find('shipAdd').text());
            } else {
                $("#txtshippingaddress").val($(shipdetails).find('OrdShippingAddress').text());
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function SendMail(OrderId) {
    $.ajax({
        type: "POST",
        url: "/Manager/WebAPI/WManager_OrderList.asmx/ViewOrderDetails",
        data: "{'OrderAutoId':" + OrderId + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            debugger;
            var xmldoc = $.parseXML(response.d);
            var orderx = $(xmldoc).find("Table");
            var PrintLbl = $(xmldoc).find("Table1");
            $("#lblOrderSNo").text(orderx.find("OrderNo").text());
            $("#lblCustomerName").text(orderx.find("CustomerName").text());
            $("#txtTo").val(orderx.find("Email").text());
            $("#hdnPrintLabel").val(PrintLbl.find("PrintLabel").text());
            var Subject = "Order No : " + orderx.find("OrderNo").text();
            $("#txtSubject").val(Subject);
            var emailBody = '<p>Dear <b>' + orderx.find("CustomerName").text() + ',</b></p>';
            emailBody += '<p>Your <b>ORDER # ' + orderx.find("OrderNo").text() + '</b> for <b>$' + parseFloat(orderx.find("PayableAmount").text()).toFixed(2) + '</b> attached. Please remit payment at your earliest convenience.</p>';
            emailBody += '<p>Thank you for your business - We appreciate it very much.</p>';
            emailBody += '<p>Sincerely ,</p>';
            emailBody += '<p><b>PRICEMART</b></p>';
            emailBody += '<p><b>1(888)51MYPSM</b> ';
            emailBody += '<b><br>1(888)516-9776</b></p>';
            $("#txtEmailBody").summernote('code', emailBody);

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
    $("#modalSendMail").modal('show');
    OrdAutoId = OrderId;
    $("#txtTo").val('');
    $("#txtSubject").val('');
    $("#txtEmailBody").val('');
}

$("#btnSendMail").click(function () {
    debugger;
    if (dynamicALL('reqMail')) {
        if ($("#txtSubject").val() == "") {
            swal({
                title: "Are you sure?",
                text: "You want to Send Mail without Subject",
                icon: "warning",
                showCancelButton: true,
                buttons: {
                    cancel: {
                        text: "No, cancel!",
                        value: null,
                        visible: true,
                        className: "btn-warning",
                        closeModal: false,
                    },
                    confirm: {
                        text: "Yes, Send it!",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: false
                    }
                }
            }).then(function (isConfirm) {
                if (isConfirm) {
                    Sending()
                }
                else {
                    swal("", "Your email will not be sent", "error");
                }
            })
            // Sending();
        }
        else if ($("#txtEmailBody").val() == "") {
            swal({
                title: "Are you sure?",
                text: "You want to send mail without message",
                icon: "warning",
                showCancelButton: true,
                buttons: {
                    cancel: {
                        text: "No, cancel!",
                        value: null,
                        visible: true,
                        className: "btn-warning",
                        closeModal: false,
                    },
                    confirm: {
                        text: "Yes, Send it!",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: false
                    }
                }
            }).then(function (isConfirm) {
                if (isConfirm) {
                    debugger;
                    Sending()
                }
                else {
                    swal("", "Your email will not be sent", "error");
                }
            })
        }
        else {
            Sending();
        }

    } //--------------------------
    else {
        toastr.error('All * fields are mandatory .', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
})
function Sending() {
    var data = {
        To: $('#txtTo').val(),
        Subject: $('#txtSubject').val(),
        EmailBody: $('#txtEmailBody').val(),
        OrderId: OrdAutoId,
        PrintLabel: $('#hdnPrintLabel').val(),
    };
    $.ajax({
        type: "POST",
        url: "/Manager/WebAPI/WManager_OrderList.asmx/getMailBoddy",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == 'true') {
                swal('', 'Email sent successfully.', 'success');
                $("#modalSendMail").modal('hide');
                // OrdAutoId = OrderId;
            } else if (response.d == 'receiverMail') {
                swal('', 'Email address could not be found.', 'error');
            } else if (response.d == 'EmailNotSend') {
                swal('', 'Email address could not be found or was misspelled.', 'error');
            }
            else if (response.d == 'false') {
                swal('', 'Opps something went wrong.', 'error');
            } else if (response.d == 'Session Expired') {
                location.href = '/';
            }
            else {
                swal('', 'Opps something went wrong.', 'error');
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function PrintOrderPop(OrderAutoId, status) {
    $('#txtHOrderAutoId').val(OrderAutoId)
    $('#PopPrintTemplate').modal('show');
    if (Number(status < 3)) {
        $('#PackingSlip').closest('.row').hide();
        $('#WithoutCategorybreakdown').closest('.row').hide();
    } else {
        $('#PackingSlip').closest('.row').show();
        $('#WithoutCategorybreakdown').closest('.row').show();
    }

    if ($('#HDDomain').val() == 'psmpa') {
        $("#divPSMPADefault").show();
        $("#PSMPADefault").prop('checked', true);
    }
    else if ($('#HDDomain').val() == 'psmnpa') {
        $("#divPSMNPADefault").show();
        $("#divPSMPADefault").hide();
        $("#rPSMNPADefault").prop('checked', true);
    }

    else {
        $("#chkdueamount").prop('checked', true);
        $("#PSMPADefault").closest('.row').hide();
    }
}

function PrintOrder() {

    $('#PopPrintTemplate').modal('hide');
    if ($("#chkdueamount").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF1.html?OrderAutoId=" + $("#txtHOrderAutoId").val() + "", "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    } else if ($("#chkdefault").prop('checked') == true) {
        window.open("/Manager/OrderPrintCC.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#Template1").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF2.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#Template2").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF3.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#PSMPADefault").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF4.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#PackingSlip").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF5.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#WithoutCategorybreakdown").prop('checked') == true) {
        window.open("/Manager/WithoutCategorybreakdown.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#rPSMNPADefault").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF4.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#Templ1").prop('checked') == true) {
        window.open("/Manager/PrintOrderItem.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#Templ2").prop('checked') == true) {
        window.open("/Manager/PrintOrderItemTemplate2.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
}
function print_NewOrder(e) {
    window.open("/Packer/OrderPrint.html?OrderAutoId=" + $(e).closest("tr").find(".orderNo span").attr("OrderAutoId"), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    setTimeout(function () {
        getOrderList(1);
    }, 2000);
}

$("#btnSearch").click(function () {
    var Driver = $("#ddlDriver").val();
    var OrderStatus = $("#ddlSStatus").val();
    localStorage.setItem('Driver', Driver);
    localStorage.setItem('Orderstatus', OrderStatus);
    getOrderList(1);
});

function viewOrderLog(OrderAutoId) {
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/WOrderLog.asmx/viewOrderLog",
        data: "{'OrderAutoId':" + OrderAutoId + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var orderlog = $(xmldoc).find("Table");
            var order = $(xmldoc).find("Table1");

            $("#lblOrderNo").text(order.find("OrderNo").text());
            $("#lblOrderDate").text(order.find("OrderDate").text());

            $("#tblOrderLog tbody tr").remove();
            var row = $("#tblOrderLog thead tr").clone(true);
            if (orderlog.length > 0) {
                $("#EmptyTable").hide();
                $.each(orderlog, function (index) {
                    $(".SrNo", row).text(index + 1);
                    $(".ActionBy", row).text($(this).find("EmpName").text());
                    $(".Date", row).text($(this).find("ActionDate").text());
                    $(".Action", row).text($(this).find("Action").text());
                    $(".Remark", row).text($(this).find("Remarks").text());

                    $("#tblOrderLog tbody").append(row);
                    row = $("#tblOrderLog tbody tr:last").clone(true);
                });
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
    $("#modalOrderLog").modal('show');
}


function BindDriver(e) {

    var tr = $(e).closest('tr');
    $('#txtHOrderAutoId').val($(tr).find('.orderNo span').attr('OrderAutoId'));
    $.ajax({
        type: "Post",
        url: "/Manager/WebAPI/WManager_OrderList.asmx/getDriverList",
        data: "{'OrderAutoId':'" + $("#txtHOrderAutoId").val() + "','LoginEmpType':'5'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var Drivers = $(xmldoc).find("Table");
                var selectDrv = $(xmldoc).find("Table1");
                $("#modalMisc").modal("toggle").find(".modal-dialog").removeClass("modal-sm").end()
                    .find(".modal-title").text("Drivers").end()
                    .find("#tblAssignDrv").show().end()
                    .find("#btnAsgn").show().end()
                    .find("#btnPrint").hide()
                    .find('#insideText').hide();
                $('#DriverText').show();
                $('#insideText').text('');
                $('#btnOk').hide();
                $("#tblDriver tbody tr").remove();
                var row = $("#tblDriver thead tr").clone(true);
                var picker = $('#txtAssignDate').pickadate('picker')
                picker.set('min', -7);
                picker.set('select', new Date($(selectDrv).find("AssignDate").text()));
                $("#txtAssignDate").attr("disabled", false);
                $.each(Drivers, function (index) {
                    $(".DrvName", row).html("<span drvautoid =" + $(this).find("AutoId").text() + ">" + $(this).find("Name").text() + "</span>");
                    $(".AsgnOrders", row).html($(this).find("AssignOrders").text());
                    if ($(this).find("AutoId").text() == $(selectDrv).find("Driver").text()) {
                        $(".Select", row).html("<input type='radio' name='driver' class='radio' onchange = 'changeDriver(this)' checked='checked'>");
                        $("#btnAsgn").attr("disabled", true);
                    } else {
                        $(".Select", row).html("<input type='radio' name='driver' class='radio' onchange = 'changeDriver(this)' >");
                    }
                    if (index == 0) {
                        $(row).css('background-color', '#ccff99');
                    }
                    else {
                        $(row).css('background-color', 'white');
                    }
                    $("#tblDriver tbody").append(row);
                    row = $("#tblDriver tbody tr:last").clone(true);
                });
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function assignDriver() {

    if (checksdrivRequiredField()) {
        var drvAutoId, flag = true;
        $("#tblDriver tbody tr").each(function () {
            if ($(this).find("input[name='driver']").is(":checked")) {
                drvAutoId = $(this).find(".DrvName span").attr("drvautoid");
                flag = false;
            }
        });

        var data = {
            DriverAutoId: drvAutoId,
            OrderAutoId: $("#txtHOrderAutoId").val(),
            AssignDate: $("#txtAssignDate").val()
        };

        if (!flag) {
            $.ajax({
                type: "Post",
                url: "/Manager/WebAPI/WManager_OrderList.asmx/AssignDriver",
                data: "{'dataValues':'" + JSON.stringify(data) + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                async: false,
                success: function (response) {
                    if (response.d != "Session Expired") {
                        if (response.d == 'true') {
                            getOrderList(1);
                            swal("", "Driver assigned successfully.", "success"
                            ).then(function () {
                                $("#modalMisc").modal("toggle");
                            });
                        } else {
                            swal("Error!", response.d, "error");
                        }


                    } else {
                        location.href = '/';
                    }

                },
                error: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                },
                failure: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                }
            });
        } else {
            toastr.error('No Driver selected.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        }
    }
    else {
        toastr.error('Assign Date Required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
}

function changeDriver(e) {
    var btnAsgn = $(e).closest(".modal-dialog").find("#btnAsgn");
    $(btnAsgn).attr("disabled", false);

}

function checksdrivRequiredField() {
    debugger;
    var boolcheck = true;
    $('.reqdriv').each(function () {
        if ($(this).val() == '') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });

    return boolcheck;
}
//--------------------------------------------------------------Bind Driver--------------------------------------------
function BindDriverDDL() {
    debugger;
    $.ajax({
        type: "POST",
        url: "/Manager/WebAPI/WManager_OrderList.asmx/BindDriver",
        data: "{}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },

        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var StateDetails = $(xmldoc).find("Table");

            $("#ddlDriver option:not(:first)").remove();
            $.each(StateDetails, function () {
                $("#ddlDriver").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("Driver").text() + "</option>");
            });
            $("#ddlDriver").select2();
        },
        error: function (response) {
            console.log(JSON.parse(response.responseText).d);
        }
    });
}
function chkProduct_OnClick(e) {
    debugger;
    row = $(e).closest('tr');
    if (row.find(".action input").prop('checked') == true) {
        //$("#btnAssignRoot").show();
    }
    else {
        //$("#btnAssignRoot").hide();
    }
}
$("#check-all").change(function () {
    if ($(this).prop("checked")) {
        $("#tblOrderList input:checkbox:not(:disabled)").prop('checked', $(this).prop("checked"));
        //$("#btnAssignRoot").show();
    }
    else {
        $("#tblOrderList input:checkbox").prop('checked', $(this).prop("checked"));
        // $("#btnAssignRoot").hide();
    }
})

$("#btnBulkPrint1").click(function () {
    if (localStorage.getItem('Driver') == '0') {
        toastr.error('No Driver selected.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
    else {
        if (localStorage.getItem('Orderstatus') != '4') {
            toastr.error('Select only Ready to ship order status ', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        }
        else {
            var flag = false; var OrderAutoIds = 0;
            //data = new Array();

            //alert(OrderAutoId.join(", "));
            var OrderAutoId = [];
            $('input:checkbox:checked', '#tblOrderList tbody').each(function (index, item) {
                var row = $(item).closest('tr');
                OrderAutoIds = row.find('.OrderAutoId').text(),
                    OrderAutoId.push(OrderAutoIds);
                flag = true;
            });
            var OAutoId = OrderAutoId.join(",");
            localStorage.setItem('BulkOrderAutoId', OAutoId);
            //if (flag) {

            //    window.open("/Manager/BulkOrderPrint.html", "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
            //} else {
            //    toastr.error('Please check atleast one Order', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            //}
        }
    }
})
$("#btnBulkPrint").click(function () {
    var flag = false; var OrderAutoIds = 0;
    var OrderAutoId = [];
    $('input:checkbox:checked', '#tblOrderList tbody').each(function (index, item) {
        var row = $(item).closest('tr');
        OrderAutoIds = row.find('.OrderAutoId').text(),
            OrderAutoId.push(OrderAutoIds);
        flag = true;
    });
    var OAutoId = OrderAutoId.join(",");
    localStorage.setItem('BulkOrderAutoId', OAutoId);
    if (flag) {
        if (localStorage.getItem('Driver') == '0') {
            toastr.error('No Driver selected.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        }
        else {
            if (localStorage.getItem('Orderstatus') != '4') {
                toastr.error('Select only Ready to ship order status ', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            }
            else {
                window.open("/Manager/BulkOrderPrint.html", "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
            }
        }

    } else {
        toastr.error('Please check atleast one Order', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
})
function actionAssigRoute() {
    var d = new Date();
    planningDate = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
    $("#txtPlanningDate").val(planningDate);
    disableAllDriver();
    PopupRouteOrderList();
    $("#number").val('0');
}
function PopupRouteOrderList() {
    Orders = [];
    $("#tblOrderList tbody tr").each(function (i) {
        if ($(this).find('.action #chkProduct').prop('checked') == true) {          
            Orders.push({
                'OrderAutoId': $(this).find('.orderNo span').attr('orderautoid')
            });
        }
    })
    $.ajax({
        type: "Post",
        url: "/Manager/WebAPI/OptimoRoute.asmx/getOrderAndDriver",
        data: "{'TableValues':'" + JSON.stringify(Orders) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var DriverList = $(xmldoc).find("Table1");
                var RouteTable = $(xmldoc).find("Table"); debugger;
                localStorage.setItem('UnscheduldedOrder', JSON.stringify(response.d));
                $("#tblRouteOrder tbody tr").remove();
                var row = $("#tblRouteOrder thead tr").clone(true);
                if (RouteTable.length > 0) {
                    $("#EmptyTable").hide();
                    $("#popupOptimoOrderDetails").modal('show');
                    $.each(RouteTable, function (index) {
                        $(".SrNo", row).text(index + 1);
                        $(".OrderNo", row).text($(this).find("OrderNo").text());
                        var d = new Date();
                        var strDate = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
                        $(".DelDate", row).html('<input type="text" value="' + strDate + '" class="form-control border-primary input-sm delDate text-center">');
                        $(".Duration", row).html('<input type="text" style="width:50px" value="15" class="border-primary input-sm text-center">');
                        $(".Type", row).html(' <select class="form-control border-primary input-sm"><option value="D">Delivery</option><option value="P">Pick Up</option><option value="T">Task</option></select >');
                        $(".Lat", row).text($(this).find("SA_Lat").text());
                        $(".Long", row).text($(this).find("SA_Long").text());
                        $(".Address", row).text($(this).find("shipAdd").text());
                        $("#tblRouteOrder tbody").append(row);
                        row = $("#tblRouteOrder tbody tr:last").clone(true);
                    });
                }
                $("#tblOrderList tbody tr").each(function (i) {
                    if ($(this).find('.action input').prop('checked') == true) {
                        $(this).find('.action input').prop('checked', false);
                    }
                });
            } else {
                location.href = '/';
            }
        },
        error: function (result) {
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function deleteAll() {
    debugger;
    var APIUrl = "https://api.optimoroute.com/v1/delete_all_orders", Key = "5d928b200fa32bf01e0211417edb3f90oDHOnEILQQ";
    $.ajax({
        type: "Post",
        url: APIUrl + "?key=" + Key,
        data: JSON.stringify({ date: $("#txtPlanningDate").val()}),
        header: { 'content-type': 'application/json' },
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            debugger;
            CreateOptimoOrder();
        }
    })
}
function CreateOptimoOrder() {
    debugger
    var data=[];
    $("#tblRouteOrder tbody tr").each(function (i) {
        console.log(this)
            var Alocation = {
                address: $(this).find('.Address').text(),
                locationName: $(this).find('.Address').text(),
                latitude: Number($(this).find('.Lat').text()),
                longitude: Number($(this).find('.Long').text())
        };
        //var timeWindows=[{
        //    twFrom: "06:00",
        //    twTo: "18:00"
        //}]
            data.push({
                orderNo: $(this).find('.OrderNo').text(),
                date: $("#txtPlanningDate").val(),
                duration: Number($("#txtDuration").val()),
                location: Alocation
                //timeWindows: timeWindows
            })
    });
    var order = {
        orders: data
    }
    var APIUrl = "https://api.optimoroute.com/v1/create_or_update_orders", Key = "5d928b200fa32bf01e0211417edb3f90oDHOnEILQQ";
    $.ajax({
        type: "Post",
        url: APIUrl + "?key=" + Key,
        //data: "{'data':'" + JSON.stringify(order) + "'}",

        data: JSON.stringify(order),
        header: { 'content-type': 'application/json' },
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            debugger;
            if (response.success == true) {
                PlanRoute();
            }
        },
        error: function (result) {
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function disableAllDriver() {   
    for (var i = 1; i <= 7; i++) {
        callDriverAPI(i, false, $("#txtPlanningDate").val());
    }
}
function increaseValue() {
    var value = parseInt(document.getElementById('number').value, 10);
    value = isNaN(value) ? 0 : value;
    value++;
    document.getElementById('number').value = value;
    enableDisableDriver(value,true);
}
function decreaseValue() {
    var value = parseInt(document.getElementById('number').value, 10);
    value = isNaN(value) ? 0 : value;
    value < 1 ? value = 1 : '';
    value--;
    document.getElementById('number').value = value;
    enableDisableDriver(Number(value)+1,false);
}
function enableDisableDriver(count, actionType) {
    debugger;
    //var externalId = "", enabled = false, count = 0,actionType=false;
    //$("#tblDriverList tbody tr").each(function () {
    //    if ($(this).find('.Action input').prop('checked') == true) {
    //        count++;
    //    }       
    //}); 
    //var tr = $(e).closest('tr');
    //if ($(tr).find('.Action input').prop('checked') == true) {
    //    actionType = true;
    //}
    //else {
    //    count++;
    //}
    if (Number(count) > 7) {
        toastr.error('Maximum 7 drivers allowed.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        $(tr).find('.Action input').prop('checked', false);
    }
    else {
        callDriverAPI(count, actionType, $("#txtPlanningDate").val());
    }
}
function callDriverAPI(DriverId, actionType, planningDate) {   
    var APIUrl = "https://api.optimoroute.com/v1/update_driver_parameters", Key = "5d928b200fa32bf01e0211417edb3f90oDHOnEILQQ";
    $.ajax({
        type: "Post",
        url: APIUrl + "?key=" + Key,
        data: JSON.stringify({ externalId: DriverId.toString(), enabled: actionType, date: planningDate }),
        header: { 'content-type': 'application/json' },
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            debugger;
            if (response.success == true) {
                //if (actionType == true) {
                //    toastr.success('Driver assigned successfully.', 'success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                //} else {
                //    toastr.success('Driver removed successfully.', 'success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                //}
            }
        },
        error: function (result) {
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function PlanRoute() {
    debugger;
    var APIUrl = "https://api.optimoroute.com/v1/start_planning", Key = "5d928b200fa32bf01e0211417edb3f90oDHOnEILQQ";   
    var data = {
        date: $("#txtPlanningDate").val(),
        balancing: "ON_FORCE",
        balanceBy: "WT",
        balancingFactor:parseFloat(1.0)
    }
    $.ajax({
        type: "Post",
        url: APIUrl + "?key=" + Key,
        data: JSON.stringify(data),
        header: { 'content-type': 'application/json' },
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            debugger;
            if (response.success == true) {
                $("#hfPlanningId").val(response.planningId);
                $("#btnPlanRoute").hide();
                $("#btnGetRoute").show();
                swal("", "Route has been planned successfully.", "success");
                $("#decrease").attr("disabled", true);
                $("#increase").attr("disabled", true);
                $("#txtPlanningDate").attr("disabled", true); 
            }
            else {
                swal("", "Oops something went wrong.Try later.", "success");
            }
        }
    })
}
function GetPlanningStatus() {
    debugger;
    var APIUrl = "https://api.optimoroute.com/v1/get_planning_status", Key = "5d928b200fa32bf01e0211417edb3f90oDHOnEILQQ";
    $.ajax({
        type: "GET",
        url: APIUrl + "?key=" + Key + "&planningId=" + Number($("#hfPlanningId").val()),
        data: {},
        header: { 'content-type': 'application/json' },
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            debugger;
            if (response.success == true) {
                if (response.status == "F") {
                    GetRoutes();
                }
                else if (response.status == "R") {
                    swal("", "Planning is in under process.", "success");
                }
                else if (response.status == "C") {
                    swal("", "Planning is cancelled by user.", "success");
                    $("#btnPlanRoute").show();
                    $("#btnGetRoute").hide();
                }
                else if (response.status == "E") {
                    swal("", "Oops,something went wrong.Try again", "success");
                    $("#btnPlanRoute").show();
                    $("#btnGetRoute").hide();
                }
            }
        }
    })
}
function GetRoutes() {
    debugger;
    var APIUrl = "https://api.optimoroute.com/v1/get_routes", Key = "5d928b200fa32bf01e0211417edb3f90oDHOnEILQQ";
    $.ajax({
        type: "GET",
        url: APIUrl + "?key=" + Key + "&date=" + $("#txtPlanningDate").val(),
        data: {},
        header: { 'content-type': 'application/json' },
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            debugger;
            if (response.success == true) {
                $("#popupRouteDetails").modal('show');
                $("#popupOptimoOrderDetails").modal('hide');
                var RouteDetails = response.routes;
                    var OrdNumber = [],drvId=[];
                var OrderList = JSON.parse(localStorage.getItem('UnscheduldedOrder'));
                var xmldoc = $.parseXML(OrderList);
                var RouteTable2 = $(xmldoc).find("Table");
                var DriverList = $(xmldoc).find("Table1");
                $("#tblRouteDetails tbody tr").remove();
                var row = $("#tblRouteDetails thead tr:last-child").clone();
                if (RouteDetails.length > 0) {
                    $.each(RouteDetails, function (i, item) {
                        $.each(RouteDetails[i].stops, function (i, items) {
                            $(".OrderNo", row).html(items.orderNo);
                            $(".Stop", row).html(items.stopNumber);
                            $(".Address", row).html(items.address);
                            $(".scheduledAtDt", row).html(items.scheduledAtDt);
                            $(".arrivalTimeDt", row).html(items.arrivalTimeDt);
                            $(".Driver", row).html(item.driverExternalId);
                            OrdNumber.push({
                                orderNo: items.orderNo
                            })
                            $("#tblRouteDetails tbody").append(row);
                            row = $("#tblRouteDetails tbody tr:last-child").clone();
                        });
                        drvId.push({
                            driverId: item.driverExternalId
                        })
                    });
                    $('#scheduleCount').html($('#tblRouteDetails tr').length - 1);
                    var match = 0;
                    if (RouteTable2.length > 0) {
                        $("#tblUnscheduledOrder tbody tr").remove();
                        var rows = $("#tblUnscheduledOrder thead tr:last-child").clone();
                        $.each(RouteTable2, function (j, item) {
                            match = 0;
                            $.each(OrdNumber, function (i, orn) {
                                if (orn.orderNo == $(item).find("OrderNo").text()) {
                                    match = 1;
                                }
                            });
                            if (match == 0) {
                                $(".OrderNo", rows).text($(item).find("OrderNo").text());
                                $(".Duration", rows).html('<input type="text" style="width:50px" value="15" class="border-primary input-sm text-center">');
                                $(".Lat", rows).text($(item).find("SA_Lat").text());
                                $(".Long", rows).text($(item).find("SA_Long").text());
                                $(".Address", rows).text($(item).find("shipAdd").text());
                                $("#tblUnscheduledOrder tbody").append(rows);
                                rows = $("#tblUnscheduledOrder tbody tr:last-child").clone();
                            }
                        });
                        $('#unscheduleCount').html($('#tblUnscheduledOrder tr').length - 1);
                    }
                    else {
                        $("#ScheduledEmptyTable").show();
                        $("#tblUnscheduledOrder").hide();
                    }
                    $("#tblDrvList tbody tr").remove();
                    var row = $("#tblDrvList thead tr:last-child").clone();                   
                    $.each(drvId, function (l, d) {
                        var ct = 0;
                        $(".ExternalDriver", row).text(d.driverId);
                        var ddl = '<select onchange="checkDriver(this)" class="form-control input-sm border-primary">';
                        $.each(DriverList, function () {
                            if (ct == 0) {
                                ddl += "<option value='0'>--Select Driver--</option>";
                                ct = 1;
                            }
                            ddl += "<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("Name").text() + "</option>"
                        })
                        ddl += '</select>';
                       
                        $(".DriverList", row).html(ddl);
                        $("#tblDrvList tbody").append(row);
                        row = $("#tblDrvList tbody tr:last").clone(true);
                    });
                }
                else {
                    $("#UnScheduledEmptyTable").show();
                    $("#tblRouteDetails").hide();
                    $("#tblUnscheduledOrder").hide();//
                    $("#UnscheduledHead").hide();
                    toastr.error('No route details found.Please replan route', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                }
            }
        }
    })
}
function ChangePlanningDate() {
    disableAllDriver();
    $("#number").val('0');
}
function ReplanRoute() {
    $("#popupRouteDetails").modal('hide');
    $("#popupOptimoOrderDetails").modal('show');
    $("#decrease").removeAttr("disabled");
    $("#increase").removeAttr("disabled");
    $("#txtPlanningDate").removeAttr("disabled");
    $("#btnPlanRoute").show();
    $("#btnGetRoute").hide();
}
function checkDriver(e) {
    debugger;
    var row = $(e).closest("tr");
    var match = 0, drviD = row.find(".DriverList > select option:selected").val();
    $("#tblDrvList tbody tr").each(function (i) {
        if (drviD == $(this).find(".DriverList > select option:selected").val()) {
            match =Number(match)+1;
        }
    });
    if (match > 1) {
        $("#btnSaveRoute").attr('disabled', true);
        toastr.error('Please select different driver.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        row.find(".DriverList > select option:selected").val('0');
    }
    else {
        $("#btnSaveRoute").removeAttr('disabled');
    }
}
function showHideCount(a) {
    if (a == 1) {
        $("#schCount").show();
        $("#unschCount").hide();
    }
    else {
        $("#schCount").hide();
        $("#unschCount").show();
    }
}
function SaveRouteDetails() {
    debugger
    var check = 0;
    $("#tblDrvList tbody tr").each(function (i) {
        if ($(this).find(".DriverList > select option:selected").val()=='0') {
            check = 1;
        }
    });
    if (check == 1) {
        toastr.error('Please map driver first befor save.', 'Error', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
    else {
        var Orders = [];
        $("#tblRouteDetails tbody tr").each(function (i, main) {
            var drvID = 0;
            $("#tblDrvList tbody tr").each(function (j, sub) {
                if ($(main).find('.Driver').text() == $(sub).find('.ExternalDriver').text()) {
                    drvID = $(sub).find(".DriverList > select option:selected").val()
                }
            })
            if (drvID != 0) {
                Orders.push({
                    'OrderNo': $(main).find('.OrderNo').text(),
                    'Driver': drvID,
                    'Stop': $(main).find('.Stop').text(),
                    'ScheduleDate': $(main).find('.scheduledAtDt').text(),
                });
            }
        })
        $.ajax({
            type: "Post",
            url: "/Manager/WebAPI/OptimoRoute.asmx/SaveRouteDetails",
            data: "{'TableValues':'" + JSON.stringify(Orders) + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d == "true") {
                    $("#btnSaveRoute").hide();
                    $("#btnRePlanRoute").hide();
                    $("#btnPrint").show();
                    toastr.success('Route detais updated successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                }
            }
        })
    }
}