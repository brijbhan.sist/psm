﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="OptimoOrderList.aspx.cs" Inherits="Manager_orderList" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .anyClass {
            height: 400px;
            overflow-y: scroll;
        }
        .incdec {
            width: 43px !important;
            height: 28px !important;
            border-radius: 2px;
            border: 1px blue inset;
        }
        .pad{padding:0 !important}
       
    </style>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Order List</h3>       
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage Order</a></li>
                        <li class="breadcrumb-item">Order List</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" id="btnBulkPrint">Print</button>
                    <button type="button" class="dropdown-item" onclick="actionAssigRoute()" data-animation="pulse" id="btnAssignRoot">Assign Route</button>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body">
        <section id="drag-area">
            <div class="row" style="display:none">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <input id="HDDomain" runat="server" type="hidden" />
                            <input type="hidden" id="txtHOrderAutoId" class="form-control input-sm" />

                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row container-fluid">
                                    <div class="col-md-4 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlCustomer" runat="server">
                                            <option value="0">All Customer</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 form-group" id="SalesPerson">
                                        <select class="form-control border-primary input-sm" id="ddlSalesPerson" runat="server">
                                            <option value="0">All Sales Person</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Order No" id="txtSOrderNo" onfocus="this.select()" />
                                    </div>

                                    <div class="col-md-4 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" placeholder="Order From Date" id="txtSFromDate" onfocus="this.select()" />

                                        </div>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text " style="padding: 0rem 1rem;">
                                                    <span class="la la-calendar-o"></span>

                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" placeholder="Order To Date" id="txtSToDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlSStatus">
                                            <option value="0">All Status</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlShippingType">
                                            <option value="0">All Shipping Type</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlDriver">
                                            <option value="0">All Driver</option>
                                        </select>
                                    </div>
                                    <div class="col-md-1 form-group">
                                        <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnSearch">Search</button>
                                    </div>                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <input type="hidden" id="hiddenPackerAutoId" runat="server" />
                                <input type="hidden" id="hiddenEmpType" runat="server" />
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblOrderList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="action text-left"> <input type="checkbox" id="check-all" /> Action</td>
                                                        <td class="status text-center">Status</td>
                                                        <td class="orderNo text-center">Order No</td>
                                                        <td class="orderDt text-center">Order Date</td>
                                                        <td class="SalesPerson">Sales Person</td>
                                                        <td class="cust">Customer</td>
                                                        <%--<td class="value price">Order Amount</td>--%>
                                                        <td class="payableAmt text-right">Payable Amount</td>
                                                        <td class="product text-center">Products</td>
                                                        <td class="CreditMemo text-center">Credit Memo</td>
                                                        <td class="Shipping text-center">Shipping Type</td>
                                                        <td class="Stoppage text-center">Stop No</td>
                                                        <td class="OrderAutoId text-center" style="display:none;">OrderAutoId</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row container-fluid">
                                    <div class="">
                                        <div class="form-group">
                                            <select id="ddlPageSize" class="form-control border-primary input-sm">
                                                <option selected="selected" value="10">10</option>
                                                <option value="50">50</option>
                                                <option value="100">100</option>
                                                <option value="500">500</option>
                                                <option value="1000">1000</option>
                                                <option value="0">All</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-10 col-sm-12">
                                        <div class="Pager"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="modal fade" id="modalSendMail" role="dialog" data-backdrop="static" data-keyboard="false">
   <div class="modal-dialog modal-lg" >
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row" style="width:100%">
                        <div class="col-md-2">
                            <h4 class="modal-title"><b>Send Mail</b></h4>
                        </div>
                        <div class="col-md-3">
                            <input type="hidden" id="hdnPrintLabel" />
                            <b>Order No</b>&nbsp;:&nbsp;<span id="lblOrderSNo"></span>
                        </div>
                        <div class="col-md-7">
                            <b>Customer Name</b>&nbsp;:&nbsp;<span id="lblCustomerName"></span>
                        </div>
                        
                    </div>
                </div>
                <div class="modal-body">
                        <div class="col-md-12">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row form-group">
                                            <div class="col-sm-2">
                                                 <label class="control-label">
                                                    To<span class="required">&nbsp;*</span>
                                                </label>
                                                </div>
                                            <div class="col-sm-10 form-group">
                                                <div class="input-group">
                                                    <input type="text" id="txtTo" class="form-control border-primary input-sm reqMail" onfocus="this.select()" onchange="validateEmail(this);" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                        <div class="row form-group">
                                            <div class="col-sm-2">Subject<span class="required">&nbsp;*</span></div>
                                            <div class="col-sm-10 form-group">
                                                <div class="input-group">
                                                    <input type="text" id="txtSubject" class="form-control border-primary input-sm reqMail" onfocus="this.select()" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                  <div class="row">
                                    <div class="col-md-12">
                                        <div class="row form-group">
                                            <div class="col-sm-2">Message</div>
                                            <div class="col-sm-10 form-group">
                                                <div class="input-group">
                                                    <textarea id="txtEmailBody" class="form-control border-primary input-sm" rows="5" placeholder="Enter your message here" runat="server"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                  <%--  </div>--%>
                </div>
                <div class="modal-footer">
                     <div class="btn-group mr-1 pull-right">
                        <button type="button" class="btn btn-success  buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnSendMail">Send</button>
                    </div>
                    <div class="btn-group mr-1 pull-right">
                        <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="modal fade" id="modalMisc" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="card-title">Drivers</h4>
                    <button type="button" class="close pull-right" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body"> 
                        <div class="row anyClass">                           
                            <div class="col-md-12">
                                <div id="insideText">
                                </div>
                                <div id="DriverText" style="display: none">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-12">
                                  <%--  <div class="table-responsive" id="tblAssignDrv" style="display: none;">--%>
                                        <div class="row form-group">
                                            <div class="col-sm-2">Assign Date :</div>
                                            <div class="col-sm-5 form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text " style="padding: 0rem 1rem;">
                                                            <span class="la la-calendar-o"></span>
                                                        </span>
                                                    </div>


                                                    <input type="text" id="txtAssignDate" class="form-control border-primary input-sm reqdriv" onfocus="this.select()" />


                                                </div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-12">                                              
                                                <table class="table table-bordered table-striped" id="tblDriver">
                                                    <thead class="bg-blue white">
                                                        <tr>
                                                            <td class="DrvName">Driver Name</td>
                                                            <td class="AsgnOrders text-center">Assigned Orders</td>
                                                            <td class="Select text-center">Select</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>                                                
                                            </div>
                                        </div>
                                 <%--   </div>--%>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>                              
                        </div> 
                </div>
                <div class="modal-footer">
                        <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1 btn-sm" style="display: none;" id="btnAsgn" onclick="assignDriver()">Assign</button>
                        <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1 btn-sm" style="display: none;" id="btnOk" onclick="packingConfirmed()" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div id="modalOrderLog" class="modal fade text-left show" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-xl">
            <!-- Modal content-->
            <div class="modal-content">

                <div class="modal-header">
                     <div class="row">
                        <div class="col-md-12">
                            <h4 class="modal-title">Order Log</h4>
                        </div>
                         <div class="col-md-12">
                            <label><b>Order No </b>&nbsp;:&nbsp;<span id="lblOrderNo"></span>&nbsp;&nbsp;<b>Order Date</b>&nbsp;:&nbsp;<span id="lblOrderDate"></span></label>
                        </div>
                    </div>
                     
                   

                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="tblOrderLog">
                            <thead class="bg-blue white">
                                <tr>
                                    <td class="SrNo text-center">SN</td>
                                    <td class="ActionBy">Action By</td>
                                    <td class="Date text-center">Date</td>
                                    <td class="Action">Action</td>
                                    <td class="Remark">Remark</td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <div id="PopPrintTemplate" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Select Invoice Template</h4>
                        <button type="button" class="close pull-right" data-dismiss="modal">×</button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-10">
                                            <input type="radio" name="Template" id="chkdefault" />
                                            <label for="chkdefault">PSM Default  - Invoice Only</label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-10">
                                            <input type="radio" name="Template" id="chkdueamount" />
                                            <label for="chkdueamount">PSM Default</label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-10">
                                            <input type="radio" name="Template" id="PackingSlip" />
                                            <label for="PackingSlip">Packing Slip</label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-10">
                                            <input type="radio" name="Template" id="WithoutCategorybreakdown" />
                                            <label for="PackingSlip">Packing Slip - Without Category Breakdown</label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row" id="divPSMPADefault" style="display:none">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-10">
                                            <input type="radio" name="Template" id="PSMPADefault" />
                                            <label for="PSMPADefault">PSM PA Default</label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                     <div class="row form-group" id="divPSMNPADefault" style="display:none">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-10">
                                            <input type="radio" name="Template" id="rPSMNPADefault" /> PSM NPA Default
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-10">
                                            <input type="radio" name="Template" id="Template1" />
                                            <label for="Template1">7 Eleven (Type 1)</label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-10">
                                            <input type="radio" name="Template" id="Template2" />
                                            <label for="Template2">7 Eleven (Type 2)</label>
                                        </div>
                                    </div>
                                     <div class="row form-group">
                                         <div class="col-md-2"></div>
                                        <div class="col-md-10">
                                            <input type="radio" name="Template" id="Templ1" /> Template 1
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-10">
                                            <input type="radio" name="Template" id="Templ2" />
                                            Template 2
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1  btn-sm" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" onclick="PrintOrder()">Print</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
     <div id="ShipAddressPopup" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="modal-title">Shipping Address</h4>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12  form-group">
                            <label class="control-label">Shipping Address</label> <span style="color: red; text-align: right;font-size:12px;"> (Please enter Address, State, City and Zipcode)</span>
                            <input type="hidden" id="hdnordAutoId"/>
                           <textarea id="txtshippingaddress" placeholder="Please Enter Shipping Address" class="form-control border-primary req" row="2"></textarea>
                        </div>
                    </div> 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse" data-dismiss="modal">Close</button>
                     <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" onclick="UpdateShipAdd()">Update</button>
                </div>
            </div>
        </div>
    </div>
    <div id="popupOptimoOrderDetails" class="modal fade" role="dialog"  data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-xl">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row w-100">
                        <div class="col-md-7">
                            <h4 class="modal-title">Order Details</h4>
                        </div>
                        <div class="col-md-2 text-right">
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col-md-2">
                           <label for="drv">No. of Driver</label>
                        </div>
                        <div class="col-md-1 text-right pad w-25">
                            <input type="button" id="decrease" class="incdec" value="-" onclick="decreaseValue()" />
                        </div>
                        <div class="col-md-1 pad w-25">
                            <input type="text" style="width: 30px; text-align: center" disabled maxlength="7" class="incdec" id="number" value="0" />
                        </div>
                        <div class="col-md-1 pad text-left w-25">
                            <input type="button" id="increase" class="incdec" value="+" style="position: relative; right: 63px;"
                                onclick="increaseValue()" />
                        </div>
                        <div class="col-md-2">
                          <label for="date">Delivery Date</label>  
                        </div>
                        <div class="col-md-2">
                            <input type="text" class="form-control border-primary input-sm text-center" onchange="ChangePlanningDate()" id="txtPlanningDate" placeholder="Select Planning Date" />
                        </div>
                         <div class="col-md-1">
                          <label for="date">Duration</label>  
                        </div>
                        <div class="col-md-2">
                            <input type="text" class="form-control border-primary input-sm text-center" value="15" id="txtDuration" placeholder="Duration" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered" id="tblRouteOrder">
                                    <thead class="bg-blue white">
                                        <tr>
                                            <td class="OrderNo text-center">Order No</td>
                                            <%--<td class="Duration text-center">Duration</td>--%>
                                            <td class="Lat text-center">Lat</td>
                                            <td class="Long text-center">Long</td>
                                            <td class="Address">Address</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                         <div class="col-md-4" style="display:none">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered" id="tblDriverList">
                                    <thead class="bg-blue white">
                                        <tr>
                                            <td class="Action text-center">Action</td>
                                            <td class="DriverName">Driver Name</td>
                                            <%--<td class="SeqNumber">Sequence Number</td>--%>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse" data-dismiss="modal">Close</button>
                     <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" id="btnPlanRoute" onclick="deleteAll()">Plan Route</button>
                     <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" style="display:none" id="btnGetRoute" onclick="GetPlanningStatus()">Get Route</button>              
                    </div>
            </div>
        </div>
    </div>
    <div id="popupRouteDetails" class="modal fade" role="dialog"  data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-xl">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="modal-title">Route Details</h4>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-body" id="navbar">
                                        <div class="row">
                                            <div class="col-md-10 col-sm12">
                                                <ul class="nav nav-tabs nav-topline">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" id="base-scheduled" onclick="showHideCount(1)" data-toggle="tab" aria-controls="tab21" href="#scheduled" aria-expanded="true">Scheduled Order List</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" id="base-unscheduled" onclick="showHideCount(2)" data-toggle="tab" aria-controls="tab22" href="#unscheduled" aria-expanded="false">Unscheduled Order List</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-2">
                                                <ul class="nav nav-tabs nav-topline">
                                                    <li class="float-right">
                                                        <span id="schCount">Total Sechedule Order : <b id="scheduleCount"></b></span><span id="unschCount" style="display: none">Total Unschedule Order : <b id="unscheduleCount"></b></span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="tab-content px-1 pt-1 border-grey border-lighten-2 border-0-top">
                                            <div role="tabpanel" class="row tab-pane active" id="scheduled" aria-expanded="true" aria-labelledby="base-scheduled">
                                                <div class="row form-group">
                                                    <div class="col-md-8">
                                                        <div class="table-responsive">
                                                            <table class="table table-striped table-bordered" id="tblRouteDetails">
                                                                <thead class="bg-blue white">
                                                                    <tr>
                                                                        <td class="OrderNo text-center">Order No</td>
                                                                        <td class="Driver text-center">Driver</td>
                                                                        <td class="Stop text-center">Stop</td>
                                                                        <td class="scheduledAtDt text-center">Schedule</td>
                                                                        <td class="arrivalTimeDt text-center">Arrival</td>
                                                                        <td class="Address">Address</td>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                            <h5 class="well text-center" id="ScheduledEmptyTable" style="display: none">No data available.</h5>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="table-responsive">
                                                            <table class="table table-striped table-bordered" id="tblDrvList">
                                                                <thead class="bg-blue white">
                                                                    <tr>
                                                                        <td class="ExternalDriver text-center">External Driver</td>
                                                                        <td class="DriverList">Driver List</td>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        </div>
                                                </div>
                                            </div>
                                            <div class="row tab-pane" id="unscheduled" aria-labelledby="base-unscheduled">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="table-responsive">
                                                            <table class="table table-striped table-bordered" id="tblUnscheduledOrder">
                                                                <thead class="bg-blue white">
                                                                    <tr>
                                                                        <td class="OrderNo text-center">Order No</td>
                                                                        <%--<td class="Duration text-center">Duration</td>--%>
                                                                        <td class="Lat text-center">Lat</td>
                                                                        <td class="Long text-center">Long</td>
                                                                        <td class="Address">Address</td>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                            <h5 class="well text-center" id="UnScheduledEmptyTable" style="display: none">No data available.</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1  btn-sm" id="btnSaveRoute" onclick="SaveRouteDetails()">Save</button>
                    <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" id="btnRePlanRoute" onclick="ReplanRoute()">Replan Route</button>
                    <button type="button" style="display:none" class="btn btn-info buttonAnimation pull-right round box-shadow-1  btn-sm" id="btnPrint" >Print</button>

                    <input type="hidden" id="hfPlanningId" />
                </div>
            </div>
        </div>
    </div>
     <script src="../js/summernote.js"></script>
    <link href="../css/summernote.css" rel="stylesheet" />
    <script>
        $(document).ready(function () {
            $('#txtEmailBody').summernote();

        });
    </script>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="/OptimoRoute/JS/Manager_OrderList.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>

</asp:Content>

