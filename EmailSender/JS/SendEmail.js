﻿var OrdAutoId = '';
$(document).ready(function () {
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    var getid = getQueryString('OrderAutoId');

   
    if (getid != null) {
        SendMail(getid);
    }
   
})

function SendMail(getid) {
    
    $.ajax({
        type: "POST",
        url: "SendEmail.aspx/ViewOrderDetails",
        data: "{'OrderAutoId':" + getid + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            debugger;
            var xmldoc = $.parseXML(response.d);
            var orderx = $(xmldoc).find("Table");
            var PrintLbl = $(xmldoc).find("Table1");
            $("#lblOrderSNo").text(orderx.find("OrderNo").text());
            $("#lblCustomerName").text(orderx.find("CustomerName").text());
            $("#txtTo").val(orderx.find("Email").text());
            $("#hdnPrintLabel").val(PrintLbl.find("PrintLabel").text());
            var Subject = "Order No : " + orderx.find("OrderNo").text();
            $("#txtSubject").val(Subject);
            var emailBody = '<p>Dear <b>' + orderx.find("CustomerName").text() + ',</b></p>';
            emailBody += '<p>Your <b>ORDER # ' + orderx.find("OrderNo").text() + '</b> for <b>$' + parseFloat(orderx.find("PayableAmount").text()).toFixed(2) + '</b> attached. Please remit payment at your earliest convenience.</p>';
            emailBody += '<p>Thank you for your business - We appreciate it very much.</p>';
            emailBody += '<p>Sincerely ,</p>';
            emailBody += '<p><b>PRICESMART</b></p>';
            emailBody += '<p><b>1(888)51MYPSM</b> ';
            emailBody += '<b><br>1(888)516-9776</b></p>';
            $("#txtEmailBody").summernote('code', emailBody);

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
    //$("#modalSendMail").modal('show');
    OrdAutoId = getid;
    $("#txtTo").val('');
    $("#txtSubject").val('');
    $("#txtEmailBody").val('');
}
$("#btnSendMail").click(function () {
    if (dynamicALL('reqMail')) {
        if ($("#txtSubject").val() == "") {
            swal({
                title: "Are you sure?",
                text: "You want to Send Mail without Subject",
                icon: "warning",
                showCancelButton: true,
                buttons: {
                    cancel: {
                        text: "No, cancel!",
                        value: null,
                        visible: true,
                        className: "btn-warning",
                        closeModal: false,
                    },
                    confirm: {
                        text: "Yes, Send it!",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: false
                    }
                }
            }).then(function (isConfirm) {
                if (isConfirm) {
                    Sending()
                }
                else {
                    swal("", "Your email will not be sent", "error");
                }
            })
            // Sending();
        }
        else if ($("#txtEmailBody").val() == "") {
            swal({
                title: "Are you sure?",
                text: "You want to send mail without message",
                icon: "warning",
                showCancelButton: true,
                buttons: {
                    cancel: {
                        text: "No, cancel!",
                        value: null,
                        visible: true,
                        className: "btn-warning",
                        closeModal: false,
                    },
                    confirm: {
                        text: "Yes, Send it!",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: false
                    }
                }
            }).then(function (isConfirm) {
                if (isConfirm) {
                    debugger;
                    Sending()
                }
                else {
                    swal("", "Your email will not be sent", "error");
                }
            })
        }
        else {
            Sending();
        }

    } //--------------------------
    else {
        toastr.error('All * fields are mandatory .', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
})
function Sending() {
    var data = {
        To: $('#txtTo').val(),
        Subject: $('#txtSubject').val(),
        EmailBody: $('#txtEmailBody').val(),
        OrderId: OrdAutoId,
        PrintLabel: $('#hdnPrintLabel').val(),
    };
    $.ajax({
        type: "POST",
        url: "SendEmail.aspx/getMailBoddy",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == 'true') {
                swal('', 'Email sent successfully.', 'success');
                $("#modalSendMail").modal('hide');
                // OrdAutoId = OrderId;
            } else if (response.d == 'receiverMail') {
                swal('', 'Email address could not be found.', 'error');
            } else if (response.d == 'EmailNotSend') {
                swal('', 'Email address could not be found or was misspelled.', 'error');
            }
            else if (response.d == 'false') {
                swal('', 'Opps something went wrong.', 'error');
            } else if (response.d == 'Session Expired') {
                location.href = '/';
            }
            else {
                swal('', 'Opps something went wrong.', 'error');
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function Close() {
    window.close();
}