﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SendEmail.aspx.cs" Inherits="Admin_SendEmail" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="apple-touch-icon" href="/app-assets/images/ico/apple-icon-120.png" />
    <link rel="icon" href="/favicon.png" type="image/gif" sizes="16x16" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Quicksand:300,400,500,700"
        rel="stylesheet" />

    <link href="/app-assets/fonts/line-awesome/css/line-awesome.min.css" rel="stylesheet" />

    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/css/core/menu/menu-types/vertical-content-menu.css" />
    <link rel="stylesheet" type="text/css" href="/app-assets/css/vendors.css" />
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/editors/summernote.css" />
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/forms/selects/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/extensions/sweetalert.css" />
    <link href="/css/Loaders.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/extensions/toastr.css" />
    <link rel="stylesheet" type="text/css" href="/app-assets/css/plugins/extensions/toastr.css" />
    <link rel="stylesheet" type="text/css" href="/app-assets/css/app.css" />
    <link href="../app-assets/css/components.css" rel="stylesheet" />
    <link href="/app-assets/css/custom.css?id=1" rel="stylesheet" />


    <script type="text/javascript" src="/js/jquery.min.js"></script>
    <script type="text/javascript" src="/js/jquery-ui.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script src="/js/ShowMessage.js" type="text/javascript"></script>
    <script src="/JS/SessionFile.js"></script>
    <script src="/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="/app-assets/vendors/js/editors/summernote/summernote.js" type="text/javascript"></script>
    <script src="/app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>
    <script src="/app-assets/vendors/js/extensions/sweetalert.min.js" type="text/javascript"></script>
    <script src="/app-assets/vendors/js/extensions/toastr.min.js" type="text/javascript"></script>
    <script src="/app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>
    <script src="/app-assets/vendors/js/animation/jquery.appear.js" type="text/javascript"></script>
    <script src="/app-assets/js/scripts/animation/animation.js" type="text/javascript"></script>
    <script src="/app-assets/js/scripts/forms/select/form-select2.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/Commonfumction.js"></script>
    <script type="text/javascript" src="/js/code39.js"></script>
    <script type="text/javascript" src="/js/resetvalidation.js"></script>
    <script type="text/javascript" src="/js/MasterPage.js"></script>
    <title></title>
</head>
<body>
    <%-- <div class="modal fade" id="modalSendMail" role="dialog" data-backdrop="static" data-keyboard="false">--%>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <div class="row" style="width: 100%">
                    <div class="col-md-2">
                        <h4 class="modal-title"><b>Send Mail</b></h4>
                    </div>
                    <div class="col-md-3">
                        <input type="hidden" id="hdnPrintLabel" />
                        <b>Order No</b>&nbsp;:&nbsp;<span id="lblOrderSNo"></span>
                    </div>
                    <div class="col-md-7">
                        <b>Customer Name</b>&nbsp;:&nbsp;<span id="lblCustomerName"></span>
                    </div>

                </div>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row form-group">
                                    <div class="col-sm-2">
                                        <label class="control-label">
                                            To<span class="required">&nbsp;*</span>
                                        </label>
                                    </div>
                                    <div class="col-sm-10 form-group">
                                        <div class="input-group">
                                            <input type="text" id="txtTo" class="form-control border-primary input-sm reqMail" onfocus="this.select()" onchange="validateEmail(this);" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row form-group">
                                    <div class="col-sm-2">Subject<span class="required">&nbsp;*</span></div>
                                    <div class="col-sm-10 form-group">
                                        <div class="input-group">
                                            <input type="text" id="txtSubject" class="form-control border-primary input-sm reqMail" onfocus="this.select()" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row form-group">
                                    <div class="col-sm-2">Message</div>
                                    <div class="col-sm-10 form-group">
                                        <div class="input-group">
                                            <textarea id="txtEmailBody" class="form-control border-primary input-sm" rows="5" placeholder="Enter your message here" runat="server"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <div class="btn-group mr-1 pull-right">
                    <button type="button" class="btn btn-success  buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnSendMail">Send</button>
                </div>
                <div class="btn-group mr-1 pull-right">

                    <button type="button" onclick="Close()" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm">Close</button>
                </div>
            </div>
        </div>

    </div>
    <%-- </div>--%>

   

     <script type="text/javascript">
         document.write('<scr' + 'ipt type="text/javascript" src="JS/SendEmail.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
</body>
</html>
