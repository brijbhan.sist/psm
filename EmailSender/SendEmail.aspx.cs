﻿using DllSendEmail;
using DllUtility;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web.Script.Serialization;
using System.Web.Services;
using Newtonsoft.Json.Linq;
using System.Web;
using System.Globalization;
using System.Diagnostics;

public partial class Admin_SendEmail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod(EnableSession = true)]
    public static string ViewOrderDetails(string OrderAutoId)
    {
        PL_SendEmail pobj = new PL_SendEmail();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {

                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"].ToString());
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                BL_SendEmail.ViewOrderDetails(pobj);
                if (!pobj.isException)
                {

                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public static string getMailBoddy(string dataValue)
    {

        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);

        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            string CSSStyle = "table{border-collapse:collapse;}td {padding: 5px 7px;}.text-center{text-align:center}";
            CSSStyle += "table {width: 100%;}body {font-size: 13px;font-family: monospace;}";
            CSSStyle += ".center{text-align: center;}.right {text-align: right;}thead > tr {background: #fff;color: #000;font-weight: 600;border-top: 1px solid #5f7c8a;}";
            CSSStyle += "thead > tr > td {text-align: center !important;}.InvContent {width: 100%;}.tdLabel {font-weight: 600;}";
            CSSStyle += "@media print {#btnPrint {display: none;}}legend {border-bottom: 1px solid #5f7c8a;}";
            CSSStyle += ".tableCSS > tbody > tr > td,.tableCSS > thead > tr > td,.tableCSS > tfoot > tr > td {border: 1px solid #5f7c8a;}pre{border:none !important;}";
            CSSStyle += "";
            string Body = "";
            Body = "<!DOCTYPE html><html xmlns='http://www.w3.org/1999/xhtml'>";
            Body += "<head> <title></title> <style  type='text/css'></style>";
            Body += "</head>";
            Body += "<body style='font-family: Arial; font-size: 13px;width:1000px'>";
            Body += "<div class='InvContent'>";
            Body += "<fieldset>";
            Body += "<table class='table tableCSS'>";
            Body += "<tbody>";
            Body += "<tr style='text-align:center'>";
            Body += "<td colspan='6' style='padding:8px 8px;'>";
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcPrintOrderMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", 522);
                sqlCmd.Parameters.AddWithValue("@OrderAutoId", Convert.ToInt32(jdv["OrderId"]));
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                DataSet Ds = new DataSet();
                sqlAdp.Fill(Ds);
                string Orders = "";
                string CompanyDetails = "";
                foreach (DataRow dr in Ds.Tables[0].Rows)
                {
                    Orders += dr[0];
                }
                CompanyDetails = Orders;

                JArray CompanyList = JArray.Parse(CompanyDetails);
                JObject CompanyDetail = (JObject)CompanyList[0];
                JArray OrderList = (JArray)CompanyDetail["OrderDetails"];
                JObject OrderDetail = (JObject)OrderList[0];

                Body += "<div><span style='font-size: 20px;text-align:center;font-weight:600' id='PageHeading'>" + OrderDetail["CustomerName"].ToString() + " - " + OrderDetail["OrderNo"].ToString() + "</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                Body += "<span style='font-size: 20px;text-align:right;font-weight:600;float:right'><i>Sales Order</i></span></div>";
                Body += "</td>";
                Body += "</tr>";
                Body += "<tr>";
                Body += "<td rowspan='2' style='width: 21% !important;'>";
                Body += "<img src='" + CompanyDetail["LogoforEmail"].ToString() + "' id='logo' class='img-responsive' style='width: 100%;'/>";
                Body += "</td>";
                Body += "<td style='width: 26% !important;'><span id='CompanyName'>" + CompanyDetail["CompanyName"].ToString() + "</span></td>";
                Body += "<td class='tdLabel' style='white-space: nowrap;width:18% !important'>Customer ID</td>";
                Body += "<td id='acNo' style='white-space: nowrap;width:23% !important'>" + OrderDetail["CustomerId"].ToString() + "</td>";
                Body += "<td class='tdLabel' style='white-space: nowrap;width:13% !important'>Sales Rep</td>";
                Body += "<td id='salesRep' style='white-space: nowrap;width:19% !important'>" + OrderDetail["SalesPerson"].ToString() + "</td>";
                Body += "</tr>";
                Body += "<tr>";
                Body += "<td><span id='Address'>" + CompanyDetail["Address"].ToString() + "</span></td>";
                Body += "<td class='tdLabel' style='white-space: nowrap'>Customer Contact</td>";
                Body += "<td id='storeName' style='white-space: nowrap'>" + OrderDetail["ContactPersonName"].ToString() + "</td>";
                Body += "<td class='tdLabel' style='white-space: nowrap'>Order No</td>";
                Body += "<td id='so'>" + OrderDetail["OrderNo"].ToString() + "</td>";
                Body += "</tr>";
                Body += "<tr>";
                Body += "<td><span id='Website' style='white-space: nowrap'>" + CompanyDetail["Website"].ToString() + "</span></td>";
                Body += "<td style='white-space: nowrap'><span id='Phone'> Phone : " + CompanyDetail["MobileNo"].ToString() + "</span></td>";
                Body += "<td class='tdLabel'>Contact No</td>";
                Body += "<td id='ContPerson'>" + OrderDetail["Contact"].ToString() + "</td>";
                Body += "<td class='tdLabel' style='white-space: nowrap'>Order Date</td>";
                Body += "<td id='soDate' style='white-space: nowrap'>" + OrderDetail["OrderDate"].ToString() + "</td>";
                Body += "</tr>";
                Body += "<tr>";
                Body += "<td><span id='EmailAddress'>" + CompanyDetail["EmailAddress"].ToString() + "</span></td>";
                Body += "<td>Fax : <span id='FaxNo'>" + CompanyDetail["FaxNo"].ToString() + "</span></td>";
                Body += "<td class='tdLabel'>Terms</td>";
                Body += "<td style='font-size: 12px'><b><span id='terms'>" + OrderDetail["TermsDesc"].ToString() + "</span></b></td>";
                Body += "<td class='tdLabel'>Ship Via</td>";
                Body += "<td id='shipVia'>" + OrderDetail["ShippingType"].ToString() + "</td>";
                Body += "</tr>";

                string Bn = OrderDetail["BusinessName"].ToString();
                if (Bn != "" && Bn != null)
                {
                    Body += "<tr>";
                    Body += "<td><span class='tdLabel'>Business Name:</span></td>";
                    Body += "<td colspan='5'><span id='BusinessName'>" + Bn + "</span></td>";
                    Body += "</tr>";
                }
                Body += "<tr>";
                Body += "<td><span class='tdLabel'>Shipping Address</span></td>";
                Body += "<td colspan='5'><span id='shipAddr' style='font-size: 15px'>" + OrderDetail["BillingAddress"].ToString() + "</span></td>";
                Body += "</tr>";
                Body += "<tr>";
                Body += "<td><span class='tdLabel'>Billing Address</span></td>";
                Body += "<td colspan='5'><span id='billAddr' style='font-size: 15px'>" + OrderDetail["ShippingAddress"].ToString() + "</span></td>";
                Body += "</tr>";
                Body += "</tbody>";
                Body += "</table>";
                Body += "</fieldset><br/><fieldset>";
                Body += "<table class='table tableCSS' id='tblProduct'>";
                Body += "<thead>";
                Body += "<tr>";
                Body += "<td class='text-center' style='width:4px;'>Qty</td><td style='width: 6px;white-space:nowrap' class='text-center'>Unit Type</td><td style='white-space: nowrap;width:8px;'>Product ID</td>" +
                "<td style='white-space: nowrap;width:30px;'>Item</td><td style='text-align: center;width:12px'>UPC</td><td class='SRP' style='text-align: center; width: 6px'>SRP</td><td class='GP' style='text-align: center; width: 6px'>GP(%)</td>" +
                "<td class='UnitPrice' style='text-align: center; width: 6px'>Price</td><td class='totalPrice' style='text-align: center; width: 10px; white-space: nowrap'>Total Price</td>";
                Body += "</tr>";
                Body += "</thead>";
                Body += "<tbody>";
                JArray ItemList = (JArray)OrderDetail["Item2"];
                if (((JArray)OrderDetail["Item1"]).Count > 0)
                {

                    ItemList = (JArray)OrderDetail["Item1"];
                }
                //JObject Items = (JObject)ItemList[0];
                int CountOrderItem = 0;
                for (var i = 0; i < ItemList.Count; i++)
                {

                    JObject Items = (JObject)ItemList[i];

                    if (Items.Count > 0)
                    {

                        CountOrderItem += Convert.ToInt32(Items["QtyShip"].ToString());
                        Body += "<tr>";
                        Body += "<td class='text-center'>" + Items["QtyShip"].ToString() + "</td><td style='width: 25px;text-align: center;'>" + Items["UnitType"].ToString() + "</td>" +
                        "<td style='white-space: nowrap;text-align: left;'>" + Items["ProductId"].ToString() + "</td>";
                        if (Items["IsExchange"].ToString() == "1")
                        {
                            Body += "<td style='white-space: nowrap'>" + Items["ProductName"] + " <i style='font-size:10px'>Exchange</i>" + "</td>";
                        }
                        else if (Items["isFreeItem"].ToString() == "1")
                        {
                            Body += "<td style='white-space: nowrap'>" + Items["ProductName"] + " <i style='font-size:10px'>Free</i>" + "</td>";
                        }
                        else
                        {
                            Body += "<td style='white-space: nowrap'>" + Items["ProductName"] + "</td>";
                        }
                        Body += "<td style='text-align: center; width: 28px'>" + Items["Barcode"] + "</td>" +
                        "<td class='SRP' style='text-align: right; width: 28px'>" + Items["SRP"].ToString() + "</td>" +
                        "<td class='GP' style='text-align: right; width: 28px'>" + Items["GP"] + "</td>" +
                        "<td class='UnitPrice' style='text-align: right; width: 28px'>" + Items["UnitPrice"].ToString() + "</td>" +
                        "<td class='totalPrice' style='text-align: right; width: 28px; white-space: nowrap'>" + Items["NetPrice"].ToString() + "</td>";
                        Body += "</tr>";
                    }
                }

                Body += "</tbody>";
                Body += "</table></fieldset><br/>";
                Body += "<fieldset>";
                Body += "<table style='width: 100%;'>";
                Body += "<tbody>";
                Body += "<tr>";
                Body += "<td style='width: 67%' colspan='2'>" + "<center><div id='TC'>" + CompanyDetail["tc"].ToString() + "</div></center>" + "</td>";
                Body += "<td style='width: 33%;'>";
                Body += "<table class='table tableCSS' style='width:100%;'>";
                Body += "<tbody>";
                Body += "<tr>";
                Body += "<td class='tdLabel'>Total Items</td>";
                Body += "<td id='totalQty' style='text-align: right;'>" + CountOrderItem + "</td>";
                Body += "</tr>";
                Body += "<tr>";
                Body += "<td class='tdLabel'>Sub Total</td>";
                Body += "<td style='text-align: right;'><span id='totalwoTax'>" + OrderDetail["TotalAmount"].ToString() + "</span></td>";
                Body += "</tr>";
                string dis = OrderDetail["OverallDiscAmt"].ToString();
                if (dis != "0.00" && dis != null)
                {
                    if (Convert.ToDecimal(dis) > 0)
                    {
                        Body += "<tr>";
                        Body += "<td class='tdLabel'>Discount</td>";
                        Body += "<td style='text-align: right;'><span id='disc'>-" + dis + "</span></td>"; ;
                        Body += "</tr>";
                    }
                }
                string tax = OrderDetail["TotalTax"].ToString();
                if (tax != "0.00" && tax != null)
                {
                    if (Convert.ToDecimal(tax) > 0)
                    {
                        Body += "<tr>";
                        Body += "<td class='tdLabel'>Tax</td>";
                        Body += "<td style='text-align: right;'><span id='tax'>" + tax + "</span></td>";
                        Body += "</tr>";
                    }
                }
                string SC = OrderDetail["ShippingCharges"].ToString();
                if (SC != "0.00" && SC != null)
                {
                    if (Convert.ToDecimal(SC) > 0)
                    {
                        Body += "<tr>";
                        Body += "<td class='tdLabel'>Shipping</td>";
                        Body += "<td style='text-align: right;'><span id='shipCharges'>" + SC + "</span></td>";
                        Body += "</tr>";
                    }
                }
                string NTax = Convert.ToString(OrderDetail["MLTax"]);
                decimal NTAXDeci = decimal.Parse(NTax, CultureInfo.InvariantCulture);

                if (NTax != null && NTax != "0.00")
                {
                    if (Convert.ToDecimal(NTax) > 0)
                    {
                        Body += "<tr>";
                        Body += "<td class='tdLabel'>" + OrderDetail["MLTaxPrintLabel"].ToString() + "</td>";
                        Body += "<td style='text-align: right;'><span id='MLTax'>" + string.Format("{0:0.00}", NTAXDeci) + "</span></td>";
                        Body += "</tr>";
                    }
                }
                string WTax = OrderDetail["WeightTax"].ToString();
                if (WTax != "" && WTax != null && WTax != "0.00")
                {
                    if (Convert.ToDecimal(WTax) > 0)
                    {
                        Body += "<tr>";
                        Body += "<td class='tdLabel'>Weight Tax</td>";
                        Body += "<td style='text-align: right;'><span id='WeightTax'>" + WTax + "</span></td>";
                        Body += "</tr>";
                    }
                }
                string AAmt = OrderDetail["AdjustmentAmt"].ToString();
                if (AAmt != null && AAmt != "0.00")
                {
                    if (Convert.ToDecimal(AAmt) > 0)
                    {
                        Body += "<tr>";
                        Body += "<td class='tdLabel'>Adjustment</td>";
                        Body += "<td style='text-align: right;'><span id='spAdjustment'>" + AAmt + "</span></td>";
                        Body += "</tr>";
                    }
                }
                Body += "<tr>";
                Body += "<td class='tdLabel'>Grand Total</td>";
                Body += "<td style='text-align: right;'><span id='grandTotal'>" + OrderDetail["GrandTotal"].ToString() + "</span></td>";
                Body += "</tr>";

                string DAmmt = OrderDetail["DeductionAmount"].ToString();
                if (DAmmt != "0.00" && DAmmt != null)
                {
                    if (Convert.ToDecimal(DAmmt) > 0)
                    {
                        Body += "<tr>";
                        Body += "<td class='tdLabel'>Credit memo</td>";
                        Body += "<td style='text-align: right;'><span id='spnCreditmemo'>-" + DAmmt + "</span></td>";
                        Body += "</tr>";
                    }
                }
                string CAmt = OrderDetail["CreditAmount"].ToString();
                if (CAmt != null && CAmt != "0.00")
                {
                    if (Convert.ToDecimal(CAmt) > 0)
                    {
                        Body += "<tr>";
                        Body += "<td class='tdLabel'>Store Credit</td>";
                        Body += "<td style='text-align: right;'><span id='spnStoreCredit'>-" + CAmt + "</span></td>";
                        Body += "</tr>";
                    }
                }
                try
                {


                    if (Convert.ToDecimal(CAmt) > 0 || Convert.ToDecimal(DAmmt) > 0)
                    {
                        Body += "<tr>";
                        Body += "<td class='tdLabel'>Payable Amount</td>";
                        Body += "<td style='text-align: right;'><span id='spnPayableAmount'>" + OrderDetail["PayableAmount"].ToString() + "</span></td>";
                        Body += "</tr>";
                    }

                }
                catch (Exception)
                {

                }

                Body += "</tbody>";
                Body += "</table>";
                Body += "</td>";
                Body += "</tr>";
                Body += "</tbody>";
                Body += "</table>";
                Body += "</fieldset><br/>";
                //---------------------------------------------------------Open Balance Report-----------------------------------------------------
                JArray DueOrder = (JArray)OrderDetail["DueOrderList"];
                if (DueOrder.Count > 0)
                {
                    Body += "<P style='page-break-before: always'></p><fieldset>";
                    Body += "<table class='table tableCSS' id='tblProduct'>";
                    Body += "<thead>";
                    Body += "<tr style='text-align:center'>";
                    Body += "<td colspan='6' style='padding:8px 8px;'>";
                    Body += "<div><span style='font-size: 20px;text-align:right;font-weight:600;float:right'><i>Open Balance</i></span></div>";
                    Body += "</td>";
                    Body += "</tr>";
                    Body += "<tr style='text-align:center'>";
                    Body += "<td colspan='6' style='padding:8px 8px;'>";
                    Body += "<div><span style='font-size: 20px;text-align:center;font-weight:600;' id='PageHeading'>" + OrderDetail["CustomerName"].ToString() + "</span></div>";
                    Body += "</td>";
                    Body += "</tr>";
                    Body += "<tr>";
                    Body += "<td style='width: 10px;white-space:nowrap' class='text-center'>Order No.</td><td style='white-space: nowrap;width:12px;'>Order Date</td>" +
                    "<td style='white-space: nowrap;width:30px;'>Order Amount</td><td class='PaidAmount' style='text-align: center; width: 10px'>Paid amount</td><td class='OpenAmount' style='text-align: center; width: 10px'>Open Amount</td>" +
                    "<td class='Aging' style='text-align: center; width: 10px'>Aging (Days)</td>";
                    Body += "</tr>";
                    Body += "</thead>";
                    Body += "<tbody>";
                    decimal TotalGrandTotal = 0;
                    decimal TotalAmtPaid = 0;
                    decimal TotalAmtDue = 0;
                    for (var j = 0; j < DueOrder.Count; j++)
                    {
                        JObject DueOrderList = (JObject)DueOrder[j];
                        if (DueOrderList.Count > 0)
                        {
                            Body += "<tr>";
                            Body += "<td class='text-center'>" + DueOrderList["OrderNo"].ToString() + "</td><td class='text-center' style='width: 25px'>" + DueOrderList["OrderDate"].ToString() + "</td>" +
                            "<td style='white-space: nowrap;text-align: right;'>" + DueOrderList["GrandTotal"].ToString() + "</td>" +
                            "<td style='white-space: nowrap;text-align: right;'>" + DueOrderList["AmtPaid"].ToString() + "</td>" +
                            "<td class='SRP' style='text-align: right; width: 28px'>" + DueOrderList["AmtDue"].ToString() + "</td>" +
                            "<td class='totalPrice' style='text-align: center; width: 28px; white-space: nowrap'>" + DueOrderList["Aging"].ToString() + "</td>";
                            Body += "</tr>";

                            TotalGrandTotal +=  Convert.ToDecimal(DueOrderList["GrandTotal"].ToString());
                            TotalAmtPaid +=  Convert.ToDecimal(DueOrderList["AmtPaid"].ToString());
                            TotalAmtDue += Convert.ToDecimal(DueOrderList["AmtDue"].ToString());
                        }
                    }


                    Body += "</tbody>";
                    Body += "<tfoot>";
                    Body += "<tr>";
                    Body += "<td class='text-center'></td><td class='text-center' style='width: 25px'><b>Total</b></td>" +
                    "<td style='white-space: nowrap;text-align: right;'><b>" + TotalGrandTotal + "</b></td>" +
                    "<td style='white-space: nowrap;text-align: right;'><b>" + TotalAmtPaid + "</b></td>" +
                    "<td class='SRP' style='text-align: right; width: 28px'><b>" + TotalAmtDue + "</b></td>" +
                    "<td class='totalPrice' style='text-align: center; width: 28px; white-space: nowrap'></td>";
                    Body += "</tr>";
                    Body += "</tfoot>";

                    Body += "</table></fieldset><br/><br/>";
                }
                JArray CreditDetails = (JArray)OrderDetail["CreditDetails"];
                if (CreditDetails.Count > 0)
                {
                    for (var k = 0; k < CreditDetails.Count; k++)
                    {
                        JObject Credit = (JObject)CreditDetails[k];

                        //---------------------------------------------------------Credit Memo-----------------------------------------------------
                        Body += "<P style='page-break-before: always'></p><fieldset>";
                        Body += "<table class='table tableCSS' id='tblProductss'>";
                        Body += "<thead>";
                        Body += "<tr style='text-align:center'>";
                        Body += "<td colspan='6' style='padding:8px 8px;'>";
                        Body += "<div><span style='font-size: 20px;text-align:center;font-weight:600' id='PageHeadings'>" + Credit["CustomerName"].ToString() + " - " + Credit["CreditNo"].ToString() + "</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                        Body += "<span style='font-size: 20px;text-align:right;font-weight:600;float:right'><i>Credit Memo</i></span></div>";

                        Body += "</td>";
                        Body += "</tr>";
                        Body += "<tr>";
                        Body += "<td rowspan='2' style='width: 21% !important;'>";
                        Body += "<img src='" + CompanyDetail["LogoforEmail"].ToString() + "' id='logo' class='img-responsive' style='width: 100%;'/>";
                        Body += "</td>";
                        Body += "<td style='width: 26% !important;text-align:left;font-weight:100'><span id='CompanyName'>" + CompanyDetail["CompanyName"].ToString() + "</span></td>";
                        Body += "<td class='tdLabel' style='white-space: nowrap;width:18% !important;text-align:left;'>Customer ID</td>";
                        Body += "<td id='acNo' style='white-space: nowrap;width:23% !important;text-align:left;font-weight:100'>" + Credit["CustomerId"].ToString() + "</td>";
                        Body += "<td class='tdLabel' style='white-space: nowrap;width:13% !important;text-align:left;'>Sales Rep</td>";
                        Body += "<td id='salesRep' style='white-space: nowrap;width:19% !important;text-align:left;font-weight:100'>" + Credit["SalesPerson"].ToString() + "</td>";
                        Body += "</tr>";
                        Body += "<tr>";
                        Body += "<td style='text-align:left;font-weight:100'><span id='Address'>" + CompanyDetail["Address"].ToString() + "</span></td>";
                        Body += "<td class='tdLabel' style='white-space: nowrap;text-align:left;'>Customer Contact</td>";
                        Body += "<td id='storeName' style='white-space: nowrap;text-align:left;font-weight:100'>" + Credit["ContactPersonName"].ToString() + "</td>";
                        Body += "<td class='tdLabel' style='white-space: nowrap;text-align:left;'>Credit No</td>";
                        Body += "<td id='so' style='text-align:left;font-weight:100'>" + Credit["CreditNo"].ToString() + "</td>";
                        Body += "</tr>";
                        Body += "<tr>";
                        Body += "<td style='text-align:left;font-weight:100'><span id='Website' style='white-space: nowrap;'>" + CompanyDetail["Website"].ToString() + "</span></td>";
                        Body += "<td style='white-space: nowrap;text-align:left;font-weight:100'>Phone : <span id='Phone'> Phone :" + CompanyDetail["MobileNo"].ToString() + "</span></td>";
                        Body += "<td class='tdLabel' style='text-align:left';>Contact No</td>";
                        Body += "<td id='ContPerson' style='text-align:left;font-weight:100'>" + Credit["Contact"].ToString() + "</td>";
                        Body += "<td class='tdLabel' style='white-space: nowrap;text-align:left;'>Credit Date</td>";
                        Body += "<td id='soDate' style='white-space: nowrap;text-align:left;font-weight:100'>" + Credit["CreditDate"].ToString() + "</td>";
                        Body += "</tr>";
                        Body += "<tr>";
                        Body += "<td style='text-align:left;font-weight:100'><span id='EmailAddress'>" + CompanyDetail["EmailAddress"].ToString() + "</span></td>";
                        Body += "<td style='text-align:left;font-weight:100'>Fax : <span id='FaxNo'>" + CompanyDetail["FaxNo"].ToString() + "</span></td>";
                        Body += "<td style='text-align:left;' class='tdLabel'>Terms</td>";
                        Body += "<td style='font-size: 12px;text-align:left;font-weight:100'><b><span id='terms'>" + Credit["TermsDesc"].ToString() + "</span></b></td>";
                        Body += "<td class='tdLabel' style='text-align:left;' colspan='2'></td>";
                        Body += "</tr>";

                        if (Bn != "" && Bn != null)
                        {
                            Body += "<tr>";
                            Body += "<td><span class='tdLabel'>Business Name</span></td>";
                            Body += "<td colspan='5'><span id='BusinessName'>" + Bn + "</span></td>";
                            Body += "</tr>";
                        }
                        Body += "<tr>";
                        Body += "<td style='text-align:left;'><span class='tdLabel'>Shipping Address</span></td>";
                        try
                        {
                            




                            
                            Body += "<td colspan='5' style='text-align:left;font-weight:100'><span id='shipAddr' style='font-size: 15px'>" + OrderDetail["ShippingAddress"].ToString() + "</span></td>";

                        }
                        catch (Exception)
                        {
                            Body += "<td colspan='5' style='text-align:left;font-weight:100'>NA</td>";
                        }
                        Body += "</tr>";
                        Body += "<tr>";
                        Body += "<td style='text-align:left;'><span class='tdLabel'>Billing Address</span></td>";
                        try
                        {
                            Body += "<td colspan='5' style='text-align:left;font-weight:100'><span id='billAddr' style='font-size: 15px'>" + OrderDetail["BillingAddress"].ToString() + "</span></td>";

                        }
                        catch (Exception)
                        {
                            Body += "<td colspan='5' style='text-align:left;font-weight:100'>NA</td>";
                        }
                         Body += "</tr>";
                        Body += "</thead>";
                        Body += "</table></fieldset><br/><fieldset>";

                        Body += "<table class='table tableCSS' id='tblProductw'>";
                        Body += "<thead>";
                        Body += "<tr>";
                        Body += "<td class='text-center' style='width:8px;'>Qty</td><td style='width: 8px;white-space:nowrap' class='text-center'>Unit Type</td><td style='white-space: nowrap;width:10px;'>Product ID</td>" +
                        "<td style='white-space: nowrap;width:40px;'>Item</td>" +
                        "<td class='UnitPrice' style='text-align: center; width: 8px'>Price</td><td class='totalPrice' style='text-align: center; width: 8px; white-space: nowrap'>Total Price</td>";
                        Body += "</tr>";
                        Body += "</thead>";
                        Body += "<tbody>";
                        JArray CreditItemList = (JArray)Credit["CreditItems"];
                        //JObject Items = (JObject)ItemList[0];
                        int CountCreditItem = 0;
                        for (var l = 0; l < CreditItemList.Count; l++)
                        {
                            JObject CreditItems = (JObject)CreditItemList[l];


                            if (CreditItems.Count > 0)
                            {
                                CountCreditItem += Convert.ToInt32(CreditItems["QtyShip"].ToString());
                                Body += "<tr>";

                                Body += "<td class='text-center'>" + CreditItems["QtyShip"].ToString() + "</td><td style='width: 25px'>" + CreditItems["UnitType"].ToString() + "</td>" +
                                "<td style='white-space: nowrap' class='text-center'>" + CreditItems["ProductId"].ToString() + "</td>" +
                                "<td style='white-space: nowrap'>" + CreditItems["ProductName"].ToString() + "</td>" +
                                "<td class='UnitPrice' style='text-align: right; width: 28px'>" + CreditItems["UnitPrice"].ToString() + "</td>" +
                                "<td class='totalPrice' style='text-align: right; width: 28px; white-space: nowrap'>" + CreditItems["NetPrice"].ToString() + "</td>";
                                Body += "</tr>";
                            }
                        }
                        Body += "</tbody>";
                        Body += "</table></fieldset><br/>";


                        Body += "<fieldset>";
                        Body += "<table style='width: 100%;'>";
                        Body += "<tbody>";
                        Body += "<tr>";
                        Body += "<td style='width: 67%' colspan='2'>" + "<center></center>" + "</td>";
                        Body += "<td style='width: 33%;'>";
                        Body += "<table class='table tableCSS' style='width:100%;'>";
                        Body += "<tbody>";
                        Body += "<tr>";
                        Body += "<td class='tdLabel'>Total Qty</td>";
                        Body += "<td id='totalQty' style='text-align: right;'>" + CountCreditItem.ToString() + "</td>";
                        Body += "</tr>";
                        Body += "<tr>";
                        Body += "<td class='tdLabel'>Sub Total</td>";
                        Body += "<td style='text-align: right;'><span id='totalwoTax'>" + Credit["TotalAmount"].ToString() + "</span></td>";
                        Body += "</tr>";
                        string diss = Credit["OverallDisc"].ToString();
                        if (diss != "0.00" && diss != null)
                        {
                            if (Convert.ToDecimal(diss) > 0)
                            {
                                Body += "<tr>";
                                Body += "<td class='tdLabel'>Discount</td>";
                                Body += "<td style='text-align: right;'><span id='disc'>" + dis + "</span></td>"; ;
                                Body += "</tr>";
                            }
                        }
                        string taxs = Credit["TotalTax"].ToString();
                        if (taxs != "0.00" && taxs != null)
                        {
                            if (Convert.ToDecimal(taxs) > 0)
                            {
                                Body += "<tr>";
                                Body += "<td class='tdLabel'>Tax</td>";
                                Body += "<td style='text-align: right;'><span id='tax'>" + tax + "</span></td>";
                                Body += "</tr>";
                            }
                        }

                        decimal NTaxs = 0;

                        try
                        {
                            NTaxs = decimal.Parse(Credit["MLTax"].ToString(), CultureInfo.InvariantCulture);
                        }
                        catch (Exception)
                        {

                           
                        }

                        
                            if (Convert.ToDecimal(NTaxs) > 0)
                            {
                                Body += "<tr>";
                                Body += "<td class='tdLabel'>" + OrderDetail["MLTaxPrintLabel"].ToString() + "</td>";
                                Body += "<td style='text-align: right;'><span id='MLTax'>" + string.Format("{0:0.00}", NTaxs) + "</span></td>";
                                Body += "</tr>";
                            }
         
                        string WTaxs = Credit["WeightTaxAmount"].ToString();
                        if (WTaxs != null)
                        {
                            if (Convert.ToDecimal(WTax) > 0)
                            {
                                Body += "<tr>";
                                Body += "<td class='tdLabel'>Weight Tax</td>";
                                Body += "<td style='text-align: right;'><span id='WeightTax'>" + WTax + "</span></td>";
                                Body += "</tr>";
                            }
                        }
                        string AAmts = Credit["AdjustmentAmt"].ToString();
                        if (AAmts != null && AAmts != "0.00")
                        {
                            if (Convert.ToDecimal(AAmt) > 0)
                            {
                                Body += "<tr>";
                                Body += "<td class='tdLabel'>Adjustment</td>";
                                Body += "<td style='text-align: right;'><span id='spAdjustment'>" + AAmt + "</span></td>";
                                Body += "</tr>";
                            }
                        }
                        Body += "<tr>";
                        Body += "<td class='tdLabel'>Grand Total</td>";
                        Body += "<td style='text-align: right;'><span id='grandTotal'>" + Credit["GrandTotal"].ToString() + "</span></td>";
                        Body += "</tr>";


                        //Body += "<tr>";
                        //Body += "<td class='tdLabel'>Payable Amount</td>";
                        //Body += "<td style='text-align: right;'><span id='spnPayableAmount'>" + Ds.Tables[6].Rows[0]["TotalAmount"].ToString() + "</span></td>";
                        //Body += "</tr>";
                        Body += "</tbody>";
                        Body += "</table>";
                        Body += "</td>";
                        Body += "</tr>";
                        Body += "</tbody>";
                        Body += "</table>";
                        Body += "</fieldset><br/>";
                    }
                }
                //-------------------------------------------------------------------------------------------------------------------------
                Body += "</div>";
                Body += "</body>";
                Body += "</html>";


                string ReceiverEmailId = jdv["To"];
                if (ReceiverEmailId != null && ReceiverEmailId != string.Empty && ReceiverEmailId.Length > 0)
                {
                    string emailmsg = MailSending.SendMailResponsewithAttachment(ReceiverEmailId, "", "", jdv["Subject"], jdv["EmailBody"], "Developer", Body.ToString(), CSSStyle);

                    if (emailmsg == "Successful")
                    {
                        return "true";
                    }
                    else
                    {
                        return "EmailNotSend";
                    }
                }
                else
                {
                    return "receiverMail";
                }
            }
            catch (Exception ex)
            {
                return (new StackTrace(ex, true)).GetFrame(0).GetFileLineNumber().ToString();
            }
            //return Body;
        }
        else
        {
            return "Session Expired";
        }
    }
    private MemoryStream PDFGenerate(string message, string ImagePath)
    {

        MemoryStream output = new MemoryStream();

        Document pdfDoc = new Document(PageSize.A4, 25, 10, 25, 10);
        PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, output);

        pdfDoc.Open();
        Paragraph Text = new Paragraph(message);
        pdfDoc.Add(Text);

        byte[] file;
        file = System.IO.File.ReadAllBytes(ImagePath);

        iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(file);
        jpg.ScaleToFit(550F, 200F);
        pdfDoc.Add(jpg);

        pdfWriter.CloseStream = false;
        pdfDoc.Close();
        output.Position = 0;

        return output;
    }
}