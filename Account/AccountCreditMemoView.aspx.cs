﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DLLAccountCreditMemoView;
using Newtonsoft.Json;

public partial class Account_AccountCreditMemoView : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Account/JS/AccountCreditMemoView.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredField'>" + text + "</script>"
                ));
        }
    }

    [WebMethod(EnableSession = true)]
    public static string OrderRemarkDeatil(string CreditAutoId)
    {
        PL_AccountCreditMemoView pobj = new PL_AccountCreditMemoView();
        pobj.CreditAutoId = Convert.ToInt32(CreditAutoId);
        BL_AccountCreditMemoView.BindCreditLog(pobj);
        return pobj.Ds.GetXml();
    }
    [WebMethod(EnableSession = true)]
    public static string editCredit(string CreditAutoId)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_AccountCreditMemoView pobj = new PL_AccountCreditMemoView();
            pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
            pobj.CreditAutoId = Convert.ToInt32(CreditAutoId);
            BL_AccountCreditMemoView.EditCredit(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Session Expired";
        }
    }


   
    [WebMethod(EnableSession = true)]
    public static string PrintCredit(string CreditAutoId)
    {
        PL_AccountCreditMemoView pobj = new PL_AccountCreditMemoView();
        pobj.CreditAutoId = Convert.ToInt32(CreditAutoId);
        BL_AccountCreditMemoView.PrintCredit(pobj);
        return pobj.Ds.GetXml();

    }

    [WebMethod(EnableSession = true)]
    public static string CheckSecurity(string datavalue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(datavalue);
        PL_AccountCreditMemoView pobj = new PL_AccountCreditMemoView();
        try
        {
            if (HttpContext.Current.Session["EmpTypeNo"] != null)
            {
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpTypeNo"].ToString());
                pobj.SecurityKey = jdv["Security"];
                BL_AccountCreditMemoView.checkSecurity(pobj);
                if (!pobj.isException)
                {
                  
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "SessionExpired";

            }
        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string CancelCreditMomo(string datavalue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(datavalue);
            PL_AccountCreditMemoView pobj = new PL_AccountCreditMemoView();
            pobj.EmpAutoId = Convert.ToInt32((HttpContext.Current.Session["EmpAutoId"]).ToString());
            pobj.CreditAutoId = Convert.ToInt32(jdv["CreditAutoId"]);
            pobj.CancelRemarks = jdv["CancelRemark"];
            BL_AccountCreditMemoView.CancelCreditMemo(pobj);
            if (!pobj.isException)
            {
                return pobj.Ds.GetXml();
            }
            else
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}