﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="PattyCash.aspx.cs" Inherits="Account_PattyCash" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Petty Cash</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin/mydashboard.aspx">Home</a></li>

                        <li class="breadcrumb-item">Petty Cash</li>

                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" id="btnAddBalance" onclick="AddBalance()">Add Balance</button>
                    <button type="button" class="dropdown-item" id="btnWithdraw" onclick="openPopupWithdraw()">Withdraw Balance</button>
                    <button type="button" class="dropdown-item" id="btnExport" style="display: none" onclick="Export()">Export</button>
                </div>
            </div>
        </div>
    </div>


    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-3 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 0.5rem;">
                                                    <span class="la la-calendar-o pl-0"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control input-sm border-primary" placeholder="From Date" id="txtSFromDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 0.5rem;">
                                                    <span class="la la-calendar-o pl-0"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control input-sm border-primary" placeholder="To Date" id="txtSToDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <div class="form-group">
                                            <select id="ddlSCategory" class="form-control input-sm border-primary">
                                                <option value="0">All Category</option>
                                            </select>

                                        </div>
                                    </div>
                                      <div class="col-md-3 form-group">
                                        <div class="form-group">
                                            <select id="ddlTtype" class="form-control input-sm border-primary">
                                                <option value="0">All Transaction Type</option>
                                                <option value="CR">Add</option>
                                                <option value="DR">Withdraw</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                  
                                    <div class="col-md-3 form-group">
                                        <div class="form-group">
                                            <select id="ddlSearchBy" class="form-control input-sm border-primary">
                                                <option value="0">All Condition</option>
                                                <option value="<"><</option>
                                                <option value=">">></option>
                                                <option value="<="><=</option>
                                                <option value=">=">>=</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <div class="input-group ">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 0.5rem;">
                                                    <span class="pl-0">$</span>
                                                </span>
                                            </div>
                                            <input type="text" id="txtamount" class="form-control input-sm border-primary text-right" onkeyup="amount()" maxlength="5" value="0.00" onkeypress="return isNumberKey(event)" />
                                        </div>
                                    </div>

                                      <div class="col-md-3" id="hdnAvailable" style="display:none">
                                        <div class="form-group" id="hideAvilableBalance" style="display: none">
                                            <label style="font-size: 17px; color: green">Available Balance =</label>&nbsp;<label id="lblAvailableBalance" style="font-size: 17px">0.00</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <button type="button" class="btn btn-info buttonAnimation pull-left round box-shadow-1 btn-sm" id="btnSearch" onclick="Search();">Search</button>
                                    </div>
                                </div>
                                <div class="row form-group">
                                  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblPattyCashist">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="action text-center">Action</td>
                                                        <td class="Date text-center">Date</td>
                                                        <td class="TransactionType text-center">Transaction Type</td>
                                                        <td class="TransactionAmount price">Transaction Amount</td>
                                                        <td class="Category text-center">Category</td>
                                                        <td class="AvailableBalance price">Available Balance</td>
                                                        <td class="Remark ">Remark</td>
                                                        <td class="username ">User Name</td>
                                                    </tr>
                                                    <tr style="background-color: aliceblue !important">
                                                        <td id="DateOpening text-center" style="background-color: oldlace; color: green; font-weight: bold"></td>
                                                        <td colspan="1" style="background-color: oldlace; padding: 15px; color: green; font-weight: bold;">Opening Balance</td>
                                                        <td style="background-color: oldlace"></td>
                                                        <td id="Total" style="text-align: right !important; padding: 15px; background-color: oldlace; color: green; font-weight: bold; font-size: 13px">0.00</td>
                                                        <td style="background-color: oldlace"></td>
                                                        <td style="background-color: oldlace"></td>
                                                        <td style="background-color: oldlace"></td>
                                                        <td style="background-color: oldlace"></td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td id="DateClosing" style="text-align: left; color: maroon"></td>
                                                        <td colspan="1" style="text-align: left; color: maroon">Closing Balance</td>
                                                        <td></td>
                                                        <td id="gTotal" style="color: maroon; font-size: 17px; text-align: right; font-weight: 600;">0.00</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex">
                                    <div class="form-group">
                                        <select class="form-control input-sm border-primary" id="ddlPageSize" onchange=" bindPattyCashTransactionLog(1);">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">ALL</option>
                                        </select>
                                    </div>
                                    <div class="ml-auto">
                                        <div id="page" class="Pager"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </section>
    </div>
    <%--------------------------------------------------Export Table------------------------------------------------------------------------------%>

    <table class="table table-striped table-bordered" id="tblPattyCashistExport" style="display: none">
        <thead>
            <tr>
                <td class="Date text-center">Date</td>
                <td class="TransactionType text-center" style="text-align: center">Transaction Type</td>
                <td class="TransactionAmount price" style="text-align: right">Transaction Amount</td>
                <td class="Category text-center" style="text-align: center">Category</td>
                <td class="AvailableBalance price" style="text-align: right">Available Balance</td>
                <td class="Remark">Remark</td>
                <td class="username ">User Name</td>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>


    <div id="modalAddBlc" class="modal fade" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg ">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Payment</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <label class="control-label">Current Balance</label>
                            </div>
                            <div class="col-md-8 form-group">
                                <div class="input-group ">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                            <span>$</span>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control input-sm border-primary" readonly="readonly" id="txtCurrentBlc" style="text-align: right" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label class="control-label">Date</label>
                                <span class="required">*</span>
                            </div>
                            <div class="col-md-8 form-group">
                                <div style="display: flex">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" style="padding: 0rem 1rem;">
                                                <span class="la la-calendar-o"></span>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control input-sm border-primary req" id="txtAddTime" runat="server" />
                                    </div>
                                    &nbsp;&nbsp;
                             <div class="input-group">
                                 <div class="input-group-prepend">
                                     <span class="input-group-text" style="padding: 0rem 1rem;">
                                         <span class="la la-calendar-o"></span>
                                     </span>
                                 </div>
                                 <input type="text" class="form-control input-sm  border-primary req" id="txtAddTime1" runat="server" />
                             </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label class="control-label">Category</label>
                                 <span class="required">*</span>
                            </div>
                            <div class="col-md-8 form-group">

                                <select id="ddlAddCategory" class="form-control input-sm border-primary ddlreq">
                                    <option value="0">Select</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label class="control-label">Add Balance</label>
                                <span class="required">*</span>
                            </div>
                            <div class="col-md-8 form-group">
                                <div class="input-group ">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                            <span>$</span>
                                        </span>
                                    </div>
                                    <input runat="server" class="form-control input-sm border-primary req" maxlength="13" id="txtAddBlc" onkeyup="AddBlc()" style="text-align: right" onkeypress="javascript:return isNumber(event)" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label class="control-label">Available Balance</label>
                            </div>
                            <div class="col-md-8 form-group">
                                <div class="input-group ">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                            <span>$</span>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control input-sm border-primary" readonly="readonly" id="txtNewAvlBlc" style="text-align: right" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label class="control-label">
                                    Remark                                
                            <span class="required">*</span>
                                </label>
                            </div>
                            <div class="col-md-8 form-group">
                                <textarea class="form-control input-sm border-primary req" id="txtAddRemark"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" value="Add" id="btnAdd" class="btn btn-primary buttonAnimation pull-right round box-shadow-1 btn-sm" onclick="Add();"/>
                    <input type="button" value="Update" id="btnAddUpdate" class="btn btn-success buttonAnimation pull-right round box-shadow-1 btn-sm" onclick="AddUpdate();" style="display: none" />
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="modalWithdrawBlc" class="modal fade" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Withdraw Payment</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">
                            <label class="control-label">Current Balance</label>
                        </div>
                        <div class="col-md-8 form-group">
                            <div class="input-group ">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" style="padding: 0rem 1rem;">
                                        <span>$</span>
                                    </span>
                                </div>
                                <input type="text" class="form-control input-sm border-primary" readonly="readonly" id="txtwithdrawCurrentBlc" style="text-align: right" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="control-label">Date</label>
                            <span class="required">*</span>
                        </div>
                        <div class="col-md-8 form-group">
                            <div style="display: flex">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                            <span class="la la-calendar-o"></span>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control input-sm border-primary reqw" id="txtWithdrawTime" runat="server" />
                                </div>
                                &nbsp;&nbsp;
                            <div class="input-group">
                                <div class="input-group-append">
                                    <span class="input-group-text" style="padding: 0rem 1rem;">
                                        <span class="ft-clock"></span>
                                    </span>
                                </div>
                                <input type="text" class="form-control input-sm border-primary reqw" id="txtWithdrawTime1" runat="server" />
                            </div>
                            </div>
                        </div>
                        <div id="wrapper1"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="control-label">Category</label>
                        </div>
                        <div class="col-md-8 form-group">

                            <select id="ddlWCategory" class="form-control input-sm border-primary ddlreqw">
                                <option value="0">--Select--</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="control-label">Withdraw Amount</label>
                            <span class="required">*</span>
                        </div>
                        <div class="col-md-8 form-group">
                            <div class="input-group ">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" style="padding: 0rem 1rem;">
                                        <span>$</span>
                                    </span>
                                </div>
                                <input type="text" class="form-control input-sm border-primary reqw" onchange="calTransactionAmt()" runat="server" id="txtTransactionAmount" maxlength="13" style="text-align: right" onkeypress="javascript:return isNumber(event)" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="control-label">Available Balance</label>
                        </div>
                        <div class="col-md-8 form-group">
                            <div class="input-group ">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" style="padding: 0rem 1rem;">
                                        <span>$</span>
                                    </span>
                                </div>
                                <input class="form-control input-sm border-primary" readonly="readonly" id="txtAvailableBalance" style="text-align: right" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="control-label">Remark</label>
                        </div>
                        <div class="col-md-8 form-group">
                            <textarea class="form-control input-sm border-primary" id="txtRemark"></textarea>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="button" value="Withdraw" id="addWithdraw" class="btn btn-primary buttonAnimation pull-right round box-shadow-1 btn-sm"  onclick="Withdraw();"/>
                    <input type="button" value="Update" id="UpdateWithdraw" class="btn btn-success buttonAnimation pull-right round box-shadow-1 btn-sm" onclick="UpdateWithdrawBalance();" style="display: none" />
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <%------------------------------------------------------------------------------%>
    <div class="modal fade" id="SecurityEnabledVoid" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Security Check </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">Security </div>
                        <div class="col-md-9">
                            <input type="password" id="txtSecurityVoid" class="form-control input-sm border-primary" />
                        </div>
                    </div>

                    <br />
                    <div class="row">
                        <div class="col-md-12">
                            <span id="errormsgVoid"></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1 btn-sm" onclick="clickonSecurityVoid()">OK</button>
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="SecurityEnvalid" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-xl">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Message</h4>
                </div>
                <div class="modal-body">
                    <div id="Envalid" style="border-color: #ac2925; border: 1px solid; color: #ac2925; min-height: 45px; line-height: 20px; padding: 9px;">
                        <strong id="Sbarcodemsg"></strong>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="JS/PattyCash.js?v=' + new Date() + '"></scr' + 'ipt>');
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <script src="../js/jquery.table2excel.js"></script>

    <script>
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
    </script>
</asp:Content>

