﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/MasterPage.master" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .Pager {
            text-align: right;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Order List</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Manage Order</a>
                        </li>
                        <li class="breadcrumb-item">Order List
                        </li>
                    </ol>
                </div>
            </div>
        </div>

    </div>

    <div class="content-body" style="min-height: 400px">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-3">
                                        <select class="form-control border-primary input-sm"  style="width: 100% !important;" id="ddlSalesPerson" runat="server" onchange=" BindCustomer();">
                                            <option value="0">All Sales Person</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <select class="form-control border-primary input-sm"  style="width: 100% !important;" id="ddlCustomertype" onchange=" BindCustomer();">
                                            <option value="0">All Customer Type</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <select class="form-control border-primary input-sm"  style="width: 100% !important;" id="ddlCustomer" runat="server">
                                            <option value="0">All Customer</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <select class="form-control border-primary input-sm"  style="width: 100% !important;" id="ddlDriver" runat="server">
                                            <option value="0">All Driver</option>
                                        </select>
                                    </div>

                                </div>
                                <div class="row form-group">

                                    <div class="col-sm-12 col-md-3">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Order No" id="txtSOrderNo" />
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <select id="ddlSStatus"  style="width: 100% !important;" class="form-control border-primary input-sm">
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <select id="ddlOrderType"  style="width: 100% !important;" class="form-control border-primary input-sm">
                                            <option value="0">All Order Type</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <input type="text" class="form-control border-primary input-sm" onkeypress="return isNumberDecimalKey(event,this)" maxlength="8" style="text-align: right" placeholder="Payable Amount" id="txtPayableAmt" />
                                    </div>

                                </div>
                                <div class="row form-group">

                                    <div class="col-sm-12 col-md-3">
                                        <select id="ddlShippingType"  style="width: 100% !important;" class="form-control border-primary input-sm">
                                            <option value="0">All Shipping Type</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0 1rem">Order From Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm date" placeholder="Order From Date" id="txtSFromDate" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0 1rem">Order To Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm date" placeholder="Order To Date" id="txtSToDate" />
                                        </div>
                                    </div>
                                    <div class="col-sm-3 pull-right">
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1 btn-sm" id="btnSearch" onclick="getDelOrdersList(1)">Search</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblDelOrders">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="Action text-center">Action</td>
                                                        <td class="OrderNo text-center">Order No</td>
                                                        <td class="OrderType  text-center">Order Type</td>
                                                        <td class="OrderDt  text-center">Order Date</td>
                                                        <td class="Customer">Customer</td>
                                                        <td class="SalesPerson">Sales Person</td>
                                                        <td class="dr">Driver</td>
                                                        <td class="DelDate text-center">Del. Date</td>
                                                        <td class="ShippingType">Shipping Type</td>
                                                        <td class="Status">Status</td>
                                                        <td class="OrderVal price">Payable Amount</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2 col-sm-2 form-group" style="padding-right: 110px">
                                        <select id="ddlPaging" class="form-control input-sm border-primary" onchange="getDelOrdersList(1)">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="/Account/JS/deliveredOrders.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
</asp:Content>

