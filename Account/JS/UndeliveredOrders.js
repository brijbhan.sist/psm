﻿$(document).ready(function () {
    $('.date').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    BindCustomerDetails();
    getUndelOrdersList(1);
});

function Pagevalue(e) {
    getUndelOrdersList(parseInt($(e).attr("page")));
};

function BindCustomerDetails() {
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/orderList.asmx/bindStatus",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var status = $(xmldoc).find("Table");
                var customer = $(xmldoc).find("Table1");

                $("#ddlSStatus option:not(:first)").remove();
                $.each(status, function () {
                    $("#ddlSStatus").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("StatusType").text() + "</option>");
                });
                $("#ddlCustomer option:not(:first)").remove();
                $.each(customer, function () {
                    $("#ddlCustomer").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("Customer").text() + "</option>");
                });
                $("#ddlCustomer").select2();

            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function getUndelOrdersList(pageIndex) {
    var search = {
        OrderNo: $("#txtSOrderNo").val(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        CustomerAutoId: $("#ddlCustomer").val(),
        StatusAutoId: $("#ddlSStatus").val(),
        PageIndex: pageIndex
    };

    $.ajax({
        type: "POST",
        url: "/Account/WebAPI/UndeliveredOrders.asmx/getUndelOrderList",
        data: "{'dataValues':'" + JSON.stringify(search) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var orderList = $(xmldoc).find("Table1");

            $("#tblUndelOrders tbody tr").remove();
            var row = $("#tblUndelOrders thead tr").clone(true);

            if (orderList.length > 0) {
                $("#EmptyTable").hide();
                $.each(orderList, function () {
                    if ($(this).find('StatusCode').text() == 11 || $(this).find('StatusCode').text() == 8) {
                        $(".OrderNo", row).html("<span OrderAutoId='" + $(this).find("AutoId").text() + "'><a href='/Account/Acccount_viewOrder.aspx?OrderAutoId=" + $(this).find("AutoId").text() + "'>" + $(this).find("OrderNo").text() + "</a></span>");

                    } else {
                        $(".OrderNo", row).html("<span OrderAutoId='" + $(this).find("AutoId").text() + "'><a href='/Driver/updateDelivery.aspx?OrderAutoId=" + $(this).find("AutoId").text() + "'>" + $(this).find("OrderNo").text() + "</a></span>");
                    }
                    //$(".OrderNo", row).html("<span OrderAutoId='" + $(this).find("AutoId").text() + "'><a href='/Sales/orderMaster.aspx?OrderNo=" + $(this).find("OrderNo").text() + "'>" + $(this).find("OrderNo").text() + "</a></span>");
                    $(".OrderDt", row).text($(this).find("OrderDate").text());
                    $(".Customer", row).text($(this).find("CustomerName").text());
                    $(".DelDate", row).text($(this).find("DelDate").text());
                    $(".OrderVal", row).text($(this).find("GrandTotal").text());
                   
                    if (Number($(this).find("StatusCode").text()) == 1) {
                        $(".Status", row).html("<span class='badge badge badge-pill badge-info mr-2'>" + $(this).find("Status").text() + "</span>");
                    } else if (Number($(this).find("StatusCode").text()) == 2) {
                        $(".Status", row).html("<span class='badge badge badge-pill badge-primary mr-2'>" + $(this).find("Status").text() + "</span>");
                    } else if (Number($(this).find("StatusCode").text()) == 3) {
                        $(".Status", row).html("<span class='badge badge badge-pill badge-primary mr-2' style='background-color:#66ff99'>" + $(this).find("Status").text() + "</span>");
                    } else if (Number($(this).find("StatusCode").text()) == 4) {
                        $(".Status", row).html("<span class='badge badge badge-pill badge-primary mr-2' style='background-color:#669900'>" + $(this).find("Status").text() + "</span>");
                    } else if (Number($(this).find("StatusCode").text()) == 5) {
                        $(".Status", row).html("<span class='badge badge badge-pill badge-warning mr-2' style='background-color:#33cc33'>" + $(this).find("Status").text() + "</span>");
                    } else if (Number($(this).find("StatusCode").text()) == 6) {
                        $(".Status", row).html("<span class='badge badge badge-pill badge-success mr-2'>" + $(this).find("Status").text() + "</span>");
                    } else if (Number($(this).find("StatusCode").text()) == 7) {
                        $(".Status", row).html("<span class='badge badge badge-pill badge-danger mr-2' style='background-color:#660000'>" + $(this).find("Status").text() + "</span>");
                    } else if (Number($(this).find("StatusCode").text()) == 8) {
                        $(".Status", row).html("<span class='badge badge badge-pill badge-danger mr-2' style='background-color:#ff9933'>" + $(this).find("Status").text() + "</span>");
                    }
                    else if (Number($(this).find("StatusCode").text()) == 9) {
                        $(".Status", row).html("<span class='badge badge badge-pill badge-danger mr-2' style='background-color:green'>" + $(this).find("Status").text() + "</span>");
                    }
                    else if (Number($(this).find("StatusCode").text()) == 10) {
                        $(".Status", row).html("<span class='badge badge badge-pill badge-danger mr-2' style='background-color:#61b960;'>" + $(this).find("Status").text() + "</span>");
                    }
                    else if (Number($(this).find("StatusCode").text()) == 11) {
                        $(".Status", row).html("<span class='badge badge badge-pill badge-danger mr-2'>" + $(this).find("Status").text() + "</span>");
                    }

                    $(".UndelRemarks", row).text($(this).find("DrvRemarks").text());
                   
                    $("#tblUndelOrders tbody").append(row);
                    row = $("#tblUndelOrders tbody tr:last").clone(true);
                });
            } else {
                $("#tblUndelOrders tbody tr").remove();
                $("#EmptyTable").show();
            }

            var pager = $(xmldoc).find("Table");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}