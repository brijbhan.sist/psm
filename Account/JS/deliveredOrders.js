﻿$(document).ready(function () {
    $('.date').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    }); 
    var d = new Date();
    var month = d.getMonth() + 1;
    if (month.toString().length == 1) {
        month = '0' + month;
    }
    var day = d.getDate();
    if (day.toString().length == 1) {
        day = '0' + day;
    }
    var year = d.getFullYear();
    $("#txtSFromDate").val(month + '/' + day + '/' + year);
    $("#txtSToDate").val(month + '/' + day + '/' + year);
    BindCustomerDetails();
    BindCustomer();
    getDelOrdersList(1);
});

function BindCustomerDetails() {
    $.ajax({
        type: "POST",
        url: "/Account/WebAPI/AccountOrderList.asmx/bindStatus",
        data: "{}",
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            console.log(response);
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var status = $(xmldoc).find("Table");
                var customer = $(xmldoc).find("Table1");
                var SalesPerson = $(xmldoc).find("Table2");
                var driverdetails = $(xmldoc).find("Table3");
                var ShippingType = $(xmldoc).find("Table4");
                var Customertypes = $(xmldoc).find("Table5");
                var OrderType = $(xmldoc).find("Table6");

                $("#ddlSStatus option:not(:first)").remove();
                $.each(status, function () {
                    $("#ddlSStatus").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("StatusType").text() + "</option>");
                });
                $("#ddlSStatus").val(6);
                $("#ddlSStatus").select2();
                $("#ddlSalesPerson option:not(:first)").remove();
                $.each(SalesPerson, function () {
                    $("#ddlSalesPerson").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("EmpName").text() + "</option>");
                });
                $("#ddlSalesPerson").select2();
                $("#ddlDriver option:not(:first)").remove();
                $.each(driverdetails, function () {
                    $("#ddlDriver").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("EmpName").text() + "</option>");
                });
                $("#ddlDriver").select2();
                $("#ddlCustomertype option:not(:first)").remove();
                $.each(Customertypes, function () {
                    $("#ddlCustomertype").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("CustomerType").text() + "</option>");
                });
                $("#ddlCustomertype").select2();

                $("#ddlOrderType option:not(:first)").remove();
                $.each(OrderType, function () {
                    $("#ddlOrderType").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("OrderType").text() + "</option>");
                });
                $("#ddlOrderType").select2();
                 $("#ddlShippingType option:not(:first)").remove();
                $.each(ShippingType, function () {
                    $("#ddlShippingType").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("ShippingType").text() + "</option>");
                });
                $("#ddlShippingType").select2();
            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

/*---------------------------------------------------Bind Category-----------------------------------------------------------*/
function BindCustomer() {
    var Customertype = $("#ddlCustomertype").val();
    var SalesPerson = $("#ddlSalesPerson").val();
    $.ajax({
        type: "POST",
        url: "/Account/WebAPI/AccountOrderList.asmx/BindCustomer",
        data: "{'Customertype':'" + Customertype + "','SalesPersonAutoId':'" + SalesPerson + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        asyn: false,
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {

                    var xmldoc = $.parseXML(response.d);
                    var CustomerDetails = $(xmldoc).find("Table");

                    $("#ddlCustomer option:not(:first)").remove();
                    $.each(CustomerDetails, function () {
                        $("#ddlCustomer").append("<option value='" + $(this).find("CustomerAutoId").text() + "'>" + $(this).find("CustomerName").text() + "</option>");
                    });
                    $("#ddlCustomer").select2(); 
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}


function Pagevalue(e) {
    getDelOrdersList(parseInt($(e).attr("page")));
};

function getDelOrdersList(pageIndex) {
   
    var search = {
        OrderNo: $("#txtSOrderNo").val().trim(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        CustomerAutoId: $("#ddlCustomer").val(),
        SalesPersonAutoId: $("#ddlSalesPerson").val(),
        CustomerType: $("#ddlCustomertype").val(),
        ShippingType: $("#ddlShippingType").val(),
        DriverAutoId: $("#ddlDriver").val(),
        OrderType: $("#ddlOrderType").val(),
        PayableAmount: $("#txtPayableAmt").val().trim(),
        Status: $("#ddlSStatus").val(),
        PageSize: $("#ddlPaging").val(),
        PageIndex: pageIndex
    };

    $.ajax({
        type: "POST",
        url: "/Account/WebAPI/AccountOrderList.asmx/getDelOrdersList",
        data: "{'dataValues':'" + JSON.stringify(search) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            if (response.d == 'session') {
                location.href = '/';
            } else {
                var xmldoc = $.parseXML(response.d);
                var orderList = $(xmldoc).find("Table1");

                $("#tblDelOrders tbody tr").remove();
                var row = $("#tblDelOrders thead tr").clone(true);

                if (orderList.length > 0) {
                    $("#EmptyTable").hide();
                    $.each(orderList, function () {
                        if ($(this).find('StatusCode').text() == 11 || $(this).find('StatusCode').text() == 8) {
                            $(".OrderNo", row).html("<span OrderAutoId='" + $(this).find("AutoId").text() + "'><a href='/Account/Acccount_viewOrder.aspx?OrderAutoId=" + $(this).find("AutoId").text() + "'>" + $(this).find("OrderNo").text() + "</a></span>");

                        } else {
                            if ($(this).find("CustomerType").text() == "3") {
                                $(".OrderNo", row).html("<span OrderAutoId='" + $(this).find("AutoId").text() + "'><a href='/Account/InternalUpdateDelivery.aspx?OrderAutoId=" + $(this).find("AutoId").text() + "'>" + $(this).find("OrderNo").text() + "</a></span>");
                            } else {
                                $(".OrderNo", row).html("<span OrderAutoId='" + $(this).find("AutoId").text() + "'><a href='/Driver/updateDelivery.aspx?OrderAutoId=" + $(this).find("AutoId").text() + "'>" + $(this).find("OrderNo").text() + "</a></span>");
                            }

                        }

                        if ($(this).find('StatusCode').text() == 11 || $(this).find('StatusCode').text() == 8) {
                            $(".Action", row).html("<a href='/Account/Acccount_viewOrder.aspx?OrderAutoId=" + $(this).find("AutoId").text() + "'><span class='la la-eye' title='View'></span></a>&nbsp;&nbsp;<a href='#' onclick='SendEmailPopup(" + $(this).find("AutoId").text() + ")'><span class='ft-mail' title='Send Mail'></span></a>");

                        } else {
                            if ($(this).find("CustomerType").text() == "3") {
                                $(".Action", row).html("<a href='/Account/InternalUpdateDelivery.aspx?OrderAutoId=" + $(this).find("AutoId").text() + "'><span class='la la-eye' title='View'></span></a>&nbsp;&nbsp;<a href='#' onclick='SendEmailPopup(" + $(this).find("AutoId").text() + ")'><span class='ft-mail' title='Send Mail'></span></a>");
                            } else {
                                $(".Action", row).html("<a href='/Driver/updateDelivery.aspx?OrderAutoId=" + $(this).find("AutoId").text() + "'><span class='la la-eye' title='View'></span></a>&nbsp;&nbsp;<a href='#' onclick='SendEmailPopup(" + $(this).find("AutoId").text() + ")'><span class='ft-mail' title='Send Mail'></span></a>");
                            }

                        }
                        $(".OrderDt", row).text($(this).find("OrderDate").text());
                        $(".OrderType", row).text($(this).find("OrderType").text());
                        $(".SalesPerson", row).text($(this).find("SalesPerson").text());
                        $(".Customer", row).text($(this).find("CustomerName").text());
                        $(".DelDate", row).text($(this).find("DelDate").text());
                        $(".OrderVal", row).text($(this).find("GrandTotal").text());
                        $(".ShippingType", row).text($(this).find("ShippingType").text());
                        $(".dr", row).text($(this).find("DriverName").text());

                       
                        if ($(this).find('StatusCode').text() == 6) {
                            $(".Status", row).html("<span class='badge badge badge-pill badge-info'>" + $(this).find("Status").text() + "</span>");
                        } else if ($(this).find('StatusCode').text() == 11) {
                            $(".Status", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("Status").text() + "</span>");
                        }
                        else {
                            $(".Status", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("Status").text() + "</span>");
                        }

                        $("#tblDelOrders tbody").append(row);
                        row = $("#tblDelOrders tbody tr:last").clone(true);
                    });
                } else {
                    $("#tblDelOrders tbody tr").remove();
                    $("#EmptyTable").show();
                }

                var pager = $(xmldoc).find("Table");
                $(".Pager").ASPSnippets_Pager({
                    ActiveCssClass: "current",
                    PagerCssClass: "pager",
                    PageIndex: parseInt(pager.find("PageIndex").text()),
                    PageSize: parseInt(pager.find("PageSize").text()),
                    RecordCount: parseInt(pager.find("RecordCount").text())
                });
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function SendEmailPopup(OrderAutoId) {
    window.open("/EmailSender/SendEmail.aspx?OrderAutoId=" + OrderAutoId + "", "popUpWindow", "height=700,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");

}
