﻿var TransactionAutoId = 0;
$(document).ready(function () {
    $("#txtNewAvlBlc").val("0.00");
    $("#txtAddBlc").val("0.00");
    $("#txtTransactionAmount").val("0.00");
    $("#txtAvailableBalance").val("0.00");

    $('#txtSFromDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtSToDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });

    $('#txtAddTime').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtWithdrawTime').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });

    $('#txtWithdrawTime1').pickatime({
        interval: 5
    });
    $('#txtAddTime1').pickatime({
        interval: 5
    });

    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    $("#txtSFromDate").val(month + '/' + day + '/' + year);
    $("#txtSToDate").val(month + '/' + day + '/' + year);


    getTransactionBalance();
    Binddropdown();
});

function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}
function Binddropdown() {

    $.ajax({
        type: "POST",
        url: "PattyCash.aspx/Binddropdown",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: false,
        cache: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                
                var getData = $.parseJSON(response.d);

                $("#ddlAddCategory option:not(:first)").remove();
                $.each(getData, function (index, item) {
                    $("#ddlAddCategory").append("<option value='" + getData[index].AutoId + "'>" + getData[index].CategoryName + "</option>");
                });
                $("#ddlAddCategory").select2();
                $("#ddlAddCategory").select2({ width: '100%' });

                $("#ddlWCategory option:not(:first)").remove();
                $.each(getData, function (index, item) {

                    $("#ddlWCategory").append("<option value='" + getData[index].AutoId + "'>" + getData[index].CategoryName + "</option>");
                });
                $("#ddlWCategory").select2();
                $("#ddlWCategory").select2({ width: '100%' });
                $("#ddlSCategory option:not(:first)").remove();
                $.each(getData, function (index, item) {

                    $("#ddlSCategory").append("<option value='" + getData[index].AutoId + "'>" + getData[index].CategoryName + "</option>");
                });
                $("#ddlSCategory").select2();


            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}

function AddBalance() {

    getTransactionBalance();
    $("#txtNewAvlBlc").val("0.00");
    $("#txtAddBlc").val("0.00");
    $("#txtAddRemark").val('');
    $("#ddlAddCategory").val('0').change();
    $("#txtAddTime").val(new Date().toLocaleDateString());
    $("#txtAddTime1").val(formatAMPM(new Date));
    $("#txtAddTime").attr('disabled', false);
    $("#txtAddTime1").attr('disabled', false);
    $("#btnAddUpdate").hide();
    $("#btnAdd").show();
    $("#alertDanger").hide();
    $("#modalAddBlc").modal('show');

    if (check == true) {
        bindPattyCashTransactionLog(1);
    }
}
function openPopupWithdraw() {
    getTransactionBalance();
    $("#txtTransactionAmount").val('0.00');
    $("#txtAvailableBalance").val('0.00');
    $("#ddlWCategory").val('0').change();
    $("#txtRemark").val('');
    $("#txtWithdrawTime").val(new Date().toLocaleDateString());
    $("#txtWithdrawTime").attr('disabled', false);
    $("#txtWithdrawTime1").val(formatAMPM(new Date));
    $("#txtWithdrawTime1").attr('disabled', false);
    $("#UpdateWithdraw").hide();
    $("#addWithdraw").show();
    $("#WalertDanger").hide();
    $("#modalWithdrawBlc").modal('show');
}
function Add() {
    if (checkRequiredField()) {

        var TransactionDate = $("#txtAddTime").val() + ' ' + $("#txtAddTime1").val();
        save($("#txtAddBlc").val(), 'CR', $("#ddlAddCategory").val(), $("#txtAddRemark").val(), TransactionDate)

        getTransactionBalance();
        $("#txtNewAvlBlc").val("0.00");
        $("#txtAddBlc").val("0.00");
        $("#txtAddRemark").val('');
        $("#txtAddTime").val('');
        $("#txtAddTime1").val('');
        $("#ddlAddCategory").val(0).change();

    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
};
function save(TransactionAmount, TransactionType, Category, Remark, TransactionDate) {
    
    var data = {
        transactionAmount: TransactionAmount,
        transactionType: TransactionType,
        Category: Category,
        remark: Remark,
        TransactionDate: TransactionDate
    }
    $.ajax({
        type: "POST",
        url: "WebAPI/PattyCash.asmx/updateTransaction",
        //data: "{'datavalue':'" + JSON.stringify(data) + "'}",
        data: JSON.stringify({ datavalue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (result) {
            if (result.d == 'SessionExpired') {
                location.href = '/'
            }
            else if (result.d == 'true') {
                if (TransactionType == 'CR') {
                    swal("", "Balance added successfully.", "success");
                    $("#modalAddBlc").modal('hide');

                    getTransactionBalance();
                    if (check == true) {
                        bindPattyCashTransactionLog(1);
                    }
                }
                else if (TransactionType == 'DR') {
                    swal("", "Balance withdraw  successfully.", "success");
                    $("#modalWithdrawBlc").modal('hide');
                    getTransactionBalance();
                    bindPattyCashTransactionLog(1);
                }
                else {
                    swal("Error!", result.d, "error");
                }
            } else {
                swal("Error!", result.d, "error");
            }
        },
        error: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        },
        failure: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        }
    });
}

function Withdraw() {
    
    if (checkRequiredFieldWithdraw()) {
        if ($("#txtTransactionAmount").val() == '0' || $("#txtTransactionAmount").val() == '0.0' || $("#txtTransactionAmount").val() == '0.00') {
            toastr.error('Please enter a valid amount.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            $("#txtTransactionAmount").addClass('border-warning');
        }
        else {
            var data = {
                NewAmount: $("#txtNewAvlBlc").val()
            }
            var TransactionDate = $("#txtWithdrawTime").val() + ' ' + $("#txtWithdrawTime1").val();
            save($("#txtTransactionAmount").val(), 'DR', $("#ddlWCategory").val(), $("#txtRemark").val(), TransactionDate);
            getTransactionBalance();
            $("#txtTransactionAmount").val('0.00');
            $("#txtAvailableBalance").val('0.00');
            $("#txtRemark").val('');
            $("#txtWithdrawTime").val('');
            $("#txtWithdrawTime1").val('');
            $("#ddlWCategory").val().change = 0;
        }
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}

function checkRequiredFieldWithdraw() {
    var boolcheck = true;
    $('.reqw').each(function () {
        if ($(this).val() == '') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });
    $('.ddlreqw').each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == 'undefined' || $(this).val() == null || $(this).val() == 'Select') {
            boolcheck = false;
            $(this).closest('div').find('.select2-selection--single').attr('style', 'border:1px solid #FF9149  !important');
        } else {
            $(this).removeClass('border-warning');
            $(this).closest('div').find('.select2-selection--single').removeAttr('style');
        }
    });
    return boolcheck;
}

function getTransactionBalance() {
    $.ajax({
        type: "POST",
        url: "WebAPI/PattyCash.asmx/getTransactionBalance",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == 'SessionExpired') {
                location.href = '/';
            } else {
                var xmldoc = $.parseXML(response.d);
                var orderList = $(xmldoc).find("Table");

                if (orderList.length > 0) {
                    $.each(orderList, function () {
                        $("#txtCurrentBlc").val($(orderList).find("CurrentBalance").text());
                        $("#txtwithdrawCurrentBlc").val($(orderList).find("CurrentBalance").text());
                        $("#lblAvailableBalance").text($(orderList).find("CurrentBalance").text());
                    });
                }
            }
        },
        failure: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        },
        error: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        }
    });
}
function amount() {
    if ($("#txtamount").val() == '') {
        $("#txtamount").val('0.00');
    }
}
function AddBlc() {
    if ($("#txtAddBlc").val() != '') {
        cal = parseFloat($("#txtCurrentBlc").val()) + parseFloat($("#txtAddBlc").val())
        $("#txtNewAvlBlc").val(cal.toFixed(2));
    }
    else if ($("#txtAddBlc").val('')) {
        $("#txtNewAvlBlc").val('0.00');
    }
}


function calTransactionAmt() {
    
    if (parseFloat($("#txtwithdrawCurrentBlc").val()) < parseFloat($("#txtTransactionAmount").val())) {
        $("#addWithdraw").hide();
        toastr.error('Insufficient balance.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
    else if (Number($("#txtTransactionAmount").val()) == 0) {
        $("#txtAvailableBalance").val('0.00');
        $("#addWithdraw").hide();
    }
    else {
        cal = parseFloat($("#txtwithdrawCurrentBlc").val()) - parseFloat($("#txtTransactionAmount").val())
        $("#txtAvailableBalance").val(cal.toFixed(2));
        // $("#addWithdraw").show();
    }
}

//----------------------------------------------For List----------------------------------------------------------------------------
var check = false;
function Search() {
    if (check == false) {
        SecurityPoP();
    }
    else {
        if ($("#ddlPageSize").val() == '0') {
            $("#page").hide();
        }
        else {
            $("#page").show();
        }
        $("#SecurityEnabledVoid").modal('hide');
        bindPattyCashTransactionLog(1);
        $("#btnExport").show();
    }
}

function SecurityPoP() {
    $("#SecurityEnabledVoid").modal('show');
    $("#txtSecurityVoid").val('');
    $("#txtSecurityVoid").focus();
}

function Pagevalue(e) {
    bindPattyCashTransactionLog(parseInt($(e).attr("page")));
};

function bindPattyCashTransactionLog(pageIndex) {
    var data = {
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        Trtype: $("#ddlTtype").val(),
        Amount: $("#txtamount").val(),
        Category: $("#ddlSCategory").val(),
        SearchBy: $("#ddlSearchBy").val(),
        PageSize: $("#ddlPageSize").val(),
        pageIndex: pageIndex
    }
    $.ajax({
        type: "POST",
        url: "WebAPI/PattyCash.asmx/getPattyCashTransactionList",
        data: "{'datavalue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            
            if (response.d == 'SessionExpired') {
                location.href = '/';
            } else {

                var xmldoc = $.parseXML(response.d);
                var PattyCashList = $(xmldoc).find("Table");

                var orderList = $(xmldoc).find("Table2");

                if (orderList.length > 0) {
                    $.each(orderList, function () {
                        $("#lblAvailableBalance").text($(orderList).find("CurrentBalance").text());
                        $("#txtCurrentBlc").val($(orderList).find("CurrentBalance").text());
                        $("#txtwithdrawCurrentBlc").val($(orderList).find("CurrentBalance").text());
                    });
                }


                var gTotal = '0.00'
                if (PattyCashList.length > 0) {
                    $('#EmptyTable').hide();
                    $('#tblPattyCashist tbody tr').remove();
                    var row = $('#tblPattyCashist thead tr:first-child').clone(true);
                    var i = 1;
                    $.each(PattyCashList, function (index) {
                        if (index == 0) {
                            $("#DateOpening").text($(this).find("CreatedDate").text())
                            var Total = parseFloat($(this).find("CurrentBalance").text());
                            $("#Total").text(parseFloat(Total).toFixed(2));
                        }
                        else {
                            $(".Date", row).text($(this).find("TransactionDate").text());
                            $(".CurrentBalance", row).text($(this).find("CurrentBalance").text());
                            $(".TransactionType", row).text($(this).find("TransactionType").text());
                            $(".Category", row).text($(this).find("Category").text());
                            $(".TransactionAmount", row).text($(this).find("TransactionAmount").text());
                            $(".AvailableBalance", row).text($(this).find("AvailableBalance").text());
                            $(".Remark", row).text($(this).find("Remark").text());
                            $(".username", row).text($(this).find("username").text());
                            if ($(this).find("ReferenceId").text() == 0) {
                                $(".action", row).html("<a href='javascript:;'><span class='ft-edit' onclick = 'EditAddWithdrawPopup(\"" + $(this).find("AutoId").text() + "\")' /></a>");
                            }
                            else {
                                $(".action", row).html("");
                            }
                            $("#tblPattyCashist tbody").append(row);
                            row = $("#tblPattyCashist tbody tr:last-child").clone(true);

                            gTotal = parseFloat($(this).find("AvailableBalance").text());
                            $("#DateClosing").text($(this).find("CreatedDate").text())
                        }
                    });
                    $("#gTotal").text(parseFloat(gTotal).toFixed(2));
                }
                else {
                    $("#Total").text('0.00');
                    $("#gTotal").text('0.00');
                    $('#tblPattyCashist tbody tr').remove();
                    $('#EmptyTable').show();
                    $("#btnExport").hide();
                }
                var pager = $(xmldoc).find("Table1");
                $(".Pager").ASPSnippets_Pager({
                    ActiveCssClass: "current",
                    PagerCssClass: "pager",
                    PageIndex: parseInt(pager.find("PageIndex").text()),
                    PageSize: parseInt(pager.find("PageSize").text()),
                    RecordCount: parseInt(pager.find("RecordCount").text())
                });
            }
        },
        failure: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        },
        error: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        }
    });
}

function Export() {
        var data = {
            FromDate: $("#txtSFromDate").val(),
            ToDate: $("#txtSToDate").val(),
            Trtype: $("#ddlTtype").val(),
            Amount: $("#txtamount").val(),
            Category: $("#ddlSCategory").val(),
            SearchBy: $("#ddlSearchBy").val(),
            PageSize: 0,
            pageIndex: 1
        }
        $.ajax({
            type: "POST",
            url: "WebAPI/PattyCash.asmx/getPattyCashTransactionList",
            data: "{'datavalue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                
                if (response.d == 'SessionExpired') {
                    location.href = '/';
                } else {
                    var xmldoc = $.parseXML(response.d);
                    var PattyCashList = $(xmldoc).find("Table");

                    if (PattyCashList.length > 0) {
                        $('#EmptyTable').hide();
                        $('#tblPattyCashistExport tbody tr').remove();
                        var row = $('#tblPattyCashistExport thead tr').clone(true);
                        $.each(PattyCashList, function () {
                            $(".Date", row).text($(this).find("TransactionDate").text());
                            //$(".CurrentBalance", row).text($(this).find("CurrentBalance").text());
                            $(".TransactionType", row).text($(this).find("TransactionType").text());
                            $(".TransactionAmount", row).text($(this).find("TransactionAmount").text());
                            $(".Category", row).text($(this).find("Category").text());
                            $(".AvailableBalance", row).text($(this).find("AvailableBalance").text());
                            $(".Remark", row).text($(this).find("Remark").text());
                            $(".username", row).text($(this).find("username").text());
                            $("#tblPattyCashistExport tbody").append(row);
                            row = $("#tblPattyCashistExport tbody tr:last-child").clone(true);
                        });
                    }
                   
                    $("#LblDate").text(new Date().toLocaleDateString());
                    $("#tblPattyCashistExport").table2excel({
                        exclude: ".noExl",
                        name: "Excel Document Name",
                        filename: "Petty Cash Transaction " + 'Date' + ' ' + new Date().toLocaleDateString(),
                        fileext: ".xls",
                        exclude_img: true,
                        exclude_links: true,
                        exclude_inputs: true
                    });
                }
            },
            failure: function (result) {
                swal("Error!", "Oops! Something went wrong.Please try later.", "error");
            },
            error: function (result) {
                swal("Error!", "Oops! Something went wrong.Please try later.", "error");
            }
        });

    }

    function clickonSecurityVoid() {
        var data = {
            Security: $("#txtSecurityVoid").val()
        }
        $.ajax({
            type: "POST",
            url: "WebAPI/PattyCash.asmx/CheckSecurity",
            data: "{'datavalue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                
                if (response.d == 'SessionExpired') {
                    location.href = '/';
                } else {
                    if (check == false) {
                        var xmldoc = $.parseXML(response.d);
                        var orderList = $(xmldoc).find("Table");
                        if (orderList.length > 0) {
                            $.each(orderList, function () {
                                if ($("#txtSecurityVoid").val() == $(orderList).find("SecurityValue").text()) {
                                    check = true
                                    $("#SecurityEnabledVoid").modal('hide');
                                    bindPattyCashTransactionLog(1);
                                    $("#btnExport").show();
                                    $("#hideAvilableBalance").show();
                                    $("#hdnAvailable").show();
                                }
                            });
                        }
                        else {
                            swal("Warning!", "Access denied.", "warning", {
                                button: "OK",
                            }).then(function () {
                                $("#txtSecurityVoid").val('');
                                $("#txtSecurityVoid").focus();
                            })
                        }
                    }
                    else {
                        $("#SecurityEnabledVoid").modal('hide');
                        bindPattyCashTransactionLog(1);
                        $("#btnExport").show();
                    }
                }
            },
            failure: function (result) {
                swal("Error!", "Oops! Something went wrong.Please try later.", "error");
            },
            error: function (result) {
                swal("Error!", "Oops! Something went wrong.Please try later.", "error");
            }
        });
    }

    function EditAddWithdrawPopup(TransactionAutoId1) {
        TransactionAutoId = TransactionAutoId1;
        $.ajax({
            type: "POST",
            url: "WebAPI/PattyCash.asmx/EditPattyBalance",
            data: "{'AutoId':'" + TransactionAutoId1 + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {

                var xmldoc = $.parseXML(response.d);
                var PattyCashDetails = $(xmldoc).find("Table");
                var orderList = $(xmldoc).find("Table1");


                if ($(PattyCashDetails).find("TransactionType").text() == "CR") {
                    $("#txtAddTime").val($(PattyCashDetails).find("TransactionDate").text());
                    $("#txtAddTime1").val($(PattyCashDetails).find("TransactionTime").text());
                    $("#txtAddBlc").val($(PattyCashDetails).find("TransactionAmount").text());
                    $("#ddlAddCategory").val($(PattyCashDetails).find("Category").text()).change();
                    $("#txtNewAvlBlc").val(parseFloat($(orderList).find("CurrentBalance").text()).toFixed(2));
                    $("#txtCurrentBlc").val(parseFloat(parseFloat($(orderList).find("CurrentBalance").text()) - parseFloat($(PattyCashDetails).find("TransactionAmount").text())).toFixed(2));
                    $("#txtAddRemark").val($(PattyCashDetails).find("Remark").text());
                    $("#btnAddUpdate").show();
                    $("#btnAdd").hide();
                    $("#txtAddTime").attr('disabled', true);
                    $("#txtAddTime1").attr('disabled', true);
                    $("#alertDanger").hide();
                    $("#modalAddBlc").modal('show');
                }
                else {
                    $("#txtWithdrawTime").val($(PattyCashDetails).find("TransactionDate").text());
                    $("#txtWithdrawTime1").val($(PattyCashDetails).find("TransactionTime").text());
                    $("#txtTransactionAmount").val($(PattyCashDetails).find("TransactionAmount").text());
                    $("#ddlWCategory").val($(PattyCashDetails).find("Category").text()).change();
                    $("#txtAvailableBalance").val(parseFloat($(orderList).find("CurrentBalance").text()).toFixed(2));
                    var CurrentBalance = parseFloat($(orderList).find("CurrentBalance").text()) + parseFloat($(PattyCashDetails).find("TransactionAmount").text());
                    $("#txtwithdrawCurrentBlc").val(parseFloat(CurrentBalance).toFixed(2));
                    $("#txtRemark").val($(PattyCashDetails).find("Remark").text());
                    $("#UpdateWithdraw").show();
                    $("#addWithdraw").hide();
                    $("#txtWithdrawTime").attr('disabled', true);
                    $("#txtWithdrawTime1").attr('disabled', true);
                    $("#WalertDanger").hide();
                    $("#modalWithdrawBlc").modal('show');
                }

            },
            failure: function (result) {
                swal("Error!", "Oops! Something went wrong.Please try later.", "error");
            },
            error: function (result) {
                swal("Error!", "Oops! Something went wrong.Please try later.", "error");
            }
        });

    }

    function AddUpdate() {
        if (Number($("#txtTransactionAmount").val()) == 0) {
            toastr.error('Withdraw Amount should be greater than 0.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            $("#txtTransactionAmount").addClass('border-warning');
        }
        else {
            $("#txtTransactionAmount").removeClass('border-warning');
            if (checkRequiredField()) {
                var TransactionDate = $("#txtAddTime").val() + ' ' + $("#txtAddTime1").val()
                Update($("#txtAddBlc").val(), 'CR', $("#ddlAddCategory").val(), $("#txtAddRemark").val(), TransactionDate, TransactionAutoId)

                getTransactionBalance();
                $("#txtNewAvlBlc").val("0.00");
                $("#txtAddBlc").val("0.00");
                $("#txtAddRemark").val('');
                $("#txtAddTime").val('');
                $("#ddlAddCategory").val(0).change();
            }
            else {
                toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            }
        }
    }


    function UpdateWithdrawBalance() {
        
        if (checkRequiredFieldWithdraw()) {
            if (Number($("#txtTransactionAmount").val()) <= 0) {
                toastr.error('Withdraw Amount should be greater than 0.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                $("#txtTransactionAmount").addClass('border-warning');
            }
            else {
                $("#txtTransactionAmount").removeClass('border-warning');
                var data = {
                    NewAmount: $("#txtNewAvlBlc").val()
                }
                var TransactionDate = $("#txtWithdrawTime").val() + ' ' + $("#txtWithdrawTime1").val()
                Update($("#txtTransactionAmount").val(), 'DR', $("#ddlWCategory").val(), $("#txtRemark").val(), TransactionDate, TransactionAutoId);
                getTransactionBalance();
                $("#txtTransactionAmount").val('0.00');
                $("#txtAvailableBalance").val('0.00');
                $("#txtRemark").val('');
                $("#txtWithdrawTime").val('');
                $("#ddlWCategory").val(0).change();
            }
        }
        else {
            toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        }
    }

    function Update(TransactionAmount, TransactionType, Category, Remark, TransactionDate, AutoId) {
        var data = {
            transactionAmount: TransactionAmount,
            transactionType: TransactionType,
            Category: Category,
            remark: Remark,
            TransactionDate: TransactionDate,
            AutoId: AutoId
        }
        $.ajax({
            type: "POST",
            url: "WebAPI/PattyCash.asmx/Update",
            //data: "{'datavalue':'" + JSON.stringify(data) + "'}",
            data: JSON.stringify({ datavalue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == 'SessionExpired') {
                    location.href = '/'
                }
                else if (result.d == 'true') {
                    
                    if (TransactionType == 'CR') {
                        swal("", "Balance updated successfully.", "success");
                        $("#modalAddBlc").modal('hide');
                        getTransactionBalance();
                        bindPattyCashTransactionLog(1);
                    } else if (TransactionType == 'DR') {
                        swal("", "Balance updated successfully.", "success");
                        $("#modalWithdrawBlc").modal('hide');
                        getTransactionBalance();
                        bindPattyCashTransactionLog(1);
                    }
                    else {
                        swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                    }

                } else {
                    swal("Error!", resul.d, "error");
                }
            },
            error: function (result) {

                swal("Error!", "Oops! Something went wrong.Please try later.", "error");
            },
            failure: function (result) {
                swal("Error!", "Oops! Something went wrong.Please try later.", "error");
            }
        });
    }
    function checkRequiredField() {
        var boolcheck = true;
        $('.req').each(function () {
            if ($(this).val() == '' || parseInt($(this).val()) == 0) {
                boolcheck = false;
                $(this).addClass('border-warning');
            } else {
                $(this).removeClass('border-warning');
            }
        });

        $('.ddlreq').each(function () {
            if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == 'undefined' || $(this).val() == null || $(this).val() == 'Select') {
                boolcheck = false;
                $(this).closest('div').find('.select2-selection--single').attr('style', 'border:1px solid #FF9149  !important');
            } else {
                $(this).removeClass('border-warning');
                $(this).closest('div').find('.select2-selection--single').removeAttr('style');
            }
        });


        return boolcheck;
    }