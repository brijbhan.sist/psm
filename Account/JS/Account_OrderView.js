﻿var rowupdate = "";
var CustomerId; var CustType; var MLQtyRate = 0.00;
$(document).ready(function () {
    var getid = getQueryString('OrderAutoId');
    if (getid != null) {
        if ($("#hEmpTypeNo").val() == 1) {
            $("#btnVoidOrder").show();
        }
        else {
            $("#btnVoidOrder").hide();
        }
        getOrderData(getid);
        $("#txtHOrderAutoId").val(getid);
    }
});
var getQueryString = function (field, url) {
    var href = url ? url : window.location.href;
    var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
    var string = reg.exec(href);
    return string ? string[1] : null;
};
function getOrderData(OrderAutoId) {
    $.ajax({
        type: "POST",
        url: "/account/WebAPI/WAccount_Order.asmx/getOrderData",
        data: "{'OrderAutoId':'" + OrderAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            debugger;
            var xmldoc = $.parseXML(response.d);
            var order = $(xmldoc).find("Table");
            var product = $(xmldoc).find("Table1");
            var duePayment = $(xmldoc).find("Table2");
            var CreditMemoDetails = $(xmldoc).find("Table3");
            var RemarksDetails = $(xmldoc).find("Table4");
            //var DeliveredMlTax = $(xmldoc).find("Table5");
            var DriverDocumentDetails = $(xmldoc).find("Table5");
            $("#Table2 tbody tr").remove();
            if ($(RemarksDetails).length > 0) {

                var rowtest = $("#Table2 thead tr").clone();
                $.each(RemarksDetails, function (index) {
                    $(".SRNO", rowtest).text((Number(index) + 1));
                    $(".EmployeeName", rowtest).text($(this).find("EmpName").text());
                    $(".EmployeeType", rowtest).text($(this).find("EmpType").text());
                    $(".Remarks", rowtest).text($(this).find("Remarks").text());
                    $("#Table2 tbody").append(rowtest);
                    rowtest = $("#Table2 tbody tr:last").clone(true);
                });
                $('#OrderRemarks').show();
            } else {
                $('#OrderRemarks').hide();
            }
            var totalCredit = 0.00;
            if (Number(CreditMemoDetails.length) > 0) {
                $("#CusCreditMemo").show();
                $("#tblCreditMemoList tbody tr").remove();
                var rowc = $("#tblCreditMemoList thead tr:last").clone(true);
                $.each(CreditMemoDetails, function (index) {

                    $(".SRNO", rowc).text(Number(index) + 1);
                    $(".Action", rowc).html("<input type='radio' name='credit' value=" + $(this).find("CreditAutoId").text() + "> ");
                    $(".CreditNo", rowc).text($(this).find("CreditNo").text());
                    $(".CreditDate", rowc).text($(this).find("CreditDate").text());
                    $(".ReturnValue", rowc).text($(this).find("ReturnValue").text());
                    totalCredit += parseFloat($(this).find("ReturnValue").text())
                    $("#tblCreditMemoList tbody").append(rowc)
                    rowc = $("#tblCreditMemoList tbody tr:last").clone(true);
                });
            } else {
                $("#CusCreditMemo").hide();
            }
            $("#TotalDue").text(totalCredit.toFixed(2));

            CustomerId = $(order).find("CustomerId").text();

            if (Number($(order).find("StatusCode").text()) == 11) {
                $("#btnSave").val("Go For Edit");
                $("#btnSave").attr('onclick', 'PopSecurity()');
                $("#btnMigrate").show();
            } else {
                $("#btnSave").val("Go For Save");
                $("#btnMigrate").hide();
            }

            if (Number($(order).find("StatusCode").text()) == 6 || Number($(order).find("StatusCode").text()) == 11) {
                $("#txtDelivered").val("Yes");
            } else {
                $("#txtDelivered").val('No');
            }
            $("#txtRemarks").val($(order).find("DrvRemarks").text());
            if (Number($(order).find("StatusCode").text()) != 8) {
                if ($(order).find("AmtDue").text() <= 0) {
                    $("#btnPayNow").hide();
                }
                else {
                    $("#btnPayNow").show();
                }

            }
            else {
                $("#btnPayNow").hide();
            }

            if (Number($(order).find("StatusCode").text()) == 8) {
                $("#btnVoidOrder").hide();
                $("#btnSave").hide();
            }
            if ($(order).find("PaymentRecev").text() == null || $(order).find("PaymentRecev").text() == "") {
                $("#AccountDeliveryInfo").hide();
            } else {
                $("#AccountDeliveryInfo").show();
            }
            $('#txtRecievedPayment').val($(order).find("PaymentRecev").text());
            $("#txtAmtPaid").val($(order).find("AmtPaid").text());
            $("#txtAmtDue").val($(order).find("AmtDue").text());
            if ($("#txtAmtDue").val() > 0) {
                $("#btnMigrate").hide();

            } else {
                $("#btnMigrate").show();
            }
            CustType = $(order).find("CustomerType").text();
            $("#txtAcctRemarks").val($(order).find("AcctRemarks").text());
            $("#txtHOrderAutoId").val($(order).find("AutoId").text());
            $("#txtOrderId").val($(order).find("OrderNo").text());
            $("#lblOrderno").text($(order).find("OrderNo").text());
            $("#txtOrderType").val($(order).find("OrderType").text());
            if ($(order).find("OrderType").text() == 'POS ORDER') {
                $('#DrvDeliveryInfo').closest('.panel').hide();
            }
            $("#txtOrderDate").val($(order).find("OrderDate").text());
            if ($(order).find("CommentType").text() != '')
                $("#ddlCommentType").val($(order).find("CommentType").text());
            $("#txtCustomerType").val($(order).find("CustomerType").text());
            $("#txtSalesPerson").val($(order).find("SalesPerson").text());
            $("#txtComment").val($(order).find("Comment").text());
            $("#txtDeliveryDate").val($(order).find("DeliveryDate").text());
            $("#txtOrderStatus").val($(order).find("Status").text());
            $("#txtCustomer").val($(order).find("CustomerName").text());
            $("#hiddenCustAutoId").val($(order).find("CustAutoId").text());
            $("#txtTerms").val($(order).find("Terms").text());
            $("#txtTerms").val($(order).find("TermsDesc").text());
            $("#OrderRemarks").val($(order).find("OrderRemarks").text());
            $("#txtAdjustment").val($(order).find("AdjustmentAmt").text());
            $("#txtMLQty").val($(order).find("MLQty").text());
            $("#txtMLTax").val($(order).find("MLTax").text());
            $("#txtShippingType").val($(order).find("ShippingType").text());
            $("#hfMLTaxStatus").val($(order).find('isMLManualyApply').text());
            var Delivered_CheckMLTax = $(order).find("Delivered_CheckMLTax").text();
            if (Number(Delivered_CheckMLTax) == 1 && Number($(order).find("StatusCode").text()) != 8 && $(order).find("EnabledTax").text() == 1) {
                $('#btnRemoveMlTax').show();
            } else {
                $('#btnRemoveMlTax').hide();
            }
            if ($(order).find('isMLManualyApply').text() == '1') {
                $('#btnRemoveMlTax').text('Remove ML Tax');
            } else {
                $('#btnRemoveMlTax').text('Add ML Tax')
            }
            if (parseFloat($(order).find("WeightOzTotalTax").text()) > 0) {
                $("#txtWeightQty").val($(order).find("WeightOzQty").text());
                $("#txtWeightTax").val($(order).find("WeightOzTotalTax").text());
            }
            else {
                $("#txtWeightQty").val('0.00');
                $("#txtWeightTax").val('0.00');
            }

            if ($(order).find("CreditAmount").text() != "") {
                $("#txtStoreCreditAmount").val($(order).find("CreditAmount").text());
            }
            if ($(order).find('PayableAmount').text() != '') {
                $("#txtPaybleAmount").val($(order).find("PayableAmount").text());
            } else {
                $("#txtPaybleAmount").val($(order).find("GrandTotal").text());
            }
            if ($(order).find("DeductionAmount").text() != "")
                $("#txtDeductionAmount").val($(order).find("DeductionAmount").text());
            $("#CreditMemoAmount").text($(order).find("CustomerCredit").text());
            $("#txtBillAddress").val($(order).find("BillAddr").text());
            //$("#txtBillState").val($(order).find("State1").text());
            //$("#txtBillCity").val($(order).find("City1").text());
            //$("#txtBillZip").val($(order).find("Zipcode1").text());

            $("#txtShipAddress").val($(order).find("ShipAddr").text());
            //$("#txtShipState").val($(order).find("State2").text());
            //$("#txtShipCity").val($(order).find("City2").text());
            //$("#txtShipZip").val($(order).find("Zipcode2").text());

            $("#txtTotalAmount").val($(order).find("TotalAmount").text());
            $("#txtOverallDisc").val($(order).find("OverallDisc").text());
            $("#txtDiscAmt").val($(order).find("OverallDiscAmt").text());
            $("#txtShipping").val($(order).find("ShippingCharges").text());
            $("#txtTotalTax").val($(order).find("TotalTax").text());
            $("#txtGrandTotal").val($(order).find("GrandTotal").text());
            $("#hiddenGrandTotal").val($(order).find("GrandTotal").text());
            $("#txtPackedBoxes").val($(order).find("PackedBoxes").text());
            $("#lblNoOfbox").text($(order).find("PackedBoxes").text());

            showDuePayments(duePayment);
            $("#tblProductDetail tbody tr").remove();
            var row = $("#tblProductDetail thead tr").clone(true);
            $.each(product, function () {
                $(".ProId", row).html($(this).find("ProductId").text());
                if ($(this).find("isFreeItem").text() == '1') {
                    $(".ProName", row).html($(this).find("ProductName").text() + " <span class='badge badge-pill badge-success'>Free</span>");
                }
                else if ($(this).find("Tax").text() == '1') {
                    $(".ProName", row).html($(this).find("ProductName").text() + " <span class='badge badge-pill badge-danger'>Taxable</span>");
                }
                else if ($(this).find("IsExchange").text() == '1') {
                    $(".ProName", row).html($(this).find("ProductName").text() + " <span class='badge badge-pill badge-primary'>Exchange</span>");
                }
                else {
                    $(".ProName", row).html($(this).find("ProductName").text());
                }
                $(".UnitType", row).html($(this).find("UnitType").text() + " (" + $(this).find("QtyPerUnit").text() + " pcs" + ")");
                $(".TtlPcs", row).text($(this).find("TotalPieces").text());
                $(".Discount", row).text($(this).find("Discount").text());
                $(".NetPrice", row).text($(this).find("NetPrice").text());
                $(".ItemTotal", row).text($(this).find("ItemTotal").text());
                $(".UnitPrice", row).html($(this).find("UnitPrice").text());
                $(".PerPrice", row).text($(this).find("PerpiecePrice").text());
                $(".SRP", row).text($(this).find("SRP").text());
                $(".GP", row).text($(this).find("GP").text());
                if (Number($(this).find("Tax").text()) == 1) {
                    $(".TaxRate", row).html('<span class="la la-check-circle success"></span>');
                } else {
                    $(".TaxRate", row).html('');
                }
                if (Number($(this).find("IsExchange").text()) == 1) {
                    $(".IsExchange", row).html('<span class="la la-check-circle success"></span>');
                } else {
                    $(".IsExchange", row).html('');
                }
                $(".Barcode", row).text($(this).find("Barcode").text());

                $(".QtyShip", row).text($(this).find("QtyShip").text());
                if ($(this).find("QtyDel").text() != "") {
                    $(".QtyDel", row).html($(this).find("QtyDel").text());
                } else {
                    $(".QtyDel", row).html($(this).find("TotalPieces").text());
                }
                $(".FreshReturn", row).html($(this).find("FreshReturnQty").text());
                $(".DemageReturn", row).html($(this).find("DamageReturnQty").text());
                $(".MissingItem", row).html($(this).find("MissingItemQty").text());
                if (Number($(this).find("FreshReturnQty").text()) > 0 || Number($(this).find("DamageReturnQty").text()) > 0) {
                    $(row).css('background-color', '#efcccc');
                } else if (Number($(this).find("MissingItemQty").text()) > 0) {
                    $(row).css('background-color', '#edb3b3');
                } else {
                    $(row).css('background-color', '#fff');
                }
                if ($(this).find("QtyShip").text() == "0") {
                    $(row).hide();
                } else {
                    $(row).show();
                }
                if (Number($(order).find("StatusCode").text()) == 8) {
                    $("#btnGenOrderCC").hide();
                } else {
                    $("#btnGenOrderCC").show();
                }
                $('#tblProductDetail tbody').append(row);
                row = $("#tblProductDetail tbody tr:last").clone(true);
            });

            debugger
            $.each(DriverDocumentDetails, function (index) {
                if ($(this).find("PhysicalPath").text() != '') {
                    if ($(this).find("fileType").text() == 'Signed Invoice') {
                        $("#imgSignInvoice").attr('src', $(this).find("PhysicalPath").text());
                        $("#imgSignInvoice").show();
                        $("#spnSignedInvoice").html('Signed Invoice');
                    } else if ($(this).find("fileType").text() == 'Sign Invoice') {
                        $("#imgSignInvoice").attr('src', $(this).find("PhysicalPath").text());
                        $("#imgSignInvoice").show();
                        $("#spnSignedInvoice").html('Signed Invoice');
                    }
                    else if ($(this).find("fileType").text() == 'Delivery Proof') {
                        $("#imgDeliveryProof").attr('src', $(this).find("PhysicalPath").text());
                        $("#imgDeliveryProof").show();
                        $("#spnDeliveryProof").html('Delivery Proof');
                    }
                    else if ($(this).find("fileType").text() == 'Other') {
                        $("#imgOther").attr('src', $(this).find("PhysicalPath").text());
                        $("#imgOther").show();
                        $("#spnOther").html('Other');
                    }
                }
            });
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function showDuePayments(duePayment) {
    $("#tblCustDues tbody tr").remove();
    var row = $("#tblCustDues thead tr").clone(true);

    var sumOrderValue = 0,
        sumPaid = 0,
        sumDue = 0;

    if (duePayment.length > 0) {
        $("#noDues").hide();
        $.each(duePayment, function () {
            $(".orderNo", row).html("<span orderautoid='" + $(this).find("AutoId").text() + "'><a href='#'>" + $(this).find("OrderNo").text() + "</a></span>");
            $(".orderDate", row).text($(this).find("OrderDate").text());
            $(".value", row).text($(this).find("GrandTotal").text());
            $(".OrderType", row).text($(this).find("OrderType").text());
            $(".amtPaid", row).text($(this).find("AmtPaid").text()).css("color", "#8bc548");
            $(".amtDue", row).text($(this).find("AmtDue").text()).css("color", "red");
            $(".pay", row).html("<input type='text' class='form-control input-sm' style='width:100px;float:right;text-align:right;' value='0.00' placeholder='0.00' onkeyup='sumPay()' />");
            $(".remarks", row).html("<input type='text' class='form-control input-sm' placeholder='Enter payment details here' />");

            sumOrderValue += Number($(this).find("GrandTotal").text());
            sumPaid += Number($(this).find("AmtPaid").text());
            sumDue += Number($(this).find("AmtDue").text());

            $('#tblCustDues').find("tbody").append(row).end()
                .find("tfoot").show().find("#sumOrderValue").text(sumOrderValue.toFixed(2)).end()
                .find("#sumPaid").text(sumPaid.toFixed(2)).css("color", "#8bc548").end()
                .find("#sumDue").text(sumDue.toFixed(2)).css("color", "red").end()
                .find("#sumPay").text("0.00").end();

            row = $("#tblCustDues tbody tr:last").clone(true);
        });
        $("#CustDues").show();
        if ($("#hiddenEmpTypeVal").val() != "5") {
            $("#CustDues").find("#btnPay_Dues").show().end()
                .find("#tblCustDues").find(".pay").show().end()
                .find(".remarks").show().end()
                .find("tfoot > tr > td").show();
        }
    } else {
        $("#tblCustDues").find("tbody tr").remove().end().find("tfoot").hide();
        $("#noDues").show();
        $("#CustDues").show();
        $("#btnPay_Dues, #btnClose_Dues").hide();
    }
}
/*-------------------------------------------------------------------------------------------------------------------------------*/
//                                            Summation of Pay Amount against Order Dues

function PayNowDelivery() {
    location.href = '/Sales/ViewCustomerDetails.aspx?PageId=' + $("#hiddenCustAutoId").val() + '&Type=1';
}
function
    printOrder_CustCopy() {
    if ($('#HDDomain').val() == 'psmpa') {
        $("#divPSMPADefault").show();
        $("#PSMPADefault").prop('checked', true);
    }
    else if ($('#HDDomain').val() == 'psmwpa') {
        $("#divPSMWPADefault").show();
        $("#chkdueamount").prop('checked', true);
    }
    else if ($('#HDDomain').val() == 'psmnpa') {
        $("#PSMNPADefault").show();
        $("#divPSMPADefault").hide();
        $("#rPSMNPADefault").prop('checked', true);
    }
    else {
        $("#chkdueamount").prop('checked', true);
        $("#PSMPADefault").closest('.row').hide();
    }
    $('#PopPrintTemplate').modal('show');
}
function PrintOrder() {
    $('#PopPrintTemplate').modal('hide');
    if ($("#chkdueamount").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF1.html?OrderAutoId=" + $("#txtHOrderAutoId").val() + "", "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    } else if ($("#chkdefault").prop('checked') == true) {
        window.open("/Manager/OrderPrintCC.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#Template1").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF2.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#Template2").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF3.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#PSMPADefault").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF4.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#PSMWPADefault").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF6.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#PackingSlip").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF5.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#WithoutCategorybreakdown").prop('checked') == true) {
        window.open("/Manager/WithoutCategorybreakdown.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    } else if ($("#rPSMNPADefault").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF4.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#Templ1").prop('checked') == true) {
        window.open("/Manager/PrintOrderItem.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#Templ2").prop('checked') == true) {
        window.open("/Manager/PrintOrderItemTemplate2.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#PSMWPANDefault").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCFPN.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
}

function saveDelUpdates() {
    location.href = '/Driver/updateDelivery.aspx?OrderAutoId=' + $("#txtHOrderAutoId").val();
}
function PopSecurity() {
    $('#SecurityEnabled').modal('show');
    $("#txtSecurity").val('');
}

function PopSecurityVoid() {
    $('#SecurityEnabledVoid').modal('show');
    $("#txtSecurityVoid").val('');
    $('#errormsgVoid').html('');
}
function clickonSecurityVoid() {
    if ($("#txtSecurityVoid").val() == "") {
        $("#txtSecurityVoid").addClass('border-warning');
        toastr.error('Security Key Required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        return;
    }

    $.ajax({
        type: "Post",
        url: "/account/WebAPI/WAccount_Order.asmx/clickonSecurityVoid",
        data: "{'CheckSecurity':'" + $("#txtSecurityVoid").val() + "','OrderAutoId':'" + $("#txtHOrderAutoId").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                getOrderData($("#txtHOrderAutoId").val());
                if (response.d == 'true') {
                    $('#SecurityEnabledVoid').modal('hide');
                    $("#CancelOrderPopup").modal('show');
                    $('#errormsgCancel').html('');
                    $("#CancelRemark").val('');
                    $("#txtSecurityVoid").removeClass('border-warning');
                } else {
                    toastr.error('Access Denied.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
                    $("#txtSecurityVoid").addClass('border-warning');
                    $("#txtSecurityVoid").val('').focus();;
                }
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function clickonSecurityvoidclose() {
    $("#txtSecurityVoid").removeClass('border-warning');
}
function clickonSecurityclose() {
    $("#txtSecurity").removeClass('border-warning');
}

function cancel() {
    swal({
        title: "Are you sure?",
        text: "You want to cancel this order.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: false,
            },
            confirm: {
                text: "Yes, void it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            CancelOrder();

        } else {
            swal("", "Your order is safe.", "error");
        }
    })
}

function CancelOrder() {
    if ($("#CancelRemark").val() == "") {
        swal("Error", "Remark required.", "error");
        return;
    }
    $.ajax({
        type: "Post",
        url: "/account/WebAPI/WAccount_Order.asmx/CancelOrder",
        data: "{'CancelRemark':'" + $("#CancelRemark").val() + "','OrderAutoId':'" + $("#txtHOrderAutoId").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                getOrderData($("#txtHOrderAutoId").val());
                if (response.d == 'true') {

                    $('#SecurityEnvalid').modal('show');
                    $('#CancelOrderPopup').modal('hide');
                    swal("", "Your order cancelled successfully.", "success");
                } else {
                    swal("Error", "Order can't cancel.", "error");
                }
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function clickonSecurity() {
    if ($("#txtSecurity").val() == "") {
        $('#SecurityEnvalid').modal('show');
        $("#txtSecurity").addClass('border-warning');
        toastr.error('Security Key Required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        return;
    }
    $.ajax({
        type: "Post",
        url: "/account/WebAPI/WAccount_Order.asmx/clickonSecurity",
        data: "{'CheckSecurity':'" + $("#txtSecurity").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d == 'true') {
                    if (CustType == "Internal") {
                        location.href = '/Account/InternalUpdateDelivery.aspx?OrderAutoId=' + $("#txtHOrderAutoId").val();
                    }
                    else {
                        location.href = '/Driver/updateDelivery.aspx?OrderAutoId=' + $("#txtHOrderAutoId").val();
                    }

                    $("#txtSecurity").removeClass('border-warning');
                } else {
                    $('#SecurityEnvalid').modal('show');
                    $("#txtSecurity").addClass('border-warning');
                    $("#txtSecurity").val('').focus();
                    toastr.error('Invalid Security Key.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
                }
            } else {
                location.href = '/';
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });


}




function RemoveMlTax() {
    $('#SecurityEnabledMLTax').modal('show');
}
var i = 0;
function clickonSecurityMLTax() {
    if (checksecRequiredField()) {
        i = 0;
        $.ajax({
            type: "Post",
            url: "/account/WebAPI/WAccount_Order.asmx/clickonSecurityMLTax",
            data: "{'CheckSecurity':'" + $("#txtSecurityMLTax").val() + "','OrderAutoId':'" + $("#txtHOrderAutoId").val() + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            async: false,
            success: function (response) {
                if (response.d != "Session Expired") {
                    if (response.d == 'true') {
                        $('#SecurityEnabledMLTax').modal('hide');
                        $('#modalMLTaxConfirmation').modal('show');
                    } else {
                        toastr.error('Access Denied.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
                    }
                } else {
                    location.href = '/';
                }
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
    else {
        toastr.error('Security Required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
}
function ConfirmMLTax() {
    var Status = Number($("#hfMLTaxStatus").val());
    var text = "";
    if (Status == 1) {
        text = "You want to remove ML Tax.";
    }
    else {
        text = "You want to add ML Tax.";
    }
    if (checksecRequiredFieldML()) {
        swal({
            title: "Are you sure?",
            text: text,
            icon: "warning",
            showCancelButton: true,
            buttons: {
                cancel: {
                    text: "No, Cancel.",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: true,
                },
                confirm: {
                    text: "Yes",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
                }
            }
        }).then(function (isConfirm) {
            if (isConfirm) {
                $("#modalMLTaxConfirmation").modal('hide');
                ConfirmSecurityMLTax(Status);
            } else {
                closeModal: true
            }
        })
    }
    else {
        toastr.error('Security Required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
}
function ConfirmSecurityMLTax(Status) {

    i = 0;
    $.ajax({
        type: "Post",
        url: "/account/WebAPI/WAccount_Order.asmx/ConfirmSecurity",
        data: "{'CheckSecurity':'" + $("#txtSecurityMLTax").val() + "','OrderAutoId':'" + $("#txtHOrderAutoId").val() + "','MLTaxRemark':'" + $("#txtMLRemark").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,

        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d == 'true') {
                    $('#btnpkd').removeAttr('disabled');
                    if (Status == 1) {
                        swal("", "ML Tax removed Successfully", "success"
                        ).then(function () {
                            location.href = '/Account/Acccount_viewOrder.aspx?OrderAutoId=' + $("#txtHOrderAutoId").val();
                        });
                    } else {
                        swal("", "ML Tax added Successfully", "success"
                        ).then(function () {
                            location.href = '/Account/Acccount_viewOrder.aspx?OrderAutoId=' + $("#txtHOrderAutoId").val();
                        });
                    }


                    i = 1;
                } else {
                    toastr.error('Access Denied.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
                }
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function ClosePop() {
    if (i == 1) {
        getOrderData($("#txtHOrderAutoId").val());
    }
    $('#SecurityEnvalidMLTax').modal('hide');
}
function checksecRequiredField() {
    var boolcheck = true;
    $('.reqseq').each(function () {
        if ($(this).val() == '') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });

    return boolcheck;
}
function checksecRequiredFieldML() {
    var boolcheck = true;
    $('.reqML').each(function () {
        if ($(this).val() == '') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });

    return boolcheck;
}

function CheckSecurityMigrate() {
    $('#SecurityEnabledMigrate').modal('show');
    $("#txtSecurityMigrate").val('');

}

function clickonSecurityMigrate() {
    if ($("#txtSecurityMigrate").val() == "") {
        $("#txtSecurityMigrate").addClass('border-warning');
        toastr.error('Security Key Required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        return;
    }

    $.ajax({
        type: "Post",
        url: "/account/WebAPI/WAccount_Order.asmx/clickonSecurityMigrate",
        data: "{'CheckSecurity':'" + $("#txtSecurityMigrate").val() + "','OrderAutoId':'" + $("#txtHOrderAutoId").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                getOrderData($("#txtHOrderAutoId").val());
                if (response.d == 'true') {
                    $('#SecurityEnabledMigrate').modal('hide');
                    $("#MigrateOrderPopup").modal('show');
                    $("#txtMigrateRemark").val('');
                    $("#txtSecurityMigrate").removeClass('border-warning');
                } else {
                    toastr.error('Access Denied.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
                    $("#txtSecurityMigrate").addClass('border-warning');
                    $("#txtSecurityMigrate").val('').focus();
                }
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function clickonSecurityMigrateclose() {
    $("#txtSecurityMigrate").removeClass('border-warning');
}
function MigrateOrder() {
    if ($("#txtMigrateRemark").val() == "") {
        $("#txtMigrateRemark").addClass('border-warning');
        toastr.error('Remarks Required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        return;
    }

    $.ajax({
        type: "Post",
        url: "/account/WebAPI/WAccount_Order.asmx/MigrateOrder",
        data: "{'Remarks':'" + $("#txtMigrateRemark").val() + "','OrderAutoId':'" + $("#txtHOrderAutoId").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "Session Expired") {
                getOrderData($("#txtHOrderAutoId").val());
                if (response.d == 'true') {
                    $("#MigrateOrderPopup").modal('hide');
                    $("#txtMigrateRemark").val('');
                    $("#txtSecurityMigrate").removeClass('border-warning');
                    swal("", "Order Migrate successfully.", "success"
                    ).then(function () {
                        location.href = '/Account/deliveredOrdersList.aspx';
                    });
                } else {
                    swal("Error", response.d, "error");
                    $("#txtSecurityMigrate").addClass('border-warning');
                    $("#txtSecurityMigrate").val('').focus();
                }
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}