﻿var WeightOz = 0.00; // Global Variable
$(document).ready(function () {
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    var CreditAutoId = getQueryString('PageId');

    Orderrmarkdeatil(CreditAutoId);
    if (CreditAutoId != null) {
        $("#btnUpdate").show();
        $("#btnPrintOrder").show();
        $("#CreditAutoId").val(CreditAutoId);
        editCredit(CreditAutoId);


    } else {
        $("#btnUpdate").hide();
        $("#btnPrintOrder").hide();
    }
});

var MLTaxRate = 0.00;

$('.close').click(function () {
    $(this).closest('div').hide();

});

function Orderrmarkdeatil(CreditAutoId) {

    $.ajax({
        type: "POST",
        url: "AccountCreditMemoView.aspx/OrderRemarkDeatil",
        data: "{'CreditAutoId':" + CreditAutoId + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var index = 0;
            var ManagerRemark = $(xmldoc).find("Table1");
            $("#tblOrderrmarkdeatil tbody tr").remove();
            var row = $("#tblOrderrmarkdeatil thead tr").clone(true);
            if (ManagerRemark.length > 0) {
                $("#EmptyTable").hide();
                $.each(ManagerRemark, function (index) {

                    $(".SRNO", row).html((Number(index) + 1));
                    $(".EmployeeName", row).text($(this).find("EmpName").text());
                    $(".EmployeeType", row).text($(this).find("EmpType").text());
                    $(".Remarks", row).html($(this).find("Remarks").text());

                    $("#tblOrderrmarkdeatil tbody").append(row);
                    row = $("#tblOrderrmarkdeatil tbody tr:last").clone(true);
                });
            }
            else {
                $("#OrderRemrak").hide();
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function editCredit(CreditAutoId) {
    $.ajax({
        type: "POST",
        url: "AccountCreditMemoView.aspx/editCredit",
        data: "{'CreditAutoId':'" + CreditAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {

            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },

        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var CreditOrder = $(xmldoc).find("Table");
                var items = $(xmldoc).find("Table1");
                var EmpType = $(xmldoc).find("Table2");
                var RemarksDetails = $(xmldoc).find("Table3");
                var BindTaxType = $(xmldoc).find("Table4");
                var CStatus = "";


                if ($(CreditOrder).find("WeightTax").text() != '') {
                    WeightOz = $(CreditOrder).find("WeightTax").text()
                }

                $("#txtOrderId").val($(CreditOrder).find("CreditNo").text());
                $("#txtOrderDate").val($(CreditOrder).find("CreditDate").text());
                $("#txtOrderStatus").val($(CreditOrder).find("STATUS").text());
                CStatus = $(CreditOrder).find("STATUS").text();
                $("#CreditType").val($(CreditOrder).find("CreditType").text());
                $("#txtRemarks").val($(CreditOrder).find("Remarks").text()).attr('disabled', true)
                $("#txtCustomer").val($(CreditOrder).find("CustomerName").text()).change();
                $("#txtManagerRemarks").val($(CreditOrder).find("ManagerRemark").text());
                $("#txtTaxType").val($(CreditOrder).find("TaxaType").text());

                $("#ddlCustomer").attr('disabled', true);
                $("#hiddenOrderStatus").val($(CreditOrder).find("StatusCode").text());
                //  updated by satish 11/05/2019
                //  end Update
                var row = $("#tblProductDetail thead tr").clone(true);
                $("#tblProductDetail tbody tr").remove();
                $.each(items, function () {

                    $(".ItemAutoId", row).text($(this).find("ItemAutoId").text());
                    $(".ProId", row).text($(this).find("ProductId").text());
                    $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span>");
                    $(".UnitType", row).html("<span UnitAutoId='" + $(this).find("UnitAutoId").text() + "' QtyPerUnit='" + $(this).find("QtyPerUnit").text() + "'>" + $(this).find("UnitType").text() + " (" + $(this).find("QtyPerUnit").text() + " pcs" + ")</span>");
                    $(".AcceptedQty", row).text($(this).find("AcceptedQty").text());

                    $(".ReturnQty", row).text($(this).find("RequiredQty").text());
                    $(".UnitPrice", row).html($(this).find("ManagerUnitPrice").text());
                    $(".TtlPcs", row).text($(this).find("TotalPeice").text());
                    $(".SRP", row).text($(this).find("SRP").text());
                    $(".TaxRate", row).html('<span WeightOz=' + $(this).find("WeightOz").text() + ' mlqty=' + $(this).find("UnitMLQty").text() + ' istaxable=' + $(this).find("TaxRate").text() + '>' + (($(this).find("TaxRate").text() == 1) ? '<tax class="la la-check-circle success"></span>' : '') + '</tax>');
                    $(".NetPrice", row).text($(this).find("NetAmount").text());
                    $(".FreshReturn", row).html($(this).find("Return_Fresh").text());
                    $(".DemageReturn", row).html($(this).find("Return_Damage").text());
                    $(".MissingItem", row).html($(this).find("Return_Missing").text());
                    $(".Action", row).html("<a href='javascript:;' id='deleterow' onclick='deleterow(this)'><span class='ft-x'></span></a>");
                    $('#tblProductDetail tbody').append(row);

                    $(".Del_MinPrice", row).text($(this).find("Del_MinPrice").text());
                    $(".Del_CostPrice", row).text($(this).find("Del_CostPrice").text());
                    row = $("#tblProductDetail tbody tr:last").clone(true);
                });

                if ($(RemarksDetails).length > 0) {

                    $("#Table3 tbody tr").remove();
                    $("#OrderRemarksDetails").show();
                    var rowtest = $("#Table3 thead tr").clone();
                    $.each(RemarksDetails, function (index) {
                        $(".SRNO", rowtest).html((Number(index) + 1));
                        $(".EmployeeName", rowtest).html($(this).find("EmpName").text());
                        $(".EmployeeType", rowtest).html($(this).find("EmpType").text());
                        $(".Remarks", rowtest).html($(this).find("Remarks").text());
                        $("#Table3 tbody").append(rowtest);
                        rowtest = $("#Table3 tbody tr:last").clone(true);
                    });
                }

                $("#btnPrintOrder").show();
                $("#txtTotalAmount").val($(CreditOrder).find("GrandTotal").text());
                $("#txtOverallDisc").val($(CreditOrder).find("OverallDisc").text());
                $("#txtDiscAmt").val($(CreditOrder).find("OverallDiscAmt").text());
                $("#txtTotalTax").val($(CreditOrder).find("TotalTax").text());
                $("#txtGrandTotal").val($(CreditOrder).find("TotalAmount").text());
                $("#txtMLQty").val($(CreditOrder).find("MLQty").text());
                $("#txtMLTax").val($(CreditOrder).find("MLTax").text());
                $("#txtWeightQuantity").val($(CreditOrder).find("WeightTotalQuantity").text());
                $("#txtWeightTax").val($(CreditOrder).find("WeightTaxAmount").text());
                $("#txtAdjustment").val($(CreditOrder).find("AdjustmentAmt").text());
                MLTaxRate = parseFloat($(CreditOrder).find("MLTaxPer").text());
                if ($(CreditOrder).find("StatusCode").text() == 1 && $(CreditOrder).find("referenceorderno").text() == '') {
                    $('#btnCancelCreditMemo').hide();
                }
                else if ($(CreditOrder).find("StatusCode").text() == 3 && $(CreditOrder).find("referenceorderno").text() != '') {
                    $('#btnCancelCreditMemo').hide();
                }
                else if ($(CreditOrder).find("StatusCode").text() == 6) {
                    $('#btnCancelCreditMemo').hide();
                }
                else {
                    $('#btnCancelCreditMemo').sho();
                }

            } else {
                location.href = '/';
            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function print_NewOrder() {
    window.open("/Manager/CreditMemoPrint.html?PrintAutoId=" + $("#CreditAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
}


// update by satish  11/05/2019
var check = false;
function CancelCreditMemo() {

    if (check == false) {
        $("#SecurityEnabledVoid").modal('show');
    } else {
        $("#ModelCancelRemark").modal('show');
    }
}

function clickonSecurityVoid() {
    if ($("#txtSecurityVoid").val() == "") {
        $("#txtSecurityVoid").addClass('border-warning');
        toastr.error('Security key is required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    var data = {
        Security: $("#txtSecurityVoid").val()
    }
    $.ajax({
        type: "POST",
        url: "AccountCreditMemoView.aspx/CheckSecurity",
        data: "{'datavalue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == 'SessionExpired') {
                location.href = '/';
            } else {
                if (check == false) {
                    var xmldoc = $.parseXML(response.d);
                    var orderList = $(xmldoc).find("Table");
                    if (orderList.length > 0) {
                        $.each(orderList, function () {
                            if ($("#txtSecurityVoid").val() == $(orderList).find("SecurityValue").text()) {
                                check = true
                                $("#SecurityEnabledVoid").modal('hide');
                                $("#ModelCancelRemark").modal('show');
                            }
                        });
                    }
                    else {
                        swal("Warning!", "Access denied.", "warning", {
                            button: "OK",
                        }).then(function () {
                            $("#txtSecurityVoid").val('');
                            $("#txtSecurityVoid").focus();
                        })
                    }
                }
                else {
                    $("#SecurityEnabledVoid").modal('hide');
                }
            }
        },
        failure: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        },
        error: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        }
    });
}
function SaveCancelReason() {
    if ($("#ReasonCancel").val() == "") {
        $("#ReasonCancel").addClass('border-warning');
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    cancelRemark();
}


function cancelRemark() {
    swal({
        title: "Are you sure?",
        text: "You want to cancel this credit memo.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, Cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: false,
            },
            confirm: {
                text: "Yes, Cancel it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            Cancelcreditmemo2();

        } else {
            swal("", "Your credit memo is safe.", "error");
        }
    })
}

function Cancelcreditmemo2() {
    var data = {
        CancelRemark: $("#ReasonCancel").val(),
        CreditAutoId: $('#CreditAutoId').val()
    }
    $.ajax({
        type: "POST",
        url: "AccountCreditMemoView.aspx/CancelCreditMomo",
        data: "{'datavalue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {

            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },

        success: function (response) {
            if (response.d != 'Session Expired') {
                if (response.d != 'false') {
                    swal("", "Credit memo cancelled successfully.", "success");
                    $("#ModelCancelRemark").modal('hide');
                    $("#ReasonCancel").val('');
                    editCredit($('#CreditAutoId').val());
                    Orderrmarkdeatil($('#CreditAutoId').val());

                } else {
                    swal("Error!", "Oops, some things went wrongs.", "error");
                }
            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

//end Update by satish 11/05/2019

