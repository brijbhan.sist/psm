﻿$(document).ready(function () {
    $('#txtSFromDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtSToDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    $("#txtSFromDate").val(month + '/' + day + '/' + year);
    $("#txtSToDate").val(month + '/' + day + '/' + year);
    bindDropdown();
    bindCustomer();
    getCreditList(1);
});

function Pagevalue(e) {
    getCreditList(parseInt($(e).attr("page")));
};
function bindDropdown() {
    $.ajax({
        type: "POST",
        url: "AccountCreditMemoList.aspx/bindAllDropdown",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            if (response.d != "Session Expired") {
                var getData = $.parseJSON(response.d);

                $("#ddlCreditType option:not(:first)").remove();
                $.each(getData[0].OrderType, function (index, item) {
                    $("#ddlCreditType").append("<option value='" + item.AutoId + "'>" + item.OrderType + "</option>");
                });

                $("#ddlSalesPerson option:not(:first)").remove();
                $.each(getData[0].salesPerson, function (index, item) {
                    $("#ddlSalesPerson").append("<option value='" + item.AutoId + "'>" + item.SalesPerson + "</option>");
                });
                $("#ddlSalesPerson").select2();

                $("#ddlSStatus option:not(:first)").remove();
                $.each(getData[0].Status, function (index, item) {
                    $("#ddlSStatus").append("<option value='" + item.AutoId + "'>" + item.StatusType + "</option>");
                });
                $("#ddlSStatus").select2();
            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function bindCustomer() {
    var SalesPerson = $("#ddlSalesPerson").val();
    $.ajax({
        type: "POST",
        url: "AccountCreditMemoList.aspx/bindCustomer",
        data: "{'SalesPerson':'" + SalesPerson + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            if (response.d != "Session Expired") {
                var getData = $.parseJSON(response.d);

                $("#ddlCustomer option:not(:first)").remove();
                $.each(getData, function (index, item) {
                    $("#ddlCustomer").append("<option value='" + getData[index].CuId + "'>" + getData[index].CUN + "</option>");
                });
                $("#ddlCustomer").select2();
            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function getCreditList(pageIndex) {
    var data = {
        CustomerAutoId: $("#ddlCustomer").val(),
        CreditNo: $("#txtSOrderNo").val().trim(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        Status: $("#ddlSStatus").val(),
        CreditType: $("#ddlCreditType").val(),
        SalesPerson: $("#ddlSalesPerson").val(),
        pageIndex: pageIndex,
        PageSize: $("#ddlPageSize").val()
    };

    $.ajax({
        type: "POST",
        url: "AccountCreditMemoList.aspx/getCreditList",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var orderList = $(xmldoc).find("Table1");
                    var EmpType = $(xmldoc).find("Table2");

                    $("#tblOrderList tbody tr").remove();
                    var row = $("#tblOrderList thead tr").clone(true);

                    if (orderList.length > 0) {
                        $("#EmptyTable").hide();
                        $.each(orderList, function () {
                            $(".orderNo", row).html("<span OrderAutoId='" + $(this).find("AutoId").text() + "'>" + $(this).find("OrderNo").text() + "</span>");
                            $(".orderDt", row).text($(this).find("OrderDate").text());
                            $(".cust", row).text($(this).find("CustomerName").text());
                            $(".SalesPerson", row).text($(this).find("SalesPerson").text());
                            $(".CreditType", row).text($(this).find("CreditType").text());
                            $(".value", row).text($(this).find("GrandTotal").text());
                            $(".product", row).text($(this).find("NoOfItems").text());
                            $(".referenceorderno", row).text($(this).find("referenceorderno").text());

                            if (Number($(this).find("StatusCode").text()) == 1) {
                                $(".status", row).html("<span class='badge badge badge-pill badge-info'>" + $(this).find("Status").text() + "</span>");
                            } else if (Number($(this).find("StatusCode").text()) == 2) {
                                $(".status", row).html("<span class='badge badge badge-pill badge-primary'>" + $(this).find("Status").text() + "</span>");
                            } else if (Number($(this).find("StatusCode").text()) == 3) {
                                $(".status", row).html("<span class='badge badge badge-pill badge-primary'>" + $(this).find("Status").text() + "</span>");
                            } else if (Number($(this).find("StatusCode").text()) == 4) {
                                $(".status", row).html("<span class='badge badge badge-pill badge-warning'>" + $(this).find("Status").text() + "</span>");
                            } else if (Number($(this).find("StatusCode").text()) == 5) {
                                $(".status", row).html("<span class='badge badge badge-pill badge-warning'>" + $(this).find("Status").text() + "</span>");
                            } else if (Number($(this).find("StatusCode").text()) == 6) {
                                $(".status", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("Status").text() + "</span>");
                            } else if (Number($(this).find("StatusCode").text()) == 7) {
                                $(".status", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("Status").text() + "</span>");
                            } else if (Number($(this).find("StatusCode").text()) == 8) {
                                $(".status", row).html("<span class='badge badge badge-pill badge-default'>" + $(this).find("Status").text() + "</span>");
                            }

                            $(".action", row).html("<a title='Edit' href='/Account/AccountCreditMemoView.aspx?PageId=" + $(this).find("AutoId").text() + "'><span class='la la-eye'></span></a>&nbsp;<a title='Log' href='javascript:;' onclick='viewOrderLog(" + $(this).find("AutoId").text() + ",1)'><span class='la la-history'></span></a></b>");

                            $(".value", row).css('text-align', 'right');
                            $("#tblOrderList tbody").append(row);
                            row = $("#tblOrderList tbody tr:last").clone(true);
                        });
                    } else {
                        $("#EmptyTable").show();
                    }

                    var pager = $(xmldoc).find("Table");
                    $(".Pager").ASPSnippets_Pager({
                        ActiveCssClass: "current",
                        PagerCssClass: "page",
                        PageIndex: parseInt(pager.find("PageIndex").text()),
                        PageSize: parseInt(pager.find("PageSize").text()),
                        RecordCount: parseInt(pager.find("RecordCount").text())
                    });

                }
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function sortBy(sortCode) {
    CreditAutoId = $("#CreditLogAutoId").val();
    viewOrderLog(CreditAutoId, sortCode);
}
function viewOrderLog(CreditAutoId, sortInCode) {
    $("#CreditLogAutoId").val(CreditAutoId);
    if (sortInCode == 1) {
        $(".la-arrow-down").show();
        $(".la-arrow-up").hide();
    } else if (sortInCode == 2) {
        $(".la-arrow-down").hide();
        $(".la-arrow-up").show();
    }
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/WCreditMemoList.asmx/viewOrderLog",
        data: "{'CreditAutoId':" + CreditAutoId + ",'sortInCode':" + sortInCode + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == 'Session Expired') {
                location.href = '/';
            } else {
                var xmldoc = $.parseXML(response.d);
                var order = $(xmldoc).find("Table");
                var orderlog = $(xmldoc).find("Table1");

                $("#lblOrderNo").text(order.find("CreditNo").text());
                $("#lblOrderDate").text(order.find("CreditDate").text());

                $("#tblOrderLog tbody tr").remove();
                var row = $("#tblOrderLog thead tr").clone(true);
                if (orderlog.length > 0) {
                    $("#EmptyTable").hide();
                    $.each(orderlog, function (index) {
                        $(".SrNo", row).html(index + 1);
                        $(".ActionBy", row).html($(this).find("EmpName").text());
                        $(".Date", row).html($(this).find("LogDate").text());
                        $(".Action", row).html($(this).find("Action").text());
                        $(".Remark", row).html($(this).find("Remarks").text());

                        $("#tblOrderLog tbody").append(row);
                        row = $("#tblOrderLog tbody tr:last").clone(true);
                    });
                }
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
    $("#modalOrderLog").modal('show');
}