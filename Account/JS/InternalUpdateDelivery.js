﻿var rowupdate = "";
var CustomerId; var MLQtyRate = 0.00, mltaxNewRate = 0.00, WeigthOZTax = 0.00;
$(document).ready(function () {
    var getid = getQueryString('OrderAutoId');
    if (getid != null) {
        getOrderData(getid);
        $("#txtHOrderAutoId").val(getid);
        BindTaxType(getid)
    }
    if ($("#hEmpTypeNo").val() == 1) {
        $("#btnVoidOrder").show();
    }
    else {
        $("#btnVoidOrder").hide();
    }
    bindProduct();
    $("#txtBarcode").focus();
    $("input[name='rblAmountChange'][value='no']").prop("checked", true);
    $("input[name='rblDeliver']").change(function () {
        if ($(this).val() == 'yes') {
            $("input[name='rblPaymentReceive']").attr("disabled", false);
            $("#DriverComment").show();
        } else {
            $("input[name='rblPaymentReceive']").attr("disabled", true).prop("checked", false);
            $("#txtRemarks").attr("disabled", true).val("");
            $("#txtAmountValue").attr("disabled", true).val("0.00");
            $("#DriverComment").hide();
        }
        $("#txtRemarks").attr("disabled", false);
    });

    $("input[name='rblPaymentReceive']").change(function () {
        if ($(this).val() == 'yes') {
            $("input[name='rblReceivedAmount']").attr("disabled", false);
            $("#ddlPaymentThru").attr("disabled", false);

        } else {
            $("input[name='rblReceivedAmount']").attr("disabled", true).prop("checked", false);
            $("#ddlPaymentThru").attr("disabled", true).val(0);
        }
    });

    $("input[name='rblReceivedAmount']").change(function () {
        if ($(this).val() == 'full') {
            $("#txtAmountValue").val($("#txtGrandTotal").val()).attr("disabled", true);
        } else {
            $("#txtAmountValue").attr("disabled", false).val("");
        }
    });
});
var getQueryString = function (field, url) {
    var href = url ? url : window.location.href;
    var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
    var string = reg.exec(href);
    return string ? string[1] : null;
};
var thisOrderCredit = 0.00;
function getOrderData(OrderAutoId) {
    $.ajax({
        type: "POST",
        url: "/Account/WebAPI/InternalUpdateDelivery.asmx/getOrderData",
        data: "{'OrderAutoId':'" + OrderAutoId + "','check':'" + 2 + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var order = $(xmldoc).find("Table1");
            var product = $(xmldoc).find("Table2");
            var duePayment = $(xmldoc).find("Table3");
            var recPaySummary = $(xmldoc).find("Table4");
            var Unit = $(xmldoc).find("Table6");
            var CreditMemoDetails = $(xmldoc).find("Table7");
            var TaxTypeMaster = $(xmldoc).find("Table8");
            var RemarksDetails = $(xmldoc).find("Table9");
            var MLRateDetails = $(xmldoc).find("Table10");

            console.log(product);


            $("#Table2 tbody tr").remove();
            if ($(RemarksDetails).length > 0) {
                $("#orderremarks").show();
                var rowtest = $("#Table2 thead tr").clone();
                $.each(RemarksDetails, function (index) {
                    $(".SRNO", rowtest).text((Number(index) + 1));
                    $(".EmployeeName", rowtest).text($(this).find("EmpName").text());
                    $(".EmployeeType", rowtest).text($(this).find("EmpType").text());
                    $(".Remarks", rowtest).text($(this).find("Remarks").text());
                    $("#Table2 tbody").append(rowtest);
                    rowtest = $("#Table2 tbody tr:last").clone(true);
                })
            } else {
                $("#orderremarks").hide();
            }
            var totalCredit = 0.00;


            if (Number(CreditMemoDetails.length) > 0) {
                $("#CusCreditMemo").show();
                $("#tblCreditMemoList tbody tr").remove();
                var rowc = $("#tblCreditMemoList thead tr:last").clone(true);
                $.each(CreditMemoDetails, function (index) {

                    $(".SRNO", rowc).text(Number(index) + 1);
                    $(".Action", rowc).html("<input type='radio' name='credit' value=" + $(this).find("CreditAutoId").text() + "> ");
                    $(".CreditNo", rowc).text($(this).find("CreditNo").text());
                    $(".CreditDate", rowc).text($(this).find("CreditDate").text());
                    $(".ReturnValue", rowc).text($(this).find("ReturnValue").text());
                    totalCredit += parseFloat($(this).find("ReturnValue").text())
                    $("#tblCreditMemoList tbody").append(rowc)
                    rowc = $("#tblCreditMemoList tbody tr:last").clone(true);
                });
            } else {
                $("#CusCreditMemo").hide();
            }
            $("#TotalDue").text(totalCredit.toFixed(2));
            var UnitDetails = [];

            $.each(Unit, function () {
                UnitDetails.push({
                    AutoId: $(this).find("UnitAutoId").text(),
                    UnitType: $(this).find("UnitType").text(),
                    ProductId: $(this).find("ProductId").text(),
                    Qty: $(this).find("Qty").text(),
                });
            });

            localStorage.setItem("UnitItems", JSON.stringify(UnitDetails))
            CustomerId = $(order).find("CustomerId").text()
            $("#txtHOrderAutoId").val($(order).find("AutoId").text());
            $("#txtOrderId").val($(order).find("OrderNo").text());
            $("#lblOrderno").text($(order).find("OrderNo").text());
            $("#txtOrderType").val($(order).find("OrderType").text());
            if ($(order).find("OrderType").text() == 'POS ORDER') {
                $('#DrvDeliveryInfo').closest('.panel').hide();
            }
            $("#txtOrderDate").val($(order).find("OrderDate").text());
            if ($(order).find("CommentType").text() != '')
                $("#ddlCommentType").val($(order).find("CommentType").text());
            $("#txtComment").val($(order).find("Comment").text());
            $("#txtDeliveryDate").val($(order).find("DeliveryDate").text());
            $("#txtOrderStatus").val($(order).find("Status").text());
            $("#txtCustomer").val($(order).find("CustomerName").text());
            $("#hiddenCustAutoId").val($(order).find("CustAutoId").text());
            $("#txtTerms").val($(order).find("Terms").text());
            $("#txtTerms").val($(order).find("TermsDesc").text());
            $("#OrderRemarks").val($(order).find("OrderRemarks").text());
            $("#txtAdjustment").val($(order).find("AdjustmentAmt").text());
            $("#hfCustomerType").val($(order).find("TypeAutoId").text());
            $("#txtShippingType").val($(order).find("ShippingType").text());
            $("#txtCustomerType").val($(order).find("CustomerType").text());
            $("#txtSalesPerson").val($(order).find("SalesPerson").text());
            if (Number($(order).find("TypeAutoId").text()) == 3) {
                $("#txtWeightQty").val('0');
                $("#txtWeightTax").val('0.00');
                $("#txtMLQty").val('0');
                $("#txtMLTax").val('0.00');
                $("#ddlTaxType option:not(:first)").remove();
                $.each(TaxTypeMaster, function () {
                    $("#ddlTaxType").append("<option TaxValue='0' value='" + $(this).find("AutoId").text() + "'></option>");
                });
                $("#ddlTaxType").val($(order).find("TaxType").text());
                $("#ddlTaxType").attr('disabled', true);
                mltaxNewRate = 0.00;
            } else {
                if ($(MLRateDetails).find('MLTaxPer').text() != '') {
                    mltaxNewRate = parseFloat($(MLRateDetails).find('MLTaxPer').text());
                }
                $("#txtWeightQty").val($(order).find("Weigth_OZQty").text());
                $("#txtWeightTax").val($(order).find("Weigth_OZTaxAmount").text());
                WeigthOZTax = $(order).find("Weigth_OZTax").text()
                $("#txtMLQty").val($(order).find("MLQty").text());
                $("#txtMLTax").val($(order).find("MLTax").text());
                $("#ddlTaxType option:not(:first)").remove();
                $.each(TaxTypeMaster, function () {
                    $("#ddlTaxType").append("<option TaxValue='" + $(this).find("Value").text() + "' value='" + $(this).find("AutoId").text() + "'>" + $(this).find("TaxableType").text() + "-(" + $(this).find("Value").text() + ")</option>");
                });

                $("#ddlTaxType").val($(order).find("TaxType").text());
                $("#ddlTaxType").attr('disabled', true);
            }



            if ($(order).find("CreditAmount").text() != "") {
                $("#txtStoreCreditAmount").val($(order).find("CreditAmount").text());
            }
            thisOrderCredit = $(order).find("CreditAmount").text();
            if ($(order).find('PayableAmount').text() != '') {
                $("#txtPaybleAmount").val($(order).find("PayableAmount").text());
            } else {
                $("#txtPaybleAmount").val($(order).find("GrandTotal").text());
            }
            if ($(order).find("DeductionAmount").text() != "")
                $("#txtDeductionAmount").val($(order).find("DeductionAmount").text());
            $("#CreditMemoAmount").text($(order).find("CustomerCredit").text())
            $("#txtBillAddress").val($(order).find("BillAddr").text());
            $("#txtBillState").val($(order).find("State1").text());
            $("#txtBillCity").val($(order).find("City1").text());
            $("#txtBillZip").val($(order).find("Zipcode1").text());

            $("#txtShipAddress").val($(order).find("ShipAddr").text());
            $("#txtShipState").val($(order).find("State2").text());
            $("#txtShipCity").val($(order).find("City2").text());
            $("#txtShipZip").val($(order).find("Zipcode2").text());

            $("#txtTotalAmount").val($(order).find("TotalAmount").text());
            $("#txtOverallDisc").val($(order).find("OverallDisc").text());
            $("#txtDiscAmt").val($(order).find("OverallDiscAmt").text());
            $("#txtShipping").val($(order).find("ShippingCharges").text());
            $("#txtTotalTax").val($(order).find("TotalTax").text());
            $("#txtGrandTotal").val($(order).find("GrandTotal").text());
            $("#hiddenGrandTotal").val($(order).find("GrandTotal").text());
            $("#txtPackedBoxes").val($(order).find("PackedBoxes").text());
            $("#lblNoOfbox").text($(order).find("PackedBoxes").text());
            if (Number($(order).find("StatusCode").text()) == 8) {
                $("#btnVoidOrder").hide();
            }
            var orderStatus = Number($(order).find("StatusCode").text());
            var EmpType = Number($("#hiddenEmpTypeVal").val());
            if (orderStatus == 11) {
                $("#btnUpdateCostPrice").hide();
            }
            else {
                $("#btnUpdateCostPrice").show();
            }
            showDuePayments(duePayment);

            $("#tblProductDetail tbody tr").remove();
            var row = $("#tblProductDetail thead tr").clone(true);
            var FreshReturnUnitAutoId = 0;
            var DamageReturnUnitAutoId = 0;
            var MissingItemUnitAutoId = 0;
            $.each(product, function () {
                if ($(this).find("FreshReturnUnitAutoId").text() != '') {
                    FreshReturnUnitAutoId = $(this).find("FreshReturnUnitAutoId").text();
                } else {
                    FreshReturnUnitAutoId = $(this).find("UnitAutoId").text();
                }
                if ($(this).find("DamageReturnUnitAutoId").text() != '') {
                    DamageReturnUnitAutoId = $(this).find("DamageReturnUnitAutoId").text();
                } else {
                    DamageReturnUnitAutoId = $(this).find("UnitAutoId").text();
                }
                // sachin changess start
                if ($(this).find("MissingItemUnitAutoId").text() != '') {
                    MissingItemUnitAutoId = $(this).find("MissingItemUnitAutoId").text();
                } else {
                    MissingItemUnitAutoId = $(this).find("UnitAutoId").text();
                }
                // sachin changess end
                $(".UpdateStock", row).text(0);
                $(".ProId", row).html($(this).find("ProductId").text() + "<span IsExchange='" + $(this).find("IsExchange").text() + "' isFreeItem='" + $(this).find("isFreeItem").text() + "'></span>");

                if ($(this).find("isFreeItem").text() == '1') {
                    $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "'>" + $(this).find("ProductName").text() + "</span> <span class='badge badge-pill badge-success'>Free</span>");
                }
                else if ($(this).find("Tax").text() == '1') {
                    $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "'>" + $(this).find("ProductName").text() + "</span> <span class='badge badge-pill badge-danger'>Taxable</span>");
                }
                else if ($(this).find("IsExchange").text() == '1') {
                    $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "'>" + $(this).find("ProductName").text() + "</span> <span class='badge badge-pill badge-primary'>Exchange</span>");
                }
                else {
                    $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "'>" + $(this).find("ProductName").text() + "</span>");

                }
                $(".UnitType", row).html("<span UnitAutoId='" + $(this).find("UnitAutoId").text() + "' QtyPerUnit='" + $(this).find("QtyPerUnit").text() + "'>" + $(this).find("UnitType").text() + " (" + $(this).find("QtyPerUnit").text() + " pcs" + ")</span>");
                $(".TtlPcs", row).text($(this).find("TotalPieces").text());
                $(".NetPrice", row).text($(this).find("NetPrice").text());
                $(".Status", row).text('0');
                if (EmpType == 5) {
                    $(".UnitPrice", row).text($(this).find("UnitPrice").text());
                } else {
                    //umang2
                    if (Number($(this).find("IsExchange").text()) == 1 || Number($(this).find("isFreeItem").text()) == 1) {
                        $(".UnitPrice", row).html("<input type='text' maxlength='6' onchange='calculateReturnQty(this)' class='form-control input-sm border-primary' style='text-align:right;' value='" + $(this).find("UnitPrice").text() + "' DefaultPrice='" + $(this).find("UnitPrice").text() + "' MinPrice='" + $(this).find("MinPrice").text() + "' onkeypress='return isNumberDecimalKey(event,this)' disabled/>");
                    } else {
                        $(".UnitPrice", row).html("<input type='text' maxlength='6' onchange='calculateReturnQty(this)' class='form-control input-sm border-primary' style='text-align:right;' value='" + $(this).find("UnitPrice").text() + "' DefaultPrice='" + $(this).find("UnitPrice").text() + "' MinPrice='" + $(this).find("MinPrice").text() + "' onkeypress='return isNumberDecimalKey(event,this)'/>");
                    }
                }
                $(".PerPrice", row).text($(this).find("PerpiecePrice").text());
                $(".SRP", row).text($(this).find("SRP").text());
                $(".GP", row).text($(this).find("GP").text());

                if (Number($(this).find("Tax").text()) == 1) {
                    $(".TaxRate", row).html('<span TaxRate="1" MLQty="' + $(this).find('UnitMLQty').text() + '" WeightOz="' + $(this).find('Weight_Oz').text() + '"></span><Taxable class="la la-check-circle success"></Taxable>');
                } else {
                    $(".TaxRate", row).html('<span TaxRate="0" MLQty="' + $(this).find('UnitMLQty').text() + '" WeightOz="' + $(this).find('Weight_Oz').text() + '"></span>');
                }
                if (Number($(this).find("IsExchange").text()) == 1) {
                    $(".IsExchange", row).html('<span IsExchange="1" ></span><IsExchange class="la la-check-circle success"></IsExchange>');
                } else {
                    $(".IsExchange", row).html('<span IsExchange="0"></span>');
                }
                $(".Barcode", row).text($(this).find("Barcode").text());
                $(".action", row).text('-');

                $(".QtyShip", row).html('<input type="text" onkeypress="return isNumberKey(event)" class="form-control input-sm border-primary text-center"  onkeyup="rowCal(this)"  value="' + $(this).find("QtyShip").text() + '" disabled/>');

                if ($(this).find("QtyDel").text() != "") {
                    $(".QtyDel", row).html($(this).find("QtyDel").text());
                } else {
                    $(".QtyDel", row).html($(this).find("TotalPieces").text());
                }

                if ($(this).find("FreshReturnQty").text() != "") {
                    $(".FreshReturn", row).html("<div class='input-group'><div class='input-group-prepend'><input type='text' onchange='calculateReturnQty(this)' class='form-control input-sm border-primary text-center' style='width:50px;padding:0.25rem 0.25rem;margin-right: 2px;' value='" + $(this).find("FreshReturnQty").text() + "' onkeypress='return isNumberKey(event)'/><select style='width:58px;padding:0' class='freshunit form-control input-sm border-primary' onchange='calculateReturnQty(this)' disabled='disabled'></select></div></div>");
                } else {
                    $(".FreshReturn", row).html("<div class='input-group'><div class='input-group-prepend'><input type='text' onchange='calculateReturnQty(this)' class='form-control input-sm border-primary  text-center' style='width:50px;padding:0.25rem 0.25rem;margin-right: 2px;' value='0' onkeypress='return isNumberKey(event)'/><select style='width:58px;padding:0' class='freshunit form-control input-sm border-primary' onchange='calculateReturnQty(this)' disabled='disabled'></select></div></div>");
                }
                if ($(this).find("DamageReturnQty").text() != "") {
                    $(".DemageReturn", row).html("<div class='input-group-prepend'><input type='text'  onchange='calculateReturnQty(this)' class='form-control input-sm border-primary  text-center' style='width:50px;padding:0.25rem 0.25rem;margin-right: 2px;' value='" + $(this).find("DamageReturnQty").text() + "'  onkeypress='return isNumberKey(event)'/><select style='width:58px;padding:0' class='demageunit form-control input-sm border-primary'  onchange='calculateReturnQty(this)' disabled='disabled'></select></div>");
                } else {
                    $(".DemageReturn", row).html("<div class='input-group-prepend'><input type='text'  onchange='calculateReturnQty(this)' class='form-control input-sm border-primary  text-center' style='width:50px;padding:0.25rem 0.25rem;margin-right: 2px;' value='0'  onkeypress='return isNumberKey(event)'/><select style='width:58px;padding:0' class='demageunit form-control input-sm border-primary'  onchange='calculateReturnQty(this)' disabled='disabled'></select></div>");
                }
                // sachin changess start
                if ($(this).find("MissingItemQty").text() != "") {
                    $(".MissingItem", row).html("<div class='input-group-prepend'><input type='text'  onchange='calculateReturnQty(this)' class='form-control input-sm border-primary  text-center' style='width:50px;padding:0.25rem 0.25rem;margin-right: 2px;' value='" + $(this).find("MissingItemQty").text() + "'  onkeypress='return isNumberKey(event)'/><select style='width:58px;padding:0' class='missingunit form-control input-sm border-primary'  onchange='calculateReturnQty(this)' disabled='disabled'></select></div>");
                } else {
                    $(".MissingItem", row).html("<div class='input-group-prepend'><input type='text'  onchange='calculateReturnQty(this)' class='form-control input-sm border-primary  text-center' style='width:50px;padding:0.25rem 0.25rem;margin-right: 2px;' value='0'  onkeypress='return isNumberKey(event)'/><select style='width:58px;padding:0' class='missingunit form-control input-sm border-primary'  onchange='calculateReturnQty(this)' disabled='disabled'></select></div>");
                }
                // sachin changess end
                if (Number($(this).find("FreshReturnQty").text()) > 0 || Number($(this).find("DamageReturnQty").text()) > 0) {
                    $(row).css('background-color', '#efcccc');
                }
                else if (Number($(this).find("MissingItemQty").text()) > 0) {
                    $(row).css('background-color', '#edb3b3');
                }
                else {
                    $(row).css('background-color', '#fff');
                }
                if ($(this).find("QtyShip").text() == "0") {
                    $(row).hide();
                } else {
                    $(row).show();
                }
                $('#tblProductDetail tbody').append(row);
                var getUnit = JSON.parse(localStorage.getItem('UnitItems'));
                $.grep(getUnit, function (e) {
                    if (e.ProductId == $(row).find(".ProId").text()) {
                        if ($(this).find("FreshReturnUnitAutoId").text() != 0) {
                            if ($(row).find(".UnitType span").attr('UnitAutoId') == e.AutoId) {
                                row.find('.freshunit').append($("<option value=" + e.AutoId + " Qty=" + e.Qty + " selected='selected'>" + e.UnitType + "</option>"));
                                row.find('.demageunit').append($("<option value=" + e.AutoId + " Qty=" + e.Qty + " selected='selected'>" + e.UnitType + "</option>"));
                                // sachin changess start
                                row.find('.missingunit').append($("<option value=" + e.AutoId + " Qty=" + e.Qty + " selected='selected'>" + e.UnitType + "</option>"));
                                // sachin changess end
                            }
                            else {
                                row.find('.freshunit').append($("<option value=" + e.AutoId + " Qty=" + e.Qty + ">" + e.UnitType + "</option>"));
                                row.find('.demageunit').append($("<option value=" + e.AutoId + " Qty=" + e.Qty + ">" + e.UnitType + "</option>"));
                                // sachin changess start
                                row.find('.missingunit').append($("<option value=" + e.AutoId + " Qty=" + e.Qty + ">" + e.UnitType + "</option>"));
                                // sachin changess end
                            }
                        } else {
                            if (FreshReturnUnitAutoId == e.AutoId) {

                                row.find('.freshunit').append($("<option value=" + e.AutoId + " Qty=" + e.Qty + " selected='selected'>" + e.UnitType + "</option>"));

                            }
                            else {
                                row.find('.freshunit').append($("<option value=" + e.AutoId + " Qty=" + e.Qty + ">" + e.UnitType + "</option>"));


                            }
                            if (DamageReturnUnitAutoId == e.AutoId) {


                                row.find('.demageunit').append($("<option value=" + e.AutoId + " Qty=" + e.Qty + " selected='selected'>" + e.UnitType + "</option>"));
                            }
                            else {

                                row.find('.demageunit').append($("<option value=" + e.AutoId + " Qty=" + e.Qty + ">" + e.UnitType + "</option>"));
                            }
                            // sachin changess start
                            if (MissingItemUnitAutoId == e.AutoId) {
                                row.find('.missingunit').append($("<option value=" + e.AutoId + " Qty=" + e.Qty + " selected='selected'>" + e.UnitType + "</option>"));
                            }
                            else {
                                row.find('.missingunit').append($("<option value=" + e.AutoId + " Qty=" + e.Qty + ">" + e.UnitType + "</option>"));
                            }
                            // sachin changess end
                        }
                    }



                });
                $(".Del_MinPrice", row).text($(this).find("Del_MinPrice").text());
                $(".Del_CostPrice", row).text($(this).find("Del_CostPrice").text());
                $(".Del_BasePrice", row).text($(this).find("Del_BasePrice").text());
                row = $("#tblProductDetail tbody tr:last").clone(true);
            });

            if (EmpType == 5 || EmpType == 2 || EmpType == 4 || EmpType == 1) {
                if (orderStatus >= 6) {

                    if (orderStatus == 6 || orderStatus == 11) {
                        $("input[name='rblDeliver'][value='yes']").prop("checked", true);
                    } else if (orderStatus == 7) {
                        $("input[name='rblDeliver'][value='no']").prop("checked", true);
                    }
                    $("#txtRemarks").val($(order).find("DrvRemarks").text()).attr("disabled", false);

                    if ($(order).find("NewTotal").text() == '') {
                        $("#btnUpdate").show();
                    }
                    else {
                        $("#DrvDeliveryInfo *").attr("disabled", true);
                        $("#btnUpdate").hide();
                    }
                }
                else {
                    $("#btnPickedOrder").hide();
                }
                if (EmpType == 5 && orderStatus == 5) {
                    $("#btnPickedOrder").show();
                }
            }
            if (orderStatus == 6 && (EmpType == 5 || EmpType == 6 || EmpType == 1)) {
                $("#DriverComment").show();
            }
            if ((EmpType == 6 || EmpType == 1) && (orderStatus == 6 || orderStatus == 11)) {

                if (EmpType == 1 && $(order).find("NewTotal").text() == '') {
                    $("#DrvDeliveryInfo *").attr("disabled", false);
                }
                else {
                    $("#DrvDeliveryInfo *").attr("disabled", true);
                }
                $("#txtRemarks").val($(order).find("DrvRemarks").text());

                if (orderStatus == 6 || (orderStatus == 11)) {
                    $("input[name='rblDeliver'][value='yes']").prop("checked", true);
                } else if (orderStatus == 7) {
                    $("input[name='rblDeliver'][value='no']").prop("checked", true);
                }

                $("#tblProductDetail").find(".QtyDel").show();

                if ($(order).find("PaymentRecev").text() == null || $(order).find("PaymentRecev").text() == "") {

                    $("#AccountDeliveryInfo").show().find("input[name='rblPaymentReceive']").attr("disabled", false).end()
                        .find("#txtAcctRemarks").attr("disabled", false).end();
                    $("#rowAmountPaid").hide();
                    $("#rowAmountDue").hide();

                    if ((EmpType == 6)) {
                        $("#btnSave").show();
                    }

                } else {
                    $("#AccountDeliveryInfo").show();
                    $("input[name='rblPaymentReceive'][value='" + $(order).find("PaymentRecev").text() + "']").prop("checked", true);
                    $("input[name='rblPaymentReceive']").attr("disabled", false);
                    $("input[name='rblReceivedAmount'][value='" + $(order).find("RecevAmt").text() + "']").prop("checked", true);
                    if ($(order).find("PaymentRecev").text() == 'no') {
                        $("input[name='rblReceivedAmount']").attr("disabled", true);
                    } else {
                        $("input[name='rblReceivedAmount']").attr("disabled", false);
                    }

                    $("#txtAmountValue").val($(order).find("AmtValue").text());
                    if ($(order).find("RecevAmt").text() == 'full' || $(order).find("RecevAmt").text() == 'none') {
                        $("#txtAmountValue").attr("disabled", true);
                    } else {
                        $("#txtAmountValue").attr("disabled", false);
                    }
                    $("input[name='rblAmountChange'][value='" + $(order).find("ValueChanged").text() + "']").prop("checked", true).attr("disabled", true);
                    $("#txtDiffAmount").val($(order).find("DiffAmt").text()).attr("disabled", true);
                    $("#txtNewTotal").val($(order).find("NewTotal").text()).attr("disabled", true);
                    $("#ddlPaymentThru").val($(order).find("PayThru").text()).attr("disabled", false);
                    if ($(order).find("PayThru").text() == 1) {
                        $("#txtTransactionNo").attr('disabled', false);
                    }
                    else {
                        $("#txtTransactionNo").attr('disabled', true);
                    }
                    $("#txtTransactionNo").val($(order).find("CheckNo").text());
                    if ($(order).find("PaymentRecev").text() == 'no') {
                        $("#ddlPaymentThru").attr("disabled", true);
                    } else {
                        $("#ddlPaymentThru").attr("disabled", false);
                    }
                    $("#txtAcctRemarks").val($(order).find("AcctRemarks").text()).attr("disabled", false);
                    $("#rowAmountPaid").show();
                    $("#txtAmtPaid").val($(order).find("AmtPaid").text());
                    $("#rowAmountDue").show();
                    $("#txtAmtDue").val($(order).find("AmtDue").text());

                    if ($(order).find("AmtDue").text() <= 0) {
                        $("#btnPayNow").hide();
                        $("#rowAmountDue").hide();
                    }
                    else {
                        $("#btnPayNow").show();
                    }
                    $("#btnSave").hide();
                    $("#btnAUpdate").show();
                    if (Number($(order).find("AmtDue").text()) == 0.00) {
                        $("#btnGenPI").show();
                    }
                }
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function calculateReturnQty(e) {

    var UnitPrice = 0.00;
    var row = $(e).closest('tr');
    var TotalPiece = $(row).find('.TtlPcs').text() || 0;
    var ReturnQty = $(row).find('.FreshReturn input').val() || 0;
    var ReturnUnitQty = $(row).find('.FreshReturn :selected').attr('qty') || 0;
    var DamageQty = $(row).find('.DemageReturn input').val() || 0;
    var DamageUnitQty = $(row).find('.DemageReturn :selected').attr('qty') || 0;
    // sachin changess start
    var MissingQty = $(row).find('.MissingItem input').val() || 0;
    var MissingUnitQty = $(row).find('.MissingItem :selected').attr('qty') || 0;
    // sachin changess end
    var TotalReturn = ((ReturnQty * ReturnUnitQty) + (DamageQty * DamageUnitQty) + (MissingQty * MissingUnitQty)) || 0;

    var reqQty = $(row).find('.QtyShip input').attr('value');
    var qtyPerUnit = $(row).find('.UnitType span').attr('qtyperunit');
    reqQty = Number(reqQty) * Number(qtyPerUnit);

    if ($(row).find('.UnitPrice input').val() == "") {
        $(row).find('.UnitPrice input').val('0.00');
    }
    UnitPrice = $(row).find('.UnitPrice input').val() || 0.00;
    var minPriceDefault = $(row).find('.UnitPrice input').attr('minprice') || 0;
    if (parseFloat(UnitPrice) < parseFloat(minPriceDefault)) {
        $(row).find('.UnitPrice input').addClass('border-warning');
    } else {
        $(row).find('.UnitPrice input').removeClass('border-warning');
    }

    if (TotalReturn <= reqQty) {
        var TotalQty = TotalPiece - TotalReturn;
        $(row).find('.QtyDel').text(TotalQty);
        var UnitQtyPer = $(row).find('.UnitType span').attr('qtyperunit');
        var TtlPcs = $(row).find('.TtlPcs').text();
        var TotalAmount1 = parseFloat(parseFloat(UnitPrice) / parseFloat(UnitQtyPer)) * parseFloat(TotalQty) || 0.00;
        var PerpiecePrie = $(row).find('.PerPrice').text() || 0.00;
        var changeAmount = parseFloat(UnitPrice) / parseFloat(UnitQtyPer) || 0.00;
        $(row).find('.PerPrice').text(changeAmount.toFixed(2))
        var Tax = $("#txtTotalTax").val();
        var NetAmount = (TotalAmount1);//+ totalAmount;
        $(row).find('.NetPrice').text(NetAmount.toFixed(2));
    }
    else {
        //$("#modalUpdateStock").modal('show');

        $(row).find('.DemageReturn input').val(0);
        $(row).find('.FreshReturn input').val(0);
        $(row).find('.MissingItem input').val(0);


        swal({
            title: "",
            text: "Total of Fresh, Return and Missing Qty should be less than or equal to Total Pieces.",
            icon: "warning",
            allowOutsideClick: false,
            closeOnClickOutside: false,
            button: "OK",
        }).then(function () {
            return;
        })

        $(row).find('.QtyDel').text($(row).find('.TtlPcs').text());
        var PerpiecePrie = $(row).find('.PerPrice').text() || 0.00;
        var Tax = $("#txtTotalTax").val();
        var totalAmount = ((PerpiecePrie * $(row).find('.QtyDel').text()) * Tax / 100) || 0.00;
        var NetAmount = (PerpiecePrie * $(row).find('.QtyDel').text()) + totalAmount || 0.00;
        $(row).find('.NetPrice').text(NetAmount.toFixed(2));

    }
    calTotalAmount();
}
function showDuePayments(duePayment) {
    $("#tblCustDues tbody tr").remove();
    var row = $("#tblCustDues thead tr").clone(true);

    var sumOrderValue = 0,
        sumPaid = 0,
        sumDue = 0;

    if (duePayment.length > 0) {
        $("#noDues").hide();
        $.each(duePayment, function () {
            $(".orderNo", row).html("<span orderautoid='" + $(this).find("AutoId").text() + "'><a href='#'>" + $(this).find("OrderNo").text() + "</a></span>");
            $(".orderDate", row).text($(this).find("OrderDate").text());
            $(".value", row).text($(this).find("GrandTotal").text());
            $(".OrderType", row).text($(this).find("OrderType").text());
            $(".amtPaid", row).text($(this).find("AmtPaid").text()).css("color", "#8bc548");
            $(".amtDue", row).text($(this).find("AmtDue").text()).css("color", "red");
            $(".pay", row).html("<input type='text' class='form-control input-sm' style='width:100px;float:right;text-align:right;' value='0.00' placeholder='0.00' onkeyup='sumPay()' />");
            $(".remarks", row).html("<input type='text' class='form-control input-sm' placeholder='Enter payment details here' />");

            sumOrderValue += Number($(this).find("GrandTotal").text());
            sumPaid += Number($(this).find("AmtPaid").text());
            sumDue += Number($(this).find("AmtDue").text());

            $('#tblCustDues').find("tbody").append(row).end()
                .find("tfoot").show().find("#sumOrderValue").text(sumOrderValue.toFixed(2)).end()
                .find("#sumPaid").text(sumPaid.toFixed(2)).css("color", "#8bc548").end()
                .find("#sumDue").text(sumDue.toFixed(2)).css("color", "red").end()
                .find("#sumPay").text("0.00").end();

            row = $("#tblCustDues tbody tr:last").clone(true);
        });

        if ($("#hiddenEmpTypeVal").val() != "5") {
            $("#CustDues").find("#btnPay_Dues").show().end()
                .find("#tblCustDues").find(".pay").show().end()
                .find(".remarks").show().end()
                .find("tfoot > tr > td").show();
        }
    } else {
        $("#tblCustDues").find("tbody tr").remove().end().find("tfoot").hide();
        $("#noDues").show();

        $("#btnPay_Dues, #btnClose_Dues").hide();
    }
}
function sumPay() {
    var sumPay = 0.00;
    $("#tblCustDues tbody tr").each(function () {
        sumPay += Number($(this).find(".pay input").val());
    });
    $("#tblCustDues tfoot tr").find("#sumPay").text(sumPay.toFixed(2));
}
function payDueAmount() {
    var Payment = [];

    $("#tblCustDues tbody tr").each(function () {
        Payment.push({
            'OrderAutoId': $(this).find('.orderNo').find('span').attr('orderAutoId'),
            'AmtPaying': $(this).find('.pay input').val(),
            'Remarks': $(this).find('.remarks input').val()
        });
    });

    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/orderMaster.asmx/payDueAmount",
        data: "{'PaymentValues':'" + JSON.stringify(Payment) + "','CustAutoId':'" + $("#hiddenCustAutoId").val() + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $("#fade").show();
        },
        complete: function () {
            $("#fade").hide();
        },
        success: function (response) {
            if (response.d == "SessionExpired") {
                location.href = "/";
            }
            else {
                var xmldoc = $.parseXML(response.d);
                var duePayment = $(xmldoc).find("Table");
                showDuePayments(duePayment);
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function rowCal(e) {

    var row = $(e).closest("tr");

    if (row.find(".QtyShip input").val() >= row.find(".QtyDel input").val()) {

        var totalPcs = (Number($(e).val()) * Number(row.find(".UnitType span").attr("QtyPerUnit")));
        row.find(".TtlPcs").text(totalPcs);

        price = Number(row.find(".UnitPrice").text());

        var netPrice = (price * Number(row.find(".QtyDel input").val()) * (1 + Number(row.find(".TaxRate").text()) * 0.01));

        row.find(".NetPrice").text(netPrice.toFixed(2));

        calTotalAmount();
    }
    else {
        swal({
            title: "",
            text: "Delivered Qty always less than or equal to shipped qty.",
            icon: "success",
            button: "OK",
        }).then(function () {
            row.find(".QtyDel input").val(row.find(".QtyShip input").val())
        })
    }
}
function calTotalAmount() {
    var total = 0.00, MLQty = 0.00, WeightOzQty = 0.00;
    console.clear();
    $("#tblProductDetail tbody tr").each(function () {
        UnitPrice = $(this).find('.UnitPrice input').val() || 0.00;
        var UnitQtyPer = $(this).find('.UnitType span').attr('qtyperunit') || 0;
        var TotalAmount1 = parseFloat(parseFloat(UnitPrice) / parseFloat(UnitQtyPer));
        total += parseFloat(parseFloat(UnitPrice) / parseFloat(UnitQtyPer)) * Number($(this).find(".QtyDel").text());
        if ($(this).find('.ProId span').attr('isexchange') == '0' && $(this).find('.ProId span').attr('isfreeitem') == '0')
            MLQty += (parseFloat($(this).find('.TaxRate span').attr('mlqty')) * Number($(this).find(".QtyDel").text()));

        WeightOzQty += (parseFloat($(this).find('.TaxRate  span').attr('weightoz') * $(this).find('.QtyDel').text()));
    });
    $("#txtTotalAmount").val(total.toFixed(2));
    if ($("#hfCustomerType").val() == '3') {
        $("#txtMLQty").val('0.00');
        $("#txtMLTax").val('0.00');
        $("#txtWeightQty").val('0.00');
        $("#txtWeightTax").val('0.00');
    }
    else {
        $("#txtMLQty").val(MLQty.toFixed(2));
        $("#txtMLTax").val((MLQty * mltaxNewRate).toFixed(2));
        $("#txtWeightQty").val(WeightOzQty.toFixed(2));
        $("#txtWeightTax").val((WeightOzQty * WeigthOZTax).toFixed(2));
    }
    calOverallDisc();
}
function calOverallDisc1() {

    if ($("#txtDiscAmt").val() == "") {
        $("#txtDiscAmt").val('0.00');
        $("#txtOverallDisc").val('0.00');
    }
    else if (Number($("#txtDiscAmt").val()) > Number($("#txtTotalAmount").val())) {
        $("#txtDiscAmt").val('0.00');
        $("#txtOverallDisc").val('0.00');
        toastr.error('Discount amount cannot be greater than Total Amount.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

    }
    else {
        var DiscAmt = parseFloat($("#txtDiscAmt").val()) || 0.00;
        var TotalAmount = (Number($("#txtTotalAmount").val()));
        if (parseFloat(TotalAmount) == 0) {
            $("#txtOverallDisc").val('0.00');
            $("#txtDiscAmt").val('0.00');
        } else {
            var per = (DiscAmt / TotalAmount) * 100;
            $("#txtOverallDisc").val(per.toFixed(2));
        }
        calTotalTax();
    }
    calTotalTax();
}
function calOverallDisc() {
    var factor = 0.00;
    if ($("#txtOverallDisc").val() == "") {
        $("#txtOverallDisc").val('0.00');
    }
    else if (parseFloat($("#txtOverallDisc").val()) > 100) {
        $("#txtOverallDisc").val('0.00');
        toastr.error('Discount % cannot be greater than 100.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }

    factor = Number($("#txtOverallDisc").val()) * 0.01 || 0.00;
    $("#txtDiscAmt").val((Number($("#txtTotalAmount").val()) * factor).toFixed(2));
    calTotalTax();
}
function calTotalTax() {
    var totalTax = 0.00, qty;
    $("#tblProductDetail tbody tr").each(function () {

        if ($(this).find('.TaxRate span').attr('taxrate') == 1) {
            var totalPrice = Number($(this).find(".NetPrice").text());
            var Disc = totalPrice * Number($("#txtOverallDisc").val()) * 0.01;
            var priceAfterDisc = totalPrice - Disc;
            if ($('#ddlTaxType').val() != null && $('#ddlTaxType').val() != "") {
                totalTax += priceAfterDisc * Number($('#ddlTaxType option:selected').attr("TaxValue")) * 0.01;
            }
        }
    });
    if ($("#hfCustomerType").val() == '3') {
        $("#txtTotalTax").val('0.00');
    }
    else {
        $("#txtTotalTax").val(totalTax.toFixed(2));
    }
    calGrandTotal();
}
function calGrandTotal() {
    var grandTotal = parseFloat($("#txtTotalAmount").val()) - parseFloat($("#txtDiscAmt").val()) + parseFloat($("#txtShipping").val()) + parseFloat($("#txtTotalTax").val()) + parseFloat($("#txtWeightTax").val())
        + parseFloat($("#txtMLTax").val());
    var round = Math.round(grandTotal);
    $("#txtAdjustment").val((round - grandTotal).toFixed(2));
    $("#txtGrandTotal").val(round.toFixed(2));

    if (Number($("#txtGrandTotal").val()) == Number($("#hiddenGrandTotal").val())) {
        $("input[name='rblAmountChange'][value='no']").prop("checked", true);
        $("#txtDiffAmount").val("0.00");
        $("#txtNewTotal").val("0.00");
    } else {
        $("input[name='rblAmountChange'][value='yes']").prop("checked", true);
        var diffAmt = Number($("#hiddenGrandTotal").val()) - Number($("#txtGrandTotal").val());
        $("#txtDiffAmount").val(diffAmt.toFixed(2));
        $("#txtNewTotal").val($("#txtGrandTotal").val());
    }

    if ($("input[name='rblReceivedAmount'][value='full']").is(":checked")) {
        $("#txtAmountValue").val($("#txtGrandTotal").val()).attr("disabled", true);
    }

    var hiddenGrandAmount = parseFloat($("#hiddenGrandTotal").val());
    var payableAmount = parseFloat($("#txtPaybleAmount").val());
    var CreditMemoAmount = parseFloat($("#txtDeductionAmount").val());
    var nowGrandTotal = parseFloat($("#txtGrandTotal").val());
    var CustomerCredit = $("#txtStoreCreditAmount").val();

    if (parseFloat(nowGrandTotal) <= parseFloat(hiddenGrandAmount)) {

        if (parseFloat(thisOrderCredit) > 0) {
            var payableAmount1 = parseFloat(CustomerCredit) + parseFloat(CreditMemoAmount);
            if (parseFloat(nowGrandTotal) > parseFloat(payableAmount1)) {
                $("#txtPaybleAmount").val((parseFloat(nowGrandTotal) - (payableAmount1)).toFixed(2));
            } else {
                $("#txtPaybleAmount").val('0.00');
            }

        } else {

            if (parseFloat(nowGrandTotal) <= CreditMemoAmount) {
                CustomerCredit = parseFloat(thisOrderCredit.replace('-', ''));
                CustomerCredit = CustomerCredit + (parseFloat(CreditMemoAmount) - parseFloat(nowGrandTotal));
                if (parseInt(CustomerCredit) > 0) {
                    $("#txtStoreCreditAmount").val('-' + parseFloat(CustomerCredit).toFixed(2));
                } else {
                    $("#txtStoreCreditAmount").val('0.00');
                }
                $("#txtPaybleAmount").val('0.00');
            } else {
                CustomerCredit = parseFloat(thisOrderCredit.replace('-', ''));
                $("#txtPaybleAmount").val((parseFloat(nowGrandTotal) - parseFloat(CreditMemoAmount) - parseFloat(CustomerCredit)).toFixed(2))
            }
        }
    } else {
        if (parseFloat(thisOrderCredit) > 0) {
            var payableAmount1 = parseFloat(CustomerCredit) + parseFloat(CreditMemoAmount);
            if (parseFloat(nowGrandTotal) > parseFloat(payableAmount1)) {
                $("#txtPaybleAmount").val((parseFloat(nowGrandTotal) - (payableAmount1)).toFixed(2));
            } else {
                $("#txtPaybleAmount").val('0.00');
            }
        } else {
            if (parseFloat(nowGrandTotal) <= CreditMemoAmount) {
                CustomerCredit = parseFloat(thisOrderCredit.replace('-', ''));
                CustomerCredit = CreditMemoAmount - (parseFloat(nowGrandTotal));
                $("#txtStoreCreditAmount").val('-' + parseFloat(CustomerCredit).toFixed(2));
                $("#txtPaybleAmount").val('0.00');
            } else {
                payableAmount = parseFloat(nowGrandTotal) - parseFloat(CreditMemoAmount);
                $("#txtStoreCreditAmount").val('0.00')
                $("#txtPaybleAmount").val(payableAmount.toFixed(2));
            }
        }
    }

    var Payable = parseFloat($("#txtPaybleAmount").val());
    var AmtPaid = parseFloat($("#txtAmtPaid").val());
    $('#txtAmtDue').val((parseFloat(Payable) - parseFloat(AmtPaid)).toFixed(2));
}
function saveDelUpdates() {
    var Product = [];
    if (($("input[name='rblPaymentReceive']:checked").length) < 1) {
        $('.ReqCheck').each(function () {
            $(this).addClass('border-warning');
        })
        toastr.error('Please Choose option Payment Received.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    else {
        $('.ReqCheck').removeClass('border-warning');
    }
    var chkmin = false;
    $("#tblProductDetail tbody tr").each(function () {
        var oldQty = 0;
        if ($(this).find('.ProId').find('span').attr('IsExchange') == '0' && $(this).find('.ProId span').attr('isFreeItem') == '0' && parseInt($(this).find(".QtyShip input").val()) > 0) {
            if (parseFloat($(this).find('.UnitPrice input').val()) < $(this).find('.UnitPrice input').attr('MinPrice')) {
                chkmin = true;
                $(this).find('.UnitPrice input').addClass('border-red')
            }
        }
        if ($(this).find('.Status').text() == '2') {
            oldQty = Number($(this).find('.QtyShip input').attr('value'));
            oldQty = oldQty * parseFloat($(this).find('.UnitType').find('span').attr('QtyPerUnit'));
        }
        Product.push({
            ProductAutoId: $(this).find('.ProName').find('span').attr('ProductAutoId'),
            IsExchange: $(this).find('.ProId').find('span').attr('IsExchange'),
            UnitAutoId: $(this).find('.UnitType').find('span').attr('UnitAutoId'),
            QtyPerUnit: $(this).find('.UnitType').find('span').attr('QtyPerUnit'),
            Barcode: $(this).find('.Barcode').text(),
            QtyShip: $(this).find('.QtyShip input').val(),
            QtyDel: $(this).find('.QtyDel').text(),
            TotalPieces: $(this).find('.TtlPcs').text(),
            UnitPrice: $(this).find('.UnitPrice input').val(),
            SRP: $(this).find('.SRP').text(),
            GP: $(this).find('.GP').text(),
            Tax: $(this).find('.TaxRate span').attr('TaxRate'),
            NetPrice: $(this).find('.NetPrice').text(),
            FreshReturnQty: $(this).find('.FreshReturn input').val(),
            FreshReturnUnitAutoId: $(this).find('.FreshReturn select').val(),
            DamageReturnQty: $(this).find('.DemageReturn input').val(),
            DamageReturnUnitAutoId: $(this).find('.DemageReturn select').val(),
            MissingItemQty: $(this).find('.MissingItem  input').val(),
            MissingItemUnitAutoId: $(this).find('.MissingItem select').val(),
            isFreeItem: $(this).find('.ProId span').attr('isFreeItem'),
            MLQty: $(this).find('.TaxRate span').attr('MLQty'),
            Status: $(this).find('.Status').text(),
            Del_MinPrice: $(this).find('.Del_MinPrice').text(),
            Del_CostPrice: $(this).find('.Del_CostPrice').text(),
            Del_BasePrice: $(this).find('.Del_BasePrice').text(),
            AddOnQty: oldQty
        });
    });
    var orderData = {
        OrderAutoId: $("#txtHOrderAutoId").val(),
        TotalAmount: $("#txtTotalAmount").val(),
        OverallDisc: $("#txtOverallDisc").val(),
        OverallDiscAmt: $("#txtDiscAmt").val(),
        ShippingCharges: $("#txtShipping").val(),
        TotalTax: $("#txtTotalTax").val(),
        GrandTotal: $("#txtGrandTotal").val(),
        PaymentRecev: $("input[name='rblPaymentReceive']:checked").val() || 'none',
        RecevAmt: $("input[name='rblReceivedAmount']:checked").val() || 'none',
        Remarks: $("#txtAcctRemarks").val(),
        MLQty: parseFloat($("#txtMLQty").val()),
        MLTax: parseFloat($("#txtMLTax").val()),
        AdjustmentAmt: parseFloat($("#txtAdjustment").val()),
    };
    if (chkmin) {
        toastr.error('Unit Price should not be less than min Price.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    $.ajax({
        type: "POST",
        url: "/Account/WebAPI/InternalUpdateDelivery.asmx/saveDelUpdates",
        data: JSON.stringify({ OrderValues: JSON.stringify(orderData), TableValues: JSON.stringify(Product) }),
        //data: "{'OrderValues':'" + JSON.stringify(orderData) + "', 'TableValues':'" + JSON.stringify(Product) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            console.log(response);
            if (response.d == "SessionExpired") {
                location.href = "/";
            }
            else if (response.d == 'true') {
                swal({
                    title: "",
                    text: "Order closed successfully.",
                    icon: "success",
                    allowOutsideClick: false,
                    closeOnClickOutside: false,
                    button: "OK",
                }).then(function () {
                    location.href = '/Account/Acccount_viewOrder.aspx?OrderAutoId=' + ($("#txtHOrderAutoId").val());
                })
            } else { swal("", response.d, "error"); }
        },
        failure: function (result) {
            swal("Error!", JSON.parse(result.responseText).d, "error");
        },
        error: function (result) {
            swal("Error!", JSON.parse(result.responseText).d, "error");
        }
    });
}
function updatedeleverOrder() {
    var Product = [];
    var chkmin = false;
    $("#tblProductDetail tbody tr").each(function () {
        var oldQty = 0;
        if ($(this).find('.ProId').find('span').attr('IsExchange') == '0' && $(this).find('.ProId span').attr('isFreeItem') == '0' && parseInt($(this).find(".QtyShip input").val()) > 0) {
            if (parseFloat($(this).find('.UnitPrice input').val()) < $(this).find('.UnitPrice input').attr('MinPrice')) {
                chkmin = true;
                $(this).find('.UnitPrice input').addClass('border-red')
            }
        }

        if ($(this).find('.Status').text() == '2') {
            oldQty = Number($(this).find('.QtyShip input').attr('value'));
            oldQty = oldQty * parseFloat($(this).find('.UnitType').find('span').attr('QtyPerUnit'));
        }

        Product.push({
            ProductAutoId: $(this).find('.ProName').find('span').attr('ProductAutoId'),
            IsExchange: $(this).find('.ProId').find('span').attr('IsExchange'),
            UnitAutoId: $(this).find('.UnitType').find('span').attr('UnitAutoId'),
            QtyPerUnit: $(this).find('.UnitType').find('span').attr('QtyPerUnit'),
            Barcode: $(this).find('.Barcode').text(),
            QtyShip: $(this).find('.QtyShip input').val(),
            QtyDel: $(this).find('.QtyDel').text(),
            TotalPieces: $(this).find('.TtlPcs').text(),
            UnitPrice: $(this).find('.UnitPrice input').val(),
            SRP: $(this).find('.SRP').text(),
            GP: $(this).find('.GP').text(),
            Tax: $(this).find('.TaxRate span').attr('TaxRate'),
            NetPrice: $(this).find('.NetPrice').text(),
            FreshReturnQty: $(this).find('.FreshReturn input').val(),
            FreshReturnUnitAutoId: $(this).find('.FreshReturn select').val(),
            DamageReturnQty: $(this).find('.DemageReturn input').val(),
            DamageReturnUnitAutoId: $(this).find('.DemageReturn select').val(),
            MissingItemQty: $(this).find('.MissingItem  input').val(),
            MissingItemUnitAutoId: $(this).find('.MissingItem select').val(),
            isFreeItem: $(this).find('.ProId span').attr('isFreeItem'),
            MLQty: $(this).find('.TaxRate span').attr('MLQty'),
            Status: $(this).find('.Status').text(),
            Del_MinPrice: $(this).find('.Del_MinPrice').text(),
            Del_CostPrice: $(this).find('.Del_CostPrice').text(),
            Del_BasePrice: $(this).find('.Del_BasePrice').text(),
            AddOnQty: oldQty
        });
    });

    if (chkmin) {
        toastr.error('Unit Price should not be less than min Price.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    var orderData = {
        OrderAutoId: $("#txtHOrderAutoId").val(),
        TotalAmount: $("#txtTotalAmount").val(),
        OverallDisc: $("#txtOverallDisc").val(),
        OverallDiscAmt: $("#txtDiscAmt").val(),
        ShippingCharges: $("#txtShipping").val(),
        TotalTax: $("#txtTotalTax").val(),
        GrandTotal: $("#txtGrandTotal").val(),
        PaymentRecev: $("input[name='rblPaymentReceive']:checked").val() || 'none',
        RecevAmt: $("input[name='rblReceivedAmount']:checked").val() || 'none',
        MLQty: parseFloat($("#txtMLQty").val()),
        MLTax: parseFloat($("#txtMLTax").val()),
        AdjustmentAmt: parseFloat($("#txtAdjustment").val()),
        Remarks: $("#txtAcctRemarks").val()
    };

    $.ajax({
        type: "POST",
        url: "/Account/WebAPI/InternalUpdateDelivery.asmx/updatedeleverOrder",
        //data: "{'OrderValues':'" + JSON.stringify(orderData) + "', 'TableValues':'" + JSON.stringify(Product) + "'}",
        data: JSON.stringify({ OrderValues: JSON.stringify(orderData), TableValues: JSON.stringify(Product) }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == "SessionExpired") {
                location.href = "/";
            }
            else if (response.d == 'true') {
                swal({
                    title: "",
                    text: "Order updated successfully.",
                    icon: "success",
                    button: "OK",
                }).then(function () {
                    $("#btnSave").hide();
                    location.href = '/Account/Acccount_viewOrder.aspx?OrderAutoId=' + ($("#txtHOrderAutoId").val());
                })
            } else (swal("Error!", 'Oops! Something went wrong.Please try later', "error"))
        },
        failure: function (result) {
            swal("Error!", JSON.parse(result.responseText).d, "error");
        },
        error: function (result) {
            swal("Error!", JSON.parse(result.responseText).d, "error");
        }
    });
}
function UpdatePayment() {
    var orderData = {
        OrderAutoId: $("#txtHOrderAutoId").val(),
        AmtValue: $("#txtPayableAmount").val() || '0.00',
        Remarks: $("#txtRemark").val()
    };

    $.ajax({
        type: "POST",
        url: "/Driver/WebAPI/updateDelivery.asmx/UpdatePayment",
        data: "{'OrderValues':'" + JSON.stringify(orderData) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == "SessionExpired") {
                location.href = "/";
            }
            else {
                $("#alertSDeliver").show().find("span").html("<strong>Amount paid saved successfully!");
                setTimeout(function () {
                    $("#alertSDeliver").hide();
                }, 3000);
                $("#txtRemark").val('');
                $("#txtPayableAmount").val('');
                getOrderData(getQueryString('OrderAutoId'));
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function Paymentthrough() {
    if ($("#ddlPaymentThru").val() == 1) {
        $("#txtTransactionNo").prop('disabled', false);
    }
    else {
        $("#txtTransactionNo").prop('disabled', true);
    }
}
function saveDelStatus() {
    if ($("input[name='rblDeliver']").is(":checked")) {
        if ($("input[name='rblDeliver']").prop('checked') == true) {
            if ($("#ddlCommentType").val() == 0) {
                $("#alertDDeliver").show().find("span").html("<strong>Comment Type Required</strong>");
                setTimeout(function () {
                    $("#alertDDeliver").hide();
                }, 2000);
                return;
            }
        }
        var DelStatus = {
            OrderAutoId: $("#txtHOrderAutoId").val(),
            Delivered: $("input[name='rblDeliver']:checked").val(),
            Remarks: $("#txtRemarks").val(),
            CommentType: $("#ddlCommentType").val(),
            Comment: $("#txtComment").val()
        };

        $.ajax({
            type: "POST",
            url: "/Driver/WebAPI/updateDelivery.asmx/saveDelStatus",
            data: "{'DelStatus':'" + JSON.stringify(DelStatus) + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d == "SessionExpired") {
                    location.href = "/";
                }
                else {
                    swal({
                        title: "",
                        text: "Information saved successfully.",
                        icon: "success",
                        button: "OK",
                    }).then(function () {
                        $("#btnSaveDelStatus").hide();
                        $("#btnUpdate").show();
                        getOrderData($("#txtHOrderAutoId").val());
                    })
                }
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    } else {
        toastr.error('Delivery Option cannot be left blank.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
function GenInvoice() {
    window.open("/Manager/OrderPrintCC.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
}
function InvoiceOption() {
    $("#modalInvoiceOptions").modal('toggle');
}
function ShowInvoice() {
    if ($("input[name='InvOption']:checked").val() == '11') {
        GenInvoice();
    }
}
function PayNowDelivery() {
    location.href = '/Sales/ViewCustomerDetails.aspx?PageId=' + $("#hiddenCustAutoId").val() + '&Type=1';
}
function PickedOrder() {
    $('#PopBarCodeforPickBox').modal('show');
}
function closePacked() {
    $('#PopBarCodeforPickBox').modal('hide');
}
var NoofBoxRead = 0;
function readBoxBarcode() {
    var checkbarcode = true;
    var count = 0;
    $("#tblBarcode tbody tr").each(function () {
        count++;
    });
    if (count < $("#txtPackedBoxes").val()) {

        for (var i = 1; i <= $("#txtPackedBoxes").val(); i++) {
            if ($("#txtReadBorCode").val().toUpperCase() == ($("#txtOrderId").val().toUpperCase() + '/' + i)) {
                $("#tblBarcode tbody tr").each(function (index, item) {
                    if (($("#txtOrderId").val() + '/' + i) == $(item).find('.Barcode').text()) {
                        checkbarcode = false;
                    }
                });
                if (checkbarcode == true) {
                    NoofBoxRead = (Number(NoofBoxRead) + 1);
                    var PackedBoxes = $("#txtPackedBoxes").val();
                    var DuePackedBox = (Number(PackedBoxes) - Number(NoofBoxRead))

                    $("#baxbarmsg").css('color', 'Green');
                    $("#txtReadBorCode").val('');
                    $("#txtReadBorCode").focus();
                    if (Number(DuePackedBox) == 0) {
                        $("#txtReadBorCode").attr('disabled', true);
                        $("#baxbarmsg").html('All boxes picked.');
                        $("#btnPick1").show();
                        var row = $("#tblBarcode thead tr").clone(true);
                        $(".SRNO", row).html(NoofBoxRead);
                        $(".Barcode", row).text($("#txtOrderId").val() + '/' + i);
                        $("#tblBarcode tbody").append(row);
                        i = Number($("#txtPackedBoxes").val() + 1);

                    } else {
                        $("#baxbarmsg").html(DuePackedBox + ' Out Of ' + $("#txtPackedBoxes").val() + ' remain');
                        //$("#baxbarmsg").html('you still need ' + DuePackedBox + ' box left to pick up');
                        var row = $("#tblBarcode thead tr").clone(true);

                        $(".SRNO", row).html(NoofBoxRead);
                        $(".Barcode", row).text($("#txtOrderId").val() + '/' + i);
                        $("#tblBarcode tbody").append(row);
                        i = Number($("#txtPackedBoxes").val() + 1);

                    }
                }
                else {
                    $("#baxbarmsg").html('Bar Code already scan');
                    $("#btnPick1").hide();
                    $("#baxbarmsg").css('color', 'Red');
                    i = Number($("#txtPackedBoxes").val() + 1);
                }

            } else {
                $("#baxbarmsg").html('Invalid Bar Code');
                $("#btnPick1").hide();
                $("#baxbarmsg").css('color', 'Red');
            }
        }
    }
    else {
        $("#txtReadBorCode").attr('disabled', true);
        $("#baxbarmsg").html('All boxes picked.');
        $("#btnPick1").show();
    }

}
function OrderPicked() {
    $('#PopBarCodeforPickBox').modal('hide');
    $('#btnPickedOrder').hide();
    $('#btnSaveDelStatus').show();
}
function printOrder_CustCopy() {
    if ($('#HDDomain').val() == 'psmpa') {
        $("#divPSMPADefault").show();
        $("#PSMPADefault").prop('checked', true);
    }
    else if ($('#HDDomain').val() == 'psmwpa') {
        $("#divPSMWPADefault").show();
        $("#chkdueamount").prop('checked', true);
    }
    else if ($('#HDDomain').val() == 'psmnpa') {
        $("#divPSMNPADefault").show();
        $("#divPSMPADefault").hide();
        $("#rPSMNPADefault").prop('checked', true);
    }
    else {
        $("#chkdueamount").prop('checked', true);
        $("#PSMPADefault").closest('.row').hide();
    }
    $('#PopPrintTemplate').modal('show');
}
function PrintOrder() {
    $('#PopPrintTemplate').modal('hide');
    if ($("#chkdueamount").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF1.html?OrderAutoId=" + $("#txtHOrderAutoId").val() + "", "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    } else if ($("#chkdefault").prop('checked') == true) {
        window.open("/Manager/OrderPrintCC.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#Template1").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF2.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#Template2").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF3.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    } else if ($("#PSMPADefault").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF4.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#PSMWPADefault").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF6.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#PackingSlip").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF5.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    } else if ($("#WithoutCategorybreakdown").prop('checked') == true) {
        window.open("/Manager/WithoutCategorybreakdown.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#rPSMNPADefault").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF4.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    } else if ($("#Templ1").prop('checked') == true) {
        window.open("/Manager/PrintOrderItem.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    } else if ($("#Templ2").prop('checked') == true) {
        window.open("/Manager/PrintOrderItemTemplate2.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#PSMWPANDefault").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCFPN.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
}
function PopSecurityVoid() {
    $('#btnClose').attr('onclick', 'closePop(1)');
    $('#SecurityEnabledVoid').modal('show');
    $("#txtSecurityVoid").val('');
    $("#txtSecurityVoid").focus();
}
function VoidOrderByRemark() {

    $("#txtRemark").removeClass('border-warning');
    if ($("#txtRemark").val().trim() != '') {
        swal({
            title: "Are you sure?",
            text: "You want to cancel this Order.",
            icon: "warning",
            showCancelButton: true,
            buttons: {
                cancel: {
                    text: "No, cancel!",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: true,
                },
                confirm: {
                    text: "Yes, cancel it!",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
                }
            }
        }).then(function (isConfirm) {
            if (isConfirm) {
                CancelOrder();
            }
            else {
                swal("", "Your order has not been cancelled.", "warning");
            }
        })
    }
    else {
        $("#txtRemark").addClass('border-warning');
        toastr.error('Remark is mandatory .', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
function CancelOrder() {
    if ($("#txtRemark").val() == "") {
        swal("Error", "Remark required.", "error");
        return;
    }
    $.ajax({
        type: "Post",
        url: "/account/WebAPI/WAccount_Order.asmx/CancelOrder",
        data: "{'CancelRemark':'" + $("#txtRemark").val() + "','OrderAutoId':'" + $("#txtHOrderAutoId").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "SessionExpired") {
                if (response.d == 'true') {
                    $("#VoidRemarkPopup").modal('hide');
                    swal("", "Your order has been cancelled successfully.", "success").then(function () {
                        location.href = '/Account/Acccount_viewOrder.aspx?OrderAutoId=' + ($("#txtHOrderAutoId").val())
                    });
                } else {
                    swal("Error", "Order can't cancel.", "error");
                }
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function clickonSecurityVoid() {
    if ($("#txtSecurityVoid").val() == "") {
        toastr.error('Security Key Required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $("#txtSecurityVoid").addClass('border-warning');
        return;
    }
    $.ajax({
        type: "Post",
        url: "/account/WebAPI/WAccount_Order.asmx/clickonSecurityVoid",
        data: "{'CheckSecurity':'" + $("#txtSecurityVoid").val() + "','OrderAutoId':'" + $("#txtHOrderAutoId").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "SessionExpired") {
                if (response.d == 'true') {
                    $("#VoidRemarkPopup").modal('show');
                    $('#SecurityEnabledVoid').modal('hide');
                } else {
                    toastr.error('Access denied.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                }
            } else {
                location.href = '/';
            }
        },
        error: function (result) {
            swal("Error!", JSON.parse(result.responseText).d, "error");
        },
        failure: function (result) {
            swal("Error!", JSON.parse(result.responseText).d, "error");
        }
    });
}
function closePop(i) {
    if (i == 1) {
        location.href = '/Account/Acccount_viewOrder.aspx?OrderAutoId=' + $("#txtHOrderAutoId").val();
    } else {
        $('#SecurityEnvalid').modal('hide');
    }
}
function focusonBarcode() {
    $("#msgMassage").modal('hide');
    $("#txtBarcode").focus();

}
var MLTaxType = 0, WeightOzTex = 0;
var price, taxRate, SRP, minPrice, CustomPrice, GP; CostPrice = 0.0;
var BUnitAutoId = 0, validcheck = 0, productStock = 0;
function bindProduct() {
    $.ajax({
        type: "POST",
        url: "/Account/WebAPI/InternalUpdateDelivery.asmx/bindProduct",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            if (response.d != "Session Expired") {
                //var xmldoc = $.parseXML(response.d);
                //var product = $(xmldoc).find("Table"); 
                //$("#ddlProduct option:not(:first)").remove();
                var ProductList = JSON.parse(response.d);
                $.each(ProductList, function (i, item) {
                    $("#ddlProduct").append("<option MLQty='" + item.MLQty + "' WeightOz='" + item.WOz + "' productid='" + item.PID + "'  value='" + item.AutoId + "'>" + item.PID + '--' + item.PName + "</option>");
                });
                $("#ddlProduct").select2();
            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(result.d);
        },
        error: function (result) {
            console.log(result.d);
        }
    });
}
function ChangeUnit() {
    $('#chkExchange').prop('checked', false);
    $('#chkIsTaxable').prop('checked', false);
    $('#chkFreeItem').prop('checked', false);
    if ($("#ddlUnitType option:selected").attr('eligibleforfree') == 1) {
        $('#chkFreeItem').attr('disabled', false);
    } else {
        $('#chkFreeItem').attr('disabled', true);
    }
    $("#alertStockQty").hide();
    var data = {
        ProductAutoId: $("#ddlProduct").val(),
        UnitAutoId: $("#ddlUnitType").val(),
        CustAutoId: $("#hiddenCustAutoId").val()
    };

    if (data.UnitAutoId != 0) {
        $.ajax({
            type: "POST",
            url: "/Account/WebAPI/InternalUpdateDelivery.asmx/selectQtyPrice",
            data: "{'dataValues':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d != "Session Expired") {
                    var xmldoc = $.parseXML(response.d);
                    var stockAndPrice = $(xmldoc).find("Table");
                    price = stockAndPrice.find("Price").text();
                    minPrice = stockAndPrice.find("MinPrice").text();
                    CustomPrice = stockAndPrice.find("CustomPrice").text();
                    taxRate = stockAndPrice.find("TaxRate").text();
                    SRP = stockAndPrice.find("SRP").text();
                    GP = stockAndPrice.find("GP").text();
                    MLQtyRate = parseFloat(stockAndPrice.find("MLQty").text()).toFixed(2)
                    WeightOzTex = parseFloat(stockAndPrice.find("WeightOz").text()).toFixed(2)
                    CostPrice = parseFloat(stockAndPrice.find("CostPrice").text()).toFixed(2)

                    if ($("#ddlUnitType").val() != 0) {
                        $("#alertStockQty").text('');
                        if (Number($("#ddlUnitType").val()) == 1) {
                            $("#alertStockQty").append(" [ Stock : " + $(stockAndPrice).find("Stock").text() + " case ] ," +
                                " [ Base Price : $" + price + " ]");
                        } else if (Number($("#ddlUnitType").val()) == 2) {
                            $("#alertStockQty").append(" [ Stock : " + $(stockAndPrice).find("Stock").text() + " box ] ," +
                                " [ Base Price : $" + price + " ]");
                        } else {
                            $("#alertStockQty").append(" [ Stock : " + $(stockAndPrice).find("Stock").text() + " pcs ] ," +
                                " [ Base Price : $" + price + " ]");
                        }
                        $("#alertStockQty").show();
                        productStock = Number($(stockAndPrice).find("Stock").text());
                        validcheck = 0;

                    } else {
                        $("#alertStockQty").hide();
                    }
                } else {
                    location.href = '/';
                }
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
};
function bindUnitType() {
    $("#alertStockQty").hide();
    if ($("#ddlProduct option:selected").attr('eligibleforfree') == 1) {
        $('#chkFreeItem').attr('disabled', false);
    } else {
        $('#chkFreeItem').attr('disabled', true);
    }
    if ($("#ddlProduct").val() !== '0') {
        $.ajax({
            type: "POST",
            url: "/Account/WebAPI/InternalUpdateDelivery.asmx/bindUnitType",
            data: "{'productAutoId':" + $("#ddlProduct").val() + "}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d != "Session Expired") {
                    var xmldoc = $.parseXML(response.d);
                    var unitType = $(xmldoc).find("Table");
                    var unitDefault = $(xmldoc).find("Table1");
                    var count = 0;
                    $("#ddlUnitType option:not(:first)").remove();
                    $.each(unitType, function () {
                        $("#ddlUnitType").append("<option unittext='" + $(this).find("UnitType").text() + "' EligibleforFree='" + $(this).find("EligibleforFree").text() + "' LatestBarcode='" + $(this).find("LatestBarcode").text() + "'  value='" + $(this).find("AutoId").text() + "' QtyPerUnit='" + $(this).find("Qty").text() + "' >" + $(this).find("UnitType").text() + " (" + $(this).find("Qty").text() + " pcs" + ")</option>");
                    });
                    if (unitDefault.length > 0) {
                        if (BUnitAutoId == 0) {
                            $('#ddlUnitType option').each(function () {
                                if (this.value == $(unitDefault).find('AutoId').text()) {
                                    $("#ddlUnitType").val($(unitDefault).find('AutoId').text()).change();
                                }
                            });
                        } else {
                            $('#ddlUnitType option').each(function () {
                                if (this.value == BUnitAutoId) {
                                    $("#ddlUnitType").val(BUnitAutoId).change();
                                }
                            });
                            BUnitAutoId = 0;
                        }
                    } else {
                        $("#ddlUnitType").val(0).change();
                    }
                } else {
                    location.href = '/';
                }

            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });

    } else {
        $('#chkFreeItem').removeAttr('disabled');
        $("#ddlUnitType option:not(:first)").remove();
    }

}
function readBarcode() {
    var Barcode = $("#txtBarcode").val();
    if (Barcode != "") {
        var chkIsTaxable = $('#chkIsTaxable').prop('checked');
        var chkExchange = $('#chkExchange').prop('checked');
        var chkFreeItem = $('#chkFreeItem').prop('checked');
        var IsExchange = 0, IsTaxable = 0, IsFreeItem = 0, chkcount = 0;

        if (chkIsTaxable == true) {
            IsTaxable = 1;
            chkcount = 1;
        }
        if (chkExchange == true) {
            IsExchange = 1;
            chkcount = chkcount + 1;
        }
        if (chkFreeItem == true) {
            IsFreeItem = 1;
            chkcount = chkcount + 1;
        }
        if (chkcount > 1) {
            toastr.error('Please check only one checkbox.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            $("#txtBarcode").val('');
            $("#txtBarcode").focus();
            return;
        }
        var data = {
            CustomerAutoId: $("#hiddenCustAutoId").val(),
            Barcode: Barcode,
            ReqQty: "1",
            IsTaxable: IsTaxable,
            IsExchange: IsExchange,
            IsFreeItem: IsFreeItem
        }
        $.ajax({
            type: "POST",
            url: "/Account/WebAPI/InternalUpdateDelivery.asmx/GetBarDetails",
            data: "{'dataValues':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                var xmldoc = $.parseXML(response.d);
                var product = $(xmldoc).find("Table");
                if (product.length > 0) {
                    var productAutoId = $(product).find('ProductAutoId').text();
                    var unitAutoId = $(product).find('UnitType').text();
                    var flag1 = false;                 
                    $("#tblProductDetail tbody tr").each(function () {
                        if ($(this).find(".ProName > span").attr("ProductAutoId") == productAutoId && $(this).find(".UnitType > span").attr("UnitAutoId") == unitAutoId && $(this).find(".ProId > span").attr("IsExchange") == IsExchange && $(this).find(".ProId > span").attr("IsFreeItem") == IsFreeItem) {
                            var Qtyreq = 1;
                            var reqQty = Number($(this).find(".QtyShip input").val()) + Number(Qtyreq);
                            $(this).find(".QtyShip input").val(reqQty);
                            flag1 = true;
                            $(this).find(".Status", row).text('2');
                            $(this).find(".FreshReturn input").attr('disabled', true);
                            $(this).find(".DemageReturn input").attr('disabled', true);
                            $(this).find(".MissingItem input").attr('disabled', true);
                            NewRowCal($(this).find(".QtyShip > input"));
                            $('#tblProductDetail tbody tr:first').before($(this));
                        }
                    });
                    if (!flag1) {
                        var row = $("#tblProductDetail thead tr").clone(true);
                        $(".ProId", row).html($(product).find('ProductId').text() + "<span IsExchange='" + IsExchange + "' isFreeItem='" + IsFreeItem + "' AutoId='" + $("#ddlProduct option:selected").val() + "' ></span>");
                        if (IsFreeItem == 1) {
                            $(".ProName", row).html("<span ProductAutoId='" + $(product).find('ProductAutoId').text() + "'>" + $(product).find('ProductName').text() + "</span>" + " <product class='badge badge badge-pill badge-success'>Free</product>");

                        } else if (IsExchange == 1) {
                            $(".ProName", row).html("<span ProductAutoId='" + $(product).find('ProductAutoId').text() + "'>" + $(product).find('ProductName').text() + "</span>" + " <product class='badge badge badge-pill badge-primary'>Exchange</product>");
                        }
                        else if (IsTaxable == 1) {
                            $(".ProName", row).html("<span ProductAutoId='" + $(product).find('ProductAutoId').text() + "'>" + $(product).find('ProductName').text() + "</span>" + " <product class='badge badge badge-pill badge-danger'>Taxable</product>");
                        } else {
                            $(".ProName", row).html("<span ProductAutoId='" + $(product).find('ProductAutoId').text() + "'>" + $(product).find('ProductName').text() + "</span>");
                        }
                        // umang


                        $(".UnitType", row).html("<span UnitAutoId='" + $(product).find('UnitType').text() + "' QtyPerUnit='" + $(product).find('UnitQty').text() + "'>" + $(product).find('UnitName').text() + "</span>");
                        $(".QtyShip", row).html('<input type="text" onkeypress="return isNumberKey(event)" class="form-control input-sm border-primary text-center"  onkeyup="return NewRowCal(this)"  value="1"/>');
                        $(".SRP", row).text($(product).find('SRP').text());
                        $(".GP", row).text($(product).find('GP').text());
                        $(".Del_MinPrice", row).text($(product).find('MinPrice').text());
                        $(".Del_CostPrice", row).text($(product).find('CostPrice').text());
                        $(".Del_BasePrice", row).text($(product).find('BasePrice').text());
                        $(".Barcode", row).html($(product).find('LatestBarcode').text());


                        if (IsTaxable == 1) {
                            $(".TaxRate", row).html('<span TaxRate="1" MLQty="' + $(product).find('MLQty').text() + '" WeightOz="' + $(product).find('WeightOz').text() + '"></span><Taxable class="la la-check-circle success"></Taxable>');
                        } else {
                            $(".TaxRate", row).html('<span TaxRate="0" MLQty="' + $(product).find('MLQty').text() + '" WeightOz="' + $(product).find('WeightOz').text() + '"></span>');
                        }
                        if (IsExchange == 1) {
                            $(".IsExchange", row).html('<span IsExchange="1" ></span><IsExchange class="la la-check-circle success"></IsExchange>');
                        } else {
                            $(".IsExchange", row).html('<span IsExchange="0"></span>');
                        }
                        var TotPiece = $(product).find('UnitQty').text();
                        $(".TtlPcs", row).text(TotPiece);
                        $(".QtyDel", row).text(TotPiece);
                        $(".FreshReturn", row).html("<div class='input-group'><div class='input-group-prepend'><input type='text' onchange='calculateReturnQty(this)' class='form-control input-sm border-primary text-center' style='width:50px;padding:0.25rem 0.25rem;margin-right: 2px;' value='0' onkeypress='return isNumberKey(event)' disabled/><select style='width:58px;padding:0' class='freshunit form-control input-sm border-primary' onchange='calculateReturnQty(this)' disabled><option value=" + $(product).find('UnitType').text() + " qty=" + $(product).find('UnitQty').text() + " selected='selected'>" + $(product).find('UnitName').text() + "<option/></select></div></div>");
                        $(".DemageReturn", row).html("<div class='input-group-prepend'><input type='text'  onchange='calculateReturnQty(this)' disabled class='form-control input-sm border-primary  text-center' style='width:50px;padding:0.25rem 0.25rem;margin-right: 2px;' value='0'  onkeypress='return isNumberKey(event)'/><select style='width:58px;padding:0' class='demageunit form-control input-sm border-primary' disabled  onchange='calculateReturnQty(this)'><option value=" + $(product).find('UnitType').text() + " qty=" + $(product).find('UnitQty').text() + " selected='selected'>" + $(product).find('UnitName').text() + "<option/></select></div>");
                        $(".MissingItem", row).html("<div class='input-group-prepend'><input type='text'  onchange='calculateReturnQty(this)' disabled class='form-control input-sm border-primary  text-center' style='width:50px;padding:0.25rem 0.25rem;margin-right: 2px;' value='0'  onkeypress='return isNumberKey(event)'/><select style='width:58px;padding:0' class='demageunit form-control input-sm border-primary' disabled  onchange='calculateReturnQty(this)'><option value=" + $(product).find('UnitType').text() + " qty=" + $(product).find('UnitQty').text() + " selected='selected'>" + $(product).find('UnitName').text() + "<option/></select></div>");
                        $(".action", row).html("<a href='javascript:;' id='deleterow' onclick='deleterow(this)'><span class='ft-x'></span></a>");
                        if ($('#chkExchange').prop('checked') == false && $('#chkFreeItem').prop('checked') == false) {
                            $(".UnitPrice", row).html("<input type='text' maxlength='6' onchange='calculateReturnQty(this)' class='form-control input-sm border-primary' style='text-align:right;' value='" + $(product).find('CostPrice').text() + "' DefaultPrice='" + $(product).find('CostPrice').text() + "' MinPrice='" + $(product).find('CostPrice').text() + "' onkeypress='return isNumberDecimalKey(event,this)'/>");
                            $(".PerPrice", row).text(parseFloat($(product).find('CostPrice').text() / TotPiece).toFixed(2));
                            $(".NetPrice", row).text(parseFloat($(product).find('CostPrice').text()).toFixed(2));
                        } else {
                            $(".UnitPrice", row).html("<input type='text' maxlength='6' onkeypress='return isNumberDecimalKey(event,this)' onchange='calculateReturnQty(this)' class='form-control input-sm border-primary' style='text-align:right;' value='0.00' minprice='0.00' BasePrice='0.00' disabled/>");
                            $(".PerPrice", row).text(parseFloat(0).toFixed(2));
                            $(".NetPrice", row).text(parseFloat(0).toFixed(2));
                        }
                        $(".Status", row).text('1');

                        if ($('#tblProductDetail tbody tr').length > 0) {
                            $('#tblProductDetail tbody tr:first').before(row);
                        }
                        else {
                            $('#tblProductDetail tbody').append(row);
                        }
                    }
                    toastr.success('Product added successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    $('#chkIsTaxable').prop('checked', false);
                    $('#chkExchange').prop('checked', false);
                    $('#chkFreeItem').prop('checked', false);
                    $("#txtBarcode").focus();
                    $("#ddlProduct").select2('val', '0');
                    $("#ddlUnitType").val(0);
                    $("#txtReqQty").val("1");
                    $("#emptyTable").hide();
                    $("#txtBarcode").val('');
                    $("#txtBarcode").focus();
                    calTotalAmount();
                    var msg = true
                } else {
                    msg = false
                    $("#yes_audio")[0].play();
                    swal({
                        title: "Error!",
                        text: "Barcode does not exists.",
                        icon: "error",
                        closeOnClickOutside: false
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $("#txtBarcode").val('');
                            $("#txtBarcode").focus();
                            $("#txtBarcode").attr('onchange', 'readBarcode()');
                        }
                    });
                }

            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    } else {
        $("#ddlProduct").val(0).change();
        $("#alertBarcodeCount").hide();
        $("#ddlProduct").attr('disabled', false);
        $("#ddlUnitType").attr('disabled', false);
    }
}
function invalidBarCode() {
    $("#txtBarcode").val('');
    $("#txtBarcode").focus();
}
function AddItemList() {

    var chkIsTaxable = $('#chkIsTaxable').prop('checked');
    var chkExchange = $('#chkExchange').prop('checked');
    var chkFreeItem = $('#chkFreeItem').prop('checked');
    var MLQty = $("#ddlProduct option:selected").attr('MLQty');
    var weightoz = $("#ddlProduct option:selected").attr('weightoz');
    var reqQty = parseInt($("#txtReqQty").val()) || 0;
    var IsExchange = 0, IsTaxable = 0, IsFreeItem = 0;
    var chkcount = 0; shwmsg = 0, rStatus = 0;
    if (reqQty > productStock) {
        swal("", "Stock not available.", "warning");
        $("#Stock")[0].play();
        return;
    }
    else if (reqQty == 0) {
        toastr.error('Please enter a valid quantity.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $("#txtReqQty").addClass('border-warning');
        return;
    }
    else {
        $("#txtReqQty").removeClass('border-warning');
    }
    if (chkIsTaxable == true) {
        IsTaxable = 1;
        chkcount = 1;
    }
    if (chkExchange == true) {
        IsExchange = 1;
        chkcount = chkcount + 1;
        MLQty = 0.00;
        weightoz = 0.00;
    }
    if (chkFreeItem == true) {
        IsFreeItem = 1;
        chkcount = chkcount + 1;
        MLQty = 0.00;
        weightoz = 0.00;
    }
    if (MLTaxType == "0") {
        MLQty = 0.00;
    }
    if (chkcount > 1) {
        toastr.error('Please check only one checkbox.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    var flag1 = false;
    var validatecheck = dynamicInputTypeSelect2('selectvalidate');
    if (!validatecheck) {
        dynamicALL('Areq');
    }
    else {
        validatecheck = dynamicALL('Areq');
    }
    if (validatecheck) {
        var productAutoId = $("#ddlProduct option:selected").val();
        var unitAutoId = $("#ddlUnitType option:selected").val();
        var product = $("#ddlProduct option:selected").text().split("--");
        $("#tblProductDetail tbody tr").each(function () {
            if ($(this).find(".ProName > span").attr("ProductAutoId") == productAutoId && $(this).find(".UnitType > span").attr("UnitAutoId") == unitAutoId && $(this).find(".ProId > span").attr("IsExchange") == IsExchange && $(this).find(".ProId > span").attr("IsFreeItem") == IsFreeItem) {
                var reqQty = Number($(this).find(".QtyShip input").val()) + Number($("#txtReqQty").val());
                $(this).find(".QtyShip input").val(reqQty);
                flag1 = true;
                $(this).find(".Status", row).text('2');
                $(this).find(".FreshReturn input").attr('disabled', true);
                $(this).find(".DemageReturn input").attr('disabled', true);
                $(this).find(".MissingItem input").attr('disabled', true);
                NewRowCal($(this).find(".QtyShip > input"));
                $('#tblProductDetail tbody tr:first').before($(this));
            }
            else if ($(this).find(".ProName span").attr('ProductAutoId') == productAutoId && $(this).find(".UnitType span").attr('UnitAutoId') != unitAutoId) {
                swal("", "You can't add different unit of added product.", "error");
                flag1 = true;
                shwmsg = 1;
            }
        });
        if (!flag1) {
            var row = $("#tblProductDetail thead tr").clone(true);

            $(".ProId", row).html($("#ddlProduct option:selected").attr("ProductId") + "<span IsExchange='" + IsExchange + "' isFreeItem='" + IsFreeItem + "' AutoId='" + $("#ddlProduct option:selected").val() + "' ></span>");
            if (IsFreeItem == 1) {
                $(".ProName", row).html("<span ProductAutoId='" + $("#ddlProduct option:selected").val() + "'>" + product[1]  + "</span>" + " <product class='badge badge badge-pill badge-success'>Free</product>");

            } else if (IsExchange == 1) {
                $(".ProName", row).html("<span ProductAutoId='" + $("#ddlProduct option:selected").val() + "'>" + product[1]  + "</span>" + " <product class='badge badge badge-pill badge-primary'>Exchange</product>");
            }
            else if (IsTaxable == 1) {
                $(".ProName", row).html("<span ProductAutoId='" + $("#ddlProduct option:selected").val() + "'>" + product[1]  + "</span>" + " <product class='badge badge badge-pill badge-danger'>Taxable</product>");
            } else {
                $(".ProName", row).html("<span ProductAutoId='" + $("#ddlProduct option:selected").val() + "'>" + product[1]  + "</span>");
            }
            // umang
            $(".UnitType", row).html("<span UnitAutoId='" + $("#ddlUnitType option:selected").val() + "' QtyPerUnit='" + $("#ddlUnitType option:selected").attr("QtyPerUnit") + "'>" + $("#ddlUnitType option:selected").text() + "</span>");
            $(".QtyShip", row).html('<input type="text" onkeypress="return isNumberKey(event)" class="form-control input-sm border-primary text-center"  onkeyup="return NewRowCal(this)"  value="' + $("#txtReqQty").val() + '"/>');
            $(".SRP", row).text(SRP);
            $(".GP", row).text(GP);
            $(".Del_MinPrice", row).text(minPrice);
            $(".Del_CostPrice", row).text(CostPrice);
            $(".Del_BasePrice", row).text(price);

            if (IsTaxable == 1) {
                $(".TaxRate", row).html('<span TaxRate="1" MLQty="' + MLQtyRate + '" WeightOz="' + WeightOzTex + '"></span><Taxable class="la la-check-circle success"></Taxable>');
            } else {
                $(".TaxRate", row).html('<span TaxRate="0" MLQty="' + MLQtyRate + '" WeightOz="' + WeightOzTex + '"></span>');
            }
            if (IsExchange == 1) {
                $(".IsExchange", row).html('<span IsExchange="1" ></span><IsExchange class="la la-check-circle success"></IsExchange>');
            } else {
                $(".IsExchange", row).html('<span IsExchange="0"></span>');
            }
            var TotPiece = $("#txtReqQty").val() * $("#ddlUnitType option:selected").attr("QtyPerUnit");
            $(".TtlPcs", row).text(TotPiece);
            $(".QtyDel", row).text(TotPiece);
            $(".Barcode", row).html($("#ddlUnitType option:selected").attr("LatestBarcode"));

            $(".FreshReturn", row).html("<div class='input-group'><div class='input-group-prepend'><input type='text' onchange='calculateReturnQty(this)' class='form-control input-sm border-primary text-center' style='width:50px;padding:0.25rem 0.25rem;margin-right: 2px;' value='0' onkeypress='return isNumberKey(event)' disabled/><select style='width:58px;padding:0' class='freshunit form-control input-sm border-primary' onchange='calculateReturnQty(this)' disabled><option value=" + $("#ddlUnitType option:selected").val() + " qty=" + $("#ddlUnitType option:selected").attr("QtyPerUnit") + " selected='selected'>" + $("#ddlUnitType option:selected").attr("unittext") + "<option/></select></div></div>");
            $(".DemageReturn", row).html("<div class='input-group-prepend'><input type='text'  onchange='calculateReturnQty(this)' disabled class='form-control input-sm border-primary  text-center' style='width:50px;padding:0.25rem 0.25rem;margin-right: 2px;' value='0'  onkeypress='return isNumberKey(event)'/><select style='width:58px;padding:0' class='demageunit form-control input-sm border-primary' disabled  onchange='calculateReturnQty(this)'><option value=" + $("#ddlUnitType option:selected").val() + " qty=" + $("#ddlUnitType option:selected").attr("QtyPerUnit") + " selected='selected'>" + $("#ddlUnitType option:selected").attr("unittext") + "<option/></select></div>");
            $(".MissingItem", row).html("<div class='input-group-prepend'><input type='text'  onchange='calculateReturnQty(this)' disabled class='form-control input-sm border-primary  text-center' style='width:50px;padding:0.25rem 0.25rem;margin-right: 2px;' value='0'  onkeypress='return isNumberKey(event)'/><select style='width:58px;padding:0' class='demageunit form-control input-sm border-primary' disabled  onchange='calculateReturnQty(this)'><option value=" + $("#ddlUnitType option:selected").val() + " qty=" + $("#ddlUnitType option:selected").attr("QtyPerUnit") + " selected='selected'>" + $("#ddlUnitType option:selected").attr("unittext") + "<option/></select></div>");
            $(".action", row).html("<a href='javascript:;' id='deleterow' onclick='deleterow(this)'><span class='ft-x'></span></a>");
            if ($('#chkExchange').prop('checked') == false && $('#chkFreeItem').prop('checked') == false) {
                $(".UnitPrice", row).html("<input type='text' maxlength='6' onchange='calculateReturnQty(this)' class='form-control input-sm border-primary' style='text-align:right;' value='" + CostPrice + "' DefaultPrice='" + CostPrice + "' MinPrice='" + CostPrice + "' onkeypress='return isNumberDecimalKey(event,this)'/>");
                $(".PerPrice", row).text(parseFloat(CostPrice / TotPiece).toFixed(2));
                $(".NetPrice", row).text(parseFloat($("#txtReqQty").val() * CostPrice).toFixed(2));
            } else {
                $(".UnitPrice", row).html("<input type='text' maxlength='6' onkeypress='return isNumberDecimalKey(event,this)' onchange='calculateReturnQty(this)' class='form-control input-sm border-primary' style='text-align:right;' value='0.00' minprice='0.00' BasePrice='0.00' disabled/>");
                $(".PerPrice", row).text(parseFloat(0).toFixed(2));
                $(".NetPrice", row).text(parseFloat(0).toFixed(2));
            }
            $(".Status", row).text('1');

            if ($('#tblProductDetail tbody tr').length > 0) {
                $('#tblProductDetail tbody tr:first').before(row);
            }
            else {
                $('#tblProductDetail tbody').append(row);
            }
        }
        if ($('#tblProductDetail tbody tr').length > 0) {
            $("#ddlCustomer").attr('disabled', true);
        }
        if (shwmsg == 0) {
            toastr.success('Product added successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        }
        $('#chkIsTaxable').prop('checked', false);
        $('#chkExchange').prop('checked', false);
        $('#chkFreeItem').prop('checked', false);
        $("#txtBarcode").focus();
        $("#ddlProduct").select2('val', '0');
        $("#ddlUnitType").val(0);
        $("#txtReqQty").val("1");
        $("#emptyTable").hide();
        calTotalAmount();
    }
}
function deleteItemrecord(e) {
    $(e).closest('tr').remove();
    swal("", "Product deleted successfully.", "success");
    calTotalAmount();
    if ($("#tblProductDetail tbody tr").length == 0) {
        $("#ddlCustomer").attr('disabled', false);
        $("#emptyTable").show();
    } else {
        $("#ddlCustomer").attr('disabled', true);
        $("#emptyTable").hide();
    }
}
function deleterow(e) {
    swal({
        title: "Are you sure?",
        text: "You want to delete Product!",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: false,
            },
            confirm: {
                text: "Yes, delete it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            deleteItemrecord(e);

        } else {
            swal("", "Your Product is safe.", "error");
        }
    })
}
function NewRowCal(e) {
    var row = $(e).closest("tr");
    var ProductAutoId = $(row).find('.ProName span').attr('productautoid');
    var UnitAutoId = $(row).find('.UnitType span').attr('unitautoid');
    var ReqQty = $(row).find('.QtyShip input').val();
    var UnitPrice = $(row).find('.UnitPrice input').val();
    var IsExchange = $(row).find('.IsExchange span').attr('IsExchange');
    var IsFreeItem = $(row).find('.ProId span').attr('IsFreeItem');
    var totalPcs = (Number($(e).val()) * Number($(row).find(".UnitType span").attr("QtyPerUnit")));
    var Cprice = Number($(row).find(".UnitPrice input").val());
    $(row).find(".TtlPcs").text(totalPcs);
    $(row).find(".QtyDel").text(totalPcs);
    var netPrice = (Cprice * Number($(row).find(".QtyShip input").val()));
    $(row).find(".NetPrice").text(netPrice.toFixed(2));
    calTotalAmount();
}
function BindTaxType(id) {

    $.ajax({
        type: "POST",
        url: "/Account/WebAPI/InternalUpdateDelivery.asmx/BindTaxType",
        data: "{'OrderAutoId':" + id + "}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var TaxTypeMaster = $(xmldoc).find("Table");
                if (TaxTypeMaster.length > 0) {
                    $("#ddlTaxType option").remove();
                    $.each(TaxTypeMaster, function () {
                        if (Number($("#hfCustomerType").val()) == 3) {
                            $("#ddlTaxType").append("<option TaxValue='0' value='" + $(this).find("AutoId").text() + "'></option>");
                        }
                        else {
                            $("#ddlTaxType").append("<option TaxValue='" + $(this).find("Value").text() + "' value='" + $(this).find("AutoId").text() + "'>" + $(this).find("TaxableType").text() + "-(" + $(this).find("Value").text() + ")</option>");
                        }
                    });
                }

            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function UpdateCostPrice() {
    $("#modalCostPriceRemark").modal('show');
}
function updatePriceConfirmation() {
    swal({
        title: "Are you sure?",
        text: "You want to update cost price",
        icon: "warning",
        showCancelButton: true,
        allowOutsideClick: false,
        closeOnClickOutside: false,
        buttons: {
            cancel: {
                text: "No, Cancel.",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: false,
            },
            confirm: {
                text: "Yes, Update it.",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (confirm) {
        if (confirm) {
            UpdatePrice();
        }
        else {
            swal("", "Process cancelled.", "error");
        }
    })
}
function UpdatePrice() {
    if ($("#txtCostPriceRemark").val() == "") {
        $("#txtCostPriceRemark").addClass('border-warning');
        toastr.error('Please fill remark.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
    else if ($('#txtCostPriceRemark').val().trim() == '') {
        $("#txtCostPriceRemark").addClass('border-warning');
        toastr.error('Please fill remark.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
    else {
        $("#txtCostPriceRemark").removeClass('border-warning');
        var orderData = {
            OrderAutoId: $("#txtHOrderAutoId").val(),
            Remarks: $("#txtCostPriceRemark").val()
        };
        $.ajax({
            type: "POST",
            url: "InternalUpdateDelivery.aspx/UpdateCostprice",
            data: "{'OrderValues':'" + JSON.stringify(orderData) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d == "true") {
                    swal("", "Cost Price updated successfully.", "success").then(function () {
                        location.reload();
                    });
                }
                else {
                    //toastr.error('Oops,Something went wrong.Please try later.', 'Error', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    toastr.error(response.d, 'Error', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

                }
            },
            error: function (result) {
                alert(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
}

function closemdlcosp() {
    $("#modalCostPriceRemark").modal('hide');
    $("#txtCostPriceRemark").val('');
    $("#txtCostPriceRemark").removeClass('border-warning');
}