﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="Acccount_viewOrder.aspx.cs" Inherits="Account_Acccount_viewOrder" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .input-group-addon {
            padding: 0 1px;
        }

        .input-group-addon {
            background-color: none !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">View Order</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Account</a>
                        </li>
                        <li class="breadcrumb-item">View Order</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12" id="Action">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a href="/Account/deliveredOrdersList.aspx" class="dropdown-item dropdowtxtSecurityVoidn-item" id="linktoOrderList"><b>Back to Order List</b></a>
                    <input type="hidden" runat="server" id="HDDomain" />
                </div>
            </div>
        </div>
    </div>


    <div class="content-body">
        <section id="panelOrderDetails">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-sm-12 col-md-3">
                                        <b>Order Information</b>
                                    </div>
                                    <div class="col-sm-12 col-md-9">

                                        <input type="hidden" id="hiddenEmpTypeVal" runat="server" />
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-3">
                                        <label class="control-label">Order No</label>
                                        <input type="hidden" id="txtHOrderAutoId" class="form-control input-sm" />
                                        <input type="text" id="txtOrderId" class="form-control border-primary input-sm" readonly="readonly" />
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label class="control-label">Order Date</label>
                                        <input type="text" id="txtOrderDate" class="form-control border-primary input-sm" readonly="readonly" />
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label class="control-label">Order Status</label>
                                        <input type="text" id="txtOrderStatus" class="form-control border-primary input-sm" readonly="readonly" />
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label class="control-label">Order Type</label>
                                        <input type="text" id="txtOrderType" class="form-control border-primary input-sm" readonly="readonly" />
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-3">
                                        <label class="control-label">Delivery Date</label>
                                        <input type="text" id="txtDeliveryDate" class="form-control border-primary input-sm" readonly="readonly" />
                                    </div>
                                    <div class="col-md-3 col-sm-2 form-group">
                                        <label class="control-label control-space">Sales Person</label>
                                        <input type="text" id="txtSalesPerson" class="form-control input-sm border-primary" readonly="readonly" />
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label class="control-label">Customer</label>
                                        <input type="text" id="txtCustomer" class="form-control border-primary input-sm" readonly="readonly" />
                                        <input type="hidden" id="hiddenCustAutoId" />
                                    </div>
                                    <div class="col-md-3 col-sm-2 form-group">
                                        <label class="control-label control-space">Customer Type</label>
                                        <input type="text" id="txtCustomerType" class="form-control input-sm border-primary" readonly="readonly" />
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-3">
                                        <label class="control-label">Terms</label>
                                        <input type="text" class="form-control border-primary input-sm" id="txtTerms" readonly="readonly" />
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label class="control-label">Shipping Type</label>
                                        <input type="text" class="form-control border-primary input-sm" id="txtShippingType" readonly="readonly" />
                                    </div>
                                    <div class="col-md-3 col-sm-4 form-group">
                                        <label class="control-label">Billing Address</label>
                                        <textarea class="form-control border-primary input-sm" id="txtBillAddress" readonly="readonly"> </textarea>

                                    </div>
                                    <div class="col-md-3 col-sm-4 form-group">
                                        <label class="control-label">Shipping Address</label>
                                        <textarea class="form-control border-primary input-sm" id="txtShipAddress" readonly="readonly"> </textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>



    <div class="content-body">
        <section id="CustDues">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title"><b>Payments History</b></h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse" id="custDuesBody">
                            <div class="card-body card-dashboard">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="table-responsive">
                                            <table id="tblCustDues" class="table table-striped table-bordered">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="orderNo text-center">Order No</td>
                                                        <td class="orderDate  text-center">Order Date</td>
                                                        <td class="OrderType  text-center">Order Type</td>
                                                        <td class="value price">Payabe Amount</td>
                                                        <td class="amtPaid price">Paid Amount</td>
                                                        <td class="amtDue price">Due Amount</td>
                                                        <td class="paya price" style="display: none;">Pay</td>
                                                        <td class="remarksa" style="display: none;">Remark</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td colspan="3" style="text-align: right;"><b>TOTAL</b></td>
                                                        <td id="sumOrderValue" style="text-align: right;"></td>
                                                        <td id="sumPaid" style="text-align: right;"></td>
                                                        <td id="sumDue" style="text-align: right;"></td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            <h5 class="well text-center" id="noDues" style="display: none">No Dues.</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer" id="btnPay_Dues1" style="display: none; padding: 5px 15px;">
                            <button type="button" class="btn btn-success btn-sm pull-right" onclick="payDueAmount()"><b>Pay</b></button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="content-body">
        <section id="panelOrderContent">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-header">
                                <h4 class="card-title"><b>Order Content</b></h4>
                            </div>
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="table-responsive">
                                            <table id="tblProductDetail" class="table table-striped table-bordered">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="ProId">ID</td>
                                                        <td class="ProName">Product</td>
                                                        <td class="UnitType text-center" style="width: 120px;">Unit</td>
                                                        <td class="Barcode">Barcode</td>
                                                        <td class="QtyShip  text-center">Shipped<br />
                                                            Qty
                                                        </td>
                                                        <td class="TtlPcs  text-center">Total
                                                            <br />
                                                            Pieces</td>
                                                        <td class="UnitPrice price">Unit
                                                            <br />
                                                            Price</td>
                                                        <td class="PerPrice price">Per
                                                            <br />
                                                            Piece Price</td>
                                                        <td class="FreshReturn  text-center">Fresh<br />
                                                            Return</td>
                                                        <td class="DemageReturn  text-center">Damage<br />
                                                            Return</td>
                                                        <td class="MissingItem  text-center">Missing<br />
                                                            Item</td>
                                                        <td class="QtyDel  text-center">Delivered
                                                            <br />
                                                            Qty</td>
                                                        <td class="SRP price" style="text-align: right;">SRP</td>
                                                        <td class="GP price" style="text-align: right;">GP (%)</td>
                                                        <td class="TaxRate  text-center" style="display: none">Tax</td>
                                                        <td class="IsExchange  text-center" style="display: none">Exchange</td>
                                                        <td class="ItemTotal price">Item<br />
                                                            Total</td>
                                                        <td class="Discount price">Discount (%)</td>
                                                        <td class="NetPrice price">Net
                                                            <br />
                                                            Price</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="content-body">
        <section id="drag-area7">
            <div class="row">
                <div class="col-md-6">
                    <div class="card" id="CusCreditMemo" style="display: none">
                        <div class="card-content collapse show">
                            <div class="card-header">
                                <b>Credit Memo</b>
                            </div>
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="table-responsive" id="dCuCreditMemo">
                                            <table id="tblCreditMemoList" class="table table-striped table-bordered">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="SRNO text-center">SRNO</td>
                                                        <td class="Action  text-center rowspan" style="display: none">Action</td>
                                                        <td class="CreditNo  text-center">Credit No.</td>
                                                        <td class="CreditDate  text-center">Credit Date</td>
                                                        <td class="ReturnValue" style="text-align: right;">Total Value</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot style="font-weight: 700; background: oldlace;">
                                                    <tr style="text-align: right;">
                                                        <td style="display: none" class="rowspan"><b></b></td>
                                                        <td colspan="3"><b>TOTAL</b></td>
                                                        <td id="TotalDue"></td>

                                                    </tr>
                                                </tfoot>
                                            </table>
                                            <h5 class="well text-center" id="H1" style="display: none">No Dues.</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card" id="OrderRemarks">
                        <div class="card-header">
                            <h4 class="card-title"><b>Order Remark</b></h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="table-responsive">
                                            <table id="Table2" class="table table-striped table-bordered">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="SRNO text-center">SN</td>
                                                        <td class="EmployeeName">Employee Name</td>
                                                        <td class="EmployeeType text-center">Employee Type</td>
                                                        <td class="Remarks">Remark</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">
                                <h4 class="card-title"><b>Driver Details</b></h4>
                            </h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <div id="DrvDeliveryInfo">
                                    <div class="row form-group">
                                        <div class="col-md-6 col-sm-6">
                                            <label class="control-label">Delivered</label>
                                            <input type="text" class="form-control border-primary input-sm" id="txtDelivered" value="No" readonly="readonly" />
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <label class="control-label">Delivery Remark</label>
                                            <textarea class="form-control border-primary input-sm" rows="2" placeholder="" id="txtRemarks" disabled="disabled"></textarea>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 form-group">
                                            <center>
                                            <img id="imgSignInvoice" style="width: 150px;height: 150px;display:none;border:1px solid blue" src=""></img><br />
                                            <span id="spnSignedInvoice"></span>
                                            </center>
                                        </div>
                                        <div class="col-md-4 form-group">
                                            <center>
                                            <img id="imgDeliveryProof" style="width: 150px;height: 150px;display:none;border:1px solid blue" src=""></img> <br />
                                            <span id="spnDeliveryProof"></span>
                                            </center>
                                        </div>
                                        <div class="col-md-4 form-group">
                                            <center>
                                            <img id="imgOther" style="width: 150px;height: 150px;display:none;border:1px solid blue" src=""></img><br />
                                            <span id="spnOther"></span>
                                            </center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card" id="AccountDeliveryInfo">
                        <div class="card-header">
                            <h4 class="card-title"><b>Account Details</b></h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-6">
                                        <!------------------------------- class NotToShow to hide fields for driver --------------------------------------->
                                        <label class="control-label">Payment Received</label>
                                        <input type="text" class="form-control border-primary input-sm" style="text-transform: capitalize" id="txtRecievedPayment" value="No" readonly="readonly" />
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <label class="control-label">Account Remark</label>
                                        <textarea class="form-control border-primary input-sm" rows="3" placeholder="" id="txtAcctRemarks" disabled="disabled"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-6 text-right">
                                        <label class="label-control">Total Amount</label>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="padding: 0rem 1rem;">
                                                        <span class=" ">$</span>
                                                    </span>
                                                </div>
                                                <input type="text" class="form-control border-primary input-sm" style="text-align: right" id="txtTotalAmount" value="0.00" readonly="readonly" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-6 text-right">
                                        <label class="form-groupt">Overall Discount</label>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div style="display: flex;">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" class="form-control border-primary input-sm" style="text-align: right;" id="txtOverallDisc" value="0.00" readonly="readonly" />
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                                            <span class="">%</span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            &nbsp;&nbsp;                                         
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;">
                                                                <span class=" ">$</span>
                                                            </span>
                                                        </div>
                                                        <input type="text" class="form-control border-primary input-sm" style="text-align: right;" id="txtDiscAmt" value="0.00" readonly="readonly" />
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-6 text-right">
                                        <label class="form-group">Shipping Charges</label>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="padding: 0rem 1rem;">
                                                        <span class=" ">$</span>
                                                    </span>
                                                </div>
                                                <input type="text" class="form-control border-primary input-sm" style="text-align: right" id="txtShipping" value="0.00" readonly="readonly" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-6 text-right">
                                        <label class="form-group">Tax Type</label>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="padding: 0rem 1rem;">
                                                        <span class=" ">$</span>
                                                    </span>
                                                </div>
                                                <input type="text" class="form-control border-primary input-sm" style="text-align: right" id="txtTaxType" value="0.00" readonly="readonly" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-6 text-right">
                                        <label class="form-group">Total Tax</label>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="padding: 0rem 1rem;">
                                                        <span class=" ">$</span>
                                                    </span>
                                                </div>
                                                <input type="text" class="form-control border-primary input-sm" style="text-align: right" id="txtTotalTax" value="0.00" readonly="readonly" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-6 text-right">
                                        <label class="form-group">ML Quantity</label>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="padding: 0rem 1rem;">
                                                        <span class=" ">$</span>
                                                    </span>
                                                </div>
                                                <input type="text" class="form-control border-primary input-sm" style="text-align: right" id="txtMLQty" value="0.00" readonly="readonly" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-6 text-right">
                                        <label class="form-group">ML Tax</label>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="padding: 0rem 1rem;">
                                                        <span class=" ">$</span>
                                                    </span>
                                                </div>
                                                <input type="text" class="form-control border-primary input-sm" style="text-align: right" id="txtMLTax" value="0.00" readonly="readonly" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-6 text-right">
                                        <label class="form-group">Weight Quantity</label>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="padding: 0rem 1rem;">
                                                        <span class=" ">$</span>
                                                    </span>
                                                </div>
                                                <input type="text" class="form-control border-primary input-sm" style="text-align: right" id="txtWeightQty" value="0.00" readonly="readonly" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-6 text-right">
                                        <label class="form-group">Weight Tax</label>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="padding: 0rem 1rem;">
                                                        <span class=" ">$</span>
                                                    </span>
                                                </div>
                                                <input type="text" class="form-control border-primary input-sm" style="text-align: right" id="txtWeightTax" value="0.00" readonly="readonly" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-6 text-right">
                                        <label class="form-group">Adjustment</label>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="padding: 0rem 1rem;">
                                                        <span class=" ">$</span>
                                                    </span>
                                                </div>
                                                <input type="text" class="form-control border-primary input-sm" style="text-align: right" id="txtAdjustment" value="0.00" readonly="readonly" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-6 text-right">
                                        <label class="form-group"><b>Grand Total</b></label>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="padding: 0rem 1rem;">
                                                        <span class=" ">$</span>
                                                    </span>
                                                </div>
                                                <input type="text" class="form-control border-primary input-sm inputBold" style="text-align: right" readonly="readonly" id="txtGrandTotal" value="0.00" />
                                                <input type="hidden" id="hiddenGrandTotal" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-6 text-right">
                                        <label class="form-group">Credit Memo Amount</label>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="padding: 0rem 1rem;">
                                                        <span class=" ">$</span>
                                                    </span>
                                                </div>
                                                <input type="text" class="form-control border-primary input-sm" style="text-align: right" id="txtDeductionAmount" value="0.00" readonly="readonly" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-6 text-right">
                                        <label class="form-group">Store Credit Amount<span style="display: none"> (max $<span id="CreditMemoAmount">0.00</span> Available)</span></label>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="padding: 0rem 1rem;">
                                                        <span class=" ">$</span>
                                                    </span>
                                                </div>
                                                <input type="text" class="form-control border-primary input-sm" style="text-align: right" readonly="true" id="txtStoreCreditAmount" onfocus="this.select()" value="0.00" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-6 text-right">
                                        <label class="form-group">Payable Amount</label>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="padding: 0rem 1rem;">
                                                        <span class=" ">$</span>
                                                    </span>
                                                </div>
                                                <input type="text" class="form-control border-primary input-sm" style="text-align: right" readonly="true" id="txtPaybleAmount" value="0.00" />
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-6 text-right">
                                        <label class="form-group"><b>Total Amount Paid</b></label>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="padding: 0rem 1rem;">
                                                        <span class=" ">$</span>
                                                    </span>
                                                </div>
                                                <input type="text" class="form-control border-primary input-sm inputBold" style="text-align: right" readonly="readonly" id="txtAmtPaid" value="0.00" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group" id="rowAmountDue">
                                    <div class="col-sm-12 col-md-6 text-right">
                                        <label class="form-group"><b>Amount Due</b></label>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="padding: 0rem 1rem;">
                                                        <span class=" ">$</span>
                                                    </span>
                                                </div>
                                                <input type="text" class="form-control border-primary input-sm inputBold" style="text-align: right" readonly="readonly" id="txtAmtDue" value="0.00" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" id="hfMLTaxStatus" />
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-6 text-right">
                                        <label class="form-group">No. of Packed Boxes</label>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <%--<div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="padding: 0rem 1rem;">
                                                        <span class=" ">No</span>
                                                    </span>
                                                </div>--%>
                                            <input type="text" class="form-control border-primary input-sm" id="txtPackedBoxes" runat="server" readonly="readonly" />
                                            <%-- </div>--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-12">
                                        <button type="button" id="btnPayNow" class="btn btn-primary buttonAnimation pull-right round box-shadow-1 btn-sm" onclick="PayNowDelivery()" style="display: none; margin-top: 5px;"><b>Pay Now</b></button>
                                        <button type="button" id="btnGenOrderCC" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" style="margin: 0 5px; display: none; margin-top: 5px;" onclick="printOrder_CustCopy()"><b>Generate Customer copy of order</b></button>
                                        <%
                                            if (Session["EmpTypeNo"] != null)
                                            {
                                                if (Session["EmpTypeNo"].ToString() == "1")
                                                {
                                        %>
                                        <button type="button" id="btnSave" class="btn btn-info buttonAnimation round pull-right  box-shadow-1  btn-sm" style="margin: 0 5px; margin-top: 5px;" onclick="saveDelUpdates()"><b>Edit</b></button>
                                        <button type="button" id="btnVoidOrder" class="btn btn-danger buttonAnimation pull-right  round box-shadow-1  btn-sm" style="margin: 0 5px; margin-top: 5px;" onclick="PopSecurityVoid()"><b>Void Order</b></button>

                                        <%
                                                }
                                            }
                                        %>

                                        <%
                                            if (Session["EmpTypeNo"] != null)
                                            {
                                                if (Session["EmpTypeNo"].ToString() == "1")
                                                {
                                        %>
                                        <button type="button" id="btnRemoveMlTax" class="btn btn-info buttonAnimation round box-shadow-1 btn-sm pull-right" onclick="RemoveMlTax()" style="margin-top: 5px;">Remove ML Tax</button>
                                        <%
                                                }
                                            }
                                        %>

                                        <button type="button" id="btnMigrate1" class="btn btn-info buttonAnimation round box-shadow-1 btn-sm" onclick="CheckSecurityMigrate()" style="margin-top: 5px; display: none">Migrate</button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div id="PopPrintTemplate" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <h4 class="modal-title">Select Invoice Template</h4>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="container">
                                <div class="row form-group">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-10">
                                        <input type="radio" checked name="Template" id="chkdefault" />
                                        PSM Default  - Invoice Only
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-10">
                                        <input type="radio" name="Template" id="chkdueamount" />
                                        PSM Default
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-10">
                                        <input type="radio" name="Template" id="PackingSlip" />
                                        Packing Slip
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-10">
                                        <input type="radio" name="Template" id="WithoutCategorybreakdown" />
                                        Packing Slip - Without Category Breakdown
                                    </div>
                                </div>
                                <div class="row form-group" id="divPSMPADefault" style="display: none">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-10">
                                        <input type="radio" name="Template" id="PSMPADefault" />
                                        PSM PA Default
                                    </div>
                                </div>
                                <div class="row form-group" id="divPSMWPADefault" style="display: none">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-10">
                                        <input type="radio" name="Template" id="PSMWPADefault" />
                                        PSM WPA-PA
                                    </div>
                                </div>
                                <div class="row form-group" id="PSMNPADefault" style="display: none">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-10">
                                        <input type="radio" name="Template" id="rPSMNPADefault" />
                                        PSM NPA Default
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-10">
                                        <input type="radio" name="Template" id="Template1" />
                                        7 Eleven (Type 1)
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-10">
                                        <input type="radio" name="Template" id="Template2" />
                                        7 Eleven (Type 2)
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-10">
                                        <input type="radio" name="Template" id="Templ1" />
                                        Template 1
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-10">
                                        <input type="radio" name="Template" id="Templ2" />
                                        Template 2
                                    </div>
                                </div>
                                 <div class="clearfix"></div>
                                <div class="row" id="divPSMWPANDefault">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-10">
                                        <input type="radio" name="Template" id="PSMWPANDefault" />
                                        <label for="PSMWPADefault">PSM PA - N</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success buttonAnimation round box-shadow-1  btn-sm" onclick="PrintOrder()">Print</button>
                    <button type="button" class="btn btn-danger buttonAnimation round box-shadow-1  btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="SecurityEnabled" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <h4>Check Security </h4>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">Security </div>
                        <div class="col-md-9">
                            <input type="password" id="txtSecurity" class="form-control border-primary input-sm" oncopy="return false;" onpaste="return false;" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success buttonAnimation round box-shadow-1  btn-sm" onclick="clickonSecurity()">OK</button>
                    <button type="button" class="btn btn-danger buttonAnimation round box-shadow-1  btn-sm" onclick="clickonSecurityclose()" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="SecurityEnabledVoid" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <h4>Check Security </h4>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">Security </div>
                        <div class="col-md-9">
                            <input type="password" id="txtSecurityVoid" class="form-control border-primary input-sm" oncopy="return false;" onpaste="return false;" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success buttonAnimation round box-shadow-1  btn-sm" onclick="clickonSecurityVoid()">OK</button>
                    <button type="button" class="btn btn-danger buttonAnimation round box-shadow-1  btn-sm" onclick="clickonSecurityvoidclose()" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="CancelOrderPopup" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content" style="width: 500px; margin-left: -120px">
                <div class="modal-header">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <h4>Void Order</h4>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">Remark </div>
                        <div class="col-md-9">
                            <textarea id="CancelRemark" class="form-control border-primary input-sm" rows="2"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success buttonAnimation round box-shadow-1  btn-sm" onclick="cancel()">Void Order</button>
                    <button type="button" class="btn btn-danger buttonAnimation round box-shadow-1  btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>





    <div class="modal fade" id="SecurityEnabledMLTax" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Check Security </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">Security </div>
                        <div class="col-md-9">
                            <input type="password" id="txtSecurityMLTax" class="form-control border-primary reqseq input-sm" onpaste="return false;" oncopy="return false;" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <span id="errormsg"></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning buttonAnimation pull-right round box-shadow-1  btn-sm" onclick="clickonSecurityMLTax()">OK</button>
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1  btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="SecurityEnvalidMLTax" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Message</h4>
                </div>
                <div class="modal-body">
                    <div style="border-color: #ac2925; border: 1px solid; color: #ac2925; min-height: 45px; line-height: 20px; padding: 9px;">
                        <strong id="SbarcodemsgMLTax"></strong>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1  btn-sm" onclick="ClosePop()">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalMLTaxConfirmation" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Remark Details </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">Remark <span class="required">*</span></div>
                        <div class="col-md-9">
                            <textarea id="txtMLRemark" maxlength="500" class="form-control border-primary reqML input-sm" rows="4"></textarea>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning buttonAnimation pull-right round box-shadow-1  btn-sm" onclick="ConfirmMLTax()">OK</button>
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1  btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="SecurityEnabledMigrate" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <h4>Check Security </h4>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">Security </div>
                        <div class="col-md-9">
                            <input type="password" id="txtSecurityMigrate" class="form-control border-primary input-sm" oncopy="return false;" onpaste="return false;" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success buttonAnimation round box-shadow-1  btn-sm" onclick="clickonSecurityMigrate()">OK</button>
                    <button type="button" class="btn btn-danger buttonAnimation round box-shadow-1  btn-sm" onclick="clickonSecurityMigrateclose()" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="MigrateOrderPopup" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content" style="width: 500px; margin-left: -120px">
                <div class="modal-header">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <h4>Migrate Order</h4>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">Remark </div>
                        <div class="col-md-9">
                            <textarea id="txtMigrateRemark" class="form-control border-primary input-sm" rows="2"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success buttonAnimation round box-shadow-1  btn-sm" onclick="MigrateOrder()">Migrate</button>
                    <button type="button" class="btn btn-danger buttonAnimation round box-shadow-1  btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

