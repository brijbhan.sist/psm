﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/MasterPage.master" %>

<script runat="server">

</script>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="col-md-12">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <div class="alert alert-warning alert-dismissable fade in" id="alertMessage" style="font-size: 14px;display:none;">
                <a href="javascript:;" class="close" aria-label="close" onclick="$('#alertMessage').hide();">&times;</a>
                <center>
                    <strong style="text-decoration:underline;">Message</strong><br /><span id="message"></span>
                </center>
            </div>
        </div>
        <div class="col-md-1"></div>
    </div>
     <script type="text/javascript" src="/js/ShowMessage.js"></script>
</asp:Content>

