﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Account_Acccount_viewOrder : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack==true)
        {
            string text = File.ReadAllText(Server.MapPath("/account/JS/Account_OrderView.js"));
            Page.Header.Controls.Add(
                new  LiteralControl(
                     "<script id='checksdrivRequiredFields'>" + text + "</script>"
                    ));
        }
        string[] GtUrl = Request.Url.Host.ToString().Split('.');
        HDDomain.Value = GtUrl[0].ToLower();
    }
}