﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="InternalUpdateDelivery.aspx.cs" Inherits="InternalUpdateDelivery" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .isvalidMin {
            border: 1px solid red;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Order Management</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Order List</a></li>
                        <li class="breadcrumb-item active">Order Details</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
            <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <button type="button" onclick=" location.href='/Account/deliveredOrdersList.aspx'" class="btn btn-info round  dropdown-menu-right box-shadow-2 px-2  " animation="pulse" id="Button5" runat="server">
                            Order List</button>
                    </li>
                </ol>
            </div>
        </div>
    </div>

    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Order Information</h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body" id="panelOrderDetails">
                                <input type="hidden" id="hiddenEmpTypeVal" runat="server" />
                                <input type="hidden" id="hfCustomerType" value="0" runat="server" />
                                <div class="row">

                                    <input type="hidden" id="txtHOrderAutoId" class="form-control input-sm" />
                                    <div class="col-md-3 col-sm-2 form-group">
                                        <label class="control-label">Order No</label>
                                        <input type="text" id="txtOrderId" class="form-control input-sm border-primary" readonly="readonly" />
                                    </div>

                                    <div class="col-md-3 col-sm-2 form-group">
                                        <label class="control-label control-space">Order Date</label>
                                        <input type="text" id="txtOrderDate" class="form-control input-sm border-primary" readonly="readonly" />
                                    </div>
                                    <div class="col-md-3 col-sm-2 form-group">
                                        <label class="control-label control-space">Order Status</label>
                                        <input type="text" id="txtOrderStatus" class="form-control input-sm border-primary" readonly="readonly" />
                                    </div>

                                    <div class="col-md-3 col-sm-2 form-group">
                                        <label class="control-label control-space">Order Type</label>
                                        <input type="text" id="txtOrderType" class="form-control input-sm border-primary" readonly="readonly" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-sm-2 form-group">
                                        <label class="control-label control-space">Delivery Date</label>
                                        <input type="text" id="txtDeliveryDate" class="form-control input-sm border-primary" readonly="readonly" />
                                    </div>
                                    <div class="col-md-3 col-sm-2 form-group">
                                        <label class="control-label control-space">Sales Person</label>
                                        <input type="text" id="txtSalesPerson" class="form-control input-sm border-primary" readonly="readonly" />
                                    </div>
                                    <div class="col-md-3 col-sm-2 form-group">
                                        <label class="control-label">Customer</label>
                                        <input type="text" id="txtCustomer" class="form-control input-sm border-primary" readonly="readonly" />
                                        <input type="hidden" id="hiddenCustAutoId" />
                                    </div>
                                    <div class="col-md-3 col-sm-2 form-group">
                                        <label class="control-label control-space">Customer Type</label>
                                        <input type="text" id="txtCustomerType" class="form-control input-sm border-primary" readonly="readonly" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-sm-2 form-group">
                                        <label class="control-label">Terms</label>
                                        <input type="text" class="form-control input-sm border-primary" id="txtTerms" readonly="readonly" />
                                    </div>
                                    <div class="col-md-3 col-sm-2 form-group">
                                        <label class="control-label control-space">Shipping Type</label>
                                        <input type="text" id="txtShippingType" class="form-control input-sm border-primary" readonly="readonly" />
                                    </div>
                                    <div class="col-md-3 col-sm-4 form-group">
                                        <label class="control-label">Billing Address</label>
                                        <textarea class="form-control border-primary input-sm" id="txtBillAddress" readonly="readonly"> </textarea>

                                    </div>
                                    <div class="col-md-3 col-sm-4 form-group">
                                        <label class="control-label">Shipping Address</label>
                                        <textarea class="form-control border-primary input-sm" id="txtShipAddress" readonly="readonly"> </textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Payments History</h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse" id="CustDues">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="tblCustDues" class="table table-striped table-bordered">
                                        <thead class="bg-blue white">
                                            <tr>
                                                <td class="orderNo text-center">Order No</td>
                                                <td class="orderDate text-center">Order Date</td>
                                                <td class="OrderType text-center">Order Type</td>
                                                <td class="value price">Payable Amount</td>
                                                <td class="amtPaid price">Paid Amount</td>
                                                <td class="amtDue price">Due Amount</td>
                                                <td class="paya price" style="display: none;">Pay</td>
                                                <td class="remarksa" style="display: none;">Remark</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="3" style="text-align: right"><b>TOTAL</b></td>
                                                <td id="sumOrderValue" style="text-align: right"></td>
                                                <td id="sumPaid" style="text-align: right"></td>
                                                <td id="sumDue" style="text-align: right"></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    <h5 class="well text-center" id="noDues" style="display: none">No Dues.</h5>
                                </div>
                                <div class="pull-right">
                                    <button type="button" class="btn btn-default btn-sm" id="btnPay_Dues1" style="display: none; padding: 5px 15px;" onclick="payDueAmount()"><b>Pay</b></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%--Add Products in account section --%>
            <section id="drag-area1">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-content collapse show">
                                <div class="card-body" id="panelProduct">

                                    <div class="row">
                                        <label class="col-sm-1 control-label">Barcode</label>
                                        <div class="col-sm-3 form-group">
                                            <input type="text" class="form-control input-sm border-primary" id="txtBarcode" autocomplete="off" runat="server" onfocus="this.select()" onkeypress="return isNumberKey(event)" placeholder="Enter Barcode here" onchange="readBarcode()" />
                                        </div>

                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label class="control-label">
                                                Product <span class="required">*</span>

                                            </label>
                                            <div class="form-group">
                                                <select class="form-control input-sm selectvalidate border-primary" id="ddlProduct" runat="server" style="width: 100% !important" onchange="bindUnitType()">
                                                    <option value="0">-Select-</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-2 col-md-2">
                                            <label class="control-label">
                                                Unit Type <span class="required">*</span>
                                            </label>
                                            <div class="form-group">
                                                <select class="form-control input-sm Areq border-primary" id="ddlUnitType" runat="server" onchange="ChangeUnit()">
                                                    <option value="0">-Select-</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <label class="control-label" style="white-space: nowrap">
                                                Quantity <span class="required">*</span>

                                            </label>
                                            <div class="form-group">
                                                <input type="text" class="form-control input-sm Areq border-primary" id="txtReqQty" value="1" maxlength="5" runat="server" onkeypress="return isNumberKey(event)" />
                                            </div>
                                        </div>

                                        <div class="col-sm-1">
                                            <label class="control-label" style="white-space: nowrap">
                                                Is Exchange 
                                            </label>
                                            <div class="form-group">
                                                <input type="checkbox" id="chkExchange" onchange="changetype(this,'chkIsTaxable','chkFreeItem')" />
                                            </div>
                                        </div>
                                        <div class="col-sm-1" style="display: none">
                                            <label class="control-label" style="white-space: nowrap">
                                                Is Taxable 
                                            </label>
                                            <div class="form-group">
                                                <input type="checkbox" id="chkIsTaxable" onchange="changetype(this,'chkExchange','chkFreeItem')" />
                                            </div>
                                        </div>
                                        <div class="col-md-1 col-sm-1">
                                            <label class="control-label" style="white-space: nowrap">Free Item</label>
                                            <div class="form-group">
                                                <input type="checkbox" id="chkFreeItem" onchange="changetype(this,'chkExchange','chkIsTaxable')" />
                                            </div>
                                        </div>
                                        <input type="hidden" value="0" id="Product_MinPrice" />
                                        <input type="hidden" value="0" id="Product_CostPrice" />
                                        <script>
                                            function changetype(e, idhtml1, idhtml2) {
                                                $('#' + idhtml1).prop('checked', false);
                                                $('#' + idhtml2).prop('checked', false);
                                            }
                                        </script>
                                        <div class="col-sm-2">
                                            <div class="pull-left">
                                                <button type="button" class="btn btn-md btn-success buttonAnimation pull-right round box-shadow-1 btn-sm" onclick="AddItemList()" id="btnAdd" style="margin-top: 22px;">&nbsp;&nbsp;&nbsp;Add&nbsp;&nbsp;&nbsp;</button>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row">
                                        <div class="col-sm-6 form-group">
                                            <div class="alert alert-danger alertSmall" id="alertStockQty" style="text-align: center; display: none; color: white !important;"></div>
                                        </div>
                                        <div class="col-sm-6 form-group">
                                            <div class="alert alert-danger alertSmall" id="Div2" style="text-align: center; display: none"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <audio class="audios" id="yes_audio" controls preload="none" style="display: none">
                <source src="/Audio/NOExists.mp3" type="audio/mpeg" />
            </audio>
            <audio class="audios" id="No_audio" controls preload="none" style="display: none">
                <source src="/Audio/WrongProduct.mp3" type="audio/mpeg" />
            </audio>

            <%--End Code--%>



            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Order Content</h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse show" id="panelOrderContent" runat="server">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="tblProductDetail" class="table table-striped table-bordered">
                                        <thead class="bg-blue white">
                                            <tr>
                                                <td class="action text-center">Action</td>
                                                <td class="ProId text-center">ID</td>
                                                <td class="ProName">Product Name</td>
                                                <td class="UnitType" style="width: 120px;">Unit</td>
                                                <td class="QtyShip text-center">Shipped<br />
                                                    Qty</td>
                                                <td class="TtlPcs text-center">Total<br />
                                                    Pieces</td>
                                                <td class="UnitPrice" style="text-align: right; width: 5%">Unit Price</td>
                                                <td class="PerPrice" style="text-align: right;">Per Piece<br />
                                                    Price</td>
                                                <td class="FreshReturn text-center">Fresh Return</td>
                                                <td class="DemageReturn text-center">Damage Return</td>
                                                <td class="MissingItem text-center">Missing Item</td>
                                                <td class="QtyDel text-center">Delivered<br />
                                                    Qty</td>
                                                <td class="NetPrice" style="text-align: right;">Net Price</td>
                                                <td class="TaxRate text-center" style="display: none">Tax</td>
                                                <td class="IsExchange text-center" style="display: none">Exchange</td>
                                                <td class="SRP " style="text-align: right; display: none">SRP</td>
                                                <td class="GP" style="text-align: right; display: none">GP</td>
                                                <td class="Status" style="text-align: right; display: none">Status</td>
                                                <td class="Del_MinPrice" style="text-align: right; display: none">Del Min Price</td>
                                                <td class="Del_CostPrice" style="text-align: right; display: none">Del Cost Price</td>
                                                <td class="Del_BasePrice" style="text-align: right; display: none">Del Base Price</td>
                                                <td class="Barcode" style="display: none">Barcode</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7">
                    <div class="card" id="CusCreditMemo" style="display: none">
                        <div class="card-header">
                            <h4 class="card-title">Credit Memo</h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse" id="Div1" runat="server">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel panel-default">
                                            <div class="card-body" id="dCuCreditMemo">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="table-responsive">
                                                            <table id="tblCreditMemoList" class="table table-striped table-bordered">
                                                                <thead class="bg-blue white">
                                                                    <tr>
                                                                        <td class="SRNO text-center">SRNO</td>
                                                                        <td class="Action text-center rowspan" style="display: none">Action</td>
                                                                        <td class="CreditNo text-center">Credit No.</td>
                                                                        <td class="CreditDate text-center">Credit Date</td>
                                                                        <td class="ReturnValue" style="text-align: right;">Total Value</td>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                                <tfoot style="font-weight: 700; background: oldlace;">
                                                                    <tr style="text-align: right;">
                                                                        <td style="display: none" class="rowspan"><b></b></td>
                                                                        <td colspan="3"><b>TOTAL</b></td>
                                                                        <td id="TotalDue"></td>

                                                                    </tr>
                                                                </tfoot>
                                                            </table>
                                                            <h5 class="well text-center" id="H1" style="display: none">No Dues.</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card" id="orderremarks" style="display: none">
                        <div class="card-header">
                            <h4 class="card-title">Order Remark</h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <div class="table-responsive">
                                            <table id="Table2" class="table table-striped table-bordered">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="SRNO text-center">SN</td>
                                                        <td class="EmployeeName">Employee Name</td>
                                                        <td class="EmployeeType text-center">Employee Type</td>
                                                        <td class="Remarks">Remark</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Driver Details</h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <div id="DrvDeliveryInfo">
                                    <div class="row">
                                        <label class="control-label col-md-4">Delivered</label>
                                        <div class="col-md-8 form-group">
                                            <div class="form-control input-sm border-primary">
                                                <input type="radio" class="radio-inline" name="rblDeliver" value="yes" />&nbsp;Yes
                                            <input type="radio" class="radio-inline" name="rblDeliver" value="no" />&nbsp;No 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="control-label col-md-4">Delivery Remark</label>
                                        <div class="col-md-8 form-group">
                                            <textarea class="form-control input-sm border-primary" rows="2" placeholder="Enter Payment details here" id="txtRemarks" disabled="disabled"></textarea>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 form-group">
                                            <button type="button" id="btnPickedOrder" class="btn btn-primary btn-sm pull-right" onclick="PickedOrder()" style="display: none;"><b>Picked Order</b></button>
                                            <button type="button" id="btnSaveDelStatus" class="btn btn-primary btn-sm pull-right" onclick="saveDelStatus()" style="display: none;"><b>Save</b></button>
                                            <button type="button" id="btnUpdate" class="btn btn-success btn-sm pull-right" onclick="saveDelStatus()" style="display: none;"><b>Update</b></button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Account Details</h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="panel panel-default" id="AccountDeliveryInfo" style="display: none;">
                                    <div class="row">
                                        <!------------------------------- class NotToShow to hide fields for driver --------------------------------------->
                                        <label class="control-label  col-md-4 col-sm-6">Payment Received ?</label>
                                        <div class="col-md-8 col-sm-6 form-group ">
                                            <div class="form-control input-sm border-primary ReqCheck">
                                                <input type="radio" class="radio-inline border-primary" name="rblPaymentReceive" value="yes" disabled="disabled" />&nbsp;Yes
                                            <input type="radio" class="radio-inline border-primary" name="rblPaymentReceive" value="no" disabled="disabled" />&nbsp;No   
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="control-label col-md-4  col-sm-6">Account Remark</label>
                                        <div class="col-md-8 col-sm-6 form-group">
                                            <textarea class="form-control input-sm border-primary" rows="3" placeholder="Enter Payment details here" id="txtAcctRemarks" disabled="disabled"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="card">
                        <div class="card-content collapse show" runat="server">
                            <div class="card-body">
                                <div class="row">
                                    <label class="col-md-5 col-sm-5 form-group text-right">Total Amount</label>
                                    <div class="col-md-7 col-sm-7 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text input-group-text-custom">$ 
                                                </span>
                                            </div>
                                            <input type="text" class="form-control input-sm border-primary text-right" id="txtTotalAmount" value="0.00" readonly="readonly" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-5 col-sm-5 form-group text-right">Overall Discount</label>
                                    <div class="col-md-7 col-sm-7">
                                        <div style="display: flex" class="form-group">
                                            <div class="input-group">

                                                <input type="text" class="form-control input-sm border-primary text-right" onkeypress="return isNumberDecimalKey(event,this)" id="txtOverallDisc" value="0.00" onkeyup="calOverallDisc()" />
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text input-group-text-custom">
                                                        <span>%</span>
                                                    </span>
                                                </div>
                                            </div>
                                            &nbsp;                                      
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text input-group-text-custom">$
                                                    </span>
                                                </div>
                                                <input type="text" class="form-control input-sm border-primary text-right" id="txtDiscAmt" value="0.00" onblur="calOverallDisc1()" onkeypress="return isNumberDecimalKey(event,this)" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-5 col-sm-5 form-group text-right">Shipping Charges</label>
                                    <div class="col-md-7 col-sm-7 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text input-group-text-custom">$
                                                </span>
                                            </div>
                                            <input type="text" class="form-control input-sm border-primary text-right" id="txtShipping" value="0.00" onkeyup="calGrandTotal()" onfocus="this.select()" readonly="readonly" />
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="row">
                                    <label class="col-md-5 col-sm-5 form-group text-right">Tax Type</label>
                                    <div class="col-md-7 col-sm-7 form-group">
                                        <select class="form-control input-sm" id="ddlTaxType" runat="server">
                                            <option value="0" taxvalue="0.0">-Select Tax Type-</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-5 col-sm-5 form-group text-right">Total Tax</label>
                                    <div class="col-md-7  col-sm-7 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text input-group-text-custom">$
                                                </span>
                                            </div>
                                            <input type="text" class="form-control input-sm border-primary text-right" id="txtTotalTax" value="0.00" readonly="readonly" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-5 form-group text-right">ML Quantity</label>
                                    <div class="col-sm-7 form-group">
                                        <input type="text" class="form-control input-sm border-primary text-right" id="txtMLQty" value="0.00" readonly="readonly" />
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-5 form-group text-right">ML Tax</label>
                                    <div class="col-sm-7 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text input-group-text-custom">$
                                                </span>
                                            </div>
                                            <input type="text" class="form-control input-sm border-primary text-right" id="txtMLTax" value="0.00" readonly="readonly" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-5 form-group text-right">Weight Quantity</label>
                                    <div class="col-sm-7 form-group">
                                        <input type="text" class="form-control input-sm border-primary text-right" id="txtWeightQty" value="0.00" readonly="readonly" />
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-5 form-group text-right">Weight Tax</label>
                                    <div class="col-sm-7 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text input-group-text-custom">$
                                                </span>
                                            </div>
                                            <input type="text" class="form-control input-sm border-primary text-right" id="txtWeightTax" value="0.00" readonly="readonly" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-5 form-group text-right">Adjustment</label>
                                    <div class="col-sm-7 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text input-group-text-custom">$
                                                </span>
                                            </div>
                                            <input type="text" class="form-control input-sm border-primary text-right" id="txtAdjustment" value="0.00" readonly="readonly" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-5 col-sm-5 form-group text-right"><b>Grand Total</b></label>
                                    <input type="hidden" id="hiddenGrandTotal" />
                                    <div class="col-md-7 col-sm-7 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text input-group-text-custom">$
                                                </span>
                                            </div>
                                            <input type="text" class="form-control input-sm inputBold border-primary text-right" readonly="readonly" id="txtGrandTotal" value="0.00" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row SMANAGER">
                                    <label class="col-md-5 col-sm-5 form-group control-space text-right">Credit Memo Amount</label>
                                    <div class="col-md-7 col-sm-7 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text input-group-text-custom">$
                                                </span>
                                            </div>
                                            <input type="text" class="form-control input-sm border-primary text-right" id="txtDeductionAmount" value="0.00" readonly="readonly" />
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="row SMANAGER">
                                    <label class="col-md-5 col-sm-5 form-group text-right">Store Credit Amount<span style="display: none"> (max $<span id="CreditMemoAmount">0.00</span> Available)</span></label>
                                    <div class="col-md-7 col-sm-7 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text input-group-text-custom">$
                                                </span>
                                            </div>
                                            <input type="text" class="form-control input-sm border-primary text-right" readonly="true" id="txtStoreCreditAmount" onfocus="this.select()" value="0.00" />
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="row SMANAGER">
                                    <label class="col-md-5 col-sm-5 form-group text-right">Payable Amount</label>
                                    <div class="col-md-7 col-sm-7 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text input-group-text-custom">$
                                                </span>
                                            </div>
                                            <input type="text" class="form-control input-sm border-primary text-right" readonly="true" id="txtPaybleAmount" value="0.00" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-md-5 col-sm-5 form-group text-right"><b>Total Amount Paid</b></label>
                                    <div class="col-md-7 col-sm-7 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text input-group-text-custom">$
                                                </span>
                                            </div>
                                            <input type="text" class="form-control input-sm inputBold border-primary text-right" readonly="readonly" id="txtAmtPaid" value="0.00" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="rowAmountDue">
                                    <label class="col-md-5 col-sm-5 form-group text-right"><b>Amount Due</b></label>
                                    <div class="col-md-7 col-sm-7 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text input-group-text-custom">$
                                                </span>
                                            </div>
                                            <input type="text" class="form-control input-sm inputBold border-primary text-right" readonly="readonly" id="txtAmtDue" value="0.00" />
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="row">
                                    <label class="col-md-5 col-sm-5 form-group text-right">No. of Packed Boxes</label>
                                    <div class="col-md-7 col-sm-7 form-group">
                                        <input type="text" class="form-control input-sm border-primary text-right" id="txtPackedBoxes" runat="server" readonly="readonly" />

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-footer">
                <div class="row">
                    <div class="col-md-12">

                        <div class="btn-group mr-1 pull-right">
                            <button type="button" id="btnGenOrderCC" class="btn btn-info buttonAnimation pull-right round box-shadow-1  btn-sm" onclick="printOrder_CustCopy()"><b>Generate Customer Copy of Order</b></button>
                        </div>
                        <div class="btn-group mr-1 pull-right">
                            <button type="button" id="btnSave" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" onclick="saveDelUpdates()" style="display: none;"><b>Save</b></button>
                        </div>
                        <div class="btn-group mr-1 pull-right">
                            <button type="button" id="btnAUpdate" class="btn btn-warning buttonAnimation pull-right round box-shadow-1  btn-sm" onclick="updatedeleverOrder()" style="display: none;"><b>Update</b></button>
                        </div>
                        <div class="btn-group mr-1 pull-right">
                            <button type="button" id="btnPayNow" class="btn btn-primary buttonAnimation pull-right round box-shadow-1  btn-sm" onclick="PayNowDelivery()" style="display: none;"><b>Pay Now</b></button>
                        </div>
                        <div class="btn-group mr-1 pull-right">
                            <button type="button" id="btnGenPI1" class="btn btn-grey-blue buttonAnimation pull-right round box-shadow-1  btn-sm" style="display: none;" onclick="InvoiceOption()"><b>Generate Invoice</b></button>
                        </div>
                        <%
                            if (Session["EmpTypeNo"] != null)
                            {
                                if (Session["EmpTypeNo"].ToString() == "1")
                                {
                        %>
                        <div class="btn-group mr-1 pull-right">
                            <button type="button" id="btnVoidOrder" class="btn btn-danger buttonAnimation pull-right round box-shadow-1  btn-sm" onclick="PopSecurityVoid()"><b>Void Order</b></button>
                        </div>
                        <%
                                }
                            }
                        %>
                        <div class="btn-group mr-1 pull-right">
                            <button type="button" id="btnUpdateCostPrice" class="btn btn-warning buttonAnimation pull-right round box-shadow-1  btn-sm" onclick="UpdateCostPrice()"><b>Update Cost Price</b></button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="modalInvoiceOptions" role="dialog">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Choose Option</h4>
                </div>
                <div class="modal-body" style="font-size: 14px;">
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label class="radio-inline">
                                <input type="radio" name="InvOption" value="11" />View / Print
                            </label>
                            <br />
                            <label class="radio-inline">
                                <input type="radio" name="InvOption" value="22" />Email
                            </label>
                            <br />
                            <label class="radio-inline">
                                <input type="radio" name="InvOption" value="33" />Fax
                            </label>
                            <br />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="ShowInvoice()">OK</button>
                </div>
            </div>

        </div>
    </div>

    <div class="modal fade" id="modalUpdateStock" role="dialog">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <%--<h4 class="modal-title">Choose Option</h4>--%>
                </div>
                <div class="modal-body" style="font-size: 14px;">
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <span>Total of Fresh and Return Qty always less than or equal to Total pieces.</span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success pull-right round box-shadow-1 btn-sm" onclick="yes()">Yes</button>
                    <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1 btn-sm" onclick="NO()">No</button>
                </div>
            </div>

        </div>
    </div>
    <div id="PopBarCodeforPickBox" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" onclick="closePacked()">&times;</button>
                    <h4 class="modal-title">Read Packed Boxes  Barcode</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">
                            Order No :
                        </div>
                        <div class="col-md-9 form-group">
                            <label id="lblOrderno"></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            No Of Boxes :
                        </div>
                        <div class="col-md-9 form-group">
                            <label id="lblNoOfbox"></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            Bar Code :
                        </div>
                        <div class="col-md-9 form-group">
                            <input type="text" id="txtReadBorCode" class="form-control input-sm" onchange="readBoxBarcode()" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                        </div>
                        <div class="col-md-9 form-group">
                            <button type="button" id="btnPick1" class="btn btn-primary btn-sm" style="display: none;" onclick="OrderPicked()"><b>Pick Order</b></button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12" style="text-align: right">
                            <span id="baxbarmsg"></span>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered" id="tblBarcode">
                                    <thead>
                                        <tr>
                                            <td class="SRNO text-center">SN</td>
                                            <td class="Barcode text-center">Bar Code</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" runat="server" id="HDDomain" />
    <div id="PopPrintTemplate" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Select Invoice Template</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="container">
                                <div class="row form-group">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-10">
                                        <input type="radio" checked name="Template" id="chkdefault" />
                                        PSM Default  - Invoice Only
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-10">
                                        <input type="radio" name="Template" id="chkdueamount" />
                                        PSM Default
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-10">
                                        <input type="radio" name="Template" id="PackingSlip" />
                                        Packing Slip
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-10">
                                        <input type="radio" name="Template" id="WithoutCategorybreakdown" />
                                        Packing Slip - Without Category Breakdown
                                    </div>
                                </div>
                                <div class="row form-group" id="divPSMPADefault" style="display: none">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-10">
                                        <input type="radio" name="Template" id="PSMPADefault" />
                                        PSM PA Default
                                    </div>
                                </div>
                                <div class="row form-group" id="divPSMWPADefault" style="display: none">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-10">
                                        <input type="radio" name="Template" id="PSMWPADefault" />
                                        PSM WPA-PA
                                    </div>
                                </div>
                                <div class="row form-group" id="divPSMNPADefault" style="display: none">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-10">
                                        <input type="radio" name="Template" id="rPSMNPADefault" />
                                        PSM NPA Default
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-10">
                                        <input type="radio" name="Template" id="Template1" />
                                        7 Eleven (Type 1)
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-10">
                                        <input type="radio" name="Template" id="Template2" />
                                        7 Eleven (Type 2)
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-10">
                                        <input type="radio" name="Template" id="Templ1" />
                                        Template 1
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-10">
                                        <input type="radio" name="Template" id="Templ2" />
                                        Template 2
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="row" id="divPSMWPANDefault">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-10">
                                        <input type="radio" name="Template" id="PSMWPANDefault" />
                                        <label for="PSMWPADefault">PSM PA - N</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1 btn-sm" onclick="PrintOrder()">Print</button>
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="SecurityEnabledVoid" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Check Security </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">Security </div>
                        <div class="col-md-9">
                            <input type="password" id="txtSecurityVoid" class="form-control input-sm border-primary" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success pull-right round box-shadow-1 btn-sm" onclick="clickonSecurityVoid()">OK</button>
                    <button type="button" class="btn btn-danger pull-right round box-shadow-1 btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="SecurityEnvalid" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Message</h4>
                </div>
                <div class="modal-body">
                    <div style="border-color: #ac2925; border: 1px solid; color: #ac2925; min-height: 45px; line-height: 20px; padding: 9px;">
                        <strong id="Sbarcodemsg"></strong>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-sm" onclick="closePop(1)" id="btnClose">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="VoidRemarkPopup" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Remark</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-md-12">
                            <textarea id="txtRemark" class="form-control input-sm border-primary req" rows="3"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-success buttonAnimation round box-shadow-1  btn-sm" onclick="VoidOrderByRemark()" id="btnVoid">Void Order</button>
                    <button type="button" class="btn btn-danger buttonAnimation round box-shadow-1  btn-sm" data-dismiss="modal" id="btnCloseRemark">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalCostPriceRemark" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Remark</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-md-12">
                            <textarea id="txtCostPriceRemark" class="form-control input-sm border-primary req" rows="3"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-warning buttonAnimation round box-shadow-1  btn-sm" onclick="updatePriceConfirmation()">Update</button>
                    <button type="button" class="btn btn-danger buttonAnimation round box-shadow-1  btn-sm" onclick="closemdlcosp()" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <audio class="audios" id="Stock" controls preload="none" style="display: none">
        <source src="/Audio/Stock.mp3" type="audio/mpeg" />
    </audio>
</asp:Content>

