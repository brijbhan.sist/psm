﻿using DLLPattyCashTransaction;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Account_PattyCash : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod(EnableSession = true)]
    public static string Binddropdown()
    {
        PL_PattyCashTransaction pobj = new PL_PattyCashTransaction();
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {

                BL_PattyCashTransaction.Binddropdown(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                return json;


            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
       
    }

}