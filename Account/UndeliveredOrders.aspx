﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/MasterPage.master" ClientIDMode="Static" %>

<script runat="server">

</script>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Undelivered Orders List</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Account</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Undelivered Orders List</a>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item">Export</button>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-4">
                                        <select class="form-control border-primary input-sm" id="ddlCustomer" runat="server">
                                            <option value="0">- All Customer -</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Order No" id="txtSOrderNo" />
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <select class="form-control input-sm" id="ddlSStatus" runat="server">
                                            <option value="0">- All Status -</option>
                                        </select>
                                    </div>

                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0 1rem">
                                                    <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm date" placeholder="From Date" id="txtSFromDate" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0 1rem">
                                                    <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm date" placeholder="To Date" id="txtSToDate" />
                                        </div>
                                    </div>

                                    <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" id="btnSearch" onclick="getUndelOrdersList(1)">Search</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="content-body">
        <section id="drag-area3">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblUndelOrders">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="OrderNo">Order No</td>
                                                        <td class="OrderDt">Order Date</td>
                                                        <td class="Customer">Customer</td>
                                                        <td class="DelDate">Del. Date</td>
                                                        <td class="OrderVal" style="text-align: right;">Order Value($)</td>
                                                        <td class="Status">Status</td>
                                                        <td class="UndelRemarks">Remark</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="Pager"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <script src="/Account/JS/UndeliveredOrders.js"></script>
</asp:Content>

