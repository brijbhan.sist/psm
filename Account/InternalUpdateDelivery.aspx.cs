﻿using DLL_Account_OrderMaster;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class InternalUpdateDelivery : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
            string text = File.ReadAllText(Server.MapPath("/Account/JS/InternalUpdateDelivery.js"));
            Page.Header.Controls.Add(
                new LiteralControl(
                     "<script id='checksdrivRequiredFields'>" + text + "</script>"
                    ));
        }
        try
        {
            hiddenEmpTypeVal.Value = Session["EmpTypeNo"].ToString();

            string[] GtUrl = Request.Url.Host.ToString().Split('.');
            HDDomain.Value = GtUrl[0].ToLower();
        }
        catch
        {
            Session.Abandon();
            Response.Redirect("~/Default.aspx", false);
        }
    }
    [WebMethod(EnableSession = true)]
    public static string UpdateCostPrice(string OrderValues)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(OrderValues);
            PL_Account_OrderMaster pobj = new PL_Account_OrderMaster();
            try
            {
                pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
                pobj.Remarks= jdv["Remarks"];
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                BL_Account_OrderMaster.UpdateCostprice(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}