﻿$(document).ready(function () {
    BindEmployeeListForChatWindow();
    
});

/*-------------------------------------------------------------------Bind Employee---------------------------------------------*/
function BindEmployeeListForChatWindow() {
    
    $.ajax({
        type: "POST",
        url: "/WebAPI/WChatWindow.asmx/BindEmployeeList",
        data: "{'UserName':'" + $("#txtSearchName").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,

        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var DropDown = $.parseJSON(response.d);
                var row = $("#Chat_tblAllEmployeeList thead tr").clone(true);
                for (var i = 0; i < DropDown.length; i++) {
                    var AllDropDownList = DropDown[i];
                    var Emplist = AllDropDownList.Emplist;
                    if (Emplist.length > 0) {
                        //$("#Chat_tblAllEmployeeList tbody tr").remove();
                        $("#Chat_EmptyTable").hide();
                        var trs = $('#Chat_tblAllEmployeeList tbody tr');

                        if ($("#txtSearchName").val() != '') {
                            for (var mn = 0; mn < trs.length; mn++) {
                                var item = trs[mn];
                                for (var c_row = 0; c_row < Emplist.length; c_row++) {
                                    var C_st = Emplist[c_row];
                                    if ($(item).find('.Chat_Id').text() != C_st.AutoId) {

                                        $(item).remove();
                                    }
                                    break;
                                }
                            }
                        }
                          
                        for (var EmpNum = 0; EmpNum < Emplist.length; EmpNum = Number(EmpNum) + 1) {
                            var Emp = Emplist[EmpNum];
                           
                            var check = false;
                            var MsgCount = ''; IsOnlineImg = ''; EmpImage = '';
                            if (Emp.UnreadMsg > 0) {
                                MsgCount = "<span class='MsgIcon'>" + Emp.UnreadMsg + "</span>";
                            }
                            if (Emp.IsOnline == '1') {
                                IsOnlineImg = "<img src='../Img/logo/online.png'/>";

                            } else {
                                IsOnlineImg = '';
                            }
                            if (Emp.ChatType == 'Individual') {
                                EmpImage = "<img src='../Img/logo/user.png' style='width:35px;margin: 0px 20px 0px 20px;' />";
                            } else {
                                EmpImage = "<img src='../Img/logo/GroupImg.png' style='width:35px;margin: 0px 20px 0px 20px;' />";

                            }
                            var Chat_Name = "<a href = '#' onclick = 'return register_popup(" + Emp.AutoId + ", \"" + Emp.EmpName + "\"," + Emp.ChatId + "," + Emp.SenderIds + ", \"" + Emp.ChatType + "\");' style = 'color:black;' > <span class='emp_img'>" + EmpImage + "</span> " + Emp.EmpName + "</a >&nbsp;&nbsp;&nbsp;" + IsOnlineImg + " <br/> <span style='padding-left: 80px;'>" + Emp.Msg + "</span>" + MsgCount;

                           
                            for (var m = 0; m < trs.length; m++) {
                                var item = trs[m];
                                if ($(item).find('.Chat_Id').text() == Emp.AutoId) {
                                    
                                    if (($(item).find('.Chat_Id span').attr('online') != Emp.IsOnline) || ($(item).find('.Chat_Id span').attr('unread_msg') != Emp.UnreadMsg)) {
                                        $(item).find('.Chat_Name').html(Chat_Name);
                                        $(item).find(".Chat_Id span").attr("online", Emp.IsOnline);
                                        $(item).find(".Chat_Id span").attr("unread_msg", Emp.UnreadMsg);
                                    }
                                    //else if (Emp.IsOnline == 0 && $(item).find('.Chat_Id').text() == Emp.AutoId && $(item).find('.Chat_Id span').attr('online') == '1') {

                                    //    $(item).find('.Chat_Name').html(Chat_Name);
                                    //    $(item).find(".Chat_Id span").attr("online", Emp.IsOnline);
                                    //    $(item).find(".Chat_Id span").attr('unread_msg', Emp.UnreadMsg);
                                    //}
                                    check = true;
                                    break;
                                }
                               
                            }
                            if (!check) {
                                console.log('test');

                                $(".Chat_Id", row).html(Emp.AutoId + '<span online=' + Emp.IsOnline + ' unread_msg=' + Emp.UnreadMsg + '></span>');


                                $(".Chat_Name", row).html(Chat_Name);


                                $("#Chat_tblAllEmployeeList tbody").append(row);
                                row = $("#Chat_tblAllEmployeeList tbody tr:last").clone(true);
                            }


                        }
                    } else {
                        $("#Chat_EmptyTable").show();
                    }

                }

            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        },
        error: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        }
    });
};

//this function can remove a array element.
Array.remove = function (array, from, to) {
    var rest = array.slice((to || from) + 1 || array.length);
    array.length = from < 0 ? array.length + from : from;
    return array.push.apply(array, rest);
};

//this variable represents the total number of popups can be displayed according to the viewport width
var total_popups = 0;

//arrays of popups ids
var popups = [];

//this is used to close a popup
function close_popup(id, ChatId) {
    for (var j = 0; j < EmpData.length; j++) {
        //console.log(EmpData[j].ChatId);
        var FindChat = EmpData[j];

        if (ChatId == FindChat.ChatId) {
            Array.remove(EmpData, j);
            //return;
        }
    }
    for (var iii = 0; iii < popups.length; iii++) {
        if (id == popups[iii]) {
            Array.remove(popups, iii);

            document.getElementById(id).style.display = "none";

            calculate_popups();

            return;
        }
    }
}
//displays the popups. Displays based on the maximum number of popups that can be displayed on the current viewport width
function display_popups() {
    var right = 220;

    var iii = 0;
    for (iii; iii < total_popups; iii++) {
        if (popups[iii] != undefined) {
            var element = document.getElementById(popups[iii]);
            element.style.right = right + "px";
            right = right + 320;
            element.style.display = "block";
        }
    }

    for (var jjj = iii; jjj < popups.length; jjj++) {
        var element = document.getElementById(popups[jjj]);
        element.style.display = "none";
    }
}
var element = '';

//creates markup for a new popup. Adds the id to popups array.
var EmpData = new Array();
var Emp = {};
function register_popup(id, name, ChatId, SenderId, ChatType) {
    if (ChatId == 0) {
        GenerateChatID(id, name, SenderId);
        ChatId = Chat;
    }

    Emp.id = id;
    Emp.ChatId = ChatId;
    Emp.SenderId = SenderId;
    EmpData.unshift(Emp);

    for (var iii = 0; iii < popups.length; iii++) {
        if (id == popups[iii]) {
            Array.remove(popups, iii);

            popups.unshift(id);

            calculate_popups();


            return;
        }
    }
    // 
    var element = '<div class="popup-box chat-popup" id="' + id + '">';
    element = element + '<div class="popup-head">';
    element = element + '<div class="popup-head-left">' + name + '</div><span id="' + ChatId + '"></span><span id="' + SenderId + '"></span>';
    element = element + '<div class="popup-head-right"><a href="#" onclick="Collapse_popup(\'' + id + '\')">&#8722;</a>&nbsp;&nbsp;&nbsp;<a href="javascript:close_popup(\'' + id + '\',\'' + ChatId + '\');">&#10005;</a></div>';
    element = element + '<div style="clear: both"></div></div>';

    element = element + '<div class="Add_Mem" id="div' + id + '" style="display:none;min-height:290px;background:white;">';
    element = element + '<div class="table-responsive" style="height: 245px;width:100%;overflow-x:hidden">';
    element = element + '<table class="table table-hover table-bordered Chat_Member" id=' + id + '>';
    element = element + '<thead class="bg-blue white" style="display: none;"><tr>';
    element = element + '<td class="Chat_GroupMember text-left" style="padding: 10px 0.5rem !important;">Action</td><td class="Chat_MemberId text-left" style="display:none"></td></tr>';
    element = element + '</thead><tbody></tbody></table></div>';

    element = element + '<div style="padding:10px"><button type="button" class="btn btn-sm btn-basic pull-left" onclick="AddRequest_Cancel(' + id + ')">Cancel</button>';
    element = element + '<button type="button" class="btn btn-sm btn-primary pull-right" id="btn' + id + '" onclick="Request_AddPeople(' + id + ',' + ChatId + ')">Add People</button>';
    element = element + '<button type="button" style="display:none" class="btn btn-sm btn-primary pull-right" id="sub' + id + '" onclick="Chat_chkbox_Onclick(' + id + ',' + ChatId + ')">Add People</button>';
    element = element + '</div></div>';

    if (ChatType != 'Individual') {
        element = element + '<div class="Add_Mem" id="confirmationdiv' + id + '" style="display:none;min-height:290px;background:white;"></div>';

        element = element + '<div class="Chat_Details"><span style="float:left;padding:5px;" class="la la la-user-plus" onclick="AddMember(' + id + ',' + ChatId + ')"></span></div>';
    }

    if (ChatType != 'Individual') {
        element = element + '<div class="MessageDiv" id="' + ChatId + '" style="display:flex;flex-flow: column-reverse;max-height: 223px;"></div>';
    } else {
        element = element + '<div class="MessageDiv" id="' + ChatId + '" style="display:flex;flex-flow: column-reverse;max-height: 253px;"></div>';

    }

    element = element + '<div class="chat-input-holder" id=' + id + ' style="margin-bottom:0px;">';
    element = element + '<textarea class="chat-input txtmsg" id="txt' + id + '" style="height:33px;"></textarea >';
    element = element + '<span style="background: gainsboro;font-size: 23px;float:right"><i class="la la la-paper-plane" onclick="SendSMS(\'' + id + '\',\'' + ChatId + '\',\'' + SenderId + '\')" style="font-size: 27px;"></i></span></div>';


    //document.getElementsByTagName("body")[0].innerHTML = document.getElementsByTagName("body")[0].innerHTML + element;
    $("#Chat_PopUp").append(element);
    if (ChatId != 0) {
        Message(ChatId, id, SenderId);
    }
    popups.unshift(id);

    calculate_popups();

}
//calculate the total number of popups suitable and then populate the toatal_popups variable.
function calculate_popups() {
    var width = window.innerWidth - 300;
    if (width < 540) {
        total_popups = 0;
    }
    else {
        width = width - 200;
        //320 is width of a single popup box
        total_popups = parseInt(width / 320);
    }

    display_popups();
}


window.addEventListener("resize", calculate_popups);
window.addEventListener("load", calculate_popups);

window.setInterval(function () {
    if (EmpData.length > 0) {
        for (var i = 0; i < EmpData.length; i++) {
            var bind_Data = EmpData[i];

            Message(bind_Data.ChatId, bind_Data.id, bind_Data.SenderId);

        }
    }
    OpenPopUp_Response();
}, 3000);

window.setInterval(function () {
    BindEmployeeListForChatWindow();

}, 10000);


function Message(ChatId, id, SenderId) {
    $.ajax({
        type: "POST",
        url: "/WebAPI/WChatWindow.asmx/ShowMessages",
        data: "{'ChatId':'" + ChatId + "','AutoId':'" + id + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var DropDown = $.parseJSON(response.d);
            $("#" + id + '>.MessageDiv').html('');
            for (var i = 0; i < DropDown.length; i++) {

                var AllDropDownList = DropDown[i];
                var ChatMessage = AllDropDownList.ChatMessage;
                if (ChatMessage.length > 0) {

                    var elm = '';
                    for (var j = 0; j < ChatMessage.length; j++) {
                        var Emp = ChatMessage[j];
                        if (Emp.SenderId == SenderId) {
                            elm = elm + '<div class="message-box-holder"><div class="message-box" >' + Emp.Message + '</div></div>'
                        } else {

                            elm = elm + '<div class="message-box-holder"><div class="message-box message-partner">' + Emp.Message + '</div ></div>';
                        }

                    }
                }

                $("#" + id + '>.MessageDiv').append(elm);
            }

        },
        failure: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        },
        error: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        }
    });

}

function SendSMS(id, ChatId, SenderId) {
    var TextMsg = $("#txt" + id).val();
    if (TextMsg != '') {

        var data = {
            ReceiverId: id,
            ChatId: ChatId,
            Message: TextMsg,

        }
        $.ajax({
            type: "POST",
            url: "/WebAPI/WChatWindow.asmx/insertSendSMS",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            //data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            async: false,
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == 'false') {
                    swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                }
                else if (result.d == 'Unauthorized access.') {
                    location.href = "/";
                }
                else {
                    if (result.d == 'true') {
                        $("#txt" + id).val('');
                        Message(ChatId, id, SenderId);

                    }
                }
            },
            error: function (result) {
                swal("Error!", result.d, "error");
            },
            failure: function (result) {
                swal("Error!", result.d, "error");
            }
        });
    }

}
function Chat_AddNewGroup() {
    $('.Chat_AddNewUser').toggle();
    BindEmpListforNewGroup();
}
function BindEmpListforNewGroup() {
    $.ajax({
        type: "POST",
        url: "/WebAPI/WChatWindow.asmx/BindAllUsersList",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var DropDown = $.parseJSON(response.d);
                var row = $("#Chat_tblEmployeeList thead tr").clone(true);
                for (var i = 0; i < DropDown.length; i++) {
                    $("#Chat_tblEmployeeList tbody tr").remove();

                    var AllDropDownList = DropDown[i];
                    var Emplist = AllDropDownList.Emplist;
                    if (Emplist.length > 0) {
                        $("#Chat_EmptyTable1").hide();
                        //$('#tblEmployeeList').append('<tr><td onclick="AddNewGroup()" style="font-size:18px;text-align:left"><span style="margin-left:22px;margin-right: 20px;"><i  class="fullscreen la la ft-user-plus"></i></span>New Group</td></tr>');
                        for (var j = 0; j < Emplist.length; j++) {
                            var Emp = Emplist[j];

                            $(".Chat_Name", row).html("<input type='checkbox' name='table_records' id='chkProduct' onclick='chkbox_OnClick(this)' style='width:10% '><a href='#' style='color:black;'><span class='emp_img'><img src='../Img/logo/user.png' style='width:35px;margin: 0px 20px 0px 20px;'/></span> " + Emp.EmpName + "</a>");
                            $(".Chat_MemberAutoId", row).text(Emp.AutoId);

                            $("#Chat_tblEmployeeList tbody").append(row);
                            row = $("#Chat_tblEmployeeList tbody tr:last").clone(true);


                        }
                    } else {
                        $("#Chat_EmptyTable1").show();
                    }
                }
            } else {
                location.href = '/';
            }

        },
        failure: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        },
        error: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        }
    });
}



function Chat_SendRequest() {
    var flag = false;
    var SendEmpData = new Array();
    $("#Chat_tblEmployeeList tbody tr").each(function () {
        var row = $(this);
        if (row.find("td").eq(0).find("input").prop("checked") == true) {
            var Emp = {};
            Emp.MemberAutoId = row.find('.Chat_MemberAutoId').text();

            SendEmpData.push(Emp);
            flag = true;
        }
    })

    if (flag) {
        if (dynamicALL('ddlChatreq')) {

            $.ajax({
                type: "POST",
                url: "/WebAPI/WChatWindow.asmx/CreateGroup",
                data: "{'dataValue':'" + JSON.stringify(SendEmpData) + "','GroupName':'" + $("#Chat_GroupName").val() + "'}",
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                async: false,
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (result) {
                    if (result.d == 'false') {
                        swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                    }
                    else if (result.d == 'Unauthorized access.') {
                        location.href = "/";
                    }
                    else {
                        toastr.success('Group added successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                        $("#Chat_GroupName").val('');
                        BindEmployeeListForChatWindow();
                        $('.Chat_AddNewUser').hide();
                    }
                },
                error: function (result) {
                    swal("Error!", result.d, "error");
                },
                failure: function (result) {
                    swal("Error!", result.d, "error");
                }
            });
        } else {
            toastr.error('Please Add Group Name', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

        }

    } else {
        toastr.error('Please select Member', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
var Chat = '';
function GenerateChatID(id, name, SenderId) {


    var data = {
        ReceiverId: id,

    }
    $.ajax({
        type: "POST",
        url: "/WebAPI/WChatWindow.asmx/GenerateChatID",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (result) {
            if (result.d == 'false') {
                swal("Error!", "Oops! Something went wrong.Please try later.", "error");
            }
            else if (result.d == 'Unauthorized access.') {
                location.href = "/";
            }
            else {
                var xmldoc = $.parseXML(result.d);
                ChatDetails = $(xmldoc).find('Table');
                Chat = $(ChatDetails).find('ChatId').text();

            }
        },
        error: function (result) {
            swal("Error!", result.d, "error");
        },
        failure: function (result) {
            swal("Error!", result.d, "error");
        }
    });

}

function OpenPopUp_Response() {
    $.ajax({
        type: "POST",
        url: "/WebAPI/WChatWindow.asmx/OpenPopUp_Response",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var DropDown = $.parseJSON(response.d);
            for (var i = 0; i < DropDown.length; i++) {
                var AllDropDownList = DropDown[i];
                var ChatMessages = AllDropDownList.ChatId;
                if (ChatMessages.length > 0) {
                    for (var j = 0; j < ChatMessages.length; j++) {
                        var Chat_Pop = ChatMessages[j];
                        //if (Pop.AutoId != '' && Pop.EmpName != '' && Pop.ChatId != '' && Pop.SenderId != '') {

                        register_popup(Chat_Pop.AutoId, Chat_Pop.EmpName, Chat_Pop.ChatId, Chat_Pop.SenderId, Chat_Pop.ChatType);
                        $("#Chat_PopUpOpenAudio")[0].play();
                    }
                }
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function Collapse_popup(PopupId) {
     
    $("#" + PopupId).toggleClass('popup_mini');

}

function AddMember(popup_Id, ChatId) {
    Chat_AllGroupMem(popup_Id, ChatId);
    $("#div" + popup_Id).show();
    $("#" + popup_Id + '>.MessageDiv').hide();
    $("#" + popup_Id + '>.chat-input-holder').hide();
    $("#btn" + popup_Id).show();
    $("#sub" + popup_Id).hide();
    $("#confirmationdiv" + popup_Id).hide();
    $("#confirmationdiv" + popup_Id).html('');
}

function AddRequest_Cancel(popup_Id) {
    $("#" + popup_Id + '>.MessageDiv').show();
    $("#" + popup_Id + '>.chat-input-holder').show();
    $("#div" + popup_Id).hide();
    $("#confirmationdiv" + popup_Id).hide();
}

function Chat_AllGroupMem(popup_Id, ChatId) {
    $("#confirmationdiv" + popup_Id).html('');
    $.ajax({
        type: "POST",
        url: "/WebAPI/WChatWindow.asmx/Chat_AllGroupMem",
        data: "{'ChatId':'" + ChatId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",

        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var DropDown = $.parseJSON(response.d);
                var row = $("#" + popup_Id + " thead tr").clone(true);
                for (var i = 0; i < DropDown.length; i++) {
                    $("#" + popup_Id + " tbody tr").remove();

                    var AllDropDownList = DropDown[i];
                    var Member = AllDropDownList.Member;
                    if (Member.length > 0) {

                        for (var j = 0; j < Member.length; j++) {
                            var Emp = Member[j];

                            $(".Chat_GroupMember", row).html("" + Emp.EmpName + "<a href='#' onclick='Chat_RemoveGroupMember(" + Emp.AutoId + ",\"" + Emp.EmpName + "\"," + Emp.ChatId + "," + popup_Id + ")' style='color:black;float:right'>&#10005;</a>");
                            $("#" + popup_Id + " tbody").append(row);
                            row = $("#" + popup_Id + " tbody tr:last").clone(true);


                        }
                    } else {
                    }
                }
            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        },
        error: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        }
    });
}
var Chat_MemberAutoId = '';
function Chat_RemoveGroupMember(MemAutoId, MemName,ChatId, popup_Id) {
     
    var em = '';
    $("#div" + popup_Id).hide();
    em = em + '<div style="padding:10px;font-size:18px">Remove from group?</div>';
    em = em + '<div style="padding:8px;height:195px;font-size:12px;">' + MemName +' will be removed from this group conversation.</div>';
    em = em + '<div style="padding:10px;"><button type="button" class="btn btn-sm btn-basic pull-left" onclick="AddMember(' + popup_Id + ',' + ChatId + ')">Cancel</button>';
    em = em + '<button type="button" class="btn btn-sm btn-danger pull-right" onclick="DeleteChat_Member(' + MemAutoId + ',' + ChatId + ',' + popup_Id + ',)">Remove</button></div>';
     
    $("#confirmationdiv" + popup_Id).append(em);
    $("#confirmationdiv" + popup_Id).show();
}
function DeleteChat_Member(MemAutoId,ChatId, popup_Id) {
    $.ajax({
        type: "POST",
        url: "/WebAPI/WChatWindow.asmx/Chat_RemoveGroupMember",
        data: "{'MemberAutoId':'" + MemAutoId + "','ChatId':'" + ChatId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",

        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                Chat_AllGroupMem(popup_Id, ChatId);
                $("#confirmationdiv" + popup_Id).hide();
              
                $("#div" + popup_Id).show();
               
            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        },
        error: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        }
    });
}

function Request_AddPeople(Popup_Id, ChatId) {
    $.ajax({
        type: "POST",
        url: "/WebAPI/WChatWindow.asmx/Request_AddPeople",
        data: "{'ChatId':'" + ChatId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",

        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            $("#btn" + Popup_Id).hide();
            $("#sub" + Popup_Id).show();
            if (response.d != "Session Expired") {
                var DropDown = $.parseJSON(response.d);
                var row = $("#" + Popup_Id + " thead tr").clone(true);
                for (var i = 0; i < DropDown.length; i++) {
                     
                    $("#" + Popup_Id + " tbody tr").remove();

                    var AllDropDownList = DropDown[i];
                    var Member = AllDropDownList.Member;
                    if (Member.length > 0) {
                        for (var j = 0; j < Member.length; j++) {
                            var Emp = Member[j];

                            $(".Chat_GroupMember", row).html("<input type='checkbox' name='table_records' id='chkProducta'  style='width:10% '><a href='#' style='color:black;'><span class='emp_img'><img src='../Img/logo/user.png' style='width:35px;margin: 0px 20px 0px 20px;'/></span> " + Emp.EmpName + "</a>");
                            $(".Chat_MemberId", row).html(Emp.AutoId);
                            $("#" + Popup_Id + " tbody").append(row);
                            row = $("#" + Popup_Id + " tbody tr:last").clone(true);
                        }



                    } else {

                    }
                }
            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        },
        error: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        }
    });
}
function Chat_chkbox_Onclick(Popup_Id, ChatId) {
     
    var flag = false;
    var SendGroupMem = new Array();
    $("#" + Popup_Id + " tbody tr").each(function () {
        var row = $(this);
        if (row.find("td").eq(0).find("input").prop("checked") == true) {
            var G_Mem = {};
            G_Mem.MemberAutoId = row.find('.Chat_MemberId').text();

            SendGroupMem.push(G_Mem);
            flag = true;
        }
    })

    if (flag) {


        $.ajax({
            type: "POST",
            url: "/WebAPI/WChatWindow.asmx/Chat_AddPeople",
            data: "{'dataValue':'" + JSON.stringify(SendGroupMem) + "','ChatId':'" + ChatId + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            async: false,
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == 'false') {
                    swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                }
                else if (result.d == 'Unauthorized access.') {
                    location.href = "/";
                }
                else {
                    //Request_AddPeople(Popup_Id, ChatId);
                    AddRequest_Cancel(Popup_Id);
                    //AddMember(Popup_Id, ChatId)
                }
            },
            error: function (result) {
                swal("Error!", result.d, "error");
            },
            failure: function (result) {
                swal("Error!", result.d, "error");
            }
        });


    } else {
        toastr.error('Please select Member', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}