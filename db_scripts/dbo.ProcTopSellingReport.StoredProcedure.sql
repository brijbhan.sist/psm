
ALTER procedure [dbo].[ProcTopSellingReport]                  
@Opcode int=null,                  
@ProductId int=null,                
@Type int=null,                  
@SalesPersons varchar(max)=null,        
@CustomerType int=null,        
@ProductName varchar(max)=null,                  
@FromDate date =NULL,                                
@ToDate date =NULL,                  
@PageIndex INT=1,                  
@PageSize INT=null,                  
@isException bit out,                                
@exceptionMessage varchar(max) out                    
AS                                
BEGIN                                 
 BEGIN TRY                                
  Set @isException=0                                
  Set @exceptionMessage='Success'                      
        IF @Opcode=41        
  BEGIN   
  select
  (
  SELECT AutoId SId,FirstName+' '+ISNULL(LASTNAME,'') AS SP  FROM EmployeeMaster WHERE EmpType IN (2) and Status=1 order by SP ASC 
   for json path
  )as SPName,
  (
   SELECT AutoId as CID,CustomerType as CUN FROM CustomerType   order by CUN ASC   
    for json path
  ) as CType
   for json path
  END        
  ELSE IF @Opcode=42                      
  BEGIN                                
                 
  select  Row_Number() Over(Order by (case when @Type=1 then SUM(doi.QtyperUnit) else SUM(doi.NetPrice) end) DESC ) as RowNumber,                
  pm.ProductId,pm.ProductName,um.UnitType,  SUM(doi.RequiredQty) AS [DEFAULT_UNIT_COUNT],  SUM(doi.TotalPieces) AS [TOTAL_PICES],                
  SUM(doi.NetPrice) AS [NET_SELES] into #TEMRESULT                
  from OrderItemMaster as doi        
  inner join OrderMaster as om on om.AutoId = doi.OrderAutoId       
  inner join CustomerMaster as cm on om.CustomerAutoId = cm.AutoId        
  inner join ProductMaster as pm on pm.AutoId = doi.ProductAutoId                
  inner join UnitMaster as um on um.AutoId = doi.UnitTypeAutoId   where (ISNULL(@FromDate,'')='' or               
  ISNULL(@ToDate,'')=''   or convert(date,OrderDate)  between convert(date,@FromDate) and CONVERT(date,@ToDate) )      
  and (ISNULL(@SalesPersons,'0')='0' or @SalesPersons='0,' OR om.SalesPersonAutoId                                        
  in (select * from dbo.fnSplitString(@SalesPersons,','))                                        
  )        
  and  (ISNULL(@CustomerType,0)=0 or cm.CustomerType=@CustomerType)      
  and om.Status=11     
  group by pm.ProductId,pm.ProductName,um.UnitType        
  order by (case when @Type=1 then SUM(doi.QtyperUnit) else SUM(doi.NetPrice) end) DESC                
  SELECT * FROM #TEMRESULT                  
  WHERE RowNumber <=@PageSize        
  SELECT case when ISNULL(@PageSize,0)=0 then 0 else COUNT(ProductId) end  AS RecordCount, @PageSize AS PageSize, @PageIndex AS PageIndex FROM #TEMRESULT 
   SELECT ISNULL(sum(TOTAL_PICES),0) AS TOTAL_PICES,ISNULL(sum(NET_SELES),0.00) AS NET_SELES FROM #TEMRESULT     WHERE RowNumber <=@PageSize          
  END                        
 END TRY                                
 BEGIN CATCH                                
 Set @isException=1                                
 Set @exceptionMessage=ERROR_MESSAGE()                                
 END CATCH                                
END 
GO
