
ALTER procedure [dbo].[ProcDetailProductSalesRepor]                      
@Opcode INT=NULL,                      
@ProductAutoId int = null,              
@BrandAutoIdSring varchar(200) = null,                     
@CategoryAutoId int = null,      
@CustomerAutoId  int = null,                    
@SubCategoryId int = null,                      
@OrderStatus int = null,                      
@EmpAutoId int = null,                      
@SalesPerson  int=NULL,                            
@FromDate date =NULL,                      
@ToDate date =NULL,
@SortBySoldQty int=NULL,
@PageIndex INT=1,                      
@PageSize INT=10,                      
@RecordCount INT=null,                      
@isException bit out,                      
@exceptionMessage varchar(max) out                      
as                   
BEGIN                       
 BEGIN TRY                      
  Set @isException=0                      
  Set @exceptionMessage='Success'                      
                      
  IF @Opcode=41                      
  BEGIN                          
	SELECT  PM.AutoId as PID,CONVERT(VARCHAR(10),PM.ProductId )+' - '+ PM.ProductName AS PN FROM ProductMaster AS PM              
	WHERE ((ISNULL(@BrandAutoIdSring,'0')='0')  or (PM.BrandAutoId in (select * from dbo.fnSplitString(@BrandAutoIdSring,','))))               
	AND (ISNULL(@CategoryAutoId,0)=0 OR pm.CategoryAutoId =@CategoryAutoId)                      
	AND (ISNULL(@SubCategoryId,0)=0 OR pm.SubcategoryAutoId =@SubCategoryId)               
	AND (PM.ProductStatus=1 or (Select count(ProductAutoId) from Delivered_Order_Items as OIM 
	where OIM.ProductAutoId=PM.AutoId AND PM.ProductStatus=0)>0) order by CONVERT(VARCHAR(10),PM.ProductId )+' - '+ PM.ProductName ASC    
	for json path
  END                     
   IF @OPCODE=42                      
  BEGIN              
  	SELECT ROW_NUMBER() OVER(ORDER BY ProductId asc) AS RowNumber, * into #Results42                        
   FROM                      
   (                      
	   select ProductId,ProductName,SUM(TotalPiece) as TotalPiece,SUM(PayableAmount) as PayableAmount from (
	  SELECT ProductId,ProductName,                                    
	  sum(ISNULL(QtyDel,0)) as TotalPiece,                      
	  (SUM(NetPrice))  as PayableAmount                      
	  from OrderMaster om inner join Delivered_Order_Items as oim on om.AutoId=oim.OrderAutoId                       
	  inner join ProductMaster as pm on pm.AutoId=oim.ProductAutoId                  
	  LEFT join BrandMaster as bm on bm.AutoId = pm.BrandAutoId                        
	  inner join EmployeeMaster as emp on emp.AutoId=om.SalesPersonAutoId                       
	  inner join CustomerMaster as cm on cm.AutoId=om.CustomerAutoId                         
	  where om.Status=11                 
	  AND  ((ISNULL(@BrandAutoIdSring,'0')='0') or (pm.BrandAutoId in (select * from dbo.fnSplitString(@BrandAutoIdSring,','))))               
	  AND  (ISNULL(@ProductAutoId,0)=0 or oim.ProductAutoId =@ProductAutoId )               
	  AND  (ISNULL(@SalesPerson,'0')='0' or om.SalesPersonAutoId =@SalesPerson )                               
	  AND (ISNULL(@CategoryAutoId,0)=0 OR pm.CategoryAutoId =@CategoryAutoId)                      
	  AND (ISNULL(@SubCategoryId,0)=0 OR pm.SubcategoryAutoId =@SubCategoryId)    
	  AND (ISNULL(@CustomerAutoId,0)=0 OR cm.AutoId =@CustomerAutoId)                         
	  AND (@FromDate is null or @ToDate is null or (CONVERT(date,OrderDate)                       
	  between convert(date,@FromDate) and CONVERT(date,@ToDate))) 
	  AND QtyDel!=0                  
	  group by ProductId,ProductName                
  
	  UNION ALL
  
	  SELECT ProductId,ProductName,                                    
	  sum(ISNULL(oim.TotalPeice,0))*-1 as TotalPiece,                      
	  (SUM(oim.NetAmount))*-1  as PayableAmount                      
	  from CreditMemoMaster om inner join CreditItemMaster as oim on om.CreditAutoId=oim.CreditAutoId                       
	  inner join ProductMaster as pm on pm.AutoId=oim.ProductAutoId                  
	  LEFT join BrandMaster as bm on bm.AutoId = pm.BrandAutoId                        
	  inner join EmployeeMaster as emp on emp.AutoId=om.SalesPersonAutoId                       
	  inner join CustomerMaster as cm on cm.AutoId=om.CustomerAutoId                         
	  where om.Status=3                 
	  AND  ((ISNULL(@BrandAutoIdSring,'0')='0') or (pm.BrandAutoId in (select * from dbo.fnSplitString(@BrandAutoIdSring,','))))               
	  AND  (ISNULL(@ProductAutoId,0)=0 or oim.ProductAutoId =@ProductAutoId )               
	  AND  (ISNULL(@SalesPerson,'0')='0' or om.SalesPersonAutoId =@SalesPerson )                               
	  AND (ISNULL(@CategoryAutoId,0)=0 OR pm.CategoryAutoId =@CategoryAutoId)                      
	  AND (ISNULL(@SubCategoryId,0)=0 OR pm.SubcategoryAutoId =@SubCategoryId)    
	  AND (ISNULL(@CustomerAutoId,0)=0 OR cm.AutoId =@CustomerAutoId)                         
	  AND (@FromDate is null or @ToDate is null or (CONVERT(date,CreditDate)                       
	  between convert(date,@FromDate) and CONVERT(date,@ToDate))) 
	  AND TotalPeice!=0                  
	  group by ProductId,ProductName                

	  ) as t1 
    group by ProductId,ProductName
	having sum(PayableAmount) !=0.00 and sum(TotalPiece) !=0.00
   ) AS T             
           
    SELECT ROW_NUMBER() OVER(ORDER BY(CASE WHEN @SortBySoldQty=1 THEN TotalPiece END) ASC,
	(CASE WHEN @SortBySoldQty=2 THEN TotalPiece END) DESC,
	(CASE WHEN @SortBySoldQty=3 THEN PayableAmount END) ASC,
	(CASE WHEN @SortBySoldQty=4 THEN PayableAmount END) DESC
	) AS RowNumber,* into #Results43 FROM(
	SELECT t.ProductId,t.ProductName,convert(decimal(10,2),t.TotalPiece/CONVERT(decimal(10,2),pd.Qty)) as TotalPiece,t.PayableAmount,
	( um.UnitType +' ['+ convert(varchar(50),Qty) + ' - PC]') as Unit 
	FROM #Results42  as t
	inner join ProductMaster as pm on t.ProductId=pm.ProductId
	inner join UnitMaster as um on um.AutoId=pm.PackingAutoId
	inner join PackingDetails as pd on pd.ProductAutoId=pm.AutoId and pd.UnitType=pm.PackingAutoId                 
      ) as tl   
	  order by 
	(CASE WHEN @SortBySoldQty=1 THEN TotalPiece END)ASC,
	(CASE WHEN @SortBySoldQty=2 THEN TotalPiece END) DESC,
	(CASE WHEN @SortBySoldQty=3 THEN PayableAmount END)ASC,
	(CASE WHEN @SortBySoldQty=4 THEN PayableAmount END) DESC 
                           
	SELECT * FROM #Results43 as r43 WHERE @PageSize=0 or  RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1 
                         
	SELECT COUNT(ProductName) AS RecordCount, case when @PageSize=0 then COUNT(ProductName) else @PageSize end AS PageSize,
	@PageIndex AS PageIndex,Format(getdate(),'MM/dd/yyyy hh:mm tt') as PrintDate FROM #Results43                    
                      
	SELECT ISNULL(SUM(convert(decimal(10,2),t.TotalPiece/CONVERT(decimal(10,2),pd.Qty))),0)  as TotalPiece,ISNULL(SUM(PayableAmount),0.00) as PayableAmount FROM #Results42   as t
	inner join ProductMaster as pm on t.ProductId=pm.ProductId
	inner join PackingDetails as pd on pd.ProductAutoId=pm.AutoId and pd.UnitType=pm.PackingAutoId                         
  END                      
  IF @Opcode=43                      
  BEGIN                      
  SELECT AutoId as CId,CategoryName as CN from CategoryMaster where Status=1  order by CN ASC   
  for json path
  END                      
    IF @Opcode=44                      
  BEGIN                      
 SELECT AutoId as  SCID,SubcategoryName as SCN from SubCategoryMaster where               
 (isnull(@CategoryAutoId,0)=0 or CategoryAutoId=@CategoryAutoId )and Status=1 order by SCN ASC     
    for json path 
END                      
  IF @Opcode=45                      
  BEGIN 
  select
  (
  SELECT AutoId AS EID,FirstName + ' ' + LastName as EN FROM EmployeeMaster where EmpType=2 and status=1 order by FirstName + ' ' + LastName ASC                              
  for json path
  )as SalesPer,
  (
  select AutoId as BID,BrandName as BN from BrandMaster where status = 1 order by BN asc     
  for json path
  )as Brand 
  for json path
          
  END                 
  IF @Opcode=46                      
  BEGIN   
	select AutoId as CUID,CustomerId+' - '+CustomerName as CUN from CustomerMaster as cm where 
	(ISNULL(@SalesPerson,0)=0 OR cm.SalesPersonAutoId =@SalesPerson) AND Status=1 order by CustomerId+' - '+CustomerName asc      
  for json path
  END                  
  END TRY                      
 BEGIN CATCH                      
  Set @isException=1                      
  Set @exceptionMessage=ERROR_MESSAGE()                      
 END CATCH                      
END 
