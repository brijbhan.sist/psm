USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcPageTrackerMaster]    Script Date: 11/12/2020 05:48:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Alter PROCEDURE [dbo].[ProcInvoiceTemplateListMaster]                      
@Opcode int=NULL,                      
@AutoId int=null,                      
@UserAutoId int=null,   
@InvoiceTemplateName varchar(200) = null,
@InvoiceDescription varchar(max) = null,
@PageIndex INT=1,      
@PageSize INT=10, 
@RecordCount INT =null, 
@isException bit out,                      
@exceptionMessage varchar(max) out                      
AS                      
BEGIN                      
	BEGIN TRY                      
		SET @exceptionMessage='Success'                      
		SET @isException=0                               
		IF @Opcode=41
		BEGIN  
			select InvoiceTemplateName,InvoiceTemplateDescription from [InvoiceTemplateListMaster]       
			

			 SELECT ROW_NUMBER() OVER(ORDER BY InvoiceTemplateName ASC) AS RowNumber, * INTO #Results from                      
            (  
			  select  InvoiceTemplateName,InvoiceTemplateDescription
				from [InvoiceTemplateListMaster]    			 
				) as t order by InvoiceTemplateName DESC  
					  SELECT COUNT(*) AS RecordCount, case when @PageSize=0 then COUNT(*) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Results  
					  Select * from #Results  
					  WHERE @PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)   
		 END	
	END TRY                      
	BEGIN CATCH                      
		SET @isException=1                      
		SET @exceptionMessage=ERROR_MESSAGE()              
	END CATCH                      
END 

