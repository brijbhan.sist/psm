Alter PROCEDURE [dbo].[Proc_DeleteAllImage]    
@ProductId varchar(50),
@ImageUrl varchar(250)
AS          
BEGIN 
     Declare @ProductAutoId int
	 IF EXISTS(SELECT * FROM [psmct.a1whm.com].[dbo].[Tbl_ProductImage] AS TPI
	 INNER JOIN [psmct.a1whm.com].[dbo].ProductMaster AS PM ON PM.AutoId=TPI.ProductAutoId WHERE PM.ProductId = @ProductId)               
	 BEGIN  
		  SET @ProductAutoId=(Select AutoId FROM [psmct.a1whm.com].[dbo].ProductMaster Where ProductId=@ProductId)  
		  DELETE FROM [psmct.a1whm.com].[dbo].Tbl_ProductImage where ImageUrl=@ImageUrl AND ProductAutoId=@ProductAutoId                   
		  DELETE FROM [psmct.easywhm.com].[dbo].Tbl_ProductImage where ImageUrl=@ImageUrl AND ProductAutoId=@ProductAutoId
	 END 
	 IF EXISTS(SELECT * FROM [psmpa.a1whm.com].[dbo].[Tbl_ProductImage] AS TPI
	 INNER JOIN [psmpa.a1whm.com].[dbo].ProductMaster AS PM ON PM.AutoId=TPI.ProductAutoId WHERE PM.ProductId = @ProductId)           
	 BEGIN  
		  SET @ProductAutoId=(Select AutoId FROM [psmpa.a1whm.com].[dbo].ProductMaster Where ProductId=@ProductId)                   
		  DELETE FROM [psmpa.a1whm.com].[dbo].Tbl_ProductImage where ImageUrl=@ImageUrl AND ProductAutoId=@ProductAutoId                   
		  DELETE FROM [psmpa.easywhm.com].[dbo].Tbl_ProductImage where ImageUrl=@ImageUrl AND ProductAutoId=@ProductAutoId
	 END 
	 IF EXISTS(SELECT * FROM [psmnpa.a1whm.com].[dbo].[Tbl_ProductImage] AS TPI
	 INNER JOIN [psmnpa.a1whm.com].[dbo].ProductMaster AS PM ON PM.AutoId=TPI.ProductAutoId WHERE PM.ProductId = @ProductId)              
	 BEGIN 
		  SET @ProductAutoId=(Select AutoId FROM [psmnpa.a1whm.com].[dbo].ProductMaster Where ProductId=@ProductId)                    
		  DELETE FROM [psmnpa.a1whm.com].[dbo].Tbl_ProductImage where ImageUrl=@ImageUrl AND ProductAutoId=@ProductAutoId                   
		  DELETE FROM [psmnpa.easywhm.com].[dbo].Tbl_ProductImage where ImageUrl=@ImageUrl AND ProductAutoId=@ProductAutoId
	 END 
	 IF EXISTS(SELECT * FROM [psmwpa.a1whm.com].[dbo].[Tbl_ProductImage] AS TPI
	 INNER JOIN [psmwpa.a1whm.com].[dbo].ProductMaster AS PM ON PM.AutoId=TPI.ProductAutoId WHERE PM.ProductId = @ProductId)                
	 BEGIN 
		  SET @ProductAutoId=(Select AutoId FROM [psmwpa.a1whm.com].[dbo].ProductMaster Where ProductId=@ProductId)                  
		  DELETE FROM [psmwpa.a1whm.com].[dbo].Tbl_ProductImage where ImageUrl=@ImageUrl AND ProductAutoId=@ProductAutoId                   
		  DELETE FROM [psmwpa.easywhm.com].[dbo].Tbl_ProductImage where ImageUrl=@ImageUrl AND ProductAutoId=@ProductAutoId
	 END 
	 IF EXISTS(SELECT * FROM [psmny.a1whm.com].[dbo].[Tbl_ProductImage] AS TPI
	 INNER JOIN [psmny.a1whm.com].[dbo].ProductMaster AS PM ON PM.AutoId=TPI.ProductAutoId WHERE PM.ProductId = @ProductId)      
	 BEGIN 
		  SET @ProductAutoId=(Select AutoId FROM [psmny.a1whm.com].[dbo].ProductMaster Where ProductId=@ProductId)                    
		  DELETE FROM [psmny.a1whm.com].[dbo].Tbl_ProductImage where ImageUrl=@ImageUrl AND ProductAutoId=@ProductAutoId                   
		  DELETE FROM [psmny.easywhm.com].[dbo].Tbl_ProductImage where ImageUrl=@ImageUrl AND ProductAutoId=@ProductAutoId
	 END 
END        