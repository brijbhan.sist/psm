	alter table DraftItemMaster drop column NetPrice
	drop function FN_DraftOrder_NetPrice
	go
	Create FUNCTION  [dbo].[FN_DraftOrder_NetPrice]  
	(  
		@ItemAutoId int  
	)  
	RETURNS decimal(18,2)  
	AS  
	BEGIN  
		DECLARE @NetAmount decimal(18,2)  
		SET @NetAmount=(SELECT (UnitPrice*ReqQty)-(((UnitPrice*ReqQty)*isnull(Oim_Discount,0))/100) FROM DraftItemMaster WHERE ItemAutoId=@ItemAutoId)  
		RETURN @NetAmount  
	END  
	alter table DraftItemMaster add [NetPrice]  AS ([dbo].[FN_DraftOrder_NetPrice]([ItemAutoId]))
	alter table DraftItemMaster add	[Del_ItemTotal]  AS (ReqQty*UnitPrice)