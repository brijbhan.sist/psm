USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcEmailReceiverMaster]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[ProcEmailReceiverMaster]      
@Opcode INT=Null,      
@EmpId varchar(50) = NULL,      
@EmpName varchar(50) = null,      
@EmailID varchar(50) = null,      
@Category varchar(50) = null,      
@CreatedBy int = null,      
@UpdatedBy int = null,      
@PageIndex INT=1,      
@PageSize INT=10,      
@RecordCount INT=null,      
@isException BIT OUT,      
@exceptionMessage VARCHAR(max) Out      
as      
begin      
BEGIN TRY      
	 SET @isException=0      
	 SET @exceptionMessage='Success'      
 if @Opcode = 11      
 begin                       
 if ((select count(EmployeeID) from EmailReceiverMaster where EmployeeID=@EmailID)=0)      
 begin      
	  insert into EmailReceiverMaster(EmployeeID,EmployeeName,EmailID,Category,CreatedBy,CreateDate)      
	  values(@EmpId,@EmpName,@EmailID,@Category,@CreatedBy,GetDate())      
 end      
 else      
 begin      
      SET @isException=1   
	  SET @exceptionMessage='Oops! Something went wrong.Please try later.'   
end      
 end      
 if @Opcode = 41      
  begin      
      select * from EmailReceiverMaster   order by EmployeeName      
  end      
 if @Opcode = 42      
  begin       
       select AutoId as  EMPId,FirstName+' '+LastName as EmployeeName,Email from EmployeeMaster as em    
       where Status=1 and  (Email!='' and Email is not null ) ORDER BY EmployeeName ASC    
  end      
 if @Opcode = 43      
  begin      
      select AutoId as  EMPId,FirstName+' '+LastName as EmployeeName,Email from EmployeeMaster as em where EmpId = @EmpId  order by EmployeeName    
  end      
 if @Opcode = 44      
  begin      
      select * from EmailReceiverMaster Where id=@EmpId       
  end      
 if @Opcode = 31      
  begin       
       delete from EmailReceiverMaster where id = @EmpId       
  end      
  if @opCode=21      
  begin      
   begin try      
   begin tran      
		update EmailReceiverMaster       
		set EmployeeName=@EmpName,EmailID=@EmailID,Category=@Category,UpdatedBy=@UpdatedBy,UpdateDate=GetDate()      
		where id=@EmpId      
   Commit tran      
   end try      
   begin catch      
   rollback tran      
	   SET @isException=1      
	   SET @exceptionMessage='Oops! Something went wrong.Please try later.'      
   end catch      
  end      
end try      
begin catch      
	  SET @isException=1      
	  SET @exceptionMessage='Oops! Something went wrong.Please try later.'        
END CATCH      
end      
GO
