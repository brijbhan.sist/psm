USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[PattyCashLogMasterAvailablebalance]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[PattyCashLogMasterAvailablebalance](@AutoId int)
Returns decimal (12,2)
as
Begin
declare @Result decimal (12,2)
set @Result=(select case when TransactionType='CR' then (CurrentBalance+TransactionAmount) else (CurrentBalance-TransactionAmount) end as AvailableBalance from  PattyCashLogMaster where AutoId=@AutoId)
Return @Result
END
GO
