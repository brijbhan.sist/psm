CREATE OR Alter PROCEDURE [dbo].[ProcPrintPurchaseOrder]                                                                                                        
@Opcode INT=NULL,                                                                                                        
@POAutoId INT=null,                                                                                                         
@type INT=null,                                                                                   
@isException bit out,             
@BulkOrderAutoId varchar(max)=null,                                                                                                       
@exceptionMessage varchar(max) out       
AS                                                                                                        
BEGIN                                                                                                       
  BEGIN TRY                                                                                                        
	Set @isException=0                                                                                                        
	Set @exceptionMessage='Success'                          
	 IF @Opcode=401                            
	 BEGIN 
	   IF @type=1
	   Begin
		Declare @VendorAutoId Int             
                                                                   
		SET @VendorAutoId = (SELECT VenderAutoId FROM [dbo].[PurchaseOrderMaster] WHERE [AutoId]=@POAutoId)   
		declare @Status int=(select status from PurchaseOrderMaster where AutoId=@POAutoId)  
  
		select PONo,Format(PODate,'MM/dd/yyyy hh:mm tt') as PODate,DeliveryDate,NoofItems,TotalOrderAmount,OrderNo,VM.VendorId,VM.VendorName,VM.ContactPerson,  
		VM.CompleteAddress,VM.Cell,vm.VendorName  
		from PurchaseOrderMaster as POM  
		INNER JOIN VendorMaster AS VM on VM.AutoId=POM.VenderAutoId  
		where POM.AutoId=@POAutoId     
   
		Select pm.ProductName,pm.ProductId,um.UnitType,pm.CurrentStock,t.UnitType as DefUnit,
		pp.AutoId,ProductAutoId,Unit,QtyPerUnit,Qty,isnull(TotalPieces,0) as TotalPieces,POAutoId,isnull(UnitPrice,0.00) as UnitPrice,
		isnull(t.CostPrice,0.00) as CostPrice,
		ReceivedPieces,RemainPieces
		from PurchaseProductsMaster as pp          
		inner join ProductMaster pm on pm.AutoId=pp.ProductAutoId          
		inner join UnitMaster um on um.AutoId=pp.Unit   
		left join (  
		Select ums.UnitType,PMS.AutoId,PDS.CostPrice from ProductMaster as PMS  
		Inner Join PackingDetails as PDS on pms.AutoId=pds.ProductAutoId And PMs.PackingAutoId=PDs.UnitType  
		Inner Join UnitMaster as UmS on ums.AutoId=pds.UnitType  
		where pms.AutoId in (Select ProductAutoId FROM PurchaseProductsMaster where POAutoId=@POAutoId)   
		) as t on t.AutoId=pm.AutoId  
		where pp.POAutoId=@POAutoId   
	  END    
	  ELSE                              
	  BEGIN                                
		SELECT SE.[AutoId],[BillNo] as PONo,CONVERT(VARCHAR(20), SE.[BillDate], 101) AS PODate,[Remarks],[VendorAutoId],                                
		SM.StatusType AS Status,SM.AutoId as StatusCode,VM.CompleteAddress,VM.VendorName                              
		FROM [dbo].[Draft_StockEntry] AS SE                                
		INNER JOIN StatusMaster AS SM ON SM.AutoId=SE.Status AND SM.Category='inv' 
		INNER JOIN VendorMaster as VM on VM.AutoId=SE.VendorAutoId WHERE SE.[AutoId]=@POAutoId 
                                
		SELECT BI.[AutoId],[ProductAutoId],PM.ProductId,PM.ProductName,[UnitAutoId],UM.UnitType as DefUnit,[Quantity] as Qty,[TotalPieces],[QtyPerUnit],
		[Price], convert(decimal(18,2),([Quantity]*[Price])) as[TotalPrice] ,                                
		(SELECT TOP 1 CostPrice FROM PackingDetails AS PD WHERE BI.ProductAutoId=PD.ProductAutoId AND BI.UnitAutoId=PD.UnitType)                                
		AS CostPrice                                
		FROM [dbo].[Draft_StockItemMaster] AS BI                                 
		INNER JOIN [dbo].[ProductMaster] As PM ON PM.AutoId = BI.[ProductAutoId]                                
		INNER JOIN [dbo].[UnitMaster] AS UM ON UM.[AutoId] = BI.[UnitAutoId]                                 
		WHERE [BillAutoId]=@POAutoId Order By BI.[AutoId] asc  
      END 
	 END
	  END TRY                                                                                                        
	  BEGIN CATCH                                                
		  Set @isException=1                                                                       
		  Set @exceptionMessage='Oops, Something went wrong.Please try later.'                                                                                                        
	  END CATCH                                                                                             
END 