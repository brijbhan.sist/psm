USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcAppProductMaster_New]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE ProcEDURE [dbo].[ProcAppProductMaster_New]                        
 @OpCode int=Null,                      
 @timeStamp datetime =null,                    
 @isException bit out,                        
 @exceptionMessage varchar(max) out                     
AS                        
BEGIN                        
 BEGIN TRY                        
  SET @isException=0                        
  SET @exceptionMessage='Success!!'                        
  IF @Opcode=41                       
  BEGIN       
     
 SELECT convert(varchar(15),ProductId) AS PId,ProductName AS PName,     
 ('http://'+DB_NAME()+[ImageUrl])  as [ImageUrl],(cm.CategoryName)as Cat,    
 (sm.SubcategoryName)as SCat,pm.AutoId,pm.CategoryAutoId as CatAutoId,pm.SubcategoryAutoId as SCatAutoId,ISNULL(Stock,0) as CStock    
 ,ISNULL(WeightOz,0) as WOz,ISNULL(MLQty,0) as MLQty,ISNULL(P_CommCode,0) as [P_CommCode],isnull(pm.IsApply_ML,0) as IsApply_ML,  
 isnull(pm.IsApply_Oz,0) as IsApply_Oz,   
 isnull((select top 1 autoid from [PackingDetails]as tem where tem.[unittype]=[PackingAutoid] and tem.[ProductAutoId]=pm.autoid),0) as DPackAutoId,  
 (select um.UnitType as UnitType,ISNULL(Location,'') as [Location]    
 ,pd.AutoId as [autoid],pd.productAutoid as [PAutoId],ISNULL(pd.Qty,0) as [Qty],    
 pd.UnitType as [UTAutoId],isnull(pd.EligibleforFree,0) as EFF,     
 ISNULL(price,0) as [Price],ISNULL(P_SRP,0) as [SRP],ISNULL(MinPrice,0) as [MPrice],    
 ISNULL(WHminPrice,0) as [WHminPrice],ISNULL(CostPrice,0) as [CPrice],ISNULL(P_CommCode,0) as [CommCode] ,  
 isnull((case when P_SRP>0 then CONVERT(DECIMAL(10,2),((P_SRP-([Price]/(case [Qty] when 0 then 1 else [Qty] end)))/(case P_SRP   
   when 0 then 1 else P_SRP end)) * 100) else 0.00 end),0.00) as [GP]  
 ,    
 ISNULL((   
	SELECT convert(nvarchar(25), BARCODE) as BC   
	FROM ItemBarcode IBC    
	WHERE IBC.ProductAutoId=pd.ProductAutoId AND IBC.UnitAutoId=PD.UNITTYPE    
	FOR   JSON path,INCLUDE_NULL_VALUES
),'[]')   AS [BCList]   
 from  PackingDetails as pd        
 inner  join UnitMaster as um on um.AutoId=pd.UnitType where pd.ProductAutoId=pm.AutoId FOR JSON path,INCLUDE_NULL_VALUES)as PTypList  
 FROM ProductMaster as pm     
 inner join CategoryMaster as cm on cm.AutoId=pm.CategoryAutoId    
 inner join SubCategoryMaster as sm on sm.AutoId=pm.SubcategoryAutoId    
  --      where pm.ProductId in (--20028,20029,20030,20031,20032,
		--20033
		----,20034,20035,
	 ----20036,20037,20038,20039
	 --)
 FOR JSON path , WITHOUT_ARRAY_WRAPPER ,INCLUDE_NULL_VALUES    
     
  End                        
 END TRY                        
 BEGIN CATCH                        
  SET @isException=1                        
  SET @exceptionMessage=ERROR_MESSAGE()                        
 END CATCH                        
END
GO
