
ALTER procedure [dbo].[ProcMigrateOrder]
@OpCode int =null
AS   
 BEGIN 
select om.AutoId as orderAutoid,ROW_NUMBER() over(order by orderAutoid) as RowNumber into #ordtemp from OrderMaster as om
inner join DeliveredOrders as do on om.AutoId=do.OrderAutoId
inner join ShippingType as st on st.AutoId=om.ShippingType
where st.MigrateStatus=1 and do.AmtDue=0 and Status=11 and CONVERT(date,OrderDate)>=CONVERT(date,'2020-07-16') and isnull(om.CreditAmount,0)=0
and datediff(day,(
select top 1 PaymentDate from CustomerPaymentDetails as pm
inner join PaymentOrderDetails as pod on pod.PaymentAutoId=pm.PaymentAutoId and pod.OrderAutoId=om.AutoId and pm.Status=0 order by PaymentDate desc
),getdate())>3
and not exists(
select pm.PaymentAutoId from CustomerPaymentDetails as pm
inner join PaymentOrderDetails as pod on pod.PaymentAutoId=pm.PaymentAutoId and pod.OrderAutoId=om.AutoId and pm.Status=0
where (select count(1) from (select distinct OrderAutoId from PaymentOrderDetails where PaymentAutoId=pm.PaymentAutoId) as t)>1  ---Multiple order
)
and not exists(
select pm.PaymentAutoId from CustomerPaymentDetails as pm
inner join PaymentOrderDetails as pod on pod.PaymentAutoId=pm.PaymentAutoId and pod.OrderAutoId=om.AutoId and pm.Status=0 and PaymentMode=5  --store credit
)
and not exists(
select 1  from [dbo].[CustomerPaymentDetails] as cpd where 
cpd.PaymentAutoId in (select od.PaymentAutoId from PaymentOrderDetails as od where od.OrderAutoId=om.AutoId)	
and cpd.NewCreditAmount>0
)


	declare @i int=1,@j int=(select count(1) from #ordtemp),@ord int
	while(@i<=@j)
	begin
		set @ord=(select orderAutoid from #ordtemp where RowNumber=@i)
	 
		EXEC [dbo].[Proc_Account_OrderMaster] 
		@opCode=211,
		@OrderAutoId=@ord,
		@isException=0,                      
		@exceptionMessage='';
		SET @i=@i+1
	end
	drop table #ordtemp
END
