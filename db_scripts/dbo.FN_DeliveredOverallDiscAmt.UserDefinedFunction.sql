USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_DeliveredOverallDiscAmt]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION  [dbo].[FN_DeliveredOverallDiscAmt]
(
	 @orderAutoId int
)
RETURNS decimal(10,2)
AS
BEGIN
	DECLARE @OverallDiscAmt decimal(10,2)
	set @OverallDiscAmt=isnull((select OverallDiscAmt from OrderMaster where AutoId=@orderAutoId)	,0.00)
	RETURN @OverallDiscAmt
END
GO
