Alter procedure ProcBarcodePrint                                     
@Opcode int=Null,  
@CategoryAutoId int=null,
@SubCategoryAutoId int=null,
@UnitType int=null,
@ProductAutoId int=null,
@Barcode varchar(50)=null,
@PrintOption int=null,
@Count int=0,
@Length int=null,
@isException bit out,                                     
@exceptionMessage varchar(max) out
AS
BEGIN
     SET @isException=0
	 SET @exceptionMessage='success'
	 IF @Opcode=401 --All Category
	 BEGIN
		select AutoId,CategoryName from CategoryMaster order by CategoryName
		for json path
	 END
	 ELSE IF @Opcode=402 --All SubCategory
	 BEGIN	      
		select AutoId,SubcategoryName from SubCategoryMaster 
		where (@CategoryAutoId is null or @CategoryAutoId=0 or  CategoryAutoId=@CategoryAutoId)
		order by SubcategoryName 
		for json path
	 END
	 ELSE IF @Opcode=403 --All Product 
	 BEGIN
	   IF @PrintOption IN (1,2,3) 
	   Begin
	        select
			(
			select distinct PM.AutoId as ID,Convert(varchar(10),[ProductId]) + '--' + [ProductName] as PRM from ProductMaster as PM
			Inner Join ItemBarcode as ib on Pm.AutoId=ib.ProductAutoId
			where ProductStatus=1
			and (@CategoryAutoId is null or @CategoryAutoId=0 or CategoryAutoId=@CategoryAutoId )
			and (@SubCategoryAutoId is null or @SubCategoryAutoId=0 or SubcategoryAutoId=@SubCategoryAutoId )
			and (1=(case when @PrintOption=3 and Len(Barcode)=14 then 1
					WHEN @PrintOption in (1,2) and LEN(barcode) in (11,12) then 1 else 0 end ))
			order by PRM  ASC 
			for json path  
			) as Product	
			for json path 
       END
	   ELSE
	   BEGIN
	        select
			(
			select PM.AutoId as ID,Convert(varchar(10),[ProductId]) + '--' + [ProductName] as PRM from ProductMaster as PM
			where ProductStatus=1
			and (@CategoryAutoId is null or @CategoryAutoId=0 or CategoryAutoId=@CategoryAutoId )
			and (@SubCategoryAutoId is null or @SubCategoryAutoId=0 or SubcategoryAutoId=@SubCategoryAutoId )
			order by PRM  ASC 
			for json path  
			) as Product	
			for json path 
	   END
	 END
	 ELSE IF @Opcode=404 -- defalut Unit Type
	 BEGIN 
	     IF @PrintOption IN (1,2,3) 
		 begin
			select
			(
			select distinct PM.ProductAutoId,UM.AutoId,UM.UnitType from PackingDetails as PM 
			inner join UnitMaster UM on UM.AutoId=PM.UnitType
			INNER JOIN ItemBarcode as IB on IB.ProductAutoId=PM.ProductAutoId AND IB.UnitAutoId=UM.AutoId
			where PM.ProductAutoId=@ProductAutoId and 
			( 
			1=(case when @PrintOption=3 and Len(Barcode)=14 then 1
				WHEN @PrintOption in (1,2) and LEN(barcode) in (11,12) then 1 else 0 end )
			) 
			for json path 
			)as Allunit, 
			(
			select PM.PackingAutoId from ProductMaster as PM
			inner join PackingDetails as PD on PM.PackingAutoId=PD.UnitType and PM.AutoId=PD.ProductAutoId
			where PM.AutoId=@ProductAutoId  
			for json path   
			)as Defaltunit 
			for json path
		end
		else
		begin
			select
			(
			select distinct PM.ProductAutoId,UM.AutoId,UM.UnitType from PackingDetails as PM 
			inner join UnitMaster UM on UM.AutoId=PM.UnitType
			where PM.ProductAutoId=@ProductAutoId 
			for json path 
			)as Allunit, 
			(
			select PM.PackingAutoId from ProductMaster as PM
			inner join PackingDetails as PD on PM.PackingAutoId=PD.UnitType and PM.AutoId=PD.ProductAutoId
			where PM.AutoId=@ProductAutoId  
			for json path   
			)as Defaltunit 
			for json path
		end
	 END
	 ELSE IF @Opcode=405 -- Add Barcode Details
		BEGIN
		IF @PrintOption IN (1,2,3) 
		BEGIN
			set @Count=(select COUNT(*) from ItemBarcode where ProductAutoId=@ProductAutoId and 
			UnitAutoId=@UnitType and 
			( 
				1=(case when @PrintOption=3 and Len(Barcode)=14 then 1
					  WHEN @PrintOption in (1,2) and LEN(barcode) in (11,12) then 1 else 0 end )
			)
			group by ProductAutoId)
		END
		ELSE
		BEGIN
			set @Count=(select COUNT(*) from ItemBarcode where ProductAutoId=@ProductAutoId and UnitAutoId=@UnitType group by ProductAutoId)
		END
		select @Count as TotalBarcodeCOUNT
		if @Count>1
		BEGIN
		    IF @PrintOption IN (1,2,3) 
			BEGIN
				select ProductAutoId,Barcode,(
				select top 1 isnull(format(OrderDate,'MM/dd/yyyy'),'') as LastUseDate from OrderItemMaster as oim 
				inner join OrderMaster as om on om.AutoId=oim.OrderAutoId
				where oim.Barcode=ib.Barcode
				order by om.OrderDate desc
				) as LastUsedDate,(
				select count(Barcode) from OrderItemMaster as oim 
				inner join OrderMaster as om on om.AutoId=oim.OrderAutoId
				where oim.Barcode=ib.Barcode
				) as count
				from ItemBarcode as ib where  ProductAutoId=@ProductAutoId and UnitAutoId=@UnitType 
				 and 
				( 
					1=(case when @PrintOption=3 and Len(Barcode)=14 then 1
						  WHEN @PrintOption in (1,2) and LEN(barcode) in (11,12) then 1 else 0 end )
				)
				order by LastUsedDate desc
			END
			ElSE
			BEGIN
				select ProductAutoId,Barcode,(
				select top 1 isnull(format(OrderDate,'MM/dd/yyyy'),'') as LastUseDate from OrderItemMaster as oim 
				inner join OrderMaster as om on om.AutoId=oim.OrderAutoId
				where oim.Barcode=ib.Barcode
				order by om.OrderDate desc
				) as LastUsedDate,(
				select count(Barcode) from OrderItemMaster as oim 
				inner join OrderMaster as om on om.AutoId=oim.OrderAutoId
				where oim.Barcode=ib.Barcode
				) as count
				from ItemBarcode as ib where  ProductAutoId=@ProductAutoId and UnitAutoId=@UnitType
				order by LastUsedDate desc
			END
				select top 1 ProductId,ProductName,(select UnitType from UnitMaster where AutoId=@UnitType) as UnitType
				from ProductMaster where AutoId=@ProductAutoId
		END
		else
		BEGIN
		     IF @PrintOption IN (1,2,3) 
			 BEGIN
				select PM.ProductId,PM.ProductName,UM.UnitType,Barcode,PM.P_SRP as SRP,(
				select top 1 isnull(format(OrderDate,'MM/dd/yyyy'),'') as LastUseDate from OrderItemMaster as oim 
				inner join OrderMaster as om on om.AutoId=oim.OrderAutoId
				where oim.Barcode=bc.Barcode
				order by om.OrderDate desc
				) as LastUsedDate from ItemBarcode as BC
				inner join ProductMaster PM on PM.AutoId=BC.ProductAutoId
				inner join UnitMaster UM on UM.AutoId=BC.UnitAutoId
				where  ProductAutoId=@ProductAutoId and BC.UnitAutoId=@UnitType 
				 and 
				( 
					1=(case when @PrintOption=3 and Len(Barcode)=14 then 1
						  WHEN @PrintOption in (1,2) and LEN(barcode) in (11,12) then 1 else 0 end )
				)
			END 
			ElSE
			BEGIN
				select PM.ProductId,PM.ProductName,UM.UnitType,Barcode,PM.P_SRP as SRP,(
				select top 1 isnull(format(OrderDate,'MM/dd/yyyy'),'') as LastUseDate from OrderItemMaster as oim 
				inner join OrderMaster as om on om.AutoId=oim.OrderAutoId
				where oim.Barcode=bc.Barcode
				order by om.OrderDate desc
				) as LastUsedDate from ItemBarcode as BC
				inner join ProductMaster PM on PM.AutoId=BC.ProductAutoId
				inner join UnitMaster UM on UM.AutoId=BC.UnitAutoId
				where  ProductAutoId=@ProductAutoId and BC.UnitAutoId=@UnitType
			END
		END	     		  
	 END
	 ELSE IF @Opcode=406 -- All Order Type
	 BEGIN
		select pm.ProductId,PM.ProductName,UM.UnitType,Barcode,PM.P_SRP as SRP from ItemBarcode as BC
		inner join ProductMaster PM on PM.AutoId=BC.ProductAutoId
		inner join UnitMaster UM on UM.AutoId=BC.UnitAutoId
		where Barcode=@Barcode
	 END
END