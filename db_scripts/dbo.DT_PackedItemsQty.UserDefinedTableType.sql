USE [psmnj.a1whm.com]
GO
DROP  PROCEDURE [dbo].[Proc_Packer_OrderMaster]
drop TYPE [dbo].[DT_PackedItemsQty]


CREATE TYPE [dbo].[DT_PackedItemsQty] AS TABLE(
	[ProductAutoId] [int] NULL,
	[UnitAutoId] [int] NULL,
	[Barcode] [varchar](50) NULL,
	[QtyShip] [int] NULL,
	[Pcs] [int] NULL,
	[OldPacked] [int] NULL,
	--[UnitPrice] [decimal](8, 2) NULL,
	--[NetPrice] [decimal](8, 2) NULL,
	[RemainQty] [int] NULL,
	[isfreeitem] [int] NULL,
	[IsExchange] [int] NULL
)
GO
