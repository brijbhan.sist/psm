USE [psmnj.a1whm.com]
GO

Drop procedure ProcProductMaster
DROP TYPE [dbo].[DT_dtbulkUnit]
GO

/****** Object:  UserDefinedTableType [dbo].[DT_dtbulkUnit]    Script Date: 01-13-2021 23:32:19 ******/
CREATE TYPE [dbo].[DT_dtbulkUnit] AS TABLE(
	[ProductAutoId] [int] NULL,
	[UnitAutoId] [int] NULL,
	[Qty] [int] NULL,
	[CostPrice] [decimal](12, 3) NULL,
	[BasePrice] [decimal](12, 3) NULL,
	[RetailPrice] [decimal](12, 3) NULL,
	[WHPrice] [decimal](12, 3) NULL,
	[SETDefault] [int] NULL,
	[SETFree] [int] NULL,
	[Status] [int] NULL,
	[Rack] [varchar](15) NULL,
	[Section] [varchar](15) NULL,
	[Row] [varchar](15) NULL,
	[BoxNo] [varchar](15) NULL
)
GO


