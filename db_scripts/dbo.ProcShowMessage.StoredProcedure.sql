
ALTER PROCEDURE [dbo].[ProcShowMessage]
@Opcode INT=NULL,
@EmpAutoId INT=NULL,
@EmpType INT=NULL,
@AutoId int=null,
@isException bit out,
@exceptionMessage varchar(max) out
AS
BEGIN
	BEGIN TRY
		Set @isException=0
		Set @exceptionMessage='Success'
		IF @Opcode = 41
		BEGIN
				SELECT * from (
					SELECT MM.[AutoId] As MessageAutoId,[Message],[Title],temp1.EmpAutoId,temp1.EmpType,
					(
					select top 1 '/MessageImageAttachments/'+messageImageUrl  from [MessageImageurl] as im where im.MessageAutoId=mm.AutoId
					) as ImageURL,ISNULL(mm.IsRepeat,0) as IsRepeat 
					FROM [dbo].[MessageMaster] AS MM				
					LEFT JOIN [MessageAssignTo]  as temp1  ON temp1.[MessageAutoId] = MM.[AutoId] where StartDate < getdate() and ExpiryDate > getdate() and temp1.EmpAutoId=@EmpAutoId
					UNION
					SELECT MM.[AutoId] As MessageAutoId,[Message],[Title],T1.EmpAutoId,T1.EmpType,
					(
					select top 1 '/MessageImageAttachments/'+messageImageUrl  from [MessageImageurl] as im where im.MessageAutoId=mm.AutoId
					) as ImageURL,ISNULL(mm.IsRepeat,0) as IsRepeat
					FROM [dbo].[MessageMaster] AS MM				
					LEFT JOIN [dbo].[MessageAssignTo] AS T1 on T1.[MessageAutoId] = MM.[AutoId]	where StartDate < getdate() and ExpiryDate > getdate() and T1.EmpType=@EmpType
				) as t where MessageAutoId not in (select lg.MessageAutoId from tbl_MessageReadLog lg where lg.EmpAutoId=@EmpAutoId)
				or IsRepeat=1
		END
		ELSE IF @Opcode = 11
		BEGIN
			INSERT into tbl_MessageReadLog (MessageAutoId,EmpAutoId,readstatus,readdate)
			values(@AutoId,@EmpAutoId,1,GETDATE())
		END
	END TRY
	BEGIN CATCH
		Set @isException=1
		Set @exceptionMessage='Oops! Something went wrong.Please try later.'--ERROR_MESSAGE()
	END CATCH
END



GO
