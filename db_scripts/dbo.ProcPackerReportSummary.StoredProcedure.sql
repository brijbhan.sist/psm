USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcPackerReportSummary]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
CREATE Procedure [dbo].[ProcPackerReportSummary]    
@Opcode INT=NULL,    
@PackerAutoId varchar (10) = null,     
@DateFrom date =NULL,    
@DateTo date =NULL,    
@PageIndex INT=1,    
@PageSize INT=10,    
@RecordCount INT=null,    
@isException bit out,    
@exceptionMessage varchar(max) out     
 as    
    
 BEGIN     
 BEGIN TRY    
  Set @isException=0    
  Set @exceptionMessage='Success'       
  if @Opcode = 41    
  begin    
  select em.AutoId,FirstName + ' ' + LastName as PackerName from EmployeeMaster as em     
  inner join EmployeeTypeMaster as etm on etm.AutoId = em.EmpType    
  where etm.TypeName = 'Packer'  order by PackerName ASC   
  end    
  if @Opcode = 42    
  begin    
  
 Select ROW_NUMBER() OVER(ORDER BY PackerAssignDate DESC) AS RowNumber,FORMAT(PackerAssignDate,'MM/dd/yyyy') as PackerAssignDate,warehouse,PackerName,SUM(totalAssignOrder) as TotalAssignOrder,SUM(PackedOrder) as TotalPackedOrder,SUM(totalAssignOrder)-
SUM(PackedOrder) as TotalPendingOrder into #RESULT  
 from     
    
(select CONVERT(date, PackerAssignDate,101) as PackerAssignDate,count(OrderNo) as totalAssignOrder,0 PackedOrder,(em.FirstName + ' ' + em.LastName) as warehouse,(em2.FirstName + ' ' + em2.LastName) as PackerName    
 from OrderMaster om     
 inner join EmployeeMaster em on em.AutoId = om.WarehouseAutoId    
 inner join EmployeeMaster em2 on em2.AutoId = om.PackerAutoId    
  where  PackerAssignDate is not null     
  and (@PackerAutoId is null or @PackerAutoId = 0 or om.PackerAutoId = @PackerAutoId)    
  and (@DateFrom is null or @DateFrom = '' or @DateTo is null or @DateTo = '' or           
     (CONVERT(date,PackerAssignDate,101)  between CONVERT(date,@DateFrom) and CONVERT(date,@DateTo)))    
 group by Convert(Date,PackerAssignDate,101),(em.FirstName + ' ' + em.LastName),(em2.FirstName + ' ' + em2.LastName)    
 union     
 select CONVERT(date, PackerAssignDate,101) as PackerAssignDate,0 as totalAssignOrder,count(OrderNo) PackedOrder,(em.FirstName + ' ' + em.LastName) as warehouse,(em2.FirstName + ' ' + em2.LastName) as PackerName    
 from OrderMaster om    
  inner join EmployeeMaster em on em.AutoId = om.WarehouseAutoId    
  inner join EmployeeMaster em2 on em2.AutoId = om.PackerAutoId    
  where om.Status >= 3 and PackerAssignDate is not null    
  and (@PackerAutoId is null or @PackerAutoId = 0 or om.PackerAutoId = @PackerAutoId)    
  and (@DateFrom is null or @DateFrom = '' or @DateTo is null or @DateTo = '' or           
     (CONVERT(date,PackerAssignDate)  between CONVERT(date,@DateFrom) and CONVERT(date,@DateTo)))    
 group by CONVERT(date, PackerAssignDate,101),(em.FirstName + ' ' + em.LastName),(em2.FirstName + ' ' + em2.LastName)) as t    
    
 group by PackerAssignDate ,PackerName,warehouse    
    
  SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize=0 then COUNT(RowNumber) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #RESULT    
    
   SELECT * FROM #RESULT    
   WHERE (@PageSize = 0 or (RowNumber BETWEEN (@PageIndex -1) * @PageSize + 1 AND (((@PageIndex -1) * @PageSize + 1) + @PageSize)-1))    

   SELECT ISNULL(SUM(TotalPendingOrder),0) AS TotalPendingOrder,ISNULL(SUM(TotalPackedOrder),0) AS TotalPackedOrder, isnull(Sum(TotalAssignOrder),0) as TotalAssignOrder from  #RESULT
  end    
    
  END TRY    
 BEGIN CATCH    
  Set @isException=1    
  Set @exceptionMessage=ERROR_MESSAGE()    
 END CATCH               
END   
GO
