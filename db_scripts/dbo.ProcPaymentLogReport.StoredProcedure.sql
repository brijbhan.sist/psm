ALTER PROCEDURE [dbo].[ProcPaymentLogReport]                
@Opcode INT=Null,                
@CustomerAutoId INT=NULL,                 
@EmpAutoId INT=NULL,                 
@PaymentAutoId  INT=NULL,                 
@FromDate date=NULL,                
@ToDate date=NULL,                
@SettledFromDate date=NULL,                
@SettledToDate date=NULL,                
@Status int =NULL,                
@PayId varchar(150)=null,        
@PaymentMode int=null,  
@Range int=null,      
@PaymentAmount decimal(10,2)=null,            
@PageIndex INT=1,                
@PageSize INT=10,                
@RecordCount INT=null,                
@isException BIT OUT,                
@exceptionMessage VARCHAR(max) OUT                
AS                
BEGIN                
  BEGIN TRY                
  SET @isException=0                
  SET @exceptionMessage='Success!!'                
  IF @Opcode=41                
   BEGIN                 
       SELECT ROW_NUMBER() OVER(ORDER BY PaymentAutoId desc) AS RowNumber, * INTO #ResultsShip from                
    (                 
                      
		SELECT  cm.AutoId as customerAutoId,om.AutoId,spm.PaymentAutoId,PayId,CustomerId,CustomerName,
		FirstName+' ' +ISNULL(LastName,'') + ' <b>('+empt.TypeName+')</b>'  as ReceivedBy,                
		spm.ReceivedDate as DateReceived,                
		CONVERT(varchar(10),ReceivedDate,101) as ReceivedDate,                
		CONVERT(varchar(10),cpd.PaymentDate,101)  as SettledDate,                
		PayType as PayCode,                
		spm.ReceivedAmount,otm.OrderType + case when om.AutoId is null then '' else ' <b>('+om.OrderNo+')</b>'  end AS PayType,                
		case when ISNULL(om.status,11)= 11 then 0 else 1 end  as ProcessStatus,spm.PaymentMode as Pmode,   
		pm.PaymentMode as PaymentMode,ReferenceId,Remarks,StatusType as Status,spm.Status as StatusCode,
		CASE WHEN spm.status=5 and spm.paymentMode=2 then spm.ChequeRemarks else spm.CancelRemarks end as CancelRemarks,               
		spm.ChequeNo,CONVERT(varchar(10),spm.ChequeDate,101) as ChequeDate                
		from SalesPaymentMaster AS spm                
		INNER JOIN CustomerMaster AS CM ON spm.customerAutoid = CM.AutoId                    
		INNER JOIN EmployeeMaster AS emp ON emp.Autoid = spm.ReceiveBy                
		INNER JOIN EmployeeTypeMaster AS empt ON emp.EmpType = empt.AutoId                
		INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId=spm.Status and SM.[Category]='Pay'                
		left join OrderMaster as om on om.AutoId=spm.OrderAutoId       
		inner join PAYMENTModeMaster as pm on pm.AutoID=spm.PaymentMode 		  
		left join CustomerPaymentDetails as cpd on cpd.refPaymentId=spm.PaymentAutoId    
		inner join OrderTypeMaster as otm on otm.AutoId=spm.PayType and Type='Payment'
		WHERE                 
		(@CustomerAutoId=0 or @CustomerAutoId is null or spm.CustomerAutoId =@CustomerAutoId)                
		and (@EmpAutoId=0 or @EmpAutoId is null or spm.ReceiveBy =@EmpAutoId)                    
		and  (@PayId='' or @PayId is null or  PayId like '%'+ @PayId +'%')                 
		and (@Status=0 or @Status is null or spm.Status =@Status)        
		and (@PaymentMode=0 or @PaymentMode is null or spm.PaymentMode =@PaymentMode)    
		and ((@Range=1 and  spm.ReceivedAmount=@PaymentAmount) or (@Range=2 and 
		spm.ReceivedAmount>@PaymentAmount) or (@Range=3 and  spm.ReceivedAmount<@PaymentAmount) or    
		(@Range=0))    
     
    
		and (@FromDate is null or @FromDate = '' or @Todate is null or @Todate = '' or                
		(CONVERT(DATE,spm.ReceivedDate) between @FromDate and @Todate))                 
		and (@SettledFromDate is null or @SettledFromDate = '' or @SettledTodate is null or @SettledTodate = '' or                
		(CONVERT(DATE,cpd.PaymentDate) between @SettledFromDate and @SettledTodate and cpd.Status=0)                
		)                 
                            
		) as t Order by PaymentAutoId desc                
                    
    SELECT COUNT(*) AS RecordCount, case when @PageSize=0 then COUNT(*) else @PageSize end AS PageSize, 
	@PageIndex AS PageIndex,ISNULL(SUM(ReceivedAmount),0.00) as ReceivedAmount,Format(getdate(),'MM/dd/yyyy hh:mm tt') as PrintDate FROM #ResultsShip   
	
    SELECT * FROM #ResultsShip                
    WHERE  ISNULL(@PageSize,0)=0 or ( RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)                
    IF(ISNULL(@PageSize,0)=0)                
    BEGIN                
		SELECT * FROM CompanyDetails                
    END              
   -- SELECT ISNULL(SUM(ReceivedAmount),0.00) as ReceivedAmount FROM #ResultsShip           
   END   
   
   ELSE IF @Opcode=42                
    BEGIN                
	  select
	   (
		SELECT AutoId as CUID ,CustomerId+' '+CustomerName AS CUN  FROM CustomerMaster WHERE STATUS=1   order by CUN
		for json path
	   )as AllCust,
	   (
		SELECT em.AutoId as EId, FirstName + ' ' + LastName +' ('+ etm.TypeName+')' AS EMN  from EmployeeMaster  as em                
		inner join EmployeeTypeMaster as etm on etm.AutoId=em.EmpType where Status=1 and etm.AutoId in (2,5,10) order by EMN ASC           
		for json path
		) as AllEmp,
		(
		SELECT * FROM StatusMaster WHERE Category='Pay' order by StatusType ASC 
		for json path
		) as AllSt,
		(
		select AutoID,PaymentMode from [dbo].[PAYMENTModeMaster] where Status=1  order by PaymentMode   
		for json path
		)as PMode
		for json path
   END 
   
   ELSE IF @Opcode=43                
   BEGIN                
  SELECT CompanyName,Address,EmailAddress,Website,MobileNo,FaxNo,Logo from CompanyDetails    
  SELECT SPM.PaymentAutoId,CM.CustomerName,pmm.PaymentMode,SPM.ReceivedAmount,CONVERT(varchar(10),PaymentDate,101) as ReceivedDate,                
  PaymentId  PayId,(SELECT [dbo].[fNumToWords] (SPM.ReceivedAmount)) as amountinword,    
  case when SPM.PaymentMode=2 then ISNULL(ChequeNo,'') else ISNULL(ReferenceId,'') end as PaymentReference,ChequeNo,    
  ReferenceId FROM CustomerPaymentDetails AS SPM                 
  INNER JOIN CustomerMaster AS CM ON SPM.CustomerAutoId=CM.AutoId                
  LEFT JOIN SalesPaymentMaster AS SP ON SP.PaymentAutoId=SPM.refPaymentId       
  left join PAYMENTModeMaster as pmm ON SPM.PaymentMode=pmm.AutoID       
  where SPM.PaymentAutoId=@PaymentAutoId              
             
  Select (CONVERT(varchar(10),OrderDate,101)) as  OrderDate,OrderNo,po.ReceivedAmount,                            
  otm.OrderType AS OrderType  from PaymentOrderDetails as po                            
  inner join OrderMaster as om on om.AutoId=po.OrderAutoId 
  INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
  where PaymentAutoId =@PaymentAutoId             
               
   END                
  END TRY                
  BEGIN CATCH                
  SET @isException=1                
  SET @exceptionMessage=ERROR_MESSAGE()                
  END CATCH                
                
                
END 
GO
