Alter PROCEDURE [dbo].[ProcDailyOrderPaymentStatus]                                                                    
@Opcode INT=NULL,                                                                                                                                    
@EmpAutoId  INT=NULL,                                                                    
@DriverAutoId INT=null,                                                                    
@OrderStatus INT=NULL,                                                                                                        
@PaymentStatus VARCHAR(100)=NULL,                                                                                                                         
@FromDate date =NULL,                                                                    
@ToDate date =NULL,                             
@CloseOrderFromDate date =NULL,                                                                    
@CloseOrderToDate date =NULL,     
@PageIndex INT=1,                                                                    
@PageSize INT=10,                                                                    
@RecordCount INT=null,                                                                    
@isException bit out,                                                                    
@exceptionMessage varchar(max) out                                                                    
AS                                                                    
BEGIN                                                                     
 BEGIN TRY                                                                    
  Set @isException=0                                                                    
  Set @exceptionMessage='Success'    
  IF @Opcode=41                                                                    
   BEGIN                                                                    
     select
	 (
    SELECT AutoId as SId,StatusType as ST FROM [dbo].[StatusMaster] WHERE  [Category]='OrderMaster' 
	AND [AutoId] IN(1,2,3,4,5,6,9,10,11)  order by ST     
	for json path
	) as AllStatus,
	(
    SELECT AutoId as SPId,(FirstName +' '+ISNULL(LastName,'')) AS SPN FROM EmployeeMaster WHERE EmpType =2 and status=1 order by SPN ASC 
	for json path
	) as SPeson,
	(
    SELECT AutoId as DId,(FirstName +' '+ISNULL(LastName,'')) AS DN FROM EmployeeMaster WHERE EmpType =5 and status=1  order by DN ASC 
	for json path
	) as Driver
	for json path
   END      
  ELSE IF @OPCODE=42                                                                    
  BEGIN                                                                    
		select ROW_NUMBER() over(order by OrderNo) as RowNumber, * into #Result FROM (                                                                          
		SELECT (convert(varchar(10),OrderDate,101) )OrderDate,OrderNo,cm.CustomerName,sm.StatusType as OrderStatus,                                                                    
		(                                                                    
		CASE                                                                
		when do.OrderAutoId is null then 'N/A'                                                                     
		WHEN ISNULL(DO.AmtPaid,0)=0 THEN 'NOT PAID'                                       
		When om.PayableAmount=isnull(DO.AmtPaid,0.00) then 'PAID'                                                                    
		else 'PARTIAL PAID'                             END                                 
		) AS PaymentStatus,                                                                    
		om.PayableAmount AS OrderTotal,                                                                    
		isnull(DO.AmtPaid,0.00) AS PaidAmount,                                                                    
		(om.PayableAmount-isnull(DO.AmtPaid,0.00)) AS DueAmount,                                  
		(select em.FirstName +' '+ em.LastName from EmployeeMaster as em where AutoId=om.SalesPersonAutoId) as SalesPerson,                                                                     
		isnull((select em.FirstName +' '+ em.LastName from EmployeeMaster as em where AutoId=om.WarehouseAutoId),'N/A') as WarehouseManager,                                                       
		isnull((select em.FirstName +' '+ em.LastName from EmployeeMaster as em where AutoId=om.PackerAutoId),'N/A') as Packer,                                                                     
		isnull((select em.FirstName +' '+ em.LastName from EmployeeMaster as em where AutoId=om.ManagerAutoId),'N/A') as SalesManager,                                                                   
		isnull((select em.FirstName +' '+ em.LastName from EmployeeMaster as em where AutoId=Driver),'N/A') as Driver,                                                            
		isnull((select em.FirstName +' '+ em.LastName from EmployeeMaster as em where AutoId=AccountAutoId),'N/A') as Account,om.Status                                                                    
		FROM OrderMaster AS OM                                                                     
		inner join customerMaster as cm on cm.autoid=om.CustomerAutoId                                                                     
		left JOIN DeliveredOrders AS DO ON OM.AutoId=DO.OrderAutoId                                                                    
		inner join StatusMaster as sm on sm.AutoId=om.Status and sm.category='OrderMaster'                                                                    
		WHERE om.STATUS!=8 and om.STATUS!=7                                                                    
		and (ISNULL(@OrderStatus,0)=0 or om.Status=@OrderStatus)                                                         
		and     (                                                                     
		(@Todate is null or @FromDate is null ) or                                                                     
		(@Todate is null and convert(date,OrderDate)=@FromDate)                                                                    
		or                                                                
		(@FromDate is null and convert(date,OrderDate)=@Todate)                                           
		or                                                          
		(convert(date,OrderDate) between convert(date,@FromDate) and convert(date,@Todate)  )                
		)                           
		and                      
		(                                                                     
		(@CloseOrderToDate is null or @CloseOrderFromDate is null ) or                                                                     
		(@CloseOrderToDate is null and convert(date,om.Order_Closed_Date)=@CloseOrderFromDate)                                                                    
		or                                                                    
		(@CloseOrderFromDate is null and convert(date,om.Order_Closed_Date)=@CloseOrderToDate)             
		or                                                          
		(convert(date,om.Order_Closed_Date) between convert(date,@CloseOrderFromDate) and convert(date,@CloseOrderToDate)  )                                                                     
		)                      
		and (ISNULL(@DriverAutoId,0)=0 or om.Driver=@DriverAutoId)                                                                     
		and (ISNULL(@EmpAutoId,0)=0 or om.SalesPersonAutoId=@EmpAutoId)                                                                    
		) AS T                                                                    
		where (ISNULL(@PaymentStatus,'0')='0' or PaymentStatus=@PaymentStatus)                                                                    
		Order by OrderNo                                            
                                           
		SELECT * FROM #Result                                        
		WHERE @PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)                                         
		SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize=0 then COUNT(RowNumber) else @PageSize end AS PageSize, @PageIndex AS PageIndex,Format(getdate(),'MM/dd/yyyy hh:mm tt') as PrintDate FROM #Result                          
		select ISNULL(sum(OrderTotal),0.00) as OrderTotal,ISNULL(sum(PaidAmount),0.00) as PaidAmount,ISNULL(sum(DueAmount),0.00) as DueAmount FROM #Result                          
  END 
  END TRY                                                    
 BEGIN CATCH                                                                    
  Set @isException=1                                                                    
  Set @exceptionMessage=ERROR_MESSAGE()                                                                    
 END CATCH                                                                    
END 