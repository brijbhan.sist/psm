ALTER function UDF_GetProductSRP
(
  @ProductId varchar(10)  
)
Returns Decimal(18,2)
AS
BEGIN
     Declare @ProductSRP decimal(18,2),@BasePrice decimal(18,2),@SRP  Decimal(18,2),@ReturnSRP decimal(18,2),@AutoId int

	 SELECT @SRP=ISNULL(NewSRP,0.00),@AutoId=AutoId FROM ProductMaster WHERE ProductId=@ProductId
	 SELECT @BasePrice=Convert(decimal(18,2),Price) FROM PackingDetails WHERE ProductAutoId=@AutoId AND UnitType=3
	 SELECT @ReturnSRP=(Case when @SRP<@BasePrice then @BasePrice else @SRP end) 
	 Return @ReturnSRP
END




