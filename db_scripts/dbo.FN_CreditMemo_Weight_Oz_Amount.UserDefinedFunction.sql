USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_CreditMemo_Weight_Oz_Amount]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Alter table CreditMemoMaster drop column [WeightTaxAmount]
GO
CREATE OR ALTER FUNCTION  [dbo].[FN_CreditMemo_Weight_Oz_Amount]
(
	 @AutoId int
)
RETURNS decimal(18,2)
AS
BEGIN
	DECLARE @WeightTaxAmount decimal(18,2)	=0.00
	if EXISTS(select CreditMemoType from CreditMemoMaster where CreditAutoId=@AutoId and ISnull(CreditMemoType,1)=2)
	BEGIN
	SET @WeightTaxAmount=isnull((select ISNULL(WeightTax,0)*ISNULL(WeightTotalQuantity,0) from CreditMemoMaster where CreditAutoId=@AutoId),0.00)	
	END
	RETURN @WeightTaxAmount
END
GO


Alter table CreditMemoMaster Add 	[WeightTaxAmount]  AS ([dbo].[FN_CreditMemo_Weight_Oz_Amount]([CreditAutoId]))