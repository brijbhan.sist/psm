USE [psmnj.a1whm.com]
GO 
CREATE FUNCTION  [dbo].[FN_Product_CurrentStock]
(
	 @AutoId int
)
RETURNS decimal(10,2)
AS
BEGIN
	DECLARE @CurrentStock decimal(10,2)
	SET @CurrentStock=isnull((SELECT Stock/convert(decimal(10,1),Qty) FROM ProductMaster as pm 
	inner join PackingDetails as pd on pd.ProductAutoId=pm.AutoId and pm.PackingAutoId=pd.UnitType WHERE pm.AutoId=@AutoId),0)
	RETURN @CurrentStock
END


Alter table ProductMaster add CurrentStock as [dbo].[FN_Product_CurrentStock](AutoId)
