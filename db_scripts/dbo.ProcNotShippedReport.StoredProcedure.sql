ALTER procedure [dbo].[ProcNotShippedReport]        
@Opcode int=null,        
@Status int=null,        
@SalsePersonAutoid int=null,        
@FromDate date =NULL,        
@ToDate date =NULL,        
@PageIndex INT = 1,        
@PageSize INT = 10,        
@RecordCount INT =null,        
@isException BIT OUT,        
@exceptionMessage VARCHAR(max) OUT        
AS        
BEGIN        
 BEGIN TRY        
  SET @isException=0        
  SET @exceptionMessage='Success'        
  IF @Opcode=41        
  BEGIN      
	select 
	(
	SELECT AutoId as STID,StatusType as ST FROM  StatusMaster where Category='OrderMaster' and AutoId not in(1,2,8) order by ST ASC  
	for json path
	) as Status,
	(
	select AutoId as SPID,FirstName+' '+LastName as SPN from EmployeeMaster where EmpType=2 order by SPN ASC
	for json path
	)as SalesPerson
	for json path
  END        
  ELSE IF @Opcode=42        
  BEGIN        
   Select ROW_NUMBER() OVER(ORDER BY (OrderNo ) desc) AS RowNumber,        
   (RequiredQty-QtyShip)      
   as RemainQty      ,((RequiredQty-QtyShip)* SellingPrice) as Amount
         
   ,* INTO #TEMRESULT from (        
   select pd.Location,CustomerId,ProductId,ISNULL(QtyShip,0) as QtyShip,cm.CustomerName,OrderNo,
   format(OrderDate,'MM/dd/yyyy') as OrderDate,
   format((Select top 1 MAX([PackingDate]) From GenPacking as gp Where gp.OrderAutoId=Oim.OrderAutoId),'MM/dd/yyyy') as PackingDate,
   ProductName,um.UnitType,      
   (um.UnitType +' ('+convert(varchar(25),QtyPerUnit) +')') as unit,        
   QtyPerUnit ,RequiredQty ,om.Status,oim.UnitPrice as SellingPrice       
   from   OrderItemMaster as  oim        
   inner join ProductMaster as pm on pm.AutoId=oim.ProductAutoId         
   inner join OrderMaster as om on om.AutoId=oim.OrderAutoId        
   inner join CustomerMaster as cm on cm.AutoId=om.CustomerAutoId        
   inner join PackingDetails as pd on pd.ProductAutoId=oim.ProductAutoId and pd.UnitType=oim.UnitTypeAutoId      
   inner join  UnitMaster as um on um.AutoId=oim.UnitTypeAutoId        
   where om.Status  not in(1,2,8)        
   and (ISNULL(@FromDate,'')='' OR ISNULL(@ToDate,'') = '' OR         
   (CONVERT(date,(select MAX([PackingDate]) from [GenPacking] as gp where gp.OrderAutoId=om.AutoId))
   BETWEEN CONVERT(date,@fromDate) AND CONVERT(date,@ToDate)))              
   and(@Status is null or @Status=0 or om.Status=@Status)        
   and(@SalsePersonAutoid is null or @SalsePersonAutoid=0 or om.SalesPersonAutoId=@SalsePersonAutoid)        
        
   ) as t       
   where ((RequiredQty-QtyShip))>0      
   ORDER BY OrderNo         
   SELECT COUNT(OrderNo) AS RecordCount, case when @PageSize=0 then COUNT(RowNumber) 
   else @PageSize end AS PageSize, @PageIndex AS PageIndex,Format(getdate(),'MM/dd/yyyy hh:mm tt') as PrintDate FROM #TEMRESULT         
   SELECT  * FROM #TEMRESULT        
   WHERE (isnull(@PageSize,0)=0 or (ISNULL(@PageIndex,0)=0 or RowNumber 
   BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))          
  
  SELECT ISNULL(SUM(RemainQty),0) AS RemainQty,ISNULL(SUM(Amount),0.00) AS Amount,
  isnull(Sum(QtyShip),0) as QtyShip, isnull(Sum(RequiredQty),0) as RequiredQty
   from  #TEMRESULT
  END        
 END TRY        
 BEGIN CATCH        
 SET @isException=1        
 SET @exceptionMessage=ERROR_MESSAGE()        
 END CATCH        
END 
GO
