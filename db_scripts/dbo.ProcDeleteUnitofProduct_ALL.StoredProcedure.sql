USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcDeleteUnitofProduct_ALL]    Script Date: 08-09-2020 05:02:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER Proc [dbo].[ProcDeleteUnitofProduct_ALL]        
@ProductId varchar(50),        
@UnitAutoid int        
        
as         
BEGIN        
  DECLARE @ProductAutoId INT=NULL,@Result INT=0        
  if(DB_NAME() in ('psmnj.a1whm.com'))                            
  BEGIN      
   SET @ProductAutoId=(SELECT Autoid FROM [psmct.a1whm.com].[dbo].[ProductMaster] where ProductId=@ProductId)        
   IF EXISTS(SELECT * FROM [psmct.a1whm.com].[dbo].[OrderItemMaster] WHERE ProductAutoId=@ProductAutoId and UnitTypeAutoId=@UnitAutoid)                            
   BEGIN                            
        SET @Result =1  
   END   
   SET @ProductAutoId=(SELECT Autoid FROM [psmpa.a1whm.com].[dbo].[ProductMaster] where ProductId=@ProductId)                  
   IF EXISTS(SELECT 1 FROM [psmpa.a1whm.com].[dbo].[OrderItemMaster] WHERE ProductAutoId=@ProductAutoId and UnitTypeAutoId=@UnitAutoid)                            
   BEGIN    
        SET @Result =1                
   END   
   SET @ProductAutoId=(SELECT Autoid FROM [psmnpa.a1whm.com].[dbo].[ProductMaster] where ProductId=@ProductId)             
   IF EXISTS(SELECT 1 FROM [psmnpa.a1whm.com].[dbo].[OrderItemMaster] WHERE ProductAutoId=@ProductAutoId and UnitTypeAutoId=@UnitAutoid)                            
   BEGIN         
        SET @Result =1        
   END    
   SET @ProductAutoId=(SELECT Autoid FROM [psmnj.a1whm.com].[dbo].[ProductMaster] where ProductId=@ProductId)         
   IF EXISTS(SELECT 1 FROM [psmnj.a1whm.com].[dbo].[OrderItemMaster] WHERE ProductAutoId=@ProductAutoId and UnitTypeAutoId=@UnitAutoid)                            
   BEGIN   
  SET @Result =1     
   END   
   SET @ProductAutoId=(SELECT Autoid FROM [psmwpa.a1whm.com].[dbo].[ProductMaster] where ProductId=@ProductId)         
   IF EXISTS(SELECT 1 FROM [psmwpa.a1whm.com].[dbo].[OrderItemMaster] WHERE ProductAutoId=@ProductAutoId and UnitTypeAutoId=@UnitAutoid)                            
   BEGIN   
  SET @Result =1                    
   END 
   SET @ProductAutoId=(SELECT Autoid FROM [psmny.a1whm.com].[dbo].[ProductMaster] where ProductId=@ProductId)         
   IF EXISTS(SELECT 1 FROM [psmny.a1whm.com].[dbo].[OrderItemMaster] WHERE ProductAutoId=@ProductAutoId and UnitTypeAutoId=@UnitAutoid)                            
   BEGIN   
   SET @Result =1                    
   END  
   SET @ProductAutoId=(SELECT Autoid FROM [psmnj.easywhm.com].[dbo].[ProductMaster] where ProductId=@ProductId)         
   IF EXISTS(SELECT 1 FROM [psmnj.easywhm.com].[dbo].[OrderItemMaster] WHERE ProductAutoId=@ProductAutoId and UnitTypeAutoId=@UnitAutoid)                            
   BEGIN   
  SET @Result =1                    
   END  
   SET @ProductAutoId=(SELECT Autoid FROM [psmpa.easywhm.com].[dbo].[ProductMaster] where ProductId=@ProductId)         
   IF EXISTS(SELECT 1 FROM [psmpa.easywhm.com].[dbo].[OrderItemMaster] WHERE ProductAutoId=@ProductAutoId and UnitTypeAutoId=@UnitAutoid)                            
   BEGIN   
  SET @Result =1                    
   END  
   SET @ProductAutoId=(SELECT Autoid FROM [psmnpa.easywhm.com].[dbo].[ProductMaster] where ProductId=@ProductId)         
   IF EXISTS(SELECT 1 FROM [psmnpa.easywhm.com].[dbo].[OrderItemMaster] WHERE ProductAutoId=@ProductAutoId and UnitTypeAutoId=@UnitAutoid)                            
   BEGIN   
  SET @Result =1                    
   END  
   SET @ProductAutoId=(SELECT Autoid FROM [psmwpa.easywhm.com].[dbo].[ProductMaster] where ProductId=@ProductId)         
   IF EXISTS(SELECT 1 FROM [psmwpa.easywhm.com].[dbo].[OrderItemMaster] WHERE ProductAutoId=@ProductAutoId and UnitTypeAutoId=@UnitAutoid)                            
   BEGIN   
  SET @Result =1                    
   END  
   SET @ProductAutoId=(SELECT Autoid FROM [psmct.easywhm.com].[dbo].[ProductMaster] where ProductId=@ProductId)         
   IF EXISTS(SELECT 1 FROM [psmct.easywhm.com].[dbo].[OrderItemMaster] WHERE ProductAutoId=@ProductAutoId and UnitTypeAutoId=@UnitAutoid)                            
   BEGIN   
  SET @Result =1                    
   END  
   SET @ProductAutoId=(SELECT Autoid FROM [psmny.easywhm.com].[dbo].[ProductMaster] where ProductId=@ProductId)         
   IF EXISTS(SELECT 1 FROM [psmny.easywhm.com].[dbo].[OrderItemMaster] WHERE ProductAutoId=@ProductAutoId and UnitTypeAutoId=@UnitAutoid)                            
   BEGIN   
  SET @Result =1                    
   END  
   IF @Result=0  
   BEGIN  
  SET @ProductAutoId=(SELECT Autoid FROM [psmct.a1whm.com].[dbo].[ProductMaster] where ProductId=@ProductId)    
  DELETE FROM [psmct.a1whm.com].[dbo].[ItemBarcode] WHERE [ProductAutoId]=@ProductAutoId AND [UnitAutoId]=@UnitAutoid        
  DELETE FROM [psmct.a1whm.com].[dbo].[PackingDetails] WHERE ProductAutoId=@ProductAutoId and UnitType=@UnitAutoid   
    
  SET @ProductAutoId=(SELECT Autoid FROM [psmpa.a1whm.com].[dbo].[ProductMaster] where ProductId=@ProductId)    
  DELETE FROM [psmpa.a1whm.com].[dbo].[ItemBarcode] WHERE [ProductAutoId]=@ProductAutoId AND [UnitAutoId]=@UnitAutoid                    
  DELETE FROM [psmpa.a1whm.com].[dbo].[PackingDetails] WHERE ProductAutoId=@ProductAutoId and UnitType=@UnitAutoid   
    
  SET @ProductAutoId=(SELECT Autoid FROM [psmnpa.a1whm.com].[dbo].[ProductMaster] where ProductId=@ProductId)    
  DELETE FROM [psmnpa.a1whm.com].[dbo].[ItemBarcode] WHERE [ProductAutoId]=@ProductAutoId AND [UnitAutoId]=@UnitAutoid        
  DELETE FROM [psmnpa.a1whm.com].[dbo].[PackingDetails] WHERE ProductAutoId=@ProductAutoId and UnitType=@UnitAutoid    
  
  SET @ProductAutoId=(SELECT Autoid FROM [psmnj.a1whm.com].[dbo].[ProductMaster] where ProductId=@ProductId)   
  DELETE FROM [psmnj.a1whm.com].[dbo].[ItemBarcode] WHERE [ProductAutoId]=@ProductAutoId AND [UnitAutoId]=@UnitAutoid             
  DELETE FROM [psmnj.a1whm.com].[dbo].[PackingDetails] WHERE ProductAutoId=@ProductAutoId and UnitType=@UnitAutoid    
  
  SET @ProductAutoId=(SELECT Autoid FROM [psmwpa.a1whm.com].[dbo].[ProductMaster] where ProductId=@ProductId)    
  DELETE FROM [psmwpa.a1whm.com].[dbo].[ItemBarcode] WHERE [ProductAutoId]=@ProductAutoId AND [UnitAutoId]=@UnitAutoid             
  DELETE FROM [psmwpa.a1whm.com].[dbo].[PackingDetails] WHERE ProductAutoId=@ProductAutoId and UnitType=@UnitAutoid     
  
  SET @ProductAutoId=(SELECT Autoid FROM [psmny.a1whm.com].[dbo].[ProductMaster] where ProductId=@ProductId)    
  DELETE FROM [psmny.a1whm.com].[dbo].[ItemBarcode] WHERE [ProductAutoId]=@ProductAutoId AND [UnitAutoId]=@UnitAutoid             
  DELETE FROM [psmny.a1whm.com].[dbo].[PackingDetails] WHERE ProductAutoId=@ProductAutoId and UnitType=@UnitAutoid     
  
  SET @ProductAutoId=(SELECT Autoid FROM [psmnj.easywhm.com].[dbo].[ProductMaster] where ProductId=@ProductId)    
  DELETE FROM [psmnj.easywhm.com].[dbo].[ItemBarcode] WHERE [ProductAutoId]=@ProductAutoId AND [UnitAutoId]=@UnitAutoid             
  DELETE FROM [psmnj.easywhm.com].[dbo].[PackingDetails] WHERE ProductAutoId=@ProductAutoId and UnitType=@UnitAutoid     
  
  SET @ProductAutoId=(SELECT Autoid FROM [psmpa.easywhm.com].[dbo].[ProductMaster] where ProductId=@ProductId)    
  DELETE FROM [psmpa.easywhm.com].[dbo].[ItemBarcode] WHERE [ProductAutoId]=@ProductAutoId AND [UnitAutoId]=@UnitAutoid             
  DELETE FROM [psmpa.easywhm.com].[dbo].[PackingDetails] WHERE ProductAutoId=@ProductAutoId and UnitType=@UnitAutoid     
  
  SET @ProductAutoId=(SELECT Autoid FROM [psmnpa.easywhm.com].[dbo].[ProductMaster] where ProductId=@ProductId)    
  DELETE FROM [psmnpa.easywhm.com].[dbo].[ItemBarcode] WHERE [ProductAutoId]=@ProductAutoId AND [UnitAutoId]=@UnitAutoid             
  DELETE FROM [psmnpa.easywhm.com].[dbo].[PackingDetails] WHERE ProductAutoId=@ProductAutoId and UnitType=@UnitAutoid     
  
  SET @ProductAutoId=(SELECT Autoid FROM [psmwpa.easywhm.com].[dbo].[ProductMaster] where ProductId=@ProductId)    
  DELETE FROM [psmwpa.easywhm.com].[dbo].[ItemBarcode] WHERE [ProductAutoId]=@ProductAutoId AND [UnitAutoId]=@UnitAutoid             
  DELETE FROM [psmwpa.easywhm.com].[dbo].[PackingDetails] WHERE ProductAutoId=@ProductAutoId and UnitType=@UnitAutoid     
  
  SET @ProductAutoId=(SELECT Autoid FROM [psmct.easywhm.com].[dbo].[ProductMaster] where ProductId=@ProductId)    
  DELETE FROM [psmct.easywhm.com].[dbo].[ItemBarcode] WHERE [ProductAutoId]=@ProductAutoId AND [UnitAutoId]=@UnitAutoid             
  DELETE FROM [psmct.easywhm.com].[dbo].[PackingDetails] WHERE ProductAutoId=@ProductAutoId and UnitType=@UnitAutoid     
  
  SET @ProductAutoId=(SELECT Autoid FROM [psmny.easywhm.com].[dbo].[ProductMaster] where ProductId=@ProductId)    
  DELETE FROM [psmny.easywhm.com].[dbo].[ItemBarcode] WHERE [ProductAutoId]=@ProductAutoId AND [UnitAutoId]=@UnitAutoid             
  DELETE FROM [psmny.easywhm.com].[dbo].[PackingDetails] WHERE ProductAutoId=@ProductAutoId and UnitType=@UnitAutoid     
  
  END  
  END        
  ELSE        
  BEGIN        
 SET @ProductAutoId=(SELECT Autoid FROM [dbo].[ProductMaster] where ProductId=@ProductId)        
 IF NOT EXISTS(SELECT 1 FROM [dbo].[OrderItemMaster] WHERE ProductAutoId=@ProductAutoId and UnitTypeAutoId=@UnitAutoid)                            
 BEGIN                       
  DELETE FROM [dbo].[ItemBarcode] WHERE [ProductAutoId]=@ProductAutoId AND [UnitAutoId]=@UnitAutoid             
  DELETE FROM [dbo].[PackingDetails] WHERE ProductAutoId=@ProductAutoId and UnitType=@UnitAutoid                          
 END              
 ELSE        
 BEGIN      
  set @Result=1        
 END       
  END  
  Return @Result    
END   