USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Order_DeductionAmount]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

alter table OrderMaster drop column Deductionamount
drop function FN_Order_DeductionAmount
go
CREATE FUNCTION  [dbo].[FN_Order_DeductionAmount]
(
	 @OrderAutoId int
)
RETURNS decimal(18,2)
AS
BEGIN
	DECLARE @DeductionAmount decimal(18,2)	
	SET @DeductionAmount=isnull((select SUM(TotalAmount) from CreditMemoMaster where OrderAutoId=@OrderAutoId),0.00)
	RETURN @DeductionAmount
END
GO
alter table OrderMaster add [Deductionamount]  AS ([dbo].[FN_Order_DeductionAmount]([AutoId]))
