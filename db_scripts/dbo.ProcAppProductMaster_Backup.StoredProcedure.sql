USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcAppProductMaster_Backup]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE ProcEDURE [dbo].[ProcAppProductMaster_Backup]                      
 @OpCode int=Null,                    
 @timeStamp datetime =null,                  
 @isException bit out,                      
 @exceptionMessage varchar(max) out                      
AS                      
BEGIN                      
 BEGIN TRY                      
  SET @isException=0                      
  SET @exceptionMessage='Success!!'                      
  IF @Opcode=41                     
  BEGIN                      
   select [AutoId], [ProductId], [ProductName], ('http://'+DB_NAME()+[ImageUrl]) as [ImageUrl],isnull([Stock],0) as CurrentStock,                    
   isnull((select top 1 autoid from [PackingDetails] where [unittype]=[PackingAutoid] and [ProductAutoId]=t.autoid),0) as DefaultPackingAutoId,                     
   [CategoryAutoId],(SELECT [CategoryName] FROM [dbo].[CategoryMaster] WHERE [AutoId]=[CategoryAutoId]) as [Category],                    
   [SubcategoryAutoId], (SELECT [SubcategoryName] FROM [dbo].[SubCategoryMaster] WHERE [AutoId]=[SubcategoryAutoId]) as [Subcategory],  
   ISNULL(MLQty,0) as MLQty, ISNULL(WeightOz,0) as WeightOz  
   from [dbo].[ProductMaster] as t                     
   where (select count(autoid) from [PackingDetails] where [ProductAutoId]=t.autoid)>0   and ProductStatus=1               
   order by productname asc              
                                
   select Autoid, isnull(Location,'') as Location, isnull(WHminPrice,0.00) as WHminPrice, [UnitType] as [UnitTypeAutoId],                    
   (select UnitType from [UnitMaster] where AutoId=t.UnitType) as [UnitType],                    
   [Qty],isnull([MinPrice],0)as [MinPrice],isnull([CostPrice],0) as [CostPrice],isnull([Price],0) as [Price],isnull([SRP],0) as [SRP],                
   isnull((case when t.srp>0 then CONVERT(DECIMAL(10,2),((t.[SRP]-([Price]/(case [Qty] when 0 then 1 else [Qty] end)))/(case t.[SRP] when 0 then 1 else t.[SRP] end)) * 100) else 0.00 end),0.00) as [GP],                
   isnull([CommCode],0.00) as [CommCode],[ProductAutoId], STUFF(                    
    (select ','+[Barcode] from [ItemBarcode] where [ProductAutoId]=t.[ProductAutoId] and [UnitAutoId]=t.[UnitType]  FOR XML PATH ('')), 1, 1, ''                    
   ) as barcode, EligibleforFree                    
   from  [dbo].[PackingDetails] as t     
  End                      
 END TRY                      
 BEGIN CATCH                      
  SET @isException=1                      
  SET @exceptionMessage=ERROR_MESSAGE()                      
 END CATCH                      
END
GO
