USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[Proc_BarCodeMaster_ALLDB]    Script Date: 08-09-2020 06:57:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[Proc_BarCodeMaster_ALLDB]               
   @ProductId VARCHAR(50)=NULL,  
   @UnitAutoId int null,  
   @Barcode varchar(250) null,
   @BarcodeType int
AS              
 BEGIN             
 IF(DB_NAME() IN ('psmct.a1whm.com','psmpa.a1whm.com','psmnpa.a1whm.com','psmnj.a1whm.com',
 'psmwpa.a1whm.com','psmny.a1whm.com'))          
 BEGIN         
	DECLARE @AutoId INT       
	--For CT DB       
	IF NOT EXISTS(SELECT Barcode FROM [psmCT.a1whm.com].[dbo].[ItemBarcode] WHERE Barcode=@Barcode)        
	BEGIN        
		set @AutoId=(select AutoId from [psmCT.a1whm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId)   
		
		insert into [psmCT.a1whm.com].[dbo].[ItemBarcode](ProductAutoId,UnitAutoId,Barcode,BarcodeType)          
		SELECT @AutoId,@UnitAutoId,@Barcode,@BarcodeType     
	END        
	--For PA DB      
	IF NOT EXISTS(SELECT Barcode FROM [psmpa.a1whm.com].[dbo].[ItemBarcode] WHERE Barcode=@Barcode)        
	BEGIN        
		SET @AutoId=(select AutoId from [psmpa.a1whm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId)   
		
		insert into [psmpa.a1whm.com].[dbo].[ItemBarcode](ProductAutoId,UnitAutoId,Barcode,BarcodeType)          
		SELECT @AutoId,@UnitAutoId,@Barcode,@BarcodeType 
	END        
	--For NJ DB      
	IF NOT EXISTS(SELECT Barcode FROM [psmnj.a1whm.com].[dbo].[ItemBarcode] WHERE Barcode=@Barcode)        
	BEGIN        
		SET @AutoId=(select AutoId from [psmnj.a1whm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId)  
		
		insert into [psmnj.a1whm.com].[dbo].[ItemBarcode](ProductAutoId,UnitAutoId,Barcode,BarcodeType)          
		SELECT @AutoId,@UnitAutoId,@Barcode,@BarcodeType
	END        
	--For NPA DB      
	IF NOT EXISTS(SELECT Barcode FROM [psmnpa.a1whm.com].[dbo].[ItemBarcode] WHERE Barcode=@Barcode)        
	BEGIN        
		SET @AutoId=(select AutoId from [psmnpa.a1whm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId)
		
		insert into [psmnpa.a1whm.com].[dbo].[ItemBarcode](ProductAutoId,UnitAutoId,Barcode,BarcodeType)          
		SELECT @AutoId,@UnitAutoId,@Barcode,@BarcodeType      
	END  
    --For WPA DB  
	IF NOT EXISTS(SELECT Barcode FROM [psmwpa.a1whm.com].[dbo].[ItemBarcode] WHERE Barcode=@Barcode)        
	BEGIN        
		SET @AutoId=(select AutoId from [psmwpa.a1whm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId) 
		
		insert into [psmwpa.a1whm.com].[dbo].[ItemBarcode](ProductAutoId,UnitAutoId,Barcode,BarcodeType)          
		SELECT @AutoId,@UnitAutoId,@Barcode,@BarcodeType
	END 
	--For NY DB  
	IF NOT EXISTS(SELECT Barcode FROM [psmny.a1whm.com].[dbo].[ItemBarcode] WHERE Barcode=@Barcode)        
	BEGIN        
		SET @AutoId=(select AutoId from [psmny.a1whm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId)    
		
		insert into [psmny.a1whm.com].[dbo].[ItemBarcode](ProductAutoId,UnitAutoId,Barcode,BarcodeType)          
		SELECT @AutoId,@UnitAutoId,@Barcode,@BarcodeType 
	END 
    --For EASY DB  
	IF NOT EXISTS(SELECT Barcode FROM [psmnj.easywhm.com].[dbo].[ItemBarcode] WHERE Barcode=@Barcode)        
	BEGIN        
		SET @AutoId=(select AutoId from [psmnj.easywhm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId)          
		insert into [psmnj.easywhm.com].[dbo].[ItemBarcode](ProductAutoId,UnitAutoId,Barcode,BarcodeType)          
		SELECT @AutoId,@UnitAutoId,@Barcode,@BarcodeType 
	END 

	--For EASY PA  
	IF NOT EXISTS(SELECT Barcode FROM [psmpa.easywhm.com].[dbo].[ItemBarcode] WHERE Barcode=@Barcode)        
	BEGIN        
		SET @AutoId=(select AutoId from [psmpa.easywhm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId)          
		insert into [psmpa.easywhm.com].[dbo].[ItemBarcode](ProductAutoId,UnitAutoId,Barcode,BarcodeType)          
		SELECT @AutoId,@UnitAutoId,@Barcode,@BarcodeType 
	END 

    --For EASY NPA  
	IF NOT EXISTS(SELECT Barcode FROM [psmnpa.easywhm.com].[dbo].[ItemBarcode] WHERE Barcode=@Barcode)        
	BEGIN        
		SET @AutoId=(select AutoId from [psmnpa.easywhm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId)          
		insert into [psmnpa.easywhm.com].[dbo].[ItemBarcode](ProductAutoId,UnitAutoId,Barcode,BarcodeType)          
		SELECT @AutoId,@UnitAutoId,@Barcode,@BarcodeType 
	END 

	--For EASY WPA  
	IF NOT EXISTS(SELECT Barcode FROM [psmwpa.easywhm.com].[dbo].[ItemBarcode] WHERE Barcode=@Barcode)        
	BEGIN        
		SET @AutoId=(select AutoId from [psmwpa.easywhm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId)          
		insert into [psmwpa.easywhm.com].[dbo].[ItemBarcode](ProductAutoId,UnitAutoId,Barcode,BarcodeType)          
		SELECT @AutoId,@UnitAutoId,@Barcode,@BarcodeType 
	END 

    --For EASY CT  
	IF NOT EXISTS(SELECT Barcode FROM [psmct.easywhm.com].[dbo].[ItemBarcode] WHERE Barcode=@Barcode)        
	BEGIN        
		SET @AutoId=(select AutoId from [psmct.easywhm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId)          
		insert into [psmct.easywhm.com].[dbo].[ItemBarcode](ProductAutoId,UnitAutoId,Barcode,BarcodeType)          
		SELECT @AutoId,@UnitAutoId,@Barcode,@BarcodeType 
	END 

	--For EASY DEMO  
	IF NOT EXISTS(SELECT Barcode FROM [psmny.easywhm.com].[dbo].[ItemBarcode] WHERE Barcode=@Barcode)        
	BEGIN        
		SET @AutoId=(select AutoId from [psmny.easywhm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId)          
		insert into [psmny.easywhm.com].[dbo].[ItemBarcode](ProductAutoId,UnitAutoId,Barcode,BarcodeType)          
		SELECT @AutoId,@UnitAutoId,@Barcode,@BarcodeType 
   END 
 END         
 ELSE        
 BEGIN        
	 IF NOT EXISTS(SELECT Barcode FROM [dbo].[ItemBarcode] WHERE Barcode=@Barcode)        
	 BEGIN        
		  SET @AutoId=(select AutoId from [dbo].[ProductMaster] WHERE ProductId = @ProductId)          
		  insert into .[dbo].[ItemBarcode](ProductAutoId,UnitAutoId,Barcode,BarcodeType)          
		  SELECT @AutoId,@UnitAutoId,@Barcode,@BarcodeType        
	 END        
 END         
 END 