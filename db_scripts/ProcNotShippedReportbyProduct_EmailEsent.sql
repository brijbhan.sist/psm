
ALTER PROCEDURE [dbo].[ProcNotShippedReportbyProduct_EmailEsent] 
@FromDate date =NULL   
AS        
BEGIN  
   declare  @DriveLocation varchar(150)
   SET @DriveLocation='D:\ProductReport\'+UPPER((select CompanyId from dbo.CompanyDetails))+'_NotShippedReportByProduct_'+
   convert(varchar(50),Format(GetDate(),'MM_dd_yyyy_hh_mm_tt'))+'.html'
          
   Select  Row_Number() over (order by ProductId) as rownumber, ProductId,REPLACE(ProductName, '  ', '') as ProductName,um.UnitType,      
   (um.UnitType +' ('+convert(varchar(25),QtyPerUnit) +')') as unit,          
   SUM(RequiredQty ) as OrderQty,SUM(ISNULL(QtyShip,0)) as QtyShip ,  
   SUM((RequiredQty-ISNULL(QtyShip,0))* oim.UnitPrice)  
    as SellingPrice,
	convert(varchar(20),pm.CurrentStock) +' ['+um1.UnitType+']'   as CurrentStock
	into #tempTbl    
   from   OrderItemMaster as  oim        
   inner join ProductMaster as pm on pm.AutoId=oim.ProductAutoId         
   inner join OrderMaster as om on om.AutoId=oim.OrderAutoId        
   inner join CustomerMaster as cm on cm.AutoId=om.CustomerAutoId        
   inner join PackingDetails as pd on pd.ProductAutoId=oim.ProductAutoId and pd.UnitType=oim.UnitTypeAutoId      
   inner join  UnitMaster as um on um.AutoId=oim.UnitTypeAutoId 
   inner join  UnitMaster as um1 on um1.AutoId=pm.PackingAutoId
   where om.Status not in(1,2,8)
   and ((RequiredQty-QtyShip))>0
   and        
   CONVERT(date,(select MAX([PackingDate]) from [GenPacking] as gp where gp.OrderAutoId=om.AutoId))
    =convert(date,GETDATE()) 
    group by ProductId,ProductName,um.UnitType,      
    (um.UnitType +' ('+convert(varchar(25),QtyPerUnit) +')')  ,convert(varchar(20),pm.CurrentStock) +' ['+um1.UnitType+']'     
    having (SUM(RequiredQty )-SUM(ISNULL(QtyShip,0)))>0    
	order by pm.ProductId

	Declare @AutoIdint int,@ProductId  int,@ProductName varchar(50),@UnitType varchar(50),@CurrentStock varchar(300),@unit varchar(50),@OrderQty int,
	@QtyShip int,@NotShippedQty int,@SellingPrice decimal(18,2),@Count int,@num int=1,@html varchar(max)='',@tr varchar(max)=''

	SET @Count=(select count(1) from #tempTbl)
	
	     set @html='<table class="table tableCSS" border="1" style="width:100%;border: 1px solid black;border-collapse:collapse;" id="NotShippedReportByHeader">
		<thead>
			<tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
				<td style="text-align: center !important;">
					<h4 style="margin:10px;">Not Shipped Report By Product</h4>
				</td>
				</tr>
				<tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
				<td style="text-align: center !important;">
					<h4 style="margin:2px">'+(select CompanyDistributerName from CompanyDetails)+'</h4>
				</td>
				</tr>
				<tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
				<td style="text-align: center !important;">
					<h4 style="margin:2px">'+Convert(varchar(50),FORMAT(GetDate(),'MM/dd/yyyy hh:ss tt'))+'</h4>
				</td>
			</tr>
		</thead>
		<tbody></tbody>
		</table>
		<br>';
		IF(select count(1) from #tempTbl )=0
	BEGIN
	SET @html=@html+'Today : All Product Shipped'
	EXEC dbo.spWriteToFile @DriveLocation, @html
	END 
	ELSE
	BEGIN
	EXEC dbo.spWriteToFile @DriveLocation, @html
		SET @html='
			<table class="table tableCSS" border="1" style="width:100%;border: 1px solid black;border-collapse:collapse;" 
			id="tblNotShippedReportByHeader"> 
			<thead>
			<tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
				<td style="text-align: center !important;" class="CurrencyName text-center">Product ID</td>
				<td style="text-align: center !important;" class="NoOfValue text-center">Product Name</td>
				<td style="text-align: center !important;" class="TotalAmount text-right">Unit</td>
				<td style="text-align: center !important;" class="TotalAmount text-right">Current Stock</td>
				<td style="text-align: center !important;" class="TotalAmount text-right">Order Qty</td>
				<td style="text-align: center !important;" class="TotalAmount text-right">Shipped Qty</td>
				<td style="text-align: center !important;" class="TotalAmount text-right">Not Shipped Qty</td>
				<td style="text-align: center !important;" class="TotalAmount text-right">Not Shipped Amount</td>
			</tr>' +   
			'</thead> <tbody>'
			EXEC dbo.spWriteToFile @DriveLocation, @html
			SET @html=''
			while(@num<=@Count)
			BEGIN 
				select 
				@ProductId=ProductId,
				@ProductName=ProductName,
				@unit=unit,
				@UnitType=UnitType,
				@CurrentStock=CurrentStock,
				@OrderQty=OrderQty,
				@QtyShip=QtyShip,
				@NotShippedQty=OrderQty-QtyShip,
				@SellingPrice=SellingPrice
				from #tempTbl where rownumber = @num 
				set @tr ='<tr>' 
								+	'<td style=''text-align:center;''>'+convert(varchar(50), @ProductId) + '</td>' 
								+	'<td style=''text-align:center;''>'+ convert(varchar(50), @ProductName) + '</td>' 
								+	'<td style=''text-align:center;''>'+ convert(varchar(50), @UnitType) + '</td>'
								+	'<td style=''text-align:center;''>'+ convert(varchar(50), @CurrentStock) + '</td>'
								+	'<td style=''text-align:center;''>'+ convert(varchar(50), @OrderQty) + '</td>' 
								+	'<td style=''text-align:center;''>'+ convert(varchar(50), @QtyShip) + '</td>' 
								+	'<td style=''text-align:center;''>'+ convert(varchar(50), @NotShippedQty) + '</td>'
								+	'<td style=''text-align:right;''>'+ convert(varchar(50), @SellingPrice) + '</td>'
						+ '</tr>' 
				EXEC dbo.spWriteToFile @DriveLocation, @tr
				set @num = @num + 1
			END
			set @html =' 
			<tfoot>
				<tr>
					<td colspan="4" style="text-align:right"><b>Total</b></td>
					<td id="CashTotal" style="text-align:center"><b>'+convert(varchar(50),(select sum(isnull(OrderQty,'0.00')) from #tempTbl))+'</b></td>
					<td id="CashTotal" style="text-align:center"><b>'+convert(varchar(50),(select sum(isnull(QtyShip,'0.00')) from #tempTbl))+'</b></td>
					<td id="CashTotal" style="text-align:center"><b>'+convert(varchar(50),(select sum(isnull(OrderQty-QtyShip,'0.00')) from #tempTbl))+'</b></td>
					<td id="CashTotal" style="text-align:right"><b>'+convert(varchar(50),(select sum(isnull(SellingPrice,'0.00')) from #tempTbl))+'</b></td>
				</tr>
			</tfoot>
			</table><p style="page-break-before: always"></p>'
			EXEC dbo.spWriteToFile @DriveLocation, @html 
		END
	--------------Email Code---------------
	Declare  @Subject varchar(max),@FromName varchar(1000),@FromEmailId varchar(1000),@Port int,@SMTPServer varchar(1000),@Password varchar(1000),@SSL  int,
	@BCCEmailId varchar(max),@MigrateEmailId varchar(1000),@CCEmailId varchar(max),@smtp_userName varchar(max)
	set @Subject = 'Not Shipped Report By Product ' +UPPER((select CompanyDistributerName from dbo.CompanyDetails))+' : ' + 
	format(GETDATE(),'MM/dd/yyy hh:mm tt');
	
	SELECT @FromEmailId=FromEmail,@FromName=DisplayName,@smtp_userName=SMTP_USERNAME,@Password=SMTP_PASSWORD,
	@SMTPServer=HOST,@Port=SMTPPORT,@SSL=EnableSsl from EmailServerMaster_AWS

	set @BCCEmailId=(select top 1 BCCEmail from [dbo].[Tbl_ReportEmailReceiver] where autoid=1);
	set @MigrateEmailId=(select top 1 ToEmail from [dbo].[Tbl_ReportEmailReceiver] where autoid=1)
	SET @CCEmailId=(select top 1 CCEmail from [dbo].[Tbl_ReportEmailReceiver] where autoid=1)
    
		 EXEC [Emailing_DB].[dbo].[ProcSubmitEmail] 
			@Opcode=11,
			@FromEmailId =@FromEmailId,
			@FromName = @FromName,
			@smtp_userName=@smtp_userName,
			@Password = @Password,
			@SMTPServer = @SMTPServer,
			@Port =@Port,
			@SSL =@SSL,
			@ToEmailId =@MigrateEmailId,
			@CCEmailId =@CCEmailId,
			@BCCEmailId =@BCCEmailId,  
			@Subject =@Subject,
			@EmailBody = '',
			@SentDate ='',
			@Status =0,
			@SourceApp ='PSM',
			@SubUrl ='NotShippedReportByProduct',
			@attachment=@DriveLocation,
			@isException=0,
			@exceptionMessage=''  
	
END