USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedTableType [dbo].[PO_OrderItemList]    Script Date: 11/20/2019 5:05:52 AM ******/
CREATE TYPE [dbo].[PO_OrderItemList] AS TABLE(
	[AutoId] [int] NULL,
	[ProductAutoId] [int] NULL,
	[Unit] [int] NULL,
	[Qty] [int] NULL,
	[TotalPieces] [int] NULL
)
GO
