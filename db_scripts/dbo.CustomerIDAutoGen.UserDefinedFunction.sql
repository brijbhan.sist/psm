USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[CustomerIDAutoGen]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[CustomerIDAutoGen]
(
@SequenceCode nvarchar(50), @SrNo int
)
RETURNS nvarchar(25)
AS
BEGIN
 declare @NewSequence nvarchar(25)
 

 select 
@NewSequence=(
PreSample
+
substring(PostSample,1,(len(PostSample)-len(@SrNo)))
+
convert(nvarchar,@SrNo)
)
from SequenceCodeGeneratorMaster 
where SequenceCode=@SequenceCode
 return @NewSequence

END


GO
