Alter procedure Proc_SalesReportByBrand
@Opcode INT=null,
@SalesPerson varchar(20)=null,
@FromDate date=null,
@ToDate date=null,                                        
@isException bit out,                                                    
@exceptionMessage varchar(max) out   
AS
BEGIN
   Declare @ParentLocation varchar(25),@ChildLocation varchar(25),@Query nvarchar(max)
   SET @ParentLocation=(Select DB_NAME())
   SET @ChildLocation=(Select ChildDB_Name from CompanyDetails)
   IF @Opcode=41
   BEGIN
		Select * from 
		(
		Select (
		SELECT AutoId,(FirstName +' '+ISNULL(LastName,'')) AS EmpName FROM EmployeeMaster
	    WHERE EmpType=2 AND Status=1 
	    order by EmpName  ASC  for json path) as EmployeeList
		) as t
		for json path
   END
   IF @Opcode=42
   BEGIN
			SET @Query='
			select  EM.FirstName+'' ''+ISNULL(EM.LastName,'''') as SalesPerson,BrandName,NetPrice 
			into #child1 from ['+@ParentLocation+'].[dbo].Ordermaster as OM
			INNER JOIN ['+@ParentLocation+'].[dbo].EmployeeMaster as EM on EM.AutoId=OM.SalesPersonAutoId
			INNER JOIN ['+@ParentLocation+'].[dbo].Delivered_Order_Items as OIM on OIM.OrderAutoId=OM.AutoId
			INNER JOIN ['+@ParentLocation+'].[dbo].ProductMaster as PM on PM.AutoId=OIM.ProductAutoId
			INNER JOIN ['+@ParentLocation+'].[dbo].BrandMaster as BM on BM.AutoId=PM.BrandAutoId
			where OM.Status=11';
			
			IF(ISNULL(@SalesPerson,'0,')!='0,' AND ISNULL(@SalesPerson,'0')!='0')
			BEGIN
			SET @Query=@Query+' AND OM.SalesPersonAutoId in 
			(select * from dbo.fnSplitString('''+Convert(varchar(15),@SalesPerson)+''','',''))'
			END
			
			IF(Convert(varchar(25),@FromDate)!=null or Convert(varchar(25),@FromDate)!='')
			BEGIN
			SET @Query=@Query+' and (Convert(date,OM.OrderDate)  between '''+Convert(varchar(25),@FromDate)+'''
			And '''+Convert(varchar(25),@ToDate)+''')'
			END
			
			SET @Query=@Query+'
			       
			UNION ALL
			select  EM.FirstName+'' ''+ISNULL(EM.LastName,'''') as SalesPerson,BrandName,NetPrice 
			from ['+@ChildLocation+'].[dbo].Ordermaster as OM
			INNER JOIN ['+@ChildLocation+'].[dbo].EmployeeMaster as EM on EM.AutoId=OM.SalesPersonAutoId
			INNER JOIN ['+@ChildLocation+'].[dbo].Delivered_Order_Items as OIM on OIM.OrderAutoId=OM.AutoId
			INNER JOIN ['+@ChildLocation+'].[dbo].ProductMaster as PM on PM.AutoId=OIM.ProductAutoId
			INNER JOIN ['+@ChildLocation+'].[dbo].BrandMaster as BM on BM.AutoId=PM.BrandAutoId
			where OM.Status=11'
			IF(ISNULL(@SalesPerson,'0,')!='0,'  AND ISNULL(@SalesPerson,'0')!='0')
			BEGIN
				SET @Query=@Query+' AND OM.SalesPersonAutoId in 
				(select * from dbo.fnSplitString('''+Convert(varchar(15),@SalesPerson)+''','',''))'
			END
			
			IF(Convert(varchar(25),@FromDate)!=null or Convert(varchar(25),@FromDate)!='')
			BEGIN
				SET @Query=@Query+' and (Convert(date,OM.OrderDate)  between '''+Convert(varchar(25),@FromDate)+'''
				And '''+Convert(varchar(25),@ToDate)+''')'
			END  
			SET @Query=@Query+'
				select 
				SalesPerson,	Flair,
				Cast(Cast((Flair*100/NetPrice)as decimal(18,2)) as nvarchar(50)) +''%''  FlairPer	,
				Hemping,
				Cast(Cast((Hemping*100/NetPrice)as decimal(18,2)) as nvarchar(50)) +''%''  HempingPer ,
				Herbsens,	
				Cast(Cast((Herbsens*100/NetPrice)as decimal(18,2)) as nvarchar(50)) +''%''  HerbsensgPer ,
				Reclex,
				Cast(Cast((Reclex*100/NetPrice)as decimal(18,2)) as nvarchar(50)) +''%''  ReclexPer ,
				SpecialReserve,	
				Cast(Cast((SpecialReserve*100/NetPrice)as decimal(18,2)) as nvarchar(50)) +''%''  SpecialReservePer ,
				Startek,	
				Cast(Cast((Startek*100/NetPrice)as decimal(18,2)) as nvarchar(50)) +''%''  StartekPer ,
				Juul,	
				Cast(Cast((Juul*100/NetPrice)as decimal(18,2)) as nvarchar(50)) +''%''  JuulPer ,
				GM,	
				Cast(Cast((GM*100/NetPrice)as decimal(18,2)) as nvarchar(50)) +''%''   GMPer ,
				NetPrice  
				from 
				(
				select   SalesPerson,		 
				sum(case when BrandName = ''Flair'' then isnull(NetPrice,0) else 0 end)[Flair],		 
				sum(case when BrandName = ''Hemping'' then isnull(NetPrice,0) else 0 end)[Hemping],
				sum(case when BrandName = ''Herbsens'' then isnull(NetPrice,0) else 0 end)[Herbsens],
				sum(case when BrandName = ''Reclex'' then isnull(NetPrice,0) else 0 end)[Reclex],
				sum(case when BrandName = ''Special Reserve'' then isnull(NetPrice,0) else 0 end)SpecialReserve,
				sum(case when BrandName = ''Startek'' then isnull(NetPrice,0) else 0 end)[Startek],
				sum(case when BrandName = ''Juul'' then isnull(NetPrice,0) else 0 end)[Juul],
				sum(case when BrandName not in  (''Flair'',''Hemping'',''Herbsens'',''Juul'',''Reclex'',''Special Reserve'',''Startek'')
				then isnull(NetPrice,0) else 0 end)[GM],
				ISNULL(sum( NetPrice),0) as NetPrice
			from #child1
			group by  SalesPerson  
			) as t order by SalesPerson
			drop table #child1' 
			EXEC sp_executesql @Query
			Select FORMAT(getdate(),'MM/dd/yyyy hh:mm tt') as PrintDate
   END
END