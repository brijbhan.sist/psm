Create or Alter PROCEDURE [dbo].[ProcSaleOrderMaster]                                                                   
@Opcode INT=NULL,                                                                    
@OrderAutoId INT=NULL,                                                                    
@LogAutoId int=null,                                                                    
@TaxType int =null,                                                                    
@BillingAutoId INT =null,                                                                    
@ShippingAutoId INT =null,                                                                    
@IsFreeItem INT =null,                                                                    
@OrderNo VARCHAR(15)=NULL,                                                                    
@OrderDate DATETIME=NULL,                                                                    
@DeliveryDate DATETIME=NULL,                                                                    
@CustomerAutoId INT=NULL,                                                                    
@Terms INT=NULL,                                                                    
@PaymentMenthod INT=NULL,                                                                    
@ReferenceNo VARCHAR(50)=NULL,                                                                    
@CreditAmount decimal(18,2)=null,                                                                    
@CreditMemoAmount decimal(18,2)=null,      
@PayableAmount decimal(18,2)=null,                                                                    
@CreditNo varchar(200)=null,                                                                    
@ProductAutoId INT=NULL,                                                                    
@UnitAutoId INT=NULL,                                                                    
@SalesPersonAutoId INT=NULL,                                                                    
@LoginEmpType INT=NULL,                                                                    
@PackerAutoId INT=NULL,                                                                    
@EmpAutoId INT=NULL,                                                                    
@OrderStatus INT=NULL,                                                                    
@ShippingType INT=NULL,                                                                    
@ReqQty  INT=NULL,                                                                    
@QtyPerUnit INT=NULL,                                                                    
@UnitPrice DECIMAL(18,2)=NULL,                                                                    
@SRP DECIMAL(18,2)=NULL,                                                                    
@GP DECIMAL(18,2)=NULL,                                                                    
@TaxRate DECIMAL(18,2)=NULL,                                                                    
@minprice  DECIMAL(18,2)=NULL,                                                                    
@TotalAmount DECIMAL(18,2)=NULL,                                                                    
@OverallDisc DECIMAL(18,2)=NULL,                                                                    
@OverallDiscAmt DECIMAL(18,2)=NULL,                                                                    
@ShippingCharges DECIMAL(18,2)=NULL,                                                                    
@TotalTax DECIMAL(18,2)=NULL,                                                                    
@GrandTotal DECIMAL(18,2)=NULL,                                                                    
@PaidAmount  DECIMAL(18,2)=NULL,                                                                    
@BalanceAmount  DECIMAL(18,2)=NULL,                                                                    
@CreditMemo DECIMAL(18,2)=NULL,                                                                
@StoreCredit  DECIMAL(18,2)=NULL,                                                                     
@PastDue  DECIMAL(18,2)=NULL,                                                                     
@Remarks VARCHAR(max)=NULL,                                                                    
@OrderItemAutoId INT=NULL,                                                                    
@IsExchange INT =NULL,                                                                    
@IsTaxable  INT =NULL,                                                       
@TableValue DT_POSOItemList readonly,                                                  
@Todate DATETIME=NULL,                          
@Fromdate DATETIME=NULL,                                                                    
@Barcode varchar(50)=NULL,                                                                    
@QtyShip INT=NULL,   
@Oim_Discount  DECIMAL(18,2)=NULL,         
@PriceLevelAutoId INT=NULL,                                                                    
@PageIndex INT = 1,                                                                    
@PageSize INT = 10,                                                                    
@MLQty DECIMAL(18,2)=NULL,                                                                    
@RecordCount INT =null,                                    
@LogRemark varchar(200) = NULL,                                                                    
@DraftAutoId int =null out,                                                          
@Adjustment DECIMAL(18,2)  =null,  
@CheckSecurity varchar(30)=NULL,
@isException bit out,                                    
@exceptionMessage varchar(max) out                                                                    
AS                                                                    
BEGIN                                                              
  BEGIN TRY                                                                    
  
  Set @isException=0                                                                    
  Set @exceptionMessage='Success'                                                                    
  Declare @TaxValue decimal(10,2),@AllocatQty int,@ShippingTaxEnabled INT                                                   
 
 If @Opcode=101                                                                      
  BEGIN                                                                    
   BEGIN TRY                                                                    
    BEGIN TRAN  
	    IF EXISTS(SELECT AutoId FROM EmployeeMaster WHERE AutoId=@EmpAutoId AND EmpType=10)
		BEGIN
			SELECT ProductAutoId,SUM(TotalPieces) AS TotalPieces INTO #TEMP FROM @TableValue AS OIM                                                                    
			GROUP BY ProductAutoId

			if Exists(Select * from #TEMP t inner join ProductMaster pm on pm.AutoId=t.ProductAutoId where t.TotalPieces > pm.Stock)
			begin
				Select ProductAutoId,ProductName,ProductId,Stock,t.TotalPieces,'Not Available' as Type from #TEMP t inner join ProductMaster pm on pm.AutoId=t.ProductAutoId 
				where t.TotalPieces > pm.Stock
				SET @exceptionMessage='Stock not available'
			end
			else if Exists(Select * from CreditMemoMaster where CreditAutoId in (select * from dbo.fnSplitString(@creditNo,',')) And Status IN (5,6))
			BEGIN
			    Select 'InvalidCRMemo' as Message,'No' as Type
			    Select CreditNo,SalesAmount,StatusType AS Status from CreditMemoMaster AS CMM 
                INNER JOIN StatusMaster AS SM ON CMM.Status=SM.AutoId AND SM.Category='CreditMaster' 
				where CMM.CreditAutoId in (select * from dbo.fnSplitString(@creditNo,',')) And Status IN (5,6)
                Set @exceptionMessage='Invalid Credit Memo'
			END
			Else
			begin
				SET @OrderNo = (SELECT DBO.SequenceCodeGenerator('OrderNumber'))                                                                     
				SET @Terms=(SELECT Terms  FROM CustomerMaster WHERE AutoId=@CustomerAutoId)                                                                    
				DECLARE @STATE INT=(SELECT State  FROM BillingAddress WHERE AutoId=@BillingAutoId )                                                                    
				Set @TaxValue =isnull((select Value from TaxTypeMaster where AutoId =@TaxType),0.00)                                                                    
				DECLARE @IsTaxApply INT=0                        
				DECLARE @Weigth_OZTax decimal(12,2)=ISNULL((select Value From Tax_Weigth_OZ),0)                  
				IF EXISTS(SELECT * FROM TaxTypeMaster WHERE AutoId=@TaxType AND State=@State)                                                              
				BEGIN                                                              
					SET @IsTaxApply=1                                                              
				END                     
				update CustomerMaster set LastOrderDate=GETDATE() where AutoId=@CustomerAutoId 
			    SET @ShippingTaxEnabled=(SELECT EnabledTax FROM ShippingType WHERE AutoId=@ShippingType) 
				 
				SET @SalesPersonAutoId=(SELECT [SalesPersonAutoId] FROM [dbo].[CustomerMaster]                                                                     
				WHERE [AutoId] = @CustomerAutoId) 
						 
				INSERT INTO [dbo].[OrderMaster] ([OrderNo],[OrderDate],[DeliveryDate],[CustomerAutoId],[Terms],                                                                    
				[SalesPersonAutoId],[OverallDiscAmt],[ShippingCharges],                                                                  
				[Status],[ShippingType],TaxType,OrderRemarks,OrderType,AmtDue,BillAddrAutoId,ShipAddrAutoId,                                                         
				DelDate,paidAmount,paymentMode,referNo,POSAutoId,TaxValue,          
				PastDue,mlTaxPer,IsTaxApply,Weigth_OZTax,ShippingTaxEnabled,PackerAssignDate)                                                                    
				values(@OrderNo,getdate(),GETDATE(),@CustomerAutoId,@Terms,                                                                    
				@SalesPersonAutoId,                                                                    
				@OverallDiscAmt,@ShippingCharges,6,@ShippingType,@TaxType,@Remarks,1,                                                        
				@BalanceAmount,@BillingAutoId,@ShippingAutoId,GETDATE(),@PaidAmount,@PaymentMenthod,@ReferenceNo,                                         
				@EmpAutoId,@TaxValue,@PastDue,ISNULL((SELECT TaxRate FROM MLTaxMaster where TaxState=@STATE),0.00)                            
				,@IsTaxApply,@Weigth_OZTax,@ShippingTaxEnabled,GETDATE())                                                                    
                
				SET @OrderAutoId = (SELECT SCOPE_IDENTITY())
				
				IF EXISTS(SELECT AUTOID FROM CustomerMaster WHERE AutoId=@CustomerAutoId AND CustomerType=3)
				BEGIN
					UPDATE OrderMaster SET isMLManualyApply=0,IsTaxApply=0,Weigth_OZTax=0 WHERE AutoId=@OrderAutoId
				END
				                                                                    
                                                               
				if @PaidAmount > 0                                                                    
				begin                                    
					declare @PayId varchar(20)=(select dbo.SequenceCodeGenerator('SalesPaymentId'))                                                                    
					INSERT INTO SalesPaymentMaster (PayId, CustomerAutoId, ReceivedDate, ReceivedAmount, PaymentMode,                                                                    
					ReferenceId,Remarks,ReceiveDate,ReceiveBy,Status,PayType,OrderAutoId)                                                                    
					VALUES (@PayId,@CustomerAutoId,GETDATE(),@PaidAmount,@PaymentMenthod,@ReferenceNo,@Remarks,GETDATE(),@EmpAutoId,1,2,@OrderAutoId)                                      
					UPDATE SequenceCodeGeneratorMaster SET currentSequence=currentSequence+1 WHERE SequenceCode='SalesPaymentId'                                                                    
				end                                                                    
                                                                    
				INSERT INTO [dbo].[Order_Original] ([AutoId],[OrderNo],[OrderDate],[DeliveryDate],[CustomerAutoId],[Terms],                                                           
				[SalesPersonAutoId],[TotalAmount],[OverallDisc],[OverallDiscAmt],[ShippingCharges],[TotalTax],[GrandTotal],[ShippingType],                                        
				TaxType,BillAddrAutoId,ShipAddrAutoId)                                                                    
				values(@OrderAutoId,@OrderNo,getdate(),GETDATE(),@CustomerAutoId,@Terms,                                                                    
				(CASE WHEN @SalesPersonAutoId = 0 THEN (SELECT [SalesPersonAutoId] FROM [dbo].[CustomerMaster] WHERE [AutoId] = @CustomerAutoId)                                                                    
				ELSE @SalesPersonAutoId END)                                                                    
				,@TotalAmount,@OverallDisc,@OverallDiscAmt,@ShippingCharges,@TotalTax,@GrandTotal,4,@TaxType,@BillingAutoId,@ShippingAutoId )                                                                    
                 
				UPDATE oo set oo.MLQty=om.MLTax,oo.MLTax=om.MLTax from OrderMaster as om
				inner join Order_Original as oo on oo.OrderNo=om.OrderNo where om.OrderNo=@OrderNo

				UPDATE SequenceCodeGeneratorMaster SET currentSequence = currentSequence + 1 WHERE SequenceCode='OrderNumber'                                                                        
                                                          
				INSERT INTO [dbo].[OrderItemMaster]([OrderAutoId],[ProductAutoId],[UnitTypeAutoId],[QtyPerUnit],[UnitPrice],           
				[RequiredQty],[SRP],[GP],[Tax],IsExchange,QtyShip,RemainQty,TaxValue,UnitMLQty,isFreeItem,Barcode,Weight_Oz,OM_MinPrice,
				OM_CostPrice,Original_UnitType,BasePrice,Oim_Discount)                                                                    
				SELECT @OrderAutoId,tb.[ProductAutoId],tb.[UnitAutoId],tb.[QtyPerUnit],tb.[UnitPrice],tb.RequiredQty,pm.[P_SRP],          
				tb.[GP],tb.[Tax],IsExchange,tb.RequiredQty,0,@TaxValue, 
				case when IsExchange=0 and isFreeItem=0 and IsApply_ML=1 then ISNULL(pm.MLQty,0) else 0 end,          
				isFreeItem,tb.Barcode,    
				case when IsExchange=0 and isFreeItem=0 and IsApply_Oz=1 then ISNULL(pm.WeightOz,0) else 0 end,
				OM_MinPrice,OM_CostPrice,tb.UnitAutoId,OM_BasePrice,tb.Oim_Discount
				FROM @TableValue as tb                                                                    
				inner join ProductMaster as pm on pm.AutoId=tb.ProductAutoId                                                                  
                                                                          
				INSERT INTO [dbo].[OrderItems_Original]([OrderAutoId],[ProductAutoId],[UnitTypeAutoId],[QtyPerUnit],[UnitPrice],[RequiredQty],[TotalPieces],                                                                    
				[SRP],[GP],[Tax],[NetPrice],IsExchange,UnitMLQty,TotalMLQty,isFreeItem)                              
				SELECT @OrderAutoId,tb.[ProductAutoId],tb.[UnitAutoId],tb.[QtyPerUnit],tb.[UnitPrice],tb.RequiredQty,tb.RequiredQty*[QtyPerUnit] ,pm.[P_SRP],                                                           
				tb.[GP],tb.[Tax],tb.[NetPrice],ISNULL(IsExchange,0),ISNULL(pm.MLQty,0),                                                                    
				(ISNULL(pm.MLQty,0)*(tb.RequiredQty*[QtyPerUnit])),ISNULL(isFreeItem,0) FROM @TableValue as tb                                                                     
				inner join ProductMaster as pm on pm.AutoId=tb.ProductAutoId                                                                    
                                                                         
				SET @LogRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=1), '[OrderNo]', @OrderNo)                                                                    
				INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                                                    
				VALUES(1,@EmpAutoId,getdate(),@LogRemark,@OrderAutoId)                                                                
                                                                         
                                                                         
				SET @PriceLevelAutoId = (SELECT [PriceLevelAutoId] FROM [dbo].[CustomerPriceLevel] WHERE [CustomerAutoId] = @CustomerAutoId)                                                                    
                                                                          
				exec UPDTAE_PRICELEVEL                                                                     
				@OrderAutoId=@OrderAutoId,                                    
				@PriceLevelAutoId=@PriceLevelAutoId,
				@EmpAutoId=@EmpAutoId
                                                                         
				
	
				insert into [dbo].[ProductStockUpdateLog]([ProductId],[OledStock],[NewStock],[UserAutoId],[DateTime],ReferenceId,ActionRemark,ReferenceType)
				SELECT pm.ProductId,isnull([Stock],0),(isnull(pm.Stock,0)-(TotalPieces)),@EmpAutoId,GETDATE(),@OrderAutoId,
				'Order No-(' + @OrderNo + ') has been generated by '+(Select FirstName+' '+LastName from Employeemaster where autoid=@EmpAutoId)+'.','OrderMaster' 
				FROM #TEMP as dt
				inner join productmaster pm on dt.[ProductAutoId] = PM.[AutoId] 
			                                             
                 UPDATE PM SET PM.Stock= (ISNULL(PM.Stock,0)-(OIM.TotalPieces)) FROM ProductMaster AS PM                                                                    
				INNER JOIN #TEMP AS OIM ON OIM.ProductAutoId=PM.AutoId


				IF EXISTS(Select * from AllowQtyPiece as AQP
				INNER JOIN OrderItemMaster as OIM ON AQP.ProductAutoId=OIM.ProductAutoId
				where SalesPersonAutoId=@SalesPersonAutoId AND OIM.OrderAutoId=@OrderAutoId
				AND convert(date,AQP.AllowDate) <= convert(date,getdate()))
				BEGIN 

					IF EXISTS(Select * from AllocatedPackingDetails AS APD 
					INNER JOIN @TableValue AS dt ON dt.ProductAutoId=APD.ProductAutoId
					AND dt.UnitAutoId=APD.PackingType where APD.OrderAutId=@OrderAutoId)
					BEGIN
						Update APD SET APD.AllocatedQty=ISNULL(dt.TotalPieces,0),APD.ProductAutoId=dt.ProductAutoId,
						APD.PackingType=dt.UnitTypeAutoId,APD.AllocateDate=GETDATE()
						FROM AllocatedPackingDetails AS APD INNER JOIN (
						Select SUM(OIM.TotalPieces) as TotalPieces,OIM.ProductAutoId,OIM.UnitTypeAutoId
						from AllowQtyPiece as AQP
						INNER JOIN OrderItemMaster as OIM ON
						AQP.ProductAutoId=OIM.ProductAutoId
						where SalesPersonAutoId=@SalesPersonAutoId AND OIM.OrderAutoId=@OrderAutoId
						AND convert(date,AQP.AllowDate) <= convert(date,getdate()) 
						AND ISNULL(OIM.QtyShip,0)>0
						group by OIM.ProductAutoId,OIM.UnitTypeAutoId
						) AS dt ON APD.OrderAutId=@OrderAutoId AND APD.ProductAutoId=dt.ProductAutoId
					END
					ELSE
					BEGIN
						INSERT INTO AllocatedPackingDetails (OrderAutId,ProductAutoId,AllocatedQty,PackingType,AllocateDate)
						Select @OrderAutoId,OIM.ProductAutoId,SUM(OIM.TotalPieces),OIM.UnitTypeAutoId,GetDate()
						from AllowQtyPiece as AQP
						INNER JOIN OrderItemMaster as OIM ON
						AQP.ProductAutoId=OIM.ProductAutoId
						where SalesPersonAutoId=@SalesPersonAutoId AND OIM.OrderAutoId=@OrderAutoId
						AND convert(date,AQP.AllowDate) <= convert(date,getdate()) 
						AND ISNULL(OIM.QtyShip,0)>0
						group by OIM.ProductAutoId,OIM.UnitTypeAutoId
					END
					END   
				
					IF(@StoreCredit > 0)                                                    
					BEGIN                              
							IF EXISTS(SELECT * FROM CustomerCreditMaster WHERE CustomerAutoId=@CustomerAutoId)                                                     
							BEGIN                                   
								update CustomerCreditMaster set CreditAmount =isnull(CreditAmount,0)-isnull(@StoreCredit,0) where CustomerAutoId=@CustomerAutoId    					
							END                                                    
							ELSE                                                    
							BEGIN                                                    
								INSERT INTO CustomerCreditMaster(CustomerAutoId,CreditAmount)                                                 
								VALUES(@CustomerAutoId,@StoreCredit)                                                      
							END  
				
							insert into tbl_Custumor_StoreCreditLog(CustomerAutoId,ReferenceType,ReferenceNo,Amount,
							CreatedDate,CreatedBy,Remarks)values(@CustomerAutoId,'OrderMaster',@OrderAutoId,@StoreCredit,getdate()
							,@EmpAutoid,'SC > 0') 

			                                                   
					END 
		                                       
					IF(@StoreCredit < 0)                                                    
					BEGIN                                                    
						IF EXISTS(SELECT * FROM CustomerCreditMaster WHERE CustomerAutoId=@CustomerAutoId)                                                                    
						BEGIN                                                    
							UPDATE CustomerCreditMaster SET CreditAmount=CreditAmount+((-1)*@StoreCredit) WHERE CustomerAutoId=@CustomerAutoId                                                    
						END                                                    
						ELSE                                                    
						BEGIN                           
							INSERT INTO CustomerCreditMaster(CustomerAutoId,CreditAmount)                                                                     
							VALUES(@CustomerAutoId,((-1)*@StoreCredit))                                                      
						END                                                    
                                                        
							INSERT INTO tbl_Custumor_StoreCreditLog(CustomerAutoId,CreatedBy,CreatedDate,ReferenceNo,ReferenceType
							,Amount,Remarks)VALUES(@CustomerAutoId,@EmpAutoId,GETDATE(),@OrderAutoId,'OrderMaster', @StoreCredit,'SC < 0')                                                     
					END    
		           
		                                           
					set @Remarks= REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=16),                                                                    
					'[OrderNo]',  (SELECT OrderNo FROM OrderMaster where AutoId = @OrderAutoId))                                                
					INSERT INTO CreditMemoLog(CreditAutoId,ActionAutoId,EmpAutoId,Remarks,LogDate)                                                                    
					select splitdata,16,@EmpAutoId, @Remarks,GETDATE() from dbo.fnSplitString(@creditNo,',')                                                                    
                                                                          
					UPDATE CreditMemoMaster set OrderAutoId=@OrderAutoId,PaidAmount=TotalAmount ,CompletedBy=@EmpAutoId,CompletionDate=GETDATE()                                                                   
					where CreditAutoId in (select * from dbo.fnSplitString(@creditNo,','))                                                                    
				
					select @OrderAutoId as OrderAutoId,'Available' as Type
				                                           
					delete from DraftItemMaster where DraftAutoId= @DraftAutoId                                                                    
					delete from DraftOrderMaster where DraftAutoId =@DraftAutoId                                                                    
				
					update CustomerMaster set LastOrderDate=GETDATE() where AutoId=@CustomerAutoId    
	           END   
			IF((Select CustomerType FROM CustomerMaster where AutoId=@CustomerAutoId)=3)
			BEGIN
			     Declare @Type int=1
			     EXEC ProcGeneratePurchaseOrderByPOS @CustomerAutoId=@CustomerAutoId,@OrderAutoId=@OrderAutoId,@Type=@Type
			END
		end 
		else
		begin
			Set @isException=1                                                                                                     
				Set @exceptionMessage='InvalidPOS'
		end
	COMMIT TRANSACTION                                                                    
	END TRY                                                                    
	BEGIN CATCH                                                                
		ROLLBACK TRAN                                                                    
		Set @isException=1                                                                    
		Set @exceptionMessage=ERROR_MESSAGE()                                                      
   End Catch                                                                     
  END                                                                    
	ELSE If @Opcode=201                                                         
		Begin                                                                  
		BEGIN TRY                                                                       
		BEGIN TRAN 
		IF EXISTS(SELECT AutoId FROM EmployeeMaster WHERE AutoId=@EmpAutoId AND EmpType=10)
		BEGIN	
			IF NOT EXISTS(SELECT AutoId FROM OrderMaster Where AutoId=@OrderAutoId AND Status=11)
			BEGIN
				select ProductAutoId,SUM(TotalPieces-OldUsed) as AvailableStock into #temp201 from (
				SELECT ProductAutoId,SUM(TotalPieces) AS TotalPieces,0 OldUsed  FROM @TableValue AS OIM 
				GROUP BY ProductAutoId
				UNION 
				select ProductAutoId,0 as TotalPieces,SUM(TotalPieces) AS OldUsed  FROM OrderItemMaster AS OIM 
				where OrderAutoId=@OrderAutoId
				GROUP BY ProductAutoId
				) as t group by t.ProductAutoId
			 
				if exists(
				select * from #temp201 as t inner join ProductMaster as pm on pm.AutoId=t.ProductAutoId
				where pm.Stock<t.AvailableStock
				)
				BEGIN
					Select ProductAutoId,ProductName,ProductId,Stock,t.AvailableStock,'Not Available' as Type from #temp201 t 
					inner join ProductMaster pm on pm.AutoId=t.ProductAutoId where pm.Stock<t.AvailableStock
					SET @exceptionMessage='Stock not available'
				END
				else if Exists(Select * from CreditMemoMaster where CreditAutoId in (select * from dbo.fnSplitString(@creditNo,',')) And Status IN (5,6))
				BEGIN
				    Select 'InvalidCRMemo' as Message,'No' as Type
					Select CreditNo,SalesAmount,StatusType AS Status from CreditMemoMaster AS CMM 
					INNER JOIN StatusMaster AS SM ON CMM.Status=SM.AutoId AND SM.Category='CreditMaster' 
					where CMM.CreditAutoId in (select * from dbo.fnSplitString(@creditNo,',')) And Status IN (5,6)
					Set @exceptionMessage='Invalid Credit Memo'
				END
				else
				begin
				    Declare @TaxState int
					set @CustomerAutoId= (select CustomerAutoId from [OrderMaster] WHERE [AutoId] = @OrderAutoId) 
					
		            SET @TaxState = (SELECT TOP 1 State FROM BillingAddress  WHERE CustomerAutoId=@CustomerAutoId and IsDefault=1)

					SET @SalesPersonAutoId=(SELECT SalesPersonAutoId FROM CustomerMaster Where AutoId=@CustomerAutoId)
					SET @CreditAmount=ISNULL((select CreditAmount from OrderMaster where AutoId=@OrderAutoId),0.00)                                                                                    
					IF (@CreditAmount !=0)                                                                                          
					BEGIN                                                    
						update CustomerCreditMaster set CreditAmount =isnull(CreditAmount,0)+@CreditAmount
						where CustomerAutoId=@CustomerAutoId  
						DELETE from tbl_Custumor_StoreCreditLog where CustomerAutoId=@CustomerAutoId and ReferenceNo=CONVERT(varchar(250),@OrderAutoId)                                                    
						and ReferenceType='OrderMaster'                                                    
					END                                                       
					SET  @ShippingTaxEnabled =(SELECT EnabledTax FROM ShippingType WHERE AutoId=@ShippingType)   
				                      
					UPDATE [dbo].[OrderMaster] SET                                                                      
					OrderRemarks= @Remarks,                                                                           
					[OverallDiscAmt] = @OverallDiscAmt,                                                                    
					[ShippingCharges] = @ShippingCharges,                              
					[ShippingType]  = @ShippingType ,                                                                    
					paymentMode= @PaymentMenthod ,                                                                         
					paidAmount= @PaidAmount,                                                        
					referNo=@ReferenceNo,
					BillAddrAutoId=@BillingAutoId,
					ShipAddrAutoId=@ShippingAutoId,
					TaxType=@TaxType,                                                        
					TaxValue=ISNULL((select value from TaxTypeMaster where AutoId=@TaxType),0),                                                                    
					PastDue= @PastDue ,
					ShippingTaxEnabled=@ShippingTaxEnabled,
					MLTaxPer=ISNULL((SELECT TaxRate FROM MLTaxMaster where TaxState=@TaxState),0.00) 
					WHERE [AutoId] = @OrderAutoId                                                     
                                                                     
					UPDATE [dbo].[Order_Original] SET                                                                     
					[TotalAmount]  = @TotalAmount,                                                                    
					[OverallDisc]  = @OverallDisc,                                                                    
					[OverallDiscAmt] = @OverallDiscAmt,                                                                    
					[ShippingCharges] = @ShippingCharges,                                            
					[TotalTax]   = @TotalTax,                                                                    
					[ShippingType]  = @ShippingType WHERE [AutoId] = @OrderAutoId 

					insert into [dbo].[ProductStockUpdateLog]([ProductId],[OledStock],[NewStock],[UserAutoId],[DateTime],ReferenceId,ActionRemark,ReferenceType)
					SELECT pm.ProductId,isnull([Stock],0),(isnull(pm.Stock,0)+TotalPieces),@EmpAutoId,GETDATE(),dt.OrderAutoId,
					'Order No-(' + om.OrderNo + ') has been updated by '+(Select FirstName+' '+LastName from Employeemaster where autoid=@EmpAutoId)+'.','OrderMaster' 
					FROM (
						select OrderAutoId,productAutoid,SUM(TotalPieces)as TotalPieces from OrderItemMaster AS oim  where                                                                     
						oim.OrderAutoId  = @OrderAutoId
						group by productAutoid,OrderAutoId
					) as dt
					inner join productmaster pm on dt.[ProductAutoId] = PM.[AutoId]
					inner join OrderMaster om on dt.OrderAutoId=om.AutoId
					where dt.OrderAutoId=@OrderAutoId  


					UPDATE PM SET [Stock] = isnull([Stock],0) + isnull(TotalPieces,0) FROM [dbo].[ProductMaster] AS PM                                  
					INNER JOIN
						( select productAutoid,SUM(TotalPieces)as TotalPieces from OrderItemMaster AS oim  where                                                                     
						oim.OrderAutoId  = @OrderAutoId
						group by productAutoid
					) as t on  pm.AutoId=t.ProductAutoId

					IF EXISTS(Select * from AllowQtyPiece as AQP
					INNER JOIN OrderItemMaster as OIM ON AQP.ProductAutoId=OIM.ProductAutoId
					where SalesPersonAutoId=@SalesPersonAutoId AND OIM.OrderAutoId=@OrderAutoId
					AND convert(date,AQP.AllowDate) <= convert(date,getdate()))
					BEGIN
				 
						Delete FROM AllocatedPackingDetails WHERE OrderAutId=@OrderAutoId
					END
			                                                                             
					DELETE FROM [dbo].[OrderItemMaster] WHERE [OrderAutoId] = @OrderAutoId                                                                    
                                                                    
					INSERT INTO [dbo].[OrderItemMaster]([OrderAutoId],[ProductAutoId],[UnitTypeAutoId],[QtyPerUnit],[UnitPrice],              
					[RequiredQty],isFreeItem,[SRP],[GP],[Tax],IsExchange,QtyShip,RemainQty,TaxValue,UnitMLQty,Weight_Oz,Barcode,OM_MinPrice,OM_CostPrice
					,Original_UnitType,BasePrice,Oim_Discount)                                                                    
					SELECT @OrderAutoId,tb.[ProductAutoId],tb.[UnitAutoId],tb.[QtyPerUnit],tb.[UnitPrice],tb.RequiredQty,isFreeItem ,pm.[P_SRP],                                                                    
					tb.[GP],tb.[Tax],IsExchange,tb.RequiredQty,0,@TaxValue,                             
					(case when tb.IsExchange=0 and tb.isFreeItem=0 and pm.IsApply_ML=1 then ISNULL(pm.MLQty,0) else 0 end),          
					(case when tb.IsExchange=0 and tb.isFreeItem=0 and pm.IsApply_Oz=1 then ISNULL(pm.WeightOz,0) else 0 end)              
					,tb.Barcode,
					OM_MinPrice,OM_CostPrice,tb.UnitAutoId,OM_BasePrice,tb.Oim_Discount
					FROM @TableValue as tb                                                                  
					inner join ProductMaster as pm on pm.AutoId=tb.ProductAutoId    
                                                                    
					DELETE FROM [dbo].[OrderItems_Original] WHERE [OrderAutoId] = @OrderAutoId    

					INSERT INTO [dbo].[OrderItems_Original]([OrderAutoId],[ProductAutoId],[UnitTypeAutoId],[QtyPerUnit],[UnitPrice],[RequiredQty],[TotalPieces],                                                                    
					[SRP],[GP],[Tax],[NetPrice],IsExchange,UnitMLQty,TotalMLQty,isFreeItem)                                                                    
					SELECT @OrderAutoId,tb.[ProductAutoId],tb.[UnitAutoId],tb.[QtyPerUnit],tb.[UnitPrice],tb.[RequiredQty],tb.[TotalPieces],tb.[SRP],                          
					tb.[GP],tb.[Tax],tb.[NetPrice],IsExchange,(case when tb.IsExchange=0 and tb.isFreeItem=0 and pm.IsApply_ML=1 then ISNULL(pm.MLQty,0) else 0 end)
					,(case when tb.IsExchange=0 and tb.isFreeItem=0 and pm.IsApply_ML=1 then ISNULL(pm.MLQty,0) else 0 end)*tb.RequiredQty*tb.QtyPerUnit,isFreeItem
					FROM @TableValue AS tb 
					inner join ProductMaster as pm on pm.AutoId=tb.ProductAutoId   
               
					IF EXISTS(Select * from AllowQtyPiece as AQP
					INNER JOIN OrderItemMaster as OIM ON AQP.ProductAutoId=OIM.ProductAutoId
					where SalesPersonAutoId=@SalesPersonAutoId AND OIM.OrderAutoId=@OrderAutoId
					AND convert(date,AQP.AllowDate) <= convert(date,getdate()))
					BEGIN
					 

						INSERT INTO AllocatedPackingDetails (OrderAutId,ProductAutoId,AllocatedQty,PackingType,AllocateDate)
						Select @OrderAutoId,OIM.ProductAutoId,SUM(OIM.TotalPieces),OIM.UnitTypeAutoId,GetDate()
						from AllowQtyPiece as AQP
						INNER JOIN OrderItemMaster as OIM ON
						AQP.ProductAutoId=OIM.ProductAutoId
						where SalesPersonAutoId=@SalesPersonAutoId AND OIM.OrderAutoId=@OrderAutoId
						AND convert(date,AQP.AllowDate) <= convert(date,getdate()) 
						AND ISNULL(OIM.QtyShip,0)>0
						group by OIM.ProductAutoId,OIM.UnitTypeAutoId
					END      
			   
					IF(@StoreCredit > 0)                                                    
					BEGIN                                           
						IF EXISTS(SELECT * FROM CustomerCreditMaster WHERE CustomerAutoId=@CustomerAutoId)                                                     
						BEGIN                                   
							update CustomerCreditMaster set CreditAmount =isnull(CreditAmount,0)-isnull(@StoreCredit,0) where CustomerAutoId=@CustomerAutoId    					
						END                                                    
						ELSE                                                    
						BEGIN                                                    
							INSERT INTO CustomerCreditMaster(CustomerAutoId,CreditAmount)                                                 
							VALUES(@CustomerAutoId,@StoreCredit)                                                      
						END  
				
						insert into tbl_Custumor_StoreCreditLog(CustomerAutoId,ReferenceType,ReferenceNo,Amount,
						CreatedDate,CreatedBy,Remarks)values(@CustomerAutoId,'OrderMaster',@OrderAutoId,@StoreCredit,getdate()
						,@EmpAutoid,'SC > 0')                         
					END 
		                                       
					IF(@StoreCredit < 0)                                                    
					BEGIN                                                    
						IF EXISTS(SELECT * FROM CustomerCreditMaster WHERE CustomerAutoId=@CustomerAutoId)                                                                    
						BEGIN                                                    
							UPDATE CustomerCreditMaster SET CreditAmount=CreditAmount+((-1)*@StoreCredit) WHERE CustomerAutoId=@CustomerAutoId                                                    
						END                                                    
						ELSE                                                    
						BEGIN                           
							INSERT INTO CustomerCreditMaster(CustomerAutoId,CreditAmount)                                                                     
							VALUES(@CustomerAutoId,((-1)*@StoreCredit))                                                      
						END                                                    
                                                        
							INSERT INTO tbl_Custumor_StoreCreditLog(CustomerAutoId,CreatedBy,CreatedDate,ReferenceNo,ReferenceType
							,Amount,Remarks)VALUES(@CustomerAutoId,@EmpAutoId,GETDATE(),@OrderAutoId,'OrderMaster', @StoreCredit,'SC > 0')                                                     
					END 
				 
					insert into [dbo].[ProductStockUpdateLog]([ProductId],[OledStock],[NewStock],[UserAutoId],[DateTime],ReferenceId,ActionRemark,ReferenceType)
					SELECT pm.ProductId,isnull([Stock],0),(isnull(pm.Stock,0)-TotalPieces),@EmpAutoId,GETDATE(),dt.OrderAutoId,
					'Order No-(' + om.OrderNo + ') has been updated by '+(Select FirstName+' '+LastName from Employeemaster where autoid=@EmpAutoId)+'.','OrderMaster' 
					FROM (
						select OrderAutoId,productAutoid,SUM(TotalPieces)as TotalPieces from OrderItemMaster AS oim  where                                                                     
						oim.OrderAutoId  = @OrderAutoId
						group by productAutoid,OrderAutoId
					) as dt
					inner join productmaster pm on dt.[ProductAutoId] = PM.[AutoId]
					inner join OrderMaster om on dt.OrderAutoId=om.AutoId
					where dt.OrderAutoId=@OrderAutoId  


					UPDATE PM SET [Stock] = isnull([Stock],0) - isnull(TotalPieces,0) FROM [dbo].[ProductMaster] AS PM                                  
					INNER JOIN
						( select productAutoid,SUM(TotalPieces)as TotalPieces from OrderItemMaster AS oim  where                                                                     
						oim.OrderAutoId  = @OrderAutoId
						group by productAutoid
					) as t on  pm.AutoId=t.ProductAutoId



					update CreditMemoMaster set OrderAutoId=null,CompletionDate=null,CompletedBy=null where OrderAutoId=@OrderAutoId

					UPDATE CreditMemoMaster set OrderAutoId=@OrderAutoId,PaidAmount=TotalAmount ,CompletedBy=@EmpAutoId,CompletionDate=GETDATE()                                                                   
					where CreditAutoId in (select * from dbo.fnSplitString(@creditNo,','))  
					
					SET @LogRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId] = 2), '[OrderNo]', (SELECT [OrderNo] FROM OrderMaster where [AutoId] = @OrderAutoId))                 
					INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                                                    
					VALUES(2,@EmpAutoId,getdate(),@LogRemark,@OrderAutoId)                                                                    
	
					IF @PaidAmount > 0                                                                    
					BEGIN                                                                     
						UPDATE SalesPaymentMaster SET ReceivedAmount=@PaidAmount ,PaymentMode=@PaymentMenthod,ReferenceId=@ReferenceNo WHERE OrderAutoId=@OrderAutoId                                                                    
					END                                                                    
					ELSE                     
					BEGIN                                                                    
						DELETE FROM SalesPaymentMaster WHERE OrderAutoId =@OrderAutoId                                                                    
					END   
				
					Select 'Available' as Type 
				END   

				IF((Select CustomerType FROM CustomerMaster where AutoId=@CustomerAutoId)=3)
				BEGIN
					 EXEC ProcGeneratePurchaseOrderByPOS @CustomerAutoId=@CustomerAutoId,@OrderAutoId=@OrderAutoId,@Type=2
				END
			END
			ELSE
			BEGIN
				SET @isException=1
				SET @exceptionMessage='OrderClosed'
			END
		END
		ELSE
		BEGIN
			SET @isException=1
			SET @exceptionMessage='InvalidPOS'
		END
	COMMIT TRANSACTION                                                                    
	END TRY                                                                    
    BEGIN CATCH                               
		ROLLBACK TRAN                                                                    
		Set @isException=1                                                                    
		Set @exceptionMessage=ERROR_MESSAGE()                                                                    
    END CATCH                                                              
  End                                                                    
  ELSE IF @Opcode=401                                                                    
  BEGIN                      
		  SELECT [AutoId],[StatusType],(SELECT TOP 1 TaxRate FROM MLTaxMaster) AS MLQtyRate FROM [dbo].[StatusMaster] WHERE [AutoId]=1 and [Category]='OrderMaster'                                                                    
		  SELECT cm.[AutoId],[CustomerId] + ' - ' + [CustomerName] +' ['+ct.CustomerType+']' As Customer FROM [dbo].[CustomerMaster] as cm                                            
		  inner join CustomerType as ct on cm.CustomerType=ct.AutoId   where status=1                                                                     
		  SELECT [AutoId],convert(varchar(10),[ProductId]) + '--' + [ProductName] as [ProductName],MLQty,isnull((WeightOz),0) as WeightOz,Stock                                                                   
		  FROM [dbo].[ProductMaster] as pm where ProductStatus=1                                      
		  and (select count(1) from PackingDetails where  ProductAutoId=pm.AutoId)>0                              
		  and ISNULL(PackingAutoId,0)!=0                                    
		  order by [ProductId]                                                              
		  SELECT * from shippingType  where Shippingstatus=1
		  SELECT AutoId,PaymentMode from PAYMENTModeMaster where AutoID not in (5)
   END                                                                     
   ELSE IF @Opcode=402                                                                    
   BEGIN                                                                            
    SELECT BA.[AutoId] As BillAddrAutoId,BA.[Address],St.[StateName] AS State,BA.[City],BA.[Zipcode] FROM [dbo].[BillingAddress] AS BA                                                                    
    INNER JOIN [dbo].[State] AS St ON St.AutoId = BA.State WHERE BA.[CustomerAutoId] = @CustomerAutoId AND BA.[IsDefault]=1                                                                    
    SELECT SA.[AutoId] As ShipAddrAutoId,SA.[Address],St.[StateName] AS State,SA.[City],SA.[Zipcode] FROM [dbo].[ShippingAddress] AS SA                         
    INNER JOIN [dbo].[State] AS St ON St.AutoId = SA.State WHERE SA.CustomerAutoId = @CustomerAutoId AND SA.[IsDefault]=1                                                                    
                                                                    
    SELECT [AutoId],[OrderNo],CONVERT(VARCHAR(20),[OrderDate]
	) AS OrderDate,DO.[AmtDue],DO.[GrandTotal],DO.[AmtPaid]                                          
    FROM [dbo].[OrderMaster] As OM                                       
    INNER JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId]                                                                         
    WHERE [CustomerAutoId] = @CustomerAutoId AND [Status] = 11 AND OM.[AmtDue] > 0.00                                                                    
                                       
    SELECT CT.[TermsDesc],CM.CustomerId +' ' +CM.CustomerName as CustomerName FROM CustomerMaster AS CM                           
    LEFT JOIN CustomerTerms AS CT ON CT.[TermsId] = CM.[Terms] WHERE [AutoId] = @CustomerAutoId                                                                     
    UPDATE DraftOrderMaster SET CustomerAutoId=@CustomerAutoId WHERE DraftAutoId=@DraftAutoId                                              
                                                                        
   END                                                                      
  ELSE IF @Opcode=403                                                                    
   BEGIN  
		DECLARE @custType2 int=(SELECT CustomerType FROM CustomerMaster WHERE AutoId=@CustomerAutoId)  

		SELECT 
		CASE WHEN @custType2=2 THEN ISNULL(WHminPrice,0)
		WHEN @custType2=3 THEN ISNULL(CostPrice,0)
		ELSE [MinPrice] END as [MinPrice],

		convert(decimal(10,2),[CostPrice]) as [CostPrice],
		convert(decimal(10,2),[Price]) as [Price],CONVERT(DECIMAL(10,2),((PM.[P_SRP]-([Price]/(CASE WHEN QTY=0 then 1 else [Qty] end)))/(CASE WHEN PM.[P_SRP]=0 then 1 else PM.[P_SRP] end )) * 100) AS GP,                                                                 
		isnull((select Top 1 [CustomPrice] from [dbo].[ProductPricingInPriceLevel] AS PPL where PPL.[ProductAutoId] = PD.[ProductAutoId] AND PPL.[UnitAutoId] = PD.[UnitType]                                                                 
		AND [PriceLevelAutoId] = (SELECT [PriceLevelAutoId] FROM [dbo].[CustomerPriceLevel] WHERE [CustomerAutoId]=@CustomerAutoId)                                                                    
		Order by [CustomPrice] desc),0) as [CustomPrice],PM.[Stock]/(CASE WHEN Qty=0 then 1 else Qty end)  as Stock,    
		PM.[TaxRate],convert(decimal(10,2),PM.[P_SRP]) as [SRP] FROM [dbo].[PackingDetails] As PD                                                                     
		INNER JOIN [dbo].[ProductMaster] AS PM ON PD.[ProductAutoId]=PM.[AutoId]                                                                        
		WHERE PD.[ProductAutoId] = @ProductAutoId and PD.[UnitType] = @UnitAutoId                                                                    
   END                                                                     
  ELSE IF @Opcode=404                                                                    
   BEGIN                     
    SELECT pm.ProductName,UM.AutoId AS AutoId, UM.[UnitType] AS UnitType, [Qty],(Stock /(CASE WHEN pd.Qty=0 then 1 else PD.Qty end) )as AvailableQty,                                                                    
    Price,EligibleforFree,pm.Stock as PStock  FROM [dbo].[PackingDetails] AS PD                                                                    
    INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = PD.UnitType                                                                    
    INNER JOIN ProductMaster as pm on pm.AutoId =PD.[ProductAutoId] WHERE PD.[ProductAutoId] = @ProductAutoId  
	and pd.PackingStatus=1
    SELECT PackingAutoId AS AutoId FROM ProductMaster  WHERE  AutoId=@ProductAutoId                                                                    
   END                                                                       
  ELSE IF @Opcode=405                                                                    
   BEGIN                         
    SELECT ROW_NUMBER() OVER(ORDER BY [AutoId] desc) AS RowNumber, * INTO #Results from                                                                    
    (                                                                       
    SELECT OM.[AutoId],OM.OrderNo,CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,CM.[CustomerName] AS CustomerName,                                                                    
  SM.StatusType AS Status,OM.[Status] As StatusCode,OM.[GrandTotal],                                                                    
    (SELECT COUNT(distinct [ProductAutoId]) from [dbo].[OrderItemMaster] WHERE OrderAutoId=OM.AutoId) AS NoOfItems                                                                     
    FROM [dbo].[OrderMaster] As OM                                                                    
    INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.AutoId = OM.CustomerAutoId                                                                     
    INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.[Status] AND SM.Category = 'OrderMaster'                                                                              
    WHERE (@OrderNo is null or @OrderNo ='' or OrderNo like '%' + @OrderNo + '%')                                                 
    and (@CustomerAutoId is null or @CustomerAutoId =0 or CustomerAutoId =@CustomerAutoId)                                                                    
    and (@FromDate is null or @FromDate = '' or @Todate is null or @Todate = '' or ([OrderDate] between @FromDate and @Todate))                                     
    and (@OrderStatus is null or @OrderStatus=0 or OM.[Status]=@OrderStatus) and                                                                     
     (@SalesPersonAutoId = 0 or OM.[SalesPersonAutoId]=@SalesPersonAutoId)                                                                    
     and OM.ShippingType!=5 and OM.ShippingType!=6                                                                    
    )as t order by [AutoId]  desc                                                                  
                                                                    
    SELECT COUNT(*) as RecordCount,@PageSize as PageSize,@PageIndex as PageIndex FROM #Results                   
    SELECT * FROM #Results                                          
    WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1                                                                    
   END                                                                    
  ELSE IF @Opcode=42                                                                    
   BEGIN 
		SET @CustomerAutoId= (SELECT [CustomerAutoId] FROM [dbo].[OrderMaster] WHERE AutoId=@OrderAutoId)

		SELECT BA.[AutoId] As BillAddrAutoId,[dbo].[ConvertFirstLetterinCapital](BA.[Address]+(case when isnull(BA.Address2,'')!='' then ', '+BA.Address2 else '' end)+', '+BA.City)+', '+UPper(St.StateCode)+' - '+BA.Zipcode  as BillingAddress,IsDefault FROM [dbo].[BillingAddress] AS BA                                                                    
		INNER JOIN [dbo].[State] AS St ON St.AutoId = BA.State 
		WHERE BA.[CustomerAutoId] = @CustomerAutoId  

		SELECT SA.[AutoId] As ShipAddrAutoId,[dbo].[ConvertFirstLetterinCapital](SA.[Address]+(case when isnull(SA.Address2,'')!='' then ', '+SA.Address2 else '' end)+', '+SA.City)+', '+St.StateCode+' - '+SA.Zipcode as ShippingAddress,IsDefault FROM [dbo].[ShippingAddress] AS SA                                                                    
		INNER JOIN [dbo].[State] AS St ON St.AutoId = SA.State
		WHERE SA.CustomerAutoId = @CustomerAutoId                                                                     
                                                                           
		SET @OrderStatus = (SELECT [Status] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId)                                                                        
                                                                         
		SELECT CM.CustomerType,OM.[AutoId],OM.[OrderNo],CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,                                                                    
		ISNULL((CONVERT(VARCHAR(20), OM.[DeliveryDate], 101)),                                                              
		(CONVERT(VARCHAR(20), OM.DelDate,101) + ' ' + convert(varchar(10), DelDate, 108))                                                         
		) AS DeliveryDate,                                                                    
		OM.[CustomerAutoId],CT.[TermsDesc],OM.[BillAddrAutoId],OM.[ShipAddrAutoId],OM.[Status] As StatusCode,DO.[AmtPaid],DO.[AmtDue],                                                                    
		SM.[StatusType] AS Status,OM.[TotalAmount],OM.[OverallDisc],OM.[OverallDiscAmt],OM.[ShippingCharges],OM.[TotalTax],OM.[GrandTotal]
		,OM.[PackedBoxes],CommentType,Comment, isnull(OM.AdjustmentAmt,0.00) as AdjustmentAmt,                
		OM.[Driver],EM.[FirstName]+ ' ' + EM.[LastName] AS DrvName,SALES.[FirstName]+ ' ' + SALES.[LastName] AS SALESNAME,OM.[ShippingType],
		CONVERT(VARCHAR(20),                                                                    
		OM.[AssignDate],101) AS AssignDate,[DrvRemarks],isnull(DeductionAmount,0.00) as DeductionAmount,                                                                    
		isnull(OM.CreditAmount,0.00)as CreditAmount,isnull(OM.PayableAmount,0.00) as PayableAmount,TaxType                                                                       
		,ISNULL(paidAmount,0) as paidAmount,paymentMode,referNo,OrderRemarks,                      
		(SELECT ccm.CreditAmount FROM CustomerCreditMaster as ccm WHERE ccm.CustomerAutoId=OM.CustomerAutoId)                                               
		as StoreCredit,ISNULL(PastDue,0.00) as PastDue ,ISNULL(MLQty,0) as MLQty  ,ISNULL(om.MLTax,0) as MLTax,Weigth_OZQty,Weigth_OZTaxAmount FROM [dbo].[OrderMaster] As OM                                                                    
		INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.Status AND SM.[Category] = 'OrderMaster'                                                      
		INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                        
		INNER JOIN [CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]	
		LEFT JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[Driver]                                                                     
		LEFT JOIN [dbo].[EmployeeMaster] AS SALES ON SALES.[AutoId] = cm.SalesPersonAutoId                                                                    
		left JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId]                                                                    
		WHERE OM.AutoId = @OrderAutoId 
	
		IF EXISTS(SELECT [AutoId] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId AND ([PaymentRecev] IS NOT NULL OR [PaymentRecev] != ''))                      
		BEGIN  	
			SELECT PM.[AutoId] AS ProductAutoId,PM.[ProductId],PM.[ProductName],UM.AutoId AS UnitAutoId,UM.[UnitType],
			OIM.[QtyPerUnit],isnull(OIM.Del_MinPrice,0.00) as OM_MinPrice,isnull(OIM.Del_CostPrice,0.00) as OM_CostPrice,                                                                    
			OIM.[UnitPrice],convert(decimal(10,2),UnitPrice/QtyPerUnit) as PerpiecePrice,                                           
			isnull(OIM.Del_MinPrice,0.00) as [MinPrice],OIM.BasePrice,oim.Barcode,                                  
			PD.[Price],OIM.[QtyShip] as RequiredQty,[TotalPieces],OIM.[SRP],OIM.[GP],OIM.[Tax],OIM.[NetPrice],OIM.[Barcode],OIM.[QtyShip],                                                                    
			0 as RemainQty,IsExchange,isFreeItem,UnitMLQty,isnull((oim.Del_Weight_Oz),0) as WeightOz,Del_discount,Del_ItemTotal
 FROM [dbo].[Delivered_Order_Items] AS OIM                                                                     
			INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                                                                    
			INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitAutoId                                                                     
			INNER JOIN [dbo].[PackingDetails] AS PD ON PD.[ProductAutoId] = OIM.[ProductAutoId] AND PD.[UnitType] = OIM.UnitAutoId                                                                     
			WHERE [OrderAutoId]=@OrderAutoId   
		END
		ELSE
		BEGIN                                  
			SELECT PM.[AutoId] AS ProductAutoId,PM.[ProductId],PM.[ProductName],UM.AutoId AS UnitAutoId,UM.[UnitType],OIM.[QtyPerUnit],isnull(OIM.OM_MinPrice,0.00) as OM_MinPrice,isnull(OIM.OM_CostPrice,0.00) as OM_CostPrice,                                                                    
			OIM.[UnitPrice],convert(decimal(10,2),UnitPrice/QtyPerUnit) as PerpiecePrice,                                           
			isnull(OIM.OM_MinPrice,0.00) as [MinPrice],OIM.BasePrice,oim.Barcode,                                  
			PD.[Price],OIM.[RequiredQty],[TotalPieces],OIM.[SRP],OIM.[GP],OIM.[Tax],OIM.[NetPrice],OIM.[Barcode],OIM.[QtyShip],                                                                    
			OIM.[RemainQty],IsExchange,isFreeItem,UnitMLQty,isnull((oim.Weight_Oz),0) as WeightOz,Oim_Discount as Del_discount,Oim_ItemTotal as Del_ItemTotal FROM [dbo].[OrderItemMaster] AS OIM                                                                     
			INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                                                                    
			INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitTypeAutoId                                                                     
			INNER JOIN [dbo].[PackingDetails] AS PD ON PD.[ProductAutoId] = OIM.[ProductAutoId] AND PD.[UnitType] = OIM.UnitTypeAutoId                                                                     
			WHERE [OrderAutoId]=@OrderAutoId                                                                     
		END  
	
		select OrderAutoId,CreditAutoId,CreditNo,CONVERT(VARCHAR(10),CreditDate,101) AS CreditDate,ISNULL(TotalAmount,0.00) as ReturnValue,ISNULL(PaidAmount,0.00)  as amtDeducted,                                                                    
		ISNULL(BalanceAmount,0.00) as amtDue,                                                                    
		CASE WHEN CreditType=1 THEN 'ERP CREDIT' ELSE 'POS CREDIT' END AS CreditType                                                
		from CreditMemoMaster AS CM  where [OrderAutoId]=@OrderAutoId  
		
		SELECT MLTaxPer as TaxRate FROM [dbo].[OrderMaster] As OM WHERE OM.AutoId = @OrderAutoId     
                   
		select * from Tax_Weigth_OZ
	                                              
   END                                                                    
  ELSE IF @Opcode=407                                                    
   BEGIN                                          
    SELECT [AutoId],[StatusType] FROM [dbo].[StatusMaster] WHERE [Category] = 'OrderMaster'                              
    IF(@LoginEmpType != 2)                                                                    
     BEGIN                                                                        
      SELECT [AutoId],[CustomerId] + ' - ' + [CustomerName] As Customer FROM [dbo].[CustomerMaster]   where status=1                             
     END                                                                    
    ELSE                                                                    
     BEGIN                                                                    
      SELECT [AutoId],[CustomerId] + ' - ' + [CustomerName] As Customer FROM [dbo].[CustomerMaster] WHERE [SalesPersonAutoId]=@SalesPersonAutoId                                                      
   and status=1                                                               
     END                                                            
   END                                                        
                                                                      
ELSE IF @Opcode=41                                                                    
   BEGIN  
		set @ShippingAutoId=(select State from BillingAddress where CustomerAutoId=@CustomerAutoId and IsDefault=1)    
		SET @TaxType = (SELECT TOP 1 State FROM BillingAddress  WHERE CustomerAutoId=@CustomerAutoId and IsDefault=1) 

		Select * from (
		Select
		(SELECT BA.[AutoId] As BillAddrAutoId,[dbo].[ConvertFirstLetterinCapital](BA.[Address]+(case when isnull(BA.Address2,'')!='' then ', '+BA.Address2 else '' end)+', '+BA.City)+', '+UPper(St.StateCode)+' - '+BA.Zipcode  as BillingAddress,IsDefault FROM [dbo].[BillingAddress] AS BA                                                                    
		INNER JOIN [dbo].[State] AS St ON St.AutoId = BA.State 
		WHERE BA.[CustomerAutoId] = @CustomerAutoId for json path, INCLUDE_NULL_VALUES) as BillingAddress,

		(SELECT SA.[AutoId] As ShipAddrAutoId,[dbo].[ConvertFirstLetterinCapital](SA.[Address]+(case when isnull(SA.Address2,'')!='' then ', '+SA.Address2 else '' end)+', '+SA.City)+', '+St.StateCode+' - '+SA.Zipcode as ShippingAddress,IsDefault FROM [dbo].[ShippingAddress] AS SA                                                                    
		INNER JOIN [dbo].[State] AS St ON St.AutoId = SA.State
		WHERE SA.CustomerAutoId = @CustomerAutoId for json path, INCLUDE_NULL_VALUES) as ShippingAddress,

		(SELECT CreditAmount FROM CustomerCreditMaster WHERE CustomerAutoId = @CustomerAutoId for json path, INCLUDE_NULL_VALUES) as CreditAmount,

		(SELECT EMP.FirstName+ '  ' +EMP.LastName AS EMPNAME,CustomerType  FROM CustomerMaster AS CM INNER JOIN                                                                    
		EmployeeMaster AS EMP ON EMP.AutoId=CM.SalesPersonAutoId WHERE cm.AutoId = @CustomerAutoId for json path, INCLUDE_NULL_VALUES) as EmployeeName,

		(Select * from (select OrderAutoId,CreditAutoId,CreditNo,CONVERT(VARCHAR(10),CreditDate,101) AS CreditDate,ISNULL(TotalAmount,0.00) as ReturnValue,ISNULL(PaidAmount,0.00)  as amtDeducted,                                                                    
		ISNULL(BalanceAmount,0.00) as amtDue,                                                                    
		CASE WHEN CreditType=1 THEN 'ERP CREDIT' ELSE 'POS CREDIT' END AS CreditType                                                                    
		from CreditMemoMaster AS CM  where Status =3 AND CustomerAutoId=@CustomerAutoId and OrderAutoId is null 	  
		Union
		select OrderAutoId,CreditAutoId,CreditNo,CONVERT(VARCHAR(10),CreditDate,101) AS CreditDate,ISNULL(TotalAmount,0.00) as ReturnValue,ISNULL(PaidAmount,0.00)  as amtDeducted,                                                                    
		ISNULL(BalanceAmount,0.00) as amtDue,                                                                    
		CASE WHEN CreditType=1 THEN 'ERP CREDIT' ELSE 'POS CREDIT' END AS CreditType                                                
		from CreditMemoMaster AS CM  where [OrderAutoId]=@OrderAutoId ) as t for json path, INCLUDE_NULL_VALUES) as CreditDetails,                                                
                                                                                    
		(SELECT CustomerAutoId,[AutoId],[OrderNo],CONVERT(VARCHAR(20),[OrderDate],101) AS OrderDate,DO.[AmtDue],DO.[GrandTotal],DO.[AmtPaid]                                                                    
		FROM [dbo].[OrderMaster] As OM                                                                    
		INNER JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId]                                                                         
		WHERE [CustomerAutoId] = @CustomerAutoId AND                                   
		[Status] = 11 AND DO.[AmtDue] > 0.00  for json path, INCLUDE_NULL_VALUES ) as OrderDetails,  
	  
		(SELECT * FROM TaxTypeMaster where status=1 and State=@ShippingAutoId  for json path, INCLUDE_NULL_VALUES ) as TaxTypeMaster,   
	  
		(SELECT ISNULL((SELECT TaxRate FROM MLTaxMaster WHERE TaxState=@TaxType),0) as TaxRate,                                            
		CASE WHEN (SELECT TaxRate FROM MLTaxMaster WHERE TaxState=@TaxType) IS NULL THEN 0 ELSE 1 END                                           
		AS MLTaxType for json path, INCLUDE_NULL_VALUES ) as MLTaxRate,                                       
                    
		(select ID,Value,PrintLabel from Tax_Weigth_OZ  for json path, INCLUDE_NULL_VALUES ) as WeightTaxRate,  
		
		(select CustomerName from CustomerMaster where AutoId=@CustomerAutoId for json path, INCLUDE_NULL_VALUES ) as CustomerName
		) as CustomerDetails for json path, INCLUDE_NULL_VALUES
   END                                                                    
                                                                      
  ELSE IF @Opcode=409                                                                    
   BEGIN                                                                           
    SELECT [AutoId],[StateCode] + ' - ' + [StateName] AS StateName FROM [dbo].[State]                                                                    
   END                                                                    
  ELSE IF @Opcode=411                                                                    
   BEGIN                                                                    
    SELECT [ProductAutoId],[UnitAutoId],PM.[Stock] FROM [dbo].[ItemBarcode] AS IB                                                                  
    INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = IB.[ProductAutoId] WHERE [Barcode]=@Barcode                                                                    
   END                                                                    
  Else If @Opcode=427                                                                    
   Begin                                                         
   SELECT PM.[Stock],Barcode FROM [dbo].[ItemBarcode] AS IB                               
    INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = IB.[ProductAutoId] WHERE ProductAutoId=@ProductAutoId                                                                     
    and IB.UnitAutoId=@UnitAutoId                                                                    
   End                                                                    
  ELSE IF @Opcode=412                                                   
   BEGIN                                                                    
    SELECT [AutoId],[FirstName] + ' ' + [LastName] As Name FROM [dbo].[EmployeeMaster] WHERE [EmpType]=5                                                                    
    SELECT [OrderNo],[Driver] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId                                                                    
  END                                                                    
  ELSE IF @Opcode=413                                                                    
   BEGIN                                                                    
    SELECT [AutoId],[FirstName] + ' ' + [LastName] As Driver FROM [dbo].[EmployeeMaster] WHERE [EmpType] = 5                                                                 
   END                                                                    
                                                                      
  ELSE IF @Opcode=416                                                                    
   BEGIN                                                                    
    UPDATE [dbo].[OrderMaster] SET [Status] = 5 WHERE [AutoId] = @OrderAutoId                                 
                                                                    
  SET @LogRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=3),'[Status1]','Ready to ship')                                                                    
    SET @LogRemark = REPLACE(@LogRemark,'[Status2]','Shipped')                                                      
    INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                                                    
    VALUES(3,@EmpAutoId,getdate(),@LogRemark,@OrderAutoId)                                                                    
                                                 
   END                                                                    
  ELSE IF @Opcode=417                                                               
   BEGIN                                                                    
    SELECT OM.[AutoId],OM.[OrderNo],CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,CONVERT(VARCHAR(20), OM.[DeliveryDate], 101) AS DeliveryDate,                                                                    
    CM.[CustomerId],CM.[CustomerName],CTy.CustomerType,CM.[Contact1] + '/' + CM.[Contact2] As Contact,CT.[TermsDesc],ST.[ShippingType],BA.[Address] As BillAddr,S.[StateName] AS State1,                                         
    BA.[City] AS City1,BA.[Zipcode] As Zipcode1,SA.[Address] As ShipAddr,S1.[StateName] As State2,SA.[City] AS City2,SA.[Zipcode] As Zipcode2,                                                                    
    OM.[Status] As StatusCode,SM.[StatusType] AS Status,OM.[TotalAmount],OM.[OverallDisc],OM.[OverallDiscAmt],OM.[ShippingCharges],OM.[TotalTax],                                                                    
    OM.[GrandTotal],EM.[FirstName] + ' ' + EM.[LastName] As SalesPerson  FROM [dbo].[OrderMaster] As OM                                                                    
    INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.Status AND SM.[Category] = 'OrderMaster'                                                   
    INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[AutoId] = OM.[BillAddrAutoId]                                                                    
    INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = OM.[ShipAddrAutoId]                                                                    
    INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                                                                     
    INNER JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                                                                     
    INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                                          
    INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]                                                                    
    LEFT JOIN [dbo].[CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                                                              
    LEFT JOIN [dbo].[ShippingType] AS ST ON ST.[AutoId] = OM.[ShippingType]                             
    INNER JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[SalesPersonAutoId] WHERE OM.AutoId = @OrderAutoId                                                                    
                                                                    
    SELECT CM.[CategoryName],CM.[CategoryId],OIM.ProductAutoId,PM.[ProductId],PM.[ProductName],PM.[ProductLocation],                                                                    
    OIM.[UnitTypeAutoId],UM.[UnitType],OIM.[QtyPerUnit],OIM.[UnitPrice],OIM.[TotalPieces],OIM.[SRP],OIM.[GP],OIM.[Tax],                                                                    
    OIM.[NetPrice],OIM.[Barcode],OIM.[QtyShip],OIM.[RequiredQty] FROM [dbo].[OrderItemMaster] AS OIM                                                                     
    INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                                                
    INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitTypeAutoId                                                                     
    INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId] WHERE [OrderAutoId] = @OrderAutoId                                                                     
    ORDER BY CM.[CategoryName] ASC                                                                    
                                                                    
                                                                        
                                             
    SET @LogRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=3),'[Status1]','New')                                                                    
    SET @LogRemark = REPLACE(@LogRemark,'[Status2]','Processed')                                                                    
    if Exists(select Status from OrderMaster where AutoId=@OrderAutoId and Status=1)                                                                    
     Begin                                                                    
 UPDATE [dbo].[OrderMaster] SET [Status] = 2 WHERE [AutoId]=@OrderAutoId AND [Status] = 1                                                                    
      INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                                                    
    VALUES(3,@EmpAutoId,getdate(),@LogRemark,@OrderAutoId)                                                                    
     End                     
                                                              
   END                                                                    
  ELSE IF @Opcode=418                                                                    
   BEGIN                                                                      
                                                                       
                                                                        
                                                                        
       SELECT [Address],[Website],[MobileNo],[FaxNo],TermsCondition,Logo FROM CompanyDetails                                                                    
    IF EXISTS(SELECT [AutoId] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId AND ([PaymentRecev] IS NOT NULL OR [PaymentRecev] != ''))                              
     BEGIN                                                               
      SELECT OM.[AutoId],OM.[OrderNo],CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,ISNULL(CONVERT(VARCHAR(20), OM.[DeliveryDate],101),CONVERT(VARCHAR(20), OM.[DelDate],101) + ' ' +convert(varchar(10), OM.[DelDate], 108)) AS DeliveryDate,       
  
    
      
        
      CM.[AutoId] As CustAutoId,CM.[CustomerId],CM.[CustomerName],CM.[Contact1] + '/' + CM.[Contact2] As Contact,CTy.CustomerType,CT.[TermsDesc],ST.[ShippingType],                                                                    
      BA.[Address] As BillAddr,                                       
      S.[StateName] AS State1,BA.[City] AS City1,BA.[Zipcode] As Zipcode1,SA.[Address] As ShipAddr,S1.[StateName] As State2,SA.[City] AS City2,                                                                    
      SA.[Zipcode] As Zipcode2,OM.[Status] As StatusCode,SM.[StatusType] AS Status,OM.[PackedBoxes],DO.[TotalAmount],DO.[OverallDisc],DO.[OverallDiscAmt],                                                                    
      DO.[ShippingCharges],DO.[TotalTax],DO.[GrandTotal],DO.[AmtPaid],DO.[AmtDue],OM.[PaymentRecev],OM.[RecevAmt],OM.[AmtValue],OM.[ValueChanged],                                                                    
      OM.[DiffAmt],OM.[NewTotal],OM.[PayThru],OM.CheckNo,OM.[DrvRemarks],OM.[AcctRemarks],EM.[FirstName] + ' ' + EM.[LastName] As SalesPerson,                                 
      EM1.[FirstName] + ' ' + EM1.[LastName] As PackerName,[CommentType],[Comment],isnull(DeductionAmount,0.00) as DeductionAmount,                                                                    
            ISNULL(DO.CreditAmount,0.00)as CreditAmount,isnull(do.PayableAmount,0.00) as PayableAmount,                                                                    
            ISNULL((select t.CreditAmount from CustomerCreditMaster as t  where t.CustomerAutoId=om.CustomerAutoid),0.00) as CustomerCredit                                                                    
            FROM [dbo].[OrderMaster] As OM                              
      INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.Status AND SM.[Category] = 'OrderMaster'                                                                    
      INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[AutoId] = OM.[BillAddrAutoId]                                                                    
      INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = OM.[ShipAddrAutoId]                                                                   
      INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                                                                     
      INNER JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                                                                     
      INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                      
      INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]                                                                    
      LEFT JOIN [dbo].[CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                                                                    
      LEFT JOIN [dbo].[ShippingType] AS ST ON ST.[AutoId] = OM.[ShippingType]                                                                     
      INNER JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[SalesPersonAutoId]                                                                    
      INNER JOIN [dbo].[EmployeeMaster] AS EM1 ON EM1.[AutoId] = OM.[PackerAutoId]                                     
      INNER JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId] WHERE OM.AutoId = @OrderAutoId                                                                    
                                                                    
      SELECT CM.[CategoryName],CM.[CategoryId],DOI.[ProductAutoId],PM.[ProductId],PM.[ProductName],DOI.[UnitAutoId],UM.[UnitType],DOI.[QtyPerUnit],                                                                    
      DOI.[UnitPrice],DOI.[Barcode],DOI.[QtyShip],DOI.[QtyDel],DOI.[TotalPieces],DOI.[SRP],DOI.[GP],DOI.[Tax],DOI.[NetPrice]                                                                    
      ,convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice,ISNULL(FreshReturnQty,0) as FreshReturnQty,FreshReturnUnitAutoId,                                                          
      ISNULL(DamageReturnQty,0) as DamageReturnQty,DamageReturnUnitAutoId            
      FROM [dbo].[Delivered_Order_Items] AS DOI                                                                     
      INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = DOI.[ProductAutoId]                                                                    
     INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = DOI.UnitAutoId                                                                     
      INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId] WHERE [OrderAutoId]=@OrderAutoId                                                                    
                                                          
      SET @CustomerAutoId = (SELECT [CustomerAutoId] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId)                                                                    
                                                 
      SELECT [AutoId],[OrderNo],CONVERT(VARCHAR(20),[OrderDate],101) AS OrderDate,DO.[AmtDue],DO.[GrandTotal],DO.[AmtPaid]                                                      
      FROM [dbo].[OrderMaster] As OM                                                                    
      INNER JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId]                                 
      WHERE [CustomerAutoId] = @CustomerAutoId AND [Status] = 6 AND OM.[AmtDue] != 0.00                                                                                  
      SELECT [AmountPaid],CONVERT(VARCHAR(20),PL.[PayDate],101) AS PayDate,EM.FirstName + ' ' + EM.[LastName] As EmpName,[Remarks] FROM [dbo].[PaymentLog] AS PL                                                                     
    INNER JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = PL.EmpAutoId WHERE [OrderAutoId] = @OrderAutoId                                                                    
                                                                    
      SELECT @LoginEmpType As LoginEmpType                     
                                                                          
      select ProductId,um.UnitType,pd.UnitType as UnitAutoId,Qty from Delivered_Order_Items as do                                                                    
      inner join ProductMaster as pm on pm.AutoId=do.ProductAutoId                                                   
      inner join PackingDetails as pd on pd.ProductAutoId=pm.AutoId                                                                    
      inner join UnitMaster as um on um.AutoId=pd.UnitType                                                                    
      where OrderAutoId=@OrderAutoId                             
                                                                          
END                                                                    
    ELSE                                                                    
     BEGIN                                                                    
      SELECT OM.[AutoId],OM.[OrderNo],CONVERT(VARCHAR(20),OM.[OrderDate], 101) AS OrderDate,                            
      CONVERT(VARCHAR(20), OM.[DeliveryDate],101) + ' ' + convert(varchar(10), getdate(), 108)--CONVERT(VARCHAR(20),OM.[DeliveryDate], 131)                                                                     
      AS DeliveryDate,                                                                    
      CM.[AutoId] As CustAutoId,CM.[CustomerId],CM.[CustomerName],CM.[Contact1] + '/' + CM.[Contact2] As Contact,CTy.CustomerType,CT.[TermsDesc],ST.[ShippingType],                                                                    
      BA.[Address] As BillAddr,S.[StateName] AS State1,BA.[City] AS City1,BA.[Zipcode] As Zipcode1,SA.[Address] As ShipAddr,S1.[StateName] As State2,                                                                    
      SA.[City] AS City2,SA.[Zipcode] As Zipcode2,OM.[Status] As StatusCode,SM.[StatusType] AS Status,OM.[TotalAmount],OM.[OverallDisc],OM.[OverallDiscAmt],                         
      OM.[ShippingCharges],OM.[TotalTax],OM.[GrandTotal],OM.[PackedBoxes],EM.[FirstName] + ' ' + EM.[LastName] As SalesPerson,                                                                    
      EM1.[FirstName] + ' ' + EM1.[LastName] As PackerName,OM.[DrvRemarks],[CommentType],[Comment]                                                                    
      ,isnull(DeductionAmount,0.00) as DeductionAmount,                                                                    
            isnull(CreditAmount,0.00)as CreditAmount,isnull(PayableAmount,0.00) as PayableAmount                                                                    
            ,ISNULL((select t.CreditAmount from CustomerCreditMaster as t  where t.CustomerAutoId=om.CustomerAutoid),0.00) as CustomerCredit                                                                    
            FROM [dbo].[OrderMaster] As OM                                                              
      INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.Status AND SM.[Category] = 'OrderMaster'                                                                    
      INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[AutoId] = OM.[BillAddrAutoId]                                                                   
      INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = OM.[ShipAddrAutoId]                                                                    
      INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                                                  
      INNER JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                                                                     
      INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                          
      INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]                                                                    
      LEFT JOIN [dbo].[CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                                  
      LEFT JOIN [dbo].[ShippingType] AS ST ON ST.[AutoId] = OM.[ShippingType]                                                                     
      INNER JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[SalesPersonAutoId]                                                                           
      INNER JOIN [dbo].[EmployeeMaster] AS EM1 ON EM1.[AutoId] = OM.[PackerAutoId] WHERE OM.AutoId = @OrderAutoId                                                          
                                                                    
      SELECT CM.[CategoryName],CM.[CategoryId],OIM.ProductAutoId,PM.[ProductId],PM.[ProductName],PM.[ProductLocation],                                                                    
      OIM.[UnitTypeAutoId] AS UnitAutoId,UM.[UnitType],OIM.[QtyPerUnit],OIM.[UnitPrice],OIM.[TotalPieces],OIM.[SRP],OIM.[GP],OIM.[Tax],                                                                    
      OIM.[NetPrice],OIM.[Barcode],OIM.[QtyShip],OIM.[RequiredQty] ,convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice                                              
      FROM [dbo].[OrderItemMaster] AS OIM                                                                     
      INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                                                                    
      INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitTypeAutoId                                                                     
      INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId] WHERE [OrderAutoId] = @OrderAutoId                                                                     
      ORDER BY CM.[CategoryName] ASC                                                                
                                                                    
      SET @CustomerAutoId = (SELECT [CustomerAutoId] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId)                                                                    
                                                                    
      SELECT [AutoId],[OrderNo],CONVERT(VARCHAR(20),[OrderDate],101) AS OrderDate,DO.[AmtDue],DO.[GrandTotal],DO.[AmtPaid]                                                                     
      FROM [dbo].[OrderMaster] As OM                                                                    
      INNER JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId]                                                                        
      WHERE [CustomerAutoId] = @CustomerAutoId AND [Status] = 6 AND OM.[AmtDue] != 0.00                                                                     
                                                    
                                                                     
      SELECT [AmountPaid],CONVERT(VARCHAR(20),PL.[PayDate],101) AS PayDate,EM.FirstName + ' ' + EM.[LastName] As EmpName,[Remarks] FROM [dbo].[PaymentLog] AS PL                                                                     
    INNER JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = PL.EmpAutoId WHERE [OrderAutoId] = @OrderAutoId                                                                    
                                                                    
      SELECT @LoginEmpType As LoginEmpType                                                                    
                                                                          
                                                                          
      select ProductId,um.UnitType,pd.UnitType as UnitAutoId,Qty from OrderItemMaster as do                                                                    
      inner join ProductMaster as pm on pm.AutoId=do.ProductAutoId                                                                    
      inner join PackingDetails as pd on pd.ProductAutoId=pm.AutoId                                                                    
      inner join UnitMaster as um on um.AutoId=pd.UnitType                                                                    
      where OrderAutoId=@OrderAutoId                                                                    
     END                                             
                                                   
    select OrderAutoId,CreditAutoId,CreditNo,CONVERT(VARCHAR(10),CreditDate,101) AS CreditDate,ISNULL(TotalAmount,0.00) as ReturnValue,ISNULL(PaidAmount,0.00)  as amtDeducted,                                                                    
    ISNULL(BalanceAmount,0.00) as amtDue                                                                    
                from CreditMemoMaster AS CM  where Status =3 --AND CustomerAutoId=@CustomerAutoId                                                                     
   AND OrderAutoId=@OrderAutoId                                                                     
   END                                          
                                                                      
                                                                       
   ELSE IF @Opcode=421                                                                    
    BEGIN                                                                    
     SELECT CM.[CustomerName],S.[StateName] As State,SA.[City],SA.[Zipcode],[PackedBoxes] FROM [dbo].[OrderMaster] As OM                                                                    
     INNER JOIN [dbo].[ShippingAddress] AS SA ON SA.[AutoId] = OM.[ShipAddrAutoId]                                                                     
     INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                                                     
     INNER JOIN [dbo].[State] AS S ON S.[AutoId] = SA.[State] WHERE OM.[OrderNo] = @OrderNo                                                        
    END        
   ELSE IF @Opcode=422                                                                     
    BEGIN                                                                    
     SELECT Distinct TOP 25 OIM.[ProductAutoId],PM.[ProductId],PM.[ProductName],PM.[Stock] FROM OrderMaster As OM                                                                    
     INNER JOIN OrderItemMaster AS OIM ON OIM.OrderAutoId = OM.[AutoId]                                                                     
     INNER JOIN [dbo].[ProductMaster] AS PM ON PM.[AutoId] = OIM.[ProductAutoId] WHERE [CustomerAutoId] = @CustomerAutoId                                                                    
    END                                                                    
    ELSE IF @Opcode=423                                                                    
    BEGIN                                                                    
       SELECT [AutoId],[ProductAutoId],[UnitAutoId] FROM [dbo].[ItemBarcode] WHERE [Barcode]=@Barcode                                                                    
    END                                                                    
	ELSE IF @Opcode=108                                                                    
    BEGIN 
		DECLARE @custType1 int,@AllowQtyInPieces int,@AllocationStatus int=1,@Om_BasePrice decimal(18,2)
		SET @custType1=(SELECT CustomerType FROM CustomerMaster	WHERE AutoId=@CustomerAutoId)
		SET @SalesPersonAutoId=(SELECT SalesPersonAutoId FROM CustomerMaster WHERE AutoId=@CustomerAutoId)
		
		Declare @availQty int=0

		IF EXISTS(SELECT * FROM DraftItemMaster WHERE DraftAutoId= @DraftAutoId AND ProductAutoId=@ProductAutoId AND                                                                                                      
		UnitAutoId=@UnitAutoId AND IsExchange=@IsExchange AND IsFreeItem=@IsFreeItem)                                                                 
		BEGIN  
			SET @availQty=ISNULL((SELECT ISNULL(ReqQty,0) FROM DraftItemMaster 
			WHERE DraftAutoId= @DraftAutoId AND ProductAutoId=@ProductAutoId AND                                                                                                      
			UnitAutoId=@UnitAutoId AND IsExchange=@IsExchange AND IsFreeItem=@IsFreeItem),0)
		END
		ELSE IF EXISTS(SELECT * FROM DraftItemMaster WHERE DraftAutoId= @DraftAutoId AND ProductAutoId=@ProductAutoId AND                                                                                                      
		UnitAutoId!=@UnitAutoId AND IsExchange=@IsExchange AND IsFreeItem=@IsFreeItem)
		BEGIN
		     SET @isException=1
			 SET @exceptionMessage='DifferentUnit'

		END
		IF @exceptionMessage!='DifferentUnit'
		BEGIN
			declare @Customrice decimal (10,2)
			
			if(@custType1=3)
			begin
				set @Customrice=(SELECT CASE WHEN @custType1=3 then ISNULL(CostPrice,0) else   [Price] end                                                                    
				FROM [dbo].[PackingDetails] As PD                                                                     
				INNER JOIN [dbo].[ProductMaster] AS PM ON PD.[ProductAutoId]=PM.[AutoId]      
				WHERE PD.[ProductAutoId] = @ProductAutoId and PD.[UnitType] = @UnitAutoId )    
			end
			else
			begin
			    set @Customrice=( SELECT top 1                                                  
				PPL.[CustomPrice] FROM [dbo].[PackingDetails] As PD                                                                     
				INNER JOIN [dbo].[ProductMaster] AS PM ON PD.[ProductAutoId]=PM.[AutoId]                                       
				LEFT JOIN [dbo].[ProductPricingInPriceLevel] AS PPL ON PPL.[ProductAutoId] = PD.[ProductAutoId] AND PPL.[UnitAutoId] = PD.[UnitType]                                                                    
				AND [PriceLevelAutoId] = (SELECT [PriceLevelAutoId] FROM [dbo].[CustomerPriceLevel] WHERE                                                                     
				[CustomerAutoId]=@CustomerAutoId)                      
				WHERE PD.[ProductAutoId] = @ProductAutoId and PD.[UnitType] = @UnitAutoId order by [CustomPrice] desc                                                                    
				) 
			eND			

			SET @UnitPrice =( SELECT CASE WHEN @custType1=3 then ISNULL(CostPrice,0) else   [Price] end                                                                    
			FROM [dbo].[PackingDetails] As PD                                                                     
			INNER JOIN [dbo].[ProductMaster] AS PM ON PD.[ProductAutoId]=PM.[AutoId]      
			WHERE PD.[ProductAutoId] = @ProductAutoId and PD.[UnitType] = @UnitAutoId                                                                    
			)                             
         
			SET @Om_BasePrice=( SELECT PD.Price FROM [dbo].[PackingDetails] As PD                                                                     
			INNER JOIN [dbo].[ProductMaster] AS PM ON PD.[ProductAutoId]=PM.[AutoId]      
			WHERE PD.[ProductAutoId] = @ProductAutoId and PD.[UnitType] = @UnitAutoId                                                                    
			)                             
		 
			SET @minprice =(SELECT (                                                                    
			CASE                                                                     
			WHEN @custType1=2 THEN ISNULL(WHminPrice,0)                                                                    
			WHEN @custType1=3 THEN ISNULL(CostPrice,0)                                                              
			ELSE [MinPrice] END) as [MinPrice]  FROM [dbo].[PackingDetails] As PD                                                                     
			INNER JOIN [dbo].[ProductMaster] AS PM ON PD.[ProductAutoId]=PM.[AutoId]                                                             
			WHERE PD.[ProductAutoId] = @ProductAutoId and PD.[UnitType] = @UnitAutoId                                                                    
			)        
	  
			set @UnitPrice = (case when @Customrice IS NULL then @UnitPrice else ( case when @Customrice < @minprice then  @UnitPrice else  @Customrice end) end)                                                                    
                                                                          
			SET @QtyPerUnit =( SELECT Qty FROM [dbo].[PackingDetails] As PD                                                                            
			WHERE PD.[ProductAutoId] = @ProductAutoId and PD.[UnitType] = @UnitAutoId                                                                    
			)                                                                    
			SET @SRP =ISNULL((SELECT PM.[P_SRP] FROM [dbo].[ProductMaster] As PM WHERE PM.[AutoId] = @ProductAutoId),0)        
			SET @GP =( SELECT                                                                     
			CONVERT(DECIMAL(10,2),((pm.[P_SRP]-([Price]/(CASE WHEN [Qty]=0 then 1 ELSE Qty end)))/(CASE WHEN pm.[P_SRP]=0 then 1 else pm.[P_SRP] end)) * 100) AS GP                                                                
			FROM [dbo].[PackingDetails] As PD         
			inner join ProductMaster as pm on pm.AutoId=pd.ProductAutoId                                                            
			WHERE PD.[ProductAutoId] = @ProductAutoId and PD.[UnitType] = @UnitAutoId                                       
			)                                                                    
			SET @TaxRate =( SELECT PM.[TaxRate] FROM  [dbo].[ProductMaster] AS PM WHERE PM.[AutoId] = @ProductAutoId                                     
			)   	
		
			SET @Barcode=ISNULL((Select top 1 Barcode FROM OrderItemMaster 
			where ProductAutoId=@ProductAutoId AND UnitTypeAutoId=@UnitAutoId order by AutoId desc),'')      
		
			IF EXISTS(SELECT * FROM AllowQtyPiece WHERE SalesPersonAutoId=@SalesPersonAutoId 
			and ProductAutoId=@ProductAutoId)  
			BEGIN  
				IF EXISTS(select * from AllowQtyPiece where CAST(AllowDate AS datetime) <= CAST(getdate() AS datetime) 
				AND SalesPersonAutoId=@SalesPersonAutoId AND ProductAutoId=@ProductAutoId)  
				BEGIN  
					SELECT @AllowQtyInPieces=(AllowQtyInPieces-UsedQty) FROM AllowQtyPiece WHERE SalesPersonAutoId=@SalesPersonAutoId
					and ProductAutoId=@ProductAutoId  
					IF (@AllowQtyInPieces>=@QtyPerUnit*(@ReqQty+ISNUll(@availQty,0)))
					BEGIN  
						SET @AllocationStatus=1 
					END
					ELSE
					BEGIN
						SET @AllocationStatus=0
						SET @AllocatQty=@QtyPerUnit*(@ReqQty+ISNUll(@availQty,0))
						Select @AllowQtyInPieces as AvailableQty,@AllocatQty-@AllowQtyInPieces  as RequiredQty,
						@ProductAutoId as ProductAutoId,'Less' as Message,@IsTaxable	
					END
				END 
			END
		 If ((select Stock from ProductMaster where AutoId=@ProductAutoId) >=(@QtyPerUnit*@ReqQty))
		 BEGIN
		  IF @AllocationStatus=1
		  BEGIN
			IF @DraftAutoId=0                                                                    
			BEGIN                                                                    
				INSERT INTO DraftOrderMaster([EmpAutoId],[CustomerAutoId],[OrderDate],[TotalItem],[ShippingType],[DeliveryDate],OrderType,Remarks)                                                             
				VALUES(@EmpAutoId,@CustomerAutoId,GETDATE(),1,@ShippingType,GETDATE(),1,@Remarks)                                                                    
				SET @DraftAutoId=SCOPE_IDENTITY()                                                                    
				IF @IsExchange=1 or @IsFreeItem=1                                                                    
				BEGIN                                                                    
					SET @UnitPrice=0                                                                    
					SET @MLQty=0                                                   
				END                                                                    
				INSERT INTO DraftItemMaster(DraftAutoId,ProductAutoId,UnitAutoId,ReqQty,QtyPerUnit,UnitPrice,minprice,SRP,GP,TaxRate,IsExchange,isFreeItem,IsTaxable,UnitMLQty,TotalMLQty,Barcode,Oim_Discount)                                                                     
				VALUES(@DraftAutoId,@ProductAutoId,@UnitAutoId,@ReqQty,@QtyPerUnit,@UnitPrice,@minprice,@SRP,@GP,@IsTaxable,@IsExchange,@IsFreeItem,@IsTaxable,@MLQty,(@MLQty*@ReqQty),@Barcode,@Oim_Discount)                                                                    
			END                                                                    
			ELSE                                                                     
			BEGIN                                                                
				IF NOT EXISTS(SELECT * FROM DraftItemMaster WHERE DraftAutoId= @DraftAutoId AND ProductAutoId=@ProductAutoId                                                                  
				AND UnitAutoId=@UnitAutoId AND IsExchange=@IsExchange AND IsFreeItem=@IsFreeItem)                                                                    
				BEGIN                                                   
					IF @IsExchange=1 or @IsFreeItem=1                                                                    
					BEGIN                                                                    
						SET @MLQty=0                                                                    
						SET @UnitPrice=0                                                                    
					END                                          
					INSERT INTO DraftItemMaster(DraftAutoId,ProductAutoId,UnitAutoId,ReqQty,QtyPerUnit,UnitPrice,minprice,SRP,GP,TaxRate,IsExchange,IsFreeItem,IsTaxable,UnitMLQty,TotalMLQty,Barcode,Oim_Discount)                                                                  
					VALUES(@DraftAutoId,@ProductAutoId,@UnitAutoId,@ReqQty,@QtyPerUnit,@UnitPrice,isnull(@minprice,0),isnull(@SRP,0),isnull(@GP,0),@IsTaxable,@IsExchange,@IsFreeItem,@IsTaxable,@MLQty,(@MLQty*@ReqQty),@Barcode,@Oim_Discount)                                             
					UPDATE DraftOrderMaster SET [TotalItem]=[TotalItem]+1,Remarks=@Remarks WHERE DraftAutoId=@DraftAutoId                                                                    
				END                                                                    
				else                                                             
				BEGIN                                                                    
					UPDATE DraftItemMaster SET ReqQty=ReqQty+@ReqQty,TotalMLQty=(@ReqQty*UnitMLQty) WHERE  DraftAutoId= @DraftAutoId                                                                    
					AND ProductAutoId=@ProductAutoId AND UnitAutoId=@UnitAutoId and IsExchange=@IsExchange AND IsFreeItem=@IsFreeItem 
				END                                                                
			END        
			update DraftOrderMaster set Remarks=@Remarks,ShippingType=@ShippingType where DraftAutoId=@DraftAutoId

			SELECT @DraftAutoId as DraftAutoId ,PM.AutoId,convert(varchar(20),[ProductId]) + '--' + [ProductName] as [ProductName],PD.UnitType,Qty,@ReqQty AS ReqQty,                                                                    
			isnull(@Customrice,@UnitPrice) as CustomPrice,isnull(@UnitPrice,0) AS UnitPrice,@Barcode as Barcode,                                                      
			isnull(@minPrice,0) AS minPrice,isnull(@UnitPrice,0) AS BasePrice,@SRP AS SRP,@GP AS GP,@TaxRate AS taxRate,                                                                    
			UM.UnitType,@Om_BasePrice as ProductBasePrice,PM.stock,(case when isnull(IsApply_ML,0)=0 then 0 else MLQty end) as ML,
			(case when isnull(IsApply_Oz,0)=0 then 0 else WeightOz end) as Oz,@Oim_Discount as Oim_Discount,0 as Del_ItemTotal
			FROM ProductMaster AS PM INNER JOIN PackingDetails AS PD ON PD.ProductAutoId =PM.AUTOID                                                                     
			INNER JOIN UnitMaster AS UM ON UM.AutoId=PD.UnitType
			WHERE PM.AutoId=@ProductAutoId AND PD.UnitType=@UnitAutoId                                                                    
		  END
		  ELSE
		  BEGIN
			   Select @AllowQtyInPieces as AvailableQty,@QtyPerUnit*@ReqQty  as RequiredQty,
			   @ProductAutoId as ProductAutoId,'Less' as Message
		  END
		 END
		 ELSE
		 BEGIN
		      SET @isException=1
			  SET @exceptionMessage='Stock'
		 END
		END
	END   
	ELSE IF @Opcode=109
	BEGIN	    
	  BEGIN TRY
		SET @SalesPersonAutoId=(SELECT SalesPersonAutoId FROM CustomerMaster WHERE AutoId=@CustomerAutoId)
		SET @QtyPerUnit =( SELECT Qty FROM [dbo].[PackingDetails] As PD                                                                            
		WHERE PD.[ProductAutoId] = @ProductAutoId and PD.[UnitType] = @UnitAutoId                                                                    
		) 
		IF EXISTS(SELECT * FROM AllowQtyPiece WHERE SalesPersonAutoId=@SalesPersonAutoId 
		and ProductAutoId=@ProductAutoId)  
		BEGIN  
			IF EXISTS(select * from AllowQtyPiece where CAST(AllowDate AS date) <= CAST(getdate() AS DATE) 
			AND SalesPersonAutoId=@SalesPersonAutoId AND ProductAutoId=@ProductAutoId)  
			BEGIN  
				SELECT @AllowQtyInPieces=(AllowQtyInPieces-UsedQty) FROM AllowQtyPiece WHERE SalesPersonAutoId=@SalesPersonAutoId and ProductAutoId=@ProductAutoId  
				IF (@AllowQtyInPieces>=(@QtyPerUnit*@ReqQty))  
				BEGIN				     
					SET @isException=0
					SET @exceptionMessage='Available'
				END
				ELSE
				BEGIN					
					 SELECT @AllowQtyInPieces as AvailableQty,@QtyPerUnit*@ReqQty as RequiredQty,
					 @ProductAutoId as ProductAutoId,'Less' as Message
				END
			END 
		END
	   END TRY
	   BEGIN CATCH
	         SET @isException=1
			 SET @exceptionMessage=ERROR_MESSAGE()
	   END CATCH
	END
    ELSE IF @Opcode=205                                               
    BEGIN                                                                    
		UPDATE DraftOrderMaster SET CustomerAutoId=@CustomerAutoId,ShippingType=@ShippingType,Remarks=@Remarks,OverallDisc=@OverallDisc,
		OverallDiscAmt=@OverallDiscAmt,ShippingCharges=@ShippingCharges
		WHERE DraftAutoId= @DraftAutoId         
    END                                                         
    ELSE IF @Opcode=206                                                                    
    BEGIN 
	declare @dd int=null
	if((select Stock from ProductMaster where AutoId=@ProductAutoId)>=@QtyPerUnit)
	begin
		UPDATE DraftItemMaster SET  ReqQty=@ReqQty,UnitPrice=@UnitPrice,TotalMLQty=(@ReqQty*UnitMLQty),Oim_Discount=ISNULL(@Oim_Discount,0)                                                                    
		WHERE  DraftAutoId= @DraftAutoId AND ProductAutoId=@ProductAutoId AND UnitAutoId=@UnitAutoId                                                                    
		and IsExchange=@IsExchange and isFreeItem=@IsFreeItem     
		update DraftOrderMaster set Remarks=@Remarks where DraftAutoId=@DraftAutoId
		
		select PM.AutoId,PM.Stock,PD.Qty as UnitQty from ProductMaster as PM
		inner join PackingDetails as PD on PD.UnitType=@UnitAutoId and PD.ProductAutoId=@ProductAutoId
		where PM.AutoId=@ProductAutoId
	end
	else
	begin
		Set @isException=1                                          
		Set @exceptionMessage='Stock is not Available.' 
	end
    END                                                                    
    ELSE IF @Opcode=424                                                            
    BEGIN                                                                     
     SELECT ROW_NUMBER() OVER(ORDER BY [DraftAutoId] desc) AS RowNumber, * INTO #Results1 from                                                                  
     (                                                                       
     SELECT OM.[DraftAutoId],(EMP.FirstName+' '+EMP.LastName ) AS EmpName , CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,CONVERT(VARCHAR(20), OM.DeliveryDate, 101) AS DeliveryDate                                                                  
  
     ,CM.[CustomerName] AS CustomerName,                                                   
      'Draft'  AS Status,                                                                  
     (SELECT COUNT(distinct [ProductAutoId]) from [dbo].[DraftItemMaster] WHERE DraftAutoId=OM.DraftAutoId) AS NoOfItems                                                                     
     FROM [dbo].[DraftOrderMaster] As OM                                                                    
     LEFT JOIN [dbo].[CustomerMaster] AS CM ON CM.AutoId = OM.CustomerAutoId                                                                            
     INNER JOIN EmployeeMaster AS EMP ON EMP.AutoId=OM.EmpAutoId                                                                     
                                                                                 
     WHERE                                                                     
     (@FromDate is null or @FromDate = '' or @Todate is null or @Todate = '' or (Convert(date,[OrderDate]) between @FromDate and @Todate))                                                                        
     and                                                                    
     OM.EmpAutoId=@EmpAutoId                              
     and (@CustomerAutoId=0 or OM.CustomerAutoId=@CustomerAutoId)                                                                    
     )as t order by [DraftAutoId]                                                                    
                                                                    
     SELECT COUNT(*) as RecordCount,@PageSize as PageSize,@PageIndex as PageIndex FROM #Results1                                                                    
     SELECT * FROM #Results1                                        
     WHERE (@PageSize=0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))                                      
     SELECT EType.AutoId as EmpType from  EmployeeMaster AS EMP                    
  INNER JOIN EmployeeTypeMaster AS EType ON EType.AutoId=EMP.EmpType  where EMP.AutoId=@EmpAutoId                                                          
                         
       END                                                   
    ELSE IF @Opcode=303                                                                    
    BEGIN                                                                    
     DELETE FROM DraftItemMaster WHERE DraftAutoId=@DraftAutoId                                                                    
     DELETE FROM DraftOrderMaster WHERE DraftAutoId=@DraftAutoId                                                                    
    END                                                                    
    ELSE IF @Opcode=425                                                                    
    BEGIN                                                                    
         SELECT ShippingType,CustomerAutoId,convert(varchar,OrderDate,101) as OrderDate,'Draft' as Status,convert(varchar,DeliveryDate,101) as DeliveryDate,
		 Remarks,OverallDisc,OverallDiscAmt,ShippingCharges
		 FROM DraftOrderMaster WHERE DraftAutoId=@DraftAutoId  

		 Declare @CustomerType INT
		 SELECT @CustomerType=(SELECT CustomerType FROM CustomerMaster WHERE AutoID=(SELECT CustomerAutoId from DraftOrderMaster WHERE DraftAutoId=@DraftAutoId))

         SELECT ItemAutoId,DraftAutoId,temp1.ProductAutoId,UnitAutoId,ReqQty,QtyPerUnit, 
		 cast((case when @CustomerType=3 and UnitPrice< PD.CostPrice then PD.CostPrice else UnitPrice end) as decimal(10,2)) as UnitPrice ,
		 cast((case 
			when @CustomerType=3 then PD.CostPrice 
			when @CustomerType=2 then PD.WHminPrice 
			else PD.MinPrice end) as decimal(10,2)) as minprice,pd.CostPrice,pd.Price as BasePrice,
		 P_SRP as SRP,GP,temp1.TaxRate,IsExchange,isFreeItem,(case when PM.IsApply_ML=1 then (ISNULL(PM.MLQty,0)) else 0 end) as UnitMLQty,
		 (case when PM.IsApply_ML=1 then (ISNULL(PM.MLQty,0)) else 0 end)*ReqQty*QtyPerUnit as TotalMLQty,temp1.Barcode,
		 IsTaxable,NetPrice,PM.ProductId,PM.ProductName,um.UnitType,(case when IsApply_Oz=1 then ISNULL(PM.WeightOz,0) else 0 end) as WeightOz,
		 ISNULL(CostPrice,0) as CostPrice ,ISNULL(Price,0) as BasePrice,isnull(temp1.Oim_Discount,0.00) as Oim_Discount,isnull(temp1.Del_ItemTotal,0.00) as Del_ItemTotal
		 FROM DraftItemMaster as temp1                                                                   
         INNER JOIN ProductMaster AS PM ON PM.AutoId=temp1.ProductAutoId                                                                    
         INNER JOIN UnitMaster AS UM ON UM.AutoId=temp1.UnitAutoId 
		 INNER JOIN PackingDetails AS PD ON UM.AutoId=PD.UnitType AND PD.ProductAutoId=temp1.ProductAutoId
		 WHERE DraftAutoId=@DraftAutoId                                                                    
    END      
	 ELSE IF @Opcode=501                                                                    
    BEGIN                                                                    
   Delete from DraftItemMaster WHERE DraftAutoId=@DraftAutoId and ProductAutoId=@ProductAutoId and UnitAutoId=@UnitAutoId and isFreeItem=@IsFreeItem and IsExchange=@IsExchange                                                                   
    END      
 ELSE IF @Opcode=426                                                                    
     BEGIN                                                                     
      SELECT ROW_NUMBER() OVER(ORDER BY [OrderNo] desc) AS RowNumber, * INTO #ResultsShip from                                                                    
      (                                                        
      SELECT OM.[AutoId],OM.OrderNo,CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,CM.[CustomerName] AS CustomerName,                                                                    
      SM.StatusType AS Status,OM.[Status] As StatusCode,OM.[GrandTotal],OM.ShippingType,OM.[SalesPersonAutoId],                                                                    
      (SELECT COUNT(distinct [ProductAutoId]) from [dbo].[OrderItemMaster] WHERE OrderAutoId=OM.AutoId) AS NoOfItems                                                                     
      FROM [dbo].[OrderMaster] As OM                                                                    
      INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.AutoId = OM.CustomerAutoId                                                                     
      INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.[Status] AND SM.Category = 'OrderMaster'                                                                               
      WHERE (@OrderNo is null or @OrderNo ='' or OrderNo like '%' + @OrderNo + '%')                                                                    
      and (@FromDate is null or @FromDate = '' or @Todate is null or @Todate = '' or (Convert(date,[OrderDate]) between @FromDate and @Todate))                                                    
      and (@OrderStatus is null or @OrderStatus=0 or OM.[Status]=@OrderStatus)                                                                     
      and (@SalesPersonAutoId = 0 or OM.[SalesPersonAutoId]=@SalesPersonAutoId)                                                                    
       and OM.ShippingType=@ShippingType                                                                    
      )as t order by [OrderNo]                                           
                                                                    
      SELECT COUNT(*) as RecordCount,@PageSize as PageSize,@PageIndex as PageIndex FROM #ResultsShip                                                                    
      SELECT * FROM #ResultsShip                                                           
      WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1                                                                    
     END                                                                    
   ELSE IF @Opcode=430                                                                    
   BEGIN    
	   IF EXISTS(SELECT [AutoId] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId AND ([PaymentRecev] IS NOT NULL OR [PaymentRecev] != ''))                      
	   BEGIN  	
			SELECT ROW_NUMBER() OVER(ORDER BY [AutoId] desc) AS RowNumber, * INTO #Results30 from                                                                    
			(                                                                       
			SELECT OM.[AutoId],OM.OrderNo,CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,CM.[CustomerName] AS CustomerName,                                                                    
			SM.StatusType AS Status,OM.[Status] As StatusCode,OM.[PayableAmount] as GrandTotal,                                                                    
			(SELECT COUNT(distinct [ProductAutoId]) from [dbo].Delivered_Order_Items WHERE OrderAutoId=OM.AutoId) AS NoOfItems  ,                                    
			st.ShippingType                                                                   
			FROM [dbo].[OrderMaster] As OM                                                         
			INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.AutoId = OM.CustomerAutoId                                      
			INNER JOIN [dbo].ShippingType AS st ON st.AutoId = OM.ShippingType                                          
			INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.[Status] AND SM.Category = 'OrderMaster'                                                      
			WHERE                                                         
			OM.OrderType=1                                                                    
			and (@OrderNo is null or @OrderNo ='' or OrderNo like '%' + @OrderNo + '%')                                                                    
			and (@CustomerAutoId is null or @CustomerAutoId =0 or CustomerAutoId =@CustomerAutoId)                                                                    
			and (@FromDate is null or @FromDate = '' or @Todate is null or @Todate = '' or (Convert(date,[OrderDate]) between @FromDate and @Todate))                                                                           
			and OM.POSAutoId=@SalesPersonAutoId                                                                    
			and (@OrderStatus is null or @OrderStatus=0 or OM.Status=@OrderStatus)                                         
			and (@ShippingType is null or @ShippingType=0 or OM.ShippingType=@ShippingType)                                                                  
                                 
			)as t order by [AutoId] desc                                                   
                                                                    
			SELECT CASE WHEN ISNULL(@PageSize,0)=0 then 0 else COUNT(*)  end as RecordCount,@PageSize as PageSize,@PageIndex as PageIndex FROM #Results30                                                                    
			SELECT * FROM #Results30                                                                    
			WHERE ISNULL(@PageSize,0)=0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1) 
	   END
	   ELSE
	   BEGIN
			SELECT ROW_NUMBER() OVER(ORDER BY [AutoId] desc) AS RowNumber, * INTO #Results31 from                                                                    
			(                                                                       
			SELECT OM.[AutoId],OM.OrderNo,CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,CM.[CustomerName] AS CustomerName,                                                                    
			SM.StatusType AS Status,OM.[Status] As StatusCode,OM.[PayableAmount] as GrandTotal,                                                                   
			(SELECT COUNT(distinct [ProductAutoId]) from [dbo].[OrderItemMaster] WHERE OrderAutoId=OM.AutoId) AS NoOfItems  ,                                    
			st.ShippingType                                                                   
			FROM [dbo].[OrderMaster] As OM                                                         
			INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.AutoId = OM.CustomerAutoId                                      
			INNER JOIN [dbo].ShippingType AS st ON st.AutoId = OM.ShippingType                                          
			INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.[Status] AND SM.Category = 'OrderMaster'                                                      
			WHERE                                                         
			OM.OrderType=1                                                                    
			and (@OrderNo is null or @OrderNo ='' or OrderNo like '%' + @OrderNo + '%')                                                                    
			and (@CustomerAutoId is null or @CustomerAutoId =0 or CustomerAutoId =@CustomerAutoId)                                                                    
			and (@FromDate is null or @FromDate = '' or @Todate is null or @Todate = '' or (Convert(date,[OrderDate]) between @FromDate and @Todate))                                                                           
			and OM.POSAutoId=@SalesPersonAutoId                                                                    
			and (@OrderStatus is null or @OrderStatus=0 or OM.Status=@OrderStatus)                                         
			and (@ShippingType is null or @ShippingType=0 or OM.ShippingType=@ShippingType)                                                                  
                                 
			)as t order by [AutoId] desc                                                   
                                                                    
			SELECT CASE WHEN ISNULL(@PageSize,0)=0 then 0 else COUNT(*)  end as RecordCount,@PageSize as PageSize,@PageIndex as PageIndex FROM #Results31                                                                    
			SELECT * FROM #Results31                                                                    
			WHERE ISNULL(@PageSize,0)=0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)                          
	  END
  END 
  ELSE IF @Opcode=455
  begin
		Select Stock from ProductMaster where AutoId=@ProductAutoId
  End
  ELSE IF @Opcode=44                                                                    
	BEGIN   
		SELECT [AutoId],[StatusType] as ST,
		(
			SELECT cm.[AutoId] as  A,[CustomerId] + ' - ' + [CustomerName] +' ['+ct.CustomerType+']' As C FROM [dbo].[CustomerMaster] as cm                                            
			inner join CustomerType as ct on cm.CustomerType=ct.AutoId   where status=1 
			order by C
			for json path
		) as CustomerList,
		(
			SELECT [AutoId],convert(varchar(10),[ProductId]) + '--' + [ProductName] as [PN],(case when isnull(IsApply_ML,0)=0 then 0 else MLQty end) as ML,
			(case when isnull(IsApply_Oz,0)=0 then 0 else WeightOz end) as Oz,Stock as ST                                                                   
			FROM [dbo].[ProductMaster] as pm where ProductStatus=1                                      
			and (select count(1) from PackingDetails where   packingstatus=1 and ProductAutoId=pm.AutoId)>0                              
			and ISNULL(PackingAutoId,0)!=0  order by       [PN]             
			for json path
		) as ProductList,
		(
			SELECT AutoId, ShippingType as ST,EnabledTax from shippingType where Shippingstatus=1
			order by ST
			for json path
		) as ShippingType,
		(
			SELECT AutoId,PaymentMode as PM from PAYMENTModeMaster where AutoID not in (5)
			order by PM
			for json path
		) as PaymentList 
		FROM [dbo].[StatusMaster] WHERE [AutoId]=1 and [Category]='OrderMaster'
		for json path
	END 
	ELSE IF @Opcode=45
	BEGIN
	    IF EXISTS(SELECT [SecurityValue] FROM [dbo].[TBL_MST_SecurityMaster] WHERE [SecurityValue]=@CheckSecurity and SecurityType=8 and typeEvent=2)                                
        BEGIN                                                                                     
			SET @isException=0
			SET @exceptionMessage='success'
        END
		ELSE
		BEGIN
		    SET @isException=1
			SET @exceptionMessage='Invalid'
		END
	END
	ELSE IF @Opcode=46
	BEGIN
	    SET @SalesPersonAutoId=(Select SalesPersonAutoId FROM CustomerMaster where AutoId=@CustomerAutoId)
	    IF EXISTS(SELECT * FROM AllowQtyPiece Where SalesPersonAutoId=@SalesPersonAutoId AND ProductAutoId=@ProductAutoId)                                
        BEGIN                                                                                     
			Update AllowQtyPiece SET AllowQtyInPieces=AllowQtyInPieces+@ReqQty where SalesPersonAutoId=@SalesPersonAutoId AND ProductAutoId=@ProductAutoId
        END
		ELSE
		BEGIN
		    SET @isException=1
			SET @exceptionMessage='NotExist'
		END
	END
	ELSE IF @Opcode=47
	BEGIN
	SET @AllocationStatus=1
	IF Not EXISTS(SELECT * FROM [dbo].[ItemBarcode] WHERE [Barcode]=@Barcode) 
	BEGIN
		SET @isException=1
		SET @exceptionMessage='Barcode'
	END
	ELSE IF EXISTS(SELECT * FROM [dbo].[ItemBarcode] as ib
	inner join ProductMaster as pm on pm.AutoId=ib.ProductAutoId and pm.ProductStatus=1 
	WHERE [Barcode]=@Barcode)                                                                                                                                                    
	BEGIN                                                                                                                                      
		SET @ProductAutoId=(SELECT [ProductAutoId] FROM [dbo].[ItemBarcode] WHERE [Barcode]=@Barcode)                                                                          
		SET @SalesPersonAutoId=(Select SalesPersonAutoId FROM CustomerMaster Where AutoId=@CustomerAutoId)
		SET @UnitAutoId=(SELECT UnitAutoId FROM [dbo].[ItemBarcode] WHERE [Barcode]=@Barcode)         
		declare @TotalReqQty int=null
		set @TotalReqQty=(select Qty from PackingDetails where ProductAutoId=@ProductAutoId 
		and UnitType=@UnitAutoId)*@ReqQty

		IF EXISTS(SELECT * FROM DraftItemMaster WHERE DraftAutoId= @DraftAutoId AND ProductAutoId=@ProductAutoId AND                                                                                                      
			UnitAutoId=@UnitAutoId AND IsExchange=@IsExchange AND IsFreeItem=@IsFreeItem)                                                                 
		BEGIN  
			SET @availQty=ISNULL((SELECT ISNULL(ReqQty,0) FROM DraftItemMaster 
			WHERE DraftAutoId= @DraftAutoId AND ProductAutoId=@ProductAutoId AND                                                                                                      
			UnitAutoId=@UnitAutoId AND IsExchange=@IsExchange AND IsFreeItem=@IsFreeItem),0)*
			(select Qty from PackingDetails where ProductAutoId=@ProductAutoId and UnitType=@UnitAutoId)
		END
		ELSE IF EXISTS(SELECT * FROM DraftItemMaster WHERE DraftAutoId= @DraftAutoId AND ProductAutoId=@ProductAutoId AND                                                                                                      
		UnitAutoId!=@UnitAutoId AND IsExchange=@IsExchange AND IsFreeItem=@IsFreeItem)
		BEGIN
		     SET @isException=1
			 SET @exceptionMessage='DifferentUnit'

		END
		IF @exceptionMessage!='DifferentUnit'
		BEGIN
			IF EXISTS(SELECT * FROM AllowQtyPiece WHERE SalesPersonAutoId=@SalesPersonAutoId 
			and ProductAutoId=@ProductAutoId)  
			BEGIN  
				IF EXISTS(select * from AllowQtyPiece where CAST(AllowDate AS date) <= CAST(getdate() AS DATE) 
				AND SalesPersonAutoId=@SalesPersonAutoId AND ProductAutoId=@ProductAutoId)  
				BEGIN  
					SELECT @AllowQtyInPieces=(AllowQtyInPieces-UsedQty) FROM AllowQtyPiece 
					WHERE SalesPersonAutoId=@SalesPersonAutoId and ProductAutoId=@ProductAutoId  
					IF (@AllowQtyInPieces>=@TotalReqQty+ISNULL(@availQty,0))  
					BEGIN  
						SET @AllocationStatus=1 
					END
					ELSE
					BEGIN
							SET @AllocationStatus=0
							SET @AllocatQty=@TotalReqQty+ISNULL(@availQty,0)
							Select @AllowQtyInPieces as AvailableQty,@AllocatQty-@AllowQtyInPieces  as RequiredQty,
							@ProductAutoId as ProductAutoId,'Less' as Message,@IsTaxable,@DraftAutoId AS DraftAutoId	
					END
				END 
			END
			IF @AllocationStatus=1
			BEGIN
			if ((select Stock from ProductMaster where AutoId=@ProductAutoId) >=@TotalReqQty)
			begin
				set @custType1 =(SELECT CustomerType FROM CustomerMaster WHERE AutoId=@CustomerAutoId)                                                                     
				SELECT PM.AutoId as ProductAutoId,PM.ProductId,PM.ProductName,PD.Qty as UnitQty,PD.UnitType, Stock as ProductStock, 
				(case when isnull(IsApply_ML,0)=0 then 0 else MLQty end) as ML,
				(case when isnull(IsApply_Oz,0)=0 then 0 else WeightOz end) as Oz,
				(CASE                                                                     
				WHEN @custType1=2 THEN ISNULL(WHminPrice,0)                                                                                                                                                     
				WHEN @custType1=3  THEN ISNULL(CostPrice,0)                                           
				ELSE [MinPrice] END) AS [MinPrice],                                                                                                                                                    
				ISNULL((SELECT TOP 1 UnitType +  ' ('+ CONVERT(VARCHAR(20),PD.QTY)+' pcs)' 
				from UnitMaster AS UM WHERE UM.AutoId=PD.UnitType),'') AS UnitName,[CostPrice],                                                                                                                                                    
				(case                                                                                                   
				when @IsExchange=1 or @IsFreeItem=1 then 0.00                                                                                                                                            
				WHEN  @custType1=3 then CONVERT(DECIMAL(10,2),ISNULL(CostPrice,0))                                                                                                                                                
				else CONVERT(DECIMAL(10,2),[Price]) end) as  [Price],                                                     
				CONVERT(DECIMAL(10,2),((PM.[P_SRP]-([Price]/(CASE WHEN [Qty]=0 then 1 else [Qty] end )))/(
				CASE WHEN ISNULL(PM.[P_SRP],0)=0 then 1 else PM.[P_SRP] END)) * 100) AS GP, 
				(case when @custType1=3 then PD.CostPrice else 
				isnull((select Top 1 [CustomPrice] 
				from [dbo].[ProductPricingInPriceLevel] AS PPL where                                    
				PPL.[ProductAutoId] = PD.[ProductAutoId] AND PPL.[UnitAutoId] = PD.[UnitType]                                                                                                                                                    
				AND [PriceLevelAutoId] = (SELECT [PriceLevelAutoId] FROM [dbo].[CustomerPriceLevel]                                                       
				WHERE [CustomerAutoId]=@CustomerAutoId)                                     
				Order by [CustomPrice] desc),null) end) as [CustomPrice],PM.[Stock]/(CASE WHEN Qty=0 THEN 1 ELSE QTY END) 
				as Stock,PM.[TaxRate],                                                                                                 
				PM.[P_SRP] AS [SRP],                                                                                                                    
				(case when @IsExchange=1 or @IsFreeItem=1 then 0.00 else (CASE WHEN ISNULL(PM.IsApply_ML,0)=1 then isnull(PM.MLQty,0) else 0 end) end ) as MLQty,
				(case when @IsExchange=1 or @IsFreeItem=1 then 0.00 else (CASE WHEN ISNULL(PM.IsApply_oz,0)=1 then isnull(PM.WeightOz,0) else 0 end) end) as WeightOz,                                              
					PD.Price as BasePrice,isnull(@Oim_Discount,0) as Oim_Discount,0.00 as Del_ItemTotal
				into #result4231 FROM [dbo].[PackingDetails] As PD                                                                                                                                                     
				INNER JOIN [dbo].[ProductMaster] AS PM ON PD.[ProductAutoId]=PM.[AutoId]                                                                                                                                                    
				WHERE PD.[ProductAutoId] = @ProductAutoId and PD.[UnitType] = @UnitAutoId                                                                                           
                                                                                                                                                          
				SET @QtyPerUnit=(SELECT TOP 1 UnitQty FROM #result4231)                                                                                                 
				SET @MLQty=(SELECT TOP 1 MLQty FROM #result4231)                                                                                                                                                    
				SET @UnitPrice=(SELECT TOP 1 Price FROM #result4231)                        
				SET @minprice=(SELECT TOP 1 MinPrice FROM #result4231)                                                                                                                 
				SET @SRP=(SELECT TOP 1 SRP FROM #result4231)                                                                                                                                  
				SET @GP=(SELECT TOP 1 GP FROM #result4231)                                                                                 
				set @Customrice=((SELECT TOP 1 [CustomPrice] FROM #result4231))                                      
				set @UnitPrice = (                                                                                                                           
				case                                                                                                                                                  
				when @IsExchange=1 or @IsFreeItem=1 then @UnitPrice                                                              
				when @Customrice IS NULL then @UnitPrice                                                                                                   
				else ( case when @Customrice < @minprice then  @UnitPrice else  @Customrice end)                                                                                                                                                  
				end                                                                         
				)  
				IF ISNULL(@DraftAutoId,0)=0                                                                                                                                  
				BEGIN                                                                                                                                                    
					INSERT INTO DraftOrderMaster([EmpAutoId],[CustomerAutoId],[OrderDate],[TotalItem],[ShippingType],[DeliveryDate],OrderType,Remarks)                                                                          
					VALUES(@EmpAutoId,@CustomerAutoId,GETDATE(),1,@ShippingType,@DeliveryDate,2,@Remarks)                                                                             
					SET @DraftAutoId=SCOPE_IDENTITY()                                                                             
					INSERT INTO DraftItemMaster(DraftAutoId,ProductAutoId,UnitAutoId,ReqQty,QtyPerUnit,UnitPrice,minprice,SRP,GP,TaxRate,IsExchange,
					IsFreeItem,IsTaxable,UnitMLQty,TotalMLQty,Barcode,Oim_Discount)                                                                          
					VALUES(@DraftAutoId,@ProductAutoId,@UnitAutoId,@ReqQty,@QtyPerUnit,@UnitPrice,@minprice,@SRP,@GP,                                                              
					@IsTaxable,@IsExchange,@IsFreeItem,@IsTaxable,@MLQty,(@MLQty*@ReqQty),@Barcode,@Oim_Discount)                                                                                                              
				END                                                                                                                                                     
				ELSE                                                                                                                                                 
				BEGIN                                                                                                        
					IF NOT EXISTS(SELECT * FROM DraftItemMaster WHERE DraftAutoId= @DraftAutoId AND ProductAutoId=@ProductAutoId AND                                                                                                      
					UnitAutoId=@UnitAutoId AND IsExchange=@IsExchange AND IsFreeItem=@IsFreeItem)                                                                 
					BEGIN 
						INSERT INTO DraftItemMaster(DraftAutoId,ProductAutoId,UnitAutoId,ReqQty,QtyPerUnit,UnitPrice,minprice,SRP,GP,TaxRate,IsExchange,IsFreeItem,IsTaxable,UnitMLQty,TotalMLQty,Barcode,Oim_Discount)                                                        
						VALUES(@DraftAutoId,@ProductAutoId,@UnitAutoId,@ReqQty,@QtyPerUnit,@UnitPrice,@minprice,@SRP,@GP,@IsTaxable,@IsExchange,@IsFreeItem,@IsTaxable,@MLQty,(@MLQty*@ReqQty),@Barcode,@Oim_Discount)                                                                   
						UPDATE DraftOrderMaster SET [TotalItem]=[TotalItem]+1,Remarks=@Remarks WHERE DraftAutoId=@DraftAutoId                                                                                                                      
					END                                                                                                               
					else                                                                                                                                
					BEGIN                                                                                
						UPDATE DraftItemMaster SET ReqQty=ReqQty+@ReqQty,TotalMLQty=isnull(UnitMLQty,0)* (ReqQty+@ReqQty),Oim_Discount=@Oim_Discount WHERE  DraftAutoId= @DraftAutoId AND                                            
						ProductAutoId=@ProductAutoId AND UnitAutoId=@UnitAutoId AND IsExchange=@IsExchange AND IsFreeItem=@IsFreeItem                                                                         
					END                                                                                                                             
				END
				SELECT TOP 1  @DraftAutoId AS DraftAutoId,* FROM #result4231   
				INSERT INTO ExtraItemLogMaster (Barcode,DraftAutoId,EmpAutoId,LogDate) VALUES (@Barcode,@DraftAutoId,@EmpAutoId,GETDATE())
			END    
			else
			begin
				Set @isException=1                                          
				Set @exceptionMessage='Stock is not Available'   
			end	
			END
		END
	ELSE
	BEGIN	
		SET @AllocatQty=@TotalReqQty+ISNULL(@availQty,0)
	    Select @AllowQtyInPieces as AvailableQty,@AllocatQty-@AllowQtyInPieces  as RequiredQty,
		@ProductAutoId as ProductAutoId,'Less' as Message,@IsTaxable,@DraftAutoId AS DraftAutoId				
	END
	End	
	ELSE
	BEGIN
		Set @isException=1                                          
		Set @exceptionMessage='Product Inactive'
	END
END
  END TRY                                                                    
  BEGIN CATCH                               
    Set @isException=1                                                                    
    Set @exceptionMessage='Oops! Something went wrong.Please try later.'            
  END CATCH                                           
END 
GO
