USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcSalesBySalePerson]    Script Date: 04/09/2020 01:59:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
alter PROCEDURE [dbo].[ProcSalesByCustomerSalesPerson]                                                                    
@Opcode INT=NULL,                                                                                                                                                                                                                                   
@SalesPersonAutoId int =null,                                                                                                                            
@SalesPerson  VARCHAR(500)=NULL,                                                         
@FromDate date =NULL,                                                                    
@ToDate date =NULL,                             
@PageIndex INT=1,                                                                    
@PageSize INT=10,                                                                    
@RecordCount INT=null,                                                                    
@isException bit out,                                                                    
@exceptionMessage varchar(max) out                                                                    
AS                                                                    
BEGIN                                                                     
 BEGIN TRY                                                                    
  Set @isException=0                                                                    
  Set @exceptionMessage='Success'                                                                    
 IF @Opcode=41                                                                    
  BEGIN                                                       
   SELECT AutoId SId,FirstName+' '+ISNULL(LASTNAME,'') AS SP  FROM EmployeeMaster WHERE EmpType IN (2) and Status=1 order by SP ASC 
   for json path
  END                                                                    
  ELSE IF @Opcode=42                                                             
  BEGIN                                         
  select ROW_NUMBER() over(order by FirstName+' '+ISNULL(LASTNAME,'')) as RowNumber,                               
  T.*, FirstName+' '+ISNULL(LASTNAME,'') AS EmployeeName into #Result55 FROM                                                                    
   ( SELECT cm.SalesPersonAutoId,COUNT(OM.Autoid) as NoOfOrders                     
   ,SUM(do.PayableAmount) AS GrandTotal FROM OrderMaster AS OM inner join                                                                    
   DeliveredOrders as do on om.AutoId=do.OrderAutoId                                                                    
   inner join CustomerMaster as cm on cm.AutoId=om.CustomerAutoId                                                                    
   WHERE                           
   om.Status=11 and                          
   (ISNULL(@SalesPerson,'0,')='0,' or ISNULL(@SalesPerson,'0')='0' OR cm.SalesPersonAutoId                                                                    
   in (select * from dbo.fnSplitString(@SalesPerson,','))                                                          
   )                                                                    
   and (@FromDate is null or @ToDate is null or (CONVERT(date,OrderDate)                                                                     
   between convert(date,@FromDate) and CONVERT(date,@ToDate)))                                                                    
   GROUP BY cm.SalesPersonAutoId                                                                    
   ) AS T                                                                    
   INNER JOIN EmployeeMaster AS EMP ON EMP.AutoId=T.SalesPersonAutoId                                              
                                           
   SELECT * FROM #Result55                                        
  WHERE @PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)                                         
 SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize=0 then COUNT(RowNumber) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Result55                        
                       
  SELECT ISNULL(SUM(NoOfOrders),0) as NoOfOrders,ISNULL(SUM(GrandTotal),0.00) as GrandTotal FROM #Result55                                                             
  END    
                     
 END TRY                                                    
 BEGIN CATCH                                                                    
  Set @isException=1                                                                    
  Set @exceptionMessage=ERROR_MESSAGE()                                                                    
 END CATCH                                                                    
END 
