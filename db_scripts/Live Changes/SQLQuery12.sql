USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Order_TotalPieces]    Script Date: 02/14/2021 05:35:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TABLE OrderItemMaster drop column [TotalPieces] 
GO
ALTER FUNCTION  [dbo].[FN_Order_TotalPieces]
(
	 @AutoId int,
	 @orderAutoId int
)
RETURNS bigint
AS
BEGIN
	DECLARE @TotalPieces bigint	
	--CAL TOTAL AMOUNT FOR NEW ,PROCESSED AND CANCELLED ORDER 
	IF EXISTS(SELECT AutoId FROM OrderMaster WHERE AutoId=@orderAutoId AND Status IN (0,1,2,8))
	BEGIN
		SET @TotalPieces=(SELECT (QtyPerUnit*RequiredQty) FROM OrderItemMaster WHERE AutoId=@AutoId)	
	END
	ELSE 
	BEGIN	 
		SET @TotalPieces=(SELECT (QtyPerUnit*	QtyShip) FROM OrderItemMaster WHERE AutoId=@AutoId)	
	END

	RETURN @TotalPieces
END


  
  go
ALTER TABLE OrderItemMaster add [TotalPieces]  AS ([dbo].[FN_Order_TotalPieces]([autoid],[OrderAutoid]))