USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_StoreCredit]    Script Date: 02/14/2021 05:44:26 ******/

ALTER TABLE OrderMaster drop column [CreditAmount] 
GO
ALTER FUNCTION  [dbo].[FN_StoreCredit]
(
	 @OrderAutoId int 
)
RETURNS decimal(18,2)
AS
BEGIN
	DECLARE @StoreCredit decimal(18,2)		 	 
	SET @StoreCredit=cast((SELECT SUM(ISNULL(Amount,0.00)) FROM tbl_Custumor_StoreCreditLog WHERE ReferenceType='OrderMaster' and
	ReferenceNo=CONVERT(varchar(10), @OrderAutoId)) as decimal	(18,2))	 
	RETURN @StoreCredit
END


  go
ALTER TABLE OrderMaster add [CreditAmount]  AS ([dbo].[FN_StoreCredit]([Autoid]))