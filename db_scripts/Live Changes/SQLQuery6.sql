USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Order_Weigth_OZTaxAmount]    Script Date: 02/14/2021 05:28:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TABLE OrderMaster DROP COLUMN [Weigth_OZTaxAmount]
go
ALTER FUNCTION  [dbo].[FN_Order_Weigth_OZTaxAmount]
(
	 @AutoId int
)
RETURNS decimal(18,2)
AS
BEGIN
	DECLARE @Weight_Oz decimal(18,2)	
	SET @Weight_Oz=isnull((select  CASE WHEN ShippingTaxEnabled=0 AND CONVERT(DATE,OrderDate)>CONVERT(DATE,'09/15/2020') THEN 0 ELSE ISNULL(Weigth_OZQty,0)*ISNULL(Weigth_OZTax,0) END from OrderMaster where AutoId=@AutoId),0.00)
	RETURN @Weight_Oz
END
go
ALTER TABLE OrderMaster add [Weigth_OZTaxAmount]  AS ([dbo].[FN_Order_Weigth_OZTaxAmount]([autoid]))