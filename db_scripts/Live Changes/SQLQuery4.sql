USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Order_PayableAmount]    Script Date: 02/14/2021 05:26:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON





ALTER TABLE OrderMaster drop column [PayableAmount] 

GO
ALTER FUNCTION  [dbo].[FN_Order_PayableAmount]  
(  
  @orderAutoId int  
)  
RETURNS decimal(18,2)  
AS  
BEGIN  
 DECLARE @PayableAmount decimal(18,2)   
  SET @PayableAmount=(SELECT  ((GrandTotal-isnull([Deductionamount],(0)))-isnull([CreditAmount],(0)))FROM OrderMaster WHERE AutoId=@orderAutoId)   
 RETURN @PayableAmount  
END  
go


ALTER TABLE OrderMaster add [PayableAmount]  AS ([dbo].[FN_Order_PayableAmount]([autoid]))
