USE [psmnj.a1whm.com]
GO

ALTER TABLE OrderMaster drop column [Weigth_OZQty] 
GO
ALTER FUNCTION  [dbo].[FN_Order_Weigth_OZQty]
(
@OrderAutoId int
)
RETURNS decimal(18,2)
AS
BEGIN
DECLARE @Weight_Oz decimal(18,2)	
	IF EXISTS(SELECT 1 FROM OrderMaster WHERE AutoId=@OrderAutoId AND Status=11)
	BEGIN
		SET @Weight_Oz=isnull((select SUM(ISNULL(Del_Weight_Oz_TotalQty ,0)) from [Delivered_Order_Items] where OrderAutoId=@OrderAutoId),0.00)
	END
	ELSE
	BEGIN
		SET @Weight_Oz=isnull((select SUM(ISNULL(Weight_Oz_TotalQty ,0)) from orderItemMaster where OrderAutoId=@OrderAutoId),0.00)
	END
RETURN @Weight_Oz
END

  go
ALTER TABLE OrderMaster add [Weigth_OZQty]  AS ([dbo].[FN_Order_Weigth_OZQty]([autoid]))