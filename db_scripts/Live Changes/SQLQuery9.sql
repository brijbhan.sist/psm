USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Order_MLTaxItem]    Script Date: 02/14/2021 05:31:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
ALTER TABLE OrderItemMaster drop column [Item_TotalMLTax] 

GO

ALTER FUNCTION  [dbo].[FN_Order_MLTaxItem]
(
	@AutoId int,
	@orderAutoId int
)
RETURNS decimal(18,2)
AS
BEGIN
	DECLARE @MLTax decimal(18,2)=0.00	
	declare @taxPer decimal(18,2)=isnull((select MLTaxPer from OrderMaster where AutoId=@orderAutoId),0.00)	 
	SET @MLTax=(SELECT TotalMLQty*@taxPer FROM OrderItemMaster WHERE AutoId=@AutoId)	 
	RETURN @MLTax
END

go
ALTER TABLE OrderItemMaster add [Item_TotalMLTax]  AS ([dbo].[FN_Order_MLTaxItem]([AutoId],[OrderAutoId]))