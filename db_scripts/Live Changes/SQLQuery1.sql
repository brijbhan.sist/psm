USE [psmnj.a1whm.com]
GO
DROP PROCEDURE [dbo].[ProcOrderMasterWeb]
DROP TYPE [dbo].[DT_WebOrderItemList]
/****** Object:  UserDefinedTableType [dbo].[DT_WebOrderItemList]    Script Date: 02/14/2021 05:10:50 ******/
CREATE TYPE [dbo].[DT_WebOrderItemList] AS TABLE(
	[ProductAutoId] [int] NULL,
	[UnitAutoId] [int] NULL,
	[QtyPerUnit] [int] NULL,
	[RequiredQty] [int] NULL,
	[Barcode] [varchar](50) NULL,
	[QtyShip] [int] NULL,
	[TotalPieces] [int] NULL,
	[UnitPrice] [decimal](18,2) NULL,
	[SRP] [decimal](18,2) NULL,
	[GP] [decimal](18,2) NULL,
	[Tax] [int] NULL,
	[IsExchange] [int] NULL,
	[IsFreeItem] [int] NULL,
	[NetPrice] [decimal](18,2) NULL,
	[OM_MinPrice] [decimal](18,2) NULL,
	[OM_CostPrice] [decimal](18,2) NULL,
	[ItemType] [varchar](50) NULL,
	[AutoId] [int] NULL
)
GO


