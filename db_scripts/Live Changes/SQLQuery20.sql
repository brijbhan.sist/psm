USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Order_TaxTotal]    Script Date: 02/14/2021 05:45:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
ALTER TABLE OrderMAster drop column [TotalTax]
GO
ALTER FUNCTION  [dbo].[FN_Order_TaxTotal]
(
	@orderAutoId int
)
RETURNS decimal(18,2)
AS
BEGIN
	DECLARE @TaxTotal decimal(18,2)	=0.00
	DECLARE @TaxValue DECIMAL(18,2)=(SELECT TaxValue FROM OrderMaster WHERE AutoId=@orderAutoId)
	IF EXISTS(SELECT AutoId FROM OrderMaster WHERE AutoId=@orderAutoId AND IsTaxApply=1)
	BEGIN
		if EXISTS(SELECT AutoId FROM OrderMaster WHERE AutoId=@orderAutoId AND Status NOT IN (11))
		BEGIN
			SET @TaxTotal=(select ISNULL(CAST(SUM(NetPrice*@TaxValue/100) AS decimal(10,2)),0) from OrderItemMaster where Tax=1
			AND OrderAutoId=@orderAutoId)
		END
		ELSE
		BEGIN
			SET @TaxTotal=(select ISNULL(CAST(SUM(NetPrice*@TaxValue/100) AS decimal(10,2)),0) from Delivered_Order_Items where Tax=1
			AND OrderAutoId=@orderAutoId)
		END
	 END
RETURN @TaxTotal
END
go
ALTER TABLE OrderMAster add [TotalTax]  AS ([dbo].[FN_Order_TaxTotal]([AutoId]))