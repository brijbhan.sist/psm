USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Order_TotalAmount]    Script Date: 02/14/2021 05:27:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON


ALTER TABLE OrderMaster DROP COLUMN [TotalAmount]
GO
ALTER FUNCTION  [dbo].[FN_Order_TotalAmount]
(
	 @orderAutoId int
)
RETURNS decimal(18,2)
AS
BEGIN
	DECLARE @TotalAmount decimal(18,2)	 
	IF EXISTS(SELECT AutoId FROM OrderMaster WHERE AutoId=@orderAutoId AND Status not IN (11))
	BEGIN
		SET @TotalAmount=ISNULL((SELECT SUM(NetPrice) FROM OrderItemMaster WHERE OrderAutoId=@orderAutoId),0)	
	END
	ELSE 
	BEGIN
		SET @TotalAmount=ISNULL((SELECT SUM(NetPrice) FROM Delivered_Order_Items WHERE OrderAutoId=@orderAutoId),0)	
	END

	RETURN @TotalAmount
END

go


ALTER TABLE OrderMaster add [TotalAmount]  AS ([dbo].[FN_Order_TotalAmount]([AutoId]))