USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Order_IsMLTotalQty]    Script Date: 02/14/2021 05:34:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TABLE OrderItemMaster drop column [TotalMLQty] 
go

ALTER FUNCTION  [dbo].[FN_Order_IsMLTotalQty]  
(  
	@AutoId int,  
	@orderAutoId int  
)  
RETURNS decimal(18,2)  
AS  
BEGIN  
DECLARE @MLTotalQty decimal(18,2)=0.00   
,@ShippingTaxEnabled int =ISNULL((select ShippingTaxEnabled from OrderMaster where AutoId=@orderAutoId),0)   
IF ([dbo].[FN_Order_IsMLtaxApply_shipping](@orderAutoId,@ShippingTaxEnabled)=1)  
BEGIN  
IF EXISTS(SELECT AutoId FROM OrderItemMaster WHERE AutoId=@AutoId AND IsExchange=0)  
BEGIN  
SET @MLTotalQty=(SELECT (TotalPieces*UnitMLQty) FROM OrderItemMaster WHERE AutoId=@AutoId)   
END  
END  
RETURN @MLTotalQty  
END  
  
  go
ALTER TABLE OrderItemMaster add [TotalMLQty]  AS ([dbo].[FN_Order_IsMLTotalQty]([AutoId],[OrderAutoId]))