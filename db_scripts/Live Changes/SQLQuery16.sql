USE [psmnj.a1whm.com]
GO
 
ALTER TABLE OrderMaster drop column [MLTax] 
GO
GO
ALTER FUNCTION  [dbo].[FN_Order_MLTax]
( 
	@orderAutoId int,
	@status int
)
RETURNS decimal(18,2)
AS
BEGIN
	DECLARE @MLTax decimal(18,2)=0.00	
	if(@status=11)
	begin	
	SET @MLTax=ISNULL((SELECT sum(Item_TotalMLTax) FROM Delivered_Order_Items WHERE OrderAutoId=@orderAutoId),0)	
	end
	else
	begin 	 
	SET @MLTax=ISNULL((SELECT sum(Item_TotalMLTax) FROM OrderItemMaster WHERE OrderAutoId=@orderAutoId),0)	
	end
	RETURN @MLTax
END

  go
ALTER TABLE OrderMaster add [MLTax]  AS ([dbo].[FN_Order_MLTax]([AutoId],[Status]))
