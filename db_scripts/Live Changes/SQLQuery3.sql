USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Order_GrandTotal]    Script Date: 02/14/2021 05:23:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER TABLE OrderMaster drop column [GrandTotal] 
go
ALTER FUNCTION  [dbo].[FN_Order_GrandTotal]  
(  
  @orderAutoId int  
)  
RETURNS decimal(18,2)  
AS  
BEGIN  
 DECLARE @GrandTotal decimal(18,2)   
  SET @GrandTotal=(SELECT (round(((([TotalAmount]-[OverallDiscAmt])+[ShippingCharges])+[TotalTax])+[MLTax]+
  ISNULL(Weigth_OZTaxAmount,0),(0))) FROM OrderMaster WHERE AutoId=@orderAutoId)   
 RETURN @GrandTotal  
END  
go

ALTER TABLE OrderMaster add [GrandTotal]  AS ([dbo].[FN_Order_GrandTotal]([autoid]))