USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Order_MLQty]    Script Date: 02/14/2021 05:30:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

ALTER TABLE OrderMaster DROP COLUMN [MLQty]
GO

ALTER FUNCTION  [dbo].[FN_Order_MLQty]
( 
	@orderAutoId int,
	@Status INT
)
RETURNS decimal(18,2)
AS
BEGIN
	DECLARE @MLQty decimal(18,2)=0.00	
	IF @Status	!=11
	BEGIN 
		SET @MLQty=(SELECT sum(TotalMLQty) FROM OrderItemMaster WHERE OrderAutoId=@orderAutoId)	
	END 
	ELSE
	BEGIN
	SET @MLQty=(SELECT sum(TotalMLQty) FROM Delivered_Order_Items WHERE OrderAutoId=@orderAutoId)	
	END
	RETURN @MLQty
END

go
ALTER TABLE OrderMaster add [MLQty]  AS ([dbo].[FN_Order_MLQty]([Autoid],[Status]))