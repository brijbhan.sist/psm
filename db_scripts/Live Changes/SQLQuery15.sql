USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Order_CostPrice]    Script Date: 02/14/2021 05:39:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
ALTER TABLE OrderMaster drop column [TotalCostPrice] 
GO
GO
ALTER FUNCTION  [dbo].[FN_Order_CostPrice]
( 
	@orderAutoId int
	
)
RETURNS decimal(18,2)
AS
BEGIN
	DECLARE @CostPrice decimal(18,2)=0.00	
	declare @Status INT=(select Status from OrderMaster where AutoId=@orderAutoId)
	
	IF @Status=11
	BEGIN 
		SET @CostPrice=(SELECT sum((Del_CostPrice/QtyPerUnit)*QtyDel) FROM Delivered_Order_Items WHERE OrderAutoId=@orderAutoId)
	END 
	ELSE IF @Status in (1,2,8)
	BEGIN
	    SET @CostPrice=(SELECT sum(OM_CostPrice*RequiredQty) FROM OrderItemMaster WHERE OrderAutoId=@orderAutoId)	
	END
	ELSE 
	BEGIN
	    SET @CostPrice=(SELECT sum(OM_CostPrice*ISNULL(QtyShip,0)) FROM OrderItemMaster WHERE OrderAutoId=@orderAutoId)	
	END
	RETURN @CostPrice
END


  
  go
ALTER TABLE OrderMaster add [TotalCostPrice]  AS ([dbo].[FN_Order_CostPrice]([autoid]))