USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Order_Weight_Oz_Qty]    Script Date: 02/14/2021 05:33:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TABLE OrderItemMaster drop column [Weight_Oz_TotalQty] 
go
ALTER FUNCTION  [dbo].[FN_Order_Weight_Oz_Qty]
(
	 @AutoId int
)
RETURNS decimal(18,2)
AS
BEGIN
	DECLARE @Weight_Oz decimal(18,2)	
	SET @Weight_Oz=isnull((select ISNULL(Weight_Oz,0)*ISNULL(TotalPieces,0) from orderItemMaster where AutoId=@AutoId),0.00)
	RETURN @Weight_Oz
END
go

ALTER TABLE OrderItemMaster add [Weight_Oz_TotalQty]  AS ([dbo].[FN_Order_Weight_Oz_Qty]([autoid]))