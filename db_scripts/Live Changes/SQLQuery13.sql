USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Order_Netmount]    Script Date: 02/14/2021 05:37:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
ALTER TABLE OrderItemMaster drop column [NetPrice] 
GO

ALTER FUNCTION  [dbo].[FN_Order_Netmount]
(
	 @AutoId int,
	 @orderAutoId int
)
RETURNS decimal(18,2)
AS
BEGIN
	DECLARE @NetAmount decimal(18,2)	
	--CAL TOTAL AMOUNT FOR NEW ,PROCESSED AND CANCELLED ORDER 
	IF EXISTS(SELECT AutoId FROM OrderMaster WHERE AutoId=@orderAutoId AND Status IN (0,1,2,8))
	BEGIN
		SET @NetAmount=ISNULL((SELECT (UnitPrice*	RequiredQty) FROM OrderItemMaster WHERE AutoId=@AutoId)	,0)
	END
	ELSE 
	BEGIN	 
		SET @NetAmount=ISNULL((SELECT (UnitPrice*	ISNULL(QtyShip,0)) FROM OrderItemMaster WHERE AutoId=@AutoId)	,0)
	END

	RETURN @NetAmount
END



  
  go
ALTER TABLE OrderItemMaster add [NetPrice]  AS ([dbo].[FN_Order_Netmount]([AutoId],[OrderAutoId]))