alter procedure ProcDropDownMaster                                     
@Opcode int=Null,  
@StatusCategory varchar(25)=null,
@isException bit out,                                     
@exceptionMessage varchar(max) out
AS
BEGIN
     SET @isException=0
	 SET @exceptionMessage='success'
	 IF @Opcode=401 --Vendor List
	 BEGIN
		Select * from 
		(
		Select (
		SELECT AutoId,[VendorName] FROM [dbo].[VendorMaster] WHERE [Status]=1 
		order by [VendorName]  ASC  for json path) as VendorList
		) as t
		for json path
	 END
	 ELSE IF @Opcode=402 --Product List
	 BEGIN	      
		Select * from 
		(  
		SELECT(
		SELECT [AutoId],Convert(varchar(10),[ProductId]) + '--' + [ProductName] as [ProductName] 
		FROM [dbo].[ProductMaster] where ProductStatus=1  order by CONVERT(VARCHAR(10),ProductId )+' '+ ProductName  ASC  for json path) as ProductList
		) as t
		for json path
	 END
	 ELSE IF @Opcode=403
	 BEGIN

	  Select * from 
		(  
		SELECT(
	      Select AutoId,StatusType from StatusMaster where Category=@StatusCategory and AutoId not in (1)  for json path) as StatustList
		) as t
		for json path     
	             
	 END
	 ELSE IF @Opcode=404 -- All Order Type
	 BEGIN
	      select AutoId, OrderType from OrderTypeMaster where Type='Order'  order by OrderType asc 
		for json path  
	 END
	  ELSE IF @Opcode=405 -- All Brand
	 BEGIN
	      select AutoId, BrandName from BrandMaster where Status=1 order by BrandName asc 
		for json path  
	 END
	 
END