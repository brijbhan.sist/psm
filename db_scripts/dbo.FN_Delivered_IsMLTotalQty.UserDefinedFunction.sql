USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Delivered_IsMLTotalQty]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 

CREATE FUNCTION  [dbo].[FN_Delivered_IsMLTotalQty]
(
	 @AutoId int,
	 @orderAutoId int
)
RETURNS decimal(10,2)
AS
BEGIN
	DECLARE @MLTotalQty decimal(10,2)=0.00	 	 
	IF ([dbo].[FN_Order_IsMLtaxApply](@orderAutoId)=1)
	BEGIN
		IF EXISTS(SELECT  AutoId FROM Delivered_Order_Items WHERE AutoId=@AutoId AND ISNULL(IsExchange,0)=0)
		BEGIN
				SET @MLTotalQty=(SELECT (QtyDel*UnitMLQty) FROM Delivered_Order_Items WHERE AutoId=@AutoId)	
		END
	END
	RETURN @MLTotalQty
END
 
GO
