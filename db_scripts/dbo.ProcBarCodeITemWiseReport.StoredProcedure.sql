
ALTER Procedure [dbo].[ProcBarCodeITemWiseReport]  
@Opcode int=null,  
@Barcode varchar(150)=null,  
@PageIndex INT=1,    
@PageSize INT=10,    
@RecordCount INT=null,    
@isException bit out,    
@exceptionMessage varchar(max) out    
as     
BEGIN     
 BEGIN TRY    
	  Set @isException=0    
	  Set @exceptionMessage='Success' 
	  IF @Opcode=21   
	  BEGIN             
	
		 select tmp.Barcode,
			psmnjum.UnitType as PSMNJ_Unit,pmsnjpm.productId as PSMNJ_productId,
			psmpaum.UnitType as PSMPA_Unit,pmspapm.productId as PSMPA_productId,
			psmctum.UnitType as PSMCT_Unit,pmsctpm.ProductId as PSMCT_productId,
			psmnpaum.UnitType as PSMnpa_Unit,pmsnpapm.ProductId as PSMnpa_productId,
			psmnpaum.UnitType as PSMWPA_Unit,pmsnpapm.ProductId as PSMWPA_productId
			into #temp    
			from (
				select Barcode from [psmnj.a1whm.com].[dbo].ItemBarcode where Barcode=@Barcode or @Barcode is null or @Barcode=''    
				union    
				select Barcode from [psmpa.a1whm.com].[dbo].ItemBarcode where Barcode=@Barcode or @Barcode is null or @Barcode=''    
				union     
				select Barcode from [psmct.a1whm.com].[dbo].ItemBarcode where Barcode=@Barcode or @Barcode is null or @Barcode=''    
				union     
				select Barcode from [psmnpa.a1whm.com].[dbo].ItemBarcode where Barcode=@Barcode or @Barcode is null or @Barcode=''
				union     
				select Barcode from [psmwpa.a1whm.com].[dbo].ItemBarcode where Barcode=@Barcode or @Barcode is null or @Barcode=''
			)  as tmp    
			left join [psmnj.a1whm.com].[dbo].ItemBarcode as psmnjtb on psmnjtb.barcode=tmp.barcode    
			left join [psmnj.a1whm.com].[dbo].UnitMaster as psmnjum on psmnjum.AutoId=psmnjtb.UnitAutoId    
			left join [psmnj.a1whm.com].[dbo].ProductMaster as pmsnjpm on pmsnjpm.AutoId=psmnjtb.ProductAutoId    
		
			left join [psmpa.a1whm.com].[dbo].ItemBarcode as psmpatb on psmpatb.barcode=tmp.barcode    
			left join [psmpa.a1whm.com].[dbo].UnitMaster as psmpaum on psmpaum.AutoId=psmpatb.UnitAutoId    
			left join [psmpa.a1whm.com].[dbo].ProductMaster as pmspapm on pmspapm.AutoId=psmpatb.ProductAutoId    
		
			left join [psmct.a1whm.com].[dbo].ItemBarcode as psmcttb on psmcttb.barcode=tmp.barcode    
			left join [psmct.a1whm.com].[dbo].UnitMaster as psmctum on psmctum.AutoId=psmcttb.UnitAutoId    
			left join [psmct.a1whm.com].[dbo].ProductMaster as pmsctpm on pmsctpm.AutoId=psmcttb.ProductAutoId    
		
			left join [psmnpa.a1whm.com].[dbo].ItemBarcode as psmnpatb on psmnpatb.barcode=tmp.barcode    
			left join [psmnpa.a1whm.com].[dbo].UnitMaster as psmnpaum on psmnpaum.AutoId=psmnpatb.UnitAutoId    
			left join [psmnpa.a1whm.com].[dbo].ProductMaster as pmsnpapm on pmsnpapm.AutoId=psmnpatb.ProductAutoId    

			left join [psmwpa.a1whm.com].[dbo].ItemBarcode as psmwpatb on psmwpatb.barcode=tmp.barcode    
			left join [psmwpa.a1whm.com].[dbo].UnitMaster as psmwpaum on psmwpaum.AutoId=psmwpatb.UnitAutoId    
			left join [psmwpa.a1whm.com].[dbo].ProductMaster as psmwpapm on psmwpapm.AutoId=psmwpatb.ProductAutoId
		
			select * from (
			SELECT ROW_NUMBER() OVER(ORDER BY Barcode) AS RowNumber, *   FROM   #temp     
				where PSMNJ_productId is null or PSMCT_productId is null or PSMPA_productId is null or PSMnpa_productId is null
				or PSMWPA_productId is null
			) as t
			WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1
   
			SELECT COUNT(Barcode) AS RecordCount, @PageSize AS PageSize, @PageIndex AS PageIndex FROM #temp    
			where PSMNJ_productId is null or PSMCT_productId is null or PSMPA_productId is null or PSMnpa_productId is null  
			or PSMWPA_productId is null
        
	   
		   

			SELECT (Select count(AutoId) as NoOfBarcode  from [psmnj.a1whm.com].[dbo].ItemBarcode ) as TotalBarcodeNJ,
			(Select count(AutoId) as NoOfBarcode  from [psmpa.a1whm.com].[dbo].ItemBarcode ) as TotalBarcodePA,
			(Select count(AutoId) as NoOfBarcode  from [psmnpa.a1whm.com].[dbo].ItemBarcode) as TotalBarcodeNPA,	
			(Select count(AutoId) as NoOfBarcode  from [psmct.a1whm.com].[dbo].ItemBarcode) as TotalBarcodeCT,	
			(Select count(AutoId) as NoOfBarcode  from [psmwpa.a1whm.com].[dbo].ItemBarcode) as TotalBarcodeWPA
		END 
		ELSE IF @OPcode=41
		BEGIN
				EXEC ProcBarcodeReport	    
		END
	END TRY    
	 BEGIN CATCH    
		  Set @isException=1    
		  Set @exceptionMessage=ERROR_MESSAGE()    
	 END CATCH    
	END 
GO
