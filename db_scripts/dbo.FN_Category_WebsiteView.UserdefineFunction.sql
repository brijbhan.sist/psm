USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Category_WebsiteView]    Script Date: 01/29/2021 03:15:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

ALTER TABLE [dbo].[CategoryMaster] DROP column [Category_ShowonWebsite] 
GO
ALTER FUNCTION  [dbo].[FN_Category_WebsiteView]
(
	 @AutoId int
)
RETURNS int
AS
BEGIN
	DECLARE @bool int=0
	
	IF EXISTS(select * from SubCategoryMaster as sm where sm.CategoryAutoId=@AutoId and sm.SubCategory_ShowonWebsite=1)
	BEGIN
	  
	   IF EXISTS(select AutoId from CategoryMaster where AutoId=@AutoId and Status=1 and IsShow=1)
	   BEGIN
			SET @bool=1
	   END
	END 
	RETURN @bool
END

GO

ALTER TABLE [dbo].[CategoryMaster] ADD [Category_ShowonWebsite]  AS ([dbo].[FN_Category_WebsiteView]([AutoId]))