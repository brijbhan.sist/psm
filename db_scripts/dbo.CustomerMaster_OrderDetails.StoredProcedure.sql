Alter Proc [dbo].[CustomerMaster_OrderDetails]
@OpCode int =null,
@CustomerAutoId int=null,
@OrderAutoId int=null,
@EmpAutoId int=null,
@PageIndex INT=1,                                                                                                  
@PageSize INT=10, 
@ReferenceNo varchar(max)=null,
@EmployeeRemarks varchar(500)=null,
@isException BIT OUT,                                
@exceptionMessage VARCHAR(max) OUT
As
Begin
BEGIN TRY                                                                               
	SET @isException=0                                                                                                  
	SET @exceptionMessage='Success!!'
	IF @Opcode = 41                                                     
		BEGIN                
			SELECT AutoId into #AutoId from OrderMaster where CustomerAutoId=@CustomerAutoId 			
			SELECT * INTO #RESULT from                        
			(                                         
			SELECT ROW_NUMBER() OVER(ORDER BY convert(datetime,OM.OrderDate) desc) AS RowNumber,OM.AutoId,OM.OrderNo,cONVERT(VARCHAR(10),OM.OrderDate,101) AS OrderDate,sm.StatusType as Status,                                                                                                  
			cONVERT(VARCHAR(10),OM.DeliveryDate,101) AS DeliveryDate,                                                                                                  
			ISNULL(OM.PayableAmount,0) AS PayableAmount, (Select ShippingType from ShippingType as t where t.AutoId=OM.ShippingType)  as  ShippingType,                    
			(select count(1) from OrderItemMaster as oim where oim.OrderAutoid=OM.AutoId) as OrderItems,                                                                                                  
			EMP.FirstName+' '+EMP.LastName AS SalesMan,EMP1.FirstName+' '+EMP1.LastName AS PackerNames,                                                                                                  
			EMP1.FirstName+' '+EMP1.LastName AS PackerName,EMP2.FirstName+' '+EMP2.LastName AS SalesManager,                                       
			EMP3.FirstName+' '+EMP3.LastName AS Account,                                                                                                  
			otm.OrderType AS OrderType,om.Status as StatusCode  FROM OrderMaster AS OM 
			INNER JOIN #AutoId AS AI ON AI.AutoId=OM.AutoId
			INNER JOIN StatusMaster AS SM ON SM.AutoId =OM.Status AND SM.Category='OrderMaster'                                                                                    
			LEFT JOIN  DeliveredOrders DO ON DO.OrderAutoId=OM.AutoId                                                                                       
			INNER JOIN EmployeeMaster AS EMP ON EMP.AutoId=OM.SalesPersonAutoId                                                                                                  
			LEFT JOIN EmployeeMaster AS EMP1 ON EMP1.AutoId=OM.PackerAutoId                                                                                                   
			LEFT JOIN EmployeeMaster AS EMP2 ON EMP2.AutoId=OM.ManagerAutoId                                                                                                   
			LEFT JOIN EmployeeMaster AS EMP3 ON EMP3.AutoId=OM.AccountAutoId   
		    INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
			) as t 
			WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)) 
			order by convert(datetime,OrderDate) desc                              
                   
			SELECT COUNT(AI.AutoId) AS RecordCount,case when @PageSize=0 then COUNT(AI.AutoId) else @PageSize end AS PageSize,@PageIndex as PageIndex 
			FROM #AutoId AS AI              
                                      
			SELECT T.*,
			(case when StatusCode=11 then isnull(ISNULL(DO.AmtDue,0),0)else 0.00 end) as AmtDue,                                                                                                  
			(case when StatusCode=11 then isnull(DO.AmtPaid,0) else 0.00 end ) as AmtPaid  FROM #RESULT AS T
			LEFT JOIN  DeliveredOrders DO ON DO.OrderAutoId=T.AutoId                                     
		END
		ELSE IF @Opcode=42                                       
		BEGIN                                                                                                                                                                                         
			SELECT om.AutoId as OrderAutoId,OrderNo  +' '+sm.StatusType as OrderNo,om.PayableAmount as DueAmount                                                                                
			FROM OrderMaster AS OM left JOIN DeliveredOrders AS DO ON OM.AutoId=DO.OrderAutoId                                                                                                  
			inner join StatusMaster as sm on sm.autoid=om.status and sm.Category='OrderMaster'                                                                                   
			WHERE CustomerAutoId=@CustomerAutoId AND ISNULL(DO.AmtDue,om.PayableAmount) > 0                                         
			and om.Status=11                                                                                              
                                                                                                
                                                                                       
			SELECT  CreditAutoId,CreditNo,CONVERT(VARCHAR(10),CreditDate,101) AS CreditDate,TotalAmount                                                                                                  
			FROM CreditMemoMaster AS OM                                                                       
			WHERE                                                                                                   
			Status=3 AND OrderAutoId is null                                                                                                   
			AND                                                                                                  
			CreditAutoId in                                                                                                   
			(select * from [dbo].[fnSplitString](@ReferenceNo,','))                                                                                          
		END  
		ELSE IF  @Opcode=21                                                              
		BEGIN                                                                 
                                             
			BEGIN TRY                                            
			BEGIN TRAN                                                                                                  
				SET @CustomerAutoId =(select CustomerAutoId from OrderMaster WHERE AutoId=@OrderAutoId)                                                                                
				declare  @TotalAmount decimal(10,2)=ISNULL((SELECT PayableAmount FROM OrderMaster WHERE AutoId=@OrderAutoId),0)                                                                                                  
				declare @ReceivedAmount decimal(10,2)=ISNULL((SELECT AmtPaid FROM DeliveredOrders WHERE OrderAutoId=@OrderAutoId),0)                                                                     
				declare @CreditAmount decimal(10,2)=ISNULL((SELECT SUM(TotalAmount) FROM CreditMemoMaster WHERE                                                                                                   
				CreditAutoId in (select * from [dbo].[fnSplitString](@ReferenceNo,','))),0)                                                                                                  
                                                                                               
				UPDATE CreditMemoMaster SET OrderAutoId=@OrderAutoId,CompletedBy=@EmpAutoId,CompletionDate=GETDATE(),                                                                                              
				CompletionRemarks=@EmployeeRemarks                                                        
				WHERE CreditAutoId in (select * from [dbo].[fnSplitString](@ReferenceNo,','))                                              
                                                                                                      
				IF(@CreditAmount>@TotalAmount)                                                                                                  
				BEGIN                                                                                         
				IF EXISTS(SELECT * FROM CustomerCreditMaster WHERE CustomerAutoId=@CustomerAutoId)                                                                                                  
				BEGIN                                                                                                   
				UPDATE CustomerCreditMaster SET CreditAmount=ISNULL(CreditAmount,0.00)+(@CreditAmount-@TotalAmount)                                                                                                  
				WHERE CustomerAutoId=@CustomerAutoId                                                          
				END                                                                                                  
				ELSE                         
				BEGIN                                                                                                  
				INSERT INTO CustomerCreditMaster(CustomerAutoId,CreditAmount)                                                                      
				VALUES(@CustomerAutoId,(@CreditAmount-@TotalAmount))                                                                         
				END                                                                                   
				INSERT INTO tbl_Custumor_StoreCreditLog(CustomerAutoId,ReferenceType,ReferenceNo,Amount,CreatedDate,CreatedBy)                                     
				values(@CustomerAutoId,'OrderMaster',@OrderAutoId,-(@CreditAmount-@TotalAmount),getdate(),@EmpAutoid)                                                                                                  
				END                                             
				IF ISNULL(@ReceivedAmount,0)>ISNULL((SELECT PayableAmount FROM OrderMaster WHERE AutoId=@OrderAutoId),0)                                              
				BEGIN                                              
				SET @CreditAmount=ISNULL(@ReceivedAmount,0)-ISNULL((SELECT PayableAmount FROM OrderMaster WHERE AutoId=@OrderAutoId),0)                                              
                                              
				IF EXISTS(SELECT * FROM CustomerCreditMaster WHERE CustomerAutoId=@CustomerAutoId)                                                                  
				BEGIN                                                                   
				UPDATE CustomerCreditMaster SET CreditAmount=ISNULL(CreditAmount,0.00)+@CreditAmount                             
				WHERE CustomerAutoId=@CustomerAutoId                                                                    
				END                                                                  
				ELSE                                                                  
				BEGIN                                                                  
				INSERT INTO CustomerCreditMaster(CustomerAutoId,CreditAmount)                                                                  
				VALUES(@CustomerAutoId,@CreditAmount)                                                                  
				END                                                  
				INSERT INTO tbl_Custumor_StoreCreditLog(CustomerAutoId,ReferenceType,ReferenceNo,Amount,CreatedDate,CreatedBy)                                                                  
				values(@CustomerAutoId,'OrderMaster',@OrderAutoId,-@CreditAmount,getdate(),@EmpAutoid)                                              
			END                                                                                        
			COMMIT TRAN                                                                                                  
			END TRY                                                                               
			BEGIN CATCH                      
				ROLLBACK TRAN                                     
				SET @isException=1                                                    
				SET @exceptionMessage=ERROR_MESSAGE()                                                                                                  
			END CATCH                                                                 
		END
		ELSE IF @Opcode=43                                                                                                  
		BEGIN  
		
		select *,5 as PaymentMode from customercreditmaster where customerAutoId=@CustomerAutoId         
                                                                                                 
		SELECT OM.AutoId as OrderAutoId,OrderNo,CONVERT(VARCHAR(10),OrderDate,101) AS OrderDate ,                                                                                                
		otm.OrderType AS                                                                                                   
		OrderType,DO.PayableAmount,DO.AmtDue,DO.AmtPaid,datediff(dd,OrderDate,getdate())  as DaysOld                                                       
		FROM DeliveredOrders AS DO  INNER JOIN OrderMaster AS OM ON OM.AutoId=DO.OrderAutoId   AND DO.AmtDue > 0     
		  INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
		WHERE CustomerAutoId=@CustomerAutoId           
         
		SELECT AutoID,PaymentMode FROM PAYMENTModeMaster   
		select AutoId,FirstName+' '+ISNULL(LastName ,'') as EmpName from EmployeeMaster where EmpType=6 and status=1
		order by EmpName
		END  


		ELSE IF @Opcode=44                                                                                                
		BEGIN  
		select emp.FirstName+' ' +ISNULL(LastName,'') as SettledBy,pod.ReceivedAmount,spd.PaymentId,format(spd.PaymentDate,'MM/dd/yyyy hh:mm tt')as PaymentDate,
		pmm.PaymentMode from PaymentOrderDetails pod
		INNER JOIN CustomerPaymentDetails spd on spd.PaymentAutoId=pod.PaymentAutoId and spd.Status=0
		INNER JOIN EmployeeMaster emp on emp.AutoId=spd.EmpAutoId
		INNER JOIN PAYMENTModeMaster pmm on pmm.AutoId=spd.PaymentMode
		where pod.OrderAutoId=@OrderAutoId         
		order by spd.PaymentDate
		END
		
END TRY                                                                                                  
BEGIN CATCH                                                                                                  
	SET @isException=1                                                           
	SET @exceptionMessage='Oops! Something went wrong.Please try later.'                                                  
END CATCH 
End
GO
