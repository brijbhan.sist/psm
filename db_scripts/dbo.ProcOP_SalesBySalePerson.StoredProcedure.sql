
ALTER PROCEDURE [dbo].[ProcOP_SalesBySalePerson]                                                                    
@Opcode INT=NULL,                                                                                                                                                                                                                                   
@SalesPersonAutoId int =null,                                                                                                                            
@SalesPerson  VARCHAR(500)=NULL,                                                         
@FromDate date =NULL,                                                                    
@ToDate date =NULL,                             
@PageIndex INT=1,                                                                    
@PageSize INT=10,                                                                    
@RecordCount INT=null,                                                                    
@isException bit out,                                                                    
@exceptionMessage varchar(max) out                                                                    
AS                                                                    
BEGIN                                                                     
 BEGIN TRY                                                                    
  Set @isException=0                                                                    
  Set @exceptionMessage='Success'                                                                    
 IF @Opcode=41                                                                    
  BEGIN                                                       
   SELECT AutoId SId,FirstName+' '+ISNULL(LASTNAME,'') AS SP  FROM EmployeeMaster WHERE EmpType IN (2) and Status=1 order by SP ASC 
   for json path
  END                                                                    
  ELSE IF @Opcode=42                                                                    
  BEGIN  
		Declare @ChildDbLocation varchar(50),@sql nvarchar(max) 
		select @ChildDbLocation=ChildDB_Name from CompanyDetails

		IF OBJECT_ID('tempdb..#Result46') IS NOT NULL
		DROP TABLE #Result46

		Create table #Result46(
		RowNumber int,EmployeeName varchar(50),SalesPersonAutoId INT,NoOfOrders int,
		GrandTotal decimal(18,2),NoofPOSOrders int,
		POSTotalAmount decimal(18,2),MigratedOrder INT,MigratedSaleAmount DECIMAL(18,2)
		);

		SET @sql='
		select ROW_NUMBER() over(order by FirstName+'' ''+ISNULL(lastName,'''')) as RowNumber,
		emp.FirstName+'' ''+ISNULL(lastName,'''') as EmployeeName,SalesPersonAutoId,
		SUM(noofOrders) as NoOfOrders,SUM(TotalAmount) as GrandTotal,SUM(NoofPOSOrders) as NoofPOSOrders,SUM(POSTotalAmount) as POSTotalAmount,
		SUM(MigratedOrder) as MigratedOrder,SUM(MigratedSaleAmount) as MigratedSaleAmount
		from (
		select SalesPersonAutoId,count(1) as noofOrders,SUM(TotalAmount) as TotalAmount,0 as NoofPOSOrders,0 as POSTotalAmount,0 as MigratedOrder,0 as MigratedSaleAmount from
		( 
		select SalesPersonAutoId,
		(ISNULL(om.TotalAmount,0)-ISNULL((select SUM(ISNULL(cmm.GrandTotal,0)) from CreditMemoMaster as cmm where cmm.OrderAutoId=om.AutoId),0)) as TotalAmount,
		0 as  POSTotalAmount
		from OrderMaster as om  where 
		om.Status=11  and ISNULL(om.OrderType,0) not in (1)
		and                                                             
		(ISNULL('''+Convert(varchar(25),@SalesPerson)+''',''0'')=''0'' OR om.SalesPersonAutoId                                                                    
		in (select * from dbo.fnSplitString('''+Convert(varchar(25),@SalesPerson)+''','',''))                                                                   
		) 
		and 
		( '''+convert(varchar(25),@FromDate)+''' IS NULL 
									OR '''+convert(varchar(25),@ToDate)+''' IS NULL 
									OR ( CONVERT(DATE, orderdate) BETWEEN 
										CONVERT(DATE, '''+convert(varchar(25),@FromDate)+''') AND 
											CONVERT(DATE, '''+convert(varchar(25),@ToDate)+''') ) )

		) as t
		group by SalesPersonAutoId
		UNION ALL

		select SalesPersonAutoId,0 as noofOrders,0 as TotalAmount,count(1) as NoofPOSOrders, SUM(POSTotalAmount) as POSTotalAmount,0 as MigratedOrder,0 as MigratedSaleAmount from
		( 
		select SalesPersonAutoId,
		(ISNULL(om.TotalAmount,0)-ISNULL((select SUM(ISNULL(cmm.GrandTotal,0)) from CreditMemoMaster as cmm where cmm.OrderAutoId=om.AutoId),0)) as POSTotalAmount
		from OrderMaster as om  where 
		om.Status=11  and ISNULL(om.OrderType,0) in (1)
		and                                                             
			(ISNULL('''+Convert(varchar(25),@SalesPerson)+''',''0'')=''0'' OR om.SalesPersonAutoId                                                                    
			in (select * from dbo.fnSplitString('''+Convert(varchar(25),@SalesPerson)+''','',''))                                                                   
		) 
		and 
		( '''+convert(varchar(25),@FromDate)+''' IS NULL 
									OR '''+convert(varchar(25),@ToDate)+''' IS NULL 
									OR ( CONVERT(DATE, orderdate) BETWEEN 
										CONVERT(DATE, '''+convert(varchar(25),@FromDate)+''') AND 
											CONVERT(DATE, '''+convert(varchar(25),@ToDate)+''') ) )
		) as t
		group by SalesPersonAutoId

		UNION ALL

		select SalesPersonAutoId,0 as noofOrders,0 as TotalAmount,0 as NoofPOSOrders,0 as POSTotalAmount,count(1) as MigratedOrder,SUM(MigratedSaleAmount) as MigratedSaleAmount from
		( 
		select SalesPersonAutoId,
		(ISNULL(om.TotalAmount,0)-ISNULL((select SUM(ISNULL(cmm.GrandTotal,0)) from ['+@ChildDbLocation+'].[dbo].CreditMemoMaster as cmm where cmm.OrderAutoId=om.AutoId),0)) as MigratedSaleAmount
		from ['+@ChildDbLocation+'].[dbo].OrderMaster as om  where 
		om.Status=11  
		and                                                             
		(ISNULL('''+Convert(varchar(25),@SalesPerson)+''',''0'')=''0'' OR om.SalesPersonAutoId                                                                    
		in (select * from dbo.fnSplitString('''+Convert(varchar(25),@SalesPerson)+''','',''))                                                                   
		) 
		and 
		( '''+convert(varchar(25),@FromDate)+''' IS NULL 
									OR '''+convert(varchar(25),@ToDate)+''' IS NULL 
									OR ( CONVERT(DATE, orderdate) BETWEEN 
										CONVERT(DATE, '''+convert(varchar(25),@FromDate)+''') AND 
											CONVERT(DATE, '''+convert(varchar(25),@ToDate)+''') ) )
		) as t
		group by SalesPersonAutoId 


		) as t

		inner join EmployeeMaster as emp on emp.AutoId=t.SalesPersonAutoId
		group by emp.FirstName+'' ''+ISNULL(lastName,''''),SalesPersonAutoId

		order by SalesPersonAutoId'
		
	INSERT INTO #Result46
	EXEC sp_executesql @sql
	select(
	select
	isnull((SELECT * FROM #Result46 WHERE @PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1) for json path, INCLUDE_NULL_VALUES),'[]') as OrderList,
	isnull((SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize=0 then COUNT(RowNumber) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Result46 for json path, INCLUDE_NULL_VALUES),'[]') as Paging,
	isnull((SELECT ISNULL(sum(GrandTotal),0.00) AS GrandTotal,ISNULL(sum(POSTotalAmount),0.00) AS POSTotalAmount,ISNULL(sum(MigratedOrder),0) AS MigratedOrder,                      
	ISNULL(sum(MigratedSaleAmount),0) AS MigratedSaleAmount,ISNULL(sum(NoOfOrders),0) AS NoOfOrders,ISNULL(sum(NoofPOSOrders),0) AS NoofPOSOrders                   
	FROM #Result46 for json path, INCLUDE_NULL_VALUES),'[]') as OverAllTotal,FORMAT(getdate(),'MM/dd/yyyy hh:mm tt') as PrintDate   
	for json path, INCLUDE_NULL_VALUES
	)as DropDownList    
  END                                                                      
                            
 END TRY                                                    
 BEGIN CATCH                                                                    
  Set @isException=1                                                                    
  Set @exceptionMessage=ERROR_MESSAGE()                                                           
 END CATCH                                                                    
END 
