USE [psmnj.a1whm.com]
GO

/****** Object:  UserDefinedFunction [dbo].[FN_PORemainQty]    Script Date: 02/08/2020 15:32:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create FUNCTION  [dbo].[FN_PRemainQty]
(
	 @POAutoId int,
	 @ProductAutoId int,
     @UnitAutoId int
	
)
RETURNS int
AS
BEGIN
	DECLARE @RemainQty int	 
	set @RemainQty=(select isnull(Qty,0)-ISNULL(ReceivedQty,0) from PurchaseProductsMaster where POAutoId=@POAutoId and ProductAutoId=@ProductAutoId  and Unit=@UnitAutoId)
	RETURN @RemainQty
END
GO


