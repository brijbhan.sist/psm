USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcAppDriverOrder]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE ProcEDURE [dbo].[ProcAppDriverOrder]                    
@Opcode INT=NULL,                      
@timeStamp datetime =null,                
@orderNumber varchar(50)=null,               
@CustomerAutoId INT=NULL,    
@DriverAutoId int=null,           
@dtDriverActivities [DT_APP_DriverActivities] readonly,                    
@isException bit out,                    
@exceptionMessage varchar(max) out                    
AS                    
BEGIN                     
 BEGIN TRY                    
  Set @isException=0                    
  Set @exceptionMessage='Success'                      
  If @Opcode=201                      
  BEGIN                    
   BEGIN TRY                    
    BEGIN TRAN     
    if exists(select * from OrderMaster where OrderNo=@orderNumber)    
    begin    
     if exists(select * from OrderMaster where OrderNo=@orderNumber and Status in (4,5))    
     begin    
      declare @newStatus int, @driverRemark varchar(max), @orderAutoId int, @DeliveryDate datetime    
    
      select top 1 @newStatus=t.orderActivityStatus,@driverRemark=t.deliveryRemark,    
      @DeliveryDate=utcActivityTimeStamp     
      from @dtDriverActivities as t order by t.orderActivityStatus desc    
    
      update om set Status=@newStatus, DrvRemarks=@driverRemark,     
      DelDate=(case when @newStatus=6 then @DeliveryDate else DelDate end),    
      DeliveryDate = (case when @newStatus=6 then @DeliveryDate else DeliveryDate end)    
      from OrderMaster as om where OrderNo=@orderNumber    
    
          
		SET @orderAutoId = (select autoid from OrderMaster where OrderNo=@orderNumber)    
		SET @customerAutoId = (select CustomerAutoId from OrderMaster where OrderNo=@orderNumber)  
		DECLARE @customerType int = (select CustomerType from customerMaster where Autoid=@customerAutoId)  
		if @customerType=3  
		BEGIN  
			EXEC [dbo].[ProcUpdatePOStatus_All]   
			@CustomerId=@customerAutoId,  
			@OrderAutoId=@OrderAutoId,  
			@Status=@newStatus  
		END               
      INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])     
      select 3, @DriverAutoId,t.utcActivityTimeStamp,    
      (    
       case t.orderActivityStatus     
       when 5 then 'From Ready to ship to Shipped.'    
       when 6 then 'From Shipped to Delivered.'    
       when 7 then 'From Shipped to Undelivered.'    
       end    
      ),    
      @orderAutoId from @dtDriverActivities as t order by t.orderActivityStatus asc    
     end    
    end    
    else     
    begin          
     Set @isException=1                 
     Set @exceptionMessage='Order does not exists'      
    end    
    COMMIT TRAN                    
   END TRY                    
   BEGIN CATCH                    
    ROLLBACK TRAN                    
    Set @isException=1                    
    Set @exceptionMessage=ERROR_MESSAGE()                    
   End Catch                     
  END                                    
 END TRY                    
 BEGIN CATCH                    
  Set @isException=1                    
  Set @exceptionMessage=ERROR_MESSAGE()                    
 END CATCH                    
END 
GO
