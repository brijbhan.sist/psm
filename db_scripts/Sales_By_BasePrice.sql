select 
		(em.FirstName +' ' + em.LastName)SalesRep
		,pm.ProductId,pm.ProductName,um.UnitType ,doi.BasePrice  ,doi.QtyDel,pd.Qty	,doi.UnitPrice,doi.NetPrice 		 
into #t1 from OrderMaster as om
inner join Delivered_Order_Items as doi on doi.OrderAutoId=om.AutoId
inner join ProductMaster as pm on pm.AutoId=doi.ProductAutoId
inner join PackingDetails as pd on pd.ProductAutoId=pm.AutoId and doi.UnitAutoId=pd.UnitType
inner join UnitMaster as um on um.AutoId=pm.PackingAutoId
inner join EmployeeMaster as em on em.AutoId=om.SalesPersonAutoId
where convert(date,om.OrderDate) between '12/01/2020' and '12/17/2020' and om.Status=11  and (em.FirstName +' ' + em.LastName) = 'Bittu Patel (Sp)'
UNION ALL
select		(em.FirstName +' ' + em.LastName)SalesRep
		,pm.ProductId,pm.ProductName,um.UnitType ,oim.BasePrice  ,oim.TotalPieces,pd.Qty ,oim.UnitPrice,oim.NetPrice 		 
from OrderMaster as om
inner join OrderItemMaster as oim on oim.OrderAutoId=om.AutoId
inner join ProductMaster as pm on pm.AutoId=oim.ProductAutoId
inner join PackingDetails as pd on pd.ProductAutoId=pm.AutoId and oim.UnitTypeAutoId=pd.UnitType
inner join UnitMaster as um on um.AutoId=pm.PackingAutoId
inner join EmployeeMaster as em on em.AutoId=om.SalesPersonAutoId
where convert(date,om.OrderDate) between '12/01/2020' and '12/17/2020' and om.Status not in (11,8)  and (em.FirstName +' ' + em.LastName) = 'Bittu Patel (Sp)'
UNION ALL
select 
		(em.FirstName +' ' + em.LastName)SalesRep
		,pm.ProductId,pm.ProductName,um.UnitType ,doi.BasePrice  ,doi.QtyDel,pd.Qty	,doi.UnitPrice,doi.NetPrice 		 
from [psmnj.easywhm.com].dbo.OrderMaster as om
inner join [psmnj.easywhm.com].dbo.Delivered_Order_Items as doi on doi.OrderAutoId=om.AutoId
inner join [psmnj.easywhm.com].dbo.ProductMaster as pm on pm.AutoId=doi.ProductAutoId
inner join [psmnj.easywhm.com].dbo.PackingDetails as pd on pd.ProductAutoId=pm.AutoId and doi.UnitAutoId=pd.UnitType
inner join [psmnj.easywhm.com].dbo.UnitMaster as um on um.AutoId=pm.PackingAutoId
inner join [psmnj.easywhm.com].dbo.EmployeeMaster as em on em.AutoId=om.SalesPersonAutoId
where convert(date,om.OrderDate) between '12/01/2020' and '12/17/2020' and om.Status=11  and (em.FirstName +' ' + em.LastName) = 'Bittu Patel (Sp)'
select 
		 SalesRep,ProductId,ProductName,UnitType,BasePrice  
		 ,convert(decimal (18,2), sum(convert(decimal (18,2),QtyDel)/Qty))QtySold
		,sum(NetPrice)TotalSales_In_This_BasePrice
		,sum((case when (BasePrice = UnitPrice) then NetPrice else 0 end ))Sales_At_BasePrice 		
		,sum((case when (BasePrice != UnitPrice) then NetPrice else 0 end ))Sales_At_PriceLeavel
from  #t1
group by SalesRep  ,ProductId,ProductName,UnitType,BasePrice
order by  ProductId
 

 drop table #t1