USE [psmnj.a1whm.com]
GO

/****** Object:  Table [dbo].[CreditMemoTypeMaster]    Script Date: 06-25-2021 06:16:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CreditMemoTypeMaster]') AND type in (N'U'))
DROP TABLE [dbo].[CreditMemoTypeMaster]
GO

/****** Object:  Table [dbo].[CreditMemoTypeMaster]    Script Date: 06-25-2021 06:16:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CreditMemoTypeMaster](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[CreditType] [varchar](50) NOT NULL
) ON [PRIMARY]
GO


  Insert into CreditMemoTypeMaster (CreditType) values ('Type 1 - NT Default')
  Insert into CreditMemoTypeMaster (CreditType) values ('Type 2 - T')
