Alter PROCEDURE [dbo].[ProcUpdateSRP_ALL]           
@ProductId VARCHAR(12)=NULL         
AS          
BEGIN   
--PSMCT
 IF EXISTS(SELECT [AutoId] FROM [psmct.a1whm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)          
 BEGIN  
  UPDATE CT SET ct.NewSRP=nj.NewSRP      
  FROM [psmct.a1whm.com].[dbo].[ProductMaster] AS CT INNER JOIN ProductMaster AS NJ        
  ON CT.ProductId=NJ.ProductId where NJ.ProductId =@ProductId        
 END 
--PSMPA
 IF EXISTS(SELECT [AutoId] FROM [psmpa.a1whm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)          
 BEGIN  
   UPDATE CT SET CT.NewSRP=NJ.NewSRP       
   FROM [psmpa.a1whm.com].[dbo].[ProductMaster] AS CT INNER JOIN ProductMaster AS NJ        
   ON CT.ProductId=NJ.ProductId where NJ.ProductId =@ProductId        
 END 
--PSMNPA
 IF EXISTS(SELECT [AutoId] FROM [psmnpa.a1whm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)          
 BEGIN    
   UPDATE CT SET CT.NewSRP=NJ.NewSRP         
   FROM [psmnpa.a1whm.com].[dbo].[ProductMaster] AS CT INNER JOIN ProductMaster AS NJ        
   ON CT.ProductId=NJ.ProductId where NJ.ProductId =@ProductId        
 END 
--PSMWPA
 IF EXISTS(SELECT [AutoId] FROM [psmwpa.a1whm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)          
 BEGIN     
   UPDATE WPA SET WPA.NewSRP=NJ.NewSRP     
   FROM [psmwpa.a1whm.com].[dbo].[ProductMaster] AS WPA INNER JOIN ProductMaster AS NJ        
   ON WPA.ProductId=NJ.ProductId where NJ.ProductId =@ProductId        
 END
--PSMNY
 IF EXISTS(SELECT [AutoId] FROM [psmny.a1whm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)          
 BEGIN  
   UPDATE WPA SET WPA.NewSRP=NJ.NewSRP  
   FROM [psmny.a1whm.com].[dbo].[ProductMaster] AS WPA INNER JOIN ProductMaster AS NJ        
   ON WPA.ProductId=NJ.ProductId where NJ.ProductId =@ProductId        
 END
--PSMNJ Easy
 IF EXISTS(SELECT [AutoId] FROM [psmnj.easywhm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)          
 BEGIN       
  UPDATE ESY SET ESY.NewSRP=NJ.NewSRP  
  FROM [psmnj.easywhm.com].[dbo].[ProductMaster] AS ESY INNER JOIN ProductMaster AS NJ        
  ON ESY.ProductId=NJ.ProductId where NJ.ProductId =@ProductId        
 END 
--PSMCT Easy
 IF EXISTS(SELECT [AutoId] FROM [psmct.easywhm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)          
 BEGIN     
  UPDATE ESY SET ESY.NewSRP=NJ.NewSRP  
  FROM [psmct.easywhm.com].[dbo].[ProductMaster] AS ESY INNER JOIN ProductMaster AS NJ        
  ON ESY.ProductId=NJ.ProductId where NJ.ProductId =@ProductId        
 END  
--PSMNPA Easy
 IF EXISTS(SELECT [AutoId] FROM [psmnpa.easywhm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)          
 BEGIN   
  UPDATE ESY SET ESY.NewSRP=NJ.NewSRP    
  FROM [psmnpa.easywhm.com].[dbo].[ProductMaster] AS ESY INNER JOIN ProductMaster AS NJ        
  ON ESY.ProductId=NJ.ProductId where NJ.ProductId =@ProductId        
 END 
--PSMPA Easy
 IF EXISTS(SELECT [AutoId] FROM [psmpa.easywhm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)          
 BEGIN     
  UPDATE ESY SET ESY.NewSRP=NJ.NewSRP     
  FROM [psmpa.easywhm.com].[dbo].[ProductMaster] AS ESY INNER JOIN ProductMaster AS NJ        
  ON ESY.ProductId=NJ.ProductId where NJ.ProductId =@ProductId        
 END 
--PSMWPA Easy
 IF EXISTS(SELECT [AutoId] FROM [psmwpa.easywhm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)          
 BEGIN     
  UPDATE ESY SET ESY.NewSRP=NJ.NewSRP     
  FROM [psmwpa.easywhm.com].[dbo].[ProductMaster] AS ESY INNER JOIN ProductMaster AS NJ        
  ON ESY.ProductId=NJ.ProductId where NJ.ProductId =@ProductId        
 END 
--PSMNY Easy
 IF EXISTS(SELECT [AutoId] FROM [psmny.easywhm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)          
 BEGIN     
  UPDATE ESY SET ESY.NewSRP=NJ.NewSRP     
  FROM [psmny.easywhm.com].[dbo].[ProductMaster] AS ESY INNER JOIN ProductMaster AS NJ        
  ON ESY.ProductId=NJ.ProductId where NJ.ProductId =@ProductId        
 END   
END        