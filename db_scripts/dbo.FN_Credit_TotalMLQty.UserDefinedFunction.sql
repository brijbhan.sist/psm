USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Credit_TotalMLQty]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Alter table CreditItemMaster drop column [TotalMLTax]
GO
CREATE OR ALTER FUNCTION  [dbo].[FN_Credit_TotalMLQty]
(
	 @AutoId int
)
RETURNS decimal(18,2)
AS
BEGIN
	DECLARE @TotalMLQty decimal(18,2)
	DECLARE @CreditMemoType int=(select CreditMemoType from CreditMemoMaster where CreditAutoId=(select CreditAutoId from CreditItemMaster where ItemAutoId=@AutoId))
	IF @CreditMemoType=1
	BEGIN
	SET @TotalMLQty=0
	END
	ELSE
	BEGIN
	SET @TotalMLQty=isnull((SELECT (AcceptedQty*UnitMLQty*QtyPerUnit) FROM CreditItemMaster WHERE ItemAutoId=@AutoId),0)
	END
	RETURN @TotalMLQty
END
GO

Alter table CreditItemMaster Add [TotalMLTax]  AS ([dbo].[FN_Credit_TotalMLQty]([ItemAutoId]))
