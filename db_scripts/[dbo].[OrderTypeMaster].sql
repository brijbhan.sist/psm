
GO

/****** Object:  Table [dbo].[OrderTypeMaster]    Script Date: 03/13/2020 11:39:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[OrderTypeMaster](
	[AutoId] [int] NULL,
	[OrderType] [varchar](30) NULL
) ON [PRIMARY]
GO


