USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Order_Weight_Oz_Qty]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

alter table OrderItemMaster drop column Weight_Oz_TotalQty
drop function FN_Order_Weight_Oz_Qty
go
CREATE FUNCTION  [dbo].[FN_Order_Weight_Oz_Qty]
(
	 @AutoId int
)
RETURNS decimal(18,2)
AS
BEGIN
	DECLARE @Weight_Oz decimal(18,2)	
	SET @Weight_Oz=isnull((select ISNULL(Weight_Oz,0)*ISNULL(TotalPieces,0) from orderItemMaster where AutoId=@AutoId),0.00)
	RETURN @Weight_Oz
END
GO
alter table OrderItemMaster add [Weight_Oz_TotalQty]  AS ([dbo].[FN_Order_Weight_Oz_Qty]([autoid]))
