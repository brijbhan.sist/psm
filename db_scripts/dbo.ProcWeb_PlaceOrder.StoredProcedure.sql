USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcWeb_PlaceOrder]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[ProcWeb_PlaceOrder]
	@UserId INT=NULL,
	@StoreId INT=NULL,
	@Opcode INT=NULL,
	@OrderAutoId int=null,  
	@OrderId INT=NULL,                                                                               
	@isException bit out,               
	@exceptionMessage varchar(max) out  
AS
BEGIN
     SET @isException=0
	 SET @exceptionMessage=''
		 Declare @OrderNo varchar(15),@Terms int,@BillAutoId int,@ShipAutoId int,@State int,@ShippingCharges decimal(18,2),--@PaymentMethod int,@ShippingMethod int,
		@SalesPersonAutoId Int,@TaxType int,@IsTaxApply INT=0,@CartId int,@PriceLevelAutoId int,@LogRemark varchar(Max)  
     If @Opcode=101                                                                                           
     BEGIN                                   
         BEGIN TRY                                                                                                                        
                BEGIN TRAN    
					
	                                                                                                                    
					SET @OrderNo = (SELECT DBO.SequenceCodeGenerator('OrderNumber')) 

					SELECT @BillAutoId=BillAddrId FROM WebCartMaster WHERE StorId=@StoreId     
					SELECT @ShipAutoId=ShippAddrId FROM WebCartMaster WHERE StorId=@StoreId   
					 
					SELECT @ShippingCharges=ISNULL(WCS.ShippingCharge,0.00) from WebCustomerShippingType  as WCS
					inner join WebCartMaster as WCM on
					WCS.ShippingAutoId=WCM.ShippingId Where  WCM.StorId=@StoreId  
					                                                                                               
					SET @State=(SELECT TOP 1 State FROM BillingAddress WHERE AutoId =@BillAutoId) 
					SELECT @TaxType= AutoId FROM TaxTypeMaster WHERE  State=@State                                          
					declare @TaxValue decimal(10,2)=isnull((select Value from TaxTypeMaster where AutoId =@TaxType),0.00)                                                    
					SET @Terms=ISNUll((SELECT Terms  FROM CustomerMaster WHERE AutoId=@StoreId),0)
					SET @SalesPersonAutoId=(SELECT SalesPersonAutoId  FROM CustomerMaster WHERE AutoId=@StoreId) 
					Select @CartId=AutoId from WebCartMaster Where StorId=@StoreId 

				IF EXISTS(SELECT top 1 * FROM TaxTypeMaster WHERE  State=@State)                                                                                      
				BEGIN  
				     SET @IsTaxApply=1                                                                                  
				END 
				ELSE
				BEGIN
				    SET @IsTaxApply=0      
				END                                                                     
				INSERT INTO [dbo].[OrderMaster] ([OrderNo],[OrderDate],[DeliveryDate],[CustomerAutoId],[Terms],[BillAddrAutoId],[ShipAddrAutoId],                                                                                          
				[SalesPersonAutoId],OverallDiscAmt,[ShippingCharges],[Status],[ShippingType],                                                                                                                      
				TaxType,OrderRemarks,OrderType,TaxValue,mlTaxPer,IsTaxApply,Weigth_OZTax)    
                                                                                                                        
				SELECT @OrderNo,getdate(),null,@StoreId,@Terms,@BillAutoId,@ShipAutoId,@SalesPersonAutoId,0,@ShippingCharges,1,1,                           
				@TaxType,'',2,@TaxValue,ISNULL((SELECT TaxRate FROM MLTaxMaster where TaxState=@STATE),0.00),                                                                                      
				@IsTaxApply,isnull((select Value from Tax_Weigth_OZ),0) 
	 
				SET @OrderAutoId=SCOPE_IDENTITY()
	                  
				INSERT INTO [dbo].[OrderItemMaster]([OrderAutoId],[ProductAutoId],[UnitTypeAutoId],[QtyPerUnit],[UnitPrice],[RequiredQty],                                                                                                                      
				[SRP],[GP],[Tax],IsExchange,TaxValue,UnitMLQty,isFreeItem,Weight_Oz)  
				                                                                                                                    
				SELECT @OrderAutoId,wcm.ProductId,ProductUnit,PerUnitQty,UnitPrice,OrderQty,0,0,0,0,@TaxValue,
				case when pm.IsApply_ML=1 then isnull(pm.MLQty,0) else 0 end as MLQty,0,ISNULL(pm.WeightOz,0) 
				FROM WebCartItemMaster as wcm
				INNER JOIN ProductMaster as pm on
				wcm.ProductId=pm.AutoId
					Where  CartMasterId=@CartId  
												                                                  
				INSERT INTO [dbo].[Order_Original] ([AutoId],[OrderNo],[OrderDate],[DeliveryDate],[CustomerAutoId],[Terms],[BillAddrAutoId],[ShipAddrAutoId],                                      
				[SalesPersonAutoId],[TotalAmount],[OverallDisc],[OverallDiscAmt],                                                        
				[ShippingCharges],[TotalTax],[GrandTotal],[ShippingType],                                                                                                                      
				TaxType,MLQty,MLTax) 
	                                                                                                                 
				SELECT [AutoId],[OrderNo],[OrderDate],[DeliveryDate],[CustomerAutoId],[Terms],[BillAddrAutoId],[ShipAddrAutoId],                                      
				[SalesPersonAutoId],[TotalAmount],[OverallDisc],[OverallDiscAmt],                                                        
				[ShippingCharges],[TotalTax],[GrandTotal],[ShippingType],                                                                                                                      
				TaxType,MLQty,MLTax from
				[dbo].[OrderMaster] WHERE AutoId=@OrderAutoId                                                                                                                      
                                                   
				UPDATE SequenceCodeGeneratorMaster SET currentSequence = currentSequence + 1 WHERE SequenceCode='OrderNumber' 
                                                                         
				INSERT INTO [dbo].[OrderItems_Original]([OrderAutoId],[ProductAutoId],[UnitTypeAutoId],[QtyPerUnit],[UnitPrice],[RequiredQty],[TotalPieces],
				[SRP],[GP],[Tax],[NetPrice],IsExchange,isFreeItem,UnitMLQty,TotalMLQty) 
					                                   
				SELECT [OrderAutoId],[ProductAutoId],[UnitTypeAutoId],[QtyPerUnit],[UnitPrice],[RequiredQty],[TotalPieces],
				[SRP],[GP],[Tax],[NetPrice],IsExchange,isFreeItem,UnitMLQty,TotalMLQty
				FROM OrderItemMaster AS tb                                                                      
				inner join ProductMaster as pm on pm.AutoId=tb.ProductAutoId WHERE OrderAutoId=@OrderAutoId 
				SET @PriceLevelAutoId = (SELECT [PriceLevelAutoId] FROM [dbo].[CustomerPriceLevel] WHERE [CustomerAutoId] = @StoreId) 

				exec UPDTAE_PRICELEVEL                                                                                                                       
				@OrderAutoId=@OrderAutoId,                                                                                                       
				@PriceLevelAutoId=@PriceLevelAutoId        
				                                   
				SET @LogRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=1), '[OrderNo]', @OrderNo)   
				                                                                                                                   
				INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                                                                                 
				VALUES(1,@StoreId,getdate(),@LogRemark,@OrderAutoId)
				                                           
				DELETE FROM WebCartItemMaster WHERE CartMasterId=@CartId 
				DELETE FROM WebCartMaster WHERE StorId=@StoreId   
				    
				update CustomerMaster set LastOrderDate = GETDATE() where AutoId =@StoreId 

				EXEC [dbo].[ProcWeb_PlaceOrder] @OrderAutoId=@OrderAutoId,@Opcode=102,@isException=0,@exceptionMessage=''

               COMMIT TRANSACTION                                                                                                                        
          END TRY                                                                                                                        
          BEGIN CATCH                                                                                                    
						ROLLBACK TRAN                                                                                             
						Set @isException=1                                                                       
						Set @exceptionMessage=ERROR_MESSAGE()                                                                   
          End Catch                                                                                                                     
   END  
	IF @Opcode=102
	BEGIN
		Select  OM.ShippingCharges,OM.Totaltax,OM.TotalAmount as SubTotal,TotalTax,PayableAmount as TotalAmount,(
		Select 'http://psmnj.a1whm.com/'+PM.ImageUrl as ImageUrl,PM.ProductName,UM.UnitType,OIM.RequiredQty,UnitPrice,NetPrice as Total from OrderItemMaster  as OIM
		INNER JOIN ProductMaster as PM on
		OIM.ProductAutoId=PM.AutoId
		INNER JOIN UNitMaster as UM on
		UM.AutoId=OIM.UnitTypeAutoId
		Where OrderAutoid=OM.AutoId
		   for json path,include_null_values
		) as ItemList from OrderMaster as OM
		Where OM.AutoId=@OrderAutoId
		for json path,include_null_values
	END
	IF @Opcode=103
    BEGIN
		SELECT top 5 OrderNo,OrderDate,DeliveryDate,PayableAmount,(Select COUNT(*) from OrderItemMaster where OrderAutoId=OM.AutoId) 
		as TotalItem,EM.FirstName+' '+EM.LastName as SalesPerson,CM.CustomerName,ST.ShippingType,SM.StatusType,BA.Address+'-'+BA.City+'-'+S.StateName+'-'+BA.Zipcode as Address FROM OrderMaster AS OM 
		INNER JOIN EmployeeMaster as EM on
		OM.SalesPersonAutoId=EM.AutoId AND EM.EmpType=2
		INNER JOIN CustomerMaster AS CM ON
		OM.CustomerAutoId=CM.AutoId
		INNER JOIN ShippingType AS ST ON
		OM.ShippingType=ST.AutoId
		INNER JOIN StatusMaster as SM ON
		OM.Status=SM.AutoId
		INNER JOIN BillingAddress AS BA ON
		OM.BillAddrAutoId=BA.AutoId 
		INNER JOIN State as S ON
		BA.State=S.AutoId
		Where OM.DeliveryDate is not null and OM.Status=11 
		for json path,include_null_values
	END
END
GO
