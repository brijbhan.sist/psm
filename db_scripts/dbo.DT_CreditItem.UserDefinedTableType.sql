USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedTableType [dbo].[DT_CreditItem]    Script Date: 11/20/2019 5:05:52 AM ******/
DROP PROCEDURE ProcCreditMemo
drop type DT_CreditItem

CREATE TYPE [dbo].[DT_CreditItem] AS TABLE(
	[ProductAutoId] [int] NULL,
	[UnitAutoId] [int] NULL,
	[RequiredQty] [int] NULL,
	[QtyPerUnit] [int] NULL,
	[UnitPrice] [decimal](18, 2) NULL,
	[SRP] [decimal](18, 2) NULL,
	[Tax] [decimal](18, 2) NULL,
	[NetPrice] [decimal](18, 2) NULL,
	OM_MinPrice [decimal](18,2) NOT NULL,
	OM_CostPrice [decimal](18,2) NOT NULL,
	OM_BasePrice [decimal](18,2) NOT NULL
)
GO


