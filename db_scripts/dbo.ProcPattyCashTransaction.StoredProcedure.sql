
alter procedure [dbo].[ProcPattyCashTransaction]        
@OpCode int=Null,        
@EmpType int =null   ,                               
@AutoId  int=Null,                                    
@CurrentBlc decimal(18,2) = null,        
@Amount decimal(12,2) = null,        
@TransactionType varchar(2) = null,        
@AvailableBalance decimal(18,2) = null,        
@Remark varchar(1000) = null,
@Category int=null,
@FromDate datetime = null,        
@ToDate datetime = null, 
@SearchBy varchar(10)=null,
@TransactionDate datetime = null,      
@SecurityKey varchar(10) = null,        
@Who varchar(20) = null,        
@PageIndex INT=1,        
@PageSize INT=10,        
@isException bit out,            
@exceptionMessage varchar(max) out             
as          
begin          
begin try          
  SET @exceptionMessage= 'Success'            
  SET @isException=0         
  if @OpCode = 11        
	BEGIN    
		BEGIN TRY                          
			BEGIN TRANSACTION        
			set @CurrentBlc = (select top 1 CurrentBalance from PattyCashMaster) 
			DECLARE @CHECK INT=0        
			if @TransactionType = 'CR'        
			begin        
				SET @AvailableBalance = @CurrentBlc + @Amount            
			end        
			else if @TransactionType = 'DR'        
			begin        
				SET @AvailableBalance = @CurrentBlc - @Amount          
				if @CurrentBlc < @Amount        
				begin        
					SET @CHECK=1        
				end        
			end        
			IF(@CHECK=0)        
			BEGIN        
				insert into [dbo].[PattyCashLogMaster]([TransactionType],[Category],[TransactionAmount],  [Remark], [AutoIdCreatedBy], [CreatedDate],[AutoIdUpdatedBy],[UpdatedDate],TransactionDate)        
				values (@TransactionType,@Category,@Amount,@Remark,@Who,GETDATE(),@Who,GETDATE(),@TransactionDate)        
				update PattyCashMaster set  UpdatedBy = @Who,UpdatedDate = GETDATE()        
			END        
			ELSE        
			BEGIN        
				SET @isException=1                    
				SET @exceptionMessage='Not sufficient balance !!'        
			END        
			COMMIT TRANSACTION                 
		END TRY                   
		BEGIN CATCH                    
			ROLLBACK TRANSACTION               
			SET @isException=1                    
			SET @exceptionMessage='Oops! Something went wrong.Please try later.'                  
		END CATCH          
	END   
  if @OpCode = 21        
   BEGIN    
	BEGIN TRY                           
		BEGIN TRANSACTION         
		set @CurrentBlc = (select top 1 CurrentBalance from PattyCashMaster)
		DECLARE @CHECK1 INT=0        
		if @TransactionType = 'CR'        
		begin        
			SET @AvailableBalance = @CurrentBlc + @Amount            
		end        
		else if @TransactionType = 'DR'        
		begin        
			SET @AvailableBalance = @CurrentBlc - @Amount          
			if @CurrentBlc < @Amount        
			begin        
				SET @CHECK1=1        
			end        
		end        
		IF(@CHECK1=0)        
		BEGIN        
			update   PattyCashLogMaster    
			set TransactionAmount=@Amount,Category=@Category,Remark=@Remark,AutoIdUpdatedBy=@Who,UpdatedDate=GETDATE() where AutoId=@AutoId    
			update PattyCashMaster set  UpdatedBy = @Who,UpdatedDate = GETDATE()        
		END        
		ELSE        
		BEGIN        
			SET @isException=1                    
			SET @exceptionMessage='Not sufficient balance !!'        
		END        
		COMMIT TRANSACTION                    
	END TRY                   
	BEGIN CATCH                    
		ROLLBACK TRANSACTION                    
		SET @isException=1                    
		SET @exceptionMessage='Oops! Something went wrong.Please try later.'                   
	END CATCH          
   END    
  if @OpCode = 41        
  begin        
       select * from PattyCashMaster        
  end   
  if @OpCode = 42        
  begin     
   select ROW_NUMBER() over(order by pcl.TransactionDate) as RowNumber,pcl.AutoId,FORMAT(pcl.TransactionDate,'MM/dd/yyyy hh:mm tt') as TransactionDate, CurrentBalance,(case when TransactionType = 'CR' then 'Add' else 'Withdraw' end) as TransactionType,
   TransactionAmount,pccm.CategoryName as Category,AvailableBalance,isnull(ReferenceId,0) as ReferenceId,Remark,(em.FirstName + ' ' + em.LastName) as username into #Result from PattyCashLogMaster pcl    
   inner join EmployeeMaster em on em.AutoId = pcl.AutoIdCreatedBy 
   left join Pattycashcategorymaster as pccm on pcl.Category=pccm.AutoId
   where 1=1
   and (@Category is null or @Category=0 or pcl.Category = @Category )
   and (@TransactionType is null or @TransactionType='0' or pcl.TransactionType=@TransactionType)
   and (ISNULL(@SearchBy,'0')='0'  
   OR (@SearchBy ='=' AND TransactionAmount=@Amount)
   OR (@SearchBy ='>' AND TransactionAmount>@Amount)     
   OR (@SearchBy ='<' AND TransactionAmount<@Amount) 
   OR (@SearchBy ='>=' AND TransactionAmount>=@Amount) 
   OR (@SearchBy ='<=' AND TransactionAmount<=@Amount))   
   
   and (@FromDate is null or @FromDate = '' or @Todate is null or @Todate = '' or                                                                                        
    (CONVERT(date,pcl.TransactionDate)  between CONVERT(date,@FromDate) and CONVERT(date,@Todate)))   
      
   SELECT top 1 0 ,AutoId,TransactionDate,CurrentBalance,TransactionType,TransactionAmount,Category,AvailableBalance,ReferenceId,Remark,username FROM #Result    
   WHERE @PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)    
   UNION    
   SELECT * FROM #Result    
   WHERE @PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)    
    
   SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize=0 then COUNT(RowNumber) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Result    
       
  end   
  if @OpCode = 43        
  begin        
   select * from TBL_MST_SecurityMaster where SecurityValue = @SecurityKey and SecurityType = @EmpType and typeEvent = 3        
  end   
  if @OpCode = 44        
  begin        
   select FORMAT(pcl.TransactionDate,'MM/dd/yyyy hh:mm tt') as TransactionDate, CurrentBalance,(case when TransactionType = 'CR' then
   'Add' else 'Withdraw' end) as TransactionType,(Select CategoryName from Pattycashcategorymaster where AutoId=Category) as Category,TransactionAmount,AvailableBalance,Remark,(em.FirstName + ' ' + em.LastName
  )       
   as username from PattyCashLogMaster pcl        
   inner join EmployeeMaster em on em.AutoId = pcl.AutoIdCreatedBy        
   where 1=1  
   
    and (@FromDate is null or @FromDate = '' or @Todate is null or @Todate = '' or                                                                                        
    (CONVERT(date,pcl.TransactionDate)  between CONVERT(date,@FromDate) and CONVERT(date,@Todate)))  

   order by pcl.TransactionDate        
  end  
  IF @OpCode=45    
 BEGIN    
   select TransactionType,ReferenceId,TransactionAmount,Category,AvailableBalance,FORMAT(TransactionDate,'MM/dd/yyyy') as TransactionDate,FORMAT(TransactionDate,'hh:mm tt') as TransactionTime,Remark from PattyCashLogMaster where AutoId=@AutoId    
   select * from PattyCashMaster    
 END  
 IF @OpCode=46    
 BEGIN    
   select  AutoId,CategoryName  from Pattycashcategorymaster Where IsDefault=0  order by CategoryName
   for json path
 END  
end try          
begin catch          
 SET @isException=1          
 SET @exceptionMessage='Oops! Something went wrong.Please try later.'          
end catch          
end 
GO
