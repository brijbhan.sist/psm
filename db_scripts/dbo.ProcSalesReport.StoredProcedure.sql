ALTER PROCEDURE [dbo].[ProcSalesReport]                                                                    
@Opcode INT=NULL,                                                                    
@CustomerAutoId INT=NULL,                                                                     
@CustomerType int=null,                                                                            
@ProductAutoId INT=NULL,                                 
@ProductAutoIdSring varchar(200)=NULL,                                
@BrandAutoIdSring varchar(200)=NULL,                                                                   
@CategoryAutoId  INT=NULL,                                                                    
@SubCategoryId INT=NULL,                                                                    
@EmpAutoId  INT=NULL,                                                                    
@DriverAutoId INT=null,                                                                    
@OrderStatus INT=NULL,                                                
@SalesPersonAutoId int =null,                                                              
@PaymentStatus VARCHAR(100)=NULL,                                                                    
@SalesPerson  VARCHAR(500)=NULL,                                                         
@FromDate date =NULL,                                                                    
@ToDate date =NULL,                             
@CloseOrderFromDate date =NULL,                                                                    
@CloseOrderToDate date =NULL,     
@OrdFromDate date =NULL,                                                                    
@OrdToDate date =NULL,   
@SearchBy INT=NULL,  
@PageIndex INT=1,                                                                    
@PageSize INT=10,                                                                    
@RecordCount INT=null,                                                                    
@isException bit out,                                                                    
@exceptionMessage varchar(max) out                                                                    
AS                                                                    
BEGIN                                                                     
 BEGIN TRY                                                                    
  Set @isException=0                                                                    
  Set @exceptionMessage='Success'                                                                    
                                                                    
  IF @Opcode=49                                                                    
  BEGIN                                                                    
  SELECT AutoId,CategoryName from CategoryMaster where Status=1  order by CategoryName ASC                                                                  
  SELECT AutoId,CustomerType from CustomerType   order by CustomerType ASC                                                                  
  END                                                      
 ELSE IF @Opcode=57                                                                    
  BEGIN                                                                    
  SELECT AutoId,CustomerType from CustomerType order by CustomerType ASC                                             
  Select em.AutoId,(em.FirstName + ' ' + em.LastName) as SalesPerson from EmployeeMaster em                                           
  inner join EmployeeTypeMaster etm on etm.AutoId = em.EmpType                                          
  where EmpType = 2 and Status=1 order by SalesPerson ASC                                                      
 END                                                                  
  ELSE IF @Opcode=50                                                                  
  BEGIN                                                                  
  SELECT AutoId AS CustomerAutoId,CustomerId+' '+CustomerName as CustomerName FROM CustomerMaster   WHERE            
  status=1 and             
   (CustomerType=@CustomerType                                       
  OR ISNULL(@CustomerType,0)=0) and (SalesPersonAutoId=@SalesPersonAutoId or ISNULL(@SalesPersonAutoId,0)=0) order by replace(CustomerId+' '+CustomerName,' ','') ASC                                                               
  END                                 
  ELSE IF @Opcode=51               
  BEGIN                                                                    
    SELECT AutoId,SubcategoryName from SubCategoryMaster where (isnull(@CategoryAutoId,0)=0 or CategoryAutoId=@CategoryAutoId )and Status=1                           
    order by SubcategoryName ASC      
        
    select AutoId, CONVERT(varchar(50), ProductId) + ' - ' + ProductName as ProductName from ProductMaster    
  Where ISNULL(@CategoryAutoId,0)=0 or CategoryAutoId=@CategoryAutoId    
  order by replace(ProductName,' ','') asc                                                                 
  END                                                                 
  ELSE                                                                    
  IF @Opcode=41                                                                    
  BEGIN                                 
    SELECT AutoId AS EmpAutoId,FirstName + ' ' + LastName as EmpName FROM EmployeeMaster where EmpType=2 and Status=1 order by EmpName ASC                                                                
    SELECT AutoId,StatusType AS StatusName FROM StatusMaster WHERE Category='OrderMaster' order by StatusName ASC                                  
    select AutoId, BrandName from BrandMaster where status = 1   order by BrandName                                                           
  END                                                                    
  ELSE If @Opcode=42                                                               
  BEGIN                                                                    
  SELECT ROW_NUMBER() OVER(ORDER BY [CustomerName]) AS RowNumber, * INTO #Results FROM                                                                    
  (                                             
   select CustomerName,CONVERT(varchar(10),OrderDate,101) as OrderDate,OrderNo,do.PayableAmount as TotalOrderAmount,                                                                    
   do.AmtPaid as TotalPaid,(isnull(do.PayableAmount,0.00)-isnull(do.AmtPaid,0.00)) as AmtDue from OrderMaster as om                                                                    
   inner join DeliveredOrders as do on om.AutoId=do.OrderAutoId                                            
   inner join  CustomerMaster as cm on cm.AutoId=om.CustomerAutoId                                                                    
   where                                  
   om.Status=11 and                                                               
   (isnull(do.PayableAmount,0.00)-isnull(do.AmtPaid,0.00))> 0                                                   
   AND (ISNULL(@CustomerAutoId,0)=0 or cm.AutoId=@CustomerAutoId)                                             
   AND(ISNULL(@CustomerType,0)=0 or cm.CustomerType=@CustomerType)                                    
   and (isnull(@SalesPersonAutoId,0)=0 or cm.SalesPersonAutoId=@SalesPersonAutoId)                                                                  
   and (@FromDate is null or @ToDate is null or (CONVERT(date,OrderDate)                                    
   between convert(date,@FromDate) and CONVERT(date,@ToDate)))                                                                    
                                                                        
  ) AS t ORDER BY [CustomerName]                                                                    
                                                                          
  SELECT COUNT(CustomerName) AS RecordCount, case when @PageSize=0 then COUNT(CustomerName) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Results                                                                     
  SELECT * FROM #Results                                                                   
  WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))                                                                   
  SELECT ISNULL(SUM(TotalOrderAmount),0.00) AS TotalOrderAmount,ISNULL(SUM(TotalPaid),0.00) AS TotalPaid,                                                        
  ISNULL(SUM(AmtDue),0.00) AS AmtDue FROM #Results                                                                           
  END                                                       
  ELSE  If @Opcode=43                                                                     
  BEGIN                                                                    
                                                              
    SELECT ROW_NUMBER() OVER(ORDER BY [CustomerName]) AS RowNumber, * INTO #Results43 FROM                                                                    
    (                                                                    
    select CustomerName,COUNT(CustomerName) as NoofOrder,isnull(sum(do.PayableAmount),0.00) as TotalOrderAmount,                                                                    
    isnull(sum(do.AmtPaid),0.00) as TotalPaid,sum((isnull(do.PayableAmount,0.00)-isnull(do.AmtPaid,0.00))) as AmtDue from OrderMaster as om                                                                    
    inner join DeliveredOrders as do on om.AutoId=do.OrderAutoId                                                       
    inner join  CustomerMaster as cm on cm.AutoId=om.CustomerAutoId                                                                    
    where                       
   om.Status=11 and                                                             
  (isnull(do.PayableAmount,0.00)-isnull(do.AmtPaid,0.00))> 0                                                   
  AND (ISNULL(@CustomerAutoId,0)=0 or cm.AutoId=@CustomerAutoId)                                    
  AND (isnull(@SalesPersonAutoId,0)=0 or cm.SalesPersonAutoId=@SalesPersonAutoId)                                                   
  AND(ISNULL(@CustomerType,0)=0 or cm.CustomerType=@CustomerType)                                                                        
  group by CustomerName                                             
                  ) AS t ORDER BY [CustomerName]                                                                                                               
    SELECT COUNT(CustomerName) AS RecordCount, case when @PageSize=0 then COUNT(CustomerName) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Results43                                                                    
    SELECT * FROM #Results43                                                                    
    WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))                                                                          
                          
 SELECT ISNULL(SUM(TotalOrderAmount),0.00) AS TotalOrderAmount,ISNULL(SUM(TotalPaid),0.00) AS TotalPaid, isnull(Sum(NoofOrder),0) as NoofOrder,                                                                   
    ISNULL(SUM(AmtDue),0.00) AS AmtDue FROM #Results43                                                                     
  END                                                                    
                                           
ELSE  If @Opcode=44                                                                  
  BEGIN                                                                  
 select om.AutoID,CustomerAutoId,OrderDate into #OrderAutoid from OrderMaster as om              
 inner join  CustomerMaster as cm on cm.AutoId=om.CustomerAutoId where               
 om.status=11                
 AND (ISNULL(@CustomerAutoId,0)=0 or cm.AutoId=@CustomerAutoId)                                                 
 AND(ISNULL(@CustomerType,0)=0 or cm.CustomerType=@CustomerType)                                     
 and (isnull(@SalesPersonAutoId,0)=0 or cm.SalesPersonAutoId=@SalesPersonAutoId)              
                                                            
  select ROW_NUMBER() over(order by CustomerName) as RowNumber, CustomerName,SUM(isnull([0-30 Days],0)) as Days30, sum([31-60 Days]) as Days60, sum([61-90 Days]) as Days90 ,sum([90-Above]) as Above90,COUNT(1) as NoofOrder,sum([Order Total]) as OrderTotal
 , sum(AmtDue) as AmtDue,sum(TotalPaid) as TotalPaid into #Result42 from (                                          
  select cm.CustomerName,                                           
  (                                          
  CASE                                
  WHEN DATEDIFF(DD,OrderDate,GETDATE())<=30 THEN do.AmtDue else 0 end                                           
  )                                          
  as [0-30 Days],                                    (                                          
  CASE                                                                
  WHEN DATEDIFF(DD,OrderDate,GETDATE())between 31 and 60 THEN do.AmtDue else 0 end             
  )                                          
  as [31-60 Days],                                          
  (                                          
  CASE                                                                
  WHEN DATEDIFF(DD,OrderDate,GETDATE())between 61 and 90 THEN do.AmtDue else 0 end                                           
  )                                          
  as [61-90 Days]                                          
  ,                                          
  (                                           
  CASE                                                                
  WHEN DATEDIFF(DD,OrderDate,GETDATE())> 90 THEN do.AmtDue else 0 end                                           
  )                                          
  as [90-Above],                                          
  isnull((do.PayableAmount),0.00) as [Order Total],                                          
  isnull((do.AmtPaid),0.00) as TotalPaid,((isnull(do.PayableAmount,0.00)-isnull(do.AmtPaid,0.00))) as AmtDue                                          
  from  #OrderAutoid  as do1                                                                    
  inner join  DeliveredOrders as do on do1.AutoId=do.OrderAutoId              
  inner join  CustomerMaster as cm on cm.AutoId=do1.CustomerAutoId                                                     
  where                                             
  (isnull(do.AmtDue,0.00))> 0                                          
                                          
  ) as t                                           
  group by  CustomerName                                                  
                                           
  SELECT * FROM #Result42                                
  WHERE @PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)                                           
  SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize=0 then COUNT(RowNumber) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Result42                                                      
  Select Sum(Days30) as Days30,Sum(Days60) as Days60,Sum(Days90) as Days90,Sum(Above90) as Above90,                                        
  Sum(NoofOrder) as NoofOrder,Sum(OrderTotal)as OrderTotal,sum(AmtDue) as AmtDue,sum(TotalPaid) as TotalPaid from  #Result42                                                                                                  
  END                                       
  ELSE IF @Opcode=45                                                                    
  BEGIN                                                       
   SELECT AutoId,FirstName+' '+ISNULL(LASTNAME,'') AS EmployeeName  FROM EmployeeMaster WHERE EmpType IN (2) and Status=1 order by EmployeeName ASC                                  
                                                                       
   SELECT AutoId,FirstName+' '+ISNULL(LASTNAME,'') AS EmployeeName  FROM EmployeeMaster WHERE EmpType IN (2) and Status=1 order by EmployeeName ASC                                                                   
  END                                                                    
  ELSE IF @Opcode=46                                                                    
  BEGIN                        
  select ROW_NUMBER() over(order by FirstName+' '+ISNULL(lastName,'')) as RowNumber, FirstName+' '+ISNULL(lastName,'') as EmployeeName ,SalesPersonAutoId,SUM(NoofOrders) as NoOfOrders,sum(TotalAmount) as GrandTotal,                                                     
  SUM(NoofPOSOrders) as NoofPOSOrders,SUM(POSTotalAmount) as POSTotalAmount into #Result46 from                                                             
  (                                                            
 SELECT SalesPersonAutoId,SUM(TotalAmount) as TotalAmount,count(TotalAmount) as NoofOrders,0 as NoofPOSOrders,0 as POSTotalAmount                                                           
 from (                                              
  select om.SalesPersonAutoId,                                                            
  (ISNULL(om.TotalAmount,0)-ISNULL((select SUM(ISNULL(cmm.GrandTotal,0)) from CreditMemoMaster as cmm where cmm.OrderAutoId=om.AutoId),0)) as TotalAmount                                                          
  from OrderMaster as om                                                                 
  where om.Status=11 and orderType!=1 and                                                             
  (ISNULL(@SalesPerson,'0')='0' OR om.SalesPersonAutoId                                                                    
in (select * from dbo.fnSplitString(@SalesPerson,','))                                                                    
  )                                                            
  and (@FromDate is null or @ToDate is null or (CONVERT(date,OrderDate)                                                                     
  between convert(date,@FromDate) and CONVERT(date,@ToDate)))                                      
 ) as t                                                              
 group by t.SalesPersonAutoId                                                            
  union                                                            
    SELECT SalesPersonAutoId,0 as NoofOrders,0 as TotalAmount,COUNT(POSTotalAmount) as NoofPOSOrders,SUM(POSTotalAmount) as POSTotalAmount                                                
 from (                                                          
  select om.SalesPersonAutoId,0 as NoofOrders,0 as TotalAmount,                                                            
  (ISNULL(om.TotalAmount,0)-ISNULL((select SUM(ISNULL(cmm.GrandTotal,0)) from CreditMemoMaster as cmm where cmm.OrderAutoId=om.AutoId),0))                          
  as POSTotalAmount from OrderMaster as om                                                                    
  where om.Status=11 and   orderType=1 and                                                             
  (ISNULL(@SalesPerson,'0')='0' OR om.SalesPersonAutoId                                                                    
  in (select * from dbo.fnSplitString(@SalesPerson,','))                                                                   
  )                                                            
  and (@FromDate is null or @ToDate is null or (CONVERT(date,OrderDate)                                                                     
  between convert(date,@FromDate) and CONVERT(date,@ToDate)))                                                               
  ) as t                                     
  group by t.SalesPersonAutoId                                                            
                                                            
  ) as t                                                            
  inner join EmployeeMaster as emp on emp.AutoId=t.SalesPersonAutoId                                                            
  group by SalesPersonAutoId,(FirstName+' '+ISNULL(lastName,''))                                                            
                                    
   SELECT * FROM #Result46                                        
  WHERE @PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)                   
 SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize=0 then COUNT(RowNumber) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Result46                                                         
                                                            
   SELECT ISNULL(sum(GrandTotal),0.00) AS GrandTotal,ISNULL(sum(POSTotalAmount),0.00) AS POSTotalAmount,                      
  ISNULL(sum(NoOfOrders),0) AS NoOfOrders,ISNULL(sum(NoofPOSOrders),0) AS NoofPOSOrders                      
  FROM #Result46                                                                         
  END                                                                      
  ELSE IF @OPCODE=47                                                                    
  BEGIN            
     SELECT  AutoId,CONVERT(VARCHAR(10),ProductId )+' '+ ProductName AS ProductName FROM ProductMaster                                                                    
    WHERE                                                                     
     (isnull(@SubCategoryId,0)=0 or SubcategoryAutoId=@SubCategoryId )and ProductStatus=1             
  order by CONVERT(VARCHAR(10),ProductId )+' '+ ProductName                                                                    
  END                                                                    
  ELSE IF @OPCODE=48                                                                    
  BEGIN                                                   
                                                                    
   SELECT ROW_NUMBER() OVER(ORDER BY  ProductId,CustomerName asc, ProductName asc,DateO asc) AS RowNumber, * into #Results48                                                                      
   FROM                     
   (                                                                    
 select CONVERT(varchar(10),OrderDate,101) as OrderDate,OrderDate as DateO,ProductId,ProductName                    
 ,OrderNo,om.CustomerAutoId,(emp.FirstName+' '+ISNULL(LastName,'')) as SalesPerson,CustomerName,                    
 (ba.Address + ', ' + ba.City + ', ' + st.StateName + ', ' + ba.Zipcode) as BillingAddress,                    
  case when (QtyShip*QtyPerUnit)!=QtyDel then                  
   cast(UnitPrice/QtyPerUnit as decimal(10,2)) else UnitPrice end as UnitPrice,                  
 (case when UnitAutoId=3 or ((QtyShip*QtyPerUnit)!=QtyDel)                  
  then CONVERT(VARCHAR(10),SUM(oim.QtyDel)) else                   
 0  end) as TotalPiece,                                             
 (case when UnitAutoId=2 and ((QtyShip*QtyPerUnit)=QtyDel) then CONVERT(VARCHAR(10),SUM(QtyShip)) else '0' end) as TotalBox,                                                                    
 (case when UnitAutoId=1 and ((QtyShip*QtyPerUnit)=QtyDel) then CONVERT(VARCHAR(10),SUM(QtyShip)) else '0' end) as TotalCase,            
 0 as R_TotalPiece,0 as R_TotalBox,0 as R_TotalCase,                    
 cast (SUM(oim.QtyDel*ISNULL(oim.UnitPrice/QtyPerUnit,0)) as decimal(10,2))  as PayableAmount,                    
 '' as CreditNo,'' as CreditDate,                    
 (SELECT UM.UnitType FROM UnitMaster AS UM  WHERE UM.AutoId=UnitAutoId) AS UnitName                    
 from Delivered_Order_Items as oim                     
 inner join OrderMaster as om on om.AutoId=oim.OrderAutoId  and oim.QtyDel>0                    
 inner join CustomerMaster as cm on cm.AutoId=om.CustomerAutoId                     
 inner join ProductMaster as pm on pm.AutoId=oim.ProductAutoId                                                    
 inner join EmployeeMaster as emp on emp.AutoId=om.SalesPersonAutoId                    
 inner join BillingAddress as ba on om.BillAddrAutoId=ba.AutoId                                                                    
 inner join State as st on ba.State=st.AutoId                           
 where                           
 ((@ProductAutoIdSring='0') or (oim.ProductAutoId in (select * from dbo.fnSplitString(@ProductAutoIdSring,','))))                                   
 AND ((@BrandAutoIdSring='0') or (pm.BrandAutoId in (select * from dbo.fnSplitString(@BrandAutoIdSring,','))))  
 AND (ISNULL(@CustomerAutoId,0) =0 or om.CustomerAutoId=@CustomerAutoId)                                                                    
 AND (ISNULL(@EmpAutoId,0) =0 or OM.SalesPersonAutoId=@EmpAutoId)                                                                    
 AND (ISNULL(@CategoryAutoId,0)=0 OR pm.CategoryAutoId =@CategoryAutoId)                                                                    
 AND (ISNULL(@SubCategoryId,0)=0 OR pm.SubcategoryAutoId =@SubCategoryId)                               
 AND (ISNULL(@CustomerType,0)=0 OR CM.CustomerType=@CustomerType)                    
 and OM.Status=11                                                            

 and (@CloseOrderFromDate is null or @CloseOrderToDate is null or (CONVERT(date,Order_Closed_Date)                                                  
 between convert(date,@CloseOrderFromDate) and CONVERT(date,@CloseOrderToDate))  
 or (CONVERT(date,OrderDate)                                                  
 between convert(date,@CloseOrderFromDate) and CONVERT(date,@CloseOrderToDate)))
    
 group by om.AutoID,OrderDate,ProductId,ProductName,OrderNo,CustomerName,UnitPrice,om.CustomerAutoId,UnitAutoId,                                                                    
 ba.Address ,ba.City,st.StateName ,ba.Zipcode,ProductAutoId,                                                                
 (emp.FirstName+' '+ISNULL(LastName,'')) ,oim.isFreeItem,oim.IsExchange,oim.Tax,QtyPerUnit ,                  
  QtyShip,QtyDel                           
                      
  UNION ALL                
                    
  SELECT '' as OrderDate,CreditDate as DateO,ProductId,ProductName,'' as OrderNo,om.CustomerAutoId,                                                                    
  (emp.FirstName+' '+ISNULL(LastName,'')) as SalesPerson,                                                                    
  CustomerName,'-' as BillingAddress,                                                                    
  UnitPrice,                    
  '0' TotalPiece,                                                                
  '0' as TotalBox,                                                                
  '0' as TotalCase,                                                                  
  (case when UnitAutoId=3 then CONVERT(VARCHAR(10),SUM(ISNULL(oim.AcceptedQty,oim.RequiredQty))) else '0' end) as R_TotalPiece,                                             
  (case when UnitAutoId=2 then CONVERT(VARCHAR(10),SUM(ISNULL(oim.AcceptedQty,oim.RequiredQty))) else '0' end) as R_TotalBox,                                                                    
  (case when UnitAutoId=1 then CONVERT(VARCHAR(10),SUM(ISNULL(oim.AcceptedQty,oim.RequiredQty))) else '0' end) as R_TotalCase,                                                                    
                              
  cast (SUM(ISNULL(oim.AcceptedQty,oim.RequiredQty)*ISNULL(oim.UnitPrice,0)) as decimal(10,2))  as PayableAmount                    
  ,CreditNo,CONVERT(varchar(10),CreditDate,101) as CreditDate,                                                        
  (SELECT UM.UnitType FROM UnitMaster AS UM  WHERE UM.AutoId=UnitAutoId) AS UnitName                                  
  from CreditMemoMaster om                       
  inner join CreditItemMaster as oim on om.CreditAutoId=oim.CreditAutoId                                                                 
  inner join ProductMaster as pm on pm.AutoId=oim.ProductAutoId        
  left join BrandMaster as bm on bm.AutoId = pm.BrandAutoId                           
  inner join CustomerMaster as cm on cm.AutoId=om.CustomerAutoId                                                                    
  left join DeliveredOrders as do on do.OrderAutoId=om.OrderAutoId                                                                    
  inner join EmployeeMaster as emp on emp.AutoId=om.CreatedBy           
  where                           
		((@ProductAutoIdSring='0') or (oim.ProductAutoId in (select * from dbo.fnSplitString(@ProductAutoIdSring,','))))                                   
		AND ((@BrandAutoIdSring='0') or (pm.BrandAutoId in (select * from dbo.fnSplitString(@BrandAutoIdSring,','))))  
		AND (ISNULL(@CustomerAutoId,0) =0 or om.CustomerAutoId=@CustomerAutoId)                                                                    
		AND (ISNULL(@EmpAutoId,0) =0 or cm.SalesPersonAutoId=@EmpAutoId)                                                                    
		AND (ISNULL(@CategoryAutoId,0)=0 OR pm.CategoryAutoId =@CategoryAutoId)                                                                    
		AND (ISNULL(@SubCategoryId,0)=0 OR pm.SubcategoryAutoId =@SubCategoryId)                               
		AND (ISNULL(@CustomerType,0)=0 OR CM.CustomerType=@CustomerType)                  
		and OM.Status=3
		and (@FromDate is null or @ToDate is null or (CONVERT(date,CreditDate)                                      
		between convert(date,@FromDate) and CONVERT(date,@ToDate)))                                                                    
		group by om.CreditAutoId,CreditDate,ProductId,ProductName,CreditNo,CustomerName,UnitPrice,om.CustomerAutoId,UnitAutoId, 
		ProductAutoId,                                                                
		(emp.FirstName+' '+ISNULL(LastName,''))                          
                    
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------                                                                
  ) AS T ORDER BY  ProductId,CustomerName asc, ProductName asc,DateO asc                                                        
                                               
                                                                         
  SELECT * FROM #Results48                                                                    
  WHERE ISNULL(@PageSize,0)=0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)                                    
                                                                       
  SELECT case when ISNULL(@PageSize,0)=0 then 0 else COUNT(OrderDate) end  AS RecordCount, @PageSize AS PageSize, @PageIndex AS PageIndex FROM #Results48                       
                        
  SELECT ISNULL(sum(convert(int,TotalPiece)),0) AS TotalPiece,ISNULL(sum(convert(int,TotalBox)),0) AS TotalBox                      
  ,ISNULL(sum(convert(int,TotalCase)),0) AS TotalCase,ISNULL(sum(PayableAmount),0.00) AS PayableAmount,ISNULL(sum(convert(int,TotalBox)),0) AS TotalBox,                      
  ISNULL(sum(convert(int,TotalCase)),0) AS TotalCase,ISNULL(sum(convert(int,R_TotalPiece)),0) AS R_TotalPiece,ISNULL(sum(convert(int,R_TotalBox)),0) AS R_TotalBox,ISNULL(sum(convert(int,R_TotalCase)),0) AS R_TotalCase                       
  FROM #Results48                                                                  
                                                                       
  END                                                                  
  ELSE IF @OPCODE=52                                                                    
  BEGIN  
		select ROW_NUMBER() over(order by OrderNo) as RowNumber, * into #tbl FROM (           
		SELECT CONVERT(varchar(10),OrderDate,101) as OrderDate,cm.CustomerName,OrderNo,sa.Address+' ' +sa.City+' '+sa.ZipCode 
		as CustomerAddress,                                                                    
		(em.FirstName+' ' +em.LastName) as DriverName,(em1.FirstName+' ' +em1.LastName) as SalesName,om.SalesPersonAutoId,                                                                    
		Stoppage                                                                    
		FROM  ORDERMASTER AS om                                                                     
		inner join customerMaster as cm on cm.autoid=om.CustomerAutoId                                                                       
		inner join EmployeeMaster as em on em.autoid=om.Driver                                                                         
		inner join EmployeeMaster as em1 on em1.autoid=om.SalesPersonAutoId                                                                  
		inner join shippingAddress as sa on sa.Autoid=om.ShipAddrAutoiD WHERE om.STATUS=4                                                                     
		--Order by om.OrderNo asc  
		) AS Tbl 
	
		SELECT * FROM #tbl                                        
		WHERE @PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)    

		SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize=0 then COUNT(RowNumber) else @PageSize end AS PageSize,
		@PageIndex AS PageIndex FROM #tbl    
  END                                                                    
  ELSE IF @OPCODE=53                                                                    
  BEGIN                                                                    
   select ROW_NUMBER() over(order by OrderNo) as RowNumber, * into #Result FROM (                                                                          
    SELECT (convert(varchar(10),OrderDate,101) )OrderDate,OrderNo,cm.CustomerName,sm.StatusType as OrderStatus,                                                                    
    (                                                                    
     CASE                                                                
     when do.OrderAutoId is null then 'N/A'                                                                     
     WHEN ISNULL(DO.AmtPaid,0)=0 THEN 'NOT PAID'                                       
     When om.PayableAmount=isnull(DO.AmtPaid,0.00) then 'PAID'                                                                    
     else 'PARTIAL PAID'                             END                                 
    ) AS PaymentStatus,                                                                    
    om.PayableAmount AS OrderTotal,                                                                    
    isnull(DO.AmtPaid,0.00) AS PaidAmount,                                                                    
    (om.PayableAmount-isnull(DO.AmtPaid,0.00)) AS DueAmount,                                  
    (select em.FirstName +' '+ em.LastName from EmployeeMaster as em where AutoId=om.SalesPersonAutoId) as SalesPerson,                                                                     
    isnull((select em.FirstName +' '+ em.LastName from EmployeeMaster as em where AutoId=om.WarehouseAutoId),'N/A') as WarehouseManager,                                                       
    isnull((select em.FirstName +' '+ em.LastName from EmployeeMaster as em where AutoId=om.PackerAutoId),'N/A') as Packer,                                                                     
    isnull((select em.FirstName +' '+ em.LastName from EmployeeMaster as em where AutoId=om.ManagerAutoId),'N/A') as SalesManager,                                                                   
    isnull((select em.FirstName +' '+ em.LastName from EmployeeMaster as em where AutoId=Driver),'N/A') as Driver,                                                            
    isnull((select em.FirstName +' '+ em.LastName from EmployeeMaster as em where AutoId=AccountAutoId),'N/A') as Account,om.Status                                                                    
    FROM OrderMaster AS OM                                                                     
    inner join customerMaster as cm on cm.autoid=om.CustomerAutoId                                                                     
    left JOIN DeliveredOrders AS DO ON OM.AutoId=DO.OrderAutoId                                                                    
 inner join StatusMaster as sm on sm.AutoId=om.Status and sm.category='OrderMaster'                                                                    
    WHERE om.STATUS!=8 and om.STATUS!=7                                                                    
    and (ISNULL(@OrderStatus,0)=0 or om.Status=@OrderStatus)                                                         
    and     (                                                                     
      (@Todate is null or @FromDate is null ) or                                                                     
      (@Todate is null and convert(date,OrderDate)=@FromDate)                                                                    
      or                                                                
      (@FromDate is null and convert(date,OrderDate)=@Todate)                                           
      or                                                          
       (convert(date,OrderDate) between convert(date,@FromDate) and convert(date,@Todate)  )                
     )                           
   and                      
   (                                                                     
      (@CloseOrderToDate is null or @CloseOrderFromDate is null ) or                                                                     
      (@CloseOrderToDate is null and convert(date,om.Order_Closed_Date)=@CloseOrderFromDate)                                                                    
      or                                                                    
      (@CloseOrderFromDate is null and convert(date,om.Order_Closed_Date)=@CloseOrderToDate)             
      or                                                          
       (convert(date,om.Order_Closed_Date) between convert(date,@CloseOrderFromDate) and convert(date,@CloseOrderToDate)  )                                                                     
     )                      
    and (ISNULL(@DriverAutoId,0)=0 or om.Driver=@DriverAutoId)                                                                     
    and (ISNULL(@EmpAutoId,0)=0 or om.SalesPersonAutoId=@EmpAutoId)                                                                    
   ) AS T                                                                    
   where (ISNULL(@PaymentStatus,'0')='0' or PaymentStatus=@PaymentStatus)                                                                    
   Order by OrderNo                                            
                                           
   SELECT * FROM #Result                                        
  WHERE @PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)                                         
 SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize=0 then COUNT(RowNumber) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Result                          
 select ISNULL(sum(OrderTotal),0.00) as OrderTotal,ISNULL(sum(PaidAmount),0.00) as PaidAmount,ISNULL(sum(DueAmount),0.00) as DueAmount FROM #Result                          
  END                                                                                        
   ELSE IF @Opcode=54                                                                    
   BEGIN                                                                    
                                                                         
    SELECT [AutoId],[StatusType] FROM [dbo].[StatusMaster] WHERE  [Category]='OrderMaster'                                  
AND [AutoId] IN(1,2,3,4,5,6,9,10,11)  order by [StatusType]                                                                   
    SELECT AutoId,(FirstName +' '+ISNULL(LastName,'')) AS EmpName FROM EmployeeMaster WHERE EmpType =2 and status=1 order by Empname ASC                                                                
    SELECT AutoId,(FirstName +' '+ISNULL(LastName,'')) AS EmpName FROM EmployeeMaster WHERE EmpType =5 and status=1  order by Empname ASC                                                               
   END                                    
                                                                    
  ELSE IF @Opcode=55                                                             
  BEGIN                                         
  select ROW_NUMBER() over(order by FirstName+' '+ISNULL(LASTNAME,'')) as RowNumber,                               
  T.*, FirstName+' '+ISNULL(LASTNAME,'') AS EmployeeName into #Result55 FROM                                                                    
   ( SELECT cm.SalesPersonAutoId,COUNT(OM.Autoid) as NoOfOrders                     
   ,SUM(do.PayableAmount) AS GrandTotal FROM OrderMaster AS OM inner join                                                                    
   DeliveredOrders as do on om.AutoId=do.OrderAutoId                                                                    
   inner join CustomerMaster as cm on cm.AutoId=om.CustomerAutoId                                                                    
   WHERE                           
   om.Status=11 and                          
   (ISNULL(@SalesPerson,'0,')='0,' or ISNULL(@SalesPerson,'0')='0' OR cm.SalesPersonAutoId                                                                    
   in (select * from dbo.fnSplitString(@SalesPerson,','))                                                          
   )                                                                    
   and (@FromDate is null or @ToDate is null or (CONVERT(date,OrderDate)                                                                     
   between convert(date,@FromDate) and CONVERT(date,@ToDate)))                                                                    
   GROUP BY cm.SalesPersonAutoId                                                                    
   ) AS T                                                                    
   INNER JOIN EmployeeMaster AS EMP ON EMP.AutoId=T.SalesPersonAutoId                                              
                                           
   SELECT * FROM #Result55                                        
  WHERE @PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)                                         
 SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize=0 then COUNT(RowNumber) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Result55                        
                       
  SELECT ISNULL(SUM(NoOfOrders),0) as NoOfOrders,ISNULL(SUM(GrandTotal),0.00) as GrandTotal FROM #Result55                                                             
  END    
  ELSE IF @Opcode=58                                                                    
   BEGIN                                                                 
		SELECT  AutoId,CONVERT(VARCHAR(10),ProductId )+' '+ ProductName AS ProductName FROM ProductMaster                                                                    
		WHERE                                                                     
		(isnull(@SubCategoryId,0)=0 or SubcategoryAutoId=@SubCategoryId )and 
		--(isnull(@CategoryAutoId,0)=0 or CategoryAutoId=@CategoryAutoId )and
		ProductStatus=1             
		order by CONVERT(VARCHAR(10),ProductId )+' '+ ProductName                                                           
   END                                          
 END TRY                                                    
 BEGIN CATCH                                                                    
  Set @isException=1                                                                    
  Set @exceptionMessage=ERROR_MESSAGE()                                                                    
 END CATCH                                                                    
END 
