USE [psmnj.a1whm.com]
GO
/****** Object:  Table [dbo].[OptimoTimeWindow]    Script Date: 5/10/2020 4:00:10 AM ******/
DROP TABLE [dbo].[OptimoTimeWindow]
GO
/****** Object:  Table [dbo].[OptimoTimeWindow]    Script Date: 5/10/2020 4:00:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OptimoTimeWindow](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[TimeFrom] [time](0) NULL,
	[TimeTo] [time](0) NULL
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[OptimoTimeWindow] ON 
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (1, CAST(N'00:00:00' AS Time), CAST(N'00:00:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (2, CAST(N'00:15:00' AS Time), CAST(N'00:15:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (3, CAST(N'00:30:00' AS Time), CAST(N'00:30:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (4, CAST(N'00:45:00' AS Time), CAST(N'00:45:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (5, CAST(N'01:00:00' AS Time), CAST(N'01:00:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (6, CAST(N'01:15:00' AS Time), CAST(N'01:15:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (7, CAST(N'01:30:00' AS Time), CAST(N'01:30:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (8, CAST(N'01:45:00' AS Time), CAST(N'01:45:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (9, CAST(N'02:00:00' AS Time), CAST(N'02:00:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (10, CAST(N'02:15:00' AS Time), CAST(N'02:15:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (11, CAST(N'02:30:00' AS Time), CAST(N'02:30:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (12, CAST(N'02:45:00' AS Time), CAST(N'02:45:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (13, CAST(N'03:00:00' AS Time), CAST(N'03:00:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (14, CAST(N'03:15:00' AS Time), CAST(N'03:15:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (15, CAST(N'03:30:00' AS Time), CAST(N'03:30:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (16, CAST(N'03:45:00' AS Time), CAST(N'03:45:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (17, CAST(N'04:00:00' AS Time), CAST(N'04:00:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (18, CAST(N'04:15:00' AS Time), CAST(N'04:15:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (19, CAST(N'04:30:00' AS Time), CAST(N'04:30:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (20, CAST(N'04:45:00' AS Time), CAST(N'04:45:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (21, CAST(N'05:00:00' AS Time), CAST(N'05:00:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (22, CAST(N'05:15:00' AS Time), CAST(N'05:15:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (23, CAST(N'05:30:00' AS Time), CAST(N'05:30:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (24, CAST(N'05:45:00' AS Time), CAST(N'05:45:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (25, CAST(N'06:00:00' AS Time), CAST(N'06:00:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (26, CAST(N'06:15:00' AS Time), CAST(N'06:15:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (27, CAST(N'06:30:00' AS Time), CAST(N'06:30:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (28, CAST(N'06:45:00' AS Time), CAST(N'06:45:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (29, CAST(N'07:00:00' AS Time), CAST(N'07:00:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (30, CAST(N'07:15:00' AS Time), CAST(N'07:15:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (31, CAST(N'07:30:00' AS Time), CAST(N'07:30:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (32, CAST(N'07:45:00' AS Time), CAST(N'07:45:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (33, CAST(N'08:00:00' AS Time), CAST(N'08:00:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (34, CAST(N'08:15:00' AS Time), CAST(N'08:15:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (35, CAST(N'08:30:00' AS Time), CAST(N'08:30:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (36, CAST(N'08:45:00' AS Time), CAST(N'08:45:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (37, CAST(N'09:00:00' AS Time), CAST(N'09:00:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (38, CAST(N'09:15:00' AS Time), CAST(N'09:15:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (39, CAST(N'09:30:00' AS Time), CAST(N'09:30:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (40, CAST(N'09:45:00' AS Time), CAST(N'09:45:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (41, CAST(N'10:00:00' AS Time), CAST(N'10:00:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (42, CAST(N'10:15:00' AS Time), CAST(N'10:15:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (43, CAST(N'10:30:00' AS Time), CAST(N'10:30:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (44, CAST(N'10:45:00' AS Time), CAST(N'10:45:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (45, CAST(N'11:00:00' AS Time), CAST(N'11:00:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (46, CAST(N'11:15:00' AS Time), CAST(N'11:15:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (47, CAST(N'11:30:00' AS Time), CAST(N'11:30:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (48, CAST(N'11:45:00' AS Time), CAST(N'11:45:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (49, CAST(N'12:00:00' AS Time), CAST(N'12:00:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (50, CAST(N'12:15:00' AS Time), CAST(N'12:15:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (51, CAST(N'12:30:00' AS Time), CAST(N'12:30:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (52, CAST(N'12:45:00' AS Time), CAST(N'12:45:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (53, CAST(N'13:00:00' AS Time), CAST(N'13:00:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (54, CAST(N'13:15:00' AS Time), CAST(N'13:15:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (55, CAST(N'13:30:00' AS Time), CAST(N'13:30:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (56, CAST(N'13:45:00' AS Time), CAST(N'13:45:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (57, CAST(N'14:00:00' AS Time), CAST(N'14:00:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (58, CAST(N'14:15:00' AS Time), CAST(N'14:15:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (59, CAST(N'14:30:00' AS Time), CAST(N'14:30:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (60, CAST(N'14:45:00' AS Time), CAST(N'14:45:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (61, CAST(N'15:00:00' AS Time), CAST(N'15:00:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (62, CAST(N'15:15:00' AS Time), CAST(N'15:15:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (63, CAST(N'15:30:00' AS Time), CAST(N'15:30:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (64, CAST(N'15:45:00' AS Time), CAST(N'15:45:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (65, CAST(N'16:00:00' AS Time), CAST(N'16:00:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (66, CAST(N'16:15:00' AS Time), CAST(N'16:15:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (67, CAST(N'16:30:00' AS Time), CAST(N'16:30:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (68, CAST(N'16:45:00' AS Time), CAST(N'16:45:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (69, CAST(N'17:00:00' AS Time), CAST(N'17:00:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (70, CAST(N'17:15:00' AS Time), CAST(N'17:15:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (71, CAST(N'17:30:00' AS Time), CAST(N'17:30:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (72, CAST(N'17:45:00' AS Time), CAST(N'17:45:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (73, CAST(N'18:00:00' AS Time), CAST(N'18:00:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (74, CAST(N'18:15:00' AS Time), CAST(N'18:15:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (75, CAST(N'18:30:00' AS Time), CAST(N'18:30:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (76, CAST(N'18:45:00' AS Time), CAST(N'18:45:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (77, CAST(N'19:00:00' AS Time), CAST(N'19:00:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (78, CAST(N'19:15:00' AS Time), CAST(N'19:15:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (79, CAST(N'19:30:00' AS Time), CAST(N'19:30:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (80, CAST(N'19:45:00' AS Time), CAST(N'19:45:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (81, CAST(N'20:00:00' AS Time), CAST(N'20:00:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (82, CAST(N'20:15:00' AS Time), CAST(N'20:15:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (83, CAST(N'20:30:00' AS Time), CAST(N'20:30:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (84, CAST(N'20:45:00' AS Time), CAST(N'20:45:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (85, CAST(N'21:00:00' AS Time), CAST(N'21:00:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (86, CAST(N'21:15:00' AS Time), CAST(N'21:15:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (87, CAST(N'21:30:00' AS Time), CAST(N'21:30:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (88, CAST(N'21:45:00' AS Time), CAST(N'21:45:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (89, CAST(N'22:00:00' AS Time), CAST(N'22:00:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (90, CAST(N'22:15:00' AS Time), CAST(N'22:15:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (91, CAST(N'22:30:00' AS Time), CAST(N'22:30:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (92, CAST(N'22:45:00' AS Time), CAST(N'22:45:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (93, CAST(N'23:00:00' AS Time), CAST(N'23:00:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (94, CAST(N'23:15:00' AS Time), CAST(N'23:15:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (95, CAST(N'23:30:00' AS Time), CAST(N'23:30:00' AS Time))
GO
INSERT [dbo].[OptimoTimeWindow] ([AutoId], [TimeFrom], [TimeTo]) VALUES (96, CAST(N'23:45:00' AS Time), CAST(N'23:45:00' AS Time))
GO
SET IDENTITY_INSERT [dbo].[OptimoTimeWindow] OFF
GO
