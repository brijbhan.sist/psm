
ALTER PROCEDURE [dbo].[ProcPriceLevelList]  
@Opcode int=null,  
@PriceLevelId VARCHAR(12)=NULL,  
@PriceLevelName VARCHAR(50)=NULL,  
@Status INT=NULL,  
@PriceLevelAutoId INT=NULL,  
@CustomerType INT=NULL,  
@isException bit out,  
@exceptionMessage varchar(max) out,  
  
@PageIndex INT = 1,  
@PageSize INT = 10,  
@RecordCount INT =null  
  
AS  
BEGIN  
 BEGIN TRY  
  Set @isException=0  
  Set @exceptionMessage='Success'       
  IF @Opcode = 41  
   BEGIN      
		SELECT ROW_NUMBER() OVER(Order by PriceLevelAutoId desc) AS RowNumber, * into #Result FROM  
		(SELECT (select count(cppl.AutoId) from CustomerPriceLevel cppl where cppl.PriceLevelAutoId=PLM.AutoId) as TotalCustomer, 
		PLM.[AutoId] AS PriceLevelAutoId,EM.FirstName+' '+EM.LastName as CreatedBy,
		[PriceLevelId],[PriceLevelName],SM.[StatusType],Convert(varchar(20),PLM.CreatedDate,101) AS CreatedDate,  
		STUFF((  
		select ' ,'+cm.CustomerName from CustomerPriceLevel as cpl   
		inner join CustomerMaster as cm on cpl.CustomerAutoId=cm.AutoId where cpl.PriceLevelAutoId=PLM.AutoId  
		for xml path('')),1,2,'') as Customer  
		FROM [dbo].[PriceLevelMaster] AS PLM  
		INNER JOIN [dbo].[StatusMaster] As SM ON SM.[AutoId] = PLM.[Status] ANd SM.Category is null
		LEFT JOIN EmployeeMaster AS EM on EM.AutoId=PLM.CreatedBy
		WHERE  
		(@PriceLevelName is null OR @PriceLevelName='' OR [PriceLevelName] LIKE '%' + @PriceLevelName + '%') AND  
		(@Status = 22 OR PLM.Status = @Status) and (@CustomerType = 0 OR PCustomerType = @CustomerType)		
		) AS T  
		SELECT * FROM #Result       
		WHERE ISNULL(@PageSize,0)=0 or RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1  
		if ISNULL(@PageSize,0)!=0
		SELECT COUNT(*) as RecordCount,@PageSize as PageSize,@PageIndex as PageIndex FROM #Result  
		END  
  ELSE IF @Opcode = 42  
   BEGIN  
    SELECT [AutoId],[StatusType] FROM [dbo].[StatusMaster] WHERE [Category] is null order by  [StatusType]
	select AutoId,CustomerType from CustomerType order by  CustomerType
   END  
 END TRY  
 BEGIN CATCH  
  Set @isException=1  
  Set @exceptionMessage=Error_Message()  
 END CATCH  
END  
  
  
  
  
GO
