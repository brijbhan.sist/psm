---PA,NPA,WPA:- State Tax

select pm.ProductId,pm.ProductName,um.UnitType+ ' ['+convert(varchar(10),pd.Qty)+' Pieces]' as Unit,SUM(convert(decimal(10,2),(convert(decimal(10,2),dio.QtyDel)/pd.Qty))) as [SoldQty] from Delivered_Order_Items as dio
inner join OrderMaster as om on om.AutoId=dio.OrderAutoId
inner join CustomerMaster as cm on cm.AutoId=om.CustomerAutoId
inner join ProductMaster as pm on pm.AutoId=dio.ProductAutoId
inner join BrandMaster as bm on pm.BrandAutoId=bm.AutoId
inner join PackingDetails as pd on pd.ProductAutoId=pm.AutoId and pd.UnitType=pm.PackingAutoId
inner join UnitMaster as um on um.AutoId=pd.UnitType
where om.Status=11 and dio.Tax=1
and QtyDel>0
and convert(date,om.OrderDate)> convert(date,getdate()-10)

group by pm.ProductId,pm.ProductName,um.UnitType+ ' ['+convert(varchar(10),pd.Qty)+' Pieces]' ,pd.Qty
order by pm.ProductId