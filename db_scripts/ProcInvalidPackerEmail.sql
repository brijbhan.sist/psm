
 ALTER procedure [dbo].[ProcInvalidPackerEmail]--Exec ProcInvalidPackerEmail
AS
BEGIN
    Declare @html varchar(max)='',@count int,@num int,@tr varchar(max),
	@OrderNo varchar(10),@OrderDate varchar(25),@Status varchar(50),@Packer varchar(50),@WarehouseManager varchar(50),
	@CustomerId varchar(50),@CustomerName varchar(50),@AssignDate varchar(25)

	select  Row_Number() over (order by om.OrderNo) as rownumber, om.OrderNo,Format(om.OrderDate,'MM/dd/yyyy hh:mm tt') as OrderDate,SM.StatusType,
	emp.FirstName+' '+ISNULL(emp.LastName,'') as Packer,emp1.FirstName+' '+ISNULL(emp1.LastName,'') as WarehouseManager,
	CustomerId,CustomerName,Format(ISNULL(om.AssignDate,''),'MM/dd/yyyy hh:mm tt') as AssignDate 
	INTO #Result from  OrderMaster as om
	inner join EmployeeMaster as emp on emp.AutoId=om.PackerAutoId
	inner join EmployeeMaster as emp1 on emp1.AutoId=om.WarehouseAutoId
	INNER JOIN StatusMaster as SM ON SM.AutoId=OM.Status and SM.Category='OrderMaster'
	INNER JOIN CustomerMaster as CM ON CM.AutoId=OM.CustomerAutoId
	where emp.EmpType!=3

	set @html=@html+'
		<table class="table tableCSS" border="1" style="width:100%;border: 1px solid black;border-collapse:collapse;"> 
		<thead>
		<tr>
		   <td style=''text-align:center;''><b>Customer Id</b></td>
			<td style=''text-align:center;''><b>Customer Name</b></td>
			<td style=''text-align:center;''><b>Order No</b></td>
			<td style=''text-align:center;''><b>Order Date</b></td>
			<td style=''text-align:center;''><b>Packer</b></td>
			<td style=''text-align:center;''><b>Assign Date</b></td>
			<td style=''text-align:center;''><b>Warehouse Manager</b></td>
			<td style=''text-align:center;''><b>Status</b></td>
       </tr>
	   </thead>
		<tbody>'
		SET @count= (select count(1) from #Result)
		SET @num=1
		while(@num<=@Count)
		BEGIN 
				select 
				@CustomerId=CustomerId,
				@CustomerName=CustomerName,
				@OrderNo= OrderNo,
				@OrderDate=OrderDate,
				@Packer=Packer,
				@AssignDate=AssignDate,
				@WarehouseManager=WarehouseManager,
				@Status=StatusType
				from #Result where rownumber = @num 
				set @tr ='<tr>' 
					+	'<td style=''text-align:center;''>'+ @CustomerId + '</td>'
					+	'<td style=''text-align:left;''>'+ @CustomerName + '</td>'
					+	'<td style=''text-align:center;''>'+ @OrderNo + '</td>'	
					+	'<td style=''text-align:center;''>'+ @OrderDate + '</td>'
					+	'<td style=''text-align:left;''>'+ @Packer + '</td>'
					+	'<td style=''text-align:center;''>'+ @AssignDate + '</td>'
					+	'<td style=''text-align:left;''>'+ @WarehouseManager + '</td>'
					+	'<td style=''text-align:center;''>'+ @Status + '</td>'
					+ '</tr>'
		set @num = @num + 1
		SET @html=@html+@tr
		END
		SET @html=@html+'</tbody></table>'

	Declare  @Subject varchar(250),@FromName varchar(500),@FromEmailId varchar(500),@Port int,@SMTPServer varchar(150),@smtp_userName varchar(150),
	@Password varchar(50),@SSL  int,@BCCEmailId varchar(max),@ToEmailId varchar(500),@CCEmailId varchar(max)
	set @Subject = 'A1WHM -' +UPPER((select CompanyDistributerName from dbo.CompanyDetails))+' - '+'Invalid Packer' 
	
	SELECT @FromEmailId=FromEmail,@FromName=DisplayName,@smtp_userName=SMTP_USERNAME,@Password=SMTP_PASSWORD,
	@SMTPServer=HOST,@Port=SMTPPORT,@SSL=EnableSsl from 
	EmailServerMaster_AWS
	
	set @BCCEmailId=(select BCCEmail from Tbl_ReportEmailReceiver where Autoid=13);
	set @ToEmailId=(select ToEmail from Tbl_ReportEmailReceiver where Autoid=13)
	SET @CCEmailId=(select CCEmail from Tbl_ReportEmailReceiver where Autoid=13)

	IF(select count(1) from #Result )>0
	BEGIN
		 EXEC [Emailing_DB].[dbo].[ProcSubmitEmail] 
		@Opcode=11,
		@FromEmailId =@FromEmailId,
		@FromName = @FromName,
		@smtp_userName=@smtp_userName,
		@Password = @Password,
		@SMTPServer = @SMTPServer,
		@Port =@Port,
		@SSL =@SSL,
		@ToEmailId =@ToEmailId,
		@CCEmailId =@CCEmailId,
		@BCCEmailId =@BCCEmailId,  
		@Subject =@Subject,
		@EmailBody = @html,
		@SentDate ='',
		@Status =0,
		@SourceApp ='PSM',
		@SubUrl ='Wrong Packer', 
		@isException=0,
		@exceptionMessage=''  
	END
END

