ALTER PROCEDURE [dbo].[ProcDraftPackingMaster_All]                   
@ProductId varchar(10)=NULL  
                
AS                  
BEGIN               
        
	Declare @RequestLocation varchar(50),@sql nvarchar(max) 
	Set @RequestLocation=(Select ProductLocation from DraftProductMaster where ProductId = @ProductId)              
    declare @DraftProductAutoId int=(select AutoId from[dbo].[DraftProductMaster] WHERE ProductId = @ProductId)     
     DECLARE @AutoId INT   
     --declare @BarCode varchar(25)=(SELECT CONVERT(VARCHAR(50),CONVERT(NUMERIC(12,0),RAND() * 99999999999)))  
  -- psmct location  
  if(DB_Name() in ('psmnj.a1whm.com'))  
  begin  
   SET @AutoId =(select AutoId from [psmct.a1whm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId)  

   insert into [psmct.a1whm.com].[dbo].[PackingDetails](UnitType,Qty,MinPrice,CostPrice,Price,ProductAutoId,WHminPrice,
   UpdateDate,EligibleforFree,CreateBy,CreateDate,ModifiedBy,PackingStatus)   
   
   select  UnitType,Qty,MinPrice,CostPrice,Price,@AutoId,WHminPrice,UpdateDate,EligibleforFree,NULL,GETDATE(),NULL   ,
   1
   from DraftPackingDetails where ProductAutoId=@DraftProductAutoId    
              
   insert into [psmct.a1whm.com].[dbo].[ItemBarcode](ProductAutoId,UnitAutoId,Barcode,BarcodeType)                
   select  @AutoId,UnitType,  
   CASE WHEN ISNULL(LatestBarcode,'')='' then (select dbo.FN_GenerateUPCAbarcode(dpd.UnitType))  else LatestBarcode end,
   CASE WHEN ISNULL(LatestBarcode,'')='' then 1  else 2 end                 
   from [dbo].DraftPackingDetails as dpd where ProductAutoId =@DraftProductAutoId   
  
   -- psmnj location  
  
    SET @AutoId=(select AutoId from [psmnj.a1whm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId) 
	
   insert into [psmnj.a1whm.com].[dbo].[PackingDetails](  
        UnitType,Qty,MinPrice,CostPrice,Price,ProductAutoId,WHminPrice,UpdateDate,EligibleforFree,CreateBy,
		CreateDate,ModifiedBy,PackingStatus,Rack,Section,Row,BoxNo)                
   select  UnitType,Qty,MinPrice,CostPrice,Price,@AutoId,WHminPrice,UpdateDate,EligibleforFree,NULL,GETDATE(),NULL,1
   ,Rack,Section,Row,BoxNo
   from DraftPackingDetails where ProductAutoId =@DraftProductAutoId   
              
   insert into [psmnj.a1whm.com].[dbo].[ItemBarcode](ProductAutoId,UnitAutoId,Barcode,BarcodeType)                
   select  @AutoId,UnitType,  
   CASE WHEN ISNULL(LatestBarcode,'')='' then (select dbo.FN_GenerateUPCAbarcode(UnitType)) else LatestBarcode end,
   CASE WHEN ISNULL(LatestBarcode,'')='' then 1  else 2 end 
   from [dbo].DraftPackingDetails where ProductAutoId = @DraftProductAutoId   
  
   -- psmpa location  
  
    SET @AutoId=(select AutoId from [psmpa.a1whm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId)  
   insert into [psmpa.a1whm.com].[dbo].[PackingDetails](UnitType,Qty,MinPrice,CostPrice,Price,ProductAutoId,WHminPrice,UpdateDate,EligibleforFree,CreateBy,CreateDate,ModifiedBy,PackingStatus)                
   select  UnitType,Qty,MinPrice,CostPrice,Price,@AutoId,WHminPrice,UpdateDate,EligibleforFree,NULL,GETDATE(),NULL,1
   from DraftPackingDetails where ProductAutoId =@DraftProductAutoId   
              
   insert into [psmpa.a1whm.com].[dbo].[ItemBarcode](ProductAutoId,UnitAutoId,Barcode)                
   select  @AutoId,UnitType,  
   CASE WHEN ISNULL(LatestBarcode,'')='' then (select dbo.FN_GenerateUPCAbarcode(UnitType))  
   else LatestBarcode end              
   from [dbo].DraftPackingDetails where ProductAutoId =@DraftProductAutoId   
  
   --psmnpa location  
  
    SET @AutoId=(select AutoId from [psmnpa.a1whm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId)  
   insert into [psmnpa.a1whm.com].[dbo].[PackingDetails](UnitType,Qty,MinPrice,CostPrice,Price,ProductAutoId,WHminPrice,UpdateDate,EligibleforFree,CreateBy,CreateDate,ModifiedBy,PackingStatus)                
   select  UnitType,Qty,MinPrice,CostPrice,Price,@AutoId,WHminPrice,UpdateDate,EligibleforFree,NULL,GETDATE(),NULL,1                 
   from DraftPackingDetails where ProductAutoId =@DraftProductAutoId   
              
   insert into [psmnpa.a1whm.com].[dbo].[ItemBarcode](ProductAutoId,UnitAutoId,Barcode,barcodeType)                
   select  @AutoId,UnitType,  
   CASE WHEN ISNULL(LatestBarcode,'')='' then (select dbo.FN_GenerateUPCAbarcode(UnitType)) else LatestBarcode end      ,
   CASE WHEN ISNULL(LatestBarcode,'')='' then 1  else 2 end 
   from [dbo].DraftPackingDetails where ProductAutoId =@DraftProductAutoId    
  
   --psmwpa location  
  
    SET @AutoId=(select AutoId from [psmwpa.a1whm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId)  
   insert into [psmwpa.a1whm.com].[dbo].[PackingDetails](UnitType,Qty,MinPrice,CostPrice,Price,ProductAutoId,WHminPrice,UpdateDate,EligibleforFree,CreateBy,CreateDate,ModifiedBy,PackingStatus)                
   select  UnitType,Qty,MinPrice,CostPrice,Price,@AutoId,WHminPrice,UpdateDate,EligibleforFree,NULL,GETDATE(),NULL,1                 
   from DraftPackingDetails where ProductAutoId =@DraftProductAutoId   
              
   insert into [psmwpa.a1whm.com].[dbo].[ItemBarcode](ProductAutoId,UnitAutoId,Barcode)                
   select  @AutoId,UnitType,  
   CASE WHEN ISNULL(LatestBarcode,'')='' then (select dbo.FN_GenerateUPCAbarcode(UnitType)) 
   else LatestBarcode end               
   from [dbo].DraftPackingDetails where ProductAutoId =@DraftProductAutoId 
   
    --psmny location  
  
    SET @AutoId=(select AutoId from [psmny.a1whm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId)  
   insert into [psmny.a1whm.com].[dbo].[PackingDetails](UnitType,Qty,MinPrice,CostPrice,Price,ProductAutoId,WHminPrice,UpdateDate,EligibleforFree,CreateBy,CreateDate,ModifiedBy,PackingStatus)                
   select  UnitType,Qty,MinPrice,CostPrice,Price,@AutoId,WHminPrice,UpdateDate,EligibleforFree,NULL,GETDATE(),NULL   ,1              
   from DraftPackingDetails where ProductAutoId =@DraftProductAutoId   
              
   insert into [psmny.a1whm.com].[dbo].[ItemBarcode](ProductAutoId,UnitAutoId,Barcode,BarcodeType)                
   select  @AutoId,UnitType,  
   CASE WHEN ISNULL(LatestBarcode,'')='' then (select dbo.FN_GenerateUPCAbarcode(UnitType))  else LatestBarcode end               ,
   CASE WHEN ISNULL(LatestBarcode,'')='' then 1  else 2 end 
   from [dbo].DraftPackingDetails where ProductAutoId =@DraftProductAutoId 

    --easywhm location  
  
    SET @AutoId=(select AutoId from [psmnj.easywhm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId)  
   insert into [psmnj.easywhm.com].[dbo].[PackingDetails](UnitType,Qty,MinPrice,CostPrice,Price,ProductAutoId,
   WHminPrice,UpdateDate,EligibleforFree,CreateBy,CreateDate,ModifiedBy,PackingStatus,Rack,Section,Row,BoxNo)                
   select  UnitType,Qty,MinPrice,CostPrice,Price,@AutoId,WHminPrice,UpdateDate,EligibleforFree,NULL,GETDATE(),NULL,1
   ,Rack,Section,Row,BoxNo
   from DraftPackingDetails where ProductAutoId =@DraftProductAutoId   
              
   insert into [psmnj.easywhm.com].[dbo].[ItemBarcode](ProductAutoId,UnitAutoId,Barcode)                
   select  @AutoId,UnitType,  
   CASE WHEN ISNULL(LatestBarcode,'')='' then (select dbo.FN_GenerateUPCAbarcode(UnitType))
   else LatestBarcode end               
   from [dbo].DraftPackingDetails where ProductAutoId =@DraftProductAutoId 

   --easywhm PA location  
  
    SET @AutoId=(select AutoId from [psmpa.easywhm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId)  
   insert into [psmpa.easywhm.com].[dbo].[PackingDetails](UnitType,Qty,MinPrice,CostPrice,Price,ProductAutoId,WHminPrice,UpdateDate,EligibleforFree,CreateBy,CreateDate,ModifiedBy)                
   select  UnitType,Qty,MinPrice,CostPrice,Price,@AutoId,WHminPrice,UpdateDate,EligibleforFree,NULL,GETDATE(),NULL                 
   from DraftPackingDetails where ProductAutoId =@DraftProductAutoId   
              
   insert into [psmpa.easywhm.com].[dbo].[ItemBarcode](ProductAutoId,UnitAutoId,Barcode)                
   select  @AutoId,UnitType,  
   CASE WHEN ISNULL(LatestBarcode,'')='' then (select dbo.FN_GenerateUPCAbarcode(UnitType))
   else LatestBarcode end               
   from [dbo].DraftPackingDetails where ProductAutoId =@DraftProductAutoId 

    --easywhm PA location  
  
    SET @AutoId=(select AutoId from [psmnpa.easywhm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId)  
   insert into [psmnpa.easywhm.com].[dbo].[PackingDetails](UnitType,Qty,MinPrice,CostPrice,Price,ProductAutoId,WHminPrice,UpdateDate,EligibleforFree,CreateBy,CreateDate,ModifiedBy)                
   select  UnitType,Qty,MinPrice,CostPrice,Price,@AutoId,WHminPrice,UpdateDate,EligibleforFree,NULL,GETDATE(),NULL                 
   from DraftPackingDetails where ProductAutoId =@DraftProductAutoId   
              
   insert into [psmnpa.easywhm.com].[dbo].[ItemBarcode](ProductAutoId,UnitAutoId,Barcode)                
   select  @AutoId,UnitType,  
   CASE WHEN ISNULL(LatestBarcode,'')='' then (select dbo.FN_GenerateUPCAbarcode(UnitType))
   else LatestBarcode end               
   from [dbo].DraftPackingDetails where ProductAutoId =@DraftProductAutoId 

   --easywhm WPA location  
  
    SET @AutoId=(select AutoId from [psmwpa.easywhm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId)  
   insert into [psmwpa.easywhm.com].[dbo].[PackingDetails](UnitType,Qty,MinPrice,CostPrice,Price,ProductAutoId,WHminPrice,UpdateDate,EligibleforFree,CreateBy,CreateDate,ModifiedBy)                
   select  UnitType,Qty,MinPrice,CostPrice,Price,@AutoId,WHminPrice,UpdateDate,EligibleforFree,NULL,GETDATE(),NULL                 
   from DraftPackingDetails where ProductAutoId =@DraftProductAutoId   
              
   insert into [psmwpa.easywhm.com].[dbo].[ItemBarcode](ProductAutoId,UnitAutoId,Barcode)                
   select  @AutoId,UnitType,  
   CASE WHEN ISNULL(LatestBarcode,'')='' then (select dbo.FN_GenerateUPCAbarcode(UnitType))
   else LatestBarcode end               
   from [dbo].DraftPackingDetails where ProductAutoId =@DraftProductAutoId 

   --easywhm CT location  
  
    SET @AutoId=(select AutoId from [psmct.easywhm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId)  
   insert into [psmct.easywhm.com].[dbo].[PackingDetails](UnitType,Qty,MinPrice,CostPrice,Price,ProductAutoId,WHminPrice,UpdateDate,EligibleforFree,CreateBy,CreateDate,ModifiedBy)                
   select  UnitType,Qty,MinPrice,CostPrice,Price,@AutoId,WHminPrice,UpdateDate,EligibleforFree,NULL,GETDATE(),NULL                 
   from DraftPackingDetails where ProductAutoId =@DraftProductAutoId   
              
   insert into [psmct.easywhm.com].[dbo].[ItemBarcode](ProductAutoId,UnitAutoId,Barcode)                
   select  @AutoId,UnitType,  
   CASE WHEN ISNULL(LatestBarcode,'')='' then (select dbo.FN_GenerateUPCAbarcode(UnitType))
   else LatestBarcode end               
   from [dbo].DraftPackingDetails where ProductAutoId =@DraftProductAutoId 

    --easywhm demo location  
  
    SET @AutoId=(select AutoId from [psmny.easywhm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId)  
   insert into [psmny.easywhm.com].[dbo].[PackingDetails](UnitType,Qty,MinPrice,CostPrice,Price,ProductAutoId,WHminPrice,UpdateDate,EligibleforFree,CreateBy,CreateDate,ModifiedBy)                
   select  UnitType,Qty,MinPrice,CostPrice,Price,@AutoId,WHminPrice,UpdateDate,EligibleforFree,NULL,GETDATE(),NULL                 
   from DraftPackingDetails where ProductAutoId =@DraftProductAutoId   
              
   insert into [psmny.easywhm.com].[dbo].[ItemBarcode](ProductAutoId,UnitAutoId,Barcode)                
   select  @AutoId,UnitType,  
   CASE WHEN ISNULL(LatestBarcode,'')='' then (select dbo.FN_GenerateUPCAbarcode(UnitType))
   else LatestBarcode end               
   from [dbo].DraftPackingDetails where ProductAutoId =@DraftProductAutoId 
 END  
	 if(DB_Name() NOT IN  ('psmnj.a1whm.com'))  
	 begin  
	   SET @AutoId=(select AutoId from [dbo].[ProductMaster] WHERE ProductId = @ProductId)      
	  insert into [dbo].[PackingDetails](UnitType,Qty,MinPrice,CostPrice,Price,ProductAutoId,WHminPrice,UpdateDate,EligibleforFree,CreateBy,CreateDate,ModifiedBy)                
	  select  UnitType,Qty,MinPrice,CostPrice,Price,@AutoId,WHminPrice,UpdateDate,EligibleforFree,NULL,GETDATE(),NULL                 
	  from DraftPackingDetails where ProductAutoId =@DraftProductAutoId   
              
	  insert into [dbo].[ItemBarcode](ProductAutoId,UnitAutoId,Barcode)                
	  select  @AutoId,UnitType,  
	  CASE WHEN ISNULL(LatestBarcode,'')='' then (select dbo.FN_GenerateUPCAbarcode(UnitType))
	  else LatestBarcode end                
	  from [dbo].DraftPackingDetails where ProductAutoId = @DraftProductAutoId   
  
	 end 
	
	SET @sql='
		UPDATE PD SET 
		PD.Rack=DPD.Rack,PD.Section=DPD.Section,PD.Row=DPD.Row,PD.BoxNo=DPD.BoxNo  
		FROM ['+@RequestLocation+'.a1whm.com].[dbo].PackingDetails AS PD             
		INNER JOIN ['+@RequestLocation+'.a1whm.com].[dbo].productMaster AS PM on PM.autoid=pd.ProductAutoid and pm.productId='+@ProductId+'
		INNER JOIN [dbo].DraftPackingDetails AS DPD ON PD.UnitType=DPD.UnitType 
		WHERE DPD.ProductAutoId='+CONVERT(VARCHAR(50),@DraftProductAutoId)+' 
      
	    UPDATE PD SET 
		PD.Rack=DPD.Rack,PD.Section=DPD.Section,PD.Row=DPD.Row,PD.BoxNo=DPD.BoxNo  
		FROM ['+@RequestLocation+'.easywhm.com].[dbo].PackingDetails AS PD  
		INNER JOIN ['+@RequestLocation+'.easywhm.com].[dbo].productMaster AS PM on PM.autoid=pd.ProductAutoid and pm.productId='+@ProductId+'
		INNER JOIN [dbo].DraftPackingDetails AS DPD ON PD.UnitType=DPD.UnitType 
		WHERE DPD.ProductAutoId='+CONVERT(VARCHAR(50),@DraftProductAutoId)+'
		'
		EXEC sp_executesql @sql    
END 