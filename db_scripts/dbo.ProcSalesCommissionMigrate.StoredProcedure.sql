Alter PROCEDURE [dbo].[ProcSalesCommissionMigrate]            
@Opcode INT=NULL,            
@CustomerAutoId INT=NULL,             
@SalesPerson INT=NULL,            
@FromDate date =NULL,            
@ToDate date =NULL,            
@PageIndex INT=1,            
@PageSize INT=10,            
@RecordCount INT=null,            
@isException bit out,            
@exceptionMessage varchar(max) out            
AS            
BEGIN             
 BEGIN TRY            
   Set @isException=0            
   Set @exceptionMessage='Success'            
   IF @Opcode=41            
   BEGIN            
    SELECT AutoId as SID,FirstName+' '+ISNULL(LASTNAME,'') AS SP  FROM EmployeeMaster 
	WHERE EmpType IN (2)  AND Status=1        
    order by SP
	for json path
   END            
   ELSE IF @Opcode=42            
   BEGIN  
        Declare @ChildDbLocation varchar(50),@sql nvarchar(max) 
		select @ChildDbLocation=ChildDB_Name from CompanyDetails
    SET @sql='
		SELECT ROW_NUMBER() OVER(ORDER BY  SalesPersonAutoId,sp,CommCode) AS RowNumber, * INTO #Results FROM            
		(            
			select SP,t1.SalesPersonAutoId,NoofProduct,t1.CommCode,        
			CAST((TotalSale-ISNULL(TotalReturn,0)) as decimal(10,2)) as TotalSale,        
			CAST((SPCommAmt-ISNULL(ReturnSPCommAmt,0)) as decimal(10,2)) as SPCommAmt,
			0 as MNoofProduct,0 as MCommCode,        
			0 as MTotalSale,        
			0 as MSPCommAmt
			from 
			( 
				select (em.FirstName+'' ''+em.LastName) as SP,om.SalesPersonAutoId,COUNT(doim.OrderAutoId) AS NoofProduct ,
				isNull(P_CommCode,0) as CommCode,           
				ISNULL(sum(doim.NetPrice),0) as TotalSale,           
				CAST((sum(doim.NetPrice)*isNull (P_CommCode,0)) AS DECIMAL(10,2)) as SPCommAmt          
				from  Delivered_Order_Items as doim    
				inner join  ProductMaster as pm on pm.AutoId = doim.ProductAutoId         
				inner join OrderMaster as om on om.AutoId=doim.OrderAutoId          
				inner join EmployeeMaster as em on em.AutoId=om.SalesPersonAutoId          
				where           
				om.Status=11 and         
				(om.SalesPersonAutoId='''+Convert(varchar(25),@SalesPerson)+''' OR 
				ISNULL('''+Convert(varchar(25),@SalesPerson)+''',0)=0) and           
				(          
				'''+convert(varchar(25),@FromDate)+''' IS NULL OR '''+convert(varchar(25),@ToDate)+'''
				IS NULL OR convert(date,OrderDate) 
				between convert(date,'''+convert(varchar(25),@FromDate)+''')           
				and convert(date,'''+convert(varchar(25),@ToDate)+''')          
				)          
				group by isNull(P_CommCode,0), em.FirstName+'' ''+em.LastName,om.SalesPersonAutoId        
			) as t1         
			left join        
			(        
				select om.SalesPersonAutoId,sum(cim.NetAmount) as TotalReturn,isNull(P_CommCode,0) as CommCode,        
				CAST((sum(cim.NetAmount)*isNull (P_CommCode,0)) AS DECIMAL(10,2)) as ReturnSPCommAmt  from CreditItemMaster as cim         
				inner join ProductMaster as pd on pd.AutoId=cim.ProductAutoId       
				inner join CreditMemoMaster as cmm  on  cmm.CreditAutoId=cim.CreditAutoId        
				inner join OrderMaster as om on om.AutoId=cmm.OrderAutoId                 
				where om.Status=11 and          
				(om.SalesPersonAutoId='''+Convert(varchar(25),@SalesPerson)+''' OR 
				ISNULL('''+Convert(varchar(25),@SalesPerson)+''',0)=0)           
				and           
				(          
				'''+convert(varchar(25),@FromDate)+''' IS NULL OR '''+convert(varchar(25),@ToDate)+''' IS 
				NULL OR convert(date,OrderDate) between convert(date,'''+convert(varchar(25),@FromDate)+''')           
				and convert(date,'''+convert(varchar(25),@ToDate)+''')          
				)               
				group by om.SalesPersonAutoId,isNull(P_CommCode,0) 
			) as t2 on t1.CommCode=t2.CommCode and t1.SalesPersonAutoId=t2.SalesPersonAutoId  
			
			UNION ALL

			select '''' as SP,0 as SalesPersonAutoId,0 as NoofProduct,0 as CommCode,        
			0 as TotalSale,0 as SPCommAmt,NoofProduct as MNoofProduct,t1.CommCode as MCommCode,        
			CAST((TotalSale-ISNULL(TotalReturn,0)) as decimal(10,2)) as MTotalSale,        
			CAST((SPCommAmt-ISNULL(ReturnSPCommAmt,0)) as decimal(10,2)) as MSPCommAmt  from 
			( 
				select om.SalesPersonAutoId,COUNT(doim.OrderAutoId) AS NoofProduct ,
				isNull(P_CommCode,0) as CommCode,           
				ISNULL(sum(doim.NetPrice),0) as TotalSale,           
				CAST((sum(doim.NetPrice)*isNull (P_CommCode,0)) AS DECIMAL(10,2)) as SPCommAmt          
				from  ['+@ChildDbLocation+'].[dbo].Delivered_Order_Items as doim    
				inner join  ['+@ChildDbLocation+'].[dbo].ProductMaster as pm on pm.AutoId = doim.ProductAutoId         
				inner join ['+@ChildDbLocation+'].[dbo].OrderMaster as om on om.AutoId=doim.OrderAutoId         
				where           
				om.Status=11 and         
				(om.SalesPersonAutoId='''+Convert(varchar(25),@SalesPerson)+''' OR 
				ISNULL('''+Convert(varchar(25),@SalesPerson)+''',0)=0) and           
				(          
				'''+convert(varchar(25),@FromDate)+''' IS NULL OR '''+convert(varchar(25),@ToDate)+''' IS NULL OR convert(date,OrderDate) 
				between convert(date,'''+convert(varchar(25),@FromDate)+''')           
				and convert(date,'''+convert(varchar(25),@ToDate)+''')          
				)          
				group by isNull(P_CommCode,0),om.SalesPersonAutoId        
			) as t1         
			left join        
			(        
				select om.SalesPersonAutoId,sum(cim.NetAmount) as TotalReturn,isNull(P_CommCode,0) as CommCode,        
				CAST((sum(cim.NetAmount)*isNull (P_CommCode,0)) AS DECIMAL(10,2)) as ReturnSPCommAmt  from ['+@ChildDbLocation+'].[dbo].CreditItemMaster as cim         
				inner join ['+@ChildDbLocation+'].[dbo].ProductMaster as pd on pd.AutoId=cim.ProductAutoId       
				inner join ['+@ChildDbLocation+'].[dbo].CreditMemoMaster as cmm  on  cmm.CreditAutoId=cim.CreditAutoId        
				inner join ['+@ChildDbLocation+'].[dbo].OrderMaster as om on om.AutoId=cmm.OrderAutoId                 
				where om.Status=11 and          
				(om.SalesPersonAutoId='''+Convert(varchar(25),@SalesPerson)+''' OR
				ISNULL('''+Convert(varchar(25),@SalesPerson)+''',0)=0)           
				and           
				(          
				'''+convert(varchar(25),@FromDate)+''' IS NULL OR '''+convert(varchar(25),@ToDate)+''' IS 
				NULL OR convert(date,OrderDate) between convert(date,'''+convert(varchar(25),@FromDate)+''')           
				and convert(date,'''+convert(varchar(25),@ToDate)+''')          
				)               
				group by om.SalesPersonAutoId,isNull(P_CommCode,0) 
			) as t2 on t1.CommCode=t2.CommCode and t1.SalesPersonAutoId=t2.SalesPersonAutoId 
                 
		) AS t ORDER BY SalesPersonAutoId,sp,CommCode 

		SELECT * FROM #Results            
		WHERE ('''+convert(varchar(25),@PageSize)+''' = 0 or (RowNumber BETWEEN('''+convert(varchar(25),@PageIndex)+''' -1) 
		* '''+convert(varchar(25),@PageSize)+''' + 1 AND((('''+convert(varchar(25),@PageIndex)+''' -1) * 
		'''+convert(varchar(25),@PageSize)+''' + 1) + '''+convert(varchar(25),@PageSize)+''') - 1))               
		SELECT COUNT(CommCode) AS RecordCount, case when '''+convert(varchar(25),@PageSize)+'''=0 then
		COUNT(CommCode) else '''+convert(varchar(25),@PageSize)+''' end AS PageSize,
		'''+convert(varchar(25),@PageIndex)+''' AS PageIndex,Format(getdate(),''MM/dd/yyyy hh:mm tt'') as PrintDate FROM #Results 
		
		SELECT ISNULL(SUM(TotalSale),0.00) AS TotalSale,ISNULL(SUM(MTotalSale),0.00) AS MTotalSale,
		isnull(Sum(NoofProduct),0) as NoofOrder,isnull(Sum(MNoofProduct),0) as MNoofOrder,
		ISNULL(SUM(SPCommAmt),0.00) AS SPCommAmt, isnull(Sum(MSPCommAmt),0) as MSPCommAmt  from  #Results  '

		EXEC sp_executesql @sql
   END          
 END TRY            
 BEGIN CATCH            
		Set @isException=1            
		Set @exceptionMessage=ERROR_MESSAGE()            
 END CATCH            
END   

GO
