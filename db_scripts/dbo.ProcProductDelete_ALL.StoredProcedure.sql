USE [psmnj.a1whm.com]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[ProcProductDelete_ALL]           
@ProductId VARCHAR(12)=NULL  ,  
@Status int out   
AS          
BEGIN         
  
 DECLARE @ProductAutoId INT  
 ---DELETE from PSMNJ  
 SET @ProductAutoId=(SELECT [AutoId] FROM [psmnj.a1whm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)      
 IF EXISTS(SELECT [AutoId] FROM [psmnj.a1whm.com].[dbo].[OrderItemMaster] WHERE ProductAutoId = @ProductAutoId)          
 BEGIN   
  SET @Status=1  
    
 END    
  
    ---DELETE from PSMCT  
 SET @ProductAutoId=(SELECT [AutoId] FROM [psmct.a1whm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)  
      
 IF EXISTS(SELECT [AutoId] FROM [psmct.a1whm.com].[dbo].[OrderItemMaster] WHERE ProductAutoId = @ProductAutoId)          
 BEGIN    
  SET @Status=1  
    
 END   
 ---DELETE from PSMPA  
 SET @ProductAutoId=(SELECT [AutoId] FROM [psmpa.a1whm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)      
 IF EXISTS(SELECT [AutoId] FROM [psmpa.a1whm.com].[dbo].[OrderItemMaster] WHERE ProductAutoId = @ProductAutoId)          
 BEGIN      
  SET @Status=1  
    
 END        
     ---DELETE from PSMNPA  
 SET @ProductAutoId=(SELECT [AutoId] FROM [psmnpa.a1whm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)      
 IF EXISTS(SELECT [AutoId] FROM [psmnpa.a1whm.com].[dbo].[OrderItemMaster] WHERE ProductAutoId = @ProductAutoId)          
 BEGIN     
  SET @Status=1  
    
 END             
         
    ---DELETE from PSMWPA  
 SET @ProductAutoId=(SELECT [AutoId] FROM [psmwpa.a1whm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)      
 IF EXISTS(SELECT [AutoId] FROM [psmwpa.a1whm.com].[dbo].[OrderItemMaster] WHERE ProductAutoId = @ProductAutoId)          
 BEGIN       
  SET @Status=1  
    
 END   
 
  ---DELETE from PSMNY 
 SET @ProductAutoId=(SELECT [AutoId] FROM [psmny.a1whm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)      
 IF EXISTS(SELECT [AutoId] FROM [psmny.a1whm.com].[dbo].[OrderItemMaster] WHERE ProductAutoId = @ProductAutoId)          
 BEGIN       
  SET @Status=1    
 END   
  
 ---DELETE from EASY  NJ
 SET @ProductAutoId=(SELECT [AutoId] FROM [psmnj.easywhm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)      
 IF EXISTS(SELECT [AutoId] FROM [psmnj.easywhm.com].[dbo].[OrderItemMaster] WHERE ProductAutoId = @ProductAutoId)          
 BEGIN       
  SET @Status=1 
 END  
 
 ---DELETE from EASY  PA
 SET @ProductAutoId=(SELECT [AutoId] FROM [psmpa.easywhm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)      
 IF EXISTS(SELECT [AutoId] FROM [psmpa.easywhm.com].[dbo].[OrderItemMaster] WHERE ProductAutoId = @ProductAutoId)          
 BEGIN       
  SET @Status=1 
 END  

 ---DELETE from EASY  NPA
 SET @ProductAutoId=(SELECT [AutoId] FROM [psmnpa.easywhm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)      
 IF EXISTS(SELECT [AutoId] FROM [psmnpa.easywhm.com].[dbo].[OrderItemMaster] WHERE ProductAutoId = @ProductAutoId)          
 BEGIN       
  SET @Status=1 
 END  
   
 ---DELETE from EASY  WPA
 SET @ProductAutoId=(SELECT [AutoId] FROM [psmwpa.easywhm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)      
 IF EXISTS(SELECT [AutoId] FROM [psmwpa.easywhm.com].[dbo].[OrderItemMaster] WHERE ProductAutoId = @ProductAutoId)          
 BEGIN       
  SET @Status=1 
 END 
 
 ---DELETE from EASY  CT
 SET @ProductAutoId=(SELECT [AutoId] FROM [psmct.easywhm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)      
 IF EXISTS(SELECT [AutoId] FROM [psmct.easywhm.com].[dbo].[OrderItemMaster] WHERE ProductAutoId = @ProductAutoId)          
 BEGIN       
  SET @Status=1 
 END 


 IF(@Status=0)  
 BEGIN  
  BEGIN TRY  
   BEGIN TRAN  
    SET @ProductAutoId=(SELECT [AutoId] FROM [psmnj.a1whm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)    
    DELETE FROM [psmnj.a1whm.com].[dbo].[ItemBarcode] WHERE ProductAutoId= @ProductAutoId      
    DELETE FROM [psmnj.a1whm.com].[dbo].[RealProductLocation] WHERE ProductAutoId= @ProductAutoId  
    DELETE FROM [psmnj.a1whm.com].[dbo].[PackingDetails] WHERE ProductAutoId= @ProductAutoId  
    DELETE FROM [psmnj.a1whm.com].[dbo].Tbl_ProductImage WHERE ProductAutoId= @ProductAutoId  
    DELETE FROM [psmnj.a1whm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId  
  
    SET @ProductAutoId=(SELECT [AutoId] FROM [psmct.a1whm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)   
	DELETE FROM [psmct.a1whm.com].[dbo].[RealProductLocation] WHERE ProductAutoId= @ProductAutoId  
    DELETE FROM [psmct.a1whm.com].[dbo].[ItemBarcode] WHERE ProductAutoId= @ProductAutoId  
    DELETE FROM [psmct.a1whm.com].[dbo].[PackingDetails] WHERE ProductAutoId= @ProductAutoId  
	DELETE FROM [psmct.a1whm.com].[dbo].Tbl_ProductImage WHERE ProductAutoId= @ProductAutoId  
    DELETE FROM [psmct.a1whm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId  
  
    SET @ProductAutoId=(SELECT [AutoId] FROM [psmpa.a1whm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)   
	DELETE FROM [psmpa.a1whm.com].[dbo].[RealProductLocation] WHERE ProductAutoId= @ProductAutoId  
    DELETE FROM [psmpa.a1whm.com].[dbo].[ItemBarcode] WHERE ProductAutoId= @ProductAutoId  
    DELETE FROM [psmpa.a1whm.com].[dbo].[PackingDetails] WHERE ProductAutoId= @ProductAutoId
	DELETE FROM [psmpa.a1whm.com].[dbo].Tbl_ProductImage WHERE ProductAutoId= @ProductAutoId  
    DELETE FROM [psmpa.a1whm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId  
  
    SET @ProductAutoId=(SELECT [AutoId] FROM [psmnpa.a1whm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)
	DELETE FROM [psmnpa.a1whm.com].[dbo].[RealProductLocation] WHERE ProductAutoId= @ProductAutoId  
    DELETE FROM [psmnpa.a1whm.com].[dbo].[ItemBarcode] WHERE ProductAutoId= @ProductAutoId  
    DELETE FROM [psmnpa.a1whm.com].[dbo].[PackingDetails] WHERE ProductAutoId= @ProductAutoId  
	DELETE FROM [psmnpa.a1whm.com].[dbo].Tbl_ProductImage WHERE ProductAutoId= @ProductAutoId  
    DELETE FROM [psmnpa.a1whm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId  
  
    SET @ProductAutoId=(SELECT [AutoId] FROM [psmwpa.a1whm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId) 
	DELETE FROM [psmwpa.a1whm.com].[dbo].[RealProductLocation] WHERE ProductAutoId= @ProductAutoId  
    DELETE FROM [psmwpa.a1whm.com].[dbo].[ItemBarcode] WHERE ProductAutoId= @ProductAutoId  
    DELETE FROM [psmwpa.a1whm.com].[dbo].[PackingDetails] WHERE ProductAutoId= @ProductAutoId  
	DELETE FROM [psmwpa.a1whm.com].[dbo].Tbl_ProductImage WHERE ProductAutoId= @ProductAutoId  
    DELETE FROM [psmwpa.a1whm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId    
  
    SET @ProductAutoId=(SELECT [AutoId] FROM [psmny.a1whm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId) 
	DELETE FROM [psmny.a1whm.com].[dbo].[RealProductLocation] WHERE ProductAutoId= @ProductAutoId  
    DELETE FROM [psmny.a1whm.com].[dbo].[ItemBarcode] WHERE ProductAutoId= @ProductAutoId  
    DELETE FROM [psmny.a1whm.com].[dbo].[PackingDetails] WHERE ProductAutoId= @ProductAutoId 
	DELETE FROM [psmny.a1whm.com].[dbo].Tbl_ProductImage WHERE ProductAutoId= @ProductAutoId  
    DELETE FROM [psmny.a1whm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId    
  
    SET @ProductAutoId=(SELECT [AutoId] FROM [psmnj.easywhm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)  
	DELETE FROM [psmnj.easywhm.com].[dbo].[RealProductLocation] WHERE ProductAutoId= @ProductAutoId  
    DELETE FROM [psmnj.easywhm.com].[dbo].[ItemBarcode] WHERE ProductAutoId= @ProductAutoId  
    DELETE FROM [psmnj.easywhm.com].[dbo].[PackingDetails] WHERE ProductAutoId= @ProductAutoId  
	DELETE FROM [psmnj.easywhm.com].[dbo].Tbl_ProductImage WHERE ProductAutoId= @ProductAutoId  
    DELETE FROM [psmnj.easywhm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId 
	
	SET @ProductAutoId=(SELECT [AutoId] FROM [psmpa.easywhm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId) 
	DELETE FROM [psmpa.easywhm.com].[dbo].[RealProductLocation] WHERE ProductAutoId= @ProductAutoId  
    DELETE FROM [psmpa.easywhm.com].[dbo].[ItemBarcode] WHERE ProductAutoId= @ProductAutoId  
    DELETE FROM [psmpa.easywhm.com].[dbo].[PackingDetails] WHERE ProductAutoId= @ProductAutoId  
	DELETE FROM [psmpa.easywhm.com].[dbo].Tbl_ProductImage WHERE ProductAutoId= @ProductAutoId  
    DELETE FROM [psmpa.easywhm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId 

	SET @ProductAutoId=(SELECT [AutoId] FROM [psmnpa.easywhm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)   
	DELETE FROM [psmnpa.easywhm.com].[dbo].[RealProductLocation] WHERE ProductAutoId= @ProductAutoId  
    DELETE FROM [psmnpa.easywhm.com].[dbo].[ItemBarcode] WHERE ProductAutoId= @ProductAutoId  
    DELETE FROM [psmnpa.easywhm.com].[dbo].[PackingDetails] WHERE ProductAutoId= @ProductAutoId  
	DELETE FROM [psmnpa.easywhm.com].[dbo].Tbl_ProductImage WHERE ProductAutoId= @ProductAutoId  
    DELETE FROM [psmnpa.easywhm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId 

	SET @ProductAutoId=(SELECT [AutoId] FROM [psmwpa.easywhm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId) 
	DELETE FROM [psmwpa.easywhm.com].[dbo].[RealProductLocation] WHERE ProductAutoId= @ProductAutoId  
    DELETE FROM [psmwpa.easywhm.com].[dbo].[ItemBarcode] WHERE ProductAutoId= @ProductAutoId  
    DELETE FROM [psmwpa.easywhm.com].[dbo].[PackingDetails] WHERE ProductAutoId= @ProductAutoId
	DELETE FROM [psmwpa.easywhm.com].[dbo].Tbl_ProductImage WHERE ProductAutoId= @ProductAutoId  
    DELETE FROM [psmwpa.easywhm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId 

	SET @ProductAutoId=(SELECT [AutoId] FROM [psmct.easywhm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)   
	DELETE FROM [psmct.easywhm.com].[dbo].[RealProductLocation] WHERE ProductAutoId= @ProductAutoId  
    DELETE FROM [psmct.easywhm.com].[dbo].[ItemBarcode] WHERE ProductAutoId= @ProductAutoId  
    DELETE FROM [psmct.easywhm.com].[dbo].[PackingDetails] WHERE ProductAutoId= @ProductAutoId 
	DELETE FROM [psmct.easywhm.com].[dbo].Tbl_ProductImage WHERE ProductAutoId= @ProductAutoId  
    DELETE FROM [psmct.easywhm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId 

	SET @ProductAutoId=(SELECT [AutoId] FROM [psmny.easywhm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)   
	DELETE FROM [psmny.easywhm.com].[dbo].[RealProductLocation] WHERE ProductAutoId= @ProductAutoId  
    DELETE FROM [psmny.easywhm.com].[dbo].[ItemBarcode] WHERE ProductAutoId= @ProductAutoId  
    DELETE FROM [psmny.easywhm.com].[dbo].[PackingDetails] WHERE ProductAutoId= @ProductAutoId 
	DELETE FROM [psmny.easywhm.com].[dbo].Tbl_ProductImage WHERE ProductAutoId= @ProductAutoId  
    DELETE FROM [psmny.easywhm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId 
   COMMIT TRAN  
    
  END TRY  
  BEGIN CATCH  
   ROLLBACK TRAN  
   SET @Status=1  
  END CATCH  
   
 END  
END        