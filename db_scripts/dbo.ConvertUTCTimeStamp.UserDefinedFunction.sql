USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[ConvertUTCTimeStamp]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[ConvertUTCTimeStamp](@Date dateTime) --select [dbo].ConvertUTCTimeStamp(getdate())
returns int
as
begin
declare 
@StartDateTime datetime  ='2018/01/01',
@TimeStamp int,
@DiffTimeStamp int = DATEDIFF(SECOND, GETUTCDATE(), GETDATE())

set @TimeStamp= DATEDIFF(SECOND,@StartDateTime,(dateadd(SECOND,-@DiffTimeStamp,@Date)))

return @TimeStamp
end
GO
