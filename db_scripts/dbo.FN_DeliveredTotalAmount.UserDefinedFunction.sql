USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_DeliveredTotalAmount]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create FUNCTION  [dbo].[FN_DeliveredTotalAmount]
(
	 @orderAutoId int
)
RETURNS decimal(10,2)
AS
BEGIN
	DECLARE @TotalAmount decimal(10,2)	
	set @TotalAmount=isnull((select TotalAmount from OrderMaster where AutoId=@orderAutoId)	,0.00)
	RETURN @TotalAmount
END

GO
