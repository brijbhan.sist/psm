USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Order_Weigth_OZTaxAmount]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

alter table OrderMaster drop column Weigth_OZTaxAmount
drop function FN_Order_Weigth_OZTaxAmount
go
CREATE FUNCTION  [dbo].[FN_Order_Weigth_OZTaxAmount]
(
	 @AutoId int
)
RETURNS decimal(18,2)
AS
BEGIN
	DECLARE @Weight_Oz decimal(18,2)	
	SET @Weight_Oz=isnull((select  ISNULL(Weigth_OZQty,0)*ISNULL(Weigth_OZTax,0) from OrderMaster where AutoId=@AutoId),0.00)
	RETURN @Weight_Oz
END
GO
alter table OrderMaster add [Weigth_OZTaxAmount]  AS ([dbo].[FN_Order_Weigth_OZTaxAmount]([autoid]))