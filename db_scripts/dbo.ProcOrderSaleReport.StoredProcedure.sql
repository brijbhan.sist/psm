    
ALTER PROCEDURE [dbo].[ProcOrderSaleReport]    
@Opcode INT=NULL,     
@CutomerType INT=NULL,    
@Customer INT=NULL,     
@SalesPerson INT=NULL,    
@Category INT=NULL,    
@SubCategory INT=NULL,    
@Product INT=NULL,    
@FromDate Date=NULL,    
@ToDate Date=NULL,    
@IsException bit out,           
@PageIndex INT =1,          
@PageSize INT=10,          
@RecordCount INT=null,             
@ExceptionMessage varchar(max) out      
AS    
BEGIN    
       Set @isException=0          
       Set @exceptionMessage='Success'      
  BEGIN TRY    
       IF @Opcode = 41          
    BEGIN
	select
	(
	SELECT AutoId as SId,(FirstName + ' ' + LastName) as Sp from EmployeeMaster  Where EmpType =2 and Status=1 order by Sp ASC    
	for json path
	) as SalesPerson,
	(
	SELECT[AutoId] as CId,[CustomerType] as Ct from CustomerType order by Ct ASC   
	for json path
	) as CType,
	
	(
	SELECT AutoId as CAId , CategoryId, CategoryName as Can FROM  CategoryMaster  where  Status=1 order by Can ASC
	for json path
	) as CatName
	for JSON path
    END       

	IF @Opcode=42
	begin
	SELECT AutoId AS CuId,CustomerId+' '+CustomerName as CUN FROM CustomerMaster   WHERE            
	status=1 and             
	(CustomerType=@CutomerType                                       
	OR ISNULL(@CutomerType,0)=0) and (SalesPersonAutoId=@SalesPerson or ISNULL(@SalesPerson,0)=0)
	order by replace(CustomerId+' '+CustomerName,' ','') ASC     
	for json path	
	end
	ELSE IF @Opcode=43               
  BEGIN  

    SELECT AutoId as SCAId,SubcategoryName as SCAN from SubCategoryMaster 
	where (isnull(@Category,0)=0 or CategoryAutoId=@Category)and Status=1                           
    order by SCAN ASC      
	for JSON path
END
ELSE IF @Opcode=44               
  BEGIN
   SELECT  AutoId as PID,CONVERT(VARCHAR(10),ProductId )+' '+ ProductName AS PN FROM ProductMaster                                                                    
    WHERE                                                                     
     (isnull(@SubCategory,0)=0 or SubcategoryAutoId=@SubCategory )
	 and ProductStatus=1  
	 and ISNULL(@Category,0)=0 or CategoryAutoId=@Category    
	 order by CONVERT(VARCHAR(10),ProductId )+' '+ ProductName  ASC

	for JSON path
End
  
    IF @Opcode=45   
    BEGIN    
   SELECT ROW_NUMBER() OVER(ORDER BY CustomerName) AS RowNumber, * INTO #Results FROM      
     (      
    SELECT CustomerId as CustomerAutoId,cm.CustomerName,sum(om.TotalAmount-om.OverallDiscAmt) as TotalNetSale,
	sum(om.TotalTax+om.MLTax+om.Weigth_OZTaxAmount) as TotalTax,sum(om.ShippingCharges) as Shipping,count(om.AutoId) as TotalOrder,
	(sum(om.TotalAmount-om.OverallDiscAmt+om.TotalTax+om.MLTax+om.Weigth_OZTaxAmount+om.ShippingCharges))/ (count(om.AutoId)) as AOV,
	sum(ISNULL(do.AmtDue,om.PayableAmount)) as DueAmt,FORMAT(MAX(OrderDate),'MM/dd/yyyy') as LastOrderDate
    FROM OrderMaster as om     
    inner join CustomerMaster as cm on om.CustomerAutoId=cm.AutoId   
	inner join DeliveredOrders as do on do.OrderAutoId=om.AutoId   
    Where (@FromDate is null or @FromDate='' or @ToDate is null or @ToDate='' or 
	Convert(date,OrderDate) between Convert(date,@FromDate) and Convert(date,@ToDate)) and --Date    
       (ISNULL(@Product,0)=0 or om.AutoId in (Select distinct orderautoid from OrderItemMaster where ProductAutoId=@Product)) and --Product    
       (ISNULL(@SalesPerson,0)=0 or om.SalesPersonAutoId=@SalesPerson) and--Sales Person    
       (ISNULL(@Customer,0)=0 or om.CustomerAutoId=@Customer) and  --Customer
       (ISNULL(@Category,0)=0 or om.AutoId in (Select distinct orderautoid from OrderItemMaster as oim inner join  ProductMaster as pm on pm.AutoId=oim.ProductAutoId where pm.CategoryAutoId=@Category)) and    
       (ISNULL(@SubCategory,0)=0 or om.AutoId in (Select distinct orderautoid from OrderItemMaster as oim inner join  ProductMaster as pm on pm.AutoId=oim.ProductAutoId where pm.SubcategoryAutoId=@SubCategory)) and    
       (ISNULL(@CutomerType,0)=0 or cm.CustomerType=@CutomerType) 
        and om.Status=11 group by CustomerId,CustomerName,LastOrderDate  
   ) AS t  ORDER BY CustomerName  
	SELECT COUNT(CustomerName) AS RecordCount, case when @PageSize=0 then COUNT(CustomerName) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Results    
	SELECT *  FROM #Results      
	WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))     
	SELECT ISNULL(SUM(TotalOrder),0) as overAllOrder,ISNULL(SUM(TotalNetSale),0.00) as TotalNetSale,
	ISNULL(SUM(TotalTax),0.00) as TotalTax,ISNULL(SUM(DueAmt),0) as overAllDueAmt,
	ISNULL(SUM(TotalNetSale+Shipping+TotalTax),0.00)/ISNULL(SUM(TotalOrder),0) as overAllAOV,
	ISNULL(SUM(Shipping),0.00) as Shipping,ISNULL(SUM(TotalNetSale+Shipping+TotalTax),0.00) as GrossSales,
	FORMAT(GETDATE(),'MM/dd/yyyy hh:mm tt') as [PrintDate] 
	FROM #Results 
    END       
 END TRY    
 BEGIN CATCH    
            SET @IsException=1    
      SET @ExceptionMessage=ERROR_MESSAGE()    
 END CATCH    
END    
    
