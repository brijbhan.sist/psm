
create or alter procedure [dbo].[ProcToDoMaster]
@Opcode int=Null,                                      
@AutoId  int=Null,      
@Title varchar(150)=null,
@Description varchar(500)=null,
@Starred int=null,
@Important int=null,
@IsDeleted int=null,
@IsCompleted int = null,
@UserAutoId int=null,
@CreatedBy int=NULL,
@CreatedDate datetime=null,
@UpdatedDate datetime=null,
@UpdatedBy int=null,  
@FromDate datetime = null,
@ToDate datetime =null,
@RecordCount int=null,
@PageIndex INT = 1,                                      
@PageSize INT = 10,    
@isException bit out,    
@exceptionMessage varchar(max) out 

AS                                      
BEGIN                                      
                                    
 SET @isException=0                                      
 SET @exceptionMessage='Success'                                      
 IF @Opcode=11                                       
 	BEGIN
		BEGIN TRY                                         
			BEGIN TRAN   
				 insert into ToDoMaster(Title,Description,Starred,Important,EmpAutoId,IsDeleted,CreatedBy,CreatedDate,UpdatedDate,IsCompleted)
				 values(@Title,@Description,@Starred,@Important,@UserAutoId,0,@CreatedBy,GETDATE(),GETDATE(),@IsCompleted)	
			COMMIT TRANSACTION                                      
		END TRY                                      
		BEGIN CATCH                                      
			ROLLBACK TRAN                                      
			SET @isException=1                                      
			SET @exceptionMessage=ERROR_MESSAGE()           
		END CATCH  
	END 

 IF @Opcode=21                                       
    BEGIN  		
		 update ToDoMaster set Title=@Title,Description=@Description,Starred=@Starred,Important=@Important,IsDeleted=0,UpdatedBy=@CreatedBy,IsCompleted=@IsCompleted,UpdatedDate=GETDATE()
		 where AutoId=@AutoId and EmpAutoId=@UserAutoId
    END 

 IF @Opcode=22                                       
    BEGIN  		
		 update ToDoMaster set IsDeleted = 1
		 where AutoId=@AutoId and EmpAutoId=@UserAutoId
    END 

 IF @Opcode=23                                       
    BEGIN  		
		 update ToDoMaster set IsDeleted = 0
		 where AutoId=@AutoId and EmpAutoId=@UserAutoId
    END 

 IF @Opcode=24                                       
    BEGIN  		
		 update ToDoMaster set IsCompleted = @IsCompleted
		 where AutoId=@AutoId and EmpAutoId=@UserAutoId
    END 

 IF @Opcode=31                                      
    BEGIN  		
		 Delete  from ToDoMaster
		 where AutoId=@AutoId and EmpAutoId=@UserAutoId
    END 

 IF @Opcode=41
	 BEGIN
	   SELECT ROW_NUMBER() OVER(ORDER BY AutoId desc) AS RowNumber, * INTO #Results from                      
	    (  
		  select TD.AutoId,Title,Description,Important,Starred,SM.AutoId as IsCompleted, SM.StatusType,
		  format(CreatedDate,'MM/dd/yyyy hh:mm tt')AS CreatedDate,format(UpdatedDate,'MM/dd/yyy') as UpdatedDate from ToDoMaster as TD
		   inner join StatusMaster SM on SM.AutoId=TD.IsCompleted and Category='Todo' 
		 where  EmpAutoId=@UserAutoId and IsDeleted=0 
		  and (@IsCompleted=0 or @IsCompleted is null or IsCompleted = @IsCompleted)
		  and (@FromDate is null or @ToDate is null or (CONVERT(date,CreatedDate)between convert(date,@FromDate) and CONVERT(date,@ToDate)))
		 ) as t 
		  order by AutoId desc

		  if(1= @Starred and 0=@Important)
			  BEGIN
			  SELECT COUNT(RowNumber) AS RecordCount,case when @PageSize=0 then COUNT(AutoId) else @PageSize end AS PageSize,@PageIndex as PageIndex 
			  FROM  #Results where Starred=1 and Important=0
			  select * from  #Results where Starred=1 
			  and (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))
			  END
		  else  if (1 = @Important and 0 = @Starred)
			  BEGIN
			  SELECT COUNT(RowNumber) AS RecordCount,case when @PageSize=0 then COUNT(AutoId) else @PageSize end AS PageSize,@PageIndex as PageIndex 
			  FROM  #Results where Important =1
			  select * from  #Results where  Important =1

			  and (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))
			  END
		  else  if(1=@Starred and 1=@Important)
			  BEGIN
			   SELECT COUNT(RowNumber) AS RecordCount,case when @PageSize=0 then COUNT(AutoId) else @PageSize end AS PageSize,@PageIndex as PageIndex 
			  FROM  #Results where   Starred =1 or Important =1 or Starred =0 or Important =0 
			  select * from  #Results where (Starred =1 or Important =1 or Starred =0 or Important =0)
			   and (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))
			  END					
	 END
 IF @Opcode=42
	 BEGIN
		  select AutoId,Title,Description,Important,Starred,IsCompleted from ToDoMaster
		  where  AutoId=@AutoId
	 END

 IF @Opcode=43
	 BEGIN
	   SELECT ROW_NUMBER() OVER(ORDER BY AutoId Asc) AS RowNumber, * INTO #Results1 from                      
	    (  
		  select TD.AutoId,Title,Description,Important,Starred,SM.AutoId as IsCompleted, SM.StatusType,format(CreatedDate,'MM/dd/yyyy hh:mm tt')AS CreatedDate,format(UpdatedDate,'MM/dd/yyy') as UpdatedDate from ToDoMaster as TD
		  inner join StatusMaster SM on SM.AutoId=TD.IsCompleted and Category='Todo' 
		  where   IsDeleted=1 
		  and (@FromDate is null or @ToDate is null or (CONVERT(date,CreatedDate)between convert(date,@FromDate) and CONVERT(date,@ToDate)))
		 ) as t 
		 order by AutoId desc

		  if(1= @Starred and 0=@Important)
			  BEGIN
			  SELECT COUNT(RowNumber) AS RecordCount,case when @PageSize=0 then COUNT(AutoId) else @PageSize end AS PageSize,@PageIndex as PageIndex 
			  FROM  #Results1 where (Starred=1 and Important=0)
			  select * from  #Results1 where (Starred=1 and Important=0)
			  and (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))
			  END
		  else  if(1 = @Important and 0 = @Starred)
			  BEGIN	
			    SELECT COUNT(RowNumber) AS RecordCount,case when @PageSize=0 then COUNT(AutoId) else @PageSize end AS PageSize,@PageIndex as PageIndex 
			  FROM  #Results1 where Important =1

			  select * from  #Results1 where  Important =1
			  
			  and (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))
			  END
		  else  if(1=@Starred and 1=@Important)
			  BEGIN
			   SELECT COUNT(RowNumber) AS RecordCount,case when @PageSize=0 then COUNT(AutoId) else @PageSize end AS PageSize,@PageIndex as PageIndex 
			  FROM  #Results1 where   Starred =1 or Important =1 or Starred =0 or Important =0

			  select * from  #Results1 where (Starred =1 or Important =1 or Starred =0 or Important =0)

			   and (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))
			  END					
	 END
IF @Opcode=44
	 BEGIN
		  select AutoId,StatusType from StatusMaster where Category='Todo' 
		  order by AutoId
	 END
END 
GO


