USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Order_TotalPieces]    Script Date: 6/12/2020 11:25:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER table OrderItemMaster  drop column TotalPieces
Drop FUNCTION  [dbo].[FN_Order_TotalPieces]
go
Create FUNCTION  [dbo].[FN_Order_TotalPieces]
(
	 @AutoId int,
	 @orderAutoId int
)
RETURNS int
AS
BEGIN
	DECLARE @TotalPieces decimal(18,2)	
	--CAL TOTAL AMOUNT FOR NEW ,PROCESSED AND CANCELLED ORDER 
	IF EXISTS(SELECT AutoId FROM OrderMaster WHERE AutoId=@orderAutoId AND Status IN (0,1,2,8))
	BEGIN
		SET @TotalPieces=(SELECT (QtyPerUnit*RequiredQty) FROM OrderItemMaster WHERE AutoId=@AutoId)	
	END
	ELSE 
	BEGIN	 
		SET @TotalPieces=(SELECT (QtyPerUnit*QtyShip) FROM OrderItemMaster WHERE AutoId=@AutoId)	
	END

	RETURN @TotalPieces
END

go
ALTER TABLE OrderItemMaster add TotalPieces as [dbo].[FN_Order_TotalPieces](autoid,OrderAutoid)
