create table CreditMemoTypeMaster
(
AutoId int identity(1,1) not null,
CreditType varchar(50) not null
)
go
INSERT INTO CreditMemoTypeMaster VALUES ('Type 1 - NT Default')
INSERT INTO CreditMemoTypeMaster VALUES ('Type 2 - T')
go
Alter table CreditMemoMaster add CreditMemoType int
Alter table CreditMemoMaster add ReferenceOrderAutoId int
go