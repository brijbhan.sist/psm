USE [psmnj.a1whm.com]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create FUNCTION  [dbo].[FN_DefaultLandline2] 
(   
  @AutoId int   
)  
RETURNS varchar(100)  
AS  
BEGIN  
	DECLARE @Landline2 varchar(100)
	set @Landline2=isnull((SELECT TOP 1 Landline2 FROM CustomerContactPerson WHERE CustomerAutoId=@AutoId AND ISDefault=1),'')
	RETURN @Landline2  
END 
go
Alter table CustomerMaster Drop column Contact2
Alter table CustomerMaster add Contact2 As ([dbo].[FN_DefaultLandline2](AutoId))