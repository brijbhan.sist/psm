USE [psmnj.a1whm.com]
GO

/****** Object:  Table [dbo].[BarcodeScanning_Log]    Script Date: 07/10/2021 19:14:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BarcodeScanning_Log](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[Barcode] [varchar](100) NULL,
	[ScanningDate] [datetime] NULL,
	[ScannedBy] [int] NULL,
	[ActionRemarks] [varchar](500) NULL,
	[ModuleName] [varchar](200) NULL,
 CONSTRAINT [PK_BarcodeScanning_Log] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


