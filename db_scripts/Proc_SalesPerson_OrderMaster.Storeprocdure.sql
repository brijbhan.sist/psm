CREATE OR ALTER PROCEDURE [dbo].[Proc_SalesPerson_OrderMaster]                                      
@Opcode INT=NULL,  
@OrderAutoId INT=NULL,    
@customerType INT=NULL, 
@CustomerAutoId  int=null,                                      
@EmpAutoId int =null,       
@OrderNo VARCHAR(15)=NULL,                                       
@PageIndex INT = 1,                                      
@PageSize INT = 10,                                      
@RecordCount INT =null, 
@CurrentStatus INT =null, 
@isException bit out,                                      
@exceptionMessage varchar(max) out                                                                             

AS                                      
BEGIN                                       
BEGIN TRY                                      
Set @isException=0                                      
Set @exceptionMessage='Success'  
Declare @StateCode varchar(5)
 
IF @Opcode=42                                      
BEGIN                         
     
	SET @CustomerAutoId=(SELECT CustomerAutoId FROM OrderMaster WHERE AutoId=@OrderAutoId)    
	IF EXISTS(SELECT AUTOID FROM CustomerMaster WHERE AutoId=@CustomerAutoId AND CustomerType=3)
	BEGIN
		UPDATE OrderMaster SET isMLManualyApply=0,IsTaxApply=0,Weigth_OZTax=0 WHERE AutoId=@OrderAutoId
	END

    IF EXISTS(SELECT [AutoId] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId AND status=11)                                      
     BEGIN                                      
      SELECT OM.[AutoId],OM.[OrderNo],CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,                                      
      ISNULL(CONVERT(VARCHAR(20), OM.[DeliveryDate],101),CONVERT(VARCHAR(20), OM.[DelDate],101)) AS DeliveryDate,                                
      CM.[AutoId] As CustAutoId,CM.[CustomerId],CM.[CustomerName],CM.[Contact1] + '/' + CM.[Contact2] As Contact,CTy.CustomerType,CT.[TermsDesc],ST.[ShippingType],                                      
      [dbo].[ConvertFirstLetterinCapital](BA.[Address]+(case when isnull(BA.Address2,'')!='' then ', '+BA.Address2 else '' end)+', '+BA.City)+', '+UPper(S.StateCode)+' - '+BA.Zipcode As BillAddr,
	  ST.[AutoId] as ShipAutoId,                                
      [dbo].[ConvertFirstLetterinCapital](SA.[Address]+(case when isnull(SA.Address2,'')!='' then ', '+SA.Address2 else '' end)+', '+SA.City)+', '+S1.StateCode+' - '+SA.Zipcode As ShipAddr,                                      
      SM.[StatusType] AS Status,OM.[PackedBoxes],DO.[TotalAmount],DO.[OverallDisc],DO.[OverallDiscAmt],                                      
      DO.[ShippingCharges],DO.[TotalTax],DO.[GrandTotal],DO.[AmtPaid],DO.[AmtDue],OM.[PaymentRecev],OM.[RecevAmt],OM.[AmtValue],OM.[ValueChanged],                                      
      OM.[DiffAmt],OM.[NewTotal],OM.[PayThru],OM.CheckNo,OM.[DrvRemarks],OM.[AcctRemarks],EM.[FirstName] + ' ' + EM.[LastName] As SalesPerson,                                      
      EM1.[FirstName] + ' ' + EM1.[LastName] As PackerName,[CommentType],[Comment],isnull(DeductionAmount,0.00) as DeductionAmount,                                      
            ISNULL(DO.CreditAmount,0.00)as CreditAmount,isnull(do.PayableAmount,0.00) as PayableAmount,                                      
            ISNULL((select t.CreditAmount from CustomerCreditMaster as t  where t.CustomerAutoId=om.CustomerAutoid),0.00) as CustomerCredit                                      
          
			,otm.OrderType,OrderRemarks,ST.EnabledTax,                                     
            ISNULL(OM.MLQty,0) as MLQty,isnull(DO.MLTax,0) as MLTax,CM.ContactPersonName,ISNULL(om.AdjustmentAmt,0) as AdjustmentAmt,                                      
            CM.BusinessName,ISNULL(UnitMLTax,0) as UnitMLTax,ISNULL(DO.AmtPaid,0) as AmtPaid,ISNULL(do.AmtDue,0) as AmtDue,                                      
      (select TaxableType from TaxTypeMaster as ttm where ttm.Autoid=om.taxType) as TaxType ,OM.Driver ,isMLManualyApply,        
      isnull(OM.Weigth_OZQty,0) as WeightOzQty,isnull(OM.Weigth_OZTaxAmount,0) as WeightOzTotalTax,dbo.UDF_CheckMLTax(OM.AutoId) as MLTaxStatus       
                                     
      FROM [dbo].[OrderMaster] As OM                                           
      INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                     
      INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.Status AND SM.[Category] = 'OrderMaster'                                      
      INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[AutoId] = CM.DefaultBillAdd                                      
      INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = CM.DefaultShipAdd                      
      INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                                       
      INNER JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                                              
      INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]                                      
      LEFT JOIN [dbo].[CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                                      
      LEFT JOIN [dbo].[ShippingType] AS ST ON ST.[AutoId] = OM.[ShippingType]                                       
      LEFT JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[SalesPersonAutoId]                                      
      LEFT JOIN [dbo].[EmployeeMaster] AS EM1 ON EM1.[AutoId] = OM.[PackerAutoId]                                       
      INNER JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId] 
	  INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
	  WHERE OM.AutoId = @OrderAutoId     
	  
	  SET @customerType=(select CustomerType from CustomerMaster where AutoId=@CustomerAutoId)
                                      
      SELECT CM.[CategoryName],CM.[CategoryId],DOI.[ProductAutoId],PM.[ProductId],PM.[ProductName],DOI.[UnitAutoId],UM.[UnitType],DOI.[QtyPerUnit],                                      
      (select top 1 requiredQty from OrderItemMaster oim where oim.orderAutoId=doi.OrderAutoId and oim.ProductAutoid=doi.ProductAutoid and oim.UnitTypeAutoId=doi.UnitAutoid )                                      
      as RequiredQty,                                      
      DOI.[UnitPrice],DOI.[Barcode],DOI.[QtyShip],DOI.[QtyDel],DOI.[TotalPieces],DOI.[SRP],DOI.[GP],DOI.[Tax],DOI.[NetPrice]                                      
      ,convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice,
	  ISNULL(FreshReturnQty,0) as FreshReturnQty,FreshReturnUnitAutoId,                                      
      ISNULL(DamageReturnQty,0) as DamageReturnQty,DamageReturnUnitAutoId,  
	  ISNULL(MissingItemQty,0) as MissingItemQty,FreshReturnUnitAutoId,
      IsExchange,isnull(TaxValue,0.00) as TaxValue,ISNULL(isFreeItem,0) AS isFreeItem,                                      
      ISNULL(UnitMLQty,0) as UnitMLQty,ISNULL(TotalMLQty,0) as TotalMLQty,
	  (DOI.Del_MinPrice) as [MinPrice],DOI.BasePrice as BasePrice,                                      
      DOI.Item_TotalMLTax as TotalMLTax,isnull(Del_discount,0.00) as Discount,isnull(Del_ItemTotal,0.00) as ItemTotal                                    
      FROM [dbo].[Delivered_Order_Items] AS DOI                                       
      INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = DOI.[ProductAutoId] 
	  INNER JOIN [dbo].[PackingDetails] AS PD ON PD.ProductAutoId = DOI.ProductAutoId and DOI.UnitAutoId=pd.UnitType
      INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = DOI.UnitAutoId                                       
      INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId] WHERE [OrderAutoId]=@OrderAutoId                                      
                                      
      SET @CustomerAutoId = (SELECT [CustomerAutoId] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId)                                      
                                      
      SELECT OM.[AutoId],[OrderNo],CONVERT(VARCHAR(20),[OrderDate],101) AS OrderDate,DO.[AmtDue],DO.[GrandTotal],DO.[AmtPaid] ,                                    
    
	  otm.OrderType,DATEDIFF(dd,OrderDate,GETDATE()) as Aging, orderdate as orderdateSort                                      
      FROM [dbo].[OrderMaster] As OM                            
      INNER JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId]   
	   INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
      WHERE [CustomerAutoId] = @CustomerAutoId AND ([Status] = 6 or [Status] = 11) AND DO.[AmtDue] > 0.00                                      
      order by orderdateSort                                       
      
	  
                                            
     END                                      
    ELSE                                
     BEGIN                                      
      SELECT OM.[AutoId],OM.[OrderNo],CONVERT(VARCHAR(20),OM.[OrderDate], 101) AS OrderDate,                                     
      CONVERT(VARCHAR(20), OM.[DeliveryDate],101)--CONVERT(VARCHAR(20),OM.[DeliveryDate], 131)                                
      AS DeliveryDate,                                      
      CM.[AutoId] As CustAutoId,CM.[CustomerId],CM.[CustomerName],CM.[Contact1] + '/' + CM.[Contact2] As Contact,CTy.CustomerType,CT.[TermsDesc],ST.[ShippingType],ST.AutoId as ShipAutoId,                                     
       [dbo].[ConvertFirstLetterinCapital](BA.[Address]+(case when isnull(BA.Address2,'')!='' then ', '+BA.Address2 else '' end)+', '+BA.City)+', '+UPper(S.StateCode)+' - '+BA.Zipcode As BillAddr,
	  ST.[AutoId] as ShipAutoId,                                
      [dbo].[ConvertFirstLetterinCapital](SA.[Address]+(case when isnull(SA.Address2,'')!='' then ', '+SA.Address2 else '' end)+', '+SA.City)+', '+S1.StateCode+' - '+SA.Zipcode As ShipAddr,                                      
      SM.[StatusType] AS Status,OM.[TotalAmount],OM.[OverallDisc],OM.[OverallDiscAmt],                                 
      OM.[ShippingCharges],OM.[TotalTax],OM.[GrandTotal],OM.[PackedBoxes],EM.[FirstName] + ' ' + EM.[LastName] As SalesPerson,                                      
      EM1.[FirstName] + ' ' + EM1.[LastName] As PackerName,OM.[DrvRemarks],[CommentType],[Comment]                                      
      ,isnull(DeductionAmount,0.00) as DeductionAmount,                                      
            isnull(CreditAmount,0.00)as CreditAmount,isnull(PayableAmount,0.00) as PayableAmount                                      
            ,ISNULL((select t.CreditAmount from CustomerCreditMaster as t  where t.CustomerAutoId=om.CustomerAutoid),0.00) as CustomerCredit                                      
            ,(select TaxableType from TaxTypeMaster as ttm where ttm.Autoid=om.taxType) as TaxType,
			
			otm.OrderType,OrderRemarks ,ST.EnabledTax,                                   
            ISNULL(MLQty,0) as MLQty,isnull(MLTax,0) as MLTax,CM.ContactPersonName,ISNULL(AdjustmentAmt,0) as AdjustmentAmt,                                      
            CM.BusinessName,OM.Driver,isMLManualyApply,isnull(OM.Weigth_OZQty,0) as WeightOzQty,isnull(OM.Weigth_OZTaxAmount,0) as WeightOzTotalTax,
			dbo.UDF_CheckMLTax(OM.AutoId) as MLTaxStatus         
    FROM [dbo].[OrderMaster] As OM                                      
      INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.Status AND SM.[Category] = 'OrderMaster'                                      
      INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[AutoId] = OM.[BillAddrAutoId]                                      
      INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = OM.[ShipAddrAutoId]                           
      INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                                       
      INNER JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                                       
      INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                       
      INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]                                      
      LEFT JOIN [dbo].[CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                                      
      LEFT JOIN [dbo].[ShippingType] AS ST ON ST.[AutoId] = OM.[ShippingType]                                       
      left JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[SalesPersonAutoId]                                             
      left JOIN [dbo].[EmployeeMaster] AS EM1 ON EM1.[AutoId] = OM.[PackerAutoId] 
	     INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
	  WHERE OM.AutoId = @OrderAutoId                                      
                            
		SET @customerType=(select CustomerType from CustomerMaster where AutoId=@CustomerAutoId)
		
      SELECT CM.[CategoryName],CM.[CategoryId],OIM.ProductAutoId,PM.[ProductId],PM.[ProductName],PM.[ProductLocation],                                      
      OIM.[UnitTypeAutoId] AS UnitAutoId,UM.[UnitType],OIM.[QtyPerUnit],OIM.[UnitPrice],OIM.[TotalPieces],OIM.[SRP],OIM.[GP],OIM.[Tax],                                      
      OIM.[NetPrice],OIM.[Barcode],OIM.[QtyShip],OIM.[RequiredQty] ,convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice,                        
      OIM.IsExchange,isnull(TaxValue,0.00) as TaxValue,ISNULL(isFreeItem,0) AS isFreeItem,UnitMLQty,
	  (case @customertype when 2 then PD.whminprice when 3 then PD.costprice else [minprice] end) as [MinPrice],PD.Price as BasePrice,                                       
      ISNULL(UnitMLQty,0) as UnitMLQty,ISNULL(TotalMLQty,0) as TotalMLQty,                                      
      oim.Item_TotalMLTax as TotalMLTax,isnull(Oim_Discount,0.00) as Discount,isnull(Oim_ItemTotal,0.00) as ItemTotal                                      
      FROM [dbo].[OrderItemMaster] AS OIM                                       
      INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId] 
	   INNER JOIN [dbo].[PackingDetails] AS PD ON PD.ProductAutoId = OIM.ProductAutoId and OIM.UnitTypeAutoId=pd.UnitType
      INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitTypeAutoId                                       
      INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId] WHERE [OrderAutoId] = @OrderAutoId                                       
      ORDER BY CM.[CategoryName] ASC                                      
                                      
      SET @CustomerAutoId = (SELECT [CustomerAutoId] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId)                                      
                                      
      SELECT OM.[AutoId],[OrderNo],CONVERT(VARCHAR(20),[OrderDate],101) AS OrderDate,DO.[AmtDue],DO.[GrandTotal],DO.[AmtPaid] ,                                      
    

	 otm.OrderType, DATEDIFF(dd,OrderDate,GETDATE()) as Aging, orderdate as orderdateSort                                      
      FROM [dbo].[OrderMaster] As OM                                      
      INNER JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId]   
	      INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
      WHERE [CustomerAutoId] = @CustomerAutoId AND ([Status] = 6 or [Status] = 11 ) AND do.[AmtDue] > 0.00                                      
      order by orderdateSort                                       
                                              
     END                                           
                                          
    select OrderAutoId,CreditAutoId,CreditNo,CONVERT(VARCHAR(10),CreditDate,101) AS CreditDate,ISNULL(TotalAmount,0.00) as ReturnValue,ISNULL(PaidAmount,0.00)  as amtDeducted,                                      
    ISNULL(BalanceAmount,0.00) as amtDue                                      
                from CreditMemoMaster AS CM  where Status =3 --AND CustomerAutoId=@CustomerAutoId                                       
                AND OrderAutoId=@OrderAutoId                                       
                                      
  SELECT * FROM (                                                                                                                    
  SELECT 'Driver' as EmpType,DrvRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om                                                                        
  inner join EmployeeMaster as emp on emp.AutoId=om.Driver                                                              
  where om.AutoId=@OrderAutoId   and ISNULL(DrvRemarks,'')!=''    
  UNION                                                                                             
  SELECT ' Account' as EmpType,CancelRemark as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om                            
  inner join EmployeeMaster as emp on emp.AutoId=om.AccountAutoId where om.AutoId=@OrderAutoId                                      
  and ISNULL(CancelRemark,'')!=''  
  UNION                                                                                             
  SELECT 'Account' as EmpType,AcctRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om                            
  inner join EmployeeMaster as emp on emp.AutoId=om.AccountAutoId where om.AutoId=@OrderAutoId                                      
  and ISNULL(AcctRemarks,'')!=''                                                                                                               
                                                                                                                                                        
  UNION                                                                                                                
  SELECT 'Sales Person' as EmpType,OrderRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName  from OrderMaster as om                                  
  inner join EmployeeMaster as emp on emp.AutoId=om.SalesPersonAutoId where                                                                                                                 
  om.AutoId=@OrderAutoId            and ISNULL(OrderRemarks,'')!=''                       
  UNION                                                                                                                
  SELECT 'Packer' as EmpType,PackerRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om                                                                                             
  inner join EmployeeMaster as emp on emp.AutoId=om.PackerAutoId                                                                                                           
  where om.AutoId=@OrderAutoId          and ISNULL(PackerRemarks,'')!=''                                                                                               
  UNION                      
  SELECT 'Sales Manager' as EmpType,ManagerRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName  from OrderMaster as om                                                                                                                
  inner join EmployeeMaster as emp on emp.AutoId=om.ManagerAutoId                                      
  where om.AutoId=@OrderAutoId     and ISNULL(ManagerRemarks,'')!=''                                                                                                                
  UNION  
  SELECT 'Sales Manager' as EmpType,MLTaxRemark as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName  from OrderMaster as om                                                                                                                
  inner join EmployeeMaster as emp on emp.AutoId=om.ManagerAutoId                                      
  where om.AutoId=@OrderAutoId  and ISNULL(MLTaxRemark,'')!='' 
  UNION
  SELECT 'Warehouse Manager' as EmpType,WarehouseRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName as EmpName  from OrderMaster as om                                                                                                                
  inner join EmployeeMaster as emp on emp.AutoId=om.WarehouseAutoId                                                                                                                 
  where om.AutoId=@OrderAutoId             and ISNULL(WarehouseRemarks,'')!=''                                                                                                  
  ) AS T                                       
             SELECT [PackingId],CONVERT(VARCHAR,[PackingDate],101) As PkgDate,EM.[FirstName] + ' ' + EM.[LastName] As Packer                                       
    FROM [dbo].[GenPacking] AS GP                                      
    INNER JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId]=GP.[PackerAutoId] WHERE [OrderAutoId]=@OrderAutoId                                      
                                      
    select CreditAutoId,CreditNo,CONVERT(VARCHAR(10),CreditDate,101) AS CreditDate,                                      
    (SELECT COUNT(*) FROM CreditItemMaster AS CIM WHERE CIM.CreditAutoId=CM.CreditAutoId) AS NoofItem,                                      
    (SELECT SUM(NetAmount) FROM CreditItemMaster AS CIM WHERE CIM.CreditAutoId=CM.CreditAutoId) AS NetAmount                                      
    from CreditMemoMaster AS CM  where Status =1 AND CustomerAutoId=@CustomerAutoId  
END                                      

  
ELSE IF @Opcode=46
BEGIN
	SELECT * FROM (                                                                                                                    
	SELECT 'Driver' as EmpType,DrvRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om                                                                        
	inner join EmployeeMaster as emp on emp.AutoId=om.Driver                                                              
	where om.AutoId=@OrderAutoId  and ISNULL(DrvRemarks,'')!=''    
   
	union                                                                                        
	SELECT ' Account' as EmpType,CancelRemark as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om                            
	inner join EmployeeMaster as emp on emp.AutoId=om.AccountAutoId 
	where om.AutoId=@OrderAutoId  and ISNULL(CancelRemark,'')!=''  
  
	union                                                                                          
	SELECT 'Account' as EmpType,AcctRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om                            
	inner join EmployeeMaster as emp on emp.AutoId=om.AccountAutoId
	where om.AutoId=@OrderAutoId  and ISNULL(AcctRemarks,'')!=''                                                                                                               
                                                                                                                                                        
	union                                                                                                         
	SELECT 'Sales Person' as EmpType,OrderRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName  from OrderMaster as om                                  
	inner join EmployeeMaster as emp on emp.AutoId=om.SalesPersonAutoId where                                                                                                                 
	om.AutoId=@OrderAutoId  and ISNULL(OrderRemarks,'')!=''                       
   
	union                                                                                                           
	SELECT 'Packer' as EmpType,PackerRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om                                                                                             
	inner join EmployeeMaster as emp on emp.AutoId=om.PackerAutoId                                                                                                           
	where om.AutoId=@OrderAutoId and ISNULL(PackerRemarks,'')!=''                                                                                               
  
	union                
	SELECT 'Sales Manager' as EmpType,ManagerRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName  from OrderMaster as om                                                                                                                
	inner join EmployeeMaster as emp on emp.AutoId=om.ManagerAutoId                                      
	where om.AutoId=@OrderAutoId and ISNULL(ManagerRemarks,'')!=''                                                                                                                
  
	union
	SELECT 'Sales Manager' as EmpType,MLTaxRemark as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName  from OrderMaster as om                                                                                                                
	inner join EmployeeMaster as emp on emp.AutoId=om.ManagerAutoId                                      
	where om.AutoId=@OrderAutoId and ISNULL(MLTaxRemark,'')!='' 
  
	union
	SELECT 'Warehouse Manager' as EmpType,WarehouseRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName as EmpName  from OrderMaster as om                                                                                                                
	inner join EmployeeMaster as emp on emp.AutoId=om.WarehouseAutoId                                                                                                                 
	where om.AutoId=@OrderAutoId and ISNULL(WarehouseRemarks,'')!=''                                                                                                  
	) AS T       
END
                                
 
END TRY                                      
BEGIN CATCH                                      
Set @isException=1                                      
Set @exceptionMessage='Opps Some thing went wrong.Please try later'--ERROR_MESSAGE()                                
END CATCH                         
END   