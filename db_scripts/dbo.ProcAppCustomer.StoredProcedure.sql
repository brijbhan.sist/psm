USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcAppCustomer]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProcAppCustomer]            
@Opcode INT=Null,              
@SalesPersonAutoId INT=NULL,       
@RouteAutoId INT=NULL,    
@timeStamp datetime=NULL,         
          
@isException BIT OUT,            
@exceptionMessage VARCHAR(max) OUT            
AS            
BEGIN            
 BEGIN TRY            
  SET @isException=0            
  SET @exceptionMessage='Success!!'            
  IF @Opcode = 41               
  BEGIN          
      
      
   if(select EmpType from [EmployeeMaster] where autoid=@SalesPersonAutoId)=2    
   begin    

	select CustomerAutoId,isnull(COUNT(1),0) as TotalOrder,ISNULL(SUM(om.GrandTotal),0) as GrandTotal,ISNULL(sum(do.AmtPaid),0) as AmtPaid,      
	isnull(SUM(do.AmtDue),0) as AmtDue into #DueOrder from OrderMaster as om      
	inner join DeliveredOrders as do  on om.AutoId=do.OrderAutoId WHERE 
	[SalesPersonAutoId]=@SalesPersonAutoId and
	do.AmtDue > 0  and Status=11        
	Group by CustomerAutoId    

    SELECT CM.AutoId,[CustomerId],[CustomerName],[ContactPersonName],CM.[Email],CM.[AltEmail],CM.[Contact1],CM.[Contact2],MobileNo,FaxNo,          
    TaxId,[TermsDesc] as Terms,ISNULL( AmtDue,0.00) as DueBalanceAmount,  
    isnull((SELECT ccm.CreditAmount FROM CustomerCreditMaster AS CCM WHERE CCM.CustomerAutoId=CM.AutoId ),0.00) as StoreCreditAmount,   
    cm.[CustomerType],cty.[CustomerType] as CustomerTypeName,          
    DefaultBillAdd, DefaultShipAdd,CM.[SalesPersonAutoId],isnull([PriceLevelAutoId],'')[PriceLevelAutoId],isnull(PLM.[PriceLevelName],'')[PriceLevelName],       
    CM.[Status],SM.[StatusType],case when (select AutoId from RouteLog where RouteAutoId=@RouteAutoId and CustomerAutoId=CM.AutoId) is not null then 1 else 0 end as routestatus      
    into #temp FROM [dbo].[CustomerMaster] AS CM                  
    INNER JOIN [dbo].[EmployeeMaster] AS EM ON EM.AutoId = CM.[SalesPersonAutoId]            
    LEFT JOIN [dbo].[CustomerPriceLevel] AS CPL ON CPL.[CustomerAutoId] = CM.[AutoId]            
    LEFT JOIN [dbo].[PriceLevelMaster] AS PLM ON PLM.[AutoId] = CPL.[PriceLevelAutoId]            
    INNER JOIN [dbo].[StatusMaster] AS SM ON SM.[AutoId] = CM.[Status] AND SM.[Category] IS NULL            
    LEFT JOIN [dbo].[CustomerTerms] as CT on [TermsId]=[Terms]          
    INNER JOIN [dbo].[CustomerType] as CTY on CTY.AutoId=CM.[CustomerType]         
    left join #DueOrder as dom on dom.CustomerAutoId =cm.AutoId      
    WHERE CM.[SalesPersonAutoId]=@SalesPersonAutoId   and Cm.Status=1         
  
    select * from #temp          
            
    select AutoId, CustomerAutoId, Address, (SELECT [StateName] FROM [State] where Autoid=State) as State, City, Zipcode from [BillingAddress]           
    where customerautoid in (select autoid from #temp)          
  
    select AutoId, CustomerAutoId, Address, (SELECT [StateName] FROM [State] where Autoid=State) as State, City, Zipcode from [ShippingAddress]           
    where customerautoid in (select autoid from #temp)          
  
    select ppl.PriceLevelAutoId, CustomerAutoId, ProductAutoId, ppl.UnitAutoId as UnitType, ppl.CustomPrice from [ProductPricingInPriceLevel] as ppl        
    inner join [CustomerPriceLevel] as cpl on ppl.pricelevelautoid=cpl.pricelevelautoid        
    where customprice is not null and (select count(*) from #temp where Autoid=customerAutoId and [PriceLevelAutoId]=ppl.PriceLevelAutoId)>0        
   end    
   else if (select EmpType from [EmployeeMaster] where autoid=@SalesPersonAutoId)=5    
   begin   

   SELECT distinct iom.CustomerAutoId into #DriverCustomer FROM [dbo].[OrderMaster]   as iom
     WHERE (Driver = @SalesPersonAutoId) and ((Status in (4,5) and AssignDate<=GETDATE()) or (Status in (6,7) and convert(date, DelDate)=convert(date,GETDATE())))  

   select CustomerAutoId,isnull(COUNT(1),0) as TotalOrder,ISNULL(SUM(om.GrandTotal),0) as GrandTotal,ISNULL(sum(do.AmtPaid),0) as AmtPaid,      
   isnull(SUM(do.AmtDue),0) as AmtDue into #DueOrder1 from OrderMaster as om      
   inner join DeliveredOrders as do  on om.AutoId=do.OrderAutoId      
	and om.CustomerAutoId in (  
     select temp.CustomerAutoId from #DriverCustomer as temp
    ) and Status=11
   WHERE do.AmtDue > 0   
   Group by CustomerAutoId    
    
    SELECT CM.AutoId,[CustomerId],[CustomerName],[ContactPersonName],CM.[Email],CM.[AltEmail],CM.[Contact1],CM.[Contact2],MobileNo,FaxNo,          
    '' TaxId,'' as Terms,ISNULL( AmtDue,0.00) as DueBalanceAmount, '0.00' as StoreCreditAmount, cm.[CustomerType],cty.[CustomerType] as CustomerTypeName,          
    DefaultBillAdd, DefaultShipAdd,CM.[SalesPersonAutoId],isnull([PriceLevelAutoId],'')[PriceLevelAutoId],'' as [PriceLevelName],       
    CM.[Status],SM.[StatusType],case when (select AutoId from RouteLog where RouteAutoId=@RouteAutoId and CustomerAutoId=CM.AutoId) is not null then 1 else 0 end as routestatus        
    FROM [dbo].[CustomerMaster] AS CM                  
    INNER JOIN [dbo].[EmployeeMaster] AS EM ON EM.AutoId = CM.[SalesPersonAutoId]            
    LEFT JOIN [dbo].[CustomerPriceLevel] AS CPL ON CPL.[CustomerAutoId] = CM.[AutoId]            
    LEFT JOIN [dbo].[PriceLevelMaster] AS PLM ON PLM.[AutoId] = CPL.[PriceLevelAutoId]            
    INNER JOIN [dbo].[StatusMaster] AS SM ON SM.[AutoId] = CM.[Status] AND SM.[Category] IS NULL            
    LEFT JOIN [dbo].[CustomerTerms] as CT on [TermsId]=[Terms]          
    INNER JOIN [dbo].[CustomerType] as CTY on CTY.AutoId=CM.[CustomerType]         
    left join #DueOrder1 as dom on dom.CustomerAutoId =cm.AutoId      
    WHERE Cm.AutoId in (  
     select temp.CustomerAutoId from #DriverCustomer as temp
    )  and cm.Status=1  
      
    select AutoId, CustomerAutoId, Address, '' as State, City, Zipcode from [BillingAddress] where 1=0         
  
    select AutoId, CustomerAutoId, Address, '' as State, City, Zipcode from [ShippingAddress] where 1=0      
  
    select ppl.PriceLevelAutoId, '' as CustomerAutoId, ProductAutoId, '' as UnitType, ppl.CustomPrice   
    from [ProductPricingInPriceLevel] as ppl where 1=0    
      
   end   
     
   select OrderNo,OrderDate,dom.AmtDue,CustomerAutoId from DeliveredOrders as dom    
   inner join OrderMaster as om on dom.OrderAutoId=om.AutoId    
   where dom.AmtDue>0 and Status=11  
  
   select  CustomerAutoId,ProductAutoId,UnitTypeAutoId,RemainQty  from OrderMaster  as om  
   inner join OrderItemMaster as oim on oim.OrderAutoId=om.AutoId  
   where RemainQty>0 and om.AutoId in   
   (select max(om.AutoId) from OrderMaster as om    
   inner join CustomerMaster as cm on cm.AutoId=om.CustomerAutoId where cm.SalesPersonAutoId=@SalesPersonAutoId and cm.Status=1 and om.Status!=8  group by CustomerAutoId)  
     
  END            
 END TRY            
 BEGIN CATCH            
  SET @isException=1            
  SET @exceptionMessage=ERROR_MESSAGE()            
 END CATCH            
END   
GO
