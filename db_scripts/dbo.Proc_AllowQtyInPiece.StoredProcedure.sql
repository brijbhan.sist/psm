
ALTER proc [dbo].[Proc_AllowQtyInPiece]  
@Opcode int=null,  
@AutoId int=null,  
@SalesPersonAutoId int=null, 
@AllowDate datetime =null,  
@ProductAutoId int =null,  
@AllowQtyInPieces int=null,  
@UsedQty int=null,  
@RemainQty int=null,  
@FromDate date =NULL,          
@ToDate date =NULL,
@EmpId int=null,          
@PageIndex INT = 1,          
@PageSize INT = 10,                 
@isException BIT OUT,          
@exceptionMessage VARCHAR(max) OUT  
as  
begin  
 BEGIN TRY            
    SET @exceptionMessage= 'Success'            
    SET @isException=0   
if(@Opcode=41)  
 begin  
   
  Select AutoId as A,Convert(varchar(25),ProductId)+' '+ ProductName as PM   from [dbo].[ProductMaster] where ProductStatus=1 order by PM ASC
  for json path
 end  
ELSE if(@Opcode=42)  
 begin  
  Select AutoId as A,FirstName+' '+ISNULL(lastName,'') as SP   from EmployeeMaster where Status=1 and emptype=2 order by SP
  for json path
 end    
else if(@Opcode=11)  
	BEGIN    
	begin try  
	begin tran  
		  if(@AutoId=0)
		  begin
				IF NOT EXISTS(SELECT TOP 1 SalesPersonAutoId FROM [dbo].[AllowQtyPiece] WHERE SalesPersonAutoId=@SalesPersonAutoId AND ProductAutoId=@ProductAutoId)
				BEGIN
					INSERT INTO [dbo].[AllowQtyPiece]([SalesPersonAutoId],[AllowDate],[ProductAutoId],[AllowQtyInPieces],[CreatedOn],[CreatedBy])
					VALUES (@SalesPersonAutoId,case when convert(date,@AllowDate)>GETDATE() then @AllowDate else  convert(datetime,@AllowDate)+convert(datetime,convert(time,getdate())) end,@ProductAutoId,@AllowQtyInPieces,getdate(),@EmpId)  
					SET @exceptionMessage= 'Insert' 
					 INSERT into [AllowQtyPiece_Log]([SalesPersonAutoId],[AllowDate],[ProductAutoId],[AllowQtyInPieces],[UsedQty],[CreatedOn],[CreatedBy])
				  SELECT [SalesPersonAutoId],[AllowDate],[ProductAutoId],[AllowQtyInPieces],[UsedQty],[CreatedOn],[CreatedBy] from [AllowQtyPiece]
				  WHERE SalesPersonAutoId=@SalesPersonAutoId AND ProductAutoId=@ProductAutoId
				END
				ELSE
				BEGIN
				  INSERT into [AllowQtyPiece_Log]([SalesPersonAutoId],[AllowDate],[ProductAutoId],[AllowQtyInPieces],[UsedQty],[CreatedOn],[CreatedBy])
				  SELECT [SalesPersonAutoId],[AllowDate],[ProductAutoId],[AllowQtyInPieces],[UsedQty],[CreatedOn],[CreatedBy] from [AllowQtyPiece]
				  WHERE SalesPersonAutoId=@SalesPersonAutoId AND ProductAutoId=@ProductAutoId

				  DELETE from [dbo].[AllowQtyPiece] WHERE SalesPersonAutoId=@SalesPersonAutoId AND ProductAutoId=@ProductAutoId
				  and AutoId <(select max(AutoId) from [dbo].[AllowQtyPiece] WHERE SalesPersonAutoId=@SalesPersonAutoId 
				  AND ProductAutoId=@ProductAutoId)

				  UPDATE [dbo].[AllowQtyPiece] SET [AllowDate]=case when convert(date,@AllowDate)>GETDATE() then @AllowDate else  convert(datetime,@AllowDate)+convert(datetime,convert(time,getdate())) end,[AllowQtyInPieces] =@AllowQtyInPieces,
				  [UpdatedOn] = getdate(),[UpdatedBy] = @EmpId  WHERE SalesPersonAutoId=@SalesPersonAutoId AND ProductAutoId=@ProductAutoId
			
					SET @exceptionMessage= 'Update' 
				END   
		     End        
		  else
		  begin
			INSERT into [AllowQtyPiece_Log]([SalesPersonAutoId],[AllowDate],[ProductAutoId],[AllowQtyInPieces],[UsedQty],[CreatedOn],[CreatedBy])
			SELECT [SalesPersonAutoId],[AllowDate],[ProductAutoId],[AllowQtyInPieces],[UsedQty],[CreatedOn],[CreatedBy] from [AllowQtyPiece]
			WHERE SalesPersonAutoId=@SalesPersonAutoId AND ProductAutoId=@ProductAutoId

			DELETE from [dbo].[AllowQtyPiece] WHERE SalesPersonAutoId=@SalesPersonAutoId AND ProductAutoId=@ProductAutoId
			and AutoId <(select max(AutoId) from [dbo].[AllowQtyPiece] WHERE SalesPersonAutoId=@SalesPersonAutoId 
			AND ProductAutoId=@ProductAutoId)

			UPDATE [dbo].[AllowQtyPiece] SET [AllowQtyInPieces] = @AllowQtyInPieces ,[UpdatedOn] = getdate(),[UpdatedBy] = @EmpId  WHERE AutoId=@AutoId
			
			SET @exceptionMessage= 'Update' 
	      End

commit transaction   
	end try  
	begin catch  
	rollback tran  
	SET @isException=1                                                                                                            
	SET @exceptionMessage=ERROR_MESSAGE();    
	end catch  
	END    
else if(@Opcode=51)  
	begin 
	SELECT ROW_NUMBER() OVER(ORDER BY AutoId DESC) AS RowNumber, * INTO #Results FROM
	( 
	 Select ap.AutoId,(em.FirstName+' '+em.LastName)as EmpNmae,format(ap.AllowDate,'MM/dd/yyyy hh:mm tt')AllowDate,pm.ProductName,
	 pm.ProductId,AllowQtyInPieces,UsedQty,UM.UnitType+' ['+convert(varchar(50),PD.Qty)+' - PC]' as DefautlUnit,
	 (AllowQtyInPieces-UsedQty) AS RemainQty from [dbo].[AllowQtyPiece] as ap
	 inner join ProductMaster pm on pm.AutoId=ap.ProductAutoId
	 inner join EmployeeMaster em on em.AutoId=ap.SalesPersonAutoId
	 INNER JOIN UnitMaster as UM on UM.AutoId=Pm.PackingAutoId
	INNER JOIN PackingDetails as PD on PD.UnitType=Pm.PackingAutoId AND PD.ProductAutoId=pm.AutoId
	where (@SalesPersonAutoId is null or @SalesPersonAutoId=0 or ap.SalesPersonAutoId=@SalesPersonAutoId)
	and (@ProductAutoId is null or @ProductAutoId=0 or pm.AutoId=@ProductAutoId)
	and (@FromDate is null or @ToDate is null or (CONVERT(date,ap.AllowDate) 
	between convert(date,@FromDate) and CONVERT(date,@ToDate)))
	) as t  order by AutoId desc  
	 
	 SELECT COUNT(AutoId) AS RecordCount, case when @PageSize=0 then COUNT(AutoId) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Results
	 SELECT * FROM #Results
	 WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))
	end
	
	else if(@Opcode=52)
  begin
  
  Select pm.PackingAutoId,um.UnitType,pd.Qty from ProductMaster as pm
  inner join PackingDetails pd on pd.UnitType=pm.PackingAutoId and pd.ProductAutoId=pm.AutoId
  inner join UnitMaster um on pm.PackingAutoId=um.AutoId
  where pm.AutoId=@ProductAutoId
 
  end
  else if(@Opcode=53)
  begin
  Delete AllowQtyPiece where AutoId=@AutoId 
  end 
  
  else if(@Opcode=54)
  begin
  Select AutoId,SalesPersonAutoId,format(AllowDate,'MM/dd/yyyy hh:mm tt')as AllowDate,ProductAutoId,AllowQtyInPieces,UsedQty  from AllowQtyPiece where AutoId=@AutoId 
  end  
  else if(@Opcode=55)
  begin
	 Select @SalesPersonAutoId=SalesPersonAutoId,@AllowDate=AllowDate,@ProductAutoId=ProductAutoId  from AllowQtyPiece where AutoId=@AutoId 
	 Exec [dbo].ProcAllocationUsedQtyDetails @SalesPersonAutoId=@SalesPersonAutoId,@AllowDate=@AllowDate,@ProductAutoId=@ProductAutoId,@exceptionMessage='success',@isException=0
	 
	 Select Em.FirstName+' '+ISNULL(EM.LastName,'') as Name,PM.ProductName,format(AQP.AllowDate,'MM/dd/yyyy hh:mm tt') as AllowDate
	 FROM AllowQtyPiece as AQP
	 INNER JOIN EmployeeMaster as EM ON EM.AutoId=AQP.SalesPersonAutoId
	 INNER JOIN ProductMaster AS PM on PM.AutoId=AQP.ProductAutoId WHERE AQP.AutoId=@AutoId
  end  
 END TRY          
 BEGIN CATCH          
    SET @isException=1          
    SET @exceptionMessage='Oops! Something went wrong.Please try later.'             
 END CATCH   
end
GO
