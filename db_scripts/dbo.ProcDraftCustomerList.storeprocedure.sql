--Alter table DraftBillingAddress Add CompleteBillingAddress AS ([dbo].[FN_DraftCompleteBillingAddress]([AutoId]))
--Alter table DraftShippingAddress Add CompleteShippingAddress AS ([dbo].[FN_DraftCompleteShippingAddress] ([AutoId]))

alter PROCEDURE [dbo].[ProcDraftCustomerList]                                
@Opcode INT=Null,                                
@CustomerAutoId INT=NULL,                                
@StateAutoId int = null,                                
@CityAutoId int = null,
@PriceLevelAutoId int = null,                                                              
@CustomerName VARCHAR(50)=NULL,                                
@CustomerType int=NULL,                                
@Status INT=NULL,                                  
@EmpAutoId INT=NULL,
@SalesPersonAutoId INT=NULL,                            
@FromDate date =NULL,                                  
@ToDate date =NULL, 
@City varchar(50)=NULL,
@State varchar(150)=NULL,
@PageIndex INT=1,                                
@PageSize INT=10,                                
@RecordCount INT=null,                           
@CustomerList  VARCHAR(500)=NULL,                                
@isException BIT OUT,                                
@exceptionMessage VARCHAR(max) OUT                                
AS                                
BEGIN                                
 BEGIN TRY                                
 SET @isException=0                                
 SET @exceptionMessage='Success!!'    
 IF @Opcode = 31                                   
   BEGIN  
   delete from DraftBillingAddress where CustomerAutoId=@CustomerAutoId
   delete from DraftShippingAddress where CustomerAutoId=@CustomerAutoId
   delete from DraftCustomerContactPerson where CustomerAutoId=@CustomerAutoId
   delete from DraftCustomerMaster where AutoId=@CustomerAutoId
   END
  IF @Opcode = 41                                   
   BEGIN                                     
    SELECT ROW_NUMBER() OVER(ORDER BY CustomerId desc) AS RowNumber, * INTO #Results FROM                                
    (                                
	select DCM.AutoId as CustomerId,CustomerName,CT.CustomerType,DCM.Status,BusinessName,CTM.TermsDesc,OPTLicence,
	(EM1.FirstName+' '+EM1.LastName) as SalesPerson,(EM.FirstName+' '+EM.LastName) as CreatedBy,
	DBA.CompleteBillingAddress,DSA.CompleteShippingAddress,FORMAt(DCM.CreatedOn,'MM/dd/yyyy hh:mm tt') as CreatedDate
	from DraftCustomerMaster as DCM
	left join PriceLevelMaster PLM on PLM.AutoId=DCM.priceLevelId
	left join CustomerType CT on CT.AutoId=DCM.CustomerType
	inner join EmployeeMaster EM on EM.AutoId=DCM.CreatedBy
	inner join EmployeeMaster EM1 on EM1.AutoId=DCM.SalesPersonAutoId
	inner join DraftBillingAddress DBA on DBA.CustomerAutoId=DCM.AutoId
	inner join DraftShippingAddress DSA on DSA.CustomerAutoId=DCM.AutoId
	inner join CustomerTerms CTM on CTM.TermsId=DCM.Terms                             
    WHERE
	  (@CustomerName IS NULL OR @CustomerName='' OR DCM.CustomerName LIKE '%' + @CustomerName + '%')                                 
      AND (@SalesPersonAutoId IS NULL OR @SalesPersonAutoId=0 OR DCM.[SalesPersonAutoId]=@SalesPersonAutoId)  
	   AND (@CustomerType IS NULL OR @CustomerType=0 OR DCM.CustomerType=@CustomerType)  
      AND (ISNULL(@State,'')='' OR DBA.State=@State or DSA.State=@State) 
	  AND (ISNULL(@City,'')='' OR DBA.City=@City or DSA.City=@City)
    ) AS t ORDER BY CustomerId desc                           
                                
    SELECT COUNT([CustomerId]) AS RecordCount,case when @PageSize=0 then COUNT([CustomerId]) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Results                                
                                    
    SELECT * FROM #Results                                
    WHERE @PageSize=0 or RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1      
                                      
   END                        
  ELSE IF @Opcode = 42                          
   BEGIN                                      
		SELECT [AutoId], [FirstName] + ' ' +[LastName] AS Name FROM [dbo].[EmployeeMaster] WHERE [EmpType]=2 AND [Status] = 1       
		order by   [FirstName] + ' ' +[LastName]                           
		select[AutoId],[CustomerType] from CustomerType where AutoId not in (3) order by [CustomerType]  ASC                    
		Select [AutoId], [StateName] as StateName from [dbo].[State] order by StateName ASC     
		select CityMaster.[AutoId],CityMaster.[CityName] as City ,CityMaster.[CityName]+' ('+s.StateName+') ' as CityName from [dbo].[CityMaster]    
		INNER JOIN State s ON s.AutoId=CityMaster.StateId    
		order by replace(CityName,' ','') ASC                         
   END                                                        
 END TRY                                
 BEGIN CATCH                                
  SET @isException=1                                
  SET @exceptionMessage=ERROR_MESSAGE()                                
 END CATCH                                
END 
GO
