USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_DeliveredCreditMemoAmount]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 

CREATE FUNCTION  [dbo].[FN_DeliveredCreditMemoAmount]
(
	 @orderAutoId int
)
RETURNS decimal(10,2)
AS
BEGIN
	DECLARE @CreditMemoAmount decimal(10,2)
	set @CreditMemoAmount=isnull((select Deductionamount from OrderMaster where AutoId=@orderAutoId)	,0.00)
	RETURN @CreditMemoAmount
END

GO
