Alter procedure [dbo].[ProcDiscountReportByCustomer]                      
@Opcode INT=NULL,                                  
@CustomerAutoId int= null,                    
@EmpAutoId int = null,                      
@SalesPerson  varchar(200)=NULL,                            
@FromDate date =NULL,                      
@ToDate date =NULL,                      
@PageIndex INT=1,                      
@PageSize INT=10,                      
@RecordCount INT=null,                      
@isException bit out,                      
@exceptionMessage varchar(max) out                      
as                   
BEGIN                       
 BEGIN TRY                      
  Set @isException=0                      
  Set @exceptionMessage='Success' 
IF @OPCODE=42                     
  BEGIN                       
		SELECT ROW_NUMBER() OVER(ORDER BY CustomerId asc) AS RowNumber, * into #Results42                        
		FROM                      
		( 
		select distinct em.AutoId as SalesPersonAutoId,om.CustomerAutoId,
		format(om.OrderDate,'MM/dd/yyyy') as OrderDate,cm.CustomerId,cm.CustomerName,
		em.FirstName+' '+ISNULL(em.LastName,'') as FirstName,
		om.OrderNo,om.TotalAmount,om.OverallDisc,om.OverallDiscAmt 
		from OrderMaster as om
		inner join CustomerMaster as cm on cm.AutoId = om.CustomerAutoId
		inner join EmployeeMaster as em on em.AutoId = om.SalesPersonAutoId
		where (ISNULL(@FromDate,'')='') OR (ISNULL(@ToDate,'')='') OR (convert(date,om.OrderDate) between @FromDate and @ToDate)
		AND om.Status=11
		AND om.OverallDiscAmt > 0
		AND  ((ISNULL(@SalesPerson,'0')='0') or 
		(om.SalesPersonAutoId in (select * from dbo.fnSplitString(@SalesPerson,','))))  
		AND ((ISNULL(@CustomerAutoId,'0')='0') OR (om.CustomerAutoId=@CustomerAutoId))
		) AS T ORDER BY CustomerId asc                      
		
		
		SELECT ISNULL(SUM(TotalAmount),0.00) as TotalAmount,ISNULL(SUM(OverallDiscAmt),0.00) as OverallDiscAmt,
		(
		SELECT COUNT(CustomerId) AS RecordCount, case when @PageSize=0 then COUNT(CustomerId) else @PageSize end AS PageSize, @PageIndex AS PageIndex,FORMAT(getdate(),'MM/dd/yyyy hh:mm tt') as PrintDate FROM #Results42
		for json path
		) as  orderPaging,
		(
		SELECT * FROM #Results42                      
		WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))
		for json path
		) as  ReportList   
		FROM #Results42
		for json path
  END                                         
  IF @Opcode=45                      
  BEGIN                          
		SELECT AutoId AS EmpAutoId,FirstName + ' ' + LastName as EmpName FROM EmployeeMaster where EmpType=2 and status=1 order by EmpName ASC                              
		SELECT AutoId,StatusType AS StatusName FROM StatusMaster WHERE Category='OrderMaster'  order by StatusName ASC                  
		select AutoId,BrandName from BrandMaster where status = 1 order by BrandName 
		select AutoId,CustomerId+'-'+CustomerName as CustomerName from CustomerMaster  where Status=1 order by CustomerName         
  END                 
  IF @Opcode=46                      
  BEGIN   
		select cm.AutoId,CustomerId+'-'+CustomerName as CustomerName from CustomerMaster as cm
		Inner Join EmployeeMaster as em on em.AutoId=cm.SalesPersonAutoId
		where 
		((ISNULL(@SalesPerson,'0')='0') or 
		(cm.SalesPersonAutoId in (select * from dbo.fnSplitString(@SalesPerson,','))))
		AND cm.Status=1 order by CustomerName asc        
  END                  
  END TRY                      
 BEGIN CATCH                      
  Set @isException=1                      
  Set @exceptionMessage=ERROR_MESSAGE()                      
 END CATCH                      
END 
GO
