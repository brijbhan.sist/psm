CREATE
OR
alter procedure [dbo].[ProcIPAddressMaster]
@Opcode int = null,
@AutoId int = null,
@EmpAutoId int = null,
@IPAddress  varchar(100)= null,
@NameofLocation varchar(100)= null,
@Description  varchar(max)= null,
@PageIndex INT = 1,                                      
@PageSize INT = 10,                                      
@RecordCount INT =null,
@isException BIT out,
@exceptionMessage VARCHAR(max) OUT
as 
begin
BEGIN TRY
	SET @isException=0
	SET @exceptionMessage='Success'
If @Opcode = 11
	BEGIN
		IF  EXISTS (select IPAddress  from IpMaster where IPAddress = trim(@IPAddress))
		BEGIN
			SET @isException=1
			SET @exceptionMessage='IP address already exists.'
		END
		IF EXISTS (select IPAddress  from IpMaster where NameofLocation = trim(@NameofLocation))
		BEGIN
			SET @isException=1
			SET @exceptionMessage='Name of location already exists.'
		END
		ELSE
		BEGIN
			insert into IpMaster(IPAddress,NameofLocation,Description,CreatedDate,CreatedBy)values(trim(@IPAddress),@NameofLocation,@Description,GETDATE(),@EmpAutoId)	
		END
	END
If @Opcode = 21
	BEGIN	
		IF  EXISTS (select IPAddress  from IpMaster where IPAddress = trim(@IPAddress) and AutoId != @AutoId)
		BEGIN
			SET @isException=1
			SET @exceptionMessage='IP address already exists.'
		END
		ELSE IF EXISTS (select IPAddress  from IpMaster where NameofLocation = trim(@NameofLocation) and AutoId != @AutoId)
		BEGIN
			SET @isException=1
			SET @exceptionMessage='Name of location already exists.'
		END		
		ELSE
		BEGIN
			Update IpMaster set IPAddress=@IPAddress,NameofLocation=@NameofLocation,Description=@Description,
			UpdateBy=@EmpAutoId,UpdatedDate=GETDATE()
			where AutoId = @AutoId
		END
		
		
	END
If @Opcode = 31
	begin				
		delete IpMaster where AutoId = @AutoId	
	end
If @Opcode = 41
	begin		
		select AutoId,IPAddress,NameofLocation,Description  from IpMaster where AutoId = @AutoId
		
	end
If @Opcode = 42
	begin
	SELECT ROW_NUMBER() OVER(ORDER BY AutoId desc) AS RowNumber, * INTO #Results from                      
	(
		select AutoId,IPAddress,NameofLocation,Description from IpMaster  		
	) as t 
	SELECT COUNT(RowNumber) AS RecordCount,case when @PageSize=0 then COUNT(AutoId) else @PageSize end AS PageSize,@PageIndex as PageIndex FROM #Results           
                                    
	SELECT * FROM #Results                                    
	WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))                                     

	end
	End TRY
BEGIN CATCH
		SET @isException=1
		SET @exceptionMessage=ERROR_MESSAGE()
END CATCH
end 
