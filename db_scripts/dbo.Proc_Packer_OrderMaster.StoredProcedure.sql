CREATE OR Alter PROCEDURE [dbo].[Proc_Packer_OrderMaster]                                                                                    
@Opcode INT=NULL,                                                              
@OrderAutoId INT=NULL,                                                                                     
@CustomerAutoId  int=null,                                                                                  
@OrderNo VARCHAR(15)=NULL,                                                                                   
@OrderStatus INT=NULL,                                                                                 
@ProductAutoId INT=NULL,                                                                                    
@UnitAutoId INT=NULL,                                                                                     
@EmpAutoId INT=NULL,                                                                                     
@ReqQty  INT=NULL,                                 
@ShipQuantity INT=NULL, 
@EmployeeType INT=NULL, 
@ShippingAutoId INT=NULL, 
@QtyPerUnit INT=NULL,                                                                               
@IsExchange int =NULL,                                                                            
@IsFreeItem INT=NULL,                                                                                 
@SalesPersonAutoId INT=NULL,                                                                                    
@Remarks VARCHAR(max)=NULL,                                                                                      
@Todate DATETIME=NULL,                                                                                    
@CheckSecurity  VARCHAR(max)=NULL,                                                                                   
@Fromdate DATETIME=NULL,                                                                                      
@PageIndex INT = 1,                                                                                    
@PageSize INT = 10,                                                                                    
@RecordCount INT =null,                                                                                  
@BarCode VARCHAR(50)=NULL,                                                                       
@PkgId VARCHAR(12)=NULL,                                                                        
@PackerAutoId INT=NULL,                                                                         
@DTPackedItemsQty DT_PackedItemsQty readonly,                                                                     
@AllProduct DT_AllPackedItemDetails readonly, 
@PackedBoxes INT=NULL,                                                                         
@LogRemark varchar(200) = NULL,   
@isException bit out,                                                              
@exceptionMessage varchar(max) out,                                                              
@ShippingType varchar(max)=NULL                                    
AS                                  
BEGIN                                
  BEGIN TRY                               
  Set @isException=0                                   
  Set @exceptionMessage='Success'                                       
  declare @shipQty int =0                                            
  IF @Opcode=41                           
   BEGIN                                                                                     
     SELECT ROW_NUMBER() OVER(ORDER BY                                                                                                
    OrderName DESC,[OrderDate] asc,OrderNo asc                                    
    ) AS RowNumber, * INTO #Results from                                          
    (                                                                                                 
    SELECT OM.[AutoId],OM.OrderNo,CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,CM.[CustomerName] AS CustomerName,                                                                        
    SM.StatusType AS Status,OM.[Status] As StatusCode,0 as [GrandTotal],(emp.FirstName + ' '+ emp.LastName) as SalesPerson,                                             
    (SELECT COUNT(distinct [ProductAutoId]) from [dbo].[OrderItemMaster] WHERE OrderAutoId=OM.AutoId) AS NoOfItems ,                                                                                              
    (                                                              
      CASE       
   WHEN OM.[Status]= 9 THEN '4'                                                                                                 
   WHEN OM.[Status]= 10 THEN '2'                                                                                                
   WHEN OM.[Status]= 2 THEN '3'                                                                                                
   WHEN OM.[Status]= 3 THEN '1'                                                                          
     END                                                                                                  
    ) OrderName,                                                                      
    (select COUNT(1) from CreditMemoMaster as cmm where (Status=1 or Status=3)  and OrderAutoId is null                                                                                              
    and cmm.CustomerAutoId = OM.CustomerAutoId ) as CreditMemo,                                                            
  (Select ShippingType from ShippingType Where AutoId=om.ShippingType) as ShippingType,OM.ShippingType  as ShipId                                                                     
    FROM [dbo].[OrderMaster] As OM                                                                                              
    INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.AutoId = OM.CustomerAutoId AND CM.Status=1                                                                                              
    INNER JOIN [dbo].[EmployeeMaster] AS emp ON emp.AutoId = OM.SalesPersonAutoId                                                                                               
    INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.[Status] AND SM.Category = 'OrderMaster'                                                                                                         
    WHERE (@OrderNo is null or @OrderNo ='' or OrderNo like '%' + @OrderNo + '%')                                                                                              
    and (@CustomerAutoId is null or @CustomerAutoId =0 or CustomerAutoId =@CustomerAutoId)                                                                                              
   and (@FromDate is null or @FromDate = '' or @Todate is null or @Todate = '' or                                       
    (CONVERT(date,OM.[OrderDate])  between CONVERT(date,@FromDate) and CONVERT(date,@Todate)))                                                                               
    and (@OrderStatus is null or @OrderStatus=0 or OM.[Status]=@OrderStatus)                                                                                              
    and PackerAutoId=@EmpAutoId and ISNULL(PackerAssignStatus,0)=1                                                       
    and OM.[Status] in (2,3,9,10)                                                                            
    and (@SalesPersonAutoId = 0 or om.[SalesPersonAutoId]=@SalesPersonAutoId)                                                                
 --Start                                                         
 and (ISNULL(@ShippingType,'0')='0' or ISNULL(@ShippingType,'0,')='0,' or OM.ShippingType in (select * from dbo.fnSplitString(@ShippingType,',')))                                                                
                                                                                     
    )as t order by   OrderName  DESC                                                                                              
                              
    SELECT COUNT(*) as RecordCount,case when @PageSize=0 then COUNT(*) else @PageSize end   as PageSize,@PageIndex as PageIndex FROM #Results                               
    SELECT * FROM #Results                                                                                              
   WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))                                                                               
   END                                                                                    
  ELSE IF @Opcode=42                                                                                    
   BEGIN      
      SET @CustomerAutoId=(SELECT CustomerAutoId FROM OrderMaster WHERE AutoId=@OrderAutoId)       
      SELECT OM.[AutoId],OM.[OrderNo],CONVERT(VARCHAR(20),OM.[OrderDate], 101) AS OrderDate,                                                                                    
      CONVERT(VARCHAR(20), OM.[DeliveryDate],101)--CONVERT(VARCHAR(20),OM.[DeliveryDate], 131)                                                                                     
      AS DeliveryDate,                                                                     
      CM.[AutoId] As CustAutoId,CM.[CustomerId],CM.CustomerType,CTy.CustomerType as CustomerTypeName,CM.[CustomerName],
	  CM.[Contact1] + '/' + CM.[Contact2] As Contact,CTy.CustomerType,CT.[TermsDesc],ST.[ShippingType],
	  ST.AutoId as ShippingAutoId,
	   [dbo].[ConvertFirstLetterinCapital](BA.[Address]+(case when isnull(BA.Address2,'')!='' then ', '+BA.Address2 else '' end)+', '+BA.City)+', '+UPper(S.StateCode)+' - '+BA.Zipcode As BillAddr,
       [dbo].[ConvertFirstLetterinCapital](SA.[Address]+(case when isnull(SA.Address2,'')!='' then ', '+SA.Address2 else '' end)+', '+SA.City)+', '+S1.StateCode+' - '+SA.Zipcode As ShipAddr,                                                                                    
      OM.[Status] As StatusCode,SM.[StatusType] AS Status,                                                                                    
      OM.[PackedBoxes],EM.[FirstName] + ' ' + EM.[LastName] As SalesPerson,OM.PackerRemarks,                                                                                    
      EM1.[FirstName] + ' ' + EM1.[LastName] As PackerName,OM.[DrvRemarks],[CommentType],[Comment]                                                                                    
      ,(select TaxableType from TaxTypeMaster as ttm where ttm.Autoid=om.taxType) as TaxType,otm.OrderType AS OrderType,OrderRemarks ,                                                                                 
	  CM.ContactPersonName,CM.BusinessName,OM.Driver,(select EM1.[FirstName] + ' ' + EM1.[LastName] from [dbo].[EmployeeMaster] AS EM1 where EM1.[AutoId]                                                                                           
	  = OM.warehouseAutoid) AS WarehouseName,replace(lower(DB_NAME()),'.a1whm.com','') as DBName,ST.AutoId as ShippingAutoId
	  FROM [dbo].[OrderMaster] As OM                                                                                    
	  INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.Status AND SM.[Category] = 'OrderMaster'                                     
      INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[AutoId] = OM.[BillAddrAutoId]                               
      INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = OM.[ShipAddrAutoId]                                                                               
      INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                                                                              
      INNER JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                                                        
      INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                                   
      INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]                     
      LEFT JOIN [dbo].[CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                                                                                    
      LEFT JOIN [dbo].[ShippingType] AS ST ON ST.[AutoId] = OM.[ShippingType]                                                                                     
      left JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[SalesPersonAutoId]                                                                                   
      left JOIN [dbo].[EmployeeMaster] AS EM1 ON EM1.[AutoId] = OM.[PackerAutoId]      
   INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'      
   WHERE OM.AutoId = @OrderAutoId                                                                                    
                                                                                  
      SELECT OIM.AutoId as OIAutoId,OIM.ProductAutoId,PM.[ProductId],PM.[ProductName],'http://'+DB_NAME()+PM.ImageUrl as ImageUrl,PM.ThumbnailImageUrl,                                                                                   
      OIM.[UnitTypeAutoId] AS UnitAutoId,UM.[UnitType],OIM.[QtyPerUnit],(ISNULL(QtyShip,0)*OIM.[QtyPerUnit]) AS [TotalPieces],OIM.[Barcode],OIM.[QtyShip],OIM.[RequiredQty] ,                                                                                  
      OIM.IsExchange,ISNULL(isFreeItem,0) AS isFreeItem, ISNULL(Tax,0) AS Tax, (oim.RequiredQty-ISNULL(QtyShip,0)) as QtyRemain,iSNULL(AddOnQty,0) as AddOnQty,OIM.[TotalPieces] as UIM,
	  ISNULL((select top 1 Location  from PackingDetails as pd where pd.ProductAutoId=PM.AutoId and                                                                                                                                                    
		pd.UnitType=OIM.UnitTypeAutoId),'') AS [ProductLocation],ISNULL(oim.AddOnPackedPeice,0) as AddOnPackedPeice,pm.Stock
      FROM [dbo].[OrderItemMaster] AS OIM                                                                                     
      INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                                                                                    
      INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitTypeAutoId                                                                            
      INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId] WHERE [OrderAutoId] = @OrderAutoId                                                                                     
      ORDER BY [ProductLocation],PM.[ProductId],PM.[ProductName] ASC                                                                      
                                                                                    
                                                                                     
                                                                                    
    SELECT * FROM (                                                                                         
         SELECT 'Driver' as EmpType,DrvRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om                                               
         inner join EmployeeMaster as emp on emp.AutoId=om.Driver                                                                                         
         where om.AutoId=@OrderAutoId   and ISNULL(DrvRemarks,'')!=''                                                   
    UNION                                                                                                                                  
         SELECT 'Account' as EmpType,AcctRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om                                                                                                              
         inner join EmployeeMaster as emp on emp.AutoId=om.AccountAutoId where om.AutoId=@OrderAutoId                                                        
      and ISNULL(AcctRemarks,'')!=''                                                                                 
                          
    UNION                                                                                          
         SELECT 'Sales Person' as EmpType,OrderRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName  from OrderMaster as om                                                                                                                     
  
     
     
         inner join EmployeeMaster as emp on emp.AutoId=om.SalesPersonAutoId where                                                                                                             
         om.AutoId=@OrderAutoId            and ISNULL(OrderRemarks,'')!=''                                                                                      
    UNION                                                                                                                                  
         SELECT 'Packer' as EmpType,PackerRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om                                                                                                                            
  
    
      
         inner join EmployeeMaster as emp on emp.AutoId=om.PackerAutoId                                                                                                                             
          where om.AutoId=@OrderAutoId          and ISNULL(PackerRemarks,'')!=''                                                                                                    
     UNION                                                                               
         SELECT 'Manager' as EmpType,ManagerRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName  from OrderMaster as om                                                                                                                         
  
    
      
         inner join EmployeeMaster as emp on emp.AutoId=om.ManagerAutoId                                                                                                            
         where om.AutoId=@OrderAutoId     and ISNULL(ManagerRemarks,'')!=''                                                                                                                                  
         UNION                                                                                     
         SELECT 'Warehouse' as EmpType,WarehouseRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName as EmpName  from OrderMaster as om                                                                  
         inner join EmployeeMaster as emp on emp.AutoId=om.WarehouseAutoId                                                                                                                                   
         where om.AutoId=@OrderAutoId             and ISNULL(WarehouseRemarks,'')!=''                                                                                                          
     ) AS T        
        
    SELECT [PackingId],CONVERT(VARCHAR,[PackingDate],101) As PkgDate,EM.[FirstName] + ' ' + EM.[LastName] As Packer                                                    
    FROM [dbo].[GenPacking] AS GP                                                                                    
    INNER JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId]=GP.[PackerAutoId] WHERE [OrderAutoId]=@OrderAutoId       
        
	  if exists(select status from OrderMaster where AutoId=@OrderAutoId and Status in (2))                                                                              
	  begin        
		  select distinct pm.AutoId,Convert(varchar(20),ProductId)+' '+ProductName as ProductName,OIM.IsExchange,ISNULL(isFreeItem,0) AS isFreeItem, ISNULL(Tax,0) AS Tax,      
		  UnitTypeAutoId from OrderItemMaster as oim                                                                                              
		  inner join ProductMaster as pm on pm.AutoId=oim.ProductAutoId                                                                                              
		  left join ItemBarcode as ib on ib.ProductAutoId=oim.ProductAutoId                                                                                               
		  and ib.UnitAutoId=oim.UnitTypeAutoId                                                                                              
		  where OrderAutoId=@OrderAutoId and (RequiredQty-ISNULL(QtyShip,0))!=0
		  ORDER BY ProductName ASC         
	  end 
	  ELSE   if exists(select status from OrderMaster where AutoId=@OrderAutoId and Status in (9))                                                                              
	  begin        
		  select distinct pm.AutoId,Convert(varchar(20),ProductId)+' '+ProductName as ProductName,OIM.IsExchange,ISNULL(isFreeItem,0) AS isFreeItem, ISNULL(Tax,0) AS Tax,      
		  UnitTypeAutoId from OrderItemMaster as oim                                                                                              
		  inner join ProductMaster as pm on pm.AutoId=oim.ProductAutoId                                                                                              
		  left join ItemBarcode as ib on ib.ProductAutoId=oim.ProductAutoId                                                                                               
		  and ib.UnitAutoId=oim.UnitTypeAutoId                                                                                              
		  where OrderAutoId=@OrderAutoId and ISNULL(AddOnQty,0)>0 and (RequiredQty-ISNULL(QtyShip,0))!=0
		  ORDER BY ProductName ASC         
	  end                                                                               
                                                     
 END                                                                                   
                                                 
  ELSE IF @Opcode=43                                                                                    
  BEGIN                                                                                    
    SELECT [AutoId],[StatusType] FROM [dbo].[StatusMaster] WHERE   [Category]='OrderMaster'   and autoid in(2,3,9,10)  order by [StatusType]                                    
    SELECT [AutoId],[CustomerId] + ' - ' + [CustomerName] As Customer FROM [dbo].[CustomerMaster]  order by   Customer                                                                                
    SELECT AutoId,(FirstName +' '+ISNULL(LastName,'')) AS EmpName FROM EmployeeMaster WHERE EmpType =2 order by EmpName ASC                                                                                  
    SELECT AutoId,(FirstName +' '+ISNULL(LastName,'')) AS EmpName FROM EmployeeMaster WHERE EmpType =5 order by EmpName ASC                                                             
 --Start                                                               
  SELECT AutoId,ShippingType FROM ShippingType order by ShippingType asc                                                                    
 --End 28-08-2019 By Rizwan Ahmad                                                                                         
  END                                                                               
  ELSE IF @Opcode=44                                      
   BEGIN                                                                                              
    SELECT [SecurityValue] FROM [dbo].[TBL_MST_SecurityMaster] WHERE [SecurityValue]=@CheckSecurity and SecurityType=3 and typeEvent=1                                                                                             
   END                                                                         
 ELSE IF @Opcode=21                                                                        
 BEGIN                                                                         
	if (select count(ProductAutoId) FROM [dbo].[ItemBarcode] AS IB                                                                                          
	INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = IB.[ProductAutoId] WHERE ProductAutoId=@ProductAutoId                                                                                           
	and IB.UnitAutoId=@UnitAutoId and Barcode!='' )>0                                                                  
	BEGIN                                                                      
		DECLARE @Stock int 
		
		SELECT top 1 @Stock=PM.[Stock] FROM [dbo].[ItemBarcode] AS IB                                                                                          
		INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = IB.[ProductAutoId] WHERE ProductAutoId=@ProductAutoId                                                                                           
		and IB.UnitAutoId=@UnitAutoId and Barcode!='' 

		SET @BarCode=ISNULL((SELECT TOP 1 Barcode From
		(select Top 2 AutoId,Barcode FROM OrderitemMaster where ProductAutoId=@ProductAutoId AND UnitTypeAutoId=@UnitAutoId and ISNULL(barcode,'')!='' order by AutoId desc) x                     
		order by x.AutoId),'')
		
		if(select COUNT(ProductAutoId) from OrderItemMaster  where                                                                        
		OrderAutoId=@OrderAutoId and ProductAutoId=@ProductAutoId and UnitTypeAutoId=@UnitAutoId and isFreeItem=@IsFreeItem                                   
		and IsExchange=@IsExchange )>0                                                                  
		Begin  
		
			set @shipQty =(select top 1 (@ReqQty+ISNULL(QtyShip,0))* QtyPerUnit from OrderItemMaster  where                                                                        
			OrderAutoId=@OrderAutoId and ProductAutoId=@ProductAutoId and UnitTypeAutoId=@UnitAutoId and isFreeItem=@IsFreeItem                                                                  
			and IsExchange=@IsExchange)  
			DECLARE @productId int=(select ProductId from ProductMaster where AutoId=@ProductAutoId)
			SET @shipQty=@shipQty-ISNULL((select SUM(OledStock-NewStock) from ProductStockUpdateLog where ProductId=@productId and ReferenceType='OrderMaster' and ReferenceId=@OrderAutoId),0)
			IF(ISNULL(@Stock,0)>=ISNULL(@shipQty,0))                                    
			BEGIN                                                                          
				IF EXISTS( SELECT 1 FROM OrderItemMaster where                                                                        
				OrderAutoId=@OrderAutoId and ProductAutoId=@ProductAutoId and UnitTypeAutoId=@UnitAutoId and isFreeItem=@IsFreeItem                                                                        
				and IsExchange=@IsExchange )                                                                        
				BEGIN                                                      
					Declare @Check INT                                                        
					SELECT @Check=dbo.FN_GetSalesPerson(@OrderAutoId,@ProductAutoId,@UnitAutoId,@ReqQty,@IsFreeItem,@IsExchange) --Updated on  11/12/2019 12:00 PM By Rizwan Ahmad                                                          
					IF (@Check=1)                                                      
					BEGIN                                                              
						update OrderItemMaster set                                                                         
						QtyShip= case when RequiredQty< ISNULL(QtyShip,0)+@ReqQty then RequiredQty else ISNULL(QtyShip,0)+@ReqQty end,                                                                      
						RemainQty= RequiredQty- (case when RequiredQty< ISNULL(QtyShip,0)+@ReqQty then RequiredQty else ISNULL(QtyShip,0)+@ReqQty end)                                                     
						,Barcode=@BarCode,ManualScan=1,AddedItemKey=@CheckSecurity where                                                                        
						OrderAutoId=@OrderAutoId and ProductAutoId=@ProductAutoId and UnitTypeAutoId=@UnitAutoId and isFreeItem=@IsFreeItem                                                 
						and IsExchange=@IsExchange                                               
                                                
						Declare @DefaultUnit int,@UpdateQty INT,@UsedQty INT --Added on 11/13/2019 at 05:02 AM for update allocated qty in piece                                            
                                                
						SELECT @DefaultUnit=QtyPerUnit from OrderItemMaster WHERE  OrderAutoId=@OrderAutoId and ProductAutoId=@ProductAutoId and                                             
						UnitTypeAutoId=@UnitAutoId and isFreeItem=@IsFreeItem and IsExchange=@IsExchange                                             
                                            
						Select @SalesPersonAutoId=SalesPersonAutoId from OrderMaster Where AutoId=@OrderAutoId                                            
                                              
                                             
						SET @UpdateQty=@DefaultUnit*@ReqQty                                          
						Select @Stock as Stock,@BarCode as Barcode,@UpdateQty as AddOnQty    
						                                                     
					END                                                    
					ELSE                                                    
					BEGIN                                  
						set @isException=1                                                                  
						set @exceptionMessage='Less' --Available qty is less than requested qty for product allocation process                                                    
					END                                                   
				end                                               
			END                                                                  
			ELSE                                                                  
			BEGIN                                                                  
				set @isException=1                      
				set @exceptionMessage='Stock'                                                               
			END                                                            
		END                                                                  
		Else                                             
		Begin                  
			set @isException=1                                                     
			set @exceptionMessage='Item'                                                                  
		End                                                                  
	END                                                                  
	ELSE                                                                  
	BEGIN                            
		set @isException=1                                                      
		set @exceptionMessage='Barcode'                                                                  
	END                                                                             
 END                                                            
 ELSE IF @Opcode=45                                                                          
 BEGIN     
	
	  Insert into PackerInvalidBarcodeDetails (PackerAutoiD,OrderAutoId,Barcode,ScanDate) 
      values ((select packerAutoId from OrderMaster where AutoId=@OrderAutoId),@OrderAutoId,@BarCode,getdate())


	IF EXISTS(select AutoId from ItemBarcode where Barcode=@BarCode)                                                                    
	BEGIN   
		DECLARE @productId45 int
		SELECT @productId45=ProductId,@ProductAutoId=[ProductAutoId],@UnitAutoId=[UnitAutoId],@Stock=PM.[Stock] FROM [dbo].[ItemBarcode] AS IB                                                   
		INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = IB.[ProductAutoId] WHERE [Barcode]=@Barcode       
        
		set @shipQty =ISNULL((select ((@ReqQty+ISNULL(QtyShip,0)) * QtyPerUnit) from OrderItemMaster  where                                                                          
		OrderAutoId=@OrderAutoId and ProductAutoId=@ProductAutoId and UnitTypeAutoId=@UnitAutoId and isFreeItem=@IsFreeItem                                    
		and IsExchange=@IsExchange),0) 
        
		IF EXISTS( SELECT 1 FROM OrderItemMaster where                                                                          
		OrderAutoId=@OrderAutoId and ProductAutoId=@ProductAutoId and UnitTypeAutoId=@UnitAutoId and isFreeItem=@IsFreeItem                                                                          
		and IsExchange=@IsExchange)                                                                     
		BEGIN   

			SET @shipQty=@shipQty-ISNULL((select SUM(OledStock-NewStock) from ProductStockUpdateLog 
			where ProductId=@productId45 and ReferenceType='OrderMaster' and ReferenceId=@OrderAutoId),0)

			IF(ISNULL(@Stock,0)>=@shipQty)                                                                             
			BEGIN
				Declare @Checkb INT                                                        
				SELECT @Checkb=dbo.FN_GetSalesPerson(@OrderAutoId,@ProductAutoId,@UnitAutoId,@ReqQty,@IsFreeItem,@IsExchange)                                                           
				IF (@Checkb=1)                                                      
				BEGIN                                
					update OrderItemMaster set                                                      
					QtyShip= case when RequiredQty< ISNULL(QtyShip,0)+@ReqQty then RequiredQty else ISNULL(QtyShip,0)+@ReqQty end                                                                           
					,Barcode=@BarCode where                                                                          
					OrderAutoId=@OrderAutoId and ProductAutoId=@ProductAutoId and UnitTypeAutoId=@UnitAutoId and isFreeItem=@IsFreeItem                                                                          
					and IsExchange=@IsExchange                                                  

					SELECT @DefaultUnit=QtyPerUnit from OrderItemMaster WHERE  OrderAutoId=@OrderAutoId and ProductAutoId=@ProductAutoId and                                             
					UnitTypeAutoId=@UnitAutoId and isFreeItem=@IsFreeItem and IsExchange=@IsExchange                                             
                                            
					Select @SalesPersonAutoId=SalesPersonAutoId from OrderMaster Where AutoId=@OrderAutoId                                 
                                                
					SET @UpdateQty=@DefaultUnit*@ReqQty                                            
					IF EXISTS(select * from AllowQtyPiece where CAST(AllowDate AS date) <= CAST(getdate() AS DATE) AND SalesPersonAutoId=@SalesPersonAutoId                                         
					AND ProductAutoId=@ProductAutoId and AllowQtyInPieces> UsedQty)                                        
					BEGIN                            
						select @ProductAutoId AS [ProductAutoId],@UnitAutoId AS [UnitAutoId],@Stock as Stock,@BarCode as Barcode                                                     
                	END                                                                         
					select @ProductAutoId AS [ProductAutoId],@UnitAutoId AS [UnitAutoId],@Stock as Stock,@BarCode as Barcode                                                     
				END                                                    
				ELSE                                                    
				BEGIN                                                    
					set @isException=1                                                                  
					set @exceptionMessage='Less'                                                   
				END                                                                          
			END                                                                      
			ELSE                                                                    
			BEGIN                                                                    
				SET @isException=1                                                                    
				SET @exceptionMessage= 'Stock'                                                                    
			END                                                                     
		end                                                                  
		ELSE                                                              
		BEGIN          
		if (SELECT IsExchange FROM OrderItemMaster where OrderAutoId=@OrderAutoId and ProductAutoId=@ProductAutoId and UnitTypeAutoId=@UnitAutoId)=1
		begin
		    SET @isException=1                                                                 
			SET @exceptionMessage= 'Exchange'  
		end
		else if(SELECT isFreeItem FROM OrderItemMaster where OrderAutoId=@OrderAutoId and ProductAutoId=@ProductAutoId and UnitTypeAutoId=@UnitAutoId)=1
		begin
		    SET @isException=1                                                                 
			SET @exceptionMessage= 'FreeItem' 
		end
		else
		begin
		    SET @isException=1                                                                 
			SET @exceptionMessage= 'Item' 
		end
			                                                                  
		END                                                                  
	END                                                                    
	ELSE                                                                    
	BEGIN	
	    Insert into PackerInvalidBarcodeDetails (PackerAutoiD,OrderAutoId,Barcode,ScanDate) 
		values ((select packerAutoId from OrderMaster where AutoId=@OrderAutoId),@OrderAutoId,@BarCode,getdate())
		SET @isException=1                         
		SET @exceptionMessage= 'Barcode'                                                                    
	END                                                                
 END                                                                            
   ELSE IF @Opcode=104                                                                                                                                         
 BEGIN                                             
    SELECT UM.AutoId AS AutoId, UM.[UnitType] AS UnitType, [Qty],ISNULL(EligibleforFree,0) as EligibleforFree FROM [dbo].[PackingDetails] AS PD                                                                       
    INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = PD.UnitType WHERE PD.[ProductAutoId] = @ProductAutoId                                                                                                                     
   -- SELECT PackingAutoId AS AutoId FROM ProductMaster  WHERE  AutoId=@ProductAutoId                                
  END                                                         
 ELSE IF @Opcode=105                                                                                                                  
 BEGIN                                                                                                                                                                       
     DECLARE @UnitPrice decimal(14,4)=(select UnitPrice/ cast(QtyPerUnit as decimal(10,2)) from OrderItemMaster where AutoId=@OrderAutoId) 
	 DECLARE @MinPrice decimal(14,4)=ISNULL((select OM_MinPrice/ cast(QtyPerUnit as decimal(10,2)) from OrderItemMaster where AutoId=@OrderAutoId),0) 
	 DECLARE @CostPrice decimal(14,4)=ISNULL((select OM_CostPrice/ cast(QtyPerUnit as decimal(10,2)) from OrderItemMaster where AutoId=@OrderAutoId),0) 
     update OrderItemMaster set UnitTypeAutoId=@UnitAutoId,QtyPerUnit=@QtyPerUnit,UnitPrice=(@UnitPrice*@QtyPerUnit),RequiredQty=@ReqQty,      
	 QtyShip=0,Barcode='',OM_MinPrice=@MinPrice*@QtyPerUnit,OM_CostPrice=@CostPrice*@QtyPerUnit where AutoId=@OrderAutoId                                                                      
 END                                                                   
  ELSE IF @Opcode=103                                                                                                              
   BEGIN                                                                                                              
    BEGIN TRY                                                                                     
	BEGIN TRAN       
	Declare @SPId INT,@Packer INT,@MasterAutoid int      
	SELECT @SPId=SalesPersonAutoId from OrderMaster where AutoId=@OrderAutoId  

	
	INSERT INTO PackerLogMaster (OrderAutoid,PackDateTime) VALUES (@OrderAutoId,GETDATE())
	SET @MasterAutoid=SCOPE_IDENTITY()

	INSERT INTO PackerLogItemMaster (MasterAutoId,ProductAutoId,UnitAutoId,QtyShip,Pcs,RemainQty) 
	Select @MasterAutoid,ProductAutoId,UnitAutoId,QtyShip,Pcs,RemainQty FROm @DTPackedItemsQty 

	INSERT INTO ProductStockLogMaster (MasterAutoId,ProductAutoId,CurrentStock) 
	Select @MasterAutoid,pm.AutoId,pm.Stock from @DTPackedItemsQty as dt      
	inner join ProductMaster pm on pm.AutoId=dt.[ProductAutoId]      
	inner join PackingDetails pd on pd.ProductAutoId=dt.[ProductAutoId] and pd.UnitType=dt.[UnitAutoId]      
	group by ProductId,pm.AutoId,pm.Stock 

	Insert INTO  AllPackedItemDetails  (MasterAutoId, ProductAutoId , UnitAutoId , Barcode ,
	QtyShip , Pcs , OldPacked , RemainQty , isfreeitem , IsExchange ) 
	Select @MasterAutoid,ProductAutoId,UnitAutoId,Barcode,
	QtyShip,Pcs,OldPacked,RemainQty,isfreeitem ,IsExchange FROM @AllProduct



	IF EXISTS(Select AutoId FROM EmployeeMaster Where AutoId=@EmpAutoId AND EmpType=3)
	BEGIN
		IF Exists(Select AutoId from OrderMaster where AutoId=@OrderAutoId and Status in (2,9))       
		begin      
			IF Exists(      
				select top 1 t.Pcs from (
					Select ProductId,pm.Stock,SUM(ISNULL([Pcs],0)) as Pcs,SUM(isnull(OldPacked,0)) as OldPacked
					from @DTPackedItemsQty as dt      
					inner join ProductMaster pm on pm.AutoId=dt.[ProductAutoId]      
					inner join PackingDetails pd on pd.ProductAutoId=dt.[ProductAutoId] and pd.UnitType=dt.[UnitAutoId]      
					group by ProductId,pm.Stock 
				) as t where stock<pcs-OldPacked      
			)      
			BEGIN 
				select * from (
					Select PM.[ProductId],PM.[ProductName],'http://'+DB_NAME()+PM.ImageUrl as ImageUrl,Pm.ThumbnailImageUrl,Pm.Stock as AvilQty,
					SUM(ISNULL(dt.[QtyShip],0)*[QtyPerUnit]) as [QtyShip],
					SUM(ISNULL(oim.[RequiredQty],0)*[QtyPerUnit]) as [RequiredQty]
					from @DTPackedItemsQty as dt 
					inner join orderItemMaster as oim on oim.ProductAutoid=dt.ProductAutoid and oim.unitTypeautoid=dt.[UnitAutoId] and [OrderAutoId] = @OrderAutoId    
					inner join ProductMaster pm on pm.AutoId=dt.[ProductAutoId]      
					inner join PackingDetails pd on pd.ProductAutoId=dt.[ProductAutoId] and pd.UnitType=dt.[UnitAutoId] 
					group by ProductId,pm.Stock,[ProductName] ,ImageUrl,ThumbnailImageUrl
				) as t where AvilQty<[QtyShip]      
				ORDER BY [ProductId] ASC   
				Select 'Stock Not Available' as msg      
			end     
			ELSE      
			BEGIN 
				   IF EXISTS(select * from [dbo].[OrderMaster] WHERE [AutoId] = @OrderAutoId and Status=9)
				   BEGIN
					    SELECT dt.ProductAutoId,SUM(ISNULL(oim.AddOnPackedPeice,0)) as [Pcs]
						INTO #resultAddon FROM [dbo].[OrderItemMaster] AS OIM                              
						INNER JOIN (SELECT * FROM @DTPackedItemsQty) AS dt 
						ON dt.[ProductAutoId] = OIM.[ProductAutoId] AND                                                                                       
						dt.[UnitAutoId] = OIM.[UnitTypeAutoId] and 
						dt.[isfreeitem]=oim.isFreeItem and 
						dt.[IsExchange]=oim.IsExchange WHERE OIM.OrderAutoId = @OrderAutoId
						and ISNULL(OIM.AddOnPackedPeice,0)>0
						group by dt.ProductAutoId

						insert into [dbo].[ProductStockUpdateLog]([ProductId],[OledStock],[NewStock],
						[UserAutoId],[DateTime],ReferenceId,ActionRemark,ReferenceType)      
						SELECT pm.ProductId,isnull([Stock],0),isnull([Stock],0) + isnull([Pcs],0),
						@EmpAutoId,GETDATE(),@OrderAutoId,      
						'Order No-(' + (select OrderNO from OrderMaster as  om 
						where om.autoid=@OrderAutoId) + ') has been add-on packed by '+
						(Select FirstName+' '+LastName from Employeemaster 
						where autoid=@EmpAutoId)+'.','OrderMaster'       
						FROM [dbo].[ProductMaster] AS PM                                                                                                         
						INNER JOIN (SELECT [ProductAutoId],([Pcs]) as [Pcs] from #resultAddon) AS dt 
						ON dt.[ProductAutoId] = PM.[AutoId]   	 

						UPDATE PM SET [Stock] = isnull([Stock],0) + isnull([Pcs],0) 
						FROM [dbo].[ProductMaster] AS PM                                                                                                         
						INNER JOIN #resultAddon AS dt ON dt.[ProductAutoId] = PM.[AutoId]

				   END
			

			SET @PkgId = (SELECT DBO.SequenceCodeGenerator('PackingId'))                                                                    
          
			INSERT INTO [dbo].[GenPacking] ([PackingId],[OrderAutoId],[PackingDate],[PackerAutoId])                                                                                                             
			VALUES(@PkgId,@OrderAutoId,GETDATE(),@PackerAutoId)                             
                          
			UPDATE SequenceCodeGeneratorMaster SET currentSequence = currentSequence + 1 WHERE SequenceCode='PackingId'                         
                        
			Select @SalesPersonAutoId=SalesPersonAutoId from OrderMaster Where AutoId=@OrderAutoId  

			UPDATE [dbo].[OrderMaster] SET                                              
			[PackerRemarks]  = @Remarks,                        
			[Status]   = case when Status=9 then 10 else 3 end,                               
			[PackedBoxes]  = isnull([PackedBoxes],0)+@PackedBoxes ,                                                                                                            
			AddonPackedQty=@PackedBoxes                                   
			WHERE [AutoId] = @OrderAutoId                                                        
                          
			UPDATE OIM SET                                            
			OIM.[Barcode]  = dt.[Barcode],                                                                                                             
			OIM.[QtyShip]  = dt.[QtyShip],                                                                                                            
			OIM.[RemainQty]  = dt.[RemainQty]                                                            
			FROM [dbo].[OrderItemMaster] AS OIM                              
			INNER JOIN (SELECT * FROM @DTPackedItemsQty) AS dt 
			ON dt.[ProductAutoId] = OIM.[ProductAutoId] AND                                                                                       
			dt.[UnitAutoId] = OIM.[UnitTypeAutoId] and 
			dt.[isfreeitem]=oim.isFreeItem and 
			dt.[IsExchange]=oim.IsExchange WHERE OIM.OrderAutoId = @OrderAutoId  

			insert into [dbo].[ProductStockUpdateLog]([ProductId],[OledStock],[NewStock],
			[UserAutoId],[DateTime],ReferenceId,ActionRemark,ReferenceType)      
			SELECT pm.ProductId,isnull([Stock],0),isnull([Stock],0) - isnull([Pcs],0),
			@EmpAutoId,GETDATE(),@OrderAutoId,      
			'Order No-(' + (select OrderNO from OrderMaster as  om 
			where om.autoid=@OrderAutoId) + ') has been packed by '+
			(Select FirstName+' '+LastName from Employeemaster 
			where autoid=@EmpAutoId)+'.','OrderMaster'       
			FROM [dbo].[ProductMaster] AS PM                                                                                                         
			INNER JOIN (SELECT [ProductAutoId],SUM(ISNULL([Pcs],0)) as [Pcs] FROM @DTPackedItemsQty where ISNULL([Pcs],0)>0 group by [ProductAutoId]) AS dt 
			ON dt.[ProductAutoId] = PM.[AutoId]   	 

			UPDATE PM SET [Stock] = isnull([Stock],0) - isnull([Pcs],0) 
			FROM [dbo].[ProductMaster] AS PM 
			INNER JOIN (SELECT [ProductAutoId],SUM(ISNULL([Pcs],0)) as [Pcs] FROM @DTPackedItemsQty where ISNULL([Pcs],0)>0 group by [ProductAutoId]) AS dt 
			ON dt.[ProductAutoId] = PM.[AutoId]       
                        
			SET @LogRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=4), '[PackingId]', @PkgId)                                                                                                            
			INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                                                            
			VALUES(4,@EmpAutoId,getdate(),@LogRemark,@OrderAutoId)  
          
			Update OrderItemMaster SET AddOnQty=0 where OrderAutoId=@OrderAutoId AND RequiredQty=QtyShip
                                        
			SELECT @CustomerAutoId=CustomerAutoId FROM [dbo].[OrderMaster]                                                    
			WHERE [AutoId] = @OrderAutoId  
	  
			IF EXISTS(Select * from AllowQtyPiece as AQP
			INNER JOIN OrderItemMaster as OIM ON AQP.ProductAutoId=OIM.ProductAutoId
			where SalesPersonAutoId=@SalesPersonAutoId AND OIM.OrderAutoId=@OrderAutoId
			AND convert(date,AQP.AllowDate) <= convert(date,getdate()))
			BEGIN
			 

			IF EXISTS(Select * from AllocatedPackingDetails AS APD 
			INNER JOIN @DTPackedItemsQty AS dt ON dt.ProductAutoId=APD.ProductAutoId
			AND dt.UnitAutoId=APD.PackingType where APD.OrderAutId=@OrderAutoId)
			BEGIN
				Update APD SET APD.AllocatedQty=ISNULL(dt.TotalPieces,0),APD.ProductAutoId=dt.ProductAutoId,
				APD.PackingType=dt.UnitTypeAutoId,APD.AllocateDate=GETDATE()
				FROM AllocatedPackingDetails AS APD INNER JOIN (
				Select SUM(OIM.TotalPieces) as TotalPieces,OIM.ProductAutoId,OIM.UnitTypeAutoId
				from AllowQtyPiece as AQP
				INNER JOIN OrderItemMaster as OIM ON
				AQP.ProductAutoId=OIM.ProductAutoId
				where SalesPersonAutoId=@SalesPersonAutoId AND OIM.OrderAutoId=@OrderAutoId
				AND convert(date,AQP.AllowDate) <= convert(date,getdate()) 
				AND ISNULL(OIM.QtyShip,0)>0
				group by OIM.ProductAutoId,OIM.UnitTypeAutoId
				) AS dt ON APD.OrderAutId=@OrderAutoId AND APD.ProductAutoId=dt.ProductAutoId
			END
			ELSE
			BEGIN
				INSERT INTO AllocatedPackingDetails (OrderAutId,ProductAutoId,AllocatedQty,PackingType,AllocateDate)
				Select @OrderAutoId,OIM.ProductAutoId,SUM(OIM.TotalPieces),OIM.UnitTypeAutoId,GetDate()
				from AllowQtyPiece as AQP
				INNER JOIN OrderItemMaster as OIM ON
				AQP.ProductAutoId=OIM.ProductAutoId
				where SalesPersonAutoId=@SalesPersonAutoId AND OIM.OrderAutoId=@OrderAutoId
				AND convert(date,AQP.AllowDate) <= convert(date,getdate()) 
				AND ISNULL(OIM.QtyShip,0)>0
				group by OIM.ProductAutoId,OIM.UnitTypeAutoId
			END
			END                                           
			if((select CustomerType from CustomerMaster where AutoId=@CustomerAutoId)=3)                                                          
			begin                                    
			Declare @Stat int --Updated on  11/12/2019 06:18 PM By Rizwan Ahmad                                                         
			SELECT @Stat=Status from [dbo].[OrderMaster]   WHERE [AutoId] = @OrderAutoId                                                            
			exec [dbo].[ProcUpdatePOStatus_All]                                                          
			@CustomerId=@CustomerAutoId,                                                          
			@OrderAutoId=@OrderAutoId,                                                          
			@Status=@Stat       
			end      
     
				Select @OrderAutoId as AutoId      
			Select 'Order has been packed' as msg       
			EXEC [dbo].[ProcAutomaticSendEmail]  @Opcode=11,@CustomerAutoId=@CustomerAutoId, 
			@OrderAutoId=@OrderAutoId,@isException=0,@exceptionMessage='' 
			end      
		ENd      
		Else      
		BEGIN      
			Select @OrderAutoId as AutoId
			Select 'Order has been already packed' as msg      
		END 
	END 
	Else      
	BEGIN     
	Select 'InvalidPacker' as msg 
		SET @isException=0
		SET @exceptionMessage='InvalidPacker'
	END 	   
    COMMIT TRANSACTION                                                                                                 
    END TRY                                                                                                          
    BEGIN CATCH                                                                 
		ROLLBACK TRAN                                                                                                            
		Set @isException=1                                                                                                            
		Set @exceptionMessage=ERROR_MESSAGE()--'Opps Some thing went wrong.'                                                                                                           
    END CATCH                                                                                                            
   END                                                          
  ELSE IF @Opcode=106  --Updated on  11/12/2019 03:33 PM By Rizwan Ahmad                                                                              
   BEGIN                                         
	  Declare @Qty INT,@AllowQtyInPieces INT ,@UpdatedQty INT,@SalesPerson INT                                                    
	  IF EXISTS(SELECT [SecurityValue] FROM [dbo].[TBL_MST_SecurityMaster]
	  WHERE [SecurityValue]=@CheckSecurity and SecurityType=8 and typeEvent=2)                                
	  BEGIN                                                                                     
		  SELECT [SecurityValue] FROM [dbo].[TBL_MST_SecurityMaster] WHERE [SecurityValue]=@CheckSecurity                                         
		  and SecurityType=8 and typeEvent=2                                                     
                                                    
		  SELECT @SalesPerson=SalesPersonAutoId From OrderMaster WHERE AutoId=@OrderAutoId    
		  
		  SELECT @Qty=SUM(RequiredQty*QtyPerUnit) FROM OrderItemMaster 
		  WHERE ProductAutoId=@ProductAutoId AND UnitTypeAutoId=@UnitAutoId
		  and OrderAutoId=@OrderAutoId  
		  
		  SELECT @AllowQtyInPieces=AllowQtyInPieces-UsedQty FROM AllowQtyPiece
		  WHERE SalesPersonAutoId=@SalesPerson and ProductAutoId=@ProductAutoId                                           
		  IF (@Qty>@AllowQtyInPieces) --Updated on  11/14/2019  By Rizwan Ahmad               
		  BEGIN                                                   
			  SET @UpdatedQty=@Qty-@AllowQtyInPieces                               
			  UPDATE AllowQtyPiece SET AllowQtyInPieces=@UpdatedQty+AllowQtyInPieces 
			  WHERE SalesPersonAutoId=@SalesPerson and ProductAutoId=@ProductAutoId                                                        
		  END                             
	  END                                                      
	  ELSE                                                    
	  BEGIN                                                    
			Set @isException=1                                                                                    
			Set @exceptionMessage='false'                                                       
	  END                                  
   END                                                   
   ELSE IF @Opcode=107  --Updated on  11/12/2019 05:27 PM By Rizwan Ahmad                                                                              
   BEGIN                                                     
  IF EXISTS(SELECT [SecurityValue] FROM [dbo].[TBL_MST_SecurityMaster] WHERE [SecurityValue]=@CheckSecurity and SecurityType=8 and typeEvent=2)                                                    
  BEGIN                                                                                
   SELECT [SecurityValue] FROM [dbo].[TBL_MST_SecurityMaster] WHERE [SecurityValue]=@CheckSecurity and SecurityType=8 and typeEvent=2                                                     
                                                    
   SELECT @SalesPerson=SalesPersonAutoId From OrderMaster WHERE AutoId=@OrderAutoId                                 
   SELECT @ProductAutoId=ProductAutoId,@UnitAutoId=UnitAutoId FROM ItemBarcode WHERE Barcode=@BarCode                                                    
   SELECT @Qty=Qty FROM PackingDetails WHERE ProductAutoId=@ProductAutoId AND UnitType=@UnitAutoId                                            
   SET @Qty=@ReqQty*@Qty                              
                                         
   SELECT @AllowQtyInPieces=AllowQtyInPieces-UsedQty FROM AllowQtyPiece WHERE SalesPersonAutoId=@SalesPerson and ProductAutoId=@ProductAutoId                                             
                                         
   IF (@Qty>@AllowQtyInPieces) --Updated on  11/14/2019  By Rizwan Ahmad                                            
   BEGIN                                                   
       SET @UpdatedQty=@Qty-@AllowQtyInPieces                                      
       UPDATE AllowQtyPiece SET AllowQtyInPieces=@UpdatedQty+AllowQtyInPieces WHERE SalesPersonAutoId=@SalesPerson and ProductAutoId=@ProductAutoId                                                    
   END                    
    END                                                      
  ELSE                                            
  BEGIN                                                    
      Set @isException=1                                    
      Set @exceptionMessage='false'                                                       
  END                                                                                      
   END                                                                   
   ELSE IF @Opcode=108  --Added on  12/14/2019 By Rizwan Ahmad                                    
   BEGIN                             
		 Declare @QtUpdate int,@RequiredQuantity int,@checkQty int,@StockQty int                    
		 Select @SalesPersonAutoId=SalesPersonAutoId from OrderMaster Where AutoId=@OrderAutoId   
		 
		 SET @Barcode=(Select top 1 ib.Barcode FROM OrderItemMaster as oim
		 inner join OrderMaster as Om on om.AutoId=OrderAutoId
		 inner join ItemBarcode as ib on ib.ProductAutoId=oim.ProductAutoId and oim.UnitTypeAutoId=ib.UnitAutoId
		 where oim.ProductAutoId=@ProductAutoId AND UnitTypeAutoId=@UnitAutoId and ISNULL(oim.Barcode,'')!=''
		 order by OrderDate desc)      
      
		 Select @QtyPerUnit=QtyPerUnit,@shipQty=QtyShip,@RequiredQuantity=RequiredQty from OrderItemMaster where OrderAutoId=@OrderAutoId and       
		 ProductAutoId=@ProductAutoId and UnitTypeAutoId=@UnitAutoId AND isFreeItem=@IsFreeItem AND IsExchange=@IsExchange      
      
		 Select @StockQty=@ShipQuantity*QtyPerUnit,@PackedBoxes=ISNULL(qtyship,0) from OrderItemMaster where OrderAutoId=@OrderAutoId and       
		 ProductAutoId=@ProductAutoId and UnitTypeAutoId=@UnitAutoId and IsExchange=@IsExchange and isFreeItem=@IsFreeItem      
         DECLARE @productId108 int=(select ProductId from ProductMaster where AutoId=@ProductAutoId)
		 SET @StockQty=@StockQty-ISNULL((select SUM(OledStock-NewStock) from ProductStockUpdateLog where ProductId=@productId108 and ReferenceType='OrderMaster' and ReferenceId=@OrderAutoId),0)

         IF((Select Stock from ProductMaster where AutoId=@ProductAutoId) < @StockQty)      
		 BEGIN      
			  select 'StockNotAvailable' as msg,@PackedBoxes as PackedBoxes      
			  SET @isException=1                                                                  
			  SET @exceptionMessage='Stock'      
		 END      
		 ELSE      
		 BEGIN        
    --Allocation code start here 
		IF EXISTS( SELECT * FROM AllowQtyPiece WHERE SalesPersonAutoId=@SalesPersonAutoId and ProductAutoId=@ProductAutoId )                      
		BEGIN                      
		IF(@shipQty!=0 AND @ShipQuantity!=0)                      
		BEGIN      
			SET @UpdateQty=ISNULL(@QtyPerUnit*@ShipQuantity,0)       
			SELECT @AllowQtyInPieces=ISNULL(AllowQtyInPieces-UsedQty,0) FROM AllowQtyPiece WHERE SalesPersonAutoId=@SalesPersonAutoId and ProductAutoId=@ProductAutoId                       
			IF (@AllowQtyInPieces>@UpdateQty or @AllowQtyInPieces=@UpdateQty)                       
			BEGIN           
				Update OrderItemMaster set QtyShip=@ShipQuantity,Barcode=@Barcode,RemainQty=RequiredQty-QtyShip where OrderAutoId=@OrderAutoid AND ProductAutoId=@ProductAutoId AND       
				UnitTypeAutoId=@UnitAutoId  AND isFreeItem=@IsFreeItem AND IsExchange=@IsExchange                     
			END      
			ELSE 
			BEGIN
			        set @isException=1                                                                  
					set @exceptionMessage='Less' 
			END                      
		END                      
		ELSE  IF(@ShipQuantity=0)                      
		BEGIN                    
			Update OrderItemMaster set QtyShip=@ShipQuantity,Barcode='',RemainQty=RequiredQty where OrderAutoId=@OrderAutoid AND ProductAutoId=@ProductAutoId AND       
			UnitTypeAutoId=@UnitAutoId  AND isFreeItem=@IsFreeItem AND IsExchange=@IsExchange                
		END                      
		ELSE                      
		BEGIN                      
			SET @UpdateQty=ISNULL(@QtyPerUnit*@ShipQuantity,0)                        
			SELECT @AllowQtyInPieces=ISNULL(AllowQtyInPieces-UsedQty,0) FROM AllowQtyPiece WHERE SalesPersonAutoId=@SalesPersonAutoId and ProductAutoId=@ProductAutoId        
                         
			IF (@AllowQtyInPieces>@UpdateQty or @AllowQtyInPieces=@UpdateQty)                      
			BEGIN                      
				Update OrderItemMaster set QtyShip=ISNULL(@ShipQuantity,0),Barcode=@Barcode,RemainQty=RequiredQty-QtyShip where OrderAutoId=@OrderAutoid AND ProductAutoId=@ProductAutoId       
				AND UnitTypeAutoId=@UnitAutoId  AND isFreeItem=@IsFreeItem AND IsExchange=@IsExchange          
        	END                       
			ELSE 
			BEGIN
			        set @isException=1                                                                  
					set @exceptionMessage='Less'        
			END                        
		END                          
		END --Allocation code end here       
		ELSE 
		BEGIN        
			update OrderItemMaster set       
			QtyShip=case       
			when @ShipQuantity=0 then null       
			WHEN @ShipQuantity>RequiredQty then QtyShip       
			else @ShipQuantity end,      
			Barcode=case       
			when @ShipQuantity=0 then null       
			else Barcode end      
			where OrderAutoId=@OrderAutoId and       
			ProductAutoId=@ProductAutoId and UnitTypeAutoId=@UnitAutoId and IsExchange=@IsExchange and isFreeItem=@IsFreeItem      
			select 'Stock Available' as msg,(Select Stock from ProductMaster where AutoId=@ProductAutoId) as AvailableStock,    
			(case when @ShipQuantity=0 then '' else @Barcode end) as Barcode      
		END      
	END      
 END                        
 ELSE IF @Opcode=109                      
 BEGIN                       
	SELECT @SalesPerson=SalesPersonAutoId From OrderMaster WHERE AutoId=@OrderAutoId                                               
	SELECT @Qty=Qty FROM PackingDetails WHERE ProductAutoId=@ProductAutoId AND UnitType=@UnitAutoId                                                    
	SET @Qty=@ReqQty*@Qty                                                    
	SELECT @AllowQtyInPieces=AllowQtyInPieces-UsedQty FROM AllowQtyPiece WHERE SalesPersonAutoId=@SalesPerson and ProductAutoId=@ProductAutoId                                           
	IF (@Qty>@AllowQtyInPieces) --Updated on  11/14/2019  By Rizwan Ahmad                                            
	BEGIN                                                   
		SET @UpdatedQty=@Qty-@AllowQtyInPieces                                        
		UPDATE AllowQtyPiece SET AllowQtyInPieces=@UpdatedQty+AllowQtyInPieces-UsedQty WHERE SalesPersonAutoId=@SalesPerson and ProductAutoId=@ProductAutoId                                                        
	END        
	ELSE       
	BEGIN      
		UPDATE AllowQtyPiece SET AllowQtyInPieces=@Qty+AllowQtyInPieces WHERE SalesPersonAutoId=@SalesPerson and ProductAutoId=@ProductAutoId      
	END      
 END         
  ELSE IF @Opcode=110                                                                                                                  
 BEGIN                                                               
 select @checkQty=PackedBoxes FROM OrderMaster  where AutoId=@OrderAutoId       
    update OrderMaster set PackedBoxes=@ReqQty,AddonPackedQty=@ReqQty      
 where AutoId=@OrderAutoId                     
         
 SET @LogRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=24), '[NewBox]', @ReqQty)       
 SET @LogRemark = REPLACE(@LogRemark, '[OldBox]',@checkQty)                                                                                                                   
 INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                                                                                            
 VALUES(24,@EmpAutoId,getdate(),@LogRemark,@OrderAutoId)        
      
      
  END       
  ELSE IF @Opcode=124                                                                                                                
 BEGIN                                                               
 select AutoId,ShipAutoId,PrintTemplate,Url,Status,Type from  Tbl_packerprintassign where ShipAutoId=@ShippingAutoId and Type=@EmployeeType            
  END       
ELSE IF @Opcode=123                                                                                                                 
BEGIN      
 select @shipQty=@ShipQuantity *QtyPerUnit,@PackedBoxes=ISNULL(qtyship,0)  from OrderItemMaster where OrderAutoId=@OrderAutoId and       
 ProductAutoId=@ProductAutoId and UnitTypeAutoId=@UnitAutoId and IsExchange=@IsExchange and isFreeItem=@IsFreeItem      
 if((Select Stock from ProductMaster where AutoId=@ProductAutoId) < @shipQty)      
 begin      
  select 'Stock Not Available' as msg,@PackedBoxes as PackedBoxes      
 end      
 else      
 begin      
   if EXISTS(SELECT * FROM oRDERMASTER)       
  update OrderItemMaster set       
   QtyShip=case       
      when @ShipQuantity=0 then null       
      WHEN @ShipQuantity>RequiredQty then QtyShip       
      else @ShipQuantity end,      
       Barcode=case       
      when @ShipQuantity=0 then null       
      else Barcode end      
       where OrderAutoId=@OrderAutoId and       
  ProductAutoId=@ProductAutoId and UnitTypeAutoId=@UnitAutoId and IsExchange=@IsExchange and isFreeItem=@IsFreeItem      
  select 'Stock Available' as msg,(Select Stock from ProductMaster where AutoId=@ProductAutoId) as AvailableStock      
 end                                                                      
 END
   ELSE IF @Opcode=125                                                                                                                                              
    BEGIN                                                                    
		SELECT CM.[CustomerName],S.[StateName] As State,SA.[City],SA.[Zipcode],[PackedBoxes] as StartBox,AddonPackedQty as [PackedBoxes],                                                                                                                          
		emp.FirstName + ' '+ISNULL(lastname,'') as SalesName                                                                             
		FROM [dbo].[OrderMaster] As OM                                                                                                                                     
		INNER JOIN [dbo].[ShippingAddress] AS SA ON SA.[AutoId] = OM.[ShipAddrAutoId]                                                               
		INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                                                                                      
		INNER join EmployeeMaster as emp on emp.AutoId= OM.SalesPersonAutoId                                                                                                                                                    
		INNER JOIN [dbo].[State] AS S ON S.[AutoId] = SA.[State] WHERE OM.[OrderNo] = @OrderNo     
		SELECT CompanyName,EmailAddress, [Address],[Website],[MobileNo],[FaxNo],TermsCondition,Logo FROM CompanyDetails                                                                                                     
   END   
   END TRY                                                                                    
   BEGIN CATCH                                                  
  Set @isException=1                                                                                    
  Set @exceptionMessage='Oops! Something went wrong.Please try later'                                                                                   
   END CATCH                                                                      
END 