 
ALTER Procedure [dbo].[ProcInvRecPO]
 @Opcode int=Null,                         
 @EmpId INT=0,                          
 @POVNumber varchar(15)='', 
 @POAutoId int=null,                        
 @ProductAutoId INT=0,                        
 @UnitAutoId INT=0,      
 @RecAutoId INT=0,    
 @QtyPerUnit INT=0,                        
 @RequiredQty INT=0,     
 @ReceivedQty INT=0,                       
 @POVDraftAutoId INT=0,                                                
 @VendorId INT=0,                         
 @Status INT=0,                        
 @Remark varchar(500)='',     
 @ReceiverRemark varchar(250)='',                       
 @Barcode varchar(25)='',      
 @PODraftId varchar(20)='', 
 @RecId varchar(20)='', 
 @Draftitems xml=null,       
 @POFromDate DATETIME=NULL,      
 @POToDate DATETIME=NULL,                 
 @PageIndex INT = 1,                                                              
 @PageSize INT = 10,                                                               
 @RecordCount INT =null,                                                              
 @isException bit out,                                                               
 @exceptionMessage varchar(max) out,
 @Price decimal(18,2)=null
AS
BEGIN
BEGIN TRY    
    Set @isException=0    
    Set @exceptionMessage='Success'    
    
 If @Opcode=401    
  BEGIN    
  Select ROW_NUMBER() OVER(Order By PRM.AutoId desc) AS RowNumber,PRM.POAutoId,PRM.AutoId,convert(varchar(20),ReceiveDate,101) as ReceiveDate,PORecieveId,FirstName+' '+LastName as ReceiveBy,
  sm.StatusType as Status,sm.ColorCode,BillNo,convert(varchar(20),BillDate,101) as BillDate,PONo,convert(varchar(20),PODate,101) as PODate,sd.StatusType as POStatus,sd.ColorCode as POColor		INTO #POResult from POReceiveMaster as PRM 
		inner join StatusMaster as sm on sm.AutoId=PRM.Status  and sm.Category='PORec' and Status in (1,4,2,3)
		inner join PurchaseOrderMaster as pom on pom.AutoId=PRM.POAutoId
		inner join StatusMaster sd on sd.AutoId=pom.Status and sd.Category='POOrd'
		inner join EmployeeMaster as emp on emp.AutoId=PRM.ReceiveBy
		WHERE (ISNULL(@RecId,'')='' or PRM.PORecieveId=@RecId) AND       
		(@POFromDate is null or @POFromDate='' or @POToDate is null or @POToDate ='' or Convert(date,PRM.ReceiveDate) between Convert(date,@POFromDate) AND Convert(date,@POToDate)) 
		and 
		(ISNULL(@Status,0)=0 OR PRM.Status =@Status)
		and (ISNULL(@VendorId,0)=0 OR pom.VenderAutoId =@VendorId)
		and (ISNULL(@PODraftId,'')='' or pom.PONo=@PODraftId)
		--and (ISNULL(@EmpId,0)=0 OR prm.ReceiveBy =@EmpId)
		order by PRM.AutoId DESC      
		SELECT COUNT(*) as RecordCount,@PageSize as PageSize,@PageIndex as PageIndex FROM #POResult        
		SELECT * FROM #POResult                           
		WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1     
 
  END       
   If @Opcode=403   
  BEGIN    
  Select ROW_NUMBER() OVER(Order By PRM.AutoId desc) AS RowNumber,PRM.POAutoId,PRM.AutoId,convert(varchar(20),ReceiveDate,101) as ReceiveDate,PORecieveId,FirstName+' '+LastName as ReceiveBy,
  sm.StatusType as Status,sm.ColorCode,BillNo,convert(varchar(20),BillDate,101) as BillDate,PONo,convert(varchar(20),PODate,101) as PODate,sd.StatusType as POStatus,sd.ColorCode as POColor,PRM.Status as StatusAutoId		
  INTO #POResult403 from POReceiveMaster as PRM 
		inner join StatusMaster as sm on sm.AutoId=PRM.Status  and sm.Category='PORec' and Status in (1,4,2,3)
		inner join PurchaseOrderMaster as pom on pom.AutoId=PRM.POAutoId
		inner join StatusMaster sd on sd.AutoId=pom.Status and sd.Category='POOrd'
		inner join EmployeeMaster as emp on emp.AutoId=PRM.ReceiveBy
		WHERE (ISNULL(@RecId,'')='' or PRM.PORecieveId=@RecId) AND       
		(@POFromDate is null or @POFromDate='' or @POToDate is null or @POToDate ='' or Convert(date,PRM.ReceiveDate) between Convert(date,@POFromDate) AND Convert(date,@POToDate)) 
		and 
		(ISNULL(@Status,0)=0 OR PRM.Status =@Status)
		and (ISNULL(@VendorId,0)=0 OR pom.VenderAutoId =@VendorId)
		and (ISNULL(@PODraftId,'')='' or pom.PONo=@PODraftId)
		--and (ISNULL(@EmpId,0)=0 OR prm.ReceiveBy =@EmpId)
		order by PRM.AutoId DESC      
		SELECT COUNT(*) as RecordCount,@PageSize as PageSize,@PageIndex as PageIndex FROM #POResult403        
		SELECT * FROM #POResult403                           
		WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1      
 
  END     
  If @Opcode=404    
  BEGIN    
  Select ROW_NUMBER() OVER(Order By PRM.AutoId desc) AS RowNumber,PRM.POAutoId,PRM.AutoId,convert(varchar(20),ReceiveDate,101) as ReceiveDate,PORecieveId,FirstName+' '+LastName as ReceiveBy,
  sm.StatusType as Status,sm.ColorCode INTO #POResult404 from POReceiveMaster as PRM 
		inner join StatusMaster as sm on sm.AutoId=PRM.Status  and sm.Category='PORec' and Status in (1,4,2,3)
		inner join PurchaseOrderMaster as pom on pom.AutoId=PRM.POAutoId
		inner join StatusMaster sd on sd.AutoId=pom.Status and sd.Category='POOrd'
		inner join EmployeeMaster as emp on emp.AutoId=PRM.ReceiveBy
		WHERE pom.AutoId=@POAutoId
		
		order by PRM.AutoId DESC      
		--SELECT COUNT(*) as RecordCount,@PageSize as PageSize,@PageIndex as PageIndex FROM #POResult        
		SELECT * FROM #POResult404                           
		--WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1     
 
  END     
 END TRY    
 BEGIN CATCH    
   Set @isException=1    
   Set @exceptionMessage=ERROR_MESSAGE()    
 END CATCH    
END
