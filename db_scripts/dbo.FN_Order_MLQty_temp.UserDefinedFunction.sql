USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Order_MLQty_temp]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create FUNCTION  [dbo].[FN_Order_MLQty_temp]
( 
	@orderAutoId int,
	@Status INT
)
RETURNS decimal(10,2)
AS
BEGIN
	--We have created this function to calculate mlqty for all orders

	DECLARE @MLQty decimal(10,2)=0.00	
	IF @Status	!=11
	BEGIN 
		SET @MLQty=(SELECT sum(TotalMLQty) FROM OrderItemMaster WHERE OrderAutoId=@orderAutoId)	
	END 
	ELSE
	BEGIN
	SET @MLQty=(SELECT sum(TotalMLQty) FROM Delivered_Order_Items WHERE OrderAutoId=@orderAutoId)	
	END
	RETURN @MLQty
END
GO
