
ALTER PROCEDURE [dbo].[ProcSalesBySalePerson]                                                                    
@Opcode INT=NULL,                                                                                                                                                                                                                                   
@SalesPersonAutoId int =null,                                                                                                                            
@SalesPerson  VARCHAR(500)=NULL,                                                         
@FromDate date =NULL,                                                                    
@ToDate date =NULL,                             
@PageIndex INT=1,                                                                    
@PageSize INT=10,                                                                    
@RecordCount INT=null,                                                                    
@isException bit out,                                                                    
@exceptionMessage varchar(max) out                                                                    
AS                                                                    
BEGIN                                                                     
 BEGIN TRY                                                                    
  Set @isException=0                                                                    
  Set @exceptionMessage='Success'                                                                    
 IF @Opcode=41                                                                    
  BEGIN                                                       
   SELECT AutoId SId,FirstName+' '+ISNULL(LASTNAME,'') AS SP  FROM EmployeeMaster WHERE EmpType IN (2) and Status=1 order by SP ASC 
   for json path
  END                                                                    
  ELSE IF @Opcode=42                                                                    
  BEGIN                        
  select ROW_NUMBER() over(order by FirstName+' '+ISNULL(lastName,'')) as RowNumber, FirstName+' '+ISNULL(lastName,'') as EmployeeName ,SalesPersonAutoId,SUM(NoofOrders) as NoOfOrders,sum(TotalAmount) as GrandTotal,                                                     
  SUM(NoofPOSOrders) as NoofPOSOrders,SUM(POSTotalAmount) as POSTotalAmount into #Result46 from                                                             
  (                                                            
 SELECT SalesPersonAutoId,SUM(TotalAmount) as TotalAmount,count(TotalAmount) as NoofOrders,0 as NoofPOSOrders,0 as POSTotalAmount                                                           
 from (                                              
  select om.SalesPersonAutoId,                                                            
  (ISNULL(om.TotalAmount,0)-ISNULL((select SUM(ISNULL(cmm.GrandTotal,0)) from CreditMemoMaster as cmm where cmm.OrderAutoId=om.AutoId),0)) as TotalAmount                                                          
  from OrderMaster as om                                                                 
  where om.Status=11 and orderType!=1 and                                                             
  (ISNULL(@SalesPerson,'0')='0' OR @SalesPerson='0,' OR om.SalesPersonAutoId                                                                    
in (select * from dbo.fnSplitString(@SalesPerson,','))                                                                    
  )                                                            
  and (@FromDate is null or @ToDate is null or (CONVERT(date,OrderDate)                                                                     
  between convert(date,@FromDate) and CONVERT(date,@ToDate)))                                      
 ) as t                                                              
 group by t.SalesPersonAutoId                                                            
  union                                                            
    SELECT SalesPersonAutoId,0 as NoofOrders,0 as TotalAmount,COUNT(POSTotalAmount) as NoofPOSOrders,SUM(POSTotalAmount) as POSTotalAmount                                                
 from (                                                          
  select om.SalesPersonAutoId,0 as NoofOrders,0 as TotalAmount,                                                            
  (ISNULL(om.TotalAmount,0)-ISNULL((select SUM(ISNULL(cmm.GrandTotal,0)) from CreditMemoMaster as cmm where cmm.OrderAutoId=om.AutoId),0))                          
  as POSTotalAmount from OrderMaster as om                                                                    
  where om.Status=11 and   orderType=1 and                                                             
  (ISNULL(@SalesPerson,'0')='0' OR @SalesPerson='0,' OR om.SalesPersonAutoId                                                                    
  in (select * from dbo.fnSplitString(@SalesPerson,','))                                                                   
  )                                                            
  and (@FromDate is null or @ToDate is null or (CONVERT(date,OrderDate)                                                                     
  between convert(date,@FromDate) and CONVERT(date,@ToDate)))                                                               
  ) as t                                     
  group by t.SalesPersonAutoId                                                            
                                                            
  ) as t                                                            
  inner join EmployeeMaster as emp on emp.AutoId=t.SalesPersonAutoId                                                            
  group by SalesPersonAutoId,(FirstName+' '+ISNULL(lastName,''))                                                            
                                    
	SELECT * FROM #Result46                                        
	WHERE @PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)                   
 
	SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize=0 then COUNT(RowNumber) else @PageSize end AS PageSize, @PageIndex AS PageIndex,Format(getdate(),'MM/dd/yyyy hh:mm tt') as PrintDate FROM #Result46                                                         
                                                            
	SELECT ISNULL(sum(GrandTotal),0.00) AS GrandTotal,ISNULL(sum(POSTotalAmount),0.00) AS POSTotalAmount,                      
	ISNULL(sum(NoOfOrders),0) AS NoOfOrders,ISNULL(sum(NoofPOSOrders),0) AS NoofPOSOrders                      
	FROM #Result46                                                                         
  END                                                                      
                            
 END TRY                                                    
 BEGIN CATCH                                                                    
  Set @isException=1                                                                    
  Set @exceptionMessage=ERROR_MESSAGE()                                                                    
 END CATCH                                                                    
END 
