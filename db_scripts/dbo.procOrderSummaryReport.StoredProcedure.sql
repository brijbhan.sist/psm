 alter Procedure [dbo].[procOrderSummaryReport]    
 AS    
 BEGIN    
  select * from     
  (    
 select om.AutoId,om.OrderNo,otm.OrderType as OrderType,om.TotalAmount,om.OverallDiscAmt , om.TotalTax,om.MLTax,om.AdjustmentAmt,om.GrandTotal,om.Deductionamount,
 om.CreditAmount,om.PayableAmount , sm.StatusType Status    
 from OrderMaster as om
 inner join StatusMaster as sm on om.Status=sm.AutoId
 INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
  where om.CreditAmount < 0    
 ) as t    
  where PayableAmount != 0    
      
 END 
GO
