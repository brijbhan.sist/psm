USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcPageTitleMaster]    Script Date: 12/04/2020 01:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[ProcPageTitleMaster]
@Opcode int=Null, 
@AutoId int=Null, 
@EmpAutoId int=null,
@PageId int=Null, 
@PageUrl varchar(150)=Null, 
@PageTitle varchar(150)=Null, 
@UserDescription varchar(max)=Null, 
@AdminDescription varchar(max)=Null, 
@Status int=Null, 
@LocationId int = Null,
@LocationName varchar(20) = null,
@CreateDate datetime=Null, 
@FromDate datetime = Null,
@ToDate datetime = Null,
@PageIndex INT=1,      
@PageSize INT=10, 
@RecordCount INT =null, 
@isException bit out,                                     
@exceptionMessage varchar(max) out   
AS
BEGIN
    SET @isException=0
	SET @exceptionMessage='success'
    BEGIN TRY
			IF @Opcode = 11
			BEGIN
			IF NOT EXISTS(SELECT [PageId] FROM [psmnj.a1whm.com].[dbo].[PageTitleMaster] WHERE [PageId] = @PageId)  
				BEGIN
					Declare @query varchar(max) ,@logquery varchar(max)
					IF @LocationId = 0
						BEGIN					
							Declare @i int=0,@rowcount int=0,@rowno int=0,@LocationName1 varchar(20)
							select ROW_NUMBER() over (order by Autoid desc) as rr,Location into #Result from LocationMaster where Status=1
							set @rowcount=(select COUNT(1) from #Result)
							while @i<=@rowcount
								Begin

									set @LocationName1=(select Location from #Result where rr=@i)
									SET @query ='                
							        INSERT INTO ['+@LocationName1+'.a1whm.com].[dbo].[PageTitleMaster] (PageId,PageTitle,PageUrl,UserDescription,AdminDescription,Status,CreatedBy,CreateDate)
							   	    VALUES ('+convert(varchar(20),@PageId)+','''+@PageTitle+''','''+@PageUrl+''','''+@UserDescription+''','''+@AdminDescription+''','+convert(varchar(20),@Status)+','+convert(varchar(20),@EmpAutoId)+','''+convert(varchar(20),GETDATE())+''')'       
									Execute(@query) 

									SET @logquery ='                
							        INSERT INTO ['+@LocationName1+'.a1whm.com].[dbo].[PageTitleMasterLog] (PageId,PageTitle,PageUrl,UserDescription,AdminDescription,Status,CreatedBy,CreateDate)
							   	    VALUES ('+convert(varchar(20),@PageId)+','''+@PageTitle+''','''+@PageUrl+''','''+@UserDescription+''','''+@AdminDescription+''','+convert(varchar(20),@Status)+','+convert(varchar(20),@EmpAutoId)+','''+convert(varchar(20),GETDATE())+''')'       
									Execute(@logquery) 
									set @i=@i+1;
								End
						END
			END
			ELSE
				BEGIN
				 SET @isException=1
				 SET @exceptionMessage='PageId already exist !'
				END
			END

			ELSE IF @Opcode = 21
			BEGIN			 
			IF EXISTS(SELECT [PageId] FROM [psmnj.a1whm.com].[dbo].[PageTitleMaster] WHERE [PageId] = @PageId)  
				BEGIN
				Declare @query1 varchar(max) ,@logquery1 varchar(max) ,@LocationName2 varchar(20)
						IF @LocationId = 1
							BEGIN								    
								SET @logquery1 ='                
							    INSERT INTO ['+@LocationName+'.a1whm.com].[dbo].[PageTitleMasterLog] (PageId,PageTitle,PageUrl,UserDescription,AdminDescription,Status,CreatedBy,CreateDate)
							   	VALUES ('+convert(varchar(20),@PageId)+','''+@PageTitle+''','''+@PageUrl+''','''+@UserDescription+''','''+@AdminDescription+''','+convert(varchar(20),@Status)+','+convert(varchar(20),@EmpAutoId)+','''+convert(varchar(20),GETDATE())+''')'       
								Execute(@logquery1) 

								SET @query1 ='               
								UPDATE ['+@LocationName+'.a1whm.com].[dbo].[PageTitleMaster] SET   PageId='+convert(varchar(20),@PageId)+',PageTitle='''+@PageTitle+''',PageUrl='''+@PageUrl+''',UserDescription='''+@UserDescription+''',AdminDescription='''+@AdminDescription+''',Status='+convert(varchar(20),@Status)+',
								UpdatedBy = '+convert(varchar(20),@EmpAutoId)+',UpdateDate = '''+convert(varchar(20),GETDATE())+''' WHERE PageId = '+convert(varchar(20),@PageId)+'	'       	  
								execute (@query1)																		
							END
						ELSE
							BEGIN  
							      Declare @j int=0,@rowcount1 int=0,@rowno1 int=0
								select ROW_NUMBER() over (order by Autoid desc) as rr,Location into #Result1 from LocationMaster where Status=1
								set @rowcount1=(select COUNT(1) from #Result)
								while @j<=@rowcount1
									Begin
										set @LocationName2=(select Location from #Result where rr=@j)

										SET @logquery1 ='                
										INSERT INTO ['+@LocationName2+'.a1whm.com].[dbo].[PageTitleMasterLog] (PageId,PageTitle,PageUrl,UserDescription,AdminDescription,Status,CreatedBy,CreateDate)
							   			VALUES ('+convert(varchar(20),@PageId)+','''+@PageTitle+''','''+@PageUrl+''','''+@UserDescription+''','''+@AdminDescription+''','+convert(varchar(20),@Status)+','+convert(varchar(20),@EmpAutoId)+','''+convert(varchar(20),GETDATE())+''')'       
										Execute(@logquery1) 

										SET @query1 ='                
										UPDATE ['+@LocationName2+'.a1whm.com].[dbo].[PageTitleMaster] SET   PageId='+convert(varchar(20),@PageId)+',PageTitle='''+@PageTitle+''',PageUrl='''+@PageUrl+''',UserDescription='''+@UserDescription+''',AdminDescription='''+@AdminDescription+''',Status='+convert(varchar(20),@Status)+',
										UpdatedBy = '+convert(varchar(20),@EmpAutoId)+',UpdateDate = '''+convert(varchar(20),GETDATE())+''' WHERE PageId = '+convert(varchar(20),@PageId)+'	'       	  
										Execute(@query1) 
										set @j=@j+1; 
										End
							END
				END					
			ELSE
			BEGIN
			   SET @isException=1
			   SET @exceptionMessage='PageId does not exist !'
			END
			End			

	        ELSE IF @Opcode=41
			BEGIN 
				Select AutoId,PageId,PageTitle,UserDescription,AdminDescription  from PageTitleMaster where Status=1 and PageId=@PageId  					  				  			
			END

			ELSE IF @Opcode=42
			BEGIN
				IF EXISTS(SELECT map.AutoId FROM ManagePageAccess map 
				INNER JOIN Module m ON m.AutoId=map.ModuleAutoId
				INNER JOIN PageMaster pm ON pm.ParentModuleAutoId=map.ModuleAutoId
				INNER JOIN EmployeeTypeMaster etm ON etm.AutoId=map.Role
				Where map.Role=@EmpAutoId AND pm.PageUrl=@PageUrl)
				BEGIN
						SET @isException=0
						SET @exceptionMessage='success'
				END
				ELSE IF NOT EXISTS(SELECT map.AutoId FROM ManagePageAccess map 
				INNER JOIN Module m ON m.AutoId=map.ModuleAutoId
				INNER JOIN PageMaster pm ON pm.ParentModuleAutoId=map.ModuleAutoId
				INNER JOIN EmployeeTypeMaster etm ON etm.AutoId=map.Role
				Where pm.PageUrl=@PageUrl)
				BEGIN
						SET @isException=0
						SET @exceptionMessage='success'
				END
				ELSE
				BEGIN
				      IF @EmpAutoId=1
					  BEGIN
					    SET @isException=0
						SET @exceptionMessage='success'
					  END
					  ELSE
					  BEGIN
						SET @isException=1
						SET @exceptionMessage='InvalidAccess'
					  END
				END
			END

			ELSE IF @Opcode = 43
			 BEGIN		
			 
		       SELECT ROW_NUMBER() OVER(ORDER BY CreateDate DESC) AS RowNumber, * INTO #Results from                      
               (  			  
			    select  AutoId,PageId,PageUrl,PageTitle,CASE when Status=1 then 'Active' else 'Unactive' end AS Status,CreateDate,UserDescription,AdminDescription				
				from [dbo].[PageTitleMaster]
				Where  (@PageId is null or @PageId =0 or PageId like '%' + (convert(varchar(15),@PageId)) + '%')
				and (@PageUrl is null or @PageUrl ='' or PageUrl like '%' + @PageUrl + '%')
				and (@PageTitle is null or @PageTitle ='' or PageTitle like '%' + @PageTitle + '%')
				and (@FromDate is null or @FromDate = '' or @Todate is null or @Todate = '' or (CONVERT(DATE,CreateDate) between @FromDate AND @Todate))	
				and (Status= @Status)
				) as t order by CreateDate  
				SELECT COUNT(*) AS RecordCount, case when @PageSize=0 then COUNT(*) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Results  
				Select * from #Results  
				WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1   
		 END

		 ELSE IF @Opcode = 44
			 BEGIN		 
		       select PageId,PageUrl,PageTitle,Status,UserDescription,AdminDescription from PageTitleMaster where AutoId = @AutoId
		     END		
	END TRY
	BEGIN CATCH
	         SET @isException=1
			 SET @exceptionMessage=ERROR_MESSAGE()
	END CATCH
END

