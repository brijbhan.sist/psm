USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Credit_AdjustmentAmt]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
Alter table CreditMemoMaster drop column AdjustmentAmt

GO
 CREATE OR ALTER FUNCTION  [dbo].[FN_Credit_AdjustmentAmt]
(
	 @CreditAutoId int
)
RETURNS decimal(18,2)
AS
BEGIN
		DECLARE @AdjustmentAmt decimal(18,2)	
		SET @AdjustmentAmt=(SELECT (round(((([GrandTotal]-[OverallDiscAmt]))+[TotalTax])+[MLTax]+[WeightTaxAmount],(0))-
		(((([GrandTotal]-[OverallDiscAmt]))+[TotalTax])+[MLTax]+[WeightTaxAmount])) FROM CreditMemoMaster WHERE CreditAutoId=@CreditAutoId)	
	

	RETURN @AdjustmentAmt
END
GO
Alter table CreditMemoMaster Add AdjustmentAmt AS ([dbo].[FN_Credit_AdjustmentAmt]([CreditAutoId]))
GO
