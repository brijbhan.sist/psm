CREATE OR Alter PROCEDURE [dbo].[ProcMLQtyReport]
@Opcode INT=NULL,
@CustomerAutoId INT=NULL,
@ProductAutoId INT=NULL,
@StateAutoId int=null,
@FromDate date =NULL,
@ToDate date =NULL,
@PageIndex INT=1,
@PageSize INT=10,
@RecordCount INT=null,
@isException bit out,
@exceptionMessage varchar(max) out
AS
BEGIN	
	BEGIN TRY
		Set @isException=0
		Set @exceptionMessage='Success'

		If @Opcode=41		
		BEGIN
		         SELECT ROW_NUMBER() OVER(ORDER BY [CustomerName]) AS RowNumber, * INTO #Results FROM
				 (
					select cm.CustomerId,cm.CustomerName,Address,City,(select StateName from State as st 
					where st.AutoId=State) as StateName,Zipcode,MLQty,MLTax from 
					(
						select CustomerAutoId, BillAddrAutoId,SUM(ISNULL(MLQty,0)) as MLQty,sum(MLTax) as  MLTax from 
						(
							select CustomerAutoId, BillAddrAutoId, isnull((case when MLQty>0 then MLQty else oim.totalmlqty end),0.00)
							as MLQty,MLTax as MLTax	from ordermaster as om
							inner join
							(
							select OrderAutoId, sum(QtyDel*pm.MLQty) as totalmlqty from ProductMaster as pm
							inner join Delivered_Order_Items as doi on pm.AutoId=doi.ProductAutoId
							where MLQty>0 and ISNULL(IsExchange,0)=0 group by OrderAutoId having sum(QtyDel*pm.MLQty)>0
							) as oim on om.AutoId=oim.OrderAutoId
							where 
							(@CustomerAutoId is null or @CustomerAutoId=0 or om.CustomerAutoId=@CustomerAutoId)
							and (@FromDate is null or @ToDate is null or (CONVERT(date,OrderDate) 
							between convert(date,@FromDate) and CONVERT(date,@ToDate)))
							and Status=11  
						) as t group by CustomerAutoId, BillAddrAutoId
					)  as om
					inner join CustomerMaster as cm on cm.AutoId=om.CustomerAutoId
					inner join BillingAddress as bm on bm.AutoId=om.BillAddrAutoId
					where CustomerName not like '%test%' and (ISNULL(@StateAutoId,0)=0 or bm.State=@StateAutoId)
				  ) as t

				SELECT COUNT(CustomerName) AS RecordCount, case when @PageSize=0 then COUNT(CustomerName)
				else @PageSize end AS PageSize, @PageIndex AS PageIndex,Format(getdate(),'MM/dd/yyyy hh:mm tt') as PrintDate FROM #Results
				SELECT * FROM #Results
				WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))		
				
				 SELECT ISNULL(SUM(MLQty),0.00) AS MLQty,ISNULL(SUM(MLTax),0.00) AS MLTax from  #Results								
		END
		ELSE IF @Opcode=42
	   BEGIN
	   select cm.CustomerId,cm.CustomerName,Address,City,(select StateName from State as st  where st.AutoId=State) as StateName,Zipcode,
					MLQty,MLTax from 
					(
					select CustomerAutoId, BillAddrAutoId,SUM(ISNULL(MLQty,0)) as MLQty,sum(MLTax) as  MLTax from 
					(
					select CustomerAutoId, BillAddrAutoId, isnull((case when MLQty>0 then MLQty else oim.totalmlqty end),0.00) as MLQty, 
					MLTax as MLTax 
					from ordermaster as om
					inner join
					(
					select OrderAutoId, sum(QtyDel*pm.MLQty) as totalmlqty from ProductMaster as pm
					inner join Delivered_Order_Items as doi on pm.AutoId=doi.ProductAutoId
					where MLQty>0 and ISNULL(IsExchange,0)=0 group by OrderAutoId having sum(QtyDel*pm.MLQty)>0
					) as oim on om.AutoId=oim.OrderAutoId
					where 
						(@CustomerAutoId is null or @CustomerAutoId=0 or om.CustomerAutoId=@CustomerAutoId)
						and (@FromDate is null or @ToDate is null or (CONVERT(date,OrderDate) 
						between convert(date,@FromDate) and CONVERT(date,@ToDate)))
						and Status=11
					) as t
					group by CustomerAutoId, BillAddrAutoId
					)  as om
					inner join CustomerMaster as cm on cm.AutoId=om.CustomerAutoId
					inner join BillingAddress as bm on bm.AutoId=om.BillAddrAutoId
					where CustomerName not like '%test%'
					and (ISNULL(@StateAutoId,0)=0 or bm.State=@StateAutoId)
	   END
		ELSE IF @Opcode=43
		BEGIN
		select
		(
		SELECT AutoId AS CUID,CustomerId + ' ' + CustomerName as CUN FROM CustomerMaster ORDER BY CUN ASC
		for json path
		) as Customer,
		(
		SELECT AutoId SId ,StateName + CASE when StateCode is null then '' else ' ['+StateCode+'] ' end as SN from State ORDER BY SN ASC
		for json path
		) as State
		for json path

		END
		

	END TRY
	BEGIN CATCH
		Set @isException=1
		Set @exceptionMessage=ERROR_MESSAGE()
	END CATCH
END	
   
GO
