USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Delivered_NetAmount]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 ALTER TABLE [dbo].[Delivered_Order_Items] DROP COLUMN 	[NetPrice] 
 go
CREATE OR ALTER FUNCTION  [dbo].[FN_Delivered_NetAmount]
(
	 @AutoId int 
)
RETURNS decimal(18,2)
AS
BEGIN
	DECLARE @NetAmount decimal(18,2)		 	 
	SET @NetAmount=cast((SELECT (((UnitPrice/QtyPerUnit) * QtyDel)-(((UnitPrice/QtyPerUnit) * QtyDel)*isnull(Del_discount,0))/100) FROM [Delivered_Order_Items]
WHERE AutoId=@AutoId) as decimal (18,2))
RETURN @NetAmount
END

GO
ALTER TABLE [dbo].[Delivered_Order_Items] add 	[NetPrice]  AS ([dbo].[FN_Delivered_NetAmount]([AutoId]))