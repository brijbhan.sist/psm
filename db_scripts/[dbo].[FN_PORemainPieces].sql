 

ALTER FUNCTION  [dbo].[FN_PORemainPieces]
(
	 @POAutoId int,
	 @ProductAutoId int,
     @UnitAutoId int
	
)
RETURNS int
AS
BEGIN
	DECLARE @RemainPieces int	 
	set @RemainPieces=(select isnull(TotalPieces,0)-ISNULL(ReceivedPieces,0) from PurchaseProductsMaster where POAutoId=@POAutoId and ProductAutoId=@ProductAutoId  and Unit=@UnitAutoId)
	RETURN @RemainPieces
END
