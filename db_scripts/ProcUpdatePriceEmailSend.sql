 ALTER procedure [dbo].[ProcUpdatePriceEmailSend]
 @ProductAutoId INT 
 AS
 BEGIN 
	Declare @html varchar(Max)='',@ProductId varchar(12),@ProductName varchar(150),@ProductImage nvarchar(max),@UpdateDate varchar(50),
	@UpdatedBy varchar(50),@UnitType varchar(20),@Quantity int,@CostPrice decimal(18,2),@WholesaleMinimumPrice decimal(18,2),
	@RetailMinimumPrice decimal(18,2),@BasePrice decimal(18,2),@Free varchar(20),@Location varchar(50),@tr varchar(max)

	SELECT @ProductId=ISNULL(ProductId,''),@ProductName=ISNULL(ProductName,''),@ProductImage=ISNULL(((SELECT top 1 ImageUrl 
	FROM ProductImageUrl)+PM.ImageUrl),'') FROM ProductMaster AS PM
	WHERE PM.AutoId=@ProductAutoId

	select top 1 @UpdateDate=FORMAT(PD.UpdateDate,'MM/dd/yyyy hh:mm tt'),@UpdatedBy=EM.FirstName+' '+ISNULL(EM.LastName,'') 
	from PackingDetails as PD
	INNER JOIN EmployeeMaster AS EM ON EM.AutoId=PD.ModifiedBy
	where ProductAutoId=@ProductAutoId

	select Row_Number() over (order by PD.AutoId) as rownumber,ISNULL(UM.UnitType,'') as UnitName,ISNULL(PD.Qty,0) as Qty,ISNULL(PD.CostPrice,0) as CostPrice,
	ISNULL(PD.WHminPrice,0) as WHminPrice,ISNULL(PD.MinPrice,0) as MinPrice
	,ISNULL(PD.Price,0) as Price,case when PD.EligibleforFree=1 then 'Yes' else 'No' end as EligibleforFree,ISNULL(PD.Location,'') as Location  into #Result	from PackingDetails  AS PD INNER JOIN UnitMaster 
	AS UM on PD.UnitType=UM.AutoId	where ProductAutoId=@ProductAutoId

	set @html='	
		<table class="table tableCSS" border="1" style="width:100%;border: 1px solid black;border-collapse:collapse;"> 
		<tbody>
		<tr>
			<td><b>Product Id</b></td>
			<td>'+ISNULL(Convert(varchar(50),@ProductId),'')+'</td>
       </tr>
	   <tr>
			<td><b>Product Name</b></td>
			<td>'+ISNULL(Convert(varchar(50),@ProductName),'')+'</td>
       </tr>
	   <tr>
			<td><b>Product Image</b></td>
			
			<td><img src='+ISNULL(Convert(nvarchar(max),@ProductImage),'')+' alt="" style="width:50px;height:50px" class="CToWUd"></td>
       </tr>
	   <tr>
			<td><b>Update Date & Time</b></td>
			<td>'+ISNULL(Convert(varchar(50),@UpdateDate),'')+'</td>
       </tr>
	   <tr>
			<td><b>Updated By</b></td>
			<td>'+ISNULL(Convert(varchar(50),@UpdatedBy),'')+'</td>
       </tr>
		</tbody>
		</table><br>
		<table class="table tableCSS" border="1" style="width:100%;border: 1px solid black;border-collapse:collapse;"> 
		<thead>
		<tr>
			<td style=''text-align:center;''><b>Unit Type</b></td>
			<td style=''text-align:center;''><b>Quantity</b></td>
			<td style=''text-align:center;''><b>Cost Price</b></td>
			<td style=''text-align:center;''><b>Wholesale Minimum Price</b></td>
			<td style=''text-align:center;''><b>Retail Minimum Price</b></td>
			<td style=''text-align:center;''><b>Base Price</b></td>
			<td style=''text-align:center;''><b>Free</b></td>
			<td style=''text-align:center;''><b>Location</b></td>
       </tr>
	   </thead>
		<tbody>'
		declare @count int= (select count(1) from #Result),@num int=1
		while(@num<=@Count)
		BEGIN 
			select 
			@UnitType=ISNULL(UnitName,''),
			@Quantity=ISNULL(Qty,0),
			@CostPrice= ISNULL(CostPrice,0.00),
			@WholesaleMinimumPrice=ISNULL(WHminPrice,0.00),
			@RetailMinimumPrice=ISNULL(MinPrice,0.00),
			@BasePrice=ISNULL(Price,0.00),
			@Free=ISNULL(EligibleforFree,''),
			@Location=ISNULL(Location,'')
			from #Result where rownumber = @num 
				set @tr ='<tr>' 
								+	'<td style=''text-align:center;''>'+ ISNULL(convert(varchar(50),@UnitType),'') + '</td>'
								+	'<td style=''text-align:center;''>'+ ISNULL(convert(varchar(50),@Quantity),'') + '</td>'
								+	'<td style=''text-align:right;''>'+  ISNULL(convert(varchar(50),@CostPrice),'') + '</td>'
								+	'<td style=''text-align:right;''>'+  ISNULL(convert(varchar(50),@WholesaleMinimumPrice),'') + '</td>'
								+	'<td style=''text-align:right;''>'+  ISNULL(convert(varchar(50),@RetailMinimumPrice),'') + '</td>'
								+	'<td style=''text-align:right;''>'+  ISNULL(convert(varchar(50),@BasePrice),'') + '</td>'
								+	'<td style=''text-align:center;''>'+ ISNULL(convert(varchar(50),@Free),'') + '</td>'
								+	'<td style=''text-align:center;''>'+ ISNULL(convert(varchar(50),@Location),'') + '</td>'
						+ '</tr>'
		set @num = @num + 1
		SET @html=@html+@tr
		END
		SET @html=@html+'</tbody></table>'
	-------------------------------------------Code For Email---------------------------------------------------------------
	Declare  @Subject varchar(250),@FromName varchar(500),@FromEmailId varchar(500),@Port int,@SMTPServer varchar(150),@smtp_userName varchar(150),
	@Password varchar(50),@SSL  int,@BCCEmailId varchar(max),@ToEmailId varchar(500),@CCEmailId varchar(max)
	set @Subject = 'A1WHM -' +UPPER((select CompanyDistributerName from dbo.CompanyDetails))+' - Price Updated' 
	
	SELECT @FromEmailId=FromEmail,@FromName=DisplayName,@smtp_userName=SMTP_USERNAME,@Password=SMTP_PASSWORD,
	@SMTPServer=HOST,@Port=SMTPPORT,@SSL=EnableSsl from EmailServerMaster_AWS

	set @BCCEmailId=(select BCCEmail from Tbl_ReportEmailReceiver where Autoid=8);
	set @ToEmailId=(select ToEmail from Tbl_ReportEmailReceiver where Autoid=8)
	SET @CCEmailId=(select CCEmail from Tbl_ReportEmailReceiver where Autoid=8)
    
	EXEC [Emailing_DB].[dbo].[ProcSubmitEmail] 
	@Opcode=11,
	@FromEmailId =@FromEmailId,
	@FromName = @FromName,
	@smtp_userName=@smtp_userName,
	@Password = @Password,
	@SMTPServer = @SMTPServer,
	@Port =@Port,
	@SSL =@SSL,
	@ToEmailId =@ToEmailId,
	@CCEmailId =@CCEmailId,
	@BCCEmailId =@BCCEmailId,  
	@Subject =@Subject,
	@EmailBody = @html,
	@SentDate ='',
	@Status =0,
	@SourceApp ='PSM',
	@SubUrl ='Update Cost Price', 
	@isException=0,
	@exceptionMessage=''  
  END

