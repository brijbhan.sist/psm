USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_CreditMemo_WeightTotal_Oz_Qty]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create FUNCTION  [dbo].[FN_CreditMemo_WeightTotal_Oz_Qty]
(
	 @AutoId int
)
RETURNS decimal(18,2)
AS
BEGIN
	DECLARE @WeightQuantity decimal(18,2)	
	SET @WeightQuantity=isnull((select sum(isnull(WeightQuantity,0)) from CreditItemMaster where CreditAutoId=@AutoId),0.00)	
	RETURN @WeightQuantity
END
GO


Alter table CreditMemoMaster drop column [WeightTotalQuantity]
Drop function [FN_CreditMemo_WeightTotal_Oz_Qty]
Alter table CreditMemoMaster Add [WeightTotalQuantity] AS ([dbo].[FN_CreditMemo_WeightTotal_Oz_Qty]([CreditAutoId]))
