Alter PROCEDURE [dbo].[ProcPageTrackerMaster]                      
@Opcode int=NULL,                      
@AutoId int=null,                      
@UserAutoId int=null,   
@EmpTypeAutoId int = null,
@AccessFromDate datetime=null,  
@AccessToDate datetime=null,  
@PageUrl varchar(200)=null,                     
@IpAddress varchar(50)=null, 
@Orderby int = null,
@PageIndex INT=1,      
@PageSize INT=10, 
@RecordCount INT =null, 
@isException bit out,                      
@exceptionMessage varchar(max) out                      
AS                      
BEGIN                      
	BEGIN TRY                      
		SET @exceptionMessage='Success'                      
		SET @isException=0            
		IF @Opcode=11                       
		BEGIN  
			INSERT INTO PageTrackerMaster (UserAutoId,AccessDate,PageUrl,IpAddress) VALUES (@UserAutoId,GetDate(),@PageUrl,@IpAddress)                   
		END 
		IF @Opcode=41
		BEGIN  
			select AutoId,TypeName from EmployeeTypeMaster                   
		END 
		
		IF @Opcode=42      
		  BEGIN  
				  select (FirstName +' '+LastName)as UserName,AutoId,EmpId  from EmployeeMaster 
				  where EmpType = @EmpTypeAutoId AND Status=1 order by UserName ASC   
		  end  
		IF @Opcode=43      
		  BEGIN  

		 IF @Orderby < 2
		 BEGIN		 
		   SELECT ROW_NUMBER() OVER(ORDER BY ActionDate DESC) AS RowNumber, * INTO #Results from                      
            (  
			  select  mm.ModuleName,emt.TypeName,emp.FirstName+' '+ISNULL(lastName,'') as UserName,FORMAT(accessDate,'MM/dd/yyy') as ActionDate,ptm.PageUrl,COUNT(ptm.PageUrl) as Count,
				pm.ParentModuleAutoId
				from PageTrackerMaster as ptm
				inner join EmployeeMaster as emp on emp.AutoId=ptm.UserAutoId
				inner join EmployeeTypeMaster as emt on emt.AutoId=emp.EmpType
				inner join PageMaster as pm on pm.PageUrl=ptm.PageUrl
				inner join Module as mm on mm.AutoId=pm.ParentModuleAutoId
				Where (@AccessFromDate is null or @AccessFromDate='' or @AccessToDate is null or @AccessToDate='' or convert(date,accessDate) between Convert(date,@AccessFromDate)  
					and Convert(date,@AccessToDate)) 
					and (ISNULL(@UserAutoId,0)=0 or ptm.UserAutoId=@UserAutoId) 
					and (ISNULL(@EmpTypeAutoId,0)=0 or emp.EmpType=@EmpTypeAutoId)
					and emp.Status = 1
				group by  mm.ModuleName,emt.TypeName,emp.FirstName+' '+ISNULL(lastName,''),FORMAT(accessDate,'MM/dd/yyy') ,ptm.PageUrl,
				pm.ParentModuleAutoId				 
				) as t order by ActionDate DESC  
					  SELECT COUNT(*) AS RecordCount, case when @PageSize=0 then COUNT(*) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Results  
					  Select * from #Results  
					  WHERE @PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)   
		 END
		 ELSE IF @Orderby = 2
		 BEGIN
		 
		   SELECT ROW_NUMBER() OVER(ORDER BY Count DESC) AS RowNumber, * INTO #Results1 from                      
            (  
			  select  mm.ModuleName,emt.TypeName,emp.FirstName+' '+ISNULL(lastName,'') as UserName,FORMAT(accessDate,'MM/dd/yyy') as ActionDate,ptm.PageUrl,COUNT(ptm.PageUrl) as Count,
				pm.ParentModuleAutoId
				from PageTrackerMaster as ptm
				inner join EmployeeMaster as emp on emp.AutoId=ptm.UserAutoId
				inner join EmployeeTypeMaster as emt on emt.AutoId=emp.EmpType
				inner join PageMaster as pm on pm.PageUrl=ptm.PageUrl
				inner join Module as mm on mm.AutoId=pm.ParentModuleAutoId
				Where (@AccessFromDate is null or @AccessFromDate='' or @AccessToDate is null or @AccessToDate='' or convert(date,accessDate) between Convert(date,@AccessFromDate)  
					and Convert(date,@AccessToDate)) 
					and (ISNULL(@UserAutoId,0)=0 or ptm.UserAutoId=@UserAutoId) 
					and (ISNULL(@EmpTypeAutoId,0)=0 or emp.EmpType=@EmpTypeAutoId)
					and emp.Status = 1
				group by  mm.ModuleName,emt.TypeName,emp.FirstName+' '+ISNULL(lastName,''),FORMAT(accessDate,'MM/dd/yyy') ,ptm.PageUrl,
				pm.ParentModuleAutoId				 
				) as t order by Count DESC  
					  SELECT COUNT(*) AS RecordCount, case when @PageSize=0 then COUNT(*) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Results1  
					  Select * from #Results1  
					  WHERE @PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)   
		 END
	END
	END TRY                      
	BEGIN CATCH                      
		SET @isException=1                      
		SET @exceptionMessage=ERROR_MESSAGE()              
	END CATCH                      
END 
GO
