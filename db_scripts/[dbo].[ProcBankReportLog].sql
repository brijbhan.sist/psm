ALTER PROCEDURE [dbo].[ProcBankReportLog]                                                
@Opcode INT =null,                                                  
@FromDate Date = null,                                                
@ToDate Date = null,                                                
@PageIndex int=null,   
@ReportAutoId int=null,
@PageSize  int=10,                                                   
@RecordCount int=null,                                                   
@isException bit out,                                                    
@exceptionMessage varchar(max) out                                                  
as                                                
begin                                                  
begin try                                                  
SET @exceptionMessage= 'Success'                                                    
   SET @isException=0                                                 
   if @Opcode = 41                                                
   begin                                                
                                                  
  select ROW_NUMBER() over(order by Convert(date,ReportDate) desc) as RowNumber,FORMAT([ReportDate],'MM/dd/yyyy') as PaymentDate,                                      
  [StoreCreditGenerated] as CreditAmount,                                      
  [TotalCustomer] as TotalNoOfCustomer,[TotalOrder]  as noOfOrder, [CASH] as TotalCash, [Check] as TotalCheck                                   
  ,[MoneyOrder] as TotalMoneyOrder,[ElectronicTransfer] as TotalElectronicTransfer,    AutoId ,                                                
   [CreditCard] as TotalCreditCard,[StoreCreditApply] as TotalStoreCredit,
   [TotalTransaction] as TotalPaid,                   
  [Short] as Short, [Expense] as Expense ,(DepositAmount) as DepositAmount
  into #Result from [dbo].[Tbl_DailyBankReport] as t                                
  where                       
  (ISNULL(@FromDate,'')='' OR ISNULL(@ToDate,'') = '' or (([ReportDate]) BETWEEN (@FromDate) AND( @ToDate)))                     
  order by Convert(date,[ReportDate]) desc                                            
                                                
  SELECT * FROM #Result                                                
  WHERE @PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)  
  order by Convert(date,PaymentDate) desc                                                 
  SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize=0 then COUNT(RowNumber) else @PageSize end AS PageSize, @PageIndex AS PageIndex,Format(getdate(),'MM/dd/yyyy hh:mm tt') as PrintDate FROM #Result                             
                            
  select sum(ISNULL(TotalNoOfCustomer,0)) as TotalNoOfCustomer,sum(ISNULL(noOfOrder,0)) as noOfOrder,sum(ISNULL(TotalCash,0.00)) as TotalCash,                      
  sum(ISNULL(CreditAmount,0.00)) as CreditAmount,  sum(ISNULL(TotalElectronicTransfer,0.00)) as TotalElectronicTransfer,                        
  sum(ISNULL(TotalCheck,0.00)) as TotalCheck,sum(ISNULL(TotalMoneyOrder,0.00)) as TotalMoneyOrder,sum(ISNULL(TotalCreditCard,0.00)) as TotalCreditCard                          
  ,sum(ISNULL(TotalStoreCredit,0.00)) as TotalStoreCredit,sum(ISNULL(TotalPaid,0.00)) as TotalPaid,sum(ISNULL(Short,0.00)) as Short,sum(ISNULL(Expense,0.00)) as Expense ,                          
   SUM(ISNULL(DepositAmount,0)) as TotalDepositAmount
   from #Result   
   

   end                                        
   if @Opcode = 42                                                
    begin                     
                                                
  select ROW_NUMBER() over(order by [ReportDate]) as RowNumber,FORMAT([ReportDate],'MM/dd/yyyy') + ':' + FORMAT([ReportDate],'dddd')                                        
  as PaymentDate,[TotalCustomer] as TotalNoOfCustomer,     
  [StoreCreditGenerated] as CreditAmount,                           
  [TotalOrder] as noOfOrder,        
  [CASH] as TotalCash,[Check] as TotalCheck,[MoneyOrder] as TotalMoneyOrder,                                                 
  [CreditCard] as TotalCreditCard,[StoreCreditApply] as TotalStoreCredit,[ElectronicTransfer] as TotalElectronicTransfer,                                  
  [TotalTransaction] as TotalPaid, [Short] as Short,[Expense] as Expense,DepositAmount from [dbo].[Tbl_DailyBankReport]
  where AutoId=@ReportAutoId
   order by PaymentDate                                                
	 
  select CustomerName,OrderNo,Cash,[Check] as tCheck,[MoneyOrder] as MoneyOrder,                                              
  [CreditCard] as CreditCard,[StoreCredit] as StoreCredit,[ElectronicTransfer] as ElectronicTransfer,                                        
  [TotalAmount] as TotalAmount,CONVERT(varchar(15),CAST([SettlementTime] AS TIME),100) as STime,[CheckNO] as ChequeNo                                               
  from [Tbl_DailybankReport_OrderSettlmentLog]  where ReportAutoId=@ReportAutoId                                
                                                
 

  select CurrencyName,[Noofcount] as NoOfValue,[TotalAmount] as TotalAmount from [dbo].[Tbl_DailyBankReport_CashDetails] where ReportAutoId=@ReportAutoId

  select [SettlementId] as PaymentId,FORMAT(SettlementDate,'MM/dd/yyyy') as PaymentDate,[PaymentMode] as PaymentMode,ReceivedAmount,[ShortAmount] as SortAmount,[ReceivedBy] as ReceivedBy,                          
  [SettlementBy] as SettlementBy,CustomerId,CustomerName                          
  from [dbo].[Tbl_DailyBankReport_ShortAmountDetails]  where ReportAutoId=@ReportAutoId                            
                
 select [SettlementId] as SettlementId,CustomerName, Format(CheckDate, 'MM/dd/yyyy') as ChequeDate,[CheckNo] as ChequeNo,      
[CheckAmount] as ReceivedAmount,[Remark] as CancelRemarks,Format(SettlementDate,'MM/dd/yyyy') as SettlementDate,      
format(CancelDate,'MM/dd/yyyy') as CancelDate,[CancelBy] as CancelBy,      
[ReferenceOrderNo] as OrderNo from [dbo].[Tbl_DailyBankReport_CheckcancelledDetails]  where ReportAutoId=@ReportAutoId      
                                                             
                                  
  Select  [StoreCredit] as CreditAmount,[PaymentMode] as PaymentMode,[SettlementId] as PaymentId,Format([SettlementDate],'MM/dd/yyyy') as PaymentDate,                        
  [ReceivedBy] as ReceivedBY,CustomerId,CustomerName ,[SettlementBy] as SettleBy                                
  from [dbo].[Tbl_DailyBankReport_GenerateCreditDetails]  where ReportAutoId=@ReportAutoId                               
                       
	Select [PaymentMode] as PaymentMode,[Expensedescription] as ExpenseDescription,[ExpenseAmount] as ExpenseAmount, [ExpenseBy] as ExpenseBy from                 
	[dbo].[Tbl_DailyBankReport_ExpenseDetails] where ReportAutoId=@ReportAutoId
  
   end                                                
   end try                                                  
  begin catch                             
   SET @isException=1                                                  
   SET @exceptionMessage= ERROR_MESSAGE()                                                   
end catch                                                  
end 
