USE [psmnj.a1whm.com]
GO

/****** Object:  Table [dbo].[Delete_tbl_OrderLog_Backup]    Script Date: 04/14/2020 22:12:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Delete_tbl_OrderLog_Backup](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[ActionTaken] [int] NULL,
	[EmpAutoId] [int] NULL,
	[ActionDate] [datetime] NULL,
	[Remarks] [varchar](200) NULL,
	[OrderAutoId] [int] NULL,
 CONSTRAINT [PK_Delete_tbl_OrderLog_Backup] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


