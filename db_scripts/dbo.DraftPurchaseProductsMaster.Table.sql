USE [psmnj.a1whm.com]
GO
/****** Object:  Table [dbo].[DraftPurchaseProductsMaster]    Script Date: 12/03/2019 5.44.05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DraftPurchaseProductsMaster](
	[ItemAutoId] [int] IDENTITY(1,1) NOT NULL,
	[ProductAutoId] [int] NULL,
	[Unit] [int] NULL,
	[QtyPerUnit] [int] NULL,
	[Qty] [int] NULL,
	[DraftAutoId] [int] NULL
) ON [PRIMARY]
GO
