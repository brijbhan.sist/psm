  
alter FUNCTION  [dbo].[FN_PackingDetails_Validated]    
(    
 @AutoId int    
)    
RETURNS int    
AS    
BEGIN    
declare @status int    
SET @status =(select COUNT(1) from (    
		select    
		CostPrice,    
		cast((select CostPrice/qty from PackingDetails as pdi where pdi.ProductAutoId=pd.ProductAutoId    
		and pdi.UnitType=pm.PackingAutoId) *Qty as decimal(10,2)) as NewCostPrice,    
		Price,    
		cast((select Price/qty from PackingDetails as pdi where pdi.ProductAutoId=pd.ProductAutoId    
		and pdi.UnitType=pm.PackingAutoId) *Qty as decimal(10,2)) as NewPrice,    
		MinPrice,    
		cast((select MinPrice/qty from PackingDetails as pdi where pdi.ProductAutoId=pd.ProductAutoId    
		and pdi.UnitType=pm.PackingAutoId) *Qty as decimal(10,2)) as NewMinPrice,    
		   WHminPrice,    
		cast((select WHminPrice/qty from PackingDetails as pdi where pdi.ProductAutoId=pd.ProductAutoId    
		and pdi.UnitType=pm.PackingAutoId) *Qty as decimal(10,2)) as NewWHminPrice    
		from PackingDetails as pd    
		inner join ProductMaster as pm on pm.AutoId=pd.ProductAutoId where pd.AutoId=@AutoId    
) as t    
where    
costPrice!=NewCostPrice    
or    
Price!=NewPrice    
or    
MinPrice!=NewMinPrice    
or    
WHminPrice!=NewWHminPrice    
or (Price<MinPrice or MinPrice<WHminPrice or WHminPrice<costPrice)  
)  
return @status    
END  
  