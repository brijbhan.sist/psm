USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Order_Weigth_OZQty]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

alter table OrderMaster drop column Weigth_OZQty
drop function FN_Order_Weigth_OZQty
go
CREATE FUNCTION  [dbo].[FN_Order_Weigth_OZQty]
(
@OrderAutoId int
)
RETURNS decimal(18,2)
AS
BEGIN
DECLARE @Weight_Oz decimal(18,2)	
	IF EXISTS(SELECT 1 FROM OrderMaster WHERE AutoId=@OrderAutoId AND Status=11)
	BEGIN
		SET @Weight_Oz=isnull((select SUM(ISNULL(Del_Weight_Oz_TotalQty ,0)) from [Delivered_Order_Items] 
		where OrderAutoId=@OrderAutoId),0.00)
	END
	ELSE
	BEGIN
		SET @Weight_Oz=isnull((select SUM(ISNULL(Weight_Oz_TotalQty ,0)) from orderItemMaster 
		where OrderAutoId=@OrderAutoId),0.00)
	END
RETURN @Weight_Oz
END
GO
alter table OrderMaster add [Weigth_OZQty]  AS ([dbo].[FN_Order_Weigth_OZQty]([autoid]))
