USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[CleanOrder]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CleanOrder] 
AS
BEGIN
	BEGIN TRY
  
	DELETE FROM tempProductDetails
	DELETE FROM BillItems
	DELETE FROM Draft_StockItemMaster
	DELETE FROM Draft_StockEntry
	--DELETE FROM Country
	DELETE FROM CreditItemMaster
	DELETE FROM CreditMemoDeductionlog
	DELETE FROM CreditMemoLog
	DELETE FROM CreditMemoMaster
	DELETE FROM CreditMoneyLog
	DELETE FROM CustomerCreditMaster	
	DELETE FROM CustomerPaymentDetails
	DELETE FROM CustomerPriceLevel
	--DELETE FROM CustomerTerms
	--DELETE FROM CustomerType
	DELETE FROM CustomerWisePricing
	DELETE FROM Delivered_Order_Items
	--DELETE FROM CompanyDetails
	DELETE FROM DeliveredOrders
	DELETE FROM DraftItemMaster
	DELETE FROM DraftOrderMaster
	DELETE FROM DriverRootLog
	DELETE FROM DrvLog
	
	DELETE FROM GenPacking
	DELETE FROM InvItemsList
	DELETE FROM InvoiceMaster
	DELETE FROM ItemBarcode
	DELETE FROM MessageAssignTo
	DELETE FROM MessageMaster
	DELETE FROM CustomerDocumentMaster
	DELETE FROM Order_Original
	DELETE FROM OrderItemMaster
	DELETE FROM OrderItems_Original
	DELETE FROM CheckMaster
	DELETE FROM OrderMaster
	DELETE FROM PackingDetails
	DELETE FROM PaymentLog
	DELETE FROM PaymentOrderDetails
	DELETE FROM PriceLevelMaster
	DELETE FROM ProductPricingInPriceLevel
	DELETE FROM SalesPaymentMaster
	--DELETE FROM SequenceCodeGeneratorMaster
	DELETE FROM ShippingAddress
	--DELETE FROM ShippingType
	--DELETE FROM State
	--DELETE FROM StatusMaster
	DELETE FROM StockEntry
	
	DELETE FROM TaxTypeMaster
	--DELETE FROM tbl_ActionMaster
	DELETE FROM tbl_OrderLog
	DELETE FROM tempTable
	DELETE FROM ProductMaster
	DELETE FROM UnitMaster
	DELETE FROM VendorMaster	
	--DELETE FROM BillingAddress
	--DELETE FROM SubCategoryMaster
--	DELETE FROM CategoryMaster
	--DELETE FROM CustomerMaster	
	--DELETE FROM EmployeeTypeMaster
	--DELETE FROM EmployeeMaster
 

	END TRY
	BEGIN CATCH
	   select 1
	END CATCH
END




GO
