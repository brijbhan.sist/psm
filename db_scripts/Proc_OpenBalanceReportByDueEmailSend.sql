 ALTER procedure Proc_OpenBalanceReportByDueEmailSend
 AS
 BEGIN 
	declare  @DriveLocation varchar(150)
	SET @DriveLocation='D:\OpenBalanceReport\'+UPPER((select CompanyId from dbo.CompanyDetails))+'_OpenBalanceReportByDue_'+
	convert(varchar(50),Format(GetDate(),'MM_dd_yyyy_hh_mm_tt'))+'.html'   

	select om.AutoID,CustomerAutoId,OrderDate,FirstName+' '+ISNULL(LastName,'') as SalesPerson 
	into #OrderAutoid 
	from OrderMaster as om              
	inner join DeliveredOrders as do on om.AutoId=do.OrderAutoId and ISNULL(dueOrderStatus,0)=0
	inner join  CustomerMaster as cm on cm.AutoId=om.CustomerAutoId             
	inner join  EmployeeMaster as emp on cm.SalesPersonAutoId=emp.AutoId where               
	om.status=11                
                                                            
	select ROW_NUMBER() over(order by SalesPerson) as RowNumber, CustomerName,SUM(isnull([0-30 Days],0)) as Days30,
	sum([31-60 Days]) as Days60, sum([61-90 Days]) as Days90 ,sum([90-Above]) as Above90,COUNT(1) as NoofOrder,
	sum([Order Total]) as OrderTotal,sum(AmtDue) as AmtDue,sum(TotalPaid) as TotalPaid,SalesPerson into #Result42 from (                                          
	select cm.CustomerName,SalesPerson,                                           
	(                                          
	CASE                                
	WHEN DATEDIFF(DD,OrderDate,GETDATE())<=30 THEN do.AmtDue else 0 end                                           
	)                                          
	as [0-30 Days],                                    (                                          
	CASE                                                                
	WHEN DATEDIFF(DD,OrderDate,GETDATE())between 31 and 60 THEN do.AmtDue else 0 end             
	)                                          
	as [31-60 Days],                                          
	(                                          
	CASE                                                                
	WHEN DATEDIFF(DD,OrderDate,GETDATE())between 61 and 90 THEN do.AmtDue else 0 end                                           
	)                                          
	as [61-90 Days]                                          
	,                                          
	(                                           
	CASE                                                                
	WHEN DATEDIFF(DD,OrderDate,GETDATE())> 90 THEN do.AmtDue else 0 end                                           
	)                                          
	as [90-Above],                                          
	isnull((do.PayableAmount),0.00) as [Order Total],                                          
	isnull((do.AmtPaid),0.00) as TotalPaid,((isnull(do.PayableAmount,0.00)-isnull(do.AmtPaid,0.00))) as AmtDue                                          
	from  #OrderAutoid  as do1                                                                    
	inner join  DeliveredOrders as do on do1.AutoId=do.OrderAutoId              
	inner join  CustomerMaster as cm on cm.AutoId=do1.CustomerAutoId                                                     
	where                                             
	(isnull(do.AmtDue,0.00))> 0                                          
                                          
	) as t                                           
	group by  SalesPerson,CustomerName    
	
	Declare	@CustomerName varchar(50),@SalesPerson varchar(50),@Days30 decimal(18,2),@Days60 decimal(18,2),@Days90 decimal(18,2),
    @Above90 decimal(18,2),@NoofOrder int,@OrderTotal decimal(18,2),@AmtDue decimal(18,2),@TotalPaid DECIMAL(18,2),@Count int,
	@num int=1,@html varchar(max)='',@tr varchar(max)='',@Sp varchar(50)=''
	SET @Count=(select count(1) from #Result42)

	if(@count>0)
	begin
	     set @html='<table class="table tableCSS" border="1" style="width:100%;border: 1px solid black;border-collapse:collapse;">
		<thead>
			<tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
				<td style="text-align: center !important;">
					<h4 style="margin:10px;">Open Balance Report - By Due</h4>
				</td>
				</tr>
				<tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
				<td style="text-align: center !important;">
					<h4 style="margin:2px">'+(select CompanyDistributerName from CompanyDetails)+'</h4>
				</td>
				</tr>
				<tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
				<td style="text-align: center !important;">
					<h4 style="margin:2px">'+Convert(varchar(50),FORMAT(GetDate(),'MM/dd/yyyy hh:mm tt'))+'</h4>
				</td>
			</tr>
		</thead>
		<tbody></tbody>
		</table>
		<br>
		<table class="table tableCSS" border="1" style="width:100%;border: 1px solid black;border-collapse:collapse;"> 
		<thead>
		<tr style="background: #ffe0b2;font-weight: 700;border-top: 1px solid #5f7c8a">
			<td style="text-align: center !important;" class="NoOfValue text-center">Sales Person</td>
			<td style="text-align: center !important;" class="CurrencyName text-center">Customer</td>
			<td style="text-align: center !important;" class="TotalAmount text-right">0-30 Days	</td>
			<td style="text-align: center !important;" class="TotalAmount text-right">31-60 Days</td>
			<td style="text-align: center !important;" class="TotalAmount text-right">61-90 Days</td>
			<td style="text-align: center !important;" class="TotalAmount text-right">90-Above</td>
			<td style="text-align: center !important;" class="TotalAmount text-right">No of Orders</td>
			<td style="text-align: center !important;" class="TotalAmount text-right">Total Amount</td>
			<td style="text-align: center !important;" class="TotalAmount text-right">Total Paid</td>
			<td style="text-align: center !important;" class="TotalAmount text-right">Total Due</td>
		</tr>' +   
		'</thead> <tbody>'
		EXEC dbo.spWriteToFile @DriveLocation, @html
		SET @html=''
		while(@num<=@Count)
		BEGIN 
			select 
			@CustomerName=CustomerName,
			@SalesPerson=SalesPerson,
			@Sp= case when @num=1 then SalesPerson else @Sp end,
			@Days30=Days30,
			@Days60=Days60,
			@Days90=Days90,
			@Above90=Above90,
			@NoofOrder=NoofOrder,
			@OrderTotal=OrderTotal,
			@TotalPaid=TotalPaid,
			@AmtDue=AmtDue
			from #Result42 where rownumber = @num 
			
			
			if(@Sp!=@SalesPerson and @num!=1)
			BEGIN
				set @tr ='<tr style=''background:#ffe0b2;font-weight:600;font-size:17px''>'  
								+	'<td style=''text-align:center;'' colspan=''2''>Total</td>' 
								+	'<td style=''text-align:right;''>'+ convert(varchar(50),(select sum(isnull(Days30,'0.00')) from #Result42 where SalesPerson=@Sp)) + '</td>'
								+	'<td style=''text-align:right;''>'+ convert(varchar(50),(select sum(isnull(Days60,'0.00')) from #Result42 where SalesPerson=@Sp)) + '</td>' 
								+	'<td style=''text-align:right;''>'+ convert(varchar(50),(select sum(isnull(Days90,'0.00')) from #Result42 where SalesPerson=@Sp)) + '</td>' 
								+	'<td style=''text-align:right;''>'+ convert(varchar(50),(select sum(isnull(Above90,'0.00')) from #Result42 where SalesPerson=@Sp)) + '</td>'
								+	'<td style=''text-align:center;''>'+ convert(varchar(50),(select sum(isnull(NoofOrder,'0.00')) from #Result42 where SalesPerson=@Sp)) + '</td>'
								+	'<td style=''text-align:right;''>'+ convert(varchar(50),(select sum(isnull(OrderTotal,'0.00')) from #Result42 where SalesPerson=@Sp)) + '</td>'
								+	'<td style=''text-align:right;''>'+ convert(varchar(50),(select sum(isnull(TotalPaid,'0.00')) from #Result42 where SalesPerson=@Sp)) + '</td>'
								+	'<td style=''text-align:right;''>'+ convert(varchar(50),(select sum(isnull(AmtDue,'0.00')) from #Result42 where SalesPerson=@Sp)) + '</td>'
						+ '</tr>'
				EXEC dbo.spWriteToFile @DriveLocation, @tr
			END
			set @tr ='<tr>' 
							+	'<td style=''text-align:left;''>'+ convert(varchar(50), @SalesPerson) + '</td>' 
							+	'<td style=''text-align:left;''>'+convert(varchar(50), @CustomerName) + '</td>' 
							+	'<td style=''text-align:right;''>'+ convert(varchar(50), @Days30) + '</td>'
							+	'<td style=''text-align:right;''>'+ convert(varchar(50), @Days60) + '</td>' 
							+	'<td style=''text-align:right;''>'+ convert(varchar(50), @Days90) + '</td>' 
							+	'<td style=''text-align:right;''>'+ convert(varchar(50), @Above90) + '</td>'
							+	'<td style=''text-align:center;''>'+ convert(varchar(50), @NoofOrder) + '</td>'
							+	'<td style=''text-align:right;''>'+ convert(varchar(50), @OrderTotal) + '</td>'
							+	'<td style=''text-align:right;''>'+ convert(varchar(50), @TotalPaid) + '</td>'
							+	'<td style=''text-align:right;''>'+ convert(varchar(50), @AmtDue) + '</td>'
					+ '</tr>'
			EXEC dbo.spWriteToFile @DriveLocation, @tr
			SET @Sp=(select SalesPerson from #Result42 where rownumber = @num)
			set @num = @num + 1
		END
		 
			set @tr ='<tr style=''background:#ffe0b2;font-weight:600;font-size:17px''>'  
						+	'<td style=''text-align:center;'' colspan=''2''>Total</td>' 
						+	'<td style=''text-align:right;''>'+ convert(varchar(50),(select sum(isnull(Days30,'0.00')) from #Result42 where SalesPerson=@Sp)) + '</td>'
						+	'<td style=''text-align:right;''>'+ convert(varchar(50),(select sum(isnull(Days60,'0.00')) from #Result42 where SalesPerson=@Sp)) + '</td>' 
						+	'<td style=''text-align:right;''>'+ convert(varchar(50),(select sum(isnull(Days90,'0.00')) from #Result42 where SalesPerson=@Sp)) + '</td>' 
						+	'<td style=''text-align:right;''>'+ convert(varchar(50),(select sum(isnull(Above90,'0.00')) from #Result42 where SalesPerson=@Sp)) + '</td>'
						+	'<td style=''text-align:center;''>'+ convert(varchar(50),(select sum(isnull(NoofOrder,'0.00')) from #Result42 where SalesPerson=@Sp)) + '</td>'
						+	'<td style=''text-align:right;''>'+ convert(varchar(50),(select sum(isnull(OrderTotal,'0.00')) from #Result42 where SalesPerson=@Sp)) + '</td>'
						+	'<td style=''text-align:right;''>'+ convert(varchar(50),(select sum(isnull(TotalPaid,'0.00')) from #Result42 where SalesPerson=@Sp)) + '</td>'
						+	'<td style=''text-align:right;''>'+ convert(varchar(50),(select sum(isnull(AmtDue,'0.00')) from #Result42 where SalesPerson=@Sp)) + '</td>'
				+ '</tr>'
		EXEC dbo.spWriteToFile @DriveLocation, @tr
		set @html =' 
		<tfoot>
			<tr style=''background:#ffe0b2;font-weight:600;font-size:17px''>
				<td colspan="2" style="text-align:center"><b>Overall Total</b></td>
				<td id="CashTotal" style="text-align:right"><b>'+convert(varchar(50),(select sum(isnull(Days30,'0.00')) from #Result42))+'</b></td>
				<td id="CashTotal" style="text-align:right"><b>'+convert(varchar(50),(select sum(isnull(Days60,'0.00')) from #Result42))+'</b></td>
				<td id="CashTotal" style="text-align:right"><b>'+convert(varchar(50),(select sum(isnull(Days90,'0.00')) from #Result42))+'</b></td>
				<td id="CashTotal" style="text-align:right"><b>'+convert(varchar(50),(select sum(isnull(Above90,'0.00')) from #Result42))+'</b></td>
			    <td id="CashTotal" style="text-align:center"><b>'+convert(varchar(50),(select sum(isnull(NoofOrder,'0.00')) from #Result42))+'</b></td>
				<td id="CashTotal" style="text-align:right"><b>'+convert(varchar(50),(select sum(isnull(OrderTotal,'0.00')) from #Result42))+'</b></td>
				<td id="CashTotal" style="text-align:right"><b>'+convert(varchar(50),(select sum(isnull(TotalPaid,'0.00')) from #Result42))+'</b></td>
				<td id="CashTotal" style="text-align:right"><b>'+convert(varchar(50),(select sum(isnull(AmtDue,'0.00')) from #Result42))+'</b></td>
			</tr>
		</tfoot>
		</table><p style="page-break-before: always"></p>'
		EXEC dbo.spWriteToFile @DriveLocation, @html 
	END
	-------------------------------------------Code For Email---------------------------------------------------------------
	Declare  @Subject varchar(250),@FromName varchar(500),@FromEmailId varchar(500),@Port int,@SMTPServer varchar(150),@smtp_userName varchar(150),
	@Password varchar(50),@SSL  int,@BCCEmailId varchar(max),@ToEmailId varchar(500),@CCEmailId varchar(max)
	set @Subject = 'Open Balance Report By Due ' +UPPER((select CompanyDistributerName from dbo.CompanyDetails))+' : ' + 
	format(GETDATE(),'MM/dd/yyy hh:mm tt');

	SELECT @FromEmailId=FromEmail,@FromName=DisplayName,@smtp_userName=SMTP_USERNAME,@Password=SMTP_PASSWORD,
	@SMTPServer=HOST,@Port=SMTPPORT,@SSL=EnableSsl from EmailServerMaster_AWS

	set @BCCEmailId=(select BCCEmail from Tbl_ReportEmailReceiver where Autoid=6);
	set @ToEmailId=(select ToEmail from Tbl_ReportEmailReceiver where Autoid=6)
	SET @CCEmailId=(select CCEmail from Tbl_ReportEmailReceiver where Autoid=6)
    
	EXEC [Emailing_DB].[dbo].[ProcSubmitEmail] 
	@Opcode=11,
	@FromEmailId =@FromEmailId,
	@FromName = @FromName,
	@smtp_userName=@smtp_userName,
	@Password = @Password,
	@SMTPServer = @SMTPServer,
	@Port =@Port,
	@SSL =@SSL,
	@ToEmailId =@ToEmailId,
	@CCEmailId =@CCEmailId,
	@BCCEmailId =@BCCEmailId,  
	@Subject =@Subject,
	@EmailBody = '',
	@SentDate ='',
	@Status =0,
	@SourceApp ='PSM',
	@SubUrl ='OpenBalanceReportByDue',
	@attachment=@DriveLocation,
	@isException=0,
	@exceptionMessage=''  
  END