USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[PSMCT_ProductSold_SalesPerson]    Script Date: 1/5/2021 6:25:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE  [dbo].[PSMCT_ProductSold_SalesPerson]
 @orderfrom datetime ,
 @todate datetime,
 @PageSize int,
 @PageIndex int,
 @isException bit out,
 @exceptionMessage varchar(max) out
AS
BEGIN
SET @isException=0
SET @exceptionMessage=''
     BEGIN TRY
		select *, (Net_Sold_Default_Qty * PSMCT_COST)PSMCT_NET_COST into #t1 from
		(
		select 
		njpm.ProductId,njpm.ProductName,njum.UnitType,njpd.Qty
		--11/30/2020 by Nilay
		-- Mitulbhai called and told : if not customprice then NJ Cost price, [was on WH min price]

		,(case when isnull(pppl.CustomPrice,0) = 0 then njpd.CostPrice else CustomPrice end ) PSMCT_COST
		,convert(decimal(18,2),((ISNULL(t1.QtyDel,0))/njpd.Qty)) as [Sold_Default_Qty]		
		,convert(decimal(18,2),((isnull(t2.QtyDel,0))/njpd.Qty)) as [CM_Default_Qty]	
		,convert(decimal(18,2),((ISNULL(t1.QtyDel,0)-isnull(t2.QtyDel,0))/njpd.Qty)) as [Net_Sold_Default_Qty]
		,(t1.NetPrice  ) as Net_Sale_Rev		
		,( isnull(t2.NetPrice,0)) as Net_CM_Rev		
		,(t1.NetPrice - isnull(t2.NetPrice,0)) as Net_Total_Sales	
		from 
		[psmnj.a1whm.com].dbo.ProductMaster as njpm
		inner join [psmnj.a1whm.com].dbo.PackingDetails as njpd on njpd.ProductAutoId=njpm.AutoId and njpd.UnitType = njpm.PackingAutoId
		inner join [psmnj.a1whm.com].dbo.UnitMaster as njum on njum.AutoId=njpd.UnitType
		left join
		(
		select  
					pm.ProductId,   pm.PackingAutoId,pd.UnitType, pd.Qty			
				,sum(doi.QtyDel) as QtyDel
				,sum(doi.NetPrice) as NetPrice			
		from [psmct.a1whm.com].dbo.OrderMaster as om
			inner join [psmct.a1whm.com].dbo.Delivered_Order_Items as doi on doi.OrderAutoId=om.AutoId
			inner join [psmct.a1whm.com].dbo.ProductMaster as pm on pm.AutoId=doi.ProductAutoId
			inner join [psmct.a1whm.com].dbo.PackingDetails as pd on pd.ProductAutoId=pm.AutoId and pd.UnitType=pm.PackingAutoId			 
		where convert(date,OrderDate) between  @orderfrom and @todate and om.Status=11 and om.OrderType !=1
		group by  pm.ProductId , pd.UnitType, pd.Qty,  pm.PackingAutoId 
		) as t1  on njpm.ProductId=t1.ProductId and njpd.UnitType = t1.UnitType


		left join (
		select   pm.ProductId,   pm.PackingAutoId, pd.UnitType, pd.Qty,
		sum(cim.TotalPeice) as QtyDel, sum(cim.NetAmount) as NetPrice
		from  [psmct.a1whm.com].dbo.CreditMemoMaster as cmm
		inner join  [psmct.a1whm.com].dbo.CreditItemMaster as cim on cim.CreditAutoId=cmm.CreditAutoId
		inner join  [psmct.a1whm.com].dbo.ProductMaster as pm on pm.AutoId=cim.ProductAutoId
		inner join  [psmct.a1whm.com].dbo.PackingDetails as pd on pd.ProductAutoId=pm.AutoId and pd.UnitType=pm.PackingAutoId		
		where convert(date,cmm.ApprovalDate) between  @orderfrom and @todate and cmm.Status=3
		group by  pm.ProductId,   pd.UnitType, pd.Qty , pm.PackingAutoId
		) as t2 on  njpm.ProductId=t2.ProductId and njpd.UnitType=t2.UnitType
	
		left join [psmnj.a1whm.com].dbo.ProductPricingInPriceLevel as pppl on 
																		pppl.ProductAutoId=njpm.AutoId
																	and pppl.UnitAutoId=njpd.UnitType 
																	and pppl.PriceLevelAutoId=2838
																	--select top 100 *  from ProductPricingInPriceLevel
		) as t
		--order by  ProductId
		----------------------------- 
		---_EASYWHM
		--------------------------------------------------
		UNION ALL

		select *, (Net_Sold_Default_Qty * PSMCT_COST)PSMCT_NET_COST  from
		(
		select 
		njpm.ProductId,njpm.ProductName,njum.UnitType,njpd.Qty
		,(case when isnull(pppl.CustomPrice,0) = 0 then njpd.CostPrice else CustomPrice end ) PSMCT_COST
		,convert(decimal(18,2),((ISNULL(t1.QtyDel,0))/njpd.Qty)) as [Sold_Default_Qty]		
		,convert(decimal(18,2),((isnull(t2.QtyDel,0))/njpd.Qty)) as [CM_Default_Qty]	
		,convert(decimal(18,2),((ISNULL(t1.QtyDel,0)-isnull(t2.QtyDel,0))/njpd.Qty)) as [Net_Sold_Default_Qty]
		,(t1.NetPrice  ) as Net_Sale_Rev		
		,( isnull(t2.NetPrice,0)) as Net_CM_Rev		
		,(t1.NetPrice - isnull(t2.NetPrice,0)) as Net_Total_Sales	
		from 
		[psmnj.a1whm.com].dbo.ProductMaster as njpm
		inner join [psmnj.a1whm.com].dbo.PackingDetails as njpd on njpd.ProductAutoId=njpm.AutoId and njpd.UnitType = njpm.PackingAutoId
		inner join [psmnj.a1whm.com].dbo.UnitMaster as njum on njum.AutoId=njpd.UnitType
		left join
		(
		select  
					pm.ProductId,   pm.PackingAutoId,pd.UnitType, pd.Qty			
				,sum(doi.QtyDel) as QtyDel
				,sum(doi.NetPrice) as NetPrice			
		from  [psmct.easywhm.com].dbo.OrderMaster as om
			inner join [psmct.easywhm.com].dbo.Delivered_Order_Items as doi on doi.OrderAutoId=om.AutoId
			inner join  [psmct.easywhm.com].dbo.ProductMaster as pm on pm.AutoId=doi.ProductAutoId
			inner join  [psmct.easywhm.com].dbo.PackingDetails as pd on pd.ProductAutoId=pm.AutoId and pd.UnitType=pm.PackingAutoId			 
		where convert(date,OrderDate) between  @orderfrom and @todate and om.Status=11 and om.OrderType !=1
		group by  pm.ProductId , pd.UnitType, pd.Qty,  pm.PackingAutoId 
		) as t1  on njpm.ProductId=t1.ProductId and njpd.UnitType = t1.UnitType


		left join (
		select   pm.ProductId,   pm.PackingAutoId, pd.UnitType, pd.Qty,
		sum(cim.TotalPeice) as QtyDel, sum(cim.NetAmount) as NetPrice
		from   [psmct.easywhm.com].dbo.CreditMemoMaster as cmm
		inner join   [psmct.easywhm.com].dbo.CreditItemMaster as cim on cim.CreditAutoId=cmm.CreditAutoId
		inner join  [psmct.easywhm.com].dbo.ProductMaster as pm on pm.AutoId=cim.ProductAutoId
		inner join   [psmct.easywhm.com].dbo.PackingDetails as pd on pd.ProductAutoId=pm.AutoId and pd.UnitType=pm.PackingAutoId		
		where convert(date,cmm.ApprovalDate) between  @orderfrom and @todate and cmm.Status=3
		group by  pm.ProductId,   pd.UnitType, pd.Qty , pm.PackingAutoId
		) as t2 on  njpm.ProductId=t2.ProductId and njpd.UnitType=t2.UnitType
	
		left join [psmnj.a1whm.com].dbo.ProductPricingInPriceLevel as pppl on 
																		pppl.ProductAutoId=njpm.AutoId
																	and pppl.UnitAutoId=njpd.UnitType 
																	and pppl.PriceLevelAutoId=2838
																	--select top 100 *  from ProductPricingInPriceLevel
		) as tx
		order by  ProductId

		--------------------------
		select ROW_NUMBER() over(order by [CategoryName]) as RowNumber, 
		njcm.CategoryName[CategoryName]
		, tr.ProductId
		,tr.ProductName
		,UnitType 
		,PSMCT_COST [PSMCT_UNIT_COST]	 
		,ISNULL(sum(Sold_Default_Qty),0) as Sold_Default_Qty
		,ISNULL(sum(CM_Default_Qty),0) as CM_Default_Qty
		,ISNULL(sum(Net_Sold_Default_Qty),0) as Net_Sold_Default_Qty

		,ISNULL(sum(PSMCT_NET_COST),0) as PSMCT_NET_COST
		,ISNULL(sum(Net_Sale_Rev),0) as Net_Sale_Rev
		,ISNULL(sum(Net_CM_Rev),0) as Net_CM_Rev
		,ISNULL(sum(Net_Total_Sales),0) as Net_Total_Sales 


		,ISNULL(sum((Net_Total_Sales-PSMCT_NET_COST)),0) as [Profit]
		into #tlist from #t1 as tr
		inner join [psmnj.a1whm.com].dbo.ProductMaster as njpm on njpm.ProductId=tr.[ProductId]
		inner join [psmnj.a1whm.com].dbo.CategoryMaster as njcm on njcm.AutoId=njpm.CategoryAutoId
  
		group by  tr.ProductId,tr.ProductName,UnitType,PSMCT_COST ,njcm.CategoryName
		having sum(Net_Sold_Default_Qty) != 0 or sum(CM_Default_Qty)!=0
		order by njcm.CategoryName,ProductId
 
		SELECT case when @PageSize=0 then 0 else COUNT(*) end as RecordCount,@PageSize as PageSize,@PageIndex as PageIndex FROM #tlist  
		SELECT * FROM #tlist  
		WHERE @PageSize=0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1) 
		SELECT ISNULL((SUM([PSMCT_UNIT_COST])),0) as [PSMCT_UNIT_COST] ,ISNULL((SUM(Sold_Default_Qty)),0) as Sold_Default_Qty,
		ISNULL((SUM(CM_Default_Qty)),0) as CM_Default_Qty,ISNULL((SUM(Net_Sold_Default_Qty)),0) as Net_Sold_Default_Qty,ISNULL((SUM(psmct_NET_COST)),0) as PSMCT_NET_COST,
		ISNULL(SUM(Net_Sale_Rev),0) as Net_Sale_Rev,ISNULL(SUM(Net_CM_Rev),0) as Net_CM_Rev,ISNULL(SUM(Net_Total_Sales),0) as Net_Total_Sales,ISNULL(SUM([Profit]),0) as [Profit]
		from #tlist

		drop table #t1,#tlist
     END TRY
	 BEGIN CATCH
	   SET @isException=1
       SET @exceptionMessage=ERROR_MESSAGE()
	 END CATCH
END



