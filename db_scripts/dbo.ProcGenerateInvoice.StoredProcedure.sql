USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcGenerateInvoice]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ProcGenerateInvoice]

@Opcode INT=NULL,
@InvAutoId INT=NULL,
@InvNo VARCHAR(12)=NULL,
@InvDate DATETIME=NULL,
@OrderAutoId INT=NULL,

@TotalAmt DECIMAL(8,2)=NULL,
@OverallDisc DECIMAL(8,2)=NULL,
@OverallDiscAmt DECIMAL(8,2)=NULL,
@Shipping DECIMAL(8,2)=NULL,
@TotalTax DECIMAL(8,2)=NULL,
@GrandTotal DECIMAL(8,2)=NULL,
@EmpAutoId INT=NULL,
@InvStatus INT=NULL,

@ToInvDate DATETIME=NULL,
@FromInvDate DATETIME=NULL,
@PageIndex INT = 1,
@PageSize INT = 10,
@RecordCount INT =null,

@isException BIT OUT,
@exceptionMessage VARCHAR(max) OUT
AS
BEGIN
	BEGIN TRY
		SET @isException=0
		SET @exceptionMessage='Success'

		If @Opcode=11		
			BEGIN
				BEGIN TRY
					BEGIN TRAN

					COMMIT TRANSACTION
				END TRY
				BEGIN CATCH
					ROLLBACK TRAN
					Set @isException=1
					Set @exceptionMessage=ERROR_MESSAGE()
				END CATCH
			END
		ELSE 
			BEGIN
				SET @isException=1
				SET @exceptionMessage=ERROR_MESSAGE()
			END
	END TRY
	BEGIN CATCH
			SET @isException=1
			SET @exceptionMessage=ERROR_MESSAGE()
	END CATCH
END




GO
