
alter PROCEDURE [dbo].[ProcDraftCustomerMaster]                                                                                                  
@Opcode INT=Null,    
@XmlContactPerson xml=null, 
@Terms INT=NULL, 
@ShipAutoId int=null,
@BillAutoId int=null,
@PriceLevelAutoId int=null,
@AutoId int=null,
@EmpAutoId INT=NULL,
@TaxId VARCHAR(50)=NULL, 
@CustomerId VARCHAR(15)=NULL,
@priceLevelId INT=NULL, 
@CustomerAutoId INT=NULL,      
@CustomerName VARCHAR(50)=NULL,  
@referenceCustomerNumber varchar(50)=null,
@CustomerType VARCHAR(100)=NULL,                                                                                                  
@BusinessName VARCHAR(100)=NULL,    
@OPTLicence VARCHAR(100)=NULL,   
@Latitude VARCHAR(max)=NULL,  
@Longitude VARCHAR(max)=NULL,
@Latitude1 VARCHAR(max)=NULL,  
@Longitude1 VARCHAR(max)=NULL,                                                                                                                                                                                         
@BillAddAutoId INT=NULL,                                                                                                  
@ShipAddAutoId INT=NULL,                                                                                                  
@SalesPersonAutoId INT=NULL,                                                                                                  
@CityAutoId int =null,   
@CityAutoId2 int =null,                                                                                  
@stateAutoId int =null,                                                                                                   
@Status INT=NULL,
@GenStatus INT=NULL,
@BillAdd VARCHAR(200)=NULL,                                                              
@BillAdd2 VARCHAR(100)=NULL,                                  
@State1 varchar(100)=NULL,
@BState int=null,
@City1 VARCHAR(50)=NULL,                           
@Zipcode1 VARCHAR(20)=NULL,    
@ZipAutoId INT=NULL,                                                            
@ShipAdd VARCHAR(200)=NULL,  
@ShipAdd2 VARCHAR(100)=NULL, 
@State2 varchar(100)=NULL,  
@SState int=null,
@City2 VARCHAR(50)=NULL,                                      
@Zipcode2 VARCHAR(20)=NULL,   
@ZipAutoId2 INT=NULL,                                                         
@ContactPersonName VARCHAR(100)=NULL,                                                                              
@Zipcode varchar(20)=NULL,                        
@StoreOpenTime time(7)=NULL,
@StoreCloseTime time(7)=NULL,  
@CustomerRemark VARCHAR(1000)=NULL, 
@PageIndex INT=1,                                                                                                  
@PageSize INT=10,                                                                                                  
@RecordCount INT=null,                                                                                                  
@isException BIT OUT,                                                                                                  
@exceptionMessage VARCHAR(max) OUT                                                                      
AS                                                                                                  
BEGIN                                                                                                  
 BEGIN TRY                                                                               
		SET @isException=0                                                                                                  
		SET @exceptionMessage='Success!!'
		Declare @ChildDbLocation varchar(50),@sql nvarchar(max) 
		select @ChildDbLocation=ChildDB_Name from CompanyDetails
		DECLARE @companyId varchar(50)   
 IF @Opcode=11                                                                                                  
  BEGIN                                                                                                            
   BEGIN TRY                                                                                          
   BEGIN TRAN  
	  
IF EXISTS(select * from [DraftCustomerMaster] where CustomerName = TRIM(@CustomerName))                                
begin                               
	SET @isException=1                                                                                            
	SET @exceptionMessage=  'Customer already exist.'                                
end 
Else IF EXISTS(select * from [CustomerMaster] where CustomerName = @CustomerName)                                
begin                               
	SET @isException=1                                                                                            
	SET @exceptionMessage=  'Customer already exist.'                                
end 
ELSE IF NOT EXISTS(SELECT * FROM State WHERE  statename=@State1)                                                          
BEGIN                           
	SET @isException=1                                                                            
	SET @exceptionMessage='State1'                                                                                                  
END                       
ELSE IF NOT EXISTS(SELECT * FROM State WHERE statename=@State2)                                                                           
BEGIN                        
	SET @isException=1                                                                                                  
	SET @exceptionMessage='State2'                                                                                                  
END 
ELSE                                                                                                   
BEGIN 
	Exec ProcinsertData_Cityzipcode_ALL @City=@City1,@State=@State1,@Zipcode1=@Zipcode1
	Exec ProcinsertData_Cityzipcode_ALL @City=@City2,@State=@State2,@Zipcode1=@Zipcode2

	 
	SET @BState =(Select AutoId from State Where StateName= @State1)                                                                                  
	SET @SState =(Select AutoId from State Where StateName= @State2) 
	SET @CityAutoId =(Select AutoId from CityMaster Where CityName= @City1)                                                                                  
	SET @CityAutoId2 =(Select AutoId from CityMaster Where CityName= @City2)  
	SET @ZipAutoId =(Select AutoId from ZipMaster Where Zipcode= @Zipcode1 and CityId=@CityAutoId)                                                                                  
	SET @ZipAutoId2 =(Select AutoId from ZipMaster Where Zipcode= @Zipcode2 and CityId=@CityAutoId2)   

	INSERT INTO [dbo].[DraftCustomerMaster]([CustomerName],[CustomerType],                                              
	[SalesPersonAutoId],[Status],[priceLevelId],[Terms],[TaxId],BusinessName,OPTLicence,                                          
	CreatedBy,CreatedOn,OptimotwFrom,OptimotwTo,CustomerRemarks)                                                                                                   
	VALUES (TRIM(@CustomerName),@CustomerType,@SalesPersonAutoId,@Status,@priceLevelId,                                                                                                  
	@Terms,@TaxId,@BusinessName,@OPTLicence,@EmpAutoId,getdate(),@StoreOpenTime,@StoreCloseTime,@CustomerRemark)
	 
	SET @CustomerAutoId = (SELECT SCOPE_IDENTITY())     

	insert into DraftCustomerContactPerson(ContactPerson,Type,MobileNo,Landline,Landline2,Fax,Email,AlternateEmail,IsDefault,CustomerAutoId)        
    select tr.td.value('ContactPerson[1]','varchar(100)') as ContactPerson,        
    tr.td.value('TypeAutoId[1]','int') as Type,        
    tr.td.value('Mobile[1]','varchar(20)') as MobileNo,  
	tr.td.value('Landline[1]','varchar(20)') as Landline,  
	tr.td.value('Landline2[1]','varchar(20)') as Landline2,  
	tr.td.value('Fax[1]','varchar(20)') as Fax,  
	tr.td.value('Email[1]','varchar(20)') as Email, 
	tr.td.value('AltEmail[1]','varchar(20)') as AlternateEmail, 	
	tr.td.value('IsDefault[1]','int') as IsDefault,
	@CustomerAutoId
    from  @XmlContactPerson.nodes('/contactpersonXML') tr(td) 
	                                                                                  
	INSERT INTO [dbo].[DraftShippingAddress] ([CustomerAutoId],[Address],[State],[Zipcode],[IsDefault],SA_Lat,SA_Long,Address2,City)                                                                                                   
	VALUES(@CustomerAutoId,@ShipAdd,@State2,@Zipcode2,1,@Latitude,@Longitude,@ShipAdd2,@City2)                                                                                                                                                                                              
                                                                   
	INSERT INTO [dbo].[DraftBillingAddress] ([CustomerAutoId],[Address],[State],[Zipcode],[IsDefault],BillSA_Lat,BillSA_Long,Address2,City)                                                                                                   
	VALUES(@CustomerAutoId,@BillAdd,@State1,@Zipcode1,1,@Latitude1,@Longitude1,@BillAdd2,@City1)                                                                                                  
                                                                                                                                                                                                                                                                         
   END                                                                                                       
  COMMIT TRANSACTION                                                                                             
  END TRY                                                    
  BEGIN CATCH                                             
  ROLLBACK TRAN                                                                                                   
	   SET @isException=1                                                                       
	   SET @exceptionMessage='Oops,Something weent wrong.Please try again.'                                                                                                         
  END CATCH                     
  END    
 Else IF @Opcode=12                                                                                                  
  BEGIN                                               
IF NOT EXISTS(SELECT * FROM State WHERE  StateName=trim(@State1))                                                          
  BEGIN                           
   SET @isException=1                                                                            
   SET @exceptionMessage='State1'                                                                                                   
  END 

  ELSE IF NOT EXISTS(SELECT * FROM State WHERE StateName=trim(@State2))                                                                           
  BEGIN                        
   SET @isException=1                                                                                                  
   SET @exceptionMessage='State2'                                                                                                    
  END                    
  else if EXISTS(select * from [CustomerMaster] where CustomerName = trim(@CustomerName))                                
  begin                               
   SET @isException=1                                                                                            
   SET @exceptionMessage=  'Customer already exist.'                                
  end                                         
  ELSE                                                                                                   
  BEGIN              
   BEGIN TRY                                                                                          
   BEGIN TRAN tr1 
   
	Exec ProcinsertData_Cityzipcode_ALL @City=@City1,@State=@State1,@Zipcode1=@Zipcode1
	Exec ProcinsertData_Cityzipcode_ALL @City=@City2,@State=@State2,@Zipcode1=@Zipcode2

   SET @CustomerId=(SELECT [dbo].[SequenceCodeGenerator]('CustomerId'))    
   SET @companyId =(SELECT TOP 1 CompanyId FROM CompanyDetails) 
   SET @BState =(Select AutoId from State Where StateName= @State1)                                                                                  
   SET @SState =(Select AutoId from State Where StateName= @State2) 
   SET @CityAutoId =(Select AutoId from CityMaster Where CityName= @City1 and StateId=@BState)                                                                                  
   SET @CityAutoId2 =(Select AutoId from CityMaster Where CityName= @City2 and StateId=@SState)  
   SET @referenceCustomerNumber =(Select AutoId from DraftCustomerMaster Where AutoId= @CustomerAutoId)  
   SET @ZipAutoId =(Select AutoId from ZipMaster Where Zipcode= @Zipcode1 and CityId=@CityAutoId)                                                                                  
   SET @ZipAutoId2 =(Select AutoId from ZipMaster Where Zipcode= @Zipcode2 and CityId=@CityAutoId2) 

	INSERT INTO [dbo].[CustomerMaster]([CustomerId],[CustomerName],[CustomerType],[SalesPersonAutoId],[Status],[Terms]
	,[TaxId],BusinessName,OPTLicence,CreatedBy,CreatedOn,UpdateOn,UpdatedBy,OptimotwFrom,OptimotwTo,referenceCustomerNumber,CustomerRemarks)  
	values(@CustomerId,@CustomerName,@CustomerType,@SalesPersonAutoId,@Status,@Terms,@TaxId,@BusinessName,@OPTLicence
	,@EmpAutoId,GETDATE(),GETDATE(),@EmpAutoId,@StoreOpenTime,@StoreCloseTime,@referenceCustomerNumber,@CustomerRemark)
	 
	SET @AutoId = (SELECT SCOPE_IDENTITY())     

	insert into CustomerContactPerson(ContactPerson,Type,MobileNo,Landline,Landline2,Fax,Email,
	AlternateEmail,IsDefault,CustomerAutoId)        
	select tr.td.value('ContactPerson[1]','varchar(100)') as ContactPerson,        
		tr.td.value('TypeAutoId[1]','int') as Type,        
		tr.td.value('Mobile[1]','varchar(20)') as MobileNo,  
		tr.td.value('Landline[1]','varchar(20)') as Landline,  
		tr.td.value('Landline2[1]','varchar(20)') as Landline2,  
		tr.td.value('Fax[1]','varchar(20)') as Fax,  
		tr.td.value('Email[1]','varchar(20)') as Email, 
		tr.td.value('AltEmail[1]','varchar(20)') as AlternateEmail, 	
		tr.td.value('IsDefault[1]','int') as IsDefault,
		@AutoId
		from  @XmlContactPerson.nodes('/contactpersonXML') tr(td) 
	                                                                                  
	INSERT INTO [dbo].[ShippingAddress] ([CustomerAutoId],[Address],Address2,[State],[SCityAutoId],[Zipcode],[IsDefault],ZipcodeAutoid,SA_Lat,SA_Long)                                                                                                   
	values(@AutoId,@ShipAdd,@ShipAdd2,@SState,@CityAutoId2,@Zipcode2,1,@ZipAutoId2,@Latitude,@Longitude)
	SET @ShipAutoId = (SELECT SCOPE_IDENTITY())                                                                                             
                                                                   
	INSERT INTO [dbo].[BillingAddress] ([CustomerAutoId],[Address],Address2,[State],[BCityAutoId],[Zipcode],[IsDefault],ZipcodeAutoid,BillSA_Lat,BillSA_Long)                                                                                                   
	values(@AutoId,@BillAdd,@BillAdd2,@BState,@CityAutoId,@Zipcode1,1,@ZipAutoId,@Latitude1,@Longitude1)                                                                                              
	
	SET @BillAutoId = (SELECT SCOPE_IDENTITY())                                                                                                    
	SET @CustomerType =(select CustomerType from [CustomerMaster] where AutoId=@AutoId)                                                                     
	IF(@priceLevelId = -1)                                         
	BEGIN                                                                                                  
		exec [ProcPriceLevelMaster] @opcode=11,@PriceLevelName=@CustomerName,@CustomerType=@CustomerType,@Status=1,@EmpAutoId=@EmpAutoId,  
		@isException=null,@exceptionMessage=null    
		set @priceLevelId=(select MAX(autoid) from [PriceLevelMaster] where PriceLevelName=@CustomerName)                                                                                       
	END                                                                                                  
                             
  IF(@priceLevelId IS NOT NULL)                                                                            
  BEGIN                                                                 
   INSERT INTO [dbo].[CustomerPriceLevel]([CustomerAutoId],[PriceLevelAutoId],[AssignedOn],[AssignedBy]) VALUES(@AutoId,@priceLevelId,getdate(),@EmpAutoId)                                                                             
  END                                                                                                  
   UPDATE [dbo].[CustomerMaster] SET [DefaultBillAdd]=@BillAutoId, [DefaultShipAdd]=@ShipAutoId WHERE [AutoId] = @AutoId                                                                                                  
                                                                            
   UPDATE [dbo].[SequenceCodeGeneratorMaster] SET [currentSequence]=[currentSequence]+1 WHERE [SequenceCode]='CustomerId' 

   delete from DraftBillingAddress where CustomerAutoId=@CustomerAutoId
   delete from DraftShippingAddress where CustomerAutoId=@CustomerAutoId
   delete from DraftCustomerContactPerson where CustomerAutoId=@CustomerAutoId
   delete from DraftCustomerMaster where AutoId=@CustomerAutoId
   COMMIT TRANSACTION tr1                                                                                            
  END TRY                                                    
  BEGIN CATCH                                             
  ROLLBACK TRAN   tr1                                                                                                
   SET @isException=1                                                                       
   SET @exceptionMessage='Oops,Something weent wrong.Please try again.'                                                                                                        
   END CATCH   
    exec ProcInsertData_CustomerMaster @CustomerAutoId=@AutoId,@isException=null,@exceptionMessage=null,@Opcode=11

  END                                                                                                       
                    
  END                  
  ELSE IF @Opcode=21                                                                                                  
  BEGIN        
   BEGIN TRY                                                                                                  
    BEGIN TRAN   
	IF NOT EXISTS(SELECT * FROM State WHERE  StateName=trim(@State1))                                                          
	BEGIN                           
		SET @isException=1                                                                            
		SET @exceptionMessage='State1'                                                                                                   
	END 
	ELSE IF NOT EXISTS(SELECT * FROM State WHERE StateName=trim(@State2))                                                                          
	BEGIN                        
		SET @isException=1                                                                                                  
		SET @exceptionMessage='State2'                                                                                                    
	END                    
	else if EXISTS(select * from [CustomerMaster] where CustomerName = trim(@CustomerName))                                
	begin                               
		SET @isException=1                                                                                            
		SET @exceptionMessage=  'Customer details already exist.'                                
	end                                                                                
	ELSE     
	    Exec ProcinsertData_Cityzipcode_ALL @City=@City1,@State=@State1,@Zipcode1=@Zipcode1
	    Exec ProcinsertData_Cityzipcode_ALL @City=@City2,@State=@State2,@Zipcode1=@Zipcode2

		BEGIN                                        
		UPDATE [dbo].[DraftCustomerMaster] SET                                                               
		[CustomerName]=TRIM(@CustomerName),                                                                                                  
		[CustomerType]=@CustomerType,                                                                                                                                                                        
		[Status]=@Status,                                                                                
		[SalesPersonAutoId]=@SalesPersonAutoId,                                                                                                  
		[Terms]=@Terms,                                                                                                                                                    
		[BusinessName]=@BusinessName,
		[priceLevelId]=@priceLevelId,
		[TaxId]=@TaxId,     
		CreatedBy=@EmpAutoId,
		CreatedOn=GETDATE(),
		OPTLicence=@OPTLicence,  
		OptimotwFrom=@StoreOpenTime,
		OptimotwTo=@StoreCloseTime,
		CustomerRemarks=@CustomerRemark
		WHERE AutoId=@CustomerAutoId                                          
		delete from DraftCustomerContactPerson where CustomerAutoId=@CustomerAutoId
		insert into DraftCustomerContactPerson(ContactPerson,Type,MobileNo,Landline,Landline2,Fax,Email,
		AlternateEmail,IsDefault,CustomerAutoId)        
		select tr.td.value('ContactPerson[1]','varchar(100)') as ContactPerson,        
		tr.td.value('TypeAutoId[1]','int') as Type,        
		tr.td.value('Mobile[1]','varchar(20)') as MobileNo,  
		tr.td.value('Landline[1]','varchar(20)') as Landline,  
		tr.td.value('Landline2[1]','varchar(20)') as Landline2,  
		tr.td.value('Fax[1]','varchar(20)') as Fax,  
		tr.td.value('Email[1]','varchar(20)') as Email, 
		tr.td.value('AltEmail[1]','varchar(20)') as AlternateEmail, 	
		tr.td.value('IsDefault[1]','int') as IsDefault,
		@CustomerAutoId
		from  @XmlContactPerson.nodes('/contactpersonXML') tr(td) 

		UPDATE [dbo].[DraftBillingAddress] SET [Address] = @BillAdd,[Address2] = @BillAdd2,[State] = @State1,City=@City1,[Zipcode]=@Zipcode1,BillSA_Lat=@Latitude1,BillSA_Long=@Longitude1 WHERE [AutoId]=@BillAddAutoId                                                                              
                    
		UPDATE [dbo].[DraftShippingAddress] SET [Address]=@ShipAdd,[Address2]=@ShipAdd2,[State]=@State2,City=@City2,[Zipcode]=@Zipcode2,SA_Lat=@Latitude,SA_Long=@Longitude   WHERE [AutoId]=@ShipAddAutoId                                                                                 
		END
	COMMIT TRANSACTION                                                                        
	END TRY                                                                                                  
	BEGIN CATCH                                                                                                    
	ROLLBACK TRAN                                                                                    
	SET @isException=1                                                                                                  
	SET @exceptionMessage='Oops,Something weent wrong.Please try again.'                                                                                                         
	END CATCH 
  END                                                                                                  
  ELSE IF @Opcode = 31                                                                                                  
   BEGIN   
    DELETE FROM DraftBillingAddress WHERE CustomerAutoId=@CustomerAutoId      
	DELETE FROM DraftShippingAddress WHERE CustomerAutoId=@CustomerAutoId      
	DELETE FROM DraftCustomerContactPerson WHERE CustomerAutoId=@CustomerAutoId      
    DELETE FROM [dbo].[DraftCustomerMaster] WHERE AutoId=@CustomerAutoId                                                                                                   
   END               
  ELSE IF @Opcode = 42                                                                                                  
   BEGIN                                                                              
	select(
		select DCM.AutoId as CustomerAutoId,CustomerName,CustomerType,SalesPersonAutoId,Status,Terms,TaxId,
		--isnull((select AutoId from PriceLevelMaster where AutoId=DCM.priceLevelId and PCustomerType=CustomerType),0) as priceLevelId,
		priceLevelId,BusinessName,
		OPTLicence,OptimotwFrom,OptimotwTo
		,DBA.AutoId as BillingAutoId,DBA.Address as BAddress,DBA.Address2 as BAddress2,
		DBA.state as BStateName,DBA.State as BState,DBA.City as BCity,
		
		DBA.Zipcode as BZipcode, DBA.BillSA_Lat,DBA.BillSA_Long

		,DSA.AutoId as ShippingAutoId,DSA.Address as SAddress,DSA.Address2 as SAddress2,
		DSA.state as SStateName, DSA.State as SState,DSA.City as SCity,
	
		DSA.Zipcode as SZipcode, DSA.SA_Lat,DSA.SA_Long,DCM.CustomerRemarks
		from  DraftCustomerMaster as DCM
		INNER JOIN DraftBillingAddress DBA on DBA.CustomerAutoId=DCM.AutoId
		INNER JOIN DraftShippingAddress DSA on DSA.CustomerAutoId=DCM.AutoId
		WHERE DCM.AutoId = @CustomerAutoId  
	for json path, INCLUDE_NULL_VALUES) as CustomerDetails                                          
   END  
  ELSE IF @Opcode = 43                                                                                                 
   BEGIN   
	select(
		select
		(SELECT [AutoId], [FirstName] + ' ' +[LastName] AS Name FROM [dbo].[EmployeeMaster] WHERE [EmpType]=2 AND [Status] = 1 order by Name for json path, INCLUDE_NULL_VALUES) as Employee,
		(SELECT [AutoId], [StatusType] FROM [dbo].[StatusMaster] WHERE [Category] is null for json path, INCLUDE_NULL_VALUES) as Status,
		(SELECT [TermsId],[TermsDesc] FROM [dbo].[CustomerTerms]  ORDER BY [TermsDesc] ASC for json path, INCLUDE_NULL_VALUES) as Terms,
		(SELECT [AutoId],[CustomerType] FROM [dbo].[CustomerType] where AutoId not in (3) ORDER BY [CustomerType] ASC for json path, INCLUDE_NULL_VALUES) as CustomerType,
		(SELECT CompanyId FROM CompanyDetails for json path, INCLUDE_NULL_VALUES) as CompanyDetails,
		(select Zipcode as ZM,CityId,zm.AutoId,Zipcode+' [ '+CM.CityName+' ]' as ZipCode from ZipMaster as ZM  
		INNER JOIN CityMaster CM ON ZM.CityId=CM.AutoId  WHERE ZM.Status=1  ORDER BY REPLACE(Zipcode+' [ '+CM.CityName+' ]',' ','') ASC for json path, INCLUDE_NULL_VALUES) as ZipCode1,
		(select AutoId,CONVERT(VARCHAR, TimeFrom, 100) as TimeFromLabel,CONVERT(VARCHAR, TimeTo, 100) as TimeToLabel,CONVERT(VARCHAR(8),TimeFrom,108)   as TimeFrom ,CONVERT(VARCHAR(8),TimeTo,108) as TimeTo from OptimoTimeWindow order by AutoId asc for json path) as StoreTime
		for json path, INCLUDE_NULL_VALUES
	)as DropDownList
  END
     ELSE IF @Opcode=44                                                                                          
   BEGIN         
		Select zm.AutoId,zm.Zipcode,cm.CityName,s.StateName from ZipMaster zm
		inner join CityMaster cm on cm.AutoId=zm.CityId
		inner join State s on s.AutoId=cm.StateId
		where zm.Zipcode=@Zipcode    
   END  
  ELSE IF @Opcode=45                                                                                          
   BEGIN         
		Select * from contactpersontypemaster
   END  
   ELSE IF @Opcode=46                                                                                          
   BEGIN         
		Select DCP.AutoId,ContactPerson,CPT.TypeName,Type,MobileNo,Landline,Landline2,Fax,Email,AlternateEmail,ISDefault 
		from DraftCustomerContactPerson as DCP
		inner join contactpersontypemaster CPT on CPT.AutoId=DCP.Type
		where DCP.CustomerAutoId=@CustomerAutoId
   END 
   else if @Opcode = 47                                
   begin           
   select(select [AutoId], [PriceLevelName] as PL FROM [dbo].[PriceLevelMaster] WHERE [Status] = 1 and PCustomerType = @CustomerType                                
   Order by PriceLevelName for json path, INCLUDE_NULL_VALUES) as PriceLevel                            
   end    
  END TRY                                                                                                  
  BEGIN CATCH                                                                                                  
   SET @isException=1                                                           
   SET @exceptionMessage='Oops,Something weent wrong.Please try again.'                                            
  END CATCH                                                                                                                                                                    
END 
GO
