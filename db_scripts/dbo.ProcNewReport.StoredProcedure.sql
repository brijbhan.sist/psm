USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcNewReport]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProcNewReport]              
AS                  
BEGIN          
  
select (em.FirstName+' '+em.LastName) as [Sales Person],pm.ProductId,ProductName,um.UnitType  ,  
COUNT(*) AS NoofProduct ,isNull(pm.P_CommCode,0) as CommCode,     
ISNULL(sum(doim.NetPrice),0) as TotalSale,     
CAST((sum(doim.NetPrice)*isNull (pm.P_CommCode,0)) AS DECIMAL(10,3)) as SPCommAmt     
from PackingDetails as pd    
right join ProductMaster as pm on pm.AutoId = pd.ProductAutoId and UnitType=3   
inner join Delivered_Order_Items as doim on pm.AutoId=doim.ProductAutoId     
inner join UnitMaster as um on um.AutoId=doim.UnitAutoId   
inner join OrderMaster as om on om.AutoId=doim.OrderAutoId    
inner join CustomerMaster as cm on cm.AutoId = om.CustomerAutoId    
inner join EmployeeMaster as em on em.AutoId=cm.SalesPersonAutoId     
where convert(date,OrderDate) between convert(date,'03/01/2019')     
and convert(date,'03/31/2019')     
group by isNull(pm.P_CommCode,0), em.FirstName+' '+em.LastName,cm.SalesPersonAutoId ,pm.ProductId,ProductName , um.UnitType  
ORDER BY  [Sales Person],CommCode  
                
END 

----------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------
GO
