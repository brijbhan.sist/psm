--EXEC DeleteOrder @OrderAutoId=null, @OrderDate=null, @OpCode=11,@OrderNo ='ORD106911',@DeleteBy=NULL, @DeleteRemark =NULL
USE [psmnj.a1whm.com]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[DeleteOrder] 
@OrderAutoId  int=null,
@OrderDate datetime=null,
@OpCode int=11,
@OrderNo varchar (25),
@DeleteBy varchar(20),  
@DeleteRemark varchar(250)
AS
BEGIN
  
	set @OrderAutoId=(select autoid from OrderMaster where OrderNo=@OrderNo)
	if exists ( select * from PaymentOrderDetails where OrderAutoId = @OrderAutoId)
	begin
		select 'Remove Payment'
	end

  else

	begin
	begin try
	begin tran
		set @OrderNo =(select OrderNo from OrderMaster where AutoId=@OrderAutoId)
		set @OrderDate=(select OrderDate from OrderMaster where AutoId=@OrderAutoId)

		DELETE from OrderItems_Original where OrderAutoId=@OrderAutoId
		DELETE from Order_Original where AutoId=@OrderAutoId
		DELETE from OrderItemMaster where OrderAutoId=@OrderAutoId
		DELETE from tbl_OrderLog where OrderAutoId=@OrderAutoId
		DELETE from GenPacking where OrderAutoId=@OrderAutoId
		DELETE from DrvLog where OrderAutoId=@OrderAutoId
		DELETE from Delivered_Order_Items where OrderAutoId=@OrderAutoId
		DELETE from DeliveredOrders where OrderAutoId=@OrderAutoId
		DELETE from OrderMaster where AutoId=@OrderAutoId

	   insert into [dbo].[DeleteOrderDetails_Backup] ([OrderNo],[OrderDate],[DeletedBy],[DeleteDate],[DeleteRemark]) 
	   values (@OrderNo,@OrderDate,@DeleteBy,GETDATE(),@DeleteRemark)
	
   commit tran
 end try
begin catch
    rollback tran
	select ERROR_MESSAGE() as Error
end catch
end

END






