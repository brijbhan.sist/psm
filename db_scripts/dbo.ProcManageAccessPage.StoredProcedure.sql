

ALTER PROCEDURE [dbo].[ProcManageAccessPage]     
@OpCode int= Null,  
@AutoId int = Null,  
@ModuleAutoId int=null,  
@SubModuleAutoId int=null,  
@UserRole int=null,  
@EmpAutoId int= null,  
@AssignedAction varchar(200)=null,  
@PageAutoId int=null,  
@Status int=null,  
@PageIndex int=null,         
@PageSize  int=10,  
@RecordCount int=null, 
@isException bit out,    
@exceptionMessage varchar(max) out    
AS    
BEGIN    
 BEGIN TRY    
  SET @isException = 0    
  SET @exceptionMessage = 'success'    
  
		IF @OpCode = 11  
		BEGIN     
			BEGIN TRY 
			BEGIN      
			IF exists(SELECT * FROM ManagePageAccess WHERE ModuleAutoId = @ModuleAutoId AND Role=@UserRole)      
			BEGIN      
			set @isException=1      
			set @exceptionMessage='Page already assigned.'      
			END      
			ELSE   
			BEGIN       
			INSERT INTO ManagePageAccess(ModuleAutoId,AssignedAction,Role,Status,CreatedBy,CreatedOn,
			UpdatedBy,UpdatedOn)VALUES(@ModuleAutoId,@AssignedAction,@UserRole,@Status,@EmpAutoId,
			GETDATE(),@EmpAutoId,GETDATE())  
			END
			END
			END TRY    
			BEGIN CATCH    
			SET @exceptionMessage = 'Oops! Something went wrong.Please try later.'    
			SET @isException = 1    
			END CATCH    
		END 

		ELSE IF @OpCode = 21  
		BEGIN     
			BEGIN TRY    
			UPDATE ManagePageAccess SET AssignedAction=@AssignedAction,UpdatedBy=@EmpAutoId,
			UpdatedOn=GETDATE() WHERE ModuleAutoId=@ModuleAutoId AND Role=@UserRole  
			END TRY    
			BEGIN CATCH    
			SET @exceptionMessage = 'Oops! Something went wrong.Please try later.'    
			SET @isException = 1    
			END CATCH    
		END

		ELSE IF @OpCode = 31   
		BEGIN     
			BEGIN TRY    
			DELETE FROM ManagePageAccess WHERE AutoId=@AutoId 
			END TRY    
			BEGIN CATCH    
			SET @exceptionMessage = 'Oops! Something went wrong.Please try later.'    
			SET @isException = 1    
			END CATCH    
		END  
 
		ELSE IF @OpCode = 41   
		BEGIN     
			BEGIN TRY    
			SELECT ROW_NUMBER() OVER(ORDER BY map.AutoId) AS RowNumber ,map.AssignedAction,m.ModuleName,pm.PageName,pm.PageUrl,etm.TypeName,map.AutoId,map.Status,map.ModuleAutoId,
			map.Role INTO #RESULT41 FROM ManagePageAccess map 
			INNER JOIN Module m ON m.AutoId=map.ModuleAutoId
			INNER JOIN PageMaster pm ON pm.ParentModuleAutoId=map.ModuleAutoId
			INNER JOIN EmployeeTypeMaster etm ON etm.AutoId=map.Role WHERE
			( ISNULL(@ModuleAutoId,'')='' OR map.ModuleAutoId=@ModuleAutoId) AND
			( ISNULL(@UserRole,'')='' OR map.Role=@UserRole)

			SELECT COUNT(ModuleAutoId) AS RecordCount, @PageSize AS PageSize, @PageIndex AS PageIndex FROM #RESULT41        
        
			SELECT * FROM #RESULT41        
			WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1 

			END TRY    
			BEGIN CATCH    
			SET @exceptionMessage = 'Oops! Something went wrong.Please try later.'    
			SET @isException = 1    
			END CATCH    
		END
    
		ELSE IF @OpCode = 42   
			BEGIN     
			BEGIN TRY    
			SELECT * FROM EmployeeTypeMaster ORDER BY TypeName ASC  
			END TRY    
			BEGIN CATCH    
			SET @exceptionMessage = 'Oops! Something went wrong.Please try later.'    
			SET @isException = 1    
			END CATCH    
		END    
  
		--ELSE IF @OpCode = 43  
		--	BEGIN     
		--	BEGIN TRY    
		--	SELECT * FROM  [dbo].[PageMaster] WHERE Role=@UserRole AND ModuleAutoId=@ModuleAutoId ORDER BY AutoId ASC  
		--	END TRY    
		--	BEGIN CATCH    
		--	SET @exceptionMessage = 'Oops! Something went wrong.Please try later.'    
		--	SET @isException = 1    
		--	END CATCH    
		--END
		--ELSE IF @OpCode = 46  
		--	BEGIN     
		--	BEGIN TRY    
		--	SELECT * FROM  [dbo].[PageMaster] WHERE  ModuleAutoId=@ModuleAutoId ORDER BY AutoId ASC  
		--	END TRY    
		--	BEGIN CATCH    
		--	SET @exceptionMessage = 'Oops! Something went wrong.Please try later.'    
		--	SET @isException = 1    
		--	END CATCH    
		--END 
		ELSE IF @OpCode = 47  
			BEGIN     
			BEGIN TRY    
		
			SELECT PA.AutoId,PA.ActionName,PM.PageName,PM.AutoId AS PageAutoId FROM PageAction PA
			INNER JOIN PageMaster PM ON PM.AutoId = PA.PageAutoId
			INNER JOIN Module M ON M.AutoId = PM.ParentModuleAutoId
			WHERE PA.PageAutoId=(SELECT AutoId FROM PageMaster
			WHERE ParentModuleAutoId=(select ModuleAutoId FROM ManagePageAccess WHERE AutoId=@AutoId)) AND PM.Role=@UserRole

			SELECT * FROM  [dbo].[ManagePageAccess] WHERE  AutoId=@AutoId ORDER BY AutoId ASC  
			END TRY    
			BEGIN CATCH    
			SET @exceptionMessage = 'Oops! Something went wrong.Please try later.'    
			SET @isException = 1    
			END CATCH    
		END 
  
		ELSE IF @OpCode = 44
		BEGIN     
			BEGIN TRY    
			SELECT m.AutoId,m.ModuleId,m.ModuleName,(select mmm.ModuleName from Module mmm WHERE mmm.AutoId=m.ParentId) as ParenntModule FROM [dbo].[Module] m 
			WHERE Status=1 AND (select count(md.AutoId) from Module md WHERE md.ParentId=m.AutoId) = 0 ORDER BY ModuleName ASC  
			END TRY    
			BEGIN CATCH    
			SET @exceptionMessage = 'Oops! Something went wrong.Please try later.'    
			SET @isException = 1    
			END CATCH    
		END   
  
		ELSE IF @OpCode = 45  
		BEGIN     
			BEGIN TRY   
			SET @PageAutoId=(SELECT AutoId FROM PageMaster PMS WHERE PMS.ParentModuleAutoId=@ModuleAutoId)

			SELECT PA.AutoId,PA.ActionName,PM.PageName,PM.AutoId AS PageAutoId FROM PageAction PA
			INNER JOIN PageMaster PM ON PM.AutoId = PA.PageAutoId
			WHERE PA.PageAutoId=@PageAutoId AND (ActionType=1 or (UserType=@UserRole AND ActionType=0))

			select AssignedAction FROM ManagePageAccess WHERE ModuleAutoId=@ModuleAutoId AND Role=@UserRole
			END TRY    
			BEGIN CATCH    
			SET @exceptionMessage = 'Oops! Something went wrong.Please try later.'    
			SET @isException = 1    
			END CATCH    
		END   
        
	END TRY    
			BEGIN CATCH    
			SET @isException=1            
			SET @exceptionMessage='Oops! Something went wrong.Please try later.'    
			END CATCH    
END    
    
    
GO
