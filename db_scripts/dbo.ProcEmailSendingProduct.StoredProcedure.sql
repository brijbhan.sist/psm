CREATE PROCEDURE [dbo].[ProcEmailSendingProduct]  
@ProductId VARCHAR(12)=NULL,
@EmpAutoId int,
@UnitType int,
@Barcode varchar(50)=null,
@PackingAutoId int =null
AS                                                                                                                                     
BEGIN   
Declare @ProductAId int,@TotalUnit int,@EmpName varchar(200)='',@LocationName varchar(100),@UnitName varchar(50),@ProperLocation varchar(100)--,@DefUnit varchar(25)                   
                          
   SELECT @ProductAId=AutoId from ProductMaster Where ProductId=@ProductId                                                                                                        
   SELECT  @EmpName=ISNULL(FirstName,'')+' '+ISNULL(LastName,'') FROM EmployeeMaster Where AutoId=@EmpAutoId                                                             
   Select @UnitName=ISNULL(UnitType,'') from UnitMaster Where AutoId=@UnitType                                                                                                    
   SELECT @LocationName=DB_NAME()                                                                  
   SELECT @ProperLocation='A1WHM - '+ UPPER(Replace(@LocationName,'.a1whm.com',''))                                                                                                 
   SELECT @TotalUnit=COUNT(*) from UnitMaster AS UM                                                                                                        
   INNER JOIN PackingDetails AS PD On                                                                                                        
   UM.AutoId=PD.UnitType where PD.ProductAutoId=@ProductAId                  
                                                                                       
   Select PM.AutoId as ProductId,PM.ProductId as Prdtid,PM.ProductName,@LocationName+PM.ImageUrl as ImageUrl,ISNULL(P_SRP,0) as SRP, --Updated on 11/24/2019 02:09 AM                                                                                    
   CM.CategoryName,SM.SubcategoryName,VM.VendorName,BM.BrandName,PM.ReOrderMark,MLQty,WeightOz,case when ProductStatus=1 then 'Active' else 'In Active' end as ProductStatus,Stock,--@DefUnit as DefaultPack,                                                                                    
   ISNUll(P_CommCode,0) as P_CommCode,case when IsApply_Oz=0 then 'No' else 'Yes' end as IsApply_Oz,case when IsApply_ML=0 then 'No' else 'Yes' end as IsApply_ML,                                                                                            
   GETDATE() as CreateDate,@ProperLocation as Location,@TotalUnit as TotalUnit, GETDATE() as UpdateDate,GETDATE() as DeleteDate,ISNULL(@EmpName,'')                                                                                                 
   as EmpName ,GETDATE() as DeleteDate,ISNULL(@Barcode,'') as Barcode,ISNULL(@UnitName,'') as UnitName from ProductMaster AS PM                                                             
   INNER JOIN CategoryMaster AS CM on PM.CategoryAutoId=CM.AutoId                                                                                    
   INNER JOIN SubCategoryMaster AS SM on PM.SubcategoryAutoId=SM.AutoId                                                                                    
   LEFT JOIN VendorMaster AS VM on PM.VendorAutoId=VM.AutoId                                                                                    
   LEFT JOIN BrandMaster AS BM on PM.BrandAutoId=BM.AutoId                                                                                    
   Where PM.AutoId=@ProductAId   
   
   Select distinct UnitAutoId from ItemBarcode as IBC Where IBC.ProductAutoId=@ProductAId                                        
                                                          
	IF (@PackingAutoId is not null and @PackingAutoId!=0)                             
	BEGIN                                                                                                      
		Select um.UnitType,CostPrice,WHminPrice,Qty,Price,IBC.Barcode as Barcode,MinPrice as RetailMinPrice,PD.ProductAutoId,um.AutoId AS UnitId,                                                             
		case when EligibleforFree=0 then 'No' else 'Yes' end as EligibleforFree,pd.Location                                                                 
		from PackingDetails as PD                                                                                                                 
		Inner Join UnitMaster as um on um.AutoId=PD.UnitType                                                                                               
		left Join  ItemBarcode as IBC on  PD.ProductAutoId=IBC.ProductAutoId and ibc.UnitAutoId=PD.UnitType                                                                                                              
		Where PD.AutoId=@PackingAutoId                                                                                                       
	END                                                                      
	IF (@PackingAutoId='0')                                                                                                      
	BEGIN                                                                                                      
		Select um.UnitType,CostPrice,WHminPrice,Qty,Price,MinPrice as RetailMinPrice,PD.ProductAutoId,um.AutoId AS UnitId,pd.productAutoId,                  
		case when PM.PackingAutoId=1 then 'Case' when PM.PackingAutoId=2 then 'Box' when PM.PackingAutoId=3 then 'Peice' when PM.PackingAutoId=0 then '' end as DefPacking,                   
		case when EligibleforFree=0 then 'No' else 'Yes' end as EligibleforFree,pd.Location      --Add By Rizwan Ahmad on 11/26/2019                                                                                                                       
		from PackingDetails as PD                                                                                                                 
		Inner Join UnitMaster as um on um.AutoId=PD.UnitType                   
		Left JOIN ProductMaster AS PM ON                  
		PM.AutoId=PD.ProductAutoId AND PM.PackingAutoId=um.AutoId                                                                                                 
		Where PD.ProductAutoId=@ProductAId                                                                                              
	END                                                   
	Select EmailID as Email from EmailReceiverMaster Where Category='Developer'  
END 