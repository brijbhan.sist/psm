ALTER ProcEDURE [dbo].[ProcAppCreditMemo_New]                        
@Opcode INT=NULL,                          
@timeStamp datetime =null,                    
@referenceCreditMemoNumber varchar(50)=null,                       
@CreditMemodate datetime = null,                      
@CustomerAutoId INT=NULL,                        
@TotalAmount DECIMAL(8,2)=NULL,                        
@OverallDisc DECIMAL(8,2)=NULL,                        
@OverallDiscAmt DECIMAL(8,2)=NULL,                       
@ShippingType INT=NULL,                         
@ShippingCharges DECIMAL(8,2)=NULL,                       
@TaxType int=NULL,         
@TotalTax DECIMAL(8,2)=NULL,                        
@GrandTotal DECIMAL(8,2)=NULL,                       
@Remarks VARCHAR(max)=NULL,                        
@DeliveryDate DATETIME=NULL,                       
@BillAddrAutoId INT=NULL,                        
@ShipAddrAutoId INT=NULL,                        
@SalesPersonAutoId INT=NULL,          
@CreditMemoItems [DT_APP_CreditMemoItemList] readonly,                        
@isException bit out,                        
@exceptionMessage varchar(max) out                        
AS                        
BEGIN                         
BEGIN TRY                        
  Set @isException=0                        
  Set @exceptionMessage='Success'                        
  declare @MLQty decimal(10,2) ,@MLTax decimal(10,2),@StateAutoId int ,@MLType INT  =0    
  declare @WeightQty decimal(10,2) ,@WeightTax decimal(10,2)=ISNULL((select Value from Tax_Weigth_OZ),0)    
              
 If @Opcode=101                          
 BEGIN                        
  BEGIN TRY                        
   BEGIN TRAN                           
   IF isnull(@referenceCreditMemoNumber,'')=''                      
   Begin                     
    Set @isException=1                        
    Set @exceptionMessage='referenceCreditMemoNumber is mandetory'                  
   End                      
   Else IF exists(select * from CreditMemoMaster where referenceCreditMemoNumber=@referenceCreditMemoNumber)                     
   Begin                     
    Set @isException=0                       
    Set @exceptionMessage='CreditMemo already exists'                  
    select CreditAutoId as CreditMemoAutoId,CreditNo as CreditMemoNo,        
    (select statustype from [dbo].[StatusMaster] where [AutoId]=Status and [Category] = 'CreditMaster') status             
    from CreditMemoMaster where referenceCreditMemoNumber=@referenceCreditMemoNumber              
   End                      
   Else                      
   Begin                      
    Declare @CreditNo varchar(20) = (SELECT DBO.SequenceCodeGenerator('CreditNo'))        
    declare @IsApply int =0, @MLtaxper DECIMAL(10,2)=0.00        
    declare @STATE int =(select top 1 State from BillingAddress where AutoId=@BillAddrAutoId)        
   IF EXISTS(SELECT * FROM MLTaxMaster WHERE TaxState=@state)        
   BEGIN        
    SET @IsApply=1        
    SET @MLtaxper=(SELECT TaxRate FROM MLTaxMaster WHERE TaxState=@state)        
    END        
    INSERT INTO CreditMemoMaster(CreditNo,CustomerAutoId,CreatedBy,CreditDate,Status,Remarks,creditType,        
    OverallDiscAmt,TotalTax,SalesAmount,PaidAmount,IsMLTaxApply,MLTAXPER,referenceCreditMemoNumber,TaxTypeAutoId,TaxValue,WeightTax        
    )        
    VALUES(@CreditNo,@CustomerAutoId,@SalesPersonAutoId,@CreditMemodate,1,@Remarks,3,        
    @OverallDiscAmt,@TotalTax,@GrandTotal,0,@IsApply,@MLtaxper,@referenceCreditMemoNumber,@TaxType,    
    Isnull((select Value from TaxTypeMaster where AutoId=@TaxType),0),@WeightTax)        
        
    dECLARE @CreditAutoId INT=SCOPE_IDENTITY()        
        
    INSERT INTO CreditItemMaster(CreditAutoId,ProductAutoId,UnitAutoId,RequiredQty,AcceptedQty,QtyPerUnit,        
    UnitPrice,ManagerUnitPrice,TaxRate,UnitMLQty,Weight_OZ,CostPrice)        
    SELECT @CreditAutoId, ProductAutoId, (select UnitType from PackingDetails as pd where pd.AutoId=temp.UnitAutoId)UnitAutoId,    
    RequiredQty,RequiredQty,QtyPerUnit,UnitPrice,        
    UnitPrice,Tax,ISNULL(MLQty,0),ISNULL(pm.WeightOz,0),
	(select CostPrice from PackingDetails as pd where pd.AutoId=temp.UnitAutoId) FROM @CreditMemoItems as temp         
    inner join ProductMaster as pm on pm.AutoId=temp.ProductAutoId        
              
    SET @Remarks = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=8), '[MemoNo]', @CreditNo)        
    INSERT INTO CreditMemoLog(CreditAutoId,ActionAutoId,EmpAutoId,Remarks,LogDate)        
    VALUES(@CreditAutoId,8,@SalesPersonAutoId, @Remarks,GETDATE())         UPDATE [dbo].[SequenceCodeGeneratorMaster] SET currentSequence=currentSequence+1 WHERE [SequenceCode]='CreditNo'        
    select @CreditNo as CreditMemoNo, @CreditAutoId as CreditMemoAutoId, (select statustype from [dbo].[StatusMaster] where [AutoId]=1 and [Category] = 'OrderMaster') status             
               
   End             
  COMMIT TRANSACTION                        
  END TRY                        
  BEGIN CATCH                        
  ROLLBACK TRAN                        
   Set @isException=1                        
   Set @exceptionMessage=ERROR_MESSAGE()                        
  End Catch                         
 END                             
If @Opcode=401                    
Begin                   

		select CreditAutoId as CreditMemoAutoId,CustomerAutoId as [CustomerAutoId], 0 as TaxType,                
		CreditNo as CreditMemoNo,isnull(referenceCreditMemoNumber,CONVERT(varchar(15),CreditAutoId)) as referenceCreditMemoNumber,                   
		(select (case when autoid=3 and isnull(cmm.OrderAutoId,0)!=0 then 'Applied' else statustype end) from [dbo].[StatusMaster]
		where [AutoId]=cmm.Status and [Category] = 'CreditMaster') as Status,  cmm.Status as StatusAutoId,              
		(select [dbo].ConvertUTCTimeStamp(isnull(CreditDate,getdate()))) as utcCreditMemoTimeStamp,TotalAmount as GrandTotal,OverallDisc,OverallDiscAmt,                
		TotalTax,ismltaxapply,MLQty,MLTaxPer,MLTax,AdjustmentAmt,GrandTotal as TotalAmount,              
		isnull((select (firstname+' '+lastname+' ('+empid+')') from employeemaster where autoid=cmm.CreatedBy),'') as SalesPersonName,                 
		isnull(Remarks,'') as CreditMemoRemark,                       
		isnull((select (firstname+' '+lastname+' ('+empid+')') from employeemaster where autoid=ApprovedBy),'Sales Manager Name') as SalesManagerName,                 
		'Sales Manager Remark Test Desc' as SalesManagerRemark,        
		DefaultBillAdd as BillAddrAutoId, DefaultShipAdd as ShipAddrAutoId,        
		'' Terms, isnull((select OrderNo from OrderMaster where AutoId=cmm.OrderAutoId),'') as OrderNo ,
		(
		select ProductAutoId,UnitMLQty as [MLQty],UnitAutoId ,                     
		QtyPerUnit, UnitPrice, RequiredQty, TotalPeice as TotalPieces, isnull(t.NetAmount,0.00) as NetPrice,
		(Case t.TaxRate when 0 then 'false' else  'true' end) as Tax            
		from CreditItemMaster as t             
		where t.CreditAutoId=cmm .CreditAutoId
		for json path,Include_NULL_VaLUES
		) 
		as CreditMemoItems ,
		(select ol.LogDate as ActionDate, Remarks, CreditAutoId as CreditMemoAutoId, (firstname+' '+lastname+' ('+empid+')') as Employee from CreditMemoLog as ol                
		inner join employeemaster as em on ol.empautoid=em.autoid                 
		where ol.CreditAutoId = cmm.CreditAutoId for json path,Include_NULL_VaLUES) as CreditMemoLog    
		from CreditMemoMaster as cmm         
		inner join CustomerMaster as cm on cmm.CustomerAutoId=cm.AutoId         
		where cmm.CreatedBy=@SalesPersonAutoId
		for json path,Include_NULL_VaLUES



--select CreditAutoId as CreditMemoAutoId,CustomerAutoId, 0 as TaxType,                
--CreditNo as CreditMemoNo,isnull(referenceCreditMemoNumber,CONVERT(varchar(15),CreditAutoId)) as referenceCreditMemoNumber,                   
--(select (case when autoid=3 and isnull(cmm.OrderAutoId,0)!=0 then 'Applied' else statustype end) from [dbo].[StatusMaster] where [AutoId]=cmm.Status and [Category] = 'CreditMaster') as Status,  cmm.Status as StatusAutoId,              
--isnull(CreditDate,getdate()) as CreditMemoDate,TotalAmount as GrandTotal,OverallDisc,OverallDiscAmt,                
--TotalTax,ismltaxapply,MLQty,MLTaxPer,MLTax,AdjustmentAmt,GrandTotal as TotalAmount,              
--isnull((select (firstname+' '+lastname+' ('+empid+')') from employeemaster where autoid=cmm.CreatedBy),'') as SalesPersonName,                 
--isnull(Remarks,'') as CreditMemoRemark,                       
--isnull((select (firstname+' '+lastname+' ('+empid+')') from employeemaster where autoid=ApprovedBy),'Sales Manager Name') as SalesManagerName,                 
--'Sales Manager Remark Test Desc' as SalesManagerRemark,        
--DefaultBillAdd as BillAddrAutoId, DefaultShipAdd as ShipAddrAutoId,        
--'' Terms, isnull((select OrderNo from OrderMaster where AutoId=cmm.OrderAutoId),'') as OrderNo        
--from CreditMemoMaster as cmm         
--inner join CustomerMaster as cm on cmm.CustomerAutoId=cm.AutoId         
--where cmm.CreatedBy=@SalesPersonAutoId        
                
--select CreditAutoId as CreditMemoAutoId, ProductAutoId,           
--(select autoid from [PackingDetails] where unittype  = isnull(t.UnitAutoId,1) and productautoid=t.productautoid) as UnitTypeAutoId,           
--QtyPerUnit, UnitPrice, RequiredQty, TotalPeice as TotalPieces, isnull(t.NetAmount,0.00) as NetPrice, t.TaxRate as Tax            
--from CreditItemMaster as t             
--where CreditAutoId in (select CreditAutoId from CreditMemoMaster where CreatedBy=@SalesPersonAutoId)                      
                 
--select ol.LogDate as ActionDate, Remarks, CreditAutoId as CreditMemoAutoId, (firstname+' '+lastname+' ('+empid+')') as Employee from CreditMemoLog as ol                
--inner join employeemaster as em on ol.empautoid=em.autoid                 
--where CreditAutoId in (select CreditAutoId from CreditMemoMaster where CreatedBy=@SalesPersonAutoId)                      
End                   
END TRY                        
BEGIN CATCH                        
Set @isException=1                        
Set @exceptionMessage=ERROR_MESSAGE()                        
END CATCH                        
END 
GO
