ALTER PROCEDURE [dbo].[ProcNotShippedReportbyProduct]        
@Opcode int=null,        
@Status int=null,  
@SortOrder int=null,  
@SortBy int=null,  
@SalsePersonAutoid int=null,        
@FromDate date =NULL,        
@ToDate date =NULL,        
@PageIndex INT = 1,        
@PageSize INT = 10,        
@RecordCount INT =null,        
@isException BIT OUT,        
@exceptionMessage VARCHAR(max) OUT        
AS        
BEGIN        
 BEGIN TRY        
  SET @isException=0        
  SET @exceptionMessage='Success'        
  IF @Opcode=41        
  BEGIN        
     select 
  (
   SELECT AutoId as STID,StatusType as ST FROM  StatusMaster where Category='OrderMaster' and AutoId not in(1,2,8) order by ST ASC  
   for json path
   ) as Status,
   (
   select AutoId SPID,FirstName+' '+LastName as SPN from EmployeeMaster where EmpType=2 order by SPN ASC
    for json path
   )as SalesPerson
   for json path  
  END        
  ELSE IF @Opcode=42        
  BEGIN        
	Select ROW_NUMBER() OVER(ORDER BY (  
	case when @SortBy=1 and @SortOrder=1 then ProductId end) asc,   
	(case when @SortBy=1 and @SortOrder=2 then ProductId end) desc,  
	(case when @SortBy=2 and @SortOrder=1 then ProductName end) asc,  
	(case when @SortBy=2 and @SortOrder=2 then ProductName end) desc,  
	(case when @SortBy=3 and @SortOrder=1 then (OrderQty-QtyShip) end) asc,  
	(case when @SortBy=3 and @SortOrder=2 then (OrderQty-QtyShip) end) desc ) AS RowNumber,        
	*,(OrderQty-QtyShip)      
	as RemainQty,SellingPrice as Amount INTO #TEMRESULT from (        
	select ProductId,REPLACE(ProductName, '  ', '') as ProductName,um.UnitType,      
	(um.UnitType +' ('+convert(varchar(25),QtyPerUnit) +')') as unit,          
	SUM(RequiredQty ) as OrderQty,SUM(ISNULL(QtyShip,0)) as QtyShip ,  
	SUM((RequiredQty-ISNULL(QtyShip,0))* oim.UnitPrice)  
	as SellingPrice,convert(varchar(20),pm.CurrentStock) +' ['+um1.UnitType+']'   as CurrentStock
	from   OrderItemMaster as  oim        
	inner join ProductMaster as pm on pm.AutoId=oim.ProductAutoId         
	inner join OrderMaster as om on om.AutoId=oim.OrderAutoId        
	inner join CustomerMaster as cm on cm.AutoId=om.CustomerAutoId        
	inner join PackingDetails as pd on pd.ProductAutoId=oim.ProductAutoId and pd.UnitType=oim.UnitTypeAutoId      
	inner join  UnitMaster as um on um.AutoId=oim.UnitTypeAutoId
	inner join  UnitMaster as um1 on um1.AutoId=pm.PackingAutoId
	where om.Status not in(1,2,8)
	and ((RequiredQty-QtyShip))>0
	and (ISNULL(@FromDate,'')='' OR ISNULL(@ToDate,'') = '' OR         
	(CONVERT(date,(select MAX([PackingDate]) from [GenPacking] as gp where gp.OrderAutoId=om.AutoId)) BETWEEN CONVERT(date,@fromDate) AND CONVERT(date,@ToDate)))        
	and(@SalsePersonAutoid is null or @SalsePersonAutoid=0 or om.SalesPersonAutoId=@SalsePersonAutoid)        
	and(@Status is null or @Status=0 or om.Status=@Status)       
	group by ProductId,ProductName,um.UnitType,convert(varchar(20), pm.CurrentStock) +' ['+um1.UnitType+']',  
	(um.UnitType +' ('+convert(varchar(25),QtyPerUnit) +')')       
	having (SUM(RequiredQty ) -SUM(ISNULL(QtyShip,0)))>0      
	) as t       
	ORDER BY   
	(case when @SortBy=1 and @SortOrder=1 then ProductId end) asc,   
	(case when @SortBy=1 and @SortOrder=2 then ProductId end) desc,   
	(case when @SortBy=2 and @SortOrder=1 then ProductName end) asc,  
	(case when @SortBy=2 and @SortOrder=2 then ProductName end) desc,   
	(case when @SortBy=3 and @SortOrder=1 then (OrderQty-QtyShip) end) asc,  
	(case when @SortBy=3 and @SortOrder=2 then (OrderQty-QtyShip) end) desc  
     
	SELECT COUNT(ProductId) AS RecordCount, case when @PageSize=0 then COUNT(RowNumber) else @PageSize end AS PageSize, @PageIndex AS PageIndex,Format(getdate(),'MM/dd/yyyy hh:mm tt') as PrintDate FROM #TEMRESULT         
	SELECT  * FROM #TEMRESULT        
	WHERE isnull(@PageSize,0)=0 or ISNULL(@PageIndex,0)=0 or RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1          
     
	SELECT ISNULL(SUM(RemainQty),0) AS RemainQty,ISNULL(SUM(Amount),0.00) AS Amount, isnull(Sum(QtyShip),0) as QtyShip, isnull(Sum(OrderQty),0) as OrderQty   
	from  #TEMRESULT    
  END        
 END TRY        
 BEGIN CATCH        
 SET @isException=1        
 SET @exceptionMessage=ERROR_MESSAGE()        
 END CATCH        
END     