USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcProductWisePLReport]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                   
create or alter PROCEDURE [dbo].[ProcNonCarryProductsReport]                        
@Opcode INT=NULL,                    
@ProductId varchar(20)=null,
@ProductName varchar(100)=null,
@Month int=null,
@PageIndex INT=1,                    
@PageSize INT=10,                    
@RecordCount INT=null,                    
@isException bit out,                    
@exceptionMessage varchar(max) out                            
AS                                    
BEGIN                       
BEGIN TRY                    
Set @isException=0                    
Set @exceptionMessage='Success'                    
  if(@Opcode=41)                    
	begin                  
		select ROW_NUMBER() over(order by ProductId) as RowNumber,* into #Result    from (                      
			select ProductId,ProductName,
			FORMAT((
			select max(orderdate) from OrderMaster as om 
			inner join Delivered_Order_Items as do on do.OrderAutoId=om.AutoId
			where convert(date,OrderDate)<convert(date,GETDATE()-@Month*30)
			and om.Status=11 and do.ProductAutoId=pm.AutoId

			),'MM/dd/yyyy') as lastdate
			from ProductMaster as PM
			where pm.AutoId not in (
			select distinct pm.AutoId from  OrderMaster  as OM
			inner join Delivered_Order_Items  as OIM on OM.AutoId=OIM.OrderAutoId
			inner join ProductMaster as PM on PM.AutoId=OIM.ProductAutoId
			where convert(date,OrderDate)>=convert(date,GETDATE()-@Month*30)
			and om.Status=11
			)
			and pm.ProductStatus=1
			and (@ProductId is null OR @ProductId='' OR ProductId=@ProductId)
			and (@ProductName is null OR @ProductName='' OR ProductName LIKE '%' + @ProductName + '%')
		) as t
                                    
	SELECT * FROM #Result                  
	WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))                   
	SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize = 0 then COUNT(RowNumber) else @PageSize end AS PageSize, @PageIndex AS PageIndex,Format(GETDATE(),'MM/dd/yyyy hh:mm tt') as PrintDate FROM #Result                    
     
 
	end                  
          
END TRY                    
BEGIN CATCH                    
Set @isException=1                    
Set @exceptionMessage=ERROR_MESSAGE()                    
END CATCH                                 
END 
GO
