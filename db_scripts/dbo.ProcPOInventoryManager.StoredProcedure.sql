ALTER Proc [dbo].[ProcPOInventoryManager]      
 @Opcode INT=NULL,                                                                                                                                              
 @POAutoId INT=NULL,                      
 @ProductAutoId  int =null,                
 @OrderNo nvarchar(15)=null,                       
 @EmpId int=null,                                                                                                                                                                                                                                              
 @RecAutoId int=null,    
 @BillAutoId int=null,
 @DraftAutoId int=null out,           
 @DeliveryDate DATETIME=NULL,              
 @RecieveStockRemark varchar(max)=null,          
 @VendorName varchar(100)=null,                                                                                                                                              
 @VenderAutoId INT=NULL,                       
 @NoofItems int=null,   
 @BillNo varchar(50)=null,  
 @BillDate DATETIME=NULL,   
 @ManagerRemark VARCHAR(max)=NULL,                                                                                                                                             
 @UnitAutoId INT=NULL,           
 @QtyPerUnit int=null,                                                                                                                                                                           
 @OrderStatus INT=NULL,              
 @PoStatus varchar(10)=null,                                                                                                              
 @PORemarks VARCHAR(max)=NULL,                   
 @Barcode varchar(50)=NULL,                
 @FromDate DateTime=null,                
 @ToDate DateTime=null,                  
 @ReqQty int=null,    
 @CostPrice decimal(11,3) =null,                                                                                                       
 @TableValue xml=null,   
 @dtbulkUnit DT_dtPOMManagePrice readonly,                                                                         
 @PageIndex INT = 1,                                                                                                                          
 @PageSize INT = 10,                                                                                                                                                                                                                                           
 @RecId varchar(20)=null,
 @isException bit out,                                     
 @exceptionMessage varchar(max) out                                                                                
AS                                                                                                                                              
 BEGIN                                                                                                                       
  BEGIN TRY                                                                                                                                      
  Set @isException=0                                                                                                                     
  Set @exceptionMessage='Success'      
        
  if @Opcode=41       
  begin       
  SELECT ROW_NUMBER() OVER(ORDER BY AutoId DESC) AS RowNumber, * INTO #Results FROM              
  (                                                                                      
  Select PO.AutoId,format(PO.PODate,'MM/dd/yyyy')as PODate,vm.VendorName,sm.StatusType,sm.ColorCode,NoofItems,                
  PONo,BillNo,FORMAT(BillDate,'MM/dd/yyyy') as BillDate,(em.FirstName+' '+em.LastName) as CreateBy from [dbo].[PurchaseOrderMaster] as Po                  
  left join VendorMaster vm on vm.AutoId=Po.VenderAutoId                   
  left join StatusMaster sm on sm.AutoId=Po.Status and sm.category='POOrd'     
  inner join EmployeeMaster as em on em.AutoId=Po.CreatedBy
  where Po.VendorType=0 and Po.Status in(1,3,2,5,6,4)   
  --Po.Status in(1)
  and (@OrderNo is null or @OrderNo ='' or PO.PONo like '%' + @OrderNo + '%')              
  and (@FromDate is null or @FromDate = '' or @Todate is null or @Todate = '' or (convert(date,PO.PODate) between Convert(date,@FromDate) and Convert(date,@Todate)))                         
  and (@PoStatus is null or @PoStatus='' or po.StockStatus=@PoStatus)                        
  and (@OrderStatus is null or @OrderStatus=0 or po.Status=@OrderStatus)                
  and (@VenderAutoId is null or @VenderAutoId=0 or po.VenderAutoId=@VenderAutoId)                   
  ) as t order by AutoId  DESC              
              
  SELECT COUNT(AutoId) AS RecordCount, case when @PageSize=0 then COUNT(AutoId) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Results              
  SELECT * FROM #Results              
  WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))                
  End       
  else if @Opcode=42      
  begin       
  Select * from StatusMaster where Category='POOrd' and AutoId in(1,2,3,4)  order by StatusType ASC              
  End        
  else if @Opcode=43      
  begin       
 Select AutoId,VendorName from VendorMaster where Status=1 and ISNULL(LocationTypeAutoId,0)=0 order by VendorName ASC              
  End    
      
  else if @Opcode=44     
  begin    
    
  Select Po.AutoId,PONo,format(PODate,'MM/dd/yyyy')as PODate,BillNo,format(BillDate,'MM/dd/yyyy')as BillDate,format(DeliveryDate,'MM/dd/yyyy') as DeliveryDate,VenderAutoId,vm.VendorName,(sm.StatusType) as Status,po.Status as StatusAutoId              
   ,sm.AutoId as StatusAutoId,NoofItems,PORemarks,StockStatus,RecieveStockRemark from [dbo].[PurchaseOrderMaster]as po             
   inner join StatusMaster sm on sm.AutoId=po.Status and  sm.category='POOrd'            
   inner join VendorMaster vm on vm.AutoId=po.VenderAutoId              
   where po.AutoId=@POAutoId   
  
    Select pm.ProductName,pm.ProductId,um.UnitType,isnull(PRP.ReceiveQty,0)as RecivedQty,pp.AutoId,isnull(pp.CostPrice,0),pp.POAutoId,pp.ProductAutoId,pp.Qty,ums.UnitType as RecUnitType    
   ,pp.QtyPerUnit,(pp.QtyPerUnit*pp.Qty)as ReqTotalPieces,isnull(PRP.QtyPerUnit*isnull(PRP.ReceiveQty,0),0)as RecTotalPieces,pp.Unit,pd.CostPrice as oldPrice,pp.CostPrice as CostPrice    
   from PurchaseProductsMaster as pp              
   inner join ProductMaster pm on pm.AutoId=pp.ProductAutoId              
   inner join UnitMaster um on um.AutoId=pp.Unit    
   inner join PackingDetails pd on pd.ProductAutoId=pp.ProductAutoId  and pd.UnitType=pp.Unit             
  left join POReceiveMaster as PMS on PMS.POAutoId=pp.POAutoId and PMS.AutoId=@RecAutoId
   left join POReceiveProduct as PRP on PRP.ProductAutoId=pp.ProductAutoId and pp.Unit=PRP.UnitAutoId and PMS.AutoId=PRP.POReceiveAutoId 
    left join UnitMaster ums on ums.AutoId=PRP.RecUnitAutoId  
   where pp.POAutoId=@POAutoId   
   
   SELECT PD.ProductAutoId,UM.AutoId AS AutoId, UM.[UnitType] AS UnitType, PD.[Qty],ISNULL(EligibleforFree,0) as EligibleforFree FROM [dbo].[PackingDetails] AS PD  
   INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = PD.UnitType            
   WHERE PD.[ProductAutoId] in (select ProductAutoId from PurchaseProductsMaster where POAutoId=@POAutoId)    

   select PORecieveId,prm.Status,StatusType,FirstName+' '+LastName as ReceiveBy,format(ReceiveDate,'MM/dd/yyyy')as ReceiveDate from POReceiveMaster as prm
   inner join StatusMaster as sm on sm.AutoId=prm.Status and Category='PORec'
   inner join EmployeeMaster as em on em.AutoId=prm.ReceiveBy
   where prm.AutoId=@RecAutoId
    
 End     
 else if(@Opcode=45)    
 begin    
 SELECT um.AutoId as UnitType,ISNULL(Qty,1) AS Qty,isnull(MinPrice,0.00)as MinPrice,isnull(CostPrice,0.00) as CostPrice,                                                                                       
   isnull(Price,0)as Price,isnull(MinPrice,0) as SRP,P_CommCode,isnull(WHminPrice,0) as WHminPrice,                      
   case when ISNULL(PM.PackingAutoId,0)=PD.UnitType then ISNULL(PM.PackingAutoId,0) else 0 end as DefaultPack,                       
   case when ISNULL((Select COUNT(*) from OrderItemMaster where ProductAutoId=@ProductAutoId AND UnitTypeAutoId=UM.AutoId),0)=0 then 0 else 1  end as OrderAttachment,                      
   UM.UnitType AS UnitName,isnull(PD.EligibleforFree,0) as Free,isnull(PD.AutoId,0) as PackingId FROM PackingDetails AS PD                                                         
   inner JOIN UnitMaster AS UM ON UM.AutoId=PD.UnitType AND PD.ProductAutoId=@ProductAutoId                        
   inner JOIN ProductMaster as pm on pm.AutoId=pd.ProductAutoId  order by UM.AutoId DESC  
    
  Select ('('+convert(varchar(50),ProductId)+' - '+ProductName+')') as ProductName from ProductMaster where AutoId=@ProductAutoId   
  
	--update PurchaseProductsMaster set CostPrice=@CostPrice where ProductAutoId=@ProductAutoId and Unit=@UnitAutoId and POAutoId=@POAutoId  

	
 end  
   
 ELSE IF @OpCode=21                                             
  BEGIN                                                                                                    
    BEGIN TRY                                                                                                  
    BEGIN TRAN                                                                                          
     insert into PackingDetailsUpdateLog(ProductAutoId,UnitType,MinPrice,CostPrice,Price,SRP,WHminPrice,UpdatedByAutoId,DateTime,Reff_Code)                                                           
     select pd.ProductAutoId,pd.UnitType,pd.MinPrice,pd.CostPrice,pd.Price,pm.P_SRP,pd.WHminPrice,@EmpId,GETDATE(),'[dbo].[ProcPOInventoryManager],OpCode=21' from PackingDetails pd                                                                             
                   
     inner join UnitMaster um on um.AutoId = pd.UnitType  
	 inner join ProductMaster pm on pm.AutoId=pd.ProductAutoId
     where ProductAutoId in (select top 1 bu.ProductAutoId from @dtbulkUnit bu)                                                        
                                                                                                                                              
     UPDATE PD SET pd.CostPrice=UM.COSTPRICE,pd.Price=UM.BASEPRICE,                                                    
     pd.minprice=UM.RetailPrice,pd.WHminPrice=UM.WHPRICE,UpdateDate=GETDATE(),ModifiedBy=@EmpId FROM PackingDetails AS PD                                                                                      
     INNER JOIN @dtbulkUnit AS UM ON UM.ProductAutoId=PD.ProductAutoId and UM.UnitAutoId=PD.UnitType                                                                                                                                                   
    
    COMMIT TRAN                                                                              
    END TRY                                                                          
    BEGIN CATCH                                           
   ROLLBACK TRANSACTION                                                                                                                                                       
   SET @isException=1                                                                                                 
   SET @exceptionMessage= 'Oops, Something went wrong .Please try again.'                                                                                                                                                    
  END CATCH                                
 END  
   
 Else If @Opcode=22  
 Begin  
 BEGIN TRY     
  BEGIN TRAN      
		update PurchaseOrderMaster set Status=4 where AutoId=@POAutoId 

		 set @RecId=(select PONo from PurchaseOrderMaster where AutoId=@POAutoId)
		 SET @RecieveStockRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=41), '[PONo]', @RecId) 

		insert into PORemarkLogs(RemarkType,Remark,POAutoId,CreatedOn,CraetedBy,ActionTaken)values  
		(@RecieveStockRemark,@ManagerRemark,@PoAutoId,GETDATE(),@EmpId,41)  

		-------------------------------------------------------Stock Entry--------------------------------------------------------------------------------------
		
		
		insert into StockEntry ([BillNo],[BillDate],[Remarks],VendorAutoId,[CreatedBy],[CreateDate],[POAutoId],[ReceivedBy],[ReceivedDate]) 
		select BillNo,BillDate,'PO Order',VenderAutoId,@EmpId,GETDATE(),@POAutoId,@EmpId,GETDATE() from PurchaseOrderMaster where AutoId=@POAutoId

		set @BillAutoId=SCOPE_IDENTITY()

		insert into BillItems ([BillAutoId],[ProductAutoId],[UnitAutoId],[Quantity],[QtyPerUnit],[Price]) select @BillAutoId,PD.ProductAutoId,PD.Unit
		,PD.Qty,PD.QtyPerUnit,PD.UnitPrice from PurchaseProductsMaster as PD where POAutoId=@POAutoId

  COMMIT TRANSACTION    
  END TRY    
  BEGIN CATCH    
  ROLLBACK TRAN    
  SET @isException=1    
  SET @exceptionMessage='Oops! Something went wrong.Please try later'
  END CATCH    
 End  
 Else If @Opcode=23  
 Begin  
    --update PurchaseProductsMaster set CostPrice=@CostPrice where ProductAutoId=@ProductAutoId and Unit=@UnitAutoId and POAutoId=@POAutoId  
 BEGIN TRY     
  BEGIN TRAN      
		update POReceiveMaster set Status=3 where POAutoId=@POAutoId and AutoId=@RecAutoId

		select Stock as OldStock,(Stock+(isnull(ppm.ReceiveQty,0)*isnull(QtyPerUnit,0))) as NewStock,ProductId,pp.POAutoId,ppm.* into #temp23 from POReceiveProduct as ppm
		inner join ProductMaster as pm on pm.AutoId=ppm.ProductAutoId
		inner join POReceiveMaster as pp on pp.AutoId=ppm.POReceiveAutoId
		where POReceiveAutoId=@RecAutoId and (isnull(ppm.ReceiveQty,0)*isnull(QtyPerUnit,0))>0 

		update pm set Stock=tmp.NewStock from ProductMaster as pm
		inner join #temp23 as tmp on tmp.ProductAutoId=pm.AutoId

		update PurchaseOrderMaster set BillNo=@BillNo,BillDate=@BillDate,RecieveStockRemark=@ManagerRemark where AutoId=@POAutoId
	
		 set @RecId=(select PORecieveId from POReceiveMaster where AutoId=@RecAutoId)
		 SET @RecieveStockRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=40), '[RecNo]', @RecId)    
		insert into PORemarkLogs(RemarkType,Remark,POAutoId,CreatedOn,CraetedBy,ActionTaken)values
		(@RecieveStockRemark,@ManagerRemark,@PoAutoId,GETDATE(),@EmpId,40)  

		insert into [dbo].[ProductStockUpdateLog]([ProductId],[OledStock],[NewStock],[UserAutoId],[DateTime],ReferenceId,ActionRemark,ReferenceType) 
		select temp.ProductId,temp.OldStock,temp.NewStock,@EmpId,GETDATE(),temp.POAutoId,'Stock Updated from PO','PurchaseOrderMaster'  from #temp23 as temp



----------------------------------------------------------------Stock--------------------------------------------------------------------------------------
select CostPrice,tb.* into #tbl from PurchaseProductsMaster as pp inner join #temp23 as tb on tb.POAutoId=pp.POAutoId and pp.ProductAutoId=tb.ProductAutoId --and pp.Unit=tb.RecUnitAutoId

if (select count(*) from StockEntry where BillNo=@BillNo)=0
	begin
			insert into StockEntry ([BillNo],[BillDate],[Remarks],VendorAutoId,[CreatedBy],[CreateDate],[POAutoId],[ReceivedBy],[ReceivedDate]) 
			select BillNo,BillDate,'PO Order',VenderAutoId,@EmpId,GETDATE(),@POAutoId,@EmpId,GETDATE() from PurchaseOrderMaster where AutoId=@POAutoId
			set @BillAutoId=SCOPE_IDENTITY()

			insert into BillItems ([BillAutoId],[ProductAutoId],[UnitAutoId],[Quantity],[QtyPerUnit],[Price]) select @BillAutoId,PD.ProductAutoId,PD.RecUnitAutoId
			,PD.ReceiveQty,PD.QtyPerUnit,PD.CostPrice from #tbl as PD where POAutoId=@POAutoId

	end

else if (select COUNT(*) from StockEntry where BillNo=@BillNo and POAutoId!=@POAutoId)>0
	begin
		SET @isException=1    
		SET @exceptionMessage='Bill No. is already settled in another PO'
	end

else if (select COUNT(*) from StockEntry where BillNo=@BillNo and POAutoId=@POAutoId)>0
	begin
		set @BillAutoId=(select AutoId from StockEntry where BillNo=@BillNo and POAutoId=@POAutoId)

		if (select COUNT(*) from BillItems as BI inner join #tbl as tmp on tmp.ProductAutoId=BI.ProductAutoId and tmp.RecUnitAutoId=BI.UnitAutoId and BillAutoId=@BillAutoId)>0
			begin
				update bi set Quantity=Quantity+tp.ReceiveQty from BillItems as bi inner join #tbl as tp on BillAutoId=@BillAutoId and  tp.ProductAutoId=bi.ProductAutoId and tp.RecUnitAutoId=bi.UnitAutoId
			end
			else
			begin
				insert into BillItems ([BillAutoId],[ProductAutoId],[UnitAutoId],[Quantity],[QtyPerUnit],[Price]) select @BillAutoId,PD.ProductAutoId,PD.RecUnitAutoId
			,PD.ReceiveQty,PD.QtyPerUnit,PD.CostPrice from #tbl as PD where POAutoId=@POAutoId
			end
	end
	
-------------------------------------------------------Automatic Close PO--------------------------------------------------------------------------------
		
		if (select count(*) from PurchaseProductsMaster where RemainPieces>0 and POAutoId=@POAutoId)=0
		begin
--		insert into StockEntry ([BillNo],[BillDate],[Remarks],VendorAutoId,[CreatedBy],[CreateDate],[POAutoId],[ReceivedBy],[ReceivedDate]) 
--		select BillNo,BillDate,'PO Order',VenderAutoId,@EmpId,GETDATE(),@POAutoId,@EmpId,GETDATE() from PurchaseOrderMaster where AutoId=@POAutoId

--		set @BillAutoId=SCOPE_IDENTITY()

--		insert into BillItems ([BillAutoId],[ProductAutoId],[UnitAutoId],[Quantity],[QtyPerUnit],[Price]) select @BillAutoId,PD.ProductAutoId,PD.Unit
--		,PD.Qty,PD.QtyPerUnit,PD.UnitPrice from PurchaseProductsMaster as PD where POAutoId=@POAutoId


		update PurchaseOrderMaster set Status=4 where AutoId=@POAutoId 

		 set @RecId=(select PONo from PurchaseOrderMaster where AutoId=@POAutoId)
		 SET @RecieveStockRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=41), '[PONo]', @RecId) 

		insert into PORemarkLogs(RemarkType,Remark,POAutoId,CreatedOn,CraetedBy,ActionTaken)values  
		(@RecieveStockRemark,@ManagerRemark,@PoAutoId,GETDATE(),@EmpId,41)  
		end
    
  COMMIT TRANSACTION    
  END TRY    
  BEGIN CATCH    
  ROLLBACK TRAN    
  SET @isException=1    
  SET @exceptionMessage=ERROR_MESSAGE()
  END CATCH    
 End  
     Else If @Opcode=24  
 Begin 
 BEGIN TRY     
  BEGIN TRAN  
		update PurchaseProductsMaster set CostPrice=@CostPrice where ProductAutoId=@ProductAutoId and Unit=@UnitAutoId and POAutoId=@POAutoId  

		--update PurchaseOrderMaster set BillNo=@BillNo,BillDate=@BillDate,RecieveStockRemark=@ManagerRemark where AutoId=@POAutoId

  COMMIT TRANSACTION    
  END TRY    
  BEGIN CATCH    
  ROLLBACK TRAN    
  SET @isException=1    
  SET @exceptionMessage='Oops! Something went wrong.Please try later'
  END CATCH    
 End   
  else if @Opcode=25     
  begin       
 Select * from StatusMaster where Category='PORec' and AutoId in(1,2,3,4)  order by StatusType ASC              
  End  
 END TRY                                                                
 BEGIN CATCH                                                 
 Set @isException=1                                           
 Set @exceptionMessage='Oops! Something went wrong.Please try later.'                                                                                                                                              
 END CATCH       
 End     
    
    
    