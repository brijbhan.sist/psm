USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcEndOfDay]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ProcEndOfDay]    
@Opcode int=Null,                       
@EndDate Datetime = NULL,   
@FromDate Datetime = NULL,   
@ToDate Datetime = NULL,          
@SalesPersonAutoId int = NULL,   
@EmpAutoId int=null,   
@PageIndex INT = 1,                                           
@PageSize INT = 10,    
@EndTime datetime=null,  
@isException bit out,                                                    
@exceptionMessage varchar(max) out        
    
AS                                                    
BEGIN                                                     
  BEGIN TRY                                                    
  Set @isException=0                                                    
  Set @exceptionMessage='Success'     
  if @Opcode = 11    
  begin    
  IF NOT EXISTS(select * from tbl_DayEndReport where SalesPersonAutoId=@SalesPersonAutoId and CONVERT(date,EndDate)=convert(date,@EndDate))  
  BEGIN  
  
   insert into tbl_DayEndReport(SalesPersonAutoId,EndDate,EndTime,CreatedOn,CreatedBy)    
   values (@SalesPersonAutoId,@EndDate,@EndTime,GETDATE(),@EmpAutoId)    
  END  
  ELSE  
  BEGIN  
  update tbl_DayEndReport SET EndDate=@EndDate,EndTime=@EndTime  where SalesPersonAutoId=@SalesPersonAutoId  
   and CONVERT(date,EndDate)=convert(date,@EndDate)  
  END  
  end    
 if @Opcode = 41    
 begin    
 select AutoId, (FirstName + ' ' + LastName) as SalesPerson from EmployeeMaster em where EmpType = 2 and Status = 1    
 end    
    
 if @Opcode = 42     
 begin    
  select ROW_NUMBER() over(order by CAST(EndDate AS datetime)+CAST(EndTime AS datetime) desc) as RowNumber, der.AutoID,(em.FirstName + ' ' + em.LastName) as SalesPerson,FORMAT(der.EndDate,'MM/dd/yyyy') as EndDate,  
  Convert(varchar(5),der.EndTime) as EndTime ,  
  (SELECT count(Autoid) FROM OrderMaster WHERE Status IN (1,2,9)    
  AND OrderDate<=(SELECT  CAST(EndDate AS datetime)+CAST(EndTime AS datetime) FROM tbl_DayEndReport AS RE WHERE  RE.AutoID=der.AutoID)  
  AND SalesPersonAutoId=(SELECT RE.SalesPersonAutoId FROM tbl_DayEndReport AS RE WHERE  RE.AutoID=der.AutoID))   
  as TotalOrder   
  into #Result from tbl_DayEndReport der    
  inner join EmployeeMaster em on em.AutoId = der.SalesPersonAutoId    
  where (ISNULL(@SalesPersonAutoId,0)=0 or der.SalesPersonAutoId=@SalesPersonAutoId)  
   and (@FromDate is null or @ToDate is null or (CONVERT(date,EndDate)between convert(date,@FromDate) and CONVERT(date,@ToDate)))  
   
  order by CAST(EndDate AS datetime)+CAST(EndTime AS datetime)  desc
  
  SELECT * FROM #Result  
      WHERE @PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)   
      SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize=0 then COUNT(RowNumber) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Result    
 end    
    
 END TRY                                                    
  BEGIN CATCH                                                    
    Set @isException=1                                                     
    Set @exceptionMessage=ERROR_MESSAGE()                                                    
  END CATCH                                                    
END 
GO
