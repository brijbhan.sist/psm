ALTER PROCEDURE [dbo].[ProcReportPaymentDailyReport]                                                
@Opcode INT =null,                                                  
@FromDate Date = null,                                                
@ToDate Date = null,                                                
@PageIndex int=null,                                                   
@PageSize  int=10,                                                   
@RecordCount int=null,                                                   
@isException bit out,                                                    
@exceptionMessage varchar(max) out                                                  
as                                                
begin                                                  
begin try                                                  
SET @exceptionMessage= 'Success'                                                    
   SET @isException=0                                                 
   if @Opcode = 41                                                
   begin                                                
  WITH COMPANYINFO(PaymentDate,PaymentMode,ReceivedAmount,OrderAutoId,CustomerAutoid,STime) AS                                                
  (                                                
  select CONVERT(date,PaymentDate) PaymentDate,  
  (select pmm.PaymentMode from PAYMENTModeMaster as pmm where pmm.autoid=pd.PaymentMode)  
  as PaymentMode ,pod.ReceivedAmount,OrderAutoId,CustomerAutoid,                                    
  format(pd.PaymentDate,'hh:mm tt') as STime                                             
  from CustomerPaymentDetails as pd                                                 
  inner join PaymentOrderDetails as pod on pd.PaymentAutoid=pod.PaymentAutoid 
   where      pd.Status=0     and            
  (ISNULL(@FromDate,'')='' OR ISNULL(@ToDate,'') = '' or (CONVERT(date,PaymentDate) BETWEEN (@FromDate) AND( @ToDate)))                  
  )                                                
  SELECT *                                                 
  into #ResultGroup FROM COMPANYINFO                                                
  PIVOT (                                                
  SUM(ReceivedAmount) FOR PaymentMode IN ([Cash],[Check],[Money Order],[Credit Card],[Store Credit],[Electronic Transfer],[NO Any Method])                                                 
  )  AS P                                                
  ORDER BY CONVERT(date,PaymentDate) desc                                            
                                                  
  select ROW_NUMBER() over(order by Convert(date,PaymentDate) desc) as RowNumber,FORMAT(PaymentDate,'MM/dd/yyyy') as PaymentDate,                                      
  (select ISNULL(sum([NewCreditAmount]),0) from CustomerPaymentDetails as p where p.Status=0 and p.PaymentMode!=5 and CONVERT(date, p.PaymentDate) =  CONVERT(date, t.PaymentDate)) as CreditAmount,                                      
  count(distinct CustomerAutoid) as TotalNoOfCustomer, COUNT(OrderAutoId) as noOfOrder, sum(isnull(cash,0))as TotalCash,sum(isnull([Check],0))                                      
  as TotalCheck,sum(isnull([Money Order],0))as TotalMoneyOrder,sum(isnull([Electronic Transfer],0))as TotalElectronicTransfer,                                                 
  sum(isnull([Credit Card],0))as TotalCreditCard,sum(isnull([Store Credit],0))as TotalStoreCredit,sum(isnull(cash,0) + (isnull([Check],0)) +                       
  isnull([Money Order],0) + isnull([Credit Card],0) + isnull([Store Credit],0)+isnull([Electronic Transfer],0)) as TotalPaid,                   
  isnull((select isnull(sum(SortAmount),0) from CustomerPaymentDetails as spd where  CONVERT(date,spd.PaymentDate) =convert(date, t.PaymentDate)),0) as Short,                         
  isnull((select sum(isnull(ExpenseAmount,0)) as d from ExpenseMaster where convert(date, ExpenseDate) = convert(date, PaymentDate)),0) as Expense                             
  into #Result from #ResultGroup as t                                
  where                       
  (ISNULL(@FromDate,'')='' OR ISNULL(@ToDate,'') = '' or ((PaymentDate) BETWEEN (@FromDate) AND( @ToDate)))                     
  group by CONVERT(date,PaymentDate)                                  
  order by Convert(date,PaymentDate) desc                                            
                                                
  SELECT * FROM #Result                                                
  WHERE @PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)  
  order by Convert(date,PaymentDate) desc                                                 
  SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize=0 then COUNT(RowNumber) else @PageSize end AS PageSize,
  @PageIndex AS PageIndex,FORMAT(getdate(),'MM/dd/yyyy hh:mm tt') as PrintDate FROM #Result                             
                            
  select sum(ISNULL(TotalNoOfCustomer,0)) as TotalNoOfCustomer,sum(ISNULL(noOfOrder,0)) as noOfOrder,sum(ISNULL(TotalCash,0.00)) as TotalCash,                      
  sum(ISNULL(CreditAmount,0.00)) as CreditAmount,  sum(ISNULL(TotalElectronicTransfer,0.00)) as TotalElectronicTransfer,                        
  sum(ISNULL(TotalCheck,0.00)) as TotalCheck,sum(ISNULL(TotalMoneyOrder,0.00)) as TotalMoneyOrder,sum(ISNULL(TotalCreditCard,0.00)) as TotalCreditCard                          
  ,sum(ISNULL(TotalStoreCredit,0.00)) as TotalStoreCredit,sum(ISNULL(TotalPaid,0.00)) as TotalPaid,sum(ISNULL(Short,0.00)) as Short,sum(ISNULL(Expense,0.00)) as Expense                           
   from #Result   
   

   end                                        
   if @Opcode = 42                                                
    begin                     
                                           
			WITH COMPANYINFO(PaymentDate,PaymentMode,ReceivedAmount,OrderAutoId,CustomerAutoid,STime,ChequeNo) AS                                  
			(                                  
			select CONVERT(date,PaymentDate) PaymentDate,
			(select pmm.PaymentMode from PAYMENTModeMaster as pmm where pmm.autoid=pd.PaymentMode) as  PaymentMode ,pod.ReceivedAmount,pod.OrderAutoId,
			pd.CustomerAutoid, format(pd.PaymentDate,'hh:mm tt') as STime ,spm.ChequeNo                                 
			from CustomerPaymentDetails as pd                                   
			inner join PaymentOrderDetails as pod on pd.PaymentAutoid=pod.PaymentAutoid
			LEFT Join SalesPaymentMaster as spm on spm.PaymentAutoId=pd.refPaymentId
			where   pd.Status=0     and          CONVERT(date,PaymentDate) = @FromDate  
  
			)                                  
			SELECT *                                   
			into #ResultGroup42                                   
			FROM COMPANYINFO                                  
			PIVOT (                                  
			SUM(ReceivedAmount) FOR PaymentMode IN ([Cash],[Check],[Money Order],[Credit Card],[Store Credit],[Electronic Transfer],[NO Any Method])                                   
			)  AS P                                                     
                                                
			ORDER BY PaymentDate                                     
                                                
  select ROW_NUMBER() over(order by PaymentDate) as RowNumber,FORMAT(PaymentDate,'MM/dd/yyyy') + ':' + FORMAT(PaymentDate,'dddd')                                        
  as PaymentDate,count(distinct CustomerAutoid) as TotalNoOfCustomer,         
  (select ISNULL(sum([NewCreditAmount]),0) from CustomerPaymentDetails as tt where tt.Status=0 and PaymentMode!=5 and  CONVERT(date, PaymentDate) =  @FromDate) as CreditAmount,                           
  COUNT(OrderAutoId) as noOfOrder,        
  sum(isnull(cash,0))as TotalCash,sum(isnull([Check],0))as TotalCheck,sum(isnull([Money Order],0))as TotalMoneyOrder,                                                 
  sum(isnull([Credit Card],0))as TotalCreditCard,sum(isnull([Store Credit],0))as TotalStoreCredit,sum(isnull([Electronic Transfer],0))as TotalElectronicTransfer,                                  
  sum(isnull(cash,0) + (isnull([Check],0)) + isnull([Money Order],0) + isnull([Credit Card],0) + isnull([Store Credit],0)+isnull([Electronic Transfer],0)) as   TotalPaid,                                    
  (select isnull(sum(SortAmount),0) from CustomerPaymentDetails where  CONVERT(date,PaymentDate) = @FromDate) as Short,                                  
  isnull((select sum(isnull(ExpenseAmount,0)) as d from ExpenseMaster where  convert(date, ExpenseDate) = convert(date, PaymentDate)),0) as Expense                                   
  from #ResultGroup42                                                
                                                
  group by PaymentDate                                               
  order by PaymentDate                                                
                                                
  select cm.CustomerName,om.OrderNo,sum(ISNULL(R42.Cash,0))as Cash,sum(isnull([Check],0)) as tCheck,sum(ISNULL(R42.[Money Order],0))as MoneyOrder,                                              
  sum(ISNULL(R42.[Credit Card],0))as CreditCard,sum(isnull(R42.[Store Credit],0))as StoreCredit,sum(isnull(R42.[Electronic Transfer],0))as ElectronicTransfer,                                        
  sum(ISNULL(R42.Cash,0) + isnull([Check],0) + ISNULL(R42.[Money Order],0) + ISNULL(R42.[Credit Card],0)                                        
  + isnull(R42.[Store Credit],0)+isnull(R42.[Electronic Transfer],0)) as TotalAmount,STime,ChequeNo                                               
  from #ResultGroup42 R42                                                
  inner join CustomerMaster cm on cm.AutoId = R42.CustomerAutoid                                                
  inner join OrderMaster om on om.AutoId = R42.OrderAutoId                                            
  group by cm.CustomerName,om.OrderNo ,STime,ChequeNo                                                 
  order by Convert(time,STime) desc,om.OrderNo                                                
                                                
  select CurrencyName,cm.CurrencyValue,sum(NoOfValue) as PNoOfValue,0 ENoOfValue ,SUM(TotalAmount) as PTotalAmount,0 as ETotalAmount                                            
  into #currenycollect                                          
  from Paymentcurrencydetails                                              
  inner join CurrencyMaster cm on cm.autoid = currencyautoid                                               
  where PaymentAutoId in (select PaymentAutoId from CustomerPaymentDetails as pd where pd.Status=0 and CONVERT(date,PaymentDate) = @FromDate)                                              
  group by currencyname,CurrencyValue                                              
  order by CurrencyValue                                              
                                           
   select CurrencyName,cm.CurrencyValue, 0 as PNoOfValue,(sum(ec.TotalCount)) as ENoOfValue,0 as PTotalAmount,(SUM(TotalAmount)) as ETotalAmount                                           
   into #currenyExpense from ExpensiveCurrency   as ec                                           
   inner join CurrencyMaster cm on cm.autoid = currencyautoid                                               
   where ec.ExpenseAutoId in (select emp3.AutoId from ExpenseMaster as emp3 where CONVERT(date,ExpenseDate) = @FromDate)                                              
   group by currencyname,cm.CurrencyValue                                
   order by cm.CurrencyValue                                             
                                          
   select CurrencyName,SUM(ISNULL(PNoOfValue,0)-ISNULL(ENoOfValue,0)) as NoOfValue ,SUM(ISNULL(PTotalAmount,0)-ISNULL(ETotalAmount,0)) as TotalAmount                          
  from (                                          
  select * from #currenycollect                                          
  union                                            
  select * from #currenyExpense                                          
  ) as t                                     
  group by CurrencyName,CurrencyValue                                     
  having SUM(ISNULL(PNoOfValue,0)-ISNULL(ENoOfValue,0))>0                                         
  order by CurrencyValue desc , CurrencyName                                
                                    
  select PaymentId,FORMAT(PaymentDate,'MM/dd/yyyy') as PaymentDate, pmm.PaymentMode,ReceivedAmount,SortAmount, (ISNULL(em.FirstName,'') + ' ' +ISNULL(em.LastName,'')) ReceivedBy,                          
  (ISNULL(em2.FirstName,'') + ' ' +ISNULL(em2.LastName,'')) as SettlementBy,cm.CustomerId,cm.CustomerName from CustomerPaymentDetails cpd                           
  inner join EmployeeMaster em on em.AutoId = cpd.ReceivedBy                                    
  inner join EmployeeMaster em2 on em2.AutoId = cpd.EmpAutoId                             
  inner join CustomerMaster cm on cm.AutoId = cpd.CustomerAutoId                    
  left join PAYMENTModeMaster as pmm on cpd.PaymentMode=pmm.AutoID                                     
  where CONVERT(date,PaymentDate) = @FromDate and SortAmount > 0                                       
                
 select cpd.PaymentId as SettlementId,cm.CustomerName, Format(spm.ChequeDate, 'MM/dd/yyyy') as ChequeDate,spm.ChequeNo,      
spm.ReceivedAmount,spm.CancelRemarks,Format(cpd.PaymentDate,'MM/dd/yyyy') as SettlementDate,      
format(spm.CancelDate,'MM/dd/yyyy') as CancelDate,(ISNULL(em.FirstName,'') + ' ' + ISNULL(LastName,'')) as CancelBy,      
        
    stuff((   select ',' + OM.OrderNo      
        from PaymentOrderDetails POD      
  INNER JOIN OrderMaster as OM on POD.OrderAutoId=OM.AutoId      
        where POD.PaymentAutoId = cpd.PaymentAutoId       
        for xml path('')      
    ),1,1,'') as OrderNo      
      
 from SalesPaymentMaster spm                                 
 left join CustomerMaster cm on cm.AutoId = spm.CustomerAutoId                              
 left join EmployeeMaster em on em.AutoId = spm.cancelBy         
 left join CustomerPaymentDetails as cpd on spm.PaymentAutoId=cpd.refPaymentId                             
 where spm.PaymentMode = 2 and CONVERT(date,spm.CancelDate) = Convert(date,@FromDate) and spm.Status=5           
                                     
  select * from #ResultGroup42                           
                                  
  Select ISNULL([NewCreditAmount],0) as CreditAmount,PMM.PaymentMode,CPD.PaymentId,Format(CPD.PaymentDate,'MM/dd/yyyy') as PaymentDate,                        
  ISNULL(EMp.FirstName,'')+' '+ISNULL(EMp.LastName,'') as ReceivedBY,CM.CustomerId,CM.CustomerName ,EM.FirstName+' '+EM.LastName as SettleBy                                
  from CustomerPaymentDetails as CPD                                  
  Inner join PAYMENTModeMaster AS PMM on                                  
  CPD.PaymentMode=PMM.AutoID                         
  INNER Join EmployeeMaster AS EMp on                        
  CPD.ReceivedBy=EMp.AutoId                        
  Inner join CustomerMaster AS CM on                                  
  CPD.CustomerAutoId=CM.AutoId                                  
  INNER JOIN EmployeeMaster AS EM on                                  
  CPD.EmpAutoId=EM.AutoId                          
  where 
  cpd.Status=0 and
  CPD.PaymentMode!=5 and  CONVERT(date, PaymentDate) =  @FromDate and CPD.[NewCreditAmount]!=0                                  
                       
	Select ISNUll(pmm.PaymentMode,'') as PaymentMode,ExpenseDescription,ExpenseAmount,ISNULL(EMP.FirstName,'')+' '+ISNULL(EMP.LastName,'') as ExpenseBy from ExpenseMaster em                  
	Inner JOIN PAYMENTModeMaster as pmm on em.PaymentAutoId=pmm.AutoID              
	Inner join EmployeeMaster as EMP on em.CreateBy=EMP.AutoId Where em.ExpenseDate=@FromDate 
  
   end                                                
   end try                                                  
  begin catch                             
   SET @isException=1                                                  
   SET @exceptionMessage= ERROR_MESSAGE()                                                   
end catch                                                  
end 
