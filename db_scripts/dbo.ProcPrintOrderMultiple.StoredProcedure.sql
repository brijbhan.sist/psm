USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcPrintOrderMultiple]    Script Date: 1/4/2020 2:16:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER Procedure [dbo].[ProcPrintOrderMultiple]
 @Opcode INT=NULL,
 @OrderAutoId INT=NULL, 
 @isException bit out,                                       
 @exceptionMessage varchar(max) out                                                                                  
AS                                                                                                                                                
BEGIN                                                                                                                           
  BEGIN TRY                                                                                                                                        
   Set @isException=0                                                                                                                       
   Set @exceptionMessage='Success'  

   If @Opcode = 41
   begin
     SELECT OM.[AutoId],OM.[OrderNo],CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,                                                                  
	  CM.[CustomerId],CM.[CustomerName],                                                                        
	  EM.[FirstName] + ' ' + EM.[LastName] As SalesPerson,                                                     
	  (select EM1.[FirstName] + ' ' + EM1.[LastName] from [dbo].[EmployeeMaster] AS EM1 where EM1.[AutoId]                                                                   
	  = OM.PackerAutoId) AS PackerName,                                                                  
	  (select EM1.[FirstName] + ' ' + EM1.[LastName] from [dbo].[EmployeeMaster] AS EM1 where EM1.[AutoId]                                                                   
	  = OM.warehouseAutoid) AS WarehouseName,OrderRemarks,WarehouseRemarks  FROM [dbo].[OrderMaster] As OM                                                                  
	  INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.Status AND SM.[Category] = 'OrderMaster'                                                                  
	  INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[AutoId] = OM.[BillAddrAutoId]                                                                  
	  INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = OM.[ShipAddrAutoId]                                                                  
	  INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                                                                   
	  INNER JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                                                                   
	  INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                                                   
	  INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]                                                                  
	  LEFT JOIN [dbo].[CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                                                                  
	  LEFT JOIN [dbo].[ShippingType] AS ST ON ST.[AutoId] = OM.[ShippingType]                                                                      
	  INNER JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[SalesPersonAutoId] WHERE OM.AutoId = @OrderAutoId                                                                  
                                                                      
	  SELECT * FROM (                                                                  
	  SELECT PM.[ProductId],PM.[ProductName],(                                                                  
	  select top 1 Location  from PackingDetails as pd where pd.ProductAutoId=PM.AutoId and                                                                  
	  pd.UnitType=OIM.UnitTypeAutoId) AS [ProductLocation],                                                                  
	  UM.[UnitType],OIM.[TotalPieces],                                                                  
	  OIM.[RequiredQty] FROM [dbo].[OrderItemMaster] AS OIM                                                                   
	  INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                                                               
	  INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitTypeAutoId        
	 WHERE [OrderAutoId] = @OrderAutoId                                                                   
	  ) AS T ORDER BY  ISNULL(ProductLocation,'') ASC,ProductId       
   END  

     END TRY                                                                  
  BEGIN CATCH                           
  Set @isException=1                                      
  Set @exceptionMessage='Oops! Something went wrong.Please try later.'                                                                                                                                                
  END CATCH                                                                                   
END 
