USE [psmnj.a1whm.com]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create FUNCTION  [dbo].[FN_DefaultEmail] 
(   
  @AutoId int   
)  
RETURNS varchar(100)  
AS  
BEGIN  
	DECLARE @Email varchar(100)
	set @Email=(SELECT top 1 Email FROM CustomerContactPerson WHERE CustomerAutoId=@AutoId AND ISDefault=1)
	RETURN @Email  
END 
--Alter table CustomerMaster Drop column Email
--Alter table CustomerMaster add Email As ([dbo].[FN_DefaultEmail](AutoId))