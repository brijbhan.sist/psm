USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Order_TaxTotal]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

alter table OrderMaster drop column TotalTax
drop function FN_Order_TaxTotal
go
CREATE FUNCTION  [dbo].[FN_Order_TaxTotal]
(
	@orderAutoId int
)
RETURNS decimal(18,2)
AS
BEGIN
	DECLARE @TaxTotal decimal(18,2)	=0.00
	DECLARE @TaxValue DECIMAL(18,2)=(SELECT TaxValue FROM OrderMaster WHERE AutoId=@orderAutoId)
	IF EXISTS(SELECT AutoId FROM OrderMaster WHERE AutoId=@orderAutoId AND IsTaxApply=1)
	BEGIN
		if EXISTS(SELECT AutoId FROM OrderMaster WHERE AutoId=@orderAutoId AND Status NOT IN (11))
		BEGIN
			SET @TaxTotal=(select ISNULL(CAST(SUM(NetPrice*@TaxValue/100) AS decimal(18,2)),0) from OrderItemMaster where Tax=1
			AND OrderAutoId=@orderAutoId)
		END
		ELSE
		BEGIN
			SET @TaxTotal=(select ISNULL(CAST(SUM(NetPrice*@TaxValue/100) AS decimal(18,2)),0) from Delivered_Order_Items where Tax=1
			AND OrderAutoId=@orderAutoId)
		END
	 END
RETURN @TaxTotal
END
GO
alter table OrderMaster add [TotalTax]  AS ([dbo].[FN_Order_TaxTotal]([AutoId]))
