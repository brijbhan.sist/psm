declare                                               
@Opcode INT =null,                                                  
@FromDate Date = '05/01/2021',                                                
@ToDate Date = '05/01/2021',                                                
@PageIndex int=1,                                                   
@PageSize  int=10
   Declare @ChildDbLocation varchar(50),@Query nvarchar(max); 
	select @ChildDbLocation=ChildDB_Name from CompanyDetails 


SET @Query='WITH COMPANYINFO(PaymentDate,PaymentMode,ReceivedAmount,OrderAutoId,CustomerAutoid,STime,ChequeNo) AS                                  
	(                                                      
	select CONVERT(date,PaymentDate) PaymentDate,
	(select pmm.PaymentMode from PAYMENTModeMaster as pmm where pmm.autoid=pd.PaymentMode) as  PaymentMode ,pod.ReceivedAmount,pod.OrderAutoId,
	pd.CustomerAutoid, format(pd.PaymentDate,''hh:mm tt'') as STime ,spm.ChequeNo                                 
	from CustomerPaymentDetails as pd                                   
	inner join PaymentOrderDetails as pod on pd.PaymentAutoid=pod.PaymentAutoid
	left Join SalesPaymentMaster as spm on spm.PaymentAutoId=pd.refPaymentId
	where   pd.Status=0  and Convert(date,PaymentDate)= '''+Convert(varchar(25),@FromDate)+''')';

	SET @Query=@Query+'  SELECT *                                                 
	into #ResultGroup1 FROM COMPANYINFO                                                
	PIVOT (                                                
	SUM(ReceivedAmount) FOR PaymentMode IN ([Cash],[Check],[Money Order],[Credit Card],[Store Credit],[Electronic Transfer],[NO Any Method])                                                 
	)  AS P                                                
	ORDER BY CONVERT(date,PaymentDate) desc ';

	SET @Query=@Query+' ;WITH COMPANYINFO2(PaymentDate,PaymentMode,ReceivedAmount,OrderAutoId,CustomerAutoid,STime,ChequeNo) AS                                  
	(                                             
	select CONVERT(date,PaymentDate) PaymentDate,
	(select pmm.PaymentMode from ['+@ChildDbLocation+'].[dbo].PAYMENTModeMaster as pmm where pmm.autoid=pd.PaymentMode) as  PaymentMode ,pod.ReceivedAmount,pod.OrderAutoId,
	pd.CustomerAutoid, format(pd.PaymentDate,''hh:mm tt'') as STime ,spm.ChequeNo                                 
	from ['+@ChildDbLocation+'].[dbo].CustomerPaymentDetails as pd                                   
	inner join ['+@ChildDbLocation+'].[dbo].PaymentOrderDetails as pod on pd.PaymentAutoid=pod.PaymentAutoid
	left Join ['+@ChildDbLocation+'].[dbo].SalesPaymentMaster as spm on spm.PaymentAutoId=pd.refPaymentId 
	where   pd.Status=0  and Convert(date,PaymentDate)= '''+Convert(varchar(25),@FromDate)+''') ';
	
	SET @Query=@Query+' SELECT *                                                 
	into #ResultGroup2 FROM COMPANYINFO2                                                
	PIVOT (                                                
	SUM(ReceivedAmount) FOR PaymentMode IN ([Cash],[Check],[Money Order],[Credit Card],[Store Credit],[Electronic Transfer],[NO Any Method])                                                 
	)  AS P                                                
	ORDER BY CONVERT(date,PaymentDate) desc ';

	SET @Query=@Query+' select PaymentDate,                                      
	CustomerAutoid, OrderAutoId, isnull(cash,0) as cash,isnull([Check],0) as [Check],isnull([Money Order],0) as [Money Order] ,
	isnull([Electronic Transfer],0) as [Electronic Transfer],isnull([Credit Card],0) as [Credit Card],isnull([Store Credit],0) as [Store Credit],                   
	(select isnull(sum(SortAmount),0) from CustomerPaymentDetails as spd where  CONVERT(date,spd.PaymentDate) =convert(date, t.PaymentDate)) as SortAmount                         
	                          
	into #ResultGroup from #ResultGroup1 as t   
   
	union  all 
	select PaymentDate,                                      
	CustomerAutoid, OrderAutoId, isnull(cash,0) as cash,isnull([Check],0) as [Check],isnull([Money Order],0) as [Money Order] ,
	isnull([Electronic Transfer],0) as [Electronic Transfer],0 as [Credit Card],isnull([Store Credit],0) as [Store Credit],                  
	(select isnull(sum(SortAmount),0) from ['+@ChildDbLocation+'].[dbo].CustomerPaymentDetails as spd where  CONVERT(date,spd.PaymentDate) =convert(date, t.PaymentDate)) as SortAmount                                                      
	from #ResultGroup2 as t ';
	  
	SET @Query=@Query+' select FORMAT(PaymentDate,''MM/dd/yyyy'') as PaymentDate,                                      
	isnull((select isnull(sum([NewCreditAmount]),0) from CustomerPaymentDetails as p where p.Status=0 and p.PaymentMode!=5 and CONVERT(date, p.PaymentDate) =  CONVERT(date, t.PaymentDate)),0) as [CreditAmount],                                      
	count(distinct CustomerAutoid) as TotalNoOfCustomer, COUNT(OrderAutoId) as noOfOrder, sum(isnull(cash,0))as TotalCash,sum(isnull([Check],0))                                      
	as TotalCheck,sum(isnull([Money Order],0))as TotalMoneyOrder,sum(isnull([Electronic Transfer],0))as TotalElectronicTransfer,                                                 
	sum(isnull([Credit Card],0))as TotalCreditCard,sum(isnull([Store Credit],0))as TotalStoreCredit,sum(isnull(cash,0) + (isnull([Check],0)) +                       
	isnull([Money Order],0) + isnull([Credit Card],0) + isnull([Store Credit],0)+isnull([Electronic Transfer],0)) as TotalPaid,                   
	isnull(sum(SortAmount),0) as Short,                         
	isnull((select sum(isnull(ExpenseAmount,0)) as d from ExpenseMaster where convert(date, ExpenseDate) = convert(date, PaymentDate)),0)  as Expense                             
	from #ResultGroup as t
	group by CONVERT(date,PaymentDate)                                  
	order by Convert(date,PaymentDate) ';
                                                                                              
                                                
	SET @Query=@Query+'	select cm.CustomerName,om.OrderNo,sum(ISNULL(R1.Cash,0))as Cash,sum(isnull([Check],0)) as tCheck,sum(ISNULL(R1.[Money Order],0))as MoneyOrder,                                              
	sum(ISNULL(R1.[Credit Card],0))as CreditCard,sum(isnull(R1.[Store Credit],0))as StoreCredit,sum(isnull(R1.[Electronic Transfer],0))as ElectronicTransfer,                                        
	sum(ISNULL(R1.Cash,0) + isnull([Check],0) + ISNULL(R1.[Money Order],0) + ISNULL(R1.[Credit Card],0)                                        
	+ isnull(R1.[Store Credit],0)+isnull(R1.[Electronic Transfer],0)) as TotalAmount,STime,ChequeNo                                               
	into #ResultGroup42 from #ResultGroup1 R1                                                
	inner join CustomerMaster cm on cm.AutoId = R1.CustomerAutoid                                                
	inner join OrderMaster om on om.AutoId = R1.OrderAutoId                                            
	group by cm.CustomerName,om.OrderNo ,STime,ChequeNo                                                 
	                                               
	union all 
  
	select cm.CustomerName,om.OrderNo,sum(ISNULL(R2.Cash,0))as Cash,sum(isnull([Check],0)) as tCheck,sum(ISNULL(R2.[Money Order],0))as MoneyOrder,                                              
	sum(ISNULL(R2.[Credit Card],0))as CreditCard,sum(isnull(R2.[Store Credit],0))as StoreCredit,sum(isnull(R2.[Electronic Transfer],0))as ElectronicTransfer,                                        
	sum(ISNULL(R2.Cash,0) + isnull([Check],0) + ISNULL(R2.[Money Order],0) + ISNULL(R2.[Credit Card],0)                                        
	+ isnull(R2.[Store Credit],0)+isnull(R2.[Electronic Transfer],0)) as TotalAmount,STime,ChequeNo                                               
	from #ResultGroup2 R2                                                
	inner join ['+@ChildDbLocation+'].[dbo].CustomerMaster cm on cm.AutoId = R2.CustomerAutoid                                                
	inner join ['+@ChildDbLocation+'].[dbo].OrderMaster om on om.AutoId = R2.OrderAutoId
	group by cm.CustomerName,om.OrderNo ,STime,ChequeNo'
	
	SET @Query=@Query+' select * from #ResultGroup42
	ORDER  By stime, orderno DESC ';
	--End bank Order details

	--Start Cash Details details
	SET @Query=@Query+' select CurrencyName,cm.CurrencyValue,sum(NoOfValue) as PNoOfValue,0 ENoOfValue ,SUM(TotalAmount) as PTotalAmount,0 as ETotalAmount                                            
  into #currenycollect                                          
  from Paymentcurrencydetails                                              
  inner join CurrencyMaster cm on cm.autoid = currencyautoid                                               
  where PaymentAutoId in (select PaymentAutoId from CustomerPaymentDetails as pd where pd.Status=0 and CONVERT(date,PaymentDate) = '''+Convert(varchar(25),@FromDate)+''')                                              
  group by currencyname,CurrencyValue
  
  Union All 

  select CurrencyName,cm.CurrencyValue,sum(NoOfValue) as PNoOfValue,0 ENoOfValue ,SUM(TotalAmount) as PTotalAmount,0 as ETotalAmount                                                                                      
  from ['+@ChildDbLocation+'].[dbo].Paymentcurrencydetails                                              
  inner join ['+@ChildDbLocation+'].[dbo].CurrencyMaster cm on cm.autoid = currencyautoid                                               
  where PaymentAutoId in (select PaymentAutoId from ['+@ChildDbLocation+'].[dbo].CustomerPaymentDetails as pd where pd.Status=0 and CONVERT(date,PaymentDate) = '''+Convert(varchar(25),@FromDate)+''')                                              
  group by currencyname,CurrencyValue'; 
                                           
  SET @Query=@Query+' select CurrencyName,cm.CurrencyValue, 0 as PNoOfValue,(sum(ec.TotalCount)) as ENoOfValue,0 as PTotalAmount,(SUM(TotalAmount)) as ETotalAmount                                           
   into #currenyExpense from ExpensiveCurrency   as ec                                           
   inner join CurrencyMaster cm on cm.autoid = currencyautoid                                               
   where ec.ExpenseAutoId in (select emp3.AutoId from ExpenseMaster as emp3 where CONVERT(date,ExpenseDate) = '''+Convert(varchar(25),@FromDate)+''')                                              
   group by currencyname,cm.CurrencyValue';                                
                                              
                                          
   SET @Query=@Query+' select CurrencyName,SUM(ISNULL(PNoOfValue,0)-ISNULL(ENoOfValue,0)) as NoOfValue ,SUM(ISNULL(PTotalAmount,0)-ISNULL(ETotalAmount,0)) as TotalAmount                          
  from (                                          
  select * from #currenycollect                                          
  union                                            
  select * from #currenyExpense                                          
  ) as t                                     
  group by CurrencyName,CurrencyValue                                     
  having SUM(ISNULL(PNoOfValue,0)-ISNULL(ENoOfValue,0))>0                                         
  order by CurrencyValue desc , CurrencyName'; 

  --End Cash Details details
  
  --End Paymnet Details details
  Set @Query=@Query+' select PaymentId,FORMAT(PaymentDate,''MM/dd/yyyy'') as PaymentDate, pmm.PaymentMode,
ReceivedAmount,SortAmount,(ISNULL(em.FirstName,'''') + '' '' +ISNULL(em.LastName,'''')) ReceivedBy,
(ISNULL(em2.FirstName,'''') + '' '' +ISNULL(em2.LastName,'''')) as SettlementBy,cm.CustomerId,
cm.CustomerName from CustomerPaymentDetails cpd                           
  inner join EmployeeMaster em on em.AutoId = cpd.ReceivedBy                                    
  inner join EmployeeMaster em2 on em2.AutoId = cpd.EmpAutoId                             
  inner join CustomerMaster cm on cm.AutoId = cpd.CustomerAutoId                    
  left join PAYMENTModeMaster as pmm on cpd.PaymentMode=pmm.AutoID                                     
  where CONVERT(date,PaymentDate) = '''+Convert(varchar(25),@FromDate)+''' and SortAmount > 0
  
  Union All

  select PaymentId,FORMAT(PaymentDate,''MM/dd/yyyy'') as PaymentDate, pmm.PaymentMode,ReceivedAmount,
  SortAmount, (ISNULL(em.FirstName,'''') + '' '' +ISNULL(em.LastName,'''')) ReceivedBy,                          
  (ISNULL(em2.FirstName,'''') + '' '' +ISNULL(em2.LastName,'''')) as SettlementBy,cm.CustomerId,
  cm.CustomerName from ['+@ChildDbLocation+'].[dbo].CustomerPaymentDetails cpd                           
  inner join ['+@ChildDbLocation+'].[dbo].EmployeeMaster em on em.AutoId = cpd.ReceivedBy                                    
  inner join ['+@ChildDbLocation+'].[dbo].EmployeeMaster em2 on em2.AutoId = cpd.EmpAutoId                             
  inner join ['+@ChildDbLocation+'].[dbo].CustomerMaster cm on cm.AutoId = cpd.CustomerAutoId                    
  left join ['+@ChildDbLocation+'].[dbo].PAYMENTModeMaster as pmm on cpd.PaymentMode=pmm.AutoID                                     
  where CONVERT(date,PaymentDate) = '''+Convert(varchar(25),@FromDate)+''' and SortAmount > 0
  '   
  --End Paymnet Details details
  --Start Settle Payment Details details
  Set @Query=@Query+'select cpd.PaymentId as SettlementId,cm.CustomerName, Format(spm.ChequeDate, ''MM/dd/yyyy'') as ChequeDate,spm.ChequeNo,      
spm.ReceivedAmount,spm.CancelRemarks,Format(cpd.PaymentDate,''MM/dd/yyyy'') as SettlementDate,      
format(spm.CancelDate,''MM/dd/yyyy'') as CancelDate,(ISNULL(em.FirstName,'''') + '' '' + ISNULL(LastName,'''')) as CancelBy,      
        
    stuff((   select '','' + OM.OrderNo      
        from PaymentOrderDetails POD      
  INNER JOIN OrderMaster as OM on POD.OrderAutoId=OM.AutoId      
        where POD.PaymentAutoId = cpd.PaymentAutoId       
        for xml path('''')      
    ),1,1,'''') as OrderNo      
      
 from SalesPaymentMaster spm                                 
 left join CustomerMaster cm on cm.AutoId = spm.CustomerAutoId                              
 left join EmployeeMaster em on em.AutoId = spm.cancelBy         
 left join CustomerPaymentDetails as cpd on spm.PaymentAutoId=cpd.refPaymentId                             
 where spm.PaymentMode = 2 and CONVERT(date,spm.CancelDate) = Convert(date,'''+Convert(varchar(25),@FromDate)+''' ) and spm.Status=5           

 Union All

 select cpd.PaymentId as SettlementId,cm.CustomerName, Format(spm.ChequeDate, ''MM/dd/yyyy'') as ChequeDate,spm.ChequeNo,      
spm.ReceivedAmount,spm.CancelRemarks,Format(cpd.PaymentDate,''MM/dd/yyyy'') as SettlementDate,      
format(spm.CancelDate,''MM/dd/yyyy'') as CancelDate,(ISNULL(em.FirstName,'''') + '' '' + ISNULL(LastName,'''')) as CancelBy,      
        
    stuff((   select '','' + OM.OrderNo      
        from ['+@ChildDbLocation+'].[dbo].PaymentOrderDetails POD      
  INNER JOIN ['+@ChildDbLocation+'].[dbo].OrderMaster as OM on POD.OrderAutoId=OM.AutoId      
        where POD.PaymentAutoId = cpd.PaymentAutoId       
        for xml path('''')      
    ),1,1,'''') as OrderNo      
      
 from ['+@ChildDbLocation+'].[dbo].SalesPaymentMaster spm                                 
 left join ['+@ChildDbLocation+'].[dbo].CustomerMaster cm on cm.AutoId = spm.CustomerAutoId                              
 left join ['+@ChildDbLocation+'].[dbo].EmployeeMaster em on em.AutoId = spm.cancelBy         
 left join ['+@ChildDbLocation+'].[dbo].CustomerPaymentDetails as cpd on spm.PaymentAutoId=cpd.refPaymentId                             
 where spm.PaymentMode = 2 and CONVERT(date,spm.CancelDate) = Convert(date,'''+Convert(varchar(25),@FromDate)+''' ) and spm.Status=5           

' 
--END Settle Payment Details details
 
--Start Generate Store Credit Details				   
Set @Query=@Query+' Select ISNULL([NewCreditAmount],0) as CreditAmount,PMM.PaymentMode,CPD.PaymentId,Format(CPD.PaymentDate,''MM/dd/yyyy'') as PaymentDate,                        
  ISNULL(EMp.FirstName,'''')+'' ''+ISNULL(EMp.LastName,'''') as ReceivedBY,CM.CustomerId,CM.CustomerName ,EM.FirstName+'' ''+EM.LastName as SettleBy                                
  from CustomerPaymentDetails as CPD                                  
  Inner join PAYMENTModeMaster AS PMM on CPD.PaymentMode=PMM.AutoID                         
  INNER Join EmployeeMaster AS EMp on CPD.ReceivedBy=EMp.AutoId                        
  Inner join CustomerMaster AS CM on CPD.CustomerAutoId=CM.AutoId                                  
  INNER JOIN EmployeeMaster AS EM on CPD.EmpAutoId=EM.AutoId                          
  where 
  cpd.Status=0 and
  CPD.PaymentMode!=5 and  CONVERT(date, PaymentDate) = '''+Convert(varchar(25),@FromDate)+''' and CPD.[NewCreditAmount]!=0                                  
 
 Union All

 Select ISNULL([NewCreditAmount],0) as CreditAmount,PMM.PaymentMode,CPD.PaymentId,Format(CPD.PaymentDate,''MM/dd/yyyy'') as PaymentDate,                        
  ISNULL(EMp.FirstName,'''')+'' ''+ISNULL(EMp.LastName,'''') as ReceivedBY,CM.CustomerId,CM.CustomerName ,EM.FirstName+'' ''+EM.LastName as SettleBy                                
  from ['+@ChildDbLocation+'].[dbo].CustomerPaymentDetails as CPD                                  
  Inner join ['+@ChildDbLocation+'].[dbo].PAYMENTModeMaster AS PMM on CPD.PaymentMode=PMM.AutoID                         
  INNER Join ['+@ChildDbLocation+'].[dbo].EmployeeMaster AS EMp on CPD.ReceivedBy=EMp.AutoId                        
  Inner join ['+@ChildDbLocation+'].[dbo].CustomerMaster AS CM on CPD.CustomerAutoId=CM.AutoId                                  
  INNER JOIN ['+@ChildDbLocation+'].[dbo].EmployeeMaster AS EM on CPD.EmpAutoId=EM.AutoId                          
  where 
  cpd.Status=0 and
  CPD.PaymentMode!=5 and  CONVERT(date, PaymentDate) = '''+Convert(varchar(25),@FromDate)+''' and CPD.[NewCreditAmount]!=0                                  
 
' 
--Start Generate Store Credit Details
Set @Query=@Query+' Select ISNUll(pmm.PaymentMode,'''') as PaymentMode,ExpenseDescription,ExpenseAmount,ISNULL(EMP.FirstName,'''')+'' ''+ISNULL(EMP.LastName,'''') as ExpenseBy
    from ExpenseMaster em                  
	Inner JOIN PAYMENTModeMaster as pmm on em.PaymentAutoId=pmm.AutoID              
	Inner join EmployeeMaster as EMP on em.CreateBy=EMP.AutoId 
	Where em.ExpenseDate='''+Convert(varchar(25),@FromDate)+'''
	'

	select @Query
	EXEC sp_executesql @Query;