select cm.CustomerId,cm.CustomerName,SUM(do.payableAmount) as
payableAmount,SUM(do.AmtDue) as AmtDue,SUM(do.AmtPaid) as 
AmtPaid into #result from OrderMaster as om
inner join DeliveredOrders as do on do.OrderAutoId=om.AutoId
inner join CustomerMaster as cm on om.CustomerAutoId=cm.AutoId
where om.Status=11
and om.AmtDue>0
group by cm.CustomerId,cm.CustomerName

select * from #result where AmtDue>0
drop table #result

