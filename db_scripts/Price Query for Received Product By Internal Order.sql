select PD.ProductAutoId,pd.UnitType,
 CAST( pd.qty*
(select cast(UnitPrice as decimal(10,2))/cast(pdin.qty as decimal(10,2)) from PackingDetails as pdin where pdin.ProductAutoId=pd.ProductAutoId and pdin.UnitType=t.UnitAutoId)
 as decimal(10,2)) as NewCastPrice
into #Result1 from PackingDetails as pd
inner join (
select pm.AutoId AS ProductAutoId,dim.UnitAutoId,UnitPrice,pd1.Qty as qtyReq
from [psmnj.a1whm.com].[dbo].[Delivered_Order_Items] as dim      
inner join  [psmnj.a1whm.com].[dbo].[Productmaster] as njpm on njpm.AutoId=dim.ProductAutoId              
inner join  [dbo].[Productmaster] as pm on pm.ProductId=njpm.ProductId       
inner join [psmnj.a1whm.com].[dbo].[PackingDetails] as pd on pd.ProductAutoId= njpm.AutoId and pd.UnitType=dim.UnitAutoId 
inner join [dbo].[PackingDetails] as pd1 on pd1.ProductAutoId= pm.AutoId and pd1.UnitType=dim.UnitAutoId  
where OrderAutoId=38550 and UnitPrice>0  and pd1.CostPrice!=UnitPrice
)  AS T ON T.ProductAutoId=PD.ProductAutoId
  
  select distinct temp.ProductAutoId
  ,temp.unitType,CostPrice,NewCastPrice,
  MinPrice,
  CAST((case when NewCastPrice > CostPrice then NewCastPrice+(NewCastPrice*10/100) else MinPrice end ) as decimal(10,2)) as NewMinPrice,
  WHminPrice,
  CAST((case when NewCastPrice > CostPrice then NewCastPrice+(NewCastPrice*15/100) else WHminPrice end )as decimal(10,2)) as NewWHminPrice,
  Price,
  CAST((case when NewCastPrice > CostPrice then NewCastPrice+(NewCastPrice*20/100) else Price end ) as decimal(10,2)) as NewPrice
   from #Result1 as temp 
  inner join PackingDetails as pd on temp.ProductAutoId=pd.ProductAutoId 
  and temp.unitType=pd.unitType 
 
 
  drop table #Result1

