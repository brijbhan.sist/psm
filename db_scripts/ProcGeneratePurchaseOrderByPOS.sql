Alter procedure ProcGeneratePurchaseOrderByPOS
@CustomerAutoId int,
@OrderAutoId int,
@Type int
AS
BEGIN
	Declare @VendorAutoId INT,@LocationAutoId int,@LocationName varchar(20),@query nvarchar(MAX),@PONo varchar(20),@EmpId int=1,
	@POAutoId int,@PONUMBER nvarchar(15),@OrderNo varchar(15),@CurrentLocationId int,@Location varchar(50)
	SET @LocationAutoId=(Select LocationAutoId from CustomerMaster where AutoId=@CustomerAutoId)
	SET @LocationName=(Select Location From LocationMaster where AutoId=@LocationAutoId)
	SET @OrderNo=(Select orderNo from OrderMaster where AutoId=@OrderAutoId)

	 Select @Location=Replace(DB_Name(),'.a1whm.com','')            
	 Select @CurrentLocationId=AutoId from LocationMaster where Location=@Location  

	SET @query='Select @VendorAutoId=AutoId from ['+Convert(varchar(50),@LocationName)+'.a1whm.com].[dbo].VendorMaster 
	where LocationAutoId='+Convert(varchar(15),@CurrentLocationId)+''
	Exec sp_executesql @query,N'@VendorAutoId nvarchar(max) OUTPUT',@VendorAutoId OUTPUT 

	SET @query='SELECT @PONo=['+Convert(varchar(50),@LocationName)+'.a1whm.com].[dbo].SequenceCodeGenerator(''PurchaseOrder'')'
	Exec sp_executesql @query,N'@PONo nvarchar(max) OUTPUT',@PONo=@PONUMBER OUTPUT

 
	
	If(@CustomerAutoId is not null AND @Type=1)        
	begin 
	SET @query='insert into ['+Convert(varchar(50),@LocationName)+'.a1whm.com].[dbo].[PurchaseOrderMaster]([PONo],[PODate],[VenderAutoId],
		[Status],[NoofItems],[PORemarks],[CreatedBy],[CreatedOn],[StockStatus],VendorType,OrderNo,TotalOrderAmount,TotalNoOfItem,DeliveryDate)
		SELECT '''+Convert(varchar(25),@PONUMBER)+''',GETDATE(),'+Convert(varchar(25),@VendorAutoId)+',6,(Select Count(AutoId) 
		FROM OrderItemMaster where OrderAutoId='+Convert(varchar(20),@OrderAutoId)+'),OM.OrderRemarks,'+Convert(varchar(25),@EmpId)+',
		GETDATE(),''Pending'',1,'''+@OrderNo+''',(Select PayableAmount from ordermaster where AutoId='+Convert(varchar(20),@OrderAutoId)+'),
		(Select Count(AutoId)FROM OrderItemMaster where OrderAutoId='+Convert(varchar(20),@OrderAutoId)+'),getdate()
		FROM OrderMaster as OM where AutoId='+Convert(varchar(20),@OrderAutoId)+'  
   
		SET @POAutoId = (SELECT SCOPE_IDENTITY())         
                
		Insert into ['+Convert(varchar(50),@LocationName)+'.a1whm.com].[dbo].[PurchaseProductsMaster]
		([ProductAutoId],[Unit],[QtyPerUnit],[Qty],[TotalPieces],[POAutoId])          
		select pm1.AutoId,UnitTypeAutoId,QtyPerUnit,RequiredQty,TotalPieces,@POAutoId
		FROM OrderItemMaster oim
		inner join ProductMaster as pm on oim.ProductAutoId=pm.AutoId
		inner join ['+Convert(varchar(50),@LocationName)+'.a1whm.com].[dbo].ProductMaster as pm1 on pm1.ProductId=pm.productId
		Where OrderAutoId='+Convert(varchar(20),@OrderAutoId)+'  

		UPDATE ['+Convert(varchar(50),@LocationName)+'.a1whm.com].[dbo].SequenceCodeGeneratorMaster 
		SET currentSequence = currentSequence + 1 WHERE SequenceCode=''PurchaseOrder'' '

	    EXEC sp_executesql @query ,N'@POAutoId nvarchar(max) OUTPUT',@POAutoId OUTPUT

		Update OrderMaster SET referenceOrderNumber=@PONUMBER WHERE AutoId=@OrderAutoId
	END
	ELSE
	BEGIN
	    SET @query='Select @POAutoId=AutoId from ['+Convert(varchar(50),@LocationName)+'.a1whm.com].[dbo].PurchaseOrderMaster 
	    where VenderAutoId='+Convert(varchar(15),@VendorAutoId)+' AND OrderNo='''+@OrderNo+''''
	    Exec sp_executesql @query,N'@POAutoId nvarchar(max) OUTPUT',@POAutoId OUTPUT 

	    SET @query='Update ['+Convert(varchar(50),@LocationName)+'.a1whm.com].[dbo].[PurchaseOrderMaster] 
		set VenderAutoId='+Convert(varchar(25),@VendorAutoId)+' ,
		PORemarks=(Select OrderRemarks FROM OrderMaster where AutoId='+Convert(varchar(20),@OrderAutoId)+'),        
		NoofItems=(Select Count(AutoId) FROM OrderItemMaster where OrderAutoId='+Convert(varchar(20),@OrderAutoId)+') ,
		UpdatedBy='+Convert(varchar(25),@EmpId)+' ,UpdatedOn=GetDate(),TotalOrderAmount=(Select PayableAmount 
		from ordermaster where AutoId='+Convert(varchar(20),@OrderAutoId)+'),TotalNoOfItem=(Select Count(AutoId) 
		FROM OrderItemMaster where OrderAutoId='+Convert(varchar(20),@OrderAutoId)+'),DeliveryDate=GETDATE()
		Where AutoId='+Convert(varchar(25),@POAutoId)+'        
        
		Delete ['+Convert(varchar(50),@LocationName)+'.a1whm.com].[dbo].[PurchaseProductsMaster] where POAutoId='+Convert(varchar(25),@POAutoId)+'          
        
		Insert into ['+Convert(varchar(50),@LocationName)+'.a1whm.com].[dbo].[PurchaseProductsMaster]([ProductAutoId],[Unit],[QtyPerUnit],[Qty],[TotalPieces],[POAutoId])          
		select pm1.AutoId,UnitTypeAutoId,QtyPerUnit,RequiredQty,TotalPieces,'+Convert(varchar(25),@POAutoId)+'  
		FROM OrderItemMaster as oim
		inner join ProductMaster as pm on oim.ProductAutoId=pm.AutoId
		inner join ['+Convert(varchar(50),@LocationName)+'.a1whm.com].[dbo].ProductMaster as pm1 on pm1.ProductId=pm.productId Where OrderAutoId='+Convert(varchar(20),@OrderAutoId)+'' 

	    EXEC sp_executesql @query 
	END
	
END