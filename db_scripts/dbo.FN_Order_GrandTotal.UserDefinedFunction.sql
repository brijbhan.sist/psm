USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Order_GrandTotal]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
alter table OrderMaster drop column GrandTotal
drop function FN_Order_GrandTotal
go
CREATE FUNCTION  [dbo].[FN_Order_GrandTotal]  
(  
  @orderAutoId int  
)  
RETURNS decimal(18,2)  
AS  
BEGIN  
 DECLARE @GrandTotal decimal(18,2)   
  SET @GrandTotal=(SELECT (round(((([TotalAmount]-[OverallDiscAmt])+[ShippingCharges])+[TotalTax])+[MLTax]+
  ISNULL(Weigth_OZTaxAmount,0),(0))) FROM OrderMaster WHERE AutoId=@orderAutoId)   
 RETURN @GrandTotal  
END  
GO
alter table OrderMaster add [GrandTotal]  AS ([dbo].[FN_Order_GrandTotal]([autoid]))