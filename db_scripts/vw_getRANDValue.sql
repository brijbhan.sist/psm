DROP VIEW vw_getRANDValue
go
CREATE VIEW vw_getRANDValue
AS
SELECT left((
 (1000000000 + (CONVERT(INT, CRYPT_GEN_RANDOM(3)) % 1000000))),10) as RandomNumber