--Alter table Ordermaster alter column DrvRemarks varchar(500)
ALTER PROCEDURE [dbo].[ProcDriverOrderMaster]                                                                                                                                                    
@Opcode INT=NULL,                                                                                                                                                    
@OrderAutoId INT=NULL,                                                                                                                                                                                                                                                                                                        
@CustomerAutoId INT=NULL,   
@DriverAutoId INT=NULL,
@AsgnDate DATE=NULL, 
@OrderNo VARCHAR(15)=NULL,
@Fromdate DATETIME=NULL,
@Todate DATETIME=NULL,
@OrderStatus INT=NULL,
@LoginEmpType INT=NULL,
@SalesPersonAutoId INT=NULL,
@LogRemark varchar(200) = NULL,
@EmpAutoId INT=NULL,
@Delivered VARCHAR(20)=NULL,  
@Comment VARCHAR(max)=NULL,   
@Remarks VARCHAR(500)=NULL,
@Barcode VARCHAR(50)=NULL,
@CommentType INT=NULL,  
@DocumentDetails xml=NULL,
@PageIndex INT = 1,    
@PageSize INT = 10,                                    
@RecordCount INT =null, 
@isException bit out,                                           
@exceptionMessage varchar(max) out                                                                                      
AS                                                                                                                                                    
BEGIN                                                                                                                               
  BEGIN TRY                                                                                                                                            
   Set @isException=0                                                                                                                           
   Set @exceptionMessage='Success'                                                                                                         
   declare @customerType varchar(50)                                                                                                                                                 
	
   IF @Opcode=415                                                                                        
   BEGIN           
		SELECT ROW_NUMBER() OVER(order by OrderDate desc) AS RowNumber, * INTO #Results from                      
		(   
		SELECT OM.CustomerAutoId,OM.[AutoId],OM.[OrderNo],CONVERT(VARCHAR(20),OM.[OrderDate],101) As OrderDate,                                                                                                                                             
		CM.[CustomerName],PayableAmount as [GrandTotal],OM.[Status],PackedBoxes,(emp.FirstName + ' '+ emp.LastName) as SalesPerson,                                                 
		SM.[StatusType] AS StatusType,[Stoppage],RootName,DriverRemarks FROM [dbo].[OrderMaster] AS OM                                                                                                                                                    
		INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                                                                      
		INNER JOIN [dbo].[EmployeeMaster] AS emp ON emp.AutoId = OM.SalesPersonAutoId                                                                                                                                                
		INNER JOIN [dbo].[StatusMaster] AS SM ON SM.[AutoId] = OM.[Status]                                                                                                        
		AND SM.[Category] = 'OrderMaster'                          
		WHERE OM.[Driver] = @DriverAutoId                                                                                                                                                   
		and CONVERT(DATE,OM.[AssignDate])=(CASE WHEN @AsgnDate IS NOT NULL OR @AsgnDate != '' THEN @AsgnDate 
		ELSE CONVERT(DATE,AssignDate) END)   
		AND (@OrderNo = '' OR @OrderNo IS NULL OR OM.[OrderNo] LIKE '%' + @OrderNo + '%')                                                                                                                      
		and (@Fromdate IS NULL OR @Fromdate='' OR @Todate IS NULL                                       
		OR @Todate='' OR OM.OrderDate BETWEEN @Fromdate AND dateadd(dd,1,@Todate))                                                                                                                                                     
		and ((@OrderStatus is null or @OrderStatus=0 or OM.Status=@OrderStatus) AND OM.Status IN (4,5,6) )                                                                     
		and(@SalesPersonAutoId = 0 or CM.[SalesPersonAutoId]=@SalesPersonAutoId)                                                                                                                                                    
		and (@CustomerAutoId is null or @CustomerAutoId=0 or OM.CustomerAutoId=@CustomerAutoId)                                                                                              
		) as t order by OrderDate desc                           
                 
		SELECT * FROM #Results                                    
		WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)) 

		SELECT COUNT(RowNumber) AS RecordCount,case when @PageSize=0 then COUNT(OrderDate) else @PageSize end AS PageSize,@PageIndex as PageIndex FROM #Results           
                                    
                                                                                                            
		SELECT [Root] FROM [dbo].[DriverRootLog] WHERE [DrvAutoId] = @DriverAutoId AND                                                                                                                                                     
		CONVERT(DATE,[AssignDate]) = (CASE WHEN @AsgnDate IS NOT NULL OR @AsgnDate != ''                                                                                                                     
		THEN @AsgnDate ELSE CONVERT(DATE,getdate()) END)                                                                                                                                                     
		END                                                                         
    ELSE IF @Opcode=416                                                                                        
	BEGIN                                                                                        
		UPDATE [dbo].[OrderMaster] SET [Status] = 5 WHERE [AutoId] = @OrderAutoId                                       
		SET @LogRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=3),'[Status1]','Ready to ship')                                                                                                                                    
		SET @LogRemark = REPLACE(@LogRemark,'[Status2]','Shipped')                                                                                
		INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                                                                                                                                    
		VALUES(3,@EmpAutoId,getdate(),@LogRemark,@OrderAutoId) 
		SELECT @CustomerAutoId=CustomerAutoId FROM [dbo].[OrderMaster]   WHERE [AutoId] = @OrderAutoId                
		if((select CustomerType from CustomerMaster where AutoId=@CustomerAutoId)=3)                      
		begin                      
			exec [dbo].[ProcUpdatePOStatus_All]                      
			@CustomerId=@CustomerAutoId,                      
			@OrderAutoId=@OrderAutoId,                      
			@Status=5                          
		end                                           
	END
	ELSE IF @Opcode=107                                                                                                                         
		BEGIN                                                               
		IF EXISTS(SELECT * FROM  OrderMaster WHERE AutoId=@OrderAutoId AND Status=11)                                                                                
		BEGIN                                                                
			Set @isException=1                                                                                                                                                    
			Set @exceptionMessage='Order has been closed now you can not update delivery status.'                                                                                     
		END                                                                                           
		ELSE                                                                                                                                 
		BEGIN                                                                                   
			UPDATE [dbo].[OrderMaster] SET                                             
			[Status] = (CASE WHEN @Delivered = 'no' THEN 7 ELSE 6 END),                                                                                                                                                    
			[DrvRemarks] = @Remarks,                                                              
			[DeliveryDate] = (CASE WHEN @Delivered = 'no' THEN DeliveryDate ELSE GETDATE() END) ,                                                  
			[DelDate] = (CASE WHEN @Delivered = 'no' THEN NULL ELSE GETDATE() END) ,                                                                                                                      
			CommentType=@CommentType,                                                                                                                    
			Comment=@Comment                                
			WHERE [AutoId] = @OrderAutoId                                                                                
                
			if 	@Delivered != 'no'
			BEGIN
				update dl SET DeliveredOrderTime=GETDATE() from DriverLog_OrdDetails as dl
				inner join OrderMaster as om on om.OrderNo=dl.orderNo where om.AutoId=@OrderAutoId
			END

			IF NOT EXISTS(select AutoId from tbl_DriverDeliveryDocuments where OrderAutoId=@OrderAutoId)
			BEGIN
			begin try
				INSERT INTO tbl_DriverDeliveryDocuments(OrderAutoId,fileName,fileType,filepath,PhysicalPath,Datetime)
				select @OrderAutoId,tr.td.value('FileName[1]','varchar(max)') as FileName,tr.td.value('FileType[1]','varchar(max)') as FileType,
				tr.td.value('FilePath[1]','varchar(max)') as Filepath,tr.td.value('PhysicalPath[1]','varchar(max)') as PhysicalPath,GETDATE()            
			    from  @DocumentDetails.nodes('/Xml') tr(td)
				end try
				begin catch
				       SET @isException=1
					   SET @exceptionMessage=ERROR_MESSAGE()
				end catch
			END
			ELSE
			BEGIN
			begin try
				    Delete from tbl_DriverDeliveryDocuments where  OrderAutoId=@OrderAutoId and fileType in ( select tr.td.value('FileType[1]','varchar(max)') as FileType from  @DocumentDetails.nodes('/Xml') tr(td) )

					INSERT INTO tbl_DriverDeliveryDocuments(OrderAutoId,fileName,fileType,filepath,PhysicalPath,Datetime)
					select @OrderAutoId,tr.td.value('FileName[1]','varchar(max)') as FileName,tr.td.value('FileType[1]','varchar(max)') as FileType,
					tr.td.value('FilePath[1]','varchar(max)') as Filepath,tr.td.value('PhysicalPath[1]','varchar(max)') as PhysicalPath,GETDATE()            
			        from  @DocumentDetails.nodes('/Xml') tr(td) 
					end try
				begin catch
				       SET @isException=1
					   SET @exceptionMessage=ERROR_MESSAGE()
				end catch
			END
			SET @LogRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=3),'[Status1]','Shipped')                                                                                     
			SET @LogRemark = REPLACE(@LogRemark,'[Status2]',(CASE WHEN @Delivered = 'no' THEN 'Undelivered' ELSE 'Delivered' END))                                                                    
			INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                                                                                                                                    
			VALUES(3,@EmpAutoId,getdate(),@LogRemark,@OrderAutoId)             
			SELECT @CustomerAutoId=CustomerAutoId FROM [dbo].[OrderMaster]                 
			WHERE [AutoId] = @OrderAutoId                
			declare @Sts int=(CASE WHEN @Delivered = 'no' THEN 7 ELSE 6 END)            
			if((select CustomerType from CustomerMaster where AutoId=@CustomerAutoId)=3)                      
			begin                      
				exec [dbo].[ProcUpdatePOStatus_All]                      
				@CustomerId=@CustomerAutoId,                      
				@OrderAutoId=@OrderAutoId,                      
				@Status = @Sts  
			end   
				Select @OrderAutoId as AutoId      
				Select 'Order has been delivered' as msg       
				EXEC [dbo].[ProcAutomaticSendEmail]  @Opcode=12,@CustomerAutoId=@CustomerAutoId, 
				@OrderAutoId=@OrderAutoId,@isException=0,@exceptionMessage=''

	END                                                                                                                             
   END 
	ELSE IF @Opcode=108  
	BEGIN
	insert into loadbarcodelog(EmpAutoId,OrderautoId,Barcode,DateTime)
	values(@EmpAutoId,@OrderAutoId,@Barcode,GETDATE())
	End
	ELSE IF @Opcode=109   
	BEGIN
	insert into unloadbarcodelog(EmpAutoId,OrderautoId,Barcode,DateTime)
	values(@EmpAutoId,@OrderAutoId,@Barcode,GETDATE())
	End
	Else If @Opcode=428                                                                                         
	Begin                                                                                   
		SELECT AutoId as OrderAutoId,CustomerAutoId,OrderNo,PackedBoxes,[DrvRemarks],CommentType,Comment,Status FROM OrderMaster WHERE AutoId=@OrderAutoId    
		select fileType,PhysicalPath from tbl_DriverDeliveryDocuments where OrderAutoId=@OrderAutoId
	End
	  ELSE IF @Opcode=407                                                                                                                                  
     BEGIN          
		SELECT * FROM (                                                                                                    
  
		SELECT  (
					SELECT [AutoId],[CustomerId] + ' - ' + [CustomerName] As Customer FROM [dbo].[CustomerMaster]        
					order by [CustomerName] ASC,[CustomerId] ASC  for json path    
				) CL,

				(
					SELECT AutoId,(FirstName +' '+ISNULL(LastName,'')) AS EmpName         
					FROM EmployeeMaster WHERE EmpType =2  order By (FirstName +' '+ISNULL(LastName,''))  for json path
				) as sL,
		
				( 
					SELECT [AutoId],[StatusType]  FROM [dbo].[StatusMaster] WHERE  [Category]='OrderMaster' 
					AND [AutoId] IN(4,5,6)          order by [StatusType]  ASC 
					for json path
				) AS Sts 

				)AS T 
		for json path
    
	END
	 ELSE IF @Opcode=408                                                                                                                                  
     BEGIN  
		SELECT  (
		SELECT [AutoId],[CustomerId] + ' - ' + [CustomerName] As Customer FROM [dbo].[CustomerMaster]  
		Where SalesPersonAutoId=@SalesPersonAutoId
		order by [CustomerName] ASC,[CustomerId] ASC  for json path    
		) CL
		for json path    
	END
  END TRY                                                                      
  BEGIN CATCH                               
  Set @isException=1                                          
  Set @exceptionMessage='Oops! Something went wrong.Please try later.'                                                                                                                                                    
  END CATCH                                                                                       
END 