
alter PROCEDURE [dbo].[procOtpReport]    
    
@Opcode INT =null,     
@DateFrom DATE=null,    
@DateTo Date=null,    
@PageIndex int=null,     
@PageSize  int=10,     
@isException bit out,      
@exceptionMessage varchar(max) out,      
@TaxType int=null      
AS    
BEGIN    
  BEGIN TRY    
    SET @exceptionMessage= 'Success'      
 SET @isException=0       
 IF @Opcode=101    
    BEGIN    
  select convert(varchar,om.OrderDate,101) as [InvoiceDate], om.OrderNo as [InvoiceNumber],bm.BrandName as [NameofCommonCarrier],     
  cm.CustomerName as [SellerName],ba.Address, ba.City, ba.Zipcode, st.StateName,    
  cm.OPTLicence as [OTPLicenseNumber],bm.BrandName as [BrandFamily],scm.SubcategoryName as [ProductDescription],     
  Sum(doi.QtyDel) as [QuantityofItemSold],    
  sum(doi.NetPrice) as [PurchasePrice],    
  om.TotalTax  as [TotalTaxCollected],    
  'Yes' as [TaxCollectedYesOrNo]    
  INTO #TEMP     
  from Delivered_Order_Items as doi    
    
  inner join OrderMaster as om on doi.OrderAutoId = om.AutoId and om.Status=11 and doi.Tax=1
  inner join ProductMaster as pm on doi.ProductAutoId = pm.AutoId    
  inner join CustomerMaster as cm on om.CustomerAutoId = cm.AutoId    
  inner join SubCategoryMaster as scm on pm.SubcategoryAutoId = scm.AutoId    
  inner join BrandMaster as bm on pm.BrandAutoId = bm.AutoId    
  inner join BillingAddress as ba on cm.DefaultBillAdd = ba.AutoId    
  inner join State as st on st.AutoId = ba.State    
  where om.TotalTax > 0 and
  (ISNULL(@DateFrom,'')='' or ISNULL(@DateTo,'')='' or convert(date,om.OrderDate) between @DateFrom and @DateTo )   
 and(@TaxType=0 or @TaxType is null or om.TaxType =@TaxType)   
  group by convert(varchar,om.OrderDate,101),om.OrderNo,bm.BrandName,cm.CustomerName,ba.Address, ba.City, ba.Zipcode, st.StateName,cm.OPTLicence,bm.BrandName,scm.SubcategoryName,om.TotalTax    
  order by om.OrderNo    
    
  SELECT  [InvoiceDate], [InvoiceNumber],    
  STUFF((SELECT DISTINCT  '/' + [NameofCommonCarrier] FROM #TEMP AS T2 WHERE T1. [InvoiceNumber]=T2.[InvoiceNumber] FOR XML PATH('')), 1, 1,'') [NameofCommonCarrier],     
  [SellerName],Address, City, Zipcode, StateName,    
  [OTPLicenseNumber],    
  STUFF((SELECT DISTINCT '/' + [BrandFamily] FROM #TEMP AS T2 WHERE T1. [InvoiceNumber]=T2.[InvoiceNumber] FOR XML PATH('')), 1, 1,'') [BrandFamily],    
  STUFF((SELECT DISTINCT '/' + [ProductDescription] FROM #TEMP AS T2 WHERE T1. [InvoiceNumber]=T2.[InvoiceNumber] FOR XML PATH('')), 1, 1,'') [ProductDescription],     
  [QuantityofItemSold], [PurchasePrice], [TotalTaxCollected], TaxCollectedYesOrNo    
  INTO #TEMP1 FROM #TEMP AS T1    
  SELECT ROW_NUMBER() over(order by InvoiceDate) as RowNumber,* into #Result from (  
  SELECT   [InvoiceDate], [InvoiceNumber],[NameofCommonCarrier], [SellerName],Address, City, Zipcode, StateName,isnull([OTPLicenseNumber],'') as [OTPLicenseNumber],[BrandFamily],    
  REPLACE([ProductDescription],'&AMP;','&')[ProductDescription],     
  sum([QuantityofItemSold]) as [QuantityofItemSold], sum([PurchasePrice]) as [PurchasePrice], [TotalTaxCollected], TaxCollectedYesOrNo    
   FROM #TEMP1 AS T1    
   group by     
   [InvoiceDate], [InvoiceNumber],[NameofCommonCarrier], [SellerName],Address, City, Zipcode, StateName,[OTPLicenseNumber],[BrandFamily],    
  REPLACE([ProductDescription],'&AMP;','&'),  [TotalTaxCollected], TaxCollectedYesOrNo    
  ) as t  
  
  SELECT * FROM #Result  
  WHERE @PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)   
  SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize=0 then COUNT(RowNumber) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Result  
 
  Select Sum([TotalTaxCollected])as Totaltax,sum([PurchasePrice])as TotalPrice,sum([QuantityofItemSold])as TotalSoldQty from #Result 
 DROP TABLE #TEMP,#TEMP1  
  
 Select FORMAT(GETDATE(),'MM/dd/yyyy hh:mm tt') as [PrintTime]
  END    

  IF @Opcode=102                       
 BEGIN           
      select AutoId as TID,TaxableType+' ['+ CONVERT(VARCHAR(10),Value)+']' as TT from [dbo].[TaxTypeMaster] where Status=1 and Value>0
	   order by TT ASC
	   for json path
 end    
  END TRY    
  BEGIN CATCH    
    SET @isException=1    
 SET @exceptionMessage=ERROR_MESSAGE()    
     
  END CATCH    
END  
GO
