create or alter PROCEDURE [dbo].[ProcUpcomingReport]                        
@Opcode INT=NULL,                    
@OrderAutoId int=null,
@PageIndex INT=1,                    
@PageSize INT=10,                    
@RecordCount INT=null,                    
@isException bit out,                    
@exceptionMessage varchar(max) out                            
AS                                    
BEGIN                       
BEGIN TRY                    
Set @isException=0                    
Set @exceptionMessage='Success'                    
  if(@Opcode=41)                    
	begin                  
		
		select om.AutoId as orderAutoid,OrderNo,FORMAT(OrderDate,'MM/dd/yyyy') as OrderDate,
		do.PayableAmount,
		(
			select sum(pod.ReceivedAmount) from CustomerPaymentDetails as pm
			inner join PaymentOrderDetails as pod on pod.PaymentAutoId=pm.PaymentAutoId and pod.OrderAutoId=om.AutoId and pm.Status=0 
		) as SettledAmt
		,(
			select sum(pm.ReceivedAmount) from CustomerPaymentDetails as pm where pm.PaymentAutoId in (
			select pod.PaymentAutoId from PaymentOrderDetails as pod where  pod.OrderAutoId=om.AutoId) and pm.Status=0 
		) as ReceivedAmt
		,(
			select sum(pm.SortAmount) from CustomerPaymentDetails as pm where pm.PaymentAutoId in (
			select pod.PaymentAutoId from PaymentOrderDetails as pod where  pod.OrderAutoId=om.AutoId) and pm.Status=0 
		) as SortAmt
		,FORMAT((
			select max(pm.PaymentDate) from CustomerPaymentDetails as pm where pm.PaymentAutoId in (
			select pod.PaymentAutoId from PaymentOrderDetails as pod where  pod.OrderAutoId=om.AutoId) and pm.Status=0 
		),'MM/dd/yyyy hh:mm tt') as settledDate,
		(
			select count(pm.PaymentDate) from CustomerPaymentDetails as pm where pm.PaymentAutoId in (
			select pod.PaymentAutoId from PaymentOrderDetails as pod where  pod.OrderAutoId=om.AutoId) and pm.Status=0 
		) as NoofPayment
		,ROW_NUMBER() over(order by orderAutoid) as RowNumber into #Result from OrderMaster as om
		inner join DeliveredOrders as do on om.AutoId=do.OrderAutoId
		inner join ShippingType as st on st.AutoId=om.ShippingType
		where st.MigrateStatus=1 and do.AmtDue=0 and Status=11 and CONVERT(date,OrderDate)>=CONVERT(date,'2020-07-16') and isnull(om.CreditAmount,0)=0
		and datediff(day,(
		select top 1 PaymentDate from CustomerPaymentDetails as pm
		inner join PaymentOrderDetails as pod on pod.PaymentAutoId=pm.PaymentAutoId and pod.OrderAutoId=om.AutoId and pm.Status=0 order by PaymentDate desc
		),getdate())>3
		and not exists(
		select pm.PaymentAutoId from CustomerPaymentDetails as pm
		inner join PaymentOrderDetails as pod on pod.PaymentAutoId=pm.PaymentAutoId and pod.OrderAutoId=om.AutoId and pm.Status=0
		where (select count(1) from (select distinct OrderAutoId from PaymentOrderDetails where PaymentAutoId=pm.PaymentAutoId) as t)>1  ---Multiple order
		)
		and not exists(
		select pm.PaymentAutoId from CustomerPaymentDetails as pm
		inner join PaymentOrderDetails as pod on pod.PaymentAutoId=pm.PaymentAutoId and pod.OrderAutoId=om.AutoId and pm.Status=0 and PaymentMode=5  --store credit
		)
		and not exists(
		select cpd.NewCreditAmount  from [dbo].[CustomerPaymentDetails] as cpd where  
		cpd.PaymentAutoId in (select od.PaymentAutoId from PaymentOrderDetails as od where od.OrderAutoId=om.AutoId)	
		and cpd.NewCreditAmount>0  
		)                  
	SELECT * FROM #Result                  
	WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))  --order by orderAutoid asc           
	SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize = 0 then COUNT(RowNumber) else @PageSize end AS PageSize, @PageIndex AS PageIndex,Format(GETDATE(),'MM/dd/yyyy hh:mm tt') as PrintDate FROM #Result                    
     
	end 
	IF @Opcode=11    
	BEGIN    
		EXEC [dbo].[Proc_Account_OrderMaster] 
		@opCode=211,
		@OrderAutoId=@OrderAutoId,
		@isException=0,                      
		@exceptionMessage='';
	end
          
END TRY                    
BEGIN CATCH                    
Set @isException=1                    
Set @exceptionMessage=ERROR_MESSAGE()                    
END CATCH                                 
END 
GO
