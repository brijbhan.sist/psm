USE [psmnj.a1whm.com]
GO

/****** Object:  UserDefinedTableType [dbo].[DT_dtbulkImageUrl]    Script Date: 07-16-2020 02:31:37 ******/
DROP TYPE [dbo].[DT_dtbulkImageUrl]
GO

/****** Object:  UserDefinedTableType [dbo].[DT_dtbulkImageUrl]    Script Date: 07-16-2020 02:31:37 ******/
CREATE TYPE [dbo].[DT_dtbulkImageUrl] AS TABLE(
	[URL] [varchar](max) NULL,
	[Thumb100] [varchar](max) NULL,
	[Thumb400] [varchar](max) NULL,
	[Status] [int] NULL
)
GO


