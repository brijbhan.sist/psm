USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedTableType [dbo].[DT_APP_OrderItemList]    Script Date: 11/20/2019 5:05:52 AM ******/
CREATE TYPE [dbo].[DT_APP_OrderItemList] AS TABLE(
	[ProductAutoId] [int] NULL,
	[UnitAutoId] [int] NULL,
	[QtyPerUnit] [int] NULL,
	[RequiredQty] [int] NULL,
	[Barcode] [varchar](50) NULL,
	[QtyShip] [int] NULL,
	[TotalPieces] [int] NULL,
	[UnitPrice] [decimal](8, 2) NULL,
	[SRP] [decimal](8, 2) NULL,
	[GP] [decimal](8, 2) NULL,
	[Tax] [int] NULL,
	[IsExchange] [int] NULL,
	[IsFree] [int] NULL,
	[NetPrice] [decimal](8, 2) NULL
)
GO
