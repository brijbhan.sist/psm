
ALTER PROCEDURE  [dbo].[ProcProductExchange_Report]               
@Opcode INT=NULL,              
@SalesPersonAutoId int = null,              
@CustomerAutoId int=null,              
@DriverAutoId int=null,              
@FromDate datetime = null,              
@ToDate datetime = null,              
@Status int = null,                 
@PageIndex INT=1,                
@PageSize INT=10,                
@RecordCount INT=null,                
@isException bit out,                
@exceptionMessage varchar(max) out                
AS                
BEGIN                 
 BEGIN TRY                
  Set @isException=0                
  Set @exceptionMessage='Success'                
              
 IF @Opcode=42                     
  BEGIN                 
  SELECT AutoId,CustomerName from CustomerMaster where status = 1  order by replace(CustomerName,' ','') ASC            
  SELECT AutoId,FirstName +' '+ isnull(LastName,'') as SalesPerson  from EmployeeMaster where EmpType = 2 and status = 1  order by SalesPerson ASC             
  select AutoId, StatusType from StatusMaster where Category = 'OrderMaster' and AutoId != 8 order by StatusType ASC            
  SELECT AutoId,FirstName + ' ' + LastName as DriverName from EmployeeMaster where EmpType in (2,5) and status = 1               
    Order by DriverName  
  END                
      ELSE if(@Opcode = 43)        
   begin        
    SELECT AutoId as CustomerAutoId,CustomerId + ' ' + CustomerName as CustomerName FROM CustomerMaster  where (SalesPersonAutoId=@SalesPersonAutoId or ISNULL(@SalesPersonAutoId,0)=0) order by replace(CustomerId + ' ' + CustomerName,' ','') ASC         
   end             
  ELSE IF @Opcode=41                
  BEGIN                
	SELECT ROW_NUMBER() OVER(ORDER BY [CustomerName]) AS RowNumber, * INTO #Results FROM                
	(   
	
	SELECT CM.CustomerId,CM.CustomerName, OrderNo,FORMAT(OrderDate,'MM/dd/yyyy') as OrderDate,ProductId,ProductName,UM.UnitType,sum(OIM.QtyDel/OIM.QtyPerUnit)      
	AS OrderQty, Em.FirstName +' ' + Em.LastName as SalesPerson,Em2.FirstName +' ' + Em2.LastName as Driver,CAST(pd.Price as decimal(10,2)) As PPrice FROM          
	Delivered_Order_Items AS OIM              
	INNER JOIN OrderMaster AS OM ON OM.AutoId=OIM.OrderAutoId              
	INNER JOIN CustomerMaster AS CM ON CM.AutoId=OM.CustomerAutoId              
	INNER JOIN ProductMaster AS PM ON PM.AutoId=OIM.ProductAutoId              
	INNER JOIN UnitMaster AS UM ON UM.AutoId=OIM.UnitAutoId            
	inner join EmployeeMaster as Em on em.AutoId = Om.SalesPersonAutoId            
	left join EmployeeMaster as Em2 on em2.AutoId = Om.Driver           
	Inner Join PackingDetails as pd on pd.ProductAutoId=OIM.ProductAutoId and pd.UnitType=OIM.UnitAutoId          
	where oim.IsExchange=1      
	and om.Status=11
	and (@CustomerAutoId = '0' or @CustomerAutoId is null or CM.AutoId = @CustomerAutoId )              
	and (@SalesPersonAutoId = '0' or @SalesPersonAutoId is null or OM.SalesPersonAutoId = @SalesPersonAutoId)            
	and (@DriverAutoId = '0' or @DriverAutoId is null or OM.Driver = @DriverAutoId)              
	and (@Status = '0' or @Status is null or om.Status = @Status)              
	and (@FromDate = '' or @FromDate is null or @ToDate = '' or @ToDate is null or CONVERT(date, OrderDate) between @FromDate and @ToDate)              
	GROUP BY CM.CustomerId,CM.CustomerName, OrderNo,OrderDate,ProductId,ProductName,UM.UnitType,Em.FirstName +' ' + Em.LastName,Em2.FirstName +' ' + Em2.LastName,pd.Price           
	having sum(OIM.QtyDel/OIM.QtyPerUnit)>0  

	Union

	SELECT CM.CustomerId,CM.CustomerName, OrderNo,FORMAT(OrderDate,'MM/dd/yyyy') as OrderDate,ProductId,ProductName,UM.UnitType,sum(OIM.RequiredQty)      
	AS OrderQty, Em.FirstName +' ' + Em.LastName as SalesPerson,Em2.FirstName +' ' + Em2.LastName as Driver,CAST(pd.Price as decimal(10,2)) As PPrice FROM          
	OrderItemMaster AS OIM              
	INNER JOIN OrderMaster AS OM ON OM.AutoId=OIM.OrderAutoId              
	INNER JOIN CustomerMaster AS CM ON CM.AutoId=OM.CustomerAutoId              
	INNER JOIN ProductMaster AS PM ON PM.AutoId=OIM.ProductAutoId              
	INNER JOIN UnitMaster AS UM ON UM.AutoId=OIM.UnitTypeAutoId            
	inner join EmployeeMaster as Em on em.AutoId = Om.SalesPersonAutoId            
	left join EmployeeMaster as Em2 on em2.AutoId = Om.Driver           
	Inner Join PackingDetails as pd on pd.ProductAutoId=OIM.ProductAutoId and pd.UnitType=OIM.UnitTypeAutoId          
	where oim.IsExchange=1      
	and om.Status NOT IN(8,11)
	and (@CustomerAutoId = '0' or @CustomerAutoId is null or CM.AutoId = @CustomerAutoId )              
	and (@SalesPersonAutoId = '0' or @SalesPersonAutoId is null or OM.SalesPersonAutoId = @SalesPersonAutoId)            
	and (@DriverAutoId = '0' or @DriverAutoId is null or OM.Driver = @DriverAutoId)              
	and (@Status = '0' or @Status is null or om.Status = @Status)              
	and (@FromDate = '' or @FromDate is null or @ToDate = '' or @ToDate is null or CONVERT(date, OrderDate) between @FromDate and @ToDate)              
	GROUP BY CM.CustomerId,CM.CustomerName, OrderNo,OrderDate,ProductId,ProductName,UM.UnitType,Em.FirstName +' ' + Em.LastName,Em2.FirstName +' ' + Em2.LastName,pd.Price           
	having sum(OIM.RequiredQty)>0              
               
	)AS t ORDER BY [CustomerName]     
	
	SELECT COUNT(CustomerName) AS RecordCount,case when @PageSize=0 then COUNT(RowNumber) else  @PageSize end AS PageSize, @PageIndex AS PageIndex,
	Format(getdate(),'MM/dd/yyyy hh:mm tt') as PrintDate FROM #Results                
                    
	SELECT * FROM #Results                
	WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))           
	select sum(isnull(OrderQty,0) * ISNULL(PPrice,0)) as OverAllAmount FROM #Results          
  END                 
 END TRY                
 BEGIN CATCH                
  Set @isException=1                
  Set @exceptionMessage=ERROR_MESSAGE()                
 END CATCH                
END 
GO
