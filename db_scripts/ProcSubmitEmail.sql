USE [Emailing_DB]
GO
/****** Object:  StoredProcedure [dbo].[ProcSubmitEmail]    Script Date: 08-12-2020 05:25:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
ALTER Proc [dbo].[ProcSubmitEmail]  
(  
 @FromEmailId varchar(100)=null,   
 @FromName varchar(100)=null,   
 @smtp_userName varchar(100)=null,  
 @Password varchar(max)=null,   
 @SMTPServer varchar(50)=null,   
 @Port int=null,   
 @SSL bit=null,   
 @ToEmailId  varchar(100)=null,   
 @CCEmailId  varchar(100)=null,   
 @BCCEmailId  varchar(100)=null,   
 @Subject  varchar(max)=null,   
 @EmailBody  varchar(max)=null,   
 @Status int=null,   
 @SentDate datetime=null,   
 @SourceApp  varchar(20)=null,   
 @SubUrl  varchar(100)=null,   
 @ErrorDescription varchar(max)=null,  
 @AutoId int = null,  
 @Opcode int = null,
 @attachment varchar(50) = null,
 @PageIndex INT=1,  
 @PageSize INT=10,  
 @RecordCount INT=null,  
 @isException BIT OUT,  
 @exceptionMessage VARCHAR(max) OUT  
)  
as begin  
BEGIN TRY  
 SET @isException=0  
 SET @exceptionMessage='Success!!'  
  if @Opcode = 11  
  begin  
    insert into EmailLog(FromEmailId, FromName, smtp_userName, Password, SMTPServer, Port, SSL, ToEmailId, CCEmailId, BCCEmailId, Subject, EmailBody, CreationDate, Status, SourceApp, SubUrl,attachment)  
 values(@FromEmailId, @FromName, @smtp_userName, @Password, @SMTPServer, @Port, @SSL, @ToEmailId, @CCEmailId, @BCCEmailId, @Subject, @EmailBody, getdate(), 0, @SourceApp, @SubUrl,@attachment)  
  end  
else if @Opcode = 41  
begin 
	select AutoId,FromEmailId,FromName,isnull(smtp_userName,0) as smtp_userName,Password,SMTPServer,Port,SSL,ToEmailId,CCEmailId,
	case when isnull(Subject,'')='Your Chamonix order item(s) have been shipped' then '' else 'bcc@chamonixstore.com' end as BCCEmailId,Subject,EmailBody,CreationDate,Attemptcount,Attachment   
	from EmailLog where Status = 0 and (isnull(Attemptcount,0)=0 or LastAttemptDate<DATEADD(minute, -1, getdate())) and smtp_username is not null   
	order by isnull(Attemptcount,0) asc, CreationDate asc  
end  
else if @Opcode = 21  
begin  
 update EmailLog set Status = case when @ErrorDescription = 'Successful' then 1 else 0 end, SentDate = GETDATE(), ErrorDescription = @ErrorDescription, Attemptcount= isnull(Attemptcount,0)+1, LastAttemptDate=GETDATE() where AutoId = @AutoId   
end  
  
End TRY  
BEGIN CATCH  
  SET @isException=1  
  SET @exceptionMessage=ERROR_MESSAGE()  
END CATCH  
end 