USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Order_MLQty]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

alter table OrderMaster drop column MLQty
drop function FN_Order_MLQty
go
CREATE FUNCTION  [dbo].[FN_Order_MLQty]
( 
	@orderAutoId int,
	@Status INT
)
RETURNS decimal(18,2)
AS
BEGIN
	DECLARE @MLQty decimal(18,2)=0.00	
	IF @Status	!=11
	BEGIN 
		SET @MLQty=(SELECT sum(TotalMLQty) FROM OrderItemMaster WHERE OrderAutoId=@orderAutoId)	
	END 
	ELSE
	BEGIN
	SET @MLQty=(SELECT sum(TotalMLQty) FROM Delivered_Order_Items WHERE OrderAutoId=@orderAutoId)	
	END
	RETURN @MLQty
END
GO
alter table OrderMaster add [MLQty]  AS ([dbo].[FN_Order_MLQty]([Autoid],[Status]))