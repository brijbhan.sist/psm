 
DELETE from StatusMaster where Category='DraftOrder' and AutoId=1
 INSERT [dbo].[StatusMaster] ([AutoId], [StatusType], [Category], [ColorCode]) VALUES (1, N'Draft', N'DraftOrder', NULL)


 
CREATE FUNCTION  [dbo].[FN_VendorNetPrice]    
(    
  @AutoId int    
)    
RETURNS decimal(10,2)    
AS    
BEGIN    
 DECLARE @OrderAmount decimal(10,2)     
  SET @OrderAmount=isnull((select sum(Quantity*Price) from BillItems where AutoId=@AutoId),0)  
 RETURN @OrderAmount    
END 
GO
ALTER TABLE BillItems add NetPrice as [dbo].[FN_VendorNetPrice](AutoId)

EXEC sp_configure 'show advanced options', 1
GO
-- To update the currently configured value for advanced options.
RECONFIGURE
GO
-- To enable the feature.
EXEC sp_configure 'xp_cmdshell', 1
GO
-- To update the currently configured value for this feature.
RECONFIGURE
GO


--Umang Gupta date-1/8/2019 Start------
sp_rename 'App_DraftCartMasterCust.DraftId', 'DraftNo', 'COLUMN'

sp_rename 'App_DraftCartitemMasterCust.ProductId', 'ProductAutoId', 'COLUMN'

Alter table App_DraftCartItemMasterCust drop column DraftId

Alter table App_DraftCartItemMasterCust add DraftAutoId INT IDENTITY not null

-----End------------------------
--Umang Gupta date-1/9/2019 Start------


    Alter table OrderMaster add TotalNOI as [dbo].[FN_Order_TotalNoOfItem](AutoId) Done

    Alter table PurchaseOrderMaster add TotalOrderAmount decimal(10,2),TotalNoOfItem int null default(0),OrderNo varchar(20) null Done
-----End------------------------


--Umang Gupta date-1/10/2019 Start------

   Update StatusMaster set StatusType='Received PO' where AutoId=7 and Category='Inv'

-----End------------------------
--------App API OrderSummary------

ALTER FUNCTION  [dbo].[FN_App_get_DraftItem_cust_NetPrice]  
(  
  @AutoId int  
)  
RETURNS decimal(10,2)
AS  
BEGIN  
 DECLARE @NetPrice  decimal(10,2)  
  
 SET @NetPrice=(select Quantity*price  from App_DraftCartItemMasterCust as ba  WHERE DraftAutoId=@AutoId)   
 RETURN @NetPrice  
END

Create FUNCTION  [dbo].[FN_App_get_DraftOrder_cust_SubTotal]  
(  
  @AutoId int  
)  
RETURNS decimal(10,2)
AS  
BEGIN  
 DECLARE @SubTotal  decimal(10,2)  
  
 SET @SubTotal=ISNULL((select SUM(NetPrice)  from App_DraftCartItemMasterCust as ba  WHERE CartAutoId=@AutoId),0)
 RETURN @SubTotal  
END

Alter table App_DraftCartItemMasterCust add NetPrice as dbo.FN_App_get_DraftItem_cust_NetPrice(DraftAutoId)
Alter table App_DraftCartMasterCust add SubTotal as [dbo].FN_App_get_DraftOrder_cust_SubTotal(AutoId)
Alter table App_DraftCartMasterCust add shipAddr as [dbo].FN_getShippingAddress(DefaultShipAutoId)
Alter table App_DraftCartMasterCust add billAddr as [dbo].[FN_getBillingAddress](DefaultBillAutoId)


Create FUNCTION  [dbo].[FN_getBillingAddress]  
(  
  @AutoId int  
)  
RETURNS varchar(200)  
AS  
BEGIN  
 DECLARE @Address  varchar(500)  
  
 SET @Address=(select Address +' '+ISNULL(City,'')+' '+  
 ISNULL((select StateName from State as st where st.AutoId=ba.State),'')+ ' ' +Zipcode  
  from BillingAddress as ba  WHERE AutoId=@AutoId)   
 RETURN @Address  
END

Create FUNCTION  [dbo].[FN_getShippingAddress]
(
	 @AutoId int
)
RETURNS varchar(200)
AS
BEGIN
	DECLARE @Address  varchar(500)

	SET @Address=(select Address +' '+ISNULL(City,'')+' '+
	ISNULL((select StateName from State as st where st.AutoId=ba.State),'')+ ' ' +Zipcode
	 from ShippingAddress as ba  WHERE AutoId=@AutoId)	
	RETURN @Address
END

------end---------------------

---For PO Vendor -------------------

Select * from StatusMaster where  Category='Inv'


Alter table PurchaseOrderMaster add BillNo varchar(20)
Alter table PurchaseOrderMaster add BillDate dateTime

Delete From StatusMaster where Category='Inv'
insert into StatusMaster(AutoId,StatusType,Category,ColorCode) values(1,'New','Inv','#08B81D')
insert into StatusMaster(AutoId,StatusType,Category,ColorCode) values(2,'Draft Received','Inv','#0576F8')
insert into StatusMaster(AutoId,StatusType,Category,ColorCode) values(3,'Forword PO','Inv','#8706A3')
insert into StatusMaster(AutoId,StatusType,Category,ColorCode) values(4,'Revert PO','Inv','#CEC516')
insert into StatusMaster(AutoId,StatusType,Category,ColorCode) values(5,'Cancel PO','Inv','#E54C61')
insert into StatusMaster(AutoId,StatusType,Category,ColorCode) values(6,'Received PO','Inv','#26A9A0')
insert into StatusMaster(AutoId,StatusType,Category,ColorCode) values(7,'Pending','Inv','#AAAF67')
insert into StatusMaster(AutoId,StatusType,Category,ColorCode) values(8,'Approved','Inv','#47CB5C')

Select * from StatusMaster where Category='INV'





alter table [dbo].[PurchaseProductsMaster] add RecivedQty int
Alter table PurchaseOrderMaster add ReceivedBy int default null,AssignedBy int default null
 
alter table [dbo].[PurchaseOrderMaster] add IMRemark varchar(max)
alter table [dbo].[PurchaseOrderMaster] add InvRecieverAutoId int


Select * from PurchaseOrderMaster where PONo like'%PO00%'and  VendorType=0
Select * from PurchaseProductsMaster

Update PurchaseOrderMaster Set VendorType=1 where PONo like'%PO00%'and  VendorType=0

--Excute ProcPurchaseOrderVENDorMaster

-----End-----------------------

insert into  [tbl_ActionMaster] values('Order has been update','Order has been changed status @OldStatus to @newStatus by @Emp')
ALTER TABLE DraftPurchaseProductsMaster ADD VendorAutoId int null

ALTER TABLE CompanyDetails ADD currentVersion varchar(500) null



