USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcAppPaymentMaster]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
alter PROCEDURE [dbo].[ProcAppPaymentMaster]                  
@Opcode INT=NULL,                    
@timeStamp datetime =null,    
@referencePaymentNumber  varchar(100)=NULL,      
@CustomerAutoId int=null,                 
@orderdate datetime = null,                
@PaymentAutoId INT=NULL,                  
@PaymentMode int=NULL,      
@PaymentId varchar(100)=NULL,      
@ReferenceId varchar(100)=NULL,                  
@Remarks varchar(100)=NULL,                 
@ReceivedDate datetime=NULL,                   
@FromDate  datetime=NULL,                  
@ToDate datetime=NULL,       
@ReceivedAmount DECIMAL(8,2)=NULL,   
@DeviceID varchar(500)=null,  
@appVersion varchar(50)=null,  
@latLong varchar(max)=null,  
@userAutoId int  =NULL,         
@isException bit out,                  
@exceptionMessage varchar(max) out                  
AS                  
BEGIN                   
 BEGIN TRY                  
  Set @isException=0                  
  Set @exceptionMessage='Success'                  
         
 IF @Opcode=11    
 BEGIN         
 BEGIN TRY           
  BEGIN TRAN    
  IF(ISNULL(@referencePaymentNumber,'')='')    
  BEGIN    
  SET @isException=1                  
  SET @exceptionMessage='referencePaymentNumber is mandetory'            
  END    
  Else IF exists(select * from SalesPaymentMaster where referencePaymentNumber=@referencePaymentNumber)               
  Begin               
   Set @isException=0                 
   Set @exceptionMessage='Payment  already received.'            
   select  referencePaymentNumber,PaymentAutoId,PayId as PaymentId,(select statustype from [dbo].[StatusMaster] where [AutoId]=Status and [Category] = 'Pay')status       
   from SalesPaymentMaster where referencePaymentNumber=@referencePaymentNumber        
  END    
  ELSE     
  BEGIN    
  SET @PaymentId=(select dbo.SequenceCodeGenerator('SalesPaymentId'))    
  INSERT INTO SalesPaymentMaster (PayId, CustomerAutoId, ReceivedDate, ReceivedAmount, PaymentMode,    
  ReferenceId,Remarks,ReceiveDate,ReceiveBy,Status,PayType,referencePaymentNumber,DateUpdate,[DeviceId],[AppVersion],[LatLong])     
  VALUES (@PaymentId,@CustomerAutoId,@ReceivedDate,@ReceivedAmount,  
  CASE WHEN ISNULL(@PaymentMode,0)=0 then 2 else @PaymentMode end   ,@ReferenceId,@Remarks,GETDATE(),@userAutoId,1,3,@referencePaymentNumber,GETDATE(),@DeviceID,@appVersion,@latLong)    
  set @PaymentAutoId=Scope_Identity()    
  UPDATE SequenceCodeGeneratorMaster SET currentSequence=currentSequence+1 WHERE SequenceCode='SalesPaymentId'    
  select @referencePaymentNumber as referencePaymentNumber,@PaymentId as PaymentId, @PaymentAutoId as PaymentAutoId, (select statustype from [dbo].[StatusMaster] where [AutoId]=1 and [Category] = 'Pay') status       
  END    
  COMMIT TRANSACTION    
 END TRY    
 BEGIN CATCH    
 ROLLBACK TRAN    
 SET @isException=1    
 SET @exceptionMessage=ERROR_MESSAGE()    
 END CATCH    
END       
  ELSE IF @Opcode=41    
  BEGIN    
   SELECT  PaymentAutoId,PayId as PaymentId,CustomerAutoId,CustomerName,    
   CONVERT(varchar(10), ReceivedDate,101) as ReceivedDate, ReceivedAmount,ReceiveBy,referencePaymentNumber,    
   spm.PaymentMode as PaymentModeAutoId, pmm.PaymentMode as [PaymentMode],ReferenceId,Remarks,    
   StatusType as Status,spm.Status as StatusCode    
   from SalesPaymentMaster AS spm    
   INNER JOIN CustomerMaster AS CM ON spm.customerAutoid = CM.AutoId    
   INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId=spm.Status and SM.[Category]='Pay'     
   Inner join [dbo].[PAYMENTModeMaster] as PMM on Pmm.AutoID=spm.PaymentMode    
   where  ReceiveBy = @userAutoId and spm.Status = 1     
  END        
  ELSE IF @Opcode=42    
  BEGIN    
    SELECT * FROM [dbo].[PAYMENTModeMaster] where status=1 order by paymentmode    
  END    
 END TRY                  
 BEGIN CATCH                  
  Set @isException=1                  
  Set @exceptionMessage=ERROR_MESSAGE()                  
 END CATCH                  
END 
GO
