ALTER   procedure [dbo].[ProcIssueinStockOrder] 
AS
BEGIN


    Declare @html varchar(max)='',@count int,@num int,@tr varchar(max),
	@OrderNo varchar(10),@OrderDate varchar(25),@Status varchar(50),@Packer varchar(50),@WarehouseManager varchar(50),
	@CustomerId varchar(50),@CustomerName varchar(50),@AssignDate varchar(25)
		

		select Row_Number() over (order by OrderNo)  as rownumber,OrderNo,OrderDate ,pm.ProductId,pm.ProductName ,om.Status into #result from OrderMaster as om
		inner join OrderItemMaster as oim on oim.OrderAutoId=om.AutoId
		inner join ProductMaster as pm on pm.AutoId=oim.ProductAutoId
		left join (
		select ReferenceId,ProductId from ProductStockUpdateLog where ReferenceType='OrderMaster'
		) as t on om.AutoId=t.ReferenceId and pm.ProductId=t.ProductId
		where   om.Status not in (1,2,8,9)
		and 
		convert(date,orderdate)>convert(date,getdate()-15)
		and t.ReferenceId is null
		and QtyShip>0
		and pm.productid not in (9999999)
		order by OrderDate desc

	set @html=@html+'
		<table class="table tableCSS" border="1" style="width:100%;border: 1px solid black;border-collapse:collapse;"> 
		<thead>
		<tr>
		   <td style=''text-align:center;''><b>order Id</b></td>
			<td style=''text-align:center;''><b>order date</b></td> 
			<td style=''text-align:center;''><b>Product Id</b></td> 
			<td style=''text-align:center;''><b>Product Name</b></td> 
			<td style=''text-align:center;''><b>Status</b></td> 
       </tr>
	   </thead>
		<tbody>'
		SET @count= (select count(1) from #result)
		SET @num=1
		while(@num<=@Count)
		BEGIN  
				set @tr ='<tr>' 
					+	'<td style=''text-align:center;''>'+ (select OrderNo from #result where rownumber = @num) + '</td>'
					+	'<td style=''text-align:left;''>'+ (select FORMAT(Orderdate,'MM/dd/yyyy hh:mm tt') from #result where rownumber = @num) + '</td>'
					+	'<td style=''text-align:left;''>'+ (select convert(varchar(100),ProductId) from #result where rownumber = @num) + '</td>'
					+	'<td style=''text-align:left;''>'+ (select ProductName from #result where rownumber = @num) + '</td>'
					+	'<td style=''text-align:left;''>'+ (select convert(varchar(100),Status) from #result where rownumber = @num) + '</td>'
					+ '</tr>'
		set @num = @num + 1
		SET @html=@html+@tr
		END
		SET @html=@html+'</tbody></table>'

	Declare  @Subject varchar(250),@FromName varchar(500),@FromEmailId varchar(500),@Port int,@SMTPServer varchar(150),@smtp_userName varchar(150),
	@Password varchar(50),@SSL  int,@BCCEmailId varchar(max),@ToEmailId varchar(500),@CCEmailId varchar(max)
	set @Subject = 'A1WHM -' +UPPER((select CompanyDistributerName from dbo.CompanyDetails))+' - '+'Invalid Stock Order' 
	
	SELECT @FromEmailId=FromEmail,@FromName=DisplayName,@smtp_userName=SMTP_USERNAME,@Password=SMTP_PASSWORD,
	@SMTPServer=HOST,@Port=SMTPPORT,@SSL=EnableSsl from EmailServerMaster_AWS 

	set @BCCEmailId=(select BCCEmail from Tbl_ReportEmailReceiver where Autoid=13);
	set @ToEmailId=(select ToEmail from Tbl_ReportEmailReceiver where Autoid=13)
	SET @CCEmailId=(select CCEmail from Tbl_ReportEmailReceiver where Autoid=13)

	IF(select count(1) from #result )>0
	BEGIN
		 EXEC [Emailing_DB].[dbo].[ProcSubmitEmail] 
		@Opcode=11,
		@FromEmailId =@FromEmailId,
		@FromName = @FromName,
		@smtp_userName=@smtp_userName,
		@Password = @Password,
		@SMTPServer = @SMTPServer,
		@Port =@Port,
		@SSL =@SSL,
		@ToEmailId =@ToEmailId,
		@CCEmailId =@CCEmailId,
		@BCCEmailId =@BCCEmailId,  
		@Subject =@Subject,
		@EmailBody = @html,
		@SentDate ='',
		@Status =0,
		@SourceApp ='PSM',
		@SubUrl ='Wrong Packer', 
		@isException=0,
		@exceptionMessage=''  
	END
END
