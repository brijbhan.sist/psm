USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcSetDefaultUnitALL]    Script Date: 08-09-2020 03:50:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER Proc [dbo].[ProcSetDefaultUnitALL]  
@ProductId varchar(250),@UnitAutoID int  
as   
BEGIN   
  declare @ProductAutoId int  
  if(DB_NAME() in ('psmnj.a1whm.com','psmpa.a1whm.com','psmnpa.a1whm.com','psmct.a1whm.com','psmwpa.a1whm.com',
  'psmny.a1whm.com'))                      
  BEGIN      
   SET @ProductAutoId=(SELECT AutoId FROM [psmnj.a1whm.com].[DBO].[ProductMaster] WHERE ProductId=@ProductId)  
   UPDATE [psmnj.a1whm.com].[DBO].[ProductMaster] SET PackingAutoId=(select UnitType from [psmnj.a1whm.com].[DBO].[PackingDetails]  
   where ProductAutoId=@ProductAutoId and UnitType=@UnitAutoID) WHERE Autoid=@ProductAutoId    
        
   SET @ProductAutoId=(SELECT AutoId FROM [psmct.a1whm.com].[DBO].[ProductMaster] WHERE ProductId=@ProductId)  
   UPDATE [psmct.a1whm.com].[DBO].[ProductMaster] SET PackingAutoId=(select UnitType from [psmct.a1whm.com].[DBO].[PackingDetails]   
   where ProductAutoId=@ProductAutoId and UnitType=@UnitAutoID) WHERE Autoid=@ProductAutoid    
  
   SET @ProductAutoId=(SELECT AutoId FROM [psmpa.a1whm.com].[DBO].[ProductMaster] WHERE ProductId=@ProductId)  
   UPDATE [psmpa.a1whm.com].[DBO].[ProductMaster] SET PackingAutoId=(select UnitType from [psmpa.a1whm.com].[DBO].[PackingDetails]   
   where ProductAutoId=@ProductAutoId and UnitType=@UnitAutoID) WHERE Autoid=@ProductAutoid    
  
   SET @ProductAutoId=(SELECT AutoId FROM [psmnpa.a1whm.com].[DBO].[ProductMaster] WHERE ProductId=@ProductId)  
   UPDATE [psmnpa.a1whm.com].[DBO].[ProductMaster] SET PackingAutoId=(select UnitType from [psmnpa.a1whm.com].[DBO].[PackingDetails]   
   where ProductAutoId=@ProductAutoId and UnitType=@UnitAutoID) WHERE Autoid=@ProductAutoid    
  
   SET @ProductAutoId=(SELECT AutoId FROM [psmwpa.a1whm.com].[DBO].[ProductMaster] WHERE ProductId=@ProductId)  
   UPDATE [psmwpa.a1whm.com].[DBO].[ProductMaster] SET PackingAutoId=(select UnitType from [psmwpa.a1whm.com].[DBO].[PackingDetails]   
   where ProductAutoId=@ProductAutoId and UnitType=@UnitAutoID) WHERE Autoid=@ProductAutoid  
  
   SET @ProductAutoId=(SELECT AutoId FROM [psmny.a1whm.com].[DBO].[ProductMaster] WHERE ProductId=@ProductId)  
   UPDATE [psmny.a1whm.com].[DBO].[ProductMaster] SET PackingAutoId=(select UnitType from [psmny.a1whm.com].[DBO].[PackingDetails]   
   where ProductAutoId=@ProductAutoId and UnitType=@UnitAutoID) WHERE Autoid=@ProductAutoid  
 
   SET @ProductAutoId=(SELECT AutoId FROM [psmnj.easywhm.com].[DBO].[ProductMaster] WHERE ProductId=@ProductId)  
   UPDATE [psmnj.easywhm.com].[DBO].[ProductMaster] SET PackingAutoId=(select UnitType from [psmnj.easywhm.com].[DBO].[PackingDetails]   
   where ProductAutoId=@ProductAutoId and UnitType=@UnitAutoID) WHERE Autoid=@ProductAutoid  
 
   SET @ProductAutoId=(SELECT AutoId FROM [psmpa.easywhm.com].[DBO].[ProductMaster] WHERE ProductId=@ProductId)  
   UPDATE [psmpa.easywhm.com].[DBO].[ProductMaster] SET PackingAutoId=(select UnitType from [psmpa.easywhm.com].[DBO].[PackingDetails]   
   where ProductAutoId=@ProductAutoId and UnitType=@UnitAutoID) WHERE Autoid=@ProductAutoid  
 
   SET @ProductAutoId=(SELECT AutoId FROM [psmnpa.easywhm.com].[DBO].[ProductMaster] WHERE ProductId=@ProductId)  
   UPDATE [psmnpa.easywhm.com].[DBO].[ProductMaster] SET PackingAutoId=(select UnitType from [psmnpa.easywhm.com].[DBO].[PackingDetails]   
   where ProductAutoId=@ProductAutoId and UnitType=@UnitAutoID) WHERE Autoid=@ProductAutoid  
 
   SET @ProductAutoId=(SELECT AutoId FROM [psmwpa.easywhm.com].[DBO].[ProductMaster] WHERE ProductId=@ProductId)  
   UPDATE [psmwpa.easywhm.com].[DBO].[ProductMaster] SET PackingAutoId=(select UnitType from [psmwpa.easywhm.com].[DBO].[PackingDetails]   
   where ProductAutoId=@ProductAutoId and UnitType=@UnitAutoID) WHERE Autoid=@ProductAutoid  
 
   SET @ProductAutoId=(SELECT AutoId FROM [psmct.easywhm.com].[DBO].[ProductMaster] WHERE ProductId=@ProductId)  
   UPDATE [psmct.easywhm.com].[DBO].[ProductMaster] SET PackingAutoId=(select UnitType from [psmct.easywhm.com].[DBO].[PackingDetails]   
   where ProductAutoId=@ProductAutoId and UnitType=@UnitAutoID) WHERE Autoid=@ProductAutoid  
 
   SET @ProductAutoId=(SELECT AutoId FROM [psmny.easywhm.com].[DBO].[ProductMaster] WHERE ProductId=@ProductId)  
   UPDATE [psmny.easywhm.com].[DBO].[ProductMaster] SET PackingAutoId=(select UnitType from [psmny.easywhm.com].[DBO].[PackingDetails]   
   where ProductAutoId=@ProductAutoId and UnitType=@UnitAutoID) WHERE Autoid=@ProductAutoid  
 
  END  
  ELSE  
  BEGIN  
   SET @ProductAutoId=(SELECT AutoId FROM [DBO].[ProductMaster] WHERE ProductId=@ProductId)  
    UPDATE ProductMaster SET PackingAutoId=(select UnitType from [PackingDetails] where ProductAutoId=@ProductAutoId   
    and UnitType=@UnitAutoID) WHERE Autoid=@ProductAutoid    
  END  
END  