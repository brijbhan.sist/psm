
ALTER PROCEDURE [dbo].[ProcAssignModule]   
@OpCode int= Null,  
@AutoId int= Null,
@ModuleAutoId int= Null,
@SubModuleAutoId int= Null,
@PageAutoId int= Null,
@AssignedLocation varchar(200)= Null,  
@LocationId int= Null,
@Status int= Null,

@isException bit out,  
@exceptionMessage varchar(max) out  
AS  
BEGIN  
	BEGIN TRY  
			SET @isException = 0  
			SET @exceptionMessage = 'success'  

		IF @OpCode = 44
		BEGIN   
			BEGIN TRY  
			SELECT * FROM  [dbo].[Module] WHERE Status=1 ORDER BY ModuleName ASC
			SELECT * FROM  [dbo].[LocationMaster] ORDER BY Location ASC
			--SELECT * FROM  [dbo].[PageMaster] WHERE Status=1 ORDER BY PageName ASC
			END TRY  
			BEGIN CATCH  
			SET @exceptionMessage = 'Oops! Something went wrong.Please try later.'  
			SET @isException = 1  
			END CATCH  
		END  

		ELSE IF @OpCode = 43
		BEGIN   
			BEGIN TRY  
			--SELECT * FROM  [dbo].[PageMaster] WHERE Status=1 AND ModuleAutoId=@ModuleAutoId ORDER BY AutoId ASC
			SELECT * FROM  [dbo].[PageMaster] WHERE Status=1 ORDER BY AutoId ASC
			END TRY  
			BEGIN CATCH  
			SET @exceptionMessage = 'Oops! Something went wrong.Please try later.'  
			SET @isException = 1  
			END CATCH  
		END
		ELSE IF @OpCode = 45
		BEGIN   
			BEGIN TRY  
			SELECT * FROM  [dbo].[PageMaster] WHERE Status=1 AND AutoId=@SubModuleAutoId ORDER BY AutoId ASC
			END TRY  
			BEGIN CATCH  
			SET @exceptionMessage = 'Oops! Something went wrong.Please try later.'  
			SET @isException = 1  
			END CATCH  
		END
	
		ELSE IF @OpCode = 11
		BEGIN      
			--if exists(SELECT ModuleAutoId FROM [AssignedPage] WHERE ModuleAutoId = @ModuleAutoId AND
			--@AssignedLocation in (SELECT * FROM [dbo].[fnSplitString] (AssignedLocation ,','))
			--AND SubModuleAutoId=@SubModuleAutoId)  
			if exists(SELECT ModuleAutoId FROM [AssignedPage] WHERE ModuleAutoId = @ModuleAutoId
			AND SubModuleAutoId=@SubModuleAutoId)  
			BEGIN  
			SET @exceptionMessage = 'Page alread assigned.'  
			SET @isException = 1  
			END   
			ELSE 
			BEGIN   
			BEGIN TRY  
			INSERT INTO [dbo].[AssignedPage] (ModuleAutoId,SubModuleAutoId,AssignedLocation,Status) VALUES(@ModuleAutoId,@SubModuleAutoId,@AssignedLocation,1)
			END TRY  
			BEGIN CATCH  
			SET @exceptionMessage = 'Oops! Something went wrong.Please try later.'  
			SET @isException = 1  
			END CATCH  
			END 
		END

		ELSE IF @OpCode = 41
		BEGIN   
			BEGIN TRY  
			SELECT md.AutoId as ModuleAutoId,md.ModuleName,pm.AutoId as SubModuleAutoId,pm.PageName,
			pm.PageUrl ,ap.AutoId as AssignedPageAutoId,ap.AssignedLocation,ap.Status,(select
			STUFF((SELECT ',' +CONVERT(varchar(50), Location) FROM  LocationMaster
			where AutoId in (SELECT * FROM [dbo].[fnSplitString] (ap.AssignedLocation ,',')) FOR XML PATH ('')), 1, 1, '')) as Location
			FROM AssignedPage ap
			INNER JOIN Module md ON md.AutoId = ap.ModuleAutoId
			LEFT JOIN PageMaster pm ON pm.AutoId = ap.SubModuleAutoId
			WHERE   
			( ISNULL(@ModuleAutoId,'')='' OR md.AutoId=@ModuleAutoId)  AND
			( ISNULL(SubModuleAutoId,'')='' OR pm.AutoId=SubModuleAutoId) AND
			( ISNULL(@LocationId,'')='' OR @LocationId in(SELECT * FROM [dbo].[fnSplitString] (ap.AssignedLocation ,',')))
			SELECT * FROM AssignedPage
			END TRY  
			BEGIN CATCH  
			SET @exceptionMessage = 'Oops! Something went wrong.Please try later.'  
			SET @isException = 1  
			END CATCH  
		END   
      
		ELSE IF @OpCode = 21
		BEGIN   
			BEGIN TRY  
			UPDATE [dbo].[AssignedPage] SET ModuleAutoId=@ModuleAutoId,SubModuleAutoId=@SubModuleAutoId,AssignedLocation=@AssignedLocation,Status=1 WHERE AutoId=@AutoId
			END TRY  
			BEGIN CATCH
			SET @exceptionMessage = 'Oops! Something went wrong.Please try later.'  
			SET @isException = 1  
			END CATCH  
		END 

		ELSE IF @OpCode = 31
		BEGIN   
			BEGIN TRY  
			DELETE FROM [dbo].[AssignedPage] WHERE AutoId=@AutoId
			END TRY  
			BEGIN CATCH  
			SET @exceptionMessage = 'Oops! Something went wrong.Please try later.'  
			SET @isException = 1  
			END CATCH  
		END 
	END TRY  
			BEGIN CATCH  
			SET @isException=1          
			SET @exceptionMessage='Oops! Something went wrong.Please try later.'  
			END CATCH  
END  
  
  
  
GO
