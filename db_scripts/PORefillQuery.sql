--{"DraftAutoId":"2238","RefillType":"2","VenderAutoId":"1103","Remark":""}
Drop table #ProductAutoId
--Drop table #ProductAutoId3
Declare @RefillType int=2,@VenderAutoId int=1103,@LocationID int,@Location varchar(50),@SqlStr nvarchar(max)='',
@DraftAutoId int=2238,@PORemarks varchar(max)='Yes this is test',@EmpId int=1
       
	   
		SELECT distinct ProductAutoId,sum(Qty) as Qty INTO #ProductAutoId FROM                
		(  
		SELECT distinct OIM.ProductAutoId,sum(QtyDel) as Qty FROM          
		Delivered_Order_Items AS OIM              
		INNER JOIN OrderMaster AS OM ON OM.AutoId=OIM.OrderAutoId 
		where om.Status=11 and (OIM.QtyDel/OIM.QtyPerUnit)>0 AND
		DeliveryDate>=(case when @RefillType=2 then DATEADD(MONTH,-1, GETDATE()) else DATEADD(day, -14, GETDATE()) end)
		group BY OIM.ProductAutoId
		Union
		SELECT distinct OIM.ProductAutoId,SUM(OIM.QtyShip*OIM.QtyPerUnit) as Qty FROM          
		OrderItemMaster AS OIM              
		INNER JOIN OrderMaster AS OM ON OM.AutoId=OIM.OrderAutoId 
		where om.Status NOT IN(8,11,1,2) and oim.RequiredQty>0 AND
		OrderDate>=(case when @RefillType=2 then DATEADD(MONTH,-1, GETDATE()) else DATEADD(day, -14, GETDATE()) end)
		group BY OIM.ProductAutoId
		) as t group By ProductAutoId


		SET @LocationID=(Select LocationAutoId From VendorMaster where AutoId=@VenderAutoId)
		SET @Location=(Select Location FROM LocationMaster where AutoId=@LocationID)

		Declare @Check int=0
		--If((Select count(ProductAutoId) from #ProductAutoId)>0)
		--BEGIN
		--	  IF ISNULL(@DraftAutoId,0)=0                                                                                                                                              
		--	   BEGIN  
		--			INSERT INTO [dbo].[DraftPurchaseOrderMaster] ([PODate] , [VenderAutoId] , [PORemarks] , [CreatedBy] , [CreatedOn]) 
		--			VALUES  (GetDate() , @VenderAutoId , @PORemarks , @EmpId , GetDate())
		--			Set @DraftAutoId = SCOPE_IDENTITY()   
		--			SET @Check=1
		--	   End  
		--	   begin  
		--		   update [dbo].[DraftPurchaseOrderMaster] set VenderAutoId=@VenderAutoId, PORemarks=@PORemarks  where DraftAutoId=@DraftAutoId  
		--	   End  
  
		--	   IF @Check=1      
		--	   Begin  
		--	        SET @SqlStr='
		--			INSERT INTO   [dbo].[DraftPurchaseProductsMaster] ([ProductAutoId] , [Unit] , [QtyPerUnit] , [Qty] , [DraftAutoId])
		--			SELECT PD.ProductAutoId,UM.AutoId,PD.Qty,cast(ISNULL(PD1.Qty/PD.Qty,0) as INT),'+Convert(varchar(50),@DraftAutoId)+' FROM ProductMaster as PM
		--			Inner Join [dbo].[PackingDetails] AS PD on PD.ProductAutoId=PM.AutoId AND PD.UnitType=PM.PackingAutoId    
		--			INNER Join ['+@Location+'.a1whm.com].[dbo].Productmaster as PM2 on PM2.ProductId=PM.ProductId	
		--			INNER JOIN #ProductAutoId as PD1 on PD1.ProductAutoId=PM.AutoId
		--			INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = PD.UnitType 
		--			WHERE PM.ProductStatus=1 AND PM2.ProductStatus=1 '
		--			 execute sp_executesql @SqlStr	
		--	   End 
		--	   ELSE
		--	   BEGIN
		--	        Select distinct  PA.ProductAutoId,Qty INTO #ProductAutoId3 from #ProductAutoId as PA where pa.ProductAutoId not in 
		--			(
		--			select ProductAutoId from DraftPurchaseProductsMaster Where DraftAutoId=@DraftAutoId
		--			)

		--			 SET @SqlStr='
		--			INSERT INTO   [dbo].[DraftPurchaseProductsMaster] ([ProductAutoId] , [Unit] , [QtyPerUnit] , [Qty] , [DraftAutoId])
		--			SELECT PD.ProductAutoId,UM.AutoId,PD.Qty,cast(ISNULL(PD1.Qty/PD.Qty,0) as INT),'+Convert(varchar(50),@DraftAutoId)+' FROM ProductMaster as PM
		--			Inner Join [dbo].[PackingDetails] AS PD on PD.ProductAutoId=PM.AutoId AND PD.UnitType=PM.PackingAutoId    
		--			INNER Join ['+@Location+'.a1whm.com].[dbo].Productmaster as PM2 on PM2.ProductId=PM.ProductId	
		--			INNER JOIN #ProductAutoId3 as PD1 on PD1.ProductAutoId=PM.AutoId
		--			INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = PD.UnitType 
		--			WHERE PM.ProductStatus=1 AND PM2.ProductStatus=1 '
		--			 execute sp_executesql @SqlStr	
		--	   END
  --      END

		SET @SqlStr='
	       Select (
		   Select 
		   (	
			Select P1.ProductAutoId,PM.ProductId,PM.ProductName,PM.CurrentStock,VM.AutoId,VM.VendorName,
			(Select UnitType from UnitMaster where AutoId=PM.PackingAutoId) as DefaultUnit,PD.Qty as QtyPerUnit,cast(ISNULL(P1.Qty/PD.Qty,0) as INT) as Qty,PM.PackingAutoId
			FROM ProductMaster as PM 
			Inner join ['+@Location+'.a1whm.com].[dbo].ProductMaster as PM1 on PM.ProductId=PM1.ProductId
			INNER JOIN #ProductAutoId as P1 on P1.ProductAutoId=PM.Autoid
			INNER JOIN PackingDetails as PD on PD.ProductAutoId=P1.ProductAutoId AND PD.UnitType=PM.PackingAutoId
			Inner Join VendorMaster as VM on VM.AutoId=PM.VendorAutoId
			Where PM.ProductStatus=1 AND PM1.ProductStatus=1 
			for json path, INCLUDE_NULL_VALUES
		   ) as ProductList,
		   (
			Select P1.ProductAutoId,um.AutoId,um.UnitType,pd.EligibleforFree,pd.qty
			from  PackingDetails as PD 
			inner join UnitMaster as um on pd.UnitType=um.AutoId
			inner join #ProductAutoId as P1 on P1.ProductAutoId=PD.ProductAutoId
			Group by P1.ProductAutoId,um.AutoId,um.UnitType,pd.qty,pd.EligibleforFree for json path, INCLUDE_NULL_VALUES
		   ) as UnitList for json path, INCLUDE_NULL_VALUES		   
		   ) as ProductDetails'

		  execute sp_executesql @SqlStr	