 
alter procedure ProcSale_By_Category                      
@Opcode INT=NULL,       
@CategoryAutoId int= null,    
@SubCategoryAutoId int= null,                    
@EmpAutoId int = null,    
@ToDate date= null,                      
@PageIndex INT=1,                      
@PageSize INT=10,                      
@RecordCount INT=null,                      
@isException bit out,   
@orderfrom date= null,   

@exceptionMessage varchar(max) out                      
as                   
BEGIN                       
 BEGIN TRY                      
  Set @isException=0                      
  Set @exceptionMessage='Success'                      
  
  
                    
   IF @OPCODE=42                     
  BEGIN      
select ROW_NUMBER() OVER(ORDER BY CategoryName asc) AS RowNumber,cm.CategoryName,scm.SubcategoryName,
sum(Net_Sale_Rev) as TotalSales into ##tempNJ
from
(
	select em.FirstName,t1.ProductId,t1.ProductName,t1.UnitType,t1.Qty as [Pcperunit],
	isnull(pppl.CustomPrice,0) as CustomPrice,
	convert(decimal(18,2),(t1.QtyDel/t1.Qty)) as [Sold_Default_Qty],
	t1.QtyDel as [Sold_Pc],
	t1.NetPrice as [SaleRev],
	convert(decimal(18,2),((t1.QtyDel-isnull(t2.QtyDel,0))/t1.Qty)) as [Net_Sold_Default_Qty],
	(t1.QtyDel-isnull(t2.QtyDel,0)) as [Net_Sold_Pc],
	(t1.NetPrice - isnull(t2.NetPrice,0)) as Net_Sale_Rev,
	t1.Del_CostPrice -isnull(((case when t1.QtyDel != 0 then (t1.Del_CostPrice/t1.QtyDel) else 0 end)*t2.QtyDel),0) as costP
	from (
		select om.SalesPersonAutoId, pd.ProductAutoId, pm.ProductId, pm.ProductName, pm.PackingAutoId, um.UnitType, pd.Qty,
		sum(doi.QtyDel) as QtyDel, sum(doi.NetPrice) as NetPrice, sum((doi.Del_CostPrice/doi.QtyPerUnit)*doi.QtyDel) as Del_CostPrice
		from OrderMaster as om
		inner join Delivered_Order_Items as doi on doi.OrderAutoId=om.AutoId
		inner join ProductMaster as pm on pm.AutoId=doi.ProductAutoId
		inner join PackingDetails as pd on pd.ProductAutoId=pm.AutoId and pd.UnitType=pm.PackingAutoId
		inner join UnitMaster as um on um.AutoId=pm.PackingAutoId
		where (@orderfrom is null or @orderfrom = '' or @todate is null or @todate = '' or                
		(CONVERT(DATE,OrderDate) between @orderfrom and @todate))   and om.Status=11
		and (@CategoryAutoId=0 or CategoryAutoId=@CategoryAutoId) and (@SubCategoryAutoId=0 or SubcategoryAutoId=@SubCategoryAutoId)
		group by om.SalesPersonAutoId, pm.ProductId, pm.ProductName, um.UnitType, pd.Qty,pd.ProductAutoId, pm.PackingAutoId
	) as t1
	left join (
		select cmm.CreatedBy, pd.ProductAutoId, pm.ProductId, pm.ProductName, pm.PackingAutoId, um.UnitType, pd.Qty,
		sum(cim.TotalPeice) as QtyDel, sum(cim.NetAmount) as NetPrice
		from CreditMemoMaster as cmm
		inner join CreditItemMaster as cim on cim.CreditAutoId=cmm.CreditAutoId
		inner join ProductMaster as pm on pm.AutoId=cim.ProductAutoId
		inner join PackingDetails as pd on pd.ProductAutoId=pm.AutoId and pd.UnitType=pm.PackingAutoId
		inner join UnitMaster as um on um.AutoId=pm.PackingAutoId
		where convert(date,cmm.ApprovalDate) between  @orderfrom and @todate and cmm.Status=3
		group by cmm.CreatedBy, pm.ProductId, pm.ProductName, um.UnitType, pd.Qty,pd.ProductAutoId, pm.PackingAutoId
	) as t2 on t1.SalesPersonAutoId=t2.CreatedBy and t1.ProductAutoId=t2.ProductAutoId and t1.PackingAutoId=t2.PackingAutoId
	inner join EmployeeMaster as em on em.AutoId=t1.SalesPersonAutoId
	left join ProductPricingInPriceLevel as pppl on pppl.ProductAutoId=t1.ProductAutoId and pppl.UnitAutoId=t1.PackingAutoId and pppl.PriceLevelAutoId=2838
) as t
inner join ProductMaster as pm on pm.ProductId = t.ProductId
inner join CategoryMaster as cm on cm.AutoId = pm.CategoryAutoId
inner join SubCategoryMaster  as scm on scm.AutoId=pm.SubcategoryAutoId 
group by  cm.CategoryName,scm.SubcategoryName
order by  cm.CategoryName,scm.SubcategoryName 
	
  SELECT COUNT(*) AS RecordCount, case when @PageSize=0 then COUNT(*) else @PageSize end AS PageSize,                                                              
	@PageIndex AS PageIndex FROM ##tempNJ                                                               
	SELECT * FROM ##tempNJ                                                                                                                                                                                
	WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)) 
	
   select isnull(sum(TotalSales),0.00) as TotalSales  from ##tempNJ


  END  
  
  IF @Opcode=45                      
  BEGIN                          
  	select(select(SELECT [AutoId],CategoryName
	FROM CategoryMaster ORDER BY CategoryName ASC for json path, INCLUDE_NULL_VALUES) as Category 
	for json path, INCLUDE_NULL_VALUES)as Category       
  END                 
  IF @Opcode=46                      
  BEGIN   
 select(select(SELECT [AutoId],SubcategoryName
	FROM SubCategoryMaster ORDER BY SubcategoryName ASC for json path, INCLUDE_NULL_VALUES) as SubCategory 
	for json path, INCLUDE_NULL_VALUES)as SubCategory           
  END                  
  END TRY                      
 BEGIN CATCH                      
  Set @isException=1                      
  Set @exceptionMessage=ERROR_MESSAGE()                      
 END CATCH                      
END 
