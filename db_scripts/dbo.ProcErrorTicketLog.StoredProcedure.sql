ALTER PROCEDURE [dbo].[ProcErrorTicketLog]      
 @Who nvarchar(25)=NULL,        
 @isException bit out,      
 @exceptionMessage nvarchar(500) out,        
 @TicketID nvarchar(50)=Null,      
 @FromDate datetime=Null,      
 @ToDate datetime=Null,      
 @Type nvarchar(50)=Null,      
 @ByEmployee nvarchar(50)=Null,      
 @Status nvarchar(50)=Null,  
 @EmployeeType nvarchar(50)=Null,  
 @DeveloperStatus nvarchar(50)=Null,  
 @Subject nvarchar(50)=Null,     
 @PageIndex INT = 1,        
 @PageSize INT = 10,         
 @opCode int=Null      
AS      
BEGIN      
 BEGIN TRY       
  BEGIN TRANSACTION      
   SET @isException=0      
   SET @exceptionMessage= ''  
   if @opCode=41      
    begin    
		SELECT * FROM EmployeeTypeMaster  ORDER BY TypeName asc  
        select [AutoId],FirstName,FirstName+' '+LastName+' - '+[EmpId] as fullname from [dbo].[EmployeeMaster] WHERE Status=1  ORDER BY  replace(FirstName+' '+LastName+' - '+[EmpId] ,' ','') asc  
    end      
    ELSE if @opCode=43      
    begin      
   select ROW_NUMBER() over(order by ticket desc) as RowNumber,
   * into #Result from (
   select TicketAutoId,TicketID,TicketDate as ticket,FORMAT(TicketDate,'MM/dd/yyyy hh:mm tt' ) as TicketDate 
   ,Priority,Type,
   Subject,Description,etm.Status,DeveloperStatus,FORMAT(TicketCloseDate,'MM/dd/yyyy hh:mm tt') as TicketCloseDate,(select firstName
+' '+LastName from EmployeeMaster where AutoId=etm.ByEmployee) as Fullname  from ErrorTicketMaster as etm  
   inner join EmployeeMaster em on em.AutoId = etm.ByEmployee  
   where   (@Status is null or @Status = '0' or etm.Status like '%'+@Status+'%')  
   and (@DeveloperStatus = '0' or @DeveloperStatus is null or DeveloperStatus like '%'+@DeveloperStatus+'%')  
   and (@EmployeeType = '0' or @EmployeeType = '' or @EmployeeType is null or em.EmpType = @EmployeeType)  
   and (@Type = '0' or @Type is null or Type=@Type)  
   and (@TicketID is null or @TicketID = '' or TicketID like '%'+@TicketID+'%')  
   and (@Subject is null or @Subject = '' or Subject like '%'+@Subject+'%')  
   and (@ByEmployee is null or @ByEmployee = '0' or ByEmployee=@ByEmployee)    
   and (@FromDate is null or @FromDate='' or @ToDate is null or @ToDate=''
    or (CONVERT(date,TicketDate) between @FromDate and @ToDate))
    ) as t  order by ticket desc
   
   SELECT * FROM #Result  
   WHERE @PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)   
   order by ROWNUMBER
   SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize=0 then COUNT(RowNumber) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Result      
    end      
  COMMIT TRAN         
 END TRY      
 BEGIN CATCH      
  ROLLBACK TRAN      
  SET @isException=1      
  SET @exceptionMessage= ERROR_MESSAGE()      
 END CATCH;      
END      
GO
