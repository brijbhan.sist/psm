USE [psmnj.a1whm.com]
GO

/****** Object:  Table [dbo].[Delete_DeliveredOrders_Backup]    Script Date: 04/14/2020 22:11:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Delete_DeliveredOrders_Backup](
	[OrderAutoId] [int] NULL,
	[PaymentAutoId] [int] NULL,
	[PayId] [int] NULL,
	[AdjustmentAmt]  AS ([DBO].[FN_DeliveredAdjustmentAmt]([OrderAutoId])),
	[GrandTotal]  AS ([DBO].[FN_DeliveredGrandTotal]([OrderAutoId])),
	[PayableAmount]  AS ([DBO].[FN_DeliveredPayableAmount]([OrderAutoId])),
	[AmtDue]  AS ([DBO].[FN_DeliveredAmtDue]([OrderAutoId])),
	[TotalAmount]  AS ([DBO].[FN_DeliveredTotalAmount]([OrderAutoId])),
	[OverallDiscAmt]  AS ([dbo].[FN_DeliveredOverallDiscAmt]([OrderAutoId])),
	[ShippingCharges]  AS ([dbo].[FN_DeliveredShippingCharges]([OrderAutoId])),
	[TotalTax]  AS ([dbo].[FN_DeliveredTotalTax]([OrderAutoId])),
	[CreditMemoAmount]  AS ([dbo].[FN_DeliveredCreditMemoAmount]([OrderAutoId])),
	[CreditAmount]  AS ([dbo].[FN_DeliveredCreditAmount]([OrderAutoId])),
	[OverallDisc]  AS ([dbo].[FN_DeliveredOverallDisc]([oRDERAutoId])),
	[MLTax]  AS ([dbo].[FN_DeliveredMLTax]([OrderAutoId])),
	[OldPaidAmount] [decimal](10, 2) NULL,
	[AdjestOrderAmount] [decimal](10, 2) NULL,
	[AmtPaid]  AS ([dbo].[FN_GETPaidAmount]([OrderAutoId]))
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Delete_DeliveredOrders_Backup] ADD  CONSTRAINT [DF_Delete_DeliveredOrders_Backup_OldPaidAmount]  DEFAULT ((0)) FOR [OldPaidAmount]
GO

ALTER TABLE [dbo].[Delete_DeliveredOrders_Backup] ADD  CONSTRAINT [DF_Delete_DeliveredOrders_Backup_AdjestOrderAmount]  DEFAULT ((0)) FOR [AdjestOrderAmount]
GO


