USE [psmnj.a1whm.com]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create FUNCTION  [dbo].[FN_DefaultAlternateEmail] 
(   
  @AutoId int   
)  
RETURNS varchar(100)  
AS  
BEGIN  
	DECLARE @AltEmail varchar(100)
	set @AltEmail=(SELECT top 1 AlternateEmail FROM CustomerContactPerson WHERE CustomerAutoId=@AutoId AND ISDefault=1)
	RETURN @AltEmail  
END 
--Alter table CustomerMaster Drop column AltEmail
--Alter table CustomerMaster add AltEmail As ([dbo].[FN_DefaultAlternateEmail](AutoId))