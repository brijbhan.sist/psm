 ALTER PROCEDURE [dbo].[ProcPriceLevelMaster]        
@Opcode int=null,        
@PriceLevelId VARCHAR(12)=NULL,        
@PriceLevelName VARCHAR(50)=NULL,        
@Status INT=NULL,        
@CreatedDate DATE=NULL,        
@PriceLevelAutoId INT=NULL,        
@ProductAutoId INT=NULL,        
@UnitAutoId INT=NULL,        
@CustomPrice DECIMAL(8,2)=NULL,        
@EmpAutoId INT=NULL,        
@CategoryAutoId INT= NULL,        
@SubcategoryAutoId INT= NULL,        
@ProductName varchar(50)=NULL,        
@ActionType INT=NULL,        
@isException bit out,        
@exceptionMessage varchar(max) out,        
@CustomerType int=null,  
@PageIndex INT = 1,        
@PageSize INT = 10,        
@RecordCount INT =null                
AS        
BEGIN        
 BEGIN TRY        
  Set @isException=0        
  Set @exceptionMessage='Success'   
  Declare @query nvarchar(MAX),@ChildDatabase varchar(50)
  select @ChildDatabase=ChildDB_Name from CompanyDetails
  IF @Opcode = 11        
	BEGIN TRY        
		BEGIN TRAN        
			IF NOT EXISTS(SELECT [PriceLevelName] FROM [PriceLevelMaster] WHERE [PriceLevelName]=@PriceLevelName )  
			BEGIN  
				SET @PriceLevelId = (SELECT DBO.SequenceCodeGenerator('CustomPriceTemp'))
				INSERT INTO [dbo].[PriceLevelMaster] ([PriceLevelId],[PriceLevelName],PCustomerType,[Status],[CreatedDate],[CreatedBy])        
				VALUES (@PriceLevelId,@PriceLevelName,@CustomerType,@Status,getdate(),@EmpAutoId)  
				UPDATE SequenceCodeGeneratorMaster SET currentSequence = currentSequence + 1 WHERE SequenceCode='CustomPriceTemp'
				SET @PriceLevelAutoId=SCOPE_IDENTITY()
				SELECT @PriceLevelAutoId AS PriceLevelAutoId  
				
				SET @query='SELECT @PriceLevelId=['+@ChildDatabase+'].[dbo].SequenceCodeGenerator(''CustomPriceTemp'')'
				Exec sp_executesql @query,N'@PriceLevelId nvarchar(max) OUTPUT',@PriceLevelId OUTPUT
				
				SET @query='
				SET IDENTITY_INSERT ['+@ChildDatabase+'].[dbo].[PriceLevelMaster] ON;
				INSERT INTO ['+@ChildDatabase+'].[dbo].[PriceLevelMaster] (AutoId,[PriceLevelId],[PriceLevelName],PCustomerType,[Status],[CreatedDate],[CreatedBy])        
				SELECT AutoId,'''+Convert(varchar(150),@PriceLevelId)+''',[PriceLevelName],PCustomerType,[Status],[CreatedDate],[CreatedBy]
				FROM PriceLevelMaster where AutoId='+Convert(varchar(10),@PriceLevelAutoId)+'
				SET IDENTITY_INSERT ['+@ChildDatabase+'].[dbo].[PriceLevelMaster] OFF;
				
				UPDATE ['+@ChildDatabase+'].[dbo].SequenceCodeGeneratorMaster SET currentSequence = currentSequence + 1 WHERE 
				SequenceCode=''CustomPriceTemp''
				'
				Exec sp_executesql @query
			END  
			ELSE  
			BEGIN  
				Set @isException=1        
				Set @exceptionMessage='Exists'  
			END     
		COMMIT TRANSACTION        
	END TRY        
	BEGIN CATCH        
		ROLLBACK TRAN        
		Set @isException=1        
		Set @exceptionMessage='Error'  
	END CATCH           
  ELSE IF @Opcode = 41        
   BEGIN        
    SELECT ROW_NUMBER() OVER(ORDER BY (CASE WHEN CustomPrice >0 THEN 1 ELSE 0 END )  DESC,ProductId ) AS RowNumber, * INTO #Results from        
    (        
    SELECT distinct t.CHK AS CHK,PM.[AutoId] AS ProductAutoId,[ProductId],[ProductName],
	CASE when @CustomerType=2 then whminprice 
	when @CustomerType=3 then pd.costprice
	ELSE PD.MinPrice END as MinPrice
	,PD.Price,        
    UM.[UnitType],PD.UnitType As UnitAutoId,t.CustomPrice         
    FROM [dbo].[ProductMaster] AS PM        
    INNER JOIN [dbo].[PackingDetails] AS PD ON PD.ProductAutoId = PM.AutoId and pm.PackingAutoId=pd.UnitType        
    INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = PD.UnitType        
    left join (        
                SELECT [ProductAutoId],[UnitAutoId],[CustomPrice], 1 as CHK FROM [dbo].[ProductPricingInPriceLevel] WHERE [PriceLevelAutoId] = @PriceLevelAutoId        
    ) as t ON t.[ProductAutoId] = PD.ProductAutoId AND t.[UnitAutoId] = PD.UnitType        
    WHERE (@CategoryAutoId IS NULL OR @CategoryAutoId=0 OR PM.[CategoryAutoId] = @CategoryAutoId)         
      AND (@SubcategoryAutoId IS NULL OR @SubcategoryAutoId=0 OR PM.[SubcategoryAutoId] = @SubcategoryAutoId)              
      AND (@ProductName IS NULL OR @ProductName='' OR replace(PM.ProductName,' ','') LIKE '%' + replace(@ProductName,' ','') + '%')       
  AND(@ProductAutoId IS NULL OR @ProductAutoId=0 or PM.ProductId like '%'+CONVERT(varchar(250),@ProductAutoId)+'%')              
    )as tb        
    order by (CASE WHEN CustomPrice >0 THEN 1 ELSE 0 END )  DESC,ProductId          
    SELECT CASE WHEN ISNULL(@PageSize,0)=0 then 0 else  COUNT(*) END as RecordCount,@PageSize as PageSize,@PageIndex as PageIndex FROM #Results        
    SELECT * FROM #Results        
    WHERE @PageSize=0 or RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1        
    ORDER BY CHK desc,[ProductId]            
   END        
  ELSE IF @Opcode = 42        
   BEGIN        
        SELECT [PriceLevelId],[PriceLevelName],PCustomerType as CustomerType,[Status] FROM [dbo].[PriceLevelMaster] WHERE [AutoId] = @PriceLevelAutoId            
   END        
  ELSE IF @Opcode = 43        
   BEGIN        
    SELECT [AutoId],[StatusType] FROM [dbo].[StatusMaster] WHERE [Category] IS NULL        
    SELECT [AutoId],[CategoryName] FROM [dbo].[CategoryMaster] ORDER BY [CategoryName] ASC  
 select * from CustomerType ORDER BY CustomerType  ASC 
   END        
  ELSE IF @Opcode = 12        
   BEGIN        
    INSERT INTO [dbo].[ProductPricingInPriceLevel] ([PriceLevelAutoId],[ProductAutoId],[UnitAutoId])        
    VALUES (@PriceLevelAutoId,@ProductAutoId,@UnitAutoId)        
   END         
  ELSE IF @Opcode = 31        
   BEGIN        
    DELETE FROM [dbo].[ProductPricingInPriceLevel] WHERE [ProductAutoId] = @ProductAutoId AND [UnitAutoId] = @UnitAutoId        
   END        
  ELSE IF @Opcode = 44        
   BEGIN        
    SELECT [ProductAutoId],[UnitAutoId],[CustomPrice] FROM [dbo].[ProductPricingInPriceLevel] WHERE [PriceLevelAutoId] = @PriceLevelAutoId        
   END        
  ELSE IF @Opcode = 21        
   BEGIN        
	declare @minPrice decimal(10,2)=( SELECT CASE when @CustomerType=2 then whminprice 
	when @CustomerType=3 then costprice
	ELSE MinPrice END as MinPrice  FROM [dbo].[PackingDetails] WHERE [ProductAutoId] = @ProductAutoId AND [UnitType] = @UnitAutoId)

    IF (@CustomPrice !=0 AND @CustomPrice !< (@minPrice))        
	BEGIN       
		declare @QtyPrice decimal(18,4)=ISNULL((select top 1 @CustomPrice/convert(decimal(10,4), Qty) from PackingDetails
		where [ProductAutoId] = @ProductAutoId AND [UnitType] = @UnitAutoId),1) 

		insert into ProductPricingInPriceLevel_log([AutoId],[PriceLevelAutoId],[ProductAutoId],[UnitAutoId],[CustomPrice],
		[createDated],[UpdatedBy])
		SELECT AutoId[AutoId],[PriceLevelAutoId],[ProductAutoId],[UnitAutoId],[CustomPrice],GETDATE(),@EmpAutoId FROM
		ProductPricingInPriceLevel WHERE ProductAutoId=@ProductAutoId and PriceLevelAutoId=@PriceLevelAutoId order by AutoId desc

		DELETE FROM [ProductPricingInPriceLevel] WHERE PriceLevelAutoId=@PriceLevelAutoId and ProductAutoId=@ProductAutoId
		
		INSERT INTO [ProductPricingInPriceLevel](PriceLevelAutoId,ProductAutoId,UnitAutoId,CustomPrice)        
		SELECT @PriceLevelAutoId,ProductAutoId,UnitType,(case when UnitType=@UnitAutoId then @CustomPrice else (@QtyPrice)*Qty end) FROM PackingDetails WHERE ProductAutoId=@ProductAutoId 		
	END        
    ELSE IF (@CustomPrice = 0)        
	BEGIN 
		insert into ProductPricingInPriceLevel_log([AutoId],[PriceLevelAutoId],[ProductAutoId],
		[UnitAutoId],[CustomPrice],[createDated],[UpdatedBy]) 
		SELECT top 1 [AutoId],[PriceLevelAutoId],[ProductAutoId],[UnitAutoId],[CustomPrice],GETDATE(),@EmpAutoId 
		FROM ProductPricingInPriceLevel_log 
		WHERE ProductAutoId=@ProductAutoId and PriceLevelAutoId=@PriceLevelAutoId order by AutoId desc
		
		UPDATE [dbo].[ProductPricingInPriceLevel] SET [CustomPrice] = NULL        
		WHERE [PriceLevelAutoId] = @PriceLevelAutoId AND [ProductAutoId] = @ProductAutoId  
	END        
    ELSE        
    BEGIN        
		Set @isException=1        
		Set @exceptionMessage='Price cannot be less than min price.'        
    END        
   END        
  ELSE If @Opcode = 22        
   BEGIN        
	IF EXISTS(SELECT [PriceLevelName] FROM [PriceLevelMaster] WHERE [PriceLevelName]=@PriceLevelName and AutoId!=@PriceLevelAutoId)  
	BEGIN  
		Set @isException=1        
		Set @exceptionMessage='Exists'
	END  
	ELSE  
	BEGIN  
		UPDATE [dbo].[PriceLevelMaster] SET [PriceLevelName] = @PriceLevelName,  
		PCustomerType=@CustomerType,[Status] = @Status WHERE [AutoId] = @PriceLevelAutoId  
		
		SET @query='
		UPDATE ['+@ChildDatabase+'].[dbo].[PriceLevelMaster] SET [PriceLevelName] = '''+Convert(varchar(250),@PriceLevelName)+''',  
		PCustomerType='+Convert(varchar(15),@CustomerType)+',[Status] = '+Convert(varchar(15),@Status)+' WHERE [AutoId] = '+Convert(varchar(15),@PriceLevelAutoId)+'  
		'
		Exec sp_executesql @query
	END         
   END        
  ELSE IF @Opcode = 45        
   BEGIN        
    SELECT [AutoId],[SubcategoryName] FROM [dbo].[SubCategoryMaster] WHERE [CategoryAutoId] = @CategoryAutoId  ORDER BY SubcategoryName  
   END            
  ELSE IF @Opcode = 46        
   BEGIN        
    SELECT PLM.[AutoId] AS PriceLevelAutoId,[PriceLevelId],[PriceLevelName],SM.[StatusType],Convert(varchar(20),[CreatedDate],101) AS CreatedDate        
    FROM [dbo].[PriceLevelMaster] AS PLM        
    INNER JOIN [dbo].[StatusMaster] As SM ON SM.[AutoId] = PLM.[Status] ANd SM.Category is null WHERE        
    (@PriceLevelName is null OR @PriceLevelName='' OR [PriceLevelName] LIKE '%' + @PriceLevelName + '%') AND        
    (@Status = 0 OR [Status] = @Status)        
   END        
  ELSE IF @Opcode=14        
  BEGIN        
       DELETE FROM [dbo].[ProductPricingInPriceLevel]        
      WHERE PriceLevelAutoId = @PriceLevelAutoId        
      IF @ActionType=1        
      BEGIN        
		INSERT INTO [ProductPricingInPriceLevel](PriceLevelAutoId,ProductAutoId,UnitAutoId)        
		select distinct @PriceLevelAutoId, ProductAutoId,UnitType from PackingDetails        
      END        
  END        
        
  ELSE IF @Opcode=47        
  BEGIN        
           
  SELECT PLM.[AutoId] AS PriceLevelAutoId,[PriceLevelId],[PriceLevelName],FORMAT(GETDATE(),'MM/dd/yyyy hh:mm tt')  AS CurrentDate,        
  STUFF((        
  select ' ,'+cm.CustomerName from CustomerPriceLevel as cpl         
  inner join CustomerMaster as cm on cpl.CustomerAutoId=cm.AutoId where cpl.PriceLevelAutoId=PLM.AutoId        
  for xml path('')),1,2,'') as Customer        
  FROM [dbo].[PriceLevelMaster] AS PLM  WHERE [AutoId] = @PriceLevelAutoId 
  
   SELECT ProductId,ProductName,UM.UnitType,
   cast(PD.MinPrice as decimal(10,2)) as MinPrice,cast(PD.Price as decimal(10,2)) as Price,[CustomPrice]  FROM [dbo].[ProductPricingInPriceLevel] AS PPL         
   INNER JOIN ProductMaster  AS PM ON PM.AutoId=PPL.ProductAutoId   AND Pm.PackingAutoId =PPL.UnitAutoId     
   INNER JOIN UnitMaster AS UM ON UM.AutoId=PPL.UnitAutoId        
   INNER  JOIN PackingDetails AS PD ON PD.ProductAutoId=PPL.ProductAutoId  AND PD.UnitType=PPL.UnitAutoId        
   WHERE [PriceLevelAutoId] = @PriceLevelAutoId and ISNULL([CustomPrice],0)>0       
    order by ProductId,ProductName
           
  SELECT [Address],[Website],[MobileNo],[FaxNo],TermsCondition,Logo FROM CompanyDetails        
  END    
  
  ELSE IF @Opcode=48        
  BEGIN   
  SELECT ROW_NUMBER() OVER(ORDER BY updateDate ) AS RowNumber, * INTO #Results999 from        
    ( 
     select pl.AutoId,pl.CustomPrice,Convert(varchar(20),pl.InsertcreateDated,101) as updateDate,um.UnitType as Unit,FirstName+' '+LastName as UpdatedBy 
	 from ProductPricingInPriceLevel_log pl
	 INNER JOIN ProductMaster pm on pm.AutoId=pl.ProductAutoId
	 INNER JOIN UnitMaster um ON um.AutoId=pl.UnitAutoId
	 left join EmployeeMaster as em on em.AutoId=pl.UpdatedBy
	 where PriceLevelAutoId=@PriceLevelAutoId And ProductAutoId=@ProductAutoId 
	 ) as pll order by updateDate
	 
	 SELECT * FROM #Results999        
    WHERE @PageSize=0 or RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1 
	SELECT count(1) AS RecordCount,@PageSize AS PageSize,@PageIndex AS PageIndex FROM #Results999        
  END  
  

  ELSE IF @Opcode=49        
  BEGIN        
  
      SELECT * FROM (                                                                                                    
  
		SELECT (
  SELECT PLM.[AutoId] AS PriceLevelAutoId,[PriceLevelId],[PriceLevelName],FORMAT(GETDATE(),'MM/dd/yyyy hh:mm tt') AS CurrentDate,        
  STUFF((        
  select ' ,'+cm.CustomerName from CustomerPriceLevel as cpl         
  inner join CustomerMaster as cm on cpl.CustomerAutoId=cm.AutoId where cpl.PriceLevelAutoId=PLM.AutoId        
  for xml path('')),1,2,'') as Customer        
  FROM [dbo].[PriceLevelMaster] AS PLM  WHERE [AutoId] = @PriceLevelAutoId for json path
  ) as pl,
  
   (SELECT ProductId,ProductName,UM.UnitType,[CustomPrice],PM.P_SRP AS SRP  FROM [dbo].[ProductPricingInPriceLevel] AS PPL         
   INNER JOIN ProductMaster  AS PM ON PM.AutoId=PPL.ProductAutoId  AND PM.PackingAutoId=PPL.UnitAutoId     
   INNER JOIN UnitMaster AS UM ON UM.AutoId=PPL.UnitAutoId             
   WHERE [PriceLevelAutoId] = @PriceLevelAutoId and ISNULL([CustomPrice],0)>0  
   order by ProductId,ProductName
   for json path ) as pd ,     
          
    (SELECT [Address],[Website],[MobileNo],[FaxNo],TermsCondition,Logo FROM CompanyDetails for json path    
	) AS cd 
	
   )AS T 
   for json path
  END





 END TRY        
 BEGIN CATCH        
  Set @isException=1        
  Set @exceptionMessage=ERROR_MESSAGE()--'Oops! Something went wrong.Please try later.'  
 END CATCH        
        
END        
        
GO
