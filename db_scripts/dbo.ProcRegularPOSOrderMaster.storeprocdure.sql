
alter PROCEDURE [dbo].[ProcRegularPOSOrderMaster]                                                                   
@Opcode INT=NULL,                                                                    
@OrderAutoId INT=NULL,                                                                    
@LogAutoId int=null,                                                                    
@TaxType int =null,                                                                    
@BillingAutoId INT =null,                                                                    
@ShippingAutoId INT =null,    
@TotalCurrencyAmount decimal(10,2)=null,
@PaymentCurrencyXml xml=null,
@PaymentId VARCHAR(10)=NULL,
@PaymentAutoId INT=NULL,
@IsFreeItem INT =null,                                                                    
@OrderNo VARCHAR(15)=NULL,                                                                    
@OrderDate DATETIME=NULL,                                                                    
@DeliveryDate DATETIME=NULL,                                                                    
@CustomerAutoId INT=NULL,                                                                    
@Terms INT=NULL,                                                                    
@PaymentMenthod INT=NULL,                                                                    
@ReferenceNo VARCHAR(50)=NULL,                                                                    
@CreditAmount decimal(10,2)=null,                                                                    
@CreditMemoAmount decimal(10,2)=null,      
@PayableAmount decimal(10,2)=null,                                                                    
@CreditNo varchar(200)=null,                                                                    
@ProductAutoId INT=NULL,                                                                    
@UnitAutoId INT=NULL,                                                                    
@SalesPersonAutoId INT=NULL,                                                                    
@LoginEmpType INT=NULL,                                                                    
@PackerAutoId INT=NULL,                                                                    
@EmpAutoId INT=NULL,                                                                    
@OrderStatus INT=NULL,                                                                    
@ShippingType INT=NULL,                                                                    
@ReqQty  INT=NULL,                                                                    
@QtyPerUnit INT=NULL,                                                                    
@UnitPrice DECIMAL(8,2)=NULL,                                                                    
@SRP DECIMAL(8,2)=NULL,                                                                    
@GP DECIMAL(8,2)=NULL,                                                                    
@TaxRate DECIMAL(8,2)=NULL,                                                                    
@minprice  DECIMAL(8,2)=NULL,                                                                    
@TotalAmount DECIMAL(8,2)=NULL,                                                                    
@OverallDisc DECIMAL(8,2)=NULL,                                                                    
@OverallDiscAmt DECIMAL(8,2)=NULL,                                                                    
@ShippingCharges DECIMAL(8,2)=NULL,                                                                    
@TotalTax DECIMAL(8,2)=NULL,                                                                    
@GrandTotal DECIMAL(8,2)=NULL,                                                                    
@PaidAmount  DECIMAL(8,2)=NULL,                                                                    
@BalanceAmount  DECIMAL(8,2)=NULL,                                                                    
@CreditMemo DECIMAL(8,2)=NULL,                                                                
@StoreCredit  DECIMAL(8,2)=NULL,                                                                     
@PastDue  DECIMAL(8,2)=NULL,                                                                     
@Remarks VARCHAR(max)=NULL,                                                                    
@OrderItemAutoId INT=NULL,                                                                    
@IsExchange INT =NULL,                                                                    
@IsTaxable  INT =NULL,                                                       
@TableValue DT_POSRegularItemList readonly,                                                  
@Todate DATETIME=NULL,                          
@Fromdate DATETIME=NULL,                                                                    
@Barcode varchar(50)=NULL,                                                                    
@QtyShip INT=NULL,                                      
@PriceLevelAutoId INT=NULL,                                                                    
@PageIndex INT = 1,                                                                    
@PageSize INT = 10,                                                                    
@MLQty DECIMAL(8,2)=NULL,                                                                    
@RecordCount INT =null,  
@CheckNo varchar(500)=NULL,                                                                                     
@CheckDate DATE=NULL,  
@PaymentRemarks varchar(500)=NULL,  
@LogRemark varchar(200) = NULL,                                                                    
@DraftAutoId int =null out,
@Adjustment DECIMAL(18,2)  =null,  
@CheckSecurity varchar(30)=NULL,
@isException bit out,                                    
@exceptionMessage varchar(max) out                                                                    
AS                                                                    
BEGIN                                                              
  BEGIN TRY                                                                    
  Set @isException=0                                                                    
  Set @exceptionMessage='Success'                                                                    
  Declare @TaxValue decimal(10,2),@AllocatQty int,@ShippingTaxEnabled INT                                                   
  If @Opcode=101                                                                      
  BEGIN                                                                    
   BEGIN TRY                                                                    
    BEGIN TRAN   
			SELECT ProductAutoId,SUM(TotalPieces) AS TotalPieces INTO #TEMP FROM @TableValue AS OIM                                                                    
			GROUP BY ProductAutoId

			if Exists(Select * from #TEMP t inner join ProductMaster pm on pm.AutoId=t.ProductAutoId where t.TotalPieces > pm.Stock)
			begin
				Select ProductAutoId,ProductName,ProductId,Stock,t.TotalPieces,'Not Available' as Type from #TEMP t inner join ProductMaster pm on pm.AutoId=t.ProductAutoId 
				where t.TotalPieces > pm.Stock
				SET @exceptionMessage='Stock not available'
			end
			else if Exists(Select * from CreditMemoMaster where CreditAutoId in (select * from dbo.fnSplitString(@creditNo,',')) And Status IN (5,6))
			BEGIN
			    Select CreditNo,SalesAmount,StatusType AS Status,'InvalidCRMemo' as Message from CreditMemoMaster AS CMM 
                INNER JOIN StatusMaster AS SM ON CMM.Status=SM.AutoId AND SM.Category='CreditMaster' 
				where CMM.CreditAutoId in (select * from dbo.fnSplitString(@creditNo,',')) And Status IN (5,6)
                Set @exceptionMessage='Invalid Credit Memo'
			END
			Else
			begin
				SET @OrderNo = (SELECT DBO.SequenceCodeGenerator('OrderNumber'))                                                                     
				SET @Terms=(SELECT Terms  FROM CustomerMaster WHERE AutoId=@CustomerAutoId)                                                                    
				DECLARE @STATE INT=(SELECT State  FROM BillingAddress WHERE AutoId=@BillingAutoId )                                                                    
				Set @TaxValue =isnull((select Value from TaxTypeMaster where AutoId =@TaxType),0.00)                                                                    
				DECLARE @IsTaxApply INT=0                        
				DECLARE @Weigth_OZTax decimal(12,2)=ISNULL((select Value From Tax_Weigth_OZ),0)                  
				IF EXISTS(SELECT * FROM TaxTypeMaster WHERE AutoId=@TaxType AND State=@State)                                                              
				BEGIN                                                              
					SET @IsTaxApply=1                                                              
				END                     
				update CustomerMaster set LastOrderDate=GETDATE() where AutoId=@CustomerAutoId 
			    SET @ShippingTaxEnabled=(SELECT EnabledTax FROM ShippingType WHERE AutoId=@ShippingType) 
				 
				SET @SalesPersonAutoId=(SELECT [SalesPersonAutoId] FROM [dbo].[CustomerMaster]                                                                     
				WHERE [AutoId] = @CustomerAutoId) 
						 
				INSERT INTO [dbo].[OrderMaster] ([OrderNo],[OrderDate],[DeliveryDate],[CustomerAutoId],[Terms],                                                                    
				[SalesPersonAutoId],[OverallDiscAmt],[ShippingCharges],                                                                  
				[Status],[ShippingType],TaxType,OrderRemarks,OrderType,AmtDue,BillAddrAutoId,ShipAddrAutoId,                                                         
				DelDate,paidAmount,paymentMode,referNo,POSAutoId,TaxValue,          
				PastDue,mlTaxPer,IsTaxApply,Weigth_OZTax,ShippingTaxEnabled,PaymentRecev)                                                                    
				values(@OrderNo,getdate(),GETDATE(),@CustomerAutoId,@Terms,                                                                    
				@SalesPersonAutoId,                                                                    
				@OverallDiscAmt,@ShippingCharges,11,@ShippingType,@TaxType,@Remarks,6,                                                        
				@BalanceAmount,@BillingAutoId,@ShippingAutoId,GETDATE(),@PaidAmount,@PaymentMenthod,@ReferenceNo,                                         
				@EmpAutoId,@TaxValue,@PastDue,ISNULL((SELECT TaxRate FROM MLTaxMaster where TaxState=@STATE),0.00)                            
				,@IsTaxApply,@Weigth_OZTax,@ShippingTaxEnabled,'yes')                                                                    
                
				SET @OrderAutoId = (SELECT SCOPE_IDENTITY())
				
				IF EXISTS(SELECT AUTOID FROM CustomerMaster WHERE AutoId=@CustomerAutoId AND CustomerType=3)
				BEGIN
					UPDATE OrderMaster SET isMLManualyApply=0,IsTaxApply=0,Weigth_OZTax=0 WHERE AutoId=@OrderAutoId
				END
                                              
				INSERT INTO [dbo].[Order_Original] ([AutoId],[OrderNo],[OrderDate],[DeliveryDate],[CustomerAutoId],[Terms],                                                           
				[SalesPersonAutoId],[TotalAmount],[OverallDisc],[OverallDiscAmt],[ShippingCharges],[TotalTax],[GrandTotal],[ShippingType],                                        
				TaxType,BillAddrAutoId,ShipAddrAutoId)                                                                    
				values(@OrderAutoId,@OrderNo,getdate(),GETDATE(),@CustomerAutoId,@Terms,                                                                    
				(CASE WHEN @SalesPersonAutoId = 0 THEN (SELECT [SalesPersonAutoId] FROM [dbo].[CustomerMaster] WHERE [AutoId] = @CustomerAutoId)                                                                    
				ELSE @SalesPersonAutoId END)                                                                    
				,@TotalAmount,@OverallDisc,@OverallDiscAmt,@ShippingCharges,@TotalTax,@GrandTotal,4,@TaxType,@BillingAutoId,@ShippingAutoId)                                                                    
                                                         
				UPDATE SequenceCodeGeneratorMaster SET currentSequence = currentSequence + 1 WHERE SequenceCode='OrderNumber'                                                                        
                                                          
				INSERT INTO [dbo].[OrderItemMaster]([OrderAutoId],[ProductAutoId],[UnitTypeAutoId],[QtyPerUnit],[UnitPrice],           
				[RequiredQty],                                                                    
				[SRP],[GP],[Tax],IsExchange,QtyShip,RemainQty,TaxValue,UnitMLQty,isFreeItem,Barcode,Weight_Oz,OM_MinPrice,OM_CostPrice,BasePrice)                                                                    
				SELECT @OrderAutoId,tb.[ProductAutoId],tb.[UnitAutoId],tb.[QtyPerUnit],tb.[UnitPrice],tb.RequiredQty,pm.[P_SRP],          
				tb.[GP],tb.[Tax],IsExchange,tb.RequiredQty,0,@TaxValue,          
				case when IsExchange=0 and isFreeItem=0 then ISNULL(pm.MLQty,0) else 0 end,          
				isFreeItem,(select top 1 Barcode from ItemBarcode as ib where ib.ProductAutoId=tb.ProductAutoId and ib.UnitAutoId=tb.UnitAutoId),                    
				case when IsExchange=0 and isFreeItem=0 then ISNULL(pm.WeightOz,0) else 0 end,
				OM_MinPrice,OM_CostPrice,OM_BasePrice
				FROM @TableValue as tb                                                                    
				inner join ProductMaster as pm on pm.AutoId=tb.ProductAutoId                                                                  
                                                                          
				INSERT INTO [dbo].[OrderItems_Original]([OrderAutoId],[ProductAutoId],[UnitTypeAutoId],[QtyPerUnit],[UnitPrice],[RequiredQty],[TotalPieces],                                                                    
				[SRP],[GP],[Tax],[NetPrice],IsExchange,UnitMLQty,TotalMLQty)                              
				SELECT @OrderAutoId,tb.[ProductAutoId],tb.[UnitAutoId],tb.[QtyPerUnit],tb.[UnitPrice],tb.RequiredQty,tb.RequiredQty*[QtyPerUnit] ,pm.[P_SRP],                                                           
				tb.[GP],tb.[Tax],tb.[NetPrice],IsExchange,ISNULL(pm.MLQty,0),                                                                    
				(ISNULL(pm.MLQty,0)*(tb.RequiredQty*[QtyPerUnit])) FROM @TableValue as tb                                                                     
				inner join ProductMaster as pm on pm.AutoId=tb.ProductAutoId                                                                    
                                                                         
				SET @LogRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=1), '[OrderNo]', @OrderNo)                                                                    
				INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                                                    
				VALUES(1,@EmpAutoId,getdate(),@LogRemark,@OrderAutoId)                                                                
                                                                         
                                                                         
				SET @PriceLevelAutoId = (SELECT [PriceLevelAutoId] FROM [dbo].[CustomerPriceLevel] WHERE [CustomerAutoId] = @CustomerAutoId)                                                                    
                                                                          
				exec UPDTAE_PRICELEVEL                                                                     
				@OrderAutoId=@OrderAutoId,                                    
				@PriceLevelAutoId=@PriceLevelAutoId,
				@EmpAutoId=@EmpAutoId
                                                                         
				
	
				insert into [dbo].[ProductStockUpdateLog]([ProductId],[OledStock],[NewStock],[UserAutoId],[DateTime],ReferenceId,ActionRemark,ReferenceType)
				SELECT pm.ProductId,isnull([Stock],0),(isnull(pm.Stock,0)-(TotalPieces)),@EmpAutoId,GETDATE(),@OrderAutoId,
				'Order No-(' + @OrderNo + ') has been generated by '+(Select FirstName+' '+LastName from Employeemaster where autoid=@EmpAutoId)+'.','OrderMaster' 
				FROM #TEMP as dt
				inner join productmaster pm on dt.[ProductAutoId] = PM.[AutoId] 
			                                             
                 UPDATE PM SET PM.Stock= (ISNULL(PM.Stock,0)-(OIM.TotalPieces)) FROM ProductMaster AS PM                                                                    
				INNER JOIN #TEMP AS OIM ON OIM.ProductAutoId=PM.AutoId


				--IF EXISTS(Select * from AllowQtyPiece as AQP
				--INNER JOIN OrderItemMaster as OIM ON AQP.ProductAutoId=OIM.ProductAutoId
				--where SalesPersonAutoId=@SalesPersonAutoId AND OIM.OrderAutoId=@OrderAutoId
				--AND convert(date,AQP.AllowDate) <= convert(date,getdate()))
				--BEGIN
				--	Update AQP SET AQP.UsedQty=ISNULL(AQP.UsedQty+dt.TotalPieces,0) FROM AllowQtyPiece AS AQP
				--	INNER JOIN 
				--	(
				--	Select SUM(OIM.TotalPieces) as TotalPieces,OIM.ProductAutoId
				--	from AllowQtyPiece as AQP
				--	INNER JOIN OrderItemMaster as OIM ON
				--	AQP.ProductAutoId=OIM.ProductAutoId
				--	where SalesPersonAutoId=@SalesPersonAutoId AND OIM.OrderAutoId=@OrderAutoId
				--	AND convert(date,AQP.AllowDate) <= convert(date,getdate()) 
				--		AND ISNULL(OIM.QtyShip,0)>0
				--		group by OIM.ProductAutoId
				--	) AS dt ON AQP.SalesPersonAutoId=@SalesPersonAutoId 
				--	AND AQP.ProductAutoId=dt.ProductAutoId

				--	IF EXISTS(Select * from AllocatedPackingDetails AS APD 
				--	INNER JOIN @TableValue AS dt ON dt.ProductAutoId=APD.ProductAutoId
				--	AND dt.UnitAutoId=APD.PackingType where APD.OrderAutId=@OrderAutoId)
				--	BEGIN
				--		Update APD SET APD.AllocatedQty=ISNULL(dt.TotalPieces,0),APD.ProductAutoId=dt.ProductAutoId,
				--		APD.PackingType=dt.UnitTypeAutoId,APD.AllocateDate=GETDATE()
				--		FROM AllocatedPackingDetails AS APD INNER JOIN (
				--		Select SUM(OIM.TotalPieces) as TotalPieces,OIM.ProductAutoId,OIM.UnitTypeAutoId
				--		from AllowQtyPiece as AQP
				--		INNER JOIN OrderItemMaster as OIM ON
				--		AQP.ProductAutoId=OIM.ProductAutoId
				--		where SalesPersonAutoId=@SalesPersonAutoId AND OIM.OrderAutoId=@OrderAutoId
				--		AND convert(date,AQP.AllowDate) <= convert(date,getdate()) 
				--		AND ISNULL(OIM.QtyShip,0)>0
				--		group by OIM.ProductAutoId,OIM.UnitTypeAutoId
				--		) AS dt ON APD.OrderAutId=@OrderAutoId AND APD.ProductAutoId=dt.ProductAutoId
				--	END
				--	ELSE
				--	BEGIN
				--		INSERT INTO AllocatedPackingDetails (OrderAutId,ProductAutoId,AllocatedQty,PackingType,AllocateDate)
				--		Select @OrderAutoId,OIM.ProductAutoId,SUM(OIM.TotalPieces),OIM.UnitTypeAutoId,GetDate()
				--		from AllowQtyPiece as AQP
				--		INNER JOIN OrderItemMaster as OIM ON
				--		AQP.ProductAutoId=OIM.ProductAutoId
				--		where SalesPersonAutoId=@SalesPersonAutoId AND OIM.OrderAutoId=@OrderAutoId
				--		AND convert(date,AQP.AllowDate) <= convert(date,getdate()) 
				--		AND ISNULL(OIM.QtyShip,0)>0
				--		group by OIM.ProductAutoId,OIM.UnitTypeAutoId
				--	END
				--	END   
					IF(@StoreCredit > 0)                                                    
					BEGIN                              
							IF EXISTS(SELECT * FROM CustomerCreditMaster WHERE CustomerAutoId=@CustomerAutoId)                                                     
							BEGIN                                   
								update CustomerCreditMaster set CreditAmount =isnull(CreditAmount,0)-isnull(@StoreCredit,0) where CustomerAutoId=@CustomerAutoId    					
							END                                                    
							ELSE                                                    
							BEGIN                                                    
								INSERT INTO CustomerCreditMaster(CustomerAutoId,CreditAmount)                                                 
								VALUES(@CustomerAutoId,@StoreCredit)                                                      
							END  
				
							insert into tbl_Custumor_StoreCreditLog(CustomerAutoId,ReferenceType,ReferenceNo,Amount,
							CreatedDate,CreatedBy,Remarks)values(@CustomerAutoId,'OrderMaster',@OrderAutoId,@StoreCredit,getdate()
							,@EmpAutoid,'SC > 0') 

			                                                   
					END 
		                                       
					IF(@StoreCredit < 0)                                                    
					BEGIN                                                    
						IF EXISTS(SELECT * FROM CustomerCreditMaster WHERE CustomerAutoId=@CustomerAutoId)                                                                    
						BEGIN                                                    
							UPDATE CustomerCreditMaster SET CreditAmount=CreditAmount+((-1)*@StoreCredit) WHERE CustomerAutoId=@CustomerAutoId                                                    
						END                                                    
						ELSE                                                    
						BEGIN                           
							INSERT INTO CustomerCreditMaster(CustomerAutoId,CreditAmount)                                                                     
							VALUES(@CustomerAutoId,((-1)*@StoreCredit))                                                      
						END                                                    
                                                        
							INSERT INTO tbl_Custumor_StoreCreditLog(CustomerAutoId,CreatedBy,CreatedDate,ReferenceNo,ReferenceType
							,Amount,Remarks)VALUES(@CustomerAutoId,@EmpAutoId,GETDATE(),@OrderAutoId,'OrderMaster', @StoreCredit,'SC < 0')                                                     
					END    
		           
		                                           
					set @Remarks= REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=16),                                                                    
					'[OrderNo]',  (SELECT OrderNo FROM OrderMaster where AutoId = @OrderAutoId))                                                
					INSERT INTO CreditMemoLog(CreditAutoId,ActionAutoId,EmpAutoId,Remarks,LogDate)                                                                    
					select splitdata,16,@EmpAutoId, @Remarks,GETDATE() from dbo.fnSplitString(@creditNo,',')                                                                    
                                                                          
					UPDATE CreditMemoMaster set OrderAutoId=@OrderAutoId,PaidAmount=TotalAmount ,CompletedBy=@EmpAutoId,CompletionDate=GETDATE()                                                                   
					where CreditAutoId in (select * from dbo.fnSplitString(@creditNo,','))                                                                    
				
					select @OrderAutoId as OrderAutoId,'Available' as Type
				                                           
					delete from DraftItemMaster where DraftAutoId= @DraftAutoId                                                                    
					delete from DraftOrderMaster where DraftAutoId =@DraftAutoId                                                                    
				
					update CustomerMaster set LastOrderDate=GETDATE() where AutoId=@CustomerAutoId    
	           END   
			 if @PaidAmount > 0   
					BEGIN
					                                                                 
				                                  
					declare @PayId varchar(20)=(select dbo.SequenceCodeGenerator('SalesPaymentId'))                                                                    
					INSERT INTO SalesPaymentMaster (PayId, CustomerAutoId, ReceivedDate, ReceivedAmount, PaymentMode,                                                                    
					ReferenceId,Remarks,ReceiveDate,ReceiveBy,Status,PayType,OrderAutoId)                                                                    
					VALUES (@PayId,@CustomerAutoId,GETDATE(),@PaidAmount,@PaymentMenthod,@ReferenceNo,@Remarks,GETDATE(),@EmpAutoId,2,2,@OrderAutoId)                                      
					
					                                                                   
				                                                                   
                     
					SET @PaymentId=(SELECT [dbo].[SequenceCodeGenerator]('PaymentId'))   

					INSERT INTO CustomerPaymentDetails(refPaymentId,CustomerAutoId,PaymentId,PaymentDate,ReceivedAmount,PaymentMode,
					ReferenceNo,EmpAutoId,ReceivedBy,PaymentRemarks,TotalCurrencyAmount) 
					select PaymentAutoId,CustomerAutoId,@PaymentId,GETDATE(),ReceivedAmount,PaymentMode,@PayId,@EmpAutoId,ReceiveBy,Remarks,
					@TotalCurrencyAmount from SalesPaymentMaster
					where  PayId=@PayId     
					
					SET @PaymentAutoId=SCOPE_IDENTITY()     
					
					INSERT INTO CustomerPaymentDetailsLog (PaymentAutoId,PaymentId,PaymentDate,EmpAutoId,PaymentRemarks)
					select PaymentAutoId,PaymentId,GETDATE(),@EmpAutoId,'Payment has been settled' from CustomerPaymentDetails
					where PaymentAutoId=@PaymentAutoId
                 
					
					INSERT INTO PaymentOrderDetails(PaymentAutoId,OrderAutoId,ReceivedAmount)                                                                                              
					values( @PaymentAutoId,@OrderAutoId,@PaidAmount)                                                                                       
                                   
				    UPDATE CustomerPaymentDetails set CreditAmount=(ReceivedAmount)-(SELECT SUM(ReceivedAmount) from
					PaymentOrderDetails as pod where pod.PaymentAutoId=@PaymentAutoId)
					where PaymentAutoId=@PaymentAutoId

					If @PaymentMenthod=1
					Begin

					INSERT INTO PaymentCurrencyDetails(PaymentAutoId,CurrencyAutoId,NoOfValue,P_CurrencyValue)                                                                                
					SELECT PaymentAutoId,CurrencyAutoId,NoOfValue,CM.CurrencyValue FROM (
					select @PaymentAutoId AS PaymentAutoId,tr.td.value('CurrencyAutoId[1]','int') as CurrencyAutoId,                                                                                    
					tr.td.value('NoOfValue[1]','int') as NoOfValue,
					(Select CurrencyValue FROM CurrencyMaster where AutoId=tr.td.value('CurrencyAutoId[1]','int')) as P_CurrencyValue				
					from  @PaymentCurrencyXml.nodes('/PaymentCurrencyXml') tr(td)   
					) AS T 
					INNER JOIN CurrencyMaster as cm on cm.AutoId=T.CurrencyAutoId 
					END

					If @PaymentMenthod=2
					BEGIN
					UPDATE SalesPaymentMaster SET Status=2,chequeNo=@CheckNo,ChequeDate=@CheckDate,ChequeRemarks=@PaymentRemarks,                                               
					DateUpdate=GETDATE()                                                                          
					where PayId=@PayId        
					END
				    UPDATE SequenceCodeGeneratorMaster SET currentSequence=currentSequence+1 WHERE SequenceCode='SalesPaymentId'                                                                               
					UPDATE [dbo].[SequenceCodeGeneratorMaster] SET [currentSequence]=[currentSequence]+1 WHERE [SequenceCode]='PaymentId' 
					
					INSERT INTO tbl_OrderLog (ActionTaken,EmpAutoId,ActionDate,Remarks,OrderAutoId) 
                    values(42,@EmpAutoid,GetDate(),'Payment ID '+@PaymentId,@OrderAutoId) 

					insert into DeliveredOrders(OrderAutoId)values(@OrderAutoId)

					insert into Delivered_Order_Items([OrderAutoId],[ProductAutoId],[UnitAutoId],[QtyPerUnit],[UnitPrice],[Barcode],[QtyShip],[SRP]
					,[GP],[Tax],[IsExchange],[TaxValue],[isFreeItem],[UnitMLQty],[Del_CostPrice],[Del_Weight_Oz],[Del_MinPrice]
					,FreshReturnQty,FreshReturnUnitAutoId,DamageReturnQty,DamageReturnUnitAutoId,MissingItemQty,MissingItemUnitAutoId,BasePrice)
					select OrderAutoId,ProductAutoId,UnitTypeAutoId,QtyPerUnit,UnitPrice,Barcode,QtyShip,SRP,GP,Tax,IsExchange,TaxValue,isFreeItem
					,UnitMLQty,OM_CostPrice,Weight_Oz,OM_MinPrice,0,UnitTypeAutoId,0,UnitTypeAutoId,0,UnitTypeAutoId,BasePrice
					from OrderItemMaster where OrderAutoId=@OrderAutoId
					END
			   
	COMMIT TRANSACTION                                                                    
	END TRY                                                                    
	BEGIN CATCH                                                                
		ROLLBACK TRAN                                                                    
		Set @isException=1                                                                    
		Set @exceptionMessage=ERROR_MESSAGE()                                                      
   End Catch                                                                     
  END                                                                    	                                                                                                                                                                                               
   ELSE IF @Opcode=403                                                                    
   BEGIN   
   
   	DECLARE @custType2 int=(SELECT CustomerType FROM CustomerMaster WHERE AutoId=@CustomerAutoId)  

    SELECT 
	CASE WHEN @custType2=2 THEN ISNULL(WHminPrice,0)
		 WHEN @custType2=3 THEN ISNULL(CostPrice,0)
		 ELSE [MinPrice] END as [MinPrice],

	convert(decimal(10,2),[CostPrice]) as [CostPrice],
	convert(decimal(10,2),[Price]) as [Price],CONVERT(DECIMAL(10,2),((PM.[P_SRP]-([Price]/(CASE WHEN QTY=0 then 1 else [Qty] end)))/(CASE WHEN PM.[P_SRP]=0 then 1 else PM.[P_SRP] end )) * 100) AS GP,                                                                 
    isnull((select Top 1 [CustomPrice] from [dbo].[ProductPricingInPriceLevel] AS PPL where PPL.[ProductAutoId] = PD.[ProductAutoId] AND PPL.[UnitAutoId] = PD.[UnitType]                                                                 
    AND [PriceLevelAutoId] = (SELECT [PriceLevelAutoId] FROM [dbo].[CustomerPriceLevel] WHERE [CustomerAutoId]=@CustomerAutoId)                                                                    
    Order by [CustomPrice] desc),0) as [CustomPrice],PM.[Stock]/(CASE WHEN Qty=0 then 1 else Qty end)  as Stock,    
	PM.[TaxRate],convert(decimal(10,2),PM.[P_SRP]) as [SRP] FROM [dbo].[PackingDetails] As PD                                                                     
    INNER JOIN [dbo].[ProductMaster] AS PM ON PD.[ProductAutoId]=PM.[AutoId]                                                                        
    WHERE PD.[ProductAutoId] = @ProductAutoId and PD.[UnitType] = @UnitAutoId                                                                    
   END                                                                     
  ELSE IF @Opcode=404                                                                    
   BEGIN                     
    SELECT pm.ProductName,UM.AutoId AS AutoId, UM.[UnitType] AS UnitType, [Qty],(Stock /(CASE WHEN pd.Qty=0 then 1 else PD.Qty end) )as AvailableQty,                                                                    
    Price,EligibleforFree,pm.Stock as PStock  FROM [dbo].[PackingDetails] AS PD                                                                    
    INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = PD.UnitType                                                                    
    INNER JOIN ProductMaster as pm on pm.AutoId =PD.[ProductAutoId] WHERE PD.[ProductAutoId] = @ProductAutoId  
	and pd.PackingStatus=1
    SELECT PackingAutoId AS AutoId FROM ProductMaster  WHERE  AutoId=@ProductAutoId                                                                    
   END                                                                                                                                      
  ELSE IF @Opcode=42                                                                    
   BEGIN                                                
    SET @CustomerAutoId= (SELECT [CustomerAutoId] FROM [dbo].[OrderMaster] WHERE AutoId=@OrderAutoId)                                                                    
    
	SELECT BA.[AutoId] As BillAddrAutoId,(BA.[Address] + ' - ' +  St.[StateName]  + ' - ' + BA.[City] + ' - ' + BA.[Zipcode] )
	as BillingAddress,IsDefault FROM [dbo].[BillingAddress] AS BA                                                                  
    INNER JOIN [dbo].[State] AS St ON St.AutoId = BA.State WHERE BA.[CustomerAutoId] = @CustomerAutoId                                                                    
    
	SELECT SA.[AutoId] As ShipAddrAutoId,(SA.[Address]+ ' - ' + St.[StateName] + ' - ' + SA.[City]+ ' - ' + SA.[Zipcode] )
	as ShippingAddress,IsDefault FROM [dbo].[ShippingAddress] AS SA                                                                    
    INNER JOIN [dbo].[State] AS St ON St.AutoId = SA.State WHERE SA.CustomerAutoId = @CustomerAutoId                                                                    
                                                                           
    SET @OrderStatus = (SELECT [Status] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId)                                                                        
                                                                         
	SELECT CM.CustomerType,OM.[AutoId],OM.[OrderNo],CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,                                                                    
	ISNULL((CONVERT(VARCHAR(20), OM.[DeliveryDate], 101)),                                                              
	(CONVERT(VARCHAR(20), OM.DelDate,101) + ' ' + convert(varchar(10), DelDate, 108))                                                         
	) AS DeliveryDate,                                                                    
    OM.[CustomerAutoId],CM.CustomerName,CT.[TermsDesc],OM.[BillAddrAutoId],OM.[ShipAddrAutoId],OM.[Status] As StatusCode,DO.[AmtPaid],DO.[AmtDue],                                                                    
    SM.[StatusType] AS Status,OM.[TotalAmount],OM.[OverallDisc],OM.[OverallDiscAmt],OM.[ShippingCharges],OM.[TotalTax],OM.[GrandTotal]
	,OM.[PackedBoxes],CommentType,Comment, isnull(OM.AdjustmentAmt,0.00) as AdjustmentAmt,                
    OM.[Driver],EM.[FirstName]+ ' ' + EM.[LastName] AS DrvName,SALES.[FirstName]+ ' ' + SALES.[LastName] AS SALESNAME,ST.ShippingType,
	CONVERT(VARCHAR(20),                                                                    
    OM.[AssignDate],101) AS AssignDate,[DrvRemarks],isnull(DeductionAmount,0.00) as DeductionAmount,                                                                    
    isnull(OM.CreditAmount,0.00)as CreditAmount,isnull(OM.PayableAmount,0.00) as PayableAmount,TaxType                                                                       
    ,ISNULL(paidAmount,0) as paidAmount,PTM.paymentMode as PaymentType,referNo,OrderRemarks,                      
    (SELECT ccm.CreditAmount FROM CustomerCreditMaster as ccm WHERE ccm.CustomerAutoId=OM.CustomerAutoId)                                               
    as StoreCredit,ISNULL(PastDue,0.00) as PastDue ,ISNULL(MLQty,0) as MLQty  ,ISNULL(om.MLTax,0) as MLTax,Weigth_OZQty,Weigth_OZTaxAmount FROM [dbo].[OrderMaster] As OM                                                                    
    INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.Status AND SM.[Category] = 'OrderMaster'                                                      
    INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                        
    INNER JOIN [CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]
	LEFT JOIN [dbo].ShippingType as ST on ST.AutoId=OM.ShippingType
	LEFT JOIN PAYMENTModeMaster as PTM on PTM.AutoID=OM.paymentMode
    LEFT JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[Driver]                                                                     
    LEFT JOIN [dbo].[EmployeeMaster] AS SALES ON SALES.[AutoId] = cm.SalesPersonAutoId                                                                    
    left JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId]                                                                    
    WHERE OM.AutoId = @OrderAutoId                                             
    IF EXISTS(SELECT [AutoId] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId AND ([PaymentRecev] IS NOT NULL OR [PaymentRecev] != ''))                      
	BEGIN  	
	    SELECT PM.[AutoId] AS ProductAutoId,PM.[ProductId],PM.[ProductName],UM.AutoId AS UnitAutoId,UM.[UnitType],
		OIM.[QtyPerUnit],isnull(OIM.Del_MinPrice,0.00) as OM_MinPrice,isnull(OIM.Del_CostPrice,0.00) as OM_CostPrice,                                                                    
		OIM.[UnitPrice],convert(decimal(10,2),UnitPrice/QtyPerUnit) as PerpiecePrice,                                           
		isnull(OIM.Del_MinPrice,0.00) as [MinPrice],isnull(OIM.BasePrice,0.00) as BasePrice,                                  
		PD.[Price],OIM.[QtyShip] as RequiredQty,[TotalPieces],OIM.[SRP],OIM.[GP],OIM.[Tax],OIM.[NetPrice],OIM.[Barcode],OIM.[QtyShip],                                                                    
		0 as RemainQty,IsExchange,isFreeItem,UnitMLQty,isnull((pm.WeightOz),0) as WeightOz FROM [dbo].[Delivered_Order_Items] AS OIM                                                                     
		INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                                                                    
		INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitAutoId                                                                     
		INNER JOIN [dbo].[PackingDetails] AS PD ON PD.[ProductAutoId] = OIM.[ProductAutoId] AND PD.[UnitType] = OIM.UnitAutoId                                                                     
		WHERE [OrderAutoId]=@OrderAutoId   
	END
	ELSE
	BEGIN                                  
    	SELECT PM.[AutoId] AS ProductAutoId,PM.[ProductId],PM.[ProductName],UM.AutoId AS UnitAutoId,UM.[UnitType],OIM.[QtyPerUnit],isnull(OIM.OM_MinPrice,0.00) as OM_MinPrice,isnull(OIM.OM_CostPrice,0.00) as OM_CostPrice,                                                                    
		OIM.[UnitPrice],convert(decimal(10,2),UnitPrice/QtyPerUnit) as PerpiecePrice,                                           
		isnull(OIM.OM_MinPrice,0.00) as [MinPrice],isnull(OIM.BasePrice,0.00) as BasePrice,                                   
		PD.[Price],OIM.[RequiredQty],[TotalPieces],OIM.[SRP],OIM.[GP],OIM.[Tax],OIM.[NetPrice],OIM.[Barcode],OIM.[QtyShip],                                                                    
		OIM.[RemainQty],IsExchange,isFreeItem,UnitMLQty,isnull((pm.WeightOz),0) as WeightOz FROM [dbo].[OrderItemMaster] AS OIM                                                                     
		INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                                                                    
		INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitTypeAutoId                                                                     
		INNER JOIN [dbo].[PackingDetails] AS PD ON PD.[ProductAutoId] = OIM.[ProductAutoId] AND PD.[UnitType] = OIM.UnitTypeAutoId                                                                     
		WHERE [OrderAutoId]=@OrderAutoId                                                                     
    END                                                                  
	select OrderAutoId,CreditAutoId,CreditNo,CONVERT(VARCHAR(10),CreditDate,101) AS CreditDate,ISNULL(TotalAmount,0.00) as ReturnValue,ISNULL(PaidAmount,0.00)  as amtDeducted,                                                                    
	ISNULL(BalanceAmount,0.00) as amtDue,                                                                    
	CASE WHEN CreditType=1 THEN 'ERP CREDIT' ELSE 'POS CREDIT' END AS CreditType                                                
	from CreditMemoMaster AS CM  where [OrderAutoId]=@OrderAutoId                                                                                    
	SELECT MLTaxPer as TaxRate FROM [dbo].[OrderMaster] As OM WHERE OM.AutoId = @OrderAutoId                    
                   
	select * from Tax_Weigth_OZ
	                                              
   END                                                                                                                               
  ELSE IF @Opcode=41                                                                    
   BEGIN                                                                           
	  SELECT BA.[AutoId] As BillAddrAutoId,(BA.[Address] + ' - ' +  St.[StateName]  + ' - ' + BA.[City] + ' - ' + BA.[Zipcode] ) as BillingAddress,IsDefault FROM [dbo].[BillingAddress] AS BA                                                                    
	  INNER JOIN [dbo].[State] AS St ON St.AutoId = BA.State WHERE BA.[CustomerAutoId] = @CustomerAutoId --AND BA.[IsDefault]=1  
	  
	  SELECT SA.[AutoId] As ShipAddrAutoId,(SA.[Address]+ ' - ' + St.[StateName] + ' - ' + SA.[City]+ ' - ' + SA.[Zipcode] ) as ShippingAddress,IsDefault FROM [dbo].[ShippingAddress] AS SA                                                                    
	  INNER JOIN [dbo].[State] AS St ON St.AutoId = SA.State WHERE SA.CustomerAutoId = @CustomerAutoId --AND SA.[IsDefault]=1                                                                    
                                                             
	  SELECT CreditAmount FROM CustomerCreditMaster WHERE CustomerAutoId = @CustomerAutoId   
	  
	  SELECT EMP.FirstName+ '  ' +EMP.LastName AS EMPNAME,CustomerType  FROM CustomerMaster AS CM INNER JOIN                                                                    
	  EmployeeMaster AS EMP ON EMP.AutoId=CM.SalesPersonAutoId WHERE cm.AutoId = @CustomerAutoId                                     
           
	  
		select OrderAutoId,CreditAutoId,CreditNo,CONVERT(VARCHAR(10),CreditDate,101) AS CreditDate,ISNULL(TotalAmount,0.00) as ReturnValue,ISNULL(PaidAmount,0.00)  as amtDeducted,                                                                    
		ISNULL(BalanceAmount,0.00) as amtDue,                                                                    
		CASE WHEN CreditType=1 THEN 'ERP CREDIT' ELSE 'POS CREDIT' END AS CreditType                                                                    
		from CreditMemoMaster AS CM  where Status =3 AND CustomerAutoId=@CustomerAutoId and OrderAutoId is null  	  
	    Union
		select OrderAutoId,CreditAutoId,CreditNo,CONVERT(VARCHAR(10),CreditDate,101) AS CreditDate,ISNULL(TotalAmount,0.00) as ReturnValue,ISNULL(PaidAmount,0.00)  as amtDeducted,                                                                    
		ISNULL(BalanceAmount,0.00) as amtDue,                                                                    
		CASE WHEN CreditType=1 THEN 'ERP CREDIT' ELSE 'POS CREDIT' END AS CreditType                                                
		from CreditMemoMaster AS CM  where [OrderAutoId]=@OrderAutoId 
	  
	  SELECT CreditAmount FROM CustomerCreditMaster WHERE CustomerAutoId=@CustomerAutoId                                                  
                                                                                    
	  SELECT CustomerAutoId,[AutoId],[OrderNo],CONVERT(VARCHAR(20),[OrderDate],101) AS OrderDate,DO.[AmtDue],DO.[GrandTotal],DO.[AmtPaid]                                                                    
	  FROM [dbo].[OrderMaster] As OM                                                                    
	  INNER JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId]                                                                         
	  WHERE [CustomerAutoId] = @CustomerAutoId AND                                   
	   [Status] = 11 AND DO.[AmtDue] > 0.00    
	   
	  set @ShippingAutoId=(select State from ShippingAddress where CustomerAutoId=@CustomerAutoId and IsDefault=1) 
	  
	  SELECT * FROM TaxTypeMaster where status=1 and State=@ShippingAutoId                                                                    
                                                                        
	  SET @TaxType = (SELECT TOP 1 State FROM BillingAddress  WHERE CustomerAutoId=@CustomerAutoId and IsDefault=1) 
	  
	  SELECT ISNULL((SELECT TaxRate FROM MLTaxMaster WHERE TaxState=@TaxType),0) as TaxRate,                                            
	  CASE WHEN (SELECT TaxRate FROM MLTaxMaster WHERE TaxState=@TaxType) IS NULL THEN 0 ELSE 1 END                                           
	  AS MLTaxType                                          
                    
	  select * from Tax_Weigth_OZ                       
                                                          
   END                                                                                                                                                                                                      
	ELSE IF @Opcode=108                                                                    
    BEGIN 
		DECLARE @custType1 int,@AllowQtyInPieces int,@AllocationStatus int=1
		SET @custType1=(SELECT CustomerType FROM CustomerMaster	WHERE AutoId=@CustomerAutoId)
		SET @SalesPersonAutoId=(SELECT SalesPersonAutoId FROM CustomerMaster WHERE AutoId=@CustomerAutoId)
		
		Declare @availQty int=0

		IF EXISTS(SELECT * FROM DraftItemMaster WHERE DraftAutoId= @DraftAutoId AND ProductAutoId=@ProductAutoId AND                                                                                                      
			UnitAutoId=@UnitAutoId AND IsExchange=@IsExchange AND IsFreeItem=@IsFreeItem)                                                                 
		BEGIN  
			SET @availQty=ISNULL((SELECT ISNULL(ReqQty,0) FROM DraftItemMaster 
			WHERE DraftAutoId= @DraftAutoId AND ProductAutoId=@ProductAutoId AND                                                                                                      
			UnitAutoId=@UnitAutoId AND IsExchange=@IsExchange AND IsFreeItem=@IsFreeItem),0)
		END
		declare @Customrice decimal (10,2)=( SELECT top 1                                                  
		PPL.[CustomPrice] FROM [dbo].[PackingDetails] As PD                                                                     
		INNER JOIN [dbo].[ProductMaster] AS PM ON PD.[ProductAutoId]=PM.[AutoId]                                       
		LEFT JOIN [dbo].[ProductPricingInPriceLevel] AS PPL ON PPL.[ProductAutoId] = PD.[ProductAutoId] AND PPL.[UnitAutoId] = PD.[UnitType]                                                                    
		AND [PriceLevelAutoId] = (SELECT [PriceLevelAutoId] FROM [dbo].[CustomerPriceLevel] WHERE                                                                     
		[CustomerAutoId]=@CustomerAutoId)                      
		WHERE PD.[ProductAutoId] = @ProductAutoId and PD.[UnitType] = @UnitAutoId order by [CustomPrice] desc                                                                    
		)                                                                                                                      
		SET @UnitPrice =( SELECT CASE WHEN @custType1=3 then ISNULL(CostPrice,0) else   [Price] end                                                                    
		FROM [dbo].[PackingDetails] As PD                                                                     
		INNER JOIN [dbo].[ProductMaster] AS PM ON PD.[ProductAutoId]=PM.[AutoId]      
		WHERE PD.[ProductAutoId] = @ProductAutoId and PD.[UnitType] = @UnitAutoId                                                                    
		)                             
                                                                                                                        
		SET @minprice =(SELECT (                                                                    
		CASE                                                                     
		WHEN @custType1=2 THEN ISNULL(WHminPrice,0)                                                                    
		WHEN @custType1=3 THEN ISNULL(CostPrice,0)                                                              
		ELSE [MinPrice] END) as [MinPrice]  FROM [dbo].[PackingDetails] As PD                                                                     
		INNER JOIN [dbo].[ProductMaster] AS PM ON PD.[ProductAutoId]=PM.[AutoId]                                                             
		WHERE PD.[ProductAutoId] = @ProductAutoId and PD.[UnitType] = @UnitAutoId                                                                    
		)        
	  
		set @UnitPrice = (case when @Customrice IS NULL then @UnitPrice else ( case when @Customrice < @minprice then  @UnitPrice else  @Customrice end) end)                                                                    
                                                                          
		SET @QtyPerUnit =( SELECT Qty FROM [dbo].[PackingDetails] As PD                                                                            
		WHERE PD.[ProductAutoId] = @ProductAutoId and PD.[UnitType] = @UnitAutoId                                                                    
		)                                                                    
		SET @SRP =ISNULL((SELECT PM.[P_SRP] FROM [dbo].[ProductMaster] As PM WHERE PM.[AutoId] = @ProductAutoId),0)        
		SET @GP =( SELECT                                                                     
		CONVERT(DECIMAL(10,2),((pm.[P_SRP]-([Price]/(CASE WHEN [Qty]=0 then 1 ELSE Qty end)))/(CASE WHEN pm.[P_SRP]=0 then 1 else pm.[P_SRP] end)) * 100) AS GP                                                                
		FROM [dbo].[PackingDetails] As PD         
		inner join ProductMaster as pm on pm.AutoId=pd.ProductAutoId                                                            
		WHERE PD.[ProductAutoId] = @ProductAutoId and PD.[UnitType] = @UnitAutoId                                       
		)                                                                    
		SET @TaxRate =( SELECT PM.[TaxRate] FROM  [dbo].[ProductMaster] AS PM WHERE PM.[AutoId] = @ProductAutoId                                     
		)   	
		
		
		IF EXISTS(SELECT * FROM AllowQtyPiece WHERE SalesPersonAutoId=@SalesPersonAutoId 
		and ProductAutoId=@ProductAutoId)  
	    BEGIN  
			IF EXISTS(select * from AllowQtyPiece where CAST(AllowDate AS datetime) <= CAST(getdate() AS datetime) 
			AND SalesPersonAutoId=@SalesPersonAutoId AND ProductAutoId=@ProductAutoId)  
			BEGIN  
				SELECT @AllowQtyInPieces=(AllowQtyInPieces-UsedQty) FROM AllowQtyPiece WHERE SalesPersonAutoId=@SalesPersonAutoId
				and ProductAutoId=@ProductAutoId  
				IF (@AllowQtyInPieces>=@QtyPerUnit*(@ReqQty+ISNUll(@availQty,0)))
				BEGIN  
					SET @AllocationStatus=1 
				END
				ELSE
				BEGIN
				     SET @AllocationStatus=0
				END
			END 
        END
		--SELECT @AllowQtyInPieces,@QtyPerUnit*(@ReqQty+ISNUll(@availQty,0))
	  IF @AllocationStatus=1
	  BEGIN
		IF @DraftAutoId=0                                                                    
		BEGIN                                                                    
			INSERT INTO DraftOrderMaster([EmpAutoId],[CustomerAutoId],[OrderDate],[TotalItem],[ShippingType],[DeliveryDate],OrderType,Remarks)                                                             
			VALUES(@EmpAutoId,@CustomerAutoId,GETDATE(),1,@ShippingType,GETDATE(),6,@Remarks)                                                                    
			SET @DraftAutoId=SCOPE_IDENTITY()                                                                    
			IF @IsExchange=1 or @IsFreeItem=1                                                                    
			BEGIN                                                                    
				SET @UnitPrice=0                                                                    
				SET @MLQty=0                                                   
			END                                                                    
			INSERT INTO DraftItemMaster(DraftAutoId,ProductAutoId,UnitAutoId,ReqQty,QtyPerUnit,UnitPrice,minprice,SRP,GP,TaxRate,IsExchange,isFreeItem,IsTaxable,UnitMLQty,TotalMLQty)                                                                     
			VALUES(@DraftAutoId,@ProductAutoId,@UnitAutoId,@ReqQty,@QtyPerUnit,@UnitPrice,@minprice,@SRP,@GP,@IsTaxable,@IsExchange,@IsFreeItem,@IsTaxable,@MLQty,(@MLQty*@ReqQty))                                                                    
		END                                                                    
		ELSE                                                                     
		BEGIN                                                                
			IF NOT EXISTS(SELECT * FROM DraftItemMaster WHERE DraftAutoId= @DraftAutoId AND ProductAutoId=@ProductAutoId                                                                  
			AND UnitAutoId=@UnitAutoId AND IsExchange=@IsExchange AND IsFreeItem=@IsFreeItem)                                                                    
			BEGIN                                                   
				IF @IsExchange=1 or @IsFreeItem=1                                                                    
				BEGIN                                                                    
					SET @MLQty=0                                                                    
					SET @UnitPrice=0                                                                    
				END                                          
				INSERT INTO DraftItemMaster(DraftAutoId,ProductAutoId,UnitAutoId,ReqQty,QtyPerUnit,UnitPrice,minprice,SRP,GP,TaxRate,IsExchange,IsFreeItem,IsTaxable,UnitMLQty,TotalMLQty)                                                                  
				VALUES(@DraftAutoId,@ProductAutoId,@UnitAutoId,@ReqQty,@QtyPerUnit,@UnitPrice,isnull(@minprice,0),isnull(@SRP,0),isnull(@GP,0),@IsTaxable,@IsExchange,@IsFreeItem,@IsTaxable,@MLQty,(@MLQty*@ReqQty))                                             
				UPDATE DraftOrderMaster SET [TotalItem]=[TotalItem]+1,Remarks=@Remarks WHERE DraftAutoId=@DraftAutoId                                                                    
			END                                                                    
			else                                                             
			BEGIN                                                                    
				UPDATE DraftItemMaster SET ReqQty=ReqQty+@ReqQty,TotalMLQty=(@ReqQty*UnitMLQty) WHERE  DraftAutoId= @DraftAutoId                                                                    
				AND ProductAutoId=@ProductAutoId AND UnitAutoId=@UnitAutoId and IsExchange=@IsExchange AND IsFreeItem=@IsFreeItem 
			END                                                                
		END        
		update DraftOrderMaster set Remarks=@Remarks,ShippingType=@ShippingType where DraftAutoId=@DraftAutoId
		SELECT @DraftAutoId as DraftAutoId ,PM.AutoId,convert(varchar(20),[ProductId]) + '--' + [ProductName] as [ProductName],PD.UnitType,Qty,@ReqQty AS ReqQty,                                                                    
		isnull(@Customrice,@UnitPrice) as CustomPrice,isnull(@UnitPrice,0) AS UnitPrice,                                                       
		isnull(@minPrice,0) AS minPrice,isnull(@UnitPrice,0) AS BasePrice,@SRP AS SRP,@GP AS GP,@TaxRate AS taxRate,                                                                    
		UM.UnitType FROM ProductMaster AS PM INNER JOIN PackingDetails AS PD ON PD.ProductAutoId =PM.AUTOID                                                                     
		INNER JOIN UnitMaster AS UM ON UM.AutoId=PD.UnitType WHERE PM.AutoId=@ProductAutoId AND PD.UnitType=@UnitAutoId                                                                    
      END
	  ELSE
	  BEGIN
	       Select @AllowQtyInPieces as AvailableQty,@QtyPerUnit*@ReqQty  as RequiredQty,
		   @ProductAutoId as ProductAutoId,'Less' as Message
	  END
	END   
	ELSE IF @Opcode=109
	BEGIN	    
	  BEGIN TRY
		SET @SalesPersonAutoId=(SELECT SalesPersonAutoId FROM CustomerMaster WHERE AutoId=@CustomerAutoId)
		SET @QtyPerUnit =( SELECT Qty FROM [dbo].[PackingDetails] As PD                                                                            
		WHERE PD.[ProductAutoId] = @ProductAutoId and PD.[UnitType] = @UnitAutoId                                                                    
		) 
		IF EXISTS(SELECT * FROM AllowQtyPiece WHERE SalesPersonAutoId=@SalesPersonAutoId 
		and ProductAutoId=@ProductAutoId)  
		BEGIN  
			IF EXISTS(select * from AllowQtyPiece where CAST(AllowDate AS date) <= CAST(getdate() AS DATE) 
			AND SalesPersonAutoId=@SalesPersonAutoId AND ProductAutoId=@ProductAutoId)  
			BEGIN  
				SELECT @AllowQtyInPieces=(AllowQtyInPieces-UsedQty) FROM AllowQtyPiece WHERE SalesPersonAutoId=@SalesPersonAutoId and ProductAutoId=@ProductAutoId  
				IF (@AllowQtyInPieces>=@QtyPerUnit*@ReqQty)  
				BEGIN  
					SET @isException=0
					SET @exceptionMessage='Available'
				END
				ELSE
				BEGIN					
					 SELECT @AllowQtyInPieces as AvailableQty,@QtyPerUnit*@ReqQty as RequiredQty,
					 @ProductAutoId as ProductAutoId,'Less' as Message
				END
			END 
		END
	   END TRY
	   BEGIN CATCH
	         SET @isException=1
			 SET @exceptionMessage=ERROR_MESSAGE()
	   END CATCH
	END
    ELSE IF @Opcode=205                                               
    BEGIN                                                                    
		UPDATE DraftOrderMaster SET CustomerAutoId=@CustomerAutoId,ShippingType=@ShippingType,Remarks=@Remarks,OverallDisc=@OverallDisc,
		OverallDiscAmt=@OverallDiscAmt,ShippingCharges=@ShippingCharges
		WHERE DraftAutoId= @DraftAutoId         
    END                                                         
    ELSE IF @Opcode=206                                                                    
    BEGIN 
	declare @dd int=null
	if((select Stock from ProductMaster where AutoId=@ProductAutoId)>=@QtyPerUnit)
	begin
		UPDATE DraftItemMaster SET  ReqQty=@ReqQty,UnitPrice=@UnitPrice,TotalMLQty=(@ReqQty*UnitMLQty)                                                                    
		WHERE  DraftAutoId= @DraftAutoId AND ProductAutoId=@ProductAutoId AND UnitAutoId=@UnitAutoId                                                                    
		and IsExchange=@IsExchange and isFreeItem=@IsFreeItem     
		update DraftOrderMaster set Remarks=@Remarks where DraftAutoId=@DraftAutoId
	end
	else
	begin
		Set @isException=1                                          
		Set @exceptionMessage='Stock is not Available.' 
	end
    END                                                                                                                                                                                         
    ELSE IF @Opcode=425                                                                    
    BEGIN                                                                    
         SELECT ShippingType,CustomerAutoId,convert(varchar,OrderDate,101) as OrderDate,'Draft' as Status,convert(varchar,DeliveryDate,101) as DeliveryDate,
		 Remarks,OverallDisc,OverallDiscAmt,ShippingCharges
		 FROM DraftOrderMaster WHERE DraftAutoId=@DraftAutoId  

		 Declare @CustomerType INT
		 SELECT @CustomerType=(SELECT CustomerType FROM CustomerMaster WHERE AutoID=(SELECT CustomerAutoId from DraftOrderMaster WHERE DraftAutoId=@DraftAutoId))

         SELECT ItemAutoId,DraftAutoId,temp1.ProductAutoId,UnitAutoId,ReqQty,QtyPerUnit,UnitPrice,
		 cast((case when @CustomerType=3 then PD.CostPrice when @CustomerType=2 then PD.WHminPrice else PD.MinPrice end)as decimal(10,2)) as minprice,
		 P_SRP as SRP,GP,temp1.TaxRate,IsExchange,isFreeItem,(ISNULL(PM.MLQty,0)) as UnitMLQty,
		 (ISNULL(PM.MLQty,0))*ReqQty*QtyPerUnit as 
		 TotalMLQty,
		 IsTaxable,NetPrice,PM.ProductId,PM.ProductName,um.UnitType,ISNULL(PM.WeightOz,0) as WeightOz,ISNULL(CostPrice,0) as CostPrice  FROM DraftItemMaster as temp1                                                                   
         INNER JOIN ProductMaster AS PM ON PM.AutoId=temp1.ProductAutoId                                                                    
         INNER JOIN UnitMaster AS UM ON UM.AutoId=temp1.UnitAutoId 
		 INNER JOIN PackingDetails AS PD ON UM.AutoId=PD.UnitType AND PD.ProductAutoId=temp1.ProductAutoId
		 WHERE DraftAutoId=@DraftAutoId                                                                    
    END      
	 ELSE IF @Opcode=501                                                                    
    BEGIN                                                                    
   Delete from DraftItemMaster WHERE DraftAutoId=@DraftAutoId and ProductAutoId=@ProductAutoId and UnitAutoId=@UnitAutoId and isFreeItem=@IsFreeItem and IsExchange=@IsExchange                                                                   
    END      
 ELSE IF @Opcode=426                                                                    
     BEGIN                                                                     
      SELECT ROW_NUMBER() OVER(ORDER BY [OrderNo] desc) AS RowNumber, * INTO #ResultsShip from                                                                    
      (                                                        
      SELECT OM.[AutoId],OM.OrderNo,CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,CM.[CustomerName] AS CustomerName,                                                                    
      SM.StatusType AS Status,OM.[Status] As StatusCode,OM.[GrandTotal],OM.ShippingType,OM.[SalesPersonAutoId],                                                                    
      (SELECT COUNT(distinct [ProductAutoId]) from [dbo].[OrderItemMaster] WHERE OrderAutoId=OM.AutoId) AS NoOfItems                                                                     
      FROM [dbo].[OrderMaster] As OM                                                                    
      INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.AutoId = OM.CustomerAutoId                                                                     
      INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.[Status] AND SM.Category = 'OrderMaster'                                                                               
      WHERE (@OrderNo is null or @OrderNo ='' or OrderNo like '%' + @OrderNo + '%')                                                                    
      and (@FromDate is null or @FromDate = '' or @Todate is null or @Todate = '' or (Convert(date,[OrderDate]) between @FromDate and @Todate))                                                    
      and (@OrderStatus is null or @OrderStatus=0 or OM.[Status]=@OrderStatus)                                                                     
      and (@SalesPersonAutoId = 0 or OM.[SalesPersonAutoId]=@SalesPersonAutoId)                                                                    
       and OM.ShippingType=@ShippingType                                                                    
      )as t order by [OrderNo]                                           
                                                                    
      SELECT COUNT(*) as RecordCount,@PageSize as PageSize,@PageIndex as PageIndex FROM #ResultsShip                                                                    
      SELECT * FROM #ResultsShip                                                           
      WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1                                                                    
     END                                                                       
  ELSE IF @Opcode=44                                                                    
	BEGIN   
		SELECT [AutoId],[StatusType] as ST,
		(
			SELECT cm.[AutoId] as  A,[CustomerId] + ' - ' + [CustomerName] +' ['+ct.CustomerType+']' As C 
			FROM [dbo].[CustomerMaster] as cm                                            
			inner join CustomerType as ct on cm.CustomerType=ct.AutoId   where status=1 AND cm.CustomerType!=3
			order by C
			for json path
		) as CustomerList,
		(
			SELECT [AutoId],convert(varchar(10),[ProductId]) + '--' + [ProductName] as [PN],MLQty as ML,isnull((WeightOz),0) as Oz,Stock as ST                                                                   
			FROM [dbo].[ProductMaster] as pm where ProductStatus=1                                      
			and (select count(1) from PackingDetails where   packingstatus=1 and ProductAutoId=pm.AutoId)>0                              
			and ISNULL(PackingAutoId,0)!=0  order by       [PN]             
			for json path
		) as ProductList,
		(
			SELECT AutoId, ShippingType as ST,EnabledTax from shippingType where Shippingstatus=1
			order by ST
			for json path
		) as ShippingType,
		(
			SELECT AutoId,PaymentMode as PM from PAYMENTModeMaster where AutoID not in (5)
			order by PM
			for json path
		) as PaymentList 
		FROM [dbo].[StatusMaster] WHERE [AutoId]=1 and [Category]='OrderMaster'
		for json path
	END 
	ELSE IF @Opcode=45
	BEGIN
	    IF EXISTS(SELECT [SecurityValue] FROM [dbo].[TBL_MST_SecurityMaster] WHERE [SecurityValue]=@CheckSecurity and SecurityType=8 and typeEvent=2)                                
        BEGIN                                                                                     
			SET @isException=0
			SET @exceptionMessage='success'
        END
		ELSE
		BEGIN
		    SET @isException=1
			SET @exceptionMessage='Invalid'
		END
	END
	ELSE IF @Opcode=46
	BEGIN
	    SET @SalesPersonAutoId=(Select SalesPersonAutoId FROM CustomerMaster where AutoId=@CustomerAutoId)
	    IF EXISTS(SELECT * FROM AllowQtyPiece Where SalesPersonAutoId=@SalesPersonAutoId AND ProductAutoId=@ProductAutoId)                                
        BEGIN                                                                                     
			Update AllowQtyPiece SET AllowQtyInPieces=AllowQtyInPieces+@ReqQty where SalesPersonAutoId=@SalesPersonAutoId AND ProductAutoId=@ProductAutoId
        END
		ELSE
		BEGIN
		    SET @isException=1
			SET @exceptionMessage='NotExist'
		END
	END
	ElSE IF @Opcode=48
	BEGIN 
	select Autoid,Currencyname,CurrencyValue from Currencymaster where Status=1  order by   CurrencyValue desc
	END
	ELSE IF @Opcode=47
	BEGIN
	    SET @AllocationStatus=1
		IF EXISTS(SELECT * FROM [dbo].[ItemBarcode] WHERE [Barcode]=@Barcode)                                                                                                                                                    
		BEGIN                                                                                                                                      
			SET @ProductAutoId=(SELECT [ProductAutoId] FROM [dbo].[ItemBarcode] WHERE [Barcode]=@Barcode)                                                                          
			SET @SalesPersonAutoId=(Select SalesPersonAutoId FROM CustomerMaster Where AutoId=@CustomerAutoId)
			SET @UnitAutoId=(SELECT UnitAutoId FROM [dbo].[ItemBarcode] WHERE [Barcode]=@Barcode)         
			declare @TotalReqQty int=null
			set @TotalReqQty=(select Qty from PackingDetails where ProductAutoId=@ProductAutoId 
			and UnitType=@UnitAutoId)*@ReqQty

			IF EXISTS(SELECT * FROM DraftItemMaster WHERE DraftAutoId= @DraftAutoId AND ProductAutoId=@ProductAutoId AND                                                                                                      
				UnitAutoId=@UnitAutoId AND IsExchange=@IsExchange AND IsFreeItem=@IsFreeItem)                                                                 
			BEGIN  
				SET @availQty=ISNULL((SELECT ISNULL(ReqQty,0) FROM DraftItemMaster 
				WHERE DraftAutoId= @DraftAutoId AND ProductAutoId=@ProductAutoId AND                                                                                                      
				UnitAutoId=@UnitAutoId AND IsExchange=@IsExchange AND IsFreeItem=@IsFreeItem),0)*
				(select Qty from PackingDetails where ProductAutoId=@ProductAutoId and UnitType=@UnitAutoId)
			END
			IF EXISTS(SELECT * FROM AllowQtyPiece WHERE SalesPersonAutoId=@SalesPersonAutoId 
			and ProductAutoId=@ProductAutoId)  
			BEGIN  
				IF EXISTS(select * from AllowQtyPiece where CAST(AllowDate AS date) <= CAST(getdate() AS DATE) 
				AND SalesPersonAutoId=@SalesPersonAutoId AND ProductAutoId=@ProductAutoId)  
				BEGIN  
					SELECT @AllowQtyInPieces=(AllowQtyInPieces-UsedQty) FROM AllowQtyPiece 
					WHERE SalesPersonAutoId=@SalesPersonAutoId and ProductAutoId=@ProductAutoId  
					IF (@AllowQtyInPieces>=@TotalReqQty+ISNULL(@availQty,0))  
					BEGIN  
						SET @AllocationStatus=1 
					END
					ELSE
					BEGIN
							SET @AllocationStatus=0
					END
				END 
			END
           IF @AllocationStatus=1
		   BEGIN
			if ((select Stock from ProductMaster where AutoId=@ProductAutoId) >=@TotalReqQty)
			begin
				set @custType1 =(SELECT CustomerType FROM CustomerMaster WHERE AutoId=@CustomerAutoId)                                                                     
				SELECT PM.AutoId as ProductAutoId,PM.ProductId,PM.ProductName,PD.Qty as UnitQty,PD.UnitType, Stock as ProductStock,                                                       
				(CASE                                                                     
				WHEN @custType1=2 THEN ISNULL(WHminPrice,0)                                                                                                                                                     
				WHEN @custType1=3  THEN ISNULL(CostPrice,0)                                           
				ELSE [MinPrice] END) AS [MinPrice] ,                                                                                                                                                    
				ISNULL((SELECT TOP 1 UnitType +  ' ('+ CONVERT(VARCHAR(20),PD.QTY)+' pcs)' 
				from UnitMaster AS UM WHERE UM.AutoId=PD.UnitType),'') AS UnitName,[CostPrice],                                                                                                                                                    
				(case                                                                                                   
				when @IsExchange=1 or @IsFreeItem=1 then 0.00                                                                                                                                            
				WHEN  @custType1=3 then CONVERT(DECIMAL(10,2),ISNULL(CostPrice,0))                                                                                                                                                
				else CONVERT(DECIMAL(10,2),[Price]) end) as  [Price],                                                     
				CONVERT(DECIMAL(10,2),((PM.[P_SRP]-([Price]/(CASE WHEN [Qty]=0 then 1 else [Qty] end )))/(CASE WHEN ISNULL(PM.[P_SRP],0)=0 then 1 else PM.[P_SRP] END)) * 100) AS GP,                                                   
				isnull((select Top 1 [CustomPrice] from [dbo].[ProductPricingInPriceLevel] AS PPL where                                    
				PPL.[ProductAutoId] = PD.[ProductAutoId] AND PPL.[UnitAutoId] = PD.[UnitType]                                                                                                                                                    
				AND [PriceLevelAutoId] = (SELECT [PriceLevelAutoId] FROM [dbo].[CustomerPriceLevel]                                                       
				WHERE [CustomerAutoId]=@CustomerAutoId)                                     
				Order by [CustomPrice] desc),null) as [CustomPrice],PM.[Stock]/(CASE WHEN Qty=0 THEN 1 ELSE QTY END)  as Stock,PM.[TaxRate],                                                                                                 
				PM.[P_SRP] AS [SRP],                                                                                                                    
				(case when @IsExchange=1 or @IsFreeItem=1 then 0.00 else PM.MLQty end) as MLQty,
				(case when @IsExchange=1 or @IsFreeItem=1 then 0.00 else isnull(WeightOz,0) end) as WeightOz,pd.Price as BasePrice
				                     
				into #result4231 FROM [dbo].[PackingDetails] As PD                                                                                                                                                     
				INNER JOIN [dbo].[ProductMaster] AS PM ON PD.[ProductAutoId]=PM.[AutoId]                                                                                                                                                    
				WHERE PD.[ProductAutoId] = @ProductAutoId and PD.[UnitType] = @UnitAutoId                                                                                           
                                                                                                                                                          
				SET @QtyPerUnit=(SELECT TOP 1 UnitQty FROM #result4231)                                                                                                 
				SET @MLQty=(SELECT TOP 1 MLQty FROM #result4231)                                                                                                                                                    
				SET @UnitPrice=(SELECT TOP 1 Price FROM #result4231)                        
				SET @minprice=(SELECT TOP 1 MinPrice FROM #result4231)                                                                                                                 
				SET @SRP=(SELECT TOP 1 SRP FROM #result4231)                                                                                                                                  
				SET @GP=(SELECT TOP 1 GP FROM #result4231)                                                                                 
				set @Customrice=((SELECT TOP 1 [CustomPrice] FROM #result4231))                                      
				set @UnitPrice = (                                                                                                                           
				case                                                                                                                                                  
				when @IsExchange=1 or @IsFreeItem=1 then @UnitPrice                                                              
				when @Customrice IS NULL then @UnitPrice                                                                                                   
				else ( case when @Customrice < @minprice then  @UnitPrice else  @Customrice end)                                                                                                                                                  
				end                                                                         
				)  
				IF ISNULL(@DraftAutoId,0)=0                                                                                                                                  
				BEGIN                                                                                                                                                    
					INSERT INTO DraftOrderMaster([EmpAutoId],[CustomerAutoId],[OrderDate],[TotalItem],[ShippingType],[DeliveryDate],OrderType,Remarks)                                                                          
					VALUES(@EmpAutoId,@CustomerAutoId,GETDATE(),1,@ShippingType,@DeliveryDate,2,@Remarks)                                                                             
					SET @DraftAutoId=SCOPE_IDENTITY()                                                                             
					INSERT INTO DraftItemMaster(DraftAutoId,ProductAutoId,UnitAutoId,ReqQty,QtyPerUnit,UnitPrice,minprice,SRP,GP,TaxRate,IsExchange,IsFreeItem,IsTaxable,UnitMLQty,TotalMLQty)                                                                          
					VALUES(@DraftAutoId,@ProductAutoId,@UnitAutoId,@ReqQty,@QtyPerUnit,@UnitPrice,@minprice,@SRP,@GP,                                                              
					@IsTaxable,@IsExchange,@IsFreeItem,@IsTaxable,@MLQty,(@MLQty*@ReqQty))                                                                                                              
				END                                                                                                                                                     
				ELSE                                                                                                                                                 
				BEGIN                                                                                                        
					IF NOT EXISTS(SELECT * FROM DraftItemMaster WHERE DraftAutoId= @DraftAutoId AND ProductAutoId=@ProductAutoId AND                                                                                                      
					UnitAutoId=@UnitAutoId AND IsExchange=@IsExchange AND IsFreeItem=@IsFreeItem)                                                                 
					BEGIN 
						INSERT INTO DraftItemMaster(DraftAutoId,ProductAutoId,UnitAutoId,ReqQty,QtyPerUnit,UnitPrice,minprice,SRP,GP,TaxRate,IsExchange,IsFreeItem,IsTaxable,UnitMLQty,TotalMLQty)                                                        
						VALUES(@DraftAutoId,@ProductAutoId,@UnitAutoId,@ReqQty,@QtyPerUnit,@UnitPrice,@minprice,@SRP,@GP,@IsTaxable,@IsExchange,@IsFreeItem,@IsTaxable,@MLQty,(@MLQty*@ReqQty))                                                                   
						UPDATE DraftOrderMaster SET [TotalItem]=[TotalItem]+1,Remarks=@Remarks WHERE DraftAutoId=@DraftAutoId                                                                                                                      
					END                                                                                                               
					else                                                                                                                                
					BEGIN                                                                                
						UPDATE DraftItemMaster SET ReqQty=ReqQty+@ReqQty,TotalMLQty=isnull(UnitMLQty,0)* (ReqQty+@ReqQty) WHERE  DraftAutoId= @DraftAutoId AND                                            
						ProductAutoId=@ProductAutoId AND UnitAutoId=@UnitAutoId AND IsExchange=@IsExchange AND IsFreeItem=@IsFreeItem                                                                         
					END                                                                                                                             
				END
			    SELECT TOP 1  @DraftAutoId AS DraftAutoId,* FROM #result4231                          
			END    
			else
			begin
				Set @isException=1                                          
				Set @exceptionMessage='Stock is not Available'   
			end	
			END
		ELSE
		BEGIN	
			 SET @AllocatQty=@TotalReqQty+ISNULL(@availQty,0)
	         Select @AllowQtyInPieces as AvailableQty,@AllocatQty-@AllowQtyInPieces  as RequiredQty,
			 @ProductAutoId as ProductAutoId,'Less' as Message,@IsTaxable				
		END
	 End                 
	END
  END TRY                                                                    
  BEGIN CATCH                               
    Set @isException=1                                                                    
    Set @exceptionMessage='Oops! Something went wrong.Please try later.'            
  END CATCH                                           
END 
GO
