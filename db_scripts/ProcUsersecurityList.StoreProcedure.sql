USE [psmnj.a1whm.com]
GO

/****** Object:  StoredProcedure [dbo].[ProcUsersecurityList]    Script Date: 10/03/2020 04:42:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

         
CREATE PROCEDURE [dbo].[ProcUsersecurityList]        
@Opcode INT =null,                  
@PageIndex int=null,         
@PageSize  int=10,         
@RecordCount int=null,         
@Status  varchar(100)=null,        
@isException bit out,          
@exceptionMessage varchar(max) out          
AS         
BEGIN        
 BEGIN TRY          
    SET @exceptionMessage= 'Success'          
    SET @isException=0           
    IF @Opcode=41        
   BEGIN               
    select et.TypeName,SecurityValue,description,InternalRemarks from TBL_MST_SecurityMaster as sm
    inner join EmployeeTypeMaster as et on et.AutoId=sm.SecurityType         
   END              
 END TRY        
 BEGIN CATCH        
    SET @isException=1        
    SET @exceptionMessage='Oops! Something went wrong.Please try later.'           
 END CATCH        
        
END 
GO

