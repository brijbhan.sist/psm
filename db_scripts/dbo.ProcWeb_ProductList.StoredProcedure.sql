USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcWeb_ProductList]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
CREATE PROCEDURE [dbo].[ProcWeb_ProductList]    
@AutoId INT=NULL,                          
 @Opcode INT=NULL,                             
 @timeStamp datetime =null,                            
 @CategoryID int=null,                       
 @SubCategoryID int=null,                         
 @ProductId  int=null,            
 @Quantity int=null,             
 @Operation int=null,            
 @CityId int=null,      
 @UserID int=null,   
 @StoreId int=null,     
 @Type int=null,                                 
 @IsException bit out,                                  
 @PageIndex INT =1,                                
 @PageSize INT=28,                                
 @RecordCount INT=null,                                                   
 @ExceptionMessage varchar(max) out                                         
AS                            
BEGIN                            
   SET @IsException=0                                                
   SET @ExceptionMessage=''                            
   BEGIN TRY                            
   IF @Opcode=41                        
   BEGIN                        
	   Select CM.CategoryName,sm.SubcategoryName from CategoryMaster as cm                          
	   inner join SubCategoryMaster as sm on                          
	   sm.CategoryAutoId=cm.AutoId  Where sm.AutoId=@SubCategoryID                          
                          
	   SELECT ROW_NUMBER() OVER(ORDER BY ProductName) AS RowNumber, * INTO #Results FROM                            
	   (                           
	   Select PM.AutoId, PM.ProductId, PM.ProductName ,(Select Url from APIUrlDetails)+PM.ImageUrl                             
	   as ImageUrl,PD.Qty as Quantity,UM.UnitType,PD.Price,'Product Description of :'+ PM.ProductName  as Description,                           
	   PM.CategoryAutoId, PM.SubcategoryAutoId from PackingDetails as PD                            
	   inner join UnitMaster as UM on                             
	   PD.UnitType=UM.AutoId                             
	   inner join ProductMaster as PM on                            
	   PD.ProductAutoId=PM.AutoId and PD.UnitType=PM.PackingAutoId                             
	   Where PM.SubcategoryAutoId=@SubCategoryID and PM.ProductStatus=1 --and PM.ImageUrl not like '%default_pic.png%'                          
	   ) AS t  ORDER BY ProductName                          
                          
	   SELECT COUNT(AutoId) AS RecordCount, case when @PageSize=0 then COUNT(AutoId) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Results                          
	   SELECT *  FROM #Results                            
	   WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))                           
   END 
   ELSE IF @Opcode=42                        
   BEGIN                        
		Select PM.AutoId, PM.ProductId, PM.ProductName ,(Select Url from APIUrlDetails)+PM.ImageUrl                             
	   as ImageUrl,PD.Qty as Quantity,UM.UnitType,PD.Price,'Product Description of :'+ PM.ProductName  as Description,                           
	   PM.CategoryAutoId, PM.SubcategoryAutoId,(select BrandName from BrandMaster where AutoId=PM.BrandAutoId) as BrandName,                        
	   case when PM.Stock>PD.Qty then 1 else 0 end as AvailableInStock,              
	   (Select CategoryName from CategoryMaster where AutoId=PM.CategoryAutoId) as CategoryName,              
	   (Select SubcategoryName from SubCategoryMaster where AutoId=PM.SubcategoryAutoId) as SubcategoryName                       
	   from ProductMaster as PM                         
	   inner join PackingDetails as PD on                                
	   PD.ProductAutoId=PM.AutoId and PD.UnitType=PM.PackingAutoId                         
	   inner join UnitMaster as UM on                             
	   PD.UnitType=UM.AutoId                          
	   Where PM.ProductId=@ProductId and PM.ProductStatus=1 --and PM.ImageUrl not like '%default_pic.png%'                          
   END  
   ELSE IF @Opcode=43  --Category List                    
   BEGIN                      
	   select cm.AutoId,cm.CategoryName,scm.AutoId as subCatAutoId,scm.SubcategoryName from CategoryMaster cm                        
	   inner join SubCategoryMaster as scm on  cm.AutoId=scm.CategoryAutoId                       
	   Where cm.Status=1 and scm.Status=1 and cm.isShow=1 and scm.isShow=1 order by cm.SeqNo,cm.CategoryName,scm.SubcategoryName asc                       
   END             
   IF @Opcode=44  --Calculate Price              
   BEGIN             
		Declare @DefaultQty int,@DefaultPrice decimal(18,2),@FindActualQty int,@FindActualPrice decimal(18,2),@UnitType int,@OutQty int            
            
		Select @UnitType=PackingAutoId from ProductMaster Where AutoId=@ProductId            
		Select @DefaultQty=Qty from PackingDetails Where ProductAutoId=@ProductId and UnitType=@UnitType            
		Select @DefaultPrice=Price from PackingDetails Where ProductAutoId=@ProductId and UnitType=@UnitType            
            
	   IF @Operation=0  --Plus            
	   BEGIN            
			BEGIN TRY            
			   SET @OutQty=(@Quantity+@DefaultQty)            
			   SET @FindActualQty=(@OutQty/@DefaultQty)            
			   SET @FindActualPrice=(@FindActualQty*@DefaultPrice)            
			   SELECT @FindActualPrice as Price,@OutQty as Quantity            
		   END TRY            
		   BEGIN CATCH            
		   SET @IsException=1                            
		   SET @ExceptionMessage=ERROR_MESSAGE()                      
		   END CATCH            
	  END            
    ELSE IF @Operation=1 --Minus            
    BEGIN            
         BEGIN TRY            
			SET @OutQty=(@Quantity-@DefaultQty)            
			IF(@OutQty<=@DefaultQty)            
			BEGIN            
			SET @FindActualPrice=@DefaultPrice            
			SET @OutQty=@DefaultQty            
			END            
			ELSE BEGIN            
			SET @FindActualQty=(@OutQty%@DefaultQty)            
			IF(@FindActualQty!=0)            
			BEGIN            
			SET @OutQty=@DefaultQty            
			SET @FindActualPrice=@DefaultPrice            
			END            
			ELSE            
			BEGIN            
			 SET @FindActualPrice=((@OutQty*@DefaultPrice)/@DefaultQty)            
			END            
			 END            
		   SELECT Cast(@FindActualPrice as decimal(10,2)) as Price,@OutQty as Quantity            
		END TRY            
		BEGIN CATCH            
		SET @IsException=1                            
		SET @ExceptionMessage=ERROR_MESSAGE()                      
		END CATCH            
    END            
    ELSE IF @Operation=2 -- On Quantity text change            
    BEGIN            
		BEGIN TRY            
			SET @FindActualQty=@Quantity%@DefaultQty 
			IF @Quantity=0 
			BEGIN
				SET @OutQty=@DefaultQty            
				SET @FindActualPrice=@DefaultPrice  
			END
			ELSE IF(@FindActualQty!=0)            
			BEGIN            
				SET @OutQty=@DefaultQty            
				SET @FindActualPrice=@DefaultPrice            
			END            
			ELSE BEGIN    
				SET @OutQty=@Quantity               
				SET @FindActualPrice=ISNULL((@Quantity/@DefaultQty),0)*@DefaultPrice            
			END            
			SELECT @FindActualPrice as Price,@OutQty as Quantity       --Updated on 10/17/2019 22:31        
		END TRY            
		BEGIN CATCH            
		SET @IsException=1                            
		SET @ExceptionMessage=ERROR_MESSAGE()                      
		END CATCH            
	 END     
	ELSE IF @Operation=3 -- On Quantity text change on cart master page           
	BEGIN            
		BEGIN TRY          
			SET @FindActualQty=@Quantity%@DefaultQty 
			IF @Quantity=0 
			BEGIN
				SET @OutQty=@DefaultQty            
				SET @FindActualPrice=@DefaultPrice  
			END
			ELSE IF(@FindActualQty!=0)            
			BEGIN            
				SET @OutQty=@DefaultQty            
				SET @FindActualPrice=@DefaultPrice            
			END            
			ELSE BEGIN    
				SET @OutQty=@Quantity               
				SET @FindActualPrice=ISNULL((@Quantity/@DefaultQty),0)*@DefaultPrice   

				Declare @Atid int  
				SELECT @Atid=AutoId FROM WEBCartMaster Where CustomerId=@UserId and StorId=@StoreId 
				UPDATE WebCartItemMaster SET TotalPieces=@OutQty,OrderQty=ISNULL((@Quantity/PerUnitQty),0),NetPrice=@FindActualPrice 
				Where CartMasterId=@Atid and ProductId=@ProductId       
			END          
			SELECT @FindActualPrice as Price,@OutQty as Quantity          
		END TRY          
		BEGIN CATCH          
			SET @IsException=1                          
			SET @ExceptionMessage=ERROR_MESSAGE()                    
		END CATCH            
	END            
	END              
   IF @Opcode=45          
   BEGIN          
        Select AutoId,CityId,Zipcode from ZipMaster Where Status=1          
   END          
   IF @Opcode=46          
   BEGIN          
		SELECT cm.CityName,StateName,c.Country from State s          
		inner join CityMaster as cm on           
		s.AutoId=cm.StateId          
		inner join Country as c on          
		c.AutoId=s.CountryId          
		WHERE cm.AutoId=@CityId          
   END      
   IF @Opcode=47      
   BEGIN      
		Declare @TotalPrice decimal,@Aid int    
    
		SELECT @Aid=AutoId FROM WEBCartMaster Where CustomerId=@UserId and StorId=@StoreId    
    
		Select @TotalPrice=SUM(NetPrice) from WebCartItemMaster Where CartMasterId=@Aid     
    
		Select CIM.AutoId,PM.ProductName,CIM.OrderQty,NetPrice,(Select Url from APIUrlDetails)+PM.ImageUrl as ImageUrl,TotalPieces as TotalPiece,PM.AutoId as ProductId,    
		@TotalPrice as TotalPrice,PD.Price as DefaultPrice,PD.Qty,(Select UnitType from UnitMaster WHERE AutoId=PD.UnitType) as UnitType from WebCartItemMaster AS CIM      
		INNER JOIN ProductMaster AS PM on     
		CIM.ProductId=PM.AutoId    
		INNER JOIN PackingDetails AS PD on    
		PM.AutoId=PD.ProductAutoId and PM.PackingAutoId=PD.UnitType Where CIM.CartMasterId=@Aid     
   END      
   IF @Opcode=48      
   BEGIN      
       DELETE from WebCartItemMaster Where AutoId=@ProductId      
   END    
   IF @Opcode=49  
   BEGIN  
      Declare @TotPrice decimal,@Ad int,@Record int,@StateId int,@TaxValue decimal(18,2),@Tax decimal(18,2),@shipId int,@PayId int,  
    @OzValue decimal(18,2),@OZTAX decimal(18,2),@MLTaxRate decimal(18,2),@MLTaxValue decimal(18,2),@ShippingCharge decimal(18,2)  
  
   SELECT @StateId=State FROM BillingAddress WHERE CustomerAutoId=@StoreId and IsDefault=1  
   SELECT @OzValue=Value from Tax_Weigth_OZ  
   SELECT @Ad=AutoId,@shipId=ShippingId,@PayId=PaymentId FROM WEBCartMaster Where CustomerId=@UserId and StorId=@StoreId   

   Select @ShippingCharge=ISNULL(st.ShippingCharge,0) from WebCartMaster as cm inner join WebCustomerShippingType as st on 
   cm.ShippingId=st.ShippingAutoId and cm.StorId=st.CustomerAutoId  Where StorId=@StoreId--Updated on 10/15/2019 01:26 AM    
  
   IF EXISTS(SELECT * FROM TaxTypeMaster Where State=@StateId)  
      BEGIN  
          SELECT @TaxValue=VALUE FROM TaxTypeMaster Where State=@StateId  
      END  
   ELSE  
   BEGIN  
       SET @TaxValue=0  
   END  
   
   SELECT @MLTaxRate=ISNULL(TaxValue,0) from WebCustomerTaxmaster Where TaxType='MLTax' and StoreId=@StoreId
   SELECT @OzValue=ISNULL(TaxValue,0) from WebCustomerTaxmaster Where TaxType='WeightTax' and StoreId=@StoreId
  
  SELECT @MLTaxValue=ISNUll(@MLTaxRate,0)*ISNULL((  
  SELECT SUM(TotalPieces*pm.WeightOz) FROM WebCartItemMaster wc  
  INNER JOIN ProductMaster pm on  
  wc.ProductId=pm.AutoId  
  Where wc.CartMasterId=@Ad and ISNULL(IsApply_ML,0)=1  
  ),0)  
  
  SELECT @OZTAX=ISNULL(@OzValue,0)*ISNULL((  
  SELECT SUM(TotalPieces*pm.WeightOz) FROM WebCartItemMaster wc  
  INNER JOIN ProductMaster pm on  
  wc.ProductId=pm.AutoId  
  Where wc.CartMasterId=@Ad and ISNULL(pm.IsApply_Oz,0)=1),0)  
  
   Select @TotPrice=SUM(NetPrice) from WebCartItemMaster Where CartMasterId=@Ad   
   IF (@TaxValue!=0)  
   BEGIN  
       SET @Tax=(@TotPrice*@TaxValue)/100  
   END  
   ELSE  
   BEGIN  
       SET @Tax=0  
   END  
   Select @Record=COUNT(*) from WebCartItemMaster Where CartMasterId=@Ad    
   Select @Record as TotalItem,@TotPrice as SubTotal,@ShippingCharge as Shipping,@OZTAX as WeightTax,@MLTaxValue as MLTax,@Tax as Tax,  
   (@TotPrice+@ShippingCharge+@OZTAX+@MLTaxValue+@Tax) as Total,@shipId as ShippingId,@PayId as PaymentId --Update on 10/14/2019 23:22 PM  
   ,(  
    SELECT WPM.AutoId,PM.PaymentMode,WPM.PaymentAutoId,IsDefault FROM WebCustomerPaymentMode as WPM  
   INNER JOIN PAYMENTModeMaster as PM on  
   WPM.PaymentAutoId=PM.AutoID  
    Where CustomerAutoId=@StoreId     
    for json path,include_null_values  
   ) as PaymentList,(  
     SELECT WS.AutoId,ST.ShippingType,WS.ShippingAutoId,ShippingCharge,IsDefault FROM WebCustomerShippingType as WS  
   INNER JOIN ShippingType AS ST  on  
   WS.ShippingAutoId=ST.AutoId Where CustomerAutoId=@StoreId  
    for json path,include_null_values  
   ) as ShippingList  
  for json path,include_null_values  
   END    
   IF @Opcode=50  
   BEGIN  
        IF(@Type=1) --Update on 10/15/2019 01:09 AM  
  BEGIN  
       UPDATE WebCartMaster SET ShippingId=@AutoId WHere StorId=@StoreId  
  END  
  ELSE  
  BEGIN  
       UPDATE WebCartMaster SET PaymentId=@AutoId WHere StorId=@StoreId  
  END  
   END  
   IF @Opcode=11  --Add to Cart      
   BEGIN      
        Declare @CartMasterId int,@Unit int,@Qty int,@Price decimal(18,2),@rem int,@ActQty int,@NetPrice decimal(18,2),      
  @MLQty decimal(18,2),@MLValue decimal(18,2),@WeightQty decimal(18,2),@WeightValue decimal(18,2),@ShippingId int,@PaymentId int      
         
  Select @Unit=PackingAutoId,@MLQty=ISNULL(MLQty,0),@WeightQty=ISNULL(WeightOz,0) from ProductMaster Where AutoId=@ProductId            
  Select @Qty=Qty,@Price=Price from PackingDetails Where ProductAutoId=@ProductId and UnitType=@Unit   
  
  Select @PaymentId=PaymentAutoId from WebCustomerPaymentMode Where CustomerAutoId=@StoreId and IsDefault=1  --Updated on 10/14/2019 22:47 PM  
        Select @ShippingId=ShippingAutoId from WebCustomerShippingType Where CustomerAutoId=@StoreId and IsDefault=1  --Updated on 10/14/2019 22:47 PM      
        
  IF NOT EXISTS(SELECT * FROM WebCartMaster WHERE CustomerId=@UserId and StorId=@StoreId)      
  BEGIN      
  INSERT INTO WebCartMaster (CustomerId,StorId,AddDate,SubTotal,Discount,DiscAmount,TaxId,TaxValue,TaxAmount,TotalMLQty,TotalMLTax  --Updated on 10/14/2019 22:47 PM  
  ,TotalWeightQty,TotalWeightTaxt,NetPayableAmount,ShippingId,paymentId) VALUES (@UserID,@StoreId,GETDATE(),0,0,0,0,0,0,0,0,0,0,0,@ShippingId,@PaymentId)      
  SET @CartMasterId=SCOPE_IDENTITY()      
  END      
  ELSE      
  BEGIN      
   SET @CartMasterId=(SELECT AutoId FROM WebCartMaster WHERE CustomerId=@UserId and StorId=@StoreId)      
  END      
  SET @rem=@Quantity%@Qty      
  IF(@rem=0)      
  BEGIN      
   SET @ActQty=@Quantity/@Qty      
  SET @NetPrice=@Price*@ActQty      
  SET @MLValue=@Quantity*@MLQty      
  SET @WeightValue=@Quantity*@WeightQty      
  END      
  IF NOT EXISTS(SELECT * FROM WebCartItemMaster WHERE CartMasterId=@CartMasterId AND ProductId=@ProductId)      
  BEGIN      
  INSERT INTO WebCartItemMaster (CartMasterId,ProductId,OrderQty,ProductUnit,PerUnitQty,TotalPieces,UnitPrice,NetPrice,PerMLQty,TotalMlQty,PerWeightQty,TotalWeightQty)      
     VALUES (@CartMasterId,@ProductId,@ActQty,@Unit,@Qty,@Quantity,@Price,@NetPrice,@MLQty,@MLValue,@WeightQty,@WeightValue)      
  END      
  ELSE      
  BEGIN      
   UPDATE WebCartItemMaster SET OrderQty=OrderQty+@ActQty,NetPrice=NetPrice+@NetPrice,TotalPieces=TotalPieces+@Quantity,PerMLQty=@MLQty,TotalMlQty=TotalMlQty+@MLValue,PerWeightQty=@WeightQty,      
   TotalWeightQty=TotalWeightQty+@WeightValue WHERE CartMasterId=@CartMasterId AND ProductId=@ProductId      
  END      
  END                      
  END TRY                            
  BEGIN CATCH                            
   SET @IsException=1                            
   SET @ExceptionMessage=ERROR_MESSAGE()                            
  END CATCH                          
END 
GO
