--alter table MessageMaster add StartDate datetime,IsRepeat int
ALTER PROCEDURE [dbo].[ProcNotificationMessage]  
@Opcode INT=NULL,  
@EmpAutoId INT=NULL,  
@TeamId INT=NULL, 
@Sequence INT=NULL, 
@Message varchar(max)=NULL,
@Title varchar(200)=NULL,
@StartDate date=null,
@StartTime varchar(20)=null,
@IsRepeat int=null,
@ExpiryDate date=null,  
@ExpiryTime varchar(20)=null,  
@Recipient varchar(50)=null,  
@MessageAutoId int=null,  
@Noblank int =null,
@dtbulkmessageImageUrl [DT_dtbulkmessageImageUrl] readonly,
@PageIndex INT = 1,  
@PageSize INT = 15,  
@RecordCount INT =null,  
@isException bit out,  
@exceptionMessage varchar(max) out  
  
AS  
BEGIN  
 BEGIN TRY  
  Set @isException=0  
  Set @exceptionMessage='Success'  
  declare @ExpiryDateTime varchar(20),@StartDateTime varchar(20)  
  IF @Opcode=101 
  BEGIN
	BEGIN TRY   
		BEGIN TRAN  
		IF CAST(@StartDate as Date)>CAST(@ExpiryDate as DATE)
		BEGIN
			Set @isException=1  
			Set @exceptionMessage='Expiry date can''t be less than Start date.'  
		END
		ELSE
		BEGIN
			SET @StartDateTime = Convert(varchar(20),@StartDate) + ' ' + Convert(varchar(8),Convert(time,@StartTime)) 
			SET @ExpiryDateTime = Convert(varchar(20),@ExpiryDate) + ' ' + Convert(varchar(8),Convert(time,@ExpiryTime))  
	 
			INSERT INTO [dbo].[MessageMaster] ([Team],[Message],[Title],[CreatedDate],[ExpiryDate],[IsExpired],[Sequence],
			[IsRead],StartDate,IsRepeat)  
			VALUES(@TeamId,@Message,@Title,getdate(),Convert(datetime,@ExpiryDateTime),0,@Sequence,0,@StartDateTime,@IsRepeat)  
  
			SET @MessageAutoId = (SELECT SCOPE_IDENTITY())
	 
			insert into [MessageImageurl] (MessageAutoId,messageImageUrl)
			Select @MessageAutoId,URL from @dtbulkmessageImageUrl
  
			IF @TeamId = 1  
			BEGIN  
				INSERT INTO [dbo].[MessageAssignTo] ([MessageAutoId],[EmpType],[EmpAutoId],[TeamId])  
				SELECT @MessageAutoId,tbl1.splitdata,NULL,@TeamId FROM (SELECT * FROM [dbo].[fnSplitString](@Recipient,',')) AS tbl1  
			END  
			ELSE IF @TeamId = 2  
			BEGIN  
				INSERT INTO [dbo].[MessageAssignTo] ([MessageAutoId],[EmpType],[EmpAutoId],[TeamId])  
				SELECT @MessageAutoId,NULL,Tbl2.splitdata,@TeamId FROM (SELECT * FROM [dbo].[fnSplitString](@Recipient,',')) As Tbl2  
			END  
		END
		COMMIT TRANSACTION  
	END TRY  
	BEGIN CATCH  
		ROLLBACK TRAN  
		Set @isException=1  
		Set @exceptionMessage='Oops! Something went wrong.Please try later.'  
	END CATCH 
  END
  ELSE IF @Opcode=401  
   BEGIN  
		IF(@TeamId = 1)  
		BEGIN  
			SELECT [AutoId],[TypeName] As Recipient FROM [dbo].[EmployeeTypeMaster]  order by Recipient
		END  
		ELSE IF(@TeamId = 2)  
		BEGIN  
			SELECT EM.[AutoId],[FirstName] + ' ' + [LastName] + ' (' + ETM.[TypeName] + ')' As Recipient
			FROM [dbo].[EmployeeMaster] AS EM  
			INNER JOIN [dbo].[EmployeeTypeMaster] AS ETM ON ETM.[AutoId] = EM.EmpType  
		END  
   END  
  ELSE IF @Opcode=402  
   BEGIN  
    SELECT MM.[AutoId] As MessageAutoId,[Team],[Message],[Title],[Sequence],FORMAT(ExpiryDate,'MM/dd/yyyy') As ExpiryDate,
	  ISNULL((Select MessageImageurl from MessageImageurl where MessageAutoId=MM.AutoId),'') as ImageUrl,
	right(convert(varchar(20),ExpiryDate,100),7) As ExpiryTime,MM.[IsExpired],temp1.EmpType,temp2.EmpName,  
    Convert(varchar(20),MM.[CreatedDate]) As CreatedDate,FORMAT(StartDate,'MM/dd/yyyy') As StartDate,
	right(convert(varchar(20),StartDate,100),7) As StartTime,IsRepeat FROM [dbo].[MessageMaster] AS MM      
    LEFT JOIN (SELECT T2.[MessageAutoId], EmpType = STUFF((SELECT ',' +CONVERT(varchar(10), T1.[EmpType]) 
	FROM [dbo].[MessageAssignTo] AS T1  
    WHERE T1.[MessageAutoId] = T2.[MessageAutoId]  
    FOR XML PATH ('')), 1, 1, '') from [dbo].[MessageAssignTo] AS T2 group by T2.[MessageAutoId]) As temp1 ON temp1.[MessageAutoId] = MM.[AutoId]  
    LEFT JOIN (SELECT T2.[MessageAutoId], EmpName = STUFF((SELECT ',' + CONVERT(varchar(10),T1.[EmpAutoId]) FROM [dbo].[MessageAssignTo] AS T1  
    WHERE T1.[MessageAutoId] = T2.[MessageAutoId]  
    FOR XML PATH ('')), 1, 1, '') from [dbo].[MessageAssignTo] AS T2 group by T2.[MessageAutoId]) As temp2 ON temp2.[MessageAutoId] = MM.[AutoId]  
    where MM.[AutoId]= @MessageAutoId  
   END  
  ELSE IF @Opcode=403  
   BEGIN  
      
    SELECT ROW_NUMBER() OVER(ORDER BY MessageAutoId desc) AS RowNumber, * INTO #Results from  
    (  
    SELECT MM.[AutoId] As MessageAutoId,[Team],Convert(varchar(50),[Message]) + case when datalength([Message])>50 then  '...' else '' end AS Message,
	Format([ExpiryDate],'MM/dd/yyyy hh:mm tt') As ExpiryDate,Format([StartDate],'MM/dd/yyyy hh:mm tt') As StartDate,MM.[IsExpired],temp1.EmpType,temp2.EmpName,  
    Format(MM.[CreatedDate],'MM/dd/yyyy hh:mm tt') As CreatedDate FROM [dbo].[MessageMaster] AS MM      
    LEFT JOIN (SELECT T2.[MessageAutoId], EmpType = STUFF((SELECT ',' + ETM.[TypeName] FROM [dbo].[MessageAssignTo] AS T1  
       INNER JOIN [dbo].[EmployeeTypeMaster] AS ETM ON ETM.[AutoId] = T1.[EmpType] WHERE T1.[MessageAutoId] = T2.[MessageAutoId]  
       FOR XML PATH ('')), 1, 1, '') from [dbo].[MessageAssignTo] AS T2 group by T2.[MessageAutoId]) As temp1 ON temp1.[MessageAutoId] = MM.[AutoId]  
    LEFT JOIN (SELECT T2.[MessageAutoId], EmpName = STUFF((SELECT ',' + EM.[FirstName] + ' ' + EM.[LastName] FROM [dbo].[MessageAssignTo] AS T1  
       INNER JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = T1.[EmpAutoId] WHERE T1.[MessageAutoId] = T2.[MessageAutoId]  
       FOR XML PATH ('')), 1, 1, '') from [dbo].[MessageAssignTo] AS T2 group by T2.[MessageAutoId]) As temp2 ON temp2.[MessageAutoId] = MM.[AutoId]  
    WHERE   
    (@ExpiryDate IS NULL OR @ExpiryDate='' OR Convert(date,[ExpiryDate]) = @ExpiryDate)
    ) as t  order by MessageAutoId desc
  
    SELECT COUNT(*) as RecordCount,@PageSize as PageSize,@PageIndex as PageIndex FROM #Results  

    SELECT * FROM #Results  
    WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1  order by MessageAutoId desc

    SELECT EmpType FROM EmployeeMaster WHERE AutoId=@EmpAutoId  
   END  
   ELSE IF @Opcode=21  
   BEGIN  
    BEGIN TRY   
     BEGIN TRAN  
		IF CAST(@StartDate as Date)>CAST(@ExpiryDate as DATE)
		BEGIN
			Set @isException=1  
			Set @exceptionMessage='Expiry date can''t be less than Start date.'  
		END
		ELSE
		BEGIN	
			SET @ExpiryDateTime = Convert(varchar(20),@ExpiryDate) + ' ' + Convert(varchar(8),Convert(time,@ExpiryTime))  
			SET @StartDateTime = Convert(varchar(20),@StartDate) + ' ' + Convert(varchar(8),Convert(time,@StartTime))  
			UPDATE [dbo].[MessageMaster] SET  
			[Team]=@TeamId,
			[Sequence]=@Sequence,
			[IsRepeat]=@IsRepeat,
			[Message]=@Message,[Title]=@Title,
			[StartDate]=Convert(datetime,@StartDateTime),  
			[ExpiryDate]=Convert(datetime,@ExpiryDateTime),  
			IsExpired=0 WHERE [AutoId]=@MessageAutoId   
	 
			IF @TeamId = 1  
			BEGIN  
			    IF EXISTS(Select * FROM MessageAssignTo where MessageAutoId=@MessageAutoId)
				BEGIN
					Delete FROM MessageAssignTo where MessageAutoId=@MessageAutoId 
				END
					INSERT INTO [dbo].[MessageAssignTo] ([MessageAutoId],[EmpType],[EmpAutoId],[TeamId])  
					SELECT @MessageAutoId,tbl1.splitdata,NULL,@TeamId FROM (SELECT * FROM [dbo].[fnSplitString](@Recipient,',')) AS tbl1  
			END  
			ELSE IF @TeamId = 2  
			BEGIN  
			    IF EXISTS(Select * FROM MessageAssignTo where MessageAutoId=@MessageAutoId)
				BEGIN  
					Delete FROM MessageAssignTo where MessageAutoId=@MessageAutoId 
				END
					INSERT INTO [dbo].[MessageAssignTo] ([MessageAutoId],[EmpType],[EmpAutoId],[TeamId])  
					SELECT @MessageAutoId,NULL,Tbl2.splitdata,@TeamId FROM (SELECT * FROM [dbo].[fnSplitString](@Recipient,',')) As Tbl2 
			END  	   
			IF @Noblank =1
			BEGIN
				DELETE fROM [MessageImageurl] WHERE MessageAutoId=@MessageAutoId
				insert into [MessageImageurl] (MessageAutoId,messageImageUrl)
				Select @MessageAutoId,URL from @dtbulkmessageImageUrl	   
			END	
	    END
     COMMIT TRANSACTION  
    END TRY  
    BEGIN CATCH  
     ROLLBACK TRAN  
     Set @isException=1  
     Set @exceptionMessage= 'Oops! Something went wrong.Please try later.'  
    END CATCH  
   END  
    ELSE IF @Opcode=31  
   BEGIN  
   if exists(select * from tbl_MessageReadLog where MessageAutoId=@MessageAutoId)
   BEGIN
	Set @isException=1  
	Set @exceptionMessage='You can''t delete read message.'  
   END
   ELSE
   BEGIN  
    BEGIN TRY   
     BEGIN TRAN  	 
      DELETE FROM [MessageAssignTo] WHERE [MessageAutoId]=@MessageAutoId  
      DELETE FROM [dbo].[MessageMaster] WHERE AutoId=@MessageAutoId         
     COMMIT TRANSACTION  
    END TRY  
    BEGIN CATCH  
     ROLLBACK TRAN  
     Set @isException=1  
     Set @exceptionMessage='Oops! Something went wrong.Please try later.'  
    END CATCH  
	END	
   END  
 END TRY  
 BEGIN CATCH  
  Set @isException=1  
  Set @exceptionMessage='Oops! Something went wrong.Please try later.'  
 END CATCH  
END  

 