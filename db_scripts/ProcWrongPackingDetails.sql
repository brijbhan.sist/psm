Alter Procedure ProcWrongPackingDetails   
 @Opcode INT=NULL,  
 @ProductId varchar(15)=null,
 @isException bit out,      
 @PageIndex INT=1,        
 @PageSize INT=10,        
 @exceptionMessage varchar(max) out    
AS      
BEGIN   
   SET @isException=0  
   SET @exceptionMessage='success'  
   IF @Opcode=401  
   BEGIN  
	SELECT ROW_NUMBER() OVER(ORDER BY AutoId  DESC) AS RowNumber, * INTO #Results from(  
	SELECT pmnj.AutoId,pmnj.ProductId,      
	UM.UnitType,      
	NJ.Qty as NJQty,

	isnull((Select Qty as PAQty from [psmpa.a1whm.com].[dbo].PackingDetails as PAP 
	where PAP.ProductAutoId=  
	(Select AutoId From [psmpa.a1whm.com].[dbo].ProductMaster   
	where ProductId=pmnj.ProductId) AND (PAP.UnitType=UM.AutoId)),0) as PAQty,
	
	isnull((Select Qty as NPAQty from [psmnpa.a1whm.com].[dbo].PackingDetails as NPAP 
	where NPAP.ProductAutoId=  
	(Select AutoId From [psmnpa.a1whm.com].[dbo].ProductMaster   
	where ProductId=pmnj.ProductId) AND (NPAP.UnitType=UM.AutoId)),0) as NPAQty,
	
	isnull((Select Qty as WPAQty from [psmwpa.a1whm.com].[dbo].PackingDetails as WPAP 
	where WPAP.ProductAutoId=  
	(Select AutoId From [psmwpa.a1whm.com].[dbo].ProductMaster   
	where ProductId=pmnj.ProductId) AND (WPAP.UnitType=UM.AutoId)),0) as WPAQty,
	
	isnull((Select Qty as CTQty from [psmct.a1whm.com].[dbo].PackingDetails as CT
	where CT.ProductAutoId=  
	(Select AutoId From [psmct.a1whm.com].[dbo].ProductMaster   
	where ProductId=pmnj.ProductId) AND (CT.UnitType=UM.AutoId)),0) as CTQty
	
	from [dbo].ProductMaster AS pmnj      
	INNER JOIN [dbo].PackingDetails AS NJ ON NJ.ProductAutoId=pmnj.AutoId    
	INNER JOIN [dbo].UnitMaster as UM on UM.AutoId=nj.UnitType 
	where (@ProductId is null or @ProductId=0 or pmnj.ProductId like '%'+@ProductId+'%')
	) as t
	Select * into #Results2 from #Results 
	where (NJQty!=PAQty) OR (NJQty!=NPAQty)
	OR (NJQty!=WPAQty) OR (NJQty!=CTQty)

	SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize=0 then COUNT(RowNumber) else @PageSize   
	end AS PageSize, @PageIndex AS PageIndex FROM #Results2   
  
	SELECT * FROM #Results2                                               
	WHERE (@PageSize=0 or  (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))   
  END  
   IF @Opcode=402
   BEGIN
        Select ProductId,Convert(varchar(20),ProductId)+'--'+ProductName as Name from ProductMaster order by Name asc
   END
END 