USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcStatusMaster]    Script Date: 01/27/2020 17:46:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
         
alter PROCEDURE [dbo].[ProcStatusMaster]        
@Opcode INT =null,            
@PageIndex int=null,         
@PageSize  int=10,         
@RecordCount int=null,         
@Status  varchar(100)=null,        
@isException bit out,          
@exceptionMessage varchar(max) out          
AS         
BEGIN        
 BEGIN TRY          
    SET @exceptionMessage= 'Success'          
    SET @isException=0    
    IF @Opcode=41        
   BEGIN        
           
    SELECT StatusType,Category,AutoId
    INTO #RESULT FROM StatusMaster    
           
    --SELECT COUNT(*) AS RecordCount, @PageSize AS PageSize, @PageIndex AS PageIndex FROM #RESULT        
        
    SELECT * FROM #RESULT        
    --WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1        
           
   END  
 END TRY        
 BEGIN CATCH        
    SET @isException=1        
    SET @exceptionMessage='Oops! Something went wrong.Please try later.'           
 END CATCH        
        
END 
