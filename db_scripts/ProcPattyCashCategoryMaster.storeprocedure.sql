alter PROCEDURE [dbo].[ProcPattyCashCategoryMaster]        
@OpCode int=Null,        
@CategoryAutoId  int=Null,        
@CategoryId VARCHAR(12)=NULL,        
@CategoryName VARCHAR(50)=NULL,        
@Description varchar(250)=NULL,        
@Status int=null,        

@isException bit out,        
@exceptionMessage varchar(max) out        
AS        
BEGIN        
 BEGIN TRY        
  SET @isException=0        
  SET @exceptionMessage='success'        
        
 IF @OpCode=11        
 BEGIN        
  IF exists(SELECT CategoryName FROM Pattycashcategorymaster WHERE CategoryName = @CategoryName)        
  BEGIN        
   set @isException=1        
   set @exceptionMessage='Category Already Exists!!'        
  END        
  ELSE       
  BEGIN       
   BEGIN TRY        
	   BEGIN TRAN        
			SET @CategoryId=(SELECT dbo.SequenceCodeGenerator('PattyCashCategoryId'))        
			INSERT INTO [dbo].[Pattycashcategorymaster]([CategoryId], [CategoryName],Description,Status)        
			VALUES (@CategoryId, @CategoryName,@Description,@Status)        
			      
			Update SequenceCodeGeneratorMaster SET currentSequence=currentSequence+1 WHERE SequenceCode='PattyCashCategoryId'        
	   COMMIT TRANSACTION        
   END TRY         
   BEGIN CATCH        
   ROLLBACK TRAN        
		   SET @isException=1        
		   SET @exceptionMessage=ERROR_MESSAGE()--'Oops! Something went wrong.Please try later.'    
   END CATCH        
  END      
 END        
 ELSE IF @Opcode=21        
 BEGIN        
  IF exists(SELECT CategoryName FROM Pattycashcategorymaster WHERE CategoryName = @CategoryName and CategoryId != @CategoryId)        
  BEGIN        
		SET @isException=1        
		SET @exceptionMessage='Category Already Exists!!'        
  END        
  ELSE        
  BEGIN        
   BEGIN TRY        
	   BEGIN TRAN        
			UPDATE Pattycashcategorymaster SET CategoryName = @CategoryName, Description = @Description, Status = @Status      
			WHERE CategoryID = @CategoryId        
			    
	   COMMIT TRANSACTION        
   END TRY         
   BEGIN CATCH        
   ROLLBACK TRAN        
		   SET @isException=1        
		   SET @exceptionMessage=ERROR_MESSAGE()--'Oops! Something went wrong.Please try later'      
   END CATCH        
  END        
 END        
 ELSE IF @Opcode=31        
 BEGIN        
   BEGIN TRY        
   BEGIN TRAN        
			DELETE FROM Pattycashcategorymaster WHERE CategoryId = @CategoryId        			     
   COMMIT TRANSACTION        
   END TRY         
   BEGIN CATCH        
   ROLLBACK TRAN        
		   SET @isException=1        
		   SET @exceptionMessage='Category has been used in another product.'    
   END CATCH        
 END        
 ELSE IF @OpCode=41        
 BEGIN        
	  SELECT CM.CategoryId, CM.CategoryName, CM.Description, SM.StatusType AS Status,CM.IsDefault FROM  Pattycashcategorymaster AS CM         
	  inner join StatusMaster AS SM ON SM.AutoId=CM.Status and SM.Category is NULL         
	  WHERE (@CategoryId is null or @CategoryId='' or CategoryId like '%'+ @CategoryId +'%')        
	  and (@CategoryName is null or @CategoryName='' or CategoryName like '%'+ @CategoryName +'%')        
	  and (@Status=2 or Status=@Status)          
            
	  SELECT dbo.SequenceCodeGenerator('PattyCashCategoryId') AS CategoryId        
 END         
 ELSE IF @OpCode=42        
 BEGIN        
		SELECT CategoryId, CategoryName, Description,Status FROM Pattycashcategorymaster WHERE CategoryId=@CategoryId        
 END        
 END TRY        
 BEGIN CATCH            
	   SET @isException=1        
	   SET @exceptionMessage=ERROR_MESSAGE()--'Oops! Something went wrong.Please try later.'    
 END CATCH        
END        
GO
