USE [psmnj.a1whm.com]
GO

/****** Object:  Table [dbo].[CustomerBankDetails]    Script Date: 12/24/2019 7:59:38 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CustomerBankDetails](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerAutoId] [int] NULL,
	[BankName] [varchar](500) NULL,
	[BankACC] [varchar](20) NULL,
	[CardTypeAutoId] [int] NULL,
	[CardNo] [varchar](20) NULL,
	[ExpiryDate] [varchar](10) NULL,
	[CVV] [varchar](20) NULL,
	[Status] [int] NULL,
	[RoutingNo] [varchar](20) NULL,
 CONSTRAINT [PK_CustomerBankDetails] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[CustomerBankDetails]  WITH CHECK ADD  CONSTRAINT [FK_CustomerBankDetails_CustomerMaster] FOREIGN KEY([CustomerAutoId])
REFERENCES [dbo].[CustomerMaster] ([AutoId])
GO

ALTER TABLE [dbo].[CustomerBankDetails] CHECK CONSTRAINT [FK_CustomerBankDetails_CustomerMaster]
GO


