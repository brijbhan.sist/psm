USE [psmnj.a1whm.com]
GO

/****** Object:  Table [dbo].[Delete_Delivered_Order_Items_Backup]    Script Date: 04/14/2020 22:10:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Delete_Delivered_Order_Items_Backup](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[OrderAutoId] [int] NULL,
	[ProductAutoId] [int] NULL,
	[UnitAutoId] [int] NULL,
	[QtyPerUnit] [int] NULL,
	[UnitPrice] [decimal](8, 2) NULL,
	[Barcode] [varchar](50) NULL,
	[QtyShip] [int] NULL,
	[SRP] [decimal](8, 2) NULL,
	[GP] [decimal](8, 2) NULL,
	[Tax] [int] NULL,
	[FreshReturnQty] [int] NULL,
	[FreshReturnUnitAutoId] [int] NULL,
	[DamageReturnQty] [int] NULL,
	[DamageReturnUnitAutoId] [int] NULL,
	[MissingItemQty] [int] NULL,
	[MissingItemUnitAutoId] [int] NULL,
	[IsExchange] [int] NULL,
	[TaxValue] [decimal](10, 2) NULL,
	[isFreeItem] [int] NULL,
	[UnitMLQty] [decimal](10, 2) NULL,
	[TotalPieces]  AS ([DBO].[FN_Delivered_TotalPieces]([AutoId])),
	[QtyPerUnit_Fresh] [int] NULL,
	[QtyPerUnit_Damage] [int] NULL,
	[QtyPerUnit_Missing] [int] NULL,
	[NetPrice]  AS ([dbo].[FN_Delivered_NetAmount]([AutoId])),
	[TotalMLQty]  AS ([dbo].[FN_Delivered_IsMLTotalQty]([AutoId],[orderAutoId])),
	[Item_TotalMLTax]  AS ([dbo].[FN_Delivered_MLTaxItem]([Autoid],[OrderAutoId])),
	[Del_CostPrice] [decimal](18, 2) NULL,
	[Del_CommCode] [decimal](10, 4) NULL,
	[Del_Weight_Oz] [decimal](15, 2) NULL,
	[Del_Weight_Oz_TotalQty]  AS ([dbo].[FN_Delivered_Weight_Oz_Qty]([autoid])),
	[QtyDel]  AS ([dbo].[FN_Delivered_TotalDelQty]([AutoId])),
 CONSTRAINT [PK_Delete_Delivered_Order_Items_Backup] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Delete_Delivered_Order_Items_Backup] ADD  CONSTRAINT [DF_Delete_Delivered_Order_Items_Backup_isFreeItem]  DEFAULT ((0)) FOR [isFreeItem]
GO

ALTER TABLE [dbo].[Delete_Delivered_Order_Items_Backup] ADD  CONSTRAINT [DF_Delete_Delivered_Order_Items_Backup_UnitMLQty]  DEFAULT ((0)) FOR [UnitMLQty]
GO

ALTER TABLE [dbo].[Delete_Delivered_Order_Items_Backup] ADD  CONSTRAINT [DF_Delete_Delivered_Order_Items_Backup_QtyPerUnit_Fresh]  DEFAULT ((0)) FOR [QtyPerUnit_Fresh]
GO

ALTER TABLE [dbo].[Delete_Delivered_Order_Items_Backup] ADD  CONSTRAINT [DF_Delete_Delivered_Order_Items_Backup_QtyPerUnit_Damage]  DEFAULT ((0)) FOR [QtyPerUnit_Damage]
GO

ALTER TABLE [dbo].[Delete_Delivered_Order_Items_Backup] ADD  CONSTRAINT [DF_Delete_Delivered_Order_Items_Backup_Del_CostPrice]  DEFAULT ((0)) FOR [Del_CostPrice]
GO

ALTER TABLE [dbo].[Delete_Delivered_Order_Items_Backup] ADD  CONSTRAINT [DF_Delete_Delivered_Order_Items_Backup_Del_CommCode]  DEFAULT ((0)) FOR [Del_CommCode]
GO

ALTER TABLE [dbo].[Delete_Delivered_Order_Items_Backup] ADD  CONSTRAINT [DF_Delete_Delivered_Order_Items_Backup_Del_Weight_Oz]  DEFAULT ((0.00)) FOR [Del_Weight_Oz]
GO


