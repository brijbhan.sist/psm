--CustomerAutoId":"0","ProductAutoId":"0","SalesAutoId":"0","CategoryAutoId":"0","SubCategoryAutoId":"0","FromDate":"01/01/2019",
--"ToDate":"03/05/2021","BrandId":"0","PageIndex":1,"PageSize":"10","SortBySoldQty":"1"
drop table #Results42
drop table #Results43
Declare	@Opcode INT=NULL,                      
@ProductAutoId int = 0,              
@BrandAutoIdSring varchar(200) = '0',                     
@CategoryAutoId int = 0,      
@CustomerAutoId  int = 0,                    
@SubCategoryId int = 0,                           
@FromDate date ='1/1/2019',                      
@ToDate date ='31/1/2021',
@SortBySoldQty int=1,                    
@SalesPerson  int=1108,   
@PageIndex INT=1,                      
@PageSize INT=10,                      
@RecordCount INT=null 
	
	
		SELECT ROW_NUMBER() OVER(ORDER BY ProductId asc) AS RowNumber, * into #Results42                        
   FROM                      
   (                      
	   select ProductId,ProductName,BrandName,SUM(TotalPiece) as TotalPiece,SUM(PayableAmount) as PayableAmount from (
	  SELECT ProductId,ProductName,bm.BrandName,                                    
	  sum(ISNULL(QtyDel,0)) as TotalPiece,                      
	  (SUM(NetPrice))  as PayableAmount                      
	  from OrderMaster om inner join Delivered_Order_Items as oim on om.AutoId=oim.OrderAutoId                       
	  inner join ProductMaster as pm on pm.AutoId=oim.ProductAutoId                  
	  LEFT join BrandMaster as bm on bm.AutoId = pm.BrandAutoId                        
	  inner join EmployeeMaster as emp on emp.AutoId=om.SalesPersonAutoId                       
	  inner join CustomerMaster as cm on cm.AutoId=om.CustomerAutoId                         
	  where om.Status=11                 
	  AND  ((ISNULL(@BrandAutoIdSring,'0')='0') or (pm.BrandAutoId in (select * from dbo.fnSplitString(@BrandAutoIdSring,','))))               
	  AND  (ISNULL(@ProductAutoId,0)=0 or oim.ProductAutoId =@ProductAutoId )               
	  AND  (ISNULL(@SalesPerson,'0')='0' or om.SalesPersonAutoId =@SalesPerson )                               
	  AND (ISNULL(@CategoryAutoId,0)=0 OR pm.CategoryAutoId =@CategoryAutoId)                      
	  AND (ISNULL(@SubCategoryId,0)=0 OR pm.SubcategoryAutoId =@SubCategoryId)    
	  AND (ISNULL(@CustomerAutoId,0)=0 OR cm.AutoId =@CustomerAutoId)                         
	  AND (@FromDate is null or @ToDate is null or (CONVERT(date,OrderDate)                       
	  between convert(date,@FromDate) and CONVERT(date,@ToDate))) 
	  AND QtyDel!=0                  
	  group by ProductId,ProductName,bm.BrandName                
  
	  UNION ALL
  
	  SELECT ProductId,ProductName, bm.BrandName,                                   
	  sum(ISNULL(oim.TotalPeice,0))*-1 as TotalPiece,                      
	  (SUM(oim.NetAmount))*-1  as PayableAmount                      
	  from CreditMemoMaster om inner join CreditItemMaster as oim on om.CreditAutoId=oim.CreditAutoId                       
	  inner join ProductMaster as pm on pm.AutoId=oim.ProductAutoId                  
	  LEFT join BrandMaster as bm on bm.AutoId = pm.BrandAutoId                        
	  inner join EmployeeMaster as emp on emp.AutoId=om.SalesPersonAutoId                       
	  inner join CustomerMaster as cm on cm.AutoId=om.CustomerAutoId                         
	  where om.Status=3                 
	  AND  ((ISNULL(@BrandAutoIdSring,'0')='0') or (pm.BrandAutoId in (select * from dbo.fnSplitString(@BrandAutoIdSring,','))))               
	  AND  (ISNULL(@ProductAutoId,0)=0 or oim.ProductAutoId =@ProductAutoId )               
	  AND  (ISNULL(@SalesPerson,'0')='0' or om.SalesPersonAutoId =@SalesPerson )                               
	  AND (ISNULL(@CategoryAutoId,0)=0 OR pm.CategoryAutoId =@CategoryAutoId)                      
	  AND (ISNULL(@SubCategoryId,0)=0 OR pm.SubcategoryAutoId =@SubCategoryId)    
	  AND (ISNULL(@CustomerAutoId,0)=0 OR cm.AutoId =@CustomerAutoId)                         
	  AND (@FromDate is null or @ToDate is null or (CONVERT(date,CreditDate)                       
	  between convert(date,@FromDate) and CONVERT(date,@ToDate))) 
	  AND TotalPeice!=0                  
	  group by ProductId,ProductName,bm.BrandName                

	  ) as t1
    group by ProductId,ProductName,BrandName
   ) AS T           
           
--		Declare
--@SortBySoldQty int=1

    SELECT ROW_NUMBER() OVER(ORDER BY(CASE WHEN @SortBySoldQty=1 THEN tl.TotalPiece END) ASC,
	(CASE WHEN @SortBySoldQty=2 THEN tl.TotalPiece END) DESC,
	(CASE WHEN @SortBySoldQty=3 THEN tl.PayableAmount END) ASC,
	(CASE WHEN @SortBySoldQty=4 THEN tl.PayableAmount END) DESC
	) AS RowNumber,* into #Results43 FROM(
	SELECT t.ProductId,t.ProductName,convert(decimal(10,2),t.TotalPiece/CONVERT(decimal(10,2),pd.Qty)) as TotalPiece,t.PayableAmount,
	( um.UnitType +' ['+ convert(varchar(50),Qty) + ' - PC]') as Unit 
	FROM #Results42  as t
	inner join ProductMaster as pm on t.ProductId=pm.ProductId
	inner join UnitMaster as um on um.AutoId=pm.PackingAutoId
	inner join PackingDetails as pd on pd.ProductAutoId=pm.AutoId and pd.UnitType=pm.PackingAutoId                 
      ) as tl   
	  order by 
	(CASE WHEN @SortBySoldQty=1 THEN tl.TotalPiece END)ASC,
	(CASE WHEN @SortBySoldQty=2 THEN tl.TotalPiece END) DESC,
	(CASE WHEN @SortBySoldQty=3 THEN tl.PayableAmount END)ASC,
	(CASE WHEN @SortBySoldQty=4 THEN tl.PayableAmount END) DESC 

	--select * from #Results42
	--select * from #Results43
                           
	--SELECT * FROM #Results43 WHERE @PageSize=0 or  RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1                    
                         
	--SELECT COUNT(ProductName) AS RecordCount, case when @PageSize=0 then COUNT(ProductName) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Results43                    
                      
	SELECT ISNULL(SUM(convert(decimal(10,2),t.TotalPiece/CONVERT(decimal(10,2),pd.Qty))),0)  as TotalPiece,ISNULL(SUM(PayableAmount),0.00) as PayableAmount FROM #Results42   as t
	inner join ProductMaster as pm on t.ProductId=pm.ProductId
	inner join PackingDetails as pd on pd.ProductAutoId=pm.AutoId and pd.UnitType=pm.PackingAutoId              