USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[App_ProcOrderEmailInformation]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[App_ProcOrderEmailInformation]    
@EmailBody varchar(max)=null,    
@ToEmail varchar(200)=null,    
@AppSource varchar(200)=null,    
@SubUrl varchar(100)=null    
as     
begin     
 declare @FromEmail varchar(200)=null,    
 @FromName varchar(100)=null,    
 @Password nvarchar(200)=null,    
 @SMTPServer varchar(100)=null,    
 @Port int=null,    
 @SSL bit    
    
 select @FromEmail=EmailId,@Password=convert(varchar(50), DecryptByPassPhrase('ESS',Pass)),@SMTPServer=server,@Port=port,@SSL=ssl from EmailServerMaster where SendTo='Developer'    
    
 insert into [PSM_EMailing_DB].[dbo].[EmailLog]     
 (FromEmailId    
 ,FromName    
 ,Password    
 ,SMTPServer    
 ,Port    
 ,SSL    
 ,ToEmailId   
 ,Subject    
 ,EmailBody    
 ,CreationDate    
 ,Status   
 ,SourceApp    
 ,SubUrl    
 )values(    
 @FromEmail,'a1whm.com',@Password,@SMTPServer,@Port,@SSL,@ToEmail,'New Order',@EmailBody,GETDATE(),0,@AppSource,@SubUrl)    
    
end
GO
