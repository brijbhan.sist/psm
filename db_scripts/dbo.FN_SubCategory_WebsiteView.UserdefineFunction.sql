USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_SubCategory_WebsiteView]    Script Date: 01/29/2021 03:11:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TABLE [dbo].[SubCategoryMaster] DROP COLUMN [SubCategory_ShowonWebsite]
GO
ALTER FUNCTION  [dbo].[FN_SubCategory_WebsiteView]
(
	 @AutoId int
)
RETURNS int
AS
BEGIN
	DECLARE @bool int=0
	
	IF EXISTS(select * from ProductMaster as pm inner join PackingDetails as pd on pm.AutoId=pd.ProductAutoId
				where pm.ShowOnWebsite=1 and SubcategoryAutoId=@AutoId AND productstatus=1 and
				pd.unittype=pm.packingautoid)
	BEGIN
	  
	   IF EXISTS(select AutoId from SubCategoryMaster where AutoId=@AutoId and Status=1 and IsShow=1)
	   BEGIN
			SET @bool=1
	   END
	END 
	RETURN @bool
END

GO
ALTER TABLE [dbo].[SubCategoryMaster] ADD [SubCategory_ShowonWebsite] AS ([dbo].[FN_SubCategory_WebsiteView]([AutoId]))