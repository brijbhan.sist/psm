USE [psmnj.a1whm.com]
GO

/****** Object:  Table [dbo].[MLTaxMaster]    Script Date: 12/19/2019 1:59:26 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MLTaxMaster](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[TaxRate] [decimal](10, 2) NULL,
	[TaxState] [int] NULL,
	[Status] [int] NULL,
	[PrintLabel] [varchar](100) NULL,
 CONSTRAINT [PK_MLTaxMaster] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


