
CREATE OR ALTER PROCEDURE [dbo].[ProcSendDuplicateOrderlogReport] 
AS      
BEGIN     
	Declare @html varchar(max)='',@tr varchar(max)='',@Count int,@Num int=1,
	@OrderNo varchar(50),@EmpName varchar(50),@ActionDate VARCHAR(50),@NoOfOrder int,@Remarks varchar(max)=''

	select ROW_NUMBER() OVER(order by ol.ActionDate) AS RowNumber,OM.OrderNo,EM.FirstName+' '+LastName as EmpName,Format(ol.ActionDate,'MM/dd/yyyy hh:ss tt') as ActionDate,
	ISNULL(Remarks,'') as Remarks,
	count(1) as NoOfOrder into #Result	from tbl_OrderLog ol
	INNER JOIN OrderMaster AS OM ON OM.AutoId=ol.OrderAutoId
	INNER JOIN EmployeeMaster AS EM ON EM.AutoId=ol.EmpAutoId
	group by OrderNo,EM.FirstName+' '+LastName,ol.ActionDate,Remarks
	having count(1)>1
	order by ol.ActionDate

	 
	set @html='<table class="table tableCSS" border="1" style="width:100%;border: 1px solid black;border-collapse:collapse;">
	<thead>
		<tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
			<td style="text-align: center !important;">
				<h4 style="margin:10px;">Duplicate Order Log</h4>
			</td>
			</tr>
			<tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
			<td style="text-align: center !important;">
				<h4 style="margin:2px">'+(select CompanyDistributerName from CompanyDetails)+'</h4>
			</td>
			</tr>
			<tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
			<td style="text-align: center !important;">
				<h4 style="margin:2px">'+Convert(varchar(50),FORMAT(GetDate(),'MM/dd/yyyy hh:ss tt'))+'</h4>
			</td>
		</tr>
	</thead>
	<tbody></tbody>
	</table>
	<br>
	<table class="table tableCSS" border="1" style="width:100%;border: 1px solid black;border-collapse:collapse;" 
	id="tblNotShippedReportByHeader"> 
	<thead>
	<tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
		<td style="text-align: center !important;" class="text-center">Order No</td>
		<td style="text-align: center !important;" class="text-center">Employee Name</td>
		<td style="text-align: center !important;" class="text-center">Action Date</td>
		<td style="text-align: center !important;" class="text-center">Remarks</td>
		<td style="text-align: center !important;" class="text-center">No Of Order</td>
	</tr>' +   
	'</thead> <tbody>' 
	SET @Count=(select count(1) from #Result)
	if(@count>0)
	begin	
	while(@num<=@Count)
	   BEGIN 
		    SELECT @OrderNo=OrderNo,@EmpName=EmpName,@ActionDate=ActionDate,@NoOfOrder=NoOfOrder,@Remarks=Remarks FROM #Result
			where RowNumber = @num 
			set @tr='<tr>' 
			+	'<td style=''text-align:center;''>'+convert(varchar(50), @OrderNo) + '</td>' 
			+	'<td style=''text-align:center;''>'+ convert(varchar(50), @EmpName) + '</td>'
			+	'<td style=''text-align:center;''>'+convert(varchar(50), @ActionDate) + '</td>' 
			+	'<td style=''text-align:center;''>'+@Remarks + '</td>' 
			+	'<td style=''text-align:center;''>'+ convert(varchar(50), @NoOfOrder) + '</td>'
			+ '</tr>' ;
			set @num = @num + 1
			SET @html=@html+@tr
		ENd
	SET @html=@html+'</tbody></table>';
	--------------Email Code---------------

	Declare  @Subject varchar(max),@FromName varchar(1000),@FromEmailId varchar(1000),@Port int,@SMTPServer varchar(1000),@smtp_userName varchar(1000),
	@Password varchar(1000),@SSL  int,
	@BCCEmailId varchar(max),@ToEmailId varchar(1000),@CCEmailId varchar(max)
	set @Subject = 'Duplicate Order Log Report ' +UPPER((select CompanyDistributerName from dbo.CompanyDetails))+' : ' + 
	format(GETDATE(),'MM/dd/yyy hh:mm tt');
	
	SELECT @FromEmailId=FromEmail,@FromName=DisplayName,@smtp_userName=SMTP_USERNAME,@Password=SMTP_PASSWORD,
	@SMTPServer=HOST,@Port=SMTPPORT,@SSL=EnableSsl from EmailServerMaster_AWS 

	set @BCCEmailId=(select top 1 BCCEmail from [dbo].[Tbl_ReportEmailReceiver] where ReportId='R00007');
	set @ToEmailId=(select top 1 ToEmail from [dbo].[Tbl_ReportEmailReceiver] where ReportId='R00007')
	SET @CCEmailId=(select top 1 CCEmail from [dbo].[Tbl_ReportEmailReceiver] where ReportId='R00007')
   
		 EXEC [Emailing_DB].[dbo].[ProcSubmitEmail] 
			@Opcode=11,
			@FromEmailId =@FromEmailId,
			@FromName = @FromName,
			@smtp_userName=@smtp_userName,
			@Password = @Password,
			@SMTPServer = @SMTPServer,
			@Port =@Port,
			@SSL =@SSL,
			@ToEmailId =@ToEmailId,
			@CCEmailId =@CCEmailId,
			@BCCEmailId =@BCCEmailId,  
			@Subject =@Subject,
			@EmailBody = @html,
			@SentDate ='',
			@Status =0,
			@SourceApp ='PSM',
			@SubUrl ='Duplicate Order Log Report', 
			@isException=0,
			@exceptionMessage='' 
			
	END
END