drop procedure Proc_Account_OrderMaster
drop type DT_Internal_Delivered_Order_Items
/****** Object:  UserDefinedTableType [dbo].[DT_Internal_Delivered_Order_Items]    Script Date: 06-03-2020 20:31:35 ******/
CREATE TYPE [dbo].[DT_Internal_Delivered_Order_Items] AS TABLE(
	[ProductAutoId] [int] NULL,
	[IsExchange] [int] NULL,
	[UnitAutoId] [int] NULL,
	[QtyPerUnit] [int] NULL,
	[Barcode] [varchar](50) NULL,
	[QtyShip] [int] NULL,
	[QtyDel] [int] NULL,
	[TotalPieces] [int] NULL,
	[UnitPrice] [decimal](18, 2) NULL,
	[SRP] [decimal](18, 2) NULL,
	[GP] [decimal](18, 2) NULL,
	[Tax] [decimal](18, 2) NULL,
	[NetPrice] [decimal](18, 2) NULL,
	[FreshReturnQty] [int] NULL,
	[FreshReturnUnitAutoId] [int] NULL,
	[DamageReturnQty] [int] NULL,
	[DamageReturnUnitAutoId] [int] NULL,
	[MissingItemQty] [int] NULL,
	[MissingItemUnitAutoId] [int] NULL,
	[isFreeItem] [int] NULL,
	[MLQty] [decimal](10, 2) NULL,
	[Status] [int] NULL,
	[Del_CostPrice] [decimal](18 ,2) NULL,
	[Del_MinPrice] [decimal](18 ,2) NULL,
	[Del_BasePrice] [decimal](18 ,2) NULL,
	AddOnQty int null
)
GO


