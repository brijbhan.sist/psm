USE [psmnj.a1whm.com]
GO

CREATE TABLE [dbo].[contactpersontypemaster](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[TypeName] [varchar](200) NULL,
 CONSTRAINT [PK_contactpersontypemaster] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[CustomerContactPerson](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[ContactPerson] [varchar](500) NULL,
	[Type] [int] NULL,
	[MobileNo] [varchar](20) NULL,
	[Landline] [varchar](20) NULL,
	[Landline2] [varchar](20) NULL,
	[Fax] [varchar](20) NULL,
	[Email] [varchar](200) NULL,
	[AlternateEmail] [varchar](200) NULL,
	[CustomerAutoId] [int] NULL,
 CONSTRAINT [PK_CustomerContactPerson] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[CustomerContactPerson]  WITH CHECK ADD  CONSTRAINT [FK_CustomerContactPerson_contactpersontypemaster] FOREIGN KEY([Type])
REFERENCES [dbo].[contactpersontypemaster] ([AutoId])
GO

ALTER TABLE [dbo].[CustomerContactPerson] CHECK CONSTRAINT [FK_CustomerContactPerson_contactpersontypemaster]
GO

ALTER TABLE [dbo].[CustomerContactPerson]  WITH CHECK ADD  CONSTRAINT [FK_CustomerContactPerson_CustomerMaster] FOREIGN KEY([CustomerAutoId])
REFERENCES [dbo].[CustomerMaster] ([AutoId])
GO

ALTER TABLE [dbo].[CustomerContactPerson] CHECK CONSTRAINT [FK_CustomerContactPerson_CustomerMaster]
GO


