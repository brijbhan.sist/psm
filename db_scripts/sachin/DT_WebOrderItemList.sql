DROP PROCEDURE [ProcOrderMasterWeb]
DROP type [DT_WebOrderItemList]

CREATE TYPE [dbo].[DT_WebOrderItemList] AS TABLE(
	[ProductAutoId] [int] NULL,
	[UnitAutoId] [int] NULL,
	[QtyPerUnit] [int] NULL,
	[RequiredQty] [int] NULL,
	[Barcode] [varchar](50) NULL,
	[QtyShip] [int] NULL,
	[TotalPieces] [int] NULL,
	[UnitPrice] [decimal](8, 2) NULL,
	[SRP] [decimal](8, 2) NULL,
	[GP] [decimal](8, 2) NULL,
	[Tax] [int] NULL,
	[IsExchange] [int] NULL,
	[IsFreeItem] [int] NULL,
	[NetPrice] [decimal](8, 2) NULL,
	[OM_MinPrice] [decimal](8, 2) NULL,
	[OM_CostPrice] [decimal](8, 2) NULL,
	[ItemType] [varchar](50) NULL,
	[AutoId] [int] NULL
)