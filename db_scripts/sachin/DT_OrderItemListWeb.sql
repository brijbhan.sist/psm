
/****** Object:  UserDefinedTableType [dbo].[DT_OrderItemListWM]    Script Date: 06-11-2020 23:47:05 ******/
CREATE TYPE [dbo].[DT_OrderItemListWeb] AS TABLE(
	[ProductAutoId] [int] NULL,
	[UnitAutoId] [int] NULL,
	[QtyPerUnit] [int] NULL,
	[RequiredQty] [int] NULL,
	[IsExchange] [int] NULL,
	[IsFreeItem] [int] NULL,
	[Status] [bit] NULL,
	[OldUnitAutoId] [int] NULL,
	[OM_CostPrice] [decimal](8, 2) NULL,
	[OM_MinPrice] [decimal](8, 2) NULL
)
GO


