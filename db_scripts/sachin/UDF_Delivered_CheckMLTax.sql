USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[UDF_Delivered_CheckMLTax]    Script Date: 05-07-2020 04:45:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER function [dbo].[UDF_Delivered_CheckMLTax]
(
 @OrderAutoId INT  --Select dbo.UDF_CheckMLTax(3218)
)
returns int
AS
BEGIN
   Declare @isMLManualyApply int=0
  IF EXISTS(Select TaxState from MLTaxMaster Where TaxState=(Select State from BillingAddress Where AutoId=(Select BillAddrAutoId from OrderMaster where AutoId=@OrderAutoId)))
BEGIN
        IF(SELECT COUNT(1) from Delivered_Order_Items WHERE OrderAutoId=@OrderAutoId AND UnitMLQty>0)>0
BEGIN
     SET @isMLManualyApply=1
END
END
return @isMLManualyApply
END