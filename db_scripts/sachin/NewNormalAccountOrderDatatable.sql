
CREATE TYPE [dbo].[DT_Acc_Normal_Delivered_Order_Items] AS TABLE(
	[ProductAutoId] [int] NULL,
	[IsExchange] [int] NULL,
	[UnitAutoId] [int] NULL,
	[QtyPerUnit] [int] NULL,
	[Barcode] [varchar](50) NULL,
	[QtyShip] [int] NULL,
	[QtyDel] [int] NULL,
	[TotalPieces] [int] NULL,
	[UnitPrice] [decimal](8, 2) NULL,
	[SRP] [decimal](8, 2) NULL,
	[GP] [decimal](8, 2) NULL,
	[Tax] [decimal](8, 2) NULL,
	[NetPrice] [decimal](8, 2) NULL,
	[FreshReturnQty] [int] NULL,
	[FreshReturnUnitAutoId] [int] NULL,
	[DamageReturnQty] [int] NULL,
	[DamageReturnUnitAutoId] [int] NULL,
	[isFreeItem] [int] NULL,
	[MLQty] [decimal](10, 2) NULL,
	[MissingItemQty] [int] NULL,
	[MissingItemUnitAutoId] [int] NULL,
	[Del_CostPrice] [decimal](8 ,2) NULL,
	[Del_MinPrice] [decimal](8 ,2) NULL
)
GO


