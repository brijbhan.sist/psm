
CREATE TABLE [dbo].[Tbl_packerprintassign](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[ShipAutoId] [int] NULL,
	[PrintTemplate] [varchar](100) NULL,
	[Url] [varchar](100) NULL,
	[Status] [int] NULL,
	[Type] [int] NOT NULL
) ON [PRIMARY]
GO
