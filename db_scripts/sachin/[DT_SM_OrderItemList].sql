drop procedure [ProcManagerEditOrderMaster]
drop type [DT_SM_OrderItemList]
CREATE TYPE [dbo].[DT_SM_OrderItemList] AS TABLE(
	[ProductAutoId] [int] NULL,
	[UnitAutoId] [int] NULL,
	[QtyPerUnit] [int] NULL,
	[RequiredQty] [int] NULL,
	[Barcode] [varchar](50) NULL,
	[QtyShip] [int] NULL,
	[TotalPieces] [int] NULL,
	[UnitPrice] [decimal](18, 2) NULL,
	[SRP] [decimal](18, 2) NULL,
	[GP] [decimal](18, 2) NULL,
	[Tax] [int] NULL,
	[IsExchange] [int] NULL,
	[IsFreeItem] [int] NULL,
	[NetPrice] [decimal](18, 2) NULL,
	[ItemType] [varchar](50) NULL,
	[AutoId] [int] NULL,
	[OM_MinPrice] [decimal](18,2) NULL,
	[OM_CostPrice] [decimal](18,2) NULL,
	[OM_BasePrice] [decimal](18,2) NULL,
	[OM_Discount] [decimal](18,2) NULL
)
GO


