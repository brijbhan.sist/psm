drop procedure ProcOrderMasterWH
drop type [DT_OrderItemListWM]

/****** Object:  UserDefinedTableType [dbo].[DT_OrderItemListWM]    Script Date: 06-19-2020 23:41:45 ******/
CREATE TYPE [dbo].[DT_OrderItemListWM] AS TABLE(
	[ProductAutoId] [int] NULL,
	[UnitAutoId] [int] NULL,
	[QtyPerUnit] [int] NULL,
	[RequiredQty] [int] NULL,
	[IsExchange] [int] NULL,
	[IsFreeItem] [int] NULL,
	[Status] [bit] NULL,
	[OldUnitAutoId] [int] NULL,
	[OM_CostPrice] [decimal](18, 2) NULL,
	[OM_MinPrice] [decimal](18, 2) NULL,
	[ItemType] [varchar](50) NULL,
	[AutoId] [int] NULL,
	[OM_BasePrice] [decimal](18, 2) NULL,
	[OM_Discount] [decimal](18, 2) NULL
)
GO


