USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Order_MLTaxItem]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

alter table OrderItemMaster drop column Item_TotalMLTax
drop function FN_Order_MLTaxItem
go
CREATE FUNCTION  [dbo].[FN_Order_MLTaxItem]
(
	@AutoId int,
	@orderAutoId int
)
RETURNS decimal(18,2)
AS
BEGIN
	DECLARE @MLTax decimal(18,2)=0.00	
	declare @taxPer decimal(18,2)=isnull((select MLTaxPer from OrderMaster where AutoId=@orderAutoId),0.00)	 
	SET @MLTax=(SELECT TotalMLQty*@taxPer FROM OrderItemMaster WHERE AutoId=@AutoId)	 
	RETURN @MLTax
END

GO

alter table OrderItemMaster add [Item_TotalMLTax]  AS ([dbo].[FN_Order_MLTaxItem]([AutoId],[OrderAutoId]))