Create Or  ALTER PROCEDURE [dbo].[ProcBulkPrintOrderMaster]                                                                                                      
@Opcode INT=NULL,                                                                                                      
@OrderAutoId INT=NULL,                                                                                             
@LoginEmpType INT=NULL,                            
@CustomerAutoId INT=NULL,        
@CreditAutoId int=null,                                                                                           
@EmpAutoId INT=NULL,                                                                                    
@isException bit out,           
@BulkOrderAutoId varchar(max)=null,                                                                                                     
@exceptionMessage varchar(max) out     
AS                                                                                                      
BEGIN                                                                                                     
  BEGIN TRY                                                                                                      
    Set @isException=0                                                                                                      
    Set @exceptionMessage='Success'                        
 IF @Opcode=41                        
   BEGIN        
  select(select CompanyName,EmailAddress, [Address],[Website],[MobileNo],[FaxNo],TermsCondition,              
	Logo,('http://'+DB_NAME()+'/img/logo/'+Logo) as LogoforEmail,        
	(	
	SELECT   OM.[AutoId],OM.[OrderNo],CONVERT(VARCHAR(20),OM.[OrderDate], 101) AS OrderDate,                                                                                                    
	CONVERT(VARCHAR(20), OM.[DeliveryDate],101) + ' ' + convert(varchar(10), getdate(), 108)                        
	AS DeliveryDate,                                  
	CM.[CustomerId],CM.[CustomerName],
	(case when Contact1=Contact2 then Contact1 when isnull(Contact2,'')!='' then CM.[Contact1] + '/ ' + CM.[Contact2] else Contact1 end)
	As Contact,CTy.CustomerType,CT.[TermsDesc],ST.[ShippingType],                                                                                                    
	BA.[Address]+' '+BA.City+' '+S.StateName+' '+BA.Zipcode As BillAddr,S.[StateName] AS State1,BA.[City] AS City1,BA.[Zipcode] As Zipcode1,
	SA.[Address]+' '+SA.City+' '+s1.StateName+' '+SA.Zipcode As ShipAddr,S1.[StateName] As State2,SA.[City] AS City2,SA.[Zipcode] As Zipcode2,
	[dbo].[ConvertFirstLetterinCapital](BA.[Address]+(case when isnull(BA.Address2,'')!='' then ', '+BA.Address2 else '' end)+', '+BA.City)+', '+UPper(S.StateCode)+' - '+BA.Zipcode  As BillAddress,                                                                                                         
   [dbo].[ConvertFirstLetterinCapital](SA.[Address]+(case when isnull(SA.Address2,'')!='' then ', '+SA.Address2 else '' end)+', '+SA.City)+', '+Upper(S1.StateCode)+' - '+SA.Zipcode as ShipAddress,                                                                   
   OM.[TotalAmount],OM.[OverallDisc],OM.[OverallDiscAmt],                                                                                                    
	OM.[ShippingCharges],OM.[TotalTax],OM.[GrandTotal],EM.[FirstName] + ' ' + EM.[LastName] As SalesPerson,                                                                                                    
	EM1.[FirstName] + ' ' + EM1.[LastName] As PackerName,                                                                      
	isnull(DeductionAmount,0.00) as DeductionAmount,                                                                                                    
	isnull(CreditAmount,0.00)as CreditAmount,isnull(PayableAmount,0.00) as PayableAmount                                                                                                  
	,ISNULL((select t.CreditAmount from CustomerCreditMaster as t  where t.CustomerAutoId=om.CustomerAutoid),0.00) as CustomerCredit                                                                                                    
	,OrderRemarks ,                                                                                                   
	ISNULL(MLQty,0) as MLQty,isnull(MLTax,0) as MLTax,OM.Weigth_OZTaxAmount as WeightTax,CM.ContactPersonName,ISNULL(AdjustmentAmt,0) as AdjustmentAmt,                                                                                                    
	-- CM.BusinessName,om.Status,CM.Email as ReceiverEmail,        
	isnull((
		SELECT CM.[CategoryName],CM.[CategoryId],DOI.[ProductAutoId],PM.[ProductId],PM.[ProductName],DOI.[UnitAutoId],UM.[UnitType],DOI.[QtyPerUnit],                                                                                                    
		DOI.[UnitPrice],DOI.[Barcode],DOI.[QtyShip],DOI.[QtyDel],DOI.[TotalPieces],DOI.[SRP],
		case when DOI.[GP] < 0 then '---' else Convert(varchar(15),DOI.[GP]) end as  [GP],  
		case when DOI.[GP] < 0 then '---' else convert(varchar(15),DOI.[GP]) end as PrintGP  
		,DOI.[Tax],DOI.[NetPrice]                                                                                                    
		,convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice,ISNULL(FreshReturnQty,0) as FreshReturnQty,FreshReturnUnitAutoId,                         
		ISNULL(DamageReturnQty,0) as DamageReturnQty,DamageReturnUnitAutoId,                                                                                                    
		IsExchange,isnull(TaxValue,0.00) as TaxValue,ISNULL(isFreeItem,0) AS isFreeItem,                                                                                                    
		ISNULL(UnitMLQty,0) as UnitMLQtys,ISNULL(TotalMLQty,0) as TotalMLQty,                                                                                                    
		DOI.Item_TotalMLTax as TotalMLTax                               
		FROM [dbo].[Delivered_Order_Items] AS DOI                                               
		INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = DOI.[ProductAutoId]                                                                                                    
		INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = DOI.UnitAutoId                                                                                                     
		INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId]                                                    
		INNER JOIN [dbo].[PackingDetails] AS PD ON PD.[ProductAutoId] = DOI.[ProductAutoId] AND PD.[UnitType] = DOI.UnitAutoId                                  
		WHERE [OrderAutoId] =OM.AutoId and OM.Status=11  and DOI.UnitMLQty >0   and DOI.QtyShip>0  
		ORDER BY CM.[CategoryName] asc,PM.ProductName ASC for json path, INCLUDE_NULL_VALUES        
	),'[]') as Item1 ,ISNULL((select Location from PackingDetails as pd where AutoId=0),'[]') as Item2  ,
		ISNULL((select top 1 PrintLabel from MLTaxMaster where TaxState=BA.State),'') as MLTaxPrintLabel ,
		ISNULL((select top 1 PrintLabel from Tax_Weigth_OZ),'') as WeightTaxPrintLabel,
		ISNULL((select top 1 PrintLabel from [TaxTypeMaster] as tt  where tt.State=BA.State),'') as TaxPrintLabel
		 FROM [dbo].[OrderMaster] As OM   --  WeightTax added by Rizwan Ahmad on 17-09-2019                                           
		 INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.Status AND SM.[Category] = 'OrderMaster'                                                                               
		 INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[AutoId] = OM.[BillAddrAutoId]                                                                                                    
		 INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = OM.[ShipAddrAutoId]                                                 
		 INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                                                                                                     
		 LEFT JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                                                                                         
		 INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                                                                                     
		 INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]                                                                                                    
		 LEFT JOIN [dbo].[CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                                                                                
		 LEFT JOIN [dbo].[ShippingType] AS ST ON ST.[AutoId] = OM.[ShippingType]                                                                                                     
		 left JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[SalesPersonAutoId]                                                                                                           
		 left JOIN [dbo].[EmployeeMaster] AS EM1 ON EM1.[AutoId] = OM.[PackerAutoId]                         
		where convert(date,orderdate) between convert(date,'07/01/2020') and convert(date,'12/31/2020')
		and om.AutoId in (
		select Orderautoid from Delivered_Order_Items where UnitMLQty >0 and QtyShip>0 and TotalMLQty>0 
		)
		and om.Status=11 and IsMLTaxApply=1 and MLTax>0 order by OM.Stoppage 
		
		asc for json path, INCLUDE_NULL_VALUES) as OrderDetails        
		 from CompanyDetails for json path, INCLUDE_NULL_VALUES) as CompanyD        
  END   
  END TRY                                                                                                      
  BEGIN CATCH                                              
  Set @isException=1                                                                     
  Set @exceptionMessage='Oops, Something went wrong.Please try later.'                                                                                                      
  END CATCH                                                                                           
END 