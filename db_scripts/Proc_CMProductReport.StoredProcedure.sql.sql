
Alter PROCEDURE  [dbo].[Proc_CMProductReport]
              
@Opcode INT=NULL,              
@SalesPersonAutoId int = null,              
@CustomerAutoId int=null,              
@DriverAutoId int=null,              
@FromDate datetime = null,              
@ToDate datetime = null,              
@Status int = null,                 
@PageIndex INT=1,                
@PageSize INT=10,                
@RecordCount INT=null,                
@isException bit out,                
@exceptionMessage varchar(max) out                
AS                
BEGIN                 
 BEGIN TRY                
  Set @isException=0                
  Set @exceptionMessage='Success'                
              
 IF @Opcode=42                     
  BEGIN                 
        
  SELECT AutoId,FirstName +' '+ isnull(LastName,'') as SalesPerson  from EmployeeMaster where EmpType = 2 and status = 1  order by SalesPerson ASC  
  for json path

  END   
  
 ELSE if(@Opcode = 43) 
 
   begin        
		 SELECT AutoId as CustomerAutoId,CustomerId + ' ' + CustomerName as CustomerName FROM CustomerMaster  where (SalesPersonAutoId=@SalesPersonAutoId or @SalesPersonAutoId=0)
         order by replace(CustomerId + ' ' + CustomerName,' ','') ASC              
		for json path
    end   

  ELSE IF @Opcode=41  
  
  BEGIN                
   SELECT ROW_NUMBER() OVER(ORDER BY SalesPerson) AS RowNumber, * INTO #Results FROM                
    (                
        select cm.CustomerId,cm.CustomerName,CreditNo,format(CreditDate,'MM/dd/yyyy') as CreditDate, isnull(om.OrderNo,'Not Applied') as AppliedOrder,
        emp.FirstName as SalesPerson,pm.ProductId,pm.ProductName,um.UnitType,sum(cim.AcceptedQty) as AcceptedQty,cim.NetAmount as NetAmount from CreditItemMaster as cim
        inner join CreditMemoMaster as cmm on cmm.CreditAutoId=cim.CreditAutoId
		left join OrderMaster as om on om.AutoId=cmm.OrderAutoId
        inner join EmployeeMaster as emp on emp.AutoId=cmm.SalesPersonAutoId
        inner join ProductMaster as pm on pm.AutoId=cim.ProductAutoId
        inner join UnitMaster as um on um.AutoId=cim.UnitAutoId   
        inner join CustomerMaster as cm on cm.AutoId=cmm.CustomerAutoId
        where cmm.Status=3

  and (@CustomerAutoId = '0' or @CustomerAutoId is null or cm.AutoId = @CustomerAutoId )              
  and (@SalesPersonAutoId = '0' or @SalesPersonAutoId is null or cmm.SalesPersonAutoId = @SalesPersonAutoId)                                      
  and (@FromDate = '' or @FromDate is null or @ToDate = '' or @ToDate is null or CONVERT(date, CreditDate) between @FromDate and @ToDate)              
  GROUP BY cm.CustomerId,cm.CustomerName,CreditNo, CreditDate,om.OrderNo,pm.ProductId,pm.ProductName,emp.FirstName,um.UnitType,cim.NetAmount          
  having sum(cim.AcceptedQty)>0       
 
               
 ) AS t ORDER BY CreditDate desc            
  SELECT COUNT(RowNumber) AS RecordCount,case when @PageSize=0 then COUNT(*) else  @PageSize end AS PageSize, @PageIndex AS PageIndex,Format(getdate(),'MM/dd/yyyy hh:mm tt') as PrintDate FROM #Results                
                    
    SELECT * FROM #Results                
    WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))           
  select sum(isnull(AcceptedQty,0))as OverAllAcceptedQty,SUM( ISNULL(NetAmount,0)) as OverAllNetAmount FROM #Results          
  END                 
 END TRY                
 BEGIN CATCH                
  Set @isException=1                
  Set @exceptionMessage=ERROR_MESSAGE()                
 END CATCH                
END 
GO
