ALTER PROCEDURE [dbo].[ProcOrderMasterWeb]                                                                                                                                                    
 @Opcode INT=NULL,                                                                                                                                                    
 @OrderAutoId INT=NULL,  
 @ApproveStatus INT=NULL,
 @OrderNo VARCHAR(15)=NULL,                                                                                                                                                    
 @IsExchange int=NULL,                                                                                                                                                                                                                                                                                                       
 @DeliveryDate DATETIME=NULL,                                                                                                                                                    
 @CustomerAutoId INT=NULL,                                                                                                                                                    
 @TaxType int=NULL,                                                                                                                                                                                                                                                                                                       
 @IsFreeItem  INT=NULL,                                                                                                                                                    
 @IsTaxable int =NULL,                                                                                                                                                    
 @CreditAmount decimal(18,2)=null, 
 @DeductAmount decimal(18,2)=null,                                                                                                                                                    
 @ProductAutoId INT=NULL,    
 @UnitAutoId INT=NULL,           
 @SalesPersonAutoId INT=NULL,           
 @LoginEmpType INT=NULL,                                        
 @EmpAutoId INT=NULL,                                                  
 @FreeItem bit=NULL,                                                          
 @OrderStatus INT=NULL,                                                                                                 
 @ShippingType INT=NULL,                                                   
 @ReqQty  INT=NULL,                                                                                
 @QtyPerUnit INT=NULL,                                                                                                            
 @UnitPrice DECIMAL(18,2)=NULL,                                                                                                        
 @SRP DECIMAL(18,2)=NULL,                                                                                      
 @GP DECIMAL(18,2)=NULL,                                                                                                                                 
 @TaxRate DECIMAL(18,2)=NULL,                                                                                                     
 @minprice  DECIMAL(18,2)=NULL,                                                                                                       
 @TotalAmount DECIMAL(18,2)=NULL,                                                                                                                                            
 @OverallDisc DECIMAL(18,2)=NULL,                                                                                               
 @OverallDiscAmt DECIMAL(18,2)=NULL,                                                                                                                             
 @ShippingCharges DECIMAL(18,2)=NULL,                                                                                                                                                    
 @MLQty DECIMAL(18,2)=NULL,                                                                                                                                                    
 @TotalTax DECIMAL(18,2)=NULL,                                                                                           
 @GrandTotal DECIMAL(18,2)=NULL,                                                                                                                                   
 @Remarks VARCHAR(500)=NULL,                                                                    
 @AddressType INT=NULL,                              
 @Address VARCHAR(200)=NULL,                                     
 @State INT=NULL,                                    
 @City VARCHAR(50)=NULL,   
 @ZipAutoId INT=NULL,
 @Zipcode varchar(10)=NULL,                                                       
 @AddressAutoId INT=NULL,                                                                       
 @BillAddrAutoId INT=NULL,                                                   
 @ShipAddrAutoId INT=NULL,                                                        
 @TableValue DT_WebOrderItemList readonly,                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
 @Barcode varchar(50)=NULL,                                                                                                                                                                                                      
 @PriceLevelAutoId INT=NULL,              
 @PageIndex INT = 1,                
 @PageSize INT = 10,                                                                                                                            
 @RecordCount INT =null,                             
 @LogRemark varchar(200) = NULL,                                                                                                                                                    
 @DraftAutoId int =null out,                                         
 @CheckSecurity VARCHAR(250)=NULL,                                                     
 @isException bit out,                                           
 @exceptionMessage varchar(max) out                                                                                      
AS                                                                                                                                                    
BEGIN                                                                                                                               
  BEGIN TRY                                                                                                                                            
   Set @isException=0                                                                                                                           
   Set @exceptionMessage='Success'                                                                                                         
   declare @customerType varchar(50)  ,@ShippingTaxEnabled int    
   
IF @Opcode=503
	BEGIN                                                                                                       
		select(
		select
		isnull((SELECT [AutoId],convert(varchar(10),[ProductId]) + '--' + [ProductName] as [ProductName],ISNULL(MLQty,0) AS MLQty, isnull(WeightOz,0)                      
		as WeightOz FROM [dbo].[ProductMaster] as pm where ProductStatus=1 and (select count(1) from PackingDetails where  ProductAutoId=pm.AutoId)>0                                                                                                                           
		order by [ProductName] ASC for json path, INCLUDE_NULL_VALUES),'[]') as Product
	
		for json path, INCLUDE_NULL_VALUES
		)as DropDownList                                                                                                                                                   
	END
                                                                                                                     
                                                                                                                 
ELSE If @Opcode=201
BEGIN
	BEGIN TRY                                                                                                     
	BEGIN TRAN                                                                                                                                
	IF @LoginEmpType=2 AND (select Status FROM [dbo].[OrderMaster] WHERE [AutoId] = @OrderAutoId )>1                                                                                                                                               
	BEGIN                                                                                                               
		SET @isException=1                                                                                      
		SET @exceptionMessage='Order has been processed so you can not update.'                                                                        
	END                                                                                                                                                
	ELSE IF @LoginEmpType=7 AND (select Status FROM [dbo].[OrderMaster] WHERE [AutoId] = @OrderAutoId )>2                                                                          
	BEGIN                                                                                                     
		SET @isException=1                                                                                                                                                
		SET @exceptionMessage='Order has been packed so you can not update.'                                                                                                                                                
	END                                          
	ELSE                                                                           
	BEGIN                    
	SET @BillAddrAutoId=(SELECT TOP 1 BillAddrAutoId FROM ordermaster WHERE AutoId =@OrderAutoId)       
	SET @State=(SELECT TOP 1 State FROM BillingAddress WHERE AutoId =@BillAddrAutoId)  
	SET @ShippingTaxEnabled=(SELECT EnabledTax FROM ShippingType WHERE AutoId=@ShippingType)
                                                          
	UPDATE  [dbo].[OrderMaster] SET                             
	[CustomerAutoId] = @CustomerAutoId,                                                                         
	[BillAddrAutoId] = @BillAddrAutoId,                                                                                                                                                    
	[ShipAddrAutoId] = @ShipAddrAutoId,                                                                                                                                                    
	[DeliveryDate]  = @DeliveryDate,        
	TaxType=@TaxType,            
	TaxValue= (select Value from TaxTypeMaster where autoid=@TaxType),       
	IsTaxApply=CASE WHEN (select count(1) from TaxTypeMaster where State=@State)=0 then 0 else 1 end,                                                                                                                                                        
	[OverallDiscAmt] = @OverallDiscAmt,                                         
	[ShippingCharges] = @ShippingCharges,                                                                                                 
	[ShippingType]  = CASE WHEN ISNULL(@ShippingType,0)=0 THEN ShippingType ELSE @ShippingType END,                                   
	[ManagerRemarks] = @Remarks,
	ManagerAutoId=@EmpAutoId,
	ShippingTaxEnabled=@ShippingTaxEnabled
	WHERE [AutoId] = @OrderAutoId                                                                        
                                                                                                                         
	UPDATE [dbo].[Order_Original] SET                                                     
	[CustomerAutoId] = @CustomerAutoId,                                                                 
	[BillAddrAutoId] = @BillAddrAutoId,                                                                                                                                                    
	[ShipAddrAutoId] = @ShipAddrAutoId,                                                                                                                                                    
	[DeliveryDate]  = @DeliveryDate,                                                                                 
	[TotalAmount]  = @TotalAmount,                                                                                                                    
	[OverallDiscAmt] = @OverallDiscAmt,                                                                                                                
	[ShippingCharges] = @ShippingCharges,                                                                                                                                                    
	[TotalTax]   = @TotalTax,                                                                                   
	[GrandTotal]  = @GrandTotal,                                                                                                                                     
	[ShippingType]  = CASE WHEN ISNULL(@ShippingType,0)=0 THEN ShippingType ELSE @ShippingType END                                                                                                                  
	WHERE [AutoId] = @OrderAutoId                                                                                                                                
 
	select * into #tempOIM from [dbo].[OrderItemMaster] where [OrderAutoId] = @OrderAutoId    

	 DELETE FROM [dbo].[OrderItemMaster] WHERE [OrderAutoId] = @OrderAutoId    

	Declare @Stat int      
	Select @Stat=Status From OrderMaster WHERE  [AutoId] = @OrderAutoId 

	INSERT INTO [dbo].[OrderItemMaster]([OrderAutoId],[ProductAutoId],[UnitTypeAutoId],[QtyPerUnit],[UnitPrice],                                                                                                 
	[RequiredQty],[SRP],[GP],[Tax],[Barcode],[QtyShip],[RemainQty],IsExchange,UnitMLQty,isFreeItem,Weight_Oz,AddOnQty,OM_CostPrice,OM_MinPrice)                                                                              
	SELECT @OrderAutoId,tb.[ProductAutoId],tb.[UnitAutoId],tb.[QtyPerUnit],tb.[UnitPrice],tb.[RequiredQty]                                  
	,pm.[P_SRP],tb.[GP],tb.[Tax],tb.[Barcode],tb.[QtyShip],(tb.[RequiredQty] - tb.[QtyShip])                                                             
	,IsExchange,
	case when pm.IsApply_ML=1 and IsExchange=0 then ISNULL(pm.MLQty,0) else 0 end,
	isFreeItem,
	case when pm.IsApply_Oz=1 and IsExchange=0  then ISNULL(pm.WeightOz,0) else 0 end,
	case when @Stat=9 then Isnull(RequiredQty-QtyShip,0) else 0 end,
	tb.OM_CostPrice,tb.OM_MinPrice FROM @TableValue AS tb                                                                                                           
	inner join ProductMaster as pm on pm.AutoId=tb.ProductAutoId
	inner join PackingDetails as pd on pd.ProductAutoId=pm.AutoId and pd.UnitType=tb.UnitAutoId
	--where tb.ItemType='New' 
	   
	--update  oim set oim.[UnitTypeAutoId]=rdt.[UnitAutoId],oim.[QtyPerUnit]=
	--rdt.[QtyPerUnit],[UnitPrice]=rdt.[UnitPrice],oim.[RequiredQty]=rdt.[RequiredQty],oim.[SRP]=rdt.[SRP],oim.[GP]=rdt.[GP],oim.[Tax]=rdt.[Tax],oim.[Barcode]=rdt.[Barcode],
	--oim.[QtyShip]=rdt.[QtyShip],oim.[RemainQty]=(rdt.[RequiredQty] - rdt.[QtyShip]),oim.IsExchange=rdt.[IsExchange],
	--oim.UnitMLQty=case when pm.IsApply_ML=1 and oim.IsExchange=0 then ISNULL(pm.MLQty,0) else 0 end,
	--oim.isFreeItem=rdt.[IsFreeItem],
	--oim.Weight_Oz=case when pm.IsApply_Oz=1 and oim.IsExchange=0  then ISNULL(pm.WeightOz,0) else 0 end,
	--oim.AddOnQty=case when rdt.[RequiredQty] > oim.[RequiredQty] then rdt.[RequiredQty]-oim.[RequiredQty] else 0 end
	--from [OrderItemMaster] as oim
	--inner join @TableValue  as rdt on rdt.[ProductAutoId]=oim.[ProductAutoId]
	--inner join ProductMaster as pm on pm.AutoId=rdt.ProductAutoId where rdt.ItemType='Old' and oim.[AutoId]=rdt.[AutoId]						 							 
	
       
	Update OIM Set          
	OIM.Original_UnitType=dt.Original_UnitType          
	from [dbo].[OrderItemMaster] as OIM           
	inner join (SELECT * FROM #tempOIM) AS dt On dt.[ProductAutoId] = OIM.[ProductAutoId] and dt.[isFreeItem] = OIM.[isFreeItem]          
	and dt.[OrderAutoId]=OIM.[OrderAutoId] and dt.[Tax] = OIM.[Tax] and dt.[IsExchange] = OIM.[IsExchange]          
	WHERE OIM.OrderAutoId = @OrderAutoId          
              
	DELETE FROM [dbo].[OrderItems_Original] WHERE [OrderAutoId] = @OrderAutoId                                                                                                                                                    
                             
	INSERT INTO [dbo].[OrderItems_Original]([OrderAutoId],[ProductAutoId],[UnitTypeAutoId],[QtyPerUnit],[UnitPrice],[RequiredQty],[TotalPieces],                                                                                                                  
           
	[SRP],[GP],[Tax],[NetPrice],isFreeItem,UnitMLQty,TotalMLQty)                                                                
	SELECT @OrderAutoId,tb.[ProductAutoId],tb.[UnitAutoId],tb.[QtyPerUnit],tb.[UnitPrice],tb.[RequiredQty],tb.[TotalPieces],tb.[SRP],                                                                 
                        
                         
	tb.[GP],tb.[Tax],tb.[NetPrice],isFreeItem,ISNULL(pm.MLQty,0),(ISNULL(pm.MLQty,0)*(tb.RequiredQty*[QtyPerUnit])) FROM @TableValue AS tb                                                                    
	inner join ProductMaster as pm on pm.AutoId=tb.ProductAutoId                                                                                                                                                      
                                                                                                                                                    
	SET @PriceLevelAutoId = (SELECT [PriceLevelAutoId] FROM [dbo].[CustomerPriceLevel] WHERE [CustomerAutoId] = @CustomerAutoId)                                                  
	exec UPDTAE_PRICELEVEL                 
	@OrderAutoId=@OrderAutoId, @PriceLevelAutoId=@PriceLevelAutoId,@EmpAutoId=@EmpAutoId                                                                                                                                
 
	SET @LogRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=2),'[OrderNo]',(SELECT [OrderNo] FROM OrderMaster where [AutoId] = @OrderAutoId))                                                                                
 
	INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                                                              
	select 2,@EmpAutoId,getdate(),CONVERT(varchar(50),pm.productId)+' has been added',@OrderAutoId from @TableValue as tb                                                                                                            
	inner join ProductMaster as pm on pm.AutoId=tb.ProductAutoId 
	where tb.ItemType='New' 

	INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                                                              
	select 2,@EmpAutoId,getdate(),CONVERT(varchar(50),pm.productId)+' has been updated',@OrderAutoId from @TableValue as tb                                                                                                            
	inner join ProductMaster as pm on pm.AutoId=tb.ProductAutoId 
	inner join OrderItemMaster oim on oim.ProductAutoId=tb.ProductAutoId
	where tb.ItemType='Old' and tb.NetPrice != oim.NetPrice and oim.AutoId=@OrderAutoId		
		
	INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])    
	select 2,@EmpAutoId,getdate(),CONVERT(varchar(50),pm.productId)+' has been updated with '+ convert(varchar(max),oim.RequiredQty-tmp.RequiredQty)+' quantity',@OrderAutoId from OrderItemMaster as oim                                                                                                            
	inner join ProductMaster as pm on pm.AutoId=oim.ProductAutoId
	inner join #tempOIM tmp on tmp.AutoId=oim.AutoId
	where tmp.RequiredQty < oim.RequiredQty

	INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId]) 
	values (2,@EmpAutoId,getdate(),@LogRemark,@OrderAutoId) 

	Drop table #tempOIM           

	SELECT @CustomerAutoId=CustomerAutoId FROM [dbo].[OrderMaster]                 
	WHERE [AutoId] = @OrderAutoId   
	


	if((select CustomerType from CustomerMaster where AutoId=@CustomerAutoId)=3)                      
	begin                      
		exec [dbo].[ProcUpdatePOStatus_All]                      
		@CustomerId=@CustomerAutoId,                      
		@OrderAutoId=@OrderAutoId,                      
		@Status=1 
	end 
		IF @ApproveStatus=22
			BEGIN
			IF EXISTS(SELECT [AutoId] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId AND Status=0)
			BEGIN                                                                                                 
			  UPDATE OrderMaster set Status=1 WHERE [AutoId]=@OrderAutoId                                                                                                           
			END 
		END
	END                                                                                                                                                
	COMMIT TRANSACTION                                                                                  
	END TRY                                                                                                                                                    
	BEGIN CATCH                                                                      
	ROLLBACK TRAN                                          
		Set @isException=1                                                                                                    
		Set @exceptionMessage='Oops! Something went wrong.Please try later.'                                                                                               
	END CATCH                                                     
END                                                                                                                                         
ELSE IF @Opcode=204                                                                        
    BEGIN                                                                                                             
		SET @CreditAmount=ISNULL((SELECT CreditAmount FROM OrderMaster WHERE AutoId=@OrderAutoId),0)                                                                                                                  
		SET @CustomerAutoId=(SELECT CustomerAutoId FROM OrderMaster WHERE AutoId=@OrderAutoId)                    
		IF(ISNULL((SELECT CreditAmount FROM OrderMaster WHERE AutoId=@OrderAutoId ),0)!=0)                                                                                                   
		BEGIN                                                                                                                                                
		UPDATE CustomerCreditMaster SET CreditAmount=ISNULL(CreditAmount,0)+@CreditAmount WHERE CustomerAutoId=@CustomerAutoId      
		END                                                                                                               
		SET @DeductAmount =ISNULL((SELECT Deductionamount FROM OrderMaster WHERE AutoId=@OrderAutoId),0)                                                                                                                                              
		SET @GrandTotal=ISNULL((SELECT GrandTotal FROM OrderMaster WHERE AutoId=@OrderAutoId),0)                                                                                                 
		if(@DeductAmount>@GrandTotal )                                                                                                                                  
		BEGIN                                                                                                
		UPDATE CustomerCreditMaster SET CreditAmount=ISNULL(CreditAmount,0)-ISNULL((@DeductAmount-@GrandTotal),0) WHERE CustomerAutoId=@CustomerAutoId   
		END                                                                                     
		DELETE from tbl_Custumor_StoreCreditLog where ReferenceNo=CONVERT(varchar(50),@OrderAutoId) and ReferenceType='OrderMaster'                                                                                                               
		UPDATE CreditMemoMaster set OrderAutoId=null where OrderAutoId=@OrderAutoId 
			 
		IF NOT EXISTS(SELECT AutoId FROM OrderMaster WHERE AutoId=@orderAutoId AND Status IN (1,2))  
		BEGIN 
		insert into [dbo].[ProductStockUpdateLog]([ProductId],[OledStock],[NewStock],[UserAutoId],[DateTime],ReferenceId,ActionRemark,ReferenceType)
		SELECT pm.ProductId,isnull([Stock],0),(isnull(pm.Stock,0)+(isnull(dt.QtyPerUnit,0) * isnull(dt.QtyShip,0))),@EmpAutoId,GETDATE(),dt.OrderAutoId,
		'Order No-(' + om.OrderNo + ') has cancelled by '+(Select FirstName+' '+LastName from Employeemaster where autoid=@EmpAutoId)+'.' ,'OrderMaster'
		FROM OrderItemMaster as dt
		inner join productmaster pm on dt.[ProductAutoId] = PM.[AutoId]
		inner join OrderMaster om on dt.OrderAutoId=om.AutoId
		where dt.OrderAutoId=@OrderAutoId and isnull(dt.QtyShip,0)>0 

		UPDATE PM SET [Stock] = isnull([Stock],0) + (isnull(dt.QtyPerUnit,0)*isnull(dt.QtyShip,0)) FROM [dbo].[ProductMaster] AS PM                                                                                                   
		INNER JOIN (SELECT QtyShip,QtyPerUnit,ProductAutoId FROM OrderItemMaster where OrderAutoId=@OrderAutoId) AS dt 
		ON dt.[ProductAutoId] = PM.[AutoId]
				 
		End
			                               
		UPDATE [dbo].[OrderMaster] SET [Status]=8,UPDATEDATE=GETDATE(),UPDATEDBY=@EmpAutoId,ManagerAutoId=@EmpAutoId,ManagerRemarks=@Remarks WHERE [AutoId]=@OrderAutoId  
			                              
		SELECT OM.[Status],SM.[StatusType] FROM [dbo].[OrderMaster] AS OM                             
		INNER JOIN [dbo].[StatusMaster] As SM ON SM.[AutoId] = OM.[Status] WHERE OM.[AutoId]=@OrderAutoId                                                                              
                                              
                              
		INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                      
		VALUES(7,@EmpAutoId,getdate(),'Cancelled',@OrderAutoId)     
    
		IF((select CustomerType from CustomerMaster where AutoId=@CustomerAutoId)=3)              
		Begin              
		exec [dbo].[ProcUpdatePOStatus_All]              
		@CustomerId=@CustomerAutoId,              
		@OrderAutoId=@OrderAutoId,              
		@Status=8                  
		End                                                                                
                                                                                
END                                                                                                                                
   
ELSE IF @Opcode=4011                                                                                                                                           
BEGIN                                                                                                       
	select(
		select
			isnull((SELECT [AutoId],[StatusType] FROM [dbo].[StatusMaster] WHERE [AutoId]=1 and [Category]='OrderMaster' for json path, INCLUDE_NULL_VALUES),'[]') as Status,
			isnull(( SELECT [AutoId],[CustomerId] + ' - ' + [CustomerName] As Customer FROM [dbo].[CustomerMaster] where Status=1 and @LoginEmpType != 2      
			Order by Customer for json path, INCLUDE_NULL_VALUES),'[]') as Customer1,
			isnull((SELECT [AutoId],[CustomerId] + ' - ' + [CustomerName] As Customer FROM [dbo].[CustomerMaster] WHERE [SalesPersonAutoId]=@SalesPersonAutoId        
			and status=1 and @LoginEmpType = 2 ORDER BY Customer  ASC for json path, INCLUDE_NULL_VALUES),'[]') as Customer2,
			isnull((SELECT * FROM [dbo].[ShippingType]  ORDER BY ShippingType  ASC  for json path, INCLUDE_NULL_VALUES),'[]') as ShippingType
	
		for json path, INCLUDE_NULL_VALUES
	)as DropDownList                                                                                                                                                   
END                                                                                                                                                       
                                                                                                 
ELSE IF @Opcode=403                                                            
BEGIN                       
	DECLARE @custType int=(SELECT CustomerType FROM CustomerMaster WHERE AutoId=@CustomerAutoId)                                                                   
	SELECT (CASE                                                                                                                                                     
	WHEN @custType=2 THEN ISNULL(WHminPrice,0)                                                                                                                                                    
	WHEN @custType=3  THEN ISNULL(CostPrice,0)                                                                                                                   
	ELSE [MinPrice] END) AS [MinPrice] ,[CostPrice],(case  WHEN  @custType=3 then ISNULL(CostPrice,0) else [Price] end)                                        
	[Price],CONVERT(DECIMAL(10,2),((PM.[P_SRP]-([Price]/(CASE WHEN Qty=0 THEN 1 ELSE [Qty] END)))/(CASE WHEN PM.[P_SRP]=0 THEN 1 ELSE PM.[P_SRP] END)) * 100) AS GP,                                                                                         
	isnull((select Top 1 [CustomPrice] from [dbo].[ProductPricingInPriceLevel] AS PPL where PPL.[ProductAutoId] = PD.[ProductAutoId]                       
	AND PPL.[UnitAutoId] = PD.[UnitType]                                                                                                         
	AND [PriceLevelAutoId] = (SELECT [PriceLevelAutoId] FROM [dbo].[CustomerPriceLevel] WHERE [CustomerAutoId]=@CustomerAutoId)                                                                                  
	Order by [CustomPrice] desc),null) as [CustomPrice],PM.[Stock]/(CASE WHEN Qty=0 THEN 1 ELSE Qty END) as Stock,PM.[TaxRate],PM.[P_SRP] AS [SRP] FROM [dbo].[PackingDetails] As PD                        
	INNER JOIN [dbo].[ProductMaster] AS PM ON PD.[ProductAutoId]=PM.[AutoId]                                                                                                                                                                       
	WHERE PD.[ProductAutoId] = @ProductAutoId and PD.[UnitType] = @UnitAutoId                                                              
END

ELSE IF @Opcode=404                                                                                                                                    
BEGIN                                                                                                                                                    
    SELECT UM.AutoId AS AutoId, UM.[UnitType] AS UnitType, [Qty],ISNULL(EligibleforFree,0) as EligibleforFree FROM [dbo].[PackingDetails] AS PD                                                      
    INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = PD.UnitType WHERE PD.[ProductAutoId] = @ProductAutoId   ORDER BY  UM.[UnitType] ASC                                                                                                          
    SELECT PackingAutoId AS AutoId FROM ProductMaster  WHERE  AutoId=@ProductAutoId                                                       
END                                                                                                                                                       
                                                                                                                                               
ELSE IF @Opcode=406                                                                                                                                                    
   BEGIN                                                                                                               
	SET @OrderAutoId = (SELECT [AutoId] FROM [dbo].[OrderMaster] WHERE [OrderNo]=@OrderNo)    
	
	SELECT * FROM (                                                                                                                                                        
	SELECT 'Driver' as EmpType,DrvRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om                                      
	inner join EmployeeMaster as emp on emp.AutoId=om.Driver                                                                                                  
	where om.AutoId=@OrderAutoId   and ISNULL(DrvRemarks,'')!=''                                                        
	UNION                                                                                             
	SELECT 'Account' as EmpType,AcctRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om  
	inner join EmployeeMaster as emp on emp.AutoId=om.AccountAutoId where om.AutoId=@OrderAutoId                                                      
	and ISNULL(AcctRemarks,'')!=''                                                                                                                            
	UNION                                                                                                                                                    
	SELECT 'Sales Person' as EmpType,OrderRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName  from OrderMaster as om  
	inner join EmployeeMaster as emp on emp.AutoId=om.SalesPersonAutoId where                                                                                          
	om.AutoId=@OrderAutoId            and ISNULL(OrderRemarks,'')!=''                                          
	UNION                                                                                                               
	SELECT 'Packer' as EmpType,PackerRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om                                                                                          
	inner join EmployeeMaster as emp on emp.AutoId=om.PackerAutoId                                        
	where om.AutoId=@OrderAutoId          and ISNULL(PackerRemarks,'')!=''                                                                                                                                   
	UNION                                                          
	SELECT 'Manager' as EmpType,ManagerRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName  from OrderMaster as om                                                                                                                         
	inner join EmployeeMaster as emp on emp.AutoId=om.ManagerAutoId                                                                          
	where om.AutoId=@OrderAutoId     and ISNULL(ManagerRemarks,'')!=''                                                                                                                       
	UNION                                                                                                                                                    
	SELECT 'Warehouse' as EmpType,WarehouseRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName as EmpName  from OrderMaster as om                                      
	inner join EmployeeMaster as emp on emp.AutoId=om.WarehouseAutoId                                                                                                                                                     
	where om.AutoId=@OrderAutoId             and ISNULL(WarehouseRemarks,'')!=''                                                                                                                                      
	) AS T                          
                                                                                                                                           
	SET @CustomerAutoId= (SELECT [CustomerAutoId] FROM [dbo].[OrderMaster] WHERE AutoId=@OrderAutoId)                                                                                                                                                    
	SET @OrderStatus = (SELECT [Status] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId)                                                    
                                                                                                       
	SELECT PackerAutoId,OM.[AutoId],OM.[OrderNo],CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,ISNULL((CONVERT(VARCHAR(20), OM.[DeliveryDate], 101)),                                                         
	(CONVERT(VARCHAR(20), OM.DelDate,101) + ' ' + convert(varchar(10), DelDate, 108))                                                                                                                                                    
	) AS DeliveryDate,TaxType,                                                                                                                                                    
	OM.[CustomerAutoId],CT.[TermsDesc],OM.[BillAddrAutoId],OM.[ShipAddrAutoId],CTy.CustomerType,
	CM.[AutoId] As CustAutoId,CM.[CustomerId],CM.[CustomerName],CM.[Contact1] + '/' + CM.[Contact2] As Contact,CTy.CustomerType,CT.[TermsDesc],                                      
    [dbo].[ConvertFirstLetterinCapital](BA.[Address]+(case when isnull(BA.Address2,'')!='' then ', '+BA.Address2 else '' end)+', '+BA.City)+', '+UPper(S.StateCode)+' - '+BA.Zipcode As BillAddr,
    [dbo].[ConvertFirstLetterinCapital](SA.[Address]+(case when isnull(SA.Address2,'')!='' then ', '+SA.Address2 else '' end)+', '+SA.City)+', '+S1.StateCode+' - '+SA.Zipcode As ShipAddr,                                            	  
	OM.[Status] As StatusCode,DO.[AmtPaid],DO.[AmtDue],                                                             
	SM.[StatusType] AS Status,OM.[TotalAmount],OM.[OverallDisc],OM.[OverallDiscAmt],OM.[ShippingCharges],OM.[TotalTax],OM.[GrandTotal],OM.[PackedBoxes],CommentType,Comment,                                                          
	OM.[Driver],EM.[FirstName]+ ' ' + EM.[LastName] AS DrvName,OM.[ShippingType],ST.ShippingType as ShippingTypeName,CONVERT(VARCHAR(20),OM.[AssignDate],101) AS AssignDate,[DrvRemarks],isnull(DeductionAmount,0.00) as DeductionAmount,ManagerRemarks,                  
	isnull(OM.CreditAmount,0.00)as CreditAmount,isnull(OM.PayableAmount,0.00) as PayableAmount,                                                                                                                                          
	ISNULL((select t.CreditAmount from CustomerCreditMaster as t  where t.CustomerAutoId=om.CustomerAutoid),0.00) as CustomerCredit,                                                                                                                           
	PackerRemarks,OrderRemarks,ISNULL(Times,0) as Times,ISNULL(om.MLQty,0) AS MLQty ,ISNULL(om.MLTax ,0) AS MLTax ,                                                                                                                   
	WarehouseRemarks,ISNULL(om.AdjustmentAmt,0) as AdjustmentAmt,isnull(Weigth_OZQty,0) as Weigth_OZQty,
	isnull(OM.Weigth_OZTax,0) as Weigth_OZTax,isnull(OM.Weigth_OZTaxAmount,0) as  Weigth_OZTaxAmount,CM.CustomerName,CM.CustomerType                                                       
	FROM [dbo].[OrderMaster] As OM                                                                             
	LEFT JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.Status AND SM.[Category] = 
	CASE WHEN OM.Status= 0 THEN 'Website' ELSE 'OrderMaster' END                                                                                                                 
	INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[AutoId] = OM.[BillAddrAutoId]                                                                                                                                   
	INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = OM.[ShipAddrAutoId]                                           
	LEFT JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                                                                                                                                                     
	LEFT JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                                                                                                                                                      
	LEFT JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]  
	INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]
	LEFT JOIN [CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                                                    
	LEFT JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[Driver]                                               
	left JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId]  
	inner join ShippingType as ST on ST.AutoId=OM.ShippingType                       
	WHERE OM.AutoId = @OrderAutoId                                                                                                                                                  
	---------------                                                                                                
	SET @customerType=(select CustomerType from CustomerMaster where AutoId=@CustomerAutoId)  
                                                   
		SELECT OIM.AutoId as ItemAutoId, PM.[AutoId] AS ProductAutoId,PM.[ProductId],PM.[ProductName],UM.AutoId AS UnitAutoId,UM.[UnitType],OIM.[QtyPerUnit],                                                                                                        
		OIM.[UnitPrice],convert(decimal(10,2),UnitPrice/QtyPerUnit) as PerpiecePrice,                                                                                                                            
		ISNULL(OIM.OM_MinPrice,0) as [MinPrice],PD.[Price],OIM.[RequiredQty],ISNULL(OIM.OM_CostPrice,0) as OM_CostPrice,ISNULL(OIM.OM_MinPrice,0) as OM_MinPrice,                                                                               
		[TotalPieces],OIM.[SRP],OIM.[GP],OIM.[Tax],OIM.[NetPrice],OIM.[Barcode],OIM.[QtyShip],                                                                                                                                   
		OIM.[RemainQty],IsExchange,isFreeItem,UnitMLQty,TotalMLQty,isnull(OIM.Weight_Oz,0) as WeightOz FROM [dbo].[OrderItemMaster] AS OIM                                                                           
		INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                                                                                                                           
		INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitTypeAutoId                                                                                                                                                     
		INNER JOIN [dbo].[PackingDetails] AS PD ON PD.[ProductAutoId] = OIM.[ProductAutoId] AND PD.[UnitType] = OIM.UnitTypeAutoId                                                                                                  
		WHERE [OrderAutoId]=@OrderAutoId                                                              
	
	select  distinct pm.AutoId,ProductName,UnitTypeAutoId from OrderItemMaster as oim                                                                                                                                                    
	inner join ProductMaster as pm on pm.AutoId=oim.ProductAutoId                                                                                                                                           
	left join ItemBarcode as ib on ib.ProductAutoId=oim.ProductAutoId                                                                                        
	and ib.UnitAutoId=oim.UnitTypeAutoId                                                                                                                                                    
	where OrderAutoId=@OrderAutoId              
	
	SET @OrderStatus=(SELECT Status FROM OrderMaster WHERE AutoId=@OrderAutoId)  
	
	SELECT TM.AutoId,TM.TaxableType,TM.Value,PrintLabel FROM TaxTypeMaster  as TM
	inner join [dbo].BillingAddress as BA on BA.State=TM.State
	inner join [dbo].[CustomerMaster] as cm on cm.DefaultBillAdd=BA.AutoId and cm.AutoId=BA.CustomerAutoId
	WHERE tm.status=1 and BA.CustomerAutoId=@CustomerAutoId 

	Declare  @CState int=(Select State from BillingAddress Where Autoid In (Select DefaultBillAdd from CustomerMaster
	where AutoId=@CustomerAutoId))

	Select (select Value from Tax_Weigth_OZ) as WeigthTaxRate,ISNULL((Select TaxRate from MLTaxMaster WHERE TaxState=@CState),0) as MLTaxRate,
	CASE WHEN (SELECT TaxRate FROM MLTaxMaster WHERE TaxState=@CState) IS NULL THEN 0 ELSE 1 END                                              
    AS MLTaxType                                     

END                                                                                                                              
            
                                                                                                                                         
  Else If @Opcode=427                                                        
   Begin                                            
		SELECT top 1 PM.[Stock],Barcode FROM [dbo].[ItemBarcode] AS IB                                                           
		INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = IB.[ProductAutoId] WHERE ProductAutoId=@ProductAutoId                                                                                                               
		and IB.UnitAutoId=@UnitAutoId and Barcode!=''                                             
   End                                                                                                                                                                
                                                                                                                                                
 ELSE IF @Opcode=423                                                                                                                                                    
    BEGIN                                                                
                                                                                                                 
	SET @ProductAutoId=(SELECT [ProductAutoId] FROM [dbo].[ItemBarcode] WHERE [Barcode]=@Barcode)                                                                          
	SET @UnitAutoId=(SELECT UnitAutoId FROM [dbo].[ItemBarcode] WHERE [Barcode]=@Barcode)                
    
	
	IF NOT EXISTS(SELECT * FROM [dbo].[ItemBarcode] WHERE [Barcode]=@Barcode)                                                                                                                                                    
	BEGIN 
	Set @isException=1                                                                                                    
	Set @exceptionMessage='Invalid Barcode'   
	END
	ELSE IF NOT EXISTS(SELECT * FROM [dbo].[ProductMaster] WHERE AutoId=@ProductAutoId and ProductStatus=1)                                                                                                                                                    
	BEGIN 
	Set @isException=1                                                                                                    
	Set @exceptionMessage='Inactive Product'   
	END

	ELSE IF EXISTS(SELECT * FROM [dbo].[ItemBarcode] as ib 
	inner join ProductMaster as pm on pm.AutoId=ib.ProductAutoId and ProductStatus=1
	inner join PackingDetails as PD on PD.ProductAutoId=pm.AutoId and PD.UnitType=ib.UnitAutoId
	WHERE [Barcode]=@Barcode and( PD.EligibleforFree=@IsFreeItem or @IsFreeItem=0))         	 
	BEGIN                                                                                                                                                                                                                                                                                       
		DECLARE @custType1 int=(SELECT CustomerType FROM CustomerMaster WHERE AutoId=@CustomerAutoId)                                                                     
		SELECT PM.AutoId as ProductAutoId,PM.ProductId,PM.ProductName,PD.Qty as UnitQty,PD.UnitType,                                                        
		(CASE                                                                     
		WHEN @custType1=2 THEN ISNULL(WHminPrice,0)                                                                                                                                                     
		WHEN @custType1=3  THEN ISNULL(CostPrice,0)                                           
		ELSE [MinPrice] END) AS [MinPrice],                                                                                                                                                    
		ISNULL((SELECT TOP 1 UnitType +  ' ('+ CONVERT(VARCHAR(20),PD.QTY)+' pcs)' from UnitMaster AS UM WHERE UM.AutoId=PD.UnitType),'') AS UnitName,                                                                                                          
  
                                          
		[CostPrice],                                                                                                                                                    
		(case                                                                                                   
		when @IsExchange=1 or @IsFreeItem=1 then 0.00                                                                                                                                            
		WHEN  @custType1=3 then CONVERT(DECIMAL(10,2),ISNULL(CostPrice,0))                                                                                                                                                
		else CONVERT(DECIMAL(10,2),[Price]) end) as  [Price],                                                     
		CONVERT(DECIMAL(10,2),((PM.[P_SRP]-([Price]/(CASE WHEN [Qty]=0 then 1 else [Qty] end )))/(CASE WHEN ISNULL(PM.[P_SRP],0)=0 then 1 else PM.[P_SRP] END)) * 100) AS GP,                                                   
		isnull((select Top 1 [CustomPrice] from [dbo].[ProductPricingInPriceLevel] AS PPL where                                    
		PPL.[ProductAutoId] = PD.[ProductAutoId] AND PPL.[UnitAutoId] = PD.[UnitType]                                                                                                                                                    
		AND [PriceLevelAutoId] = (SELECT [PriceLevelAutoId] FROM [dbo].[CustomerPriceLevel]                                                       
		WHERE [CustomerAutoId]=@CustomerAutoId)                                     
		Order by [CustomPrice] desc),null) as [CustomPrice],PM.[Stock]/(CASE WHEN Qty=0 THEN 1 ELSE QTY END)  as Stock,PM.[TaxRate],                                                                                                 
		PM.[P_SRP] AS [SRP],                                                                                                                    
		(case when @IsExchange=1 or @IsFreeItem=1 then 0.00 else PM.MLQty end) as MLQty,(case when @IsExchange=1 or @IsFreeItem=1 then 0.00 else isnull(WeightOz,0) end) as WeightOz                                                            
                     
		into #result423 FROM [dbo].[PackingDetails] As PD                                                                                                                                                     
		INNER JOIN [dbo].[ProductMaster] AS PM ON PD.[ProductAutoId]=PM.[AutoId]                                                                                                                                                    
		WHERE PD.[ProductAutoId] = @ProductAutoId and PD.[UnitType] = @UnitAutoId                                                                                           
                                                                                                                                                          
		SET @QtyPerUnit=(SELECT TOP 1 UnitQty FROM #result423)                                                                                                 
		SET @MLQty=(SELECT TOP 1 MLQty FROM #result423)                                                                                                                                                    
		SET @UnitPrice=(SELECT TOP 1 Price FROM #result423)                        
		SET @minprice=(SELECT TOP 1 MinPrice FROM #result423)                                                                                                                 
		SET @SRP=(SELECT TOP 1 SRP FROM #result423)                                                                                                                                  
		SET @GP=(SELECT TOP 1 GP FROM #result423)                                                                                 
		declare @Customrice decimal (10,2)=((SELECT TOP 1 [CustomPrice] FROM #result423))                                      
		set @UnitPrice = (                                                                                                                           
		case                                                                                                                                                  
		when @IsExchange=1 or @IsFreeItem=1 then @UnitPrice                                                              
		when @Customrice IS NULL then @UnitPrice                                                                                                   
		else ( case when @Customrice < @minprice then  @UnitPrice else  @Customrice end)                                                                                                                                                  
		end                                                                         
		)               
		IF ISNULL(@DraftAutoId,0)=0                                                                                                                                      
		BEGIN                                                                                                                                                    
		INSERT INTO DraftOrderMaster([EmpAutoId],[CustomerAutoId],[OrderDate],[TotalItem],[ShippingType],[DeliveryDate],OrderType)                                                                          
		VALUES(@EmpAutoId,@CustomerAutoId,GETDATE(),1,@ShippingType,@DeliveryDate,2)                                                                             
		SET @DraftAutoId=SCOPE_IDENTITY()                                                                               
		INSERT INTO DraftItemMaster(DraftAutoId,ProductAutoId,UnitAutoId,ReqQty,QtyPerUnit,UnitPrice,minprice,SRP,GP,TaxRate,IsExchange,IsFreeItem,UnitMLQty,TotalMLQty)                                                                          
		VALUES(@DraftAutoId,@ProductAutoId,@UnitAutoId,@ReqQty,@QtyPerUnit,@UnitPrice,@minprice,@SRP,@GP,                                                              
		@IsTaxable,@IsExchange,@IsFreeItem,@MLQty,(@MLQty*@ReqQty))                                                                                                              
		END                                                                                                                                                     
		ELSE                                                                                                                                                    
		BEGIN                                                                                                        
		IF NOT EXISTS(SELECT * FROM DraftItemMaster WHERE DraftAutoId= @DraftAutoId AND ProductAutoId=@ProductAutoId AND                                                                                                      
		UnitAutoId=@UnitAutoId AND IsExchange=@IsExchange AND IsFreeItem=@IsFreeItem)                                                                 
		BEGIN                                                                                     
		INSERT INTO DraftItemMaster(DraftAutoId,ProductAutoId,UnitAutoId,ReqQty,QtyPerUnit,UnitPrice,minprice,SRP,GP,TaxRate,IsExchange,IsFreeItem,UnitMLQty,TotalMLQty)                                                        
			VALUES(@DraftAutoId,@ProductAutoId,@UnitAutoId,@ReqQty,@QtyPerUnit,@UnitPrice,@minprice,@SRP,@GP,@IsTaxable,@IsExchange,@IsFreeItem,@MLQty,(@MLQty*@ReqQty))                                                                   
			UPDATE DraftOrderMaster SET [TotalItem]=[TotalItem]+1 WHERE DraftAutoId=@DraftAutoId                                                                                                                      
	END                                                                                                               
	else                                                                                                                                
	BEGIN                                                                                
		UPDATE DraftItemMaster SET ReqQty=ReqQty+@ReqQty,TotalMLQty=isnull(UnitMLQty,0)* (ReqQty+@ReqQty) WHERE  DraftAutoId= @DraftAutoId AND                                            
		ProductAutoId=@ProductAutoId AND UnitAutoId=@UnitAutoId AND IsExchange=@IsExchange AND IsFreeItem=@IsFreeItem                                                                         
	END                                                                                                                             
	END                                                                                                                                                    
		SELECT TOP 1  @DraftAutoId AS DraftAutoId,* FROM #result423                                                                                                                 
	END 
	ELSE
	BEGIN
		Set @isException=1                                                                                                                           
		Set @exceptionMessage='Free not allow'
	END
    END                                                                                                                                                    
ELSE IF @Opcode=108                                                                                                                                          
BEGIN                                                     
	SET @State=(SELECT TOP 1 State FROM BillingAddress WHERE CustomerAutoId =@CustomerAutoId)  
	IF EXISTS(SELECT * FROM MLTaxMaster WHERE TaxsTATE=@State)                                                                                                  
	BEGIN                                                                                         
	SET @MLQty=ISNULL((SELECT MLQty FROM ProductMaster  WHERE AutoId=@ProductAutoId),0)                                                                                                                                                    
	END                                                                                                                  
	ELSE                                                                                                                                              
	BEGIN                                                                                                                                                    
	SET @MLQty=0                                                                                                                      
	END                                                                                                                     
	IF ISNULL(@DraftAutoId,0)=0                                                                                                                                                    
	BEGIN                                              
	INSERT INTO DraftOrderMaster([EmpAutoId],[CustomerAutoId],[OrderDate],[TotalItem],[ShippingType],[DeliveryDate],OrderType,Remarks) 
	VALUES(@EmpAutoId,@CustomerAutoId,GETDATE(),1,@ShippingType,@DeliveryDate,2,@Remarks)  
	SET @DraftAutoId=SCOPE_IDENTITY()                                 
	IF @IsExchange=1 OR @IsFreeItem=1                              
	BEGIN                                                                                                          
	SET @UnitPrice=0                                                                                                                                                    
	SET @MLQty=0                                                                                                                         
	END                                        
	INSERT INTO DraftItemMaster(DraftAutoId,ProductAutoId,UnitAutoId,ReqQty,QtyPerUnit,UnitPrice,minprice,SRP,GP,TaxRate,IsExchange,isFreeItem,UnitMLQty,TotalMLQty,IsTaxable)                                                                       
	VALUES(@DraftAutoId,@ProductAutoId,@UnitAutoId,@ReqQty,@QtyPerUnit,@UnitPrice,@minprice,@SRP,@GP,@IsTaxable,@IsExchange,@IsFreeItem,@MLQty,@MLQty*@ReqQty,@IsTaxable)  
	END                                                                           
	ELSE                                           
	BEGIN  
	IF NOT EXISTS(SELECT * FROM DraftItemMaster WHERE DraftAutoId= @DraftAutoId                             
	AND ProductAutoId=@ProductAutoId AND UnitAutoId=@UnitAutoId and IsExchange=@IsExchange  and IsFreeItem=@IsFreeItem )                                                                                              
	BEGIN                                                                                                                                                    
	IF @IsExchange=1 OR @IsFreeItem=1                                                                                                                                                    
	BEGIN                                                                                 
		SET @UnitPrice=0                                                                                                                                                    
		SET @MLQty=0                                                                                                                                                          
	END                                                                        
		INSERT INTO DraftItemMaster(DraftAutoId,ProductAutoId,UnitAutoId,ReqQty,QtyPerUnit,UnitPrice,minprice,SRP,GP,TaxRate,IsExchange,isFreeItem,UnitMLQty,TotalMLQty,IsTaxable)                                                                       
		VALUES(@DraftAutoId,@ProductAutoId,@UnitAutoId,@ReqQty,@QtyPerUnit,@UnitPrice,@minprice,@SRP,@GP,@IsTaxable,@IsExchange,@IsFreeItem,@MLQty,@MLQty*@ReqQty,@IsTaxable) 
		UPDATE DraftOrderMaster SET [TotalItem]=[TotalItem]+1,Remarks=@Remarks WHERE DraftAutoId=@DraftAutoId                                                                                                           
	END                                                                                                  
	ELSE                                
		BEGIN                                                 
			UPDATE dt SET ReqQty=ReqQty+@ReqQty,dt.UnitMLQty=(case when @IsFreeItem=1 or @IsExchange=1 then 0 else  pm.MLQty end),                                                                                                                 
			dt.TotalMLQty=(case when @IsFreeItem=1 or @IsExchange=1 then 0 else  ISNULL(pm.MLQty,0)*@ReqQty end) from DraftItemMaster as dt                                                                        
			inner join ProductMaster as pm on pm.AutoId=dt.ProductAutoId  WHERE  DraftAutoId= @DraftAutoId                                                                     
			AND ProductAutoId=@ProductAutoId AND UnitAutoId=@UnitAutoId and IsExchange=@IsExchange                                                        
			and IsFreeItem=@IsFreeItem                                                                                         
		END                                  
	END 
END  
	
ELSE IF @Opcode=206                                                                                     
BEGIN  
	UPDATE dt SET  ReqQty=@ReqQty,UnitPrice=@UnitPrice,                                                                                                                   
	dt.UnitMLQty=(case when @IsFreeItem=1 or @IsExchange=1 then 0 else  pm.MLQty end),
	dt.TotalMLQty=(case when @IsFreeItem=1 or @IsExchange=1 then 0 else  ISNULL(pm.MLQty,0)*@ReqQty end)                                                                   
	from DraftItemMaster as dt  inner join ProductMaster as pm on pm.AutoId=dt.ProductAutoId                                                     
	WHERE  DraftAutoId= @DraftAutoId AND ProductAutoId=@ProductAutoId AND UnitAutoId=@UnitAutoId                                                                                                                                 
	and IsExchange=@IsExchange and IsFreeItem=@IsFreeItem                                                                                                                                                     
END 
	
ELSE IF @Opcode=425                                                                                                                          
BEGIN      
	Declare @StateID INT
	SET @CustomerAutoId=(SELECT CustomerAutoId  FROM DraftOrderMaster WHERE DraftAutoId=@DraftAutoId)
	SET @StateID=(Select State from BillingAddress Where Autoid In (Select DefaultBillAdd from CustomerMaster where AutoId=@CustomerAutoId))

	SELECT ShippingType,CustomerAutoId,convert(varchar,OrderDate,101) as OrderDate,(select StatusType from StatusMaster where Category='DraftOrder' and AutoId=1) as 
	Status,convert(varchar,DeliveryDate,101) as DeliveryDate,Remarks,(select Value from Tax_Weigth_OZ) as Tax_Weigth_OZ,
	ISNULL((Select TaxRate from MLTaxMaster WHERE TaxState=@StateID),0) as MLTaxRate,OverallDisc,OverallDiscAmt,ShippingCharges,Remarks FROM DraftOrderMaster WHERE DraftAutoId=@DraftAutoId 		

	declare @custType425 int=(select cm.CustomerType from CustomerMaster as cm where AutoId=@CustomerAutoId)   
		
	SELECT  temp1.ItemAutoId,temp1.DraftAutoId ,temp1.ProductAutoId ,temp1.UnitAutoId ,temp1.ReqQty ,temp1.QtyPerUnit,temp1.UnitPrice ,              
	(              
	CASE                                                                 
	WHEN @custType425=2 THEN ISNULL(WHminPrice,0)                                                                                                                                                 
	WHEN @custType425=3  THEN ISNULL(CostPrice,0)                                                                                                  
	ELSE pd.MinPrice END              
	) AS [minprice]  ,              
	temp1.SRP ,temp1.GP ,temp1.TaxRate ,temp1.NetPrice ,temp1.IsExchange,              
	temp1.isFreeItem,temp1.IsTaxable ,temp1.UnitMLQty ,temp1.TotalMLQty,PM.ProductId,PM.ProductName,um.UnitType,isnull(PM.WeightOz,0) as WeightOz  FROM DraftItemMaster as temp1                                                                      
	INNER JOIN ProductMaster AS PM ON PM.AutoId=temp1.ProductAutoId                                                   
	INNER JOIN PackingDetails AS pd ON pd.ProductAutoId=temp1.ProductAutoId  and pd.UnitType=temp1.UnitAutoId                                                  
	INNER JOIN UnitMaster AS UM ON UM.AutoId=temp1.UnitAutoId WHERE DraftAutoId=@DraftAutoId                                                                                                
END                                                                                                                                                                                                                                                         
                                                          
Else If @Opcode=212                                                                                                                                                    
BEGIN                                                       
	Delete from DraftItemMaster WHERE DraftAutoId=@DraftAutoId and ProductAutoId=@ProductAutoId and UnitAutoId=@UnitAutoId                                              
	and isFreeItem=@IsFreeItem and IsExchange=@IsExchange                                                                                                         
END  

ELSE IF @Opcode=431                                                                 
BEGIN                                                                                                 
	SELECT [SecurityValue] FROM [dbo].[TBL_MST_SecurityMaster] WHERE [SecurityValue]=@CheckSecurity                                                                                                                 
END 

ELSE IF @Opcode=502 
BEGIN
	IF EXISTS(SELECT [AutoId] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId AND Status=0)
	BEGIN                                                                                                 
	UPDATE OrderMaster set Status=1 WHERE [AutoId]=@OrderAutoId                                                                                                           
	END
END 

ELSE IF @Opcode = 213                                                        
begin    
	set @ProductAutoId=(select ProductAutoId from OrderItemMaster where AutoId=@DraftAutoId)                                              
	set @UnitAutoId=(select UnitTypeAutoId from OrderItemMaster where AutoId=@DraftAutoId)                                                    
	set @CustomerAutoId=(SELECT CustomerAutoId FROM OrderMaster WHERE AutoId=@OrderAutoId)                                        
	set @customerType=(SELECT CustomerType FROM CustomerMaster WHERE AutoId=@CustomerAutoId) 
	Declare @CustomPrice decimal(18,2)=0.00
	SELECT @MinPrice= (CASE                                                                                                                                                     
	WHEN @custType=2 THEN ISNULL(WHminPrice,0)                                          
	WHEN @custType=3  THEN ISNULL(CostPrice,0)                                                                                                                   
	ELSE [MinPrice] END), 
	@UnitPrice=(Case when @customerType=3 then CostPrice else Price end),
	@CustomPrice=isnull((select Top 1 [CustomPrice] from [dbo].[ProductPricingInPriceLevel] AS PPL where PPL.[ProductAutoId] = PD.[ProductAutoId] AND PPL.[UnitAutoId] = PD.[UnitType]                                                                                                      
	AND [PriceLevelAutoId] = (SELECT [PriceLevelAutoId] FROM [dbo].[CustomerPriceLevel] WHERE [CustomerAutoId]=@CustomerAutoId)                                                                                      
	Order by [CustomPrice] desc),null)
	FROM [dbo].[PackingDetails] As PD                        
	INNER JOIN [dbo].[ProductMaster] AS PM ON PD.[ProductAutoId]=PM.[AutoId]                                                                                                                                  
	WHERE PD.[ProductAutoId] = @ProductAutoId and PD.[UnitType] = @UnitAutoId    
	  
	Update OrderItemMaster SET IsExchange=@IsExchange,isFreeItem=@IsFreeItem,Tax=@IsTaxable,
	UnitPrice=
	case when @IsExchange=0  and @IsFreeItem=0 then 
	(case when  @CustomPrice is null then @UnitPrice  
	when @MinPrice > @CustomPrice then @UnitPrice else @CustomPrice end
	)
	else 0.00 end
	where AutoId=@DraftAutoId

	Select @UnitPrice as Price,@CustomPrice as CustomPrice,@minprice as MinPrice
end                                                        
  
	
END TRY                                                                      
BEGIN CATCH                               
Set @isException=1                                          
Set @exceptionMessage='Oops! Something went wrong.Please try later.'                                                                                                                                                    
END CATCH                                                                                       
END 