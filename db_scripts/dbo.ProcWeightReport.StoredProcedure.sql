alter Procedure [dbo].[ProcWeightReport]    
@OpCode int=Null,            
@BrandName VARCHAR(200)=NULL,          
@ProductAutoId int = NULL,    
@StateAutoId int = NULL,    
@FromDate datetime=Null,          
@ToDate datetime=Null,  
@Description varchar(250)=NULL,          
@Status int=null,          
@PageIndex INT=1,          
@PageSize INT=10,          
@RecordCount INT=null,          
@isException bit out,          
@exceptionMessage varchar(max) out    
as     
BEGIN          
 BEGIN TRY          
   SET @isException=0          
   SET @exceptionMessage='success'        
   if @OpCode = 41    
   begin    
   select ROW_NUMBER() over(order by InvoiceDate,InvoiceNumber) as RowNumber,    
   * into #Result from (    
            Select FORMAT(OM.OrderDate,'MM/dd/yyyy HH:mm tt') as InvoiceDate,OM.OrderNo as InvoiceNumber,BM.BrandName,SA.Address,SA.City,s.StateName,    
   SA.Zipcode,CM.OPTLicence,CategoryName  as ProductDescription,cast(sum(OIM.Del_Weight_Oz_TotalQty) as decimal(18,2)) as TotalOzSold,    
   cast(sum(oim.Del_Weight_Oz_TotalQty*OM.Weigth_OZTax) as decimal(18,2) )  as TotalOzTax,CM.CustomerName  --Column customer name added by Rizwan Ahmad on 11/08/2019 01:28 AM  
   from Delivered_Order_Items OIM    
   Inner join OrderMaster as OM on    
   OIM.OrderAutoId=OM.AutoId    
   Inner JOIN ProductMaster as PM on    
   OIM.ProductAutoId=PM.AutoId    
   Inner Join CategoryMaster as cmm on cmm.AutoId=PM.CategoryAutoId    
   Inner Join BrandMaster as BM on    
   BM.AutoId=PM.BrandAutoId    
   Inner Join ShippingAddress as SA on    
   OM.ShipAddrAutoId=SA.AutoId    
   Inner JOIn CustomerMaster as CM on    
   OM.CustomerAutoId=CM.AutoId    
   INNer join State as s on    
   SA.State=s.AutoId    
           WHERE  Del_Weight_Oz_TotalQty>0 and OM.Weigth_OZTax>0    
     and ((@BrandName='0') or (BM.AutoId in (select * from dbo.fnSplitString(@BrandName,','))))         
     and (isnull(@ProductAutoId,0) = '0' or @ProductAutoId = '' or PM.AutoId = @ProductAutoId)    
     and (isnull(@StateAutoId,0) = '0' or @StateAutoId = '' or s.AutoId = @StateAutoId)    
     and (@FromDate is null or @FromDate='' or @ToDate is null or @ToDate='' or (Convert(date, OM.OrderDate) between @FromDate and @Todate))    
     group by OM.OrderDate,OM.OrderNo,SA.Address,SA.City,s.StateName,    
     SA.Zipcode,CM.OPTLicence,BM.BrandName,cmm.CategoryName,CM.CustomerName    
  ) as t     
  SELECT * FROM #Result      
   WHERE @PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)       
   SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize=0 then COUNT(RowNumber) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Result    
      
   Select isnull(sum(TotalOzSold),0.00) as TotalOzSolds,isnull(sum(TotalOzTax),0.00) as TotalOzTaxs from #Result  --Column Calculate Total added by Rizwan Ahmad on 11/08/2019 02:27 AM
   end    
    
  else if @OpCode = 42    
   begin    
      select ROW_NUMBER() over(order by InvoiceDate,InvoiceNumber) as RowNumber,    
   * into #Result1 from (     
   Select FORMAT(OM.OrderDate,'MM/dd/yyyy HH:mm tt') as InvoiceDate,OM.OrderNo as InvoiceNumber,BM.BrandName,SA.Address,SA.City,s.StateName,    
   SA.Zipcode,CM.OPTLicence,CategoryName  as ProductDescription,cast(sum(OIM.Del_Weight_Oz_TotalQty) as decimal(12,2)) as TotalOzSold,    
   cast(sum(oim.Del_Weight_Oz_TotalQty*OM.Weigth_OZTax) as decimal(12,2))  as TotalOzTax,CM.CustomerName   --Column customer name added by Rizwan Ahmad on 11/08/2019 01:28 AM   
   from Delivered_Order_Items OIM    
   Inner join OrderMaster as OM on    
   OIM.OrderAutoId=OM.AutoId    
   Inner JOIN ProductMaster as PM on    
   OIM.ProductAutoId=PM.AutoId    
   Inner Join CategoryMaster as cmm on cmm.AutoId=PM.CategoryAutoId    
   Inner Join BrandMaster as BM on    
   BM.AutoId=PM.BrandAutoId    
   Inner Join ShippingAddress as SA on    
   OM.ShipAddrAutoId=SA.AutoId    
   Inner JOIn CustomerMaster as CM on    
   OM.CustomerAutoId=CM.AutoId    
   INNer join State as s on    
   SA.State=s.AutoId    
           WHERE  Del_Weight_Oz_TotalQty>0 and OM.Weigth_OZTax>0    
     and ((@BrandName='0') or (BM.AutoId in (select * from dbo.fnSplitString(@BrandName,','))))         
     and (isnull(@ProductAutoId,0) = '0' or @ProductAutoId = '' or PM.AutoId = @ProductAutoId)    
     and (isnull(@StateAutoId,0) = '0' or @StateAutoId = '' or s.AutoId = @StateAutoId)    
     and (@FromDate is null or @FromDate='' or @ToDate is null or @ToDate='' or (Convert(date, OM.OrderDate) between @FromDate and @Todate))    
     group by OM.OrderDate,OM.OrderNo,SA.Address,SA.City,s.StateName,    
     SA.Zipcode,CM.OPTLicence,BM.BrandName,cmm.CategoryName,CM.CustomerName  
	 	   ) as t   
	 Select * from #Result1
     Select sum(TotalOzSold) as PrintTotalOzSolds,sum(TotalOzTax) as PrintTotalOzTaxs from #Result1 --Column Calculate Total added by Rizwan Ahmad on 11/08/2019 02:28 27
     Select FORMAT(GETDATE(),'MM/dd/yyyy hh:mm tt') as [PrintTime]
   end    
   else if @OpCode = 43    
   begin  
   select 
   (
    select AutoId as BID,BrandName AS BN from BrandMaster where Status = 1 order by BrandName ASC 
	for json path
	) as Brand,
	(
    select AutoId as PID,convert(varchar(20),ProductId)+' '+ProductName as PN from ProductMaster where ProductStatus = 1 order by PN ASC    
	for json path
	)as Product,
	(
    select AutoId SID,StateName as SN from State where Status = 1  
	for json path
	) as Status
	for json path
   end    
   else if @OpCode = 44    
   begin    
    select AutoId as PCID,ProductName as PCN from ProductMaster where ProductStatus = 1    
    and ((@BrandName='0') or (BrandAutoId in (select * from dbo.fnSplitString(@BrandName,','))))   
	for json path
   end    
    END TRY          
   BEGIN CATCH              
  SET @isException=1          
  SET @exceptionMessage='Oops! Something went wrong.Please try later.'    
 END CATCH          
END     
GO
