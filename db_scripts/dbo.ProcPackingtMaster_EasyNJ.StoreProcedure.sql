

ALTER PROCEDURE [dbo].[ProcPackingtMaster_EasyNJ]                 
@ProductAutoId int=NULL,
@EmpAutoId int=null
AS                
BEGIN             
  declare @ProductId int,@AutoId int
  SET @ProductId=(select ProductId from[dbo].[ProductMaster] WHERE AutoId = @ProductAutoId) 
  --PSMNJ
  IF NOT EXISTS(SELECT [AutoId] FROM [psmnj.easywhm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId)                
  BEGIN                   
    EXEC [dbo].[ProcProductMaster_ALL]              
    @ProductId=@ProductId              
  END 
  ELSE
  BEGIN
    SET @AutoId=(select AutoId from [psmnj.easywhm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId)                
	
	insert into [psmnj.easywhm.com].[dbo].[PackingDetailsUpdateLog](ProductAutoId,UnitType,MinPrice,CostPrice,Price,WHminPrice,UpdatedByAutoId,DateTime,Reff_Code)                                                                             
	select pd.ProductAutoId,pd.UnitType,pd.MinPrice,pd.CostPrice,pd.Price,pd.WHminPrice,@EmpAutoId,GETDATE(),'PSMNJ' from 
	[psmnj.easywhm.com].[dbo].PackingDetails AS PD                 
	INNER JOIN [dbo].PackingDetails AS NJ ON PD.UnitType=NJ.UnitType AND NJ.ProductAutoId=@ProductAutoId                
	WHERE PD.ProductAutoId=@AutoId 
	and PD.Qty!=nj.Qty	
	
	UPDATE PA SET 
	PA.[Qty]=(case when PA.UnitType=3 then 1 else NJ.Qty end ),
	PA.CostPrice=(case when PA.UnitType=3 then pa.CostPrice else ((pa.CostPrice/pa.Qty)*nj.qty) end ),    
	PA.WHminPrice=(case when PA.UnitType=3 then pa.WHminPrice else ((pa.WHminPrice/pa.Qty)*nj.qty) end ),    
	PA.MinPrice=(case when PA.UnitType=3 then pa.MinPrice else ((pa.MinPrice/pa.Qty)*nj.qty) end ),    
	PA.Price=(case when PA.UnitType=3 then pa.Price else ((pa.Price/pa.Qty)*nj.qty) end ),             
	PA.UpdateDate=GETDATE(),PA.EligibleforFree=NJ.EligibleforFree             
	FROM [psmnj.easywhm.com].[dbo].PackingDetails AS PA             
	INNER JOIN [dbo].PackingDetails AS NJ ON PA.UnitType=NJ.UnitType AND NJ.ProductAutoId=@ProductAutoId            
	WHERE PA.ProductAutoId=@AutoId 
	and pa.Qty!=nj.Qty
      
	insert into [psmnj.easywhm.com].[dbo].PackingDetails(UnitType,Qty,MinPrice,CostPrice,Price,ProductAutoId,WHminPrice,UpdateDate,EligibleforFree,CreateBy,CreateDate,ModifiedBy)              
	select  UnitType,Qty,MinPrice,WHminPrice,Price,@AutoId,WHminPrice,UpdateDate,EligibleforFree,NULL,GETDATE(),NULL               
	from PackingDetails where ProductAutoId=@ProductAutoId  AND             
	UnitType NOT IN (SELECT T.UnitType FROM  [psmnj.easywhm.com].[dbo].PackingDetails AS T WHERE T.ProductAutoId=@AutoId)
   
	update [psmnj.easywhm.com].[dbo].[ProductMaster] SET  
	PackingAutoId=(SELECT PackingAutoId from [dbo].[ProductMaster] as pm WHERE pm.ProductId = @ProductId)  
	WHERE ProductId = @ProductId 
            
	insert into [psmnj.easywhm.com].[dbo].[ItemBarcode](ProductAutoId,UnitAutoId,Barcode)              
	select  @AutoId,UnitAutoId,Barcode               
	from [dbo].ItemBarcode where ProductAutoId =@ProductAutoId     and   Barcode not in(select Barcode from [psmny.a1whm.com].[dbo].[ItemBarcode])        
  END
  --PSMPA
  IF NOT EXISTS(SELECT [AutoId] FROM [psmpa.easywhm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId)                
  BEGIN                   
    EXEC [dbo].[ProcProductMaster_ALL]              
    @ProductId=@ProductId              
  END   
  ELSE
  BEGIN
    SET @AutoId=(select AutoId from [psmpa.easywhm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId)                
	
	insert into [psmpa.easywhm.com].[dbo].[PackingDetailsUpdateLog](ProductAutoId,UnitType,MinPrice,CostPrice,Price,WHminPrice,UpdatedByAutoId,DateTime,Reff_Code)                                                                             
	select pd.ProductAutoId,pd.UnitType,pd.MinPrice,pd.CostPrice,pd.Price,pd.WHminPrice,@EmpAutoId,GETDATE(),'PSMNJ' from 
	[psmpa.easywhm.com].[dbo].PackingDetails AS PD                 
	INNER JOIN [dbo].PackingDetails AS NJ ON PD.UnitType=NJ.UnitType AND NJ.ProductAutoId=@ProductAutoId                
	WHERE PD.ProductAutoId=@AutoId 
	and PD.Qty!=nj.Qty	
	
	UPDATE PA SET 
	PA.[Qty]=(case when PA.UnitType=3 then 1 else NJ.Qty end ),
	PA.CostPrice=(case when PA.UnitType=3 then pa.CostPrice else ((pa.CostPrice/pa.Qty)*nj.qty) end ),    
	PA.WHminPrice=(case when PA.UnitType=3 then pa.WHminPrice else ((pa.WHminPrice/pa.Qty)*nj.qty) end ),    
	PA.MinPrice=(case when PA.UnitType=3 then pa.MinPrice else ((pa.MinPrice/pa.Qty)*nj.qty) end ),    
	PA.Price=(case when PA.UnitType=3 then pa.Price else ((pa.Price/pa.Qty)*nj.qty) end ),             
	PA.UpdateDate=GETDATE(),PA.EligibleforFree=NJ.EligibleforFree             
	FROM [psmpa.easywhm.com].[dbo].PackingDetails AS PA             
	INNER JOIN [dbo].PackingDetails AS NJ ON PA.UnitType=NJ.UnitType AND NJ.ProductAutoId=@ProductAutoId            
	WHERE PA.ProductAutoId=@AutoId 
	and pa.Qty!=nj.Qty
      
	insert into [psmpa.easywhm.com].[dbo].PackingDetails(UnitType,Qty,MinPrice,CostPrice,Price,ProductAutoId,WHminPrice,UpdateDate,EligibleforFree,CreateBy,CreateDate,ModifiedBy)              
	select  UnitType,Qty,MinPrice,WHminPrice,Price,@AutoId,WHminPrice,UpdateDate,EligibleforFree,NULL,GETDATE(),NULL               
	from PackingDetails where ProductAutoId=@ProductAutoId  AND             
	UnitType NOT IN (SELECT T.UnitType FROM  [psmpa.easywhm.com].[dbo].PackingDetails AS T WHERE T.ProductAutoId=@AutoId)
   
	update [psmpa.easywhm.com].[dbo].[ProductMaster] SET  
	PackingAutoId=(SELECT PackingAutoId from [dbo].[ProductMaster] as pm WHERE pm.ProductId = @ProductId)  
	WHERE ProductId = @ProductId 
            
	insert into [psmpa.easywhm.com].[dbo].[ItemBarcode](ProductAutoId,UnitAutoId,Barcode)              
	select  @AutoId,UnitAutoId,Barcode               
	from [dbo].ItemBarcode where ProductAutoId =@ProductAutoId     and   Barcode not in(select Barcode from [psmpa.easywhm.com].[dbo].[ItemBarcode])        
  END
  --PSMNPA
  IF NOT EXISTS(SELECT [AutoId] FROM [psmnpa.easywhm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId)                
  BEGIN                   
    EXEC [dbo].[ProcProductMaster_ALL]              
    @ProductId=@ProductId              
  END  
  ELSE
  BEGIN
    SET @AutoId=(select AutoId from [psmnpa.easywhm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId)                
	
	insert into [psmnpa.easywhm.com].[dbo].[PackingDetailsUpdateLog](ProductAutoId,UnitType,MinPrice,CostPrice,Price,WHminPrice,UpdatedByAutoId,DateTime,Reff_Code)                                                                             
	select pd.ProductAutoId,pd.UnitType,pd.MinPrice,pd.CostPrice,pd.Price,pd.WHminPrice,@EmpAutoId,GETDATE(),'PSMNJ' from 
	[psmnpa.easywhm.com].[dbo].PackingDetails AS PD                 
	INNER JOIN [dbo].PackingDetails AS NJ ON PD.UnitType=NJ.UnitType AND NJ.ProductAutoId=@ProductAutoId                
	WHERE PD.ProductAutoId=@AutoId 
	and PD.Qty!=nj.Qty	
	
	UPDATE PA SET 
	PA.[Qty]=(case when PA.UnitType=3 then 1 else NJ.Qty end ),
	PA.CostPrice=(case when PA.UnitType=3 then pa.CostPrice else ((pa.CostPrice/pa.Qty)*nj.qty) end ),    
	PA.WHminPrice=(case when PA.UnitType=3 then pa.WHminPrice else ((pa.WHminPrice/pa.Qty)*nj.qty) end ),    
	PA.MinPrice=(case when PA.UnitType=3 then pa.MinPrice else ((pa.MinPrice/pa.Qty)*nj.qty) end ),    
	PA.Price=(case when PA.UnitType=3 then pa.Price else ((pa.Price/pa.Qty)*nj.qty) end ),             
	PA.UpdateDate=GETDATE(),PA.EligibleforFree=NJ.EligibleforFree             
	FROM [psmnpa.easywhm.com].[dbo].PackingDetails AS PA             
	INNER JOIN [dbo].PackingDetails AS NJ ON PA.UnitType=NJ.UnitType AND NJ.ProductAutoId=@ProductAutoId            
	WHERE PA.ProductAutoId=@AutoId 
	and pa.Qty!=nj.Qty
      
	insert into [psmnpa.easywhm.com].[dbo].PackingDetails(UnitType,Qty,MinPrice,CostPrice,Price,ProductAutoId,WHminPrice,UpdateDate,EligibleforFree,CreateBy,CreateDate,ModifiedBy)              
	select  UnitType,Qty,MinPrice,WHminPrice,Price,@AutoId,WHminPrice,UpdateDate,EligibleforFree,NULL,GETDATE(),NULL               
	from PackingDetails where ProductAutoId=@ProductAutoId  AND             
	UnitType NOT IN (SELECT T.UnitType FROM  [psmnpa.easywhm.com].[dbo].PackingDetails AS T WHERE T.ProductAutoId=@AutoId)
   
	update [psmnpa.easywhm.com].[dbo].[ProductMaster] SET  
	PackingAutoId=(SELECT PackingAutoId from [dbo].[ProductMaster] as pm WHERE pm.ProductId = @ProductId)  
	WHERE ProductId = @ProductId 
            
	insert into [psmnpa.easywhm.com].[dbo].[ItemBarcode](ProductAutoId,UnitAutoId,Barcode) 
	select  @AutoId,UnitAutoId,Barcode               
	from [dbo].ItemBarcode where ProductAutoId =@ProductAutoId     and   Barcode not in(select Barcode from [psmny.a1whm.com].[dbo].[ItemBarcode])        
  
  END
  --PSMWPA
  IF NOT EXISTS(SELECT [AutoId] FROM [psmwpa.easywhm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId)                
  BEGIN                   
    EXEC [dbo].[ProcProductMaster_ALL]              
    @ProductId=@ProductId              
  END 
  ELSE
  BEGIN
    SET @AutoId=(select AutoId from [psmwpa.easywhm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId)                
	
	insert into [psmwpa.easywhm.com].[dbo].[PackingDetailsUpdateLog](ProductAutoId,UnitType,MinPrice,CostPrice,Price,WHminPrice,UpdatedByAutoId,DateTime,Reff_Code)                                                                             
	select pd.ProductAutoId,pd.UnitType,pd.MinPrice,pd.CostPrice,pd.Price,pd.WHminPrice,@EmpAutoId,GETDATE(),'PSMNJ' from 
	[psmwpa.easywhm.com].[dbo].PackingDetails AS PD                 
	INNER JOIN [dbo].PackingDetails AS NJ ON PD.UnitType=NJ.UnitType AND NJ.ProductAutoId=@ProductAutoId                
	WHERE PD.ProductAutoId=@AutoId 
	and PD.Qty!=nj.Qty	
	
	UPDATE PA SET 
	PA.[Qty]=(case when PA.UnitType=3 then 1 else NJ.Qty end ),
	PA.CostPrice=(case when PA.UnitType=3 then pa.CostPrice else ((pa.CostPrice/pa.Qty)*nj.qty) end ),    
	PA.WHminPrice=(case when PA.UnitType=3 then pa.WHminPrice else ((pa.WHminPrice/pa.Qty)*nj.qty) end ),    
	PA.MinPrice=(case when PA.UnitType=3 then pa.MinPrice else ((pa.MinPrice/pa.Qty)*nj.qty) end ),    
	PA.Price=(case when PA.UnitType=3 then pa.Price else ((pa.Price/pa.Qty)*nj.qty) end ),             
	PA.UpdateDate=GETDATE(),PA.EligibleforFree=NJ.EligibleforFree             
	FROM [psmwpa.easywhm.com].[dbo].PackingDetails AS PA             
	INNER JOIN [dbo].PackingDetails AS NJ ON PA.UnitType=NJ.UnitType AND NJ.ProductAutoId=@ProductAutoId            
	WHERE PA.ProductAutoId=@AutoId 
	and pa.Qty!=nj.Qty
      
	insert into [psmwpa.easywhm.com].[dbo].PackingDetails(UnitType,Qty,MinPrice,CostPrice,Price,ProductAutoId,WHminPrice,UpdateDate,EligibleforFree,CreateBy,CreateDate,ModifiedBy)              
	select  UnitType,Qty,MinPrice,WHminPrice,Price,@AutoId,WHminPrice,UpdateDate,EligibleforFree,NULL,GETDATE(),NULL               
	from PackingDetails where ProductAutoId=@ProductAutoId  AND             
	UnitType NOT IN (SELECT T.UnitType FROM  [psmwpa.easywhm.com].[dbo].PackingDetails AS T WHERE T.ProductAutoId=@AutoId)
   
	update [psmwpa.easywhm.com].[dbo].[ProductMaster] SET  
	PackingAutoId=(SELECT PackingAutoId from [dbo].[ProductMaster] as pm WHERE pm.ProductId = @ProductId)  
	WHERE ProductId = @ProductId 
            
	insert into [psmwpa.easywhm.com].[dbo].[ItemBarcode](ProductAutoId,UnitAutoId,Barcode) 
	select  @AutoId,UnitAutoId,Barcode               
	from [dbo].ItemBarcode where ProductAutoId =@ProductAutoId     and   Barcode not in(select Barcode from [psmny.a1whm.com].[dbo].[ItemBarcode])        
  
  END
  --PSMCT
  IF NOT EXISTS(SELECT [AutoId] FROM [psmct.easywhm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId)                
  BEGIN                   
    EXEC [dbo].[ProcProductMaster_ALL]              
    @ProductId=@ProductId              
  END
  ELSE
  BEGIN
    SET @AutoId=(select AutoId from [psmct.easywhm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId)                
	
	insert into [psmct.easywhm.com].[dbo].[PackingDetailsUpdateLog](ProductAutoId,UnitType,MinPrice,CostPrice,Price,WHminPrice,UpdatedByAutoId,DateTime,Reff_Code)                                                                             
	select pd.ProductAutoId,pd.UnitType,pd.MinPrice,pd.CostPrice,pd.Price,pd.WHminPrice,@EmpAutoId,GETDATE(),'PSMNJ' from 
	[psmct.easywhm.com].[dbo].PackingDetails AS PD                 
	INNER JOIN [dbo].PackingDetails AS NJ ON PD.UnitType=NJ.UnitType AND NJ.ProductAutoId=@ProductAutoId                
	WHERE PD.ProductAutoId=@AutoId 
	and PD.Qty!=nj.Qty	
	
	UPDATE PA SET 
	PA.[Qty]=(case when PA.UnitType=3 then 1 else NJ.Qty end ),
	PA.CostPrice=(case when PA.UnitType=3 then pa.CostPrice else ((pa.CostPrice/pa.Qty)*nj.qty) end ),    
	PA.WHminPrice=(case when PA.UnitType=3 then pa.WHminPrice else ((pa.WHminPrice/pa.Qty)*nj.qty) end ),    
	PA.MinPrice=(case when PA.UnitType=3 then pa.MinPrice else ((pa.MinPrice/pa.Qty)*nj.qty) end ),    
	PA.Price=(case when PA.UnitType=3 then pa.Price else ((pa.Price/pa.Qty)*nj.qty) end ),             
	PA.UpdateDate=GETDATE(),PA.EligibleforFree=NJ.EligibleforFree             
	FROM [psmct.easywhm.com].[dbo].PackingDetails AS PA             
	INNER JOIN [dbo].PackingDetails AS NJ ON PA.UnitType=NJ.UnitType AND NJ.ProductAutoId=@ProductAutoId            
	WHERE PA.ProductAutoId=@AutoId 
	and pa.Qty!=nj.Qty
      
	insert into [psmct.easywhm.com].[dbo].PackingDetails(UnitType,Qty,MinPrice,CostPrice,Price,ProductAutoId,WHminPrice,UpdateDate,EligibleforFree,CreateBy,CreateDate,ModifiedBy)              
	select  UnitType,Qty,MinPrice,WHminPrice,Price,@AutoId,WHminPrice,UpdateDate,EligibleforFree,NULL,GETDATE(),NULL               
	from PackingDetails where ProductAutoId=@ProductAutoId  AND             
	UnitType NOT IN (SELECT T.UnitType FROM  [psmct.easywhm.com].[dbo].PackingDetails AS T WHERE T.ProductAutoId=@AutoId)
   
	update [psmct.easywhm.com].[dbo].[ProductMaster] SET  
	PackingAutoId=(SELECT PackingAutoId from [dbo].[ProductMaster] as pm WHERE pm.ProductId = @ProductId)  
	WHERE ProductId = @ProductId 
            
	insert into [psmct.easywhm.com].[dbo].[ItemBarcode](ProductAutoId,UnitAutoId,Barcode) 
	select  @AutoId,UnitAutoId,Barcode               
	from [dbo].ItemBarcode where ProductAutoId =@ProductAutoId     and   Barcode not in(select Barcode from [psmny.a1whm.com].[dbo].[ItemBarcode])        
  
  END
  --DEMO
  IF NOT EXISTS(SELECT [AutoId] FROM [psmny.easywhm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId)                
  BEGIN                   
    EXEC [dbo].[ProcProductMaster_ALL]              
    @ProductId=@ProductId              
  END   
  ELSE
  BEGIN
    SET @AutoId=(select AutoId from [psmny.easywhm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId)                
	
	insert into [psmny.easywhm.com].[dbo].[PackingDetailsUpdateLog](ProductAutoId,UnitType,MinPrice,CostPrice,Price,WHminPrice,UpdatedByAutoId,DateTime,Reff_Code)                                                                             
	select pd.ProductAutoId,pd.UnitType,pd.MinPrice,pd.CostPrice,pd.Price,pd.WHminPrice,@EmpAutoId,GETDATE(),'PSMNJ' from 
	[psmny.easywhm.com].[dbo].PackingDetails AS PD                 
	INNER JOIN [dbo].PackingDetails AS NJ ON PD.UnitType=NJ.UnitType AND NJ.ProductAutoId=@ProductAutoId                
	WHERE PD.ProductAutoId=@AutoId 
	and PD.Qty!=nj.Qty	
	
	UPDATE PA SET 
	PA.[Qty]=(case when PA.UnitType=3 then 1 else NJ.Qty end ),
	PA.CostPrice=(case when PA.UnitType=3 then pa.CostPrice else ((pa.CostPrice/pa.Qty)*nj.qty) end ),    
	PA.WHminPrice=(case when PA.UnitType=3 then pa.WHminPrice else ((pa.WHminPrice/pa.Qty)*nj.qty) end ),    
	PA.MinPrice=(case when PA.UnitType=3 then pa.MinPrice else ((pa.MinPrice/pa.Qty)*nj.qty) end ),    
	PA.Price=(case when PA.UnitType=3 then pa.Price else ((pa.Price/pa.Qty)*nj.qty) end ),             
	PA.UpdateDate=GETDATE(),PA.EligibleforFree=NJ.EligibleforFree             
	FROM [psmny.easywhm.com].[dbo].PackingDetails AS PA             
	INNER JOIN [dbo].PackingDetails AS NJ ON PA.UnitType=NJ.UnitType AND NJ.ProductAutoId=@ProductAutoId            
	WHERE PA.ProductAutoId=@AutoId 
	and pa.Qty!=nj.Qty
      
	insert into [psmny.easywhm.com].[dbo].PackingDetails(UnitType,Qty,MinPrice,CostPrice,Price,ProductAutoId,WHminPrice,UpdateDate,EligibleforFree,CreateBy,CreateDate,ModifiedBy)              
	select  UnitType,Qty,MinPrice,WHminPrice,Price,@AutoId,WHminPrice,UpdateDate,EligibleforFree,NULL,GETDATE(),NULL               
	from PackingDetails where ProductAutoId=@ProductAutoId  AND             
	UnitType NOT IN (SELECT T.UnitType FROM  [psmny.easywhm.com].[dbo].PackingDetails AS T WHERE T.ProductAutoId=@AutoId)
   
	update [psmny.easywhm.com].[dbo].[ProductMaster] SET  
	PackingAutoId=(SELECT PackingAutoId from [dbo].[ProductMaster] as pm WHERE pm.ProductId = @ProductId)  
	WHERE ProductId = @ProductId 
            
	insert into [psmny.easywhm.com].[dbo].[ItemBarcode](ProductAutoId,UnitAutoId,Barcode) 
	select  @AutoId,UnitAutoId,Barcode               
	from [dbo].ItemBarcode where ProductAutoId =@ProductAutoId     and   Barcode not in(select Barcode from [psmny.a1whm.com].[dbo].[ItemBarcode])        
  
  END
END     
GO
