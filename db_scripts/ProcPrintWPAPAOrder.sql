CREATE OR Alter PROCEDURE [dbo].[ProcPrintWPAPAOrder]                                                                                                      
@Opcode INT=NULL,                                                                                                      
@OrderAutoId INT=NULL,                                                                                             
@LoginEmpType INT=NULL,                            
@CustomerAutoId INT=NULL,        
@CreditAutoId int=null,                                                                                           
@EmpAutoId INT=NULL,                                                                                    
@isException bit out,           
@BulkOrderAutoId varchar(max)=null,                                                                                                     
@exceptionMessage varchar(max) out                                                                                                      
AS                                                                                                      
BEGIN                                                                                                     
  BEGIN TRY                                                                                                      
    Set @isException=0                                                                                                      
    Set @exceptionMessage='Success'    
	IF @Opcode=40                        
	BEGIN
		DECLARE @Status int
		SELECT CompanyName,EmailAddress, [Address],[Website],[MobileNo],[FaxNo],TermsCondition,Logo FROM [psmpa.a1whm.com].[dbo].CompanyDetails                                                                                                                                              
		SELECT @Status=Status from OrderMaster WHERE AutoId=@OrderAutoId
        SELECT OM.[AutoId],OM.[OrderNo],CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,                                                                                                                                    
		ISNULL(CONVERT(VARCHAR(20), OM.[DeliveryDate],101),CONVERT(VARCHAR(20), OM.[DelDate],101) + ' ' +convert(varchar(10), OM.[DelDate], 108)) AS DeliveryDate,  CM.[AutoId] As CustAutoId,CM.[CustomerId]              
		,CM.[CustomerName],
		case when CM.[Contact2]!='' and CM.Contact1!=Contact2 then CM.[Contact1] + '/ ' + CM.[Contact2] else CM.[Contact1] end As Contact,
		CTy.CustomerType,CT.[TermsDesc],ST.[ShippingType],                                                                                                                                         
    
		--BA.[Address] As BillAddr,                                                                                                                                           
		--S.[StateName] AS State1,ba.State as [stateid],BA.[City] AS City1,BA.[Zipcode] As Zipcode1,SA.[Address] As ShipAddr,S1.[StateName] As State2,SA.[City] AS City2,                                              
		--SA.[Zipcode] As Zipcode2,
		
		[dbo].[ConvertFirstLetterinCapital](BA.[Address]+(case when isnull(BA.Address2,'')!='' then ', '+BA.Address2 else '' end)+', '+BA.City)+', '+UPper(S.StateCode)+' - '+BA.Zipcode As BillAddr, 
	     S.StateCode AS State1,BA.[City] AS City1,BA.[Zipcode] As Zipcode1,
	     
		 [dbo].[ConvertFirstLetterinCapital](SA.[Address]+(case when isnull(SA.Address2,'')!='' then ', '+SA.Address2 else '' end)+', '+SA.City)+', '+S1.StateCode+' - '+SA.Zipcode As ShipAddr, 
	      S1.StateCode AS State2,SA.[City] AS City2,SA.[Zipcode] As Zipcode2,

		OM.[Status] As StatusCode,SM.[StatusType] AS Status,OM.[PackedBoxes],om.[TotalAmount],om.[OverallDisc],om.[OverallDiscAmt],                                                                                        
		om.[ShippingCharges],om.[TotalTax],om.[GrandTotal],om.paidAmount,om.[AmtDue],OM.[PaymentRecev],OM.[RecevAmt],OM.[AmtValue],OM.[ValueChanged],                                                                                                                 
 
		OM.[DiffAmt],OM.[NewTotal],OM.[PayThru],OM.CheckNo,OM.[DrvRemarks],OM.[AcctRemarks],EM.[FirstName] + ' ' + EM.[LastName] As SalesPerson,                                                                          
		EM1.[FirstName] + ' ' + EM1.[LastName] As PackerName,[CommentType],[Comment],isnull(DO.CreditMemoAmount,om.DeductionAmount) as DeductionAmount,                                                                                                                                
 
		ISNULL(om.CreditAmount,om.CreditAmount)as CreditAmount,isnull(om.PayableAmount,OM.PayableAmount) as PayableAmount,                                                                           
		ISNULL((select t.CreditAmount from CustomerCreditMaster as t  where t.CustomerAutoId=om.CustomerAutoid),0.00) as CustomerCredit                                                                                                                              
		,ISNULL((select top 1 PrintLabel from MLTaxMaster as mlt where mlt.TaxState=BA.State),'') as MLTaxPrintLabel
		,om.TaxType,otm.OrderType AS OrderType,OrderRemarks,                                                                                                                                                    
		ISNULL(OM.MLQty,0) as MLQty,isnull(om.MLTax,om.MLTax) as MLTax,CM.ContactPersonName,ISNULL(om.AdjustmentAmt,0) as AdjustmentAmt, 
		TTM.PrintLabel AS TaxPrintLabel,(SELECT top 1 PrintLabel from [dbo].[Tax_Weigth_OZ]) AS WeightTaxPrintLabel,
		CM.BusinessName,ISNULL(UnitMLTax,0) as UnitMLTax,om.Weigth_OZTaxAmount as  WeightTax,OM.TaxValue,om.Status as stautoid  FROM [dbo].[OrderMaster] As OM                                                                                                                        
		INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.Status AND SM.[Category] = 'OrderMaster'                                                                                                                                              
		INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[AutoId] = OM.[BillAddrAutoId]                                                                 
		INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = OM.[ShipAddrAutoId]                           
		INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                                                                                                                                                     
		INNER JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                                                                                                                                     
		INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                                                                                                                                               
		INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]                                                                                             
		LEFT JOIN [dbo].[CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                                                                                                                    
		LEFT JOIN [dbo].[ShippingType] AS ST ON ST.[AutoId] = OM.[ShippingType]                                                                                                                        
		LEFT JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[SalesPersonAutoId]                                                                              
		LEFT JOIN [dbo].[EmployeeMaster] AS EM1 ON EM1.[AutoId] = OM.[PackerAutoId]      
		LEFT JOIN [dbo].[TaxTypeMaster] AS TTM ON TTM.[AutoId] = OM.[TaxType]  
		left JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId] 
		INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
		WHERE OM.AutoId = @OrderAutoId                                                                                                                                                 
        
		IF EXISTS(SELECT [AutoId] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId AND Status=11)                                                                               
                              
		BEGIN                                                                                                                   
		                                                                                                                              
			SELECT CM.[CategoryName],CM.[CategoryId],DOI.[ProductAutoId],PM.[ProductId],PM.[ProductName],DOI.[UnitAutoId],UM.[UnitType],DOI.[QtyPerUnit],
			DOI.[UnitPrice],DOI.[Barcode],DOI.[QtyShip],DOI.[QtyDel],DOI.[TotalPieces],DOI.[SRP],DOI.[GP],  
			case when DOI.[GP] < 0 then '---' else Convert(varchar(15),DOI.[GP]) end as PrintGP  
			,DOI.[Tax],DOI.[NetPrice]                                                                                                                                         
			,convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice,ISNULL(FreshReturnQty,0) as FreshReturnQty,FreshReturnUnitAutoId,                                                                                           
			ISNULL(DamageReturnQty,0) as DamageReturnQty,DamageReturnUnitAutoId,                                                                                         
			IsExchange,isnull(TaxValue,0.00) as TaxValue,ISNULL(isFreeItem,0) AS isFreeItem,                                                                   
			ISNULL(UnitMLQty,0) as UnitMLQty,ISNULL(TotalMLQty,0) as TotalMLQty,                                                                                                                                                    
			DOI.Item_TotalMLTax as TotalMLTax               
			FROM [dbo].[Delivered_Order_Items] AS DOI                                                                                                                                      
			INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = DOI.[ProductAutoId]                                                                                      
			INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = DOI.UnitAutoId                                    
			INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId] WHERE [OrderAutoId]=@OrderAutoId                                                              
			AND QtyShip>0                                                             
			ORDER BY CM.[CategoryName] ASC                          
		END                                                          
		ELSE                                                                                                   
		BEGIN 
			
			SELECT CM.[CategoryName],CM.[CategoryId],OIM.ProductAutoId,PM.[ProductId],PM.[ProductName],PM.[ProductLocation],                                                                 
			OIM.[UnitTypeAutoId] AS UnitAutoId,UM.[UnitType],OIM.[QtyPerUnit],OIM.[UnitPrice],OIM.[TotalPieces],OIM.[SRP],OIM.[GP],  
			case when OIM.[GP] < 0 then '---' else convert(varchar(15),OIM.[GP]) end as PrintGP  
			,OIM.[Tax],                                                 
			OIM.[NetPrice],OIM.[Barcode], OIM.[QtyShip],OIM.[RequiredQty] ,convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice,                                                                                                        
  
			OIM.IsExchange,isnull(TaxValue,0.00) as TaxValue,ISNULL(isFreeItem,0) AS isFreeItem,UnitMLQty,                                                                                                                                                    
			ISNULL(UnitMLQty,0) as UnitMLQty,ISNULL(TotalMLQty,0) as TotalMLQty,                                                                                     
			oim.Item_TotalMLTax as TotalMLTax                                                                  
			FROM [dbo].[OrderItemMaster] AS OIM                                                  
			INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                                               
			INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitTypeAutoId                                                                        
			INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId] WHERE [OrderAutoId] = @OrderAutoId                                                                                
			AND (case when @Status=1 or @Status=2 then RequiredQty  else QtyShip end) >0                                                                         
			ORDER BY CM.[CategoryName] ASC                                                                      
		END                                                                                                                                                         
			SET @CustomerAutoId = (SELECT [CustomerAutoId] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId)         
			SELECT OM.[AutoId],[OrderNo],CONVERT(VARCHAR(20),[OrderDate],101) AS OrderDate,DO.[AmtDue],DO.[GrandTotal],DO.[AmtPaid] ,                                                                                                                             
			otm.OrderType AS OrderType ,DATEDIFF(dd,OrderDate,GETDATE()) as Aging, orderdate as orderdateSort 
			FROM [dbo].[OrderMaster] As OM                                                                                                                         
			INNER JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId]     
			INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
			WHERE [CustomerAutoId] = @CustomerAutoId AND ([Status] = 6 or [Status] = 11 ) AND do.[AmtDue] > 0.00                                                                                   
			order by orderdateSort                                                                                            
			select OrderAutoId,CreditAutoId,CreditNo,CONVERT(VARCHAR(10),CreditDate,101) AS CreditDate,ISNULL(TotalAmount,0.00) as ReturnValue,ISNULL(PaidAmount,0.00)  
			as amtDeducted,        
			ISNULL(BalanceAmount,0.00) as amtDue                                                                                                            
			from CreditMemoMaster AS CM where  OrderAutoId=@OrderAutoId               
	END
  END TRY
  BEGIN CATCH
	Set @isException=0                                                                                                      
    Set @exceptionMessage=ERROR_MESSAGE()
  END CATCH
END  