USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Order_AdjustmentAmt]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

alter table OrderMaster drop column AdjustmentAmt
drop function FN_Order_AdjustmentAmt
go
 CREATE FUNCTION  [dbo].[FN_Order_AdjustmentAmt]    
(    
  @orderAutoId int    
)    
RETURNS decimal(18,2)    
AS    
BEGIN    
 DECLARE @AdjustmentAmt decimal(18,2)     
   declare @GrandTotal decimal(18,2)=(SELECT ((([TotalAmount]-[OverallDiscAmt])+[ShippingCharges])+[TotalTax]+[MLTax]+ISNULL(Weigth_OZTaxAmount,0)) FROM OrderMaster WHERE AutoId=@orderAutoId)  
  SET @AdjustmentAmt=((round(@GrandTotal,0))-(@GrandTotal))     
     
    
 RETURN @AdjustmentAmt    
END    
GO
alter table OrderMaster add [AdjustmentAmt]  AS ([dbo].[FN_Order_AdjustmentAmt]([autoid]))
