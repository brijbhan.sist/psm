Alter PROCEDURE [dbo].[ProcSalesCommission]            
@Opcode INT=NULL,            
@CustomerAutoId INT=NULL,             
@SalesPerson INT=NULL,            
@FromDate date =NULL,            
@ToDate date =NULL,            
@PageIndex INT=1,            
@PageSize INT=10,            
@RecordCount INT=null,            
@isException bit out,            
@exceptionMessage varchar(max) out            
AS            
BEGIN             
 BEGIN TRY            
   Set @isException=0            
   Set @exceptionMessage='Success'            
   IF @Opcode=41            
   BEGIN            
		SELECT AutoId as SID,FirstName+' '+ISNULL(LASTNAME,'') AS SP  FROM EmployeeMaster WHERE EmpType IN (2) AND Status=1           
		order by SP
		for json path
   END            
   ELSE IF @Opcode=42            
   BEGIN 
		SELECT ROW_NUMBER() OVER(ORDER BY  SalesPersonAutoId,sp,CommCode) AS RowNumber, SP,SalesPersonAutoId,NoofProduct,
		(SELECT C_DisplayName FROM Tbl_CommissionMaster AS CM WHERE T.CommCode=CM.CommissionCode) AS CommCode,TotalSale,SPCommAmt INTO #Results FROM            
		(            
			select SP,t1.SalesPersonAutoId,NoofProduct,t1.CommCode,        
			CAST((TotalSale-ISNULL(TotalReturn,0)) as decimal(10,2)) as TotalSale,        
			CAST((SPCommAmt-ISNULL(ReturnSPCommAmt,0)) as decimal(10,2)) as SPCommAmt 
			from ( 
				select (em.FirstName+' '+em.LastName) as SP,om.SalesPersonAutoId,COUNT(doim.OrderAutoId) AS NoofProduct ,
				isNull(doim.Del_CommCode,0) as CommCode,ISNULL(sum(doim.NetPrice),0) as TotalSale,CAST((sum(doim.NetPrice)*isNull (Del_CommCode,0)) 
				AS DECIMAL(10,2)) as SPCommAmt  from  Delivered_Order_Items as doim    
				inner join  ProductMaster as pm on pm.AutoId = doim.ProductAutoId         
				inner join OrderMaster as om on om.AutoId=doim.OrderAutoId          
				inner join EmployeeMaster as em on em.AutoId=om.SalesPersonAutoId          
				where   om.Status=11 and         
				(om.SalesPersonAutoId=@SalesPerson OR ISNULL(@SalesPerson,0)=0) and           
				(          
				@FromDate IS NULL OR @ToDate IS NULL OR convert(date,OrderDate) between convert(date,@FromDate)           
				and convert(date,@ToDate)          
				)          
				group by isNull(doim.Del_CommCode,0), em.FirstName+' '+em.LastName,om.SalesPersonAutoId        
			) as t1         
			left join        
			(        
				select om.SalesPersonAutoId,sum(cim.NetAmount) as TotalReturn,isNull(P_CommCode,0) as CommCode,        
				CAST((sum(cim.NetAmount)*isNull (P_CommCode,0)) AS DECIMAL(10,2)) as ReturnSPCommAmt  from CreditItemMaster as cim         
				inner join ProductMaster as pd on pd.AutoId=cim.ProductAutoId       
				inner join CreditMemoMaster as cmm  on  cmm.CreditAutoId=cim.CreditAutoId        
				inner join OrderMaster as om on om.AutoId=cmm.OrderAutoId                 
				where om.Status=11 and          
				(om.SalesPersonAutoId=@SalesPerson OR ISNULL(@SalesPerson,0)=0)           
				and           
				(          
				@FromDate IS NULL OR @ToDate IS NULL OR convert(date,OrderDate) between convert(date,@FromDate)           
				and convert(date,@ToDate)          
				)               
				group by om.SalesPersonAutoId,isNull(P_CommCode,0)
			) as t2 on t1.CommCode=t2.CommCode and t1.SalesPersonAutoId=t2.SalesPersonAutoId 
		) AS t ORDER BY SalesPersonAutoId,sp,CommCode   
		SELECT * FROM #Results            
		WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))               
		SELECT COUNT(CommCode) AS RecordCount, case when @PageSize=0 then COUNT(CommCode) else @PageSize end AS PageSize, @PageIndex AS PageIndex,Format(getdate(),'MM/dd/yyyy hh:mm tt') as PrintDate FROM #Results   
            
		SELECT ISNULL(SUM(SPCommAmt),0.00) AS SPCommAmt,ISNULL(SUM(TotalSale),0.00) AS TotalSale, isnull(Sum(NoofProduct),0) as NoofOrder from  #Results  
  END          
 END TRY            
 BEGIN CATCH            
		Set @isException=1            
		Set @exceptionMessage=ERROR_MESSAGE()            
 END CATCH            
END   

GO