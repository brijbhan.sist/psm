USE [psmnj.a1whm.com]
GO

/****** Object:  UserDefinedTableType [dbo].[DT_dtbulkUnitNew]    Script Date: 01/04/2020 03:17:37 ******/

Drop procedure ProcDraftProductMaster
DROP TYPE [dbo].[DT_dtbulkUnitNew]
GO

/****** Object:  UserDefinedTableType [dbo].[DT_dtbulkUnitNew]    Script Date: 01/04/2020 03:17:37 ******/
CREATE TYPE [dbo].[DT_dtbulkUnitNew] AS TABLE(
	[ProductAutoId] [int] NULL,
	[UnitAutoId] [int] NULL,
	[Qty] [int] NULL,
	[CostPrice] [decimal](8,3) NULL,
	[BasePrice] [decimal](8,3) NULL,
	[RetailPrice] [decimal](8,3) NULL,
	[WHPrice] [decimal](8,3) NULL,
	[SETDefault] [int] NULL,
	[SETFree] [int] NULL,
	[Barcode] [varchar](100) NULL,
	[UnitType] [varchar](100) NULL,
	[isFree] [varchar](100) NULL,
	[Rack] [varchar](15) NULL,
	[Section] [varchar](15) NULL,
	[Row] [varchar](15) NULL,
	[BoxNo] [varchar](15) NULL
)
GO


