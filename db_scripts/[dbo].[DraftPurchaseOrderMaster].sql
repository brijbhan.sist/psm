USE [psmnj.a1whm.com]
GO

/****** Object:  Table [dbo].[DraftPurchaseOrderMaster]    Script Date: 01/27/2020 11:58:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DraftPurchaseOrderMaster](
	[DraftAutoId] [int] IDENTITY(1,1) NOT NULL,
	[PODate] [datetime] NULL,
	[VenderAutoId] [int] NULL,
	[PORemarks] [varchar](500) NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[VendorType] [int] NOT NULL,
	[Status] [int] NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[DraftPurchaseOrderMaster] ADD  DEFAULT ((0)) FOR [VendorType]
GO


