ALTER PROCEDURE [dbo].[OrderWisePackerReport]                      
                            
@Opcode INT=NULL,          
@PackerAutoId int=NULL,          
@DateFrom date =NULL,          
@DateTo date =NULL,          
@PageIndex INT=1,          
@PageSize INT=10,          
@RecordCount INT=null,          
@isException bit out,          
@exceptionMessage varchar(max) out                     
AS                          
BEGIN           
 BEGIN TRY          
  Set @isException=0          
  Set @exceptionMessage='Success'                 
          
if @Opcode = 41          
begin         
 select  ROW_NUMBER() OVER(ORDER BY PackerAutoId,PackerAssignDate desc) AS RowNumber,PackerAutoId,
 emp.FirstName+' '+EMP.LastName AS WarehouseManager,        
 emp1.FirstName+' '+EMP1.LastName AS PackerName,CustomerName        
 ,OrderNo,FORMAT(OrderDate,'MM/dd/yyyy hh:mm tt') as OrderDate,FORMAT(PackerAssignDate,'MM/dd/yyyy hh:mm tt') as  PackerAssignDate,        
 (SELECT TOP 1 FORMAT(PackingDate,'MM/dd/yyyy hh:mm tt') FROM GenPacking AS GP WHERE GP.OrderAutoId=OM.AutoId ORDER BY PackingDate DESC) AS PackingDate,        
 (SELECT COUNT(1) FROM OrderItemMaster AS OIM WHERE OIM.OrderAutoId=OM.AutoId) as  NoofItems,        
 (SELECT COUNT(1) FROM OrderItemMaster AS OIM WHERE OIM.OrderAutoId=OM.AutoId and ISNULL(QtyShip,0)>0) as  NoofPackedItems,        
 (select count(1) from OrderItemMaster AS OIM WHERE OIM.OrderAutoId=OM.AutoId and ISNULL(ManualScan,0)=1) as ManuallyScanned into #Results41 from OrderMaster as om         
 inner join EmployeeMaster as  emp on  OM.WarehouseAutoId=EMP.AutoId        
 inner join EmployeeMaster as  emp1 on  OM.PackerAutoId=EMP1.AutoId        
 inner join CustomerMaster as  cm on  cm.AutoId=om.CustomerAutoId             
 where   
 om.Status >= 3 and PackerAssignDate is not null AND Om.Status!=8
 AND
 (@PackerAutoId is null or @PackerAutoId = 0 or om.PackerAutoId =  @PackerAutoId )           
 and (@DateFrom is null or @DateFrom = '' or @DateTo is null or @DateTo = '' or                 
 (CONVERT(date,PackerAssignDate)  between CONVERT(date,@DateFrom) and CONVERT(date,@DateTo)))        
  ORDER BY PackerAutoId,PackerAssignDate     desc        
         
 SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize=0 then COUNT(RowNumber) else @PageSize end AS PageSize, @PageIndex AS PageIndex,FORMAT(getdate(),'MM/dd/yyyy hh:mm tt') as PrintDate FROM #Results41          
              
 SELECT * FROM #Results41          
 WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))      
     
  SELECT ISNULL(SUM(NoofItems),0) AS NoofItems, isnull(Sum(NoofPackedItems),0) as NoofPackedItems, isnull(Sum(ManuallyScanned),0) as ManuallyScanned    
   from  #Results41         
end          
if @Opcode = 42          
begin          
 select em.AutoId,FirstName + ' ' + LastName as PackerName from EmployeeMaster as em          
 inner join EmployeeTypeMaster etm on etm.AutoId = em.EmpType          
 Where etm.AutoId = 3 and em.Status=1  order by PackerName ASC      
end          
                       
END TRY          
 BEGIN CATCH          
  Set @isException=1          
  Set @exceptionMessage=ERROR_MESSAGE()          
 END CATCH                     
END 
GO
