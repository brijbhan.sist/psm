USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Credit_AcceptedTotalQty]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION  [dbo].[FN_Credit_AcceptedTotalQty]
(
	 @AutoId int
)
RETURNS decimal(18,2)
AS
BEGIN
	DECLARE @AcceptedTotalQty decimal(18,2)
	SET @AcceptedTotalQty=isnull((SELECT (AcceptedQty*QtyPerUnit) FROM CreditItemMaster WHERE ItemAutoId=@AutoId),0)
	RETURN @AcceptedTotalQty
END

GO
Alter table CreditItemMaster drop column [AcceptedTotalQty]
Drop function [FN_Credit_AcceptedTotalQty]
Alter table CreditItemMaster Add [AcceptedTotalQty]  AS ([dbo].[FN_Credit_AcceptedTotalQty]([ItemAutoId]))