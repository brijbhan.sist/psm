
ALTER PROCEDURE  [dbo].[ProcPSMNY_ProductSold]
 @FromDate datetime ,
 @ToDate datetime ,
 @PageSize int,
 @PageIndex int,
 @isException bit out,
 @exceptionMessage varchar(max) out
  
AS
BEGIN
SET @isException=0
SET @exceptionMessage=''
BEGIN TRY
select *, (Net_Sold_Default_Qty * psmny_COST)psmny_NET_COST into #t1 from
(
	select 
		njpm.ProductId,njpm.ProductName,njum.UnitType,njpd.Qty
		,(case when isnull(pppl.CustomPrice,0) = 0 then njpd.CostPrice else CustomPrice end ) psmny_COST
			,convert(decimal(18,2),((t1.QtyDel)/njpd.Qty)) as [Sold_Default_Qty]		
			,convert(decimal(18,2),((isnull(t2.QtyDel,0))/njpd.Qty)) as [CM_Default_Qty]	
			,convert(decimal(18,2),((t1.QtyDel-isnull(t2.QtyDel,0))/njpd.Qty)) as [Net_Sold_Default_Qty]
			,(t1.NetPrice  ) as Net_Sale_Rev		
			,( isnull(t2.NetPrice,0)) as Net_CM_Rev		
			,(t1.NetPrice - isnull(t2.NetPrice,0)) as Net_Total_Sales	
	from 
		[psmnj.a1whm.com].dbo.ProductMaster as njpm
			inner join [psmnj.a1whm.com].dbo.PackingDetails as njpd on njpd.ProductAutoId=njpm.AutoId and njpd.UnitType = njpm.PackingAutoId
			inner join [psmnj.a1whm.com].dbo.UnitMaster as njum on njum.AutoId=njpd.UnitType
	left join
	(
			select  
					  pm.ProductId,   pm.PackingAutoId,pd.UnitType, pd.Qty			
					,sum(doi.QtyDel) as QtyDel
					,sum(doi.NetPrice) as NetPrice			
			from [psmny.a1whm.com].dbo.OrderMaster as om
				inner join [psmny.a1whm.com].dbo.Delivered_Order_Items as doi on doi.OrderAutoId=om.AutoId
				inner join [psmny.a1whm.com].dbo.ProductMaster as pm on pm.AutoId=doi.ProductAutoId
				inner join [psmny.a1whm.com].dbo.PackingDetails as pd on pd.ProductAutoId=pm.AutoId and pd.UnitType=pm.PackingAutoId			 
			where convert(date,OrderDate) between  @FromDate and @ToDate and om.Status=11 --and om.OrderType !=1
			group by  pm.ProductId , pd.UnitType, pd.Qty,  pm.PackingAutoId 
		) as t1  on njpm.ProductId=t1.ProductId and njpd.UnitType = t1.UnitType


	left join (
		select   pm.ProductId,   pm.PackingAutoId, pd.UnitType, pd.Qty,
		sum(cim.TotalPeice) as QtyDel, sum(cim.NetAmount) as NetPrice
		from  [psmny.a1whm.com].dbo.CreditMemoMaster as cmm
		inner join  [psmny.a1whm.com].dbo.CreditItemMaster as cim on cim.CreditAutoId=cmm.CreditAutoId
		inner join  [psmny.a1whm.com].dbo.ProductMaster as pm on pm.AutoId=cim.ProductAutoId
		inner join  [psmny.a1whm.com].dbo.PackingDetails as pd on pd.ProductAutoId=pm.AutoId and pd.UnitType=pm.PackingAutoId		
		where convert(date,cmm.ApprovalDate) between  @FromDate and @ToDate and cmm.Status=3
		group by  pm.ProductId,   pd.UnitType, pd.Qty , pm.PackingAutoId
	) as t2 on  njpm.ProductId=t2.ProductId and njpd.UnitType=t2.UnitType
	
	left join [psmnj.a1whm.com].dbo.ProductPricingInPriceLevel as pppl on 
																			pppl.ProductAutoId=njpm.AutoId
																		and pppl.UnitAutoId=t1.UnitType 
																		and pppl.PriceLevelAutoId=2838
																		 
) as t
 
----------------------------- 
---_EASYWHM
--------------------------------------------------
UNION ALL

select *, (Net_Sold_Default_Qty * psmny_COST)psmny_NET_COST  from
(
	select 
		njpm.ProductId,njpm.ProductName,njum.UnitType,njpd.Qty
		,(case when isnull(pppl.CustomPrice,0) = 0 then njpd.CostPrice else CustomPrice end ) psmny_COST
			,convert(decimal(18,2),((t1.QtyDel)/njpd.Qty)) as [Sold_Default_Qty]		
			,convert(decimal(18,2),((isnull(t2.QtyDel,0))/njpd.Qty)) as [CM_Default_Qty]	
			,convert(decimal(18,2),((t1.QtyDel-isnull(t2.QtyDel,0))/njpd.Qty)) as [Net_Sold_Default_Qty]
			,(t1.NetPrice  ) as Net_Sale_Rev		
			,( isnull(t2.NetPrice,0)) as Net_CM_Rev		
			,(t1.NetPrice - isnull(t2.NetPrice,0)) as Net_Total_Sales	
	from 
		[psmnj.a1whm.com].dbo.ProductMaster as njpm
			inner join [psmnj.a1whm.com].dbo.PackingDetails as njpd on njpd.ProductAutoId=njpm.AutoId and njpd.UnitType = njpm.PackingAutoId
			inner join [psmnj.a1whm.com].dbo.UnitMaster as njum on njum.AutoId=njpd.UnitType
	left join
	(
			select  
					  pm.ProductId,   pm.PackingAutoId,pd.UnitType, pd.Qty			
					,sum(doi.QtyDel) as QtyDel
					,sum(doi.NetPrice) as NetPrice			
			from  [psmny.easywhm.com].dbo.OrderMaster as om
				inner join [psmny.easywhm.com].dbo.Delivered_Order_Items as doi on doi.OrderAutoId=om.AutoId
				inner join  [psmny.easywhm.com].dbo.ProductMaster as pm on pm.AutoId=doi.ProductAutoId
				inner join  [psmny.easywhm.com].dbo.PackingDetails as pd on pd.ProductAutoId=pm.AutoId and pd.UnitType=pm.PackingAutoId			 
			where convert(date,OrderDate) between  @FromDate and @ToDate and om.Status=11 --and om.OrderType !=1
			group by  pm.ProductId , pd.UnitType, pd.Qty,  pm.PackingAutoId 
		) as t1  on njpm.ProductId=t1.ProductId and njpd.UnitType = t1.UnitType


	left join (
		select   pm.ProductId,   pm.PackingAutoId, pd.UnitType, pd.Qty,
		sum(cim.TotalPeice) as QtyDel, sum(cim.NetAmount) as NetPrice
		from   [psmny.easywhm.com].dbo.CreditMemoMaster as cmm
		inner join   [psmny.easywhm.com].dbo.CreditItemMaster as cim on cim.CreditAutoId=cmm.CreditAutoId
		inner join  [psmny.easywhm.com].dbo.ProductMaster as pm on pm.AutoId=cim.ProductAutoId
		inner join   [psmny.easywhm.com].dbo.PackingDetails as pd on pd.ProductAutoId=pm.AutoId and pd.UnitType=pm.PackingAutoId		
		where convert(date,cmm.ApprovalDate) between  @FromDate and @ToDate and cmm.Status=3
		group by  pm.ProductId,   pd.UnitType, pd.Qty , pm.PackingAutoId
	) as t2 on  njpm.ProductId=t2.ProductId and njpd.UnitType=t2.UnitType
	
	left join [psmnj.a1whm.com].dbo.ProductPricingInPriceLevel as pppl on 
																			pppl.ProductAutoId=njpm.AutoId
																		and pppl.UnitAutoId=t1.UnitType 
																		and pppl.PriceLevelAutoId=2838
																		 
) as tx
order by  ProductId

--------------------------
select ROW_NUMBER() over(order by [CategoryName]) as RowNumber,
njcm.CategoryName[CategoryName]
, tr.ProductId
 ,tr.ProductName
 ,UnitType 
,psmny_COST [psmny_UNIT_COST]	 
,sum(Sold_Default_Qty)Sold_Default_Qty
,sum(CM_Default_Qty)CM_Default_Qty
,sum(Net_Sold_Default_Qty)Net_Sold_Default_Qty

,sum(psmny_NET_COST)psmny_NET_COST
,sum(Net_Sale_Rev)Net_Sale_Rev
,sum(Net_CM_Rev)Net_CM_Rev
,sum(Net_Total_Sales)Net_Total_Sales 


,sum((Net_Total_Sales-psmny_NET_COST))[Profit]
 into #tlist from #t1 as tr
 inner join [psmnj.a1whm.com].dbo.ProductMaster as njpm on njpm.ProductId=tr.[ProductId]
 inner join [psmnj.a1whm.com].dbo.CategoryMaster as njcm on njcm.AutoId=njpm.CategoryAutoId
  
group by  tr.ProductId,tr.ProductName,UnitType,psmny_COST ,njcm.CategoryName
having sum(Net_Sold_Default_Qty) != 0
 order by njcm.CategoryName,ProductId
 
  
SELECT case when @PageSize=0 then 0 else COUNT(*) end as RecordCount,@PageSize as PageSize,@PageIndex as PageIndex FROM #tlist  
SELECT * FROM #tlist  
WHERE @PageSize=0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)
SELECT ISNULL((SUM([psmny_UNIT_COST])),0) as [psmny_UNIT_COST] ,ISNULL((SUM(Sold_Default_Qty)),0) as Sold_Default_Qty,
ISNULL((SUM(CM_Default_Qty)),0) as CM_Default_Qty,ISNULL((SUM(Net_Sold_Default_Qty)),0) as Net_Sold_Default_Qty,ISNULL((SUM(psmny_NET_COST)),0) as psmny_NET_COST,
ISNULL(SUM(Net_Sale_Rev),0) as Net_Sale_Rev,ISNULL(SUM(Net_CM_Rev),0) as Net_CM_Rev,ISNULL(SUM(Net_Total_Sales),0) as Net_Total_Sales,ISNULL(SUM([Profit]),0) as [Profit]
from #tlist
 drop table #t1,#tlist
END TRY
BEGIN CATCH

SET @isException=1
SET @exceptionMessage='false'
END CATCH
END



