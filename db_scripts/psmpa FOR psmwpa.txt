delete FROM DraftPurchaseOrderMaster
delete FROM DraftPurchaseProductsMaster
delete FROM Draft_StockEntry
delete FROM Draft_StockItemMaster

--delete FROM VendorLog
--delete FROM PurchaseOrderMaster
--delete FROM PurchaseProductsMaster
--delete FROM BillItems
--delete FROM StockEntry
--delete  FROM VendorMaster

go


--select * from EmployeeMaster where EmpType=2
--select * from CustomerMaster where SalesPersonAutoId=1208

DELETE FROM tbl_OrderLog where OrderAutoId in (
select om.autoid from OrderMaster as om where CustomerAutoId not in (
select cm.AutoId from CustomerMaster as cm where SalesPersonAutoId=1208)
)

DELETE FROM OrderItemMaster where OrderAutoId in (
select om.autoid from OrderMaster as om where CustomerAutoId not in (
select cm.AutoId from CustomerMaster as cm where SalesPersonAutoId=1208)
)


DELETE FROM GenPacking where OrderAutoId in (
select om.autoid from OrderMaster as om where CustomerAutoId not in (
select cm.AutoId from CustomerMaster as cm where SalesPersonAutoId=1208)
)

DELETE FROM DrvLog where OrderAutoId in (
select om.autoid from OrderMaster as om where CustomerAutoId not in (
select cm.AutoId from CustomerMaster as cm where SalesPersonAutoId=1208)
)

DELETE FROM OrderItems_Original where OrderAutoId in (
select om.autoid from OrderMaster as om where CustomerAutoId not in (
select cm.AutoId from CustomerMaster as cm where SalesPersonAutoId=1208)
)


DELETE FROM Order_Original where AutoId in (
select om.autoid from OrderMaster as om where CustomerAutoId not in (
select cm.AutoId from CustomerMaster as cm where SalesPersonAutoId=1208)
)


DELETE FROM PaymentOrderDetails where OrderAutoId in (
select om.autoid from OrderMaster as om where CustomerAutoId not in (
select cm.AutoId from CustomerMaster as cm where SalesPersonAutoId=1208)
)


DELETE FROM Delivered_Order_Items where OrderAutoId in (
select om.autoid from OrderMaster as om where CustomerAutoId not in (
select cm.AutoId from CustomerMaster as cm where SalesPersonAutoId=1208)
)


DELETE FROM DeliveredOrders where OrderAutoId in (
select om.autoid from OrderMaster as om where CustomerAutoId not in (
select cm.AutoId from CustomerMaster as cm where SalesPersonAutoId=1208)
)
 
DELETE FROM OrderMaster  where CustomerAutoId not in (
select cm.AutoId from CustomerMaster as cm where SalesPersonAutoId=1208)
 
 DELETE FROM PaymentCurrencyDetails where PaymentAutoId
 in (
 
 select cpd.PaymentAutoId FROM CustomerPaymentDetails as cpd  where CustomerAutoId not in (
select cm.AutoId from CustomerMaster as cm where SalesPersonAutoId=1208)
)

 DELETE FROM PaymentOrderDetails where PaymentAutoId
 in (
 
 select cpd.PaymentAutoId FROM CustomerPaymentDetails as cpd  where CustomerAutoId not in (
select cm.AutoId from CustomerMaster as cm where SalesPersonAutoId=1208)
)

 DELETE FROM CustomerPaymentDetails  where CustomerAutoId not in (
select cm.AutoId from CustomerMaster as cm where SalesPersonAutoId=1208)
 
 

 DELETE FROM SalesPaymentMaster  where CustomerAutoId not in (
select cm.AutoId from CustomerMaster as cm where SalesPersonAutoId=1208)
 

 
DELETE FROM ProductPricingInPriceLevel  where PriceLevelAutoId 
in ( select PriceLevelAutoId from CustomerPriceLevel where 
CustomerAutoId not in (
select cm.AutoId from CustomerMaster as cm where SalesPersonAutoId=1208)
)

DELETE FROM CustomerPriceLevel  where CustomerAutoId not in (
select cm.AutoId from CustomerMaster as cm where SalesPersonAutoId=1208)

DELETE FROM ProductPricingInPriceLevel  where PriceLevelAutoId 
  in (
select AutoId FROM PriceLevelMaster where  autoid not in 
(
select PriceLevelAutoId FROM CustomerPriceLevel 
 where CustomerAutoId  in (
select cm.AutoId from CustomerMaster as cm where SalesPersonAutoId=1208)
 )
 )

 
 
DELETE FROM PriceLevelMaster where  autoid not in 
(
select PriceLevelAutoId FROM CustomerPriceLevel 
 where CustomerAutoId  in (
select cm.AutoId from CustomerMaster as cm where SalesPersonAutoId=1208)
 ) 

 
DELETE FROM CreditItemMaster where  CreditAutoId not in 
(
select c.CreditAutoId FROM CreditMemoMaster as c
 where CustomerAutoId  in (
select cm.AutoId from CustomerMaster as cm where SalesPersonAutoId=1208)
 ) 
 
DELETE FROM CreditMemoLog where  CreditAutoId not in 
(
select c.CreditAutoId FROM CreditMemoMaster as c
 where CustomerAutoId  in (
select cm.AutoId from CustomerMaster as cm where SalesPersonAutoId=1208)
 ) 
 
DELETE FROM CreditMemoMaster  
 where CustomerAutoId  not in (
select cm.AutoId from CustomerMaster as cm where SalesPersonAutoId=1208)
 
DELETE FROM RouteLog  
 where CustomerAutoId  not in (
select cm.AutoId from CustomerMaster as cm where SalesPersonAutoId=1208)


DELETE FROM CustomerCreditMaster  
 where CustomerAutoId  not in (
select cm.AutoId from CustomerMaster as cm where SalesPersonAutoId=1208)

DELETE FROM BillingAddress  
 where CustomerAutoId  not in (
select cm.AutoId from CustomerMaster as cm where SalesPersonAutoId=1208)
 
 DELETE FROM ShippingAddress  
 where CustomerAutoId  not in (
select cm.AutoId from CustomerMaster as cm where SalesPersonAutoId=1208)

DELETE FROM tbl_Custumor_StoreCreditLog  
 where CustomerAutoId  not in (
select cm.AutoId from CustomerMaster as cm where SalesPersonAutoId=1208)


DELETE FROM   CustomerMaster   where SalesPersonAutoId!=1208
  