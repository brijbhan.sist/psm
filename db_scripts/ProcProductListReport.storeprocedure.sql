Alter PROCEDURE [dbo].[ProProductListREport]                                      
@Opcode INT=NULL,    
@EmpAutoId int=null,  
@WebsiteDisplay int =null,    
@ShowOnWebsite int=null,  
@ProductName varchar(200)=null,  
@ProductId int null,  
@CategoryAutoId int =null,  
@SubCategoryAutoId int =null,  
@BrandAutoId int =null,  
@PageIndex INT = 1,      
@PageSize INT = 10,                                      
@RecordCount INT =null,                                      
@isException BIT OUT,                                      
@exceptionMessage VARCHAR(max) OUT                                      
AS                                      
BEGIN                                      
	BEGIN TRY                                      
	SET @isException=0                                      
	SET @exceptionMessage='Success'                                      
	DECLARE @STATE INT                                       
	If @Opcode=21                                                     
	BEGIN 
		INSERT INTO ProductWebsiteStatusChangeLog (ProductAutoId,OldStatus,NewStatus,UpdatedBy,UpdateDate)
		Select Autoid,ShowOnWebsite,@ShowOnWebsite,@EmpAutoId,GETDATE() from ProductMaster where ProductId=@ProductId

		update ProductMaster set ShowOnWebsite=@ShowOnWebsite,ModifiedDate=GETDATE(),ModifiedBy=@EmpAutoId,UpdateDate=GETDATE()
		where ProductId=@ProductId
	END  
	If @Opcode=41                                                     
	BEGIN                               
		SELECT ROW_NUMBER() OVER(ORDER BY ProductId desc) AS RowNumber, * INTO #Results from                        
		(                                         
		select PM.AutoId,ProductId,ProductName,ImageUrl,BM.BrandName,pm.ThumbnailImageUrl,CM.CategoryName as Category,  
		SCM.SubcategoryName as Subcategory,(case when ShowOnWebsite=1 then 'Yes' else 'No' end) as ShowOnWebsite   from ProductMaster     as PM    
		inner join CategoryMaster CM on CM.AutoId=PM.CategoryAutoId  
		inner join SubCategoryMaster SCM on SCM.AutoId=PM.SubcategoryAutoId  
		left join BrandMaster BM on BM.AutoId=PM.BrandAutoId  
		WHERE      pm.ProductStatus=1 and                                 
		( @WebsiteDisplay is null or @WebsiteDisplay=2 or PM.ShowOnWebsite=@WebsiteDisplay)  
		and (@productName='' or @productName is null or  PM.ProductName like '%' + @productName + '%')  
		and (@CategoryAutoId=0 or @CategoryAutoId is null or PM.CategoryAutoId=@CategoryAutoId)  
		and (@SubCategoryAutoId=0 or @SubCategoryAutoId is null or PM.SubcategoryAutoId=@SubCategoryAutoId)  
		and (@BrandAutoId=0 or @BrandAutoId is null or PM.BrandAutoId=@BrandAutoId)  
		and (@ProductId=0 or @ProductId is null or PM.ProductId=@ProductId)  
		)as t order by ProductId 
		SELECT COUNT(RowNumber) AS RecordCount,case when @PageSize=0 then COUNT(ProductId) else @PageSize end AS PageSize,@PageIndex as PageIndex FROM #Results             
	    SELECT * FROM #Results                                      
		WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))                                                                        
	END    
	If @Opcode=42                                                                                                           
	BEGIN  
		select  
		(  
		select AutoId,CategoryName from CategoryMaster order by CategoryName asc  
		for json Path  
		) as Category,  
		(  
		select AutoId,BrandName from BrandMaster  order by BrandName asc  
		for json Path  
		) as Brand  
		for json Path  
	END  
	If @Opcode=43                                                                                                           
	BEGIN    
		select AutoId,SubcategoryName from SubCategoryMaster where (@CategoryAutoId=0  or @CategoryAutoId is null or CategoryAutoId=@CategoryAutoId)  
		order by SubcategoryName asc  
		for json Path  
	END     
	END TRY                                      
	BEGIN CATCH                                      
		SET @isException=1                                      
		SET @exceptionMessage=ERROR_MESSAGE()                                      
	END CATCH                                      
END   