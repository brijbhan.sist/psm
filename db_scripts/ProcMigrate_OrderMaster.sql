
                
ALTER PROCEDURE [dbo].[ProcMigrate_OrderMaster]                              
 @Opcode INT=NULL,                                                                                                                                                    
 @OrderAutoId INT=NULL,   
 @db_name varchar(max)=NULL,
 @sql_query varchar(max)='',
 @TotalAmount decimal (18,2)=null,
 @DamageStockAutoId int=null,
 @NewOrderAutoId int=null,
 @NewCreditAutoId int=null,
 @NewPaymentAutoId int=null,
 @DamageId varchar(30)=null,
 @OrderNo varchar(20)=null,
 @isException bit out,                                           
 @exceptionMessage varchar(max) out            
 AS                                      
BEGIN                                      
   BEGIN TRY  
    begin tran
   set @db_name=(select [ChildDB_Name] from CompanyDetails)
   set @OrderNo=(select OrderNo from OrderMaster where AutoId=@OrderAutoId)
   if(@db_name is not null)
   begin
		select * into #tmpOrderMaster from OrderMaster where AutoId=@OrderAutoId  
		set @sql_query='insert into ['+@db_name+'].[dbo].[OrderMaster] 
		([OrderNo] ,[OrderDate] ,[DeliveryDate],[CustomerAutoId] ,[Terms] ,[BillAddrAutoId] ,[ShipAddrAutoId],[SalesPersonAutoId] ,
		[OverallDiscAmt],[ShippingCharges],[Status],[PackerAutoId],[Driver] ,[AssignDate],[Stoppage],[DelDate],[PaymentRecev],[RecevAmt],
		[AmtValue] ,[ValueChanged] ,[DiffAmt],[NewTotal],[PayThru],[DrvRemarks] ,[AmtDue],[PackedBoxes],[AcctRemarks],[ShippingType] ,[CommentType] ,[Comment] ,
		[CheckNo] ,[AddonPackedQty],[TaxType],[ManagerAutoId],[AccountAutoId],[PackerRemarks],[OrderRemarks] ,[RootName],[OrderType],[paidAmount],
		[paymentMode],[referNo],[POSAutoId],[TaxValue],[PastDue],[referenceOrderNumber],[ManagerRemarks] ,[PackerAssignDate],[PackerAssignStatus],
		[Times],[UPDATEDATE],[UPDATEDBY],[WarehouseAutoId],[WarehouseRemarks], [UnitMLTax],[MLTaxPer],[CancelledDate],[CancelledBy] ,[DriverRemarks],
		[isMLManualyApply],[IsTaxApply],[RouteAutoId],[RouteStatus],[DeviceID] ,[appVersion],[latLong],[AppOrderdate],[CancelRemark],[Weigth_OZTax] ,
		[Order_Closed_Date] ,[MLTaxRemark] ,[OrdShippingAddress] ,[DraftOrderDate],[SA_Lat],[SA_Long],[DriverCarId],[ShippingTaxEnabled],
		[MigrateRemarks],[SA_City],[SA_State],[SA_Zipcode],[SA_Address],[DeliveryFromTime],[DeliveryToTime],[ScheduleAt],
		[DeliveredAddress],[SA_Address2],[CustDefaultFromTime],[CustDefaultToTime],[staticpayableAmount],[OM_MigrationType]) 

		select [OrderNo] ,[OrderDate] ,[DeliveryDate],[CustomerAutoId] ,[Terms] ,[BillAddrAutoId] ,[ShipAddrAutoId],[SalesPersonAutoId] ,
		[OverallDiscAmt],[ShippingCharges],[Status],[PackerAutoId],[Driver] ,[AssignDate],[Stoppage],[DelDate],[PaymentRecev],[RecevAmt],
		[AmtValue] ,[ValueChanged] ,[DiffAmt],[NewTotal],[PayThru],[DrvRemarks] ,[AmtDue],[PackedBoxes],[AcctRemarks],[ShippingType] ,[CommentType] ,[Comment] ,
		[CheckNo] ,[AddonPackedQty],[TaxType],[ManagerAutoId],[AccountAutoId],[PackerRemarks],[OrderRemarks] ,[RootName],[OrderType],[paidAmount],
		[paymentMode],[referNo],[POSAutoId],[TaxValue],[PastDue],[referenceOrderNumber],[ManagerRemarks] ,[PackerAssignDate],[PackerAssignStatus],
		[Times],[UPDATEDATE],[UPDATEDBY],[WarehouseAutoId],[WarehouseRemarks], [UnitMLTax],[MLTaxPer],[CancelledDate],[CancelledBy] ,[DriverRemarks],
		[isMLManualyApply],[IsTaxApply],[RouteAutoId],[RouteStatus],[DeviceID] ,[appVersion],[latLong],[AppOrderdate],[CancelRemark],[Weigth_OZTax] ,
		[Order_Closed_Date],[MLTaxRemark] ,[OrdShippingAddress] ,[DraftOrderDate],[SA_Lat],[SA_Long],[DriverCarId],[ShippingTaxEnabled],
		[MigrateRemarks],[SA_City],[SA_State],[SA_Zipcode],[SA_Address],[DeliveryFromTime],[DeliveryToTime],[ScheduleAt],
		[DeliveredAddress],[SA_Address2],[CustDefaultFromTime],[CustDefaultToTime],[staticpayableAmount],[OM_MigrationType] from #tmpOrderMaster' 
		exec  (@sql_query) 
		 
		set @sql_query='insert into ['+@db_name+'].[dbo].[OrderItemMaster] ([OrderAutoId],[ProductAutoId],[UnitTypeAutoId],[QtyPerUnit],
		[UnitPrice],[RequiredQty],[SRP],[GP],[Tax],[Status],
		[Barcode],[QtyShip],[RemainQty],[IsExchange],[TaxValue],[UnitMLQty],[isFreeItem],[ManualScan],[Weight_Oz],[Original_UnitType],[AddOnQty],
		[OM_MinPrice],[OM_CostPrice],[BasePrice],[AddedItemKey],[AddOnPackedPeice] ,[Oim_Discount],[Oim_DiscountAmount]) 
		select (select om.AutoId from ['+@db_name+'].[dbo].[OrderMaster] as om where OrderNo='''+@OrderNo+''') as OrderAutoId,pmnew.AutoId,unittypeAutoId,QtyPerUnit,
		UnitPrice,RequiredQty,SRP,GP,Tax,Status,Barcode,QtyShip,RemainQty,IsExchange,TaxValue,UnitMLQty,isFreeItem,
		ManualScan,Weight_Oz,Original_UnitType,AddOnQty,[OM_MinPrice],[OM_CostPrice],[BasePrice],[AddedItemKey],[AddOnPackedPeice] ,[Oim_Discount]
		,[Oim_DiscountAmount] from [dbo].OrderItemMaster as oim
		inner join ProductMaster as pm on pm.AutoId=oim.ProductAutoId
		inner join ['+@db_name+'].[dbo].ProductMaster as pmnew on pmnew.ProductId=pm.ProductId
		where convert(varchar(20),OrderAutoId)='+convert(varchar(20),@OrderAutoId)
		exec (@sql_query) 

		 
		
		set @sql_query='insert into ['+@db_name+'].[dbo].[Order_Original] (AutoId,[OrderNo],[OrderDate],[DeliveryDate],[CustomerAutoId],[Terms],[BillAddrAutoId],[SalesPersonAutoId],[ShipAddrAutoId],
		[TotalAmount],[OverallDisc],[OverallDiscAmt],[ShippingCharges],[TotalTax],[GrandTotal],[ShippingType],[TaxType],[MLQty],[MLTax])
		select (select om.AutoId from ['+@db_name+'].[dbo].[OrderMaster] as om where OrderNo='''+@OrderNo+'''),[OrderNo],[OrderDate],[DeliveryDate],[CustomerAutoId],[Terms],[BillAddrAutoId],[SalesPersonAutoId],[ShipAddrAutoId],
		[TotalAmount],[OverallDisc],[OverallDiscAmt],[ShippingCharges],[TotalTax],[GrandTotal],[ShippingType],[TaxType],[MLQty],[MLTax] 
		from [dbo].[Order_Original] where OrderNo='''+@OrderNo+''''
		exec (@sql_query) 

		set @sql_query='insert into ['+@db_name+'].[dbo].[OrderItems_Original] ([OrderAutoId],[ProductAutoId],[UnitTypeAutoId],[QtyPerUnit]
		,[UnitPrice],[RequiredQty],
		[TotalPieces],[SRP],[GP],[Tax],[NetPrice],[IsExchange],[isFreeItem],[UnitMLQty],[TotalMLQty]) 
		select  (select om.AutoId from ['+@db_name+'].[dbo].[OrderMaster] as om where OrderNo='''+@OrderNo+'''),pmnew.AutoId,[UnitTypeAutoId]
		,[QtyPerUnit],[UnitPrice],[RequiredQty],
		[TotalPieces],[SRP],[GP],[Tax],[NetPrice],[IsExchange],[isFreeItem],[UnitMLQty],[TotalMLQty] from OrderItems_Original as oim
		inner join productMaster as pm on pm.autoid=oim.ProductAutoid
		inner join ['+@db_name+'].[dbo].productMaster as pmnew on pmnew.productid=pm.ProductId
		where convert(varchar(20),OrderAutoId)='+CONVERT(varchar(50),@OrderAutoId)
		exec (@sql_query)

		
		set @sql_query='insert into ['+@db_name+'].[dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[ActionIPAddress],[OrderAutoId]) 
		select [ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[ActionIPAddress],(select om.AutoId from ['+@db_name+'].[dbo].[OrderMaster] as om where OrderNo='''+@OrderNo+''')
		from [dbo].[tbl_OrderLog] where convert(varchar(20),OrderAutoId)='+CONVERT(varchar(50),@OrderAutoId)
		exec (@sql_query)  

		set @sql_query='insert into ['+@db_name+'].[dbo].[DeliveredOrders] (OrderAutoId,OldPaidAmount,AdjestOrderAmount,dueOrderStatus,[PaymentAutoId],[PayId])
		SELECT (select om.AutoId from ['+@db_name+'].[dbo].[OrderMaster] as om where OrderNo='''+@OrderNo+'''),
		OldPaidAmount,AdjestOrderAmount,1,[PaymentAutoId],[PayId]
		FROM DeliveredOrders  where convert(varchar(20),OrderAutoId)='+CONVERT(varchar(50),@OrderAutoId)
		exec (@sql_query)      
		
	
		set @sql_query='insert into ['+@db_name+'].[dbo].[Delivered_Order_Items] ([OrderAutoId],[ProductAutoId] ,[UnitAutoId] ,[QtyPerUnit] ,[UnitPrice] ,[Barcode],[QtyShip] ,[SRP] ,[GP]
		,[Tax],[FreshReturnQty] ,[FreshReturnUnitAutoId],[DamageReturnQty],[DamageReturnUnitAutoId],[IsExchange],[TaxValue] ,[isFreeItem],[UnitMLQty]
		,[QtyPerUnit_Fresh],[QtyPerUnit_Damage],[Del_CostPrice] ,[Del_CommCode],[Del_Weight_Oz]
		,[MissingItemQty],[MissingItemUnitAutoId],[QtyPerUnit_Missing],[Del_MinPrice],[BasePrice],[Del_discount]
		,[StaticDelQty] ,[StaticNetPrice])
		select (select om.AutoId from ['+@db_name+'].[dbo].[OrderMaster] as om where OrderNo='''+@OrderNo+'''),PMNew.AutoId,[UnitAutoId] ,[QtyPerUnit] ,[UnitPrice] ,[Barcode],[QtyShip] ,[SRP] ,[GP]
		,[Tax],[FreshReturnQty] ,[FreshReturnUnitAutoId],[DamageReturnQty],[DamageReturnUnitAutoId],[IsExchange],[TaxValue] ,[isFreeItem],[UnitMLQty]
		,[QtyPerUnit_Fresh],[QtyPerUnit_Damage],[Del_CostPrice] ,[Del_CommCode],[Del_Weight_Oz]
		,[MissingItemQty],[MissingItemUnitAutoId],[QtyPerUnit_Missing],[Del_MinPrice],[BasePrice],[Del_discount]
		,[StaticDelQty] ,[StaticNetPrice] from Delivered_Order_Items AS DOI
		INNER JOIN ProductMaster AS PM ON PM.AutoId=DOI.ProductAutoId
		INNER JOIN ['+@db_name+'].[dbo].[ProductMaster] AS PMNew ON PMNew.ProductId=pm.ProductId
		where convert(varchar(20),OrderAutoId)='+CONVERT(varchar(50),@OrderAutoId)
		exec (@sql_query)                                                                                                             

		select * into #tmpDrvLog from DrvLog where OrderAutoId=@OrderAutoId
		set @sql_query='insert into ['+@db_name+'].[dbo].[DrvLog] ([DrvAutoId],[OrderAutoId],[AssignDate])
		 select [DrvAutoId],(select om.AutoId from ['+@db_name+'].[dbo].[OrderMaster] as om where OrderNo='''+@OrderNo+'''),[AssignDate] from #tmpDrvLog'
		exec (@sql_query) 

		set @sql_query='insert into ['+@db_name+'].[dbo].[GenPacking] ([PackingId],[OrderAutoId],[PackingDate],[PackerAutoId]) 
		select [PackingId],(select om.AutoId from ['+@db_name+'].[dbo].[OrderMaster] as om where OrderNo='''+@OrderNo+'''),PackingDate,[PackerAutoId] from [dbo].[GenPacking]
		where convert(varchar(20),OrderAutoId)='+CONVERT(varchar(50),@OrderAutoId)
		exec (@sql_query)  

		set @sql_query='insert into ['+@db_name+'].[dbo].[AllocatedPackingDetails] ([OrderAutId],[ProductAutoId],[AllocatedQty],[PackingType],[AllocateDate]) 
		select [OrderAutId],[ProductAutoId],[AllocatedQty],[PackingType],[AllocateDate] from [dbo].[AllocatedPackingDetails]
		where convert(varchar(20),OrderAutId)='+CONVERT(varchar(50),@OrderAutoId)
		exec (@sql_query)  

------------------------------------------------------Credit Memo-------------------------------------------------------------


		if (select count(*) from [CreditMemoMaster] where OrderAutoId=@OrderAutoId)>0
			begin
				set @sql_query='insert into ['+@db_name+'].[dbo].[CreditMemoMaster] ([CreditNo],[CustomerAutoId],[CreatedBy],[CreditDate],[Remarks],[ApprovedBy],
				[CompletedBy],[ApprovalDate],[CompletionDate],[Status],[PaidAmount],[OrderAutoId],[SalesAmount],[CreditType],[OverallDiscAmt],
				[TotalTax],[ApplyMLQty],[MLTaxPer],[IsMLTaxApply],[CompletionRemarks],[referenceCreditMemoNumber],[TaxTypeAutoId],
				[TaxValue],[ManagerRemark],[WeightTax],[UpdatedBy],[UpdatedDate],[CancelRemark],[SalesPersonAutoId],[MLTaxRemark]
				,[CreditMemoType],[ReferenceOrderAutoId],[TotalTax_Old]) select
				[CreditNo],[CustomerAutoId],[CreatedBy],[CreditDate],[Remarks],[ApprovedBy],
				[CompletedBy],[ApprovalDate],[CompletionDate],[Status],[PaidAmount],(select om.AutoId from ['+@db_name+'].[dbo].[OrderMaster] as om where OrderNo='''+@OrderNo+'''),[SalesAmount],[CreditType],[OverallDiscAmt],
				[TotalTax],[ApplyMLQty],[MLTaxPer],[IsMLTaxApply],[CompletionRemarks],[referenceCreditMemoNumber],[TaxTypeAutoId],
				[TaxValue],[ManagerRemark],[WeightTax],[UpdatedBy],[UpdatedDate],[CancelRemark],[SalesPersonAutoId],[MLTaxRemark]
				,[CreditMemoType],[ReferenceOrderAutoId],[TotalTax_Old] from 
				[dbo].[CreditMemoMaster] where convert(varchar(50),OrderAutoId)='+convert(varchar(50),@OrderAutoId)
				exec (@sql_query)                                                  

				set @sql_query='insert into ['+@db_name+'].[dbo].[CreditItemMaster] ([CreditAutoId],[ProductAutoId],[UnitAutoId],[UnitPrice],[SRP],
				[TaxRate],[RequiredQty],[QtyPerUnit],
				[AcceptedQty],[ManagerUnitPrice],[UnitMLQty],[Weight_OZ],[CostPrice],[QtyPerUnit_Fresh],[QtyPerUnit_Damage],[QtyPerUnit_Missing],
				[QtyReturn_Fresh],[QtyReturn_Damage],[QtyReturn_Missing],[OM_MinPrice],[OM_CostPrice],[OM_BasePrice])		 
				select (select cmm.CreditAutoId from ['+@db_name+'].[dbo].[CreditMemoMaster] as cmm where cmm.CreditNo=cm.CreditNo) as CreditMemo,
				pmnew.[AutoId],[UnitAutoId],[UnitPrice],[SRP],cim.[TaxRate],
				[RequiredQty],[QtyPerUnit],[AcceptedQty],[ManagerUnitPrice],[UnitMLQty],[Weight_OZ],[CostPrice],
				[QtyPerUnit_Fresh],[QtyPerUnit_Damage],[QtyPerUnit_Missing],[QtyReturn_Fresh],[QtyReturn_Damage],[QtyReturn_Missing]
				,[OM_MinPrice],[OM_CostPrice],[OM_BasePrice] from CreditItemMaster AS cim 
				inner join CreditMemoMaster as cm on cm.CreditAutoId=cim.CreditAutoId
				inner join ProductMaster as pm on pm.AutoId=cim.ProductAutoId
				inner join ['+@db_name+'].[dbo].ProductMaster as pmnew on pm.ProductId=pmnew.ProductId
				where convert(varchar(50),cm.OrderAutoId)='+convert(varchar(50),@OrderAutoId)
		         exec (@sql_query)                                                  
																	   
				set @sql_query='insert into ['+@db_name+'].[dbo].[CreditMemoLog] ([CreditAutoId],[ActionAutoId],[EmpAutoId],[Remarks],[LogDate],[ActionIPAddress]) 
				select (select cmm.CreditAutoId from ['+@db_name+'].[dbo].[CreditMemoMaster] as cmm where cmm.CreditNo=cm.CreditNo) as CreditMemo,
				[ActionAutoId],[EmpAutoId],cml.[Remarks],[LogDate],[ActionIPAddress]
				from CreditMemoLog AS cml 
				inner join CreditMemoMaster as cm on cm.CreditAutoId=cml.CreditAutoId 
				where convert(varchar(50),cm.OrderAutoId)='+convert(varchar(50),@OrderAutoId)
				
				exec (@sql_query)                                                                           
			end

--------------------------------------------------------Payment details-----------------------------------------------------
                
				set @sql_query='insert into ['+@db_name+'].[dbo].[SalesPaymentMaster] ([PayId],[CustomerAutoId],[ReceivedDate],[ReceivedAmount],[PaymentMode],
				[ReferenceId],[Remarks],[ReceiveDate],[ReceiveBy],[Status],[PayType],[OrderAutoId],[cancelBy],[CancelDate],[CancelRemarks],
				[referencePaymentNumber],[DateUpdate],[ChequeNo],[ChequeDate],[ChequeRemarks],[BankCharges],
				[HoldBy],[DeviceId],[AppVersion],[LatLong]) 
				select [PayId],[CustomerAutoId],[ReceivedDate],[ReceivedAmount],[PaymentMode],
				[ReferenceId],[Remarks],[ReceiveDate],[ReceiveBy],[Status],[PayType],[OrderAutoId],[cancelBy],[CancelDate],[CancelRemarks],
				[referencePaymentNumber],[DateUpdate],[ChequeNo],[ChequeDate],[ChequeRemarks],[BankCharges],[HoldBy]
				,[DeviceId],[AppVersion],[LatLong] from SalesPaymentMaster
				where PaymentAutoId in (select cpd.refPaymentId from PaymentOrderDetails as pod
				inner join CustomerPaymentDetails as cpd on cpd.PaymentAutoId=pod.PaymentAutoId and Status=0 where convert(varchar(50),pod.OrderAutoId)='+convert(varchar(50),@OrderAutoId)+')'
				exec (@sql_query)
				
				
				set @sql_query='insert into ['+@db_name+'].[dbo].[CustomerPaymentDetails] ([PaymentId],[PaymentDate],[ReceivedAmount],[EmpAutoId],
				[ReceivedBy],[PaymentRemarks],[EmployeeRemarks],[CustomerAutoId],[PaymentMode],[ReferenceNo],[CreditAmount],[refPaymentId],
				[CANCELBY],[CANCELDATE],[CANCELREMARKS],[BACKCHARGES],[Status],[DateUpdate],[SortAmount],[TotalCurrencyAmount],[MigrationType]) 
				select
				[PaymentId],[PaymentDate],cpd.[ReceivedAmount],[EmpAutoId],
				[ReceivedBy],[PaymentRemarks],[EmployeeRemarks],[CustomerAutoId],[PaymentMode],[ReferenceNo],[CreditAmount],
				(
				select spm.PaymentAutoId from ['+@db_name+'].[dbo].[SalesPaymentMaster] as spm where spm.PayId=(
				select sm.payid from SalesPaymentMaster as sm where sm.PaymentAutoId=cpd.refPaymentId
				)
				)				,
				[CANCELBY],[CANCELDATE],[CANCELREMARKS],[BACKCHARGES],[Status],[DateUpdate],[SortAmount],[TotalCurrencyAmount]
				,[MigrationType] from [CustomerPaymentDetails] as cpd
				inner join PaymentOrderDetails as pod on cpd.PaymentAutoId=pod.PaymentAutoId and Status=0
				where convert(varchar(50),pod.OrderAutoId)='+convert(varchar(50),@OrderAutoId)
				exec (@sql_query)
                
				set @sql_query='insert into ['+@db_name+'].[dbo].[PaymentOrderDetails] ([PaymentAutoId],[OrderAutoId],[ReceivedAmount]) 
				select pdnew.PaymentAutoId,(select om.AutoId from ['+@db_name+'].[dbo].[OrderMaster] as om where OrderNo='''+@OrderNo+''') as OrderAutoId,pod.ReceivedAmount from PaymentOrderDetails as pod 
				inner join [CustomerPaymentDetails] as cpd on cpd.PaymentAutoId=pod.PaymentAutoId and Status=0
				inner join ['+@db_name+'].[dbo].[CustomerPaymentDetails] as pdnew on pdnew.PaymentId=cpd.PaymentId
				where convert(varchar(50),pod.OrderAutoId)='+CONVERT(varchar(20),@OrderAutoId)
				exec (@sql_query) 
				

				set @sql_query='insert into ['+@db_name+'].[dbo].[PaymentCurrencyDetails] ([CurrencyAutoId],[PaymentAutoId],[NoofValue],[TotalAmount],[OldTotalAmount],[P_CurrencyValue]) 
				select pcd.CurrencyAutoId,pdnew.PaymentAutoId,pcd.NoofValue,pcd.TotalAmount,[OldTotalAmount],[P_CurrencyValue] from PaymentCurrencyDetails as pcd 
				inner join [CustomerPaymentDetails] as cpd on cpd.PaymentAutoId=pcd.PaymentAutoId and Status=0
				inner join ['+@db_name+'].[dbo].[CustomerPaymentDetails] as pdnew on pdnew.PaymentId=cpd.PaymentId
				inner join [dbo].PaymentOrderDetails as pod on pod.PaymentAutoId=cpd.PaymentAutoId
				where convert(varchar(50),pod.OrderAutoId)='+CONVERT(varchar(20),@OrderAutoId)
				exec (@sql_query) 

		------------------------------------------------Insert Damage Stock-------------------------------------------
         SET @DamageId=(select dbo.SequenceCodeGenerator('DamageId'))  

		 insert into [dbo].[DamageStockMaster] ([OrderId],[Status],[Type],[CreatedDate]) values(@OrderNo,1,1,GETDATE())
		 set @DamageStockAutoId=SCOPE_IDENTITY()

		 insert into [dbo].[DamageStockItems] ([DamageStockAutoId],[ProductAutoId],[UnitTypeAutoId],[QtyPerUnit],[UnitPrice],[RequiredQty],
		 [QtyShip],[RemainQty],[NetPrice],[OM_CostPrice],[OM_MinPrice]) select @DamageStockAutoId,ProductAutoId,UnitTypeAutoId,
		 QtyPerUnit,UnitPrice,RequiredQty,QtyShip,RemainQty,NetPrice,OM_CostPrice,OM_MinPrice from OrderItemMaster where OrderAutoId=@OrderAutoId

		  UPDATE SequenceCodeGeneratorMaster SET currentSequence=currentSequence+1 WHERE SequenceCode='DamageId'   

		--------------------------------------------------delete credit --------------------------------------------

		delete from CreditMemoLog where CreditAutoId in (select CreditAutoId from CreditMemoMaster where OrderAutoId=@OrderAutoId)
		delete from CreditItemMaster where CreditAutoId in (select CreditAutoId from CreditMemoMaster where OrderAutoId=@OrderAutoId)
		delete from CreditMemoMaster where OrderAutoId=@OrderAutoId
	     
		 -----------------------------------delete payment details------------------------------------------------
		
		delete from [dbo].[PaymentCurrencyDetails] where PaymentAutoId in (select po.PaymentAutoId from PaymentOrderDetails po where OrderAutoId=@OrderAutoId)
		
		delete from SalesPaymentMaster where PaymentAutoId in (
			select refPaymentId  from [dbo].[CustomerPaymentDetails] as cpd where cpd.PaymentAutoId in (select od.PaymentAutoId from PaymentOrderDetails as od where OrderAutoId=@OrderAutoId)
		)

		delete from [dbo].[CustomerPaymentDetails] where PaymentAutoId in (select po.PaymentAutoId from PaymentOrderDetails po where OrderAutoId=@OrderAutoId)
		delete from PaymentOrderDetails where OrderAutoId=@OrderAutoId
		-----------------------------------------delete order details---------------------------------------------

		delete from OrderItemMaster where OrderAutoId=@OrderAutoId 
		delete from tbl_OrderLog where OrderAutoId=@OrderAutoId
		delete from DeliveredOrders where OrderAutoId=@OrderAutoId
		delete from Delivered_Order_Items where OrderAutoId=@OrderAutoId
		delete from DrvLog where OrderAutoId=@OrderAutoId
		Delete from GenPacking where OrderAutoId=@OrderAutoId
		delete from Order_Original where AutoId=@OrderAutoId
		delete from OrderItems_Original where OrderAutoId=@OrderAutoId
		delete from OrderMaster where AutoId=@OrderAutoId
		delete from [dbo].[AllocatedPackingDetails] where OrderAutId=@OrderAutoId
 end
 else
 begin
		Set @isException=1                      
	    Set @exceptionMessage='Please assign sub child location.'
 end    
 commit tran
  END TRY                      
  BEGIN CATCH       
  rollback tran
	  Set @isException=1                      
	  Set @exceptionMessage=ERROR_MESSAGE()--'Oops! Something went wrong.Please try later.'                    
  END CATCH 
END 
