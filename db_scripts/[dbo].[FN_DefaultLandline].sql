USE [psmnj.a1whm.com]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create FUNCTION  [dbo].[FN_DefaultLandline] 
(   
  @AutoId int   
)  
RETURNS varchar(100)  
AS  
BEGIN  
	DECLARE @Landline varchar(100)
	set @Landline=isnull((SELECT TOP 1 Landline FROM CustomerContactPerson WHERE CustomerAutoId=@AutoId AND ISDefault=1),'')
	RETURN @Landline  
END 
GO
Alter table CustomerMaster Drop column Contact1
Alter table CustomerMaster add Contact1 As ([dbo].[FN_DefaultLandline](AutoId))