USE [psmnj.a1whm.com]
GO

/****** Object:  UserDefinedTableType [dbo].[DriverLog]    Script Date: 07-04-2020 04:17:26 ******/
DROP TYPE [dbo].[DriverLog]
GO

/****** Object:  UserDefinedTableType [dbo].[DriverLog]    Script Date: 07-04-2020 04:17:26 ******/
CREATE TYPE [dbo].[DriverLog] AS TABLE(
	[PlanningId] [int] NOT NULL,
	[DriverNo] [int] NOT NULL,
	[DriverAutoId] [int] NOT NULL,
	[DriverPlanningAutoId] [int] NOT NULL,
	[DriverStartTime] [varchar](10) NOT NULL,
	[DraverEndTime] [varchar](10) NOT NULL,
	[NoOfStop] [varchar](50) NOT NULL,
	[CarAutoId] [int] NOT NULL,
	[RouteDate] [datetime] NOT NULL
)
GO


