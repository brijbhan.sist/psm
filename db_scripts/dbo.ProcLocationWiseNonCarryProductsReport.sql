
                   
create or alter PROCEDURE [dbo].[ProcLocationBasedNonCarryProductsReport]                        
@Opcode INT=NULL,                    
@ProductId varchar(20)=null,
@ProductName varchar(100)=null,
@Month int=null,
@CategoryAutoId int=0,
@SubCategoryAutoId int=0,
@BrandAutoId int=0,
@OrderAutoIds varchar(max)='',
@PageIndex INT=1,                    
@PageSize INT=10,                    
@RecordCount INT=null,                    
@isException bit out,                    
@exceptionMessage varchar(max) out                            
AS                                    
BEGIN                       
BEGIN TRY                    
	Set @isException=0                    
	Set @exceptionMessage='Success'                    
  if(@Opcode=41)                    
	begin    
	    Declare @LiveDate date
		SET @LiveDate=(Select LiveDate from CompanyDetails)

		select ROW_NUMBER() over(order by ProductId asc) as RowNumber,* into #Result    from (                      
			select PM.AutoId,ProductId,ProductName,CM.CategoryName,SCM.SubcategoryName,BM.BrandName,Format(PM.CreateDate,'MM/dd/yyyy') as CreateDate,
			FORMAT((
			select max(orderdate) from OrderMaster as om 
			inner join Delivered_Order_Items as do on do.OrderAutoId=om.AutoId
			where om.Status=11 and do.ProductAutoId=pm.AutoId

			),'MM/dd/yyyy') as lastdate,PM.ProductStatus
			from ProductMaster as PM
			inner join CategoryMaster CM on CM.AutoId=PM.CategoryAutoId  
			inner join SubCategoryMaster SCM on SCM.AutoId=PM.SubcategoryAutoId  
			left join BrandMaster BM on BM.AutoId=PM.BrandAutoId  

			where pm.AutoId not in (
			select distinct OIM.ProductAutoId from  OrderMaster  as OM
			inner join Delivered_Order_Items  as OIM on OM.AutoId=OIM.OrderAutoId
			where  convert(date,OrderDate)>=(case when @Month=0 then CONVERT(date,@LiveDate) else convert(date,GETDATE()-@Month*30) end)
			and om.Status=11
			)
			and pm.ProductStatus=1
			and (@ProductName is null OR @ProductName='' OR ProductId like '%'+@ProductName+'%' or ProductName like '%'+@ProductName+'%' )
			--and (@ProductName is null OR @ProductName='' OR ProductName LIKE '%' + @ProductName + '%')
			and (@CategoryAutoId=0 or @CategoryAutoId is null or PM.CategoryAutoId=@CategoryAutoId)  
			and (@SubCategoryAutoId=0 or @SubCategoryAutoId is null or PM.SubcategoryAutoId=@SubCategoryAutoId)  
			and (@BrandAutoId=0 or @BrandAutoId is null or PM.BrandAutoId=@BrandAutoId)  
		) as t

		Select COUNT(ProductId) as TotalRecord FROM #Result
                                    
		SELECT * FROM #Result                  
		WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))   order by ProductId asc
		
		SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize = 0 then COUNT(RowNumber) else @PageSize end AS PageSize,
		@PageIndex AS PageIndex,Format(GETDATE(),'MM/dd/yyyy hh:mm tt') as PrintDate FROM #Result 
	end 
	If @Opcode=42                                                                                                           
	BEGIN  
		select  
		(  
		select AutoId,CategoryName from CategoryMaster order by CategoryName asc  
		for json Path  
		) as Category,  
		(  
		select AutoId,BrandName from BrandMaster  order by BrandName asc  
		for json Path  
		) as Brand  
		for json Path  
	END  
	If @Opcode=43                                                                                                           
	BEGIN    
		select AutoId,SubcategoryName from SubCategoryMaster where (@CategoryAutoId=0  or @CategoryAutoId is null or CategoryAutoId=@CategoryAutoId)  
		order by SubcategoryName asc  
		for json Path  
	END 
	If @Opcode=44                                                                                                           
	BEGIN	
	   BEGIN TRY
	    BEGIN Transaction 

		Insert into ProductMasterLog
		(ProductId,ProductName,ProductLocation,ImageUrl,CategoryAutoId,SubcategoryAutoId,Stock,ProductType,TaxRate,PackingAutoId,
		VendorAutoId,ReOrderMark,MLQty,UpdateDate,ProductStatus,CreateBy,CreateDate,ModifiedBy,ModifiedDate,BrandAutoId,P_CommCode,
		WeightOz,IsApply_ML,IsApply_Oz,ThumbnailImageUrl,ThirdImage,NewSRP,ShowOnWebsite,IsOutOfStock,Description,P_SRP,CurrentStock,
		CheckImageURLExistense,CheckThirdImageExistense,Product_ShowOnWebsite,OriginalImageUrl) 

		Select ProductId,ProductName,ProductLocation,ImageUrl,CategoryAutoId,SubcategoryAutoId,Stock,ProductType,TaxRate,
		PackingAutoId,VendorAutoId,ReOrderMark,MLQty,UpdateDate,ProductStatus,CreateBy,CreateDate,ModifiedBy,ModifiedDate,
		BrandAutoId,P_CommCode,WeightOz,IsApply_ML,IsApply_Oz,ThumbnailImageUrl,ThirdImage,NewSRP,ShowOnWebsite,IsOutOfStock,
		Description,P_SRP,CurrentStock,CheckImageURLExistense,CheckThirdImageExistense,Product_ShowOnWebsite,OriginalImageUrl
		from ProductMaster WHERE AutoId  IN (select * from dbo.fnSplitString(@OrderAutoIds,','))   

		Update ProductMaster SET ProductStatus=0,UpdateDate=GETDATE() Where AutoId IN (select * from dbo.fnSplitString(@OrderAutoIds,','))   

		Commit Transaction

	   END TRY
	   BEGIN CATCH
	      RollBack Transaction
	      SET @isException=1
		  SET @exceptionMessage=ERROR_MESSAGE()
	   END CATCH
	END 
END TRY                    
BEGIN CATCH                    
Set @isException=1                    
Set @exceptionMessage=ERROR_MESSAGE()                    
END CATCH                                 
END 
GO
