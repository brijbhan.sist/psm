alter table ordermaster DROP column TotalCostPrice
drop FUNCTION  [dbo].[FN_Order_CostPrice]
go
Create FUNCTION  [dbo].[FN_Order_CostPrice]
( 
	@orderAutoId int
	
)
RETURNS decimal(18,2)
AS
BEGIN
	DECLARE @CostPrice decimal(18,2)=0.00	
	declare @Status INT=(select Status from OrderMaster where AutoId=@orderAutoId)
	
	IF @Status=11
	BEGIN 
		SET @CostPrice=(SELECT sum((Del_CostPrice/QtyPerUnit)*QtyDel) FROM Delivered_Order_Items WHERE OrderAutoId=@orderAutoId)
	END 
	ELSE IF @Status in (1,2,8)
	BEGIN
	    SET @CostPrice=(SELECT sum(OM_CostPrice*RequiredQty) FROM OrderItemMaster WHERE OrderAutoId=@orderAutoId)	
	END
	ELSE 
	BEGIN
	    SET @CostPrice=(SELECT sum(OM_CostPrice*ISNULL(QtyShip,0)) FROM OrderItemMaster WHERE OrderAutoId=@orderAutoId)	
	END
	RETURN @CostPrice
END
go
alter table ordermaster add TotalCostPrice as [dbo].[FN_Order_CostPrice](autoid)