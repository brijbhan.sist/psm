USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcCustomer_CreditMemoList]    Script Date: 10/3/2020 7:22:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[ProcCustomer_CreditMemoList]
@OpCode int =null,
@CustomerAutoId int=null,
@PageIndex INT=1,                                                                                                  
@PageSize INT=10, 
@isException BIT OUT,                                
@exceptionMessage VARCHAR(max) OUT

AS
BEGIN
     SET @isException=0
	 SET @exceptionMessage=''
     BEGIN TRY
        IF @OpCode=40
		BEGIN
		    SELECT ROW_NUMBER() OVER(ORDER BY Convert(datetime,CreditDate) DESC) AS RowNumber, * INTO #RESULT FROM                              
            ( 
		    SELECT CreditAutoId,ot.OrderType AS CreditType,CreditNo,                                                                                                  
			CONVERT(varchar(10),CreditDate,101) as CreditDate,CONVERT(varchar(10),CompletionDate,101) as ApplyDate,                                   
			StatusType,NetAmount as  SalesAmount,                                           
			(SELECT OrderNo FROM ORDERMASTER AS OM WHERE OM.AUTOID=CMM.ORDERAUTOID) as AppliedOrder,                                                                         
			(SELECT FirstName+' '+ISNULL(LASTNAME,'') FROM EmployeeMaster AS emp1 WHERE emp1.AUTOID=CMM.createdby) as CreatedBy,                                                                                                  
			(SELECT  FirstName+' '+ISNULL(LASTNAME,'')  FROM EmployeeMaster AS emp2 WHERE emp2.AUTOID=CMM.ApprovedBy) as ApprovedBy,                                                                                                  
			(SELECT  FirstName+' '+ISNULL(LASTNAME,'')  FROM EmployeeMaster AS emp3 WHERE emp3.AUTOID=CMM.CompletedBy) as SettledBy,                                                                                       
			TotalAmount,OrderAutoId,Status FROM CreditMemoMaster AS CMM                                                                                                  
			inner join statusmaster as sm on sm.autoid=cmm.Status  and sm.Category='CreditMaster'
			INNER JOIN OrderTypeMaster as ot on ot.AutoId=CMM.CreditType and ot.Type='Credit'
			WHERE CustomerAutoId=@CustomerAutoId  
			 )AS t ORDER BY Convert(datetime,CreditDate) DESC

			SELECT COUNT(CreditAutoId) AS RecordCount, case when @PageSize=0 then COUNT(CreditAutoId) else @PageSize end AS PageSize,                                                              
			@PageIndex AS PageIndex FROM #RESULT    
			
			SELECT * from #RESULT  
			WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)) 
			 
		END
	 END TRY
	 BEGIN CATCH
	       SET @isException=1
		   SET @exceptionMessage=ERROR_MESSAGE()
	 END CATCH
END