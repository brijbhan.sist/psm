ALTER  PROCEDURE [dbo].[ProcSalesReportByDue]                                                                   
@Opcode INT=NULL,                                                                    
@CustomerAutoId INT=NULL,                                                                     
@CustomerType int=null,                                                                                                                          
@EmpAutoId  INT=NULL,                                                                                                 
@SalesPersonAutoId int =null,  
@SalesPerson  VARCHAR(500)=NULL,                                                         
@PageIndex INT=1,                                                                    
@PageSize INT=10,                                                                    
@RecordCount INT=null,                                                                    
@isException bit out,                                                                    
@exceptionMessage varchar(max) out                                                                    
AS                                                                    
BEGIN                                                                     
 BEGIN TRY                                                                    
  Set @isException=0                                                                    
  Set @exceptionMessage='Success'                                                                    
                                                                                                                       
 IF @Opcode=41                                                                 
  BEGIN                                                                    
  select 
  (SELECT AutoId  as CId,CustomerType as Ct from CustomerType order by CustomerType ASC
  for json path
  ) as CType,
  (
  Select em.AutoId as SID ,(em.FirstName + ' ' + em.LastName) as SP from EmployeeMaster em                                           
  inner join EmployeeTypeMaster etm on etm.AutoId = em.EmpType                                          
  where EmpType = 2 and Status=1 order by SP ASC 
  for json path
  ) as SalesList
   for JSON path
 END                                                                  
  ELSE IF @Opcode=42                                                               
  BEGIN   
  SELECT AutoId AS CuId,CustomerId+' '+CustomerName as CUN FROM CustomerMaster   WHERE            
  status=1 and             
   (CustomerType=@CustomerType                                       
  OR ISNULL(@CustomerType,0)=0) and (SalesPersonAutoId=@SalesPersonAutoId or ISNULL(@SalesPersonAutoId,0)=0)
  order by replace(CustomerId+' '+CustomerName,' ','') ASC     
   for json path
   
  END                                                

ELSE  If @Opcode=43                                                                  
  BEGIN                                                                  
 select om.AutoID,CustomerAutoId,OrderDate,FirstName+' '+ISNULL(LastName,'') as SalesPerson into #OrderAutoid from OrderMaster as om              
 inner join  CustomerMaster as cm on cm.AutoId=om.CustomerAutoId        
 inner join  DeliveredOrders as do on do.OrderAutoId=om.AutoId and ISNULL(do.dueOrderStatus,0)=0 
 inner join  EmployeeMaster as emp on cm.SalesPersonAutoId=emp.AutoId where               
 om.status=11                
 AND (ISNULL(@CustomerAutoId,0)=0 or cm.AutoId=@CustomerAutoId)                                                 
 AND(ISNULL(@CustomerType,0)=0 or cm.CustomerType=@CustomerType)                                     
 and (isnull(@SalesPersonAutoId,0)=0 or cm.SalesPersonAutoId=@SalesPersonAutoId)              
                                                            
  select ROW_NUMBER() over(order by CustomerName) as RowNumber, CustomerName,SUM(isnull([0-30 Days],0)) as Days30, sum([31-60 Days]) as Days60, sum([61-90 Days]) as Days90 ,sum([90-Above]) as Above90,COUNT(1) as NoofOrder,sum([Order Total]) as OrderTotal
 , sum(AmtDue) as AmtDue,sum(TotalPaid) as TotalPaid,SalesPerson into #Result42 from (                                          
  select cm.CustomerName,SalesPerson,                                           
  (                                          
  CASE                                
  WHEN DATEDIFF(DD,OrderDate,GETDATE())<=30 THEN do.AmtDue else 0 end                                           
  )                                          
  as [0-30 Days],                                    (                                          
  CASE                                                                
  WHEN DATEDIFF(DD,OrderDate,GETDATE())between 31 and 60 THEN do.AmtDue else 0 end             
  )                                          
  as [31-60 Days],                                          
  (                                          
  CASE                                                                
  WHEN DATEDIFF(DD,OrderDate,GETDATE())between 61 and 90 THEN do.AmtDue else 0 end                                           
  )                                          
  as [61-90 Days]                                          
  ,                                          
  (                                           
  CASE                                                                
  WHEN DATEDIFF(DD,OrderDate,GETDATE())> 90 THEN do.AmtDue else 0 end                                           
  )                                          
  as [90-Above],                                          
  isnull((do.PayableAmount),0.00) as [Order Total],                                          
  isnull((do.AmtPaid),0.00) as TotalPaid,((isnull(do.PayableAmount,0.00)-isnull(do.AmtPaid,0.00))) as AmtDue                                          
  from  #OrderAutoid  as do1                                                                    
  inner join  DeliveredOrders as do on do1.AutoId=do.OrderAutoId              
  inner join  CustomerMaster as cm on cm.AutoId=do1.CustomerAutoId                                                     
  where                                             
  (isnull(do.AmtDue,0.00))> 0                                          
                                          
  ) as t                                           
  group by  CustomerName,SalesPerson                          
  SELECT * FROM #Result42                                
  WHERE @PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)                                           
  SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize=0 then COUNT(RowNumber) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Result42                                                      
  Select Sum(Days30) as Days30,Sum(Days60) as Days60,Sum(Days90) as Days90,Sum(Above90) as Above90,                                        
  Sum(NoofOrder) as NoofOrder,Sum(OrderTotal)as OrderTotal,sum(AmtDue) as AmtDue,sum(TotalPaid) as TotalPaid from  #Result42 
   Select FORMAT(GETDATE(),'MM/dd/yyyy hh:mm tt') as [PrintTime]
  END          
	
 END TRY                                                    
 BEGIN CATCH                                                                    
  Set @isException=1                                                                    
  Set @exceptionMessage=ERROR_MESSAGE()                                                                    
 END CATCH                                                                    
END 
