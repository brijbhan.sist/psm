USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_CityName]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION  [dbo].[FN_CityName]
(
	 @AutoId int
)
RETURNS varchar(200)
AS
BEGIN
	DECLARE @CityName  varchar(150)
	SET @CityName=(SELECT CityName FROM CityMaster WHERE AutoId=@AutoId)	
	RETURN @CityName
END
GO
