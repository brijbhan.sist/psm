USE [psmnj.a1whm.com]
GO

/****** Object:  UserDefinedTableType [dbo].[DT_dtbulkImageUrl]    Script Date: 11/13/2020 05:33:15 ******/
CREATE TYPE [dbo].[DT_dtbulkmessageImageUrl] AS TABLE(
	[URL] [varchar](max) NULL
)
GO
