USE [psmnj.a1whm.com]
GO

/****** Object:  UserDefinedTableType [dbo].[ProductAutoIdData]    Script Date: 10/28/2020 03:25:03 ******/
CREATE TYPE [dbo].[ProductAutoIdData] AS TABLE(
	[ProductAutoId] [int] NOT NULL
)
GO


