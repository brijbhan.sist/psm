USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcPSMCT_ProductSold]    Script Date: 12/1/2020 1:31:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE  [dbo].[ProcPSMCT_ProductSold]
	@FromDate datetime=null ,
	@ToDate datetime =null,
	@PageSize int=10,
	@PageIndex int=1,
	@isException bit=null out,
	@exceptionMessage varchar(max)=null out
  
AS
BEGIN
SET @isException=0
SET @exceptionMessage=''
BEGIN TRY
select *, (Net_Sold_Default_Qty * PSMCT_COST)PSMCT_NET_COST into #t1 from
(
	select 
		njpm.ProductId,njpm.ProductName,njum.UnitType,njpd.Qty
		,(case when isnull(pppl.CustomPrice,0) = 0 then njpd.CostPrice else CustomPrice end ) PSMCT_COST
			,convert(decimal(18,2),((t1.QtyDel)/njpd.Qty)) as [Sold_Default_Qty]					
			,convert(decimal(18,2),((t1.QtyDel)/njpd.Qty)) as [Net_Sold_Default_Qty]
			,(t1.NetPrice  ) as Net_Sale_Rev						
			,(t1.NetPrice ) as Net_Total_Sales	
	from 
		[psmnj.a1whm.com].dbo.ProductMaster as njpm
			inner join [psmnj.a1whm.com].dbo.PackingDetails as njpd on njpd.ProductAutoId=njpm.AutoId and njpd.UnitType = njpm.PackingAutoId
			inner join [psmnj.a1whm.com].dbo.UnitMaster as njum on njum.AutoId=njpd.UnitType
	left join
	(
			select  
					  pm.ProductId,   pm.PackingAutoId,pd.UnitType, pd.Qty			
					,sum(doi.QtyDel) as QtyDel
					,sum(doi.NetPrice) as NetPrice			
			from [psmct.a1whm.com].dbo.OrderMaster as om
				inner join [psmct.a1whm.com].dbo.Delivered_Order_Items as doi on doi.OrderAutoId=om.AutoId
				inner join [psmct.a1whm.com].dbo.ProductMaster as pm on pm.AutoId=doi.ProductAutoId
				inner join [psmct.a1whm.com].dbo.PackingDetails as pd on pd.ProductAutoId=pm.AutoId and pd.UnitType=pm.PackingAutoId			 
			where convert(date,OrderDate) between  @FromDate and @ToDate and om.Status=11 and om.OrderType=1
			group by  pm.ProductId , pd.UnitType, pd.Qty,  pm.PackingAutoId 
		) as t1  on njpm.ProductId=t1.ProductId and njpd.UnitType = t1.UnitType
 
	left join [psmnj.a1whm.com].dbo.ProductPricingInPriceLevel as pppl on 
																			pppl.ProductAutoId=njpm.AutoId
																		and pppl.UnitAutoId=t1.UnitType 
																		and pppl.PriceLevelAutoId=2838
																		 
) as t
 
----------------------------- 
-- EASYWHM
--------------------------------------------------
UNION ALL

select *, (Net_Sold_Default_Qty * PSMCT_COST)PSMCT_NET_COST  from
(
	select 
		njpm.ProductId,njpm.ProductName,njum.UnitType,njpd.Qty
		,(case when isnull(pppl.CustomPrice,0) = 0 then njpd.CostPrice else CustomPrice end ) PSMCT_COST
			,convert(decimal(18,2),((t1.QtyDel)/njpd.Qty)) as [Sold_Default_Qty]		
		 
			,convert(decimal(18,2),((t1.QtyDel )/njpd.Qty)) as [Net_Sold_Default_Qty]
			,(t1.NetPrice  ) as Net_Sale_Rev		
			 
			,(t1.NetPrice  ) as Net_Total_Sales	
	from 
		[psmnj.a1whm.com].dbo.ProductMaster as njpm
			inner join [psmnj.a1whm.com].dbo.PackingDetails as njpd on njpd.ProductAutoId=njpm.AutoId and njpd.UnitType = njpm.PackingAutoId
			inner join [psmnj.a1whm.com].dbo.UnitMaster as njum on njum.AutoId=njpd.UnitType
	left join
	(
			select  
					  pm.ProductId,   pm.PackingAutoId,pd.UnitType, pd.Qty			
					,sum(doi.QtyDel) as QtyDel
					,sum(doi.NetPrice) as NetPrice			
			from  [psmct.easywhm.com].dbo.OrderMaster as om
				inner join [psmct.easywhm.com].dbo.Delivered_Order_Items as doi on doi.OrderAutoId=om.AutoId
				inner join  [psmct.easywhm.com].dbo.ProductMaster as pm on pm.AutoId=doi.ProductAutoId
				inner join  [psmct.easywhm.com].dbo.PackingDetails as pd on pd.ProductAutoId=pm.AutoId and pd.UnitType=pm.PackingAutoId			 
			where convert(date,OrderDate) between  @FromDate and @ToDate and om.Status=11 and om.OrderType=1
			group by  pm.ProductId , pd.UnitType, pd.Qty,  pm.PackingAutoId 
		) as t1  on njpm.ProductId=t1.ProductId and njpd.UnitType = t1.UnitType
		 
	left join [psmnj.a1whm.com].dbo.ProductPricingInPriceLevel as pppl on 
																			pppl.ProductAutoId=njpm.AutoId
																		and pppl.UnitAutoId=t1.UnitType 
																		and pppl.PriceLevelAutoId=2838
																		 
) as tx
order by  ProductId

--------------------------
select row_Number() over(order by njcm.CategoryName,tr.ProductId) as RowNumber,
njcm.CategoryName[CategoryName]
, tr.ProductId
 ,tr.ProductName
 ,UnitType 
,PSMCT_COST [PSMCT_UNIT_COST]	 
,sum(Sold_Default_Qty)Sold_Default_Qty
,sum(Net_Sold_Default_Qty)Net_Sold_Default_Qty

,sum(PSMCT_NET_COST)PSMCT_NET_COST
,sum(Net_Sale_Rev)Net_Sale_Rev
 
,sum(Net_Total_Sales)Net_Total_Sales 


,sum((Net_Total_Sales-PSMCT_NET_COST))[Profit]

into #result
 from #t1 as tr
 inner join [psmnj.a1whm.com].dbo.ProductMaster as njpm on njpm.ProductId=tr.[ProductId]
 inner join [psmnj.a1whm.com].dbo.CategoryMaster as njcm on njcm.AutoId=njpm.CategoryAutoId
 where Net_Sold_Default_Qty >0 

group by  tr.ProductId,tr.ProductName,UnitType,PSMCT_COST ,njcm.CategoryName

 order by njcm.CategoryName,ProductId
 
SELECT case when @PageSize=0 then 0 else COUNT(*) end as RecordCount,@PageSize as PageSize,@PageIndex as PageIndex FROM #result  
SELECT * FROM #result  
WHERE (@PageSize=0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))
SELECT ISNULL((SUM(Sold_Default_Qty)),0) as Sold_Default_Qty,
ISNULL((SUM(Net_Sold_Default_Qty)),0) as Net_Sold_Default_Qty,ISNULL((SUM(PSMCT_NET_COST)),0) as PSMCT_NET_COST,
ISNULL(SUM(Net_Sale_Rev),0) as Net_Sale_Rev,ISNULL(SUM(Net_Total_Sales),0) as Net_Total_Sales,ISNULL(SUM([Profit]),0) as [Profit]
from #result

 drop table #t1
 

END TRY
BEGIN CATCH

SET @isException=1
SET @exceptionMessage='false'
END CATCH
END 