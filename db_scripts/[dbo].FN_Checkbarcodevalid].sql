CREATE OR ALTER   function [dbo].[FN_Checkbarcodevalid]
(@barcode varchar(13))
Returns varchar(13)
BEGIN
declare @i int=1,@Count int=len(@barcode)-1,@Status int
declare @num int=0,@Even int=0,@Odd int=0,@lastDigit int
WHILE @i <= @Count                                                          
BEGIN   
	SET @num=(Select SUBSTRING(@Barcode, @i, 1))
	If(ISNULL(@i,0)%2=0)
	BEGIN
		SET @Even=ISNULL(@Even,0)+ISNULL(@num,0);
	END
	ELSE
	BEGIN
		SET @Odd=ISNULL(@Odd,0)+ISNULL(@num,0);
	END
	SET @i=@i+1  
END 
	SET @lastDigit=@Even+(@Odd*3);
	SET @lastDigit=@lastDigit%10
	SET @lastDigit=10-@lastDigit
	if(@lastDigit=SUBSTRING(@Barcode, len(@Barcode), 1))
	BEGIN
	 SET @Status=0
	END
	ELSE 
	BEGIN
	 SET @Status=1
	END
	return @Status
 END