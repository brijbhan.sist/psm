alter PROCEDURE [dbo].[ProductWiseCommissionReport]          
@Opcode INT=NULL,            
@SalesPersonName varchar (50)=NULL,            
@ProductId varchar (50)=NULL,            
@ProductName varchar (50)=NULL,            
@SalesPersonAutoId int = null,            
@CommissionCode varchar (10) = null,            
@DateFrom date =NULL,            
@DateTo date =NULL,            
@PageIndex INT=1,            
@PageSize INT=10,            
@RecordCount INT=null,            
@isException bit out,            
@exceptionMessage varchar(max) out                       
AS                            
BEGIN             
 BEGIN TRY            
  Set @isException=0            
  Set @exceptionMessage='Success'                   
            
if @Opcode = 41            
begin            
select ROW_NUMBER() OVER(ORDER BY SalesPerson) AS RowNumber,SalesPerson,ProductId,ProductName,UnitType,
(SELECT C_DisplayName FROM Tbl_CommissionMaster AS CM WHERE T.CommCode=CM.CommissionCode) AS CommCode,NoofProduct AS [SoldQty],        
TotalSale as [SoldAmount],SPCommAmt as [SoldCommissionAmount],ISNULL(ReturnNoofProduct,0) AS [CreditQty],ISNULL(ReturnAmount,0) as [CreditAmount],        
ISNULL(ReturnSPCommAmt,0) as [CreditCommissionAmount],(NoofProduct-ISNULL(ReturnNoofProduct,0)) as [ActualSoldQty],        
(TotalSale-isnull(ReturnAmount,0)) as [ActualSoldAmount],(SPCommAmt-ISNULL(ReturnSPCommAmt,0)) as [ActualCommissionAmount] into #Results41 from (        
SELECT   (em.FirstName+' '+em.LastName) as [SalesPerson],pm.ProductId,        
   ProductName,um.UnitType,doim.UnitAutoId, om.SalesPersonAutoId,           
   SUM(doim.QtyDel) AS NoofProduct,0 as ReturnNoofProduct ,isNull(pm.P_CommCode,0) as CommCode,               
   ISNULL(sum(doim.NetPrice),0) as TotalSale,0 as  ReturnAmount,             
   CAST((sum(doim.NetPrice)*isNull (pm.P_CommCode,0)) AS DECIMAL(10,2)) as SPCommAmt,0 as ReturnSPCommAmt              
   from Delivered_Order_Items as doim              
   inner join ProductMaster as pm on pm.AutoId = doim.ProductAutoId        
   inner join UnitMaster as um on um.AutoId=doim.UnitAutoId             
   inner join OrderMaster as om on om.AutoId=doim.OrderAutoId and om.Status=11        
   inner join CustomerMaster as cm on cm.AutoId = om.CustomerAutoId              
   inner join EmployeeMaster as em on em.AutoId=om.SalesPersonAutoId         
   where         
   (@DateFrom is null or @DateFrom = '' or @DateTo is null or @DateTo = '' or                   
   (CONVERT(date,OM.OrderDate)  between CONVERT(date,@DateFrom) and CONVERT(date,@DateTo)))         
   and (@SalesPersonAutoId is null or @SalesPersonAutoId = 0 or om.SalesPersonAutoId =  @SalesPersonAutoId )        
   AND (@CommissionCode is null or @CommissionCode = ''  or pm.P_CommCode like '%' +@CommissionCode+ '%' )           
   and (@ProductId is null or @ProductId='' or ProductId like '%'+ @ProductId +'%')            
   and (@ProductName is null or @ProductName='' or ProductName like '%'+ @ProductName +'%')          
   group by isNull(pm.P_CommCode,0), em.FirstName+' '+em.LastName,om.SalesPersonAutoId ,pm.ProductId,ProductName , um.UnitType,doim.UnitAutoId          
           
        
   union         
        
   select  (em.FirstName+' '+em.LastName) as SalesPerson,pm.ProductId,ProductName,um.UnitType,doim.UnitAutoId,om.SalesPersonAutoId,        
   0 as NoofProduct,sum(convert(int,doim.TotalPeice)) AS ReturnNoofProduct ,isNull(pm.P_CommCode,0) as CommCode,           
   0 as TotalSale,ISNULL(sum(doim.NetAmount),0) as ReturnAmount,           
   0 as SPCommAmt,CAST((sum(doim.NetAmount)*isNull (pm.P_CommCode,0)) AS DECIMAL(10,2)) as ReturnSPCommAmt          
   from CreditItemMaster as doim            
   inner join productmaster as pm on pm.autoid=doim.productautoid         
   inner join UnitMaster as um on um.AutoId=doim.UnitAutoId             
   inner join CreditMemoMaster as cmm on cmm.CreditAutoId=doim.CreditAutoId        
   inner join OrderMaster as om on om.AutoId=cmm.OrderAutoId and om.status=11        
   inner join CustomerMaster as cm on cm.AutoId = om.CustomerAutoId          
   inner join EmployeeMaster as em on em.AutoId=om.SalesPersonAutoId          
   where  (@DateFrom is null or @DateFrom = '' or @DateTo is null or @DateTo = '' or                   
   (CONVERT(date,om.OrderDate)  between CONVERT(date,@DateFrom) and CONVERT(date,@DateTo)))         
   and (@SalesPersonAutoId is null or @SalesPersonAutoId = 0 or om.SalesPersonAutoId =  @SalesPersonAutoId )        
   AND (@CommissionCode is null or @CommissionCode = ''  or pm.P_CommCode like '%' +@CommissionCode+ '%' )           
   and (@ProductId is null or @ProductId='' or ProductId like '%'+ @ProductId +'%')            
   and (@ProductName is null or @ProductName='' or ProductName like '%'+ @ProductName +'%')        
   group by isNull(pm.P_CommCode,0), em.FirstName+' '+em.LastName,om.SalesPersonAutoId,pm.ProductId,ProductName,um.UnitType,doim.UnitAutoId        
   ) as t        
  
   SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize=0 then COUNT(RowNumber) else @PageSize end AS PageSize, @PageIndex AS PageIndex,Format(getdate(),'MM/dd/yyyy hh:mm tt') as PrintDate FROM #Results41            
                
   SELECT * FROM #Results41            
   WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))       
       
   SELECT ISNULL(SUM(SoldAmount),0.00) AS SoldAmount,ISNULL(SUM(ActualSoldAmount),0.00) AS ActualSoldAmount,ISNULL(SUM(SoldCommissionAmount),0.00) AS SoldCommissionAmount, isnull(Sum(SoldQty),0) as SoldQty    
   ,ISNULL(SUM(CreditAmount),0.00) AS CreditAmount,ISNULL(SUM(ActualCommissionAmount),0.00) AS ActualCommissionAmount,isnull(Sum(ActualSoldQty),0) as ActualSoldQty,isnull(Sum(CreditQty),0) as CreditQty,    
   ISNULL(SUM(ActualSoldAmount),0.00) AS ActualSoldAmount,ISNULL(SUM(CreditCommissionAmount),0.00) AS CreditCommissionAmount    
    from  #Results41          
end            
if @Opcode = 42            
   begin
   select 
   (
   select em.AutoId as SID, FirstName + ' ' + LastName as SP from EmployeeMaster as em            
   inner join EmployeeTypeMaster etm on etm.AutoId = em.EmpType            
   Where etm.AutoId = 2   order by SP ASC      
   for json path
   )as SalesList,
   (
   select distinct ISNULL(P_CommCode,0) as CCode,cm.C_DisplayName as CD from ProductMaster as pm
   inner join Tbl_CommissionMaster as cm on pm.P_CommCode=cm.CommissionCode
   order by ISNULL(P_CommCode,0) 
   for json path
   )as CoCode
   for json path
 end            
                         
END TRY            
 BEGIN CATCH            
  Set @isException=1            
  Set @exceptionMessage=ERROR_MESSAGE()            
 END CATCH                       
END     
  
GO
