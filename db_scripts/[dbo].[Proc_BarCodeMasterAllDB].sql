ALTER PROCEDURE [dbo].[Proc_BarCodeMaster_ALLDB]             
   @ProductId VARCHAR(50)=NULL,
   @UnitAutoId int null,
   @Barcode varchar(250) null         
AS            
 BEGIN           
 IF(DB_NAME() IN ('psmct.a1whm.com','psmpa.a1whm.com','psmnpa.a1whm.com','psmnj.a1whm.com','psmwpa.a1whm.com'))        
 BEGIN       
   DECLARE @AutoId INT     
   --For CT DB     
   IF NOT EXISTS(SELECT Barcode FROM [psmCT.a1whm.com].[dbo].[ItemBarcode] WHERE Barcode=@Barcode)      
   BEGIN      
		set @AutoId=(select AutoId from [psmCT.a1whm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId)        
		insert into [psmCT.a1whm.com].[dbo].[ItemBarcode](ProductAutoId,UnitAutoId,Barcode)        
		SELECT @AutoId,@UnitAutoId,@Barcode      
   END      
   --For PA DB    
   IF NOT EXISTS(SELECT Barcode FROM [psmpa.a1whm.com].[dbo].[ItemBarcode] WHERE Barcode=@Barcode)      
   BEGIN      
		SET @AutoId=(select AutoId from [psmpa.a1whm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId)        
		insert into [psmpa.a1whm.com].[dbo].[ItemBarcode](ProductAutoId,UnitAutoId,Barcode)        
		SELECT @AutoId,@UnitAutoId,@Barcode      
   END      
   --For NJ DB    
   IF NOT EXISTS(SELECT Barcode FROM [psmnj.a1whm.com].[dbo].[ItemBarcode] WHERE Barcode=@Barcode)      
   BEGIN      
		SET @AutoId=(select AutoId from [psmnj.a1whm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId)        
		insert into [psmnj.a1whm.com].[dbo].[ItemBarcode](ProductAutoId,UnitAutoId,Barcode)        
		SELECT @AutoId,@UnitAutoId,@Barcode      
   END      
   --For NPA DB    
   IF NOT EXISTS(SELECT Barcode FROM [psmnpa.a1whm.com].[dbo].[ItemBarcode] WHERE Barcode=@Barcode)      
   BEGIN      
		SET @AutoId=(select AutoId from [psmnpa.a1whm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId)        
		insert into [psmnpa.a1whm.com].[dbo].[ItemBarcode](ProductAutoId,UnitAutoId,Barcode)        
		SELECT @AutoId,@UnitAutoId,@Barcode      
   END
   IF NOT EXISTS(SELECT Barcode FROM [psmwpa.a1whm.com].[dbo].[ItemBarcode] WHERE Barcode=@Barcode)      
   BEGIN      
		SET @AutoId=(select AutoId from [psmwpa.a1whm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId)        
		insert into [psmwpa.a1whm.com].[dbo].[ItemBarcode](ProductAutoId,UnitAutoId,Barcode)        
		SELECT @AutoId,@UnitAutoId,@Barcode      
   END      
 END       
 ELSE      
 BEGIN      
	IF NOT EXISTS(SELECT Barcode FROM [dbo].[ItemBarcode] WHERE Barcode=@Barcode)      
	BEGIN      
		SET @AutoId=(select AutoId from [dbo].[ProductMaster] WHERE ProductId = @ProductId)        
		insert into .[dbo].[ItemBarcode](ProductAutoId,UnitAutoId,Barcode)        
		SELECT @AutoId,@UnitAutoId,@Barcode      
	END      
 END       
 END 