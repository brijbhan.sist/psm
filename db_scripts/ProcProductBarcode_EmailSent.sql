CREATE OR ALTER PROCEDURE [dbo].[ProcProductBarcode_EmailSent]  
AS        
BEGIN   
 
	Declare @html varchar(max)='',@tr varchar(max)=''

	 
	     set @html='<table class="table tableCSS" border="1" style="width:100%;border: 1px solid black;border-collapse:collapse;">
		<thead>
			<tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
				<td style="text-align: center !important;">
					<h4 style="margin:10px;">Product/Barcode</h4>
				</td>
				</tr>
				<tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
				<td style="text-align: center !important;">
					<h4 style="margin:2px">'+(select CompanyDistributerName from CompanyDetails)+'</h4>
				</td>
				</tr>
				<tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
				<td style="text-align: center !important;">
					<h4 style="margin:2px">'+Convert(varchar(50),FORMAT(GetDate(),'MM/dd/yyyy hh:mm tt'))+'</h4>
				</td>
			</tr>
		</thead>
		<tbody></tbody>
		</table>
		<br>
		<table class="table tableCSS" border="1" style="width:100%;border: 1px solid black;border-collapse:collapse;" 
		id="tblNotShippedReportByHeader"> 
		<thead>
		<tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
			<td style="text-align: center !important;" class="text-center">Count</td>
			<td style="text-align: center !important;" class="text-center">PSMNJ</td>
			<td style="text-align: center !important;" class="text-center">PSMNY</td>
			<td style="text-align: center !important;" class="text-center">PSMCT</td>
			<td style="text-align: center !important;" class="text-center">PSMNPA</td>
			<td style="text-align: center !important;" class="text-center">PSMPA</td>
			<td style="text-align: center !important;" class="text-center">PSMWPA</td> 
		</tr>' +   
		'</thead> <tbody>' ;
		
		declare @PSMNJ int,	@PSMNY int	 ,@PSMCT int,@PSMNPA int,@PSMPA int,@PSMWPA int

		select 
		@PSMNJ=(select COUNT(1) from [psmnj.a1whm.com].[dbo].ProductMaster),
		@PSMNY=(select COUNT(1) from [psmny.a1whm.com].[dbo].ProductMaster),
		@PSMCT=(select COUNT(1) from [psmct.a1whm.com].[dbo].ProductMaster),
		@PSMNPA=(select COUNT(1) from [psmnpa.a1whm.com].[dbo].ProductMaster),
		@PSMPA=(select COUNT(1) from [psmpa.a1whm.com].[dbo].ProductMaster),
		@PSMWPA=(select COUNT(1) from [psmwpa.a1whm.com].[dbo].ProductMaster)


		set @tr ='<tr>' 
		+	'<td style=''text-align:center;''>Product</td>' 
		+	'<td style=''text-align:center;''>'+convert(varchar(50), @PSMNJ) + '</td>' 
		+	'<td style=''text-align:center;''>'+ convert(varchar(50), @PSMNY) + '</td>'
		+	'<td style=''text-align:center;''>'+convert(varchar(50), @PSMCT) + '</td>' 
		+	'<td style=''text-align:center;''>'+ convert(varchar(50), @PSMNPA) + '</td>' 
		+	'<td style=''text-align:center;''>'+ convert(varchar(50), @PSMPA) + '</td>'
		+	'<td style=''text-align:center;''>'+convert(varchar(50), @PSMWPA) + '</td>'  
		+ '</tr>' ;
		SET @html=@html+@tr;

		select 
		@PSMNJ=(select COUNT(1) from [psmnj.a1whm.com].[dbo].ItemBarcode),
		@PSMNY=(select COUNT(1) from [psmny.a1whm.com].[dbo].ItemBarcode),
		@PSMCT=(select COUNT(1) from [psmct.a1whm.com].[dbo].ItemBarcode),
		@PSMNPA=(select COUNT(1) from [psmnpa.a1whm.com].[dbo].ItemBarcode),
		@PSMPA=(select COUNT(1) from [psmpa.a1whm.com].[dbo].ItemBarcode),
		@PSMWPA=(select COUNT(1) from [psmwpa.a1whm.com].[dbo].ItemBarcode)


		set @tr ='<tr>' 
		+	'<td style=''text-align:center;''>Barcode</td>' 
		+	'<td style=''text-align:center;''>'+convert(varchar(50), @PSMNJ) + '</td>' 
		+	'<td style=''text-align:center;''>'+ convert(varchar(50), @PSMNY) + '</td>'
		+	'<td style=''text-align:center;''>'+convert(varchar(50), @PSMCT) + '</td>' 
		+	'<td style=''text-align:center;''>'+ convert(varchar(50), @PSMNPA) + '</td>' 
		+	'<td style=''text-align:center;''>'+ convert(varchar(50), @PSMPA) + '</td>'
		+	'<td style=''text-align:center;''>'+convert(varchar(50), @PSMWPA) + '</td>'  
		+ '</tr>' ;
		SET @html=@html+@tr;

		select 
		@PSMNJ=(select COUNT(1) from [psmnj.a1whm.com].[dbo].PackingDetails),
		@PSMNY=(select COUNT(1) from [psmny.a1whm.com].[dbo].PackingDetails),
		@PSMCT=(select COUNT(1) from [psmct.a1whm.com].[dbo].PackingDetails),
		@PSMNPA=(select COUNT(1) from [psmnpa.a1whm.com].[dbo].PackingDetails),
		@PSMPA=(select COUNT(1) from [psmpa.a1whm.com].[dbo].PackingDetails),
		@PSMWPA=(select COUNT(1) from [psmwpa.a1whm.com].[dbo].PackingDetails)


		set @tr ='<tr>' 
		+	'<td style=''text-align:center;''>Packing Details</td>' 
		+	'<td style=''text-align:center;''>'+convert(varchar(50), @PSMNJ) + '</td>' 
		+	'<td style=''text-align:center;''>'+ convert(varchar(50), @PSMNY) + '</td>'
		+	'<td style=''text-align:center;''>'+convert(varchar(50), @PSMCT) + '</td>' 
		+	'<td style=''text-align:center;''>'+ convert(varchar(50), @PSMNPA) + '</td>' 
		+	'<td style=''text-align:center;''>'+ convert(varchar(50), @PSMPA) + '</td>'
		+	'<td style=''text-align:center;''>'+convert(varchar(50), @PSMWPA) + '</td>'  
		+ '</tr>' ;
			 
	    SET @html=@html+@tr;
	
	--EXEC dbo.spWriteToFile @DriveLocation, @html 

	--------------Email Code---------------
	Declare  @Subject varchar(max),@FromName varchar(1000),@FromEmailId varchar(1000),@Port int,@SMTPServer varchar(1000),@Password varchar(1000),@SSL  int,
	@BCCEmailId varchar(max),@MigrateEmailId varchar(1000),@CCEmailId varchar(max),@smtp_userName varchar(max)
	set @Subject = 'Barcode Report ' +UPPER((select CompanyDistributerName from dbo.CompanyDetails))+' : ' + 
	format(GETDATE(),'MM/dd/yyy hh:mm tt');

	SELECT @FromEmailId=FromEmail,@FromName=DisplayName,@smtp_userName=SMTP_USERNAME,@Password=SMTP_PASSWORD,
	@SMTPServer=HOST,@Port=SMTPPORT,@SSL=EnableSsl from EmailServerMaster_AWS

	set @BCCEmailId=(select top 1 BCCEmail from [dbo].[Tbl_ReportEmailReceiver] where autoid=4);
	set @MigrateEmailId=(select top 1 ToEmail from [dbo].[Tbl_ReportEmailReceiver] where autoid=4)
	SET @CCEmailId=(select top 1 CCEmail from [dbo].[Tbl_ReportEmailReceiver] where autoid=4)
   
		 EXEC [Emailing_DB].[dbo].[ProcSubmitEmail] 
			@Opcode=11,
			@FromEmailId =@FromEmailId,
			@FromName = @FromName,
			@smtp_userName=@FromEmailId,
			@Password = @Password,
			@SMTPServer = @SMTPServer,
			@Port =@Port,
			@SSL =@SSL,
			@ToEmailId =@MigrateEmailId,
			@CCEmailId =@CCEmailId,
			@BCCEmailId =@BCCEmailId,  
			@Subject =@Subject,
			@EmailBody = @html,
			@SentDate ='',
			@Status =0,
			@SourceApp ='PSM',
			@SubUrl ='Barcode report', 
			@isException=0,
			@exceptionMessage=''  
END