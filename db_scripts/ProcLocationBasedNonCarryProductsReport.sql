create or alter PROCEDURE [dbo].[ProcLocationBasedNonCarryProductsReport]                        
@Opcode INT=NULL,                    
@ProductId varchar(20)=null,
@ProductName varchar(100)=null,
@Month int=null,
@CategoryAutoId int=0,
@SubCategoryAutoId int=0,
@BrandAutoId int=0,
@OrderAutoIds varchar(max)='',
@Location int=0,
@PageIndex INT=1,                    
@PageSize INT=10,                    
@RecordCount INT=null,                    
@isException bit out,                    
@exceptionMessage varchar(max) out                            
AS                                    
BEGIN                       
BEGIN TRY                    
	Set @isException=0                    
	Set @exceptionMessage='Success'                    
  if(@Opcode=41)                    
	begin    
	    Declare @LiveDate date
		SET @LiveDate=(Select LiveDate from CompanyDetails)

		Select ROW_NUMBER() over(order by ProductId asc) as RowNumber,* into #Result    from ( 
			select PM.AutoId,ProductId,ProductName,CM.CategoryName,SCM.SubcategoryName,BM.BrandName,Format(PM.CreateDate,'MM/dd/yyyy') as CreateDate,
			FORMAT((
			select max(orderdate) from [psmnj.a1whm.com].[dbo].OrderMaster as om 
			inner join [psmnj.a1whm.com].[dbo].Delivered_Order_Items as do on do.OrderAutoId=om.AutoId
			where om.Status=11 and do.ProductAutoId=pm.AutoId
			),'MM/dd/yyyy') as lastdate,PM.ProductStatus,'PSMNJ' as Location,1 as LocationAutoId
			from [psmnj.a1whm.com].[dbo].ProductMaster as PM
			inner join [psmnj.a1whm.com].[dbo].CategoryMaster CM on CM.AutoId=PM.CategoryAutoId  
			inner join [psmnj.a1whm.com].[dbo].SubCategoryMaster SCM on SCM.AutoId=PM.SubcategoryAutoId  
			left join [psmnj.a1whm.com].[dbo].BrandMaster BM on BM.AutoId=PM.BrandAutoId  
			where pm.AutoId not in (
			select distinct OIM.ProductAutoId from  [psmnj.a1whm.com].[dbo].OrderMaster  as OM
			inner join [psmnj.a1whm.com].[dbo].Delivered_Order_Items  as OIM on OM.AutoId=OIM.OrderAutoId
			where  convert(date,OrderDate)>=(case when @Month=0 then CONVERT(date,@LiveDate) else convert(date,GETDATE()-@Month*30) end)
			and om.Status=11
			) and pm.ProductStatus=1 
			and (@ProductName is null OR @ProductName='' OR ProductId like '%'+@ProductName+'%' or ProductName like '%'+@ProductName+'%' )
			and (@CategoryAutoId=0 or @CategoryAutoId is null or PM.CategoryAutoId=@CategoryAutoId)  
			and (@SubCategoryAutoId=0 or @SubCategoryAutoId is null or PM.SubcategoryAutoId=@SubCategoryAutoId)  
			and (@BrandAutoId=0 or @BrandAutoId is null or PM.BrandAutoId=@BrandAutoId)  
			and (@Location=0 or 1=@Location)

			UNION All

			select PM.AutoId,ProductId,ProductName,CM.CategoryName,SCM.SubcategoryName,BM.BrandName,Format(PM.CreateDate,'MM/dd/yyyy') as CreateDate,
			FORMAT((
			select max(orderdate) from [psmct.a1whm.com].[dbo].OrderMaster as om 
			inner join [psmct.a1whm.com].[dbo].Delivered_Order_Items as do on do.OrderAutoId=om.AutoId
			where om.Status=11 and do.ProductAutoId=pm.AutoId
			),'MM/dd/yyyy') as lastdate,PM.ProductStatus,'PSMCT' as Location,2 as LocationAutoId
			from [psmct.a1whm.com].[dbo].ProductMaster as PM
			inner join [psmct.a1whm.com].[dbo].CategoryMaster CM on CM.AutoId=PM.CategoryAutoId  
			inner join [psmct.a1whm.com].[dbo].SubCategoryMaster SCM on SCM.AutoId=PM.SubcategoryAutoId  
			left join [psmct.a1whm.com].[dbo].BrandMaster BM on BM.AutoId=PM.BrandAutoId  
			where pm.AutoId not in (
			select distinct OIM.ProductAutoId from  [psmct.a1whm.com].[dbo].OrderMaster  as OM
			inner join [psmct.a1whm.com].[dbo].Delivered_Order_Items  as OIM on OM.AutoId=OIM.OrderAutoId
			where  convert(date,OrderDate)>=(case when @Month=0 then CONVERT(date,@LiveDate) else convert(date,GETDATE()-@Month*30) end)
			and om.Status=11
			) and pm.ProductStatus=1 
			and (@ProductName is null OR @ProductName='' OR ProductId like '%'+@ProductName+'%' or ProductName like '%'+@ProductName+'%' )
			and (@CategoryAutoId=0 or @CategoryAutoId is null or PM.CategoryAutoId=@CategoryAutoId)  
			and (@SubCategoryAutoId=0 or @SubCategoryAutoId is null or PM.SubcategoryAutoId=@SubCategoryAutoId)  
			and (@BrandAutoId=0 or @BrandAutoId is null or PM.BrandAutoId=@BrandAutoId)  
			and (@Location=0 or 2=@Location)

			UNION All

			select PM.AutoId,ProductId,ProductName,CM.CategoryName,SCM.SubcategoryName,BM.BrandName,Format(PM.CreateDate,'MM/dd/yyyy') as CreateDate,
			FORMAT((
			select max(orderdate) from [psmpa.a1whm.com].[dbo].OrderMaster as om 
			inner join [psmpa.a1whm.com].[dbo].Delivered_Order_Items as do on do.OrderAutoId=om.AutoId
			where om.Status=11 and do.ProductAutoId=pm.AutoId
			),'MM/dd/yyyy') as lastdate,PM.ProductStatus,'PSMPA' as Location,4 as LocationAutoId
			from [psmpa.a1whm.com].[dbo].ProductMaster as PM
			inner join [psmpa.a1whm.com].[dbo].CategoryMaster CM on CM.AutoId=PM.CategoryAutoId  
			inner join [psmpa.a1whm.com].[dbo].SubCategoryMaster SCM on SCM.AutoId=PM.SubcategoryAutoId  
			left join [psmpa.a1whm.com].[dbo].BrandMaster BM on BM.AutoId=PM.BrandAutoId  
			where pm.AutoId not in (
			select distinct OIM.ProductAutoId from  [psmpa.a1whm.com].[dbo].OrderMaster  as OM
			inner join [psmpa.a1whm.com].[dbo].Delivered_Order_Items  as OIM on OM.AutoId=OIM.OrderAutoId
			where  convert(date,OrderDate)>=(case when @Month=0 then CONVERT(date,@LiveDate) else convert(date,GETDATE()-@Month*30) end)
			and om.Status=11
			) and pm.ProductStatus=1 
			and (@ProductName is null OR @ProductName='' OR ProductId like '%'+@ProductName+'%' or ProductName like '%'+@ProductName+'%' )
			and (@CategoryAutoId=0 or @CategoryAutoId is null or PM.CategoryAutoId=@CategoryAutoId)  
			and (@SubCategoryAutoId=0 or @SubCategoryAutoId is null or PM.SubcategoryAutoId=@SubCategoryAutoId)  
			and (@BrandAutoId=0 or @BrandAutoId is null or PM.BrandAutoId=@BrandAutoId)  
			and (@Location=0 or 4=@Location)

			UNION All

			select PM.AutoId,ProductId,ProductName,CM.CategoryName,SCM.SubcategoryName,BM.BrandName,Format(PM.CreateDate,'MM/dd/yyyy') as CreateDate,
			FORMAT((
			select max(orderdate) from [psmnpa.a1whm.com].[dbo].OrderMaster as om 
			inner join [psmnpa.a1whm.com].[dbo].Delivered_Order_Items as do on do.OrderAutoId=om.AutoId
			where om.Status=11 and do.ProductAutoId=pm.AutoId
			),'MM/dd/yyyy') as lastdate,PM.ProductStatus,'PSMNPA' as Location,5 as LocationAutoId
			from [psmnpa.a1whm.com].[dbo].ProductMaster as PM
			inner join [psmnpa.a1whm.com].[dbo].CategoryMaster CM on CM.AutoId=PM.CategoryAutoId  
			inner join [psmnpa.a1whm.com].[dbo].SubCategoryMaster SCM on SCM.AutoId=PM.SubcategoryAutoId  
			left join [psmnpa.a1whm.com].[dbo].BrandMaster BM on BM.AutoId=PM.BrandAutoId  
			where pm.AutoId not in (
			select distinct OIM.ProductAutoId from  [psmnpa.a1whm.com].[dbo].OrderMaster  as OM
			inner join [psmnpa.a1whm.com].[dbo].Delivered_Order_Items  as OIM on OM.AutoId=OIM.OrderAutoId
			where  convert(date,OrderDate)>=(case when @Month=0 then CONVERT(date,@LiveDate) else convert(date,GETDATE()-@Month*30) end)
			and om.Status=11
			) and pm.ProductStatus=1 
			and (@ProductName is null OR @ProductName='' OR ProductId like '%'+@ProductName+'%' or ProductName like '%'+@ProductName+'%' )
			and (@CategoryAutoId=0 or @CategoryAutoId is null or PM.CategoryAutoId=@CategoryAutoId)  
			and (@SubCategoryAutoId=0 or @SubCategoryAutoId is null or PM.SubcategoryAutoId=@SubCategoryAutoId)  
			and (@BrandAutoId=0 or @BrandAutoId is null or PM.BrandAutoId=@BrandAutoId)  
			and (@Location=0 or 5=@Location)

			UNION All

			select PM.AutoId,ProductId,ProductName,CM.CategoryName,SCM.SubcategoryName,BM.BrandName,Format(PM.CreateDate,'MM/dd/yyyy') as CreateDate,
			FORMAT((
			select max(orderdate) from [psmwpa.a1whm.com].[dbo].OrderMaster as om 
			inner join [psmwpa.a1whm.com].[dbo].Delivered_Order_Items as do on do.OrderAutoId=om.AutoId
			where om.Status=11 and do.ProductAutoId=pm.AutoId
			),'MM/dd/yyyy') as lastdate,PM.ProductStatus,'PSMWPA' as Location,9 as LocationAutoId
			from [psmwpa.a1whm.com].[dbo].ProductMaster as PM
			inner join [psmwpa.a1whm.com].[dbo].CategoryMaster CM on CM.AutoId=PM.CategoryAutoId  
			inner join [psmwpa.a1whm.com].[dbo].SubCategoryMaster SCM on SCM.AutoId=PM.SubcategoryAutoId  
			left join [psmwpa.a1whm.com].[dbo].BrandMaster BM on BM.AutoId=PM.BrandAutoId  
			where pm.AutoId not in (
			select distinct OIM.ProductAutoId from  [psmwpa.a1whm.com].[dbo].OrderMaster  as OM
			inner join [psmwpa.a1whm.com].[dbo].Delivered_Order_Items  as OIM on OM.AutoId=OIM.OrderAutoId
			where  convert(date,OrderDate)>=(case when @Month=0 then CONVERT(date,@LiveDate) else convert(date,GETDATE()-@Month*30) end)
			and om.Status=11
			) and pm.ProductStatus=1 
			and (@ProductName is null OR @ProductName='' OR ProductId like '%'+@ProductName+'%' or ProductName like '%'+@ProductName+'%' )
			and (@CategoryAutoId=0 or @CategoryAutoId is null or PM.CategoryAutoId=@CategoryAutoId)  
			and (@SubCategoryAutoId=0 or @SubCategoryAutoId is null or PM.SubcategoryAutoId=@SubCategoryAutoId)  
			and (@BrandAutoId=0 or @BrandAutoId is null or PM.BrandAutoId=@BrandAutoId)  
			and (@Location=0 or 9=@Location)

			UNION All

			select PM.AutoId,ProductId,ProductName,CM.CategoryName,SCM.SubcategoryName,BM.BrandName,Format(PM.CreateDate,'MM/dd/yyyy') as CreateDate,
			FORMAT((
			select max(orderdate) from [psmny.a1whm.com].[dbo].OrderMaster as om 
			inner join [psmny.a1whm.com].[dbo].Delivered_Order_Items as do on do.OrderAutoId=om.AutoId
			where om.Status=11 and do.ProductAutoId=pm.AutoId
			),'MM/dd/yyyy') as lastdate,PM.ProductStatus,'PSMNY' as Location,10 as LocationAutoId
			from [psmny.a1whm.com].[dbo].ProductMaster as PM
			inner join [psmny.a1whm.com].[dbo].CategoryMaster CM on CM.AutoId=PM.CategoryAutoId  
			inner join [psmny.a1whm.com].[dbo].SubCategoryMaster SCM on SCM.AutoId=PM.SubcategoryAutoId  
			left join [psmny.a1whm.com].[dbo].BrandMaster BM on BM.AutoId=PM.BrandAutoId  
			where pm.AutoId not in (
			select distinct OIM.ProductAutoId from  [psmny.a1whm.com].[dbo].OrderMaster  as OM
			inner join [psmny.a1whm.com].[dbo].Delivered_Order_Items  as OIM on OM.AutoId=OIM.OrderAutoId
			where  convert(date,OrderDate)>=(case when @Month=0 then CONVERT(date,@LiveDate) else convert(date,GETDATE()-@Month*30) end)
			and om.Status=11
			) and pm.ProductStatus=1 
			and (@ProductName is null OR @ProductName='' OR ProductId like '%'+@ProductName+'%' or ProductName like '%'+@ProductName+'%' )
			and (@CategoryAutoId=0 or @CategoryAutoId is null or PM.CategoryAutoId=@CategoryAutoId)  
			and (@SubCategoryAutoId=0 or @SubCategoryAutoId is null or PM.SubcategoryAutoId=@SubCategoryAutoId)  
			and (@BrandAutoId=0 or @BrandAutoId is null or PM.BrandAutoId=@BrandAutoId)
			and (@Location=0 or 10=@Location)

		) as t

		Select COUNT(ProductId) as TotalRecord FROM #Result
                                    
		SELECT * FROM #Result                  
		WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))   order by ProductId asc
		
		SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize = 0 then COUNT(RowNumber) else @PageSize end AS PageSize,
		@PageIndex AS PageIndex,Format(GETDATE(),'MM/dd/yyyy hh:mm tt') as PrintDate FROM #Result 
	end 
	If @Opcode=42                                                                                                           
	BEGIN  
		select  
		(  
		select AutoId,CategoryName from CategoryMaster order by CategoryName asc  
		for json Path  
		) as Category,  
		(  
		select AutoId,BrandName from BrandMaster  order by BrandName asc  for json Path  
		) as Brand,
		(
		select AutoId,Location from LocationMaster order by AutoId asc  for json Path 
		) as Location
		for json Path  
	END  
	If @Opcode=43                                                                                                           
	BEGIN    
		select AutoId,SubcategoryName from SubCategoryMaster where (@CategoryAutoId=0  or @CategoryAutoId is null or CategoryAutoId=@CategoryAutoId)  
		order by SubcategoryName asc  
		for json Path  
	END 
	If @Opcode=44                                                                                                           
	BEGIN	
	   BEGIN TRY
	    BEGIN Transaction 
		
		Declare @TempTable TABLE (ID INT identity(1,1),Location varchar(150))   
		Declare @TempTable1 TABLE (ID INT  identity(1,1),ProductAutoId int,Location int)  

		INSERT INTO @TempTable Select * from dbo.fnSplitString(@OrderAutoIds,',')

		INSERT INTO @TempTable1
		SELECT REVERSE(PARSENAME(REPLACE(REVERSE(Location), '-', '.'), 1)) AS [AutoId],REVERSE(PARSENAME(REPLACE(REVERSE(Location), '-', '.'), 2)) AS [Location] FROM @TempTable

		Declare @i int=1,@Count int=0,@ProductAutoID int                                            
		SELECT @Count=COUNT(*) from @TempTable1                                            
		IF (@Count>0)                                         
		BEGIN                                                       
			WHILE @i <= @Count                                                          
			BEGIN                                                          
				Select @ProductAutoID=ProductAutoId,@Location=Location from @TempTable1 where ID=@i  
				
				IF @Location=1 --PSMNJ
				BEGIN

					Insert into ProductMasterLog
					(ProductId,ProductName,ProductLocation,ImageUrl,CategoryAutoId,SubcategoryAutoId,Stock,ProductType,TaxRate,PackingAutoId,
					VendorAutoId,ReOrderMark,MLQty,UpdateDate,ProductStatus,CreateBy,CreateDate,ModifiedBy,ModifiedDate,BrandAutoId,P_CommCode,
					WeightOz,IsApply_ML,IsApply_Oz,ThumbnailImageUrl,ThirdImage,NewSRP,ShowOnWebsite,IsOutOfStock,Description,P_SRP,CurrentStock,
					CheckImageURLExistense,CheckThirdImageExistense,Product_ShowOnWebsite,OriginalImageUrl) 

					Select ProductId,ProductName,ProductLocation,ImageUrl,CategoryAutoId,SubcategoryAutoId,Stock,ProductType,TaxRate,
					PackingAutoId,VendorAutoId,ReOrderMark,MLQty,UpdateDate,ProductStatus,CreateBy,CreateDate,ModifiedBy,ModifiedDate,
					BrandAutoId,P_CommCode,WeightOz,IsApply_ML,IsApply_Oz,ThumbnailImageUrl,ThirdImage,NewSRP,ShowOnWebsite,IsOutOfStock,
					Description,P_SRP,CurrentStock,CheckImageURLExistense,CheckThirdImageExistense,Product_ShowOnWebsite,OriginalImageUrl
					from ProductMaster WHERE AutoId=@ProductAutoID 
					
					Update [psmnj.a1whm.com].[dbo].ProductMaster SET ProductStatus=0,UpdateDate=GETDATE() Where AutoId=@ProductAutoID   
				END
				ELSE IF @Location=2 --PSMCT
				BEGIN

					Insert into [psmct.a1whm.com].[dbo].ProductMasterLog
					(ProductId,ProductName,ProductLocation,ImageUrl,CategoryAutoId,SubcategoryAutoId,Stock,ProductType,TaxRate,PackingAutoId,
					VendorAutoId,ReOrderMark,MLQty,UpdateDate,ProductStatus,CreateBy,CreateDate,ModifiedBy,ModifiedDate,BrandAutoId,P_CommCode,
					WeightOz,IsApply_ML,IsApply_Oz,ThumbnailImageUrl,ThirdImage,NewSRP,ShowOnWebsite,IsOutOfStock,Description,P_SRP,CurrentStock,
					CheckImageURLExistense,CheckThirdImageExistense,Product_ShowOnWebsite,OriginalImageUrl) 

					Select ProductId,ProductName,ProductLocation,ImageUrl,CategoryAutoId,SubcategoryAutoId,Stock,ProductType,TaxRate,
					PackingAutoId,VendorAutoId,ReOrderMark,MLQty,UpdateDate,ProductStatus,CreateBy,CreateDate,ModifiedBy,ModifiedDate,
					BrandAutoId,P_CommCode,WeightOz,IsApply_ML,IsApply_Oz,ThumbnailImageUrl,ThirdImage,NewSRP,ShowOnWebsite,IsOutOfStock,
					Description,P_SRP,CurrentStock,CheckImageURLExistense,CheckThirdImageExistense,Product_ShowOnWebsite,OriginalImageUrl
					from [psmct.a1whm.com].[dbo].ProductMaster WHERE AutoId=@ProductAutoID   

				   Update [psmct.a1whm.com].[dbo].ProductMaster SET ProductStatus=0,UpdateDate=GETDATE() Where AutoId=@ProductAutoID   
				END
				ELSE IF @Location=4 --PSMPA
				BEGIN 

					Insert into [psmpa.a1whm.com].[dbo].ProductMasterLog
					(ProductId,ProductName,ProductLocation,ImageUrl,CategoryAutoId,SubcategoryAutoId,Stock,ProductType,TaxRate,PackingAutoId,
					VendorAutoId,ReOrderMark,MLQty,UpdateDate,ProductStatus,CreateBy,CreateDate,ModifiedBy,ModifiedDate,BrandAutoId,P_CommCode,
					WeightOz,IsApply_ML,IsApply_Oz,ThumbnailImageUrl,ThirdImage,NewSRP,ShowOnWebsite,IsOutOfStock,Description,P_SRP,CurrentStock,
					CheckImageURLExistense,CheckThirdImageExistense,Product_ShowOnWebsite,OriginalImageUrl) 

					Select ProductId,ProductName,ProductLocation,ImageUrl,CategoryAutoId,SubcategoryAutoId,Stock,ProductType,TaxRate,
					PackingAutoId,VendorAutoId,ReOrderMark,MLQty,UpdateDate,ProductStatus,CreateBy,CreateDate,ModifiedBy,ModifiedDate,
					BrandAutoId,P_CommCode,WeightOz,IsApply_ML,IsApply_Oz,ThumbnailImageUrl,ThirdImage,NewSRP,ShowOnWebsite,IsOutOfStock,
					Description,P_SRP,CurrentStock,CheckImageURLExistense,CheckThirdImageExistense,Product_ShowOnWebsite,OriginalImageUrl
					from [psmpa.a1whm.com].[dbo].ProductMaster WHERE AutoId=@ProductAutoID    

				   Update [psmpa.a1whm.com].[dbo].ProductMaster SET ProductStatus=0,UpdateDate=GETDATE() Where AutoId=@ProductAutoID   
				END
				ELSE IF @Location=5 --PSMNPA
				BEGIN  

					Insert into [psmnpa.a1whm.com].[dbo].ProductMasterLog
					(ProductId,ProductName,ProductLocation,ImageUrl,CategoryAutoId,SubcategoryAutoId,Stock,ProductType,TaxRate,PackingAutoId,
					VendorAutoId,ReOrderMark,MLQty,UpdateDate,ProductStatus,CreateBy,CreateDate,ModifiedBy,ModifiedDate,BrandAutoId,P_CommCode,
					WeightOz,IsApply_ML,IsApply_Oz,ThumbnailImageUrl,ThirdImage,NewSRP,ShowOnWebsite,IsOutOfStock,Description,P_SRP,CurrentStock,
					CheckImageURLExistense,CheckThirdImageExistense,Product_ShowOnWebsite,OriginalImageUrl) 

					Select ProductId,ProductName,ProductLocation,ImageUrl,CategoryAutoId,SubcategoryAutoId,Stock,ProductType,TaxRate,
					PackingAutoId,VendorAutoId,ReOrderMark,MLQty,UpdateDate,ProductStatus,CreateBy,CreateDate,ModifiedBy,ModifiedDate,
					BrandAutoId,P_CommCode,WeightOz,IsApply_ML,IsApply_Oz,ThumbnailImageUrl,ThirdImage,NewSRP,ShowOnWebsite,IsOutOfStock,
					Description,P_SRP,CurrentStock,CheckImageURLExistense,CheckThirdImageExistense,Product_ShowOnWebsite,OriginalImageUrl
					from [psmnpa.a1whm.com].[dbo].ProductMaster WHERE AutoId=@ProductAutoID   

				   Update [psmnpa.a1whm.com].[dbo].ProductMaster SET ProductStatus=0,UpdateDate=GETDATE() Where AutoId=@ProductAutoID    
				END
				ELSE IF @Location=9 --PSMWPA
				BEGIN 

					Insert into [psmwpa.a1whm.com].[dbo].ProductMasterLog
					(ProductId,ProductName,ProductLocation,ImageUrl,CategoryAutoId,SubcategoryAutoId,Stock,ProductType,TaxRate,PackingAutoId,
					VendorAutoId,ReOrderMark,MLQty,UpdateDate,ProductStatus,CreateBy,CreateDate,ModifiedBy,ModifiedDate,BrandAutoId,P_CommCode,
					WeightOz,IsApply_ML,IsApply_Oz,ThumbnailImageUrl,ThirdImage,NewSRP,ShowOnWebsite,IsOutOfStock,Description,P_SRP,CurrentStock,
					CheckImageURLExistense,CheckThirdImageExistense,Product_ShowOnWebsite,OriginalImageUrl) 

					Select ProductId,ProductName,ProductLocation,ImageUrl,CategoryAutoId,SubcategoryAutoId,Stock,ProductType,TaxRate,
					PackingAutoId,VendorAutoId,ReOrderMark,MLQty,UpdateDate,ProductStatus,CreateBy,CreateDate,ModifiedBy,ModifiedDate,
					BrandAutoId,P_CommCode,WeightOz,IsApply_ML,IsApply_Oz,ThumbnailImageUrl,ThirdImage,NewSRP,ShowOnWebsite,IsOutOfStock,
					Description,P_SRP,CurrentStock,CheckImageURLExistense,CheckThirdImageExistense,Product_ShowOnWebsite,OriginalImageUrl
					from [psmwpa.a1whm.com].[dbo].ProductMaster WHERE AutoId=@ProductAutoID  

				   Update [psmwpa.a1whm.com].[dbo].ProductMaster SET ProductStatus=0,UpdateDate=GETDATE() Where AutoId=@ProductAutoID   
				END
				ELSE IF @Location=10 --PSMNY
				BEGIN
					Insert into [psmny.a1whm.com].[dbo].ProductMasterLog
					(ProductId,ProductName,ProductLocation,ImageUrl,CategoryAutoId,SubcategoryAutoId,Stock,ProductType,TaxRate,PackingAutoId,
					VendorAutoId,ReOrderMark,MLQty,UpdateDate,ProductStatus,CreateBy,CreateDate,ModifiedBy,ModifiedDate,BrandAutoId,P_CommCode,
					WeightOz,IsApply_ML,IsApply_Oz,ThumbnailImageUrl,ThirdImage,NewSRP,ShowOnWebsite,IsOutOfStock,Description,P_SRP,CurrentStock,
					CheckImageURLExistense,CheckThirdImageExistense,Product_ShowOnWebsite,OriginalImageUrl) 

					Select ProductId,ProductName,ProductLocation,ImageUrl,CategoryAutoId,SubcategoryAutoId,Stock,ProductType,TaxRate,
					PackingAutoId,VendorAutoId,ReOrderMark,MLQty,UpdateDate,ProductStatus,CreateBy,CreateDate,ModifiedBy,ModifiedDate,
					BrandAutoId,P_CommCode,WeightOz,IsApply_ML,IsApply_Oz,ThumbnailImageUrl,ThirdImage,NewSRP,ShowOnWebsite,IsOutOfStock,
					Description,P_SRP,CurrentStock,CheckImageURLExistense,CheckThirdImageExistense,Product_ShowOnWebsite,OriginalImageUrl
					from [psmny.a1whm.com].[dbo].ProductMaster WHERE AutoId=@ProductAutoID   

				   Update [psmny.a1whm.com].[dbo].ProductMaster SET ProductStatus=0,UpdateDate=GETDATE() Where AutoId=@ProductAutoID   

				END
				SET @i=@i+1                                                          
			END   
			Commit Transaction
		END 
	   END TRY
	   BEGIN CATCH
	      RollBack Transaction
	      SET @isException=1
		  SET @exceptionMessage=ERROR_MESSAGE()
	   END CATCH
	END 
END TRY                    
BEGIN CATCH                    
Set @isException=1                    
Set @exceptionMessage=ERROR_MESSAGE()                    
END CATCH                                 
END 
GO