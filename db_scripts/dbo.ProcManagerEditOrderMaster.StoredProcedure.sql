Create or Alter PROCEDURE [dbo].[ProcManagerEditOrderMaster]                                                                                                                                                    
 @Opcode INT=NULL,                                                                                                                                                    
 @OrderAutoId INT=NULL,                                                                                                                                                    
 @LogAutoId int=null,                                                                                                                                                    
 @OrderNo VARCHAR(15)=NULL,                                                                                                                                                    
 @IsExchange int=NULL,                                                                                                                                                    
 @PaymentId VARCHAR(20)=NULL,                                                                                                                                                    
 @PaymentAutoId INT=NULL,                                                                                                                                                    
 @OrderDate DATETIME=NULL,                                                                                                                                                    
 @DeliveryDate DATETIME=NULL,                                                                                                                                                    
 @CustomerAutoId INT=NULL,                                                                                                                                                    
 @TaxType int=NULL,                                                                                                                                                    
 @OrderType int =NULL,                                                                                                                                                    
 @Terms INT=NULL,                                                                                                                                                    
 @Times INT=NULL,                                                                                                                                                    
 @IsFreeItem  INT=NULL,                                                                                                                                                    
 @IsTaxable int =NULL,                                                                                                                                                    
 @CreditAmount decimal(18,2)=null,                                                                                                                                                    
 @MLTax decimal(18,2)=null,                                                                                                                                                    
 @CreditMemoAmount decimal(18,2)=null,                                                                                                                                                    
 @PayableAmount decimal(18,2)=null,                                                                                                                                                    
 @CreditNo varchar(200)=null,                                                                                                                                                    
 @ProductAutoId INT=NULL,    
 @UnitAutoId INT=NULL,           
 @SalesPersonAutoId INT=NULL,           
 @LoginEmpType INT=NULL,                                        
 @PackerAutoId INT=NULL,                                      
 @EmpAutoId INT=NULL,                                                  
 @FreeItem bit=NULL,                                                          
 @OrderStatus INT=NULL,                                                                                                 
 @ShippingType INT=NULL,                                                   
 @ReqQty  INT=NULL,                                                                                
 @QtyPerUnit INT=NULL,                                                                                                            
 @UnitPrice DECIMAL(18,2)=NULL,                                                                                                        
 @SRP DECIMAL(18,2)=NULL,                                                                                      
 @GP DECIMAL(18,2)=NULL,                                                                                                                                 
 @TaxRate DECIMAL(18,2)=NULL,                                                                                                     
 @minprice  DECIMAL(18,2)=NULL,                                                                                                       
 @TotalAmount DECIMAL(18,2)=NULL,                                                                                                                                            
 @OverallDisc DECIMAL(18,2)=NULL,                                                                                               
 @OverallDiscAmt DECIMAL(18,2)=NULL,                                                                                                                             
 @ShippingCharges DECIMAL(18,2)=NULL,                                                                                                                                                    
 @MLQty DECIMAL(18,2)=NULL,                                                                                                                                                    
 @TotalTax DECIMAL(18,2)=NULL,                                                                                           
 @GrandTotal DECIMAL(18,2)=NULL,                                                                                                   
 @PackedBoxes INT=NULL,                                                                                               
 @CustomerName VARCHAR(200)=NULL,                                                                                                 
 @Delivered VARCHAR(20)=NULL,                                                                                                                                                    
 @PaymentRecev VARCHAR(20)=NULL,                                                                                                                                                    
 @RecevAmt VARCHAR(20)=NULL,                                                                                                                                                    
 @AmtValue DECIMAL(18,2)=NULL,                                                                                                                                                    
 @ValueChanged VARCHAR(20)=NULL,                                                                                                                                      
 @ManagerRemarks VARCHAR(max)=NULL,                                                                                                                   
 @DiffAmt DECIMAL(18,2)=NULL,                                                                                                                                                    
 @NewTotal DECIMAL(18,2)=NULL,                    
 @PayThru INT=NULL,                                    
 @CheckNo varchar(150) =null,                                 
 @Remarks VARCHAR(max)=NULL,                                                                    
 @CommentType INT=NULL,                                                                                                                          
 @Comment VARCHAR(max)=NULL,                                   
 @AddressType INT=NULL,                              
 @Address VARCHAR(200)=NULL,                             
 @Address2 VARCHAR(200)=NULL,                                     
 @State INT=NULL,                                    
 @City VARCHAR(50)=NULL,                                
 @StateName VARCHAR(150)=NULL,                                
 @Lat VARCHAR(50)=NULL,                                
 @Long VARCHAR(50)=NULL,   
 @ZipAutoId INT=NULL,
 @Zipcode varchar(10)=NULL,                                                       
 @AddressAutoId INT=NULL,                                                                       
 @BillAddrAutoId INT=NULL,                                                   
 @ShipAddrAutoId INT=NULL,                                                        
 @OrderItemAutoId INT=NULL,                                                                                
 @TableValue [DT_SM_OrderItemList] readonly,                                                                                                                         
 @TableAutoId DT_ProAutoIdList readonly,                                                                                                                                                    
 @AsgnOrder DT_OrderAsgn readonly,                                                                
 @DelItems DT_Delivered_Order_Items readonly,                                                                         
 @Payment DT_PayAgainstOrder readonly,                                                                                                                                                    
 @CreditTable [DT_CreditMemo] readonly,                                                                                                                                  
 @Todate DATETIME=NULL,                                                                                                                                                    
 @Fromdate DATETIME=NULL,                                                                                    
 @Barcode varchar(50)=NULL,                                                                                                                                                    
 @QtyShip INT=NULL,                                                                                                                                              
 @FromDelDate DATE=NULL,                                                                                     
 @ToDelDate DATE=NULL,                                                                           
 @DriverAutoId INT=NULL,                                                                                                                                             
 @AsgnDate DATE=NULL,                                                                                                                        
 @Root VARCHAR(200)=NULL,                                                                                                                                                    
 @Stoppage VARCHAR(10)=NULL,                                                                                                                                                    
 @PkgAutoId INT=NULL,                                                                                                                                                    
 @PkgId VARCHAR(12)=NULL,                                    
 @PkgDt DATETIME=NULL,                                                   
 @PriceLevelAutoId INT=NULL,              
 @CustomerTypeAutoId int=null,          
 @PageIndex INT = 1,                
 @PageSize INT = 10,                                                                                                                            
 @RecordCount INT =null,                           
 @LastBasePrice DECIMAL(18,2)=NULL,                                                                                                                                    
 @CurrentPrice DECIMAL(18,2)=NULL,                                                                   
 @AdjustmentAmt  DECIMAL(18,2)=NULL,                                                   
 @LogRemark varchar(200) = NULL,                                                                                                                                                    
 @DraftAutoId int =null out,                                         
 @CheckSecurity VARCHAR(250)=NULL,                                                     
 @TypeShipping  VARCHAR(250)=NULL,                                                                                                                               
 @isException bit out,                                           
 @exceptionMessage varchar(max) out                                                                                      
AS                                                                                                                                                    
BEGIN                                                                                                                               
  BEGIN TRY                                                                                                                                            
   Set @isException=0                                                                                                                           
   Set @exceptionMessage='Success'                                                                                                         
   declare @customerType varchar(50)                                                                                                                                                 
  IF @Opcode=102                                                                                     
   BEGIN  
   DECLARE @CityAutoId INT=NULL,@ZipcodeAutoid INT =NULL;
   IF NOT EXISTS(SELECT AutoId FROM State WHERE  StateName=@StateName)                                                          
	BEGIN                           
		SET @isException=1                                                                            
		SET @exceptionMessage='Invalid State'                                                                                                  
	END 
	ELSE
    BEGIN
		Exec ProcinsertData_Cityzipcode_ALL @City=@City,@State=@StateName,@Zipcode1=@Zipcode
		SET @State =(Select top 1 StateId from CityMaster Where CityName= trim(@City)) 
		SET @CityAutoId=(Select AutoId from CityMaster Where CityName=trim(@City)) 		
		SET @ZipcodeAutoid =(Select AutoId from ZipMaster Where Zipcode=@Zipcode and CityId=@CityAutoId)                                          
   IF(@AddressType = 11)                                                                                                                                                  
   BEGIN                  
	BEGIN TRY   
		 IF NOT EXISTS(SELECT AutoId FROM BillingAddress where Address=trim(@Address) AND CustomerAutoId=@CustomerAutoId)
		 BEGIN                                                                     
		    BEGIN TRAN  
			UPDATE [dbo].[BillingAddress] SET [IsDefault]=0 WHERE [CustomerAutoId]=@CustomerAutoId 
			
			INSERT INTO [dbo].[BillingAddress] ([CustomerAutoId],[Address],[State],[Zipcode],[IsDefault],BCityAutoId,ZipcodeAutoid,BillSA_Lat,BillSA_Long,Address2)                                                                                                                                             
		    VALUES(@CustomerAutoId,@Address,@State,@Zipcode,1,@CityAutoId,@ZipcodeAutoid,@Lat,@Long,@Address2) 
			
			SET @BillAddrAutoId=SCOPE_IDENTITY()

			UPDATE [dbo].[CustomerMaster] SET [DefaultBillAdd]=@BillAddrAutoId
			WHERE [AutoId]=@CustomerAutoId 

			Update OrderMaster SET BillAddrAutoId=@BillAddrAutoId where OrderNo=@OrderNo

		    COMMIT TRANSACTION    
		 END  
		 ELSE
		 BEGIN                                                                             
			SET @isException=1                                                                                                                    
			SET @exceptionMessage='AddrExist' 
		 END
	END TRY                                                                                              
	BEGIN CATCH                                                                                                           
		ROLLBACK TRAN                                                                              
		SET @isException=1                                                                                                                    
		SET @exceptionMessage=ERROR_MESSAGE()--'Oops! Something went wrong.Please try later.'                                                                                            
	END CATCH                                                                                                                                                  
   END                                                          
   ELSE IF(@AddressType = 22)                                                                                     
	BEGIN                                                             
	BEGIN TRY  
	   IF NOT EXISTS(Select AutoId FROm ShippingAddress Where Address=trim(@Address) AND CustomerAutoId=@CustomerAutoId)
	   BEGIN
		    BEGIN TRAN                                                                                                                            
			UPDATE [dbo].[ShippingAddress] SET [IsDefault] = 0 WHERE [CustomerAutoId]=@CustomerAutoId  
			
			INSERT INTO [dbo].[ShippingAddress] ([CustomerAutoId],[Address],[State],[Zipcode],[IsDefault],SCityAutoId,ZipcodeAutoid,SA_Lat,SA_Long,Address2)                                                                                                                    
			VALUES(@CustomerAutoId,@Address,@State,@Zipcode,1,@CityAutoId,@ZipcodeAutoid,@Lat,@Long,@Address2)      
			
			SET @ShipAddrAutoId=SCOPE_IDENTITY()

			UPDATE [dbo].[CustomerMaster] SET DefaultShipAdd=@ShipAddrAutoId
			WHERE [AutoId]=@CustomerAutoId 

			Update OrderMaster SET ShipAddrAutoId=@ShipAddrAutoId where OrderNo=@OrderNo
			
			COMMIT TRANSACTION  
		END
		ELSE
		BEGIN                                                                                                                                         
			SET @isException=1         
			SET @exceptionMessage='AddrExist'     
		END
	END TRY                                                                                         
	BEGIN CATCH                                                                                                                                                  
		ROLLBACK TRAN                                                                                                                                                  
		SET @isException=1         
		SET @exceptionMessage=ERROR_MESSAGE()--'Oops! Something went wrong.Please try later.'                                                                                                                                  
	END CATCH                                                                            
	END              
	END
   END   
                                                                                                               
  ELSE IF @Opcode=104                                       
   BEGIN                                                                                                                                                    
  IF EXISTS(SELECT [AutoId] FROM [dbo].[DriverRootLog] WHERE [DrvAutoId] = @DriverAutoId                                                                                                                                                     
  AND [AssignDate] = @AsgnDate)                                                                                          
   BEGIN                                                                                                    
    UPDATE [dbo].[DriverRootLog] SET [Root] = @Root WHERE [DrvAutoId] = @DriverAutoId AND [AssignDate] = @AsgnDate                                                                                                                                            
  
    
      
        
    UPDATE OM SET [Stoppage] = t.[Stoppage],RootName=@Root,DriverRemarks=t.Remarks FROM [dbo].[OrderMaster] As OM                                                                                                                                              
    INNER JOIN (SELECT * FROM @AsgnOrder) AS t ON t.[OrderAutoId] = OM.[AutoId]                                                                 
   END                                                                                    
  ELSE                                             
   BEGIN                                                                                                                                                    
    INSERT INTO [dbo].[DriverRootLog] ([DrvAutoId],[AssignDate],[Root])                                                                                                                     
    VALUES(@DriverAutoId,@AsgnDate,@Root)                           
    UPDATE OM SET [Stoppage] = t.[Stoppage],RootName=@Root,DriverRemarks=t.Remarks FROM [dbo].[OrderMaster] As OM                                                                                                                                              
 
    
      
       
    INNER JOIN (SELECT * FROM @AsgnOrder) AS t ON t.[OrderAutoId] = OM.[AutoId]                                                                                                                                                     
   END                                                                                                
   END                      
                                                                                                                                                       
  ELSE IF @Opcode=109                                                                                                                    
  BEGIN                                                                                                               
   BEGIN TRY                                                                       
    BEGIN TRAN                                                                                                                                                         
                                                                                                                                                        
      SET @OrderAutoId=(SELECT AutoId FROM OrderMaster WHERE OrderNo=@OrderNo)                                                  
     INSERT INTO [dbo].CreditMemoDeductionlog([OrderAutoId],[CreditMemoAutoId],[DeductionAmount],[PayDate],[PayTime]                                              
     ,[EmpAutoId],[Remarks])                                                                                                                        
     SELECT @OrderAutoId,CreditAutoId,DeductAmount,GETDATE(),GETDATE(),@EmpAutoId,[Remarks] FROM @CreditTable As p                                                                                                                                            
  
                                                                                  
                                   
                                                                                                                                                     
     UPDATE OM SET PaidAmount=(ISNULL(PaidAmount,0)+DeductAmount)                                                                                                                                                    
      FROM [dbo].CreditMemoMaster AS OM                                                                            
     INNER JOIN @CreditTable As pay ON pay.[CreditAutoId] = OM.CreditAutoId WHERE OrderAutoId IS NOT NULL                                                                                                                                                    
                                                                                                                                
     UPDATE OM SET OrderAutoId =@OrderAutoId,PaidAmount=(ISNULL(PaidAmount,0)+DeductAmount)                                                                                                                                                    
      FROM [dbo].CreditMemoMaster AS OM                                                                                                      
     INNER JOIN @CreditTable As pay ON pay.[CreditAutoId] = OM.CreditAutoId WHERE OrderAutoId IS NULL                                                                                                
                                                                                                      
                                                       
               SET @LogRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=13), '[OrderNo]', @OrderNo)                                                                                                                          
 
     
     
         
         
            
              
                
      INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                                                    
      VALUES(13,@EmpAutoId,getdate(),@LogRemark,@OrderAutoId)                                                                                       
                                                                                                                                                          
                                                                        
    COMMIT TRANSACTION                                                                                                                                      
   END TRY                                                                                                                                               
   BEGIN CATCH                                                                                                                                                    
    ROLLBACK TRAN                                                                                                                                                    
    Set @isException=1                                                                         
    Set @exceptionMessage='Oops! Something went wrong.Please try later.'                    
 END CATCH           
  END                                                                                               
  ELSE IF @Opcode=31                                                                                                          
  BEGIN                                                                                                                                                    
   BEGIN TRY            BEGIN TRAN                                                                                                                              
                                                                                       
      SET @OrderAutoId=(SELECT OrderAutoId FROM CreditMemoDeductionlog WHERE AutoId=@LogAutoId)                                                                                                         
      SET @OrderNo=(SELECT OrderNo FROM OrderMaster WHERE AutoId=@OrderAutoId)                                                                                                                                            
      declare @CreditAutoId int=(SELECT CreditMemoAutoId FROM CreditMemoDeductionlog WHERE AutoId=@LogAutoId)                           
      declare @DeductAmount decimal(18,2)=(SELECT DeductionAmount FROM CreditMemoDeductionlog WHERE AutoId=@LogAutoId)                                                  
                                                                                                       
     UPDATE OM SET PaidAmount=(ISNULL(PaidAmount,0)-@DeductAmount),                                                                                                                                         
     OrderAutoId=case when OrderAutoId=@OrderAutoId then null else OrderAutoId end FROM [dbo].CreditMemoMaster AS OM                                                                             
     WHERE CreditAutoId=@CreditAutoId 
	 
		delete from  [dbo].CreditMemoDeductionlog where AutoId=@LogAutoId                                                                            
                                                                                                                                                                     
		SET @LogRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=14), '[OrderNo]', @OrderNo)                                                                                                                       
   
      INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                                     
      VALUES(14,@EmpAutoId,getdate(),@LogRemark,@OrderAutoId)                                                      
    COMMIT TRANSACTION                                                                                                             
   END TRY                                                                                                             
   BEGIN CATCH                                                                                                                                                    
  ROLLBACK TRAN                                                                                                                    
    Set @isException=1                         
    Set @exceptionMessage='Oops! Something went wrong.Please try later.'                                                                        
   END CATCH                                                                                                        
  END                                                                                                                                       
  ELSE If @Opcode=201                                                                                                                                                       
   BEGIN TRY                                                                                                     
    BEGIN TRAN                                                                                                                                
 IF @LoginEmpType=2 AND (select Status FROM [dbo].[OrderMaster] WHERE [AutoId] = @OrderAutoId )>1                                                                                                                                               
 BEGIN                                                                                                               
  SET @isException=1                                                                                      
  SET @exceptionMessage='Order has been processed so you can not update.'                                                                        
 END                                                                                                                                                
 ELSE IF @LoginEmpType=7 AND (select Status FROM [dbo].[OrderMaster] WHERE [AutoId] = @OrderAutoId )>2                                                                          
 BEGIN                                                                                                     
  SET @isException=1                                                                                                                                                
  SET @exceptionMessage='Order has been packed so you can not update.'                                                                                                                                                
 END                                          
 ELSE                                                                           
 BEGIN                    
  SET @BillAddrAutoId=(SELECT TOP 1 BillAddrAutoId FROM ordermaster WHERE AutoId =@OrderAutoId)       
  SET @State=(SELECT TOP 1 State FROM BillingAddress WHERE AutoId =@BillAddrAutoId)       
                                                          
  UPDATE  [dbo].[OrderMaster] SET                             
     [CustomerAutoId] = @CustomerAutoId,                                                                         
     [BillAddrAutoId] = @BillAddrAutoId,                                                                                                                                                    
     [ShipAddrAutoId] = @ShipAddrAutoId,                                                                                                                                                    
     [DeliveryDate]  = @DeliveryDate,        
  TaxType=@TaxType,            
  TaxValue= (select Value from TaxTypeMaster where autoid=@TaxType),       
  IsTaxApply=CASE WHEN (select count(1) from TaxTypeMaster where State=@State)=0 then 0 else 1 end,                                                                                                                                                        
     [OverallDiscAmt] = @OverallDiscAmt,                                         
     [ShippingCharges] = @ShippingCharges,                                                                                                 
     [ShippingType]  = CASE WHEN ISNULL(@ShippingType,0)=0 THEN ShippingType ELSE @ShippingType END,                                   
     [OrderRemarks] = @Remarks                                                             
  WHERE [AutoId] = @OrderAutoId                                                                        
                                                                                                                         
   UPDATE [dbo].[Order_Original] SET                                                     
     [CustomerAutoId] = @CustomerAutoId,                                                                 
     [BillAddrAutoId] = @BillAddrAutoId,                                                                                                                                                    
     [ShipAddrAutoId] = @ShipAddrAutoId,                                                                                                                                                    
     [DeliveryDate]  = @DeliveryDate,                                                                                 
     [TotalAmount]  = @TotalAmount,                                                                                                                    
     [OverallDiscAmt] = @OverallDiscAmt,                                                                                                                
     [ShippingCharges] = @ShippingCharges,                                                                                                                                                    
     [TotalTax]   = @TotalTax,                                                                                   
     [GrandTotal]  = @GrandTotal,                                                                                                                                     
     [ShippingType]  = CASE WHEN ISNULL(@ShippingType,0)=0 THEN ShippingType ELSE @ShippingType END                                                                                                                  
    WHERE [AutoId] = @OrderAutoId                                                                                                                             
           
 select * into #tempOIM from [dbo].[OrderItemMaster] where [OrderAutoId] = @OrderAutoId                                                                                                                                
           
 DELETE FROM [dbo].[OrderItemMaster] WHERE [OrderAutoId] = @OrderAutoId                                                                                                                                                                                       
  
    
      
        
                   
 INSERT INTO [dbo].[OrderItemMaster]([OrderAutoId],[ProductAutoId],[UnitTypeAutoId],[QtyPerUnit],[RequiredQty],                                                                                                                       
 Barcode,QtyShip,[UnitPrice],[SRP],[GP],[Tax],IsExchange,UnitMLQty,isFreeItem,Weight_Oz)                                                                             
 SELECT @OrderAutoId,tb.[ProductAutoId],tb.[UnitAutoId],tb.[QtyPerUnit],tb.[RequiredQty],tb.Barcode,tb.QtyShip,                                                                                                                                             
 [UnitPrice],pm.[P_SRP],tb.[GP],tb.[Tax],tb.IsExchange,                                    
 ISNULL(pm.MLQty,0),isFreeItem, case when IsExchange=0 and isFreeItem=0 then ISNULL(pm.WeightOz,0) else 0 end FROM                       
 @TableValue AS tb                         
 inner join ProductMaster as pm on pm.AutoId=tb.ProductAutoId            
           
 Update OIM Set          
 OIM.Original_UnitType=dt.Original_UnitType          
    from [dbo].[OrderItemMaster] as OIM           
 inner join (SELECT * FROM #tempOIM) AS dt On dt.[ProductAutoId] = OIM.[ProductAutoId] and dt.[isFreeItem] = OIM.[isFreeItem]          
 and dt.[OrderAutoId]=OIM.[OrderAutoId] and dt.[Tax] = OIM.[Tax] and dt.[IsExchange] = OIM.[IsExchange]          
 WHERE OIM.OrderAutoId = @OrderAutoId          
          
 Drop table #tempOIM           
          
   DELETE FROM [dbo].[OrderItems_Original] WHERE [OrderAutoId] = @OrderAutoId                                                                                                                                                    
                             
   INSERT INTO [dbo].[OrderItems_Original]([OrderAutoId],[ProductAutoId],[UnitTypeAutoId],[QtyPerUnit],[UnitPrice],[RequiredQty],[TotalPieces],                                                                                                                
  
    
      
       
           
   [SRP],[GP],[Tax],[NetPrice],isFreeItem,UnitMLQty,TotalMLQty)                                                                
   SELECT @OrderAutoId,tb.[ProductAutoId],tb.[UnitAutoId],tb.[QtyPerUnit],tb.[UnitPrice],tb.[RequiredQty],tb.[TotalPieces],tb.[SRP],                                                                 
                        
                         
   tb.[GP],tb.[Tax],tb.[NetPrice],isFreeItem,ISNULL(pm.MLQty,0),(ISNULL(pm.MLQty,0)*(tb.RequiredQty*[QtyPerUnit])) FROM @TableValue AS tb                                                                    
   inner join ProductMaster as pm on pm.AutoId=tb.ProductAutoId                                                                                                                                                      
                                                                                                                                                    
   SET @PriceLevelAutoId = (SELECT [PriceLevelAutoId] FROM [dbo].[CustomerPriceLevel] WHERE [CustomerAutoId] = @CustomerAutoId)                                                  
   exec UPDTAE_PRICELEVEL                 
   @OrderAutoId=@OrderAutoId, @PriceLevelAutoId=@PriceLevelAutoId,@EmpAutoId=@EmpAutoId                                                                                                                                
 
   SET @LogRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId] = 2), '[OrderNo]', (SELECT [OrderNo] FROM OrderMaster where [AutoId] = @OrderAutoId))                                                                            
  
   
   INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                                                                                                                                    
   VALUES(2,@EmpAutoId,getdate(),@LogRemark,@OrderAutoId)   
   
   SELECT @CustomerAutoId=CustomerAutoId FROM [dbo].[OrderMaster]                 
	WHERE [AutoId] = @OrderAutoId                
	if((select CustomerType from CustomerMaster where AutoId=@CustomerAutoId)=3)                      
	begin                      
	exec [dbo].[ProcUpdatePOStatus_All]                      
	@CustomerId=@CustomerAutoId,                      
	@OrderAutoId=@OrderAutoId,                      
	@Status=1 
	end        
                                                                                     
  END                                                                                                                                                
    COMMIT TRANSACTION                                                                                  
   END TRY                                                                                                                                                    
    BEGIN CATCH                                                                      
     ROLLBACK TRAN                                          
     Set @isException=1                                                                                                    
     Set @exceptionMessage='Oops! Something went wrong.Please try later.'                                                                                               
    END CATCH                                                     
  ELSE IF @Opcode = 202                                                                                                                                                     
 BEGIN                                                                                                
  BEGIN TRY                                                                                                                           
  BEGIN TRAN 
   IF Exists(Select AutoId FROM EmployeeMaster Where AutoId=@EmpAutoId AND EmpType=8)
	 BEGIN
     IF Exists(Select AutoId FROM EmployeeMaster Where AutoId=@DriverAutoId AND EmpType in (2,5))
	 BEGIN
		SET @CustomerAutoId=(SELECT CUSTOMERAUTOID FROM OrderMaster WHERE AUTOID=@OrderAutoId)                                                                                                  
		SET @customerType =(select CustomerType from CustomerMaster where AutoId=@CustomerAutoId)                                                                                                  
		if( (select COUNT(1) from OrderItemMaster as oim                                                                                                   
		inner join PackingDetails as pd on oim.ProductAutoId=pd.ProductAutoId and oim.UnitTypeAutoId=pd.UnitType                                 
		where OrderAutoId=@OrderAutoId and isFreeItem=0 and IsExchange=0                                                                                                  
		and oim.UnitPrice<(case @customertype when 2 then pd.whminprice when 3 then pd.costprice else [minprice] end))=0)                                                                                                  
		BEGIN                                                                                              
		UPDATE [dbo].[OrderMaster] SET [Driver] = case when @DriverAutoId is null then Driver else @DriverAutoId end
		,[AssignDate]=ISNULL(@AsgnDate,GETDATE()),[Status]=4,[Stoppage] = NULL,                                                                                                                                                    
		ManagerAutoId=@EmpAutoId WHERE [AutoId] = @OrderAutoId             
		INSERT INTO [dbo].[DrvLog] ([DrvAutoId],[OrderAutoId],[AssignDate]) VALUES(@DriverAutoId,@OrderAutoId,Convert(date,getdate()))                                                                              
		SELECT EM.[FirstName] + ' ' + Em.[LastName] AS Name,CONVERT(VARCHAR(20),OM.[AssignDate],101) AS AssignDate,
		SM.[StatusType],OM.[Status] 
		FROM [dbo].[OrderMaster] AS OM                                                                                                                             
		INNER JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[Driver]                                                                                                                            
		INNER JOIN [dbo].[StatusMaster] AS SM ON SM.[AutoId] = OM.[Status] and SM.Category='OrderMaster'
		WHERE OM.[AutoId] = @OrderAutoId                                                                 
		SET @LogRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=3),'[Status1]','Packed')
		SET @LogRemark = REPLACE(@LogRemark,'[Status2]','Ready to ship')                                                                                                           
		INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                                              
		VALUES(3,@EmpAutoId,getdate(),@LogRemark,@OrderAutoId)                                                                                                                                                    
                                                                                                              
		SET @LogRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=5), '[DriverName]',                                                                                                 
		(SELECT [FirstName] + ' ' + [LastName] AS DrvName FROM [dbo].[EmployeeMaster] WHERE [AutoId] = @DriverAutoId))                                                                                                                            
		INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                                                                                          
		VALUES(5,@EmpAutoId,getdate(),@LogRemark,@OrderAutoId)  
   
		Select @OrderAutoId as AutoId      
		Select 'Driver has been changed' as msg       
		EXEC [dbo].[ProcAutomaticSendEmail]  @Opcode=12,@CustomerAutoId=@CustomerAutoId, 
		@OrderAutoId=@OrderAutoId,@isException=0,@exceptionMessage=''
		 END
	 ELSE
	 BEGIN                                   
		Set @isException=1                                                                                                            
		Set @exceptionMessage='Unauthorized Access.'
	 END
	 END
	 ELSE
	 BEGIN                                   
		Set @isException=1                                                                                                            
		Set @exceptionMessage='Unauthorized Access.'
	 END
   END                                                 
   ELSE                                                  
   BEGIN                                    
		Set @isException=1                                                                                                            
		Set @exceptionMessage='Some Product have unit price less than min Price.Please fix it and try again.'                                                                                    
   END                                                                                
  COMMIT TRANSACTION                                                              
  END TRY                                                                                                                                     
  BEGIN CATCH                                                                                                                            
  ROLLBACK TRAN                                                                                                                                              
   Set @isException=1                                                                             
   Set @exceptionMessage='Oops,some thing went wrong .Please try again'                                                                                                
  END CATCH                                                                               
 END                                                                                                                                                  
   ELSE IF @Opcode = 2021                                                                                                                                                    
 BEGIN                                                                                                
  BEGIN TRY                                                                                                                                                    
  BEGIN TRAN                                                               
  SET @OrderStatus  =(SELECT [Status] FROM [dbo].[OrderMaster] WHERE [AutoId] = @OrderAutoId)                                                     
  UPDATE [dbo].[OrderMaster] SET PackerAutoId = @PackerAutoId,PackerAssignDate=getdate(),                                                                                           
  WarehouseAutoId=@EmpAutoId,WarehouseRemarks=@Remarks,                                                                                    
  PackerAssignStatus =1,Times =@Times,                                                                                              
  [Status] =(case when [Status]=1 then 2 else Status end)                                                                                        
  WHERE [AutoId] = @OrderAutoId                                                                                                                                                    
  SET @LogRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=18), '[PackerName]', (SELECT [FirstName] + ' ' + [LastName] AS packername FROM [dbo].[EmployeeMaster] WHERE [AutoId] = @PackerAutoId))                            
  INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                                                                           
  VALUES(18,@EmpAutoId,getdate(),@LogRemark,@OrderAutoId)                                                                     
  IF(@OrderStatus=1)                                                                                                                                                    
  BEGIN                                                                                                                                                         
  SET @LogRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=3),'[Status1]','New')                                                            
  SET @LogRemark = REPLACE(@LogRemark,'[Status2]','Processed.')                                                                
  INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                                                                                                                                    
  VALUES(3,@EmpAutoId,getdate(),@LogRemark,@OrderAutoId)                                                                           
  END              
                
	SELECT @CustomerAutoId=CustomerAutoId FROM [dbo].[OrderMaster]                 
	WHERE [AutoId] = @OrderAutoId                
	if((select CustomerType from CustomerMaster where AutoId=@CustomerAutoId)=3)                      
	begin                      
	exec [dbo].[ProcUpdatePOStatus_All]                      
	@CustomerId=@CustomerAutoId,                      
	@OrderAutoId=@OrderAutoId,                      
	@Status=2                          
	end                                                                                                                        
                                         
  COMMIT TRANSACTION                                                                                                                                                    
  END TRY                                                                          
  BEGIN CATCH                                                                                 
  ROLLBACK TRAN                                                         
  Set @isException=1                                                                                            
  Set @exceptionMessage='Oops! Something went wrong.Please try later.'                                                                                        
  END CATCH                                                                                                
 END                                                                                                                                                  
 ELSE IF @Opcode = 203                                                                                                           
  BEGIN                                                                                                                     
  BEGIN TRY                                     
  BEGIN TRAN                                   
  if((select count(Status) FROM [dbo].[OrderMaster] WHERE [AutoId] = @OrderAutoId and status in (3,4,9,10))=0)  
 BEGIN                                                                          
	SET @isException=1                                                                                                                                              
	SET @exceptionMessage='Order has been shipped so you can not update.'                                        
 END 
 ELSE
 BEGIN 
 
  DECLARE @oldOrderstatus INT=(SELECT Status from OrderMaster where AutoId=@OrderAutoId)
  DECLARE @COUNT INT=(SELECT COUNT(*) FROM OrderItemMaster WHERE [OrderAutoId] = @OrderAutoId)                                                                    
  DECLARE @COUNT1 INT=(SELECT COUNT(*) FROM @TableValue)                                                                                   
                                                                                                   
  declare @Remark varchar(500)                                                        
  SET @CustomerAutoId =(SELECT CustomerAutoId FROM OrderMaster WHERE AutoId=@OrderAutoId)                                                                                                                 
  SET @CreditAmount=ISNULL((select CreditAmount from OrderMaster where AutoId=@OrderAutoId),0.00)                                                                                   
                                                                                                                                     
  SET @CreditMemoAmount=ISNULL((select Deductionamount from OrderMaster where AutoId=@OrderAutoId),0.00)                                                                                                                
  IF (@CreditAmount<0)                                                                              
  BEGIN                                   
	update CustomerCreditMaster set CreditAmount=isnull(CreditAmount,0)+Isnull(@CreditAmount,0) where CustomerAutoId= @CustomerAutoId     
  END                                                                                                                                          
  DELETE FROM tbl_Custumor_StoreCreditLog WHERE ReferenceType='OrderMaster' AND ReferenceNo=CONVERT(VARCHAR(10),@OrderAutoId)                                                                                                      
  SET @BillAddrAutoId=(SELECT TOP 1 BillAddrAutoId FROM ordermaster WHERE AutoId =@OrderAutoId)       
  SET @State=(SELECT TOP 1 State FROM BillingAddress WHERE AutoId =@BillAddrAutoId) 
 
  DECLARE @ShippingTaxEnabled int=(SELECT EnabledTax FROM ShippingType WHERE AutoId=@ShippingType)

  UPDATE [dbo].[OrderMaster] SET         
  TaxType=@TaxType,            
  TaxValue= (select Value from TaxTypeMaster where autoid=@TaxType),       
  IsTaxApply=CASE WHEN (select count(1) from TaxTypeMaster where State=@State)=0 then 0 else 1 end,      
  [DeliveryDate]  = @DeliveryDate,                                                                 
  [OverallDiscAmt] = @OverallDiscAmt,                                 
  [ShippingCharges] = @ShippingCharges,                                  
  [Driver]   = (case when Status<=3 then null else Driver end),                                                                                                                                                    
  [AssignDate]  =(case when Status<=3 then null else [AssignDate] end),                                                                                         
  ManagerAutoId=case when @LoginEmpType=8 then @EmpAutoId else ManagerAutoId end , ManagerRemarks=@ManagerRemarks,                                                                                                  
  [ShippingType]= CASE WHEN ISNULL(@ShippingType,0)=0 THEN ShippingType ELSE @ShippingType END,                                                                                        
  PackedBoxes=(case when Status in (3,4) then (CASE WHEN ISNULL(@PackedBoxes,0)=0 then PackedBoxes else @PackedBoxes end) else PackedBoxes end) ,    
  ShippingTaxEnabled=@ShippingTaxEnabled
  WHERE [AutoId] = @OrderAutoId                                                                                                        
  declare @check int=0                                                                 
	if EXISTS(select * from CreditMemoMaster where OrderAutoId=@OrderAutoId)                                                      
	BEGIN                                                                                                                               
		SET @check=1                                                                                       
		update CreditMemoMaster set OrderAutoId=null,CompletedBy=null,CompletionDate=null where OrderAutoId=@OrderAutoId                                                                                                                              
	END                                                             
	                                                                                                                               
		DECLARE @i INT=0                                           
		SELECT ROW_NUMBER() OVER (ORDER BY ProductAutoId DESC) AS ROWNo,* INTO #ADDON  FROM @TableValue                                                                                                                   
		SET @i=1                                                                                                                                                    
		WHILE @i<=(SELECT COUNT(*) FROM @TableValue)                                                                                         
		BEGIN                                                                                                                                     
			SET @ProductAutoId=(SELECT ProductAutoId FROM #ADDON WHERE ROWNo=@i)                                                                                                                                                
			SET @UnitAutoId=(SELECT UnitAutoId FROM #ADDON WHERE ROWNo=@i)                                                                                                                                     
			SET @ReqQty=(SELECT RequiredQty FROM #ADDON WHERE ROWNo=@i)                                                                                                                                                  
			SET @IsExchange=(SELECT IsExchange FROM #ADDON WHERE ROWNo=@i)                                                                                                                                                    
			SET @IsFreeItem=(SELECT IsFreeItem FROM #ADDON WHERE ROWNo=@i)
				IF NOT EXISTS(SELECT * FROM OrderItemMaster WHERE OrderAutoId=@OrderAutoId AND ProductAutoId=@ProductAutoId AND UnitTypeAutoId=@UnitAutoId and IsExchange=@IsExchange AND IsFreeItem=@IsFreeItem AND @ReqQty<=RequiredQty)    
				BEGIN                                                                                                                               
					SET @i=(SELECT COUNT(*) FROM @TableValue)                                                                                                                                   
					UPDATE OrderMaster SET Status=9 WHERE  [AutoId] = @OrderAutoId                                                                                                                                                    
				END                                                                           
			SET @i=@i+1                                                                                                  
		  END                                                 
	                                                                             
	update CreditMemoMaster set OrderAutoId=@OrderAutoId ,PaidAmount=TotalAmount ,CompletedBy=@EmpAutoId,CompletionDate=GetDate()                                
	where CreditAutoId in (select * from dbo.fnSplitString(@creditNo,','))                                          
  if EXISTS(select * from CreditMemoMaster where OrderAutoId=@OrderAutoId)                                                                                                                                                    
  BEGIN                                                                                                                                  
  IF(@check=0)                                                                                
  BEGIN                                                                                
  SET @Remark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=15),                                                                     
  '[OrderNo]',  (SELECT OrderNo FROM OrderMaster where AutoId = @OrderAutoId))                                                                                                                        
  INSERT INTO CreditMemoLog(CreditAutoId,ActionAutoId,EmpAutoId,Remarks,LogDate)                                                                                                       
  select splitdata,15,@EmpAutoId, @Remark,GETDATE() from dbo.fnSplitString(@creditNo,',')                                                   
                                                                         
  SET @LogRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=15),'[OrderNo]',                                      
  (SELECT [OrderNo] FROM OrderMaster where [AutoId] = @OrderAutoId))                                                                                                                                                         
  INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                            
  VALUES(15,@EmpAutoId,getdate(),@LogRemark,@OrderAutoId)                                                                                 
  END                                                                                  
  SET @check=0                                                                                
  END                                                                                    
                                                                                 
  IF(@check=1)                                                                              
  BEGIN                                                  
	  SET @Remark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=16),                                                                                                               
	  '[OrderNo]',  (SELECT OrderNo FROM OrderMaster where AutoId = @OrderAutoId))                                                                          
	  INSERT INTO CreditMemoLog(CreditAutoId,ActionAutoId,EmpAutoId,Remarks,LogDate)                                                                
	  select splitdata,16,@EmpAutoId, @Remark,GETDATE() from dbo.fnSplitString(@creditNo,',')                                                                                
	  SET @LogRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=16),'[OrderNo]',                                                                             
	  (SELECT [OrderNo] FROM OrderMaster where [AutoId] = @OrderAutoId))                                                                                                                                                         
	  INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                                                                                                                            
	  VALUES(16,@EmpAutoId,getdate(),@LogRemark,@OrderAutoId)                               
  END                                                                                
 

  --UMANG3          
  select * into #tempOIMSM from [dbo].[OrderItemMaster] where [OrderAutoId] = @OrderAutoId                                                        
                                                                               
 Declare @Stat int      
 Select @Stat=Status From OrderMaster WHERE  [AutoId] = @OrderAutoId                          
                    
	update  oim set oim.[UnitTypeAutoId]=rdt.[UnitAutoId],oim.[QtyPerUnit]=
	rdt.[QtyPerUnit],[UnitPrice]=rdt.[UnitPrice],oim.[RequiredQty]=rdt.[RequiredQty],oim.[SRP]=rdt.[SRP],oim.[GP]=rdt.[GP],oim.[Tax]=rdt.[Tax],oim.[Barcode]=rdt.[Barcode],
	oim.[QtyShip]=rdt.[QtyShip],oim.[RemainQty]=(rdt.[RequiredQty] - rdt.[QtyShip]),oim.IsExchange=rdt.[IsExchange],
	oim.UnitMLQty=case when pm.IsApply_ML=1 and oim.IsExchange=0 then ISNULL(pm.MLQty,0) else 0 end,
	oim.isFreeItem=rdt.[IsFreeItem],Oim_Discount=rdt.OM_Discount,
	oim.Weight_Oz=case when pm.IsApply_Oz=1 and oim.IsExchange=0  then ISNULL(pm.WeightOz,0) else 0 end,
	oim.AddOnQty= CASE WHEN rdt.[RequiredQty] = oim.[RequiredQty] then ISNULL(AddOnQty,0) else 0  end +case when rdt.[RequiredQty] > oim.[RequiredQty] then rdt.[RequiredQty]-oim.[RequiredQty] else 0 end
	from [OrderItemMaster] as oim
	inner join @TableValue  as rdt on rdt.[ProductAutoId]=oim.[ProductAutoId]
	inner join ProductMaster as pm on pm.AutoId=rdt.ProductAutoId where rdt.ItemType='Old' and oim.[AutoId]=rdt.[AutoId]
                                                                                                           
	INSERT INTO [dbo].[OrderItemMaster]([OrderAutoId],[ProductAutoId],[UnitTypeAutoId],[QtyPerUnit],[UnitPrice],                                                                                                 
	[RequiredQty],[SRP],[GP],[Tax],[Barcode],[QtyShip],[RemainQty],IsExchange,UnitMLQty,isFreeItem,
	Weight_Oz,AddOnQty,OM_CostPrice,OM_MinPrice,BasePrice,Oim_Discount)                                                                              
	SELECT @OrderAutoId,tb.[ProductAutoId],tb.[UnitAutoId],tb.[QtyPerUnit],tb.[UnitPrice],tb.[RequiredQty]                                  
	,pm.[P_SRP],tb.[GP],tb.[Tax],tb.[Barcode],tb.[QtyShip],(tb.[RequiredQty] - tb.[QtyShip])                                                             
	,IsExchange,
	case when pm.IsApply_ML=1 and IsExchange=0 then ISNULL(pm.MLQty,0) else 0 end,
	isFreeItem,
	case when pm.IsApply_Oz=1 and IsExchange=0  then ISNULL(pm.WeightOz,0) else 0 end,
	case when @Stat=9 then Isnull(RequiredQty-QtyShip,0) else 0 end,
	tb.OM_CostPrice,tb.OM_MinPrice,tb.OM_BasePrice,OM_Discount FROM @TableValue AS tb                                                                                                           
	inner join ProductMaster as pm on pm.AutoId=tb.ProductAutoId
	inner join PackingDetails as pd on pd.ProductAutoId=pm.AutoId and pd.UnitType=tb.UnitAutoId
	where tb.ItemType='New' 
		

	 
            
	Update OIM Set          
	OIM.Original_UnitType=dt.Original_UnitType          
	from [dbo].[OrderItemMaster] as OIM           
	inner join (SELECT * FROM #tempOIMSM) AS dt On dt.[ProductAutoId] = OIM.[ProductAutoId] and dt.[isFreeItem] = OIM.[isFreeItem]          
	and dt.[OrderAutoId]=OIM.[OrderAutoId] and dt.[Tax] = OIM.[Tax] and dt.[IsExchange] = OIM.[IsExchange]          
	WHERE OIM.OrderAutoId = @OrderAutoId          
          
     if EXISTS(Select Autoid from OrderMaster  WHERE  [AutoId] = @OrderAutoId AND  Status=9)
	 BEGIN
	   update OrderItemMaster set AddOnPackedPeice =ISNULL((QtyPerUnit*ISNULL(QtyShip,0)),0) where OrderAutoId=@OrderAutoId
	 END
            
	if EXISTS(Select Autoid from OrderMaster  WHERE  [AutoId] = @OrderAutoId AND  Status=9 and (@oldOrderstatus=3 or @oldOrderstatus=4 or @oldOrderstatus=10) )
	BEGIN

		 
		SET @SalesPersonAutoId=(SELECT SalesPersonAutoId FROM OrderMaster WHERE AutoId=@OrderAutoId) 
		IF EXISTS(Select * from AllocatedPackingDetails where OrderAutId=@OrderAutoId)
		BEGIN
				DELETE FROM AllocatedPackingDetails where OrderAutId=@OrderAutoId
		END
	END
  SET @CreditMemoAmount=ISNULL((select Deductionamount from OrderMaster where AutoId=@OrderAutoId),0.00)                                                                                                       
  SET @GrandTotal=ISNULL((select GrandTotal from OrderMaster where AutoId=@OrderAutoId),0.00)                                                                        
  if(@CreditMemoAmount>@GrandTotal)                                                                                                    
  BEGIN                                                                                                    
   INSERT INTO tbl_Custumor_StoreCreditLog(CustomerAutoId,ReferenceType,ReferenceNo,Amount,CreatedDate,CreatedBy)                                                                
   values(@CustomerAutoId,'OrderMaster',@OrderAutoId,-(@CreditMemoAmount-@GrandTotal),getdate(),@EmpAutoid)                                                                                                      
   IF EXISTS(SELECT TOP 1 * FROM CustomerCreditMaster WHERE CustomerAutoId=@CustomerAutoId)                                                                                                    
   BEGIN                                                                                                    
    UPDATE CustomerCreditMaster SET CreditAmount=ISNULL(CreditAmount,0.00)+((@CreditMemoAmount-@GrandTotal))                                                                                                    
    WHERE CustomerAutoId=@CustomerAutoId                                                                                
   END                                                                                                    
   ELSE                                                                                                                              
   BEGIN                                                                    
    INSERT INTO CustomerCreditMaster(CustomerAutoId,CreditAmount)                                                                              
    VALUES(@CustomerAutoId,((@CreditMemoAmount-@GrandTotal)))                                                                                                                          
   END                                                                                                     
  END                                                                                        
  SET @PriceLevelAutoId = (SELECT [PriceLevelAutoId] FROM [dbo].[CustomerPriceLevel] WHERE [CustomerAutoId] = @CustomerAutoId)                                                
  exec UPDTAE_PRICELEVEL                                                                                                             
  @OrderAutoId=@OrderAutoId,                
  @PriceLevelAutoId=@PriceLevelAutoId,
  @EmpAutoId=@EmpAutoId
  SET @LogRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=2),'[OrderNo]',(SELECT [OrderNo] FROM OrderMaster where [AutoId] = @OrderAutoId))                                                                                
 
  		INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                                                              
		select 2,@EmpAutoId,getdate(),CONVERT(varchar(50),pm.productId)+' has been added'+ 
		case when tb.IsExchange=1 then ' as Exchange' when tb.IsFreeItem=1 then ' as Free' else '' end as tp,
		@OrderAutoId from @TableValue as tb                                                                                                            
		inner join ProductMaster as pm on pm.AutoId=tb.ProductAutoId 
		where tb.ItemType='New' 

		INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                                                              
		select 2,@EmpAutoId,getdate(),CONVERT(varchar(50),pm.productId)+' has been updated',@OrderAutoId from @TableValue as tb                                                                                                            
		inner join ProductMaster as pm on pm.AutoId=tb.ProductAutoId 
		inner join OrderItemMaster oim on oim.ProductAutoId=tb.ProductAutoId
		where tb.ItemType='Old' and tb.NetPrice != oim.NetPrice and oim.AutoId=@OrderAutoId

		
		
		INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])    
		select 2,@EmpAutoId,getdate(),CONVERT(varchar(50),pm.productId)+' has been updated with '+ convert(varchar(max),oim.RequiredQty-tmp.RequiredQty)+' quantity',@OrderAutoId from OrderItemMaster as oim                                                                                                            
		inner join ProductMaster as pm on pm.AutoId=oim.ProductAutoId
		inner join #tempOIMSM tmp on tmp.AutoId=oim.AutoId
		where tmp.RequiredQty < oim.RequiredQty
		
   Drop table #tempOIMSM    

  if((select CustomerType from CustomerMaster where AutoId=@CustomerAutoId)=3)                  
  begin                  
 exec [dbo].[ProcUpdatePOStatus_All]                  
  @CustomerId=@CustomerAutoId,                  
  @OrderAutoId=@OrderAutoId,                  
  @Status=9                      
  end              
                                           
  END                                                               
  COMMIT TRANSACTION                                                                 
  END TRY                                                                                              
  BEGIN CATCH                                                 
  ROLLBACK TRAN                                                                                                                       
   Set @isException=1                                                                                                                                                    
   Set @exceptionMessage=ERROR_MESSAGE();                                                                                                                                                    
  END CATCH                                                                                                               
 END                                                           
                                                  
  ELSE IF @Opcode=204                                                                        
     BEGIN                                                                                                             
			SET @CreditAmount=ISNULL((SELECT CreditAmount FROM OrderMaster WHERE AutoId=@OrderAutoId),0)                                                                                                                  
			SET @CustomerAutoId=(SELECT CustomerAutoId FROM OrderMaster WHERE AutoId=@OrderAutoId)                    
			IF(ISNULL((SELECT CreditAmount FROM OrderMaster WHERE AutoId=@OrderAutoId ),0)!=0)                                                                                                   
			BEGIN                                                                                                                                                
			UPDATE CustomerCreditMaster SET CreditAmount=ISNULL(CreditAmount,0)+@CreditAmount WHERE CustomerAutoId=@CustomerAutoId      
			END                                                                                                               
			SET @DeductAmount =ISNULL((SELECT Deductionamount FROM OrderMaster WHERE AutoId=@OrderAutoId),0)                                                                                                                                              
			SET @GrandTotal=ISNULL((SELECT GrandTotal FROM OrderMaster WHERE AutoId=@OrderAutoId),0)                                                                                                 
			if(@DeductAmount>@GrandTotal )                                                                                                                                  
			BEGIN                                                                                                
			UPDATE CustomerCreditMaster SET CreditAmount=ISNULL(CreditAmount,0)-ISNULL((@DeductAmount-@GrandTotal),0) WHERE CustomerAutoId=@CustomerAutoId   
			END                                                                                     
			DELETE from tbl_Custumor_StoreCreditLog where ReferenceNo=CONVERT(varchar(50),@OrderAutoId) and ReferenceType='OrderMaster'                                                                                                               
			UPDATE CreditMemoMaster set OrderAutoId=null where OrderAutoId=@OrderAutoId 
			 
			IF NOT EXISTS(SELECT AutoId FROM OrderMaster WHERE AutoId=@orderAutoId AND Status IN (1,2))  
			BEGIN 
			insert into [dbo].[ProductStockUpdateLog]([ProductId],[OledStock],[NewStock],[UserAutoId],[DateTime],ReferenceId,ActionRemark,ReferenceType)
			SELECT pm.ProductId,isnull([Stock],0),(isnull(pm.Stock,0)+(isnull(dt.QtyPerUnit,0) * isnull(dt.QtyShip,0))),@EmpAutoId,GETDATE(),dt.OrderAutoId,
			'Order No-(' + om.OrderNo + ') has cancelled by '+(Select FirstName+' '+LastName from Employeemaster where autoid=@EmpAutoId)+'.' ,'OrderMaster'
			FROM OrderItemMaster as dt
			inner join productmaster pm on dt.[ProductAutoId] = PM.[AutoId]
			inner join OrderMaster om on dt.OrderAutoId=om.AutoId
			where dt.OrderAutoId=@OrderAutoId and isnull(dt.QtyShip,0)>0 

			UPDATE PM SET [Stock] = isnull([Stock],0) + (isnull(dt.QtyPerUnit,0)*isnull(dt.QtyShip,0)) FROM [dbo].[ProductMaster] AS PM                                                                                                   
			INNER JOIN (SELECT QtyShip,QtyPerUnit,ProductAutoId FROM OrderItemMaster where OrderAutoId=@OrderAutoId) AS dt 
			ON dt.[ProductAutoId] = PM.[AutoId]
				 
			End
			                               
			UPDATE [dbo].[OrderMaster] SET [Status]=8,UPDATEDATE=GETDATE(),UPDATEDBY=@EmpAutoId,ManagerAutoId=@EmpAutoId,ManagerRemarks=@Remarks WHERE [AutoId]=@OrderAutoId  
			                              
			SELECT OM.[Status],SM.[StatusType] FROM [dbo].[OrderMaster] AS OM                             
			INNER JOIN [dbo].[StatusMaster] As SM ON SM.[AutoId] = OM.[Status] WHERE OM.[AutoId]=@OrderAutoId                                                                              
                                              
                              
			INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                      
			VALUES(7,@EmpAutoId,getdate(),'Cancelled',@OrderAutoId)     
    
			IF((select CustomerType from CustomerMaster where AutoId=@CustomerAutoId)=3)              
			Begin              
			exec [dbo].[ProcUpdatePOStatus_All]              
			@CustomerId=@CustomerAutoId,              
			@OrderAutoId=@OrderAutoId,              
			@Status=8                  
			End 
			
			if((SELECT OM.[Status] FROM [dbo].[OrderMaster] AS OM   WHERE OM.[AutoId]=@OrderAutoId) >2)
			BEGIN
			UPDATE AQ SET AQ.AllowQtyInPieces = AllowQtyInPieces+isnull(TotalPieces,0) FROM [dbo].[AllowQtyPiece] AS AQ                                                                                                  
			INNER JOIN
			(
			SELECT SUM(OIM.[AllocatedQty]) as TotalPieces,OM.SalesPersonAutoId,ProductAutoId  FROM [AllocatedPackingDetails] OIM
			INNER JOIN OrderMaster OM ON OM.AutoId=OIM.OrderAutId
			where OIM.OrderAutId=@OrderAutoId  
			group by SalesPersonAutoId,ProductAutoId
			) AS dt ON AQ.SalesPersonAutoId=dt.SalesPersonAutoId and AQ.ProductAutoId=dt.ProductAutoId
			END
                                                                                
   END                                                                                                                                
  --ELSE IF @Opcode=301                                                                                                     
  -- BEGIN                                                                                                                                                   
                                                                                                    
  --  DELETE FROM  [dbo].[OrderItems_Original] WHERE  [OrderAutoId] = @OrderAutoId                                                                                
  --  DELETE FROM  [dbo].[Order_Original] WHERE [AutoId] = @OrderAutoId                                                                                                                   
  --  DELETE FROM [dbo].[OrderItemMaster] WHERE [OrderAutoId] = @OrderAutoId                     
  --  DELETE FROM  [dbo].[GenPacking] WHERE [OrderAutoId] = @OrderAutoId                                                                                                                                                    
  --  DELETE FROM [dbo].[DrvLog] WHERE [OrderAutoId] = @OrderAutoId                                                                                                                                                    
  --  DELETE FROM [dbo].[OrderMaster] WHERE [AutoId] = @OrderAutoId                                                                                                                                                    
  -- END      
   ELSE IF @Opcode=4001                                                                                                     
   BEGIN                                                                                                                                                   
	select(select Zipcode as ZM,CityId,zm.AutoId,Zipcode+' [ '+CM.CityName+' ] ' as ZipCode from ZipMaster as ZM  
	INNER JOIN CityMaster CM ON ZM.CityId=CM.AutoId  WHERE ZM.Status=1  ORDER BY REPLACE(' [ '+CM.CityName+' ] ',' ','') ASC for json path, INCLUDE_NULL_VALUES) as ZipCode1
	for json path, INCLUDE_NULL_VALUES                                                                                                                                               
   END      
  ELSE IF @Opcode=302                                                                              
   BEGIN                                                                                                                                                     
    IF(@AddressType=11)                                                                                                                                               
    BEGIN                                          
     DELETE FROM [dbo].[BillingAddress] WHERE [AutoId]=@AddressAutoId                                                                                      
    END                                                                                                                                     
    ELSE IF(@AddressType = 22)                                                                                                             
    BEGIN                                                                   
     DELETE FROM [dbo].[ShippingAddress] WHERE [AutoId]=@AddressAutoId                                                                                         
    END                                                                                       
   END                                                                         
  --ELSE IF @Opcode=401                                                                                                                                           
  -- BEGIN                                                                                                      
  --  SELECT [AutoId],[StatusType] FROM [dbo].[StatusMaster] WHERE [AutoId]=1 and [Category]='OrderMaster'                                                                                                                                                     
  --  IF(@LoginEmpType != 2)                                                                                            
  --   BEGIN                                                                                                       
  --    SELECT [AutoId],[CustomerId] + ' - ' + [CustomerName] As Customer FROM [dbo].[CustomerMaster] where Status=1       
  -- Order by Customer                                                                                                                       
  --   END                                                                                                                                                    
  --  ELSE          
  --  BEGIN                                                                                                                                                
  --    SELECT [AutoId],[CustomerId] + ' - ' + [CustomerName] As Customer FROM [dbo].[CustomerMaster] WHERE [SalesPersonAutoId]=@SalesPersonAutoId        
  -- and status=1   ORDER BY Customer  ASC         
                   
                      
  --  END                                                                                   
  --SELECT [AutoId],convert(varchar(10),[ProductId]) + '--' + [ProductName] as [ProductName],ISNULL(MLQty,0) AS MLQty, isnull(WeightOz,0)                      
  --as WeightOz                                                                                                                                   
  --FROM [dbo].[ProductMaster] as pm where ProductStatus=1                                                               
  --and (select count(1) from PackingDetails where  ProductAutoId=pm.AutoId)>0                                                                                                                           
  --order by [ProductName] ASC                                                                                                         
  --  SELECT * FROM [dbo].[ShippingType] where Shippingstatus=1  ORDER BY ShippingType  ASC                                                                                                                  
                                                                                                                                                        
  -- END                                              
    ELSE IF @Opcode=4011                                                                                                                                           
   BEGIN                                                                                                       
  select(
	select
		isnull((SELECT [AutoId],[StatusType] FROM [dbo].[StatusMaster] WHERE [AutoId]=1 and [Category]='OrderMaster' for json path, INCLUDE_NULL_VALUES),'[]') as Status,
		isnull((SELECT [AutoId],[CustomerId] + ' - ' + [CustomerName] As Customer FROM [dbo].[CustomerMaster] WHERE [SalesPersonAutoId]=@SalesPersonAutoId        
		and status=1 and @LoginEmpType = 2 ORDER BY Customer  ASC for json path, INCLUDE_NULL_VALUES),'[]') as Customer2,
		isnull((SELECT * FROM [dbo].[ShippingType] where Shippingstatus=1 ORDER BY ShippingType  ASC  for json path, INCLUDE_NULL_VALUES),'[]') as ShippingType
	
	for json path, INCLUDE_NULL_VALUES
)as DropDownList                                                                                                                                                   
   END    
   
   ELSE IF @Opcode=4051                                                                                                                                          
   BEGIN                                                                                                       
   select
   (
	  SELECT [AutoId] as A,convert(varchar(10),[ProductId]) + '--' + [ProductName] as PN,ISNULL(MLQty,0) AS ML, isnull(WeightOz,0)                      
		as Oz FROM [dbo].[ProductMaster] as pm where ProductStatus=1 and (select count(1) from PackingDetails where PackingStatus=1 and ProductAutoId=pm.AutoId)>0                                                                                                                           
		order by PN ASC for json path, INCLUDE_NULL_VALUES
		) as Product	
	for json path, INCLUDE_NULL_VALUES                                                                                                                                                 
   END  

  ELSE IF @Opcode=402              
   BEGIN                                          
    SELECT BA.[AutoId] As BillAddrAutoId,BA.[Address],St.[StateName] AS State,BA.[City],BA.[Zipcode] FROM [dbo].[BillingAddress] AS BA                                                                    
    INNER JOIN [dbo].[State] AS St ON St.AutoId = BA.State WHERE BA.[CustomerAutoId] = @CustomerAutoId AND BA.[IsDefault]=1                                                                    
    SELECT SA.[AutoId] As ShipAddrAutoId,SA.[Address],St.[StateName] AS State,SA.[City],SA.[Zipcode]                                                                                                                                                     
    FROM [dbo].[ShippingAddress] AS SA                                                                                                                
    INNER JOIN [dbo].[State] AS St ON St.AutoId = SA.State WHERE SA.CustomerAutoId = @CustomerAutoId AND SA.[IsDefault]=1                                                                                                                   
                                                                                                                                    
    SELECT [AutoId],[OrderNo],CONVERT(VARCHAR(20),[OrderDate],101) AS OrderDate,DO.[AmtDue],DO.[GrandTotal],DO.[AmtPaid]                                                                                                         
    FROM [dbo].[OrderMaster] As OM                                                     
    INNER JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId]                                  
    WHERE [CustomerAutoId] = @CustomerAutoId AND [Status] = 6 AND OM.[AmtDue] != 0.00                                                                                        
                                                                                                                                                    
    SELECT CT.[TermsDesc] FROM CustomerMaster AS CM                                                                                                                    
    LEFT JOIN CustomerTerms AS CT ON CT.[TermsId] = CM.[Terms] WHERE [AutoId] = @CustomerAutoId                                                                             
    SET @AddressAutoId = (SELECT TOP 1 State FROM BillingAddress  WHERE CustomerAutoId=@CustomerAutoId)  
	
	SELECT * FROM TaxTypeMaster WHERE status=1           
	
    SELECT ISNULL((SELECT TaxRate FROM MLTaxMaster WHERE TaxState=@AddressAutoId),0) as TaxRate,                                                                                                                                                  
    CASE WHEN (SELECT TaxRate FROM MLTaxMaster WHERE TaxState=@AddressAutoId) IS NULL THEN 0 ELSE 1 END                                              
    AS MLTaxType                                     
                                   
  select * from Tax_Weigth_OZ                                                                                                                                
   END                                                                                                 
  ELSE IF @Opcode=403                                                            
   BEGIN                       
	DECLARE @custType int=(SELECT CustomerType FROM CustomerMaster WHERE AutoId=@CustomerAutoId)                                                                   
	SELECT (CASE                                                                                                                                                     
	WHEN @custType=2 THEN ISNULL(WHminPrice,0)                                                                                                                                                    
	WHEN @custType=3  THEN ISNULL(CostPrice,0)                                                                                                                   
	ELSE [MinPrice] END) AS [MinPrice] ,[CostPrice],PD.Price as BasePrice,(case  WHEN  @custType=3 then ISNULL(CostPrice,0) else [Price] end)                                        
	[Price],CONVERT(DECIMAL(18,2),((PM.[P_SRP]-([Price]/(CASE WHEN Qty=0 THEN 1 ELSE [Qty] END)))/(CASE WHEN PM.[P_SRP]=0 THEN 1 ELSE PM.[P_SRP] END)) * 100) AS GP,    
	isnull((select Top 1 [CustomPrice] from [dbo].[ProductPricingInPriceLevel] AS PPL where PPL.[ProductAutoId] = PD.[ProductAutoId]                       
	AND PPL.[UnitAutoId] = PD.[UnitType]                                                                                                         
	AND [PriceLevelAutoId] = (SELECT [PriceLevelAutoId] FROM [dbo].[CustomerPriceLevel] WHERE [CustomerAutoId]=@CustomerAutoId)                                                                                  
	Order by [CustomPrice] desc),null) as [CustomPrice],PM.[Stock]/(CASE WHEN Qty=0 THEN 1 ELSE Qty END) as Stock,PM.[TaxRate],PM.[P_SRP] AS [SRP] FROM [dbo].[PackingDetails] As PD                        
	INNER JOIN [dbo].[ProductMaster] AS PM ON PD.[ProductAutoId]=PM.[AutoId]                                                                                                                                                                       
	WHERE PD.[ProductAutoId] = @ProductAutoId and PD.[UnitType] = @UnitAutoId                                                              
   END                                                                                           
  ELSE IF @Opcode=404                                                                                                                                    
 BEGIN                                                                                                                                                    
    SELECT UM.AutoId AS AutoId, UM.[UnitType] AS UnitType, [Qty],ISNULL(EligibleforFree,0) as EligibleforFree FROM [dbo].[PackingDetails] AS PD                                                      
    INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = PD.UnitType WHERE PD.[ProductAutoId] = @ProductAutoId   ORDER BY  UM.[UnitType] ASC                                                                                                          
    SELECT PackingAutoId AS AutoId FROM ProductMaster  WHERE  AutoId=@ProductAutoId                                                       
  END                                                                                                                                                       
  ELSE IF @Opcode=405                                                                                                                                               
   BEGIN                                                                               
    SELECT ROW_NUMBER() OVER(ORDER BY                                                                                  
    CASE @LoginEmpType WHEN 3 THEN OrderName ELSE [OrderNo] END DESC,                                                                                                            
    CASE @LoginEmpType WHEN 3 THEN [OrderDate] ELSE [OrderDate] END DESC,                                 
 CASE @LoginEmpType WHEN 3 THEN OrderNo ELSE OrderNo END DESC                                     
    ) AS RowNumber, * INTO #Results from                                                               
    (  SELECT OM.[AutoId],OM.OrderNo,CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,CM.[CustomerName] AS CustomerName,                   
    SM.StatusType AS Status,OM.[Status] As StatusCode,OM.[GrandTotal],(emp.FirstName + ' '+ emp.LastName) as SalesPerson,                               
 --Satart                                                                                      
 (Select ShippingType from ShippingType Where AutoId=om.ShippingType) as ShippingType,OM.ShippingType as ShipId,                                                                                        
 --End 30-08-2019 By Rizwan Ahmad Add Shipping Type col                                                                     
    (SELECT COUNT(distinct [ProductAutoId]) from [dbo].[OrderItemMaster] WHERE OrderAutoId=OM.AutoId) AS NoOfItems ,                                                                                                                                 
    (                                                                                                                                                    
      CASE WHEN OM.[Status]= 9 THEN '4'                                                                                                                                                     
        WHEN OM.[Status]= 1 THEN '3'                                               
        WHEN OM.[Status]= 2 THEN '2'                                                                                                             
        WHEN OM.[Status]= 3 THEN '1'                                       
     END                                                   
    ) OrderName,                                                                                                         
    (select COUNT(*) from CreditMemoMaster as cmm where (Status=1 or Status=3)  and OrderAutoId is null                                                                                                                                             
  and cmm.CustomerAutoId = OM.CustomerAutoId ) as CreditMemo                                                                                                         
 FROM [dbo].[OrderMaster] As OM                                                                                                                                                    
    INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.AutoId = OM.CustomerAutoId                                               
    INNER JOIN [dbo].[EmployeeMaster] AS emp ON emp.AutoId = OM.SalesPersonAutoId                                                                        
    INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.[Status] AND SM.Category = 'OrderMaster'                                                                                                                                                          
  
    
     
    WHERE (@OrderNo is null or @OrderNo ='' or OrderNo like '%' + @OrderNo + '%')                                 
    and (@CustomerAutoId is null or @CustomerAutoId =0 or CustomerAutoId =@CustomerAutoId)                                            
    and (@FromDate is null or @FromDate = '' or @Todate is null or @Todate = '' or                                                                                        
    (CONVERT(date,OM.[OrderDate])  between CONVERT(date,@FromDate) and CONVERT(date,@Todate)))                                                                                                                                                    
    and (@OrderStatus is null or @OrderStatus=0 or OM.[Status]=@OrderStatus)                                
    and (((@LoginEmpType=3 or @LoginEmpType=7) and PackerAutoId=@PackerAutoId and ISNULL(PackerAssignStatus,0)=1 and ((@OrderStatus is null or @OrderStatus=0 or OM.[Status]=@OrderStatus)                                      
          
            
              
                
                  
                     
                     
    and OM.[Status] in (1,2,3,9,10))) or @LoginEmpType!=3)                                                                                                            
    and ((@LoginEmpType=7 and ((@OrderStatus is null or @OrderStatus=0 or OM.[Status]=@OrderStatus)                                                    
    and OM.[Status] in (1,2,9))) or @LoginEmpType!=7)                                                                                                                                                    
    and(@SalesPersonAutoId = 0 or om.[SalesPersonAutoId]=@SalesPersonAutoId)                                                                                           
 and(@EmpAutoId = 0 or @EmpAutoId is null or CM.[SalesPersonAutoId]=@EmpAutoId)                                                                                                 
 and (ISNULL(@TypeShipping,'0')='0' or ISNULL(@TypeShipping,'0,')='0,' or om.ShippingType in (select * from dbo.fnSplitString(@TypeShipping,',')))                                                                                                           
     --and OM.ShippingType!=5 and OM.ShippingType!=6                                                                                                                                  
    )as t order by   CASE @LoginEmpType WHEN 3 THEN OrderName ELSE [OrderNo] END DESC                                                                                                                                 
                                
   SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize=0 then COUNT(RowNumber) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Results    
    SELECT * FROM #Results               
    WHERE (ISNULL(@PageSize,0)=0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))                                                                          
   END                                                                                                                                                    
  ELSE IF @Opcode=406                                                                                                                                                    
   BEGIN                                                                                                               
	   
	
	SET @OrderAutoId = (SELECT [AutoId] FROM [dbo].[OrderMaster] WHERE [OrderNo]=@OrderNo)    
	
	SELECT * FROM (                                                                                                                                                        
	SELECT 'Driver' as EmpType,DrvRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om                                      
	inner join EmployeeMaster as emp on emp.AutoId=om.Driver                                                                                                  
	where om.AutoId=@OrderAutoId   and ISNULL(DrvRemarks,'')!=''                                                        
	UNION                                                                                             
	SELECT 'Account' as EmpType,AcctRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om  
	inner join EmployeeMaster as emp on emp.AutoId=om.AccountAutoId where om.AutoId=@OrderAutoId                                                      
	and ISNULL(AcctRemarks,'')!=''                                                                                                                            
	UNION                                                                                                                                                    
	SELECT 'Sales Person' as EmpType,OrderRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName  from OrderMaster as om  
	inner join EmployeeMaster as emp on emp.AutoId=om.SalesPersonAutoId where                                                                                          
	om.AutoId=@OrderAutoId            and ISNULL(OrderRemarks,'')!=''                                          
	UNION                                                                                                               
	SELECT 'Packer' as EmpType,PackerRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om                                                                                          
	inner join EmployeeMaster as emp on emp.AutoId=om.PackerAutoId                                        
	where om.AutoId=@OrderAutoId          and ISNULL(PackerRemarks,'')!=''                                                                                                                                   
	UNION                                                          
	SELECT 'Manager' as EmpType,ManagerRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName  from OrderMaster as om                                                                                                                         
	inner join EmployeeMaster as emp on emp.AutoId=om.ManagerAutoId                                                                          
	where om.AutoId=@OrderAutoId     and ISNULL(ManagerRemarks,'')!=''                                                                                                                       
	UNION                                                                                                                                                    
	SELECT 'Warehouse' as EmpType,WarehouseRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName as EmpName  from OrderMaster as om                                      
	inner join EmployeeMaster as emp on emp.AutoId=om.WarehouseAutoId                                                                                                                                                     
	where om.AutoId=@OrderAutoId             and ISNULL(WarehouseRemarks,'')!=''                                                                                                                                      
	) AS T                          
                                                                                                                                           
	SET @CustomerAutoId= (SELECT [CustomerAutoId] FROM [dbo].[OrderMaster] WHERE AutoId=@OrderAutoId)                                                                                                                                                    
	SET @OrderStatus = (SELECT [Status] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId)                                                    
                                                                                                       
	SELECT PackerAutoId,OM.[AutoId],OM.[OrderNo],CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,ISNULL((CONVERT(VARCHAR(20), OM.[DeliveryDate], 101)),                                                         
	(CONVERT(VARCHAR(20), OM.DelDate,101) + ' ' + convert(varchar(10), DelDate, 108))                                                                                                                                                    
	) AS DeliveryDate,TaxType,                                                                                                                                                    
	OM.[CustomerAutoId],CM.CustomerName,CM.CustomerType,CT.[TermsDesc],OM.[BillAddrAutoId],CTy.CustomerType as CustomerTypeName ,
	BA.[Address]+(case when BA.Address2!='' then ', '+ISNULL(BA.Address2,'') else '' end) As BillAddr,S.StateCode,S.[StateName] AS State1,BA.[City] AS City1
	,BA.[Zipcode] As Zipcode1,
	OM.[ShipAddrAutoId],SA.[Address]+(case when SA.Address2!='' then ', '+ISNULL(SA.Address2,'') else '' end) As ShipAddr,S1.StateCode as StateCode2,S1.[StateName] As State2,
	SA.[City] AS City2,	SA.[Zipcode] As Zipcode2,
	OM.[Status] As StatusCode,DO.[AmtPaid],DO.[AmtDue],                                                             
	SM.[StatusType] AS Status,OM.[TotalAmount],OM.[OverallDisc],OM.[OverallDiscAmt],OM.[ShippingCharges],OM.[TotalTax],OM.[GrandTotal],OM.[PackedBoxes],CommentType,Comment,                                                          
	OM.[Driver],EM.[FirstName]+ ' ' + EM.[LastName] AS DrvName,OM.[ShippingType],ST.ShippingType as ShippingTypeName,CONVERT(VARCHAR(20),
	OM.[AssignDate],101) AS AssignDate,[DrvRemarks],isnull(DeductionAmount,0.00) as DeductionAmount,ManagerRemarks,                  
	isnull(OM.CreditAmount,0.00)as CreditAmount,isnull(OM.PayableAmount,0.00) as PayableAmount,                                                                                                                                          
	ISNULL((select t.CreditAmount from CustomerCreditMaster as t  where t.CustomerAutoId=om.CustomerAutoid),0.00) as CustomerCredit,                                                                                                                           
	PackerRemarks,OrderRemarks,ISNULL(Times,0) as Times,ISNULL(om.MLQty,0) AS MLQty ,ISNULL(om.MLTax ,0) AS MLTax ,                                                                                                                   
	WarehouseRemarks,ISNULL(om.AdjustmentAmt,0) as AdjustmentAmt,isnull(Weigth_OZQty,0) as Weigth_OZQty,
	isnull(OM.Weigth_OZTax,0) as Weigth_OZTax,isnull(OM.Weigth_OZTaxAmount,0) as  Weigth_OZTaxAmount,CM.CustomerName,CM.CustomerType ,
	isnull((select mlt.TaxRate from MLTaxMaster as mlt where mlt.TaxState=BA.State),0) as MLTaxRate
	FROM [dbo].[OrderMaster] As OM                                                                             
	INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.Status AND SM.[Category] = 'OrderMaster'                                                                                                                          
	INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[AutoId] = OM.[BillAddrAutoId]                                                                                                                                   
	INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = OM.[ShipAddrAutoId]                                           
	LEFT JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                                                                                                                                                     
	LEFT JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                                                                                                                                                      
	LEFT JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]
    INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]   
	LEFT JOIN [CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                                                    
	LEFT JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[Driver]                                               
	left JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId]  
	inner join ShippingType as ST on ST.AutoId=OM.ShippingType                       
	WHERE OM.AutoId = @OrderAutoId                                                                                                                                                  
	---------------                                                                                                
	SET @customerType=(select CustomerType from CustomerMaster where AutoId=@CustomerAutoId)  
	
	                                                          
		SELECT OIM.AutoId as ItemAutoId, PM.[AutoId] AS ProductAutoId,PM.[ProductId],PM.[ProductName],UM.AutoId AS UnitAutoId,UM.[UnitType],OIM.[QtyPerUnit],                                                                                                        
		OIM.[UnitPrice],convert(decimal(18,2),UnitPrice/QtyPerUnit) as PerpiecePrice,                                                                                                                            
		ISNULL(OIM.[OM_MinPrice],0) as [MinPrice],PD.[Price],OIM.[RequiredQty], PD.EligibleforFree,                                                                                
		[TotalPieces],OIM.[SRP],OIM.[GP],OIM.[Tax],OIM.[NetPrice],OIM.[Barcode],OIM.[QtyShip],ISNULL(OIM.[OM_MinPrice],0) as OM_MinPrice
		,ISNULL(OIM.[OM_CostPrice],0) as OM_CostPrice,ISNULL(OIM.[BasePrice],0) as OM_BasePrice,                                                                                                                                
		OIM.[RemainQty],IsExchange,isFreeItem,UnitMLQty,TotalMLQty,isnull(OIM.Weight_Oz,0) as WeightOz,
		isnull(Oim_Discount,0.00) as Discount,isnull(Oim_ItemTotal,0.00) as ItemTotal FROM [dbo].[OrderItemMaster] AS OIM                                                                           
		INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                                                                                                                           
		INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitTypeAutoId                                                                                                                                                     
		INNER JOIN [dbo].[PackingDetails] AS PD ON PD.[ProductAutoId] = OIM.[ProductAutoId] AND PD.[UnitType] = OIM.UnitTypeAutoId                                                                                                  
		WHERE [OrderAutoId]=@OrderAutoId     
		

	SELECT [PackingId],CONVERT(VARCHAR,[PackingDate],101) As PkgDate,EM.[FirstName] + ' ' + EM.[LastName] As Packer                                                 
	FROM [dbo].[GenPacking] AS GP                                                    
	INNER JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId]=GP.[PackerAutoId] WHERE [OrderAutoId]=@OrderAutoId      
	       
	             
	
	SET @OrderStatus=(SELECT Status FROM OrderMaster WHERE AutoId=@OrderAutoId)   
	
	IF NOT EXISTS(SELECT * FROM CreditMemoMaster WHERE OrderAutoId=@OrderAutoId)             
	BEGIN                  
		IF @OrderStatus <= 4 or @OrderStatus=10                                                                                                                  
		BEGIN                                                                                                                              
			select OrderAutoId,CreditAutoId,CreditNo,CONVERT(VARCHAR(10),CreditDate,101) AS CreditDate,ISNULL(TotalAmount,0.00) as ReturnValue,ISNULL(PaidAmount,0.00)  as amtDeducted,                                                                                
			ISNULL(BalanceAmount,0.00) as amtDue                                                                                                                                                    
			from CreditMemoMaster AS CM  where Status =3 AND CustomerAutoId=@CustomerAutoId and OrderAutoId is null-- and BalanceAmount > 0    
			
			select CreditAutoId,CreditNo,CONVERT(VARCHAR(10),CreditDate,101) AS CreditDate,                                                                        
			(SELECT COUNT(1) FROM CreditItemMaster AS CIM WHERE CIM.CreditAutoId=CM.CreditAutoId) AS NoofItem,                                                                              
			(SELECT SUM(NetAmount) FROM CreditItemMaster AS CIM WHERE CIM.CreditAutoId=CM.CreditAutoId) AS NetAmount
			from CreditMemoMaster AS CM  where Status =1 AND CustomerAutoId=@CustomerAutoId                                                                                                                                                    
		END                      
	END                                                                                                                                                    
	ELSE                               
	BEGIN                                                                                                                     
		select OrderAutoId,CreditAutoId,CreditNo,CONVERT(VARCHAR(10),CreditDate,101) AS CreditDate,ISNULL(TotalAmount,0.00) as ReturnValue,ISNULL(PaidAmount,0.00)  as amtDeducted,                                                                                  
		ISNULL(BalanceAmount,0.00) as amtDue                                                                                                           
		from CreditMemoMaster AS CM  where Status =3                                                                                         
		AND OrderAutoId=@OrderAutoId 
		Union
		select OrderAutoId,CreditAutoId,CreditNo,CONVERT(VARCHAR(10),CreditDate,101) AS CreditDate,ISNULL(TotalAmount,0.00) as ReturnValue,ISNULL(PaidAmount,0.00)  as amtDeducted,                                                                                  
		ISNULL(BalanceAmount,0.00) as amtDue                                                                                                           
		from CreditMemoMaster AS CM  where Status =3                                                                                         
		AND (@LoginEmpType=8 AND CustomerAutoId=@CustomerAutoId AND @OrderStatus <= 4  AND CM.TotalAmount>0 )
		and OrderAutoId is null

		IF (@OrderStatus <= 4)                                                                                                                                                    
		BEGIN                                                                                                                                                    
			select CreditAutoId,CreditNo,CONVERT(VARCHAR(10),CreditDate,101) AS CreditDate,                                                                                         
			(SELECT COUNT(1) FROM CreditItemMaster AS CIM WHERE CIM.CreditAutoId=CM.CreditAutoId) AS NoofItem,                                                                                                                                                    
			(SELECT SUM(NetAmount) FROM CreditItemMaster AS CIM WHERE CIM.CreditAutoId=CM.CreditAutoId) AS NetAmount                                                                                                                                                   
			from CreditMemoMaster AS CM where Status =1 AND CustomerAutoId=@CustomerAutoId                                         
			AND TotalAmount>0            
		END                                                                                                                                                    
	END                                                           
   END                                                                                                                              
  ELSE IF @Opcode=407                                                                                                                                  
     BEGIN          
  if @LoginEmpType= 7                                                                                                                                       
  BEGIN                                                                                                                                                
   SELECT [AutoId],[StatusType] FROM [dbo].[StatusMaster] WHERE  [Category]='OrderMaster'                                                        
   AND [AutoId] IN(1,2,9)      order by [StatusType] ASC                                                                                      
  END                                                                                                                                                
  ELSE IF(@LoginEmpType= 3 )                                                                             
  BEGIN                                                                                                                      
   SELECT [AutoId],[StatusType] FROM [dbo].[StatusMaster] WHERE  [Category]='OrderMaster'                                             
   AND [AutoId] IN(1,2,3,9,10)       order by [StatusType]  ASC                                                              
  END                                                                                                                                                    
  ELSE IF(@LoginEmpType= 6)                                                                                                            
  BEGIN                                                            
   SELECT [AutoId],[StatusType] FROM [dbo].[StatusMaster] WHERE  [Category]='OrderMaster'                                                                                     
AND [AutoId] IN(6,11,8)          order by [StatusType]  ASC                                                                                                                                                      
  END            
  ELSE IF(@LoginEmpType= 5)                                                                                                       
  BEGIN                                                                                                        
   SELECT [AutoId],[StatusType] FROM [dbo].[StatusMaster] WHERE  [Category]='OrderMaster'                                                                                                                                                     
   AND [AutoId] IN(4,5,6)          order by [StatusType]  ASC                                                                                                                                                      
END                                                                
  ELSE                                                                                                                                                    
  BEGIN                                                                                                                                             
   SELECT [AutoId],[StatusType] FROM [dbo].[StatusMaster] WHERE   [Category]='OrderMaster'          
         order by [StatusType]                                                                                                                                
END                                                                                               
                                                                                                
  IF(@LoginEmpType != 2)                                       
  BEGIN                                       
   SELECT [AutoId],[CustomerId] + ' - ' + [CustomerName] As Customer FROM [dbo].[CustomerMaster]        
    order by [CustomerName] ASC,[CustomerId] ASC                                                                                              
  END                                                                       
  ELSE                                                                        
  BEGIN                                                                                                                                                    
   SELECT [AutoId],[CustomerId] + ' - ' + [CustomerName] As Customer FROM [dbo].[CustomerMaster]                                                                                                                                                    
   WHERE [SalesPersonAutoId]=@SalesPersonAutoId order by replace([CustomerName],' ','')    asc                                                                                                                                       
  END                                                                                                                                 
  SELECT AutoId,(FirstName +' '+ISNULL(LastName,'')) AS EmpName         
  FROM EmployeeMaster WHERE EmpType =2  order By (FirstName +' '+ISNULL(LastName,''))-- Updated on 11/19/2019 By Rizwan Ahmad EmpId removed                                                                                                          
                                                
  SELECT AutoId,(FirstName +' '+ISNULL(LastName,'')) AS EmpName FROM        
   EmployeeMaster WHERE EmpType =5  order by (FirstName +' '+ISNULL(LastName,''))-- Updated on 11/19/2019 By Rizwan Ahmad EmpId removed                                                                                                           
                                                    
  SELECT AutoId,ShippingType FROM ShippingType  ORDER BY ShippingType ASC         
  select AutoId,CustomerType FROM CustomerType  ORDER BY CustomerType ASC        
     select AutoId, OrderType from OrderTypeMaster where Type='Order'       
 END              
  ELSE IF @Opcode=435                                    
  BEGIN                                                      
  SELECT AutoId AS CustomerAutoId,CustomerId + ' ' + CustomerName as CustomerName FROM CustomerMaster  WHERE (CustomerType=@CustomerTypeAutoId                                     
  OR ISNULL(@CustomerTypeAutoId,0)=0) and (SalesPersonAutoId=@SalesPersonAutoId or ISNULL(@SalesPersonAutoId,0)=0) order by CustomerName ASC                                                    
  END              
  ELSE IF @Opcode=408                                                                                                     
	BEGIN                                                                  
	IF(@AddressType = 11)                                                                                                                                                    
	BEGIN                                                                                                                      
		SELECT BA.[AutoId],CM.[CustomerName],BA.[Address]+(case when BA.Address2!='' then ', '+ISNULL(BA.Address2,'') else '' end) as Address,S.[StateName] As State,[City],[Zipcode],BA.[IsDefault]                                                 
		FROM [dbo].[BillingAddress] As BA                                                                                                                                      
		INNER JOIN [dbo].[State] As S ON S.[AutoId]=BA.[State]                                                                                                                                                     
		INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = BA.[CustomerAutoId] WHERE [CustomerAutoId]=@CustomerAutoId                                                                                                                                      
	END                                                                                                                                                    
	ELSE IF(@AddressType = 22)                                                          
	BEGIN                                                                                                                                                    
		SELECT SA.[AutoId],CM.[CustomerName],SA.[Address]+(case when SA.Address2!='' then ', '+ISNULL(SA.Address2,'') else '' end) as Address,S.[StateName] As State ,SA.[City],SA.[Zipcode],SA.[IsDefault] FROM [dbo].[ShippingAddress] As SA 
		INNER JOIN [dbo].[State] As S ON S.[AutoId]=SA.[State]                                                                                                                                              
		INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = SA.[CustomerAutoId] WHERE [CustomerAutoId]=@CustomerAutoId  
	END                                                                                   
   END                    
  ELSE IF @Opcode=409                                                                        
 BEGIN                                                                                              
    SELECT [AutoId],[StateCode] + ' - ' + [StateName] AS StateName FROM [dbo].[State]                                 
   END                                                                                                                   
  ELSE IF @Opcode=410                                                                                                                                                        
   BEGIN                                                                   
    BEGIN TRY                                                                                                           BEGIN TRAN                                                                                                                            
   
    
      
        
         
             
              
                               
                    
                      
               
   IF(@AddressType = 11)                                                                                                                                                    
      BEGIN                                                                                                             
       UPDATE [dbo].[BillingAddress] SET [IsDefault]=0 WHERE [CustomerAutoId]=@CustomerAutoId                                                                                                                                                    
       UPDATE [dbo].[BillingAddress] SET [IsDefault]=1 WHERE [AutoId]=@AddressAutoId                                                                                                                                                    
       UPDATE [dbo].[CustomerMaster] SET [DefaultBillAdd]=@AddressAutoId WHERE [AutoId]=@CustomerAutoId                                                                  
      END                                   ELSE IF(@AddressType = 22)                                                                                                                                                    
      BEGIN                                                                                                
       UPDATE [dbo].[ShippingAddress] SET [IsDefault]=0 WHERE [CustomerAutoId]=@CustomerAutoId                                                                                                                                                    
       UPDATE [dbo].[ShippingAddress] SET [IsDefault]=1 WHERE [AutoId]=@AddressAutoId                                                                                                                                                  
    UPDATE [dbo].[CustomerMaster] SET [DefaultShipAdd]=@AddressAutoId WHERE [AutoId]=@CustomerAutoId                                                       
      END                                                                                            
     COMMIT TRANSACTION                                                                      
    END TRY                                                                                                                                             
    BEGIN CATCH                                                                                         
     ROLLBACK TRAN                                                                                                                                         
     Set @isException=1                                                                             
    Set @exceptionMessage='Oops! Something went wrong.Please try later.'                                                                                 
    END CATCH                                                                                                                                                    
   END                                                                                 
  ELSE IF @Opcode=411                                                                                                                                                    
   BEGIN                                                                                                                                 
    SELECT [ProductAutoId],[UnitAutoId],PM.[Stock] FROM [dbo].[ItemBarcode] AS IB                                                                                                                                                    
    INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = IB.[ProductAutoId] WHERE [Barcode]=@Barcode                                                                      
   END                                                                                                                                          
  Else If @Opcode=427                                                        
   Begin                                            
    SELECT top 1 PM.[Stock],Barcode FROM [dbo].[ItemBarcode] AS IB                                                           
 INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = IB.[ProductAutoId] WHERE ProductAutoId=@ProductAutoId                                                                                                               
    and IB.UnitAutoId=@UnitAutoId and Barcode!=''                                             
   End                                                                                                 
  ELSE IF @Opcode=412                                                                                                                                                    
   BEGIN 
		SELECT em.[AutoId],[FirstName] + ' ' + [LastName] + ' [' + etm.TypeName + ']' as Name, 
		(select count (1) from OrderMaster om where Status in (4,5) and em.AutoId = om.Driver) as AssignOrders FROM [dbo].[EmployeeMaster] em            
		inner join EmployeeTypeMaster etm on etm.AutoId =  em.EmpType            
		WHERE ([EmpType]=5 or em.autoid in (SELECT SalesPersonAutoId            
		FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId                    
		) ) AND Status=1               
		order by em.EmpType,FirstName                                                        
                            
		SELECT [OrderNo],[Driver],PackerAutoId,convert(varchar(10),isnull(AssignDate,getdate()),101) as AssignDate,OrderDate                                       
		FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId 

	

   END                                                                                                                                                    
  ELSE IF @Opcode=413                                                                                         
   BEGIN  
	SELECT EM.AutoId,[FirstName] + ' ' + [LastName]+' ['+ETM.TypeName+']' As Driver FROM [dbo].[EmployeeMaster] AS EM
	INNER JOIN EmployeeTypeMaster as ETM ON EM.EmpType=ETM.AutoId
	WHERE ([EmpType] = 5 or [EmpType] = 2)
    and status=1 ORDER BY [FirstName] ASC,[LastName] ASC 
   END                     
  ELSE IF @Opcode=414                                                                      
   BEGIN                                                           
                   
    SELECT ROW_NUMBER() OVER(ORDER BY [AssignDate] desc) AS RowNumber, * INTO #Values from                                                                                                                                                    
    (                                                                                                                                                         
    SELECT [TotalOrders],[Driver],EM.[FirstName] + ' ' + EM.[LastName] As DrvName,[AssignDate] FROM (                                                    
SELECT COUNT([AutoId]) AS TotalOrders,[Driver],CONVERT(date,[AssignDate]) AS [AssignDate] FROM [dbo].[OrderMaster] where Status in (4,5)                                                                                                                      
   
    
      
       
               
               
                  
                     
                      
     group by [driver],CONVERT(date,[AssignDate])) AS Tb                                                                                                                                             
    INNER JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = Tb.[Driver]                                                                                                    
    WHERE (@DriverAutoId IS NULL OR @DriverAutoId = 0 OR Tb.Driver = @DriverAutoId) AND                                                   
       (@Fromdate IS NULL OR @Fromdate='' OR @Todate IS NULL OR @Todate='' OR Tb.AssignDate BETWEEN @Fromdate AND @Todate)                                                                                                     
    )as t                                                                        
                                                                                                                                                    
    SELECT COUNT(*) as RecordCount,@PageSize as PageSize,@PageIndex as PageIndex FROM #Values                                                                       
    SELECT [TotalOrders],[Driver],DrvName,CONVERT(VARCHAR(20),[AssignDate],101) AS AssignDate FROM #Values                                                                                                                                                    
    WHERE RowNumber BETWEEN(@PageIndex - 1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1                                     END                                                                                                     
 
     
      
       
          
             
              
                
                  
                    
                     
                                            
  ELSE IF @Opcode=415                                                                                        
   BEGIN                                                                                                                                                        
    SELECT OM.CustomerAutoId,OM.[AutoId],OM.[OrderNo],CONVERT(VARCHAR(20),OM.[OrderDate],101) As OrderDate,                                                                                                                                             
    CM.[CustomerName],PayableAmount as [GrandTotal],OM.[Status],PackedBoxes,(emp.FirstName + ' '+ emp.LastName) as SalesPerson,                                                 
    SM.[StatusType] AS StatusType,[Stoppage],RootName,DriverRemarks FROM [dbo].[OrderMaster] AS OM                                                                                                                                                    
    INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                                                                      
    INNER JOIN [dbo].[EmployeeMaster] AS emp ON emp.AutoId = OM.SalesPersonAutoId                                                                                                                                                
    INNER JOIN [dbo].[StatusMaster] AS SM ON SM.[AutoId] = OM.[Status]                                                                                                        
     AND SM.[Category] = 'OrderMaster'                          
    WHERE OM.[Driver] = @DriverAutoId                                                                                                                                                   
    and CONVERT(DATE,OM.[AssignDate])=(CASE WHEN @AsgnDate IS NOT NULL OR @AsgnDate != '' THEN @AsgnDate ELSE CONVERT(DATE,AssignDate) END)   
    AND (@OrderNo = '' OR @OrderNo IS NULL OR OM.[OrderNo] LIKE '%' + @OrderNo + '%')                                                                                                                      
    and (@Fromdate IS NULL OR @Fromdate='' OR @Todate IS NULL                                       
    OR @Todate='' OR OM.OrderDate BETWEEN @Fromdate AND dateadd(dd,1,@Todate))                                                                                                                                                     
    and ((@OrderStatus is null or @OrderStatus=0 or OM.Status=@OrderStatus) AND OM.Status IN (4,5) )                                                                     
  and(@SalesPersonAutoId = 0 or CM.[SalesPersonAutoId]=@SalesPersonAutoId)                                                                                                                                                    
    and (@CustomerAutoId is null or @CustomerAutoId=0 or OM.CustomerAutoId=@CustomerAutoId)                                                                                              
               -- Order by OM.Status, CONVERT(VARCHAR(20),OM.[OrderDate],101) desc,convert(int,[Stoppage])  
			   order by CustomerName asc                                                                                                                            
                                                                                                            
    SELECT [Root] FROM [dbo].[DriverRootLog] WHERE [DrvAutoId] = @DriverAutoId AND                                                                                                                                                     
    CONVERT(DATE,[AssignDate]) = (CASE WHEN @AsgnDate IS NOT NULL OR @AsgnDate != ''                                                                                                                     
    THEN @AsgnDate ELSE CONVERT(DATE,getdate()) END)                                                                                                                                                     
  END                                                                         
  ELSE IF @Opcode=416                                                                                        
   BEGIN                                                                                        
    UPDATE [dbo].[OrderMaster] SET [Status] = 5 WHERE [AutoId] = @OrderAutoId                                       
    SET @LogRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=3),'[Status1]','Ready to ship')                                                                                                                                 
  
   
       
        
          
           
    SET @LogRemark = REPLACE(@LogRemark,'[Status2]','Shipped')                                                                                
    INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                                                                                                                                    
    VALUES(3,@EmpAutoId,getdate(),@LogRemark,@OrderAutoId) 
	SELECT @CustomerAutoId=CustomerAutoId FROM [dbo].[OrderMaster]   WHERE [AutoId] = @OrderAutoId                
 if((select CustomerType from CustomerMaster where AutoId=@CustomerAutoId)=3)                      
 begin                      
  exec [dbo].[ProcUpdatePOStatus_All]                      
  @CustomerId=@CustomerAutoId,                      
  @OrderAutoId=@OrderAutoId,                      
  @Status=5                          
 end            
                                                  
   END  
   ELSE IF @Opcode=417 
   BEGIN                                       
		SELECT OM.[AutoId],OM.[OrderNo],CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,CONVERT(VARCHAR(20), OM.[DeliveryDate], 101) AS DeliveryDate,                                                                                                    
		CM.[CustomerId],CM.[CustomerName],CTy.AutoId as TypeAutoId,CTy.CustomerType,CM.[Contact1] + '/' + CM.[Contact2] As Contact,CT.[TermsDesc],ST.[ShippingType],BA.[Address] As BillAddr,S.[StateName] AS State1,
		BA.[City] AS City1,BA.[Zipcode] As Zipcode1,SA.[Address] As ShipAddr,S1.[StateName] As State2,SA.[City] AS City2,SA.[Zipcode] As Zipcode2, 
		OM.[Status] As StatusCode,SM.[StatusType] AS Status,OM.[TotalAmount],OM.[OverallDisc],OM.[OverallDiscAmt],OM.[ShippingCharges],OM.[TotalTax],OM.[GrandTotal],                                                                                      
		EM.[FirstName] + ' ' + EM.[LastName] As SalesPerson,                                                                                                                           
		(select EM1.[FirstName] + ' ' + EM1.[LastName] from [dbo].[EmployeeMaster] AS EM1 where EM1.[AutoId]                                                
		= OM.PackerAutoId) AS PackerName,                                                                                                                
		(select EM1.[FirstName] + ' ' + EM1.[LastName] from [dbo].[EmployeeMaster] AS EM1 where EM1.[AutoId]                                                                                           
		= OM.warehouseAutoid) AS WarehouseName,ISNULL(AdjustmentAmt,0) as AdjustmentAmt,                                                                                           
		om.TaxType,OrderRemarks,WarehouseRemarks  FROM [dbo].[OrderMaster] As OM                                                                                                                                                    
		INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.Status AND SM.[Category] = 'OrderMaster'                                                                                                       
		INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[AutoId] = OM.[BillAddrAutoId]                                                                                                                                                    
		INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = OM.[ShipAddrAutoId]                                                                                        
		INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                                                                                                                                                     
		INNER JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                                                                                                                                                     
		INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                                        
		INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]                                                                                 
		LEFT JOIN [dbo].[CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]       
		LEFT JOIN [dbo].[ShippingType] AS ST ON ST.[AutoId] = OM.[ShippingType]                                                                                                             
		INNER JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[SalesPersonAutoId] WHERE OM.AutoId = @OrderAutoId   

		SELECT * FROM (                                
		SELECT CM.[CategoryName],CM.[CategoryId],OIM.ProductAutoId,PM.[ProductId],PM.[ProductName],(                                                                                                                                                    
		select top 1 Location  from PackingDetails as pd where pd.ProductAutoId=PM.AutoId and                                                                                                                                                    
		pd.UnitType=OIM.UnitTypeAutoId) AS [ProductLocation],                                                 
		OIM.[UnitTypeAutoId],UM.[UnitType],OIM.[QtyPerUnit],OIM.[UnitPrice],OIM.[TotalPieces],OIM.[SRP],OIM.[GP],OIM.[Tax],                                                                                                 
		OIM.[NetPrice],OIM.[Barcode],OIM.[QtyShip],OIM.[RequiredQty],OIM.IsExchange FROM [dbo].[OrderItemMaster] AS OIM                                                                                                                                
		INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                                                                  
		INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitTypeAutoId                     
		INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId] WHERE [OrderAutoId] = @OrderAutoId 
		) AS T ORDER BY  ISNULL(ProductLocation,'') ASC,ProductId   
		
		SET @LogRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=3),'[Status1]','New')
		SET @LogRemark = REPLACE(@LogRemark,'[Status2]','Processed') 
		
		if Exists(select Status from OrderMaster where AutoId=@OrderAutoId and Status=1)                                                         
		Begin                                                                                                                                     
			UPDATE [dbo].[OrderMaster] SET [Status] = 2 WHERE [AutoId]=@OrderAutoId AND [Status] = 1                                                                                                                                           
			INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                                                                   
			VALUES(3,@EmpAutoId,getdate(),@LogRemark,@OrderAutoId)                                                                                                                                   
		End    
		
		SELECT * FROM TAXTypeMaster  
		
		SELECT * FROM (                                                                                                                                                        
		SELECT 'Driver' as EmpType,DrvRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om 
		inner join EmployeeMaster as emp on emp.AutoId=om.Driver                                                
		where om.AutoId=@OrderAutoId AND @LoginEmpType!=5  and  DrvRemarks is not null  and  DrvRemarks!='' 
		UNION                                                                        
		SELECT 'Account' as EmpType,AcctRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om                                                                                                      
		inner join EmployeeMaster as emp on emp.AutoId=om.AccountAutoId where om.AutoId=@OrderAutoId                                                  
		AND @LoginEmpType!=6 and  AcctRemarks is not null  and  AcctRemarks!=''                                                                                                                 
		UNION                                                        
		SELECT 'Sales Person' as EmpType,OrderRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName  from OrderMaster as om 
		inner join EmployeeMaster as emp on emp.AutoId=om.SalesPersonAutoId where                                                                                                       
		om.AutoId=@OrderAutoId AND @LoginEmpType!=2 and  OrderRemarks is not null  and  OrderRemarks!=''                                                                                                             
		UNION                                            
		SELECT 'Packer' as EmpType,PackerRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om
		inner join EmployeeMaster as emp on emp.AutoId=om.PackerAutoId                                                                                            
		where om.AutoId=@OrderAutoId AND @LoginEmpType!=3  and  PackerRemarks is not null  and  PackerRemarks!=''                                                                                                                                    
		UNION                                                                                                                                                    
		SELECT 'Manager' as EmpType,ManagerRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName  from OrderMaster as om 
		inner join EmployeeMaster as emp on emp.AutoId=om.ManagerAutoId                                                                                                                                                     
		where om.AutoId=@OrderAutoId AND @LoginEmpType!=8 and  ManagerRemarks is not null  and  ManagerRemarks!=''  
		) AS T                                                                                                                              
   END                                                                   
   ELSE IF @Opcode=418                                                               
   BEGIN                                                                                                        
		SELECT CompanyName,EmailAddress, [Address],[Website],[MobileNo],[FaxNo],TermsCondition,Logo FROM CompanyDetails                                                                                                                                
		SET @CustomerAutoId = (SELECT [CustomerAutoId] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId)                                                                                  
		SET @customerType = (SELECT CustomerType FROM [dbo].CustomerMaster WHERE [AutoId]=@CustomerAutoId)                                                                                                    
                                                                                
		IF EXISTS(SELECT [AutoId] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId AND ([PaymentRecev] IS NOT NULL OR [PaymentRecev] != ''))  
		BEGIN                                                                                                                               
			SELECT OM.[AutoId],OM.[OrderNo],CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,                                                   
			ISNULL(CONVERT(VARCHAR(20), OM.[DeliveryDate],101),CONVERT(VARCHAR(20), OM.[DelDate],101) + ' ' +convert(varchar(10), OM.[DelDate], 108)) AS DeliveryDate,                                        
			CM.[AutoId] As CustAutoId,CM.[CustomerId],CM.[CustomerName],CM.[Contact1] + '/' + CM.[Contact2] As Contact,CTy.AutoId as TypeAutoId,CTy.CustomerType,CT.[TermsDesc],ST.[ShippingType],                                                           
			BA.[Address] As BillAddr,                                                                                                                                                    
			S.[StateName] AS State1,ba.State as [stateid],BA.[City] AS City1,BA.[Zipcode] As Zipcode1,SA.[Address] As ShipAddr,S1.[StateName] As State2,SA.[City] AS City2,                                
			SA.[Zipcode] As Zipcode2,OM.[Status] As StatusCode,SM.[StatusType] AS Status,OM.[PackedBoxes],DO.[TotalAmount],DO.[OverallDisc],DO.[OverallDiscAmt],                                
			DO.[ShippingCharges],DO.[TotalTax],DO.[GrandTotal],DO.[AmtPaid],DO.[AmtDue],OM.[PaymentRecev],OM.[RecevAmt],OM.[AmtValue],OM.[ValueChanged],                                                                                 
			OM.[DiffAmt],OM.[NewTotal],OM.[PayThru],OM.CheckNo,OM.[DrvRemarks],OM.[AcctRemarks],EM.[FirstName] + ' ' + EM.[LastName] As SalesPerson,                                                                          
			EM1.[FirstName] + ' ' + EM1.[LastName] As PackerName,[CommentType],[Comment],isnull(DeductionAmount,0.00) as DeductionAmount,                                                                           
			ISNULL(DO.CreditAmount,0.00)as CreditAmount,isnull(do.PayableAmount,0.00) as PayableAmount,                                                              
			ISNULL((select t.CreditAmount from CustomerCreditMaster as t  where t.CustomerAutoId=om.CustomerAutoid),0.00) as CustomerCredit  
			,om.TaxType,otm.OrderType AS OrderType,OrderRemarks,                                                                                                              
			ISNULL(OM.MLQty,0) as MLQty,isnull(DO.MLTax,0) as MLTax,CM.ContactPersonName,ISNULL(om.AdjustmentAmt,0) as AdjustmentAmt, 
			CM.BusinessName,ISNULL(UnitMLTax,0) as UnitMLTax,OM.Weigth_OZTaxAmount as WeightTax,                             
			isnull(OM.Weigth_OZQty,0) as Weigth_OZQty,isnull(OM.Weigth_OZTax,0) as Weigth_OZTax,isnull(OM.Weigth_OZTaxAmount,0) as Weigth_OZTaxAmount,
			MLTaxPer
			FROM [dbo].[OrderMaster] As OM --Add Weight Tax By Rizwan Ahmad on 17-09-2019 03:06 AM                                                                         
			INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.Status AND SM.[Category] = 'OrderMaster'                                                                                                            
			INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[AutoId] = OM.[BillAddrAutoId]                                                                                    
			INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = OM.[ShipAddrAutoId]                                                   
			INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                                                                                                                                      
			INNER JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                                                 
			INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                       
			INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]                                                                                                                                                    
			LEFT JOIN [dbo].[CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                                                                                                                                                    
			LEFT JOIN [dbo].[ShippingType] AS ST ON ST.[AutoId] = OM.[ShippingType]                                                                                    
			LEFT JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[SalesPersonAutoId]                                                                                                             
			LEFT JOIN [dbo].[EmployeeMaster] AS EM1 ON EM1.[AutoId] = OM.[PackerAutoId]                                                                                                                                                     
			INNER JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId] 
			   INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
			WHERE OM.AutoId = @OrderAutoId                                                                                                
                                                                                                                                                    
			SELECT CM.[CategoryName],CM.[CategoryId],DOI.[ProductAutoId],PM.[ProductId],PM.[ProductName],DOI.[UnitAutoId],UM.[UnitType],DOI.[QtyPerUnit],                        
                                       
                
			DOI.[UnitPrice],DOI.[Barcode],DOI.[QtyShip],DOI.[QtyDel],DOI.[TotalPieces],DOI.[SRP],DOI.[GP],DOI.[Tax],DOI.[NetPrice]                                                                            
			,convert(decimal(18,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice,ISNULL(FreshReturnQty,0) as FreshReturnQty,FreshReturnUnitAutoId,                                                                              
			ISNULL(DamageReturnQty,0) as DamageReturnQty,DamageReturnUnitAutoId,MissingItemQty,MissingItemUnitAutoId,                                                                                                                                      
			IsExchange,isnull(TaxValue,0.00) as TaxValue,ISNULL(isFreeItem,0) AS isFreeItem,                                                                                                                                                    
			ISNULL(UnitMLQty,0) as UnitMLQty,ISNULL(TotalMLQty,0) as TotalMLQty,                                                                                                     
			DOI.Item_TotalMLTax as TotalMLTax ,                                                     
			(case @customertype when 2 then pd.whminprice when 3 then pd.costprice else [minprice] end) as [MinPrice], isnull(DOI.Del_Weight_Oz,0) as Weight_Oz                                                                                   
			FROM [dbo].[Delivered_Order_Items] AS DOI                                                                                               
			INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = DOI.[ProductAutoId]                                                                                                                                                    
			INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = DOI.UnitAutoId                                                                                                     
			INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId]                                                                                                     
			INNER JOIN [dbo].[PackingDetails] AS PD ON PD.[ProductAutoId] = DOI.[ProductAutoId] AND PD.[UnitType] = DOI.UnitAutoId                                                           
			WHERE [OrderAutoId]=@OrderAutoId                                                                                                                               
                                                                                                                                                 
			SELECT OM.[AutoId],[OrderNo],CONVERT(VARCHAR(20),[OrderDate],101) AS OrderDate,DO.[AmtDue],DO.[GrandTotal],DO.[AmtPaid] ,  
                  
			otm.OrderType AS OrderType ,DATEDIFF(dd,OrderDate,GETDATE()) as Aging, orderdate as orderdateSort                                                                                 
			FROM [dbo].[OrderMaster] As OM                                                                                         
			INNER JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId]  
			   INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
			WHERE [CustomerAutoId] = @CustomerAutoId AND ([Status] = 6 or [Status] = 11) AND DO.[AmtDue] > 0.00                                                                                                                                                    
			order by orderdateSort                                                                
                                                                                                                                                    
			SELECT [AmountPaid],CONVERT(VARCHAR(20),PL.[PayDate],101) AS PayDate,EM.FirstName + ' ' + EM.[LastName] As EmpName,[Remarks] FROM [dbo].[PaymentLog] AS PL                                                                                 
			INNER JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = PL.EmpAutoId WHERE [OrderAutoId] = @OrderAutoId                                                             
                                                        
			SELECT @LoginEmpType As LoginEmpType                                                                                                                                                    
                                                                                                                                                          
			select ProductId,um.UnitType,pd.UnitType as UnitAutoId,Qty from Delivered_Order_Items as do                                                                                                                                                    
			inner join ProductMaster as pm on pm.AutoId=do.ProductAutoId                                                                                
			inner join PackingDetails as pd on pd.ProductAutoId=pm.AutoId                                                                                                             
			inner join UnitMaster as um on um.AutoId=pd.UnitType                                                                                                                                                    
			where OrderAutoId=@OrderAutoId                                                            
                                                                                                                                                
		END                                                                                                                          
		ELSE                                                                                                                                                    
		BEGIN                                                                                                               
			SELECT OM.[AutoId],OM.[OrderNo],CONVERT(VARCHAR(20),OM.[OrderDate], 101) AS OrderDate,                                                                                                                                                    
			CONVERT(VARCHAR(20), OM.[DeliveryDate],101) + ' ' + convert(varchar(10), getdate(), 108)                                                                                                                                                   
			AS DeliveryDate,                                                       
			CM.[AutoId] As CustAutoId,CM.[CustomerId],CM.[CustomerName],CM.[Contact1] + '/' + CM.[Contact2] As Contact,CTy.AutoId as TypeAutoId,CTy.CustomerType,CT.[TermsDesc],ST.[ShippingType],                                                       
			BA.[Address] As BillAddr,S.[StateName] AS State1,BA.[City] AS City1,BA.[Zipcode] As Zipcode1,SA.[Address] As ShipAddr,S1.[StateName] As State2,  
			SA.[City] AS City2,SA.[Zipcode] As Zipcode2,OM.[Status] As StatusCode,SM.[StatusType] AS Status,OM.[TotalAmount],OM.[OverallDisc],OM.[OverallDiscAmt], 
			OM.[ShippingCharges],OM.[TotalTax],OM.[GrandTotal],OM.[PackedBoxes],EM.[FirstName] + ' ' + EM.[LastName] As SalesPerson,                                                                       
			EM1.[FirstName] + ' ' + EM1.[LastName] As PackerName,OM.[DrvRemarks],[CommentType],[Comment]                                                                                                                           
			,isnull(DeductionAmount,0.00) as DeductionAmount,                                  
			isnull(CreditAmount,0.00)as CreditAmount,isnull(PayableAmount,0.00) as PayableAmount                                                                                                                                                  
			,ISNULL((select t.CreditAmount from CustomerCreditMaster as t  where t.CustomerAutoId=om.CustomerAutoid),0.00) as CustomerCredit 
			,om.TaxType,otm.OrderType AS OrderType,OrderRemarks ,                                                                                                                                
			ISNULL(MLQty,0) as MLQty,isnull(MLTax,0) as MLTax,CM.ContactPersonName,OM.Weigth_OZTaxAmount as WeightTax,                                                    
			ISNULL(AdjustmentAmt,0) as AdjustmentAmt,                                                                                
			CM.BusinessName,isnull(OM.Weigth_OZQty,0) as Weigth_OZQty,isnull(OM.Weigth_OZTax,0) as Weigth_OZTax,isnull(OM.Weigth_OZTaxAmount,0) as
			Weigth_OZTaxAmount,MLTaxPer FROM [dbo].[OrderMaster] As OM  --Weight Tax added by Rizwan Ahmad on 17-09-2019 
			INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.Status AND SM.[Category] = 'OrderMaster'                                                                                     
			INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[AutoId] = OM.[BillAddrAutoId]                                                                                                     
			INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = OM.[ShipAddrAutoId]                                                                                                                                                    
			INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                                                                                                                                                     
			INNER JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                                                                                                                                         
			INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                                                 
			INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]                                                                                                                                    
			LEFT JOIN [dbo].[CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                                          
			LEFT JOIN [dbo].[ShippingType] AS ST ON ST.[AutoId] = OM.[ShippingType]                                                                                                   
			left JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[SalesPersonAutoId]                                                                                                                   
			left JOIN [dbo].[EmployeeMaster] AS EM1 ON EM1.[AutoId] = OM.[PackerAutoId]
			INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
			WHERE OM.AutoId = @OrderAutoId                                                                                  
                                                                                                                      
			SELECT CM.[CategoryName],CM.[CategoryId],OIM.ProductAutoId,PM.[ProductId],PM.[ProductName],PM.[ProductLocation],                                                         
			OIM.[UnitTypeAutoId] AS UnitAutoId,UM.[UnitType],OIM.[QtyPerUnit],OIM.[UnitPrice],OIM.[TotalPieces],OIM.[SRP],OIM.[GP],OIM.[Tax],                                                                                                                     
			OIM.[NetPrice],OIM.[Barcode],OIM.[QtyShip],OIM.[RequiredQty] ,                                                                                                                                
			convert(decimal(18,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice,                                                                                                                                                    
			OIM.IsExchange,isnull(TaxValue,0.00) as TaxValue,ISNULL(isFreeItem,0) AS isFreeItem,UnitMLQty,                                                                                                                                                    
			ISNULL(UnitMLQty,0) as UnitMLQty,ISNULL(TotalMLQty,0) as TotalMLQty,                                                                                                                                                    
			oim.Item_TotalMLTax as TotalMLTax ,                                                                                
			(case @customertype when 2 then pd.whminprice when 3 then pd.costprice else [minprice] end) as [MinPrice],isnull(Weight_Oz,0) as Weight_Oz                                                             
			FROM [dbo].[OrderItemMaster] AS OIM                                                                                         
			INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                                                           
			INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitTypeAutoId                                                                               
			INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId]                                                                           
			INNER JOIN [dbo].[PackingDetails] AS PD ON PD.[ProductAutoId] = OIM.[ProductAutoId] AND PD.[UnitType] = OIM.UnitTypeAutoId                                                             
			WHERE [OrderAutoId] = @OrderAutoId                                                                                                                      
			ORDER BY CM.[CategoryName] ASC                                                                                                                                                    
                                                                                                                                                    
			SET @CustomerAutoId = (SELECT [CustomerAutoId] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId)                                                                                                        
                                                                                          
			SELECT OM.[AutoId],[OrderNo],CONVERT(VARCHAR(20),[OrderDate],101) AS OrderDate,DO.[AmtDue],DO.[GrandTotal],DO.[AmtPaid] , otm.OrderType AS OrderType ,  
			DATEDIFF(dd,OrderDate,GETDATE()) as Aging, orderdate as orderdateSort                                                                                                                                                    
			FROM [dbo].[OrderMaster] As OM                                                                                                                                             
			INNER JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId]    
			INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
			WHERE [CustomerAutoId] = @CustomerAutoId AND ([Status] = 6 or [Status] = 11 ) AND do.[AmtDue] > 0.00                                                                                               
			order by orderdateSort                                                              
                                                                             
			SELECT [AmountPaid],CONVERT(VARCHAR(20),PL.[PayDate],101) AS PayDate,EM.FirstName + ' ' + EM.[LastName] As EmpName,[Remarks] FROM [dbo].[PaymentLog] AS PL  
     
			INNER JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = PL.EmpAutoId WHERE [OrderAutoId] = @OrderAutoId     
                                                                                                                           
			SELECT @LoginEmpType As LoginEmpType                                                                                                                
                                                                                                                                                          
			select distinct ProductId,um.UnitType,pd.UnitType as UnitAutoId,Qty from OrderItemMaster as do                                                                                               
			inner join ProductMaster as pm on pm.AutoId=do.ProductAutoId                                                                                                                                                 
			inner join PackingDetails as pd on pd.ProductAutoId=pm.AutoId                                                     
			inner join UnitMaster as um on um.AutoId=pd.UnitType                                                                                       
			where OrderAutoId=@OrderAutoId                                                    
		END                                                                                                                                                  
                                                          
			select OrderAutoId,CreditAutoId,CreditNo,CONVERT(VARCHAR(10),CreditDate,101) AS CreditDate,ISNULL(TotalAmount,0.00) as ReturnValue,ISNULL(PaidAmount,0.00)  as amtDeducted,  
			ISNULL(BalanceAmount,0.00) as amtDue                                                          
			from CreditMemoMaster AS CM  where Status =3 --AND CustomerAutoId=@CustomerAutoId                                                                                                                                                     
			AND OrderAutoId=@OrderAutoId  
			
			SELECT * FROM TAXTypeMaster                                                                              
                                                                                     
		SELECT * FROM (                                                                                                                                                        
		SELECT 'Driver' as EmpType,DrvRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om                                                                
		inner join EmployeeMaster as emp on emp.AutoId=om.Driver                                                                                                             
		where om.AutoId=@OrderAutoId AND @LoginEmpType!=5 -- and  DrvRemarks is not null  and  DrvRemarks!=''                                                                                                 
		UNION                                                                   
		SELECT 'Account' as EmpType,AcctRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om                                                                                                                            
		inner join EmployeeMaster as emp on emp.AutoId=om.AccountAutoId where om.AutoId=@OrderAutoId                                                               
		AND @LoginEmpType!=6 --and  AcctRemarks is not null  and  AcctRemarks!=''                                                                                                                                                     
		UNION                                                                                                     
		SELECT 'Sales Person' as EmpType,OrderRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName  from OrderMaster as om                                                     
		inner join EmployeeMaster as emp on emp.AutoId=om.SalesPersonAutoId where                       
		om.AutoId=@OrderAutoId AND @LoginEmpType!=2 --and  OrderRemarks is not null  and  OrderRemarks!=''                                                                                                                                                   
  
		UNION                                                                                                       
		SELECT 'Packer' as EmpType,PackerRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om                                                            
		inner join EmployeeMaster as emp on emp.AutoId=om.PackerAutoId                                                                                                                                                   
		where om.AutoId=@OrderAutoId AND @LoginEmpType!=3  --and  PackerRemarks is not null  and  PackerRemarks!=''                                           
		UNION                                                                                                                                           
		SELECT 'Manager' as EmpType,ManagerRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName  from OrderMaster as om  
       
		inner join EmployeeMaster as emp on emp.AutoId=om.ManagerAutoId                                                                               
		where om.AutoId=@OrderAutoId AND @LoginEmpType!=1 --and  ManagerRemarks is not null  and  ManagerRemarks!=''  
            
		) AS T                                                            
                                   
		SELECT * FROM MLTaxMaster where TaxState in (select State from [BillingAddress] where AutoId in (select BillAddrAutoId from OrderMaster where AutoId=@OrderAutoId))  
   END                                                                                      
   ELSE IF @Opcode=419                                                                       
    BEGIN                                                         
     SELECT ROW_NUMBER() OVER(ORDER BY [AutoId] desc) AS RowNumber, * INTO #Values1 from                                                                                                                           
     (                                           
		SELECT OM.[AutoId],sm.StatusType as Status,OM.[OrderNo],CONVERT(VARCHAR(20),OM.[OrderDate],101) As OrderDate,CM.[CustomerName],CM.CustomerType,OM.[GrandTotal],                                                                                          
        
		CONVERT(VARCHAR(20),OM.[DelDate],101) As DelDate,(emp.FirstName + ' '+ emp.LastName) as SalesPerson,                                                                                                                              
		(emp1.FirstName + ' '+ emp1.LastName) as DriverName,om.Status as StatusCode,                                                                                                                                             
		otm.OrderType AS OrderType  FROM [dbo].[OrderMaster] AS OM                                                                                                      
		INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                          
		INNER JOIN [dbo].[EmployeeMaster] AS emp ON emp.AutoId = OM.SalesPersonAutoId                            
		left JOIN [dbo].[EmployeeMaster] AS emp1 ON emp1.AutoId = OM.Driver                                                                                                                                                     
		inner join StatusMaster as sm on sm.AutoId= OM.Status and Category='OrderMaster'     
		   INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
		WHERE ((@OrderStatus =0  and OM.[Status] in (6,11,8)) or (OM.[Status] = @OrderStatus and AccountAutoId is not null) or (@OrderStatus!=8 and om.Status=@OrderStatus))                                                                                    
             
		AND (@OrderType =0  or @OrderType is null or  OM.OrderType=@OrderType)                                                                                 
		and(@SalesPersonAutoId = 0 or om.[SalesPersonAutoId]=@SalesPersonAutoId)               
		and(@CustomerTypeAutoId = 0 or CM.CustomerType=@CustomerTypeAutoId)             
		and(@DriverAutoId = 0 or OM.Driver =@DriverAutoId)                                        
		and(@PayableAmount = 0 or OM.[GrandTotal] =@PayableAmount)                                                                                                                          
		AND (@CustomerAutoId is null or @CustomerAutoId=0 or CustomerAutoId=@CustomerAutoId)and                                                                                                                                                    
		(@OrderNo = '' OR @OrderNo IS NULL OR OM.[OrderNo] LIKE '%' + @OrderNo + '%')                                                    
		AND                                                                             
		(@FromDelDate = '' OR @FromDelDate IS NULL OR @ToDelDate = '' OR @ToDelDate IS NULL OR                                                                                                                                                     
		(CONVERT(date,OM.[OrderDate]) BETWEEN CONVERT(date,@fromDelDate) AND CONVERT(date,@ToDelDate)))                                                                                         
     )AS t                                                                                       
                                     
     SELECT CASE WHEN ISNULL(@PageSize,0)=0 then 0 else COUNT(*) end  as RecordCount,@PageSize as PageSize,@PageIndex as PageIndex FROM #Values1 
	 SELECT * FROM #Values1                                          
     WHERE ISNULL(@PageSize,0)=0 or                                                                     
  (RowNumber BETWEEN(@PageIndex - 1) * @PageSize + 1 AND (((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)                                                                   
    END                                                                                                                                                    
   ELSE IF @Opcode=420                                                                                           BEGIN                                                                                                                                         
             
SELECT TotalOrders, Convert(varchar(20),AssignDate,101) AS AssignDate FROM                                                                     
     (SELECT COUNT([AutoId]) AS TotalOrders,CONVERT(date,[AssignDate]) AS AssignDate FROM [dbo].[OrderMaster]                                                                                                                                                  
  
    
     
         
    WHERE [Driver] = @DriverAutoId AND (@Fromdate IS NULL OR @Fromdate='' OR @Todate IS NULL OR @Todate='' OR CONVERT(date,[AssignDate]) BETWEEN @Fromdate AND @Todate)                                                         
     GROUP BY CONVERT(date,[AssignDate])) As t ORDER BY AssignDate desc                                                                                                                                                    
    END                                                                       
   ELSE IF @Opcode=421                                                                                                                                                    
    BEGIN                                                                    
    SELECT CM.[CustomerName],S.[StateName] As State,SA.[City],SA.[Zipcode],[PackedBoxes] as StartBox,AddonPackedQty as [PackedBoxes],                                                                                                                          
     emp.FirstName + ' '+ISNULL(lastname,'') as SalesName                                                                             
      FROM [dbo].[OrderMaster] As OM                                                                                                                                     
     INNER JOIN [dbo].[ShippingAddress] AS SA ON SA.[AutoId] = OM.[ShipAddrAutoId]                                                               
     INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                                                                                      
     INNER join EmployeeMaster as emp on emp.AutoId= OM.SalesPersonAutoId                                                                                                                                                    
     INNER JOIN [dbo].[State] AS S ON S.[AutoId] = SA.[State] WHERE OM.[OrderNo] = @OrderNo                                                                                                                                          
     SELECT CompanyName,EmailAddress, [Address],[Website],[MobileNo],[FaxNo],TermsCondition,Logo FROM CompanyDetails                                                                                                                                           
 
    
       
        
         
     update [OrderMaster] set AddonPackedQty=PackedBoxes  WHERE [OrderNo] = @OrderNo                                                                                                                                   
   END                                                              
   ELSE IF @Opcode=422                
    BEGIN                                                                                                   
     SELECT Distinct TOP 25 OIM.[ProductAutoId],PM.[ProductId],PM.[ProductName],PM.[Stock] FROM OrderMaster As OM                                                                                                                                              
  
    
      
   INNER JOIN OrderItemMaster AS OIM ON OIM.OrderAutoId = OM.[AutoId]                                                                      
     INNER JOIN [dbo].[ProductMaster] AS PM ON PM.[AutoId] = OIM.[ProductAutoId] WHERE [CustomerAutoId] = @CustomerAutoId                                                           
    
      
       
        
           
              
               
    END                                                                                                                                                    
 ELSE IF @Opcode=423                                                                                                                                                    
    BEGIN                                                                
                                                                                                                 
                    
     IF EXISTS(SELECT * FROM [dbo].[ItemBarcode] WHERE [Barcode]=@Barcode)                                                                                                                                                    
     BEGIN                                                                                                                                         
         SET @ProductAutoId=(SELECT [ProductAutoId] FROM [dbo].[ItemBarcode] WHERE [Barcode]=@Barcode)                                                                          
            SET @UnitAutoId=(SELECT UnitAutoId FROM [dbo].[ItemBarcode] WHERE [Barcode]=@Barcode)                                                                                                                                        
      DECLARE @custType1 int=(SELECT CustomerType FROM CustomerMaster WHERE AutoId=@CustomerAutoId)                                                                     
      SELECT PM.AutoId as ProductAutoId,PM.ProductId,PM.ProductName,PD.Qty as UnitQty,PD.UnitType,                                                        
      (CASE                                                                     
      WHEN @custType1=2 THEN ISNULL(WHminPrice,0)                                                                                                                                                     
      WHEN @custType1=3  THEN ISNULL(CostPrice,0)                                           
      ELSE [MinPrice] END) AS [MinPrice] ,                                                                                                                                                    
      ISNULL((SELECT TOP 1 UnitType +  ' ('+ CONVERT(VARCHAR(20),PD.QTY)+' pcs)' from UnitMaster AS UM WHERE UM.AutoId=PD.UnitType),'')
	  AS UnitName,                          
      [CostPrice],                                                                                                                                                    
      (case                                                                                                   
      when @IsExchange=1 or @IsFreeItem=1 then 0.00                                                                                                                                            
      WHEN  @custType1=3 then CONVERT(DECIMAL(18,2),ISNULL(CostPrice,0))                                                                                                                                                
      else CONVERT(DECIMAL(18,2),[Price]) end) as  [Price],                                                     
      CONVERT(DECIMAL(18,2),((PM.[P_SRP]-([Price]/(CASE WHEN [Qty]=0 then 1 else [Qty] end )))/(CASE WHEN ISNULL(PM.[P_SRP],0)=0 then 1 else PM.[P_SRP] END)) * 100) AS GP,                                                   
      isnull((select Top 1 [CustomPrice] from [dbo].[ProductPricingInPriceLevel] AS PPL where                                    
      PPL.[ProductAutoId] = PD.[ProductAutoId] AND PPL.[UnitAutoId] = PD.[UnitType]                                                                                                                                                    
      AND [PriceLevelAutoId] = (SELECT [PriceLevelAutoId] FROM [dbo].[CustomerPriceLevel]                                                       
      WHERE [CustomerAutoId]=@CustomerAutoId)                                     
      Order by [CustomPrice] desc),null) as [CustomPrice],PM.[Stock]/(CASE WHEN Qty=0 THEN 1 ELSE QTY END)  as Stock,PM.[TaxRate],                                                                                                 
      PM.[P_SRP] AS [SRP],PD.Price as BasePrice,                                                                                                                  
      (case when @IsExchange=1 or @IsFreeItem=1 then 0.00 else PM.MLQty end) as MLQty,
	  (case when @IsExchange=1 or @IsFreeItem=1 then 0.00 else isnull(WeightOz,0) end) as WeightOz  
      into #result423 FROM [dbo].[PackingDetails] As PD                                                                                                                                                     
      INNER JOIN [dbo].[ProductMaster] AS PM ON PD.[ProductAutoId]=PM.[AutoId]                                                                                                                                                    
      WHERE PD.[ProductAutoId] = @ProductAutoId and PD.[UnitType] = @UnitAutoId                                                                                           
                                                                                                                                                          
      SET @QtyPerUnit=(SELECT TOP 1 UnitQty FROM #result423)                                                                                                 
      SET @MLQty=(SELECT TOP 1 MLQty FROM #result423)                                                                                                                                                    
      SET @UnitPrice=(SELECT TOP 1 Price FROM #result423)                        
      SET @minprice=(SELECT TOP 1 MinPrice FROM #result423)                                                                                                                 
      SET @SRP=(SELECT TOP 1 SRP FROM #result423)                                                                                                                                  
      SET @GP=(SELECT TOP 1 GP FROM #result423)                                                                                 
      declare @Customrice decimal (18,2)=((SELECT TOP 1 [CustomPrice] FROM #result423))                                      
    set @UnitPrice = (                                                                                                                           
       case                                                                                                                                                  
       when @IsExchange=1 or @IsFreeItem=1 then @UnitPrice                                                              
       when @Customrice IS NULL then @UnitPrice                                                                                                   
       else ( case when @Customrice < @minprice then  @UnitPrice else  @Customrice end)                                                                                                                                                  
       end                                                                         
      )               
      IF ISNULL(@DraftAutoId,0)=0                                                                                                                                      
      BEGIN                                                                                                                                                    
         INSERT INTO DraftOrderMaster([EmpAutoId],[CustomerAutoId],[OrderDate],[TotalItem],[ShippingType],[DeliveryDate],OrderType)                                                                          
         VALUES(@EmpAutoId,@CustomerAutoId,GETDATE(),1,@ShippingType,@DeliveryDate,2)                                                                             
		 SET @DraftAutoId=SCOPE_IDENTITY()                                                                               
         INSERT INTO DraftItemMaster(DraftAutoId,ProductAutoId,UnitAutoId,ReqQty,QtyPerUnit,UnitPrice,minprice,SRP,GP,TaxRate,IsExchange,IsFreeItem,UnitMLQty,TotalMLQty)                                                                          
         VALUES(@DraftAutoId,@ProductAutoId,@UnitAutoId,@ReqQty,@QtyPerUnit,@UnitPrice,@minprice,@SRP,@GP,                                                              
         @IsTaxable,@IsExchange,@IsFreeItem,@MLQty,(@MLQty*@ReqQty))                                                                                                              
      END                                                                                                                                                     
      ELSE                                                                                                                                                    
      BEGIN                                                                                                        
          IF NOT EXISTS(SELECT * FROM DraftItemMaster WHERE DraftAutoId= @DraftAutoId AND ProductAutoId=@ProductAutoId AND                                                                                                      
          UnitAutoId=@UnitAutoId AND IsExchange=@IsExchange AND IsFreeItem=@IsFreeItem)                                                                 
          BEGIN                                                                                     
            INSERT INTO DraftItemMaster(DraftAutoId,ProductAutoId,UnitAutoId,ReqQty,QtyPerUnit,UnitPrice,minprice,SRP,GP,TaxRate,IsExchange,IsFreeItem,UnitMLQty,TotalMLQty)                                                        
             VALUES(@DraftAutoId,@ProductAutoId,@UnitAutoId,@ReqQty,@QtyPerUnit,@UnitPrice,@minprice,@SRP,@GP,@IsTaxable,@IsExchange,@IsFreeItem,@MLQty,(@MLQty*@ReqQty))                                                                   
             UPDATE DraftOrderMaster SET [TotalItem]=[TotalItem]+1 WHERE DraftAutoId=@DraftAutoId                                                                                                                      
       END                                                                                                               
       else                                                                                                                                
       BEGIN                                                                                
              UPDATE DraftItemMaster SET ReqQty=ReqQty+@ReqQty,TotalMLQty=isnull(UnitMLQty,0)* (ReqQty+@ReqQty) WHERE  DraftAutoId= @DraftAutoId AND                                            
              ProductAutoId=@ProductAutoId AND UnitAutoId=@UnitAutoId AND IsExchange=@IsExchange AND IsFreeItem=@IsFreeItem                                                                         
   END                                                                                                                             
      END                                                                                                                                                    
      SELECT TOP 1  @DraftAutoId AS DraftAutoId,* FROM #result423                   
                                                                                              
         END                                                                                                                                                    
                                                                                                                                                           
    END                                                                                                                                                    
    ELSE IF @Opcode=108                                                                                                                                          
	BEGIN                                                     
		SET @State=(SELECT TOP 1 State FROM BillingAddress WHERE CustomerAutoId =@CustomerAutoId)  
		IF EXISTS(SELECT * FROM MLTaxMaster WHERE TaxsTATE=@State)                                                                                                  
		BEGIN                                                                                         
			SET @MLQty=ISNULL((SELECT MLQty FROM ProductMaster  WHERE AutoId=@ProductAutoId),0)                                                                                                                                                    
 		END                                                                                                                  
		ELSE                                                                                                                                              
		BEGIN                                                                                                                                                    
			SET @MLQty=0                                                                                                                      
		END                                                                                                                     
		IF ISNULL(@DraftAutoId,0)=0                                                                                                                                                    
		BEGIN                                              
			INSERT INTO DraftOrderMaster([EmpAutoId],[CustomerAutoId],[OrderDate],[TotalItem],[ShippingType],[DeliveryDate],OrderType,Remarks) 
			VALUES(@EmpAutoId,@CustomerAutoId,GETDATE(),1,@ShippingType,@DeliveryDate,2,@Remarks)  
			SET @DraftAutoId=SCOPE_IDENTITY()                                 
			IF @IsExchange=1 OR @IsFreeItem=1                              
			BEGIN                                                                                                          
			SET @UnitPrice=0                                                                                                                                                    
			SET @MLQty=0                                                                                                                         
			END                                        
			INSERT INTO DraftItemMaster(DraftAutoId,ProductAutoId,UnitAutoId,ReqQty,QtyPerUnit,UnitPrice,minprice,SRP,GP,TaxRate,IsExchange,isFreeItem,UnitMLQty,TotalMLQty,IsTaxable)                                                                       
			VALUES(@DraftAutoId,@ProductAutoId,@UnitAutoId,@ReqQty,@QtyPerUnit,@UnitPrice,@minprice,@SRP,@GP,@IsTaxable,@IsExchange,@IsFreeItem,@MLQty,@MLQty*@ReqQty,@IsTaxable)  
		END                                                                           
		ELSE                                           
		BEGIN  
			IF NOT EXISTS(SELECT * FROM DraftItemMaster WHERE DraftAutoId= @DraftAutoId                             
			AND ProductAutoId=@ProductAutoId AND UnitAutoId=@UnitAutoId and IsExchange=@IsExchange  and IsFreeItem=@IsFreeItem )                                                                                              
			BEGIN                                                                                                                                                    
				IF @IsExchange=1 OR @IsFreeItem=1                                                                                                                                                    
				BEGIN                                                                                 
					SET @UnitPrice=0                                                                                                                                                    
					SET @MLQty=0                                                                                                                                                          
				END                                                                        
				INSERT INTO DraftItemMaster(DraftAutoId,ProductAutoId,UnitAutoId,ReqQty,QtyPerUnit,UnitPrice,minprice,SRP,GP,TaxRate,IsExchange,isFreeItem,UnitMLQty,TotalMLQty,IsTaxable)                                                                       
				VALUES(@DraftAutoId,@ProductAutoId,@UnitAutoId,@ReqQty,@QtyPerUnit,@UnitPrice,@minprice,@SRP,@GP,@IsTaxable,@IsExchange,@IsFreeItem,@MLQty,@MLQty*@ReqQty,@IsTaxable) 
				UPDATE DraftOrderMaster SET [TotalItem]=[TotalItem]+1,Remarks=@Remarks WHERE DraftAutoId=@DraftAutoId                                                                                                           
			END                                                                                                  
			ELSE                                
			BEGIN                                                 
				UPDATE dt SET ReqQty=ReqQty+@ReqQty,dt.UnitMLQty=(case when @IsFreeItem=1 or @IsExchange=1 then 0 else  pm.MLQty end),                                                                                                                 
				dt.TotalMLQty=(case when @IsFreeItem=1 or @IsExchange=1 then 0 else  ISNULL(pm.MLQty,0)*@ReqQty end) from DraftItemMaster as dt                                                                        
				inner join ProductMaster as pm on pm.AutoId=dt.ProductAutoId  WHERE  DraftAutoId= @DraftAutoId                                                                     
				AND ProductAutoId=@ProductAutoId AND UnitAutoId=@UnitAutoId and IsExchange=@IsExchange                                                        
				and IsFreeItem=@IsFreeItem                                                                                         
			END                                  
	END 
	END                                                                       
    ELSE IF @Opcode=205                                                                                                                                                    
    BEGIN                                                                                                                                         
		  UPDATE DraftOrderMaster SET CustomerAutoId=@CustomerAutoId,DeliveryDate=@DeliveryDate,ShippingType=@ShippingType,Remarks=@Remarks,OverallDisc=@OverallDisc,
		  OverallDiscAmt=@OverallDiscAmt,ShippingCharges=@ShippingCharges
		  WHERE DraftAutoId= @DraftAutoId                                                                                                   
    END                                                                                                                                                    
    ELSE IF @Opcode=206                                                                                     
    BEGIN  
		UPDATE dt SET  ReqQty=@ReqQty,UnitPrice=@UnitPrice,                                                                                                                   
		dt.UnitMLQty=(case when @IsFreeItem=1 or @IsExchange=1 then 0 else  pm.MLQty end),
		dt.TotalMLQty=(case when @IsFreeItem=1 or @IsExchange=1 then 0 else  ISNULL(pm.MLQty,0)*@ReqQty end)                                                                   
		from DraftItemMaster as dt  inner join ProductMaster as pm on pm.AutoId=dt.ProductAutoId                                                     
		WHERE  DraftAutoId= @DraftAutoId AND ProductAutoId=@ProductAutoId AND UnitAutoId=@UnitAutoId                                                                                                                                 
		and IsExchange=@IsExchange and IsFreeItem=@IsFreeItem                                                                                                                                                     
    END                                                                    
		ELSE IF @Opcode=424                                                                                 
			BEGIN                                                 
			SELECT ROW_NUMBER() OVER(ORDER BY [DraftAutoId] desc) AS RowNumber, * INTO #Results1 from                                                                                                 
			(                                                       
			SELECT OM.[DraftAutoId],(EMP.FirstName+' '+EMP.LastName ) AS EmpName , CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,CONVERT(VARCHAR(20), OM.DeliveryDate, 101) AS DeliveryDate                                                
                                            
			,CM.[CustomerName] AS CustomerName,                                                     
			(select StatusType from StatusMaster where Category='DraftOrder' and AutoId=1)  AS Status,ST.ShippingType,                                                       
			(SELECT COUNT(distinct [ProductAutoId]) from [dbo].[DraftItemMaster] WHERE DraftAutoId=OM.DraftAutoId) AS NoOfItems                              
			FROM [dbo].[DraftOrderMaster] As OM                                                                   
			LEFT JOIN [dbo].[CustomerMaster] AS CM ON CM.AutoId = OM.CustomerAutoId                                                                              
			LEFT JOIN [dbo].[ShippingType] AS ST ON ST.AutoId = OM.ShippingType                                                                    
			INNER JOIN EmployeeMaster AS EMP ON EMP.AutoId=OM.EmpAutoId                                                                                                                             
			WHERE (@CustomerName is null or @CustomerName ='' or CM.[CustomerName] like '%' + @CustomerName + '%')      
			and (@FromDate is null or @FromDate = '' or @Todate is null or @Todate = '' or (CONVERT(DATE,[OrderDate]) between @FromDate AND @Todate))                                                                                
			and (@SalesPersonAutoId = 0 or OM.EmpAutoId=@SalesPersonAutoId)  and OrderType =2                                                                                                                                               
			)as t order by [DraftAutoId]                                                                                                                                                    
                                                                                                          
			SELECT COUNT(*) as RecordCount,@PageSize as PageSize,@PageIndex as PageIndex FROM #Results1                                                                                                                                                    
			SELECT * FROM #Results1                                                                                                  
			WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1                         
			SELECT EType.AutoId as EmpType from  EmployeeMaster AS EMP                                                                                                                                                     
			INNER JOIN EmployeeTypeMaster AS EType ON EType.AutoId=EMP.EmpType  where EMP.AutoId=@EmpAutoId                   
		END                                                                                                  
    ELSE IF @Opcode=303                                                                                                                                                    
    BEGIN                                                                                                    
     DELETE FROM DraftItemMaster WHERE DraftAutoId=@DraftAutoId                                                                                                                        
	 DELETE FROM DraftOrderMaster WHERE DraftAutoId=@DraftAutoId                                                                 
    END                                                                                                               
    ELSE IF @Opcode=425                                                                                                                          
    BEGIN      
	    Declare @StateID INT
		SET @CustomerAutoId=(SELECT CustomerAutoId  FROM DraftOrderMaster WHERE DraftAutoId=@DraftAutoId)
		SET @StateID=(Select State from BillingAddress Where Autoid In (Select DefaultBillAdd from CustomerMaster where AutoId=@CustomerAutoId))

		SELECT ShippingType,CustomerAutoId,convert(varchar,OrderDate,101) as OrderDate,(select StatusType from StatusMaster where Category='DraftOrder' and AutoId=1) as 
		Status,convert(varchar,DeliveryDate,101) as DeliveryDate,Remarks,(select Value from Tax_Weigth_OZ) as Tax_Weigth_OZ,
		ISNULL((Select TaxRate from MLTaxMaster WHERE TaxState=@StateID),0) as MLTaxRate,OverallDisc,OverallDiscAmt,ShippingCharges,Remarks FROM DraftOrderMaster WHERE DraftAutoId=@DraftAutoId 		

		declare @custType425 int=(select cm.CustomerType from CustomerMaster as cm where AutoId=@CustomerAutoId)   
		
		SELECT  temp1.ItemAutoId,temp1.DraftAutoId ,temp1.ProductAutoId ,temp1.UnitAutoId ,temp1.ReqQty ,temp1.QtyPerUnit,temp1.UnitPrice ,              
		(              
		CASE                                                                 
		WHEN @custType425=2 THEN ISNULL(WHminPrice,0)                                                                                                                                                 
		WHEN @custType425=3  THEN ISNULL(CostPrice,0)                                                                                                  
		ELSE pd.MinPrice END              
		) AS [minprice]  ,              
		temp1.SRP ,temp1.GP ,temp1.TaxRate ,temp1.NetPrice ,temp1.IsExchange,              
		temp1.isFreeItem,temp1.IsTaxable ,temp1.UnitMLQty ,temp1.TotalMLQty,PM.ProductId,PM.ProductName,um.UnitType,isnull(PM.WeightOz,0) as WeightOz  FROM DraftItemMaster as temp1                                                                      
		INNER JOIN ProductMaster AS PM ON PM.AutoId=temp1.ProductAutoId                                                   
		INNER JOIN PackingDetails AS pd ON pd.ProductAutoId=temp1.ProductAutoId  and pd.UnitType=temp1.UnitAutoId                                                  
		INNER JOIN UnitMaster AS UM ON UM.AutoId=temp1.UnitAutoId WHERE DraftAutoId=@DraftAutoId                                                                                                
    END                                                                                                                                                    
    ELSE IF @Opcode=426                                                                                                                                                    
     BEGIN                                                                                               
      SELECT ROW_NUMBER() OVER(ORDER BY [OrderNo] desc) AS RowNumber, * INTO #ResultsShip from                                                                                                                                                    
    (                                                                                                             
      SELECT OM.[AutoId],OM.OrderNo,CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,CM.[CustomerName] AS CustomerName,                                                              
   SM.StatusType AS Status,OM.[Status] As StatusCode,OM.[GrandTotal],OM.ShippingType,OM.[SalesPersonAutoId],                                                                                                                                                  
      (SELECT COUNT(distinct [ProductAutoId]) from [dbo].[OrderItemMaster] WHERE OrderAutoId=OM.AutoId) AS NoOfItems                                                                                              
      FROM [dbo].[OrderMaster] As OM                                                 
      INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.AutoId = OM.CustomerAutoId                                                                                                                                                     
      INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.[Status] AND SM.Category = 'OrderMaster'                                                                 
      WHERE (@OrderNo is null or @OrderNo ='' or OrderNo like '%' + @OrderNo + '%')                                                                                                                          
      and (@FromDate is null or @FromDate = '' or @Todate is null or @Todate = '' or ([OrderDate] between @FromDate and @Todate))         
      and (@OrderStatus is null or @OrderStatus=0 or OM.[Status]=@OrderStatus)                                                                                            
      and (@SalesPersonAutoId = 0 or OM.[SalesPersonAutoId]=@SalesPersonAutoId)                                                                                                                                  
       and OM.ShippingType=@ShippingType                                                
      )as t order by [OrderNo]                                                                                                                              
                              
      SELECT COUNT(*) as RecordCount,@PageSize as PageSize,@PageIndex as PageIndex FROM #ResultsShip                             
      SELECT * FROM #ResultsShip                                                                                                                                                    
      WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1                                                                                                                                           
  
    
      
        
         
     END                                                                                                                                                    
   Else If @Opcode=428                                                                                         
   Begin                                                                                   
       SELECT AutoId as OrderAutoId,CustomerAutoId,OrderNo,PackedBoxes,[DrvRemarks],CommentType,Comment,Status FROM OrderMaster                                                                                                                                
  
    
      
        
          
           
              
       WHERE AutoId=@OrderAutoId                                                                                                                                                    
   End                                                                                                                                                    
   Else If @Opcode=429                                                                      
   Begin         
   SELECT [AutoId],[OrderNo],CONVERT(VARCHAR(20),[OrderDate],101) AS OrderDate,DO.[AmtDue],DO.[GrandTotal],DO.[AmtPaid]     FROM [dbo].[OrderMaster] As OM                                                                                                     
 
     
      
        
          
            
             
                
                                       
     INNER JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId]                                                                                                                                
 WHERE [CustomerAutoId] = @CustomerAutoId AND ([Status] = 6 or [Status] = 11) AND DO.[AmtDue] > 0.00                                                               
                                                                     
   End                                                                                                                                                    
   Else If @Opcode=430                                                
   Begin                                                    
     SELECT [Address],[Website],[MobileNo],[FaxNo],TermsCondition,Logo FROM CompanyDetails                                            
     SELECT OM.[OrderNo],CONVERT(VARCHAR(20),OM.[OrderDate],101) As OrderDate,CONVERT(VARCHAR(20),OM.[AssignDate],101) as AssignDate ,(emp.FirstName + ' '+ emp.LastName) as Driver,        
     CM.[CustomerName],PayableAmount as [GrandTotal],[Stoppage],RootName,DriverRemarks FROM [dbo].[OrderMaster] AS OM                                                            
     INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                                                                                               
     INNER JOIN [dbo].[EmployeeMaster] AS emp ON emp.AutoId = OM.Driver                                            
     INNER JOIN [dbo].[StatusMaster] AS SM ON SM.[AutoId] = OM.[Status]                                                                                                                                                    
     AND SM.[Category] = 'OrderMaster'                                                                                                                                                    
      WHERE OM.[Driver] =@DriverAutoId    
	  and OM.Status in (4,5)                                                                                                                                                
      and CONVERT(DATE,OM.[AssignDate])=(CASE WHEN @AsgnDate IS NOT NULL OR @AsgnDate != ''  	
     THEN @AsgnDate ELSE CONVERT(DATE,AssignDate) END)                                                                                           
     Order by (emp.FirstName + ' '+ emp.LastName), convert(int,[Stoppage]),OM.[OrderNo]                                                                                                                                                     
     select @DriverAutoId,@AsgnDate                                                                                                                                                        
   End                                                                                                         
                                                                                                                                                       
   ELSE IF @Opcode=431                                                                 
   BEGIN                                                                                                 
    SELECT [SecurityValue] FROM [dbo].[TBL_MST_SecurityMaster] WHERE [SecurityValue]=@CheckSecurity                                                                                                                 
   END                                                              
   ELSE IF @Opcode=51                                                            
   BEGIN                                                                          
  SELECT CompanyName,EmailAddress, [Address],[Website],[MobileNo],[FaxNo],TermsCondition,Logo FROM CompanyDetails                                                                                                                                              
 
  IF EXISTS(SELECT [AutoId] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId AND ([PaymentRecev] IS NOT NULL OR [PaymentRecev] != ''))                                                                               
                              
  BEGIN                                                                                                                   
  SELECT OM.[AutoId],OM.[OrderNo],CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,                                                                                                                                    
  ISNULL(CONVERT(VARCHAR(20), OM.[DeliveryDate],101),CONVERT(VARCHAR(20), OM.[DelDate],101) + ' ' +convert(varchar(10), OM.[DelDate], 108)) AS DeliveryDate,  CM.[AutoId] As CustAutoId,CM.[CustomerId]              
  ,CM.[CustomerName],CM.[Contact1] + '/' + CM.[Contact2] As Contact,CTy.CustomerType,CT.[TermsDesc],ST.[ShippingType],                                                                                                                                         
    
  BA.[Address] As BillAddr,                                                                                                                                           
  S.[StateName] AS State1,ba.State as [stateid],BA.[City] AS City1,BA.[Zipcode] As Zipcode1,SA.[Address] As ShipAddr,S1.[StateName] As State2,SA.[City] AS City2,                                              
  SA.[Zipcode] As Zipcode2,OM.[Status] As StatusCode,SM.[StatusType] AS Status,OM.[PackedBoxes],DO.[TotalAmount],DO.[OverallDisc],DO.[OverallDiscAmt],                                                                                        
  DO.[ShippingCharges],DO.[TotalTax],DO.[GrandTotal],DO.[AmtPaid],DO.[AmtDue],OM.[PaymentRecev],OM.[RecevAmt],OM.[AmtValue],OM.[ValueChanged],                                                                                                                 
 
  OM.[DiffAmt],OM.[NewTotal],OM.[PayThru],OM.CheckNo,OM.[DrvRemarks],OM.[AcctRemarks],EM.[FirstName] + ' ' + EM.[LastName] As SalesPerson,                                                                          
  EM1.[FirstName] + ' ' + EM1.[LastName] As PackerName,[CommentType],[Comment],isnull(do.CreditMemoAmount,om.DeductionAmount) as DeductionAmount,                                                                                                                                
 
  ISNULL(DO.CreditAmount,om.CreditAmount)as CreditAmount,isnull(do.PayableAmount,OM.PayableAmount) as PayableAmount,                                                                           
  ISNULL((select t.CreditAmount from CustomerCreditMaster as t  where t.CustomerAutoId=om.CustomerAutoid),0.00) as CustomerCredit                                                                                                                              
  ,ISNULL((select top 1 PrintLabel from MLTaxMaster as mlt where mlt.TaxState=BA.State),'') as MLTaxPrintLabel
  ,om.TaxType,otm.OrderType AS OrderType,OrderRemarks,                                                                                                                                                    
  ISNULL(OM.MLQty,0) as MLQty,isnull(DO.MLTax,0) as MLTax,CM.ContactPersonName,ISNULL(om.AdjustmentAmt,0) as AdjustmentAmt, 
  TTM.PrintLabel AS TaxPrintLabel,(SELECT top 1 PrintLabel from [dbo].[Tax_Weigth_OZ]) AS WeightTaxPrintLabel,
  CM.BusinessName,ISNULL(UnitMLTax,0) as UnitMLTax,om.Weigth_OZTaxAmount as  WeightTax,OM.TaxValue  FROM [dbo].[OrderMaster] As OM                                                                                                                        
  INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.Status AND SM.[Category] = 'OrderMaster'                                                                                                                                              
  INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[AutoId] = OM.[BillAddrAutoId]                                                                 
  INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = OM.[ShipAddrAutoId]                           
  INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                                                                                                                                                     
  INNER JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                                                                                                                                     
  INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                                                                                                                                               
  INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]                                                                                             
  LEFT JOIN [dbo].[CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                                                                                                                    
  LEFT JOIN [dbo].[ShippingType] AS ST ON ST.[AutoId] = OM.[ShippingType]                                                                                                                        
  LEFT JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[SalesPersonAutoId]                                                                              
  LEFT JOIN [dbo].[EmployeeMaster] AS EM1 ON EM1.[AutoId] = OM.[PackerAutoId]      
    LEFT JOIN [dbo].[TaxTypeMaster] AS TTM ON TTM.[AutoId] = OM.[TaxType]  
  INNER JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId] 
     INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
  WHERE OM.AutoId = @OrderAutoId                                                                                                                                                 
                                                                                                                                      
  SELECT CM.[CategoryName],CM.[CategoryId],DOI.[ProductAutoId],PM.[ProductId],PM.[ProductName],DOI.[UnitAutoId],UM.[UnitType],DOI.[QtyPerUnit],                                                                                                               
   
  DOI.[UnitPrice],DOI.[Barcode],DOI.[QtyShip],DOI.[QtyDel],DOI.[TotalPieces],DOI.[SRP],DOI.[GP],  
  case when DOI.[GP] < 0 then '---' else Convert(varchar(15),DOI.[GP]) end as PrintGP  
  ,DOI.[Tax],DOI.[NetPrice]                                                                                                                                         
  ,convert(decimal(18,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice,ISNULL(FreshReturnQty,0) as FreshReturnQty,FreshReturnUnitAutoId,                                                                                           
  ISNULL(DamageReturnQty,0) as DamageReturnQty,DamageReturnUnitAutoId,                                                                                         
  IsExchange,isnull(TaxValue,0.00) as TaxValue,ISNULL(isFreeItem,0) AS isFreeItem,                                                                   
  ISNULL(UnitMLQty,0) as UnitMLQty,ISNULL(TotalMLQty,0) as TotalMLQty,                                                                                                                                                    
  DOI.Item_TotalMLTax as TotalMLTax               
  FROM [dbo].[Delivered_Order_Items] AS DOI                                                                                                                                      
  INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = DOI.[ProductAutoId]                                                                                      
  INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = DOI.UnitAutoId                                    
  INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId] WHERE [OrderAutoId]=@OrderAutoId                                                              
  AND QtyShip>0                                                             
   ORDER BY CM.[CategoryName] ASC                                                                            
  SET @CustomerAutoId = (SELECT [CustomerAutoId] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId)                                                                                       
                                                                                              
  SELECT OM.[AutoId],[OrderNo],CONVERT(VARCHAR(20),[OrderDate],101) AS OrderDate,DO.[AmtDue],DO.[GrandTotal],DO.[AmtPaid] ,                                                                                         
  otm.OrderType AS OrderType ,DATEDIFF(dd,OrderDate,GETDATE()) as Aging, orderdate as orderdateSort                                                                                                              
  
  FROM [dbo].[OrderMaster] As OM                                                                                            
  INNER JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId]   
  INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
  WHERE [CustomerAutoId] = @CustomerAutoId AND ([Status] = 6 or [Status] = 11) AND DO.[AmtDue] > 0.00                                                                                  
  order by orderdateSort                                                                                                                           
                                                                 
  END                                                          
  ELSE                                                                                                   
  BEGIN                                                                                                  
  SELECT OM.[AutoId],OM.[OrderNo],CONVERT(VARCHAR(20),OM.[OrderDate], 101) AS OrderDate,                                                                   
  CONVERT(VARCHAR(20), OM.[DeliveryDate],101) + ' ' + convert(varchar(10), getdate(), 108)--CONVERT(VARCHAR(20),OM.[DeliveryDate], 131)                                                                                                                        
  
  AS DeliveryDate,                                                                                                                                                    
  CM.[AutoId] As CustAutoId,CM.[CustomerId],CM.[CustomerName],CM.[Contact1] + '/' + CM.[Contact2] As Contact,CTy.CustomerType,CT.[TermsDesc],ST.[ShippingType],                                        
  BA.[Address] As BillAddr,S.[StateName] AS State1,BA.[City] AS City1,BA.[Zipcode] As Zipcode1,SA.[Address] As ShipAddr,S1.[StateName] As State2,                                                                                                    
  SA.[City] AS City2,SA.[Zipcode] As Zipcode2,OM.[Status] As StatusCode,SM.[StatusType] AS Status,OM.[TotalAmount],OM.[OverallDisc],OM.[OverallDiscAmt],                                                 
  OM.[ShippingCharges],OM.[TotalTax],OM.[GrandTotal],OM.[PackedBoxes],EM.[FirstName] + ' ' + EM.[LastName] As SalesPerson,                                                                                                                       
  EM1.[FirstName] + ' ' + EM1.[LastName] As PackerName,OM.[DrvRemarks],[CommentType],[Comment]                                    
  ,isnull(DeductionAmount,0.00) as DeductionAmount,                                                                                                                                                    
  isnull(CreditAmount,0.00)as CreditAmount,isnull(PayableAmount,0.00) as PayableAmount                                                                    
  ,ISNULL((select t.CreditAmount from CustomerCreditMaster as t  where t.CustomerAutoId=om.CustomerAutoid),0.00) as CustomerCredit               
  ,om.TaxType,otm.OrderType AS OrderType,OrderRemarks ,                                                                                                                                      
  ISNULL(MLQty,0) as MLQty,isnull(MLTax,0) as MLTax,CM.ContactPersonName,ISNULL(AdjustmentAmt,0) as AdjustmentAmt,
       TTM.PrintLabel AS TaxPrintLabel,(SELECT top 1 PrintLabel from [dbo].[Tax_Weigth_OZ]) AS  WeightTaxPrintLabel,
                                                                                                                   
  CM.BusinessName,om.Weigth_OZTaxAmount as  WeightTax,OM.TaxValue FROM [dbo].[OrderMaster] As OM                                                                                                                                                    
  INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.Status AND SM.[Category] = 'OrderMaster'                                                                                   
  INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[AutoId] = OM.[BillAddrAutoId]                                                                
  INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = OM.[ShipAddrAutoId]                                                       
  INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                                                                                                                                                     
  INNER JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                                                    
  INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                                                                                                    
  INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]                                                                           
  LEFT JOIN [dbo].[CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                                                                                                                                           
  LEFT JOIN [dbo].[ShippingType] AS ST ON ST.[AutoId] = OM.[ShippingType]                                               
  left JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[SalesPersonAutoId]                                                                                                                              
  left JOIN [dbo].[EmployeeMaster] AS EM1 ON EM1.[AutoId] = OM.[PackerAutoId]
  LEFT JOIN [dbo].[TaxTypeMaster] AS TTM ON TTM.[AutoId] = OM.[TaxType]  
     INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
  WHERE OM.AutoId = @OrderAutoId                                                                                                                                                  
  
                                                                             
  SELECT @OrderStatus=Status FROM [dbo].[OrderMaster] As OM WHERE OM.AutoId = @OrderAutoId                                                                                                                                                                     
  
  SELECT CM.[CategoryName],CM.[CategoryId],OIM.ProductAutoId,PM.[ProductId],PM.[ProductName],PM.[ProductLocation],                                                                 
  OIM.[UnitTypeAutoId] AS UnitAutoId,UM.[UnitType],OIM.[QtyPerUnit],OIM.[UnitPrice],OIM.[TotalPieces],OIM.[SRP],OIM.[GP],  
  case when OIM.[GP] < 0 then '---' else convert(varchar(15),OIM.[GP]) end as PrintGP  
  ,OIM.[Tax],                                                 
  OIM.[NetPrice],OIM.[Barcode],OIM.[QtyShip],OIM.[RequiredQty] ,convert(decimal(18,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice,                                                                                                        
  
  OIM.IsExchange,isnull(TaxValue,0.00) as TaxValue,ISNULL(isFreeItem,0) AS isFreeItem,UnitMLQty,                                                                                                                                                    
  ISNULL(UnitMLQty,0) as UnitMLQty,ISNULL(TotalMLQty,0) as TotalMLQty,                                                                                     
  oim.Item_TotalMLTax as TotalMLTax                                                                  
  FROM [dbo].[OrderItemMaster] AS OIM                                                  
  INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                                               
  INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitTypeAutoId                                                                        
  INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId] WHERE [OrderAutoId] = @OrderAutoId                                                                                
  AND (QtyShip>0)                                                                           
  ORDER BY CM.[CategoryName] ASC                                                                                  
                                                         
  SET @CustomerAutoId = (SELECT [CustomerAutoId] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId)                                                                                                     
                                                                                                                          
  SELECT OM.[AutoId],[OrderNo],CONVERT(VARCHAR(20),[OrderDate],101) AS OrderDate,DO.[AmtDue],DO.[GrandTotal],DO.[AmtPaid] ,                                                                                                                             
 otm.OrderType AS OrderType ,DATEDIFF(dd,OrderDate,GETDATE()) as Aging, orderdate as orderdateSort                                                                                                             
   
  FROM [dbo].[OrderMaster] As OM                                                                                                                         
  INNER JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId]     
     INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
  WHERE [CustomerAutoId] = @CustomerAutoId AND ([Status] = 6 or [Status] = 11 ) AND do.[AmtDue] > 0.00                                                                                   
  order by orderdateSort                                                                                                                                                     
  END                                                                                                                                                         
                                                                    
  select OrderAutoId,CreditAutoId,CreditNo,CONVERT(VARCHAR(10),CreditDate,101) AS CreditDate,ISNULL(TotalAmount,0.00) as ReturnValue,ISNULL(PaidAmount,0.00)  as amtDeducted,                                                                                  
  
  ISNULL(BalanceAmount,0.00) as amtDue                                                                                                            
  from CreditMemoMaster AS CM where Status =3                                                                                               
  AND OrderAutoId=@OrderAutoId                                                                                    
   END                                                                                        
   ELSE IF @Opcode=432                                                                                                                                  
   BEGIN                                                                                    
  SELECT OM.[AutoId],OM.[OrderNo],CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,CONVERT(VARCHAR(20), OM.[DeliveryDate], 101) AS DeliveryDate,                                                                                                         
  
  CM.[CustomerId],CM.[CustomerName],CTy.CustomerType,CM.[Contact1] + '/' + CM.[Contact2] As Contact,CT.[TermsDesc],ST.[ShippingType],BA.[Address] As BillAddr,S.[StateName] AS State1,                                                                         
  
  BA.[City] AS City1,BA.[Zipcode] As Zipcode1,SA.[Address] As ShipAddr,S1.[StateName] As State2,SA.[City] AS City2,SA.[Zipcode] As Zipcode2,      
  OM.[Status] As StatusCode,SM.[StatusType] AS Status,OM.[TotalAmount],OM.[OverallDisc],OM.[OverallDiscAmt],OM.[ShippingCharges],OM.[TotalTax],                                                                                                                
 
  OM.[GrandTotal],EM.[FirstName] + ' ' + EM.[LastName] As SalesPerson,                                                                                                
  (select EM1.[FirstName] + ' ' + EM1.[LastName] from [dbo].[EmployeeMaster] AS EM1 where EM1.[AutoId]                                                                                               = OM.PackerAutoId) AS PackerName,                         
  
  (select EM1.[FirstName] + ' ' + EM1.[LastName] from [dbo].[EmployeeMaster] AS EM1 where EM1.[AutoId]                                                                                                                        
  = OM.warehouseAutoid) AS WarehouseName,ISNULL(AdjustmentAmt,0) as AdjustmentAmt,                                                                                                                                  
  om.TaxType,OrderRemarks,WarehouseRemarks  FROM [dbo].[OrderMaster] As OM                                                                       
  INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.Status AND SM.[Category] = 'OrderMaster'                                                                                                        
  INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[AutoId] = OM.[BillAddrAutoId]                                                                                                                        
  INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = OM.[ShipAddrAutoId]                                                                                                                                                    
  INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                                                                                                                                                     
  INNER JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                                                                                                           
  INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                                                         
  INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]                                                                                                                                  
  LEFT JOIN [dbo].[CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                                     
  LEFT JOIN [dbo].[ShippingType] AS ST ON ST.[AutoId] = OM.[ShippingType]                                                        
  INNER JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[SalesPersonAutoId]                                                                                                                                   
  WHERE OM.AutoId = @OrderAutoId                                      
                                                                                             
  SELECT * FROM (                                                                                                                                                    
  SELECT CM.[CategoryName],CM.[CategoryId],OIM.ProductAutoId,PM.[ProductId],PM.[ProductName],(                                
   select top 1 Location  from PackingDetails as pd where pd.ProductAutoId=PM.AutoId and          
   pd.UnitType=OIM.UnitTypeAutoId) AS [ProductLocation],                                                                 
   OIM.[UnitTypeAutoId],UM.[UnitType],OIM.[QtyPerUnit],OIM.[UnitPrice],OIM.[TotalPieces],OIM.[SRP],OIM.[GP],OIM.[Tax],                                                                           
   OIM.[NetPrice],OIM.[Barcode],OIM.[QtyShip],OIM.[RequiredQty],OIM.IsExchange FROM [dbo].[OrderItemMaster] AS OIM                                                                       
   INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                                                                                                                                                    
   INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitTypeAutoId                                                                                                                                                     
   INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId]                                                                                                                          
   WHERE [OrderAutoId] = @OrderAutoId AND ISNULL(QtyShip,0)=0                                                                                                            
  ) AS T ORDER BY  ISNULL(ProductLocation,'') ASC,ProductId                                                                                                                                                      
                                                                                        
   END                                                          
                                                           
   if @Opcode = 433                                                        
   begin                                                        
                                                        
    SELECT em.[AutoId],[FirstName] + ' ' + [LastName] + ' [' + etm.TypeName + ']' as Name, etm.AutoId as EmpTypeAutoId, (select count (1) from OrderMaster om where Status in (4,5) and em.AutoId = om.Driver) as AssignOrders FROM [dbo].[EmployeeMaster] em 
  
     
     
         
         
             
             
                
                       
                                                      
  inner join EmployeeTypeMaster etm on etm.AutoId =  em.EmpType                                                        
  WHERE ([EmpType]=5 or em.autoid in (SELECT SalesPersonAutoId                                                        
   FROM [dbo].[OrderMaster] WHERE CONVERT(date,AssignDate) = CONVERT(date,@AsgnDate) and Driver = @DriverAutoId                                                
  ) ) AND Status=1                                                           
   order by em.EmpType,FirstName                                                         
                                                        
   SELECT [OrderNo],[Driver],PackerAutoId,convert(varchar(10),isnull(AssignDate,getdate()),101) as AssignDate                                                                                   
   FROM [dbo].[OrderMaster] WHERE CONVERT(date,AssignDate) = CONVERT(date,@AsgnDate)                                                        
   end                                       
                                                           
   if @Opcode = 434                                                        
   begin     
   begin tran    
    SELECT AutoId as OrderAutoId INTO #tbl434  FROM OrderMaster where Driver = @DraftAutoId and Convert(date,AssignDate) = Convert(date, @AsgnDate )                                                                                                           
                                                         
    
  update OrderMaster set Driver = @DriverAutoId, UPDATEDATE = GETDATE(), UPDATEDBY = @EmpAutoId                                                    
  where Driver = @DraftAutoId and Convert(date,AssignDate) = Convert(date, @AsgnDate )      
     
  SET @LogRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=22), '[DriverName]', (SELECT [FirstName] + ' ' + [LastName] AS DrvName FROM [dbo].[EmployeeMaster] WHERE [AutoId] = @DriverAutoId))                               
             
  INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])              
  select 22,@EmpAutoId,getdate(),@LogRemark,OrderAutoId from #tbl434   
  COMMIT TRANSACTION  
   end                                          
                                           
   if @Opcode = 213                                                        
   begin   
   
	  set @ProductAutoId=(select ProductAutoId from OrderItemMaster where AutoId=@DraftAutoId)                                              
	  set @UnitAutoId=(select UnitTypeAutoId from OrderItemMaster where AutoId=@DraftAutoId)                                                    
	  set @CustomerAutoId=(SELECT CustomerAutoId FROM OrderMaster WHERE AutoId=@OrderAutoId)                                        
	  set @customerType=(SELECT CustomerType FROM CustomerMaster WHERE AutoId=@CustomerAutoId) 
	  Declare @CustomPrice decimal(18,2)=0.00
	  SELECT @MinPrice= (CASE                                                                                                                                                     
	  WHEN @custType=2 THEN ISNULL(WHminPrice,0)                                          
	  WHEN @custType=3  THEN ISNULL(CostPrice,0)                                                                                                                   
	  ELSE [MinPrice] END), 
	  @UnitPrice=(Case when @customerType=3 then CostPrice else Price end),
	  @CustomPrice=isnull((select Top 1 [CustomPrice] from [dbo].[ProductPricingInPriceLevel] AS PPL where PPL.[ProductAutoId] = PD.[ProductAutoId] AND PPL.[UnitAutoId] = PD.[UnitType]                                                                                                      
	  AND [PriceLevelAutoId] = (SELECT [PriceLevelAutoId] FROM [dbo].[CustomerPriceLevel] WHERE [CustomerAutoId]=@CustomerAutoId)                                                                                      
	  Order by [CustomPrice] desc),null)
	  FROM [dbo].[PackingDetails] As PD                        
	  INNER JOIN [dbo].[ProductMaster] AS PM ON PD.[ProductAutoId]=PM.[AutoId]                                                                                                                                  
	  WHERE PD.[ProductAutoId] = @ProductAutoId and PD.[UnitType] = @UnitAutoId    
	  
	Update OrderItemMaster SET IsExchange=@IsExchange,isFreeItem=@IsFreeItem,Tax=@IsTaxable,
	UnitPrice=
	case when @IsExchange=0  and @IsFreeItem=0 then 
	(case when  @CustomPrice is null then @UnitPrice  
	when @MinPrice > @CustomPrice then @UnitPrice else @CustomPrice end
	)
	else 0.00 end,
	Oim_Discount=(case when @IsExchange=1  or @IsFreeItem=1 then 0.00 else Oim_Discount end),
	Oim_DiscountAmount=(case when @IsExchange=1  or @IsFreeItem=1 then 0.00 else Oim_Discount end)
	where AutoId=@DraftAutoId


	Select @UnitPrice as Price,@CustomPrice as CustomPrice,@minprice as MinPrice
   end                                                        
  --    ELSE IF @Opcode=4231                                                                                                                                                    
  --  BEGIN                                                                
                                                                                                                 
                    
  --   IF EXISTS(SELECT * FROM [dbo].[ItemBarcode] WHERE [Barcode]=@Barcode)                                                                                                                                                    
  --   BEGIN                                                                                                                                      
  --       SET @ProductAutoId=(SELECT [ProductAutoId] FROM [dbo].[ItemBarcode] WHERE [Barcode]=@Barcode)                                                                          
  --       SET @UnitAutoId=(SELECT UnitAutoId FROM [dbo].[ItemBarcode] WHERE [Barcode]=@Barcode)         
		-- declare @TotalReqQty int=null
		-- set @TotalReqQty=(select Qty from PackingDetails where ProductAutoId=@ProductAutoId and UnitType=@UnitAutoId)*@ReqQty
		--if ((select Stock from ProductMaster where AutoId=@ProductAutoId) >=@TotalReqQty)
		--begin
  --    set @custType1 =(SELECT CustomerType FROM CustomerMaster WHERE AutoId=@CustomerAutoId)                                                                     
  --    SELECT PM.AutoId as ProductAutoId,PM.ProductId,PM.ProductName,PD.Qty as UnitQty,PD.UnitType, Stock as ProductStock,                                                       
  --    (CASE                                                                     
  --    WHEN @custType1=2 THEN ISNULL(WHminPrice,0)                                                                                                                                                     
  --    WHEN @custType1=3  THEN ISNULL(CostPrice,0)                                           
  --    ELSE [MinPrice] END) AS [MinPrice] ,                                                                                                                                                    
  --    ISNULL((SELECT TOP 1 UnitType +  ' ('+ CONVERT(VARCHAR(20),PD.QTY)+' pcs)' from UnitMaster AS UM WHERE UM.AutoId=PD.UnitType),'') AS UnitName,                                                                                                          
  
                                          
  --    [CostPrice],                                                                                                                                                    
  --    (case                                                                                                   
  --    when @IsExchange=1 or @IsFreeItem=1 then 0.00                                                                                                                                            
  --    WHEN  @custType1=3 then CONVERT(DECIMAL(10,2),ISNULL(CostPrice,0))                                                                                                                                                
  --    else CONVERT(DECIMAL(10,2),[Price]) end) as  [Price],                                                     
  --    CONVERT(DECIMAL(10,2),((PM.[P_SRP]-([Price]/(CASE WHEN [Qty]=0 then 1 else [Qty] end )))/(CASE WHEN ISNULL(PM.[P_SRP],0)=0 then 1 else PM.[P_SRP] END)) * 100) AS GP,                                                   
  --    isnull((select Top 1 [CustomPrice] from [dbo].[ProductPricingInPriceLevel] AS PPL where                                    
  --    PPL.[ProductAutoId] = PD.[ProductAutoId] AND PPL.[UnitAutoId] = PD.[UnitType]                                                                                                                                                    
  --    AND [PriceLevelAutoId] = (SELECT [PriceLevelAutoId] FROM [dbo].[CustomerPriceLevel]                                                       
  --    WHERE [CustomerAutoId]=@CustomerAutoId)                                     
  --    Order by [CustomPrice] desc),null) as [CustomPrice],PM.[Stock]/(CASE WHEN Qty=0 THEN 1 ELSE QTY END)  as Stock,PM.[TaxRate],                                                                                                 
  --    PM.[P_SRP] AS [SRP],                                                                                                                    
  --    (case when @IsExchange=1 or @IsFreeItem=1 then 0.00 else PM.MLQty end) as MLQty,(case when @IsExchange=1 or @IsFreeItem=1 then 0.00 else isnull(WeightOz,0) end) as WeightOz                                              
                      
  --    into #result4231 FROM [dbo].[PackingDetails] As PD                                                                                                                                                     
  --    INNER JOIN [dbo].[ProductMaster] AS PM ON PD.[ProductAutoId]=PM.[AutoId]                                                                                                                                                    
  --    WHERE PD.[ProductAutoId] = @ProductAutoId and PD.[UnitType] = @UnitAutoId                                                                                           
                                                                                                                                                          
  --    SET @QtyPerUnit=(SELECT TOP 1 UnitQty FROM #result4231)                                                                                                 
  --    SET @MLQty=(SELECT TOP 1 MLQty FROM #result4231)                                                                                                                                                    
  --    SET @UnitPrice=(SELECT TOP 1 Price FROM #result4231)                        
  --    SET @minprice=(SELECT TOP 1 MinPrice FROM #result4231)                                                                                                                 
  --    SET @SRP=(SELECT TOP 1 SRP FROM #result4231)                                                                                                                                  
  --    SET @GP=(SELECT TOP 1 GP FROM #result4231)                                                                                 
  --    set @Customrice=((SELECT TOP 1 [CustomPrice] FROM #result4231))                                      
  --  set @UnitPrice = (                                                                                                                           
  --     case                                                                                                                                                  
  --     when @IsExchange=1 or @IsFreeItem=1 then @UnitPrice                                                              
  --     when @Customrice IS NULL then @UnitPrice                                                                                                   
  --     else ( case when @Customrice < @minprice then  @UnitPrice else  @Customrice end)                                                                                                                                                  
  --     end                                                                         
  --    )               
  --    IF ISNULL(@DraftAutoId,0)=0                                                                                                                                      
  --    BEGIN                                                                                                                                                    
  --       INSERT INTO DraftOrderMaster([EmpAutoId],[CustomerAutoId],[OrderDate],[TotalItem],[ShippingType],[DeliveryDate],OrderType,Remarks)                                                                          
  --       VALUES(@EmpAutoId,@CustomerAutoId,GETDATE(),1,@ShippingType,@DeliveryDate,2,@Remarks)                                                                             
		-- SET @DraftAutoId=SCOPE_IDENTITY()                                                                             
  --       INSERT INTO DraftItemMaster(DraftAutoId,ProductAutoId,UnitAutoId,ReqQty,QtyPerUnit,UnitPrice,minprice,SRP,GP,TaxRate,IsExchange,IsFreeItem,UnitMLQty,TotalMLQty)                                                                          
  --       VALUES(@DraftAutoId,@ProductAutoId,@UnitAutoId,@ReqQty,@QtyPerUnit,@UnitPrice,@minprice,@SRP,@GP,                                                              
  --       @IsTaxable,@IsExchange,@IsFreeItem,@MLQty,(@MLQty*@ReqQty))                                                                                                              
  --    END                                                                                                                                                     
  --    ELSE                                                                                                                                                    
  --    BEGIN                                                                                                        
  --        IF NOT EXISTS(SELECT * FROM DraftItemMaster WHERE DraftAutoId= @DraftAutoId AND ProductAutoId=@ProductAutoId AND                                                                                                      
  --        UnitAutoId=@UnitAutoId AND IsExchange=@IsExchange AND IsFreeItem=@IsFreeItem)                                                                 
  --        BEGIN                                                                                   
  --              INSERT INTO DraftItemMaster(DraftAutoId,ProductAutoId,UnitAutoId,ReqQty,QtyPerUnit,UnitPrice,minprice,SRP,GP,TaxRate,IsExchange,IsFreeItem,UnitMLQty,TotalMLQty)                                                        
  --               VALUES(@DraftAutoId,@ProductAutoId,@UnitAutoId,@ReqQty,@QtyPerUnit,@UnitPrice,@minprice,@SRP,@GP,@IsTaxable,@IsExchange,@IsFreeItem,@MLQty,(@MLQty*@ReqQty))                                                                   
  --               UPDATE DraftOrderMaster SET [TotalItem]=[TotalItem]+1,Remarks=@Remarks WHERE DraftAutoId=@DraftAutoId                                                                                                                      
  --     END                                                                                                               
  --     else                                                                                                                                
  --     BEGIN                                                                                
  --            UPDATE DraftItemMaster SET ReqQty=ReqQty+@ReqQty,TotalMLQty=isnull(UnitMLQty,0)* (ReqQty+@ReqQty) WHERE  DraftAutoId= @DraftAutoId AND                                            
  --            ProductAutoId=@ProductAutoId AND UnitAutoId=@UnitAutoId AND IsExchange=@IsExchange AND IsFreeItem=@IsFreeItem                                                                         
  -- END                                                                                                                             
  --    END                                                                                                                                                    
  --    SELECT TOP 1  @DraftAutoId AS DraftAutoId,* FROM #result4231                   
                                                                                              
  --       END    
		-- else
		-- begin
		--	Set @isException=1                                          
		--	Set @exceptionMessage='Stock is not Available'   
		-- end
  --      End                                                                                                                                                   
  --  END   
	ELSE IF @Opcode=4002
	BEGIN
	    SET @Zipcode=(SELECT top 1 Zipcode from ZipMaster Where AutoId=@ZipAutoId)
		select (select CityName from CityMaster where AutoId in (select CityId from ZipMaster where Zipcode=@Zipcode)) as CityName,                                                                                  
		(select StateName from State where AutoId in (select StateId from CityMaster where AutoId in (select CityId from ZipMaster where  
		Zipcode=@Zipcode))) as StateName,   
     
		(select AutoId from CityMaster where AutoId in (select CityId from ZipMaster where Zipcode=@Zipcode)) as CityId,                                                                                  
		(select AutoId from State where AutoId in (select StateId from CityMaster where AutoId in (select CityId from ZipMaster where   Zipcode=@Zipcode)))   
		as StateId,     
                                                                                   
		(select ZoneName from ZoneMaster as zm where zm.AutoId=zip.ZoneId) as ZoneName FROM ZipMaster as zip WHERE   Zipcode=@Zipcode  
   END
   ELSE IF @Opcode=32
	BEGIN
	     
		 BEGIN TRY
			 BEGIN TRAN
						insert into ManagerItemDelete(deletedBy,OrderAutoId,ProductAutoId,UnitTypeAutoId,QtyPerUnit,UnitPrice,RequiredQty,SRP,GP,Tax,
						Barcode,QtyShip,RemainQty,IsExchange,TaxValue,UnitMLQty,isFreeItem,ManualScan,Weight_Oz,Original_UnitType,
						AddOnQty,OM_MinPrice,OM_CostPrice)
						select @EmpAutoId,do.OrderAutoId,do.ProductAutoId,do.UnitTypeAutoId,do.QtyPerUnit,do.UnitPrice,do.RequiredQty,do.SRP,do.GP,do.Tax,do.
						Barcode,do.QtyShip,do.RemainQty,do.IsExchange,do.TaxValue,do.UnitMLQty,do.isFreeItem,do.ManualScan,do.Weight_Oz,do.Original_UnitType,do.
						AddOnQty,do.OM_MinPrice,do.OM_CostPrice from [dbo].OrderItemMaster as do 
						where do.AutoId=@OrderAutoId 
		  
						INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                                                              
						select 2,@EmpAutoId,getdate(),CONVERT(varchar(50),pm.productId)+' has been deleted',OrderAutoId from OrderItemMaster as tb                                                                                                            
						inner join ProductMaster as pm on pm.AutoId=tb.ProductAutoId 
						where tb.AutoId=@OrderAutoId

						insert into [dbo].[ProductStockUpdateLog]([ProductId],[OledStock],[NewStock],[UserAutoId],[DateTime],ReferenceId,ActionRemark,ReferenceType)
						SELECT pm.ProductId,isnull([Stock],0),isnull([Stock],0) + isnull([Pcs],0),@EmpAutoId,GETDATE(),(select OrderAutoid from OrderItemMaster where autoid=@OrderAutoId),
						CONVERT(varchar(50),PM.productId)+' has been deleted for Order No-(' + (select om.OrderNO from OrderMaster as  om inner join OrderItemMaster oiim on om.AutoId=oiim.OrderAutoId where oiim.autoid=@OrderAutoId) + ') by '+(
						Select FirstName+' '+LastName from Employeemaster where autoid=@EmpAutoId)+'.','OrderMaster'
						FROM [dbo].[ProductMaster] AS PM                                                                                                  
						INNER JOIN (SELECT [ProductAutoId],SUM([TotalPieces]) as [Pcs] FROM [dbo].[OrderItemMaster] as oim
						where OIM.[AutoId] = @OrderAutoId and QtyShip>0
						group by [ProductAutoId])
						AS dt ON dt.[ProductAutoId] = PM.[AutoId]

						UPDATE PM SET PM.[Stock] = ISNULL(PM.[Stock],0) + ISNULL([TotalPieces],0) FROM [dbo].[ProductMaster] As PM                                                                          
						INNER JOIN (select [ProductAutoId],SUM(ISNULL(OIM.[TotalPieces],0)) as [TotalPieces] from [dbo].[OrderItemMaster] AS OIM 
						where OIM.AutoId=@OrderAutoId  and QtyShip>0
						group by [ProductAutoId]
						) as t on t.ProductAutoId=pm.AutoId
						
						SET @SalesPersonAutoId=(SELECT SalesPersonAutoId from OrderMaster WHERE AutoId=(
						SELECT OrderAutoId FROM OrderItemMaster Where AutoId=@OrderAutoId))

						 
						SET @ProductAutoId=(SELECT ProductAutoId from OrderItemMaster where AutoId=@OrderAutoId)
						IF EXISTS(Select * from AllocatedPackingDetails where ProductAutoId=@ProductAutoId)
						BEGIN
							 DELETE FROM AllocatedPackingDetails where ProductAutoId=@ProductAutoId
						END
						delete from [dbo].OrderItemMaster where AutoId=@OrderAutoId 
			COMMIT TRAN
		END TRY
		BEGIN CATCH
			ROLLBACK TRAN
			Set @isException=1                                          
			Set @exceptionMessage='Oops! Something went wrong.Please try later.'  
		END CATCH

	 END
	  Else If @Opcode=211                                      
  BEGIN
		Declare @OldStatus varchar(50)=null
		Select @OldStatus=sm.StatusType from OrderMaster as om 
		inner join statusmaster sm on sm.AutoId=om.Status and sm.Category='OrderMaster'
		where om.AutoId=@OrderAutoId  
		IF NOT EXISTS(SELECT AutoId FROM OrderMaster WHERE AutoId=@orderAutoId AND Status IN (1,2,8,6,11))  
		BEGIN 
				IF NOT EXISTS(SELECT AutoId FROM OrderMaster WHERE AutoId=@orderAutoId AND Status IN (9))  
				BEGIN 
					insert into [dbo].[ProductStockUpdateLog]([ProductId],[OledStock],[NewStock],[UserAutoId],[DateTime],ReferenceId,ActionRemark,ReferenceType)
					SELECT pm.ProductId,isnull([Stock],0),(isnull(pm.Stock,0)+(isnull(dt.QtyPerUnit,0) * isnull(dt.QtyShip,0))),@EmpAutoId,GETDATE(),dt.OrderAutoId,
					'Order No-(' + om.OrderNo + ') has set as processed by '+(Select FirstName+' '+LastName from Employeemaster where autoid=@EmpAutoId)+'.','OrderMaster' 
					FROM OrderItemMaster as dt
					inner join productmaster pm on dt.[ProductAutoId] = PM.[AutoId]
					inner join OrderMaster om on dt.OrderAutoId=om.AutoId
					where dt.OrderAutoId=@OrderAutoId and isnull(dt.QtyShip,0)>0 

					UPDATE PM SET [Stock] = isnull([Stock],0) + (isnull(dt.QtyPerUnit,0)*isnull(dt.QtyShip,0)) FROM [dbo].[ProductMaster] AS PM                                                                                                   
					INNER JOIN (SELECT QtyShip,QtyPerUnit,ProductAutoId FROM OrderItemMaster where OrderAutoId=@OrderAutoId
					AND isnull(QtyShip,0)>0) AS dt 
					ON dt.[ProductAutoId] = PM.[AutoId]  
					
					--For deallocate qty---
					SET @SalesPersonAutoId=(select SalesPersonAutoId from OrderMaster where AutoId=@OrderAutoId)
					 
					IF EXISTS(Select * from AllocatedPackingDetails where OrderAutId=@OrderAutoId)
					BEGIN
						 DELETE FROM AllocatedPackingDetails where OrderAutId=@OrderAutoId
					END

				END
		       
				SET @LogRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=25), '@OldStatus', @OldStatus) 
				SET @LogRemark = REPLACE(@LogRemark, '@NewStatus', 'Processed')  
				SET @LogRemark = REPLACE(@LogRemark, '@Emp', (Select FirstName+' '+LastName from Employeemaster where autoid=@EmpAutoId))                                                                                                                                                                                                            
				INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                                                                                      
				VALUES(25,@EmpAutoId,getdate(),@LogRemark,@OrderAutoId)  
                                        
				UPDATE [OrderMaster] SET Status=2,UPDATEDATE=GETDATE(),UPDATEDBY=@EmpAutoId ,
				PackedBoxes=NULL,AddonPackedQty=NULL
				WHERE AutoId=@OrderAutoId 
   
				--For Po Order ---
				SELECT @CustomerAutoId=CustomerAutoId FROM [dbo].[OrderMaster]  WHERE [AutoId] = @OrderAutoId 
				set @CustomerAutoId=(select @CustomerAutoId from  [OrderMaster] WHERE AutoId=@OrderAutoId)              
				if((select CustomerType from CustomerMaster where AutoId=@CustomerAutoId)=3)        
				begin        
					exec [dbo].[ProcUpdatePOStatus_All]        
					@CustomerId=@CustomerAutoId,        
					@OrderAutoId=@OrderAutoId,        
					@Status=2            
				end 
				
		end
		ELSE
		BEGIN
			SET @isException=1                                      
			SET @exceptionMessage='You can changed status on '+@OldStatus+ ' Status.'
		END

		----End-----
		  
  END 

  END TRY                                                                      
  BEGIN CATCH                               
  Set @isException=1                                          
  Set @exceptionMessage=ERROR_MESSAGE()--'Oops! Something went wrong.Please try later.'                                                                                                                                                    
  END CATCH                                                                                       
END 