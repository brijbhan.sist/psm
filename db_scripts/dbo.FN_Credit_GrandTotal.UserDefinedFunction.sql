USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Credit_GrandTotal]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 

 
--alter table CreditMemoMaster drop column GrandTotal
--drop function FN_Credit_GrandTotal
--alter table CreditMemoMaster add  GrandTotal AS [dbo].[FN_Credit_GrandTotal](CreditAutoId)
CREATE FUNCTION  [dbo].[FN_Credit_GrandTotal]
(
	 @AutoId int
)
RETURNS decimal(18,2)
AS
BEGIN
	DECLARE @GrandTotal decimal(18,2)
	SET @GrandTotal=isnull((SELECT SUM(NetAmount) FROM CreditItemMaster WHERE CreditAutoId=@AutoId),0)
	RETURN @GrandTotal
END


GO
