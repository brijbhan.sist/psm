USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcMLTaxMaster]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[ProcMLTaxMaster]    
@OpCode int=Null,    
@TaxAutoId  int=Null,    
@TaxId VARCHAR(12)=NULL,    
@TaxName VARCHAR(50)=NULL,    
@Rate varchar(250)=NULL,    
@Status int=null,    
@State int=null,    
@PrintLabel varchar(100)=null,
@isException bit out,    
@exceptionMessage varchar(max) out    
AS    
BEGIN    
 BEGIN TRY    
   SET @isException=0    
   SET @exceptionMessage='success'    
    
  IF @OpCode=11    
  BEGIN   
  IF exists(SELECT AutoId FROM MLTaxMaster WHERE TaxState = @State)    
     BEGIN    
		set @isException=1    
		set @exceptionMessage='Tax already added for this state.'    
     END 
    ELSE    
     BEGIN TRY    
      BEGIN TRAN           
     INSERT INTO MLTaxMaster(TaxState,TaxRate,Status,[PrintLabel])    
     VALUES (@State,@Rate,@Status,@PrintLabel)           
      COMMIT TRANSACTION    
     END TRY     
     BEGIN CATCH    
      ROLLBACK TRAN    
    SET @isException=1    
    SET @exceptionMessage='Oops! Something went wrong.Please try later'    
     END CATCH    
  END    
  ELSE IF @Opcode=21    
   BEGIN     
    IF exists(SELECT AutoId FROM MLTaxMaster WHERE TaxState = @State and AutoId != @TaxId)    
    BEGIN    
   SET @isException=1    
   SET @exceptionMessage='State already exists.'    
    END    
    ELSE       
    BEGIN    
   UPDATE MLTaxMaster SET TaxRate = @Rate, Status = @Status, TaxState = @State,PrintLabel=@PrintLabel WHERE AutoId = @TaxId    
    END    
	end   
  ELSE IF @Opcode=31    
   BEGIN    
       DELETE FROM MLTaxMaster WHERE AutoId = @TaxId    
   END    
  ELSE IF @OpCode=61    
   BEGIN    
  SELECT CM.AutoId,st.StateName, CM.TaxState, CM.TaxRate, SM.StatusType AS Status,PrintLabel FROM  MLTaxMaster AS CM     
  inner join StatusMaster AS SM ON SM.AutoId=CM.Status and SM.Category is NULL     
  left join State AS st ON st.AutoId=CM.TaxState    
   WHERE (@TaxId is null or @TaxId='' or cm.AutoId like '%'+ @TaxId +'%')  
   and (@Status=2 or cm.status=@Status)
    and (@State=0 or cm.TaxState=@State)
   END     
  ELSE IF @OpCode=42    
    BEGIN    
        SELECT AutoId, TaxRate,Status,TaxState,PrintLabel FROM MLTaxMaster WHERE AutoId=@TaxId    
   END    
  ELSE IF @OpCode=43    
     BEGIN    
         SELECT AutoId, StateName FROM State     
     END   
	 ELSE IF @OpCode=44   
     BEGIN    
         SELECT ID,Value,PrintLabel FROM Tax_Weigth_OZ     
     END   
	  ELSE IF @OpCode=45   
     BEGIN    
        update Tax_Weigth_OZ set Value=@Rate,[PrintLabel]=@PrintLabel where Id=@TaxId  
     END    
 END TRY    
 BEGIN CATCH        
    SET @isException=1    
    SET @exceptionMessage='Oops! Something went wrong.Please try later.'    
 END CATCH    
END    
    
    
    
GO
