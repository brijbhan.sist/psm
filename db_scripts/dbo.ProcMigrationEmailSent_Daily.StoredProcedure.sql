USE [psmnj.easywhm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcMigrationEmailSent_Daily]    Script Date: 8/5/2020 6:49:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
ALTER PROCEDURE [dbo].[ProcMigrationEmailSent_Daily]
	-- migration will be 11:00 PM every day....

	 -- need to send every day at 11:30 PM
	@count int=null,
	@i int = 1,
	@html varchar(max)=null,
	@tr varchar(max)=null,
	@MigrateEmailId varchar(500)=null,
	@customerid varchar(50)=null,
	@customername varchar(250)=null,
	@salesrep varchar(50)=null,
	@paybleamount decimal(18,2)=null,
	@paymentmode varchar(50)=null,
	@orderdate datetime=null,
	@orderno varchar(50)=null,
	@orderclosedate datetime=null,
	@migrationdate datetime=null,
	@PaymentDate datetime=null,
	@settledby varchar(50)=null,
	@FromEmailId varchar(100)=null, 
	@FromName varchar(100)=null, 
	@smtp_userName varchar(100)=null,
	@Password varchar(max)=null, 
	@SMTPServer varchar(50)=null, 
	@Port int=null, 
	@SSL bit=null, 
	@ToEmailId  varchar(100)=null, 
	@CCEmailId  varchar(100)=null, 
	@BCCEmailId  varchar(100)=null, 
@Subject  varchar(max)=null
AS
BEGIN
		select  Row_Number() over (order by cm.CustomerName) as rownumber
				,cm.CustomerId
				,cm.CustomerName
				,(em.FirstName+' '+ isnull(em.LastName,''))[SalesRep]
				,om.PayableAmount
				,pmm.PaymentMode
				,om.OrderDate
				,om.OrderNo
				,om.Order_Closed_Date
				,om.MigrationDate 
				,cpd.PaymentDate 
				,(emac.FirstName +' '+ isnull(emac.LastName,'')) [SettledBy]
	into #t1 from OrderMaster as om
	inner join EmployeeMaster as em on em.AutoId=om.SalesPersonAutoId
	inner join CustomerMaster as cm on cm.AutoId=om.CustomerAutoId
	inner join ShippingType as st on st.AutoId = om.ShippingType
	inner join PaymentOrderDetails as pod on pod.OrderAutoId = om.AutoId
	inner join CustomerPaymentDetails as cpd on cpd.PaymentAutoId = pod.PaymentAutoId
	inner join PAYMENTModeMaster as pmm on pmm.AutoID=cpd.PaymentMode
	inner join EmployeeMaster as emac on emac.AutoId=cpd.EmpAutoId
	where convert(date,om.MigrationDate) = convert(date,getdate())
	set @count = (select count(1) from #t1)
	while(@i<=@count)
		BEGIN
			select 
				@customerid = CustomerId,
				@customername = CustomerName,
				@salesrep = SalesRep,
				@paybleamount = PayableAmount ,
				@paymentmode = PaymentMode,
				@orderdate = OrderDate,
				@orderno = OrderNo,
				@orderclosedate = Order_Closed_Date,
				@migrationdate = MigrationDate,
				@PaymentDate = PaymentDate,
				@settledby = SettledBy
			from #t1 where rownumber = @i

			set @tr = isnull(@tr,'') + '<tr>' 
											+	'<td style=''text-align:center;''>'+ @customerid + '</td>' 
											+	'<td>'+ @customername + '</td>' 
											+	'<td style=''text-align:center;''>'+ @salesrep + '</td>' 
											+	'<td style=''text-align:right;''>'+ convert(varchar(50), @paybleamount) + '</td>' 
											+	'<td style=''text-align:center;''>'+ @paymentmode + '</td>' 
											+	'<td style=''text-align:center;''>'+ format(@orderdate,'MM/dd/yyy hh:mm tt') + '</td>' 
											+	'<td style=''text-align:center;''>'+ @orderno + '</td>' 
											+	'<td style=''text-align:center;''>'+ format(@orderclosedate,'MM/dd/yyy hh:mm tt') + '</td>' 
											+	'<td style=''text-align:center;''>'+ format(@PaymentDate,'MM/dd/yyy hh:mm tt') + '</td>' 
											+	'<td style=''text-align:center;''>'+ format(@migrationdate,'MM/dd/yyy hh:mm tt') + '</td>' 
											+	'<td style=''text-align:center;''>'+ @settledby + '</td>' 
									+ '</tr>' 
			set @i = @i + 1
		END 

		set @html = '<html> <head>	<style> 
										 
										 table {border-collapse: collapse;} 
										 table,td,th {border: 1px solid black;}
									</style>
							</head> 
								<table>  <thead> <tr>	
													<th> Customer ID </th>
													<th> Customer Name </th>
													<th> Sales Rep </th>
													<th> Order Total </th>
													<th> Payment Mode </th>
													<th> Order Date </th>
													<th> Order No </th>
													<th> Order Close Date </th>
													<th> Settled Date </th>
													<th> Order Migration Date </th>
													<th> Settled By</th>
												</tr>' +   
										'</thead> <tbody>' + @tr + '</tbody> </table> </html>'

		

		set @Subject = 'Migration Order Details for ' +UPPER((select CompanyId from dbo.CompanyDetails))+' : ' + format(ISNULL(@migrationdate,GETDATE()),'MM/dd/yyy hh:mm tt');

		SELECT @FromEmailId=EmailId,@Port=port,@SMTPServer=server,@Password=convert(varchar(50), DecryptByPassPhrase('ESS',Pass)),@SSL=ssl from EmailServerMaster 
		where  SendTo='Developer' 

		set @BCCEmailId='naim.uddeen.786@gmail.com,uneel913@gmail.com,brijbhan.sist@gmail.com'; 

		set @MigrateEmailId=(select top 1 [MigrateEmail] from [dbo].[CompanyDetails])

		IF(select count(1) from #t1 )=0
		BEGIN
			SET @html='<b>Today''s, Order is not migrated.'
		END
	  EXEC [Emailing_DB].[dbo].[ProcSubmitEmail] 

	    @Opcode=11,
		@FromEmailId =@FromEmailId,
		@FromName = @FromName,
		@smtp_userName=@FromEmailId,
		@Password = @Password,
		@SMTPServer = @SMTPServer,
		@Port =@Port,
		@SSL =@SSL,
		@ToEmailId =@MigrateEmailId,
		@CCEmailId =@CCEmailId,
		@BCCEmailId =@BCCEmailId,  
		@Subject =@Subject,
		@EmailBody = @html,
		@SentDate ='',
		@Status =0,
		@SourceApp ='PSM',
		@SubUrl ='Status Changed'	,
		@isException=0,
		@exceptionMessage=''  

END
