USE [psmnj.a1whm.com]
GO

/****** Object:  UserDefinedTableType [dbo].[DT_Acc_Normal_Delivered_Order_Items]    Script Date: 12-03-2020 04:44:57 ******/

GO
Drop procedure ProcOrderMaster
Drop type DT_Acc_Normal_Delivered_Order_Items
go
CREATE TYPE [dbo].[DT_Acc_Normal_Delivered_Order_Items] AS TABLE(
	[ProductAutoId] [int] NULL,
	[IsExchange] [int] NULL,
	[UnitAutoId] [int] NULL,
	[QtyPerUnit] [int] NULL,
	[Barcode] [varchar](50) NULL,
	[QtyShip] [int] NULL,
	[QtyDel] [int] NULL,
	[TotalPieces] [int] NULL,
	[UnitPrice] [decimal](18, 2) NULL,
	[SRP] [decimal](18, 2) NULL,
	[GP] [decimal](18, 2) NULL,
	[Tax] [decimal](18, 2) NULL,
	[NetPrice] [decimal](18, 2) NULL,
	[FreshReturnQty] [int] NULL,
	[FreshReturnUnitAutoId] [int] NULL,
	[DamageReturnQty] [int] NULL,
	[DamageReturnUnitAutoId] [int] NULL,
	[isFreeItem] [int] NULL,
	[MLQty] [decimal](18, 2) NULL,
	[MissingItemQty] [int] NULL,
	[MissingItemUnitAutoId] [int] NULL,
	[Del_CostPrice] [decimal](18, 2) NULL,
	[Del_MinPrice] [decimal](18, 2) NULL,
	[Del_BasePrice] [decimal](18, 2) NULL,
	[Del_Discount] [decimal](18, 2) NULL

)
GO


