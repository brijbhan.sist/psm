USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_OrderOverallDisc]    Script Date: 04-24-2021 02:01:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
alter table OrderMaster drop column OverallDisc
drop function FN_OrderOverallDisc
go
Create FUNCTION  [dbo].[FN_OrderOverallDisc]
(
	 @orderAutoId int
)
RETURNS decimal(18,2)
AS
BEGIN
	DECLARE @OverallDisc decimal(18,2)
	set @OverallDisc=isnull((select (case when TotalAmount=0 then 0 else (OverallDiscAmt/TotalAmount) end) 
	from OrderMaster where AutoId=@orderAutoId)	,0.00)*100
	RETURN @OverallDisc
END
go
alter table OrderMaster add [OverallDisc]  AS ([dbo].[FN_OrderOverallDisc]([AutoId]))

