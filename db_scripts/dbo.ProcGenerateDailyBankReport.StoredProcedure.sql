ALTER PROCEDURE [dbo].[ProcGenerateDailyBankReport]               
@FromDate Date =NULL                                            
as                                                
begin                                                  
	                
    SET @FromDate=GETDATE()
	IF NOT EXISTS(select * from [dbo].[Tbl_DailyBankReport] where CONVERT(date,[ReportDate])=CONVERT(date,@FromDate))
	BEGIN
	      BEGIN TRY
			BEGIN TRAN
			;WITH COMPANYINFO(PaymentDate,PaymentMode,ReceivedAmount,OrderAutoId,CustomerAutoid,STime,ChequeNo) AS                                  
			(                                  
			select CONVERT(date,PaymentDate) PaymentDate,
			(select pmm.PaymentMode from PAYMENTModeMaster as pmm where pmm.autoid=pd.PaymentMode) as  PaymentMode ,pod.ReceivedAmount,pod.OrderAutoId,
			pd.CustomerAutoid, format(pd.PaymentDate,'hh:mm tt') as STime ,spm.ChequeNo                                 
			from CustomerPaymentDetails as pd                                   
			inner join PaymentOrderDetails as pod on pd.PaymentAutoid=pod.PaymentAutoid
			LEFT Join SalesPaymentMaster as spm on spm.PaymentAutoId=pd.refPaymentId
			where   pd.Status=0     and          CONVERT(date,PaymentDate) = @FromDate  
  
			)                                  
			SELECT *                                   
			into #ResultGroup42                                   
			FROM COMPANYINFO                                  
			PIVOT (                                  
			SUM(ReceivedAmount) FOR PaymentMode IN ([Cash],[Check],[Money Order],[Credit Card],[Store Credit],[Electronic Transfer],[NO Any Method])                                   
			)  AS P                                                     
                                                
			ORDER BY PaymentDate                                     
            
			DECLARE @ReportAutoId int

		    IF (SELECT COUNT(1) FROM #ResultGroup42)>0
			BEGIN	
				INSERT INTO [dbo].[Tbl_DailyBankReport]([ReportDate],[Createddate],[TotalCustomer],[TotalOrder],[CASH],[Check],[CreditCard],[ElectronicTransfer]
				,[MoneyOrder],[StoreCreditApply],[StoreCreditGenerated],[TotalTransaction],[Short],[Expense])
				select PaymentDate,getdate(),count(distinct CustomerAutoid), COUNT(OrderAutoId),        
				sum(isnull(cash,0)),sum(isnull([Check],0)),sum(isnull([Credit Card],0)),sum(isnull([Electronic Transfer],0)),sum(isnull([Money Order],0)),                                                 
				sum(isnull([Store Credit],0)), (select ISNULL(sum([NewCreditAmount]),0) from CustomerPaymentDetails as tt where tt.Status=0 and PaymentMode!=5 and  CONVERT(date, PaymentDate) =  @FromDate) ,
				sum(isnull(cash,0) + (isnull([Check],0)) + isnull([Money Order],0) + isnull([Credit Card],0) + isnull([Store Credit],0)+isnull([Electronic Transfer],0)
				+(select ISNULL(sum([NewCreditAmount]),0) from CustomerPaymentDetails as tt where tt.Status=0 and PaymentMode!=5 and  CONVERT(date, PaymentDate) =  @FromDate)
				)
				
				as   TotalPaid,                   
				(select isnull(sum(SortAmount),0) from CustomerPaymentDetails where  CONVERT(date,PaymentDate) = @FromDate) as Short,                                  
				isnull((select sum(isnull(ExpenseAmount,0)) as d from ExpenseMaster where  convert(date, ExpenseDate) = convert(date, PaymentDate)),0)
				from #ResultGroup42                                                
				group by PaymentDate                                               
				order by PaymentDate                                                
				
				SET @ReportAutoId=SCOPE_IDENTITY() 
			END
			ELSE
			BEGIN
				INSERT INTO [dbo].[Tbl_DailyBankReport]([ReportDate],[Createddate],[TotalCustomer],[TotalOrder],[CASH],[Check],[CreditCard],[ElectronicTransfer]
				,[MoneyOrder],[StoreCreditApply],[StoreCreditGenerated],[TotalTransaction],[Short],[Expense])
				VALUES(@FromDate,GETDATE(),0,0,0,0,0,0,0,0,0,0,0,0)

				SET @ReportAutoId=SCOPE_IDENTITY() 
			END
			
	 
			INSERT INTO [dbo].[Tbl_DailybankReport_OrderSettlmentLog]([ReportAutoId],[CustomerId],[CustomerName],[OrderNo],[OrderDate],[SettlementTime],[Cash],[Check],[CheckNO]
			,[CreditCard],[ElectronicTransfer],[MoneyOrder],[StoreCredit],[TotalAmount])
			select @ReportAutoId,cm.CustomerId,cm.CustomerName,om.OrderNo,om.OrderDate,STime,sum(ISNULL(R42.Cash,0))as Cash,sum(isnull([Check],0)) as tCheck,ChequeNo,
			sum(ISNULL(R42.[Credit Card],0))as CreditCard,sum(isnull(R42.[Electronic Transfer],0))as ElectronicTransfer, 
			sum(ISNULL(R42.[Money Order],0))as MoneyOrder,sum(isnull(R42.[Store Credit],0))as StoreCredit,
			sum(ISNULL(R42.Cash,0) + isnull([Check],0) + ISNULL(R42.[Money Order],0) + ISNULL(R42.[Credit Card],0)                                        
			+ isnull(R42.[Store Credit],0)+isnull(R42.[Electronic Transfer],0)) as TotalAmount                                               
			from #ResultGroup42 R42                                                
			inner join CustomerMaster cm on cm.AutoId = R42.CustomerAutoid                                                
			inner join OrderMaster om on om.AutoId = R42.OrderAutoId                                            
			group by cm.CustomerId,cm.CustomerName,om.OrderNo ,om.OrderDate,STime,ChequeNo                                                 
			order by Convert(time,STime) desc,om.OrderNo                
                                                
			select CurrencyName,cm.CurrencyValue,sum(NoOfValue) as PNoOfValue,0 ENoOfValue ,SUM(TotalAmount) as PTotalAmount,0 as ETotalAmount                                            
			into #currenycollect                                          
			from Paymentcurrencydetails                                              
			inner join CurrencyMaster cm on cm.autoid = currencyautoid                                               
			where PaymentAutoId in (select PaymentAutoId from CustomerPaymentDetails where CONVERT(date,PaymentDate) = @FromDate)                                              
			group by currencyname,CurrencyValue                                              
			order by CurrencyValue                                              
                                           
			select CurrencyName,cm.CurrencyValue, 0 as PNoOfValue,(sum(ec.TotalCount)) as ENoOfValue,0 as PTotalAmount,(SUM(TotalAmount)) as ETotalAmount                                           
			into #currenyExpense from ExpensiveCurrency   as ec                                           
			inner join CurrencyMaster cm on cm.autoid = currencyautoid                                               
			where ec.ExpenseAutoId in (select emp3.AutoId from ExpenseMaster as emp3 where CONVERT(date,ExpenseDate) = @FromDate)                                              
			group by currencyname,cm.CurrencyValue                                
			order by cm.CurrencyValue                                             
     
	 
			INSERT INTO [dbo].[Tbl_DailyBankReport_CashDetails]
				   ([ReportAutoId]
				   ,[CurrencyName]
				   ,[Noofcount]
				   ,[TotalAmount])
			select @ReportAutoId,CurrencyName,SUM(ISNULL(PNoOfValue,0)-ISNULL(ENoOfValue,0)) as NoOfValue ,SUM(ISNULL(PTotalAmount,0)-ISNULL(ETotalAmount,0)) as TotalAmount                          
			from (                                          
			select * from #currenycollect                                          
			union                                            
			select * from #currenyExpense                                          
			) as t                                     
			group by CurrencyName,CurrencyValue                                     
			having SUM(ISNULL(PNoOfValue,0)-ISNULL(ENoOfValue,0))!=0                                         
			order by CurrencyValue desc , CurrencyName                                
     
			INSERT INTO [dbo].[Tbl_DailyBankReport_ShortAmountDetails]([ReportAutoId],[SettlementId],[SettlementDate],[CustomerId],[CustomerName],[ReceivedBy],[SettlementBy],[PaymentMode],[ReceivedAmount]
			,[ShortAmount])
			select @ReportAutoId,PaymentId,PaymentDate,cm.CustomerId,cm.CustomerName,(ISNULL(em.FirstName,'') + ' ' +ISNULL(em.LastName,'')) ReceivedBy,                          
			(ISNULL(em2.FirstName,'') + ' ' +ISNULL(em2.LastName,'')) as SettlementBy,pmm.PaymentMode,ReceivedAmount,SortAmount from CustomerPaymentDetails cpd                           
			inner join EmployeeMaster em on em.AutoId = cpd.ReceivedBy                                    
			inner join EmployeeMaster em2 on em2.AutoId = cpd.EmpAutoId                             
			inner join CustomerMaster cm on cm.AutoId = cpd.CustomerAutoId                    
			left join PAYMENTModeMaster as pmm on cpd.PaymentMode=pmm.AutoID                                     
			where CONVERT(date,PaymentDate) = @FromDate and SortAmount > 0                                       
                
	
			INSERT INTO [dbo].[Tbl_DailyBankReport_CheckcancelledDetails]([ReportAutoId],[SettlementId],[SettlementDate],[CustomerId],[CustomerName],[CheckNo],[CheckDate],[CheckAmount]
			,[CancelDate],[CancelBy],[Remark],[ReferenceOrderNo])
			select @ReportAutoId,cpd.PaymentId,CPD.PaymentDate,CM.CustomerId,cm.CustomerName,spm.ChequeNo,ChequeDate,      
			spm.ReceivedAmount,spm.CancelDate   
			,(ISNULL(em.FirstName,'') + ' ' + ISNULL(LastName,'')) as CancelBy,      spm.CancelRemarks,
        
			stuff((   select ',' + OM.OrderNo      
			from PaymentOrderDetails POD      
			INNER JOIN OrderMaster as OM on POD.OrderAutoId=OM.AutoId      
			where POD.PaymentAutoId = cpd.PaymentAutoId       
			for xml path('')      
			),1,1,'') as OrderNo      
      
			from SalesPaymentMaster spm                                 
			left join CustomerMaster cm on cm.AutoId = spm.CustomerAutoId                              
			left join EmployeeMaster em on em.AutoId = spm.cancelBy         
			left join CustomerPaymentDetails as cpd on spm.PaymentAutoId=cpd.refPaymentId                             
			where spm.PaymentMode = 2 and CONVERT(date,spm.CancelDate) = Convert(date,@FromDate) and spm.Status=5           
                                     
			INSERT INTO [dbo].[Tbl_DailyBankReport_GenerateCreditDetails]([ReportAutoId],[SettlementId]	,[SettlementDate],[CustomerId],[CustomerName],[PaymentMode],[StoreCredit]
			,[ReceivedBy],[SettlementBy])                           
			Select @ReportAutoId,CPD.PaymentId,PaymentDate,CM.CustomerId,CM.CustomerName,PMM.PaymentMode,ISNULL([NewCreditAmount],0),
			ISNULL(EMp.FirstName,'')+' '+ISNULL(EMp.LastName,'') as ReceivedBY,EM.FirstName+' '+EM.LastName as SettleBy                                
			from CustomerPaymentDetails as CPD                                  
			Inner join PAYMENTModeMaster AS PMM on                                  
			CPD.PaymentMode=PMM.AutoID                         
			INNER Join EmployeeMaster AS EMp on                        
			CPD.ReceivedBy=EMp.AutoId                        
			Inner join CustomerMaster AS CM on                                  
			CPD.CustomerAutoId=CM.AutoId                                  
			INNER JOIN EmployeeMaster AS EM on                                  
			CPD.EmpAutoId=EM.AutoId                          
			where 
			cpd.Status=0 and
			CPD.PaymentMode!=5 and  CONVERT(date, PaymentDate) =  @FromDate and CPD.[NewCreditAmount]!=0                                  
      
			INSERT INTO [dbo].[Tbl_DailyBankReport_ExpenseDetails]([ReportAutoId],[PaymentMode],[ExpenseAmount],[ExpenseBy],[Expensedescription])
			Select @ReportAutoId,ISNUll(pmm.PaymentMode,'') as PaymentMode,ExpenseAmount,ISNULL(EMP.FirstName,'')+' '+ISNULL(EMP.LastName,'') as ExpenseBy,ExpenseDescription from ExpenseMaster em                  
			Inner JOIN PAYMENTModeMaster as pmm on em.PaymentAutoId=pmm.AutoID              
			Inner join EmployeeMaster as EMP on em.CreateBy=EMP.AutoId Where em.ExpenseDate=@FromDate 
			COMMIT TRAN
		END TRY
		BEGIN CATCH
			ROLLBACK TRAN
		END CATCH
			  
	END
end 
