Alter PROCEDURE [dbo].[ProcCategoryMaster]        
@OpCode int=Null,        
@CategoryAutoId  int=Null,        
@CategoryId VARCHAR(12)=NULL,        
@CategoryName VARCHAR(100)=NULL,        
@Description varchar(250)=NULL,        
@Status int=null,        
@SeqNo int=null, 
@IsShow int=null, 
   
@isException bit out,        
@exceptionMessage varchar(max) out        
AS        
BEGIN        
 BEGIN TRY        
  SET @isException=0        
  SET @exceptionMessage='success'        
        
 IF @OpCode=11        
 BEGIN        
  IF exists(SELECT CategoryName FROM CategoryMaster WHERE CategoryName = TRIM(@CategoryName))        
  BEGIN        
   set @isException=1        
   set @exceptionMessage='Category Already Exists!!'        
  END        
  ELSE       
  BEGIN       
   BEGIN TRY        
	   BEGIN TRAN        
			SET @CategoryId=(SELECT dbo.SequenceCodeGenerator('CategoryId'))        
			INSERT INTO [dbo].[CategoryMaster]([CategoryId], [CategoryName],Description,Status,SeqNo,IsShow)        
			VALUES (@CategoryId, TRIM(@CategoryName),@Description,@Status,@SeqNo,@IsShow)        
			IF (DB_NAME()='psmnj.a1whm.com')        
			BEGIN        
			EXEC [ProcCategoryMaster_ALL] @CategoryId=@CategoryId        
			END        
			Update SequenceCodeGeneratorMaster SET currentSequence=currentSequence+1 WHERE SequenceCode='CategoryId'        
	   COMMIT TRANSACTION        
   END TRY         
   BEGIN CATCH        
   ROLLBACK TRAN        
		   SET @isException=1        
		   SET @exceptionMessage=ERROR_MESSAGE()--'Oops! Something went wrong.Please try later.'    
   END CATCH        
  END      
 END        
 ELSE IF @Opcode=21        
 BEGIN        
  IF exists(SELECT CategoryName FROM CategoryMaster WHERE CategoryName = TRIM(@CategoryName) and CategoryId != @CategoryId)        
  BEGIN        
		SET @isException=1        
		SET @exceptionMessage='Category Already Exists!!'        
  END        
  ELSE        
  BEGIN        
   BEGIN TRY        
	   BEGIN TRAN        
			UPDATE CategoryMaster SET CategoryName = TRIM(@CategoryName), Description = @Description, Status = @Status, SeqNo=@SeqNo,IsShow=@IsShow       
			WHERE CategoryID = @CategoryId        
			IF (DB_NAME()='psmnj.a1whm.com')      
			BEGIN        
			    EXEC [ProcCategoryMaster_ALL] @CategoryId=@CategoryId        
			END       
	   COMMIT TRANSACTION        
   END TRY         
   BEGIN CATCH        
   ROLLBACK TRAN        
		   SET @isException=1        
		   SET @exceptionMessage=ERROR_MESSAGE()--'Oops! Something went wrong.Please try later'      
   END CATCH        
  END        
 END        
 ELSE IF @Opcode=31        
 BEGIN        
   BEGIN TRY        
   BEGIN TRAN  
            SET @CategoryAutoId=(SELECT AutoId FROM CategoryMaster WHERE CategoryId=@CategoryId)
			IF NOT EXISTS(SELECT * FROM [SubCategoryMaster] WHERE CategoryAutoId=@CategoryAutoId)    
			BEGIN    
				DELETE FROM [CategoryMaster] WHERE CategoryId=@CategoryId  
			END
			IF (DB_NAME()='psmnj.a1whm.com')        
			BEGIN        
				EXEC [ProcCategoryMaster_ALL_Delete] @CategoryId=@CategoryId         
			END       
   COMMIT TRANSACTION        
   END TRY         
   BEGIN CATCH        
   ROLLBACK TRAN        
		   SET @isException=1        
		   SET @exceptionMessage='Category has been used in another product.'    
   END CATCH        
 END        
 ELSE IF @OpCode=41        
 BEGIN        
	  SELECT CM.CategoryId, CM.CategoryName, CM.Description, SM.StatusType AS Status,IsShow,SeqNo FROM  CategoryMaster AS CM         
	  inner join StatusMaster AS SM ON SM.AutoId=CM.Status and SM.Category is NULL         
	  WHERE (@CategoryId is null or @CategoryId='' or CategoryId like '%'+ @CategoryId +'%')        
	  and (@CategoryName is null or @CategoryName='' or CategoryName like '%'+ @CategoryName +'%')        
	  and (@Status=2 or Status=@Status)          
            
	  SELECT dbo.SequenceCodeGenerator('CategoryId') AS CategoryId        
 END         
 ELSE IF @OpCode=42        
 BEGIN        
		SELECT CategoryId, CategoryName, Description,Status,IsShow,SeqNo FROM CategoryMaster WHERE CategoryId=@CategoryId        
 END        
 END TRY        
 BEGIN CATCH            
	   SET @isException=1        
	   SET @exceptionMessage=ERROR_MESSAGE()--'Oops! Something went wrong.Please try later.'    
 END CATCH        
END        
GO
