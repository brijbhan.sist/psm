USE [psmnj.a1whm.com]
GO

/****** Object:  Table [dbo].[POReceiveMaster]    Script Date: 02/11/2020 16:58:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[POReceiveMaster](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[ReceiveDate] [datetime] NULL,
	[ReceiveBy] [int] NULL,
	[POAutoId] [int] NULL,
	[Status] [int] NULL,
	[PORecieveId] [varchar](20) NULL,
	[Remarks] [varchar](max) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_POReceiveMaster] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


