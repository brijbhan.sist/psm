Alter PROCEDURE [dbo].[Proc_AllImage]    
@ProductId varchar(15)=NULL, 
@defaultImg VARCHAR(max)= NULL
AS          
BEGIN 
 Declare @ProductAutoId int ,@PAutoID int
 SET @PAutoID=(Select AutoId FROM [dbo].ProductMaster Where ProductId=@ProductId) 
 IF NOT EXISTS(SELECT * FROM [psmct.a1whm.com].[dbo].[Tbl_ProductImage] AS TPI
 INNER JOIN [psmct.a1whm.com].[dbo].ProductMaster AS PM ON PM.AutoId=TPI.ProductAutoId WHERE PM.ProductId = @ProductId)          
 BEGIN 
    SET @ProductAutoId=(Select AutoId FROM [psmct.a1whm.com].[dbo].ProductMaster Where ProductId=@ProductId)                
	INSERT INTO [psmct.a1whm.com].[dbo].[Tbl_ProductImage]        
	(ProductAutoId,ImageUrl,Thumbnail_100,Thumbnaill_400 )
	select @ProductAutoId,ImageUrl,Thumbnail_100,Thumbnaill_400    
	FROM [dbo].[Tbl_ProductImage] where ProductAutoId =@PAutoID
	
	UPDATE [psmct.a1whm.com].[dbo].[Tbl_ProductImage] SET isDefault=case when 
	imageurl=@defaultImg then 1 else 0 end where ProductAutoId=@ProductAutoId

	INSERT INTO [psmct.easywhm.com].[dbo].[Tbl_ProductImage]        
	(ProductAutoId,ImageUrl,Thumbnail_100,Thumbnaill_400 )
	select @ProductAutoId,ImageUrl,Thumbnail_100,Thumbnaill_400    
	FROM [dbo].[Tbl_ProductImage] where ProductAutoId =@PAutoID
	
	UPDATE [psmct.easywhm.com].[dbo].[Tbl_ProductImage] SET isDefault=case when 
	imageurl=@defaultImg then 1 else 0 end where ProductAutoId=@ProductAutoId
 END 
 else
 BEGIN 
    SET @ProductAutoId=(Select AutoId FROM [psmct.a1whm.com].[dbo].ProductMaster Where ProductId=@ProductId)
	delete from [psmct.a1whm.com].[dbo].[Tbl_ProductImage] where ProductAutoId=@ProductAutoId
	delete from [psmct.easywhm.com].[dbo].[Tbl_ProductImage] where ProductAutoId=@ProductAutoId

	INSERT INTO [psmct.a1whm.com].[dbo].[Tbl_ProductImage]        
	(ProductAutoId,ImageUrl,Thumbnail_100,Thumbnaill_400 ) 
	select @ProductAutoId,ImageUrl,Thumbnail_100,Thumbnaill_400    
	FROM [dbo].[Tbl_ProductImage] where ProductAutoId =@PAutoID
	
	UPDATE [psmct.a1whm.com].[dbo].[Tbl_ProductImage] SET isDefault=case when
	imageurl=@defaultImg then 1 else 0 end where ProductAutoId=@ProductAutoId     

	INSERT INTO [psmct.easywhm.com].[dbo].[Tbl_ProductImage]        
	(ProductAutoId,ImageUrl,Thumbnail_100,Thumbnaill_400 ) 
	select @ProductAutoId,ImageUrl,Thumbnail_100,Thumbnaill_400    
	FROM [dbo].[Tbl_ProductImage] where ProductAutoId =@PAutoID
	
	UPDATE [psmct.easywhm.com].[dbo].[Tbl_ProductImage] SET isDefault=case when
	imageurl=@defaultImg then 1 else 0 end where ProductAutoId=@ProductAutoId     
 END 
 IF NOT EXISTS(SELECT * FROM [psmpa.a1whm.com].[dbo].[Tbl_ProductImage] AS TPI
 INNER JOIN [psmpa.a1whm.com].[dbo].ProductMaster AS PM ON PM.AutoId=TPI.ProductAutoId WHERE PM.ProductId = @ProductId)          
 BEGIN 
    SET @ProductAutoId=(Select AutoId FROM [psmpa.a1whm.com].[dbo].ProductMaster Where ProductId=@ProductId)         
	INSERT INTO [psmpa.a1whm.com].[dbo].[Tbl_ProductImage]        
	(ProductAutoId,ImageUrl,Thumbnail_100,Thumbnaill_400 ) 
	select @ProductAutoId,ImageUrl,Thumbnail_100,Thumbnaill_400    
	FROM [dbo].[Tbl_ProductImage] where ProductAutoId =@PAutoID
	

	UPDATE [psmpa.a1whm.com].[dbo].[Tbl_ProductImage] SET isDefault=case when
	imageurl=@defaultImg then 1 else 0 end where ProductAutoId=@ProductAutoId   
	
	INSERT INTO [psmpa.easywhm.com].[dbo].[Tbl_ProductImage]        
	(ProductAutoId,ImageUrl,Thumbnail_100,Thumbnaill_400 ) 
	select @ProductAutoId,ImageUrl,Thumbnail_100,Thumbnaill_400    
	FROM [dbo].[Tbl_ProductImage] where ProductAutoId =@PAutoID
	
	UPDATE [psmpa.easywhm.com].[dbo].[Tbl_ProductImage] SET isDefault=case when
	imageurl=@defaultImg then 1 else 0 end where ProductAutoId=@ProductAutoId   
 END  
 else
 BEGIN 
    SET @ProductAutoId=(Select AutoId FROM [psmpa.a1whm.com].[dbo].ProductMaster Where ProductId=@ProductId)
	delete from [psmpa.a1whm.com].[dbo].[Tbl_ProductImage] where ProductAutoId=@ProductAutoId
	delete from [psmpa.easywhm.com].[dbo].[Tbl_ProductImage] where ProductAutoId=@ProductAutoId

	INSERT INTO [psmpa.a1whm.com].[dbo].[Tbl_ProductImage]        
	(ProductAutoId,ImageUrl,Thumbnail_100,Thumbnaill_400 )
	select @ProductAutoId,ImageUrl,Thumbnail_100,Thumbnaill_400    
	FROM [dbo].[Tbl_ProductImage] where ProductAutoId =@PAutoID
	
	UPDATE [psmpa.a1whm.com].[dbo].[Tbl_ProductImage] SET isDefault=case when 
	imageurl=@defaultImg then 1 else 0 end where ProductAutoId=@ProductAutoId  
	
	INSERT INTO [psmpa.easywhm.com].[dbo].[Tbl_ProductImage]        
	(ProductAutoId,ImageUrl,Thumbnail_100,Thumbnaill_400 )
	select @ProductAutoId,ImageUrl,Thumbnail_100,Thumbnaill_400    
	FROM [dbo].[Tbl_ProductImage] where ProductAutoId =@PAutoID
	
	UPDATE [psmpa.easywhm.com].[dbo].[Tbl_ProductImage] SET isDefault=case when 
	imageurl=@defaultImg then 1 else 0 end where ProductAutoId=@ProductAutoId  
 END    
 IF NOT EXISTS(SELECT * FROM [psmnpa.a1whm.com].[dbo].[Tbl_ProductImage] AS TPI
 INNER JOIN [psmnpa.a1whm.com].[dbo].ProductMaster AS PM ON PM.AutoId=TPI.ProductAutoId WHERE PM.ProductId = @ProductId)          
 BEGIN
    SET @ProductAutoId=(Select AutoId FROM [psmnpa.a1whm.com].[dbo].ProductMaster Where ProductId=@ProductId)         
	INSERT INTO [psmnpa.a1whm.com].[dbo].[Tbl_ProductImage]        
	(ProductAutoId,ImageUrl,Thumbnail_100,Thumbnaill_400 )  
	select @ProductAutoId,ImageUrl,Thumbnail_100,Thumbnaill_400    
	FROM [dbo].[Tbl_ProductImage] where ProductAutoId =@PAutoID
	
	UPDATE [psmnpa.a1whm.com].[dbo].[Tbl_ProductImage] SET isDefault=case when
	imageurl=@defaultImg then 1 else 0 end where ProductAutoId=@ProductAutoId   

	INSERT INTO [psmnpa.easywhm.com].[dbo].[Tbl_ProductImage]        
	(ProductAutoId,ImageUrl,Thumbnail_100,Thumbnaill_400 )  
	select @ProductAutoId,ImageUrl,Thumbnail_100,Thumbnaill_400    
	FROM [dbo].[Tbl_ProductImage] where ProductAutoId =@PAutoID
	
	UPDATE [psmnpa.easywhm.com].[dbo].[Tbl_ProductImage] SET isDefault=case when
	imageurl=@defaultImg then 1 else 0 end where ProductAutoId=@ProductAutoId   
 END       
 else
 BEGIN 
    SET @ProductAutoId=(Select AutoId FROM [psmnpa.a1whm.com].[dbo].ProductMaster Where ProductId=@ProductId)
	delete from [psmnpa.a1whm.com].[dbo].[Tbl_ProductImage] where ProductAutoId=@ProductAutoId
	delete from [psmnpa.easywhm.com].[dbo].[Tbl_ProductImage] where ProductAutoId=@ProductAutoId

	INSERT INTO [psmnpa.a1whm.com].[dbo].[Tbl_ProductImage]       
	(ProductAutoId,ImageUrl,Thumbnail_100,Thumbnaill_400 ) 
	select @ProductAutoId,ImageUrl,Thumbnail_100,Thumbnaill_400    
	FROM [dbo].[Tbl_ProductImage] where ProductAutoId =@PAutoID
	
	UPDATE [psmnpa.a1whm.com].[dbo].[Tbl_ProductImage] SET isDefault=case when 
	imageurl=@defaultImg then 1 else 0 end where ProductAutoId=@ProductAutoId    
	
	INSERT INTO [psmnpa.easywhm.com].[dbo].[Tbl_ProductImage]       
	(ProductAutoId,ImageUrl,Thumbnail_100,Thumbnaill_400 ) 
	select @ProductAutoId,ImageUrl,Thumbnail_100,Thumbnaill_400    
	FROM [dbo].[Tbl_ProductImage] where ProductAutoId =@PAutoID
	
	UPDATE [psmnpa.easywhm.com].[dbo].[Tbl_ProductImage] SET isDefault=case when 
	imageurl=@defaultImg then 1 else 0 end where ProductAutoId=@ProductAutoId    
 END  
IF NOT EXISTS(SELECT * FROM [psmwpa.a1whm.com].[dbo].[Tbl_ProductImage] AS TPI
 INNER JOIN [psmwpa.a1whm.com].[dbo].ProductMaster AS PM ON PM.AutoId=TPI.ProductAutoId WHERE PM.ProductId = @ProductId)           
 BEGIN 
    SET @ProductAutoId=(Select AutoId FROM [psmwpa.a1whm.com].[dbo].ProductMaster Where ProductId=@ProductId)             
	INSERT INTO [psmwpa.a1whm.com].[dbo].[Tbl_ProductImage]        
	(ProductAutoId,ImageUrl,Thumbnail_100,Thumbnaill_400 )
	select @ProductAutoId,ImageUrl,Thumbnail_100,Thumbnaill_400    
	FROM [dbo].[Tbl_ProductImage] where ProductAutoId =@PAutoID
	
	UPDATE [psmwpa.a1whm.com].[dbo].[Tbl_ProductImage] SET isDefault=case when
	imageurl=@defaultImg then 1 else 0 end where ProductAutoId=@ProductAutoId  
	
	INSERT INTO [psmwpa.easywhm.com].[dbo].[Tbl_ProductImage]        
	(ProductAutoId,ImageUrl,Thumbnail_100,Thumbnaill_400 )
	select @ProductAutoId,ImageUrl,Thumbnail_100,Thumbnaill_400    
	FROM [dbo].[Tbl_ProductImage] where ProductAutoId =@PAutoID
	
	UPDATE [psmwpa.easywhm.com].[dbo].[Tbl_ProductImage] SET isDefault=case when
	imageurl=@defaultImg then 1 else 0 end where ProductAutoId=@ProductAutoId  
 END        
 else
 BEGIN 
    SET @ProductAutoId=(Select AutoId FROM [psmwpa.a1whm.com].[dbo].ProductMaster Where ProductId=@ProductId)
	delete from [psmwpa.a1whm.com].[dbo].[Tbl_ProductImage] where ProductAutoId=@ProductAutoId
	delete from [psmwpa.easywhm.com].[dbo].[Tbl_ProductImage] where ProductAutoId=@ProductAutoId

	INSERT INTO [psmwpa.a1whm.com].[dbo].[Tbl_ProductImage]       
	(ProductAutoId,ImageUrl,Thumbnail_100,Thumbnaill_400 )
	select @ProductAutoId,ImageUrl,Thumbnail_100,Thumbnaill_400    
	FROM [dbo].[Tbl_ProductImage] where ProductAutoId =@PAutoID
	
	UPDATE [psmwpa.a1whm.com].[dbo].[Tbl_ProductImage] SET isDefault=case when
	imageurl=@defaultImg then 1 else 0 end where ProductAutoId=@ProductAutoId     
	
	INSERT INTO [psmwpa.easywhm.com].[dbo].[Tbl_ProductImage]       
	(ProductAutoId,ImageUrl,Thumbnail_100,Thumbnaill_400 )
	select @ProductAutoId,ImageUrl,Thumbnail_100,Thumbnaill_400    
	FROM [dbo].[Tbl_ProductImage] where ProductAutoId =@PAutoID
	
	UPDATE [psmwpa.easywhm.com].[dbo].[Tbl_ProductImage] SET isDefault=case when
	imageurl=@defaultImg then 1 else 0 end where ProductAutoId=@ProductAutoId     
 END  
 IF NOT EXISTS(SELECT * FROM [psmny.a1whm.com].[dbo].[Tbl_ProductImage] AS TPI
 INNER JOIN [psmny.a1whm.com].[dbo].ProductMaster AS PM ON PM.AutoId=TPI.ProductAutoId WHERE PM.ProductId = @ProductId)         
 BEGIN 
    SET @ProductAutoId=(Select AutoId FROM [psmny.a1whm.com].[dbo].ProductMaster Where ProductId=@ProductId)        
	INSERT INTO [psmny.a1whm.com].[dbo].[Tbl_ProductImage]        
	(ProductAutoId,ImageUrl,Thumbnail_100,Thumbnaill_400 )
	select @ProductAutoId,ImageUrl,Thumbnail_100,Thumbnaill_400    
	FROM [dbo].[Tbl_ProductImage] where ProductAutoId =@PAutoID
	
	UPDATE [psmny.a1whm.com].[dbo].[Tbl_ProductImage] SET isDefault=case when 
	imageurl=@defaultImg then 1 else 0 end where ProductAutoId=@ProductAutoId 
	
	INSERT INTO [psmny.easywhm.com].[dbo].[Tbl_ProductImage]        
	(ProductAutoId,ImageUrl,Thumbnail_100,Thumbnaill_400 )
	select @ProductAutoId,ImageUrl,Thumbnail_100,Thumbnaill_400    
	FROM [dbo].[Tbl_ProductImage] where ProductAutoId =@PAutoID
	
	UPDATE [psmny.easywhm.com].[dbo].[Tbl_ProductImage] SET isDefault=case when 
	imageurl=@defaultImg then 1 else 0 end where ProductAutoId=@ProductAutoId 
 END        
 BEGIN
    SET @ProductAutoId=(Select AutoId FROM [psmny.a1whm.com].[dbo].ProductMaster Where ProductId=@ProductId) 
	delete from [psmny.a1whm.com].[dbo].[Tbl_ProductImage] where ProductAutoId=@ProductAutoId
	delete from [psmny.easywhm.com].[dbo].[Tbl_ProductImage] where ProductAutoId=@ProductAutoId

	INSERT INTO [psmny.a1whm.com].[dbo].[Tbl_ProductImage]       
	(ProductAutoId,ImageUrl,Thumbnail_100,Thumbnaill_400 ) 
	select @ProductAutoId,ImageUrl,Thumbnail_100,Thumbnaill_400    
	FROM [dbo].[Tbl_ProductImage] where ProductAutoId =@PAutoID
	
	UPDATE [psmny.a1whm.com].[dbo].[Tbl_ProductImage] SET isDefault=case when
	imageurl=@defaultImg then 1 else 0 end where ProductAutoId=@ProductAutoId       
	
	INSERT INTO [psmny.easywhm.com].[dbo].[Tbl_ProductImage]       
	(ProductAutoId,ImageUrl,Thumbnail_100,Thumbnaill_400 ) 
	select @ProductAutoId,ImageUrl,Thumbnail_100,Thumbnaill_400    
	FROM [dbo].[Tbl_ProductImage] where ProductAutoId =@PAutoID
	
	UPDATE [psmny.easywhm.com].[dbo].[Tbl_ProductImage] SET isDefault=case when
	imageurl=@defaultImg then 1 else 0 end where ProductAutoId=@ProductAutoId       
 END  
END        