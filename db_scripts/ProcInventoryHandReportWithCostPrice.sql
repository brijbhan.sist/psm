CREATE OR ALTER PROCEDURE [dbo].[ProcInventoryHandReportWithCostPrice]                                                                                                                                                                                  
@OpCode int=Null,                                                                                                                                                                                  
@AutoId  int=Null,                                                                                                                                                                                  
@CategoryAutoId int=Null,                                                                                                                                                                                  
@SubcategoryAutoId  int=Null,                                                                                                                                                                                  
@ProductId VARCHAR(12)=NULL,   
@Barcode VARCHAR(50)=NULL,   
@ProductName VARCHAR(50)=NULL,      
@BrandName VARCHAR(50)=NULL,      
@BrandAutoId int=Null,     
@Status int=NULL,                     
@BarcodeAutoId INT= NULL,                                                                                          
@PageIndex INT = 1,                                                                                                                    
@PageSize INT = 10,                                                                                                  
@isException bit out,   
@RecordCount INT =null,   
@exceptionMessage varchar(max) out                                                                                                                             
AS                                                                                                                                       
BEGIN                                                                                                                                               
 BEGIN TRY                                                                                                                                                         
  SET @isException=0                                                                                                                                                                         
  SET @exceptionMessage='Success'                                                                                                                                                                                  
IF @OpCode=41                                                                                         
 BEGIN   
 select   
 (  
   SELECT [AutoId] as CAID, [CategoryName] as CAN FROM [dbo].[CategoryMaster] WHERE [Status]=1 ORDER BY  CAN ASC    
   for json path  
 ) as Category,  
 (  
   SELECT AutoId as VID,VendorName as VN FROM VENDORMASTER WHERE Status=1     order by  VN asc   
   for json path  
  )as Vendor,  
  (  
   SELECT AutoId as BID,BrandName as BN FROM BrandMaster WHERE Status=1     order by  BN asc    
   for json path  
  )as Brand,  
  (  
   SELECT AutoId as COID,CommissionCode as COC,C_DisplayName as CD from Tbl_CommissionMaster where Status=1   order by COC asc  
   for json path  
  )as Commission  
  for json path  
 END                                                   
 ELSE IF @Opcode=42                                                                                                                                                                                  
 BEGIN                                                                           
	 SELECT [AutoId] as SCAID, trim([SubcategoryName]) as SCAN FROM [dbo].[SubCategoryMaster] WHERE [Status]=1 
	 AND [CategoryAutoId]=@CategoryAutoId  ORDER BY  (SCAN) ASC    
	 for json path  
 END                                                                                                                                                 
 ELSE IF @Opcode=44                                                                                                                                                                                  
 BEGIN      
	  SELECT  ROW_NUMBER() OVER(ORDER BY [ProductId] asc) AS RowNumber,[ProductId],[ProductName],CM.CategoryName AS Category,SCM.SubcategoryName      
	  AS Subcategory, VM.VendorName AS Vendor,                                            
	  ISNULL(convert(varchar,(Stock/(case (select Qty from PackingDetails as temp1                                                                                                                                                  
	  where temp1.productAutoId = pm.AutoId and temp1.UnitType =pm.PackingAutoId) 
	  when 0 then 1 else (select Qty from PackingDetails as temp1                                              
	  where temp1.productAutoId = pm.AutoId and temp1.UnitType =pm.PackingAutoId) end)))                                                                                
	                                                                                                          
	  ,0) as [Stock] ,ReOrderMark,ImageUrl,(case when pm.ProductStatus=1 then 'Active' else 'Inactive' end) as Status,      
	  bm.BrandName as  BrandName ,cast(isnull(pm.P_CommCode,0) as decimal(10,4))  as  CommCode,
	  isnull(Convert(decimal(18,2),(SELECT CostPrice From PackingDetails where ProductAutoId=PM.AutoId AND UnitType=PM.PackingAutoId),0),0.00) as CostPrice,
	  um.UnitType,ThumbnailImageUrl into #temp44                                                                                                                                    
	  FROM ProductMaster AS PM                                                                                                                                    
	  INNER JOIN [dbo].[CategoryMaster] AS CM ON PM.CategoryAutoId = CM.AutoId                                                                                                                                            
	  INNER JOIN [dbo].[SubCategoryMaster] AS SCM ON PM.SubcategoryAutoId = SCM.AutoId  
	  LEFT JOIN [dbo].[VendorMaster] AS VM ON VM.AutoId = PM.VendorAutoId  
	  LEFT join dbo.UnitMaster as um on PM.PackingAutoId=um.AutoId  
	  LEFT join BrandMaster as bm on bm.AutoId=pm.Brandautoid   
      
	  WHERE 
	  pm.Stock>0 and
	  (@CategoryAutoId IS NULL OR @CategoryAutoId='' OR PM.[CategoryAutoId] = @CategoryAutoId)                                                            
	  and   (@Status IS NULL OR @Status=2 OR PM.ProductStatus = @Status)                                                                                                                                                                                 
	  AND   (@SubcategoryAutoId IS NULL OR @SubcategoryAutoId='' OR PM.[SubcategoryAutoId] = @SubcategoryAutoId)    
	  AND    (@BrandAutoId IS NULL OR @BrandAutoId='' OR bm.AutoId = @BrandAutoId)    
	  AND   (@ProductId IS NULL OR @ProductId='' OR PM.[ProductId] LIKE '%'+ @ProductId +'%')                                                                     
	  AND   (@ProductName IS NULL OR @ProductName='' OR PM.ProductName LIKE '%' + @ProductName + '%')                                                              
	  and  
	  (pm.AutoId in (select ProductAutoId from [dbo].[BillItems])    
  
	  OR (PM.ProductId IN (SELECT lg.ProductId FROM ProductStockUpdateLog as lg WHERE ActionRemark='Manual'))  
  
	  )  
	  and (ISNULL(@Barcode,'')='' or pm.AutoId in (select ProductAutoId from ItemBarcode where Barcode=@Barcode))  
	  order by pm.ProductId,ProductName  
  
	  SELECT  COUNT(pm.[ProductId])                                                                                                                                                                                  
	  AS RecordCount, case when @PageSize=0 then COUNT(distinct pm.[ProductId]) else @PageSize end AS PageSize,
	  @PageIndex AS PageIndex FROM #temp44 as PM                          
                                                                                  
	  SELECT *   from #temp44 as PM                                                                     
	  WHERE @PageSize=0 or  RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1        
	  ORDER BY [ProductId] asc   
	  
	  select ISNULL(Sum([Stock]*Convert(decimal(18,2),CostPrice)),0) as OverAllTotal,FORMAT(getdate(),'MM/dd/yyyy hh:mm tt') as PrintDate from #temp44
 END                                                                                                                                                                
 END TRY                                                      
 BEGIN CATCH                                                                                                                 
    SET @isException=1                                                                                                                                                  
    SET @exceptionMessage='Oops, Something went wrong .Please try again.'                                                                                               
 END CATCH                                                                                                                                                                           
END   