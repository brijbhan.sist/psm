
/****** Object:  UserDefinedFunction [dbo].[FN_StoreCredit]    Script Date: 04-24-2021 05:24:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Alter table OrderMaster drop column CreditAmount
Drop function FN_StoreCredit
go
Create FUNCTION  [dbo].[FN_StoreCredit]
(
	 @OrderAutoId int 
)
RETURNS decimal(18,2)
AS
BEGIN
	DECLARE @StoreCredit decimal(18,2)		 	 
	SET @StoreCredit=cast((SELECT SUM(ISNULL(Amount,0.00)) FROM tbl_Custumor_StoreCreditLog WHERE ReferenceType='OrderMaster' and
	ReferenceNo=CONVERT(varchar(25), @OrderAutoId)) as decimal	(18,2))	 
	RETURN @StoreCredit
END
go
Alter table OrderMaster add [CreditAmount]  AS ([dbo].[FN_StoreCredit]([Autoid]))
