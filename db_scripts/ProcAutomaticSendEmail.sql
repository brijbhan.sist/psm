
ALTER Proc [dbo].[ProcAutomaticSendEmail]
(
 @FromEmailId varchar(100)=null, 
 @FromName varchar(100)=null, 
 @smtp_userName varchar(100)=null,
 @Password varchar(max)=null, 
 @SMTPServer varchar(50)=null, 
 @Port int=null, 
 @SSL bit=null, 
 @ToEmailId  varchar(100)=null, 
 @CCEmailId  varchar(100)=null, 
 @BCCEmailId  varchar(100)=null, 
 @Subject  varchar(max)=null, 
 @EmailBody  varchar(max)=null, 
 @Status int=null, 
 @SentDate datetime=null, 
 @SourceApp  varchar(20)=null, 
 @SubUrl  varchar(100)=null, 
 @ErrorDescription varchar(max)=null,
 @AutoId int = null,
 @Opcode int = null,
 @PageIndex INT=1,
 @PageSize INT=10,
 @RecordCount INT=null,
 @isException BIT OUT,
 @exceptionMessage VARCHAR(max) OUT,
 @OrderAutoId int =null,
 @CustomerAutoId int=null,
 @CustomerName varchar(300)=null,
 @PackerAutoId int=null,
 @DriverName varchar(300)=null,
 @DriverAutoId int=null,
 @OrderNo varchar(15)=null,
 @PackerName varchar(300)=null
)
as begin 
	SET @isException=0
	SET @exceptionMessage='Success!!'
  SELECT @FromEmailId=EmailId,@Port=port,@SMTPServer=server,@Password=convert(varchar(50), DecryptByPassPhrase('ESS',Pass)),@SSL=ssl from EmailServerMaster 
  where  SendTo='Developer' 
  if @Opcode = 11
  begin
	  set @FromName=DB_NAME()
	  set @CustomerName=(select CustomerName from CustomerMaster where AutoId=@CustomerAutoId)
	  set @PackerAutoId=ISNULL((select packerAutoiD from OrderMaster where AutoId=@OrderAutoId),'')
	  set @PackerName=ISNULL((select FirstName+' '+ISNULL(LastName,'')  from EmployeeMaster where AutoId=@PackerAutoId),'')
	  set @OrderNo=ISNULL((select OrderNo from OrderMaster where AutoId=@OrderAutoId),'')
	  set @Subject='Order No : '+@OrderNo+ ' ' +@CustomerName +' Packed By : ' +@PackerName+ ' at '+(select FORMAT(PackerAssignDate,'hh:mm:tt') from OrderMaster where AutoId=@OrderAutoId)+' '+(select FORMAT(PackerAssignDate,'MM:dd:yyyy') from OrderMaster where AutoId=@OrderAutoId)
	  set @EmailBody='<span>Customer Name : '+@CustomerName+'<span><br/><span>Order No : '+@OrderNo+'<span><br/><span>Sales Person Remark:'+ISNULL((select OrderRemarks from OrderMaster where AutoId=@OrderAutoId),'N/A')+'<span><br/><span>Warehouse Manager Remarks :  '+ISNULL((select WarehouseRemarks from OrderMaster where AutoId=@OrderAutoId),'N/A')+'<span><br/><span><br/>'+@PackerName
    
  end
  if @Opcode = 12
  begin
	  set @FromName=DB_NAME()
	  set @CustomerName=(select CustomerName from CustomerMaster where AutoId=@CustomerAutoId)
	  set @DriverAutoId=(select Driver from OrderMaster where AutoId=@OrderAutoId)
	  set @DriverName=(select FirstName+' '+ISNULL(LastName,'')   from EmployeeMaster where AutoId=@DriverAutoId)
	  set @OrderNo=(select OrderNo from OrderMaster where AutoId=@OrderAutoId)
	  set @Subject='Order No : '+@OrderNo+ ' ' +@CustomerName +' Delivered By : ' +@DriverName+ ' at '+(select FORMAT(PackerAssignDate,'hh:mm:tt') from OrderMaster where AutoId=@OrderAutoId)+' '+(select FORMAT(PackerAssignDate,'MM:dd:yyyy') from OrderMaster where AutoId=@OrderAutoId)
	  set @EmailBody='<span>Customer Name : '+@CustomerName+'<span><br/><span>Order No : '+@OrderNo+'<span><br/><span>Driver Remark:'+ISNULL((select DriverRemarks from OrderMaster where AutoId=@OrderAutoId),'N/A')+'<span><br/><span>Payment Status :  '+(select case when PaymentRecev='yes' then 'Paid' else 'Not Paid' end PaymentStatus from OrderMaster where AutoId=@OrderAutoId)+'<span><br/><span>Deliver Time : '+(select FORMAT(DeliveryDate,'MM:dd:yyyy') from OrderMaster where AutoId=@OrderAutoId)+'<span><br/>'+@DriverName
  end
  if @Opcode = 13
  begin
	  set @FromName=DB_NAME()
	  set @CustomerName=(select CustomerName from CustomerMaster where AutoId=@CustomerAutoId)
	  set @DriverAutoId=(select Driver from OrderMaster where AutoId=@OrderAutoId)
	  set @DriverName=(select FirstName+' '+ISNULL(LastName,'')  from EmployeeMaster where AutoId=@DriverAutoId)
	  set @OrderNo=(select OrderNo from OrderMaster where AutoId=@OrderAutoId)
	  set @Subject='Order No : '+@OrderNo+ ' ' +@CustomerName +' Assign To : ' +@DriverName
	  set @EmailBody='<span>Customer Name : '+@CustomerName+'<span><br/><span>Order No : '+@OrderNo+'<span><br/><span>Sales Person Remark:'+ISNULL((select OrderRemarks from OrderMaster where AutoId=@OrderAutoId),'N/A')+'<span><br/><span>Sales Person Remark for Driver:'+(select OrderRemarks from OrderMaster where AutoId=@OrderAutoId)+'<span><br/><span>Warehouse Manager Remarks :  '+ISNULL((select WarehouseRemarks from OrderMaster where AutoId=@OrderAutoId),'')+'<span><br/><span>Payment Status :  '+(select case when PaymentRecev='yes' then 'Paid' else 'Not Paid' end PaymentStatus from OrderMaster where AutoId=@OrderAutoId)+'<span><br/><span>Deliver Time : '+(select FORMAT(DeliveryDate,'MM:dd:yyyy') from OrderMaster where AutoId=@OrderAutoId)+'<span><br/>'+@DriverName
   end
   if @Opcode = 14
  begin
	  set @FromName=REPLACE(DB_NAME(),'.a1whm.com','')
	  set @CustomerName=(select CustomerName from CustomerMaster where AutoId=@CustomerAutoId)
	  set @DriverAutoId=(select SalesPersonAutoId from OrderMaster where AutoId=@OrderAutoId)
	  set @DriverName=(select FirstName+' '+ISNULL(LastName,'')  from EmployeeMaster where AutoId=@DriverAutoId)
	  set @OrderNo=(select OrderNo from OrderMaster where AutoId=@OrderAutoId)
	  set @Subject=@FromName+' - '+'Order No : '+@OrderNo+ ' migrate successfully.' 
	  set @EmailBody='<span>Customer Name : '+@CustomerName+'<span><br/><span>Order No : '+@OrderNo+'<span><br/><span>Sales Person :'+@DriverName;
   end
   EXEC [Emailing_DB].[dbo].[ProcSubmitEmail] 

	    @Opcode=11,
		@FromEmailId =@FromEmailId,
		@FromName = @FromName,
		@smtp_userName=@FromEmailId,
		@Password = @Password,
		@SMTPServer = @SMTPServer,
		@Port =@Port,
		@SSL =@SSL,
		@ToEmailId ='uneel913@gmail.com',
		@CCEmailId ='',
		@BCCEmailId ='naim.uddeen.786@gmail.com',  
		@Subject =@Subject,
		@EmailBody = @EmailBody,
		@SentDate ='',
		@Status =0,
		@SourceApp ='PSM',
		@SubUrl ='Status Changed'	,
		@isException=0,
		@exceptionMessage=''  
end 