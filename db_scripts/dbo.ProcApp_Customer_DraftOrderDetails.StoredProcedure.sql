ALTER PROCEDURE [dbo].[ProcApp_Customer_DraftOrderDetails]      
@Opcode int=NULL,      
@CustomerAutoId int=null,     
@OrderAutoId int=null,     
@DraftAutoId int =null,     
@Remarks VARCHAR(500)=NULL,  
@isException bit out,      
@exceptionMessage varchar(max) out      
AS      
BEGIN      
BEGIN TRY      
	SET @exceptionMessage= 'Success'      
	SET @isException=0       
       
		IF @Opcode=41       
		BEGIN 
			select DraftNo as draftNo,AutoId as draftAId,CustomerAutoId as cusAId,  
			dbo.[ConvertUTCTimeStamp](DraftDate) as delDate,  
			DefaultBillAutoId as billAID,  
			billAddr,DefaultShipAutoId as shipAID,shipAddr,  
			ISNULL((  
			select pm.AutoId as pAID,ProductId as productId,productName,um.AutoId as uAID,um.UnitType as unitName,  
			Quantity as qty,Price as price,NetPrice from App_DraftCartItemMasterCust as dcm   
			inner join ProductMaster as pm on pm.AutoId=dcm.ProductAutoId  
			inner join UnitMaster as um on um.AutoId=dcm.UnitType  
			where dcm.CartAutoId=do.AutoId  
			for json path  , INCLUDE_NULL_VALUES  
  
			),'[]')  
			as ProductDetails,  
			SubTotal  
			from App_DraftCartMasterCust as do  
			where AutoId=@DraftAutoId  
			for json path , INCLUDE_NULL_VALUES  
		END      
  
		Else If @Opcode=11                                                                                                                         
		BEGIN                                                                 
			BEGIN TRY                                                                                                                                                      
			BEGIN TRAN  
					if exists(select AutoId from App_DraftCartMasterCust where AutoId=@DraftAutoId)  
					Begin  
					DECLARE @OrderNo VARCHAR(25)= (SELECT DBO.SequenceCodeGenerator('OrderNumber'))                                                                         
					DECLARE @BillAddrAutoId INT=(SELECT DefaultBillAutoId FROM App_DraftCartMasterCust WHERE AutoId=@DraftAutoId)       
  
					declare @State INT=(SELECT TOP 1 State FROM BillingAddress WHERE AutoId =@BillAddrAutoId)                                                                                                                                                         
					DECLARE @Terms INT=(SELECT Terms  FROM CustomerMaster WHERE AutoId=@CustomerAutoId)                                                                                                                    
					DECLARE @IsTaxApply INT=0                                                                                                                      
                                                                
					INSERT INTO [dbo].[OrderMaster] ([OrderNo],[OrderDate],[DeliveryDate],[CustomerAutoId],[Terms],[BillAddrAutoId],[ShipAddrAutoId],                                                                                                                        
					[SalesPersonAutoId],OverallDiscAmt,[ShippingCharges],[Status],[ShippingType],                                                                                                                                       
					TaxType,OrderRemarks,OrderType,TaxValue,mlTaxPer,IsTaxApply,Weigth_OZTax)                   
					SELECT @OrderNo,getdate(),getdate(),CustomerAutoId,@Terms,DefaultBillAutoId,DefaultShipAutoId,                                                                                                                    
					(SELECT [SalesPersonAutoId] FROM [dbo].[CustomerMaster] WHERE [AutoId] = @CustomerAutoId),                                  
					0,0,1,1,0,@Remarks,3,0,ISNULL((SELECT TaxRate FROM MLTaxMaster where TaxState=@STATE),0.00),0,0  
					FROM App_DraftCartMasterCust  WHERE AutoId=@DraftAutoId   
					SET @OrderAutoId =SCOPE_IDENTITY()  
					INSERT INTO [dbo].[OrderItemMaster]([OrderAutoId],[ProductAutoId],[UnitTypeAutoId],[QtyPerUnit],[UnitPrice],[RequiredQty],  
					[SRP],[GP],[Tax],IsExchange,TaxValue,UnitMLQty,isFreeItem,Weight_Oz)                                                                            
					SELECT @OrderAutoId,tb.ProductAutoId,tb.UnitType,pd.[Qty],tb.Price,tb.Quantity ,pm.[P_SRP],                                                            
					CONVERT(DECIMAL(10,2),((PM.[P_SRP]-(tb.Price/(CASE WHEN Qty=0 THEN 1 ELSE [Qty] END)))/(CASE WHEN PM.[P_SRP]=0 THEN 1 ELSE PM.[P_SRP] END)) * 100)     
					,0,0,0,0, 0,0                                
					FROM App_DraftCartItemMasterCust AS tb                                       
					inner join ProductMaster as pm on pm.AutoId=tb.ProductAutoId  
					inner join PackingDetails as pd on pd.ProductAutoId=tb.ProductAutoId and tb.UnitType=pd.UnitType WHERE DraftAutoId=@DraftAutoId                                                                                                
               
                                                                           
                                                                                                                                                    
					DELETE FROM App_DraftCartMasterCust WHERE AutoId=@DraftAutoId                                                                                                       
					DELETE FROM App_DraftCartItemMasterCust WHERE DraftAutoId=@DraftAutoId                                                                                                                                                    
                                                                        
					update CustomerMaster set LastOrderDate = GETDATE() where AutoId =@CustomerAutoId                                                                                              
                                                                                                                        
					select @OrderAutoId as OrderAutoId,@OrderNo as OrderNo       
  
					UPDATE SequenceCodeGeneratorMaster SET currentSequence = currentSequence + 1 WHERE SequenceCode='OrderNumber'    
					end  
					else  
					Begin  
					Set @isException=1                                                                                                     
					Set @exceptionMessage='Draft Auto is not exists'  
					end    
  
			COMMIT TRAN  
			END TRY  
			BEGIN CATCH  
				ROLLBACK TRAN  
				Set @isException=1                                                                                                     
				Set @exceptionMessage='Server error'               
			END CATCH  
		END  
   
		Else If @Opcode=42  
		Begin  
			select AutoId as DraftAutoId,DraftNo,CustomerAutoId,(dbo.ConvertUTCTimeStamp(DraftDate)) as DraftDate,SubTotal from App_DraftCartMasterCust   
			where CustomerAutoId=@CustomerAutoId  
			for json path  , INCLUDE_NULL_VALUES  
		End  
  
		Else If @Opcode=43  
		Begin  
			select om.AutoId as OrderAutoId,OrderNo,dbo.ConvertUTCTimeStamp(OrderDate) as OrderDate,om.Status,sm.StatusType,  
			count(1) as NumberOfItems,Cast(GrandTotal as decimal(10,2)) as OrderAmount   
			from OrderMaster as om  
			inner join StatusMaster as sm on sm.AutoId=om.Status and  Category='OrderMaster'  
			inner join OrderItemMaster as oim on oim.OrderAutoId=om.AutoId  
			where CustomerAutoId=@CustomerAutoId  
			group by om.AutoId,OrderNo,OrderDate,om.Status,sm.StatusType,GrandTotal  
			for json path  , INCLUDE_NULL_VALUES  
		End  
  
		Else if @Opcode=44  
		Begin  
			select om.AutoId as OrderAutoId,OrderNo,dbo.ConvertUTCTimeStamp(OrderDate) as OrderDate,om.Status,sm.StatusType,  
			CustomerAutoId,CustomerId,CustomerName,[dbo].[FN_getBillingAddress](BillAddrAutoId) as BillingAddress,[dbo].[FN_getShippingAddress](ShipAddrAutoId) as ShippingAddress,dbo.ConvertUTCTimeStamp(DeliveryDate) as DeliveryDate,st.ShippingType,ttm.TaxableType, 
 
			PayableAmount as SubTotalAmt,OverallDiscAmt,ShippingCharges,TotalTax,MLQty,MLTaxPer,Weigth_OZTaxAmount,AdjustmentAmt,GrandTotal,paidAmount,PayableAmount,(Isnull(PayableAmount,0)-Isnull(paidAmount,0)) as BalanceAmt,  
			ISNULL((  
			select  pm.AutoId as ProductAutoId,Pm.ProductId,Pm.ProductName,'http://psmnj.a1whm.com'+pm.ImageUrl as ImageUrl,  Um.UnitType,um.AutoId as UnitAutoId,UnitPrice,QtyPerUnit as Qty  
			from OrderItemMaster as oim  
			inner join ProductMaster as pm on pm.AutoId=oim.ProductAutoId   
			left join UnitMaster as um on um.AutoId=oim.unittypeAutoId  
			where OrderAutoId=om.AutoId  
			for json path  , INCLUDE_NULL_VALUES  
			),'[]')  
			as OrderItemDetails   
			from OrderMaster as om  
			inner join StatusMaster as sm on sm.AutoId=om.Status and  Category='OrderMaster'  
			left join CustomerMaster as cm on cm.AutoId=om.CustomerAutoId  
			left join ShippingType as st on st.AutoId=om.ShippingType  
			left join TaxTypeMaster as ttm on ttm.AutoId=om.TaxType  
			where om.AutoId=@OrderAutoId  
			for json path  , INCLUDE_NULL_VALUES  
		End  
		ELSE IF @Opcode=31
		BEGIN
			DELETE FROM App_DraftCartItemMasterCust WHERE DraftAutoId=@DraftAutoId
			DELETE FROM App_DraftCartMasterCust WHERE AutoId=@DraftAutoId
		END
  
END TRY      
BEGIN CATCH      
	SET @isException=1      
	SET @exceptionMessage= ERROR_MESSAGE()      
	END CATCH      
END