--insert into TBL_MST_SecurityMaster(SecurityType,SecurityValue,typeEvent,description)
--values(6,12345,8,'Account Check Security For Payment Edit')
ALTER Proc [dbo].[CustomerMaster_PaymentDetails]
@OpCode int =null,
@CustomerAutoId int=null,
@PaymentId VARCHAR(10)=NULL,
@Status INT=NULL,
@FromDate DateTime =null,                                                                                                  
@ToDate DateTime =null,
@OrderNo varchar(40) =null,
@PaymentAutoId INT=NULL,
@EmpAutoId INT=NULL,
@BillAddAutoId INT=NULL, 
@StoreCredit DECIMAL(10,2)=NULL,                                                                                                  
@CreditAmount decimal(18,2)=NULL,                                                            
@ReceivedAmount decimal(18,2)=NULL,                                                                         
@PaymentMethod INT=NULL,                                                                       
@ReferenceNo varchar(20)=NULL,                                                                       
@PaymentRemarks varchar(500)=NULL,  
@ReceivedPaymentBy INT=NULL,                                                                          
@EmployeeRemarks varchar(500)=NULL,                                                                 
@TableValue  CustomerPayment readonly,                                                                                                                                           
@PaymentCurrencyXml xml=null,                                                                                    
@SortAmount decimal(18,2)=null,    
@CancelRemark VARCHAR(250)=NULL,                                                                           
@TotalCurrencyAmount decimal(10,2)=null, 
@SecurityKey varchar(20)=NULL,    
@CheckNo varchar(500)=NULL,                                                                                     
@CheckDate DATE=NULL,                    
@PageIndex INT=1,                                                                                                  
@PageSize INT=10, 
@isException BIT OUT,                                
@exceptionMessage VARCHAR(max) OUT
With recompile
As
Begin
BEGIN TRY                                                                               
	SET @isException=0                                                                                                  
	SET @exceptionMessage='Success!!'
		IF @Opcode = 41                                    
		BEGIN                                                                                                  
			SELECT ROW_NUMBER() OVER(ORDER BY PaymentId desc) AS RowNumber, * INTO #Results from                        
			(                                         
			SELECT PaymentAutoId,PaymentId,format(PaymentDate,'MM/dd/yyyy hh:mm tt')  as PaymentDate, 
			ISNULL(pmm.PaymentMode,'') as PaymentMethod,                                                                                       
			( SELECT COUNT(*) FROM PaymentOrderDetails AS TEMP WHERE  TEMP.PaymentAutoId=CPD.PaymentAutoId) AS Totalordes,                                                           
			(isnull(ReceivedAmount,0) - isnull(SortAmount,0)) as ReceivedAmount ,(EMP.FIRSTNAME+' ' +EMP.LASTNAME) AS EmpName   
			,case when isnull(CPD.Status,0)=0 then 'Settled' else 'Cancelled' end as Status,CPD.CreditAmount                                
			FROM CustomerPaymentDetails AS CPD                                                                                                  
			INNER JOIN EmployeeMaster AS EMP ON EMP.AutoId=CPD.ReceivedBy                      
			left join PAYMENTModeMaster as pmm on CPD.PaymentMode=pmm.AutoID       
			where CustomerAutoId =@CustomerAutoId                                                                                                  
			and (@PaymentId is null or @PaymentId='' or PaymentId like '%'+@PaymentId+'%')                                                                                                  
			and (@Status is null or @Status='-1' or isnull(CPD.Status,0)=@Status)                                                                                                  
			and (@FromDate is null or @FromDate = '' or @Todate is null or @Todate = '' or ( CONVERT(DATE,PaymentDate) between CONVERT(DATE,@FromDate) and CONVERT(DATE,@Todate)))                           
			)as t order  by PaymentAutoId desc                                    
                   
			SELECT case when @PageSize=0 then 0 else COUNT(RowNumber) end AS RecordCount, @PageSize AS PageSize, @PageIndex AS PageIndex FROM #Results                                                                                         
                                                                                                
			SELECT * FROM #Results                                                                                                  
			WHERE @PageSize=0 or RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1                                                                                                 
		END 

		ELSE IF @Opcode = 42                                                                                                  
		BEGIN                                                                                                
			SELECT om.AutoId as OrderAutoId,OrderNo,OrderDate,DO.PayableAmount,DO.AmtPaid,DO.AmtDue,                                                                                                  
			otm.OrderType as OrderType  ,      
			datediff(dd,OrderDate,getdate())  as DaysOld      
			into #result46  FROM OrderMaster AS OM INNER JOIN DeliveredOrders AS DO ON OM.AutoId=DO.OrderAutoId  
			   INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
			where CustomerAutoId =@CustomerAutoId and DO.AmtDue >0                                                                                                
			and Status=11                                                                                                
			select OrderAutoId,OrderNo,CONVERT(VARCHAR(10),OrderDate,101) AS OrderDate,PayableAmount,AmtPaid,AmtDue,AmtPaid,OrderType,DaysOld 
			from #result46                                                                                                   
			where (@OrderNo is null or @OrderNo='' or OrderNo like '%'+@OrderNo+'%') and 
			(@FromDate is null or @FromDate = '' or @Todate is null or 
			@Todate = '' or (convert(date,[OrderDate]) between convert(date,@FromDate) and convert(date,@Todate))) order by CONVERT(datetime,OrderDate,101) desc
			select SUM(AmtDue) as TotalDueAmount from #result46                                                                                         
			select Autoid,Currencyname,CurrencyValue from Currencymaster where Status=1  order by   CurrencyValue desc                                                                                           
		END

		ELSE IF @Opcode = 43                                                                                                  
		BEGIN                                                                                                  
			SET @CustomerAutoId=(SELECT CustomerAutoId from CustomerPaymentDetails where PaymentAutoId=@PaymentAutoId)                    
                          
			SELECT CPD.PaymentAutoId,PaymentId,FORMAT(PaymentDate,'MM/dd/yyyy') as PaymentDate,                                                                                               
			CPD.paymentMode AS  PaymentMethod,ReferenceNo,                                                                         
			(SELECT COUNT(*) FROM PaymentOrderDetails AS TEMP WHERE  TEMP.PaymentAutoId=CPD.PaymentAutoId) AS Totalordes,                                                                                                  
			CPD.ReceivedAmount,(EMP.FIRSTNAME+' ' +EMP.LASTNAME) AS EmpName ,ReceivedBy AS ProcessedBy ,                    
			EmployeeRemarks,PaymentRemarks,                                                                                                  
			isnull(CreditAmount,0.0) as CreditAmount,SPM.ChequeNo,convert(varchar(10),SPM.ChequeDate,101) AS ChequeDate,                                                                              
			isnull((select CreditAmount from CustomerCreditMaster where CustomerAutoId=@CustomerAutoId),0.00)as TotalCreditAmount,                                                                                                  
			CPD.Status,case when CPD.Status=0 then 'Settled' else 'Cancelled' end as StatusName,SortAmount,TotalCurrencyAmount                                                                           
			FROM CustomerPaymentDetails AS CPD                                              
			INNER JOIN EmployeeMaster AS EMP ON EMP.AutoId=CPD.EmpAutoId                                                                                                  
			LEFT JOIN SalesPaymentMaster AS SPM ON SPM.PaymentAutoId=CPD.refPaymentId                                                                                                  
			where CPD.PaymentAutoId =@PaymentAutoId                                                                                                  
                                                                                         
			select (CONVERT(varchar(10),OrderDate,101)) as  OrderDate,OrderNo,po.ReceivedAmount,                
			otm.OrderType AS OrderType  from PaymentOrderDetails as po                                                                                                  
			inner join OrderMaster as om on om.AutoId=po.OrderAutoId 
			INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
			where PaymentAutoId =@PaymentAutoId                                                                                       
                                                                              
			select CurrencyName,NoofValue,TotalAmount from PaymentCurrencyDetails as pcd                                                                                    
			inner join CurrencyMaster as cm on cm.AutoId=pcd.CurrencyAutoId                                                                                    
			where PaymentAutoId=@PaymentAutoId order by cm.CurrencyValue desc                                                                             
                                              
			SELECT AutoId,PaymentMode from PAYMENTModeMaster --added on 11/29/2019 By Rizwan Ahmad                                                                                     
		END
		
		ELSE IF @Opcode=44        
		BEGIN                                           
                                                                                                     
			SET @CustomerAutoId=(SELECT CustomerAutoId FROM CustomerPaymentDetails WHERE  PaymentAutoId=@PaymentAutoId)                                                                                                  
			
			SELECT OrderAutoId,ReceivedAmount INTO #RESULT411 FROM PaymentOrderDetails                                                                                                 
			WHERE  PaymentAutoId=@PaymentAutoId                                                                        
                                                                                                        
			SELECT om.AutoId as OrderAutoId,(otm.OrderType) AS                                                                                                   
			OrderType,OrderNo,(convert(varchar(10),OrderDate,101)) as OrderDate,DO.PayableAmount,DO.AmtPaid,DO.AmtDue,                                                             
			ReceivedAmount,0 AS Pay                                                                                                  
			FROM OrderMaster AS OM INNER JOIN DeliveredOrders AS DO ON OM.AutoId=DO.OrderAutoId                                                                                                  
			INNER JOIN #RESULT411 AS TEMP ON temp.OrderAutoId=OM.AutoId    
			INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
			UNION                                                                                                  
			SELECT om.AutoId as OrderAutoId,(otm.OrderType) AS                                                                                                   
			OrderType,OrderNo,(convert(varchar(10),OrderDate,101) )OrderDate,DO.PayableAmount,DO.AmtPaid,DO.AmtDue,                                                     
			0.00 AS ReceivedAmount,1 AS Pay                        
			FROM OrderMaster AS OM INNER JOIN DeliveredOrders AS DO ON OM.AutoId=DO.OrderAutoId   
			INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
			where CustomerAutoId =@CustomerAutoId and DO.AmtDue  >0                                                                                                  
			And OrderAutoId not in (select OrderAutoId from #RESULT411)                                                                                              
                                                                                           
			select CurrencyName,cm.CurrencyValue,cm.AutoId,case when NoofValue is null then 0 else NoofValue end as NoofValue,                                                               
			case when TotalAmount is null then 0 else TotalAmount end as TotalAmount   from CurrencyMaster as cm                                               
			left join PaymentCurrencyDetails  as pcd on pcd.CurrencyAutoId=cm.AutoId and PaymentAutoId=@PaymentAutoId     
			where Status=1                                                                             
			order by cm.CurrencyValue  desc                                                                         
                                                                                    
		END 
		
		ELSE IF @Opcode=21                                                                                    
		BEGIN                                                                         
		BEGIN TRY                                                                                                  
		BEGIN TRAN                                                                                                  
			set @CustomerAutoId =(select CustomerAutoId from CustomerPaymentDetails where PaymentAutoId=@PaymentAutoId)                                                                                                  
			declare @credit decimal(10,2)=isnull((select ReceivedAmount-ISNULL(SortAmount,0) from CustomerPaymentDetails where PaymentAutoId=@PaymentAutoId),0.00)-                      
			ISNULL((select SUM(ISNULL(ReceivedAmount,0)) from paymentOrderDetails where PaymentAutoId=@PaymentAutoId),0)                                                              
                                                              
			declare @PAYMODE INT=(select PaymentMode from CustomerPaymentDetails where PaymentAutoId=@PaymentAutoId)                                                                                                  
			if @credit > 0                                                                                                  
			begin                                                                                                  
				UPDATE CustomerCreditMaster set CreditAmount=(ISNULL(CreditAmount,0)-@credit) where CustomerAutoId=@CustomerAutoId                                                                                                  
				DELETE FROM  tbl_Custumor_StoreCreditLog where ReferenceType='CustomerPaymentDetails' and ReferenceNo=@PaymentAutoId                                                          
			end                                                                                                  
                                                                                              
			if @PAYMODE =5                                                                                                  
			begin                                                                                                  
				UPDATE CustomerCreditMaster set CreditAmount=(ISNULL(CreditAmount,0)+ ISNULL((select ReceivedAmount from CustomerPaymentDetails                                     
				where PaymentAutoId=@PaymentAutoId),0)) where CustomerAutoId=@CustomerAutoId                                                                                                  
				DELETE FROM  tbl_Custumor_StoreCreditLog where ReferenceType='CustomerPaymentDetails' and ReferenceNo=@PaymentAutoId                         
			end                                                                                    
                                                                           
			DELETE FROM PaymentOrderDetails where PaymentAutoId=@PaymentAutoId                  
                                                                                                        
			INSERT INTO PaymentOrderDetails(PaymentAutoId,OrderAutoId,ReceivedAmount)                                                                                                  
			SELECT @PaymentAutoId,OrderAutoId,ReceivedAmount FROM @TableValue 
                                                                                   
			UPDATE CustomerPaymentDetails set ReceivedAmount =@ReceivedAmount,PaymentMode= @PaymentMethod,                                                       
			ReferenceNo= @ReferenceNo,ReceivedBy=@ReceivedPaymentBy,PaymentRemarks=@PaymentRemarks,SortAmount=@SortAmount 
			,TotalCurrencyAmount=@TotalCurrencyAmount,                                                      
			EmployeeRemarks=@EmployeeRemarks where PaymentAutoId=@PaymentAutoId  	

			INSERT INTO CustomerPaymentDetailsLog (PaymentAutoId,PaymentId,PaymentDate,EmpAutoId,PaymentRemarks) 
            VALUES (@PaymentAutoId,(Select top 1 PaymentId from CustomerPaymentDetails where PaymentAutoId=@PaymentAutoId),GETDATE(),@EmpAutoId,'Payment has been updated') 

			if @PaymentMethod=1                                           
			BEGIN                                                                                    
				DELETE from PaymentCurrencyDetails where PaymentAutoId=@PaymentAutoId                    
				INSERT INTO PaymentCurrencyDetails(PaymentAutoId,CurrencyAutoId,NoOfValue,P_CurrencyValue)                                                                                
				SELECT PaymentAutoId,CurrencyAutoId,NoOfValue,CM.CurrencyValue FROM (
				select @PaymentAutoId AS PaymentAutoId,tr.td.value('CurrencyAutoId[1]','int') as CurrencyAutoId,                                                                                    
				tr.td.value('NoOfValue[1]','int') as NoOfValue,
				(Select CurrencyValue FROM CurrencyMaster where AutoId=tr.td.value('CurrencyAutoId[1]','int')) as P_CurrencyValue
				from  @PaymentCurrencyXml.nodes('/PaymentCurrencyXml') tr(td)   
				) AS T 
				INNER JOIN CurrencyMaster as cm on cm.AutoId=T.CurrencyAutoId
			END 
			
			if @PaymentMethod =5                                                       
			begin                                                                                                  
				UPDATE CustomerCreditMaster set CreditAmount=(ISNULL(CreditAmount,0)- ISNULL(@ReceivedAmount,0)) where CustomerAutoId=@CustomerAutoId                                                                                                  
				insert into tbl_Custumor_StoreCreditLog(CustomerAutoId,ReferenceType,ReferenceNo,Amount,CreatedDate,CreatedBy)                                                                                                  
				values(@CustomerAutoId,'CustomerPaymentDetails',@PaymentAutoId,@ReceivedAmount,getdate(),@EmpAutoid)                                                                        
			end                                                                                               
			ELSE                                                                                         
			BEGIN                                                                                            
				SET @StoreCredit=ISNULL((SELECT ReceivedAmount-ISNULL(SortAmount,0) FROM CustomerPaymentDetails WHERE PaymentAutoId=@PaymentAutoId),0)-                                                                            
				ISNULL((SELECT SUM(ISNULL(ReceivedAmount,0)) FROM PaymentOrderDetails where PaymentAutoId=@PaymentAutoId),0) 
				UPDATE CustomerPaymentDetails set CreditAmount=@StoreCredit where PaymentAutoId=@PaymentAutoId

				IF(ISNULL(@StoreCredit,0)>0)                                                                                            
				BEGIN                                                                          
					IF EXISTS(SELECT * FROM CustomerCreditMaster WHERE CustomerAutoId=@CustomerAutoId)                                                                                                  
					BEGIN                                              
						UPDATE CustomerCreditMaster SET CreditAmount=isnull((CreditAmount),0)+@StoreCredit                                                                                             
						WHERE CustomerAutoId=@CustomerAutoId                                      
					END                                                                     
					ELSE                                                                    
					BEGIN                                                                                                  
						INSERT CustomerCreditMaster VALUES (@CustomerAutoId,@StoreCredit)                                                             
					END                                                                                                  
					insert into tbl_Custumor_StoreCreditLog(CustomerAutoId,ReferenceType,ReferenceNo,Amount,CreatedDate,CreatedBy)                                 
					values(@CustomerAutoId,'CustomerPaymentDetails',@PaymentAutoId,-@StoreCredit,getdate(),@EmpAutoid)                                                     
				END                                                                                             
			END                                                                                               
		COMMIT TRAN                                                                                                  
		END TRY                                                                                
		BEGIN CATCH                                                                                                  
		ROLLBACK TRAN                                                                                                  
		SET @isException=1                                                                      
		SET @exceptionMessage='Oops! Something went wrong.Please try later.'                                                                                             
		END CATCH                                                                                                  
		END 

		ELSE IF @Opcode=11                                                                                              
		BEGIN                                                                                                   
		BEGIN TRY                                                                                              
		BEGIN TRAN                                                                                    
				SET @PaymentId=(SELECT [dbo].[SequenceCodeGenerator]('PaymentId'))                                                                                                                                                     
                                      
				IF (@PaymentMethod=5 AND  (ISNULL((Select sum(CreditAmount) from CustomerCreditMaster where CustomerAutoId=@CustomerAutoId),0)<(SELECT SUM(ReceivedAmount) FROM @TableValue )))                                                  
				BEGIN                                                  
					SET @isException=1                                                  
					SET @exceptionMessage='Customer have not sufficient amount in Store Credit.'                                                  
				END                                                  
				ELSE IF ((select count(*) from CustomerPaymentDetails where refPaymentId=@BillAddAutoId)>0 and @PaymentMethod!=5)                                  
				BEGIN                                                  
					SET @isException=1                                                  
					SET @exceptionMessage='This payment has been already settled.'                                                  
				END                                                  
				ELSE                                                                           
				BEGIN                                                                         
					INSERT INTO CustomerPaymentDetails(refPaymentId,CustomerAutoId,PaymentId,PaymentDate,ReceivedAmount,PaymentMode,
					ReferenceNo,EmpAutoId,ReceivedBy,PaymentRemarks,EmployeeRemarks,CreditAmount,SortAmount,TotalCurrencyAmount)                     
					VALUES(@BillAddAutoId,@CustomerAutoId,@PaymentId,GETDATE(),@ReceivedAmount,@PaymentMethod,@ReferenceNo,@EmpAutoId,
					@ReceivedPaymentBy,@PaymentRemarks,@EmployeeRemarks,isnull(@StoreCredit,0),@SortAmount,@TotalCurrencyAmount)     
					                        
					UPDATE SalesPaymentMaster SET STATUS=2,cancelBy=@EmpAutoId,CancelDate=GETDATE(),CancelRemarks=@EmployeeRemarks,                                                                                              
					DateUpdate=GETDATE()  WHERE PaymentAutoId=@BillAddAutoId   
						
					SET @PaymentAutoId=SCOPE_IDENTITY()     
					
					INSERT INTO CustomerPaymentDetailsLog (PaymentAutoId,PaymentId,PaymentDate,EmpAutoId,PaymentRemarks) 
                    VALUES (@PaymentAutoId,@PaymentId,GETDATE(),@EmpAutoId,'Payment has been settled') 
					
					INSERT INTO PaymentOrderDetails(PaymentAutoId,OrderAutoId,ReceivedAmount)                                                                                              
					SELECT @PaymentAutoId,OrderAutoId,ReceivedAmount FROM @TableValue                                                                                       
                                   
				    UPDATE CustomerPaymentDetails set CreditAmount=(ReceivedAmount-SortAmount)-(SELECT SUM(ReceivedAmount) from
					PaymentOrderDetails as pod where pod.PaymentAutoId=@PaymentAutoId)
					where PaymentAutoId=@PaymentAutoId

					INSERT INTO PaymentCurrencyDetails(PaymentAutoId,CurrencyAutoId,NoOfValue,P_CurrencyValue)                                                                                
					SELECT PaymentAutoId,CurrencyAutoId,NoOfValue,CM.CurrencyValue FROM (
					select @PaymentAutoId AS PaymentAutoId,tr.td.value('CurrencyAutoId[1]','int') as CurrencyAutoId,                                                                                    
					tr.td.value('NoOfValue[1]','int') as NoOfValue,
					(Select CurrencyValue FROM CurrencyMaster where AutoId=tr.td.value('CurrencyAutoId[1]','int')) as P_CurrencyValue				
					from  @PaymentCurrencyXml.nodes('/PaymentCurrencyXml') tr(td)   
					) AS T 
					INNER JOIN CurrencyMaster as cm on cm.AutoId=T.CurrencyAutoId                        
				                                                                                  
					UPDATE [dbo].[SequenceCodeGeneratorMaster] SET [currentSequence]=[currentSequence]+1 WHERE [SequenceCode]='PaymentId' 
					
					INSERT INTO tbl_OrderLog (ActionTaken,EmpAutoId,ActionDate,Remarks,OrderAutoId) 
                    SELECT 42,@EmpAutoid,GetDate(),'Payment ID '+@PaymentId,OrderAutoId FROM @TableValue    
                                                                 
				IF @PaymentMethod=5                                                      
				BEGIN                                                                                              
					UPDATE CustomerCreditMaster SET CreditAmount=isnull((CreditAmount),0)-@ReceivedAmount WHERE CustomerAutoId=@CustomerAutoId                                                                                       
					insert into tbl_Custumor_StoreCreditLog(CustomerAutoId,ReferenceType,ReferenceNo,Amount,CreatedDate,CreatedBy)                                                                                              
					values(@CustomerAutoId,'CustomerPaymentDetails',@PaymentAutoId,@ReceivedAmount,getdate(),@EmpAutoid)                                                                     
				END                                                                                         
				ELSE                             
				BEGIN                                                                                      
					SET @StoreCredit=ISNULL((SELECT ReceivedAmount FROM SalesPaymentMaster WHERE PaymentAutoId=@BillAddAutoId),0)                                                                    
					-ISNULL((SELECT SUM(ISNULL(ReceivedAmount,0)) FROM PaymentOrderDetails where PaymentAutoId=@PaymentAutoId),0)-ISNULL(@SortAmount,0)                                                       
                                                                                           
					IF(ISNULL(@StoreCredit,0)>0)                                                   
					BEGIN                                                                                        
						IF EXISTS(SELECT * FROM CustomerCreditMaster WHERE CustomerAutoId=@CustomerAutoId)                                                       
						BEGIN                                                          
							UPDATE CustomerCreditMaster SET CreditAmount=isnull((CreditAmount),0)+@StoreCredit                                                  
							WHERE CustomerAutoId=@CustomerAutoId                                      
						END                                                                                              
						ELSE                                                                                               
						BEGIN                                                               
							INSERT CustomerCreditMaster VALUES (@CustomerAutoId,@StoreCredit)                                                                                              
						END                                                                               
							insert into tbl_Custumor_StoreCreditLog(CustomerAutoId,ReferenceType,ReferenceNo,Amount,CreatedDate,CreatedBy)                                                                                             
							values(@CustomerAutoId,'CustomerPaymentDetails',@PaymentAutoId,-@StoreCredit,getdate(),@EmpAutoid)                                                            
						END                                                                                              
					END                                                                                          
                                                                                                 
					                
					END                                             
		COMMIT TRAN                                                                                              
		END TRY                                                                            
		BEGIN CATCH                                                                                              
		ROLLBACK TRAN                                                                                              
			SET @isException=1                                                                                              
			SET @exceptionMessage='Oops! Something went wrong.Please try later.'                                                                                              
		END CATCH                                                                                              
		END

		ELSE IF @Opcode=45                                                        
		BEGIN                                                                                                   
			SELECT ROW_NUMBER() OVER(ORDER BY PayId desc) AS RowNumber, * INTO #Results413 from                        
			(                                         
			SELECT  om.AutoId,PaymentAutoId,PayId,CustomerName,FirstName+' '+ISNULL(LastName,'') + ' <b>('+empt.TypeName+')</b>'  as ReceivedBy,                                                            
			CONVERT(varchar(10),ReceivedDate,101) as ReceivedDate,PayType as PayCode,                                                                                                  
			ReceivedAmount,
			otm.OrderType AS PayType,                                                                                                  
			case when ISNULL(om.status,11)= 11 then 0 else 1 end  as ProcessStatus,spm.PaymentMode as Pmode,   
			pmm.PaymentMode,                
			ReferenceId,Remarks,StatusType as Status,spm.Status as StatusCode,                                                                                                  
			spm.ChequeNo,CONVERT(varchar(10),spm.ChequeDate,101) as ChequeDate                                                                        
			from SalesPaymentMaster AS spm                              
			INNER JOIN CustomerMaster AS CM ON spm.customerAutoid = CM.AutoId                                                                                                      
			INNER JOIN EmployeeMaster AS emp ON emp.Autoid = spm.ReceiveBy                                                                                                 
			INNER JOIN EmployeeTypeMaster AS empt ON emp.EmpType = empt.AutoId                                       
			INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId=spm.Status and SM.[Category]='Pay'                                                                                                  
			left join OrderMaster as om on om.AutoId=spm.OrderAutoId                    
			left join PaymentModeMaster as pmm on                    
			spm.PaymentMode=pmm.AutoId       
			inner join OrderTypeMaster as otm on otm.AutoId=spm.PayType and Type='Payment'
			WHERE spm.CustomerAutoId   =@CustomerAutoId                                                  
			and (@Status=0 or @Status is null or spm.Status =@Status)                                                                                                  
			and (@FromDate is null or @FromDate = '' or @Todate is null or @Todate = '' or                       
			(CONVERT(DATE,ReceivedDate) between CONVERT(DATE,@FromDate) and CONVERT(DATE,@Todate)))                                                                                                         
			)as t Order by PayId desc                               
                   
			SELECT COUNT(RowNumber) AS RecordCount,case when @PageSize=0 then COUNT(PayId) else @PageSize end AS PageSize,@PageIndex as PageIndex FROM #Results413             
                                      
			SELECT * FROM #Results413                                      
			WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))                                                                                              
		END 

		ELSE IF @Opcode=22                                                                      
		BEGIN                                                      
			IF (select count(*) from CustomerPaymentDetails where refPaymentId=@PaymentAutoId)>0                                                  
			BEGIN                                                  
				SET @isException=1                                                  
				SET @exceptionMessage='You can not cancel this payment because this payment has been already settled.'                                                  
			END                                                   
			ELSE                                                  
			BEGIN                                                                                             
				UPDATE SalesPaymentMaster SET Status=3,cancelBy=@EmpAutoId,CancelDate=GETDATE(),CancelRemarks=@EmployeeRemarks,                                                                                                  
				DateUpdate=GETDATE()                                                                                                  
				where PaymentAutoId=@PaymentAutoId                                                   
			END                                                       
       END 

		ELSE IF @Opcode=23                                                                                                  
		BEGIN                   
		IF NOT EXISTS(Select * from SalesPaymentMaster where PaymentAutoId=@PaymentAutoId and Status=5)            
		BEGIN                                                                                               
		BEGIN TRY                                                                                                  
		BEGIN TRAN                                                                                          
			declare @RefAutoid int                                      
			SET @CustomerAutoId=(select CustomerAutoid from SalesPaymentMaster WHERE PaymentAutoId=@PaymentAutoId)                                                                                                  
                                                                                                      
			UPDATE SalesPaymentMaster SET Status=5,cancelBy=@EmpAutoId,CancelDate=GETDATE(),CancelRemarks=@EmployeeRemarks                                                                                                  
			,BankCharges=@ReceivedAmount,DateUpdate=GETDATE()  where PaymentAutoId=@PaymentAutoId             
          
			SET @RefAutoid=(select PaymentAutoId from  CustomerPaymentDetails WHERE refPaymentId=@PaymentAutoId)  
          
			UPDATE CustomerPaymentDetails SET CANCELBY=@EmpAutoId,CANCELDATE=GETDATE(),CANCELREMARKS=@EmployeeRemarks,               
			status=1,DateUpdate=GETDATE(),CreditAmount=0.00 WHERE refPaymentId=@PaymentAutoId                                                 
			DECLARE @PAYMENT DECIMAL(10,2)=(SELECT ReceivedAmount FROM CustomerPaymentDetails WHERE PaymentAutoId=@RefAutoid)                                                                                                  
            
			UPDATE DO SET DO.dueOrderStatus=0 FROM PaymentOrderDetails AS POD 
			INNER  JOIN DeliveredOrders AS DO ON DO.OrderAutoId=POD.OrderAutoId
			WHERE POD.PaymentAutoId=@RefAutoid

			DECLARE @SETTLEMENTAMOUNT DECIMAL(10,2)=ISNULL((SELECT SUM(ISNULL(ReceivedAmount,0.00)) FROM PaymentOrderDetails                                                                                                 
			WHERE PaymentAutoId=@RefAutoid),0)                                  
                                                                                              
			IF(@PAYMENT>@SETTLEMENTAMOUNT)                                                                                                  
			BEGIN                                                                                                  
				DECLARE @TotalAmount decimal(10,2)=(@PAYMENT-@SETTLEMENTAMOUNT)                                                                                                  
				UPDATE CustomerCreditMaster SET CreditAmount =ISNULL(CreditAmount,0.00)-ISNULL(@TotalAmount,0)                             
				WHERE CustomerAutoId=@CustomerAutoId                                                                                                  
			END 
			
            Delete FROM tbl_Custumor_StoreCreditLog WHERE ReferenceNo=@RefAutoid AND ReferenceType='CustomerPaymentDetails' 
			
			Set @OrderNo = (SELECT DBO.SequenceCodeGenerator('OrderNumber'))                                               
                                                                                                      
			Declare @Terms int=(SELECT Terms  FROM CustomerMaster WHERE AutoId=@CustomerAutoId)                                                                                                  
			DECLARE @DEFAULTBIIL INT=(SELECT DefaultBillAdd  FROM CustomerMaster WHERE AutoId=@CustomerAutoId)                                                                                           
			DECLARE @DEFAULTSHIP INT=(SELECT DefaultShipAdd  FROM CustomerMaster WHERE AutoId=@CustomerAutoId)
			DECLARE @SalesPerson INT=(SELECT SalesPersonAutoId  FROM CustomerMaster WHERE AutoId=@CustomerAutoId)
			INSERT INTO [dbo].[OrderMaster] ([OrderNo],[OrderDate],[DeliveryDate],[CustomerAutoId],[Terms],[BillAddrAutoId],[ShipAddrAutoId],                                                                                                  
			[SalesPersonAutoId],[OverallDiscAmt],[ShippingCharges],                                             
			[Status],[ShippingType],                                                                                                  
			TaxType,OrderRemarks,TaxValue,DelDate,PaymentRecev,RecevAmt,AmtDue,OrderType,AccountAutoId,Order_Closed_Date,AcctRemarks)                                            
			values(@OrderNo,GETDATE(),GETDATE(),@CustomerAutoId,@Terms,@DEFAULTBIIL,@DEFAULTSHIP,@SalesPerson,0.00,0.00,11,1,0,@EmployeeRemarks,0.00,                                                                                                  
			GETDATE(),'yes','none',@ReceivedAmount,2,@EmpAutoId,getdate(),@EmployeeRemarks)                                                                                                  
			Declare @OrderAutoId int=(SELECT SCOPE_IDENTITY())                                                                                                  
			declare @PackingAutoId int=27734                                                                                
			INSERT INTO [dbo].[Order_Original] ([AutoId],[OrderNo],[OrderDate],[DeliveryDate],[CustomerAutoId],[Terms],[BillAddrAutoId]                                                                             
			,[ShipAddrAutoId],[SalesPersonAutoId],[TotalAmount],[OverallDisc],[OverallDiscAmt],[ShippingCharges],[TotalTax],[GrandTotal]                                                                          
			,[ShippingType],TaxType)                                                                                                  
			values(@OrderAutoId,@OrderNo,GETDATE(),GETDATE(),@CustomerAutoId,@Terms,@DEFAULTBIIL,@DEFAULTSHIP,@EmpAutoId,@ReceivedAmount,0.00,0.00,0.00,0.00,                                                                                                  
			@ReceivedAmount,0.00,0.00)                                                                                                  
                                                                                                      
			INSERT INTO [dbo].[OrderItemMaster]([OrderAutoId],[ProductAutoId],[UnitTypeAutoId],[QtyPerUnit],[UnitPrice],                                                                                                  
			[RequiredQty],QtyShip,RemainQty,                                                                                                  
			[SRP],[GP],[Tax],IsExchange,TaxValue,UnitMLQty)              
			SELECT @OrderAutoId,[ProductAutoId],[UnitType],1,@ReceivedAmount,1,1,0,0.00,                                                                                        
			0.00,0.00,0,0,0.0 FROM PackingDetails pm  WHERE [AutoId]=@PackingAutoId                                   
                                                                                                      
			INSERT INTO [dbo].[OrderItems_Original]([OrderAutoId],[ProductAutoId],[UnitTypeAutoId],[QtyPerUnit],[UnitPrice],                                                                                                  
			[RequiredQty],                                                                                                  
			[TotalPieces],[SRP],[GP],[Tax],[NetPrice],IsExchange)                                                                                                  
			SELECT @OrderAutoId,[ProductAutoId],[UnitType],1,@ReceivedAmount,1,1 ,0.00,                                                              
			0.00,0.00,@ReceivedAmount,0 FROM PackingDetails pm  WHERE [AutoId]=@PackingAutoId                                                                                                  
                                                             
			INSERT INTO [dbo].[DeliveredOrders] ([OrderAutoId])                                                                                                  
			VALUES(@OrderAutoId)                                                                                                  
                                                                                  
			INSERT INTO [dbo].[Delivered_Order_Items] ([OrderAutoId],[ProductAutoId],[UnitAutoId],[QtyPerUnit],[UnitPrice],                                                                                                  
			[Barcode],[QtyShip],[SRP],[GP],[Tax],FreshReturnQty,FreshReturnUnitAutoId,DamageReturnQty,DamageReturnUnitAutoId,                                                                                   
			IsExchange,TaxValue,QtyPerUnit_Fresh ,QtyPerUnit_Damage)                                                                                                  
			SELECT @OrderAutoId,[ProductAutoId],tbl.[UnitType],1,@ReceivedAmount,null,1,0.00,0.00,0.00,                                                                                                  
			0,3,0,3,0,0,Qty,Qty FROM PackingDetails  As tbl WHERE [AutoId]=@PackingAutoId                                                                                                  
			UPDATE SequenceCodeGeneratorMaster SET currentSequence = currentSequence + 1 WHERE SequenceCode='OrderNumber'  
			
			INSERT INTO tbl_OrderLog(ActionTaken,EmpAutoId,ActionDate,Remarks,OrderAutoId) VALUES (44,@EmpAutoId,GetDate(),'Order No '+'['+Convert(varchar,@OrderAutoId)+']',@OrderAutoId)

		COMMIT TRAN                                                                                                  
		END TRY                                                                                                  
		BEGIN CATCH                                                                                                 
		ROLLBACK TRAN              
		SET @isException=1                    
		SET @exceptionMessage='Oops! Something went wrong.Please try later.'                                                   
		END CATCH               
		END            
		END 
		ELSE if @Opcode=24                                
		BEGIN                                
			update SalesPaymentMaster set status = 3, CancelRemarks = @CancelRemark, cancelBy = @EmpAutoId, CancelDate = GETDATE() where PaymentAutoId = @PaymentAutoId 
					
		END 

		ELSE IF @Opcode=25                                                                                
		BEGIN                                                                                                  
			UPDATE SalesPaymentMaster SET Status=4,chequeNo=@CheckNo,ChequeDate=@CheckDate,ChequeRemarks=@PaymentRemarks,                                               
			DateUpdate=GETDATE(),  HoldBy=@EmpAutoId                                                                           
			where PaymentAutoId=@PaymentAutoId                                                                             
		END

		ELSE IF @Opcode=47                                                 
		BEGIN     		
		 Set @Status=(Select Status from SalesPaymentMaster where PaymentAutoId=@PaymentAutoId)
		 IF (@Status=2)
		 BEGIN                                                                                                
			SET @isException=1                                                           
			SET @exceptionMessage='Settled' 
		     
		 END
		 ELSE IF (@Status=3)
		 BEGIN                                                                                                
			SET @isException=1                                                           
			SET @exceptionMessage='Cancelled' 
		 END
		 ELSE
		 BEGIN
			SELECT  
			ISNULL((SELECT  PaymentAutoId,PayId,CONVERT(varchar(10),ReceivedDate,101) as ReceivedDate,                                                                                                  
				PayType as PayCode,ReceivedAmount,otm.OrderType AS PayType,                                                                                                  
				PaymentMode,ReferenceId,spm.ReceiveBy,                                                                                             
				spm.Remarks,OrderAutoId,ChequeNo,CONVERT(varchar(10),ChequeDate,101) as ChequeDate from SalesPaymentMaster AS spm 
				inner join OrderTypeMaster as otm on otm.AutoId=spm.PayType and Type='Payment'
				WHERE PaymentAutoId=@PaymentAutoId                                                                                                   
				for json path
			),'[]') as PayList,
			ISNULL((
				SELECT OM.AutoId as AId,OrderNo ONO,CONVERT(VARCHAR(10),OrderDate,101) AS OD ,                                                                                                  
				(otm.OrderType) AS                                                                                                 
				OT,DO.PayableAmount as Pay,DO.AmtDue,DO.AmtPaid ,datediff(dd,OrderDate,getdate())  as Dd                                                                                                 
				FROM OrderMaster AS OM INNER JOIN DeliveredOrders AS DO ON OM.AutoId=DO.OrderAutoId   and om.Status=11  AND DO.AmtDue > 0   
				INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
				WHERE CustomerAutoId=@CustomerAutoId                                      
				for json path
			),'[]') as DList, 
			(
				SELECT AutoID,PaymentMode as PM FROM PAYMENTModeMaster for json path
			) as PM
			,
			(
				SELECT AutoId,FirstName+' '+ISNULL(LastName ,'') as EmpName from EmployeeMaster where EmpType in (2,5,10,6) and status=1
				order by EmpName for json path
			) as EList
			,
			ISNULL((
				SELECT Autoid,Currencyname CN, CurrencyValue as CV from Currencymaster where Status=1  order by   CurrencyValue desc
				for json path
			),'[]') as CList

			for json path
         END
		END
		ELSE IF @Opcode=49                                
	    BEGIN                                
			 select * from TBL_MST_SecurityMaster where SecurityValue = @SecurityKey and SecurityType = @EmpAutoId and typeEvent = 5                                
	    END  
		ELSE IF @Opcode=59                                
	    BEGIN                                
			 select * from TBL_MST_SecurityMaster where SecurityValue = @SecurityKey and SecurityType = @EmpAutoId and typeEvent = 7                               
	    END 
		ELSE IF @Opcode=431                                
	    BEGIN   
		
		insert into SalesPaymentMasterLog
		(
		PaymentId,[PayId],[CustomerAutoId],[ReceivedDate],[ReceivedAmount],[PaymentMode],[ReferenceId],[Remarks],[ReceiveDate],[Status],[PayType],[OrderAutoId],[cancelBy],
		[CancelDate],[CancelRemarks],[referencePaymentNumber],[DateUpdate],[ChequeNo],[ChequeDate] ,[ChequeRemarks] ,[BankCharges],[HoldBy],[DeviceId],[AppVersion] ,[LatLong]
		)
		select PaymentAutoId,[PayId],[CustomerAutoId],[ReceivedDate],[ReceivedAmount],[PaymentMode],[ReferenceId],[Remarks],[ReceiveDate],[Status]
		,[PayType],[OrderAutoId],[cancelBy],[CancelDate],[CancelRemarks],[referencePaymentNumber],[DateUpdate],[ChequeNo],[ChequeDate] ,[ChequeRemarks] ,[BankCharges],[HoldBy]
		,[DeviceId],[AppVersion] ,[LatLong] from SalesPaymentMaster  where PaymentAutoId=@PaymentAutoId      
	          
		delete from SalesPaymentMaster where PaymentAutoId=@PaymentAutoId      
			  
	    END 
		ELSE IF @OpCode=50
		BEGIN
		     Select  PaymentAutoId,PaymentId,FORMAT(PaymentDate,'MM/dd/yyyy hh:mm tt') as PaymentDate,PaymentRemarks,EM.FirstName+' '+EM.LastName AS EmpName FROM CustomerPaymentDetailsLog AS CPD
             INNER JOIN EmployeeMaster AS EM ON CPD.EmpAutoId=EM.AutoId WHERE PaymentAutoId=@PaymentAutoId	
			 order by PaymentDate desc
		END
		ELSE IF @Opcode=51        
		BEGIN 

			if Exists(SELECT PaymentDate FROM CustomerPaymentDetails WHERE  PaymentAutoId=@PaymentAutoId
			and CONVERT(date,PaymentDate)=CONVERT(date,GETDATE()))
				BEGIN
				SET @isException=0
				set @exceptionMessage='Valid'
				END
			ELSE
				BEGIN
				SET @isException=1
				set @exceptionMessage='Not Valid'
				END
		END
		ELSE IF @Opcode=52                                
	    BEGIN                                
			 select * from TBL_MST_SecurityMaster where SecurityValue = @SecurityKey and SecurityType = @EmpAutoId and typeEvent = 8                                
	    END 
END TRY                                                                                                  
BEGIN CATCH                                                                                                  
	SET @isException=1                                                           
	SET @exceptionMessage='Oops! Something went wrong.Please try later.'                                                  
END CATCH 
End
