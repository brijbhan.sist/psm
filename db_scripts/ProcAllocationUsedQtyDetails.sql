Alter procedure  ProcAllocationUsedQtyDetails -- Exec ProcAllocationUsedQtyDetails 1104,'2020-09-02',11222,0,'success'
	@SalesPersonAutoId int,
	@AllowDate datetime,
	@ProductAutoId int,
@isException BIT OUT,          
@exceptionMessage VARCHAR(max) OUT  
AS
BEGIN
SET @exceptionMessage= 'Success'            
    SET @isException=0  
    Declare @ChildDbLocation varchar(50),@sql nvarchar(max)  
	select @ChildDbLocation=ChildDB_Name from CompanyDetails

	declare @productId int=(select ProductId from ProductMaster where autoid=@ProductAutoId)
BEGIN TRY
	SET @sql='
	select OM.OrderNo,Format(OM.OrderDate,''MM/dd/yyyy'') as OrderDate,CM.CustomerName,
	OIM.QtyShip as PackedQty,OIM.TotalPieces as PackedPieces,
	UM.UnitType as DefautlUnit
	from Delivered_Order_Items as  oim 
	inner join OrderMaster as om on om.AutoId=oim.OrderAutoId
	INNER JOIN CustomerMaster as CM on OM.CustomerAutoId=CM.AutoId
	INNER JOIN UnitMaster as UM on UM.AutoId=OIM.UnitAutoId
	INNER JOIN PackingDetails as PD on PD.UnitType=OIM.UnitAutoId AND PD.ProductAutoId=OIM.ProductAutoId
	where OM.status=11 and OIM.ProductAutoId='+Convert(varchar(15),@ProductAutoId)+'
	and OM.SalesPersonAutoId='+Convert(varchar(15),@SalesPersonAutoId)+'
	and PackerAssignDate >= '''+Convert(varchar(20),@Allowdate)+''' AND OIM.TotalPieces>0

	UNION

	select OM.OrderNo,Format(OM.OrderDate,''MM/dd/yyyy'') as OrderDate,CM.CustomerName,
	OIM.QtyShip as PackedQty,OIM.TotalPieces as PackedPieces,
	UM.UnitType as DefautlUnit
	from ['+@ChildDbLocation+'].[dbo].Delivered_Order_Items as  oim 
	inner join ['+@ChildDbLocation+'].[dbo].OrderMaster as om on om.AutoId=oim.OrderAutoId
	inner join ['+@ChildDbLocation+'].[dbo].ProductMaster as pm on pm.AutoId=oim.ProductAutoId
	INNER JOIN ['+@ChildDbLocation+'].[dbo].CustomerMaster as CM on OM.CustomerAutoId=CM.AutoId
	INNER JOIN ['+@ChildDbLocation+'].[dbo].UnitMaster as UM on UM.AutoId=OIM.UnitAutoId
	INNER JOIN ['+@ChildDbLocation+'].[dbo].PackingDetails as PD on PD.UnitType=OIM.UnitAutoId AND PD.ProductAutoId=OIM.ProductAutoId
	where OM.status=11 and pm.ProductId='+Convert(varchar(15),@productId)+'
	and OM.SalesPersonAutoId='+Convert(varchar(15),@SalesPersonAutoId)+'
	and PackerAssignDate >='''+Convert(varchar(20),@Allowdate)+''' AND OIM.TotalPieces>0

	UNION

	select OM.OrderNo,Format(OM.OrderDate,''MM/dd/yyyy'') as OrderDate,CM.CustomerName,
	OIM.QtyShip as PackedQty,OIM.TotalPieces as PackedPieces,
	UM.UnitType as DefautlUnit
	from OrderItemMaster as  oim 
	inner join OrderMaster as om on om.AutoId=oim.OrderAutoId
	INNER JOIN CustomerMaster as CM on OM.CustomerAutoId=CM.AutoId
	INNER JOIN UnitMaster as UM on UM.AutoId=OIM.UnitTypeAutoId
	INNER JOIN PackingDetails as PD on PD.UnitType=OIM.UnitTypeAutoId AND PD.ProductAutoId=OIM.ProductAutoId
	where om.status in (3,4,5,6,7,9,10) and OIM.ProductAutoId='+Convert(varchar(15),@ProductAutoId)+'
	and OM.SalesPersonAutoId='+Convert(varchar(15),@SalesPersonAutoId)+'
	and PackerAssignDate >= '''+Convert(varchar(20),@Allowdate)+''' AND OIM.TotalPieces>0

	UNION

	select OM.OrderNo,Format(OM.OrderDate,''MM/dd/yyyy'') as OrderDate,CM.CustomerName,
	OIM.QtyShip as PackedQty,OIM.TotalPieces as PackedPieces,
	UM.UnitType as DefautlUnit
	from ['+@ChildDbLocation+'].[dbo].OrderItemMaster as  oim 
	inner join ['+@ChildDbLocation+'].[dbo].OrderMaster as om on om.AutoId=oim.OrderAutoId
	inner join ['+@ChildDbLocation+'].[dbo].ProductMaster as pm on pm.AutoId=oim.ProductAutoId
	INNER JOIN ['+@ChildDbLocation+'].[dbo].CustomerMaster as CM on OM.CustomerAutoId=CM.AutoId
	INNER JOIN ['+@ChildDbLocation+'].[dbo].UnitMaster as UM on UM.AutoId=OIM.UnitTypeAutoId
	INNER JOIN ['+@ChildDbLocation+'].[dbo].PackingDetails as PD on PD.UnitType=OIM.UnitTypeAutoId AND PD.ProductAutoId=OIM.ProductAutoId
	where om.status in (3,4,5,6,7,9,10) and pm.ProductId='+Convert(varchar(15),@productId)+'
	and OM.SalesPersonAutoId='+Convert(varchar(15),@SalesPersonAutoId)+'
	and PackerAssignDate>='''+Convert(varchar(20),@Allowdate)+''' AND OIM.TotalPieces>0
	'
	--Select @sql
	EXEC sp_executesql @sql
END TRY
BEGIN CATCH
         SET @exceptionMessage= ERROR_MESSAGE()         
    SET @isException=1   
END CATCH
END

