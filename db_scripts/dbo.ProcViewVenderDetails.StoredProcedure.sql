alter procedure [dbo].[ProcViewVenderDetails]  
@OpCode int=Null,    
@Vendorid varchar(30)=null,  
@VendorAutoId int= null,     
@DocumentName varchar(100)=null,  
@DocumentURL varchar(500)=null,  
@EmpAutoId int=null,  
@FileAutoId int=null,  
@PayId varchar(20)=null,  
@ExpiryDate varchar(10)=null,              
@BankName varchar(500)=NULL,     
@Remarks varchar(max)=NULL,    
@BankAcc varchar(20)=NULL,            
@CardNo varchar(20)=NULL,            
@CVV varchar(20)=NULL,            
@CardType int=NULL,            
@AutoId int=null,  
@Status int=null,  
@RoutingNo varchar(20)=null,  
@FromDate date=NULL,                                        
@ToDate date=NULL, 
@Zipcode varchar(6)=null,
@PageIndex INT=1,        
@PageSize INT=10,        
@RecordCount INT=null,        
@isException bit out,        
@exceptionMessage varchar(max) out    
as  
BEGIN        
 BEGIN TRY        
   SET @isException=0        
   SET @exceptionMessage='success'   
   IF @OpCode = 41  
   BEGIN  
	   select Vendorid,VendorName,ContactPerson,case when LocationTypeAutoId=1 then 'Internal' else 'Normal' end as LocationAutoId 
	   from VendorMaster where Vendorid = @Vendorid   order by VendorName
	   end  
	   else if @OpCode = 42  
	   BEGIN   
	   set @VendorAutoId = (select AutoId from VendorMaster where VendorId = @Vendorid)  
  
	  SELECT ROW_NUMBER() OVER(ORDER BY AutoId DESC) AS RowNumber, * INTO #Results FROM        
	  (                                                                                
	  Select PO.AutoId,format(PO.PODate,'MM/dd/yyyy hh:mm tt')as PODate,vm.VendorName,sm.StatusType,sm.ColorCode,NoofItems,isnull(PORemarks,'No Remark') as PORemarks,          
	  isnull(format(PO.DeliveryDate,'MM/dd/yyyy'),'N/A')as DeliveryDate,PONo,StockStatus as POStatus from [dbo].[PurchaseOrderMaster] as Po            
	  left join VendorMaster vm on vm.AutoId=Po.VenderAutoId             
	  left join StatusMaster sm on sm.AutoId=Po.Status and sm.category='OrderMaster'          
	  where po.VenderAutoId=@VendorAutoId  
	  ) as t order by AutoId  DESC        
        
	  SELECT COUNT(AutoId) AS RecordCount, case when @PageSize=0 then COUNT(AutoId) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Results        
	  SELECT * FROM #Results        
	  WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))  
 END  
 else if @OpCode = 43  
 begin  
        set @VendorAutoId = (select AutoId from VendorMaster where VendorId = @Vendorid)  
     select Format(cm.CheckDate,'MM/dd/yyyy') as CheckDate,CheckNO,isnull(CheckAmount,0.00) as CheckAmount,vm.VendorName,(em.FirstName +' '+em.LastName) CreatedBy from CheckMaster cm  
  inner join VendorMaster vm on vm.AutoId = cm.VendorAutoId and cm.Type=2  
  inner join EmployeeMaster em on em.AutoId = cm.CreatedBy  
  where (@Vendorid='' or cm.VendorAutoId=@VendorAutoId)   
  order by cm.CheckDate desc  
 end    
  ELSE IF @Opcode=45                                                                            
   BEGIN                                                                 
    SET @VendorAutoId=(SELECT AutoId FROM VendorMaster WHERE VendorId=@VendorId)                                                                                            
    INSERT INTO vendorDocumentMaster(VendorAutoId,DocumentName,DocumentURL,UploadedDate,ModifyDate                    
    ,UploadedBy,ModifyBy)                                                      
    VALUES(@VendorAutoId,@DocumentName,@DocumentURL,GETDATE(),GETDATE(),@EmpAutoId,@EmpAutoId)                                                               
   END     
    ELSE IF @Opcode=46                                                                                            
   BEGIN                                                                                            
       SET @VendorAutoId=(SELECT AutoId FROM VendorMaster WHERE VendorId=@VendorId)                                                   
       SELECT  AutoId,DocumentName,DocumentURL FROM vendorDocumentMaster                                               
       WHERE VendorAutoId =@VendorAutoId                                                                                 
   END    
      ELSE IF @Opcode=47                                                         
   BEGIN                                                                                             
     UPDATE vendorDocumentMaster SET DocumentName =@DocumentName,                                                                                            
     DocumentURL=case when @DocumentURL is null or @DocumentURL='' then DocumentURL else @DocumentURL end                                                                                            
     WHERE AutoId=@FileAutoId                                                                                            
   END                          
   ELSE IF @Opcode=48                                                                        
   BEGIN                                                                                             
     DELETE FROM  vendorDocumentMaster  WHERE AutoId=@FileAutoId                                                                                            
   END    
    else if @Opcode=49                          
   begin                          
    select AutoId,CardType from [dbo].[CardTypeMaster] order by CardType asc                
   end               
   else if @Opcode=50                          
   begin          
     BEGIN TRY                                      
      BEGIN TRAN          
		  set @VendorAutoId=(Select AutoId from VendorMaster where VendorId=@Vendorid)
		  if @Status=1  
		  begin  
			  if exists(select BankAcc from VendorBankDetails where BankACC=@BankAcc)
			  begin
				 SET @isException=1                                  
				 SET @exceptionMessage='This Account No. is already exist.'   
			  end
			  else
			  begin
				  INSERT INTO  [dbo].[VendorBankDetails]([VendorAutoId],[BankName],[BankACC],Status,RoutingNo,BankRemark)             
				  values (@VendorAutoId,@BankName,@BankAcc,1,@RoutingNo,@Remarks)   
			  end
		  end  
		  else  
		  begin  
			  if exists(select CardNo from VendorBankDetails where CardNo=@CardNo)
			  begin
				 SET @isException=1                                  
				 SET @exceptionMessage='This Card No. is already exist.' 
			  end
			  else
			  begin
				   INSERT INTO  [dbo].[VendorBankDetails]([VendorAutoId],[CardTypeAutoId],[CardNo],[ExpiryDate],[CVV],Status,Zipcode,CardRemark)             
				   values (@VendorAutoId,@CardType,@CardNo,@ExpiryDate,@CVV,2,@Zipcode,@Remarks)  
			  end
		 end  
		 COMMIT TRAN                                                                                            
		 END TRY                                                                         
		 BEGIN CATCH                                                                                            
			  ROLLBACK TRAN                               
				 SET @isException=1                                              
				 SET @exceptionMessage=ERROR_MESSAGE()                                                                                            
		 END CATCH                          
   end              
    else if @Opcode=51                          
   begin          
		select cbd.AutoId,BankName,BankACC,RoutingNo,BankRemark from [dbo].[VendorBankDetails] as cbd   
		where VendorAutoId=(select AutoId from VendorMaster where VendorId=@Vendorid) and Status=1     
   
		select cbd.AutoId,CardNo,CONVERT(VARCHAR(10),ExpiryDate,101) AS ExpiryDate,CVV,CardType,Zipcode,CardRemark from [dbo].[VendorBankDetails] as cbd            
		inner join [dbo].[CardTypeMaster] as ctm on ctm.AutoId=cbd.CardTypeAutoId            
		where VendorAutoId=(select AutoId from VendorMaster where VendorId=@Vendorid) and Status=2      
   end  
    else if @Opcode=52                          
   begin                          
    select AutoId,BankName,BankACC,CardRemark,BankRemark,CardNo,RoutingNo,CONVERT(VARCHAR(10),ExpiryDate,101) AS ExpiryDate,CardTypeAutoId,CVV,Zipcode from VendorBankDetails where AutoId=@AutoId          
    end         
    else if @Opcode=53                          
   begin   
    BEGIN TRY                                      
  BEGIN TRAN                             
   set @VendorAutoId=(Select AutoId from VendorMaster where VendorId=@Vendorid)   
   if @Status=1  
   begin  
     IF exists(SELECT BankACC FROM VendorBankDetails WHERE BankACC = @BankAcc and AutoId != @AutoId)
	 begin
		 SET @isException=1                                  
		 SET @exceptionMessage='This Account No. is already exist.'  
	 end
	 else
	 begin
		update VendorBankDetails set [BankName]=@BankName,[BankACC]=@BankAcc,RoutingNo=@RoutingNo,BankRemark=@Remarks--,[CardTypeAutoId]=@CardType,[CardNo]=@CardNo,[ExpiryDate]=@ExpiryDate,[CVV]=@CVV            
		where AutoId=@AutoId and Status=1   
  end
  end  
  else  
  begin  
    IF exists(SELECT CardNo FROM VendorBankDetails WHERE CardNo = @CardNo and AutoId != @AutoId)
	 begin
		 SET @isException=1                                  
		 SET @exceptionMessage='This Account No. is already exist.'  
	 end
	 else
	 begin
	  update VendorBankDetails set [CardTypeAutoId]=@CardType,[CardNo]=@CardNo,[ExpiryDate]=@ExpiryDate,[CVV]=@CVV,Zipcode=@Zipcode,CardRemark=@Remarks            
	  where AutoId=@AutoId and Status=2  
    end
  end        
   COMMIT TRAN                                                                                            
    END TRY                                                                         
    BEGIN CATCH                                                                                            
     ROLLBACK TRAN                               
     SET @isException=1                                              
     SET @exceptionMessage=ERROR_MESSAGE()                                                                                            
    END CATCH                            
   end         
    else if @Opcode=54                          
   begin                          
    delete from  VendorBankDetails where AutoId=@AutoId       
   end    
	ELSE IF @Opcode=55                          
	BEGIN                  
		IF EXISTS(Select * from StockEntry WHERE VendorAutoId = (Select AutoId from VendorMaster Where VendorId=@VendorId))                                   
		BEGIN                       
			SET @isException=1                                  
			SET @exceptionMessage='Vender has been used in Purchased Order.'                                  
		END 
		ELSE IF EXISTS(SELECT * FROM VendorBankDetails WHERE VendorAutoId = (Select AutoId from VendorMaster Where VendorId=@VendorId)) 
		BEGIN
		    SET @isException=1                                  
			SET @exceptionMessage='Vender has been used in Bank Details.' 
		END
		ELSE IF EXISTS(SELECT * FROM vendorDocumentMaster WHERE VendorAutoId = (Select AutoId from VendorMaster Where VendorId=@VendorId)) 
		BEGIN
		    SET @isException=1                                  
			SET @exceptionMessage='Vender has been used in Document Details.' 
		END
		ELSE IF EXISTS(SELECT * FROM VendorLog WHERE VendorAutoId = (Select AutoId from VendorMaster Where VendorId=@VendorId)) 
		BEGIN
		    SET @isException=1                                  
			SET @exceptionMessage='Vender has been used in Vendor Log.' 
		END
		ELSE IF EXISTS(SELECT * FROM VendorPayMaster WHERE VendorAutoId = (Select AutoId from VendorMaster Where VendorId=@VendorId)) 
		BEGIN
		    SET @isException=1                                  
			SET @exceptionMessage='Vender has been used in Vendor Pay Master.' 
		END
		ELSE                                  
		BEGIN                                     
			DELETE FROM [dbo].[VendorMaster] WHERE [VendorId]=@VendorId                                     
		END              
    END     
    if @OpCode = 56   
	begin    
		SELECT ROW_NUMBER() OVER(ORDER BY PaymentDate desc) AS RowNumber ,(vpm.AutoId) as PaymentAutoId,vpm.PayId,vm.VendorName,
		Format(vpm.PaymentDate,'MM/dd/yyyy') as PaymentDate,vpm.PaymentAmount,     
		pm.PaymentMode, (select count(AutoId) from VenderPaymentOrderDetails where PaymentAutoId=vpm.AutoId) as NoOfBill,   
		vpm.Remark,vpm.ReferenceID,vpm.Status,sm.StatusType,(case when vpm.PaymentType=0 or vpm.PaymentType=0 then 'NA' else PTM.type end)
		as PaymentType  INTO #RESULT561 from VendorPayMaster vpm    
		inner join VendorMaster vm on vm.AutoId = vpm.VendorAutoId    
		inner join StatusMaster sm on sm.AutoId = vpm.Status and sm.Category = 'Pay'    
		inner join PAYMENTModeMaster pm on pm.AutoID = vpm.PaymentMode  
		left join PaymentTypeMaster PTM on ptm.AutoId=vpm.PaymentType
		where  vpm.VendorAutoId=(select AutoId from VendorMaster where VendorId=@Vendorid)  
		and (@FromDate is null or @ToDate is null or (CONVERT(date,vpm.PaymentDate) between convert(date,@FromDate) and CONVERT(date,@ToDate)))   
		order by PayId desc   
		SELECT COUNT(PaymentAutoId) AS RecordCount, @PageSize AS PageSize, @PageIndex AS PageIndex FROM #RESULT561         
          
		SELECT * FROM #RESULT561          
		WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1  order by PayId desc
  
		SELECT ROW_NUMBER() OVER(ORDER BY vl.AutoId) AS RowNumber ,vl.AutoId,Format(ActionDate,'MM/dd/yyyy') as ActionDate,
		em.FirstName+' '+LastName as ActionBy,Remarks,Action,VendorAutoId  
		INTO #RESULT56 FROM VendorLog as vl  
		inner join EmployeeMaster as em on em.AutoId=vl.ActionBy  
		inner join [dbo].[tbl_ActionMaster] as am on am.AutoId=vl.ActionTaken  
		WHERE  VendorAutoId=(select AutoId from VendorMaster where VendorId=@Vendorid)       
             
		SELECT COUNT(AutoId) AS RecordCount, @PageSize AS PageSize, @PageIndex AS PageIndex FROM #RESULT56          
          
		SELECT * FROM #RESULT56          
		WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1            
	end      
    else if @OpCode = 57  
   BEGIN   
   set @VendorAutoId = (select AutoId from VendorMaster where VendorId = @Vendorid)  
	select ROW_NUMBER() over(order by bd desc) as RowNumber,* into #Result from   
	( select  se.[AutoId],vm.VendorName,[BillNo],  
	[BillDate] as bd,  
	CONVERT(VARCHAR(20), SE.[BillDate], 101) AS BillDate,isnull(DueAmount,0.00) as DueAmount,isnull(PaidAmount,0.00) as PaidAmount,CONVERT(VARCHAR(20), se.UpdateDate, 101) AS UpdateDate,emp.FirstName+' '+LastName as UpdatedBy,--(select PONo from PurchaseOrderMaster where VenderAutoId=@VendorAutoId) as PONo,                   
	(SELECT COUNT(distinct [ProductAutoId]) from [dbo].[BillItems] WHERE BillAutoId=SE.AutoId) AS NoOfItems,                       
	OrderAmount as  TotalAmount,                      
	[Remarks],Reference_Code  FROM [dbo].[StockEntry] AS SE                       
	left join vendormaster as vm on vm.autoid=se.vendorautoid     
	left join EmployeeMaster as emp on emp.AutoId=se.UpdatedBy   
	where VendorAutoId=@VendorAutoId                  
  ) as t                    
  order by bd desc                                               
  SELECT * FROM #Result                
  WHERE ISNULL(@PageSize,0) = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)                 
  SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize=0 then COUNT(RowNumber) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Result   
 END                       
   END TRY        
 BEGIN CATCH            
  SET @isException=1        
  SET @exceptionMessage='Oops! Something went wrong.Please try later.'  
 END CATCH        
END   
GO
