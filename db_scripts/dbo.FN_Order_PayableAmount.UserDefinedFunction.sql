USE [psmnj.a1whm.com]
GO
Alter table OrderMaster drop column PayableAmount
Drop function [dbo].[FN_Order_PayableAmount]
GO
Create FUNCTION  [dbo].[FN_Order_PayableAmount]  
(  
  @orderAutoId int  
)  
RETURNS decimal(18,2)  
AS  
BEGIN  
	DECLARE @PayableAmount decimal(18,2)   
	SET @PayableAmount=(SELECT  Round(((GrandTotal-isnull([Deductionamount],(0)))-isnull([CreditAmount],(0))),0) FROM OrderMaster WHERE AutoId=@orderAutoId)   
	RETURN @PayableAmount  
END 
GO

Alter table OrderMaster Add PayableAmount  AS ([dbo].[FN_Order_PayableAmount]([autoid]))