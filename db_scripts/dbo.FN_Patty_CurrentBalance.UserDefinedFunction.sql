USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Patty_CurrentBalance]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
CREATE FUNCTION  [dbo].[FN_Patty_CurrentBalance]
(

)
RETURNS decimal(10,2)
AS
BEGIN
	DECLARE @Currentbalance decimal(10,2)	
	set @Currentbalance=Isnull(ISNULL((select Sum(Isnull(TransactionAmount,0)) from PattyCashLogMaster where TransactionType='CR'),0)-
	ISNULL((select Sum(Isnull(TransactionAmount,0)) from PattyCashLogMaster where TransactionType='DR'),0),0)
	RETURN @Currentbalance
END
GO
