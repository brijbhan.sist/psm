alter proc [dbo].[Proc_ProductPackedWithoutBarcodeReport]  
@Opcode int=null,      
@SalesPerson VARCHAR(200)=null,                    
@CustomerAutoId INT=NULL,                                     
@ShippingAutoId VARCHAR(200)=null,   
@OrderNo VARCHAR(12)=null, 
@FromDate date=NULL,                
@ToDate date=NULL,     
@PageIndex INT = 1,          
@PageSize INT = 10,                 
@isException BIT OUT,          
@exceptionMessage VARCHAR(max) OUT  

as  
begin  
 BEGIN TRY            
    SET @exceptionMessage= 'Success'            
    SET @isException=0   
IF @Opcode=41                           
   BEGIN                                                                                     
     SELECT ROW_NUMBER() OVER(ORDER BY OrderNo asc                                    
    ) AS RowNumber, * INTO #Results from                                          
    (                                                                                                 
		select pm.ProductId,pm.ProductName,OrderNo,em.FirstName+' '+em.LastName as PackedBy,
		format(om.PackerAssignDate,'MM/dd/yyyy hh:mm tt') AS PackedDate1,
		format((select top 1 PackingDate from GenPacking where OrderAutoId=om.AutoId order by AutoId desc),'MM/dd/yyyy hh:mm tt') AS PackedDate,
		[SecurityAllowedName] as ApprovedBy
		from OrderItemMaster as oim 
		inner join OrderMaster as om on om.AutoId=oim.OrderAutoId
		inner join ProductMaster as pm on pm.AutoId=oim.ProductAutoId
		inner join EmployeeMaster as em on em.AutoId=om.PackerAutoId
		inner join [dbo].[TBL_MST_SecurityMaster] as sm on sm.[SecurityValue]=oim.AddedItemKey and SecurityType=3 and typeEvent=1
		where oim.AddedItemKey>'0'  and
       (@OrderNo is null or @OrderNo ='' or OrderNo like '%' + @OrderNo + '%')                                                                                              
       and (@CustomerAutoId is null or @CustomerAutoId =0 or ProductAutoId =@CustomerAutoId)
	   and (@FromDate is null or @FromDate = '' or @Todate is null or @Todate = '' or                
		(CONVERT(DATE,(select top 1 PackingDate from GenPacking where OrderAutoId=om.AutoId order by AutoId desc)) between @FromDate and @Todate))       
    )as t order by OrderNo  asc                                                                                              
                              
    SELECT COUNT(*) as RecordCount,case when @PageSize=0 then COUNT(*) else @PageSize end   as PageSize,@PageIndex as PageIndex FROM #Results                               
    SELECT * FROM #Results                                                                                              
   WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))                                                                               
   select format(GETDATE(),'MM/dd/yyyy hh:mm tt') AS PrintDate
  END         
 ELSE IF @OpCode=42                                                                                    
BEGIN                                                                                                  
	select
		(
		SELECT AutoId,convert(varchar(20),ProductId)+' - '+ProductName as ProductName  FROM ProductMaster order by ProductName asc 
		for json path
		) as Product
	for JSON path                                                                                        
END
ELSE IF @OpCode=43                                                                                    
BEGIN   
	Select (Select CM.AutoId,cm.CustomerId+' '+CM.CustomerName as CustomerName FROM CustomerMaster  as CM                                      
	where (ISNULL(@SalesPerson,'0')='0' OR SalesPersonAutoId in (select * from dbo.fnSplitString(@SalesPerson,',')))
	order by cm.CustomerId+' '+CM.CustomerName asc
	for json path )  as Customer	for JSON path                                                                         
END
 END TRY          
 BEGIN CATCH          
    SET @isException=1          
    SET @exceptionMessage='Oops! Something went wrong.Please try later.'             
 END CATCH   
end

--update AllowQtyPiece set UsedQty=4 where AutoId=11
GO
