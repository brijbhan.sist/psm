USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[getBarcodeLastDigit]    Script Date: 08-04-2021 20:54:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create   function [dbo].[FN_GenerateUPCAbarcode] -- select dbo.getBarcodeLastDigit('1')
(@unittype int)
Returns varchar(13)
BEGIN
    declare @Barcode varchar(15)
	SET @Barcode=(SELECT RandomNumber FROM vw_getRANDValue)
	SET @Barcode=@Barcode+convert(varchar(10),@unittype)      
	Declare @i int=1,@Count int=11,@num int=0,@multiplier int=0,@Even int=0,@Odd int=0,@lastDigit int=0  
                                                       
	WHILE @i <= @Count                                                          
	BEGIN   
		SET @num=(Select SUBSTRING(@Barcode, @i, 1))

		If(ISNULL(@i,0)%2=0)
		BEGIN
		    SET @Even=ISNULL(@Even,0)+ISNULL(@num,0);
		END
		ELSE
		BEGIN
		    SET @Odd=ISNULL(@Odd,0)+ISNULL(@num,0);
		END
		SET @i=@i+1  
	END 
	SET @lastDigit=@Even+(@Odd*3);
	SET @lastDigit=@lastDigit%10
	SET @lastDigit=10-@lastDigit
	SET @Barcode=@Barcode+CONVERT(VARCHAR(10),@lastDigit)
	return @Barcode

END


