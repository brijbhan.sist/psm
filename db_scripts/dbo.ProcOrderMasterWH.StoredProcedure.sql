 create or Alter PROCEDURE [dbo].[ProcOrderMasterWH]                                                                                                                                                    
 @Opcode INT=NULL,                                                                                                                                                    
 @OrderAutoId INT=NULL,                                                                                                                                                                                                                                                                                                    
 @OrderNo VARCHAR(15)=NULL,                                                                                                                                                    
 @IsExchange int=NULL,                                                                                                                                                                                                                                                                                                        
 @CustomerAutoId INT=NULL,                                                                                                                                                                                                                                                                                                       
 @IsFreeItem  INT=NULL,                                                                                                                                                    
 @IsTaxable int =NULL,                                                                                                                                                    
 @ProductAutoId INT=NULL,    
 @UnitAutoId INT=NULL, 
 @Times INT=NULL,          
 @LoginEmpType INT=NULL,
 @PackerAutoId INT=NULL,                                        
 @EmpAutoId INT=NULL,                                                  
 @ReqQty  INT=NULL,                                                                                
 @QtyPerUnit INT=NULL,                                                                                                            
 @OrderItemAutoId INT=NULL,                                                                                
 @TableValue [dbo].[DT_OrderItemListWM] readonly,                                                                                                                                                                                                                                                         
 @Barcode varchar(50)=NULL, 
 @LogRemark varchar(200) = NULL,  
 @Remarks VARCHAR(max)=NULL,                                                                                                                                                     
 @QtyShip INT=NULL,                                                                                                                                              
 @PriceLevelAutoId INT=NULL,              
 @CustomerTypeAutoId int=null,          
 @CheckSecurity VARCHAR(250)=NULL,                                                     
 @isException bit out,                                           
 @exceptionMessage varchar(max) out                                                                                      
AS                                                                                                                                                    
BEGIN                                                                                                                               
  BEGIN TRY                                                                                                                                            
   Set @isException=0                                                                                                                           
   Set @exceptionMessage='Success'                                                                                                         
   declare @customerType varchar(50)
                                                                                                                                                    
   IF @Opcode=41                                                                                                                                                    
   BEGIN  
       IF EXISTS(Select AutoId FROM OrderMaster where OrderNo=@OrderNo)
	   BEGIN
			SET @OrderAutoId = (SELECT [AutoId] FROM [dbo].[OrderMaster] WHERE [OrderNo]=@OrderNo)                                  
			SELECT * FROM (                                                                                                                                                        
			SELECT 'Driver' as EmpType,DrvRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om                                      
			inner join EmployeeMaster as emp on emp.AutoId=om.Driver                                                                                                  
			where om.AutoId=@OrderAutoId and ISNULL(DrvRemarks,'')!=''                                                        
		UNION                                                                                             
			SELECT 'Account' as EmpType,AcctRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om                          
			inner join EmployeeMaster as emp on emp.AutoId=om.AccountAutoId where om.AutoId=@OrderAutoId                                                      
			and ISNULL(AcctRemarks,'')!=''                                                                     
		UNION                                                                                                                                                    
			SELECT 'Sales Person' as EmpType,OrderRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName  from OrderMaster as om                                                                                                                      
			inner join EmployeeMaster as emp on emp.AutoId=om.SalesPersonAutoId where                                                                                          
			om.AutoId=@OrderAutoId and ISNULL(OrderRemarks,'')!=''                                          
		UNION                                                                                                               
			SELECT 'Packer' as EmpType,PackerRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om                                                                                          
			inner join EmployeeMaster as emp on emp.AutoId=om.PackerAutoId                                        
			where om.AutoId=@OrderAutoId and ISNULL(PackerRemarks,'')!=''                                                                                                                                   
		UNION                                                          
			SELECT 'Manager' as EmpType,ManagerRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName  from OrderMaster as om                                                                                                                         
			inner join EmployeeMaster as emp on emp.AutoId=om.ManagerAutoId                                                                          
			where om.AutoId=@OrderAutoId and ISNULL(ManagerRemarks,'')!=''                                                                                                                       
		UNION                                                                                                                                                    
			SELECT 'Warehouse' as EmpType,WarehouseRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName as EmpName  from OrderMaster as om                                      
			inner join EmployeeMaster as emp on emp.AutoId=om.WarehouseAutoId                                                                                                                                                     
			where om.AutoId=@OrderAutoId and ISNULL(WarehouseRemarks,'')!=''                                                                                                                                      
						  ) AS T                          
                                                                                                                                           
			SET @CustomerAutoId= (SELECT [CustomerAutoId] FROM [dbo].[OrderMaster] WHERE AutoId=@OrderAutoId)                                                                                                                                                    
			Declare @OrderStatus int= (SELECT [Status] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId)                                                    
                
                                                                                   
			SELECT PackerAutoId,OM.[AutoId],OM.[OrderNo],CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,ISNULL((CONVERT(VARCHAR(20), OM.[DeliveryDate], 101)),                                                         
			(CONVERT(VARCHAR(20), OM.DelDate,101) + ' ' + convert(varchar(10), DelDate, 108))) AS DeliveryDate,TaxType,                                                                                                                                                    
			OM.[CustomerAutoId],CT.[TermsDesc],CTy.CustomerType,
			[dbo].[ConvertFirstLetterinCapital](BA.[Address]+(case when isnull(BA.Address2,'')!='' then ', '+BA.Address2 else '' end)+', '+BA.City)+', '+UPper(S.StateCode)+' - '+BA.Zipcode As BillAddr,
	        [dbo].[ConvertFirstLetterinCapital](SA.[Address]+(case when isnull(SA.Address2,'')!='' then ', '+SA.Address2 else '' end)+', '+SA.City)+', '+S1.StateCode+' - '+SA.Zipcode As ShipAddr,                                      
            OM.[Status] As StatusCode,DO.[AmtPaid],DO.[AmtDue],otm.OrderType,EMP.[FirstName]+ ' ' + EMP.[LastName] AS SalesPerson,                                                             
			SM.[StatusType] AS Status,OM.[TotalAmount],OM.[OverallDisc],OM.[OverallDiscAmt],OM.[ShippingCharges],OM.[TotalTax],OM.[GrandTotal],OM.[PackedBoxes],CommentType,Comment,                                                          
			OM.[Driver],EM.[FirstName]+ ' ' + EM.[LastName] AS DrvName,OM.[ShippingType],CONVERT(VARCHAR(20),OM.[AssignDate],101) AS AssignDate
			,[DrvRemarks],isnull(DeductionAmount,0.00) as DeductionAmount,ManagerRemarks,                  
			isnull(OM.CreditAmount,0.00)as CreditAmount,isnull(OM.PayableAmount,0.00) as PayableAmount,                                                                                                                                          
			ISNULL((select t.CreditAmount from CustomerCreditMaster as t  where t.CustomerAutoId=om.CustomerAutoid),0.00) as CustomerCredit,                                                                                                                           
			PackerRemarks,OrderRemarks,case when OM.Status=1 then (Select top 1 Time from PackerTimeMaster) else ISNULL(OM.Times,30) end as Times,ISNULL(om.MLQty,0) AS MLQty ,ISNULL(om.MLTax ,0) AS MLTax ,                                                                                                                   
			WarehouseRemarks,ISNULL(om.AdjustmentAmt,0) as AdjustmentAmt,isnull(Weigth_OZQty,0) as Weigth_OZQty,
			isnull(OM.Weigth_OZTax,0) as Weigth_OZTax,isnull(OM.Weigth_OZTaxAmount,0) as  Weigth_OZTaxAmount,CM.CustomerName,ST.ShippingType as ShippingTypeName                                                      
			FROM [dbo].[OrderMaster] As OM                                                                             
			INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.Status AND SM.[Category] = 'OrderMaster'                                                                                                                          
			INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[AutoId] = OM.[BillAddrAutoId]                                                                                                                                   
			INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = OM.[ShipAddrAutoId]  
			INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
			LEFT JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                                                                                                                                                     
			LEFT JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                                                                                                                                                      
			LEFT JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                                                                                                                                     
			LEFT JOIN [CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]  
			INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]    
			LEFT JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[Driver]   
			INNER JOIN [dbo].[EmployeeMaster] AS EMP ON EMP.[AutoId] = OM.SalesPersonAutoId 
			left JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId]  
			inner join [dbo].[ShippingType] as ST on ST.AutoId=OM.ShippingType                       
			WHERE OM.AutoId = @OrderAutoId 
			                                                                                                                                                                                                                                               
			SET @customerType=(select CustomerType from CustomerMaster where AutoId=@CustomerAutoId)     
			
		SELECT OIM.AutoId as ItemAutoId, PM.[AutoId] AS ProductAutoId,PM.[ProductId],PM.[ProductName],UM.AutoId AS UnitAutoId,UM.[UnitType],OIM.[QtyPerUnit],                                                                                                        
		OIM.[UnitPrice],convert(decimal(10,2),UnitPrice/QtyPerUnit) as PerpiecePrice,                                                                                                                            
		ISNULL(OIM.OM_MinPrice,0) as [MinPrice],PD.[Price],OIM.[RequiredQty],ISNULL(OIM.OM_CostPrice,0) as CostPrice,OIM.BasePrice,                                                                              
		[TotalPieces],OIM.[SRP],OIM.[GP],OIM.[Tax],OIM.[NetPrice],OIM.[Barcode],OIM.[QtyShip],                                                                                                                                   
		OIM.[RemainQty],IsExchange,isFreeItem,UnitMLQty,TotalMLQty,isnull(OIM.Weight_Oz,0) as WeightOz,
	    isnull(Oim_Discount,0.00) as Discount,Oim_ItemTotal FROM [dbo].[OrderItemMaster] AS OIM                                                                           
		INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                                                                                                                           
		INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitTypeAutoId                                                                                                                                                     
		INNER JOIN [dbo].[PackingDetails] AS PD ON PD.[ProductAutoId] = OIM.[ProductAutoId] AND PD.[UnitType] = OIM.UnitTypeAutoId                                                                                                  
		WHERE [OrderAutoId]=@OrderAutoId order by PM.[ProductId]  asc

		SELECT PD.ProductAutoId,UM.AutoId AS AutoId, UM.[UnitType] AS UnitType, PD.[Qty],ISNULL(EligibleforFree,0) as EligibleforFree  
		FROM [dbo].[PackingDetails] AS PD                                                                                           
		INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = PD.UnitType      
		WHERE PD.[ProductAutoId] in (select ProductAutoId from OrderItemMaster where OrderAutoId=@OrderAutoId)
	   END
	   ELSE
	   BEGIN
	        SET @isException=1
			SET @exceptionMessage='InvalidOrder'
	   END
  END   
  
  ELSE If @Opcode=42                                                                                                         
  BEGIN                
	SELECT [AutoId],convert(varchar(10),[ProductId]) + '-' + [ProductName] as [ProductName],ISNULL(MLQty,0) AS MLQty, isnull(WeightOz,0) as WeightOz                                                                                                            
	FROM [dbo].[ProductMaster] as pm where ProductStatus=1and (select count(1) from PackingDetails where  PackingStatus=1 and ProductAutoId=pm.AutoId)>0   
	order by [ProductName] 
	for json path, INCLUDE_NULL_VALUES 
  END

  ELSE IF @Opcode=43                                                                                                                      
	BEGIN                                                                                                                                      
		SELECT UM.AutoId AS AutoId, UM.[UnitType] AS UnitType, [Qty],ISNULL(EligibleforFree,0) as EligibleforFree FROM [dbo].[PackingDetails] AS PD                                                                                           
		INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = PD.UnitType WHERE PD.[ProductAutoId] = @ProductAutoId  ORDER BY UM.[UnitType] ASC                                                                                            
		
		SELECT PackingAutoId AS AutoId FROM ProductMaster  WHERE  AutoId=@ProductAutoId                                          
	END 

	ELSE IF @Opcode=44                                                                                                                                                    
    BEGIN                                                                
                    
     IF EXISTS(SELECT * FROM [dbo].[ItemBarcode] WHERE [Barcode]=@Barcode)                                                                                                                                                    
     BEGIN                                                                                                                                         
         SET @ProductAutoId=(SELECT [ProductAutoId] FROM [dbo].[ItemBarcode] WHERE [Barcode]=@Barcode)                                                                          
         SET @UnitAutoId=(SELECT UnitAutoId FROM [dbo].[ItemBarcode] WHERE [Barcode]=@Barcode)                                                                                                                                        
      DECLARE @custType int=(SELECT CustomerType FROM CustomerMaster WHERE AutoId=@CustomerAutoId) 

      SELECT PM.AutoId as ProductAutoId,PM.ProductId,PM.ProductName,PD.Qty as UnitQty,PD.UnitType,PD.Price as BasePrice,
	  (CASE                                                                                                                                                     
		 WHEN @custType=2 THEN ISNULL(PD.WHminPrice,0)                                                                                                                                                    
		 WHEN @custType=3  THEN ISNULL(PD.CostPrice,0)                                                                                                                   
		 ELSE PD.MinPrice END) AS [MinPrice] ,PD.CostPrice,
      ISNULL((SELECT TOP 1 UnitType +  ' ('+ CONVERT(VARCHAR(20),PD.QTY)+' pcs)' from UnitMaster AS UM WHERE UM.AutoId=PD.UnitType),'') AS UnitName                                                                                                          
	  FROM [dbo].[PackingDetails] As PD                                                                                                                                                     
      INNER JOIN [dbo].[ProductMaster] AS PM ON PD.[ProductAutoId]=PM.[AutoId]                                                                                                                                                    
      WHERE PD.[ProductAutoId] = @ProductAutoId and PD.[UnitType] = @UnitAutoId   
	  
	  SELECT UM.AutoId AS AutoId, UM.[UnitType] AS UnitType, [Qty],ISNULL(EligibleforFree,0) as EligibleforFree FROM [dbo].[PackingDetails] AS PD                                                                                           
	  INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = PD.UnitType WHERE PD.[ProductAutoId] = @ProductAutoId                                                                                                                                                                                                                                                        
                                                                                                                                                           
    END  
	End

	ELSE If @Opcode=21                                                                                                                                                       
		BEGIN TRY                                                                                                     
		BEGIN TRAN  
			IF EXISTS(select AutoId FROM [dbo].[OrderMaster] WHERE [AutoId] = @OrderAutoId )                                                                                                                                           
			BEGIN                                                                                                              
				IF((select Status FROM [dbo].[OrderMaster] WHERE [AutoId] = @OrderAutoId )>2)                                                                                                                                           
				BEGIN                                                                                                                                                                                                    
					SET @exceptionMessage='No'                                                                        
				END                                                                                                                                                                                          
				ELSE                                                                           
				BEGIN  
					Set @CustomerAutoId= (SELECT CustomerAutoId FROM OrderMaster WHERE AutoId=@OrderAutoId)          
					DECLARE @custType1 int=(SELECT CustomerType FROM CustomerMaster WHERE AutoId=@CustomerAutoId)       
					Set @PricelevelAutoId =(Select PriceLevelAutoId from CustomerPriceLevel where CustomerAutoId=@CustomerAutoId) 

					select * into #tempOIM from [dbo].[OrderItemMaster] where [OrderAutoId] = @OrderAutoId 
				
					INSERT INTO [dbo].[OrderItemMaster]([OrderAutoId],[ProductAutoId],[UnitTypeAutoId],[QtyPerUnit],[RequiredQty],[TaxValue],                                                                                                                       
					Barcode,[UnitPrice],[SRP],[GP],[Tax],IsExchange,UnitMLQty,isFreeItem,Weight_Oz,OM_CostPrice,OM_MinPrice,BasePrice,Oim_Discount)                                                                             
					SELECT @OrderAutoId,tb.[ProductAutoId],tb.[UnitAutoId],tb.[QtyPerUnit],tb.[RequiredQty],
					ISNULL((select TaxValue from OrderMaster where AutoId=@OrderAutoId),0.00),null,                                                                                                                                             
					case when tb.IsExchange=1 then 0.00 when tb.isFreeItem=1 then 0.00 else 
					isnull((select top 1 pl.CustomPrice from ProductPricingInPriceLevel
					as PL where pl.PriceLevelAutoId=@PriceLevelAutoId
					and pl.ProductAutoId=tb.ProductAutoId and pl.UnitAutoId=tb.UnitAutoId
					and pl.CustomPrice >=(CASE                                                                                                                                                                                                                          
					WHEN @custType1=3  THEN ISNULL(pd.CostPrice,0)    
					WHEN @custType1=2  THEN ISNULL(pd.WHminPrice,0)                                  
					ELSE pd.MinPrice END)
					order  by PL.createDated desc
					),
					(CASE                                                                                                                                                                                                                          
					WHEN @custType1=3  THEN ISNULL(pd.CostPrice,0)                                           
					ELSE pd.price END)) end
					,pm.[P_SRP],
					CONVERT(DECIMAL(10,2),((PM.[P_SRP]-(pd.[Price]/(CASE WHEN pd.[Qty]=0 then 1 else pd.[Qty] end )))/(CASE WHEN ISNULL(PM.[P_SRP],0)=0 then 1 else PM.[P_SRP] END)) * 100) AS GP
					,0,tb.IsExchange,ISNULL(pm.MLQty,0),tb.isFreeItem,case when tb.IsExchange=0 and tb.isFreeItem=0 then 
					ISNULL(pm.WeightOz,0) else 0 end,tb.OM_MinPrice,tb.OM_CostPrice,tb.OM_BasePrice,tb.OM_Discount FROM                       
					@TableValue AS tb                         
					inner join ProductMaster as pm on pm.AutoId=tb.ProductAutoId
					inner join PackingDetails pd on pd.ProductAutoId=pm.AutoId and     
		             tb.[UnitAutoId]=pd.UnitType 
					where [Status]=1
				
					Update OIM set 
					OIM.UnitTypeAutoId=DT.[UnitAutoId],
					OIm.RequiredQty=DT.[RequiredQty],
					Oim.UnitPrice=case when dt.[IsFreeItem]=1 then 0.00 when dt.[IsExchange]=1 then 0.00 else 
					(case when DT.[OldUnitAutoId]=DT.[UnitAutoId] then Oim.UnitPrice else Oim.UnitPrice/convert(decimal(10,2),oim.QtyPerUnit)* DT.QtyPerUnit 
					end) end,
					Oim.OM_minPrice=(case when DT.[OldUnitAutoId]=DT.[UnitAutoId] then Oim.OM_minPrice else Oim.OM_minPrice/convert(decimal(10,2),oim.QtyPerUnit)* DT.QtyPerUnit end),
					oim.QtyPerUnit=DT.QtyPerUnit,Oim_Discount=DT.OM_Discount
					from [dbo].[OrderItemMaster] as OIM
					inner join(Select * from @TableValue where [Status]=0)as DT on DT.ProductAutoId=OIM.ProductAutoId and       
		            dt.[IsExchange] = OIM.[IsExchange] and dt.[IsFreeItem]=oim.isFreeItem and DT.[OldUnitAutoId]=OIM.UnitTypeAutoId         
		            WHERE OIM.OrderAutoId = @OrderAutoId 

					SET @LogRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=2),
					'[OrderNo]', (Select OrderNo from OrderMaster where AutoId=@OrderAutoId))       
				

 
  					INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                                                              
					select 2,@EmpAutoId,getdate(),CONVERT(varchar(50),pm.productId)+' has been added'+ 
					case when tb.IsExchange=1 then ' as Exchange' when tb.IsFreeItem=1 then ' as Free' else '' end as tp,
					@OrderAutoId from @TableValue as tb                                                                                                            
					inner join ProductMaster as pm on pm.AutoId=tb.ProductAutoId 
					where tb.ItemType='New' 



					INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                                                              
					select 2,@EmpAutoId,getdate(),CONVERT(varchar(50),pm.productId)+' Unit has been updated'+
					CONVERT(varchar(50),(case when oim.IsExchange=1 then ' (Exchange)' when oim.IsFreeItem=1 then ' (Free)' else '' end)),					
					@OrderAutoId from @TableValue as tb                                                                                                            
					inner join ProductMaster as pm on pm.AutoId=tb.ProductAutoId 
					inner join #tempOIM oim on oim.ProductAutoId=tb.ProductAutoId and oim.isFreeItem=tb.IsFreeItem and oim.IsExchange=tb.IsExchange
					where tb.ItemType='Old' and tb.OldUnitAutoId != tb.UnitAutoId 
		
					INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])    
					select 2,@EmpAutoId,getdate(),CONVERT(varchar(50),pm.productId)+' has been updated with '+ convert(varchar(max),
					oim.RequiredQty-tmp.RequiredQty)+' quantity'+CONVERT(varchar(50),(case when oim.IsExchange=1 then ' (Exchange)' when oim.IsFreeItem=1 then ' (Free)' else '' end)),
					@OrderAutoId from OrderItemMaster as oim                                                                                                            
					inner join ProductMaster as pm on pm.AutoId=oim.ProductAutoId
					inner join #tempOIM tmp on tmp.AutoId=oim.AutoId
					where tmp.RequiredQty < oim.RequiredQty or tmp.RequiredQty > oim.RequiredQty
          
					SELECT @CustomerAutoId=CustomerAutoId FROM [dbo].[OrderMaster]                 
					WHERE [AutoId] = @OrderAutoId                
					if((select CustomerType from CustomerMaster where AutoId=@CustomerAutoId)=3)                      
					begin                      
					exec [dbo].[ProcUpdatePOStatus_All]                      
					@CustomerId=@CustomerAutoId,                      
					@OrderAutoId=@OrderAutoId,                      
					@Status=2 
				end        
				Set @exceptionMessage='Yes' 
			END
			END
			ELSE
			BEGIN
				Set @isException=1                                                                                                    
				Set @exceptionMessage='InvalidOrder'   
			END
		COMMIT TRANSACTION                                                                                  
		END TRY                                                                                                                                                    
		BEGIN CATCH                                                                      
		ROLLBACK TRAN                                          
			Set @isException=1                                                                                                    
			Set @exceptionMessage=ERROR_MESSAGE()                                                                                            
		END CATCH 

		ELSE IF @Opcode = 22                                                                                                                                                    
		BEGIN                                                                                                
		BEGIN TRY                                                                                                                                                    
		BEGIN TRAN 
		IF EXISTS(Select AutoId from OrderMaster where AutoId=@OrderAutoId)
		BEGIN
			IF EXISTS(SELECT Autoid FROM EmployeeMaster Where Autoid=@EmpAutoId AND EmpType=7)
				BEGIN
				IF EXISTS(SELECT Autoid FROM EmployeeMaster Where Autoid=@PackerAutoId AND EmpType=3)
				BEGIN
					SET @OrderStatus  =(SELECT [Status] FROM [dbo].[OrderMaster] WHERE [AutoId] = @OrderAutoId)                                                     
					UPDATE [dbo].[OrderMaster] SET PackerAutoId = @PackerAutoId,PackerAssignDate=getdate(),                                                                                           
					WarehouseAutoId=@EmpAutoId,WarehouseRemarks=@Remarks,                                                                                    
					PackerAssignStatus =1,Times =@Times,                                                                                              
					[Status] =(case when [Status]=1 then 2 else Status end)                                                                                        
					WHERE [AutoId] = @OrderAutoId                                                                                                                                                    
					SET @LogRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=18), '[PackerName]', (SELECT [FirstName] + ' ' + [LastName] AS packername FROM [dbo].[EmployeeMaster] WHERE [AutoId] = @PackerAutoId))                            
					INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                                                                           
					VALUES(18,@EmpAutoId,getdate(),@LogRemark,@OrderAutoId)                                                                     
					IF(@OrderStatus=1)                                                                                                                                                    
					BEGIN                                                                                                                                                         
						SET @LogRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=3),'[Status1]','New')                                                            
						SET @LogRemark = REPLACE(@LogRemark,'[Status2]','Processed.')                                                                
						INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                                                                                                                                    
						VALUES(3,@EmpAutoId,getdate(),@LogRemark,@OrderAutoId)                                                                           
					END              
					SELECT @CustomerAutoId=CustomerAutoId FROM [dbo].[OrderMaster]                 
					WHERE [AutoId] = @OrderAutoId                
					if((select CustomerType from CustomerMaster where AutoId=@CustomerAutoId)=3)                      
					begin                      
						exec [dbo].[ProcUpdatePOStatus_All]                      
						@CustomerId=@CustomerAutoId,                      
						@OrderAutoId=@OrderAutoId,                      
						@Status=2                          
					end 
				END
				ELSE
				BEGIN
					Set @isException=1                                                                                            
					Set @exceptionMessage='InvalidPacker'    
				END
			  END
			ELSE
			BEGIN
				Set @isException=1                                                                                            
				Set @exceptionMessage='Unauthorized Access'
			END
		END
		ELSE
		BEGIN
			Set @isException=1                                                                                            
			Set @exceptionMessage='InvalidOrder'
		END
		COMMIT TRANSACTION                                                                                                                                                    
		END TRY                                                                          
		BEGIN CATCH                                                                                 
		ROLLBACK TRAN                                                         
			Set @isException=1                                                                                            
			Set @exceptionMessage='Oops! Something went wrong.Please try later.'                                                                                        
		END CATCH                                                                                                
		END                                                                                                                                              
  END TRY                                                                      
  BEGIN CATCH                               
	  Set @isException=1                                          
	  Set @exceptionMessage='Oops! Something went wrong.Please try later.'                                                                                                                                                    
  END CATCH                                                                                       
END 