USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[Proc_BarCodeDelete_ALLDB]    Script Date: 08-09-2020 07:02:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[Proc_BarCodeDelete_ALLDB]           
@Barcode varchar(250) null       
AS          
BEGIN         
 IF(DB_NAME() IN ('psmct.a1whm.com','psmpa.a1whm.com','psmnpa.a1whm.com','psmnj.a1whm.com',
 'psmwpa.a1whm.com','psmny.a1whm.com'))      
 BEGIN     
    DELETE FROM [psmct.a1whm.com].[dbo].[ItemBarcode] WHERE Barcode=@Barcode     
    DELETE FROM [psmpa.a1whm.com].[dbo].[ItemBarcode] WHERE Barcode=@Barcode     
    DELETE FROM [psmnpa.a1whm.com].[dbo].[ItemBarcode] WHERE Barcode=@Barcode     
    DELETE FROM [psmnj.a1whm.com].[dbo].[ItemBarcode] WHERE Barcode=@Barcode     
	DELETE FROM [psmwpa.a1whm.com].[dbo].[ItemBarcode] WHERE Barcode=@Barcode  
	
	DELETE FROM [psmny.a1whm.com].[dbo].[ItemBarcode] WHERE Barcode=@Barcode  
	DELETE FROM [psmnj.easywhm.com].[dbo].[ItemBarcode] WHERE Barcode=@Barcode  
	DELETE FROM [psmpa.easywhm.com].[dbo].[ItemBarcode] WHERE Barcode=@Barcode  
	DELETE FROM [psmnpa.easywhm.com].[dbo].[ItemBarcode] WHERE Barcode=@Barcode  
	DELETE FROM [psmwpa.easywhm.com].[dbo].[ItemBarcode] WHERE Barcode=@Barcode  
	DELETE FROM [psmct.easywhm.com].[dbo].[ItemBarcode] WHERE Barcode=@Barcode  
	DELETE FROM [psmny.easywhm.com].[dbo].[ItemBarcode] WHERE Barcode=@Barcode  
 END     
 ELSE    
 BEGIN    
   DELETE FROM [dbo].[ItemBarcode] WHERE Barcode=@Barcode  
 END     
END  