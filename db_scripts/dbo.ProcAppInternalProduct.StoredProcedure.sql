USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcAppInternalProduct]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE ProcEDURE [dbo].[ProcAppInternalProduct]                        
 @OpCode int=Null,                      
 @timeStamp datetime =null,                    
 @isException bit out,                        
 @exceptionMessage varchar(max) out                     
AS                        
BEGIN                        
 BEGIN TRY                        
  SET @isException=0                        
  SET @exceptionMessage='Success!!'                        
  IF @Opcode=41                       
  BEGIN       
     
 SELECT  convert(varchar(15),ProductId) AS [ProductId],ProductName AS [ProductName],     
 ('http://www.psmnj1.a1whm.com'+[ImageUrl])  as [ImageUrl],pm.AutoId,ISNULL(convert(varchar,(Stock/pd.Qty))+' ('+um.UnitType+')' ,'') as DefaultStock    
 ,ISNULL(WeightOz,0) as WeightOz,ISNULL(MLQty,0) as MLQty,isnull(pm.IsApply_ML,0) as IsApply_ML,  
 isnull(pm.IsApply_Oz,0) as IsApply_Oz,pd.CostPrice as DefaultCostPrice,(convert(varchar(50),pd.Qty) +' ('+um.UnitType+')')as DefaultUnit   
 FROM ProductMaster as pm 
 inner join PackingDetails pd on pd.UnitType=pm.PackingAutoId and pd.ProductAutoId=pm.AutoId 
 join UnitMaster as um on um.AutoId=pd.UnitType      
 FOR JSON path , WITHOUT_ARRAY_WRAPPER ,INCLUDE_NULL_VALUES    
     
  End                        
 END TRY                        
 BEGIN CATCH                        
  SET @isException=1                        
  SET @exceptionMessage=ERROR_MESSAGE()                        
 END CATCH                        
END
GO
