alter PROCEDURE [dbo].[ProcAccountCreditMemoView]          
@Opcode INT=NULL,          
@CreditAutoId INT=NULL,          
@CustomerAutoId INT=NULL,                   
@EmpAutoId INT=NULL,                                                 
@Status INT=NULL,          
@EmpType  INT=NULL,                  
@SecurityKey varchar(50)=null,         
@CancelRemarks varchar(500)=null,  
@Remark varchar(500)=NULL, 
@PageIndex INT = 1,          
@PageSize INT = 10,          
@RecordCount INT =null,          
@isException BIT OUT,          
@exceptionMessage VARCHAR(max) OUT          
AS          
BEGIN          
 BEGIN TRY          
  SET @isException=0          
  SET @exceptionMessage='Success'          
  DECLARE @STATE INT           

	IF @Opcode=47          
   BEGIN          
  SELECT CreditNo,convert(varchar(10),CreditDate,103)  AS CreditDate FROM CreditMemoMaster WHERE CreditAutoId=@CreditAutoId          
  SELECT (EM.FirstName+' '+ EM.LastName ) AS EmpName, ETM.TypeName AS EmpType,format(LogDate,'MM/dd/yyyy hh:mm tt' ) LogDate,          
  AM.[Action],Remarks FROM CreditMemoLog AS CLOG          
  INNER JOIN EmployeeMaster AS EM ON EM.AutoId=CLOG.EmpAutoId          
  INNER JOIN [dbo].[tbl_ActionMaster] As AM ON AM.[AutoId] = CLOG.[ActionAutoId]         
  inner join EmployeeTypeMaster As ETM on ETM.AutoId=EM.EmpType        
  WHERE CLOG.CreditAutoId=@CreditAutoId order by LogDate desc          
   END          
	IF @Opcode=46          
   BEGIN          
	SET @EmpType=(SELECT EmpType FROM EmployeeMaster WHERE AutoId=@EmpAutoId)          
	SET @Status=(SELECT Status FROM CreditMemoMaster as cmm  WHERE CreditAutoId=@CreditAutoId)          
	SET @CustomerAutoId=(SELECT CustomerAutoId FROM CreditMemoMaster as cmm         
	WHERE CreditAutoId=@CreditAutoId)          
           
	SELECT CreditNo,convert(varchar(10),CreditDate,101) as CreditDate,cmm.Status as StatusCode,StatusType AS STATUS,CMM.MLTaxPer,    
	CMM.CustomerAutoId,Remarks,ISNULL(OverallDisc,0 ) as OverallDisc,CM.CustomerName,          
	ISNULL(OverallDiscAmt,0) as OverallDiscAmt,ISNULL(TotalTax,0) as TotalTax,ISNULL(GrandTotal,TotalAmount) as GrandTotal,          
	ISNULL(MLQty,0) as MLQty,ISNULL(MLTax,0) as MLTax,ISNULL(AdjustmentAmt,0) as AdjustmentAmt,TotalAmount,ISNULL(ApplyMLQty,0) as ApplyMLQty,ManagerRemark,ISNULL((cmm.WeightTotalQuantity),0) as WeightTotalQuantity,        
	isnull((cmm.WeightTaxAmount),0) as WeightTaxAmount,ISNULL((WeightTax),0) as WeightTax,cmm.TaxTypeAutoId,--  TaxTypeAutoId Added By Rizwan Ahmad on 11/09/2019 02:20 AM      
	(SELECT OrderNo from OrderMaster as mo where mo.AutoId=cmm.OrderAutoId) as referenceorderno,isnull(TTM.TaxableType,'') as TaxaType ,      
	OT.OrderType as CreditType --updated by satish 11/04/2019        
	FROM CreditMemoMaster as cmm          
	INNER JOIN StatusMaster AS SM ON SM.AutoId=CMM.Status and Category='CreditMaster'           
	INNER JOIN CustomerMaster as CM on CMM.CustomerAutoId=cm.AutoId
	INNER JOIN [dbo].OrderTypeMaster AS OT ON OT.AutoId = cmm.CreditType AND OT.Type='CREDIT' 
	Left JOIN TaxTypeMaster as TTM ON cmm.TaxTypeAutoId=TTM.AutoId      
	WHERE CreditAutoId=@CreditAutoId          
                  
	SELECT ItemAutoId,PM.ProductId,PM.ProductName,UM.UnitType,CIM.ItemAutoId,CIM.ProductAutoId,CIM.UnitAutoId,CIM.RequiredQty,
	CIM.TotalPeice,CIM.AcceptedQty,CIM.AcceptedTotalQty,CIM.QtyPerUnit,CIM.UnitPrice,CIM.ManagerUnitPrice,CIM.SRP,CIM.TaxRate,
	CIM.NetAmount,isnull(CIM.UnitMLQty,0) as UnitMLQty,isnull(CIM.TotalMLTax,0) as TotalMLTax,10000 as MAXQty,
	isnull((PM.WeightOz),0) as WeightOz
	,(convert(varchar(50),(ISNULL(QtyReturn_Fresh,0)))+' '+(select UnitType from UnitMaster where AutoId=CIM.QtyPerUnit_Fresh)) as Return_Fresh,
	(convert(varchar(50),(ISNULL(QtyReturn_Damage,0)))+' '+(select UnitType from UnitMaster where AutoId=CIM.QtyPerUnit_Damage)) as Return_Damage,
	(convert(varchar(50),(ISNULL(QtyReturn_Fresh,0)))+' '+(select UnitType from UnitMaster where AutoId=CIM.QtyPerUnit_Missing)) as Return_Missing
	FROM CreditItemMaster as CIM           
	INNER JOIN ProductMaster AS PM ON PM.AutoId=CIM.ProductAutoId          
	INNER JOIN UnitMaster AS UM ON UM.AutoId=CIM.UnitAutoId 
	
	WHERE CreditAutoId=@CreditAutoId          
              
              
	SELECT EmpType FROM EmployeeMaster WHERE AutoId=@EmpAutoId          
                     
	SELECT 'Sales Manager' as EmpType,ManagerRemark as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName,* from CreditMemoMaster as cmm                   
	left join EmployeeMaster as emp on emp.AutoId=cmm.ApprovedBy                   
	where CreditAutoId=@CreditAutoId        
        
	Declare @StateId int,@DefaultShipAdd int  --New Statement Added By Rizwan Ahmad on 11/09/2019 02:20 AM      
	Select @CustomerAutoId=CustomerAutoId FROM CreditMemoMaster WHERE CreditAutoId=@CreditAutoId      
	Select @DefaultShipAdd=DefaultShipAdd from CustomerMaster where AutoId=@CustomerAutoId      
	Select @StateId=State from ShippingAddress Where AutoId=@DefaultShipAdd          
   END          
    ELSE IF @Opcode=49          
    BEGIN          
    SELECT CompanyName,EmailAddress, [Address],[Website],[MobileNo],[FaxNo],TermsCondition,Logo FROM CompanyDetails        
         
    SELECT OM.[CreditNo] as OrderNo,CONVERT(VARCHAR(20), OM.[CreditDate], 101) AS OrderDate,               
    CM.[AutoId] As CustAutoId,CM.[CustomerId],CM.[CustomerName],CM.[Contact1] + '/' + CM.[Contact2] As Contact,CTy.CustomerType,CT.[TermsDesc],          
    BA.[Address] As BillAddr,          
    S.[StateName] AS State1,BA.[City] AS City1,BA.[Zipcode] As Zipcode1,SA.[Address] As ShipAddr,S1.[StateName] As State2,SA.[City] AS City2,          
    SA.[Zipcode] As Zipcode2,OM.[TotalAmount],ISNULL(OM.[OverallDisc],0) as OverallDisc1,ISNULL(OM.[OverallDiscAmt],0) as OverallDisc,ISNULL(OM.[TotalTax],0) as TotalTax,isnull(OM.[GrandTotal],OM.[TotalAmount]) as GrandTotal,          
    ISNULL(MLQty,0) as MLQty,isnull(MLTax,0) as MLTax,CM.ContactPersonName,ISNULL(AdjustmentAmt,0) as AdjustmentAmt,          
    CM.BusinessName,EM.[FirstName] + ' ' + EM.[LastName] As SalesPerson FROM [dbo].[CreditMemoMaster] As OM          
    INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                     
  INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]          
    INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[CustomerAutoId] = OM.[CustomerAutoId] AND BA.IsDefault=1          
    INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[CustomerAutoId] = OM.[CustomerAutoId] AND SA.IsDefault=1          
    LEFT JOIN [dbo].[CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                 
    INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]           
    INNER JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]           
    left JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.CreatedBy          
    WHERE OM.CreditAutoId=@CreditAutoId          
              
    SELECT CM.[CategoryName],CM.[CategoryId],OIM.ProductAutoId,PM.[ProductId],PM.[ProductName],PM.[ProductLocation],          
    OIM.[UnitAutoId] AS UnitAutoId,UM.[UnitType],OIM.[QtyPerUnit],OIM.[UnitPrice],          
    OIM.NetAmount AS  [NetPrice],OIM.AcceptedQty AS QtyShip,'' AS BarCode          
    FROM [dbo].[CreditItemMaster] AS OIM           
    INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]          
    INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitAutoId           
    INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId] WHERE CreditAutoId=@CreditAutoId           
    ORDER BY CM.[CategoryName] ASC,PM.ProductId asc         
    END          
 if @Opcode = 50        
 begin        
    IF EXISTS(SELECT TOP 1 CreditAutoId FROM  CreditMemoMaster where CreditAutoId = @CreditAutoId AND OrderAutoId is null)
	BEGIN

		IF EXISTS(SELECT TOP 1 CreditAutoId FROM  CreditMemoMaster where CreditAutoId = @CreditAutoId AND STATUS=3)
		BEGIN
			insert into [dbo].[ProductStockUpdateLog]([ProductId],[OledStock],[NewStock],[UserAutoId],[DateTime],ReferenceId,ActionRemark,ReferenceType)
			SELECT pm.ProductId,isnull([Stock],0),(isnull(pm.Stock,0)-ISNULL(dt.QtyReturn_Fresh*PD.Qty,0)),@EmpAutoId,GETDATE(),dt.CreditAutoId,
			'Credit Momo No -(' + om.CreditNo + ') has been cancelled by '+(Select FirstName+' '+LastName from Employeemaster where autoid=@EmpAutoId)+'.','CreditMemoMaster' 
			FROM CreditItemMaster as dt
			inner join productmaster pm on dt.[ProductAutoId] = PM.[AutoId]
			inner join CreditMemoMaster om on dt.CreditAutoId=om.CreditAutoId
			left join PackingDetails  as PD on PD.ProductAutoId=PM.AutoId and PD.UnitType=dt.QtyPerUnit_Fresh
			where dt.CreditAutoId=@CreditAutoId and isnull(dt.AcceptedTotalQty,0)>0   

			UPDATE PM SET [Stock] = isnull([Stock],0) - (isnull(dt.QtyReturn_Fresh*PD.Qty,0) ) FROM [dbo].[ProductMaster] AS PM                                                                                                   
			INNER JOIN CreditItemMaster as dt ON dt.[ProductAutoId] = PM.[AutoId] 
			left join PackingDetails  as PD on PD.ProductAutoId=PM.AutoId and PD.UnitType=dt.QtyPerUnit_Fresh
			where CreditAutoId=@CreditAutoId
				
		END

		update CreditMemoMaster set Status = 6,CancelRemark = @CancelRemarks,UpdatedBy = @EmpAutoId,UpdatedDate = GETDATE() where CreditAutoId = @CreditAutoId        
		SET @Remark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=20), '[MemoNo]', (SELECT CreditNo FROM CreditMemoMaster where CreditAutoId = @CreditAutoId))          
		INSERT INTO CreditMemoLog(CreditAutoId,ActionAutoId,EmpAutoId,Remarks,LogDate)          
		VALUES(@CreditAutoId,20,@EmpAutoId, @Remark + '<br><b>Remark:</b> ' + @CancelRemarks ,GETDATE())   
	END  
	ELSE
	BEGIN
	    declare @OrderAutoId int =(SELECT top 1 OrderAutoId FROM  CreditMemoMaster where CreditAutoId = @CreditAutoId)
		SET @isException=1          
		SET @exceptionMessage='You can not cancel this credit memo becuase this already attached in <b>'+ (select OrderNO from OrderMaster where AutoId=@OrderAutoId)+' </b>Order no.'
	END
	     
 end        
 if @Opcode = 51        
 begin        
 select * from TBL_MST_SecurityMaster where SecurityValue = @SecurityKey and SecurityType = @EmpAutoId and typeEvent = 6        
 end        
 END TRY          
 BEGIN CATCH          
   SET @isException=1          
   SET @exceptionMessage=ERROR_MESSAGE()          
 END CATCH          
END            
GO
