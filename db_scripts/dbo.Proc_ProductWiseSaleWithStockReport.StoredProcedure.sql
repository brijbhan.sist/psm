 
ALTER PROCEDURE [dbo].[Proc_ProductWiseSaleWithStockReport]          
@OpCode int=null,          
@ProductAutoId int =null,          
@Status int =null,      
@FromDate date=null,      
@ToDate date=null,      
@PageIndex INT=1,        
@PageSize INT=10,        
@RecordCount INT=null           
AS          
BEGIN              
	IF @OpCode=41          
	BEGIN          
		Select ROW_NUMBER() OVER(ORDER BY ProductId) AS RowNumber,       
		ProductId,ProductName,       
		SUM(ISNULL([Peice],0)) AS [Peice],SUM(ISNULL([Box],0)) AS [Box],SUM(ISNULL([Case],0)) AS [Case],PM.Stock as [AvailableQty] into #RESULT       
		FROM (        
		SELECT ProductAutoId,        
		CASE WHEN UnitTypeAutoId= 3 THEN (ISNULL(RequiredQty,0)) ELSE 0 END AS [Peice],        
		CASE WHEN UnitTypeAutoId= 2 THEN (ISNULL(RequiredQty,0)) ELSE 0 END AS [Box],        
		CASE WHEN UnitTypeAutoId= 1 THEN (ISNULL(RequiredQty,0)) ELSE 0 END AS [Case]        
		FROM OrderItemMaster as oim        
		INNER JOIN OrderMaster as om on om.AutoId=oim.OrderAutoId        
		WHERE  ((ISNULL(@Status,0)=0 and om.Status in (1,2,9)) or om.Status=@Status)      
		and (ISNULL(@FromDate,'')='' or ISNULL(@ToDate,'')='' or Convert(date, om.OrderDate) between @FromDate and @ToDate)     
		and (ISNULL(@ProductAutoId,0)=0 or oim.ProductAutoId=@ProductAutoId)    
		) AS T          
		INNER JOIN ProductMaster AS PM ON PM.AutoId=T.ProductAutoId         
      
		GROUP BY ProductId,ProductName,PM.Stock        
		ORDER BY ProductId      
      
		SELECT COUNT(RowNumber) AS RecordCount, @PageSize AS PageSize, @PageIndex AS PageIndex FROM #RESULT        
           
		SELECT * FROM #RESULT        
		WHERE @PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize)-1 )    
        
		 SELECT ISNULL(SUM(Peice),0) AS Peice, isnull(Sum(Box),0) as Box, isnull(Sum(AvailableQty),0) as AvailableQty,isnull(sum([Case]),0) as [Case] 
		 from  #RESULT    
	END        
	IF @OpCode=42      
	BEGIN      
		select (
		SELECT AutoId, Convert(varchar,ProductId) +' '+ ProductName as PName  FROM ProductMaster      
		WHERE ProductStatus=1 ORDER BY  PName,REPLACE(ProductName,' ','') for json path
		) as PList, 
		(
		Select AutoId,StatusType from StatusMaster Where AutoId in (1,2,9) and Category='OrderMaster'  ORDER BY  StatusType ASC    
		for json path 
		) as STList
		for json path 
	END      
END   
GO
