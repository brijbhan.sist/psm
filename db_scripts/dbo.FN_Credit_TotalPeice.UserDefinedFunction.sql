USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Credit_TotalPeice]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create FUNCTION  [dbo].[FN_Credit_TotalPeice]
(
	 @AutoId int
)
RETURNS int
AS
BEGIN
	DECLARE @TotalPeice int
	SET @TotalPeice=isnull((SELECT (AcceptedQty*QtyPerUnit) FROM CreditItemMaster WHERE ItemAutoId=@AutoId),0)
	RETURN @TotalPeice
END
GO

Alter table CreditItemMaster drop column [TotalPeice]
Drop function [FN_Credit_TotalPeice]
Alter table CreditItemMaster Add [TotalPeice]  AS ([dbo].[FN_Credit_TotalPeice]([ItemAutoId]))

 
