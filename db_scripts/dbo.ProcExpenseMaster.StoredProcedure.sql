USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcExpenseMaster]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
alter Procedure [dbo].[ProcExpenseMaster]    
@Opcode INT=NULL,     
@isException BIT OUT,    
@exceptionMessage VARCHAR(max) OUT,    
@AutoId int=null,  
@PaymetTypeAutoId int = null,    
@ExpenseDescription varchar(500)=null,    
@ExpenseDate datetime=null,    
@ExpenseAmount decimal(18,2)=0,  
@Category int=null,
@CreateBy int=null,    
@UpdateBy int=null,
@PaymentSource  int=null,
@PageIndex INT =1,          
@PageSize INT=10,          
@RecordCount INT=null,    
@FromDate datetime=null,  
@XmlCurrency xml = null,   
@ToDate datetime=null,
@SearchBy varchar(10)=null
    
AS    
BEGIN    
BEGIN TRY  
SET @isException=0    
SET @exceptionMessage='success'  
  if @Opcode = 41  
   begin   
   select AutoId, CurrencyValue,CurrencyName from CurrencyMaster order by CurrencyValue desc   
   end 
IF @OpCode=42    
	BEGIN
		select
		(
		select AutoID,PaymentMode from [dbo].[PAYMENTModeMaster] where AutoID=1
		for json path
		) as PaymentMode,
		(
		select AutoId,Type  from PaymentTypeMaster where AutoId=2   
		for json path
		)  PaymentTypeMaster,
		
		( select AutoId,CategoryName from Expensecategorymaster where status=1
		for json path
		)
		category
		for json path
	END 
 
IF(@Opcode=401)    
	BEGIN    
		SELECT ROW_NUMBER() OVER(ORDER BY AutoId desc) AS RowNumber, * INTO #Results FROM      
		(      
		SELECT em.AutoId,ExpenseDescription,Format(ExpenseDate,'MM/dd/yyyy') as ExpenseDate,ExpenseAmount,ecm.CategoryName as Category,
		CreateDate,CreateBy,em.UpdateDate,em.UpdateBy,pm.PaymentMode,ptm.Type as PaymentSource
		FROM  ExpenseMaster em  
		inner join [dbo].[PAYMENTModeMaster] pm on pm.AutoID = em.PaymentAutoId  
		left join Expensecategorymaster  ecm on ecm.AutoId=em.Category
		left join PaymentTypeMaster ptm on ptm.AutoId=em.PaymentSource 
		WHERE 

		(
		ISNULL(@category,0)=0 or category=@category )
		AND
		(
		ISNULL(@SearchBy,'0')='0'  OR 
		(@SearchBy ='=' and ExpenseAmount=@ExpenseAmount) OR 
		(@SearchBy ='>' AND ExpenseAmount>@ExpenseAmount) OR
		(@SearchBy ='<' AND ExpenseAmount<@ExpenseAmount)  OR 
		(@SearchBy ='>=' AND ExpenseAmount>=@ExpenseAmount)  OR 
		(@SearchBy ='<=' AND ExpenseAmount<=@ExpenseAmount)
		
		)     
		and (@FromDate='' or @FromDate is null or @ToDate is  null or  @ToDate='' or (Convert(date,ExpenseDate) between @FromDate and @ToDate))  
		) AS t  ORDER BY AutoId  desc  
		SELECT COUNT(AutoId) AS RecordCount, @PageSize AS PageSize, @PageIndex AS PageIndex FROM #Results      
          
		SELECT *  FROM #Results      
		WHERE (@PageSize=0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))     
		SELECT @category
	END   
	
IF(@Opcode=402)    
	BEGIN    
		SELECT em.AutoId,ExpenseDescription, Format(ExpenseDate,'MM/dd/yyyy') as ExpenseDate,ExpenseAmount,Category,CreateDate     
		,CreateBy,UpdateDate,UpdateBy,PaymentAutoId as PaymentMode,PaymentSource,Category FROM  ExpenseMaster em WHERE em.AutoId=@AutoId   
		
		SELECT ecm.AutoId,ECM.CurrencyName,ECM.CurrencyValue,TotalCount,isNull(TotalAmount,0.0) AS TotalAmount FROM  CurrencyMaster AS ECM
		LEFT JOIN ExpensiveCurrency EC ON EC.CurrencyAutoId=ECM.AutoId AND ExpenseAutoId=@AutoId
		WHERE ECM.Status=1
	END    
IF(@Opcode=101)    
	BEGIN    
	begin try  
	begin tran  
				INSERT INTO ExpenseMaster (ExpenseDescription,ExpenseDate,ExpenseAmount,CreateDate,CreateBy,PaymentAutoId,PaymentSource,Category)    
				values(@ExpenseDescription,@ExpenseDate,@ExpenseAmount,GETDATE(),@CreateBy,@PaymetTypeAutoId,@PaymentSource,@Category)  
				set @AutoId =  SCOPE_IDENTITY() 
				if @PaymentSource=2 
				BEGIN
				set @Category=(select AutoId from Pattycashcategorymaster Where CategoryName='Expense')
				insert into PattyCashLogMaster(TransactionType,TransactionAmount,Remark,AutoIdCreatedBy,CreatedDate,AutoIdUpdatedBy,UpdatedDate,TransactionDate,Category,ReferenceId)
				values('CR',@ExpenseAmount,@ExpenseDescription,@CreateBy,GETDATE(),@CreateBy,GETDATE(),GETDATE(),@Category,@AutoId)
				END	    
		
				insert into ExpensiveCurrency(CurrencyAutoId,TotalCount,TotalAmount,ExpenseAutoId,CurrencyValue)   
				select tr.td.value('AutoId[1]','int') as Bill,          
				tr.td.value('Count[1]','int') as TotalCount,          
				tr.td.value('TotalAmount[1]','decimal(10,2)') as TotalAmount,   
				@AutoId,       
				(select top 1 CurrencyValue from CurrencyMaster where AutoId = tr.td.value('AutoId[1]','int'))  
				from  @XmlCurrency.nodes('/XmlExpensivecurrency') tr(td)  



	commit transaction  
	end try  
	begin catch  
	rollback tran                                   
	SET @isException=1                                                                                                            
	SET @exceptionMessage='Oops! Something went wrong.Please try later.'     
	end catch  
	END      
IF(@Opcode=301)    
	BEGIN    
		DELETE FROM ExpenseMaster Where Autoid=@AutoId    
		DELETE FROM PattyCashLogMaster Where ReferenceId=@AutoId and (Category=(select t.autoid from Pattycashcategorymaster as t where CategoryName='Expense')) 
		
	END    
IF(@Opcode=501)    
	BEGIN    
	begin try  
	begin tran  
				Update ExpenseMaster    
				SET ExpenseDescription=@ExpenseDescription,ExpenseDate=@ExpenseDate,ExpenseAmount=@ExpenseAmount,PaymentSource=@PaymentSource,  
				UpdateBy=@UpdateBy,UpdateDate=GetDate(),PaymentAutoId = @PaymetTypeAutoId,Category=@Category Where Autoid=@AutoId   
     
				if @PaymentSource=2 
				BEGIN
				set @Category=(select AutoId from Pattycashcategorymaster Where CategoryName='Expense')
				update PattyCashLogMaster set TransactionType='CR',TransactionAmount=@ExpenseAmount, Remark=@ExpenseDescription,AutoIdUpdatedBy=@UpdateBy,
				UpdatedDate=GETDATE(),TransactionDate=GETDATE(),Category=@Category where ReferenceId=@AutoId and Category=(select t.autoid from Pattycashcategorymaster as t where CategoryName='Expense')
				END	 
				
				delete ExpensiveCurrency where ExpenseAutoId = @AutoId  
				insert into ExpensiveCurrency(CurrencyAutoId,TotalCount,TotalAmount,ExpenseAutoId,CurrencyValue)   
				select tr.td.value('AutoId[1]','int') as Bill,          
					tr.td.value('Count[1]','int') as TotalCount,          
					tr.td.value('TotalAmount[1]','decimal(10,2)') as TotalAmount,   
				@AutoId,       
				(select top 1 CurrencyValue from CurrencyMaster where AutoId = tr.td.value('AutoId[1]','int'))  
				from  @XmlCurrency.nodes('/XmlExpensivecurrency') tr(td)  
	commit transaction   
	end try  
	begin catch  
	rollback tran  
	SET @isException=1                                                                                                            
	SET @exceptionMessage='Oops! Something went wrong.Please try later.'    
	end catch  
	END    
END TRY          
BEGIN CATCH          
SET @isException=1          
SET @exceptionMessage='Oops! Something went wrong.Please try later.'             
END CATCH    
END
GO
