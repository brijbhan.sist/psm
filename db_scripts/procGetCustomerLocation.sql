ALTER Procedure [dbo].[procGetCustomerLocation]
@Opcode int=null,
@SalesPerson int=null,
@Status int=null, 
@isException bit out,
@exceptionMessage varchar(max) out
AS
BEGIN
	Set @isException=0
	Set @exceptionMessage='Success'
	IF @Opcode=41
	BEGIN
		select CustomerId,CustomerName,ba.SA_Lat,ba.SA_Long,emp.FirstName+' '+emp.LastName as Name,
		ISNULL(Format(LastOrderDate,'MM/dd/yyyy'),'N/A') as LastOrderDate,
		ISNULL((
			select top 1 rm.RouteName from RouteLog as rl
			inner join RouteMaster as rm on rm.AutoId=rl.RouteAutoId
			where rl.CustomerAutoId=cm.autoid
			order by rl.CreatedOn
		),'N/A') as RouteName
		from CustomerMaster as cm
		inner join ShippingAddress as ba on cm.DefaultShipAdd=ba.AutoId and cm.AutoId=ba.CustomerAutoId and SA_Lat is not null
		inner join EmployeeMaster as emp on emp.AutoId=cm.SalesPersonAutoId
		where (@Status IS NULL OR @Status=2 OR cm.Status = @Status)
		AND (@SalesPerson IS NULL OR @SalesPerson=0 OR SalesPersonAutoId = @SalesPerson)
		for json path

	END
	ELSE IF @Opcode=42
	BEGIN
	     select  (
		select AutoId,FirstName+' '+LastName as Name from EmployeeMaster where EmpType=2 AND Status=1
		order by Name
		for JSON path
		) as SalesPerson,
		(
		select optimoStart_Lat,optimoStart_Long from CompanyDetails 
		for json path
		) as CompanyDetails
		for json path
	END
END