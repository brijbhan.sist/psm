USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcOrderListAccount]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
   
CREATE PROCEDURE [dbo].[ProcOrderListAccount]  
@Opcode INT=NULL,  
@OrderAutoId INT=NULL,  
@OrderNo VARCHAR(15)=NULL,  
@Todate DATETIME=NULL,  
@Fromdate DATETIME=NULL,  
@PageIndex INT = 1,  
@PageSize INT = 10,  
@RecordCount INT =null,  
@OrderStatus INT =null,  
@CustomerAutoId int =null,  
@isException bit out,  
@exceptionMessage varchar(max) out  
AS  
BEGIN  
 BEGIN TRY  
 Set @isException=0  
 Set @exceptionMessage='Success'  
 If @Opcode=41  
 BEGIN  
   SELECT ROW_NUMBER() OVER(ORDER BY [OrderNo] desc) AS RowNumber, * INTO #Results from  
   (     
   SELECT OM.[AutoId],OM.OrderNo,CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,CM.[CustomerName] AS CustomerName,  
   SM.StatusType AS Status,OM.[Status] As StatusCode,OM.[GrandTotal],OM.[DrvRemarks],  
   (SELECT COUNT(distinct [ProductAutoId]) from [dbo].[OrderItemMaster] WHERE OrderAutoId=OM.AutoId) AS NoOfItems   
   FROM [dbo].[OrderMaster] As OM  
   INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.AutoId = OM.CustomerAutoId   
   INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.[Status] AND SM.Category = 'OrderMaster'             
   WHERE (@OrderNo is null or @OrderNo ='' or OrderNo like '%' + @OrderNo + '%')  
   and (@FromDate is null or @FromDate = '' or @Todate is null or @Todate = '' or ([OrderDate] between @FromDate and @Todate))      
   and OM.[Status] not in (6,11) and (@OrderStatus is null or @OrderStatus=0 or OM.[Status]=@OrderStatus )   
   and (@CustomerAutoId is null or @CustomerAutoId=0 or OM.CustomerAutoId=@CustomerAutoId )   
   )as t order by [OrderNo]  
  
   SELECT COUNT(*) as RecordCount,@PageSize as PageSize,@PageIndex as PageIndex FROM #Results  
   SELECT * FROM #Results  
   WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1       
 END  
 END TRY  
  BEGIN CATCH  
    Set @isException=1  
    Set @exceptionMessage=ERROR_MESSAGE()  
  END CATCH  
END  
  
  
  
  
GO
