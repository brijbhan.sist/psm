drop table Expensecategorymaster 
CREATE TABLE Expensecategorymaster(
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[CategoryId] [varchar](12) NULL,
	[CategoryName] [varchar](50) NULL,
	[Description] [varchar](250) NULL,
	[Status] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdateBy] [int] NULL,
	[UpdateDate] [datetime] NULL,

	)


insert into [dbo].[SequenceCodeGeneratorMaster](SequenceCode,PreSample,PostSample,currentSequence,Description)
values('ExpensecategoryId','PCat','0000',1,'ID for Generated Expense Category')