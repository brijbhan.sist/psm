  
Alter Proc [dbo].[Product_Report_By_Status_SalesRep]  
@Opcode int=null,   
@fromdate date=null,  
@todate date=null,
@salespersonautoid varchar(100)=null,
@CustomerAutoId int =null,
@productid varchar(50)=null, 
@isException bit out,    
@PageIndex INT=1,      
@PageSize INT=10,      
@exceptionMessage varchar(max) out  
as   
begin  
BEGIN TRY      
  Set @isException=0      
  Set @exceptionMessage='Success'      
  IF @Opcode=41      
  BEGIN                                                                         
		select em.FirstName[SalesRep],cm.AutoId,cm.CustomerName,pm.ProductId,pm.ProductName,sm.StatusType,sum(oim.TotalPieces)[Orders] 
		into  #t1 from OrderMaster as om 
		inner join OrderItemMaster as oim on oim.OrderAutoId = om.AutoId
		inner join ProductMaster as pm on pm.AutoId = oim.ProductAutoId
		inner join EmployeeMaster as em on em.AutoId = om.SalesPersonAutoId
		inner join CustomerMaster cm on cm.AutoId = om.CustomerAutoId
		inner join StatusMaster as sm on sm.AutoId = om.Status  and sm.Category = 'OrderMaster'
		where convert(date,om.OrderDate) between @fromdate and @todate 
		and pm.ProductId in (@productid) 
		and (@CustomerAutoId is null or @CustomerAutoId=0 or om.CustomerAutoId=@CustomerAutoId)
		and (om.Status not in (8,11))
		and((ISNULL(@salespersonautoid,'0')='0,') OR @salespersonautoid='0,' OR om.SalesPersonAutoId  in (select * from dbo.fnSplitString(@salespersonautoid,','))) 
		group by  em.FirstName,pm.ProductId,pm.ProductName,sm.StatusType,cm.CustomerName,cm.AutoId

		UNION 

		select em.FirstName[SalesRep],cm.AutoId,cm.CustomerName,pm.ProductId,pm.ProductName ,sm.StatusType,sum(oim.QtyDel)[Orders] 
		from OrderMaster as om 
		inner join Delivered_Order_Items as oim on oim.OrderAutoId = om.AutoId
		inner join ProductMaster as pm on pm.AutoId = oim.ProductAutoId
		inner join EmployeeMaster as em on em.AutoId = om.SalesPersonAutoId
		inner join CustomerMaster cm on cm.AutoId = om.CustomerAutoId
		inner join StatusMaster as sm on sm.AutoId = om.Status  and sm.Category = 'OrderMaster'
		where convert(date,om.OrderDate) between @fromdate and @todate 
		and pm.ProductId in (@productid)
		and (@CustomerAutoId is null or @CustomerAutoId=0 or om.CustomerAutoId=@CustomerAutoId)
		and (om.Status in   (11) )
		and((ISNULL(@salespersonautoid,'0')='0,') OR @salespersonautoid='0,' OR om.SalesPersonAutoId  in (select * from dbo.fnSplitString(@salespersonautoid,','))) 
		group by  em.FirstName,pm.ProductId,pm.ProductName,sm.StatusType ,cm.CustomerName,cm.AutoId

		select [SalesRep],CustomerName,convert(varchar(20),ProductId)ProductId,ProductName,
		isnull([New],0)[New],
		isnull([Processed],0)[Processed],	isnull([Add-On],0)[Add-On],
		isnull([Packed],0)[Packed],	isnull([Add-On-Packed],0)[Add-On-Packed],
		isnull([Ready To Ship],0)[ReadyToShip]
		,isnull([Shipped],0)[Shipped]	,isnull([Delivered],0)[Delivered]
		,isnull([Close],0)[Close]
		,isnull([Undelivered],0)[Undelivered]	,isnull([Cancelled],0)[Cancelled]
		into #t2 from #t1
		PIVOT
		(
		MAX([Orders]) 
		FOR [StatusType] 
		IN (
		[New],[Processed],[Add-On],[Packed],[Add-On-Packed],[Ready To Ship]
		,[Shipped],[Delivered],[Close],[Undelivered],[Cancelled]
		)
		) AS P

		select [SalesRep],CustomerName,ProductId,ProductName ,
		[New],[Processed],[Add-On],[Packed],[Add-On-Packed],[ReadyToShip],[Shipped],[Delivered],[Close]
			 
		,([New]+[Processed]+[Add-On]+[Packed]+[Add-On-Packed]+[ReadyToShip]+[Shipped]+[Delivered]+[Close])[Total]
		into #t3 from #t2
		order by ([New]+[Processed]+[Add-On]+[Packed]+[Add-On-Packed]+[ReadyToShip]+[Shipped]+[Delivered]+[Close]) desc


		SELECT ROW_NUMBER() OVER(ORDER BY ProductId  DESC                                                                                                                                           
		) AS RowNumber, * INTO #Results from(
		select * from #t3
		) as t
		order by t.ProductId desc


		SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize=0 then COUNT(RowNumber) else @PageSize 
		end AS PageSize, @PageIndex AS PageIndex FROM #Results 

		  SELECT * FROM #Results                                             
  WHERE @PageSize=0 or  RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1 

		drop table #t1,#t2,#t3             
		
 END  
 
 IF @OpCode=42      
	BEGIN      
		select (
		SELECT AutoId,ProductId, Convert(varchar,ProductId) +' '+ ProductName as PName  FROM ProductMaster      
		WHERE ProductStatus=1 ORDER BY  PName,REPLACE(ProductName,' ','') for json path
		) as PList, 
		(
		Select EM.AutoId,EM.[FirstName] + ' ' + Em.[LastName] AS SalesPersonName from EmployeeMaster EM  where EM.Status=1 and EmpType=2

		order  by SalesPersonName
		for json path 
		) as SalesPerson,
		(
		SELECT AutoId AS CUID,CustomerId + ' ' + CustomerName as CUN FROM CustomerMaster ORDER BY CUN ASC
		for json path
		) as Customer
		for json path 
	END   
 END TRY      
 BEGIN CATCH      
		Set @isException=1      
		Set @exceptionMessage=ERROR_MESSAGE()      
 END CATCH       
end  
GO
