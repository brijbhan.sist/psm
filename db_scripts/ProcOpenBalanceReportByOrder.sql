Alter  PROCEDURE ProcOpenBalanceReportByOrder                                                                
@Opcode INT=NULL,                                                                    
@CustomerAutoId INT=NULL,                                                                     
@CustomerType int=null,                                                                                                                          
@EmpAutoId  INT=NULL,                                                                                                 
@SalesPersonAutoId int =null,  
@SalesPerson  VARCHAR(500)=NULL,             
@FromDate date =NULL,                                                                    
@ToDate date =NULL,   
@SearchBy INT=NULL,
@ShippingType varchar(50)=null,
@PageIndex INT=1,                                                                    
@PageSize INT=10,        
@SortBy int=null,
@ordering varchar(10)=null,
@RecordCount INT=null,                                                                    
@isException bit out,                                                                    
@exceptionMessage varchar(max) out                                                                    
AS                                                                    
BEGIN                                                                     
 BEGIN TRY                                                                    
  Set @isException=0                                                                    
  Set @exceptionMessage='Success'                                                                    
                                                                                                                       
 IF @Opcode=41                                                                 
  BEGIN                                                                    
	  select 
	  (SELECT AutoId  as CId,CustomerType as Ct from CustomerType order by CustomerType ASC
	  for json path
	  ) as CType,
	  (
	  Select em.AutoId as SID ,(em.FirstName + ' ' + em.LastName) as SP from EmployeeMaster em                                           
	  inner join EmployeeTypeMaster etm on etm.AutoId = em.EmpType                                          
	  where EmpType = 2 and Status=1 order by SP ASC 
	  for json path
	  ) as SalesList,
	  (
	  SELECT AutoId,ShippingType FROM ShippingType where Shippingstatus=1 order by   ShippingType  
	  for json path
	  ) as ShippingType
	   for JSON path
 END                                                                  
  ELSE IF @Opcode=42                                                               
  BEGIN   
  SELECT AutoId AS CuId,CustomerId+' '+CustomerName as CUN FROM CustomerMaster   WHERE            
  status=1 and             
   (CustomerType=@CustomerType                                       
  OR ISNULL(@CustomerType,0)=0) and (SalesPersonAutoId=@SalesPersonAutoId or ISNULL(@SalesPersonAutoId,0)=0)
  order by replace(CustomerId+' '+CustomerName,' ','') ASC     
   for json path
   
  END                                                

ELSE If @Opcode=43                                                               
BEGIN  
  SELECT ROW_NUMBER() OVER(ORDER BY  
	CASE when @SortBy=1 and @ordering='ASC'   
	THEN SalesPerson  end ASC,
	CASE when @SortBy=1 and @ordering='DESC'   
	THEN SalesPerson  end DESC,
	CASE when @SortBy=2 and @ordering='ASC'   
	THEN CustomerName  end ASC,
	CASE when @SortBy=2 and @ordering='DESC'   
	THEN CustomerName  end DESC,
	CASE when @SortBy=3 and @ordering='ASC'   
	THEN OrderNo  end ASC,
	CASE when @SortBy=3 and @ordering='DESC'   
	THEN OrderNo  end DESC,
	CASE when @SortBy=4 and @ordering='ASC'   
	THEN AmtDue  end ASC,
	CASE when @SortBy=4 and @ordering='DESC'   
    THEN AmtDue  end DESC
	) AS RowNumber, * INTO #Results1 FROM                                                                    
  (                                             
   select CustomerId,CustomerName ,CONVERT(varchar(10),OrderDate,101) as OrderDate,OrderNo,do.PayableAmount as TotalOrderAmount,                                                                    
   do.AmtPaid as TotalPaid,(isnull(do.PayableAmount,0.00)-isnull(do.AmtPaid,0.00)) as AmtDue,st.ShippingType,
   em.FirstName+' '+Isnull(em.LastName,'') as SalesPerson from OrderMaster as om                                                                    
   inner join DeliveredOrders as do on om.AutoId=do.OrderAutoId  and ISNULL(do.dueOrderStatus,0)=0                                           
   inner join  CustomerMaster as cm on cm.AutoId=om.CustomerAutoId 
   inner join ShippingType as st on st.AutoId=om.ShippingType
   Inner join EmployeeMaster as em on em.AutoId=cm.SalesPersonAutoId
   where                                  
   om.Status=11 and                                                               
   (isnull(do.PayableAmount,0.00)-isnull(do.AmtPaid,0.00))> 0                                                   
   AND (ISNULL(@CustomerAutoId,0)=0 or cm.AutoId=@CustomerAutoId)                                             
   AND(ISNULL(@CustomerType,0)=0 or cm.CustomerType=@CustomerType)                                    
   and (isnull(@SalesPersonAutoId,0)=0 or cm.SalesPersonAutoId=@SalesPersonAutoId)                                                                  
   and (@FromDate is null or @ToDate is null or (CONVERT(date,OrderDate)                                    
   between convert(date,@FromDate) and CONVERT(date,@ToDate)))  
   and (ISNULL(@ShippingType,'0')='0' or (@ShippingType = '0,') or OM.ShippingType in (select * from dbo.fnSplitString(@ShippingType,','))) 
  )  AS t ORDER BY 
	CASE when @SortBy=1 and @ordering='ASC'   
	THEN SalesPerson  end ASC,
	CASE when @SortBy=1 and @ordering='DESC'   
	THEN SalesPerson  end DESC,
	CASE when @SortBy=2 and @ordering='ASC'   
	THEN CustomerName  end ASC,
	CASE when @SortBy=2 and @ordering='DESC'   
	THEN CustomerName  end DESC,
	CASE when @SortBy=3 and @ordering='ASC'   
	THEN OrderNo  end ASC,
	CASE when @SortBy=3 and @ordering='DESC'   
	THEN OrderNo  end DESC,
	CASE when @SortBy=4 and @ordering='ASC'   
	THEN AmtDue  end ASC,
	CASE when @SortBy=4 and @ordering='DESC'   
	THEN AmtDue  end DESC
                                                                        
  SELECT COUNT(CustomerName) AS RecordCount, case when @PageSize=0 then COUNT(CustomerName) else @PageSize end AS PageSize, @PageIndex AS PageIndex,Format(getdate(),'MM/dd/yyyy hh:mm tt') as PrintDate FROM #Results1                                                                     
  SELECT * FROM #Results1                                                                   
  WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))                                                                   
  SELECT ISNULL(SUM(TotalOrderAmount),0.00) AS TotalOrderAmount,ISNULL(SUM(TotalPaid),0.00) AS TotalPaid,                                                        
  ISNULL(SUM(AmtDue),0.00) AS AmtDue FROM #Results1                                                                           
  END
  
 END TRY                                                    
 BEGIN CATCH                                                                    
  Set @isException=1                                                                    
  Set @exceptionMessage=ERROR_MESSAGE()                                                                    
 END CATCH                                                                    
END 
