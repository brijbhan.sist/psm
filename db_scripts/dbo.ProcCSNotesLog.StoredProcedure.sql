USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcCSNotesLog]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
CREATE PROCEDURE [dbo].[ProcCSNotesLog]
	@Who nvarchar(25)=NULL,

	@isException bit out,
	@exceptionMessage nvarchar(500) out,
	@CSLId nvarchar(50)=Null,
	@CustId nvarchar(50)=Null,
	@Notes nvarchar(Max)=Null,
	@OrderMemo varchar(200)=null,
	@RefrenceNo nvarchar(50)=Null,
	@OrderNo nvarchar(25)=Null,
	@FromPage nvarchar(50)=Null,
	@EmpId nvarchar(50)=Null,
	@Status nvarchar(50)=Null,
	@opCode int=Null
AS
BEGIN
	BEGIN TRY	
		BEGIN TRANSACTION
			
			if @opCode=11
				begin
				
			            insert into CSNotesLog(EmpId,CustId,FromPage,RefrenceNo,OrderNo,Notes,NoteDateTime )
						   Values(@EmpId,@CustId,@FromPage,@RefrenceNo,@OrderNo,@Notes,Getdate() )
						   
				SET @isException=0
				--EXEC	[dbo].[ProcTrackUserActivityLog]
				--		@Who=@Who,
				--		@ActionCode='82',
				--		@RefType='Order/InvoiceNo',
				--		@RefId=@RefrenceNo
				end
				
				if @opCode=41 --select notes by refrence
				begin
				 Select m.OrderNo,
					Replace(strng,'@ems',char(10)+char(13)) As [OrderNote]
					From
					(
						Select distinct ST2.OrderNo, 
							(
								Select (select FirstName from UserDetailMaster where userId=empId)+' '+
								 (convert(char(10), NoteDateTime, 101)
									+ ' ' + convert(char(5), NoteDateTime, 108)+ 
									' '+Right(CONVERT(CHAR(20), NoteDateTime, 22),2))
									+'@ems'+ST1.Notes + '@ems' AS [text()]
								From dbo.CSNotesLog ST1
								Where ST1.OrderNo = ST2.OrderNo
								ORDER BY ST1.NoteDateTime desc
								For XML PATH ('')
							) strng
						From dbo.CSNotesLog ST2 where orderNo=@orderNo
					) m		
				     
				     
				    SET @isException=0
				End
				if @opCode=42 --select feedback
				begin
				   select EmpId,FromPage,RefrenceNo,Notes,CustID,NoteDateTime from CSNotesLog
				    where RefrenceNo=@RefrenceNo
				    and CustId=@CustId 
				    SET @isException=0
				End
				

		COMMIT TRAN			
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		SET @isException=1
		SET @exceptionMessage= ERROR_MESSAGE()
	END CATCH;
END





GO
