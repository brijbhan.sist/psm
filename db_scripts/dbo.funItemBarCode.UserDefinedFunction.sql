USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[funItemBarCode]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create function [dbo].[funItemBarCode](@barcode varchar(50))
returns varchar(25)
as begin
 declare @Newbarcode varchar(50)

 if(len(@barcode)=1)
	Begin
		set @Newbarcode='000000000000000'+@barcode
	End
 Else if(len(@barcode)=2)
		Begin
			set @Newbarcode='00000000000000'+@barcode
		End
 Else if(len(@barcode)=3)
		Begin
			set @Newbarcode='0000000000000'+@barcode
		End
 Else if(len(@barcode)=4)
		Begin
			set @Newbarcode='000000000000'+@barcode
		End
 Else if(len(@barcode)=5)
		Begin
			set @Newbarcode='00000000000'+@barcode
		End
 Else if(len(@barcode)=6)
		Begin
			set @Newbarcode='0000000000'+@barcode
		End
 Else if(len(@barcode)=7)
		Begin
			set @Newbarcode='000000000'+@barcode
		End
 Else if(len(@barcode)=8)
		Begin
			set @Newbarcode='00000000'+@barcode
		End
 Else if(len(@barcode)=9)
		Begin
			set @Newbarcode='0000000'+@barcode
		End
 Else if(len(@barcode)=10)
		Begin
			set @Newbarcode='000000'+@barcode
		End
 Else if(len(@barcode)=11)
		Begin
			set @Newbarcode='00000'+@barcode
		End
 Else if(len(@barcode)=12)
		Begin
			set @Newbarcode='0000'+@barcode
		End
 Else if(len(@barcode)=13)
		Begin
			set @Newbarcode='000'+@barcode
		End
 Else if(len(@barcode)=14)
		Begin
			set @Newbarcode='00'+@barcode
		End
 Else if(len(@barcode)=15)
		Begin
			set @Newbarcode='0'+@barcode
		End
 Else if(len(@barcode)=16)
		Begin
			set @Newbarcode=@barcode
		End

 
 return @Newbarcode
end
GO
