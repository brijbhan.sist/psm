USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_DeliveredTotalTax]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 

CREATE FUNCTION  [dbo].[FN_DeliveredTotalTax]
(
	 @orderAutoId int
)
RETURNS decimal(10,2)
AS
BEGIN
	DECLARE @TotalTax decimal(10,2)
	set @TotalTax=isnull((select TotalTax from OrderMaster where AutoId=@orderAutoId)	,0.00)
	RETURN @TotalTax
END

GO
