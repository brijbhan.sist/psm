Select Isnull(dio.StaticDelQty,0) as StaticDelQty,om.CustomerAutoId,dio.ProductAutoId,dio.OrderAutoId
into #Result from Delivered_Order_Items as dio
inner join OrderMaster as om on om.AutoId=dio.OrderAutoId
where  (om.Status=11 and dio.Tax=1) or (dio.Item_TotalMLTax>0 and om.MLTax>0) and StaticDelQty>0


Select * from #Result
drop table #Result






select pm.ProductId,pm.ProductName,um.UnitType+ ' ['+convert(varchar(10),pd.Qty)+' Pieces]' as Unit,SUM(convert(decimal(10,2),
(convert(decimal(10,2),Isnull(t.StaticDelQty,0))/isnull(pd.Qty,0)))) as [SoldQty] from #Result as t
inner join CustomerMaster as cm on cm.AutoId=t.CustomerAutoId
inner join ProductMaster as pm on pm.AutoId=t.ProductAutoId
inner join BrandMaster as bm on pm.BrandAutoId=bm.AutoId
inner join PackingDetails as pd on pd.ProductAutoId=pm.AutoId and pd.UnitType=pm.PackingAutoId
inner join UnitMaster as um on um.AutoId=pd.UnitType
group by pm.ProductId,pm.ProductName,um.UnitType+ ' ['+convert(varchar(10),pd.Qty)+' Pieces]' ,pd.Qty



