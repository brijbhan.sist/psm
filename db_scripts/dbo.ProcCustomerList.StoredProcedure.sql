      
alter PROCEDURE [dbo].[ProcCustomerList]                                
@Opcode INT=Null,                                
@CustomerAutoId INT=NULL,                                
@StateAutoId int = null,                                
@CityAutoId int = null,
@PriceLevelAutoId int = null, 
@CustemerTypeAutoId int = null,                                
@CustomerId VARCHAR(15)=NULL,                                
@CustomerName VARCHAR(50)=NULL,                                
@CustomerType VARCHAR(100)=NULL,                                
@Status INT=NULL,   
@OrderNo VARCHAR(50)=NULL,                                 
@EmpAutoId INT=NULL,
@EmptypeNo int=null,
@SalesPersonAutoId INT=NULL,                            
@FromDate date =NULL,                                  
@ToDate date =NULL,                               
@PageIndex INT=1,                                
@PageSize INT=10,                                
@RecordCount INT=null,                           
@CustomerList  VARCHAR(500)=NULL,                                
@isException BIT OUT,                                
@exceptionMessage VARCHAR(max) OUT                                
AS                                
BEGIN                                
 BEGIN TRY                                
 SET @isException=0                                
 SET @exceptionMessage='Success!!' 
	Declare @ChildDbLocation varchar(50),@sql nvarchar(max) 
	select @ChildDbLocation=ChildDB_Name from CompanyDetails
 IF @Opcode = 31                                   
   BEGIN  
	set @PriceLevelAutoId =(select PriceLevelAutoId from CustomerPriceLevel where CustomerAutoId=@CustomerAutoId)
	if not exists(select * from OrderMaster where CustomerAutoId=@CustomerAutoId)
	BEGIN
	if not exists(select CustomerAutoId,PriceLevelId from CustomerPriceLevel as CPL
	inner join PriceLevelMaster as PLM on PLM.AutoId=CPL.PriceLevelAutoId
	where CustomerAutoId!=@CustomerAutoId and CPL.PriceLevelAutoId=@PriceLevelAutoId)
	Begin
			BEGIN TRY
			BEGIN TRAN
						delete from BillingAddress where CustomerAutoId=@CustomerAutoId
						delete from ShippingAddress where CustomerAutoId=@CustomerAutoId
						delete from CustomerPriceLevel where CustomerAutoId=@CustomerAutoId
						delete from CustomerCreditMaster where CustomerAutoId=@CustomerAutoId
						delete from CustomerContactPerson  where CustomerAutoId=@CustomerAutoId
						delete from CustomerDocumentMaster where CustomerAutoId=@CustomerAutoId
						delete from CustomerBankDetails where CustomerAutoId=@CustomerAutoId
						delete from PriceLevelMaster where AutoId=@PriceLevelAutoId  
						delete from CustomerMaster where AutoId=@CustomerAutoId

						SET @sql='
						delete from BillingAddress where CustomerAutoId='+Convert(varchar(25),@CustomerAutoId)+'
						delete from ShippingAddress where CustomerAutoId='+Convert(varchar(25),@CustomerAutoId)+'
						delete from CustomerPriceLevel where CustomerAutoId='+Convert(varchar(25),@CustomerAutoId)+'
						delete from CustomerCreditMaster where CustomerAutoId='+Convert(varchar(25),@CustomerAutoId)+'
						delete from CustomerContactPerson  where CustomerAutoId='+Convert(varchar(25),@CustomerAutoId)+'
						delete from CustomerDocumentMaster where CustomerAutoId='+Convert(varchar(25),@CustomerAutoId)+'
						delete from CustomerBankDetails where CustomerAutoId='+Convert(varchar(25),@CustomerAutoId)+'
						delete from PriceLevelMaster where AutoId='+Convert(varchar(25),@PriceLevelAutoId)+'  
						delete from CustomerMaster where AutoId='+Convert(varchar(25),@CustomerAutoId)+'
						'
						EXEC sp_executesql @sql
			COMMIT TRAN
		END TRY
		BEGIN CATCH 
		ROLLBACK TRAN
			SET @isException=1                                
			SET @exceptionMessage=ERROR_MESSAGE()   
		END CATCH
	END
	else
	Begin
		BEGIN TRY
			BEGIN TRAN
					delete from BillingAddress where CustomerAutoId=@CustomerAutoId
					delete from ShippingAddress where CustomerAutoId=@CustomerAutoId
					delete from CustomerPriceLevel where CustomerAutoId=@CustomerAutoId
					delete from CustomerContactPerson  where CustomerAutoId=@CustomerAutoId
					delete from CustomerDocumentMaster where CustomerAutoId=@CustomerAutoId
					delete from CustomerCreditMaster where CustomerAutoId=@CustomerAutoId
					delete from CustomerBankDetails where CustomerAutoId=@CustomerAutoId
					delete from CustomerMaster where AutoId=@CustomerAutoId

					SET @sql='
					delete from BillingAddress where CustomerAutoId='+Convert(varchar(25),@CustomerAutoId)+'
					delete from ShippingAddress where CustomerAutoId='+Convert(varchar(25),@CustomerAutoId)+'
					delete from CustomerPriceLevel where CustomerAutoId='+Convert(varchar(25),@CustomerAutoId)+'
					delete from CustomerCreditMaster where CustomerAutoId='+Convert(varchar(25),@CustomerAutoId)+'
					delete from CustomerContactPerson  where CustomerAutoId='+Convert(varchar(25),@CustomerAutoId)+'
					delete from CustomerDocumentMaster where CustomerAutoId='+Convert(varchar(25),@CustomerAutoId)+'
					delete from CustomerBankDetails where CustomerAutoId='+Convert(varchar(25),@CustomerAutoId)+'
					delete from CustomerMaster where AutoId='+Convert(varchar(25),@CustomerAutoId)+'
					'
					EXEC sp_executesql @sql
		COMMIT TRAN
		END TRY
		BEGIN CATCH 
		ROLLBACK TRAN
			SET @isException=1                                
			SET @exceptionMessage='1'+ERROR_MESSAGE()   
		END CATCH
	END	
	END
	else
	BEGIN
		SET @isException=1                                
		SET @exceptionMessage='customer can not be deleted'    
	END
   END
  IF @Opcode = 41                                   
   BEGIN                                     
    SELECT ROW_NUMBER() OVER(ORDER BY [CustomerName]) AS RowNumber, * INTO #Results FROM                                
    (                                
    SELECT [CustomerId],[CustomerName],CM.[Email],CM.[AltEmail],CM.[Contact1],CM.[Contact2],PLM.[PriceLevelName],                                
    EM.FirstName + ' ' + EM.LastName As SalesPerson,CM.[Status],SM.[StatusType],[ContactPersonName]                                
    FROM [dbo].[CustomerMaster] AS CM                                      
    INNER JOIN [dbo].[EmployeeMaster] AS EM ON EM.AutoId = CM.[SalesPersonAutoId]                                
    LEFT JOIN [dbo].[CustomerPriceLevel] AS CPL ON CPL.[CustomerAutoId] = CM.[AutoId]                                
    LEFT JOIN [dbo].[PriceLevelMaster] AS PLM ON PLM.[AutoId] = CPL.[PriceLevelAutoId]                                
    INNER JOIN [dbo].[StatusMaster] AS SM ON SM.[AutoId] = CM.[Status] AND SM.[Category] IS NULL                                
    WHERE (@CustomerId IS NULL OR @CustomerId='' OR CM.[CustomerId] LIKE '%' + @CustomerId + '%')                                 
      AND (@CustomerName IS NULL OR @CustomerName='' OR CM.CustomerName LIKE '%' + @CustomerName + '%')                                 
      AND (@SalesPersonAutoId IS NULL OR @SalesPersonAutoId=0 OR CM.[SalesPersonAutoId]=@SalesPersonAutoId)                                      
                                            
    ) AS t ORDER BY [CustomerName]                                
                                
    SELECT COUNT([CustomerId]) AS RecordCount, @PageSize AS PageSize, @PageIndex AS PageIndex FROM #Results                                
                                    
    SELECT * FROM #Results                                
    WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1      
                                      
   END                                
  ELSE IF @Opcode = 42                                
   BEGIN                                
    SELECT [CustomerId],[CustomerName],[CustomerType],[Email],[AltEmail],[Contact1],[Contact2],[Status],[SalesPersonAutoId],BA.[AutoId] AS BillAutoId,BA.[Address] As BillAddress,                                
    BA.[State] AS State1,BA.[City] As City1,BA.[Zipcode] As Zipcode1,SA.[AutoId] AS ShipAutoId,SA.[Address] As ShipAddress,SA.[State] As State2,SA.[City] AS City2,                                
    SA.[Zipcode] As Zipcode2,CPL.[PriceLevelAutoId],[Terms],[MobileNo],[FaxNo],[TaxId],[ContactPersonName] FROM [dbo].[CustomerMaster] As CM       
    INNER JOIN [dbo].[BillingAddress] AS BA ON BA.AutoId=CM.DefaultBillAdd                                
    INNER JOIN [dbo].[ShippingAddress] AS SA ON SA.AutoId=CM.DefaultShipAdd                                 
    LEFT JOIN [dbo].[CustomerPriceLevel] AS CPL ON CPL.[CustomerAutoId] = CM.[AutoId]                                
    WHERE [CustomerId] = @CustomerId                                    
   END                                
  ELSE IF @Opcode = 43                          
   BEGIN                                      
		SELECT [AutoId], [FirstName] + ' ' +[LastName] AS Name FROM [dbo].[EmployeeMaster] WHERE [EmpType]=2 AND [Status] = 1       
		order by   [FirstName] + ' ' +[LastName]                           
		select[AutoId],[CustomerType] from CustomerType order by [CustomerType]  ASC                    
		Select [AutoId], [StateName] as StateName from [dbo].[State] order by StateName ASC     
		select CityMaster.[AutoId], CityMaster.[CityName]+' ('+s.StateName+') ' as CityName from [dbo].[CityMaster]    
		INNER JOIN State s ON s.AutoId=CityMaster.StateId    
		order by replace(CityName,' ','') ASC                         
   END                                 
     ELSE IF @Opcode = 44                              
   BEGIN   
        select distinct CustomerAutoId into #tmpCust from OrderMaster where OrderNo like '%'+@OrderNo+'%'

		SELECT ROW_NUMBER() OVER(ORDER BY [CustomerName]) AS RowNumber, * INTO #Results44 FROM                              
		(                              
			SELECT [CustomerId],[CustomerName],CM.[Email],CM.[AltEmail],CM.[Contact1],CM.[Contact2],PLM.[PriceLevelName],                              
			CONVERT(varchar(10),LastOrderDate,101)as LastOrderDate,                              
			EM.FirstName + ' ' + EM.LastName As SalesPerson,CM.[Status],SM.[StatusType],[ContactPersonName],                              
			isnull((select creditamount from CustomerCreditMaster where CustomerAutoId=CM.AutoId ),0.0)StoreCreditAmount, cm.AutoId as custAutoId                              
			FROM [dbo].[CustomerMaster] AS CM                                   
			LEFT JOIN [dbo].[EmployeeMaster] AS EM ON EM.AutoId = CM.[SalesPersonAutoId]                              
			LEFT JOIN [dbo].[CustomerPriceLevel] AS CPL ON CPL.[CustomerAutoId] = CM.[AutoId]                              
			LEFT JOIN [dbo].[PriceLevelMaster] AS PLM ON PLM.[AutoId] = CPL.[PriceLevelAutoId]                              
			INNER JOIN [dbo].[StatusMaster] AS SM ON SM.[AutoId] = CM.[Status] AND SM.[Category] IS NULL                              
			inner join ShippingAddress as sa on sa.AutoId = cm.DefaultShipAdd                              
			inner join BillingAddress as ba on ba.AutoId = cm.DefaultBillAdd                              
			WHERE 
			(
				@CustomerId IS NULL OR @CustomerId='' OR CM.[CustomerId] LIKE '%' + @CustomerId + '%')                               
				AND (@CustomerName IS NULL OR @CustomerName='' OR CM.CustomerName LIKE '%' + @CustomerName + '%')                               
				AND (@SalesPersonAutoId IS NULL OR @SalesPersonAutoId=0 OR CM.[SalesPersonAutoId]=@SalesPersonAutoId)                              
				and (@CustemerTypeAutoId is null or @CustemerTypeAutoId=0 or cm.CustomerType=@CustemerTypeAutoId)                              
				and(@StateAutoId is null or @StateAutoId=0 or sa.State=@StateAutoId)                              
				and(@StateAutoId is null or @StateAutoId=0 or ba.State=@StateAutoId)                               
				and(@CityAutoId is null or @CityAutoId=0 or sa.SCityAutoId=@CityAutoId)                              
				and(@CityAutoId is null or @CityAutoId=0 or ba.BCityAutoId=@CityAutoId)                             
				and(@Status is null or @Status=2 or cm.Status=@Status)    
				and(@OrderNo is null or @OrderNo='' or cm.AutoId in (select CustomerAutoId from #tmpCust)
			)                              
		) AS t ORDER BY [CustomerName]                              
                              
		SELECT              
		COUNT([CustomerId]) AS RecordCount, case when @PageSize=0 then COUNT(*) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Results44                              
                                  
		SELECT * into #Results44temp FROM #Results44                              
		WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))            
                              
		--select CustomerAutoId,COUNT(OM.AutoId) as TotalOrder,SUM(om.PayableAmount) as GrandTotal,ISNULL(sum(ISNULL(do.AmtPaid,0)),0)         
		--as AmtPaid                             
		--INTO #RESULToRDER                               
		--from DeliveredOrders as do        
		--inner join OrderMaster as om    on om.AutoId=do.OrderAutoId  and Status=11   
		--inner join #Results as t    on om.CustomerAutoId=t.custAutoId                          
		--where CustomerAutoId in (select custAutoId from #Results44temp)  
		--Group by CustomerAutoId  
		
		select CustomerAutoId,COUNT(OM.AutoId) as TotalOrder,SUM(om.PayableAmount) as GrandTotal,ISNULL(sum(ISNULL(do.AmtPaid,0)),0)         
		as AmtPaid                             
		INTO #RESULToRDER                               
		from DeliveredOrders as do        
		inner join OrderMaster as om    on om.AutoId=do.OrderAutoId  and Status=11     
		inner join #Results44temp as cm    on om.CustomerAutoId=cm.custAutoId
		Group by CustomerAutoId           
                                  
		select t1.*,isnull(GrandTotal,0) AS OrderAmount,isnull(AmtPaid,0) AS PaidAmount,(isnull(GrandTotal,0)-isnull(AmtPaid,0)) AS DueAmount,        
		isnull(TotalOrder,0) as TotalOrder                               
		from #Results44temp as t1                               
		left join  #RESULToRDER as t2 on t1.custAutoId=t2.CustomerAutoId                              
	END                                
	if @Opcode = 46                                
	begin                                
	select c.[AutoId], [CityName]  +case when ISNULL(@StateAutoId,'0')='0' then +' ('+s.StateName+') ' else '' end as CityName from [dbo].[CityMaster] as c    
	INNER JOIN State s ON s.AutoId=c.StateId    
	where (ISNULL(@StateAutoId,'0')='0' OR [StateId] = @StateAutoId) order by  replace(CityName,' ','')              

    
                       
   end                                
    Else if @Opcode=47                          
   Begin                          
   select AutoId,Customerid+' '+CustomerName as CustomerName from CustomerMaster where Status=1  order by Customerid+' '+CustomerName                         
   End                          
   else if @Opcode=48                          
   Begin                          
   select ROW_NUMBER() OVER(ORDER BY OrderDate asc) AS RowNumber,* into #Results48 from  (                          
   select OrderDate,FORMAT(convert(datetime,OrderDate),'MM/dd/yyyy hh:mm tt') as TransactionDate,                          
  'New Order Id :'+OrderNo+case when CreditAmount>0 or Deductionamount>0 then '*' else '' end as TransactionType,PayableAmount as OrderAmount                          
  ,0.00 as PaidAmount from OrderMaster                           
  where                          
  Status not in (7,8)                           
                            
  and (@FromDate is null or @ToDate is null or (CONVERT(date,OrderDate)                                   
    between convert(date,@FromDate) and CONVERT(date,@ToDate)))                            
  and (ISNULL(@CustomerList,'0')='0' OR CustomerAutoId                                  
    in (select * from dbo.fnSplitString(@CustomerList,','))                                  
        )                           
  union                          
  select PaymentDate,format(PaymentDate,'MM/dd/yyyy hh:mm tt') as TransactionDate,                          
  'Payment ID :'+cpd.PaymentId+' ('+      
  (SELECT PMM.PaymentMode FROM PAYMENTModeMaster AS PMM WHERE PMM.AUTOID=cpd.PaymentMode)+' )' as TransactionType,0.00 as OrderAmount,cpd.ReceivedAmount as PaidAmount                          
  from CustomerPaymentDetails as cpd                          
  where      
  cpd.status=0 and PaymentMode!=5 and    
  (@FromDate is null or @ToDate is null or (CONVERT(date,PaymentDate)                                   
    between convert(date,@FromDate) and CONVERT(date,@ToDate)))                           
  and (ISNULL(@CustomerList,'0')='0' OR cpd.CustomerAutoId                                  
    in (select * from dbo.fnSplitString(@CustomerList,','))                                  
        )                           
                          
 ) as  t order by OrderDate                          
 SELECT * FROM #Results48                                  
   WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))                                                      
    SELECT COUNT(OrderDate) AS RecordCount, @PageSize AS PageSize, @PageIndex AS PageIndex FROM #Results48                           
 select * from #Results48                            
                       
 if(@FromDate is not null)                     
 Begin                          
 select Isnull((select sum(PayableAmount) from OrderMaster where CustomerAutoId in (select * from dbo.fnSplitString(@CustomerList,',')) and  Status not in (7,8)  and (CONVERT(date,OrderDate)<convert(date,@FromDate))),0) as OrderAmount,                    
 
     
     
       
 Isnull((select sum(ReceivedAmount) from CustomerPaymentDetails where CustomerAutoId in (select * from dbo.fnSplitString(@CustomerList,',')) and (CONVERT(date,PaymentDate)<convert(date,@FromDate))),0) as PaidAmount,                          
 ISNULL((Isnull((select sum(PayableAmount) from OrderMaster where CustomerAutoId in (select * from dbo.fnSplitString(@CustomerList,',')) and  Status not in (7,8)  and (CONVERT(date,OrderDate)<convert(date,@FromDate))),0)-                      
 isnull((select sum(ReceivedAmount) from CustomerPaymentDetails where CustomerAutoId in (select * from dbo.fnSplitString(@CustomerList,',')) and (CONVERT(date,PaymentDate)<convert(date,@FromDate))),0)),0) as BalanceForwardAmount                       
 End                          
 else                          
 Begin                  
 select 0.00 as BalanceForwardAmount                          
 End                          
                                       
   End                          
 END TRY                                
 BEGIN CATCH                                
  SET @isException=1                                
  SET @exceptionMessage=ERROR_MESSAGE()                                
 END CATCH                                
END 
GO
