USE [psmnj.a1whm.com]
GO

/****** Object:  Table [dbo].[DraftItemMaster]    Script Date: 12/26/2019 12:18:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DraftItemMaster](
	[ItemAutoId] [int] IDENTITY(1,1) NOT NULL,
	[DraftAutoId] [int] NULL,
	[ProductAutoId] [int] NULL,
	[UnitAutoId] [int] NULL,
	[ReqQty] [int] NULL,
	[QtyPerUnit] [int] NULL,
	[UnitPrice] [decimal](18, 2) NULL,
	[minprice] [decimal](18, 2) NULL,
	[SRP] [decimal](18, 2) NULL,
	[GP] [decimal](18, 2) NULL,
	[TaxRate] [decimal](18, 2) NULL,
	[NetPrice] [decimal](18, 2) NULL,
	[IsExchange] [int] NULL,
	[isFreeItem] [int] NULL,
	[UnitMLQty] [decimal](10, 2) NULL,
	[TotalMLQty] [decimal](10, 2) NULL,
	[IsTaxable] [int] NULL,
 CONSTRAINT [PK_DraftItemMaster] PRIMARY KEY CLUSTERED 
(
	[ItemAutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[DraftItemMaster] ADD  DEFAULT ((0)) FOR [isFreeItem]
GO

ALTER TABLE [dbo].[DraftItemMaster] ADD  DEFAULT ((0)) FOR [UnitMLQty]
GO

ALTER TABLE [dbo].[DraftItemMaster]  WITH NOCHECK ADD  CONSTRAINT [FK_DraftItemMaster_DraftItemMaster] FOREIGN KEY([DraftAutoId])
REFERENCES [dbo].[DraftOrderMaster] ([DraftAutoId])
GO

ALTER TABLE [dbo].[DraftItemMaster] CHECK CONSTRAINT [FK_DraftItemMaster_DraftItemMaster]
GO

ALTER TABLE [dbo].[DraftItemMaster]  WITH NOCHECK ADD  CONSTRAINT [FK_DraftItemMaster_ProductMaster] FOREIGN KEY([ProductAutoId])
REFERENCES [dbo].[ProductMaster] ([AutoId])
GO

ALTER TABLE [dbo].[DraftItemMaster] CHECK CONSTRAINT [FK_DraftItemMaster_ProductMaster]
GO

ALTER TABLE [dbo].[DraftItemMaster]  WITH NOCHECK ADD  CONSTRAINT [FK_DraftItemMaster_UnitMaster] FOREIGN KEY([UnitAutoId])
REFERENCES [dbo].[UnitMaster] ([AutoId])
GO

ALTER TABLE [dbo].[DraftItemMaster] CHECK CONSTRAINT [FK_DraftItemMaster_UnitMaster]
GO


