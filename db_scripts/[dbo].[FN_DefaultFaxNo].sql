USE [psmnj.a1whm.com]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create FUNCTION  [dbo].[FN_DefaultFaxNo] 
(   
  @AutoId int   
)  
RETURNS varchar(100)  
AS  
BEGIN  
	DECLARE @FaxNo varchar(10)
	set @FaxNo=isnull((SELECT TOP 1 Fax FROM CustomerContactPerson WHERE CustomerAutoId=@AutoId AND ISDefault=1),'')
	RETURN @FaxNo  
END 
go
