
 alter table CreditItemMaster alter column UnitPrice decimal(18,2)
 alter table CreditItemMaster alter column [SRP] decimal(18,2)
 alter table CreditItemMaster alter column [ManagerUnitPrice] decimal(18,2)
 alter table CreditItemMaster alter column [UnitMLQty] decimal(18,2)
 alter table CreditItemMaster alter column [Weight_OZ] decimal(18,2)
 alter table CreditItemMaster alter column [CostPrice] decimal(18,2)
 alter table CreditItemMaster alter column [OM_MinPrice] decimal(18,2)
 alter table CreditItemMaster alter column [OM_CostPrice] decimal(18,2)
 alter table CreditItemMaster alter column [OM_BasePrice] decimal(18,2)




 alter table CreditMemoMaster alter column [SalesAmount] decimal(18,2)
 alter table CreditMemoMaster alter column [OverallDiscAmt] decimal(18,2)
 alter table CreditMemoMaster alter column [ApplyMLQty] decimal(18,2)
 alter table CreditMemoMaster alter column [MLTaxPer] decimal(18,2)
 alter table CreditMemoMaster alter column [TaxValue] decimal(18,2)
 alter table CreditMemoMaster alter column [WeightTax] decimal(18,2)
 alter table CreditMemoMaster alter column [PaidAmount] decimal(18,2)

 