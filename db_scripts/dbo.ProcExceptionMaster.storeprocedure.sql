                 
alter PROCEDURE [dbo].[ProcExceptionMaster]                        
@Opcode INT=NULL,                                        
@PageIndex INT=1,                    
@PageSize INT=10,                    
@RecordCount INT=null,                    
@isException bit out,                    
@exceptionMessage varchar(max) out                            
AS                                    
BEGIN                       
 BEGIN TRY                    
  Set @isException=0                    
  Set @exceptionMessage='Success'                    
 IF @Opcode=41                    
  BEGIN  
	select ROW_NUMBER() over(order by [AutoId]) as RowNumber,[AutoId],[UserName],[AppVersion],[deviceID],[functionName],
	[requestContainer],[errordetails],[propertyName],convert(varchar, [errorDate], 100) as errorDate,convert(varchar, Createddate, 100) as Createddate
	into #Result from [tbl_ExceptionMaster]
                      
	SELECT * FROM #Result                  
	WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))                   
  
	SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize = 0 then COUNT(RowNumber) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Result                        
  END                           
 END TRY                    
 BEGIN CATCH                    
  Set @isException=1                    
  Set @exceptionMessage=ERROR_MESSAGE()                    
 END CATCH                                 
END 
GO
