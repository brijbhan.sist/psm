
  ALTER procedure [dbo].[RealProductEmailSending]
 @ProductId varchar(15),
 @Status int
 AS                 
 BEGIN 
   
	Declare @html varchar(Max)='',@ProductName varchar(150),@ProductImage varchar(max),@UpdateDate varchar(50),
	@UpdatedBy varchar(50),@UnitType varchar(20),@Quantity int,@CostPrice decimal(18,2),@WholesaleMinimumPrice decimal(18,2),
	@RetailMinimumPrice decimal(18,2),@BasePrice decimal(18,2),@Category varchar(50),@SubCategory varchar(50),@Free varchar(20),
	@CreatedBy varchar(50),@CreateDate varchar(50),@BrandName varchar(50),@ReOrderMark int,@Location varchar(50),@tr varchar(max),@td varchar(max)='',
	@IsApply_ML varchar(10),@MLQty decimal(18,2),@IsApply_Oz varchar(10),@WeightOz decimal(18,2),@P_SRP decimal(18,2),@P_CommCode decimal(18,2),@count int,
	@num int,@DraftStatus varchar(50),@DraftLocation varchar(50),@DraftSubject varchar(50),@query nvarchar(MAX),
	@DraftCCEmail varchar(150),@ProductLocation varchar(50)
	 
		SELECT @ProductId=ProductId,@ProductName=ProductName,@ProductImage=(SELECT top 1 ImageUrl FROM ProductImageUrl)+PM.ThumbnailImageUrl,@ReOrderMark=PM.ReOrderMark,
		@UpdateDate=Format(getdate(),'MM/dd/yyyy hh:mm tt'),@BrandName=BM.BrandName,@Category=CM.CategoryName,
		@SubCategory=SCM.SubcategoryName,@UpdatedBy=EM1.FirstName+' '+ISNULL(EM1.LastName,''),
		@CreatedBy=EM.FirstName+' '+ISNULL(EM.LastName,''),@CreateDate=Format(getdate(),'MM/dd/yyyy hh:mm tt'),
		@IsApply_ML=case when PM.IsApply_ML=1 then 'Yes' else 'No' end,@ProductLocation=PM.ProductLocation,
		@MLQty=PM.MLQty,@IsApply_Oz=case when PM.IsApply_Oz=1 then 'Yes' else 'No' end,@WeightOz=PM.WeightOz,@P_SRP=PM.P_SRP,@P_CommCode=PM.P_CommCode
		FROM ProductMaster AS PM
		INNER JOIN CategoryMaster AS CM ON PM.CategoryAutoId=CM.AutoId
		INNER JOIN SubCategoryMaster AS SCM ON SCM.AutoId=PM.SubcategoryAutoId
		INNER JOIN BrandMaster AS BM ON BM.AutoId=PM.BrandAutoId
		INNER JOIN EmployeeMaster AS EM ON EM.AutoId=PM.CreateBy
		INNER JOIN EmployeeMaster AS EM1 ON EM1.AutoId=PM.ModifiedBy
		WHERE PM.ProductId=@ProductId

		select Row_Number() over (order by PD.AutoId) as rownumber,um.UnitType,CostPrice,WHminPrice,Qty,Price,
		PD.LatestBarcode as Barcode,MinPrice as RetailMinPrice,PD.ProductAutoId,um.AutoId AS UnitId,                                                             
		case when EligibleforFree=0 then 'No' else 'Yes' end as EligibleforFree,
		(SELECT PackingAutoId FROM ProductMaster AS PM WHERE PM.AUTOID=PD.PRODUCTAUTOID 
		AND PM.PackingAutoId=PD.UnitType) AS DefPacking,PD.Location into #Result	
		from [dbo].[PackingDetails] as PD                                                                                                                 
		Inner Join UnitMaster as um on um.AutoId=PD.UnitType   
		Where PD.ProductAutoId=(Select AutoId From ProductMaster where ProductId=@ProductId)    

		select Row_Number() over (order by LM.AutoId) as rownumber, LM.Location,DPL.Status into #Result2 from RealProductLocation AS DPL
		INNER JOIN LocationMaster AS LM ON DPL.LocationId=LM.AutoId
		where  ProductAutoId=(Select AutoId From ProductMaster where ProductId=@ProductId)
	

	--Product Details----
	set @html='	
		<table class="table tableCSS" border="1" style="width:100%;border: 1px solid black;border-collapse:collapse;"> 
		<tbody>
		<tr>
			<td><b>Product Id</b></td>
			<td>'+@ProductId+'</td>
       </tr>
	   <tr>
			<td><b>Product Name</b></td>
			<td>'+@ProductName+'</td>
       </tr>
	   <tr>
			<td><b>Product Image</b></td>			
			<td><img src='+@ProductImage+' alt="" style="width:50px;height:50px" class="CToWUd"></td>
       </tr>
	   <tr>
			<td><b>Category</b></td>			
			<td>'+@Category+'</td>
       </tr>
	   <tr>
			<td><b>Sub Category</b></td>			
			<td>'+@SubCategory+'</td>
       </tr>
	    <tr>
			<td><b>Brand</b></td>			
			<td>'+@BrandName+'</td>
       </tr>
	    <tr>
			<td><b>Re Order Mark (Pieces)</b></td>			
			<td>'+Convert(varchar(50),@ReOrderMark)+'</td>
       </tr>
	    <tr>
			<td><b>Is Apply ML Quantity</b></td>
			
			<td>'+@IsApply_ML+'</td>
       </tr>
	   <tr>
			<td><b>ML Quantity</b></td>			
			<td>'+Convert(varchar(50),@MLQty)+'</td>
       </tr>
	   <tr>
			<td><b>Is Apply Weight (Oz)</b></td>			
			<td>'+@IsApply_Oz+'</td>
       </tr>
	   <tr>
			<td><b>Weight (Oz)</b></td>			
			<td>'+Convert(varchar(50),@WeightOz)+'</td>
       </tr>
	   <tr>
			<td><b>Commision Code</b></td>			
			<td>'+Convert(varchar(50),@P_CommCode)+'</td>
       </tr>
	   <tr>
			<td><b>SRP</b></td>
			
			<td>'+Convert(varchar(50),@P_SRP)+'</td>
       </tr>
	   '
	   If @Status=1
	   Begin
	   SET @html=@html+'
	    <tr>
			<td><b>Create Date & Time</b></td>
			<td>'+@CreateDate+'</td>
       </tr>
	   <tr>
			<td><b>Created By</b></td>
			<td>'+@CreatedBy+'</td>
       </tr>
	   '
	   End
	   ELSE
	   BEGIN
		  SET @html=@html+' 
		   <tr>
				<td><b>Update Date & Time</b></td>
				<td>'+@UpdateDate+'</td>
		   </tr>
		   <tr>
				<td><b>Updated By</b></td>
				<td>'+@UpdatedBy+'</td>
		   </tr>
		   '
	   END
	 SET @html=@html+' 
		</tbody>
		</table><br>'
	
	--Location-----------
		SET @count= (select count(1) from #Result2)
	SET @num=1
	while(@num<=@Count)
	BEGIN     
			select 
			@DraftLocation=Location,
			@DraftStatus=case when Status=0 then 'Inactive' else 'Active' end
			from #Result2 where rownumber = @num 
			set @tr =+'<td style=''text-align:center;''><b>'+ @DraftLocation + '</b> : '+case when @DraftStatus='Active' then '<span style="color:green">Active</span>' else '<span style="color:red">Inactive</span>' end +'</td>'						
	set @num = @num + 1
	SET @td=@td+@tr
	END	
	SET @html=@html+'<table class="table tableCSS" border="1" style="width:100%;border: 1px solid black;border-collapse:collapse;"><tbody><thead><tr>'+@td+'</tr></thead></tbody></table><br/>'
	
	
    --Packing Details----
	set @html=@html+'
		<table class="table tableCSS" border="1" style="width:100%;border: 1px solid black;border-collapse:collapse;"> 
		<thead>
		<tr>
			<td style=''text-align:center;''><b>Unit Type</b></td>
			<td style=''text-align:center;''><b>Quantity</b></td>
			<td style=''text-align:center;''><b>Cost Price</b></td>
			<td style=''text-align:center;''><b>Wholesale Minimum Price</b></td>
			<td style=''text-align:center;''><b>Retail Minimum Price</b></td>
			<td style=''text-align:center;''><b>Base Price</b></td>
			<td style=''text-align:center;''><b>Free</b></td>
			<td style=''text-align:center;''><b>Location</b></td>
       </tr>
	   </thead>
		<tbody>'
			SET @count= (select count(1) from #Result)
		SET @num=1
		while(@num<=@Count)
		BEGIN   
				select 
				@UnitType=UnitType,
				@Quantity=Qty,
				@CostPrice= CostPrice,
				@WholesaleMinimumPrice=WHminPrice,
				@RetailMinimumPrice=RetailMinPrice,
				@BasePrice=Price,
				@Free=EligibleforFree,
				@Location=Location
				from #Result where rownumber = @num 
				set @tr ='<tr>' 
								+	'<td style=''text-align:center;''>'+ @UnitType + '</td>'
								+	'<td style=''text-align:center;''>'+ convert(varchar(50),@Quantity) + '</td>'
								+	'<td style=''text-align:right;''>'+ convert(varchar(50),@CostPrice) + '</td>'
								+	'<td style=''text-align:right;''>'+ convert(varchar(50),@WholesaleMinimumPrice) + '</td>'
								+	'<td style=''text-align:right;''>'+ convert(varchar(50),@RetailMinimumPrice) + '</td>'
								+	'<td style=''text-align:right;''>'+ convert(varchar(50),@BasePrice) + '</td>'
								+	'<td style=''text-align:center;''>'+ @Free + '</td>'
								+	'<td style=''text-align:center;''>'+ @Location + '</td>'
						+ '</tr>'
		set @num = @num + 1
		SET @html=@html+@tr
		END
		SET @html=@html+'</tbody></table>'
	--EXEC dbo.spWriteToFile @DriveLocation, @html 
	IF @Status=1
	BEGIN
	   SET @DraftSubject='New Product Created'
	END
	ELSE
	BEGIN
		SET @DraftSubject='Product updated'
	END
	
	SET @query='Select @CCEmail=CCEmail from ['+@ProductLocation+'.a1whm.com].[dbo].Tbl_ReportEmailReceiver where Autoid=10'
	Exec sp_executesql @query,N'@CCEmail nvarchar(max) OUTPUT',@CCEmail=@DraftCCEmail OUTPUT 

	-------------------------------------------Code For Email---------------------------------------------------------------
	Declare  @Subject varchar(250),@FromName varchar(500),@FromEmailId varchar(500),@Port int,@SMTPServer varchar(150),
	@Password varchar(50),@SSL  int,@BCCEmailId varchar(max),@ToEmailId varchar(500),@CCEmailId varchar(max),@smtp_userName varchar(max)
	set @Subject = 'A1WHM -' +UPPER((select ProductLocation from DraftProductMaster Where ProductId=@ProductId))+' - '+@DraftSubject 
	
	SELECT @FromEmailId=FromEmail,@FromName=DisplayName,@smtp_userName=SMTP_USERNAME,@Password=SMTP_PASSWORD,
	@SMTPServer=HOST,@Port=SMTPPORT,@SSL=EnableSsl from 
	EmailServerMaster_AWS  

	set @BCCEmailId=(select BCCEmail from Tbl_ReportEmailReceiver where Autoid=9);
	set @ToEmailId=(select ToEmail from Tbl_ReportEmailReceiver where Autoid=9)+','+@DraftCCEmail
	SET @CCEmailId=(select CCEmail from Tbl_ReportEmailReceiver where Autoid=9)
    
	EXEC [Emailing_DB].[dbo].[ProcSubmitEmail] 
	@Opcode=11,
	@FromEmailId =@FromEmailId,
	@FromName = @FromName,
	@smtp_userName=@smtp_userName,
	@Password = @Password,
	@SMTPServer = @SMTPServer,
	@Port =@Port,
	@SSL =@SSL,
	@ToEmailId =@ToEmailId,
	@CCEmailId =@CCEmailId,
	@BCCEmailId =@BCCEmailId,  
	@Subject =@DraftSubject,
	@EmailBody = @html,
	@SentDate ='',
	@Status =0,
	@SourceApp ='PSM',
	@SubUrl =@DraftSubject, 
	@isException=0,
	@exceptionMessage=''  
  END