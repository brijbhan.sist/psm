            
ALTER PROCEDURE [dbo].[ProcEmployeeMaster]                                        
@Opcode int=Null,                                      
@AutoId  int=Null,      
@EmpAutoId int=Null,                                     
@EmpId VARCHAR(12)=NULL,                          
@EmpCode VARCHAR(30)=NULL,                           
@ProfileName VARCHAR(30)=NULL,                                      
@EmpType int=NULL,                                      
@FirstName VARCHAR(50)=NULL,       
@AssignDeviceAutoId VARCHAR(500)=NULL,  
@IpAddressAutoId VARCHAR(500)=NULL,
@LastName VARCHAR(50)=NULL,                                      
@Email VARCHAR(100)=NULL,                                      
@Contact VARCHAR(12)=NULL,                                      
@Address VARCHAR(200)=NULL,                                      
@State VARCHAR(50)=NULL,                                      
@City VARCHAR(50)=NULL,                                      
@Zipcode VARCHAR(50)=NULL,                                      
@Status int=null,                                      
@UserName VARCHAR(100)=NULL,                                      
@Password varchar(max)=null,                                      
@ImageURL varchar(100)=null,   
@DeliveryStartTime time(7)=NULL,
@DeliveryCloseTime time(7)=NULL,
@PageIndex INT = 1,                                      
@PageSize INT = 10,                                      
@RecordCount INT =null,                                      
@IsLoginIp INT =null,                
@IsAppLogin INT =null,                                 
@IPAddress varchar(250)=null,                                     
@isException bit out,                                      
@exceptionMessage varchar(max) out                                      
AS                                      
BEGIN                                      
                                    
 SET @isException=0                                      
 SET @exceptionMessage='Success'                                      
 DECLARE @companyId varchar(50)                                      
 IF @Opcode=11                                       
 BEGIN                               
                              
  SET @EmpId=(select dbo.SequenceCodeGenerator('EmpId'))                                    
  SET @companyId =(SELECT TOP 1 CompanyId FROM CompanyDetails)                     
  IF EXISTS(SELECT [UserName] FROM [dbo].[EmployeeMaster] WHERE EmpLoyeeCode=@EmpCode)                                       
  BEGIN                                      
  SET @isException=1                                      
  SET @exceptionMessage='Employee Code already exist.';                                      
  END                                                  
  ELSE IF EXISTS(SELECT [UserName] FROM [dbo].[EmployeeMaster] WHERE [UserName]=@UserName+'@'+@companyId)                                       
  BEGIN                                      
  SET @isException=1                                      
  SET @exceptionMessage='User Name already exist.';                                      
  END                                      
  else                                      
  BEGIN TRY                                         
    
  BEGIN TRAN                                      
    declare @deviceAutoId int                                         
    INSERT INTO [dbo].[EmployeeMaster]([EmpId],[EmpType],FirstName,LastName,Email,Contact,Address,[State],[City],[Zipcode],                                      
    Status,UserName,Password,CreatedDate,ImageURL,isApplyIP,IP_Address,EmpLoyeeCode,ProfileName,IsAppLogin,OptimotwFrom,OptimotwTo)                                      
    VALUES(@EmpId,@EmpType,@FirstName,@LastName,@Email,@Contact,@Address,@State,@City,@Zipcode,@Status, @UserName+'@'+@companyId,                                      
    EncryptByPassPhrase('WHM',@Password),GETDATE(),@ImageURL,@IsLoginIp,@IpAddress,@EmpCode,@ProfileName,@IsAppLogin,@DeliveryStartTime,@DeliveryCloseTime)      
	set @EmpAutoId=SCOPE_IDENTITY();
	if(@AssignDeviceAutoId!='')
	begin
	   if exists (select * from [dbo].[AssignDeviceMaster] where EmpAutoId=@EmpAutoId)
	   Begin
	   update [dbo].[AssignDeviceMaster] set [DeviceAutoId]=(select convert(int,splitdata) as AssignDeviceAutoId from [dbo].[fnSplitString] (@AssignDeviceAutoId,','))	 where  EmpAutoId=@EmpAutoId
	   END
	   else
	   BEGIN
		insert into [dbo].[AssignDeviceMaster]([DeviceAutoId],[EmpAutoId])  
		select convert(int,splitdata) as AssignDeviceAutoId,@EmpAutoId from [dbo].[fnSplitString] (@AssignDeviceAutoId,',')		
		set @deviceAutoId=SCOPE_IDENTITY()
		END
	end
	if(@IpAddressAutoId!='')
	BEGIN	
	insert into [dbo].[EmployeeIpAddress](IPAutoId,[EmpAutoId])  
	select convert(int,splitdata) as IPAutoId,@EmpAutoId from [dbo].[fnSplitString] (@IpAddressAutoId,',')				
	END
    UPDATE SequenceCodeGeneratorMaster SET currentSequence=currentSequence+1 WHERE SequenceCode='EmpId'   
	
	
	Exec ProcInsertData_EmployeeMaster @EmpAutoId=@EmpAutoId,@isException=null,@exceptionMessage=null,@AssignDeviceAutoId=@AssignDeviceAutoId,@Opcode=11

  COMMIT TRANSACTION                                      
  END TRY                                      
  BEGIN CATCH                                      
  ROLLBACK TRAN                                      
   SET @isException=1                                      
   SET @exceptionMessage=ERROR_MESSAGE()---'Oops! something wrong.Please try later.'               
  END CATCH                                      
 END                                       
 IF @Opcode=21                                       
 BEGIN    
  BEGIN TRY                                         
    
  BEGIN TRAN  
	SET @companyId =(SELECT TOP 1 CompanyId FROM CompanyDetails)                        
	 IF EXISTS(SELECT [UserName] FROM [dbo].[EmployeeMaster] WHERE EmpLoyeeCode=@EmpCode and AutoId != @AutoId)                                       
	 BEGIN                                      
		  SET @isException=1                 
		  SET @exceptionMessage='Employee Code already exist.';                                      
	 END                                                  
	 ELSE IF EXISTS(SELECT [UserName] FROM [dbo].[EmployeeMaster] WHERE [UserName]=@UserName + '@'+@companyId and AutoId != @AutoId)                         
	 BEGIN                                      
		  SET @isException=1                                      
		  SET @exceptionMessage='User Name already exist.';                                      
	 END                                      
	 ELSE    
   
  DECLARE @CreateType int =(select emptype from EmployeeMaster where AutoId=@EmpAutoId)
  Declare @PSW varchar(150),@LocationName varchar(100),@ProperLocation varchar(100),@EmpName varchar(150)--added on 11/30/2019 By Rizwan Ahmad      
  SELECT @PSW=CONVERT(varchar(100),DecryptByPassphrase('WHM',Password)) FROM EmployeeMaster WHERE AutoId=@AutoId        
   
  
INSERT INTO [dbo].[EmployeeMaster_Log]
           ([EmpId],[EmpType],[FirstName],[LastName],[Email]
           ,[Contact],[Address],[State],[City],[Zipcode]
           ,[Status],[UserName],[Password],[CreatedDate],[ImageURL],[IP_Address]
           ,[isApplyIP],[UpdateDate],[updateby],[EmployeeCode],[ProfileName]
           ,[IsAppLogin],[OptimotwFrom],[OptimotwTo])
			 SELECT [EmpId],[EmpType],[FirstName],[LastName],[Email]
           ,[Contact],[Address],[State],[City],[Zipcode]
           ,[Status],[UserName],CONVERT(varchar(100),DecryptByPassphrase('WHM',Password)),[CreatedDate],[ImageURL],[IP_Address]
           ,[isApplyIP],[UpdateDate],[updateby],[EmployeeCode],[ProfileName]
           ,[IsAppLogin],[OptimotwFrom],[OptimotwTo]
		   FROM EmployeeMaster WHERE AutoId=@AutoId

  UPDATE [dbo].[EmployeeMaster] SET [EmpType]=@EmpType,FirstName=@FirstName,LastName=@LastName,Email=@Email,Contact=@Contact,                                      
  [Address]=@Address,[State]=@State,[City]=@City,[Zipcode]=@Zipcode,[Status]=@Status,EmployeeCode=@EmpCode,ProfileName=@ProfileName,                                      
  UserName=@UserName+'@'+@companyId,[Password]=EncryptByPassPhrase('WHM',@Password),UpdateDate=GETDATE(),
  IP_Address=CASE WHEN @IpAddress is null then IP_Address else @IpAddress end ,
  isApplyIP=CASE WHEN @IsLoginIp is null then isApplyIP else @IsLoginIp end,updateby=@EmpAutoId,
  ImageURL=@ImageURL,
  IsAppLogin=CASE WHEN @CreateType !=1 then IsAppLogin else @IsAppLogin end   ,OptimotwFrom=@DeliveryStartTime,OptimotwTo=@DeliveryCloseTime      
  WHERE AutoId = @AutoId      --By Rizwan Ahmad on 05-09-2019       
   
    delete from [dbo].[AssignDeviceMaster] where EmpAutoId=@AutoId
	if(@AssignDeviceAutoId!='')
	begin		
		insert into [dbo].[AssignDeviceMaster]([DeviceAutoId],[EmpAutoId])  select convert(int,splitdata) as AssignDeviceAutoId,@AutoId from [dbo].[fnSplitString] (@AssignDeviceAutoId,',')
		set @deviceAutoId=SCOPE_IDENTITY()
	end
	delete from [dbo].[EmployeeIpAddress] where EmpAutoId=@AutoId
	if(@IpAddressAutoId!='')
	BEGIN
	insert into [dbo].[EmployeeIpAddress](IPAutoId,[EmpAutoId])  
	select convert(int,splitdata) as IPAutoId,@AutoId from [dbo].[fnSplitString] (@IpAddressAutoId,',')				
	END

  IF (@PSW!=@Password)      
  BEGIN      
   SELECT @LocationName=DB_NAME()                                           
   SELECT @ProperLocation='A1TS - '+ UPPER(Replace(@LocationName,'.a1whm.com',''))         
   SELECT  @EmpName=ISNULL(FirstName,'')+' '+ISNULL(LastName,'') FROM EmployeeMaster Where AutoId=@EmpAutoId      
      
   SELECT EmpId,ETM.TypeName as EmpType,ISNULL(FirstName,'')+' '+ISNULL(LastName,'') as EmployeeName,UserName,@ProperLocation as Location,      
   CONVERT(varchar(100),DecryptByPassphrase('WHM',[Password])) as Password,@EmpName as UpdateBy,GETDATE() as UpdateDate FROM EmployeeMaster as EM      
   INNER JOIN EmployeeTypeMaster ETM ON      
   EM.EmpType=ETM.AutoId WHERE EM.AutoId = @AutoId      
      
    Select EmailID as Email from EmailReceiverMaster Where Category='Developer'        
	
  END 

  	Exec ProcInsertData_EmployeeMaster @EmpAutoId=@AutoId,@AssignDeviceAutoId=@AssignDeviceAutoId,@isException=null,@exceptionMessage=null,@Opcode=21

    COMMIT TRANSACTION                                      
  END TRY                                      
  BEGIN CATCH                                      
  ROLLBACK TRAN                                      
   SET @isException=1                                      
   SET @exceptionMessage=ERROR_MESSAGE()---'Oops! something wrong.Please try later.'               
  END CATCH  
  END
IF @Opcode=22                                       
 BEGIN    
  BEGIN TRY                                         
    
  BEGIN TRAN  
  SET @companyId =(SELECT TOP 1 CompanyId FROM CompanyDetails)                        	 
  SELECT @PSW=CONVERT(varchar(100),DecryptByPassphrase('WHM',Password)) FROM EmployeeMaster WHERE AutoId=@AutoId        
   
  UPDATE [dbo].[EmployeeMaster] SET isApplyIP=@IsLoginIp WHERE AutoId = @AutoId      --By Rizwan Ahmad on 05-09-2019          
  IF (@PSW!=@Password and @Password!='')      
  BEGIN      
		UPDATE [dbo].[EmployeeMaster] SET [Password]=CASE WHEN @Password='' then Password else EncryptByPassPhrase('WHM',@Password) end,
		isApplyIP=@IsLoginIp WHERE AutoId = @AutoId  

		declare @db_name varchar(1000)=(select [ChildDB_Name] from CompanyDetails)
		if @db_name is not null
		begin
			declare @sql_query nvarchar(max)='update emp set [Password]=EncryptByPassPhrase(''WHM'',CONVERT(varchar(100),DecryptByPassphrase(''WHM'',em.[Password])))
			,isApplyIP=em.isApplyIP from ['+@db_name+'].[dbo].[EmployeeMaster] as 
			emp inner join [dbo].[EmployeeMaster] as em on em.AutoId=emp.AutoId where emp.AutoId='+convert(varchar(10),@EmpAutoId)+';'
			exec (@sql_query)  
		END
   SELECT @LocationName=DB_NAME()                                           
   SELECT @ProperLocation='A1WHM - '+ UPPER(Replace(@LocationName,'.a1whm.com',''))         
   SELECT  @EmpName=ISNULL(FirstName,'')+' '+ISNULL(LastName,'') FROM EmployeeMaster Where AutoId=@EmpAutoId      
      
   SELECT EmpId,ETM.TypeName as EmpType,ISNULL(FirstName,'')+' '+ISNULL(LastName,'') as EmployeeName,UserName,@ProperLocation as Location,      
   CONVERT(varchar(100),DecryptByPassphrase('WHM',[Password])) as Password,@EmpName as UpdateBy,GETDATE() as UpdateDate FROM EmployeeMaster as EM      
   INNER JOIN EmployeeTypeMaster ETM ON      
   EM.EmpType=ETM.AutoId WHERE EM.AutoId = @AutoId      
      
   Select EmailID as Email from EmailReceiverMaster Where Category='Developer'        
	
  END 
  COMMIT TRANSACTION                                      
  END TRY                                      
  BEGIN CATCH                                      
  ROLLBACK TRAN                                      
   SET @isException=1                                      
   SET @exceptionMessage=ERROR_MESSAGE()---'Oops! something wrong.Please try later.'               
  END CATCH  
  END  

 ELSE IF @Opcode=41                                        
 BEGIN                                      
  SELECT [AutoId],[TypeName] FROM [dbo].[EmployeeTypeMaster] where [AutoId] not in (1,12) order by [TypeName]                                    
  SELECT [AutoId],[StatusType] FROM [dbo].[StatusMaster] WHERE [Category] IS NULL                                      
  SELECT CompanyId FROM CompanyDetails    
  select AutoId,DeviceId,DeviceName from DeviceMaster where Status=1
  select AutoId,CONVERT(VARCHAR, TimeFrom, 100) as TimeFromLabel,CONVERT(VARCHAR, TimeTo, 100) as TimeToLabel,CONVERT(VARCHAR(8),TimeFrom,108)   as TimeFrom ,
  CONVERT(VARCHAR(8),TimeTo,108) as TimeTo from OptimoTimeWindow order by AutoId asc
  select AutoId,NameofLocation from IpMaster
 END                                       
 ELSE IF @Opcode=42                                      
 BEGIN                                      
   SELECT ROW_NUMBER() OVER(ORDER BY FirstName  asc, EmpType asc,LoginDate desc) AS RowNumber, * INTO #Results from                                      
   (                                       
   SELECT Em.AutoId,EmployeeCode,EM.UserName as UserName ,CONVERT(varchar(100),DecryptByPassphrase('WHM',[Password])) AS [Password],ProfileName,ETM.[TypeName] AS EmpType,[FirstName],[LastName],[Email],[Contact],[Address],[State],[City],[Zipcode],                                       
   SM.StatusType As Status,[ImageURL],IsAppLogin,(select top 1 FORMAT(ipa.LoginDate,'MM/dd/yyyy hh:mm tt') from ipaddress ipa where         
   ipa.EmpAutoId = em.AutoId order by ipa.LoginDate desc) as LoginDate,DeviceId=STUFF  
(  
     (  
       SELECT DISTINCT ', ' + CAST(DeviceId AS VARCHAR(MAX))  
       FROM AssignDeviceMaster t2   
	   inner join DeviceMaster as dm on dm.AutoId=t2.DeviceAutoId
       WHERE t2.EmpAutoId =EM.AutoId
       FOR XML PATH('')  
     ),1,1,'' 
	)  FROM [dbo].[EmployeeMaster] AS EM                     
    --Column ISAppLogin add by Rizwan Ahmad on 05-09-2019                                 
   INNER JOIN [dbo].[EmployeeTypeMaster] AS ETM ON ETM.[AutoId]=EM.EmpType and em.EmpType not in (1,12)                                      
   INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId=EM.Status and SM.Category IS NULL   
   --left join DeviceMaster as dm on dm.AutoId=em.AssignDeviceAutoId
   WHERE (@EmpType=0 or [EmpType]=@EmpType)and(@EmpCode is null or @EmpCode  ='' or [EmployeeCode] like '%'+@EmpCode+'%')                                      
   and(@FirstName is null or @FirstName='' or [FirstName] like '%'+@FirstName+'%' or @LastName is null or @LastName='' or [LastName] like '%'+@LastName+'%'  or (FirstName+' '+LastName)like '%'+@FirstName+'%')                                      
   and(@Email is null or @Email='' or [Email] like '%'+@Email+'%')and(@Status=2 or em.[Status]=@Status)             
             
    group by Em.AutoId,Password,UserName,EmployeeCode,ProfileName,ETM.[TypeName], [FirstName],[LastName],[Email],[Contact],[Address],[State],[City],[Zipcode],                                       
   SM.StatusType,[ImageURL],IsAppLogin                                   
   )as t order by FirstName  asc, EmpType asc, LoginDate desc                                     
                                      
   SELECT  case when isnull(@PageSize,0)=0 then  0  else COUNT(*) end as  RecordCount ,@PageSize as PageSize,@PageIndex as PageIndex FROM #Results                                      
   SELECT * FROM #Results                                      
   WHERE ISNULL(@PageSize,0)=0 or RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1                                      
 END                                      
 ELSE IF @Opcode=43                                      
 BEGIN                                      
   SELECT AutoId,EmployeeCode,ProfileName,[EmpType],[FirstName],[LastName],[Email],[Contact],[Address],[State],[City],[Zipcode],[Status],[UserName],
   CONVERT(VARCHAR(8),OptimotwFrom,108)   as OptimotwFrom,CONVERT(VARCHAR(8),OptimotwTo,108)   as OptimotwTo,
   CONVERT(varchar(100),DecryptByPassphrase('WHM',[Password])) AS [Password],[ImageURL],[isApplyIP],[IP_Address],IsAppLogin,AssignDeviceAutoId=STUFF  
(  
     (  
       SELECT DISTINCT ',' + CAST(dm.AutoId AS VARCHAR(MAX))  
       FROM AssignDeviceMaster t2   
	   inner join DeviceMaster as dm on dm.AutoId=t2.DeviceAutoId
       WHERE t2.EmpAutoId =t1.AutoId
       FOR XML PATH('')  
     ),1,1,'' 
	),
	IpAddressAutoId=STUFF  
(  
     (  
       SELECT DISTINCT ',' + CAST(IM.AutoId AS VARCHAR(MAX))  
       FROM EmployeeIpAddress t3   
	   inner join IpMaster as IM on IM.AutoId=t3.IPAutoId
       WHERE t3.EmpAutoId =t1.AutoId
       FOR XML PATH('')  
     ),1,1,'' 
	)
   FROM [dbo].[EmployeeMaster] as t1 WHERE AutoId = @AutoId                                  
      --Column ISAppLogin add by Rizwan Ahmad on 05-09-2019                
 END                                      
 ELSE IF @Opcode=31                                      
 BEGIN                                    
  BEGIN TRY                        
  BEGIN TRAN 
        delete  EmployeeIpAddress where EmpAutoId=@AutoId
        delete [dbo].[AssignDeviceMaster] where EmpAutoId=@AutoId
		delete EmployeeMaster where AutoId = @AutoId
		
		
  COMMIT TRAN                        
  END TRY                                      
  BEGIN CATCH                              
  ROLLBACK   TRAN                              
   SET @isException=1                                      
   SET @exceptionMessage='This employee can not deleted due to used in process.'                        
  END CATCH                          
                         
 END                             
 Else if @Opcode=45                            
 Begin                            
   SELECT EmpId,ETM.[TypeName] AS EmpType,FirstName,LastName,Email,Contact,Address,State,City,Zipcode,UserName,                                    
   [ImageURL] FROM [dbo].[EmployeeMaster] as em     
   inner join EmployeeTypeMaster as etm on etm.AutoId=em.EmpType                            
   where em.AutoId=@AutoId                            
 End  
 

 ELSE IF @Opcode=46     
			BEGIN 
		
				IF EXISTS(SELECT * FROM [dbo].[EmployeeMaster] as EM  WHERE AutoId=@EmpAutoId                    
				and em.Status=1)                 
                    
				BEGIN                     
				SELECT 'success' as response,ProfileName,EM.AutoId,EM.[EmpType] As EmpTypeNo,ETM.TypeName AS [EmpType],                    
				[FirstName] AS Name,[UserName] ,[EmpId] ,Email,(select top 1 Logo from CompanyDetails) as ImageURL,(select top 1 CompanyId from CompanyDetails) as DBLocation,  
				CASE   
				WHEN (select convert(date,SubscriptionExpiryDate) from CompanyDetails) < convert(date,GETDATE()) then   
				'Your subscription expired on' +' '+ (select FORMAT(SubscriptionExpiryDate,'dd MMMM yyyy')+'.' from CompanyDetails)  
				WHEN (select convert(date,SubscriptionAlertDate) from CompanyDetails) <= convert(date,GETDATE()) then   
				'Your subscription will expire on' +' '+ (select FORMAT(SubscriptionExpiryDate,'dd MMMM yyyy')+'.' from CompanyDetails) else   
				'0' end as Submsg,    
				CASE   
				WHEN (select convert(date,SubscriptionExpiryDate) from CompanyDetails) < convert(date,GETDATE()) then 1  
				WHEN (select convert(date,SubscriptionAlertDate) from CompanyDetails) <= convert(date,GETDATE()) then 0 else   
				'1' end as ExpMsg   
				FROM [dbo].[EmployeeMaster] as EM                     
				INNER JOIN [dbo].[EmployeeTypeMaster] AS ETM on EM.EmpType=ETM.AutoId  WHERE EM.AutoId=@EmpAutoId                    
				and em.Status=1                    
				                   
				insert into ipaddress(EmpAutoId,IPAddress,LoginDate,Remarks)                     
				values(@EmpAutoId,@IPAddress,GETDATE(),'login successfully.')    
                 
			end                   
			    
			END
		Else if @Opcode=47
		Begin                            
		SELECT AutoId,FirstName+' '+ISNULL(LastName,'') as UserName,ISNULL(isApplyIP,0) as isApplyIP,CONVERT(varchar(100),DecryptByPassphrase('WHM',[Password])) AS [Password],
		UserName as UserId FROM [dbo].[EmployeeMaster] as em                               
		where em.AutoId=@AutoId                            
		End                           
                                         
END 
GO
