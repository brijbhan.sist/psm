ALTER proc [dbo].[Proc_ReceiveStockForPurchase_All]                 
 @VenderAutoId INT=NULL,              
 @PoNo varchar(50)=null,              
 @EmpId int=null              
As              
Begin              
declare @LocationAutoId int=(Select LocationAutoId from VendorMaster where autoid=@VenderAutoId)              
declare @OrderAutoId int=null,@BillAutoId int=null,@CostPrice decimal(10,2)=null,@UnitPrice Decimal(10,2)=null  
declare @CLocation varchar(50)=Replace(DB_Name(),'.a1whm.com','')            
declare @CurrentLocationId int =(Select AutoId from LocationMaster where Location=@CLocation)   
              
 if(@LocationAutoId=1)              
  BEGIN              
	  set @OrderAutoId=(Select omx.AutoId from [psmnj.a1whm.com].[dbo].[OrderMaster] as omx          
	  inner join [psmnj.a1whm.com].[dbo].[CustomerMaster] as cmx on omx.CustomerAutoId=cmx.AutoId           
	  where referenceOrderNumber=@PoNo and cmx.LocationAutoId=@CurrentLocationId)              
             
	  INSERT INTO [dbo].[StockEntry] ([BillNo],[BillDate],[Remarks], [VendorAutoId],CreatedBy,Reference_Code)               
	  Select OrderNo,OrderDate,OrderRemarks,@VenderAutoId,@EmpId,referenceOrderNumber from [psmnj.a1whm.com].[dbo].[OrderMaster] 
	  where AutoId=@OrderAutoId                                       
                              
	  set @BillAutoId = SCOPE_IDENTITY()                                    
                                   
	  INSERT INTO [dbo].[BillItems] ([BillAutoId],[ProductAutoId],[UnitAutoId],[Quantity],[QtyPerUnit],[Price])                                    
	  Select @BillAutoId,pm.AutoId,dim.UnitAutoId,dim.QtyShip-(ISNULL(dim.FreshReturnQty,0)+ISNULL(dim.DamageReturnQty,0)+ISNULL(dim.MissingItemQty,0)),QtyPerUnit,UnitPrice 
	  from [psmnj.a1whm.com].[dbo].[Delivered_Order_Items] as dim              
	  inner join  [psmnj.a1whm.com].[dbo].[Productmaster] as njpm on njpm.AutoId=dim.ProductAutoId              
	  inner join  [dbo].[Productmaster] as pm on pm.ProductId=njpm.ProductId              
	  where OrderAutoId=@OrderAutoId AND dim.QtyDel>0       
           
         
	  INSERT INTO ProductStockUpdateLog (ProductId, OledStock, NewStock, UserAutoId , DateTime, ReferenceId,
	  ActionRemark ,ReferenceType)                        
	  SELECT pm.ProductId,PM.Stock,isnull(PM.Stock,0)+isnull(TotalPieces,0),@EmpId,GETDATE(),@BillAutoId,
	  'Internal Order Received','[BillItems]' 
	  from ( 
		  select ProductId,SUM(tb.QtyDel) as TotalPieces
		  FROM [psmnj.a1whm.com].[dbo].[Delivered_Order_Items] AS tb               
		  inner join  [psmnj.a1whm.com].[dbo].[Productmaster] as njpm on njpm.AutoId=tb.ProductAutoId 
		  where tb.OrderAutoId=@OrderAutoId 
		  group by ProductId
	  ) as t
	  inner join  [dbo].[ProductMaster] as PM on t.ProductId=PM.ProductId  where t.TotalPieces>0        
	          
      UPDATE PM SET PM.[Stock]= isnull(pm.[Stock],0)+t.TotalPieces from 
	  ( 
		  select ProductId,SUM(tb.QtyDel) as TotalPieces
		  FROM [psmnj.a1whm.com].[dbo].[Delivered_Order_Items] AS tb               
		  inner join  [psmnj.a1whm.com].[dbo].[Productmaster] as njpm on njpm.AutoId=tb.ProductAutoId 
		  where tb.OrderAutoId=@OrderAutoId 
		  group by ProductId
	  ) as t
	  inner join  [dbo].[Productmaster] as pm on pm.ProductId=t.ProductId 
                    
  END
  if(@LocationAutoId=2)              
  BEGIN              
	  set @OrderAutoId=(Select omx.AutoId from [psmct.a1whm.com].[dbo].[OrderMaster] as omx          
	  inner join [psmct.a1whm.com].[dbo].[CustomerMaster] as cmx on omx.CustomerAutoId=cmx.AutoId           
	  where referenceOrderNumber=@PoNo and cmx.LocationAutoId=@CurrentLocationId)              
             
	  INSERT INTO [dbo].[StockEntry] ([BillNo],[BillDate],[Remarks], [VendorAutoId],CreatedBy,Reference_Code)               
	  Select OrderNo,OrderDate,OrderRemarks,@VenderAutoId,@EmpId,referenceOrderNumber from [psmct.a1whm.com].[dbo].[OrderMaster] where AutoId=@OrderAutoId                                       
                              
	  set @BillAutoId = SCOPE_IDENTITY()                                    
                                   
	  INSERT INTO [dbo].[BillItems] ([BillAutoId],[ProductAutoId],[UnitAutoId],[Quantity],[QtyPerUnit],[Price])                                    
	  Select @BillAutoId,pm.AutoId,dim.UnitAutoId,dim.QtyShip-(ISNULL(dim.FreshReturnQty,0)+ISNULL(dim.DamageReturnQty,0)+ISNULL(dim.MissingItemQty,0)),QtyPerUnit,UnitPrice 
	  from [psmct.a1whm.com].[dbo].[Delivered_Order_Items] as dim              
	  inner join  [psmct.a1whm.com].[dbo].[Productmaster] as njpm on njpm.AutoId=dim.ProductAutoId              
	  inner join  [dbo].[Productmaster] as pm on pm.ProductId=njpm.ProductId              
	  where OrderAutoId=@OrderAutoId        
           
         
	  INSERT INTO ProductStockUpdateLog (ProductId, OledStock, NewStock, UserAutoId , DateTime, ReferenceId, ActionRemark,ReferenceType )                        
	  SELECT pm.ProductId,PM.Stock,isnull(PM.Stock,0)+isnull(TotalPieces,0),@EmpId,GETDATE(),@BillAutoId,'Internal Order Received','[BillItems]' 
	  from ( 
		  select ProductId,SUM(tb.QtyDel) as TotalPieces
		  FROM [psmct.a1whm.com].[dbo].[Delivered_Order_Items] AS tb               
		  inner join  [psmct.a1whm.com].[dbo].[Productmaster] as njpm on njpm.AutoId=tb.ProductAutoId 
		  where tb.OrderAutoId=@OrderAutoId 
		  group by ProductId
	  ) as t
	  inner join  [dbo].[ProductMaster] as PM on t.ProductId=PM.ProductId    where t.TotalPieces>0               
	          
      UPDATE PM SET PM.[Stock]= isnull(pm.[Stock],0)+t.TotalPieces from 
	  ( 
		  select ProductId,SUM(tb.QtyDel) as TotalPieces
		  FROM [psmct.a1whm.com].[dbo].[Delivered_Order_Items] AS tb               
		  inner join  [psmct.a1whm.com].[dbo].[Productmaster] as njpm on njpm.AutoId=tb.ProductAutoId 
		  where tb.OrderAutoId=@OrderAutoId 
		  group by ProductId
	  ) as t
	  inner join  [dbo].[Productmaster] as pm on pm.ProductId=t.ProductId 
                    
  END 
            
  if(@LocationAutoId=4)              
  BEGIN              
	  set @OrderAutoId=(Select omx.AutoId from [psmpa.a1whm.com].[dbo].[OrderMaster] as omx          
	  inner join [psmpa.a1whm.com].[dbo].[CustomerMaster]as cmx on omx.CustomerAutoId=cmx.AutoId           
	  where referenceOrderNumber=@PoNo and cmx.LocationAutoId=@CurrentLocationId)          
          
          
	  INSERT INTO [dbo].[StockEntry] ([BillNo],[BillDate],[Remarks], [VendorAutoId],CreatedBy,Reference_Code)               
	  Select OrderNo,OrderDate,OrderRemarks,@VenderAutoId,@EmpId,referenceOrderNumber from [psmpa.a1whm.com].[dbo].[OrderMaster] where AutoId=@OrderAutoId                                       
                               
	  set @BillAutoId = SCOPE_IDENTITY()                                    
                                    
	  INSERT INTO [dbo].[BillItems] ([BillAutoId],[ProductAutoId],[UnitAutoId],[Quantity],[QtyPerUnit],[Price])              
	  Select @BillAutoId,pm.AutoId,dim.UnitAutoId,dim.QtyShip-(ISNULL(dim.FreshReturnQty,0)+ISNULL(dim.DamageReturnQty,0)+ISNULL(dim.MissingItemQty,0)),QtyPerUnit,UnitPrice 
	  from [psmpa.a1whm.com].[dbo].[Delivered_Order_Items] as dim              
	  inner join  [psmpa.a1whm.com].[dbo].[Productmaster] as njpm on njpm.AutoId=dim.ProductAutoId              
	  inner join  [dbo].[Productmaster] as pm on pm.ProductId=njpm.ProductId              
	  where OrderAutoId=@OrderAutoId               
            
                 
                         
	  INSERT INTO ProductStockUpdateLog (ProductId, OledStock, NewStock, UserAutoId , DateTime, ReferenceId, ActionRemark ,ReferenceType)                        
	  SELECT pm.ProductId,PM.Stock,isnull(PM.Stock,0)+isnull(TotalPieces,0),@EmpId,GETDATE(),@BillAutoId,'Internal Order Received','[BillItems]'  
	  from ( 
		  select ProductId,SUM(tb.QtyDel) as TotalPieces
		  FROM [psmpa.a1whm.com].[dbo].[Delivered_Order_Items] AS tb               
		  inner join  [psmpa.a1whm.com].[dbo].[Productmaster] as njpm on njpm.AutoId=tb.ProductAutoId 
		  where tb.OrderAutoId=@OrderAutoId 
		  group by ProductId
	  ) as t
	  inner join  [dbo].[ProductMaster] as PM on t.ProductId=PM.ProductId   where t.TotalPieces>0                
	          
      UPDATE PM SET PM.[Stock]= isnull(pm.[Stock],0)+t.TotalPieces from 
	  ( 
		  select ProductId,SUM(tb.QtyDel) as TotalPieces
		  FROM [psmpa.a1whm.com].[dbo].[Delivered_Order_Items] AS tb               
		  inner join  [psmpa.a1whm.com].[dbo].[Productmaster] as njpm on njpm.AutoId=tb.ProductAutoId 
		  where tb.OrderAutoId=@OrderAutoId 
		  group by ProductId
	  ) as t
	  inner join  [dbo].[Productmaster] as pm on pm.ProductId=t.ProductId
	  
  END              
  if(@LocationAutoId=5)              
  BEGIN              
	  set @OrderAutoId=(Select omx.AutoId from [psmnpa.a1whm.com].[dbo].[OrderMaster] as omx          
	  inner join [psmnpa.a1whm.com].[dbo].[CustomerMaster]as cmx on omx.CustomerAutoId=cmx.AutoId           
	  where referenceOrderNumber=@PoNo and cmx.LocationAutoId=@CurrentLocationId)          
          
	 INSERT INTO [dbo].[StockEntry] ([BillNo],[BillDate],[Remarks], [VendorAutoId],CreatedBy,Reference_Code)               
	  Select OrderNo,OrderDate,OrderRemarks,@VenderAutoId,@EmpId,referenceOrderNumber from [psmnpa.a1whm.com].[dbo].[OrderMaster] where AutoId=@OrderAutoId                                       
                               
	  set @BillAutoId = SCOPE_IDENTITY()                                    
                                    
	  INSERT INTO [dbo].[BillItems] ([BillAutoId],[ProductAutoId],[UnitAutoId],[Quantity],[QtyPerUnit],[Price])                                    
	  Select @BillAutoId,pm.AutoId,dim.UnitAutoId,dim.QtyShip-(ISNULL(dim.FreshReturnQty,0)+ISNULL(dim.DamageReturnQty,0)+ISNULL(dim.MissingItemQty,0)),QtyPerUnit,UnitPrice 
	  from [psmnpa.a1whm.com].[dbo].[Delivered_Order_Items] as dim              
	  inner join  [psmnpa.a1whm.com].[dbo].[Productmaster] as njpm on njpm.AutoId=dim.ProductAutoId              
	  inner join  [dbo].[Productmaster] as pm on pm.ProductId=njpm.ProductId              
	  where OrderAutoId=@OrderAutoId               
                                         
                         
	  INSERT INTO ProductStockUpdateLog (ProductId, OledStock, NewStock, UserAutoId , DateTime, ReferenceId, ActionRemark ,ReferenceType)                        
	  SELECT pm.ProductId,PM.Stock,isnull(PM.Stock,0)+isnull(TotalPieces,0),@EmpId,GETDATE(),@BillAutoId,'Internal Order Received','[BillItems]'  
	  from ( 
		  select ProductId,SUM(tb.QtyDel) as TotalPieces
		  FROM [psmnpa.a1whm.com].[dbo].[Delivered_Order_Items] AS tb               
		  inner join  [psmnpa.a1whm.com].[dbo].[Productmaster] as njpm on njpm.AutoId=tb.ProductAutoId 
		  where tb.OrderAutoId=@OrderAutoId 
		  group by ProductId
	  ) as t
	  inner join  [dbo].[ProductMaster] as PM on t.ProductId=PM.ProductId    where t.TotalPieces>0               
	          
      UPDATE PM SET PM.[Stock]= isnull(pm.[Stock],0)+t.TotalPieces from 
	  ( 
		  select ProductId,SUM(tb.QtyDel) as TotalPieces
		  FROM [psmnpa.a1whm.com].[dbo].[Delivered_Order_Items] AS tb               
		  inner join  [psmnpa.a1whm.com].[dbo].[Productmaster] as njpm on njpm.AutoId=tb.ProductAutoId 
		  where tb.OrderAutoId=@OrderAutoId 
		  group by ProductId
	  ) as t
	  inner join  [dbo].[Productmaster] as pm on pm.ProductId=t.ProductId       
               
  END              
  
 if(@LocationAutoId=7)              
 BEGIN              
	  set @OrderAutoId=(Select omx.AutoId from [psm.a1whm.com].[dbo].[OrderMaster] as omx          
	  inner join [psm.a1whm.com].[dbo].[CustomerMaster]as cmx on omx.CustomerAutoId=cmx.AutoId           
	  where referenceOrderNumber=@PoNo and cmx.LocationAutoId=@CurrentLocationId)              
              
	  INSERT INTO [dbo].[StockEntry] ([BillNo],[BillDate],[Remarks], [VendorAutoId],CreatedBy,Reference_Code)               
	  Select OrderNo,OrderDate,OrderRemarks,@VenderAutoId,@EmpId,referenceOrderNumber from [psm.a1whm.com].[dbo].[OrderMaster] where AutoId=@OrderAutoId                                       
                               
	  set @BillAutoId = SCOPE_IDENTITY()                                    
                                    
	  INSERT INTO [dbo].[BillItems] ([BillAutoId],[ProductAutoId],[UnitAutoId],[Quantity],[QtyPerUnit],[Price])                                    
	  Select @BillAutoId,pm.AutoId,dim.UnitAutoId,dim.QtyShip-(ISNULL(dim.FreshReturnQty,0)+ISNULL(dim.DamageReturnQty,0)+ISNULL(dim.MissingItemQty,0)),QtyPerUnit,UnitPrice 
	  from [psm.a1whm.com].[dbo].[Delivered_Order_Items] as dim              
	  inner join  [psm.a1whm.com].[dbo].[Productmaster] as njpm on njpm.AutoId=dim.ProductAutoId              
	  inner join  [dbo].[Productmaster] as pm on pm.ProductId=njpm.ProductId              
	  where OrderAutoId=@OrderAutoId               
                                 
      
	  INSERT INTO ProductStockUpdateLog (ProductId, OledStock, NewStock, UserAutoId , DateTime, ReferenceId, ActionRemark,ReferenceType )                        
	  SELECT pm.ProductId,PM.Stock,isnull(PM.Stock,0)+isnull(TotalPieces,0),@EmpId,GETDATE(),@BillAutoId,'Internal Order Received','[BillItems]' 
	  from ( 
		  select ProductId,SUM(tb.QtyDel) as TotalPieces
		  FROM [psm.a1whm.com].[dbo].[Delivered_Order_Items] AS tb               
		  inner join  [psm.a1whm.com].[dbo].[Productmaster] as njpm on njpm.AutoId=tb.ProductAutoId 
		  where tb.OrderAutoId=@OrderAutoId 
		  group by ProductId
	  ) as t
	  inner join  [dbo].[ProductMaster] as PM on t.ProductId=PM.ProductId    where t.TotalPieces>0               
	          
      UPDATE PM SET PM.[Stock]= isnull(pm.[Stock],0)+t.TotalPieces from 
	  ( 
		  select ProductId,SUM(tb.QtyDel) as TotalPieces
		  FROM [psm.a1whm.com].[dbo].[Delivered_Order_Items] AS tb               
		  inner join  [psm.a1whm.com].[dbo].[Productmaster] as njpm on njpm.AutoId=tb.ProductAutoId 
		  where tb.OrderAutoId=@OrderAutoId 
		  group by ProductId
	  ) as t
	  inner join  [dbo].[Productmaster] as pm on pm.ProductId=t.ProductId           
  End   
  if(@LocationAutoId=9)              
  BEGIN              
	  set @OrderAutoId=(Select omx.AutoId from [psmwpa.a1whm.com].[dbo].[OrderMaster] as omx          
	  inner join [psmwpa.a1whm.com].[dbo].[CustomerMaster]as cmx on omx.CustomerAutoId=cmx.AutoId           
	  where referenceOrderNumber=@PoNo and cmx.LocationAutoId=@CurrentLocationId)            
              
	  INSERT INTO [dbo].[StockEntry] ([BillNo],[BillDate],[Remarks], [VendorAutoId],CreatedBy,Reference_Code)               
	  Select OrderNo,OrderDate,OrderRemarks,@VenderAutoId,@EmpId,referenceOrderNumber from [psmwpa.a1whm.com].[dbo].[OrderMaster] where AutoId=@OrderAutoId                                       
                               
	  set @BillAutoId = SCOPE_IDENTITY()                                    
                                    
	  INSERT INTO [dbo].[BillItems] ([BillAutoId],[ProductAutoId],[UnitAutoId],[Quantity],[QtyPerUnit],[Price])                                    
	  Select @BillAutoId,pm.AutoId,dim.UnitAutoId,dim.QtyShip-(ISNULL(dim.FreshReturnQty,0)+ISNULL(dim.DamageReturnQty,0)+ISNULL(dim.MissingItemQty,0)),QtyPerUnit,UnitPrice
	  from [psmwpa.a1whm.com].[dbo].[Delivered_Order_Items] as dim              
	  inner join  [psmwpa.a1whm.com].[dbo].[Productmaster] as njpm on njpm.AutoId=dim.ProductAutoId              
	  inner join  [dbo].[Productmaster] as pm on pm.ProductId=njpm.ProductId              
	  where OrderAutoId=@OrderAutoId               
          
        INSERT INTO ProductStockUpdateLog (ProductId, OledStock, NewStock, UserAutoId , DateTime, ReferenceId, ActionRemark ,ReferenceType)                        
	  SELECT pm.ProductId,PM.Stock,isnull(PM.Stock,0)+isnull(TotalPieces,0),@EmpId,GETDATE(),@BillAutoId,'Internal Order Received','[BillItems]'  	 
	  from ( 
		  select ProductId,SUM(tb.QtyDel) as TotalPieces
		  FROM [psmwpa.a1whm.com].[dbo].[Delivered_Order_Items] AS tb               
		  inner join  [psmwpa.a1whm.com].[dbo].[Productmaster] as njpm on njpm.AutoId=tb.ProductAutoId 
		  where tb.OrderAutoId=@OrderAutoId 
		  group by ProductId
	  ) as t
	  inner join  [dbo].[ProductMaster] as PM on t.ProductId=PM.ProductId   where t.TotalPieces>0                
	          
      UPDATE PM SET PM.[Stock]= isnull(pm.[Stock],0)+t.TotalPieces from 
	  ( 
		  select ProductId,SUM(tb.QtyDel) as TotalPieces
		  FROM [psmwpa.a1whm.com].[dbo].[Delivered_Order_Items] AS tb               
		  inner join  [psmwpa.a1whm.com].[dbo].[Productmaster] as njpm on njpm.AutoId=tb.ProductAutoId 
		  where tb.OrderAutoId=@OrderAutoId 
		  group by ProductId
	  ) as t
	  inner join  [dbo].[Productmaster] as pm on pm.ProductId=t.ProductId              
  End   
  if(@LocationAutoId=10)              
  BEGIN              
	  set @OrderAutoId=(Select omx.AutoId from [psmny.a1whm.com].[dbo].[OrderMaster] as omx          
	  inner join [psmny.a1whm.com].[dbo].[CustomerMaster]as cmx on omx.CustomerAutoId=cmx.AutoId           
	  where referenceOrderNumber=@PoNo and cmx.LocationAutoId=@CurrentLocationId)            
              
	  INSERT INTO [dbo].[StockEntry] ([BillNo],[BillDate],[Remarks], [VendorAutoId],CreatedBy,Reference_Code)               
	  Select OrderNo,OrderDate,OrderRemarks,@VenderAutoId,@EmpId,referenceOrderNumber from [psmny.a1whm.com].[dbo].[OrderMaster] where AutoId=@OrderAutoId                                       
                               
	  set @BillAutoId = SCOPE_IDENTITY()                                    
                                    
	  INSERT INTO [dbo].[BillItems] ([BillAutoId],[ProductAutoId],[UnitAutoId],[Quantity],[QtyPerUnit],[Price])                                    
	  Select @BillAutoId,pm.AutoId,dim.UnitAutoId,dim.QtyShip-(ISNULL(dim.FreshReturnQty,0)+ISNULL(dim.DamageReturnQty,0)+ISNULL(dim.MissingItemQty,0)),QtyPerUnit,UnitPrice
	  from [psmny.a1whm.com].[dbo].[Delivered_Order_Items] as dim              
	  inner join  [psmny.a1whm.com].[dbo].[Productmaster] as njpm on njpm.AutoId=dim.ProductAutoId              
	  inner join  [dbo].[Productmaster] as pm on pm.ProductId=njpm.ProductId              
	  where OrderAutoId=@OrderAutoId               
          
        INSERT INTO ProductStockUpdateLog (ProductId, OledStock, NewStock, UserAutoId , DateTime, ReferenceId, ActionRemark ,ReferenceType)                        
	  SELECT pm.ProductId,PM.Stock,isnull(PM.Stock,0)+isnull(TotalPieces,0),@EmpId,GETDATE(),@BillAutoId,'Internal Order Received','[BillItems]'  	 
	  from ( 
		  select ProductId,SUM(tb.QtyDel) as TotalPieces
		  FROM [psmny.a1whm.com].[dbo].[Delivered_Order_Items] AS tb               
		  inner join  [psmny.a1whm.com].[dbo].[Productmaster] as njpm on njpm.AutoId=tb.ProductAutoId 
		  where tb.OrderAutoId=@OrderAutoId 
		  group by ProductId
	  ) as t
	  inner join  [dbo].[ProductMaster] as PM on t.ProductId=PM.ProductId    where t.TotalPieces>0               
	          
      UPDATE PM SET PM.[Stock]= isnull(pm.[Stock],0)+t.TotalPieces from 
	  ( 
		  select ProductId,SUM(tb.QtyDel) as TotalPieces
		  FROM [psmny.a1whm.com].[dbo].[Delivered_Order_Items] AS tb               
		  inner join  [psmny.a1whm.com].[dbo].[Productmaster] as njpm on njpm.AutoId=tb.ProductAutoId 
		  where tb.OrderAutoId=@OrderAutoId 
		  group by ProductId
	  ) as t
	  inner join  [dbo].[Productmaster] as pm on pm.ProductId=t.ProductId              
  End   
End 
