ALTER Proc [dbo].[ProcMissingReport]
@Opcode INT=NULL,                    
@SalesPerson int=null,                    
@CustomerAutoId INT=NULL,                                     
@CategoryAutoId int=null,                    
@SubCategoryAutoId int=null,                    
@FromDate date =NULL,                    
@ToDate date =NULL,                    
@PageIndex INT=1,                    
@PageSize INT=10,                    
@RecordCount INT=null,                    
@isException bit out,                    
@exceptionMessage varchar(max) out   
As
Begin
BEGIN TRY                                                                               
	SET @isException=0                                                                                                  
	SET @exceptionMessage='Success!!'
	IF @Opcode = 41                                                                                 
		BEGIN    
		select ROW_NUMBER() over(order by ProductId) as RowNumber, t.*
		into #Result44 from ( 
			select
			cm.CustomerId,cm.CustomerName,om.OrderNo
			,em.FirstName as [SalesRep]
			,FORMAT(convert(date,(om.OrderDate)),'MM/dd/yyyy') as OrderDate
			,(select FirstName from EmployeeMaster where AutoId =om.PackerAutoId )[Packer]
			,(select top 1 FORMAT(packingdate,'MM/dd/yyyy HH:mm tt') from GenPacking where OrderAutoId = om.AutoId order by PackingDate desc)[PackedTime]
			,(select FirstName from EmployeeMaster where AutoId = om.Driver )[Driver]
			,FORMAT(convert(date,(om.DeliveryDate)),'MM/dd/yyyy') as DeliveryDate
			,(select FirstName from EmployeeMaster where AutoId =om.AccountAutoId )[Account]
			,FORMAT(convert(date,(om.Order_Closed_Date)),'MM/dd/yyyy') as OrderCloseDate
			,(Convert(varchar(50),MissingItemQty) + ' - ' +um.UnitType) [MissingDetails]
			,pm.ProductId
			,pm.ProductName
			from OrderMaster as om
			inner join CustomerMaster as cm on cm.AutoId = om.CustomerAutoId
			inner join Delivered_Order_Items as doi on om.AutoId = doi.OrderAutoId
			inner join EmployeeMaster as em on em.AutoId = om.SalesPersonAutoId
			inner join ProductMaster as pm on pm.AutoId = doi.ProductAutoId
			inner join UnitMaster as um on um.AutoId = doi.MissingItemUnitAutoId
			where doi.MissingItemQty > 0 and om.Status=11
			and                  
		   (ISNULL(@FromDate,'')='' OR ISNULL(@ToDate,'') = '' OR                                     
		   (CONVERT(date,OM.Order_Closed_Date) BETWEEN CONVERT(date,@fromDate) AND CONVERT(date,@ToDate)))                      
		   AND        
		   (ISNULL(@CustomerAutoId,0)=0 OR OM.CustomerAutoId=@CustomerAutoId)                      
		   AND                                 
		   (ISNULL(@SalesPerson,0)=0 OR om.SalesPersonAutoId=@SalesPerson)                      
		   AND                      
		   (ISNULL(@CategoryAutoId,0)=0 OR PM.CategoryAutoId=@CategoryAutoId)                      
		   AND                      
		   (ISNULL(@SubCategoryAutoId,0)=0 OR PM.SubcategoryAutoId=@SubCategoryAutoId)
		    
		   ) as t order by t.OrderDate asc

		   SELECT * FROM #Result44                  
		  WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))                   
		  SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize = 0 then COUNT(RowNumber) else @PageSize end AS PageSize, @PageIndex AS PageIndex,Format(getdate(),'MM/dd/yyyy hh:mm tt') as PrintDate FROM #Result44
			
		END
		ELSE IF @OpCode=42                                                                                    
				BEGIN                                                                                                  
			select
				  (
					SELECT AutoId as CAID, CategoryName as CAN FROM CategoryMaster order by CAN asc 
					for json path
				  ) as Category,
				 
				  (
				  Select em.AutoId as SID ,(em.FirstName + ' ' + em.LastName) as SP from EmployeeMaster em                                           
				  inner join EmployeeTypeMaster etm on etm.AutoId = em.EmpType                                          
				  where EmpType = 2 and Status=1 order by SP ASC 
				  for json path
				  ) as SalesPerson
			for JSON path                                                                                        
		END
		ELSE IF @OpCode=43                                                                                    
		BEGIN        
		  SELECT AutoId as SCID,SubcategoryName as SCN from SubCategoryMaster    
		  where (isnull(@CategoryAutoId,0)=0 or CategoryAutoId=@CategoryAutoId )and Status=1     
		  order by SCN  ASC  
		  for json path
		END
		ELSE IF @OpCode=44                                                                                    
		BEGIN        
		SELECT AutoId,CustomerId+' '+CustomerName as CustomerName FROM CustomerMaster where @SalesPerson=0 or @SalesPerson is null or SalesPersonAutoId=@SalesPerson  order by AutoId asc 
		for json path  
		END
END TRY                                                                                                  
BEGIN CATCH                                                                                                  
	SET @isException=1                                                           
	SET @exceptionMessage=ERROR_MESSAGE()--'Oops! Something went wrong.Please try later.'                                                  
END CATCH 
End
GO
