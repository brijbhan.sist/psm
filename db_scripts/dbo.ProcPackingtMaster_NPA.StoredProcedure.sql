ALTER PROCEDURE [dbo].[ProcPackingtMaster_NPA]         
@ProductAutoId int=NULL ,
@EmpAutoId int =null
AS        
BEGIN           
      
 declare @ProductId int=(select ProductId from[dbo].[ProductMaster] WHERE AutoId = @ProductAutoId)      
 IF NOT EXISTS(SELECT [AutoId] FROM [psmnpa.a1whm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId)        
 BEGIN       
   EXEC [dbo].[ProcProductMaster_ALL]      
   @ProductId=@ProductId      
 END      
 DECLARE @AutoId INT=(select AutoId from [psmnpa.a1whm.com].[dbo].[ProductMaster] WHERE ProductId = @ProductId)      
 
    insert into [psmnpa.a1whm.com].[dbo].[PackingDetailsUpdateLog](ProductAutoId,UnitType,MinPrice,CostPrice,Price,WHminPrice,UpdatedByAutoId,DateTime,Reff_Code)                                                                             
	select pd.ProductAutoId,pd.UnitType,pd.MinPrice,pd.CostPrice,pd.Price,pd.WHminPrice,@EmpAutoId,GETDATE(),'PSMNJ' from 
	[psmnpa.a1whm.com].[dbo].PackingDetails AS PD                 
	INNER JOIN [dbo].PackingDetails AS NJ ON PD.UnitType=NJ.UnitType AND NJ.ProductAutoId=@ProductAutoId                
	WHERE PD.ProductAutoId=@AutoId 
	and PD.Qty!=nj.Qty	
	
	UPDATE PA SET 
	PA.[Qty]=(case when PA.UnitType=3 then 1 else NJ.Qty end ),
	PA.CostPrice=(case when PA.UnitType=3 then pa.CostPrice else ((pa.CostPrice/pa.Qty)*nj.qty) end ),    
	PA.WHminPrice=(case when PA.UnitType=3 then pa.WHminPrice else ((pa.WHminPrice/pa.Qty)*nj.qty) end ),    
	PA.MinPrice=(case when PA.UnitType=3 then pa.MinPrice else ((pa.MinPrice/pa.Qty)*nj.qty) end ),    
	PA.Price=(case when PA.UnitType=3 then pa.Price else ((pa.Price/pa.Qty)*nj.qty) end ),       
	PA.UpdateDate=GETDATE(),PA.EligibleforFree=NJ.EligibleforFree 
	FROM [psmnpa.a1whm.com].[dbo].PackingDetails AS PA       
	INNER JOIN [dbo].PackingDetails AS NJ ON PA.UnitType=NJ.UnitType AND NJ.ProductAutoId=@ProductAutoId      
	WHERE PA.ProductAutoId=@AutoId
	and pa.Qty!=nj.Qty
    
  insert into [psmnpa.a1whm.com].[dbo].PackingDetails(UnitType,Qty,MinPrice,CostPrice,Price,ProductAutoId,WHminPrice,
  UpdateDate,EligibleforFree,CreateBy,CreateDate,ModifiedBy,PackingStatus)      
  select  UnitType,Qty,MinPrice,CostPrice,Price,@AutoId,WHminPrice,UpdateDate,EligibleforFree,CreateBy,CreateDate,ModifiedBy,PackingStatus       
  from PackingDetails where ProductAutoId=@ProductAutoId AND       
  UnitType NOT IN (SELECT T.UnitType FROM  [psmnpa.a1whm.com].[dbo].PackingDetails AS T WHERE T.ProductAutoId=@AutoId)      
   
   
  update [psmnpa.a1whm.com].[dbo].[ProductMaster] SET  
  PackingAutoId=(SELECT PackingAutoId from [dbo].[ProductMaster] as pm WHERE pm.ProductId = @ProductId)  
  WHERE ProductId = @ProductId  
  
      
  insert into [psmnpa.a1whm.com].[dbo].[ItemBarcode](ProductAutoId,UnitAutoId,Barcode)      
  select  @AutoId,UnitAutoId,Barcode       
  from [dbo].ItemBarcode where ProductAutoId =@ProductAutoId      
  and   Barcode not in(select Barcode from [psmnpa.a1whm.com].[dbo].[ItemBarcode])      
END      
         
GO
