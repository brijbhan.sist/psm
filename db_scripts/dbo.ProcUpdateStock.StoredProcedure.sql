
ALTER procedure [dbo].[ProcUpdateStock]    
@OpCode int=Null,    
@Productid int = null,    
@ProductName varchar = null,    
@ProductQuantity int = null,    
@OldQty int = null,    
@UserAutoId int = null,    
@Barcode varchar(250)=null,  
@PageIndex INT=1,    
@PageSize INT=10,    
@RecordCount INT=null,    
@isException bit out,    
@exceptionMessage varchar(max) out    
AS    
BEGIN    
 BEGIN TRY    
  SET @isException=0    
  SET @exceptionMessage='success'    
     
  if @OpCode = 41    
  begin    
  SELECT Productid, CONVERT(varchar(50), Productid)+ ' - ' + ProductName AS ProductDetail from ProductMaster ORDER BY ProductDetail ASC;  
  end    
  if @OpCode = 42    
  begin    
		select Productid, Stock,(select ImageUrl from ProductImageUrl)+ ThirdImage as Productimage  from ProductMaster where Productid = @Productid    
    
		select ROW_NUMBER() OVER(ORDER BY psul.DateTime desc) AS RowNumber,pm.ProductId, pm.ProductName, psul.NewStock as [NewStock],
		psul.OledStock as [OledStock], format(DateTime,'MM/dd/yyyy hh:mm tt') as Date,  
		em.FirstName + ' (' + etm.TypeName + ')' as UserName,ActionRemark,psul.AutoId into #temp42 from ProductMaster as pm   
		inner join [dbo].[ProductStockUpdateLog] as psul on psul.ProductId = pm.ProductId     
		inner join EmployeeMaster as em on em.AutoId = psul.UserAutoId inner join EmployeeTypeMaster as etm on etm.AutoId = em.EmpType      
		where pm.ProductId = @Productid     
		order by  psul.AutoId desc, Date desc   
    
		SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize = 0 then COUNT(RowNumber) else @PageSize end AS PageSize,
		@PageIndex AS PageIndex,(Select PackingAutoid from ProductMaster where ProductId=@Productid) as PackingAutoid FROM #temp42    
    
		SELECT * FROM #temp42    
		WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize)-1)) 
		order by AutoId desc, Date desc

		
		select UM.AutoId,UM.UnitType,PD.Qty from PackingDetails as PD
		INNER JOIN UnitMaster as UM ON UM.AutoId=PD.UnitType
		where ProductAutoId=(Select AutoId FROM ProductMaster where ProductId=@Productid) order by UM.AutoId desc
  end    
    
  if @OpCode = 21    
  begin    
  BEGIN TRY     
  BEGIN TRAN      
        
  set @OldQty = (select Stock from ProductMaster where ProductId = @Productid)    
         
  insert into [dbo].[ProductStockUpdateLog]([ProductId],[OledStock],[NewStock],[UserAutoId],[DateTime],ReferenceType,ActionRemark)
  values(@Productid,@OldQty,@ProductQuantity,@UserAutoId,GETDATE(),'ProductMaster','Manual Stock has updated by '+(Select FirstName+' '+LastName from Employeemaster where autoid=@UserAutoId))    
  update ProductMaster set Stock = @ProductQuantity where Productid = @Productid       
  SET @isException=0    
    
  COMMIT TRANSACTION    
  END TRY    
  BEGIN CATCH    
  ROLLBACK TRAN    
	  SET @isException=1    
	  SET @exceptionMessage='Oops! Something went wrong.Please try later'
  END CATCH    
  end    
  ELSE IF @OpCode=43  
  BEGIN  
    SELECT ProductId FROM ItemBarcode as im   
    INNER JOIN ProductMaster AS PM ON PM.AutoId=IM.ProductAutoId WHERE Barcode=@Barcode  
  END  
  END TRY    
 BEGIN CATCH        
   SET @isException=1    
   SET @exceptionMessage='Oops! Something went wrong.Please try later'
 END CATCH    
END 
GO
