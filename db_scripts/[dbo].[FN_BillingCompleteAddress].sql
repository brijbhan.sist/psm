USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_BillingCompleteAddress]    Script Date: 07-18-2020 22:32:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION  [dbo].[FN_BillingCompleteAddress] 
(   
  @AutoId int   
)  
RETURNS varchar(Max)  
AS  
BEGIN  
	DECLARE @CompleteAddress varchar(MAX)
	set @CompleteAddress=(SELECT [Address]+', '+ISNULL([Address2],'')+', '+City+', '+(SELECT StateName FROM State Where AutoId=State)+', '+Zipcode FROM BillingAddress WHere Autoid=@AutoId)
	RETURN @CompleteAddress  
END 