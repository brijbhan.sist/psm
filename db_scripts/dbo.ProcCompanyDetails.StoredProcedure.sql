--Alter table CompanyDetails add OptimoDriverLimit int
ALTER PROCEDURE [dbo].[ProcCompanyDetails]  
@OpCode int=Null,  
@AutoId int=NULL,  
@DateDifference int=NULL,
@CompanyId varchar(12)=NULL,  
@CompanyName varchar(100)=NULL,  
@Address varchar(MAX)=NULL,  
@EmailAddress varchar(100)=NULL,  
@Website varchar(100)=NULL,  
@MobileNo varchar(20)=NULL,  
@FaxNo varchar(20)=NULL,  
@Logo varchar(max)=null,
@StartLongitude varchar(max)=null,
@StartLatitude varchar(max)=null,
@currentVersion varchar(max)=null,
@SubscriptionAlertDate datetime = null,
@SubscriptionExpiryDate datetime = null,
@appLogoutTime datetime = null,
@OptimoDriverLimit int=null,
@StartDate datetime = null,
@TermsCondition varchar(max)=null,  
@isException bit out,  
@exceptionMessage varchar(max) out  
AS  
BEGIN  
BEGIN TRY  
  
  SET @isException=0  
  SET @exceptionMessage=''  
    
  
IF @OpCode=21  
   BEGIN  
      IF (CAST(@StartDate as Date)>CAST(@SubscriptionAlertDate as Date)) 
	  BEGIN
			SET @isException=1  
			SET @exceptionMessage='Subcription Alert Date can''t be less than Start Date' 
	  END
	  ELSE IF(CAST(@SubscriptionAlertDate as Date)>CAST(@SubscriptionExpiryDate as Date))
	  BEGIN
			SET @isException=1  
			SET @exceptionMessage='Subcription Expiry Date can''t be less than Subcription Alert Date' 
	  END
	  ELSE
	  BEGIN
			UPDATE CompanyDetails  
			SET  CompanyName = @CompanyName   
			,[Address] = @Address,
			EmailAddress=@EmailAddress   
			,[Website] = @Website,  
			[MobileNo] = replace(@MobileNo  ,' ','')
			,[FaxNo] = replace(@FaxNo  ,' ','')  
			,TermsCondition = @TermsCondition  
			,Logo =  @Logo   
			,SubscriptionAlertDate = @SubscriptionAlertDate
			,SubscriptionExpiryDate = @SubscriptionExpiryDate
			,LiveDate = @StartDate
			,currentVersion = @currentVersion,
			DateDifference=@DateDifference,
			optimoStart_Lat=@StartLatitude,
			optimoStart_Long=@StartLongitude,
			OptimoDriverLimit=@OptimoDriverLimit
			if(select COUNT(LogoutTime) from App_LogoutTime)>0 
			BEGIN
				update App_LogoutTime set LogoutTime=format(@appLogoutTime, 'HH:mm tt')
			END
			ELSE                                              
			BEGIN
				INSERT INTO App_LogoutTime (LogoutTime) VALUES(format(@appLogoutTime, 'HH:mm tt'))
			END
	  END
   END  
    
 else if @OpCode=41  
    BEGIN  
         select CompanyId,CompanyName,[Address],[EmailAddress],[Website],[MobileNo],currentVersion,[FaxNo],
		 TermsCondition,format(LiveDate,'MM/dd/yyyy') as StartDate,OptimoDriverLimit,  
        Logo,format(SubscriptionAlertDate, 'MM/dd/yyyy') as SubscriptionAlertDate ,
		format(SubscriptionExpiryDate,'MM/dd/yyyy') as SubscriptionExpiryDate,DateDifference,
		optimoStart_Long,optimoStart_Lat from CompanyDetails  
          
		select convert(varchar(20),LogoutTime) as LogoutTime from App_LogoutTime
		
    End  
    END TRY  
 BEGIN CATCH  
  SET @isException=1  
  SET @exceptionMessage='Oops! Something went wrong.Please try later.'--ERROR_MESSAGE()  
 END CATCH  
END  
  
  
  