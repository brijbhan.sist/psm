alter PROCEDURE [dbo].[ProcWebOrderMaster]                                                                                                                                              
@Opcode INT=NULL,                                                                                                                                                  
@OrderNo VARCHAR(15)=NULL,                                                                                                                                              
@CustomerAutoId INT=NULL,                             
@SalesPersonAutoId INT=NULL,                                            
@OrderItemAutoId INT=NULL,                                                                                                                       
@Todate DATETIME=NULL,                                                                                                                                              
@Fromdate DATETIME=NULL,                                                                                                                
@PageIndex INT = 1,                                        
@PageSize INT = 10,
@LoginEmpType INT=NULL,   
@CheckSecurity  VARCHAR(max)=NULL,  
@RecordCount INT =null,                                                                                                                           
@isException bit out,                                     
@exceptionMessage varchar(max) out                                                                                
AS                                                                                                                                              
BEGIN                                                                                                                         
BEGIN TRY                                                                                                                                      
Set @isException=0                                                                                                                     
Set @exceptionMessage='Success'  
IF @Opcode=41
BEGIN
	select AutoId,FirstName+' '+ISNULL(LastName,'') as empName  from EmployeeMaster where EmpType=2 and status=1 order by empName asc
	for json path
END
ELSE IF @Opcode=42
BEGIN
	select autoid,customerName from CustomerMaster 
	where status=1 and SalesPersonAutoId=@SalesPersonAutoId or @SalesPersonAutoId is null or @SalesPersonAutoId=0 order by CustomerName   asc
	for json path 
	 
END
ELSE IF @Opcode=43                                                                                                                                         
BEGIN                                                                         
SELECT ROW_NUMBER() OVER(ORDER BY [OrderNo]  DESC                                                                                                                                           
) AS RowNumber, * INTO #Results from                                                         
(  SELECT OM.[AutoId],OM.OrderNo,CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,CM.[CustomerName] AS CustomerName,             
SM.StatusType AS Status,OM.[Status] As StatusCode,OM.[GrandTotal],(emp.FirstName + ' '+ emp.LastName) as SalesPerson,                         
--Satart                                                                                
(Select ShippingType from ShippingType Where AutoId=om.ShippingType) as ShippingType,OM.ShippingType as ShipId,                                                                                  
--End 30-08-2019 By Rizwan Ahmad Add Shipping Type col                                                               
(SELECT COUNT(distinct [ProductAutoId]) from [dbo].[OrderItemMaster] WHERE OrderAutoId=OM.AutoId) AS NoOfItems                                             
FROM [dbo].[OrderMaster] As OM                                                                                                                                              
INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.AutoId = OM.CustomerAutoId                                         
INNER JOIN [dbo].[EmployeeMaster] AS emp ON emp.AutoId = OM.SalesPersonAutoId                          
INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.[Status] AND SM.Category = 'Website'
WHERE (@OrderNo is null or @OrderNo ='' or OrderNo like '%' + @OrderNo + '%') 
and OM.OrderType=4 AND OM.Status=0
and (@CustomerAutoId is null or @CustomerAutoId =0 or CustomerAutoId =@CustomerAutoId)                                      
and (@FromDate is null or @FromDate = '' or @Todate is null or @Todate = '' or                                                                                  
(CONVERT(date,OM.[OrderDate])  between CONVERT(date,@FromDate) and CONVERT(date,@Todate)))                                                                                                   
and(@SalesPersonAutoId = 0 or om.[SalesPersonAutoId]=@SalesPersonAutoId)                                                                        
)as t order by   [OrderNo] DESC                                                                                                                           
                          
SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize=0 then COUNT(RowNumber) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Results                                                                                                   
  
        
SELECT * FROM #Results                                                                                                                                              
WHERE (ISNULL(@PageSize,0)=0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))                                                                    
END            
   	 else if @Opcode=44
	begin
		IF EXISTS(SELECT [SecurityValue] FROM [dbo].[TBL_MST_SecurityMaster] WHERE [SecurityValue]=@CheckSecurity and typeEvent=3)    
			begin
				set @isException=0
			end
			else
			begin
				set @isException=1
			end 
			
	end          
END TRY                                                                
BEGIN CATCH                                                 
Set @isException=1                                    
Set @exceptionMessage='Oops! Something went wrong.Please try later.'                                                                                                                                              
END CATCH                                                                                 
END 
GO
