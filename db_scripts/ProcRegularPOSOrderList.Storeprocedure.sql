create PROCEDURE [dbo].[ProcRegularPOSOrderList]                                                                   
@Opcode INT=NULL,                                                                    
@OrderAutoId INT=NULL,                                                                                                                                     
@OrderNo VARCHAR(15)=NULL,                                                                    
@SalesPersonAutoId INT=NULL, 
@LoginEmpType INT=NULL,
@EmpAutoId INT=NULL,                                                                    
@OrderStatus INT=NULL,                                                                    
@ShippingType INT=NULL,                                                                                                                                                                                                                                     
@Todate DATETIME=NULL,                          
@Fromdate DATETIME=NULL,                                                                                                       
@CustomerAutoId INT=NULL,                                                                    
@PageIndex INT = 1,                                                                    
@PageSize INT = 10,                                                                                                                                      
@RecordCount INT =null,                                     
@CheckSecurity varchar(30)=NULL,
@DraftAutoId int =null out, 
@isException bit out,                                    
@exceptionMessage varchar(max) out                                                                    
AS                                                                    
BEGIN                                                              
  BEGIN TRY                                                                    
  Set @isException=0                                                                    
  Set @exceptionMessage='Success'                                                                                                                                      
 IF @Opcode=41                                                                    
   BEGIN    
	   IF EXISTS(SELECT [AutoId] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId AND ([PaymentRecev] IS NOT NULL OR [PaymentRecev] != ''))                      
	   BEGIN  	
			SELECT ROW_NUMBER() OVER(ORDER BY [AutoId] desc) AS RowNumber, * INTO #Results30 from                                                                    
			(                                                                       
			SELECT OM.[AutoId],OM.OrderNo,CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,CM.[CustomerName] AS CustomerName,                                                                    
			SM.StatusType AS Status,OM.[Status] As StatusCode,OM.[GrandTotal],                                                                    
			(SELECT COUNT(distinct [ProductAutoId]) from [dbo].Delivered_Order_Items WHERE OrderAutoId=OM.AutoId) AS NoOfItems  ,                                    
			st.ShippingType                                                                   
			FROM [dbo].[OrderMaster] As OM                                                         
			INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.AutoId = OM.CustomerAutoId                                      
			INNER JOIN [dbo].ShippingType AS st ON st.AutoId = OM.ShippingType                                          
			INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.[Status] AND SM.Category = 'OrderMaster'    
			WHERE                                                         
			OM.OrderType=6                                                                   
			and (@OrderNo is null or @OrderNo ='' or OrderNo like '%' + @OrderNo + '%')                                                                    
			and (@CustomerAutoId is null or @CustomerAutoId =0 or CustomerAutoId =@CustomerAutoId)                                                                    
			and (@FromDate is null or @FromDate = '' or @Todate is null or @Todate = '' or (Convert(date,[OrderDate]) between @FromDate and @Todate))                                                                           
			and OM.POSAutoId=@SalesPersonAutoId                                                                    
			and (@OrderStatus is null or @OrderStatus=0 or OM.Status=@OrderStatus)                                         
			and (@ShippingType is null or @ShippingType=0 or OM.ShippingType=@ShippingType)                                                                  
                                 
			)as t order by [AutoId] desc                                                   
                                                                    
			SELECT CASE WHEN ISNULL(@PageSize,0)=0 then 0 else COUNT(*)  end as RecordCount,@PageSize as PageSize,@PageIndex as PageIndex FROM #Results30                                                                    
			SELECT * FROM #Results30                                                                    
			WHERE ISNULL(@PageSize,0)=0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1) 
	   END
	   ELSE
	   BEGIN
			SELECT ROW_NUMBER() OVER(ORDER BY [AutoId] desc) AS RowNumber, * INTO #Results31 from                                                                    
			(                                                                       
			SELECT OM.[AutoId],OM.OrderNo,CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,CM.[CustomerName] AS CustomerName,                                                                    
			SM.StatusType AS Status,OM.[Status] As StatusCode,OM.[GrandTotal],                                                                    
			(SELECT COUNT(distinct [ProductAutoId]) from [dbo].[OrderItemMaster] WHERE OrderAutoId=OM.AutoId) AS NoOfItems  ,                                    
			st.ShippingType                                                                   
			FROM [dbo].[OrderMaster] As OM                                                         
			INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.AutoId = OM.CustomerAutoId                                      
			INNER JOIN [dbo].ShippingType AS st ON st.AutoId = OM.ShippingType                                          
			INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.[Status] AND SM.Category = 'OrderMaster'                                                      
			WHERE                                                         
			OM.OrderType=6                                                                    
			and (@OrderNo is null or @OrderNo ='' or OrderNo like '%' + @OrderNo + '%')                                                                    
			and (@CustomerAutoId is null or @CustomerAutoId =0 or CustomerAutoId =@CustomerAutoId)                                                                    
			and (@FromDate is null or @FromDate = '' or @Todate is null or @Todate = '' or (Convert(date,[OrderDate]) between @FromDate and @Todate))                                                                           
			and OM.POSAutoId=@SalesPersonAutoId                                                                    
			and (@OrderStatus is null or @OrderStatus=0 or OM.Status=@OrderStatus)                                         
			and (@ShippingType is null or @ShippingType=0 or OM.ShippingType=@ShippingType)                                                                  
                                 
			)as t order by [AutoId] desc                                                   
                                                                    
			SELECT CASE WHEN ISNULL(@PageSize,0)=0 then 0 else COUNT(*)  end as RecordCount,@PageSize as PageSize,@PageIndex as PageIndex FROM #Results31                                                                    
			SELECT * FROM #Results31                                                                    
			WHERE ISNULL(@PageSize,0)=0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)                          
	  END
  END 
  ELSE IF @Opcode=42                                                                    
	BEGIN   
		SELECT
		(
			SELECT cm.[AutoId] as  A,[CustomerId] + ' - ' + [CustomerName] +' ['+ct.CustomerType+']' As C FROM [dbo].[CustomerMaster] as cm                                            
			inner join CustomerType as ct on cm.CustomerType=ct.AutoId   where status=1 
			order by C
			for json path
		) as CustomerList,		
		(
			SELECT AutoId, ShippingType as ST,EnabledTax from shippingType where Shippingstatus=1
			order by ST
			for json path
		) as ShippingType		
		for json path
	END 
	ELSE IF @Opcode=43                                                            
    BEGIN                                                                     
     SELECT ROW_NUMBER() OVER(ORDER BY [DraftAutoId] desc) AS RowNumber, * INTO #Results1 from                                                                  
     (                                                                       
     SELECT OM.[DraftAutoId],(EMP.FirstName+' '+EMP.LastName ) AS EmpName , CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,CONVERT(VARCHAR(20), OM.DeliveryDate, 101) AS DeliveryDate                                                                  
  
     ,CM.[CustomerName] AS CustomerName,                                                   
      'Draft'  AS Status,                                                                  
     (SELECT COUNT(distinct [ProductAutoId]) from [dbo].[DraftItemMaster] WHERE DraftAutoId=OM.DraftAutoId) AS NoOfItems                                                                     
     FROM [dbo].[DraftOrderMaster] As OM                                                                    
     LEFT JOIN [dbo].[CustomerMaster] AS CM ON CM.AutoId = OM.CustomerAutoId                                                                            
     INNER JOIN EmployeeMaster AS EMP ON EMP.AutoId=OM.EmpAutoId                                                                     
                                                                                 
     WHERE OM.OrderType=6                                                                    
     and (@FromDate is null or @FromDate = '' or @Todate is null or @Todate = '' or (Convert(date,[OrderDate]) between @FromDate and @Todate))                                                                        
     and                                                                    
     OM.EmpAutoId=@EmpAutoId                              
     and (@CustomerAutoId=0 or OM.CustomerAutoId=@CustomerAutoId)                                                                    
     )as t order by [DraftAutoId]                                                                    
                                                                    
     SELECT COUNT(*) as RecordCount,@PageSize as PageSize,@PageIndex as PageIndex FROM #Results1                                                                    
     SELECT * FROM #Results1                                        
     WHERE (@PageSize=0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))                                      
     SELECT EType.AutoId as EmpType from  EmployeeMaster AS EMP                    
  INNER JOIN EmployeeTypeMaster AS EType ON EType.AutoId=EMP.EmpType  where EMP.AutoId=@EmpAutoId                                                          
                         
       END 
	ELSE IF @Opcode=31                                                                    
	BEGIN                                                                    
		DELETE FROM DraftItemMaster WHERE DraftAutoId=@DraftAutoId                                                                    
		DELETE FROM DraftOrderMaster WHERE DraftAutoId=@DraftAutoId                                                                    
	END   
	ELSE IF @Opcode=45
	BEGIN
	    IF EXISTS(SELECT [SecurityValue] FROM [dbo].[TBL_MST_SecurityMaster] WHERE [SecurityValue]=@CheckSecurity and SecurityType=8 and typeEvent=2)                                
        BEGIN                                                                                     
			SET @isException=0
			SET @exceptionMessage='success'
        END
		ELSE
		BEGIN
		    SET @isException=1
			SET @exceptionMessage='Invalid'
		END
	END
	
  END TRY                                                                    
  BEGIN CATCH                               
    Set @isException=1                                                                    
    Set @exceptionMessage='Oops! Something went wrong.Please try later.'            
  END CATCH                                           
END 
GO
