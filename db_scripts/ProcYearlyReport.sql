Alter PROCEDURE [dbo].[ProcYearlyReport]                                                
@Opcode INT =null,                                                  
@SelectYear varchar(10) = null,  
@Customer int=null,
@PageIndex int=null,                                                   
@PageSize  int=10,                                                   
@RecordCount int=null,                                                   
@isException bit out,                                                    
@exceptionMessage varchar(max) out                                                  
as                                                
begin                                                  
begin try                                                  
	SET @exceptionMessage= 'Success'                                                    
	SET @isException=0   
	DECLARE @Query nvarchar(MAX),@ParentDb varchar(25),@ChildDb varchar(25)
	SET @ParentDb=(Select DB_NAME())
	SET @ChildDb=(Select ChildDB_Name FROm CompanyDetails)
   if @Opcode=40
   begin
		Select * from 
		(Select (SELECT AutoId,CustomerId + ' ' + CustomerName as CustomerName FROM CustomerMaster order by CustomerName asc 
		for json path) as CustomerList,
		(select Year(LiveDate) as LiveDate from CompanyDetails) as YearList
		) as t for json path
   end
   if @Opcode = 41                                                
   begin  
        SET @Query='
		Select AutoId,CustomerAutoId,OrderDate into  #OrderMaster FROM ['+@ParentDb+'].dbo.OrderMaster as om
	    where  om.Status in (11) and ('+Convert(varchar(25),@Customer)+' is null or '+Convert(varchar(25),@Customer)+'=0 
		or om.CustomerAutoId='+Convert(varchar(25),@Customer)+')'
		IF @SelectYear!='0'
		BEGIN
		    SET @Query=@Query+'and YEAR(om.OrderDate) = YEAR('''+Convert(varchar(30),@SelectYear)+''')'		
		END
        SET @Query=@Query+'
	    Select AutoId,CustomerAutoId,OrderDate into #COrderMaster FROM ['+@ChildDb+'].dbo.OrderMaster as om
	    where om.Status in (11) and ('+Convert(varchar(25),@Customer)+' is null or '+Convert(varchar(25),@Customer)+'=0 
		or om.CustomerAutoId='+Convert(varchar(25),@Customer)+')'
		IF @SelectYear!='0'
		BEGIN
		    SET @Query=@Query+'and YEAR(om.OrderDate) = YEAR('''+Convert(varchar(30),@SelectYear)+''')'		
		END
        SET @Query=@Query+'
		SELECT cm.CustomerName,em.FirstName[SalesRep],YEAR(om.OrderDate)[YearName],MONTH(om.OrderDate)[MonthNo],DATENAME(MONTH,
		om.OrderDate)[MonthName],count(1)[Orders],sum(omm.PayableAmount)[Total] 
		into #t1 FROM ['+@ParentDb+'].dbo.CustomerMaster as cm
		inner join #OrderMaster as om on om.CustomerAutoId = cm.AutoId
		inner join ['+@ParentDb+'].dbo.OrderMaster as omm on omm.AutoId=om.AutoId
		inner join ['+@ParentDb+'].dbo.EmployeeMaster as em on em.AutoId = cm.SalesPersonAutoId
		where cm.CustomerType !=3 and ('+Convert(varchar(25),@Customer)+' is null or '+Convert(varchar(25),@Customer)+'=0 
		or om.CustomerAutoId='+Convert(varchar(25),@Customer)+')
		group by cm.CustomerName,em.FirstName,YEAR(om.OrderDate),MONTH(om.OrderDate),DATENAME(MONTH,om.OrderDate)

		UNION ALL

		SELECT cm.CustomerName,em.FirstName[SalesRep],YEAR(om.OrderDate)[YearName],MONTH(om.OrderDate)[MonthNo],
		DATENAME(MONTH,om.OrderDate)[MonthName]
		,count(1)[Orders],sum(omm.PayableAmount)[Total] 
		FROM ['+@ChildDb+'].dbo.CustomerMaster as cm
		inner join #COrderMaster as om on om.CustomerAutoId = cm.AutoId
		inner join ['+@ChildDb+'].dbo.OrderMaster as omm on omm.AutoId=om.AutoId
		inner join ['+@ParentDb+'].dbo.EmployeeMaster as em on em.AutoId = cm.SalesPersonAutoId
		where cm.CustomerType !=3 and ('+Convert(varchar(25),@Customer)+' is null or '+Convert(varchar(25),@Customer)+'=0 
		or om.CustomerAutoId='+Convert(varchar(25),@Customer)+')
		group by cm.CustomerName,em.FirstName,YEAR(om.OrderDate),MONTH(om.OrderDate),DATENAME(MONTH,om.OrderDate)

		order by CustomerName,SalesRep,MonthNo

		select CustomerName,SalesRep ,isnull(January,0)January,isnull(February,0)February,isnull(March,0)March,
		isnull(April,0)April,isnull(May,0)May
		,isnull(June,0)June,isnull(July,0)July,isnull(August,0)August
		,isnull(September,0)September,isnull(October,0)October,isnull(November,0)November,isnull(December,0) December
		into #t2 from #t1
		PIVOT
		(
		MAX(Orders) 
		FOR  [MonthName]
		IN (
		January,February,March,April,May,June,July,August,September,October,November,December
		)
		) AS P
		select CustomerName,SalesRep,sum(January)JanOrd,sum(February)FebOrd,sum(March)MarOrd,sum(April)AprOrd,sum(May)MayOrd
		,sum(June)JunOrd,sum(July)JulOrd,sum(August)AugOrd,sum(September)SepOrd,sum(October)OctOrd,sum(November)NovOrd,
		sum(December)DecOrd
		into #t22 from #t2 
		group by CustomerName,SalesRep

		select CustomerName,SalesRep ,isnull(January,0)January,isnull(February,0)February,isnull(March,0)March,isnull(April,0)April,
		isnull(May,0)May
		,isnull(June,0)June,isnull(July,0)July,isnull(August,0)August
		,isnull(September,0)September,isnull(October,0)October,isnull(November,0)November,isnull(December,0) December
		into #t3 from #t1
		PIVOT
		(
		MAX([Total]) 
		FOR  [MonthName]
		IN (
			January,February,March,April,May,June,July,August,September,October,November,December
			)
		) AS P
					
		select CustomerName,SalesRep,sum(January)JanTotal,sum(February)FebTotal,sum(March)MarTotal,sum(April)AprTotal,sum(May)MayTotal
		,sum(June)JunTotal,sum(July)JulTotal,sum(August)AugTotal,sum(September)SepTotal,sum(October)OctTotal,sum(November)NovTotal,
		sum(December)DecTotal
		into #t33 from #t3 
		group by CustomerName,SalesRep
		select  ROW_NUMBER() over (Order by t2.CustomerName ) as RowNumber , t2.CustomerName,t2.SalesRep
		,t2.JanOrd,t3.JanTotal,t2.FebOrd,t3.FebTotal,t2.MarOrd,t3.MarTotal,t2.AprOrd,t3.AprTotal,t2.MayOrd,t3.MayTotal,t2.JunOrd
		,t3.JunTotal,t2.JulOrd,t3.JulTotal,t2.AugOrd,t3.AugTotal,t2.SepOrd,t3.SepTotal,t2.OctOrd,t3.OctTotal,t2.NovOrd,t3.NovTotal
		,t2.DecOrd,t3.DecTotal
		,(t2.JanOrd+t2.FebOrd+t2.MarOrd+t2.AprOrd+t2.MayOrd+t2.JunOrd+t2.JulOrd+t2.AugOrd+t2.SepOrd+t2.OctOrd+NovOrd+DecOrd)[YearOrders]
		,(JanTotal+FebTotal+MarTotal+AprTotal+MayTotal+JunTotal+JulTotal+AugTotal+SepTotal+OctTotal+NovTotal+DecTotal)[YearTotal]
		into #Results from #t22 as t2 
		inner join #t33 as t3
		on t2.CustomerName = t3.CustomerName and t2.SalesRep = t3.SalesRep

		Select sum(JanOrd)JanOrd,sum(JanTotal)JanTotal,sum(FebOrd)FebOrd,sum(FebTotal)FebTotal,sum(MarOrd)MarOrd,sum(MarTotal)MarTotal,
		sum(AprOrd)AprOrd,sum(AprTotal)AprTotal,sum(MayOrd)MayOrd,sum(MayTotal)MayTotal,sum(JunOrd)JunOrd
		,sum(JunTotal)JunTotal,sum(JulOrd)JulOrd,sum(JulTotal)JulTotal,sum(AugOrd)AugOrd,sum(AugTotal)AugTotal,sum(SepOrd)SepOrd,
		sum(SepTotal)SepTotal,sum(OctOrd)OctOrd,sum(OctTotal)OctTotal,sum(NovOrd)NovOrd,sum(NovTotal)NovTotal
		,sum(DecOrd)DecOrd,sum(DecTotal)DecTotal,sum(YearOrders)YearOrders,sum(YearTotal)YearTotal from #Results

		SELECT COUNT(*) AS RecordCount, case when '+Convert(varchar(25),@PageSize)+'=0 then COUNT(*) else
		'+Convert(varchar(25),@PageSize)+' end AS PageSize,
		'+Convert(varchar(10),@PageIndex)+' AS PageIndex,Format(getdate(),''MM/dd/yyyy hh:mm tt'') as PrintDate FROM #Results  
		Select * from #Results 
		WHERE '+Convert(varchar(25),@PageSize)+' = 0 or (RowNumber BETWEEN('+Convert(varchar(10),@PageIndex)+' -1) * '+Convert(varchar(25),@PageSize)+'
		+ 1 AND((('+Convert(varchar(25),@PageIndex)+' -1) * '+Convert(varchar(25),@PageSize)+' + 1) + '+Convert(varchar(25),@PageSize)+') - 1)   
		order by YearTotal desc'
		EXEC sp_executesql @Query
   end                                        
   end try                                                  
  begin catch                             
		SET @isException=1                                                  
		SET @exceptionMessage= ERROR_MESSAGE()                                                   
  end catch                                                  
end 
