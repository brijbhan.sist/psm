Alter procedure ProcManagePrice                                                                                                                                                                          
@OpCode int=Null, 
@EmpAutoId int=null,
@ProductAutoIdData ProductAutoIdData  readonly,
@ProductId int=null,
@ProductName varchar(200)=null,
@ProductAutoid int=null,
@Qty int=null,
@CostPrice decimal(18,2)=null,
@Price decimal(18,2)=null,
@MinPrice decimal(18,2)=NULL,
@WMinPrice DECIMAL(18,2)=NULL,
@ReOrderMark int = null,
@BrandAutoId int=Null,  
@CategoryAutoId int=Null,                                                                                                                                                                                
@SubcategoryAutoId  int=Null,
@SRP decimal(18,2)=null,
@CostPriceCompare int = null,
@PageIndex INT = 1,                                                                                                                  
@PageSize INT = 10,                                                                                                                                                      
@RecordCount INT =null, 
@isException bit out,                                                                                                                                                       
@exceptionMessage varchar(max) out                            
AS
BEGIN	
    SET @isException=0
	SET @exceptionMessage='success'
	Declare @SRPMessage varchar(MAX)
	IF @OpCode=101
	BEGIN
		SELECT ROW_NUMBER() OVER(ORDER BY ProductId) AS RowNumber, * INTO #Results from                      
		( 
		SELECT pm.AutoId,pm.ProductId,pm.ProductName,um.AutoId as UnitType,UM.UnitType AS UnitName,
		ISNULL(Qty,1) AS Qty,isnull(MinPrice,0.00)as MinPrice,pm.P_SRP as SRP,CAST((pm.Stock/pd.Qty) as decimal(14,2)) as Stock,
		isnull(CostPrice,0.00) as CostPrice,isnull(Price,0)as Price,isnull(WHminPrice,0) as WHminPrice, CAST((pm.ReOrderMark) as int)/ISNULL(Qty,1) as ReOrderMark,  
		isnull(PD.AutoId,0) as PackingId,pd.AutoId as PackingAutoId FROM UnitMaster AS UM                                                                           
		inner JOIN PackingDetails AS PD ON UM.AutoId=PD.UnitType              
		inner JOIN ProductMaster as pm on pm.AutoId=pd.ProductAutoId 
		where PD.UnitType=pm.PackingAutoId and  ( @CostPriceCompare = 0 or ( CostPrice = case when @CostPriceCompare = 1 then WHminPrice 
	    when @CostPriceCompare = 2 and CostPrice = MinPrice then WHminPrice 
	    when @CostPriceCompare = 3 and CostPrice = MinPrice  and CostPrice=WHminPrice then Price end ))
		AND (@ProductName='' or @ProductName is null or PM.ProductName like '%' + @ProductName + '%') 
		AND(@ProductId=0 or @ProductId is null or PM.ProductId like '%'+Convert(varchar(15),@ProductId)+'%')
		AND(@CategoryAutoId IS NULL OR @CategoryAutoId='' OR PM.[CategoryAutoId] = @CategoryAutoId)                                                                                                                                                                                                                                         
		AND(@SubcategoryAutoId IS NULL OR @SubcategoryAutoId='' OR PM.[SubcategoryAutoId] = @SubcategoryAutoId)  
		AND(@BrandAutoId IS NULL OR @BrandAutoId='' OR PM.BrandAutoId = @BrandAutoId)  
		AND PM.ProductStatus=1
		 
		) as t order by ProductId

		SELECT COUNT(RowNumber) AS RecordCount,case when @PageSize=0 then COUNT(ProductId) else @PageSize end AS PageSize,
		@PageIndex as PageIndex FROM #Results           
                                    
		SELECT * FROM #Results                                    
		WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) *
		@PageSize + 1) + @PageSize) - 1))                                                                      
 	END
	if @OpCode=102
	begin
	   BEGIN TRY
		   BEGIN TRANSACTION
		        
				IF EXISTS(SELECT PD.AutoId FROM PackingDetails as PD
				INNER JOIN ProductMaster as PM on PD.ProductAutoId=PM.AutoId AND PD.UnitType=PM.PackingAutoId
				WHERE PM.AutoId = @ProductAutoId AND @SRP<cast(@Price/PD.Qty as decimal(10,4)))
				BEGIN
					SET @isException=1
					SET @exceptionMessage='SRP can not be less than Base Price'
				END
				ELSE
				BEGIN
				
				Select PD.UnitType,Price,MinPrice,CostPrice,WHminPrice into #result102 from PackingDetails as PD where ProductAutoId=@ProductAutoId 

				INSERT INTO PackingDetailsUpdateLog  (ProductAutoId,UnitType,Price,MinPrice,CostPrice,WHminPrice,
				UpdatedByAutoId,DateTime,Reff_Code) 
				Select @ProductAutoid,PD.UnitType,Price,MinPrice,CostPrice,WHminPrice,@EmpAutoId,GetDate(),'ProcManagePrice-102' 
				from PackingDetails as PD
				where ProductAutoId=@ProductAutoId 

				

				UPDATE PackingDetails SET                                 
				Price=CAST((@Price/@Qty)*Qty AS decimal(10,4)),MinPrice=CAST((@MinPrice/@Qty)*Qty AS decimal(10,4)),                                    
				CostPrice=CAST((@CostPrice/@Qty)*Qty AS decimal(10,4)),                                
				WHminPrice=CAST((@WMinPrice/@Qty)*Qty AS decimal(10,4)),
				ModifiedBy = @EmpAutoId, UpdateDate=GETDATE()  where ProductAutoId =@ProductAutoId

				Update ProductMaster SET ModifiedBy = @EmpAutoId ,ModifiedDate = GETDATE(),NewSRP=@SRP 				 
				where AutoId = @ProductAutoId
				
				IF EXISTS(Select AutoId from PackingDetails as pd inner join #result102 as temp on temp.UnitType=pd.UnitType
				where ProductAutoId=@ProductAutoid AND pd.Price!=temp.Price)
				BEGIN 
					insert into ProductPricingInPriceLevel_log([AutoId],[PriceLevelAutoId],[ProductAutoId],[UnitAutoId],[CustomPrice],
					[createDated],[UpdatedBy])
					SELECT [AutoId],[PriceLevelAutoId],[ProductAutoId],[UnitAutoId],[CustomPrice],GETDATE(),@EmpAutoId FROM
					ProductPricingInPriceLevel WHERE ProductAutoId=@ProductAutoId order by AutoId desc

					DELETE FROM [ProductPricingInPriceLevel] WHERE ProductAutoId=@ProductAutoId
				END

				END
			   Exec ProcUpdatePriceEmailSend @ProductAutoId=@ProductAutoId 
		   COMMIT TRANSACTION
	   END TRY
	   BEGIN CATCH
	      ROLLBACK TRANSACTION
	      SET @isException=1
		  SET @exceptionMessage=ERROR_MESSAGE()
	   END CATCH
	end
    if @Opcode = 201 
	Begin
		UPDATE [dbo].[ProductMaster] SET 
		ReOrderMark = (@ReOrderMark) ,ModifiedBy = @EmpAutoId ,ModifiedDate = GETDATE()                                                                                                  
		WHERE  AutoId=@ProductAutoid
	END
	if @Opcode = 202 
	Begin
		Declare @TempTable TABLE (ID INT IDENTITY(1, 1),ProductAutoId INT)
		Declare @count int,@num int
		IF EXISTS(SELECT PD.AutoId FROM PackingDetails as PD
		INNER JOIN ProductMaster as PM on PD.ProductAutoId=PM.AutoId AND PD.UnitType=PM.PackingAutoId
		WHERE PM.AutoId IN (select ProductAutoId from @ProductAutoIdData) AND @SRP<cast(@Price/PD.Qty as decimal(10,4)))
		BEGIN
			SET @isException=1
			SET @exceptionMessage='SRP'
		END
		ELSE
		BEGIN
				  BEGIN TRY
				   BEGIN TRANSACTION

						Select pd.ProductAutoId,PD.UnitType,Price,MinPrice,CostPrice,WHminPrice into #result202 from PackingDetails as PD where ProductAutoId=@ProductAutoId

						INSERT INTO PackingDetailsUpdateLog  (ProductAutoId,UnitType,Price,MinPrice,CostPrice,WHminPrice,
						UpdatedByAutoId,DateTime,Reff_Code) 
						Select @ProductAutoid,PD.UnitType,Price,MinPrice,CostPrice,WHminPrice,@EmpAutoId,GetDate(),'ProcManagePrice-202' 
						from PackingDetails as PD
						where ProductAutoId=@ProductAutoId 

						

						UPDATE pd set 
						Price=CAST((@Price/convert(decimal(10,4),(select pdi.Qty from PackingDetails as pdi where pdi.ProductAutoId=pd.ProductAutoId and pdi.UnitType=pm.PackingAutoId)))*Qty AS decimal(10,4)),
						MinPrice=CAST((@MinPrice/convert(decimal(10,4),(select pdi.Qty from PackingDetails as pdi 
						where pdi.ProductAutoId=pd.ProductAutoId and pdi.UnitType=pm.PackingAutoId)))*Qty AS decimal(10,4)),                                    
						CostPrice=CAST((@CostPrice/convert(decimal(10,4),(select pdi.Qty from PackingDetails as pdi 
						where pdi.ProductAutoId=pd.ProductAutoId and pdi.UnitType=pm.PackingAutoId)))*Qty AS decimal(10,4)),                                
						WHminPrice=CAST((@WMinPrice/convert(decimal(10,4),(select pdi.Qty from PackingDetails as pdi
						where pdi.ProductAutoId=pd.ProductAutoId and pdi.UnitType=pm.PackingAutoId)))*Qty AS decimal(10,4))
						from PackingDetails as pd 
						inner join productMaster as pm on pd.ProductAutoId=pm.AutoId
						where ProductAutoId  in (select ProductAutoId from @ProductAutoIdData)
			
						Update ProductMaster SET --ReOrderMark=@ReOrderMark,
						ModifiedBy = @EmpAutoId ,ModifiedDate = GETDATE(),NewSRP=@SRP 				 
						where AutoId IN  (select ProductAutoId from @ProductAutoIdData)

						IF EXISTS(SELECT PD.ProductAutoId FROM PackingDetails AS PD INNER JOIN #result202 AS TEMP ON TEMP.ProductAutoId=PD.ProductAutoId
						AND TEMP.UnitType=PD.UnitType AND PD.Price!=TEMP.Price)
						BEGIN
							insert into ProductPricingInPriceLevel_log([AutoId],[PriceLevelAutoId],[ProductAutoId],[UnitAutoId],[CustomPrice],
							[createDated],[UpdatedBy])
							Select pp.AutoId,pp.PriceLevelAutoId,pp.ProductAutoId,pp.UnitAutoId,CustomPrice,GetDate(),@EmpAutoId FROM ProductPricingInPriceLevel as pp 
							Where ProductAutoId IN (SELECT PD.ProductAutoId FROM PackingDetails AS PD INNER JOIN #result202 AS TEMP ON TEMP.ProductAutoId=PD.ProductAutoId
							AND TEMP.UnitType=PD.UnitType AND PD.Price!=TEMP.Price)

							Delete FROM ProductPricingInPriceLevel Where ProductAutoId IN 
							(SELECT PD.ProductAutoId FROM PackingDetails AS PD INNER JOIN #result202 AS TEMP ON TEMP.ProductAutoId=PD.ProductAutoId
							AND TEMP.UnitType=PD.UnitType AND PD.Price!=TEMP.Price)
						END
						INSERT into @TempTable   
						select ProductAutoId from @ProductAutoIdData

						SET @count= (select count(1) from @ProductAutoIdData)
						SET @num=1

						While(@num<=@Count)
						BEGIN
							SET @ProductAutoId=(Select ProductAutoId FROM @TempTable where ID=@num) 
							Exec ProcUpdatePriceEmailSend @ProductAutoId=@ProductAutoId
							set @num = @num + 1
						END
					COMMIT TRANSACTION
					END TRY
					BEGIN CATCH
					ROLLBACK TRANSACTION
					SET @isException=1
					SET @exceptionMessage=ERROR_MESSAGE()
					END CATCH

		END
	END
	
	if @Opcode = 203 
	Begin
		update pm set ReOrderMark=@ReOrderMark*ISNULL(Qty,1),
		ModifiedBy = @EmpAutoId ,ModifiedDate = GETDATE() from ProductMaster as pm
		inner join PackingDetails as pd on pd.ProductAutoId=pm.AutoId and pd.UnitType=pm.PackingAutoId
		where pm.AutoId IN  (select ProductAutoId from @ProductAutoIdData)
	END
	
	
	IF @OpCode=41 
	BEGIN 
		select 
		(
		SELECT [AutoId] as CAID, [CategoryName] as CAN FROM [dbo].[CategoryMaster] WHERE [Status]=1 ORDER BY  CAN ASC  
		for json path
		) as Category,
		(
		SELECT AutoId as VID,VendorName as VN FROM VENDORMASTER WHERE Status=1     order by  VN asc 
		for json path
		)as Vendor,
		(
		SELECT AutoId as BID,BrandName as BN FROM BrandMaster WHERE Status=1     order by  BN asc  
		for json path
		)as Brand,
		(
		SELECT AutoId as COID,CommissionCode as COC from Tbl_CommissionMaster where Status=1   order by COC asc
		for json path
		)as Commission
		for json path
	END                                                 
    IF @Opcode=42                                                                                                                                                                                
	 BEGIN                                                                         
		 SELECT [AutoId] as SCAID, [SubcategoryName] as SCAN FROM [dbo].[SubCategoryMaster] WHERE [Status]=1 AND [CategoryAutoId]=@CategoryAutoId  ORDER BY  SCAN ASC  
		 for json path
	 END  
END