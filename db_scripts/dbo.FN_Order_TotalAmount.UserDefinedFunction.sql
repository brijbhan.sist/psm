USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Order_TotalAmount]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
alter table OrderMaster drop column TotalAmount
drop function FN_Order_TotalAmount
go
	CREATE FUNCTION  [dbo].[FN_Order_TotalAmount]  
	(  
	  @orderAutoId int  
	)  
	RETURNS decimal(18,2)  
	AS  
	BEGIN  
		 DECLARE @TotalAmount decimal(18,2)=0.00  
		 IF EXISTS(SELECT 1 FROM OrderMaster WHERE AutoId=@orderAutoId AND Status IN (11))  
		 BEGIN  
			SET @TotalAmount=isnull((SELECT SUM(NetPrice) FROM Delivered_Order_Items WHERE OrderAutoId=@orderAutoId),0)   
		 END  
		 ELSE IF EXISTS(SELECT 1 FROM DeliveredOrders AS DO INNER JOIN OrderMaster AS OM ON OM.AutoId=DO.OrderAutoId AND Status=8 WHERE OrderAutoId=@orderAutoId)  
		 BEGIN  
			SET @TotalAmount=ISNULL((SELECT SUM(NetPrice) FROM Delivered_Order_Items WHERE OrderAutoId=@orderAutoId),0)  
		 END  
		 ELSE  
		 BEGIN  
			SET @TotalAmount=ISNULL((SELECT SUM(NetPrice) FROM OrderItemMaster WHERE OrderAutoId=@orderAutoId),0)   
		 END   
			RETURN @TotalAmount  
	END  
GO
alter table OrderMaster add [TotalAmount]  AS ([dbo].[FN_Order_TotalAmount]([AutoId]))