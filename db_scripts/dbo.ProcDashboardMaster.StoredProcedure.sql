alter PROCEDURE [dbo].[ProcDashboardMaster]    
@Opcode INT=NULL,    
@EmpAutoId INT=NULL,  
@Emptype int =null,
@Type INT=NULL,    
@FROMDATE DATE =NULL ,    
@TODATE DATE =NULL,   
@AutoId int=null,
@isException bit out,    
@exceptionMessage varchar(max) out    
AS    
BEGIN     
 BEGIN TRY    
  Set @isException=0    
  Set @exceptionMessage='Success' 
  IF @Type=0    
  BEGIN    
   SET @FROMDATE =GETDATE()    
   SET @TODATE =GETDATE()    
  END    
  ELSE IF @Type=1    
  BEGIN    
      SELECT @FROMDATE =  DATEADD(DD,-1, GETDATE())    
   SELECT @TODATE = DATEADD(DD,-1, GETDATE())    
  END      
  ELSE IF @Type=2    
  BEGIN    
     SELECT @FROMDATE =DATEADD(mm, DATEDIFF(mm, 0, GETDATE()), 0)    
     SELECT @TODATE =DATEADD (dd, -1, DATEADD(mm, DATEDIFF(mm, 0, GETDATE()) + 1, 0))    
  END     
  ELSE IF @Type=3    
  BEGIN    
     SELECT @FROMDATE =DATEADD(mm, DATEDIFF(mm, 0, (DATEADD(mm, -1, GETDATE()))), 0)    
     SELECT @TODATE =DATEADD (dd, -1, DATEADD(mm, DATEDIFF(mm, 0, GETDATE()), 0))    
  END    
      
  If @Opcode=41      
  BEGIN    
   select * from (    
   select (select COUNT(AutoId)  from OrderMaster where CONVERT(DATE,OrderDate) =CONVERT(date,GETDATE())  and   Status!=8) as TotalOrder,    
   isnull((select SUM(PayableAmount) as TotalOrder from OrderMaster where CONVERT(DATE,OrderDate) =CONVERT(date,GETDATE())  and  Status!=8),0) as TotalSales,    
   (select COUNT(Stock) from ProductMaster where Stock < 1 ) as OutOfStock,    
   (select  COUNT(*) from ProductMaster where Stock< ReOrderMark ) as ThresholdValue    
   ) as t    
       
   select     
     case    
      when sm.AutoId=1 then 1    
      when sm.AutoId=2 then 2    
      when sm.AutoId=3 then 3    
      when sm.AutoId=8 then 4    
      when sm.AutoId=9 then 5    
      when sm.AutoId=10 then 6    
      when sm.AutoId=4 then 7    
      when sm.AutoId=5 then 8    
      when sm.AutoId=6 then 9    
      when sm.AutoId=7 then 10    
     end as statusodrer,    
   sm.StatusType,(select count(om.AutoId) as TotalOrder     
    from OrderMaster  as om  where om.Status=sm.AutoId ) as TotalOrder    
   from StatusMaster sm where Category='OrderMaster' and  sm.AutoId!=11 and sm.AutoId!=8 and  sm.AutoId!=7
   order by statusodrer    
      
   --Start    
         Select Logo from CompanyDetails    
   --End 03-09-2019  By Rizwan Ahmad  
  
  END    
  ELSE IF @Opcode=42    
  BEGIN    
           
   select (em.FirstName+ ' '+ LastName) as SalesPerson,count(GrandTotal) as TotalOrder,    
   sum(PayableAmount) as TotalSales,cast(avg(GrandTotal) as decimal(10,2)) as AOV from OrderMaster  as om     
   inner join EmployeeMaster em on om.SalesPersonAutoId=em.AutoId      
   WHERE om.Status!=8 and CONVERT(DATE,OrderDate) BETWEEN @FROMDATE AND @TODATE    
   group by (em.FirstName+ ' '+ LastName)    
  END    
  ELSE IF @Opcode=43    
  BEGIN        
		SELECT sm.StatusType,totalCount,ReceivedAmount FROM (
		select 1 as Status,count(1) as totalCount,ISNULL(SUM(receivedAmount),0) as ReceivedAmount from SalesPaymentMaster as spm 
		where Status=1 and (@FROMDATE is null or  (CONVERT(DATE,spm.ReceivedDate) BETWEEN @FROMDATE AND @TODATE ))
		UNION
		select 2 as Status,count(1) as totalCount,ISNULL(SUM(spm.receivedAmount),0) as ReceivedAmount from SalesPaymentMaster as spm 
		inner join CustomerPaymentDetails  as cpd on cpd.refPaymentId=spm.PaymentAutoId
		where spm.Status=2 and (@FROMDATE is null or  (CONVERT(DATE,cpd.PaymentDate) BETWEEN @FROMDATE AND @TODATE ))
		UNION
		select 4 as status,count(1) as totalCount,ISNULL(SUM(receivedAmount),0) as ReceivedAmount from SalesPaymentMaster as spm 
		where Status=4 and (@FROMDATE is null or  (CONVERT(DATE,spm.DateUpdate) BETWEEN @FROMDATE AND @TODATE ))
		UNION
		select 3 as Status,count(1) as totalCount,ISNULL(SUM(receivedAmount),0) as ReceivedAmount from SalesPaymentMaster as spm 
		where Status=3 and (@FROMDATE is null or  (CONVERT(DATE,spm.CancelDate) BETWEEN @FROMDATE AND @TODATE ))
		UNION
		select 5 as Status,count(1) as totalCount,ISNULL(SUM(receivedAmount),0) as ReceivedAmount from SalesPaymentMaster as spm 
		where Status=5 and (@FROMDATE is null or  (CONVERT(DATE,spm.CancelDate) BETWEEN @FROMDATE AND @TODATE ))

		) AS T
		INNER JOIN StatusMaster AS SM ON SM.AutoId=T.Status AND SM.Category='Pay'
  END    
  ELSE IF @Opcode=44    
  BEGIN      
   select sm.StatusType,(select count(isnull(om.TotalAmount,0)) from CreditMemoMaster OM where om.Status=sm.AutoId     
   and (@FROMDATE is null or  (CONVERT(DATE,om.CreditDate) BETWEEN @FROMDATE AND @TODATE ))) as  totalCount,       
   isnull((select SUM(isnull(om.TotalAmount,0)) from CreditMemoMaster OM where om.Status=sm.AutoId     
   and (@FROMDATE is null or  (CONVERT(DATE,om.CreditDate) BETWEEN @FROMDATE AND @TODATE ))),0) as ReceivedAmount  from StatusMaster   as SM     
   where Category='CreditMaster'  
  END    
  ELSE IF @Opcode=45    
  BEGIN        
     select cm.AutoId,spm.paymentAutoId,cm.CustomerName,format(ReceiveDate,'MM/dd/yyyy') as ReceiveDate,ReceivedAmount,ChequeNo,
	 format(ChequeDate,'MM/dd/yyyy') as DepositeDate,spm.PayId as PaymentId,
	 em.FirstName+' '+em.LastName as ReceivedBy,ems.FirstName+' '+ems.LastName as HoldBy from SalesPaymentMaster as spm
	 inner join CustomerMaster as cm on cm.AutoId=spm.CustomerAutoId
	 inner join EmployeeMaster as em on em.AutoId=spm.ReceiveBy 
	 left join EmployeeMaster as ems on ems.AutoId=spm.HoldBy 
	 where convert(date,ChequeDate)<=convert(date,GETDATE()) and spm.Status=4
	 and PaymentMode=2
	 order by cm.CustomerName asc,convert(date,ChequeDate) desc

	 select  @Type as EmpType
  END     
  
 ELSE IF @Opcode=46    
  BEGIN        
	select TicketAutoId,TicketID,TicketDate as ticket,FORMAT(TicketDate,'MM/dd/yyyy HH:mm tt' ) as TicketDate 
	,Priority,Type,
	Subject,Description,etm.Status,DeveloperStatus,FORMAT(TicketCloseDate,'MM/dd/yyyy HH:mm tt') as TicketCloseDate,(select firstName
	+' '+LastName from EmployeeMaster where AutoId=etm.ByEmployee) as Fullname  from ErrorTicketMaster as etm  
	inner join EmployeeMaster em on em.AutoId = etm.ByEmployee  
	where etm.Status!='Close' and etm.DeveloperStatus='Close' and etm.ByEmployee=@EmpAutoId
	and TicketDate>getdate()
  end
  ELSE IF @Opcode=47    
  BEGIN  
	SELECT MM.[AutoId] As AutoId,'' as SenderName,Title,Team,Message,CONVERT(varchar(20),mm.CreatedDate,109) as MsgDate FROM [dbo].[MessageMaster] AS MM	
	LEFT JOIN [MessageAssignTo]  as temp1  ON temp1.[MessageAutoId] = MM.[AutoId] where ExpiryDate > getdate() or TeamId=1  or temp1.EmpType=@Emptype  and temp1.EmpAutoId=@EmpAutoId
	UNION
	SELECT MM.[AutoId] As AutoId,'' as SenderName,Title,Team,Message,CONVERT(varchar(20),mm.CreatedDate,109) as MsgDate FROM [dbo].[MessageMaster] AS MM
	LEFT JOIN [dbo].[MessageAssignTo] AS T1 on T1.[MessageAutoId] = MM.[AutoId]	where ExpiryDate > getdate()  or TeamId=1 or T1.EmpType=@Emptype  and T1.EmpType=(select ee.EmpType from EmployeeMaster as ee where ee.AutoId=@EmpAutoId)

	select COUNT(*) as TotalCount from [dbo].[MessageMaster] as mm 
	inner join MessageAssignTo as mat on mat.MessageAutoId=mm.AutoId	
	where EmpAutoId=@EmpAutoId or mat.EmpType=@Emptype  and [IsRead]=0 and mm.ExpiryDate>=GETDATE()

  end
   ELSE IF @Opcode=48    
  BEGIN        
  
	 select mm.AutoId,Message from [dbo].[MessageMaster] as mm
	 where mm.AutoId=@AutoId
	 update [dbo].[MessageMaster] set IsRead=1 where AutoId=@AutoId and Team=2
  end
  ELSE IF @Opcode=49
  BEGIN
		SELECT OM.[AutoId],OM.OrderNo, OM.[OrderDate]  as omdt,CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,CM.[CustomerName] AS CustomerName,             
		SM.StatusType AS Status,OM.[Status] As StatusCode,OM.[GrandTotal],(emp.FirstName + ' '+ emp.LastName) as SalesPerson, 
		(Select ShippingType from ShippingType Where AutoId=om.ShippingType) as ShippingType,OM.ShippingType as ShipId,      
		(SELECT COUNT(distinct [ProductAutoId]) from [dbo].[OrderItemMaster] WHERE OrderAutoId=OM.AutoId) AS NoOfItems                                             
		FROM [dbo].[OrderMaster] As OM                                                                                                                                              
		INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.AutoId = OM.CustomerAutoId                                         
		INNER JOIN [dbo].[EmployeeMaster] AS emp ON emp.AutoId = OM.SalesPersonAutoId                          
		INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.[Status] AND SM.Category = 'Website'
		order by omdt desc
	END
 END TRY    
 BEGIN CATCH    
		Set @isException=1    
		Set @exceptionMessage='Oops! Something went wrong.Please try later'    
 END CATCH    
END     
       
GO
