 
alter PROCEDURE [dbo].[ProcLogin]                      
@Opcode int=NULL,                      
@EmpAutoId int=null,                      
@EmpType int=null,                      
@UserName varchar(100)=null,                      
@PageUrl varchar(250)=null,                     
@Password varchar(max)=null,                      
@NewPassword varchar(max)=null,                      
@IPAddress varchar(max)=null,                      
@isException bit out,                      
@exceptionMessage varchar(max) out                      
AS                      
BEGIN                      
 BEGIN TRY                      
     SET @exceptionMessage= 'Success'                      
     SET @isException=0            
		IF @Opcode=21                       
			BEGIN                      
				IF EXISTS(SELECT [Password] from EmployeeMaster where AutoId=@EmpAutoId and CONVERT(varchar(100),DecryptByPassphrase('WHM',[Password]))=@Password )                      
				BEGIN                      
				UPDATE EmployeeMaster set Password=EncryptByPassPhrase('WHM',@NewPassword) where AutoId=@EmpAutoId                      
				END                      
				else                      
				BEGIN                      
				SET @isException=1                      
				SET @exceptionMessage= 'Old Password Not Match !!!'                      
				END                      
			END                      
                      
		ELSE IF @Opcode=41     
			BEGIN  
				SET @EmpAutoId=(SELECT top 1 AutoId from [dbo].[EmployeeMaster] as EM  WHERE [UserName]=@UserName)  
				IF EXISTS(SELECT * FROM [dbo].[EmployeeMaster] as EM  WHERE [UserName]=@UserName                    
				and em.Status=1                  
				and CONVERT(varchar(100),DecryptByPassphrase('WHM',[Password]))=@Password                    
				and (isApplyIp=0 or IP_Address like '%'+@IPAddress+'%')  )                    
				BEGIN  
				
				execute [dbo].[WaitForDelay]
				SELECT 'success' as response,ProfileName,EM.AutoId,EM.[EmpType] As EmpTypeNo,ETM.TypeName AS [EmpType],                    
				[FirstName] AS Name,[UserName] ,[EmpId] ,Email,(select top 1 Logo from CompanyDetails) as ImageURL,(select top 1 CompanyId from CompanyDetails) as DBLocation,  
				CASE   
				WHEN (select convert(date,SubscriptionExpiryDate) from CompanyDetails) < convert(date,GETDATE()) then   
				'Your subscription expired on' +' '+ (select FORMAT(SubscriptionExpiryDate,'dd MMMM yyyy')+'.' from CompanyDetails)  
				WHEN (select convert(date,SubscriptionAlertDate) from CompanyDetails) <= convert(date,GETDATE()) then   
				'Your subscription will expire on' +' '+ (select FORMAT(SubscriptionExpiryDate,'dd MMMM yyyy')+'.' from CompanyDetails) else   
				'0' end as Submsg,    
				CASE   
				WHEN (select convert(date,SubscriptionExpiryDate) from CompanyDetails) < convert(date,GETDATE()) then 1  
				WHEN (select convert(date,SubscriptionAlertDate) from CompanyDetails) <= convert(date,GETDATE()) then 0 else   
				'1' end as ExpMsg   
				FROM [dbo].[EmployeeMaster] as EM                     
				INNER JOIN [dbo].[EmployeeTypeMaster] AS ETM on EM.EmpType=ETM.AutoId  WHERE [UserName]=@UserName                    
				and em.Status=1                  
				and CONVERT(varchar(100),DecryptByPassphrase('WHM',[Password]))=@Password                    
				and (isApplyIp=0 or IP_Address like '%'+@IPAddress+'%')                    
                
				SET @EmpAutoId=(SELECT top 1 AutoId from [dbo].[EmployeeMaster] as EM  WHERE [UserName]=@UserName                    
				and CONVERT(varchar(100),DecryptByPassphrase('WHM',[Password]))=@Password                    
				)                    
				insert into ipaddress(EmpAutoId,IPAddress,LoginDate,Remarks)                     
				values(@EmpAutoId,@IPAddress,GETDATE(),'login successfully.')    
                 
			end                   
			ELSE
				IF EXISTS(SELECT * FROM [dbo].[EmployeeMaster] as EM  WHERE [UserName]=@UserName                    
				and CONVERT(varchar(100),DecryptByPassphrase('WHM',[Password]))=@Password                     
				and em.Status=0                   
				)                
				BEGIN                
				SET @isException=1                    
				SET @exceptionMessage= 'Access Denied'   
				insert into ipaddress(EmpAutoId,IPAddress,LoginDate,Remarks)                     
				values(@EmpAutoId,@IPAddress,GETDATE(),'Access Denied.')               
			end                 
			ELSE
				IF EXISTS(SELECT * FROM [dbo].[EmployeeMaster] as EM  WHERE [UserName]=@UserName                    
				and em.Status=1                  
				and CONVERT(varchar(100),DecryptByPassphrase('WHM',[Password]))=@Password                    
				and isApplyIp=1)                
				BEGIN                
				SET @isException=1                    
				SET @exceptionMessage= 'Access Denied'   
				
				insert into ipaddress(EmpAutoId,IPAddress,LoginDate,Remarks)                     
				values(@EmpAutoId,@IPAddress,GETDATE(),'Access Denied.')                 
			end                
				ELSE                
				BEGIN                
				SET @isException=1                    
				SET @exceptionMessage= 'Username And / Or Password Incorrect'    
				
				insert into ipaddress(EmpAutoId,IPAddress,LoginDate,Remarks)                     
				values(@EmpAutoId,@IPAddress,GETDATE(),'Username And / Or Password Incorrect.')                
			END        
                  
			END                      
	Else if @Opcode=42                      
			Begin                      
				select logo,CompanyId,(CompanyDistributerName) as Distributors  from CompanyDetails                      
				End    
	ELSE IF @Opcode=43                      
			BEGIN                                                                                                                                    
			SELECT mpa.ModuleAutoId,pm.PageUrl,mpa.AssignedAction,
			(select
			STUFF((SELECT ',' +CONVERT(varchar(50), ActionName) FROM  PageAction
			where AutoId in (SELECT * FROM [dbo].[fnSplitString] (mpa.AssignedAction ,',')) FOR XML PATH ('')), 1, 1, '')) as Action
			from ManagePageAccess mpa
			INNER JOIN PageMaster pm ON pm.ParentModuleAutoId=mpa.ModuleAutoId
			WHERE pm.PageUrl=@PageUrl and mpa.Role=@EmpType                  
			END
	ELSE IF @Opcode=44                      
			BEGIN 
				IF (@EmpType = 1)
				BEGIN
					SELECT m.ModuleName,m.AutoId,m.ParentId,m.HasParent As HasChild ,ISNULL(pm.PageUrl,'#') as PageUrl,pm.Role,m.IconClass,
					(select count(md.AutoId) from Module md WHERE md.ParentId=m.AutoId) as totalChild,SequenceNo
					from Module m
					LEFT JOIN PageMaster pm ON pm.ParentModuleAutoId=m.AutoId
					where m.Status=1 AND m.MenuType!=1
					 ORDER By (case when ISNULL(SequenceNo,0)=0 then 100 else SequenceNo end),ModuleName
				END
				ELSE
				BEGIN
				select * from (
					SELECT m.ModuleName,m.AutoId,m.ParentId,m.HasParent As HasChild ,ISNULL(pm.PageUrl,'#') as PageUrl,pm.Role,m.IconClass,
					(select count(md.AutoId) from Module md WHERE md.ParentId=m.AutoId) as totalChild,mpa.AutoId As AccessAutoId,m.SequenceNo
					from Module m
					inner  JOIN PageMaster pm ON pm.ParentModuleAutoId=m.AutoId
					inner JOIN ManagePageAccess mpa ON mpa.ModuleAutoId=m.AutoId
					WHERE mpa.Role=@EmpType  and m.Status=1
					UNION
					select mm.ModuleName,mm.AutoId,mm.ParentId,mm.HasParent As HasChild ,'#' as PageUrl,0 as Role,mm.IconClass,
					1 as totalChild,0 As AccessAutoId,mm.SequenceNo from Module mm where autoid in
					(
					SELECT ParentId
					from Module m 
					inner JOIN ManagePageAccess mpa ON mpa.ModuleAutoId=m.AutoId
					WHERE mpa.Role=@EmpType and HasParent=1 and m.Status=1 
					) 
					UNION
					select im.ModuleName,im.AutoId,im.ParentId,im.HasParent As HasChild ,'#' as PageUrl,0 as Role,im.IconClass,
					1 as totalChild,0,im.SequenceNo As AccessAutoId from Module im
					where im.AutoId in (
					select mm.ParentId  from Module mm where autoid in
					(
					SELECT ParentId
					from Module m 
					inner JOIN ManagePageAccess mpa ON mpa.ModuleAutoId=m.AutoId
					WHERE mpa.Role=@EmpType and HasParent=1 and m.Status=1
					)   and mm.HasParent=1
					)
				) as t order by (case when ISNULL(SequenceNo,0)=0 then 100 else SequenceNo end),ModuleName
				END
		
			END
             
	END TRY                      
			BEGIN CATCH                      
			SET @isException=1                      
			SET @exceptionMessage= ERROR_MESSAGE()              
			END CATCH                      
	END 
GO
