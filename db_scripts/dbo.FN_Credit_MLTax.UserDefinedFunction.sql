USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Credit_MLTax]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
--alter table CreditMemoMaster drop column MLTax
--drop function FN_Credit_MLTax
--alter table CreditMemoMaster add  MLTax as  [dbo].FN_Credit_MLTax(CreditAutoId)
--update CreditMemoMaster set IsMLTaxApply=1

Create FUNCTION  [dbo].[FN_Credit_MLTax]
( 
	 @CreditAutoId int
)
RETURNS decimal(18,2)
AS
BEGIN
	DECLARE @MLTax decimal(18,2)=0.00
	SET @MLTax=isnull((SELECT sum(Item_TotalMLTax) FROM CreditItemMaster WHERE CreditAutoId=@CreditAutoId),0)	
	RETURN @MLTax
END


GO
