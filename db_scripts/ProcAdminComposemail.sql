  
ALter PROCEDURE [dbo].[ProcAdminComposemail]                                                                     
@Opcode INT=NULL,                                                                      
@ToEmailId varchar(50)=null,  
@Subject varchar(200)=null,  
@EmailBody varchar(max)=null,  
@CreatedBy int=null,  
@EmpAutoId int=null,  
@StoreId int=null,  
@PageIndex INT=1,                                                                                          
@PageSize INT=10,                                                                                          
@RecordCount INT=null,   
@isException bit out,                                      
@exceptionMessage varchar(max) out                                                                      
AS                                                                      
BEGIN                                                                
  BEGIN TRY                                                                      
  Set @isException=0                                                                      
  Set @exceptionMessage='Success'                                                                                                              
  If @Opcode=101                                                                        
  BEGIN    
    DECLARE @CustomerName varchar(100)=null,@FromEmailId varchar(100)=null,@Password varchar(50)=null,@FromName varchar(100)='Pricesmart',@smtp_userName varchar(100)=null,@PSW varchar(20)=null,@SMTPServer varchar(20)=null,  
 @Port INT=null,@SSL varchar(20)=null,@CCEmailId varchar(100)='mike@mypricesmart.com',@BCCEmailId varchar(100)='uneel913@gmail.com',@SourceApp varchar(100)=null,@SubUrl varchar(100)='mypsm.net'      
    
  insert into [PriceSmart].[dbo].[EmailLog](ToEmailId,Subject,Message,CreatedBy,StoreId,CreatedDate)values(@ToEmailId,@Subject,@EmailBody,@CreatedBy,@StoreId,GETDATE())  
  
 SELECT @FromEmailId=EmailId,@Port=port,@SMTPServer=server,@Password=convert(varchar(100),DecryptBypassphrase('WHM',Pass)),@SSL=ssl,@EmpAutoId=CreatedBy FROM [PriceSmart].[dbo].EmailServerMaster  
 set @EmailBody = '<pre>'+@EmailBody+'</pre>'  
 EXEC [Emailing_DB].[dbo].[ProcSubmitEmail] @Opcode=11,@FromEmailId=@FromEmailId,@FromName=@FromName,@smtp_userName=@FromEmailId,@Password=@Password,@SMTPServer=@SMTPServer,  
    @Port=@Port,@SSL=@SSL,@ToEmailId=@ToEmailId,@CCEmailId=@CCEmailId,@BCCEmailId=@BCCEmailId,@Subject=@Subject,@EmailBody=@EmailBody,@SourceApp=@SourceApp,@SubUrl=@SubUrl,@IsException=0,@ExceptionMessage=''  
   
  END      
  If @Opcode=102                                                                       
  BEGIN    
       
   SELECT ToEmailId,Subject,Message,(EM.FirstName+' '+EM.LastName) as EmpName,format(convert(date,EL.CreatedDate),'MM/dd/yyyy') as CreatedDate   
   from [PriceSmart].[dbo].[EmailLog] as  EL   
      inner join [psmnj.a1whm.com].dbo.EmployeeMaster EM on EM.AutoId=EL.CreatedBy  
   where StoreId=@StoreId  
     
  END      
  END TRY                                                                      
  BEGIN CATCH                                 
    Set @isException=1                                                                      
    Set @exceptionMessage='Oops! Something went wrong.Please try later.'              
  END CATCH  
  If @Opcode=103  
  BEGIN  
       IF(DB_NAME()='PriceSmart')   
    BEGIN  
           Select top 1 Email from [PriceSmart].[dbo].WebDratStoreContactDetails Where  
     StoreAutoId=@StoreId  
    END  
    ELSE  
    BEGIN  
        Declare @DBName varchar(50),@Query nvarchar(max)  
     SET @DBName=DB_NAME()  
        SET @Query='select Email from ['+@DBName+'].[dbo].CustomerContactPerson   
     where ISDefault=1 and CustomerAutoId='+convert(varchar(20),@StoreId)+''  
     execute Sp_executesql @Query  
    END  
  END  
END   