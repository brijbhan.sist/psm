USE [psmnj.a1whm.com]
GO
/****** Object:  Table [dbo].[tbl_ActionMaster]    Script Date: 10/3/2020 4:38:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_ActionMaster](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[Action] [varchar](100) NULL,
	[ActionDesc] [varchar](200) NULL,
 CONSTRAINT [PK__tbl_Acti__6B232905B76EF65C] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tbl_ActionMaster] ON 
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (1, N'New Order has been generated', N'Order No. [OrderNo]')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (2, N'Order has been updated', N'Order No. [OrderNo]')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (3, N'Order Status changed', N'From [Status1] to [Status2].')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (4, N'Order has been packed', N'Packing Id is [PackingId].')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (5, N'Driver has been assigned', N'Driver Name [DriverName].')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (6, N'Order delivery details has been updated
', N'[AccountRemark]
')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (7, N'Order is cancelled
', N'[ManagerRemark]
')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (8, N'New Credit memo has been generated', N'Credit Memo No  [MemoNo]')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (9, N'Credit memo has been updated', N'Credit Memo No  [MemoNo]')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (10, N'Credit memo has been approved.', N'Credit Memo No  [MemoNo]')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (11, N'Credit memo has been review by account manager.', N'Credit Memo No  [MemoNo]')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (12, N'Credit memo has been completed successfully.', N'Credit Memo No  [MemoNo]')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (13, N'Apply Credit memo in order', N'Order No : [OrderNo]')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (14, N'Remove CreditMemo from Orderm', N'Order No : [OrderNo]')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (15, N'Attached Credit memo in order No ', N'Order No : [OrderNo]')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (16, N'deattached Credit Memo from Order ', N'Order No : [OrderNo]')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (17, N'Order viewed by packer', N'Order No. [OrderNo]')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (18, N'Packer has been assigned.', N'Packer Name [PackerName]')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (19, N'ML Tax has been Removed successfully.', N'Order No : [OrderNo]')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (20, N'Credit memo has been cancelled by sales manager.', N'Credit Memo No  [MemoNo]')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (21, N'Vendor Payment has been settled', N'Payment ID [PaymentID]')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (22, N'Driver has been Changed', N'Driver Name [DriverName].')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (23, N'Order has been changed status packed to processed by SalesManeger', N'SaleManager Name [SaleManager Name]')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (24, N'Packed Boxes changed', N'Packed Box has been changed [OldBox] to [NewBox]')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (25, N'Order has been update', N'Order has been changed status @OldStatus to @newStatus by @Emp')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (26, N'Boxes has been change successfully', N'Order No. [OrderNo]')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (27, N'Vendor Payment has been Updated', N'Payment ID [PaymentID]')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (31, N'ML Tax has been added successfully.', N'Order No : [OrderNo]')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (32, N'Create PO Draft successfully', N'Draft No : [Draft No]')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (33, N'Generate PO successfully', N'PONo : [PONo]')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (34, N'Create Receive Draft successfully', N'ReceiveNo : [RecNo]')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (35, N'Submit Receive stock successfully', N'ReceiveNo : [RecNo]')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (36, N'Forward PO Draft to Inventory Manager', N'Draft No : [Draft No]')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (37, N'Revert PO Draft to Inventory Receiver', N'Draft No : [Draft No]')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (38, N'Cancel PO', N'PONo : [PONo]')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (39, N'Revert Receive PO', N'ReceiveNo : [RecNo]')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (40, N'Partial Close Receive Stock', N'ReceiveNo : [RecNo]')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (41, N'Close PO', N'PONo : [PONo]')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (42, N'Payment settled for this order', N'Payment Id : [Payment Id]')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (43, N'Shipping address changed', N'Order Id : [Order Id]')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (44, N'Check return', N'Order No. [OrderNo]')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (45, N'Sales person changed', N'Sales Person Name [SalesPersonName]')
GO
INSERT [dbo].[tbl_ActionMaster] ([AutoId], [Action], [ActionDesc]) VALUES (46, N'Cost price updated', N'Order No :[Order No]')
GO
SET IDENTITY_INSERT [dbo].[tbl_ActionMaster] OFF
GO
