	--alter table CustomerContactPerson add  CreatedDate datetime,CreatedBy int ,ModifyDate datetime,ModifyBy int
	ALTER PROCEDURE [dbo].[ProcCustomerMaster]                                                                                                  
	@Opcode INT=Null,    
	@XmlContactPerson xml=null, 
	@LocationAutoId int = null,                                
	@LocationName varchar (20) = null,                                                                                                  
	@CustomerAutoId INT=NULL,                                                                                                  
	@OrderAutoId INT=NULL,                              
	@SecurityKey VARCHAR(15)=NULL,                                                                                                
	@FileAutoId INT =NULL,                                                                                                  
	@PaymentAutoId INT=NULL,                               
	@CancelRemark VARCHAR(250)=NULL,                                                                                                
	@CustomerId VARCHAR(15)=NULL,                                                                                                  
	@DocumentName VARCHAR(100)=NULL,                                                                                                  
	@DocumentURL VARCHAR(max)=NULL,  
	@Latitude VARCHAR(max)=NULL,  
	@Longitude VARCHAR(max)=NULL,
	@Latitude1 VARCHAR(max)=NULL,  
	@Longitude1 VARCHAR(max)=NULL,
	@PaymentId VARCHAR(10)=NULL,                                                                                                  
	@CustomerName VARCHAR(100)=NULL,                                                                                                  
	@CustomerType VARCHAR(100)=NULL,                                                                                                  
	@BusinessName VARCHAR(100)=NULL,                                                                                                  
	@OPTLicence VARCHAR(100)=NULL,                                                                                                  
	@Email VARCHAR(50)=NULL,                                                                                                  
	@AltEmail VARCHAR(50)=NULL,                                                                                                  
	@Contact1 VARCHAR(30)=NULL,                                                                                                  
	@Contact2 VARCHAR(30)=NULL,                                                                                                  
	@BillAddAutoId INT=NULL,                                                                                                  
	@ShipAddAutoId INT=NULL,                                                                                                  
	@SalesPersonAutoId INT=NULL,                                                                                                  
	@CityAutoId int =null,   
	@CityAutoId2 int =null,                                                                                  
	@stateAutoId int =null,                                                                                                   
	@Status INT=NULL,                                                                                                  
	@OrderNo varchar(40) =null,                                                                                                  
	@FromDate Date =null,                                                                                                  
	@ToDate Date =null,                                                                                                  
	@Terms INT=NULL,                                                                                                  
	@MobileNo VARCHAR(20)=NULL,                                                                                                  
	@FaxNo VARCHAR(20)=NULL,                                                                                                  
	@TaxId VARCHAR(50)=NULL,                                                                                                
	@StoreCredit DECIMAL(10,2)=NULL,                                                         
	@BillAdd VARCHAR(200)=NULL,                                                              
	@BillAdd2 VARCHAR(100)=NULL,                                  
	@State1 INT=NULL,                                                                   
	@City1 VARCHAR(50)=NULL,                           
	@Zipcode1 VARCHAR(20)=NULL,    
	@ZipAutoId INT=NULL,                                                            
	@ShipAdd VARCHAR(200)=NULL,  
	@ShipAdd2 VARCHAR(100)=NULL, 
	@State2 INT=NULL,                                                                                                  
	@City2 VARCHAR(50)=NULL,                                      
	@Zipcode2 VARCHAR(20)=NULL,   
	@ZipAutoId2 INT=NULL,                                                         
	@ContactPersonName VARCHAR(100)=NULL,                                                                          
	@PriceLevelAutoId INT=NULL,                                                                                                  
	@EmpAutoId INT=NULL,                                                                                                  
	@CreditAmount decimal(18,2)=NULL,                                                            
	@ReceivedAmount decimal(18,2)=NULL,                                                                         
	@PaymentMethod INT=NULL,                                                                       
	@ReferenceNo varchar(20)=NULL,                                                                       
	@PaymentRemarks varchar(500)=NULL,                                                                       
	@ReceivedPaymentBy INT=NULL,                                                                          
	@EmployeeRemarks varchar(500)=NULL,                                                                 
	@TableValue  CustomerPayment readonly,                                                       
	@CheckNo varchar(500)=NULL,                                                                                     
	@PaymentCurrencyXml xml=null,                                                                                    
	@SortAmount decimal(18,2)=null,                                                                              
	@TotalCurrencyAmount decimal(10,2)=null,                                                                                                
	@CheckDate DATE=NULL,                    
	@RoutingNo varchar(20)=null,                   
	@ExpiryDate varchar(10)=null,                    
	@BankName varchar(500)=NULL,                  
	@BankAcc varchar(20)=NULL,                  
	@CardNo varchar(20)=NULL,      
	@Zipcode varchar(20)=NULL,  
	@StateName varchar(150)=NULL,   
	@StateName2 varchar(150)=NULL,
	@CVV varchar(20)=NULL,                  
	@CardType int=NULL,                  
	@AutoId int=null,  
	@Type int=NULL, 
	@Landline varchar(50)=NULL, 
	@Landline2 varchar(50)=NULL, 
	@StoreOpenTime time(7)=NULL,
	@StoreCloseTime time(7)=NULL,
	@UserName VARCHAR(100)=NULL,  
	@Password varchar(max)=null,  
	@CustomerRemark VARCHAR(1000)=NULL,
	@IsDefault int=NULL, 
	@IsAppLogin int=null,                                                              
	@PageIndex INT=1,                                                                                                  
	@PageSize INT=10,                                                                                                  
	@RecordCount INT=null,                                                                                                  
	@isException BIT OUT,                                                                                                  
	@exceptionMessage VARCHAR(max) OUT                                                                      
	AS                                                                                                  
	BEGIN                                                                                                  
	 BEGIN TRY                                                                               
			SET @isException=0                                                                                                  
			SET @exceptionMessage='Success!!'
			Declare @ChildDbLocation varchar(50),@sql nvarchar(max) 
			select @ChildDbLocation=ChildDB_Name from CompanyDetails
			DECLARE @companyId varchar(50)   
	 IF @Opcode=11                                                                                                  
	  BEGIN                                                                                                            
	   BEGIN TRY                                                                                          
	   BEGIN TRAN     
    
		IF NOT EXISTS(SELECT * FROM State WHERE  statename=@StateName)                                                          
		BEGIN                           
			SET @isException=1                                                                            
			SET @exceptionMessage='State1'                                                                                                  
		END                       
		ELSE IF NOT EXISTS(SELECT * FROM State WHERE statename=@StateName2)                                                                           
		BEGIN                        
			SET @isException=1                                                                                                  
			SET @exceptionMessage='State2'                                                                                                  
		END                               
		else if EXISTS(select * from [CustomerMaster] where CustomerName = trim(@CustomerName))                                
		begin                               
			SET @isException=1                                                                                            
			SET @exceptionMessage=  'Customer details already exist.'                                
		end                                
		else if (select count(*) from [CustomerMaster] where LocationAutoId != 0 and LocationAutoId = @LocationAutoId and CustomerType = 3) > 0                                
		begin                                      
			SET @isException=1                                 
			set @LocationName = (select Location from LocationMaster where AutoId = @LocationAutoId)                                                                                         
			SET @exceptionMessage=  @LocationName + ' ' + 'Customer already exist.'                                
		end                                                                                                  
		ELSE                                                                                                   
		BEGIN                  
			SET @CustomerId=(SELECT [dbo].[SequenceCodeGenerator]('CustomerId'))    
			SET @companyId =(SELECT TOP 1 CompanyId FROM CompanyDetails)
		

			Exec ProcinsertData_Cityzipcode_ALL @City=@City1,@State=@StateName,@Zipcode1=@Zipcode1
			Exec ProcinsertData_Cityzipcode_ALL @City=@City2,@State=@StateName2,@Zipcode1=@Zipcode2
                                                                                                
                                          
			SET @State1 =(Select top 1 AutoId from State Where StateName= trim(@StateName))                                                                                  
			SET @State2 =(Select top 1 AutoId from State Where StateName= trim(@StateName2)) 

			SET @CityAutoId=(Select  top 1 AutoId from CityMaster Where StateId=@State1 and  CityName=trim(@City1))  
			SET @CityAutoId2=(Select top 1 AutoId from CityMaster Where StateId=@State2 and  CityName= trim(@City2))
		
			SET @ZipAutoId =(Select  top 1 AutoId from ZipMaster Where Zipcode= @Zipcode1 and CityId=@CityAutoId)                                                                                  
			SET @ZipAutoId2 =(Select top 1 AutoId from ZipMaster Where Zipcode= @Zipcode2 and CityId=@CityAutoId2)   
			INSERT INTO [dbo].[CustomerMaster]([CustomerId],[CustomerName],[CustomerType],                                              
			[SalesPersonAutoId],[Status],[Terms],[TaxId],BusinessName,OPTLicence,                                          
			CreatedBy,CreatedOn,UpdateOn,UpdatedBy,LocationAutoId,OptimotwFrom,OptimotwTo,CustomerRemarks)                                                                                                   
			VALUES (@CustomerId,trim(@CustomerName),@CustomerType,@SalesPersonAutoId,@Status,                                                                                                  
			@Terms,@TaxId,@BusinessName,@OPTLicence,@EmpAutoId,getdate(),getdate(),@EmpAutoId,@LocationAutoId,@StoreOpenTime,@StoreCloseTime,@CustomerRemark)
	 
			SET @CustomerAutoId = (SELECT SCOPE_IDENTITY())     

			if @UserName!=''
			begin                                                                                               
				update cm  set UserName=@UserName+'@'+@companyId,
				Password=EncryptByPassPhrase('WHM',@Password),
				IsAppLogin=@IsAppLogin from CustomerMaster as cm where AutoId=@CustomerAutoId    
			end

			insert into CustomerContactPerson(ContactPerson,Type,MobileNo,Landline,Landline2,Fax,Email,AlternateEmail,IsDefault,CustomerAutoId)        
			select tr.td.value('ContactPerson[1]','varchar(100)') as ContactPerson,        
			tr.td.value('TypeAutoId[1]','int') as Type,        
			tr.td.value('Mobile[1]','varchar(20)') as MobileNo,  
			tr.td.value('Landline[1]','varchar(20)') as Landline,  
			tr.td.value('Landline2[1]','varchar(20)') as Landline2,  
			tr.td.value('Fax[1]','varchar(20)') as Fax,  
			tr.td.value('Email[1]','varchar(20)') as Email, 
			tr.td.value('AltEmail[1]','varchar(20)') as AlternateEmail, 	
			tr.td.value('IsDefault[1]','int') as IsDefault,
			@CustomerAutoId
			from  @XmlContactPerson.nodes('/contactpersonXML') tr(td) 
	                                                                                  
			INSERT INTO [dbo].[ShippingAddress] ([CustomerAutoId],[Address],[State],[SCityAutoId],[Zipcode],[IsDefault],ZipcodeAutoid,SA_Lat,SA_Long,Address2)                                                                                                   
			VALUES(@CustomerAutoId,@ShipAdd,@State2,@CityAutoId2,@Zipcode2,1,@ZipAutoId2,@Latitude,@Longitude,@ShipAdd2)                                                                                                   
			SET @ShipAddAutoId = (SELECT SCOPE_IDENTITY())                                                                                             
                                                                   
			INSERT INTO [dbo].[BillingAddress] ([CustomerAutoId],[Address],[State],[BCityAutoId],[Zipcode],[IsDefault],ZipcodeAutoid,BillSA_Lat,BillSA_Long,Address2)                                                                                                   
			VALUES(@CustomerAutoId,@BillAdd,@State1,@CityAutoId,@Zipcode1,1,@ZipAutoId,@Latitude1,@Longitude1,@BillAdd2)                                                                                                  
			SET @BillAddAutoId = (SELECT SCOPE_IDENTITY())                                                                                                    
                                                                            
			IF(@PriceLevelAutoId = -1)                                         
			BEGIN                                                                                                  
				exec [ProcPriceLevelMaster] @opcode=11,@PriceLevelName=@CustomerName,@CustomerType=@CustomerType,@Status=1,@EmpAutoId=@EmpAutoId,  
				@isException=null,@exceptionMessage=null    
				set @PriceLevelAutoId=(select MAX(autoid) from [PriceLevelMaster])                                                                                       
			END                                                                                                  
                             
			IF(@PriceLevelAutoId IS NOT NULL)                                                                            
			BEGIN                                                                 
				INSERT INTO [dbo].[CustomerPriceLevel]([CustomerAutoId],[PriceLevelAutoId],[AssignedOn],[AssignedBy]) VALUES(@CustomerAutoId,@PriceLevelAutoId,getdate(),@EmpAutoId)                                                                             
				set @PriceLevelAutoId=SCOPE_IDENTITY()
			END                                                                                                  
			UPDATE [dbo].[CustomerMaster] SET [DefaultBillAdd]=@BillAddAutoId, [DefaultShipAdd]=@ShipAddAutoId WHERE [AutoId] = @CustomerAutoId                                                                                                  
                                                                            
			UPDATE [dbo].[SequenceCodeGeneratorMaster] SET [currentSequence]=[currentSequence]+1 WHERE [SequenceCode]='CustomerId'                                                                                    
                                                                                    
			IF NOT EXISTS(SELECT AutoId FROM CustomerCreditMaster WHERE CustomerAutoId=@CustomerAutoId)                                                                                  
			BEGIN                                                                                  
				INSERT INTO CustomerCreditMaster(CustomerAutoId,CreditAmount)                                                                                  
				VALUES(@CustomerAutoId,0.00)          
				declare @CustomerCreditAutoId int
				set @CustomerCreditAutoId=SCOPE_IDENTITY()
			END 
			exec ProcInsertData_CustomerMaster @CustomerAutoId=@CustomerAutoId,@isException=null,@exceptionMessage=null,@Opcode=11
	  END                                                                                                       
	  COMMIT TRANSACTION                                                                                             
	  END TRY                                                    
	  BEGIN CATCH                                             
	  ROLLBACK TRAN                                                                                                   
	   SET @isException=1                                                                       
	   SET @exceptionMessage=ERROR_MESSAGE()                                                                                                         
	   END CATCH                     
	  END                                            
	  ELSE IF @Opcode=21                                                                                                  
	  BEGIN                  
	   BEGIN TRY                                                                                                  
		BEGIN TRAN                                                                              
                                                         
		if exists(select * from [CustomerMaster] where CustomerName = trim(@CustomerName)  and [CustomerId]!=@CustomerId)                                
		begin                                
			SET @isException=1                                                                                                 
			SET @exceptionMessage= 'Customer details already exist.'                                
		end                                
		else if exists(select * from [CustomerMaster] where LocationAutoId != 0 and LocationAutoId = @LocationAutoId and @CustomerType = 3  and [CustomerId]!=@CustomerId)                                
		begin                                
			SET @isException=1                                 
			set @LocationName = (select Location from LocationMaster where AutoId = @LocationAutoId)                                                                                                 
			SET @exceptionMessage=  @LocationName + ' ' + 'Customer already exist.'                                
		end                                                                                        
		ELSE                                                   
		BEGIN     
   
                                           
	   UPDATE [dbo].[CustomerMaster] SET                                                               
	   [CustomerName]=trim(@CustomerName),                                                                                                  
	   [CustomerType]=@CustomerType,                                                                                                                                                                        
	   [Status]=@Status,                                                                                
	   [SalesPersonAutoId]=@SalesPersonAutoId,                                                                                                  
	   [Terms]=@Terms,                                                                                                                                                    
	   [BusinessName]=@BusinessName,                                                               
	   [TaxId]=@TaxId,                                                                                               
	   OPTLicence=@OPTLicence,  
	   LocationAutoId =(case when @CustomerType=3 then  @LocationAutoId else null end),                                                                            
	   UpdateOn=GETDATE(),UpdatedBy=@EmpAutoId,
	   OptimotwFrom=@StoreOpenTime,
	   OptimotwTo=@StoreCloseTime,
	   CustomerRemarks=@CustomerRemark
	   WHERE [CustomerId]=@CustomerId                                          
                                                                                 
                  
	   SET @CustomerAutoId = (SELECT [AutoId] FROM [dbo].[CustomerMaster] WHERE [CustomerId] = @CustomerId)                                                                             
                                             
	   DELETE FROM [dbo].[CustomerPriceLevel] WHERE [CustomerAutoId] = @CustomerAutoId                                                                                                   
                                                                                                       
	   IF(@PriceLevelAutoId = -1)                                                                                                  
	   BEGIN   
		   IF NOT EXISTS(SELECT [PriceLevelName] FROM [PriceLevelMaster] WHERE [PriceLevelName]=@CustomerName)
		   BEGIN
				EXEC [ProcPriceLevelMaster] @opcode=11,@PriceLevelName=@CustomerName,@Status=1,@EmpAutoId=@EmpAutoId,@isException=null,@exceptionMessage=null
		   END
			SET @PriceLevelAutoId=(select top 1 autoid from [PriceLevelMaster] WHERE [PriceLevelName]=@CustomerName)     
	   END                                                                        
   
		IF EXISTS(SELECT 1 FROM [CustomerPriceLevel] WHERE [CustomerAutoId]=  @CustomerAutoId)
		BEGIN
			DECLARE @CHECK INT=(SELECT PriceLevelAutoId FROM [CustomerPriceLevel] WHERE [CustomerAutoId]=  @CustomerAutoId)
			UPDATE [dbo].[CustomerPriceLevel] SET 
			[PriceLevelAutoId]=CASE WHEN @CHECK=@PriceLevelAutoId  THEN [PriceLevelAutoId] ELSE @PriceLevelAutoId END,
			[AssignedBy]=CASE WHEN @CHECK=@PriceLevelAutoId  THEN AssignedBy ELSE @EmpAutoId END,
			[AssignedOn]=CASE WHEN @CHECK=@PriceLevelAutoId  THEN AssignedOn ELSE GETDATE() END
			WHERE [CustomerAutoId]=  @CustomerAutoId
		END
		ELSE
		BEGIN                                                                     
			IF(@PriceLevelAutoId IS NOT NULL)                                                                                                  
			BEGIN                                                                                                                   
				INSERT INTO [dbo].[CustomerPriceLevel]([CustomerAutoId],[PriceLevelAutoId],[AssignedOn],[AssignedBy])                                                      
				VALUES(@CustomerAutoId,@PriceLevelAutoId,getdate(),@EmpAutoId)                                                                                           
			END  
	   END
	   IF NOT  EXISTS(SELECT AutoId FROM CustomerCreditMaster WHERE CustomerAutoId=@CustomerAutoId)                                                                            
	   BEGIN                                                                                  
		INSERT INTO CustomerCreditMaster(CustomerAutoId,CreditAmount)                            
		VALUES(@CustomerAutoId,0.00)  
		set @CustomerCreditAutoId=SCOPE_IDENTITY()

	   END  

		exec ProcInsertData_CustomerMaster @CustomerAutoId=@CustomerAutoId,@isException=null,@exceptionMessage=null,@Opcode=21

	 END                                                                           
	 COMMIT TRANSACTION                                                                        
	 END TRY                                                                                                  
	 BEGIN CATCH                                                                                                    
	  ROLLBACK TRAN                                                                                    
	  SET @isException=1                                                                                                  
	  SET @exceptionMessage=ERROR_MESSAGE()                                                                                                         
	 END CATCH                                                      
	  END                                                                                                  
	  ELSE IF @Opcode = 31                                                                                                  
	   BEGIN                                                                                                  
		DELETE FROM [dbo].[CustomerMaster] WHERE [CustomerId]=@CustomerId                                                                                                   
	   END               
	  ELSE IF @Opcode = 42                                                                                                  
	   BEGIN                                                                              
		select(
			select [CustomerId],[CustomerName],[CustomerType],CM.Status,[SalesPersonAutoId],[UserName],CONVERT(varchar(100),DecryptByPassphrase('WHM',[Password])) AS [Password],
			IsAppLogin,CPL.[PriceLevelAutoId],[Terms],[TaxId],[ContactPersonName],BusinessName,CustomerRemarks,
			(SELECT ZoneName FROM ZoneMaster AS ZM WHERE AUTOID=(SELECT top 1 ZONEID FROM  ZipMaster ZM1 WHERE ZM1.ZIPCODE=SA.ZIPCODE))AS Zone1,               
			OPTLicence,isnull(LocationAutoId,0) as LocationAutoId,CONVERT(VARCHAR(8),OptimotwFrom,108)   as OptimotwFrom,CONVERT(VARCHAR(8),OptimotwTo,108)   as OptimotwTo
			from CustomerMaster as CM
			LEFT JOIN [dbo].[BillingAddress] AS BA ON BA.AutoId=CM.DefaultBillAdd 
			LEFT JOIN [dbo].[ShippingAddress] AS SA ON SA.AutoId=CM.DefaultShipAdd 
			LEFT JOIN [dbo].[CustomerPriceLevel] AS CPL ON CPL.[CustomerAutoId] = CM.[AutoId] 
			WHERE CM.AutoId = @CustomerAutoId  
		for json path, INCLUDE_NULL_VALUES) as CustomerDetails                                          
	   END  
		ELSE IF @Opcode = 43                                                                                                 
	   BEGIN   
		select(
			select
			(SELECT [AutoId], [FirstName] + ' ' +[LastName] AS Name FROM [dbo].[EmployeeMaster] WHERE [EmpType]=2 AND [Status] = 1 order by Name for json path, INCLUDE_NULL_VALUES) as Employee,
			(SELECT [AutoId], [StatusType] FROM [dbo].[StatusMaster] WHERE [Category] is null for json path, INCLUDE_NULL_VALUES) as Status,
			(SELECT [TermsId],[TermsDesc] FROM [dbo].[CustomerTerms]  ORDER BY [TermsDesc] ASC for json path, INCLUDE_NULL_VALUES) as Terms,
			(SELECT [AutoId],[CustomerType] FROM [dbo].[CustomerType] ORDER BY [CustomerType] ASC for json path, INCLUDE_NULL_VALUES) as CustomerType,
			(SELECT CompanyId FROM CompanyDetails for json path, INCLUDE_NULL_VALUES) as CompanyDetails,
			(select Zipcode as ZM,CityId,zm.AutoId,Zipcode+' [ '+CM.CityName+' ]' as ZipCode from ZipMaster as ZM  
			INNER JOIN CityMaster CM ON ZM.CityId=CM.AutoId  WHERE ZM.Status=1  ORDER BY REPLACE(Zipcode+' [ '+CM.CityName+' ]',' ','') ASC for json path, INCLUDE_NULL_VALUES) as ZipCode1,
			(select AutoId,CONVERT(VARCHAR, TimeFrom, 100) as TimeFromLabel,CONVERT(VARCHAR, TimeTo, 100) as TimeToLabel,CONVERT(VARCHAR(8),TimeFrom,108)   as TimeFrom ,CONVERT(VARCHAR(8),TimeTo,108) as TimeTo from OptimoTimeWindow order by AutoId asc for json path) as StoreTime
			for json path, INCLUDE_NULL_VALUES
		)as DropDownList
	   END   
	   ELSE IF @Opcode = 44                                                                                       
	   BEGIN                                                                                                  
		   SET @CustomerAutoId=(SELECT AutoId FROM CustomerMaster AS CM  WHERE [CustomerId] = @CustomerId)                                                                                                  
                                                                                                      
			  SELECT [CustomerId],[CustomerName],ct.[CustomerType],emp.FirstName +' '+ emp.LastName as EmpName,emp.Email as [AltEmail],                                                                                                
			  [SalesPersonAutoId],[ContactPersonName], cm.CustomerRemarks,   
			  FORMAT (CreatedOn, 'MM/dd/yyyy') as CreatedOn,
			  FORMAT(UpdateOn, 'MM/dd/yyyy') as UpdateOn,  
			  (SELECT FirstName+' '+ISNULL(LastName,'') FROM EmployeeMaster AS emp1 WHERE emp1.AutoId=CM.Createdby) as CreatedBy,                                                                                                  
				(SELECT  FirstName+' '+ISNULL(LastName,'')  FROM EmployeeMaster AS emp2 WHERE emp2.AutoId=CM.updatedBy) as UpdatedBy,                                                                                                                     
			  ISNULL((select CreditAmount from CustomerCreditMaster where customerAutoid=cm.Autoid),0.00) AS CreditAmount,                                                                                                  
			  ISNULL((SELECT ZoneName FROM ZoneMaster AS ZM WHERE AUTOID=(SELECT top 1 ZONEID FROM  ZipMaster ZM1 WHERE                                                                                          
				 ZM1.ZIPCODE=(select sa.ZipCode from  shippingAddress as sa where sa.autoid=cm.DefaultShipAdd))) ,'NA')AS Zone1                                            
			   FROM [dbo].[CustomerMaster] As CM                                                                                                  
			  INNER JOIN [dbo].CustomerType AS ct ON ct.AutoId=CM.CustomerType                                                                                                  
			  LEFT JOIN [dbo].EmployeeMaster AS emp ON emp.AutoId=CM.SalesPersonAutoId                                                                                                   
			  LEFT JOIN [dbo].[CustomerPriceLevel] AS CPL ON CPL.[CustomerAutoId] = CM.[AutoId]                                                                         
			  WHERE [CustomerId] = @CustomerId                                                                                                   
			  SELECT EMP.AutoId,ISNULL(FirstName,'')+' ' +ISNULL(LastName,'') AS EmpName,SM.StatusType Status FROM EmployeeMaster AS EMP  --Changed on 12/03/2019 By Rizwan Ahmad                                                                                         
     		 INNER JOIN StatusMaster AS SM ON SM.AUTOID=EMP.STATUS AND Category IS NULL and EmpType in (2,5,6,10)  order by EMP.FirstName                                                                                               
	   END                                                                                                  
	   ELSE IF @Opcode = 45                                                     
	BEGIN                
		SET @CustomerAutoId=(SELECT AutoId FROM CustomerMaster AS CM  WHERE [CustomerId] = @CustomerId)                                                                       
         
                                         
		SELECT AutoId into #AutoId from OrderMaster where CustomerAutoId=@CustomerAutoId 
       
		SELECT ROW_NUMBER() OVER(ORDER BY OrderNo desc) AS RowNumber, * INTO #Results45 from                        
		(                                         
		SELECT OM.AutoId,OM.OrderNo,cONVERT(VARCHAR(10),OM.OrderDate,101) AS OrderDate,sm.StatusType as Status,                                                                                                  
		cONVERT(VARCHAR(10),OM.DeliveryDate,101) AS DeliveryDate,                                                                                                  
		ISNULL(OM.PayableAmount,0) AS PayableAmount,                                                                   
		(case when om.status=11 then isnull(ISNULL(DO.AmtDue,0),0)else 0.00 end) as AmtDue,                                                                                                  
		(case when om.status=11 then isnull(DO.AmtPaid,0) else 0.00 end ) as AmtPaid,                                             
		ISNULL(OM.AmtValue,0) AS PaidAmt,                                                                     
		(select count(1) from OrderItemMaster as oim where oim.OrderAutoid=OM.AutoId) as OrderItems,                                                                                                  
		EMP.FirstName+' '+EMP.LastName AS SalesMan,EMP1.FirstName+' '+EMP1.LastName AS PackerNames,                                                                                                  
		EMP1.FirstName+' '+EMP1.LastName AS PackerName,EMP2.FirstName+' '+EMP2.LastName AS SalesManager,                                       
		EMP3.FirstName+' '+EMP3.LastName AS Account,                                                                                                  
		otm.OrderType AS OrderType,om.Status as StatusCode  FROM OrderMaster AS OM 
		INNER JOIN #AutoId AS AI ON AI.AutoId=OM.AutoId
		INNER JOIN StatusMaster AS SM ON SM.AutoId =OM.Status AND SM.Category='OrderMaster'                                                                                    
		LEFT JOIN  DeliveredOrders DO ON DO.OrderAutoId=OM.AutoId                                                                                       
		INNER JOIN EmployeeMaster AS EMP ON EMP.AutoId=OM.SalesPersonAutoId                                                                                                  
		LEFT JOIN EmployeeMaster AS EMP1 ON EMP1.AutoId=OM.PackerAutoId                                                                                                   
		LEFT JOIN EmployeeMaster AS EMP2 ON EMP2.AutoId=OM.ManagerAutoId                                                                                                   
		LEFT JOIN EmployeeMaster AS EMP3 ON EMP3.AutoId=OM.AccountAutoId    
		 INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
	   )as t order by OrderNo desc                              
                   
		SELECT COUNT(RowNumber) AS RecordCount,case when @PageSize=0 then COUNT(OrderNo) else @PageSize end AS PageSize,@PageIndex as PageIndex FROM #Results45             
                                      
		SELECT * FROM #Results45                                      
		WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))   
	                                                                                              
		SELECT CreditAutoId,CASE WHEN CreditType=3 THEN 'POS CREDIT' ELSE 'ERP CREDIT' END AS CreditType,CreditNo,                                                                                                  
		CONVERT(varchar(10),CreditDate,101) as CreditDate,CONVERT(varchar(10),CompletionDate,101) as ApplyDate,                                   
		StatusType,NetAmount as  SalesAmount,                                           
		(SELECT OrderNo FROM ORDERMASTER AS OM WHERE OM.AUTOID=CMM.ORDERAUTOID) as AppliedOrder,                                                                         
		(SELECT FirstName+' '+ISNULL(LASTNAME,'') FROM EmployeeMaster AS emp1 WHERE emp1.AUTOID=CMM.createdby) as CreatedBy,                                                                                                  
		(SELECT  FirstName+' '+ISNULL(LASTNAME,'')  FROM EmployeeMaster AS emp2 WHERE emp2.AUTOID=CMM.ApprovedBy) as ApprovedBy,                                                                                                  
		(SELECT  FirstName+' '+ISNULL(LASTNAME,'')  FROM EmployeeMaster AS emp3 WHERE emp3.AUTOID=CMM.CompletedBy) as SettledBy,                                                                                       
		TotalAmount,OrderAutoId,Status FROM CreditMemoMaster AS CMM                                                                                                  
		inner join statusmaster as sm on sm.autoid=cmm.Status  and sm.Category='CreditMaster'                                                                                                  
		WHERE CustomerAutoId=@CustomerAutoId                                                                                                  
	   END                                                                        
	   ELSE IF @Opcode = 46                                                                                                  
	   BEGIN                                                                                                
			SET @CustomerAutoId=(SELECT AutoId FROM CustomerMaster AS CM  WHERE [CustomerId] = @CustomerId) 
		
			SELECT om.AutoId as OrderAutoId,OrderNo,OrderDate,DO.PayableAmount,DO.AmtPaid,DO.AmtDue,                                                                                                  
			otm.OrderType AS OrderType  ,      
			datediff(dd,OrderDate,getdate())  as DaysOld      
			into #result46  FROM OrderMaster AS OM INNER JOIN DeliveredOrders AS DO ON OM.AutoId=DO.OrderAutoId   
			 INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
			where CustomerAutoId =@CustomerAutoId and DO.AmtDue >0                                                                                                
			and Status=11                                                                                                
                                                                      
			select OrderAutoId,OrderNo,CONVERT(VARCHAR(10),OrderDate,101) AS OrderDate,PayableAmount,AmtPaid,AmtDue,AmtPaid,OrderType,DaysOld from #result46                                                                                                   
			where (@OrderNo is null or @OrderNo='' or OrderNo like '%'+@OrderNo+'%') and (@FromDate is null or @FromDate = '' or @Todate is null or 
			@Todate = '' or ([OrderDate] between @FromDate and @Todate)) order by CONVERT(datetime,OrderDate,101) desc
		
			select SUM(AmtDue) as TotalDueAmount from #result46                                                                                         
                                                                                       
			select Autoid,Currencyname,CurrencyValue from Currencymaster where Status=1  order by   CurrencyValue desc                                                                                           
	   END                                           
                                                                                     
	   ELSE IF @Opcode = 47                                     
	   BEGIN                                                                                                  
	   SET @CustomerAutoId=(SELECT AutoId FROM CustomerMaster AS CM  WHERE [CustomerId] = @CustomerId)                                                               
	   SELECT ROW_NUMBER() OVER(ORDER BY PaymentId desc) AS RowNumber, * INTO #Results from                        
		(                                         
		SELECT PaymentAutoId,PaymentId,format(PaymentDate,'MM/dd/yyyy HH:mm tt')  as PaymentDate, 
	  ISNULL(pmm.PaymentMode,'') as PaymentMethod,                                                                                       
	  ( SELECT COUNT(*) FROM PaymentOrderDetails AS TEMP WHERE  TEMP.PaymentAutoId=CPD.PaymentAutoId) AS Totalordes,                                                           
	  (isnull(ReceivedAmount,0) - isnull(SortAmount,0)) as ReceivedAmount ,(EMP.FIRSTNAME+' ' +EMP.LASTNAME) AS EmpName   
	  ,case when isnull(CPD.Status,0)=0 then 'Settled' else 'Cancelled' end as Status,CPD.CreditAmount                                
	  FROM CustomerPaymentDetails AS CPD                                                                                                  
	  INNER JOIN EmployeeMaster AS EMP ON EMP.AutoId=CPD.ReceivedBy                      
	  left join PAYMENTModeMaster as pmm on CPD.PaymentMode=pmm.AutoID       
	  where CustomerAutoId =@CustomerAutoId                                                                                                  
	  and (@PaymentId is null or @PaymentId='' or PaymentId like '%'+@PaymentId+'%')                                                                                                  
	  and (@Status is null or @Status='-1' or isnull(CPD.Status,0)=@Status)                                                                                                  
	  and (@FromDate is null or @FromDate = '' or @Todate is null or @Todate = '' or (PaymentDate between @FromDate and @Todate))                           
		)as t order  by PaymentAutoId desc                                    
                   
		SELECT case when @PageSize=0 then 0 else COUNT(RowNumber) end AS RecordCount, @PageSize AS PageSize, @PageIndex AS PageIndex FROM #Results                                                                                         
                                                                                                
		SELECT * FROM #Results                                                                                                  
		WHERE @PageSize=0 or RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1                                                                                                 
	   END                                                                                                  
	   ELSE IF @Opcode = 48                                                                                                  
	   BEGIN                                                                                                  
	  SET @CustomerAutoId=(SELECT CustomerAutoId from CustomerPaymentDetails where PaymentAutoId=@PaymentAutoId)                    
                          
	  SELECT CPD.PaymentAutoId,PaymentId,FORMAT(PaymentDate,'MM/dd/yyyy') as PaymentDate,  --Modified date on 11/29/2019                                                                                                
	   CPD.paymentMode AS  PaymentMethod,ReferenceNo,                                                                         
	  ( SELECT COUNT(*) FROM PaymentOrderDetails AS TEMP WHERE  TEMP.PaymentAutoId=CPD.PaymentAutoId) AS Totalordes,                                                                                                  
	  CPD.ReceivedAmount,(EMP.FIRSTNAME+' ' +EMP.LASTNAME) AS EmpName ,ReceivedBy AS ProcessedBy ,                    
	  EmployeeRemarks,PaymentRemarks,                                                                                                  
	  isnull(CreditAmount,0.0) as CreditAmount,SPM.ChequeNo,convert(varchar(10),SPM.ChequeDate,101) AS ChequeDate,                                                                              
	  isnull((select CreditAmount from CustomerCreditMaster where CustomerAutoId=@CustomerAutoId),0.00)as TotalCreditAmount,                                                                                                  
	  CPD.Status,case when CPD.Status=0 then 'Settled' else 'Cancelled' end as StatusName,SortAmount,TotalCurrencyAmount                                                                           
	  FROM CustomerPaymentDetails AS CPD                                              
	  INNER JOIN EmployeeMaster AS EMP ON EMP.AutoId=CPD.EmpAutoId                                                                                                  
	   LEFT JOIN SalesPaymentMaster AS SPM ON SPM.PaymentAutoId=CPD.refPaymentId                                                                                                  
	  where CPD.PaymentAutoId =@PaymentAutoId                                                                                                  
                                                                                         
	  select (CONVERT(varchar(10),OrderDate,101)) as  OrderDate,OrderNo,po.ReceivedAmount,                
	  otm.OrderType AS OrderType  from PaymentOrderDetails as po                                                                                                  
	  inner join OrderMaster as om on om.AutoId=po.OrderAutoId
	  INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
	  where PaymentAutoId =@PaymentAutoId                                                                                       
                                          
                                                                              
	  select CurrencyName,NoofValue,TotalAmount from PaymentCurrencyDetails as pcd                                                                                    
	  inner join CurrencyMaster as cm on cm.AutoId=pcd.CurrencyAutoId                                                                                    
	  where PaymentAutoId=@PaymentAutoId                                                                        
	  order by   cm.CurrencyValue      desc                                                                             
                                              
			SELECT AutoId,PaymentMode from PAYMENTModeMaster --added on 11/29/2019 By Rizwan Ahmad                                                                                     
	   END                      
	   ELSE IF @Opcode=49                                                                                                  
	   BEGIN                                                      
		   SET @CustomerAutoId=(SELECT AutoId FROM CustomerMaster AS CM  WHERE [CustomerId] = @CustomerId)                                                                                                  
                                                                          
		   SELECT AUTOID INTO #OrderAutoId FROM OrderMaster WHERE CustomerAutoId= @CustomerAutoId                                                                                                  
		   SELECT ROW_NUMBER() OVER(ORDER BY AutoId desc) AS RowNumber, * INTO #Results47 FROM                                                                                                  
		(                                                                                                  
		SELECT OL.AutoId, EM.[FirstName] + ' ' + Em.[LastName] AS EmpName,AM.[Action],[Remarks],FORMAT([ActionDate],'MM/dd/yyyy hh:mm:tt') As ActionDate,OM.[OrderNo],
		FORMAT(Om.[OrderDate],'MM/dd/yyyy hh:mm:tt')  As OrderDate                                                                                          
		   FROM [dbo].[tbl_OrderLog] AS OL                                                            
		   INNER JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OL.[EmpAutoId]                                                                                                  
		   INNER JOIN [dbo].[tbl_ActionMaster] As AM ON AM.[AutoId] = OL.[ActionTaken]                                                                                           
		   INNER JOIN [dbo].[OrderMaster] AS OM ON OM.[AutoId] = OL.[OrderAutoId]                      
		   WHERE OrderAutoId IN (SELECT * FROM #OrderAutoId)                                                                                                  
		 --  ORDER BY OL.[AutoId] desc                                                                                                  
		  ) AS t ORDER BY AutoId                                                                                                  
       
                                     
		SELECT COUNT(RowNumber) AS RecordCount, @PageSize AS PageSize, @PageIndex AS PageIndex FROM #Results47                                                                                          
                                                                                                
		SELECT * FROM #Results47                                                                                                  
		WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1                                                                                                   
	   END                                                                                                  
	 ELSE IF @Opcode=411         
	  BEGIN                                           
                                                                                                     
	   SET @CustomerAutoId=(SELECT CustomerAutoId FROM CustomerPaymentDetails WHERE  PaymentAutoId=@PaymentAutoId)                                                                                                  
	   SELECT OrderAutoId,ReceivedAmount INTO #RESULT411 FROM PaymentOrderDetails                                                                                                 
	   WHERE  PaymentAutoId=@PaymentAutoId                                                                        
                                                                                                        
	   SELECT om.AutoId as OrderAutoId,otm.OrderType AS                                                                                                   
	   OrderType,OrderNo,(convert(varchar(10),OrderDate,101)) as OrderDate,DO.PayableAmount,DO.AmtPaid,DO.AmtDue,                                                             
	ReceivedAmount,0 AS Pay                                                                                                  
		FROM OrderMaster AS OM INNER JOIN DeliveredOrders AS DO ON OM.AutoId=DO.OrderAutoId                                                                                                  
		INNER JOIN #RESULT411 AS TEMP ON temp.OrderAutoId=OM.AutoId   
		INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
		UNION                                                                                                  
		SELECT om.AutoId as OrderAutoId,otm.OrderType AS                                                                                                   
	   OrderType,OrderNo,(convert(varchar(10),OrderDate,101) )OrderDate,DO.PayableAmount,DO.AmtPaid,DO.AmtDue,                                                     
		0.00 AS ReceivedAmount,1 AS Pay                        
		FROM OrderMaster AS OM INNER JOIN DeliveredOrders AS DO ON OM.AutoId=DO.OrderAutoId       
		INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
	   where CustomerAutoId =@CustomerAutoId and DO.AmtDue  >0                                                                                                  
	   And OrderAutoId not in (select OrderAutoId from #RESULT411)                                                                                              
                                                                                           
	   select CurrencyName,CurrencyValue,cm.AutoId,CurrencyValue,case when NoofValue is null then 0 else NoofValue end as NoofValue,                                                               
	   case when TotalAmount is null then 0 else TotalAmount end as TotalAmount   from CurrencyMaster as cm                                               
	   left join PaymentCurrencyDetails  as pcd on pcd.CurrencyAutoId=cm.AutoId and PaymentAutoId=@PaymentAutoId     
	where Status=1                                                                             
	   order by cm.CurrencyValue  desc                                                                         
                                                                                    
	  END                                                                                                  
                                                                    
	   ELSE IF @Opcode=23                                                                                  
	   BEGIN                                                                       
		SET @CustomerAutoId=(SELECT AutoId FROM CustomerMaster WHERE CustomerId=@CustomerId)                                                                                                  
		INSERT INTO CustomerDocumentMaster(CustomerAutoId,DocumentName,DocumentURL,UploadedDate,ModifyDate                          
	,UploadedBy,ModifyBy)                                                            
		VALUES(@CustomerAutoId,@DocumentName,@DocumentURL,GETDATE(),GETDATE(),@EmpAutoId,@EmpAutoId)                                                                     
	   END                                                                                                  
	   ELSE IF @Opcode=412                                                                                                  
	   BEGIN                                             
		   SET @CustomerAutoId=(SELECT AutoId FROM CustomerMaster WHERE CustomerId=@CustomerId)                                                         
		   SELECT  FileAutoId,DocumentName,DocumentURL FROM CustomerDocumentMaster                                                                                        
		   WHERE CustomerAutoId =@CustomerAutoId                                                                                       
	   END                                                                                                  
	   ELSE IF @Opcode=24                                                       BEGIN                                                                                                   
		 UPDATE CustomerDocumentMaster SET DocumentName =@DocumentName,                                                                                                  
		 DocumentURL=case when @DocumentURL is null or @DocumentURL='' then DocumentURL else @DocumentURL end                                                
		 WHERE FileAutoId=@FileAutoId                                                                                                  
	   END                                
	   ELSE IF @Opcode=32                                                                              
	   BEGIN                                                                                                   
		 DELETE FROM  CustomerDocumentMaster  WHERE FileAutoId=@FileAutoId                                                                                                  
	   END                                                                
	   ELSE IF @Opcode=413                                                         
	   BEGIN                                                                                                   
			SET @CustomerAutoId=(SELECT AutoId FROM CustomerMaster WHERE CustomerId=@CustomerId)       
		 SELECT ROW_NUMBER() OVER(ORDER BY PayId desc) AS RowNumber, * INTO #Results413 from                        
		(                                         
	   SELECT  om.AutoId,PaymentAutoId,PayId,CustomerName,FirstName+' '+ISNULL(LastName,'') + ' <b>('+empt.TypeName+')</b>'  as ReceivedBy,     
	  CONVERT(varchar(10),ReceivedDate,101) as ReceivedDate,PayType as PayCode,                                                                                                  
	  ReceivedAmount,case when PayType=2 then 'POS PAYMENT' ELSE 'SALES PAYMENT' END              
	  + case when om.AutoId is null then '' else ' <b>('+om.OrderNo+')</b>'  end AS PayType,                                                                                                  
	  case when ISNULL(om.status,11)= 11 then 0 else 1 end  as ProcessStatus,spm.PaymentMode as Pmode,   
	  pmm.PaymentMode,                
	  ReferenceId,Remarks,StatusType as Status,spm.Status as StatusCode,                                                                                                  
	  spm.ChequeNo,CONVERT(varchar(10),spm.ChequeDate,101) as ChequeDate                                                                        
	  from SalesPaymentMaster AS spm                              
	  INNER JOIN CustomerMaster AS CM ON spm.customerAutoid = CM.AutoId                                                                                                      
	  INNER JOIN EmployeeMaster AS emp ON emp.Autoid = spm.ReceiveBy                                                                                                 
	  INNER JOIN EmployeeTypeMaster AS empt ON emp.EmpType = empt.AutoId                                       
	  INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId=spm.Status and SM.[Category]='Pay'                                                                                                  
	  left join OrderMaster as om on om.AutoId=spm.OrderAutoId                    
	  left join PaymentModeMaster as pmm on                    
	  spm.PaymentMode=pmm.AutoId                                                                                                  
	  WHERE spm.CustomerAutoId   =@CustomerAutoId                                                  
	  and (@Status=0 or @Status is null or spm.Status =@Status)                                                                                                  
	  and (@FromDate is null or @FromDate = '' or @Todate is null or @Todate = '' or                       
	  (ReceivedDate between @FromDate and @Todate))                                                                                                         
	   )as t Order by PayId desc                               
                   
		SELECT COUNT(RowNumber) AS RecordCount,case when @PageSize=0 then COUNT(PayId) else @PageSize end AS PageSize,@PageIndex as PageIndex FROM #Results413             
                                      
		SELECT * FROM #Results413                                      
		WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))                                                                                              
	   END                                                                                                  
	 ELSE IF @Opcode=414                                                 
	   BEGIN                                                                                                          
	  SELECT  PaymentAutoId,PayId,CONVERT(varchar(10),ReceivedDate,101) as ReceivedDate,                                                                                                  
	  PayType as PayCode,ReceivedAmount,case when PayType=1 then 'POS PAYMENT' ELSE 'SALES PAYMENT' END AS PayType,                                                                                                  
	  PaymentMode,ReferenceId,Remarks,spm.ReceiveBy,                                                                                             
	  spm.Remarks,OrderAutoId,ChequeNo,CONVERT(varchar(10),ChequeDate,101) as ChequeDate from SalesPaymentMaster AS spm WHERE PaymentAutoId=@PaymentAutoId                                                                                                   
	  set @CustomerAutoId =(select CustomerAutoId from SalesPaymentMaster WHERE PaymentAutoId=@PaymentAutoId )                     
                                                                                                   
	  SELECT OM.AutoId as OrderAutoId,OrderNo,CONVERT(VARCHAR(10),OrderDate,101) AS OrderDate ,                                                                                                  
	  otm.OrderType AS                                                                                                 
	  OrderType,DO.PayableAmount,DO.AmtDue,DO.AmtPaid ,datediff(dd,OrderDate,getdate())  as DaysOld                                                                                                 
	  FROM OrderMaster AS OM INNER JOIN DeliveredOrders AS DO ON OM.AutoId=DO.OrderAutoId   
	   INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
	  WHERE CustomerAutoId=@CustomerAutoId AND DO.AmtDue > 0   and om.Status=11                       
                      
	  SELECT AutoID,PaymentMode FROM PAYMENTModeMaster                                                                                          
	   END                                                                                                  
	   ELSE IF @Opcode=25                                                                      
	   BEGIN                                                      
	  IF (select count(*) from CustomerPaymentDetails where refPaymentId=@PaymentAutoId)>0                                                  
	  BEGIN                                                  
	   SET @isException=1                                                  
	   SET @exceptionMessage='You can not cancel this payment because this payment has been already settled.'                                                  
	  END                                                   
	  ELSE                                                  
	  BEGIN                                                                                             
	UPDATE SalesPaymentMaster SET Status=3,cancelBy=@EmpAutoId,CancelDate=GETDATE(),CancelRemarks=@EmployeeRemarks,                                                                                                  
	   DateUpdate=GETDATE()                                                                                                  
	   where PaymentAutoId=@PaymentAutoId                                                   
	  END                                                       
                                                                                                     
	   END                                                                                                  
	 ELSE IF @Opcode=26                                                                                 
	   BEGIN                                                                                                  
	  UPDATE SalesPaymentMaster SET Status=4,chequeNo=@CheckNo,ChequeDate=@CheckDate,ChequeRemarks=@PaymentRemarks,                                               
	  DateUpdate=GETDATE()                                                                             
	  where PaymentAutoId=@PaymentAutoId                                                                             
	   END                                                                                            
                                                                                                   
	   ELSE IF @Opcode=415                                                                                                  
	   BEGIN                                                                                     
	   set @CustomerAutoId =(select AutoId from customerMaster WHERE customerid   =@customerid )                                                                                                         
		select *,5 as PaymentMode from customercreditmaster where customerAutoId=@CustomerAutoId          
                                                                                                 
		SELECT om.AutoId as OrderAutoId,OrderNo,CONVERT(VARCHAR(10),OrderDate,101) AS OrderDate ,                                                                                                
		otm.OrderType AS                                                                                                   
		OrderType,DO.PayableAmount,DO.AmtDue,DO.AmtPaid,datediff(dd,OrderDate,getdate())  as DaysOld                                                       
		FROM OrderMaster AS OM INNER JOIN DeliveredOrders AS DO ON OM.AutoId=DO.OrderAutoId     
		 INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
		WHERE CustomerAutoId=@CustomerAutoId AND DO.AmtDue > 0           
         
	 SELECT AutoID,PaymentMode FROM PAYMENTModeMaster -- Added on 11/29/2019 By Rizwan Ahmad                                                                
	   END                                                                                                  
	   ELSE IF @Opcode=416                                                                   
	   BEGIN                      
		set @CustomerAutoId =(select AutoId from customerMaster WHERE customerid   =@customerid )                                                                                       
		select CONVERT(VARCHAR(10),lo.CreatedDate,101) AS CreatedDate,(FirstName+' '+LastName) as Receivedby,Amount,                                                                                                
		case                                                                                                   
		when ReferenceType='OrderMaster' then (select OrderNo from OrderMaster om  where om.AutoId=lo.ReferenceNo)                                                                                   
		when ReferenceType='CustomerPaymentDetails' then (select sp.PayId from SalesPaymentMaster sp  where sp.PaymentAutoId=lo.ReferenceNo)                                                                                                  
		else '' end as ReferenceId                                                                                                      
		from tbl_Custumor_StoreCreditLog as lo inner join EmployeeMaster as emp on emp.AutoId=lo.CreatedBy                                                                                                  
		where customerAutoId=@CustomerAutoId                                                                             
                                                                                                       
	   END                                                                                    
	   ELSE IF @Opcode=417                                       
	   BEGIN                                                                                                   
	  set @CustomerAutoId =(select AutoId from customerMaster WHERE customerid=@customerid)                                                                                                     
	  SELECT om.AutoId as OrderAutoId,OrderNo  +' '+sm.StatusType as OrderNo,om.PayableAmount as DueAmount                                                                                
	  FROM OrderMaster AS OM left JOIN DeliveredOrders AS DO ON OM.AutoId=DO.OrderAutoId                                                                                                  
	  inner join StatusMaster as sm on sm.autoid=om.status and sm.Category='OrderMaster'                                                                                   
	  WHERE CustomerAutoId=@CustomerAutoId AND ISNULL(DO.AmtDue,om.PayableAmount) > 0                                         
	  and om.Status=11                                                                                              
                                                                                                
                                                                                       
	  SELECT  CreditAutoId,CreditNo,CONVERT(VARCHAR(10),CreditDate,101) AS CreditDate,TotalAmount                                                                                                  
	  FROM CreditMemoMaster AS OM                                                                       
	  WHERE                                                                                                   
	  Status=3 AND OrderAutoId is null                                                                                                   
	  AND                                                                                                  
	  CreditAutoId in                                                                                                   
	  (select * from [dbo].[fnSplitString](@ReferenceNo,','))                                                                                          
	   END                                                    
               
	   ELSE IF @Opcode=418                                                                                                  
	   BEGIN                                                                                                  
		SELECT AutoId,CityName FROM CityMaster where StateId=@State1                                                                                                  
	   END                                                                       
	   ELSE IF @Opcode=419                                                                                                  
	   BEGIN                                                        
		SELECT AutoId,Zipcode FROM ZipMaster where Zipcode like '%'+@Zipcode1+'%'                                                                                            
	   END                                                                                   
	   ELSE IF @Opcode=420                                                                                           
	   BEGIN                                                                                                                                                                                                              
	  select (select CityName from CityMaster where AutoId in (select CityId from ZipMaster where Zipcode=@Zipcode1)) as CityName,                                                                                  
	  (select StateName from State where AutoId in (select StateId from CityMaster where AutoId in (select CityId from ZipMaster where  
		 Zipcode=@Zipcode1))) as StateName,   
     
	  (select AutoId from CityMaster where AutoId in (select CityId from ZipMaster where Zipcode=@Zipcode1)) as CityId,                                                                                  
	  (select AutoId from State where AutoId in (select StateId from CityMaster where AutoId in (select CityId from ZipMaster where   Zipcode=@Zipcode1)))   
	  as StateId,     
                                                                                   
	  (select ZoneName from ZoneMaster as zm where zm.AutoId=zip.ZoneId) as ZoneName FROM ZipMaster as zip WHERE   Zipcode=@Zipcode1   
	   END                                 
	   else if @Opcode = 421                                
	   begin           
	   select(select [AutoId], [PriceLevelName] as PL FROM [dbo].[PriceLevelMaster] WHERE [Status] = 1 and PCustomerType = @CustomerType                                
	   Order by PriceLevelName for json path, INCLUDE_NULL_VALUES) as PriceLevel
	   --SELECT [AutoId], [PriceLevelName] FROM [dbo].[PriceLevelMaster] WHERE [Status] = 1 and PCustomerType = @CustomerType                                
	   --Order by PriceLevelName                            
	   end                                
                                
	   else if @Opcode=422                                
	   begin                                
	   select * from LocationMaster where Location != @LocationName                                
	   end                                
                                     
	   else if @Opcode=423                                
	   begin                                
	   select * from TBL_MST_SecurityMaster where SecurityValue = @SecurityKey and SecurityType = @EmpAutoId and typeEvent = 5                                
	   end                               
                              
	   else if @Opcode=424                                
	   begin                                
	   update SalesPaymentMaster set status = 3, CancelRemarks = @CancelRemark, cancelBy = @EmpAutoId, CancelDate = GETDATE() where PaymentAutoId = @PaymentAutoId                              
	   end                               
	   else if @Opcode=425                                
	   begin                 
		select AutoId,CardType from [dbo].[CardTypeMaster] order by  CardType asc                    
	   end                     
	   else if @Opcode=426                                
	   begin   
			 BEGIN TRY                                      
	  BEGIN TRAN                 
	 set @CustomerAutoId=(Select AutoId from CustomerMaster where CustomerId=@CustomerId)     
		 if @Status=1  
		 begin               
	  INSERT INTO  [dbo].[CustomerBankDetails]([CustomerAutoId],[BankName],[BankACC],Status,RoutingNo)                   
	  values (@CustomerAutoId,@BankName,@BankAcc,1,@RoutingNo)  
	  end
	  else  
	  begin  
	   INSERT INTO  [dbo].[CustomerBankDetails]([CustomerAutoId],[CardTypeAutoId],[CardNo],[ExpiryDate],[CVV],Status)             
	  values (@CustomerAutoId,@CardType,@CardNo,@ExpiryDate,@CVV,2)   
	  end  
	   COMMIT TRAN                                                                                            
		END TRY                                                                         
		BEGIN CATCH                                                                                            
		 ROLLBACK TRAN                               
		 SET @isException=1                                              
		 SET @exceptionMessage=ERROR_MESSAGE()                                                                                            
		END CATCH                       
	   end                    
		else if @Opcode=427                                
	   begin     
	   select cbd.AutoId,BankName,BankACC,RoutingNo from [dbo].[CustomerBankDetails] as cbd  

	  where CustomerAutoId=(select AutoId from CustomerMaster where CustomerId=@CustomerId)    and Status=1     
   
		select cbd.AutoId,CardNo,CONVERT(VARCHAR(10),ExpiryDate,101) AS ExpiryDate,CVV,CardType from [dbo].[CustomerBankDetails] as cbd            
	 inner join [dbo].[CardTypeMaster] as ctm on ctm.AutoId=cbd.CardTypeAutoId            
	 where CustomerAutoId=(select AutoId from CustomerMaster where CustomerId=@CustomerId)    and Status=2 
	   end               
		else if @Opcode=428                                
	   begin                                
		select AutoId,BankName,BankACC,CardNo,RoutingNo,CardRemark,BankRemark,CONVERT(VARCHAR(10),ExpiryDate,101) AS ExpiryDate,CardTypeAutoId,CVV,Zipcode from [dbo].[CustomerBankDetails] where AutoId=@AutoId                
		end               
		else if @Opcode=429                                
	   begin  
		BEGIN TRY                                      
	  BEGIN TRAN                             
	   set @CustomerAutoId=(Select AutoId from CustomerMaster where CustomerId=@CustomerId)    
	   if @Status=1  
	   begin  
	   update CustomerBankDetails set [BankName]=@BankName,[BankACC]=@BankAcc,RoutingNo=@RoutingNo,BankRemark=@PaymentRemarks,
	   ModifyDate=GETDATE(),ModifyBy=@EmpAutoId--,[CardTypeAutoId]=@CardType,[CardNo]=@CardNo,[ExpiryDate]=@ExpiryDate,[CVV]=@CVV            
	  where AutoId=@AutoId and Status=1   
	  end  
	  else  
	  begin  
	  update CustomerBankDetails set [CardTypeAutoId]=@CardType,[CardNo]=@CardNo,[ExpiryDate]=@ExpiryDate,ModifyDate=GETDATE(),ModifyBy=@EmpAutoId,[CVV]=@CVV,Zipcode=@Zipcode1,CardRemark=@PaymentRemarks            
	  where AutoId=@AutoId and Status=2  
    
	  end        
	   COMMIT TRAN                                                                                            
		END TRY                                                                         
		BEGIN CATCH                                                                                            
		 ROLLBACK TRAN                               
		 SET @isException=1                                              
		 SET @exceptionMessage='Oops! Something went wrong.Please try later.'                                                                                     
		END CATCH                
	   end               
		else if @Opcode=430                                
	   begin                                
		delete from  CustomerBankDetails where AutoId=@AutoId             
	   end              
		ELSE IF @Opcode=431                                                                                           
	   BEGIN         
			Select distinct ZM.CityId,CM.StateId,CM.CityName,SM.StateName,z.ZoneName from ZipMaster as ZM  
			INNER JOIN CityMaster AS CM ON  ZM.CityId=CM.AutoId  
			left join ZoneMaster z on z.AutoId=ZM.ZoneId
			INNER JOIN State as SM ON CM.StateId=SM.AutoId Where ZM.CityId=@CityAutoId AND ZM.Zipcode=@Zipcode1    
	   END    
	   ELSE IF @Opcode=432                                                                                          
	   BEGIN    
		 BEGIN TRY
			IF NOT EXISTS(SELECT AutoId FROM State WHERE StateName=trim(@StateName))                                                                           
			BEGIN                        
				SET @isException=0                                                                                                  
				SET @exceptionMessage='Invalidstate'                                                                                                  
			END
		 END TRY
		 BEGIN CATCH
			 SET @isException=1
			 SET @exceptionMessage=ERROR_MESSAGE()
		 END CATCH
	   END  
	   ELSE IF @Opcode=433                                                                                          
	   BEGIN         
			Select * from contactpersontypemaster
	   END  
	   ELSE IF @Opcode=434                                                                                          
	   BEGIN         
			Select ccp.AutoId,ccp.ContactPerson,ccp.Type,ctm.TypeName,ccp.MobileNo,ccp.Landline,ccp.Landline2,ccp.Fax,ccp.Email,ccp.AlternateEmail,
			ccp.CustomerAutoId,case when ISDefault=1 then 'Yes' else 'No' end as ISDefault from CustomerContactPerson ccp
			INNER JOIN contactpersontypemaster ctm on ctm.AutoId=ccp.Type
			where ccp.CustomerAutoId=@CustomerAutoId
	   END  
	   ELSE IF @Opcode=435                                                                                         
	   BEGIN 
		 BEGIN TRY
		  BEGIN TRANSACTION   
			IF @IsDefault=1
			BEGIN
				IF EXISTS(SELECT AUTOID FROM CustomerContactPerson WHERE ISDefault=1 AND CustomerAutoId=@CustomerAutoId)
				BEGIN
						UPDATE CustomerContactPerson SET ISDefault=0 WHere CustomerAutoId=@CustomerAutoId
				END
			END
			INSERT INTO CustomerContactPerson (ContactPerson,Type,MobileNo,Landline,Landline2,Fax,Email,AlternateEmail,CustomerAutoId,IsDefault,CreatedBy,CreatedDate,ModifyDate,ModifyBy) 
			VALUES(@ContactPersonName,@Type,@MobileNo,@Landline,@Landline2,@FaxNo,@Email,@AltEmail,@CustomerAutoId,@IsDefault,@EmpAutoId,GETDATE(),GETDATE(),@EmpAutoId)
			Declare @AutoIdentity int
			SET @AutoIdentity=scope_identity()
			if(@IsDefault=0)
			BEGIN
				IF NOT EXISTS(SELECT AUTOID FROM CustomerContactPerson WHERE ISDefault=1 AND CustomerAutoId=@CustomerAutoId)
				BEGIN
						UPDATE CustomerContactPerson SET ISDefault=1 WHere autoid=(select max(autoid) from  CustomerContactPerson where CustomerAutoId=@CustomerAutoId)
				END
			END
			SET @sql='
			IF '+Convert(varchar(20),@IsDefault)+'=1
			BEGIN
				IF EXISTS(SELECT AUTOID FROM ['+@ChildDbLocation+'].[dbo].CustomerContactPerson WHERE ISDefault=1 AND CustomerAutoId='+Convert(varchar(25),@CustomerAutoId)+')
				BEGIN
					UPDATE ['+@ChildDbLocation+'].[dbo].CustomerContactPerson SET ISDefault=0 WHere CustomerAutoId='+Convert(varchar(25),@CustomerAutoId)+'
				END
			END

			SET IDENTITY_INSERT ['+@ChildDbLocation+'].[dbo].CustomerContactPerson ON
			INSERT INTO ['+@ChildDbLocation+'].[dbo].CustomerContactPerson (AutoId,ContactPerson,Type,MobileNo,Landline,Landline2,Fax,Email,AlternateEmail,CustomerAutoId,IsDefault) 
			VALUES('+Convert(varchar(25),@AutoIdentity)+','''+@ContactPersonName+''',
			'+Convert(varchar(20),@Type)+','''+@MobileNo+''','''+@Landline+''',
			'''+@Landline2+''','''+@FaxNo+''','''+@Email+''','''+@AltEmail+''',
			'+Convert(varchar(25),@CustomerAutoId)+','+Convert(varchar(20),@IsDefault)+')
			SET IDENTITY_INSERT ['+@ChildDbLocation+'].[dbo].CustomerContactPerson OFF

			if('+Convert(varchar(20),@IsDefault)+'=0)
			BEGIN
				IF NOT EXISTS(SELECT AUTOID FROM ['+@ChildDbLocation+'].[dbo].CustomerContactPerson WHERE
				ISDefault=1 AND CustomerAutoId='+Convert(varchar(25),@CustomerAutoId)+')
				BEGIN
					UPDATE ['+@ChildDbLocation+'].[dbo].CustomerContactPerson SET ISDefault=1
					Where autoid=(select max(autoid) from  ['+@ChildDbLocation+'].[dbo].CustomerContactPerson where CustomerAutoId='+Convert(varchar(25),@CustomerAutoId)+')
				END
			END'
			EXEC sp_executesql @sql
			COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
		ROLLBACK TRANSACTION
			 SET @isException=1
			 SET @exceptionMessage=ERROR_MESSAGE()
		END CATCH
	   END  
	   ELSE IF @Opcode=436                                                                                         
	   BEGIN   
		 BEGIN TRY
		  BEGIN TRANSACTION
			SET @CustomerAutoId =(select customerautoid from CustomerContactPerson where AutoId=@AutoId)
			IF @IsDefault=1
			BEGIN
				IF EXISTS(SELECT AUTOID FROM CustomerContactPerson WHERE ISDefault=1 AND CustomerAutoId=@CustomerAutoId)
				BEGIN
						UPDATE CustomerContactPerson SET ISDefault=0 WHere CustomerAutoId=@CustomerAutoId
				END
			END
			UPDATE CustomerContactPerson SET ContactPerson=@ContactPersonName,Type=@Type,MobileNo=@MobileNo,Landline=@Landline,Landline2=@Landline2,ModifyDate=GETDATE(),ModifyBy=@EmpAutoId,
			Fax=@FaxNo,Email=@Email,AlternateEmail=@AltEmail,IsDefault=@IsDefault where AutoId=@AutoId
			SET @sql='
						IF '+Convert(varchar(20),@IsDefault)+'=1
						BEGIN
							IF EXISTS(SELECT AUTOID FROM ['+@ChildDbLocation+'].[dbo].CustomerContactPerson 
							WHERE ISDefault=1 AND CustomerAutoId='+Convert(varchar(20),@CustomerAutoId)+')
							BEGIN
								UPDATE ['+@ChildDbLocation+'].[dbo].CustomerContactPerson SET ISDefault=0 
								WHere CustomerAutoId='+Convert(varchar(20),@CustomerAutoId)+'
							END
						END

					update ecp set 
					ecp.[ContactPerson]=ccp.ContactPerson,
					ecp.[Type]=ccp.[Type],
					ecp.[MobileNo]=ccp.[MobileNo],
					ecp.[Landline]=ccp.[Landline],
					ecp.[Landline2]=ccp.[Landline2],
					ecp.[Fax]=ccp.[Fax],
					ecp.[Email]=ccp.[Email],
					ecp.[AlternateEmail]=ccp.[AlternateEmail],
					ecp.[ISDefault]=ccp.[ISDefault]
					FROM [dbo].[CustomerContactPerson] as ccp
					inner join ['+@ChildDbLocation+'].[dbo].[CustomerContactPerson] as ecp on ecp.CustomerAutoId=ccp.CustomerAutoId
					and ccp.AutoId=ecp.AutoId
					where ccp.AutoId='+Convert(varchar(20),@AutoId)+'
			 '
			EXEC sp_executesql @sql
		COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
		ROLLBACK TRANSACTION
			 SET @isException=1
			 SET @exceptionMessage=ERROR_MESSAGE()
		END CATCH
	   END 
	   ELSE IF @Opcode=437                                                                                         
	   BEGIN    
			DELETE FROM CustomerContactPerson where AutoId=@AutoId
			SET @sql='DELETE FROM ['+@ChildDbLocation+'].[dbo].CustomerContactPerson where AutoId='+convert(varchar(20),@AutoId)+''
			EXEC sp_executesql @sql
	   END 
	  END TRY                                                                                                  
	  BEGIN CATCH                                                                                                  
	   SET @isException=1                                                           
	   SET @exceptionMessage='Oops! Something went wrong.Please try later.'                                                  
	  END CATCH                                                                                                                                                                    
	END 
	GO
