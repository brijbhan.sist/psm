
create function [dbo].[UDF_CreditMemo_CheckMLTax]
(
 @CreditAutoId INT,  --Select dbo.[UDF_CreditMemo_CheckMLTax](562,2941)
 @CustomerAutoId INT
)
returns int
AS
BEGIN
   Declare @isMLManualyApply int=0
  IF EXISTS(Select TaxState from MLTaxMaster Where taxrate>0 and TaxState=(Select State from ShippingAddress SA INNER JOIN CustomerMaster CM ON CM.AutoId=SA.CustomerAutoId Where CustomerAutoId=@CustomerAutoId AND CM.DefaultShipAdd=SA.AutoId))
BEGIN
        IF(SELECT COUNT(1) from CreditItemMaster WHERE CreditAutoId=@CreditAutoId AND UnitMLQty>0)>0
		BEGIN
			 SET @isMLManualyApply=1
		END
END
return @isMLManualyApply
END