 
ALTER PROCEDURE [dbo].[ProcOrderAverageReprot]    
@Opcode INT=NULL,    
@FromDate datetime = null,    
@ToDate datetime = null,    
@Employee int=null,    
@EmpType int=null,    
@isException bit out,      
@exceptionMessage varchar(max) out      
AS      
BEGIN       
 BEGIN TRY      
   Set @isException=0      
   Set @exceptionMessage='Success'      
 IF @Opcode=41       
 BEGIN    
            Select WeekDay,sum(WeekDayCount) as WeekDayCount,sum(TotalOrders) as TotalOrders,sum(AvgWeeklyOrders) as AvgWeeklyOrders,sum(PackerOrder) as PackerOrder,    
   sum(AvgPacker) as AvgPacker,sum(DeliverOrder) as DeliverOrder,sum(AvgDeliver) as AvgDeliver,sum(ClosedOrder) as ClosedOrder,sum(AvgClosed) as AvgClosed,    
   sum(CancelOrder) as CancelOrder,sum(AvgCancel) as AvgCancel     
   from (    
   select wd as WeekDay, count(1) as WeekDayCount,0 as TotalOrders, 0 as AvgWeeklyOrders,0 as PackerOrder,0 as AvgPacker,0 as DeliverOrder,0 as AvgDeliver,    
   0 as ClosedOrder,0 as AvgClosed,0 as CancelOrder,0 as AvgCancel  from (    
   select convert(date,OrderDate) as OrderDate,DATENAME(w, OrderDate) as wd,count(1) as oc     
   from OrderMaster    
   where     
   (@FromDate = '' or @FromDate is null or @ToDate = '' or @ToDate is null or (CONVERT(date, OrderDate) between @FromDate and @ToDate))    
   group by convert(date,OrderDate),DATENAME(w, OrderDate)    
   ) as t group by wd     
  union    
   select wd as WeekDay, 0 as WeekDayCount,sum(oc) as TotalOrders, sum(oc)/count(1) as AvgWeeklyOrders,0 as PackerOrder,0 as AvgPacker,0 as DeliverOrder,0 as AvgDeliver,0 as ClosedOrder,0 as AvgClosed,0 as CancelOrder,0 as AvgCancel  from(    
   select convert(date,OrderDate) as OrderDate,DATENAME(w, OrderDate) as wd,count(1) as oc     
   from OrderMaster    
   inner join EmployeeMaster as em on     
   OrderMaster.SalesPersonAutoId=em.AutoId    
   where (@EmpType=0 or (@EmpType=2 and (OrderMaster.SalesPersonAutoId=@Employee or @Employee=0)) or @EmpType!=2 ) and    
   (@FromDate = '' or @FromDate is null or @ToDate = '' or @ToDate is null or CONVERT(date, OrderDate) between @FromDate and @ToDate)    
   group by convert(date,OrderDate),DATENAME(w, OrderDate)    
   ) as t group by wd      
  Union     
   select wd as WeekDay, 0 as WeekDayCount,0 as TotalOrders, 0 as AvgWeeklyOrders,sum(oc) as PackerOrder,sum(oc)/count(1) as AvgPacker,0 as DeliverOrder,0 as AvgDeliver,0 as ClosedOrder,0 as AvgClosed,0 as CancelOrder,0 as AvgCancel from(    
   select convert(date,gp.PackingDate) as OrderDate,DATENAME(w, gp.PackingDate) as wd,count(1) as oc     
   from OrderMaster as om    
   inner join GenPacking as gp on    
   om.AutoId=gp.OrderAutoId    
   inner join EmployeeMaster as em on     
   om.PackerAutoId=em.AutoId    
   where (@EmpType=0 or (@EmpType=3 and (@Employee=0 or om.PackerAutoId=@Employee))  or @EmpType!=3) and        
   (gp.PackingDate is not null or gp.PackingDate!='') and (@FromDate = '' or @FromDate is null or @ToDate = '' or @ToDate is null     
   or CONVERT(date, gp.PackingDate) between @FromDate and @ToDate)    
   group by convert(date,gp.PackingDate),DATENAME(w, gp.PackingDate)    
   ) as t  group by wd     
  Union    
   select wd as WeekDay, 0 as WeekDayCount,0 as TotalOrders, 0 as AvgWeeklyOrders,0 as PackerOrder,0 as AvgPacker,sum(oc) as DeliverOrder,sum(oc)/count(1) as AvgDeliver,0 as ClosedOrder,0 as AvgClosed,0 as CancelOrder,0 as AvgCancel from(    
   select convert(date,DeliveryDate) as OrderDate,DATENAME(w, DeliveryDate) as wd,count(1) as oc     
   from OrderMaster    
   inner join EmployeeMaster as em on     
   OrderMaster.ManagerAutoId=em.AutoId    
   where (@EmpType=0 or (@EmpType=5 and (@Employee=0 or OrderMaster.Driver=@Employee)) or @EmpType!=5) and    
    (DeliveryDate is not null or DeliveryDate!='') and (@FromDate = '' or @FromDate is null or @ToDate = '' or @ToDate is null or CONVERT(date, DeliveryDate) between @FromDate and @ToDate)    
   group by convert(date,DeliveryDate),DATENAME(w, DeliveryDate)    
   ) as t group by wd     
  Union    
   select wd as WeekDay, 0 as WeekDayCount,0 as TotalOrders, 0 as AvgWeeklyOrders,0 as PackerOrder,0 as AvgPacker,0 as DeliverOrder,0 as AvgDeliver,0 as ClosedOrder,0 as AvgClosed,sum(oc) as CancelOrder,sum(oc)/count(1) as AvgCancel from(    
   select convert(date,CancelledDate) as OrderDate,DATENAME(w, CancelledDate) as wd,count(1) as oc     
   from OrderMaster    
   inner join EmployeeMaster as em on     
   OrderMaster.CancelledBy=em.AutoId    
   where (@EmpType=0 or (@EmpType=2 and (@Employee=0 or OrderMaster.SalesPersonAutoId=@Employee)) or @EmpType!=2) and    
         (@EmpType=0 or (@EmpType=6 and (@Employee=0 or OrderMaster.AccountAutoId=@Employee)) or @EmpType!=6) and    
      (@EmpType=0 or (@EmpType=8 and (@Employee=0 or OrderMaster.ManagerAutoId=@Employee)) or @EmpType!=8) and    
         OrderMaster.Status=8 and (@FromDate = '' or @FromDate is null or @ToDate = '' or @ToDate is null or CONVERT(date, CancelledDate) between @FromDate and @ToDate)    
   group by convert(date,CancelledDate),DATENAME(w, CancelledDate)    
   ) as t group by wd      
  Union    
	select wd as WeekDay, 0 as WeekDayCount,0 as TotalOrders, 0 as AvgWeeklyOrders,0 as PackerOrder,0 as AvgPacker,0 as DeliverOrder,0 as AvgDeliver,
	sum(oc) as ClosedOrder,sum(oc)/count(1) as AvgClosed,0 as CancelOrder,0 as AvgCancel from(      
	select convert(date,Order_Closed_Date) as OrderDate,DATENAME(w, Order_Closed_Date) as wd,count(1) as oc       
	from OrderMaster     
	inner join EmployeeMaster as em on       
	OrderMaster.WarehouseAutoId=em.AutoId      
	where (@EmpType=0 or (@EmpType=6 and (@Employee=0 or OrderMaster.AccountAutoId=@Employee)) or @EmpType!=6) and      
	(Order_Closed_Date is not null or Order_Closed_Date!='') and (@FromDate = '' or @FromDate is null or @ToDate = '' or @ToDate is null or 
	CONVERT(date, Order_Closed_Date) between @FromDate and @ToDate)   
	group by convert(date,Order_Closed_Date),DATENAME(w, Order_Closed_Date)      
	) as t group by wd         
   )as m    
  group by WeekDay    
 order by case when [WeekDay]='Monday' then 1
			 when [WeekDay]='Tuesday' then 2
			 when [WeekDay]='Wednesday' then 3
			 when [WeekDay]='Thursday' then 4
			 when [WeekDay]='Friday' then 5
			 when [WeekDay]='Saturday' then 6
			 when [WeekDay]='Sunday' then 7 end asc    
 END     
 IF @Opcode=42 --User Type    
 BEGIN    
      Select AutoId as TID,TypeName as TN from EmployeeTypeMaster where AutoId in (2,3,5,6,8) order by TypeName asc  
	  for json path 
 END      
 IF @Opcode=43--All User    
 BEGIN    
 if(@EmpType != 0)    
 begin    
       Select AutoId as ETId,FirstName+' '+LastName as Name from EmployeeMaster where EmpType=@EmpType  and Status=1   
    order by FirstName+' '+LastName  for json path
    end    
    else    
    begin    
     Select AutoId as ETId,FirstName+' '+LastName as Name from EmployeeMaster where EmpType in (2,3,5,6,8)  and Status=1  
  order by FirstName+' '+LastName    for json path
    end    
 END    
 END TRY      
 BEGIN CATCH      
   Set @isException=1      
   Set @exceptionMessage=ERROR_MESSAGE()      
 END CATCH      
END       
