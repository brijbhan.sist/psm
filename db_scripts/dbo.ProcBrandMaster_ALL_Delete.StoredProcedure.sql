USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcBrandMaster_ALL_Delete]    Script Date: 08-09-2020 06:13:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[ProcBrandMaster_ALL_Delete]      
@BrandId  VARCHAR(50)=Null      
AS      
BEGIN       
    
 DECLARE @BrandAutoId INT= (SELECT AutoId FROM [psmct.a1whm.com].[dbo].[BrandMaster] WHERE BrandId=@BrandId)    
 IF NOT EXISTS(SELECT * FROM [psmct.a1whm.com].[dbo].[ProductMaster] WHERE BrandAutoId=@BrandAutoId)      
 BEGIN      
  DELETE FROM [psmct.a1whm.com].[dbo].[BrandMaster] WHERE BrandId=@BrandId    
 END      
   
 SET @BrandAutoId= (SELECT AutoId FROM [psmpa.a1whm.com].[dbo].[BrandMaster] WHERE BrandId=@BrandId)    
 IF NOT EXISTS(SELECT * FROM [psmpa.a1whm.com].[dbo].[ProductMaster] WHERE BrandAutoId=@BrandAutoId)      
 BEGIN      
  DELETE FROM [psmpa.a1whm.com].[dbo].[BrandMaster] WHERE BrandId=@BrandId    
 END   
  
 SET @BrandAutoId= (SELECT AutoId FROM [psmnpa.a1whm.com].[dbo].[BrandMaster] WHERE BrandId=@BrandId)    
 IF NOT EXISTS(SELECT * FROM [psmnpa.a1whm.com].[dbo].[ProductMaster] WHERE BrandAutoId=@BrandAutoId)      
 BEGIN      
  DELETE FROM [psmnpa.a1whm.com].[dbo].[BrandMaster] WHERE BrandId=@BrandId    
 END  
  
 SET @BrandAutoId= (SELECT AutoId FROM [psmwpa.a1whm.com].[dbo].[BrandMaster] WHERE BrandId=@BrandId)    
 IF NOT EXISTS(SELECT * FROM [psmwpa.a1whm.com].[dbo].[ProductMaster] WHERE BrandAutoId=@BrandAutoId)      
 BEGIN      
  DELETE FROM [psmwpa.a1whm.com].[dbo].[BrandMaster] WHERE BrandId=@BrandId    
 END  
 
 SET @BrandAutoId= (SELECT AutoId FROM [psmny.a1whm.com].[dbo].[BrandMaster] WHERE BrandId=@BrandId)    
 IF NOT EXISTS(SELECT * FROM [psmny.a1whm.com].[dbo].[ProductMaster] WHERE BrandAutoId=@BrandAutoId)      
 BEGIN      
  DELETE FROM [psmny.a1whm.com].[dbo].[BrandMaster] WHERE BrandId=@BrandId    
 END 

 SET @BrandAutoId= (SELECT AutoId FROM [psmnj.easywhm.com].[dbo].[BrandMaster] WHERE BrandId=@BrandId)    
 IF NOT EXISTS(SELECT * FROM [psmnj.easywhm.com].[dbo].[ProductMaster] WHERE BrandAutoId=@BrandAutoId)      
 BEGIN      
  DELETE FROM [psmnj.easywhm.com].[dbo].[BrandMaster] WHERE BrandId=@BrandId    
 END 
 
 SET @BrandAutoId= (SELECT AutoId FROM [psmpa.easywhm.com].[dbo].[BrandMaster] WHERE BrandId=@BrandId)    
 IF NOT EXISTS(SELECT * FROM [psmpa.easywhm.com].[dbo].[ProductMaster] WHERE BrandAutoId=@BrandAutoId)      
 BEGIN      
  DELETE FROM [psmpa.easywhm.com].[dbo].[BrandMaster] WHERE BrandId=@BrandId    
 END 

 SET @BrandAutoId= (SELECT AutoId FROM [psmnpa.easywhm.com].[dbo].[BrandMaster] WHERE BrandId=@BrandId)    
 IF NOT EXISTS(SELECT * FROM [psmnpa.easywhm.com].[dbo].[ProductMaster] WHERE BrandAutoId=@BrandAutoId)      
 BEGIN      
  DELETE FROM [psmnpa.easywhm.com].[dbo].[BrandMaster] WHERE BrandId=@BrandId    
 END 

 SET @BrandAutoId= (SELECT AutoId FROM [psmwpa.easywhm.com].[dbo].[BrandMaster] WHERE BrandId=@BrandId)    
 IF NOT EXISTS(SELECT * FROM [psmwpa.easywhm.com].[dbo].[ProductMaster] WHERE BrandAutoId=@BrandAutoId)      
 BEGIN      
  DELETE FROM [psmwpa.easywhm.com].[dbo].[BrandMaster] WHERE BrandId=@BrandId    
 END 

 SET @BrandAutoId= (SELECT AutoId FROM [psmct.easywhm.com].[dbo].[BrandMaster] WHERE BrandId=@BrandId)    
 IF NOT EXISTS(SELECT * FROM [psmct.easywhm.com].[dbo].[ProductMaster] WHERE BrandAutoId=@BrandAutoId)      
 BEGIN      
  DELETE FROM [psmct.easywhm.com].[dbo].[BrandMaster] WHERE BrandId=@BrandId    
 END 

 SET @BrandAutoId= (SELECT AutoId FROM [psmny.easywhm.com].[dbo].[BrandMaster] WHERE BrandId=@BrandId)    
 IF NOT EXISTS(SELECT * FROM [psmny.easywhm.com].[dbo].[ProductMaster] WHERE BrandAutoId=@BrandAutoId)      
 BEGIN      
  DELETE FROM [psmny.easywhm.com].[dbo].[BrandMaster] WHERE BrandId=@BrandId    
 END 
END  