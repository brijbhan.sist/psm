Alter PROCEDURE [dbo].[ProcMLQtySummaryReport]
@Opcode INT=NULL,
@CustomerAutoId INT=NULL,
@ProductAutoId INT=NULL,
@StateAutoId int=null,
@FromDate date =NULL,
@ToDate date =NULL,
@PageIndex INT=1,
@PageSize INT=10,
@RecordCount INT=null,
@isException bit out,
@exceptionMessage varchar(max) out
AS
BEGIN	
	BEGIN TRY
		Set @isException=0
		Set @exceptionMessage='Success'

	 IF @Opcode=41
		BEGIN
       select
		(
		SELECT AutoId AS CUID,CustomerId + ' ' + CustomerName as CUN FROM CustomerMaster ORDER BY CUN ASC
		for json path
		) as Customer,		
		(
		SELECT AutoId as PID ,(Convert(varchar(10),ProductId) + ' - ' +ProductName) AS PN from ProductMaster	ORDER BY PN	
		for json path
		) as Product
		for json path
		
		END
		ELSE If @Opcode=42	
		BEGIN
		
		  SELECT ROW_NUMBER() OVER(ORDER BY [CustomerName]) AS RowNumber, * INTO #Results43 FROM
				(
				select cm.CustomerName,CONVERT(varchar(10),Orderdate,101) as OrderDate,OrderNo,
				ISNULL(oim.UnitMLQty,0) as UnitMLQty,isnull(oim.TotalMLQty,0) as TotalMLQty,pm.ProductName,
				isnull(oim.QtyDel,0) as TotalPieces from Delivered_Order_Items as oim 
				inner join OrderMaster as om on om.AutoId=oim.OrderAutoId
				inner join CustomerMaster as cm on cm.AutoId=om.CustomerAutoId
				inner join ProductMaster as pm on pm.AutoId=oim.ProductAutoId
				where TotalPieces>0 and UnitMLQty>0 and
				(@CustomerAutoId is null or @CustomerAutoId=0 or cm.AutoId=@CustomerAutoId)
				and (@ProductAutoId is null or @ProductAutoId=0 or oim.ProductAutoId=@ProductAutoId)
				and (@FromDate is null or @ToDate is null or (CONVERT(date,OrderDate) 
				between convert(date,@FromDate) and CONVERT(date,@ToDate))) AND om.Status=11
				
				) AS t ORDER BY [CustomerName]
				
				SELECT COUNT(CustomerName) AS RecordCount, case when @PageSize=0 then COUNT(CustomerName) else @PageSize end
				AS PageSize, @PageIndex AS PageIndex,Format(getdate(),'MM/dd/yyyy hh:mm tt') as PrintDate FROM #Results43
				SELECT * FROM #Results43
				WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))		
												
				 SELECT ISNULL(SUM(UnitMLQty),0.00) AS UnitMLQty,ISNULL(SUM(TotalMLQty),0.00) as TotalMLQty,ISNULL(SUM(TotalPieces),0) AS TotalPieces from  #Results43
		END
	
	  
	   ELSE If @Opcode=43	
		BEGIN
		
		 
				select cm.CustomerName,CONVERT(varchar(10),Orderdate,101) as OrderDate,OrderNo,
				ISNULL(oim.UnitMLQty,0) as UnitMLQty,isnull(oim.TotalMLQty,0) as TotalMLQty,pm.ProductName,
				isnull(oim.TotalPieces,0) as TotalPieces from OrderItemMaster as oim 
				inner join OrderMaster as om on om.AutoId=oim.OrderAutoId
				inner join CustomerMaster as cm on cm.AutoId=om.CustomerAutoId
				inner join ProductMaster as pm on pm.AutoId=oim.ProductAutoId
				where TotalPieces>0 and UnitMLQty>0 and
				(@CustomerAutoId is null or @CustomerAutoId=0 or cm.AutoId=@CustomerAutoId)
				and (@ProductAutoId is null or @ProductAutoId=0 or pm.AutoId=@ProductAutoId)
				and (@FromDate is null or @ToDate is null or (CONVERT(date,OrderDate) 
				between convert(date,@FromDate) and CONVERT(date,@ToDate)))													
				
		END
	END TRY
	BEGIN CATCH
		Set @isException=1
		Set @exceptionMessage=ERROR_MESSAGE()
	END CATCH
END	
   
GO
