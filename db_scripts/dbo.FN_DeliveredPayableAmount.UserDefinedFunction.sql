USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_DeliveredPayableAmount]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION  [dbo].[FN_DeliveredPayableAmount]
(
	 @orderAutoId int
)
RETURNS decimal(10,2)
AS
BEGIN
	DECLARE @PayableAmount decimal(10,2)	
	set @PayableAmount=isnull((select PayableAmount from OrderMaster where AutoId=@orderAutoId)	,0.00)
	RETURN @PayableAmount
END

GO
