ALTER FUNCTION [dbo].[fn_GetCustomerAndSalesPerson]   
(   
 @VenorAutoId int,  
 @LocationId int  
)   
RETURNS @output TABLE(CustomerAutoId int,SalesPersonAutoId int,LocationAutoId int,  
ShipAddrAutoId int,BillAddrAutoId int,IsTaxApply int,TaxValue decimal(10,2),TaxType int,  
Weigth_Tax decimal(15,2),MLTax decimal(10,2),Terms int  
  
)   
BEGIN   
   
	Declare @CustomerAutoId int,@SalesPersonAutoId int,@LocationAutoId int  
	Declare @Terms int,@ShipAddrAutoId int,@BillAddrAutoId int,@StateId int  
	declare @TaxValue decimal(10,2),@TaxType int,@IsTaxApply int=0  
	DECLARE @Weigth_Tax decimal(15,2),@MLTax decimal(10,2)   
	select @LocationAutoId=LocationAutoId from VendorMaster where AutoId=@VenorAutoId  
	IF(@LocationAutoId=1)  
	BEGIN  
		select @CustomerAutoId=AutoId,@SalesPersonAutoId=SalesPersonAutoId,@ShipAddrAutoId=DefaultShipAdd,@BillAddrAutoId=DefaultBillAdd,  
		@Terms=Terms from [psmnj.a1whm.com].[dbo].[CustomerMaster]   
		where LocationAutoId=@LocationId  
		Set @StateId=(Select State from [psmnj.a1whm.com].[dbo].[BillingAddress] where AutoId=@BillAddrAutoId)   
  
		IF EXISTS(SELECT * FROM [psmnj.a1whm.com].[dbo].[TaxTypeMaster] WHERE State=@StateId)    
		BEGIN    
		SET @IsTaxApply=1   
		select  @TaxValue=Value,@TaxType=AutoId from [psmnj.a1whm.com].[dbo].[TaxTypeMaster] WHERE State=@StateId   
		END        
		set @Weigth_Tax= ISNULL((select value from [psmnj.a1whm.com].[dbo].[Tax_Weigth_OZ] where [ID]=1),0)   
		set @MLTax=ISNULL((SELECT TaxRate FROM [psmnj.a1whm.com].[dbo].[MLTaxMaster] where TaxState=@StateId),0.00)   
	END  
	ELSE IF (@LocationAutoId=2)  
	BEGIN  
		select @CustomerAutoId=AutoId,@SalesPersonAutoId=SalesPersonAutoId,@ShipAddrAutoId=DefaultShipAdd,@BillAddrAutoId=DefaultBillAdd,  
		@Terms=Terms from [psmct.a1whm.com].[dbo].[CustomerMaster]   
		where LocationAutoId=@LocationId  
		Set @StateId=(Select State from [psmct.a1whm.com].[dbo].[BillingAddress] where AutoId=@BillAddrAutoId)   
  
		IF EXISTS(SELECT * FROM [psmct.a1whm.com].[dbo].[TaxTypeMaster] WHERE State=@StateId)    
		BEGIN    
		SET @IsTaxApply=1   
		select  @TaxValue=Value,@TaxType=AutoId from [psmct.a1whm.com].[dbo].[TaxTypeMaster] WHERE State=@StateId   
		END   
		set @Weigth_Tax= ISNULL((select value from [psmct.a1whm.com].[dbo].[Tax_Weigth_OZ] where [ID]=1),0)     
		set @MLTax=ISNULL((SELECT TaxRate FROM [psmct.a1whm.com].[dbo].[MLTaxMaster] where TaxState=@StateId),0.00)    
	END  
	ELSE IF (@LocationAutoId=4)  
	BEGIN  
		select @CustomerAutoId=AutoId,@SalesPersonAutoId=SalesPersonAutoId,@ShipAddrAutoId=DefaultShipAdd,@BillAddrAutoId=DefaultBillAdd,  
		@Terms=Terms from [psmpa.a1whm.com].[dbo].[CustomerMaster]   
		where LocationAutoId=@LocationId  
		Set @StateId=(Select State from [psmpa.a1whm.com].[dbo].[BillingAddress] where AutoId=@BillAddrAutoId)   
  
		IF EXISTS(SELECT * FROM [psmpa.a1whm.com].[dbo].[TaxTypeMaster] WHERE State=@StateId)    
		BEGIN    
		SET @IsTaxApply=1   
		select  @TaxValue=Value,@TaxType=AutoId from [psmpa.a1whm.com].[dbo].[TaxTypeMaster] WHERE State=@StateId   
		END   
		set @Weigth_Tax= ISNULL((select value from [psmpa.a1whm.com].[dbo].[Tax_Weigth_OZ] where [ID]=1),0)  
     
		set @MLTax=ISNULL((SELECT TaxRate FROM [psmpa.a1whm.com].[dbo].[MLTaxMaster] where TaxState=@StateId),0.00)    
	END  
	ELSE IF (@LocationAutoId=5)  
	BEGIN  
		select @CustomerAutoId=AutoId,@SalesPersonAutoId=SalesPersonAutoId,@ShipAddrAutoId=DefaultShipAdd,@BillAddrAutoId=DefaultBillAdd,  
		@Terms=Terms from [psmnpa.a1whm.com].[dbo].[CustomerMaster]   
		where LocationAutoId=@LocationId  
		Set @StateId=(Select State from [psmnpa.a1whm.com].[dbo].[BillingAddress] where AutoId=@BillAddrAutoId)   
  
		IF EXISTS(SELECT * FROM [psmnpa.a1whm.com].[dbo].[TaxTypeMaster] WHERE State=@StateId)    
		BEGIN    
		SET @IsTaxApply=1   
		select  @TaxValue=Value,@TaxType=AutoId from [psmnpa.a1whm.com].[dbo].[TaxTypeMaster] WHERE State=@StateId   
		END   
		set @Weigth_Tax= ISNULL((select value from [psmnpa.a1whm.com].[dbo].[Tax_Weigth_OZ] where [ID]=1),0)  
		set @MLTax=ISNULL((SELECT TaxRate FROM [psmnpa.a1whm.com].[dbo].[MLTaxMaster] where TaxState=@StateId),0.00)    
	END  

	ELSE IF (@LocationAutoId=7)  
	BEGIN  
		select @CustomerAutoId=AutoId,@SalesPersonAutoId=SalesPersonAutoId,@ShipAddrAutoId=DefaultShipAdd,@BillAddrAutoId=DefaultBillAdd,  
		@Terms=Terms from [psm.a1whm.com].[dbo].[CustomerMaster]   
		where LocationAutoId=@LocationId  
		Set @StateId=(Select State from [psm.a1whm.com].[dbo].[BillingAddress] where AutoId=@BillAddrAutoId)   
  
		IF EXISTS(SELECT * FROM [psm.a1whm.com].[dbo].[TaxTypeMaster] WHERE State=@StateId)    
		BEGIN    
		SET @IsTaxApply=1   
		select  @TaxValue=Value,@TaxType=AutoId from [psm.a1whm.com].[dbo].[TaxTypeMaster] WHERE State=@StateId   
		END   
		set @Weigth_Tax= ISNULL((select value from [psm.a1whm.com].[dbo].[Tax_Weigth_OZ] where [ID]=1),0)   
		set @MLTax=ISNULL((SELECT TaxRate FROM [psm.a1whm.com].[dbo].[MLTaxMaster] where TaxState=@StateId),0.00)   
	END
	ELSE IF (@LocationAutoId=8)  
	BEGIN  
		select @CustomerAutoId=AutoId,@SalesPersonAutoId=SalesPersonAutoId,@ShipAddrAutoId=DefaultShipAdd,@BillAddrAutoId=DefaultBillAdd,  
		@Terms=Terms from [demo.a1whm.com].[dbo].[CustomerMaster]   
		where LocationAutoId=@LocationId  
		Set @StateId=(Select State from [demo.a1whm.com].[dbo].[BillingAddress] where AutoId=@BillAddrAutoId)   
  
		IF EXISTS(SELECT * FROM [demo.a1whm.com].[dbo].[TaxTypeMaster] WHERE State=@StateId)    
		BEGIN    
		SET @IsTaxApply=1   
		select  @TaxValue=Value,@TaxType=AutoId from [demo.a1whm.com].[dbo].[TaxTypeMaster] WHERE State=@StateId   
		END   
		set @Weigth_Tax= ISNULL((select value from [demo.a1whm.com].[dbo].[Tax_Weigth_OZ] where [ID]=1),0)   
		set @MLTax=ISNULL((SELECT TaxRate FROM [demo.a1whm.com].[dbo].[MLTaxMaster] where TaxState=@StateId),0.00)   
	END
	ELSE IF (@LocationAutoId=9)  
	BEGIN  
		select @CustomerAutoId=AutoId,@SalesPersonAutoId=SalesPersonAutoId,@ShipAddrAutoId=DefaultShipAdd,@BillAddrAutoId=DefaultBillAdd,  
		@Terms=Terms from [psmwpa.a1whm.com].[dbo].[CustomerMaster]   
		where LocationAutoId=@LocationId  
		Set @StateId=(Select State from [psmwpa.a1whm.com].[dbo].[BillingAddress] where AutoId=@BillAddrAutoId)   
  
		IF EXISTS(SELECT * FROM [demo.a1whm.com].[dbo].[TaxTypeMaster] WHERE State=@StateId)    
		BEGIN    
		SET @IsTaxApply=1   
		select  @TaxValue=Value,@TaxType=AutoId from [psmwpa.a1whm.com].[dbo].[TaxTypeMaster] WHERE State=@StateId   
		END   
		set @Weigth_Tax= ISNULL((select value from [psmwpa.a1whm.com].[dbo].[Tax_Weigth_OZ] where [ID]=1),0)   
		set @MLTax=ISNULL((SELECT TaxRate FROM [psmwpa.a1whm.com].[dbo].[MLTaxMaster] where TaxState=@StateId),0.00)   
	END
	 INSERT INTO @output (CustomerAutoId,SalesPersonAutoId,LocationAutoId,  
	 ShipAddrAutoId,BillAddrAutoId,IsTaxApply,TaxValue,TaxType,Weigth_Tax,MLTax,Terms)   
	 select @CustomerAutoId as CustomerAutoId,@SalesPersonAutoId,@LocationAutoId,  
	 @ShipAddrAutoId,@BillAddrAutoId,@IsTaxApply,0,@TaxType,0,0,@Terms    
	RETURN   
  END  
GO
