USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Order_MLTax]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

alter table OrderMaster drop column MLTax
drop function FN_Order_MLTax
go
CREATE FUNCTION  [dbo].[FN_Order_MLTax]
( 
	@orderAutoId int,
	@status int
)
RETURNS decimal(18,2)
AS
BEGIN
	DECLARE @MLTax decimal(18,2)=0.00	
	if(@status=11)
	begin	
	SET @MLTax=(SELECT sum(Item_TotalMLTax) FROM Delivered_Order_Items WHERE OrderAutoId=@orderAutoId)	
	end
	else
	begin 	 
	SET @MLTax=(SELECT sum(Item_TotalMLTax) FROM OrderItemMaster WHERE OrderAutoId=@orderAutoId)	
	end
	RETURN @MLTax
END
GO
alter table OrderMaster add [MLTax]  AS ([dbo].[FN_Order_MLTax]([AutoId],[Status]))
