USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcCommissionMaster]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[ProcCommissionMaster]  
	 @Opcode int=null,    
	 @AutoId int=null,    
	 @CommissionCode decimal(18,4)=null,    
	 @Description varchar(250)=null,  
	 @Status int=null,    
	 @Createdby int=null,    
	 @CommissionXml xml=null,                                                
	 @isException bit out,                            
	 @exceptionMessage varchar(max) out      
as    
BEGIN                       
BEGIN TRY                      
	  SET @isException=0                      
	  SET @exceptionMessage='Success'    
 IF @Opcode=11    
 BEGIN    
  IF EXISTS(SELECT *from Tbl_CommissionMaster where  CommissionCode=(SELECT tr.td.value('CommissionCode[1]','decimal(18,4)') as CommissionCode FROM @CommissionXml.nodes('/XmlCommission') tr(td) ))     
  BEGIN    
	   SET @isException=1    
	   SET @exceptionMessage='Commission code already exist.'    
  END    
  ELSE    
  BEGIN    
	   insert into Tbl_CommissionMaster(CommissionCode,Description,Status,CreatedBy,CreatedOn,UpdatedOn,UpdatedBy)    
	   SELECT tr.td.value('CommissionCode[1]','decimal(18,4)') as CommissionCode,    
	   tr.td.value('Description[1]',' varchar(250)') as Description,    
	   tr.td.value('Status[1]','int') as Status,    
	   @Createdby,    
	   GETDATE(),   
	   GETDATE(),  
	   @Createdby   
	 from  @CommissionXml.nodes('/XmlCommission') tr(td)    
  END    
 END    
    
 ELSE IF @Opcode=21    
 BEGIN    
	   IF  EXISTS(SELECT *from Tbl_CommissionMaster where      
	   CommissionCode=(SELECT tr.td.value('CommissionCode[1]','decimal(18,4)') as CommissionCode FROM @CommissionXml.nodes('/XmlCommission') tr(td) ) and     
	   Autoid!=(SELECT tr.td.value('Autoid[1]','int') as Autoid FROM @CommissionXml.nodes('/XmlCommission') tr(td)))    
	   BEGIN    
	   SET @isException=1    
	   SET @exceptionMessage='Commission code already exist.'    
	   END    
	   BEGIN    
	   UPDATE Tbl_CommissionMaster     
	   SET CommissionCode=(SELECT tr.td.value('CommissionCode[1]','decimal(18,4)') as CommissionCode FROM @CommissionXml.nodes('/XmlCommission') tr(td) ),    
	   Description=(SELECT tr.td.value('Description[1]',' varchar(250)') as Description FROM @CommissionXml.nodes('/XmlCommission') tr(td)),    
	   Status=(SELECT tr.td.value('Status[1]','int') as Status FROM @CommissionXml.nodes('/XmlCommission') tr(td)),    
	   CreatedBy=@Createdby,    
	   CreatedOn=GETDATE(),    
	   UpdatedBy=@Createdby,    
	   UpdatedOn=GETDATE() WHERE     
	   Autoid=(SELECT tr.td.value('Autoid[1]','int') as Autoid FROM @CommissionXml.nodes('/XmlCommission') tr(td))    
  END     
 END  
 ELSE IF @Opcode=41    
 BEGIN    
       SELECT AutoId,CommissionCode,Description,(case when Status=1 then 'Active' ELSE  'Inactive' END) as Status FROM Tbl_CommissionMaster  
 END    
    
 ELSE IF @OpCode=42    
 BEGIN    
       SELECT AutoId,CommissionCode,Description,Status FROM Tbl_CommissionMaster WHERE AutoId=@AutoId 
 END    
 ELSE IF @Opcode=31    
 BEGIN    
  BEGIN try   
       IF EXISTS(Select * from ProductMaster Where P_CommCode=(Select CommissionCode from Tbl_CommissionMaster where AutoId=@AutoId))
	   BEGIN
	       SET @isException=1                      
           SET @exceptionMessage='Commission code has been used in application.'       
	   END
	   ELSE
	   BEGIN
            DELETE FROM Tbl_CommissionMaster WHERE AutoId =@AutoId  
	   END  
  END try    
  BEGIN CATCH                      
	   SET @isException=1                      
	   SET @exceptionMessage='Commission code has been used in application.'                      
  END CATCH      
 END     
END TRY                      
BEGIN CATCH                      
	   SET @isException=1                      
       SET @exceptionMessage='Oops! Something went wrong.Please try later.'                    
END CATCH                      
END 
GO
