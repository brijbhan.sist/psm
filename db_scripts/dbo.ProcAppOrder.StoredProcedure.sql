USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcAppOrder]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
alter PROCEDURE [dbo].[ProcAppOrder]                    
@Opcode INT=NULL,                      
@timeStamp datetime =null,                
@referenceOrderNumber varchar(50)=null,                   
@orderdate datetime = null,                  
@CustomerAutoId INT=NULL,                    
@TotalAmount DECIMAL(8,2)=NULL,                    
@OverallDisc DECIMAL(8,2)=NULL,                    
@OverallDiscAmt DECIMAL(8,2)=NULL,                   
@ShippingType INT=NULL,                     
@ShippingCharges DECIMAL(8,2)=NULL,                   
@TaxType int=NULL,                        
@TotalTax DECIMAL(8,2)=NULL,                    
@GrandTotal DECIMAL(8,2)=NULL,                   
@Remarks VARCHAR(max)=NULL,                    
@DeliveryDate DATETIME=NULL,                   
@BillAddrAutoId INT=NULL,                    
@ShipAddrAutoId INT=NULL,                    
@SalesPersonAutoId INT=NULL,      
@RouteAutoId int =null,  
@RouteStatus int =null, 
@DeviceID varchar(500)=null,  
@appVersion varchar(50)=null,  
@latLong varchar(max)=null,  
@orderItems [DT_APP_OrderItemList] readonly,                    
@isException bit out,                    
@exceptionMessage varchar(max) out                    
AS                    
BEGIN                     
 BEGIN TRY                    
  Set @isException=0                    
  Set @exceptionMessage='Success'                    
 declare @MLQty decimal(10,2) ,@MLTax decimal(10,2),@StateAutoId int ,@MLType INT  =0          
  If @Opcode=101                      
  BEGIN                    
 BEGIN TRY                    
  BEGIN TRAN                       
  IF isnull(@referenceOrderNumber,'')=''                  
  Begin                 
   Set @isException=1                    
   Set @exceptionMessage='referenceOrderNumber is mandetory'              
  End                  
  Else IF exists(select * from [OrderMaster] where referenceOrderNumber=@referenceOrderNumber and SalesPersonAutoId=@SalesPersonAutoId)                 
  Begin                 
   Set @isException=0                   
   Set @exceptionMessage='Order already exists'              
   select AutoId as OrderAutoId,OrderNo,(select statustype from [dbo].[StatusMaster] where [AutoId]=Status and [Category] = 'OrderMaster')status         
   from [OrderMaster] where referenceOrderNumber=@referenceOrderNumber          
  End                  
  Else                  
  Begin                
   Declare @OrderNo varchar(50) =(SELECT DBO.SequenceCodeGenerator('OrderNumber'))                  
                       
	Declare @Terms int=(SELECT Terms  FROM CustomerMaster WHERE AutoId=@CustomerAutoId)                    
	set @StateAutoId=(select top 1 State from BillingAddress where CustomerAutoId=@CustomerAutoId)    
	DECLARE @IsTaxApply INT=0   
	DECLARE @Weigth_Tax decimal(15,2)=ISNULL((select value from [Tax_Weigth_OZ] where [ID]=1),0)  
  IF EXISTS(SELECT * FROM TaxTypeMaster WHERE AutoId=@TaxType AND State=@StateAutoId)  
  BEGIN  
  SET @IsTaxApply=1  
  END        
    declare @TaxValue decimal(10,2)=isnull((select Value from TaxTypeMaster where AutoId =@TaxType),0.00)    
    INSERT INTO [dbo].[OrderMaster] ([OrderNo],OrderType,[OrderDate],[DeliveryDate],[CustomerAutoId],[Terms],[BillAddrAutoId],[ShipAddrAutoId],                    
    [SalesPersonAutoId],[OverallDiscAmt],[ShippingCharges],[Status],                  
    [ShippingType], TaxType,TaxValue,OrderRemarks,referenceOrderNumber,mlTaxPer,IsTaxApply,RouteAutoId,RouteStatus,
	DeviceID,appVersion,latLong,AppOrderdate,Weigth_OZTax)                   
    Values(@OrderNo,2,GETDATE(),GETDATE(),@CustomerAutoId,@Terms,@BillAddrAutoId,@ShipAddrAutoId,                   
    @SalesPersonAutoId,@OverallDiscAmt,@ShippingCharges,1,                  
    @ShippingType,@TaxType,@TaxValue,@Remarks,@referenceOrderNumber,  
 ISNULL((SELECT TaxRate FROM MLTaxMaster where TaxState=@StateAutoId),0.00),@IsTaxApply,@RouteAutoId,@RouteStatus,
 @DeviceID,@appVersion,@latLong,@orderdate,@Weigth_Tax)                  
                       
    Declare @OrderAutoId int = (SELECT SCOPE_IDENTITY())                    
    UPDATE SequenceCodeGeneratorMaster SET currentSequence = currentSequence + 1 WHERE SequenceCode='OrderNumber'                  
          
        
    INSERT INTO [dbo].[OrderItemMaster]([OrderAutoId], [ProductAutoId], [UnitTypeAutoId], [QtyPerUnit], [UnitPrice],                  
    [RequiredQty], [SRP], [GP], [Tax],  [IsExchange],UnitMLQty,isFreeItem,Weight_Oz)                    
    SELECT @OrderAutoId, [ProductAutoId], (select unittype from [PackingDetails] where autoid = [UnitAutoId]), [QtyPerUnit], [UnitPrice],                   
    RequiredQty,[P_SRP], [GP], [Tax],  IsExchange,
	(case when IsExchange=1 then 0 else ISNULL(MLQty,0) end),IsFree,pm.WeightOz  FROM @orderItems  as dt     
    inner join ProductMaster as pm on pm.AutoId=dt.ProductAutoId        
        
    
   declare @PriceLevelAutoId int= (SELECT [PriceLevelAutoId] FROM [dbo].[CustomerPriceLevel] WHERE [CustomerAutoId] = @CustomerAutoId)    
          
   exec UPDTAE_PRICELEVEL     
   @OrderAutoId=@OrderAutoId,    
   @PriceLevelAutoId=@PriceLevelAutoId,
   @EmpAutoId=@SalesPersonAutoId
    
    SET @Remarks = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=1), '[OrderNo]', @OrderNo)                  
                         
    INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                    
    VALUES(1,@SalesPersonAutoId,getdate(),@Remarks,@OrderAutoId)                    
                  
    update CustomerMaster set LastOrderDate = GETDATE() where AutoId =@CustomerAutoId                    
                       
    select @OrderNo as OrderNo, @OrderAutoId as OrderAutoId, (select statustype from [dbo].[StatusMaster] where [AutoId]=1 and [Category] = 'OrderMaster') status                  
    End                      
                  
    COMMIT TRANSACTION                    
   END TRY                    
   BEGIN CATCH                    
    ROLLBACK TRAN                    
    Set @isException=1                    
    Set @exceptionMessage=ERROR_MESSAGE()                    
   End Catch                     
  END                         
  If @Opcode=401                
  Begin               
  select AutoId,CustomerAutoId, BillAddrAutoId, ShipAddrAutoId, ShippingType, ISNULL(TaxType,0) as TaxType,            
  OrderNo,isnull(referenceOrderNumber,CONVERT(varchar(15),Autoid)) as referenceOrderNumber,            
  isnull((select termsdesc from [CustomerTerms] where termsid=terms),'')Terms,            
  (select statustype from [dbo].[StatusMaster] where [AutoId]=Status and [Category] = 'OrderMaster') as Status,  Status as StatusAutoId,          
  isnull(OrderDate,getdate()) as OrderDate,isnull(DeliveryDate,'') DeliveryDate,TotalAmount,OverallDisc,OverallDiscAmt,ShippingCharges,            
  TotalTax,ismltaxapply,MLQty,MLTaxPer,MLTax,
  Weigth_OZTax as [WeightTax],Weigth_OZQty as [TotalWeightQty],[Weigth_OZTaxAmount] as [TotalWeightAmount],
  AdjustmentAmt,GrandTotal,isnull(Deductionamount,0.00) as CreditMemoAmount,isnull(CreditAmount,0.00) as StoreCreditAmount,PayableAmount,            
  0.00 as [PaidAmount], (PayableAmount - 0.00) as [BalanceAmount],            
  isnull((select (firstname+' '+lastname+' ('+empid+')') from employeemaster where autoid=SalesPersonAutoId),'') as SalesPersonName,             
  isnull(OrderRemarks,'') as OrderRemark,             
  isnull((select (firstname+' '+lastname+' ('+empid+')') from employeemaster where autoid=PackerAutoId),'') as PackerName,             
  isnull(PackerRemarks,'') as PackerRemark,             
  isnull((select (firstname+' '+lastname+' ('+empid+')') from employeemaster where autoid=ManagerAutoId),'') as SalesManagerName,             
  '' as SalesManagerRemark,             
  isnull((select (firstname+' '+lastname+' ('+empid+')') from employeemaster where autoid=Driver),'') as DriverName,              
  isnull(DrvRemarks,'') as DriverRemark,             
  isnull((select (firstname+' '+lastname+' ('+empid+')') from employeemaster where autoid=AccountAutoId),'') as AccountName,              
  isnull(AcctRemarks,'') as AccoutRemark,    
   isnull(PackedBoxes,0) as PackedBoxes, isnull(Stoppage,0) as Stoppage,
   ISNULL(Driver,0) as DriverAutoId           
  from OrderMaster             
  where (SalesPersonAutoId=@SalesPersonAutoId and Status not in (11,8)) or (Driver=@SalesPersonAutoId and     
  ((Status in (4,5) and AssignDate<=GETDATE()) or (Status in (6,7) and convert(date, DelDate)=convert(date,GETDATE()))))       
            
  select OrderAutoId, ProductAutoId,       
  (select autoid from [PackingDetails] where unittype  = isnull(UnitTypeAutoId,1) and productautoid=t.productautoid) as UnitTypeAutoId,       
  QtyPerUnit, UnitPrice, RequiredQty, TotalPieces, isnull(NetPrice,0.00) as NetPrice, Tax, (case when IsExchange=0 then  0 else 1 end) as IsExchange             
  from OrderItemMaster as t         
  where orderautoid in (select autoid from OrderMaster where SalesPersonAutoId=@SalesPersonAutoId)                  
             
  select ActionDate, Remarks, OrderAutoId, (firstname+' '+lastname+' ('+empid+')') as Employee from [tbl_OrderLog] as ol            
  inner join employeemaster as em on ol.empautoid=em.autoid             
  where orderautoid in (select autoid from OrderMaster where SalesPersonAutoId=@SalesPersonAutoId)                  
  End               
  If @Opcode=404                  
  Begin                  
   SELECT *,(select top 1 StateName from state where AutoId=t.State) as [StateName] FROM TaxTypeMaster as t where status=1                    
  End                  
  If @Opcode=405                  
  Begin                  
   SELECT * FROM [dbo].[ShippingType]                    
  End               
  If @Opcode=406                 
  Begin                  
 select TaxRate,(select top 1 StateName from state where AutoId=t.TaxState) as [StateName] from MLTaxMaster as t              
  End                   
 END TRY                    
 BEGIN CATCH                    
  Set @isException=1                    
  Set @exceptionMessage=ERROR_MESSAGE()                    
 END CATCH                    
END 
GO
