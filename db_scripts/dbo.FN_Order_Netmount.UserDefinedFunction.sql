USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Order_Netmount]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


alter table OrderItemMaster drop column NetPrice
drop function FN_Order_Netmount
go
Create FUNCTION  [dbo].[FN_Order_Netmount]
(
	 @AutoId int,
	 @orderAutoId int
)
RETURNS decimal(18,2)
AS
BEGIN
	DECLARE @NetAmount decimal(18,2)	
	--CAL TOTAL AMOUNT FOR NEW ,PROCESSED AND CANCELLED ORDER 
		IF EXISTS(SELECT AutoId FROM OrderMaster WHERE AutoId=@orderAutoId AND Status IN (0,1,2,8))
		BEGIN
		SET @NetAmount=(SELECT (UnitPrice*RequiredQty)-(((UnitPrice*RequiredQty)* isnull(Oim_Discount,0))/100) FROM OrderItemMaster WHERE AutoId=@AutoId)
		END
		ELSE
		BEGIN
		SET @NetAmount=(SELECT (UnitPrice*QtyShip)-(((UnitPrice*QtyShip)*isnull(Oim_Discount,0))/100) FROM OrderItemMaster WHERE AutoId=@AutoId)
		END

	RETURN @NetAmount
END
GO
ALTER table OrderItemMaster add [NetPrice]  AS ([dbo].[FN_Order_Netmount]([AutoId],[OrderAutoId]))