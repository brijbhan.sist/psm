ALTER proc [dbo].[Proc_PackedStatusReport]  
@Opcode int=null,      
@SalesPerson VARCHAR(200)=null,                    
@CustomerAutoId INT=NULL,                                     
@ShippingAutoId VARCHAR(200)=null,   
@OrderNo VARCHAR(12)=null, 
@PageIndex INT = 1,          
@PageSize INT = 10,                 
@isException BIT OUT,          
@exceptionMessage VARCHAR(max) OUT  
as  
begin  
 BEGIN TRY            
    SET @exceptionMessage= 'Success'            
    SET @isException=0   
if(@Opcode=41)  
 begin  
		---Search By Multiple sales person,customer,shipping type multiple , 
		declare @marginamt decimal(18,2) = 20.00
		declare @check varchar(40) = 'Check'
		declare @good varchar(40) = 'Good'
		SELECT ROW_NUMBER() OVER(ORDER BY  OrderNo asc                                      
		) AS RowNumber, * INTO #Results from                                      
		(  
		select em.FirstName[SalesRep],cm.CustomerId,cm.CustomerName,sa.City,sat.StateCode,sa.Zipcode,sat.StateName
		,om.OrderNo
	,CONVERT(VARCHAR(20), isnull(om.DeliveryDate,om.OrderDate), 101) AS [DeliveryDate]
		,st.ShippingType
		,om.TotalNOI[NoOfProducts]
		,sum(oim.RequiredQty)[NoOfItems_Ordered]	
		,om.TotalAmount[SubTotal]	
		,om.OverallDiscAmt
		,om.PayableAmount
		,( sum(oim.NetPrice) - sum(pd.CostPrice * oim.QtyShip) - (om.OverallDiscAmt)) as Profit 
		--If check then in Font should be in RED 
		from OrderMaster as om
		inner join OrderItemMaster as oim on om.AutoId = oim.OrderAutoId
		inner join ProductMaster as pm on pm.AutoId = oim.ProductAutoId
		inner join UnitMaster as um on um.AutoId=oim.UnitTypeAutoId 
		inner join CustomerMaster as cm on cm.AutoId = om.CustomerAutoId
		inner join PackingDetails as pd on pd.ProductAutoId = pm.AutoId and pd.UnitType = oim.UnitTypeAutoId
		inner join EmployeeMaster as em on em.AutoId=om.SalesPersonAutoId
		inner join ShippingAddress as sa on sa.AutoId = om.ShipAddrAutoId
		inner join ShippingType as st on st.AutoId = om.ShippingType
		inner join State as sat on sat.AutoId = sa.State
		where  om.Status = 3
		AND        
		   (ISNULL(@CustomerAutoId,0)=0 OR OM.CustomerAutoId=@CustomerAutoId)                      
		   AND                                 
		   (ISNULL(@SalesPerson,'0')='0' OR om.SalesPersonAutoId                                                                    
			in (select * from dbo.fnSplitString(@SalesPerson,','))                                                                   
			)
		   AND                      
		   (ISNULL(@ShippingAutoId,'0')='0' OR st.AutoId                                                                    
			in (select * from dbo.fnSplitString(@ShippingAutoId,','))                                                                   
			)
			AND                      
		   (ISNULL(@OrderNo,'')='' OR om.OrderNo=@OrderNo)
		   and 
		cm.CustomerType not in (3) and oim.QtyShip>0  
		group by om.OrderNo,om.TotalNOI,om.PayableAmount ,om.Deductionamount,om.TotalAmount,om.OverallDiscAmt,om.TotalTax
		,om.MLTax,em.FirstName,st.ShippingType,cm.CustomerName,cm.CustomerId,sa.City,sat.StateName,sa.Zipcode,sat.StateCode,	isnull(om.DeliveryDate,om.OrderDate)

		)as t order by OrderNo DESC  
	
		SELECT COUNT(1) AS RecordCount, case when @PageSize=0 then COUNT(1) else @PageSize end AS PageSize, @PageIndex AS PageIndex 
		FROM #Results                                               
		SELECT * FROM #Results                                      
		WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)
		)  
	    SELECT sum(SubTotal) as SubTotal,sum(OverallDiscAmt) as OverallDiscAmt,sum(PayableAmount) as PayableAmount,
		sum(Profit) as Profit FROM #Results
		Select FORMAT(GETDATE(),'MM/dd/yyyy hh:mm tt') as [PrintTime]
 end   
 ELSE IF @OpCode=42                                                                                    
BEGIN                                                                                                  
	select
		(
		SELECT st.AutoId,st.ShippingType FROM ShippingType st order by st.ShippingType asc 
		for json path
		) as Shipping,
		(
		SELECT CM.AutoId,cm.CustomerId+' '+CM.CustomerName as CustomerName FROM CustomerMaster CM order by cm.CustomerId+' '+CM.CustomerName asc 
		for json path
		) as Customer,
		(
		Select em.AutoId as SID ,(em.FirstName + ' ' + em.LastName) as SP from EmployeeMaster em                                           
		inner join EmployeeTypeMaster etm on etm.AutoId = em.EmpType                                          
		where EmpType = 2 and Status=1 order by SP ASC 
		for json path
		) as SalesPerson
	for JSON path                                                                                        
END
ELSE IF @OpCode=43                                                                                    
BEGIN   
	Select (Select CM.AutoId,cm.CustomerId+' '+CM.CustomerName as CustomerName FROM CustomerMaster  as CM                                      
	where (ISNULL(@SalesPerson,'0')='0' OR SalesPersonAutoId in (select * from dbo.fnSplitString(@SalesPerson,',')))
	order by cm.CustomerId+' '+CM.CustomerName asc
	for json path )  as Customer	for JSON path                                                                         
END
 END TRY          
 BEGIN CATCH          
    SET @isException=1          
    SET @exceptionMessage='Oops! Something went wrong.Please try later.'             
 END CATCH   
end

--update AllowQtyPiece set UsedQty=4 where AutoId=11
GO
