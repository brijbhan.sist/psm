USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Credit_AdjustmentAmt]    Script Date: 10/30/2020 21:44:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
ALTER TABLE AllowQtyPiece drop column UsedQty

GO
 ALTER FUNCTION  [dbo].[FN_Allocation_UsedQty]
(
	 @SalesAutoAutoId int,
	 @Allowdate datetime,
	 @ProductAutoId int
)
RETURNS int
AS
BEGIN
		DECLARE @DeliveredPeices int,@NotDelievered int,@UsedQty int
		SET @DeliveredPeices=ISNULL((select SUM(QtyDel) from Delivered_Order_Items as  oim 
		inner join OrderMaster as om on om.AutoId=oim.OrderAutoId
		where status=11 and ProductAutoId=@ProductAutoId
		and SalesPersonAutoId=@SalesAutoAutoId
		and PackerAssignDate >= @Allowdate),0)

		declare @productId int=(select ProductId from ProductMaster where autoid=@ProductAutoId)
		IF(DB_NAME()='psmnj.a1whm.com')
		BEGIN
		    SET @DeliveredPeices=@DeliveredPeices+ISNULL((select SUM(QtyDel) from [psmnj.easywhm.com].[dbo].Delivered_Order_Items as  oim 
			inner join [psmnj.easywhm.com].[dbo].OrderMaster as om on om.AutoId=oim.OrderAutoId
			inner join [psmnj.easywhm.com].[dbo].ProductMaster as pm on pm.AutoId=oim.ProductAutoId
			where status=11 and pm.ProductId=@productId
			and SalesPersonAutoId=@SalesAutoAutoId
			and PackerAssignDate >= @Allowdate),0)
		END
		ELSE IF(DB_NAME()='psmct.a1whm.com')
		BEGIN
			SET @DeliveredPeices=@DeliveredPeices+ISNULL((select SUM(QtyDel) from [psmct.easywhm.com].[dbo].Delivered_Order_Items as  oim 
			inner join [psmct.easywhm.com].[dbo].OrderMaster as om on om.AutoId=oim.OrderAutoId
			inner join [psmct.easywhm.com].[dbo].ProductMaster as pm on pm.AutoId=oim.ProductAutoId
			where status=11 and pm.ProductId=@productId
			and SalesPersonAutoId=@SalesAutoAutoId
			and PackerAssignDate >= @Allowdate),0)
		end
		ELSE IF(DB_NAME()='psmnpa.a1whm.com')
		BEGIN
		    SET @DeliveredPeices=@DeliveredPeices+ISNULL((select SUM(QtyDel) from [psmnpa.easywhm.com].[dbo].Delivered_Order_Items as  oim 
			inner join [psmnpa.easywhm.com].[dbo].OrderMaster as om on om.AutoId=oim.OrderAutoId
			inner join [psmnpa.easywhm.com].[dbo].ProductMaster as pm on pm.AutoId=oim.ProductAutoId
			where status=11 and pm.ProductId=@productId
			and SalesPersonAutoId=@SalesAutoAutoId
			and PackerAssignDate >= @Allowdate),0)
		END
		ELSE IF(DB_NAME()='psmpa.a1whm.com')
		BEGIN
			SET @DeliveredPeices=@DeliveredPeices+ISNULL((select SUM(QtyDel) from [psmpa.easywhm.com].[dbo].Delivered_Order_Items as  oim 
			inner join [psmpa.easywhm.com].[dbo].OrderMaster as om on om.AutoId=oim.OrderAutoId
			inner join [psmpa.easywhm.com].[dbo].ProductMaster as pm on pm.AutoId=oim.ProductAutoId
			where status=11 and pm.ProductId=@productId
			and SalesPersonAutoId=@SalesAutoAutoId
			and PackerAssignDate >= @Allowdate),0)
		end
		ELSE IF(DB_NAME()='psmwpa.a1whm.com')
		BEGIN
			SET @DeliveredPeices=@DeliveredPeices+ISNULL((select SUM(QtyDel) from [psmwpa.easywhm.com].[dbo].Delivered_Order_Items as  oim 
			inner join [psmwpa.easywhm.com].[dbo].OrderMaster as om on om.AutoId=oim.OrderAutoId
			inner join [psmwpa.easywhm.com].[dbo].ProductMaster as pm on pm.AutoId=oim.ProductAutoId
			where status=11 and pm.ProductId=@productId
			and SalesPersonAutoId=@SalesAutoAutoId
			and PackerAssignDate >= @Allowdate),0)
		end
		ELSE IF(DB_NAME()='psmny.a1whm.com')
		BEGIN
			SET @DeliveredPeices=@DeliveredPeices+ISNULL((select SUM(QtyDel) from [psmny.easywhm.com].[dbo].Delivered_Order_Items as  oim 
			inner join [psmny.easywhm.com].[dbo].OrderMaster as om on om.AutoId=oim.OrderAutoId
			inner join [psmny.easywhm.com].[dbo].ProductMaster as pm on pm.AutoId=oim.ProductAutoId
			where status=11 and pm.ProductId=@productId
			and SalesPersonAutoId=@SalesAutoAutoId
			and PackerAssignDate >= @Allowdate),0)
		end
		SET @NotDelievered=ISNULL((select sum(TotalPieces) from OrderItemMaster as  oim 
		inner join OrderMaster as om on om.AutoId=oim.OrderAutoId
		where om.status in (3,4,5,6,7,9,10) and ProductAutoId=@ProductAutoId
		and SalesPersonAutoId=@SalesAutoAutoId
		and PackerAssignDate >= @Allowdate),0)

		
		IF(DB_NAME()='psmnj.a1whm.com')
		BEGIN
		    SET @NotDelievered=@NotDelievered+ISNULL((select SUM(TotalPieces) from [psmnj.easywhm.com].[dbo].OrderItemMaster as  oim 
			inner join [psmnj.easywhm.com].[dbo].OrderMaster as om on om.AutoId=oim.OrderAutoId
			inner join [psmnj.easywhm.com].[dbo].ProductMaster as pm on pm.AutoId=oim.ProductAutoId
			where om.status in (3,4,5,6,7,9,10) and pm.ProductId=@productId
			and SalesPersonAutoId=@SalesAutoAutoId
			and PackerAssignDate >= @Allowdate),0)
		END
		ELSE IF(DB_NAME()='psmct.a1whm.com')
		BEGIN
			SET @NotDelievered=@NotDelievered+ISNULL((select SUM(TotalPieces) from [psmct.easywhm.com].[dbo].OrderItemMaster as  oim 
			inner join [psmct.easywhm.com].[dbo].OrderMaster as om on om.AutoId=oim.OrderAutoId
			inner join [psmct.easywhm.com].[dbo].ProductMaster as pm on pm.AutoId=oim.ProductAutoId
			where om.status in (3,4,5,6,7,9,10) and pm.ProductId=@productId
			and SalesPersonAutoId=@SalesAutoAutoId
			and PackerAssignDate >= @Allowdate),0)
		end
		ELSE IF(DB_NAME()='psmnpa.a1whm.com')
		BEGIN
		    SET @NotDelievered=@NotDelievered+ISNULL((select SUM(TotalPieces) from [psmnpa.easywhm.com].[dbo].OrderItemMaster as  oim 
			inner join [psmnpa.easywhm.com].[dbo].OrderMaster as om on om.AutoId=oim.OrderAutoId
			inner join [psmnpa.easywhm.com].[dbo].ProductMaster as pm on pm.AutoId=oim.ProductAutoId
			where om.status in (3,4,5,6,7,9,10) and pm.ProductId=@productId
			and SalesPersonAutoId=@SalesAutoAutoId
			and PackerAssignDate >= @Allowdate),0)
		END
		ELSE IF(DB_NAME()='psmpa.a1whm.com')
		BEGIN
			SET @NotDelievered=@NotDelievered+ISNULL((select SUM(TotalPieces) from [psmpa.easywhm.com].[dbo].OrderItemMaster as  oim 
			inner join [psmpa.easywhm.com].[dbo].OrderMaster as om on om.AutoId=oim.OrderAutoId
			inner join [psmpa.easywhm.com].[dbo].ProductMaster as pm on pm.AutoId=oim.ProductAutoId
			where om.status in (3,4,5,6,7,9,10) and pm.ProductId=@productId
			and SalesPersonAutoId=@SalesAutoAutoId
			and PackerAssignDate >= @Allowdate),0)
		end
		ELSE IF(DB_NAME()='psmwpa.a1whm.com')
		BEGIN
			SET @NotDelievered=@NotDelievered+ISNULL((select SUM(TotalPieces) from [psmwpa.easywhm.com].[dbo].OrderItemMaster as  oim 
			inner join [psmwpa.easywhm.com].[dbo].OrderMaster as om on om.AutoId=oim.OrderAutoId
			inner join [psmwpa.easywhm.com].[dbo].ProductMaster as pm on pm.AutoId=oim.ProductAutoId
			where om.status in (3,4,5,6,7,9,10) and pm.ProductId=@productId
			and SalesPersonAutoId=@SalesAutoAutoId
			and PackerAssignDate >= @Allowdate),0)
		end
		ELSE IF(DB_NAME()='psmny.a1whm.com')
		BEGIN
			SET @NotDelievered=@NotDelievered+ISNULL((select SUM(TotalPieces) from [psmny.easywhm.com].[dbo].OrderItemMaster as  oim 
			inner join [psmny.easywhm.com].[dbo].OrderMaster as om on om.AutoId=oim.OrderAutoId
			inner join [psmny.easywhm.com].[dbo].ProductMaster as pm on pm.AutoId=oim.ProductAutoId
			where om.status in (3,4,5,6,7,9,10) and pm.ProductId=@productId
			and SalesPersonAutoId=@SalesAutoAutoId
			and PackerAssignDate >= @Allowdate),0)
		end

		SET @UsedQty=@DeliveredPeices+@NotDelievered
	RETURN @UsedQty
END


go

ALTER TABLE AllowQtyPiece ADD UsedQty AS [dbo].[FN_Allocation_UsedQty]([SalesPersonAutoId],[AllowDate],[ProductAutoId])
