USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[RandomNumber]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[RandomNumber]()
Returns varchar(20)
BEGIN	
	declare @Randno bigint = (SELECT floor(random_value * 999999999999) FROM random_val_view)
	return Convert(varchar(20),@Randno)
END


GO
