--alter table ordermaster add TotalCostPrice as [dbo].[FN_Order_CostPrice]
--( 
--	autoid
	
--)

alter PROCEDURE [dbo].[Proc_OM_OrderList]                                      
@Opcode INT=NULL,                                      
@OrderAutoId INT=NULL,                                       
@CustomerAutoId  int=null,     
@AsgnDate datetime=null,   
@OrderStatus INT=NULL,
@OrderNo VARCHAR(15)=NULL,                                       
@OrderDate DATETIME=NULL,     
@SalesPersonAutoId INT=NULL,  
@EmpAutoId INT=NULL,    
@Remarks VARCHAR(max)=NULL,    
@Fromdate DATETIME=NULL,  
@TicketNo varchar(30)=null,
@Todate DATETIME=NULL,     
@PageIndex INT = 1,                                      
@PageSize INT = 10,                                      
@RecordCount INT =null,                                       
@isException bit out,                                      
@exceptionMessage varchar(max) out,                                                                              

@customerType varchar(50)= NULL 


AS                                      
BEGIN                                       
  BEGIN TRY                                      
  Set @isException=0                                      
  Set @exceptionMessage='Success'                                       
  IF @Opcode=41                                      
   BEGIN  
    SELECT ROW_NUMBER() OVER(ORDER BY  OrderNo DESC                                      
    ) AS RowNumber, * INTO #Results from                                      
    (                                         
    SELECT OM.[AutoId],OM.OrderNo,CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,CM.[CustomerName] AS CustomerName,isnull(OM.Stoppage,'') as StopNo,                           
    SM.StatusType AS Status,OM.[Status] As StatusCode,OM.[GrandTotal],(emp.FirstName + ' '+ emp.LastName) as SalesPerson,OM.[PayableAmount],                                      
    (SELECT COUNT(distinct [ProductAutoId]) from [dbo].[OrderItemMaster] WHERE OrderAutoId=OM.AutoId) AS NoOfItems ,  isnull(om.TotalCostPrice,0.00) as CostPrice,                                    
                                           
    (select COUNT(1) from CreditMemoMaster as cmm where (Status=1 or Status=3)  and OrderAutoId is null                                      
    and cmm.CustomerAutoId = OM.CustomerAutoId ) as CreditMemo ,                  
    (Select ShippingType from ShippingType Where AutoId=om.ShippingType) as ShippingType,OM.ShippingType  as ShipId                                       
    FROM [dbo].[OrderMaster] As OM                                      
    INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.AutoId = OM.CustomerAutoId                                       
    INNER JOIN [dbo].[EmployeeMaster] AS emp ON emp.AutoId = OM.SalesPersonAutoId                                       
    INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.[Status] AND SM.Category = 'OrderMaster'                                                 
    WHERE (@OrderNo is null or @OrderNo ='' or OrderNo like '%' + @OrderNo + '%')                                      
    and (@CustomerAutoId is null or @CustomerAutoId =0 or CustomerAutoId =@CustomerAutoId)                                      
    and (@FromDate is null or @FromDate = '' or @Todate is null or @Todate = '' or                                       
    (CONVERT(date,OM.[OrderDate]) between CONVERT(date,@FromDate) and CONVERT(date,@Todate)))                                      
    and (@OrderStatus is null or @OrderStatus=0 or OM.[Status]=@OrderStatus)                                           
    and(@SalesPersonAutoId IS NULL OR @SalesPersonAutoId = 0 or om.[SalesPersonAutoId]=@SalesPersonAutoId)     
                     
    )as t order by [OrderNo] DESC                                    
    -- SELECT COUNT(1) AS RecordCount, case when @PageSize=0 then COUNT(1) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Results                                               
    --SELECT * FROM #Results                                      
    --WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))         
	
	select
   (
   select (
    ISNULL((  SELECT COUNT(1) AS RecordCount, case when @PageSize=0 then COUNT(1) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Results 
   for json path, INCLUDE_NULL_VALUES),'[]')
   ) as RecordCount ,
  
    ISNULL((SELECT * FROM #Results                                      
    WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))  
   for json path, INCLUDE_NULL_VALUES),'[]')
   as OrderList

   for json path, INCLUDE_NULL_VALUES
   )as test 
   END                                                    
                                        
  ELSE IF @Opcode=43                                      
  BEGIN                                      
  
  select
   (
   select (
    ISNULL((SELECT [AutoId],[StatusType] FROM [dbo].[StatusMaster] WHERE   [Category]='OrderMaster'   ORDER BY [StatusType] ASC 
   for json path, INCLUDE_NULL_VALUES),'[]')
   ) as Status ,
  
    ISNULL((SELECT [AutoId],[CustomerName] As Customer FROM [dbo].[CustomerMaster] ORDER BY replace(CustomerName,' ','') ASC 
   for json path, INCLUDE_NULL_VALUES),'[]')
   as Customer,

    ISNULL((SELECT AutoId,(FirstName +' '+ISNULL(LastName,'')) AS EmpName FROM EmployeeMaster WHERE EmpType =2 ORDER BY EmpName 
   for json path, INCLUDE_NULL_VALUES),'[]')
   as SalePerson,


    ISNULL((SELECT AutoId,ShippingType FROM ShippingType  order by   ShippingType 
   for json path, INCLUDE_NULL_VALUES),'[]')
   as ShippingType
   for json path, INCLUDE_NULL_VALUES
   )as test 
  END                                      
 
  ELSE IF @Opcode=31
  BEGIN
  if (select count(*) from PaymentOrderDetails where OrderAutoId=@OrderAutoId)=0
  begin
		BEGIN TRY
			BEGIN TRAN
					
					--Incomplete and hold suggest by nilay sir (store credit not revert yet)
					 SET @CustomerAutoId =(SELECT CustomerAutoId FROM OrderMaster WHERE AutoId=@OrderAutoId) 
					 DECLARE @CreditAmount decimal(10,2)=ISNULL((select CreditAmount from OrderMaster where AutoId=@OrderAutoId),0.00) 
				
					 IF (@CreditAmount<0)                                                                              
					  BEGIN                                   
						update CustomerCreditMaster set CreditAmount=isnull(CreditAmount,0)+Isnull(@CreditAmount,0) where CustomerAutoId= @CustomerAutoId     
						DELETE FROM tbl_Custumor_StoreCreditLog WHERE ReferenceType='OrderMaster' AND ReferenceNo=CONVERT(VARCHAR(10),@OrderAutoId)
					  END
					  if EXISTS(select * from CreditMemoMaster where OrderAutoId=@OrderAutoId)
					  BEGIN                                                                                                                                                                                                                     
						update CreditMemoMaster set OrderAutoId=null,CompletedBy=null,CompletionDate=null where OrderAutoId=@OrderAutoId                                                                                                                              
					  END
					  



					  insert into [dbo].[Delete_OrderMaster_Backup]([OrderNo],[OrderDate],[DeliveryDate],[CustomerAutoId],[Terms],[BillAddrAutoId],[ShipAddrAutoId],[SalesPersonAutoId],[OverallDiscAmt],
					  [ShippingCharges],[Status],[PackerAutoId],[Driver],[AssignDate],[Stoppage],[DelDate],[PaymentRecev],[RecevAmt],[AmtValue],[ValueChanged],[DiffAmt],[NewTotal],[PayThru],[DrvRemarks],
					  [AmtDue],[PackedBoxes],[AcctRemarks],[ShippingType],[CommentType],[Comment],[CheckNo],[AddonPackedQty],[TaxType],[ManagerAutoId],[AccountAutoId],[PackerRemarks],[OrderRemarks],
					  [RootName],[OrderType],[paidAmount],[paymentMode],[referNo],[POSAutoId],[TaxValue],[PastDue],[referenceOrderNumber],[ManagerRemarks],[PackerAssignDate],[PackerAssignStatus],[Times],
					  [UPDATEDATE],[UPDATEDBY],[WarehouseAutoId],[WarehouseRemarks],[UnitMLTax],[MLTaxPer],[CancelledDate],[CancelledBy],
					  [DriverRemarks],[isMLManualyApply],[IsTaxApply],[RouteAutoId],[RouteStatus],[DeviceID],[appVersion],[latLong],[AppOrderdate],[CancelRemark],
					  [Order_Closed_Date],[IsAttached],[AttachedDate],[MLTaxRemark],[OrdShippingAddress],
					  [DraftCreateDate],[DraftOrderDate],[ReferenceNo],DeleteRemarks) 
					  select [OrderNo],[OrderDate],[DeliveryDate],[CustomerAutoId],[Terms],[BillAddrAutoId],[ShipAddrAutoId],[SalesPersonAutoId],[OverallDiscAmt],
					  [ShippingCharges],[Status],[PackerAutoId],[Driver],[AssignDate],[Stoppage],[DelDate],[PaymentRecev],[RecevAmt],[AmtValue],[ValueChanged],[DiffAmt],[NewTotal],[PayThru],[DrvRemarks],
					  [AmtDue],[PackedBoxes],[AcctRemarks],[ShippingType],[CommentType],[Comment],[CheckNo],[AddonPackedQty],[TaxType],[ManagerAutoId],[AccountAutoId],[PackerRemarks],[OrderRemarks],
					  [RootName],[OrderType],[paidAmount],[paymentMode],[referNo],[POSAutoId],[TaxValue],[PastDue],[referenceOrderNumber],[ManagerRemarks],[PackerAssignDate],[PackerAssignStatus],[Times],
					  [UPDATEDATE],[UPDATEDBY],[WarehouseAutoId],[WarehouseRemarks],[UnitMLTax],[MLTaxPer],[CancelledDate],[CancelledBy],
					  [DriverRemarks],[isMLManualyApply],[IsTaxApply],[RouteAutoId],[RouteStatus],[DeviceID],[appVersion],[latLong],[AppOrderdate],[CancelRemark],
					 [Order_Closed_Date],[IsAttached],[AttachedDate],[MLTaxRemark],[OrdShippingAddress],
					  [DraftCreateDate],[DraftOrderDate],@TicketNo,@Remarks from OrderMaster where AutoId=@OrderAutoId 


					  insert into [dbo].[Delete_OrderItemMaster_Backup] (OrderAutoId,ProductAutoId,UnitTypeAutoId,QtyPerUnit,UnitPrice,RequiredQty,SRP,GP,Tax,Status,Barcode,QtyShip,RemainQty,IsExchange,
					  TaxValue,UnitMLQty,isFreeItem,ManualScan,Weight_Oz,Original_UnitType,AddOnQty) 
					  select OrderAutoId,ProductAutoId,UnitTypeAutoId,QtyPerUnit,UnitPrice,RequiredQty,SRP,GP,Tax,Status,Barcode,QtyShip,RemainQty,IsExchange,TaxValue,UnitMLQty,isFreeItem,
					  ManualScan,Weight_Oz,Original_UnitType,AddOnQty from OrderItemMaster where OrderAutoId=@OrderAutoId

					  insert into [dbo].[Delete_tbl_OrderLog_Backup] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId]) select [ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId]
					  from tbl_OrderLog where OrderAutoId=@OrderAutoId

					  insert into [dbo].[Delete_DeliveredOrders_Backup] (OrderAutoId)
					  select OrderAutoId from Delivered_Order_Items where OrderAutoId=@OrderAutoId

					  insert into [dbo].[Delete_Delivered_Order_Items_Backup] (OrderAutoId,ProductAutoId,UnitAutoId,QtyPerUnit,UnitPrice,Barcode,QtyShip,SRP,GP,Tax,FreshReturnQty,
					  FreshReturnUnitAutoId,DamageReturnQty,DamageReturnUnitAutoId,MissingItemQty,MissingItemUnitAutoId,IsExchange,TaxValue,isFreeItem,UnitMLQty,QtyPerUnit_Fresh,
					  QtyPerUnit_Damage,QtyPerUnit_Missing,Del_CostPrice,Del_CommCode)
					  select OrderAutoId,ProductAutoId,UnitAutoId,QtyPerUnit,UnitPrice,Barcode,QtyShip,SRP,GP,Tax,FreshReturnQty,FreshReturnUnitAutoId,DamageReturnQty,DamageReturnUnitAutoId
					  ,MissingItemQty,MissingItemUnitAutoId,IsExchange,TaxValue,isFreeItem,UnitMLQty,QtyPerUnit_Fresh,QtyPerUnit_Damage,QtyPerUnit_Missing,
					  Del_CostPrice,Del_CommCode from Delivered_Order_Items where OrderAutoId=@OrderAutoId

					  insert into [dbo].[Delete_DrvLog_Backup] ([DrvAutoId],[OrderAutoId],[AssignDate]) select [DrvAutoId],[OrderAutoId],[AssignDate] from DrvLog where OrderAutoId=@OrderAutoId

					  insert into [dbo].[Delete_GenPacking_Backup] ([PackingId],[OrderAutoId],[PackingDate],[PackerAutoId]) select [PackingId],[OrderAutoId],[PackingDate],[PackerAutoId] from GenPacking
					  where OrderAutoId=@OrderAutoId 
    
					  delete from OrderItemMaster where OrderAutoId=@OrderAutoId
					  delete from tbl_OrderLog where OrderAutoId=@OrderAutoId
					  delete from DeliveredOrders where OrderAutoId=@OrderAutoId
					  delete from Delivered_Order_Items where OrderAutoId=@OrderAutoId
					  delete from DrvLog where OrderAutoId=@OrderAutoId
					  Delete from GenPacking where OrderAutoId=@OrderAutoId
					  delete from Order_Original where AutoId=@OrderAutoId
					  delete from OrderItems_Original where OrderAutoId=@OrderAutoId
					  delete from OrderMaster where AutoId=@OrderAutoId

					 COMMIT TRAN
					 END TRY
					 BEGIN CATCH
						 SET @isException=1                                                                                            
						 SET @exceptionMessage=  'Oops,something went wrong.Please try again.'  
					 END CATCH
end
else
	begin
   SET @isException=1                                                                                            
   SET @exceptionMessage=  'Order cannot be deleted because payment has been taken'  
	end
  END
  else IF @Opcode=42                                      
   BEGIN                                         
    SET @CustomerAutoId=(SELECT CustomerAutoId FROM OrderMaster WHERE AutoId=@OrderAutoId)  

    IF EXISTS(SELECT [AutoId] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId AND ([PaymentRecev] IS NOT NULL OR [PaymentRecev] != ''))                                      
     BEGIN                                      
      SELECT OM.[AutoId],OM.[OrderNo],CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,                                      
      ISNULL(CONVERT(VARCHAR(20), OM.[DeliveryDate],101),CONVERT(VARCHAR(20), OM.[DelDate],101)) AS DeliveryDate,                                
      CM.[AutoId] As CustAutoId,CM.[CustomerId],CM.[CustomerName],CM.[Contact1] + '/' + CM.[Contact2] As Contact,CTy.CustomerType,CT.[TermsDesc],ST.[ShippingType],                                      
      BA.[Address] As BillAddr,                                      
      S.[StateName] AS State1,ba.State as [stateid],BA.[City] AS City1,BA.[Zipcode] As Zipcode1,SA.[Address] As ShipAddr,S1.[StateName] As State2,SA.[City] AS City2,                                      
      SA.[Zipcode] As Zipcode2,OM.[Status] As StatusCode,SM.[StatusType] AS Status,OM.[PackedBoxes],DO.[TotalAmount],DO.[OverallDisc],DO.[OverallDiscAmt],                                      
      DO.[ShippingCharges],DO.[TotalTax],DO.[GrandTotal],DO.[AmtPaid],DO.[AmtDue],OM.[PaymentRecev],OM.[RecevAmt],OM.[AmtValue],OM.[ValueChanged],                                      
      OM.[DiffAmt],OM.[NewTotal],OM.[PayThru],OM.CheckNo,OM.[DrvRemarks],OM.[AcctRemarks],EM.[FirstName] + ' ' + EM.[LastName] As SalesPerson,                                      
      EM1.[FirstName] + ' ' + EM1.[LastName] As PackerName,[CommentType],[Comment],isnull(DeductionAmount,0.00) as DeductionAmount,                                      
            ISNULL(DO.CreditAmount,0.00)as CreditAmount,isnull(do.PayableAmount,0.00) as PayableAmount,                                      
            ISNULL((select t.CreditAmount from CustomerCreditMaster as t  where t.CustomerAutoId=om.CustomerAutoid),0.00) as CustomerCredit                                      
          
			,otm.OrderType,OrderRemarks,                                      
            ISNULL(OM.MLQty,0) as MLQty,isnull(DO.MLTax,0) as MLTax,CM.ContactPersonName,ISNULL(om.AdjustmentAmt,0) as AdjustmentAmt,                                      
            CM.BusinessName,ISNULL(UnitMLTax,0) as UnitMLTax,ISNULL(DO.AmtPaid,0) as AmtPaid,ISNULL(do.AmtDue,0) as AmtDue,                                      
      (select TaxableType from TaxTypeMaster as ttm where ttm.Autoid=om.taxType) as TaxType ,OM.Driver ,isMLManualyApply,        
      isnull(OM.Weigth_OZQty,0) as WeightOzQty,isnull(OM.Weigth_OZTaxAmount,0) as WeightOzTotalTax,dbo.UDF_CheckMLTax(OM.AutoId) as MLTaxStatus 
      FROM [dbo].[OrderMaster] As OM                                      
      INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.Status AND SM.[Category] = 'OrderMaster'                                      
      INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[AutoId] = OM.[BillAddrAutoId]                                      
      INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = OM.[ShipAddrAutoId]                      
      INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                                       
      INNER JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                                       
      INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                                 
      INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]                                      
      LEFT JOIN [dbo].[CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                                      
      LEFT JOIN [dbo].[ShippingType] AS ST ON ST.[AutoId] = OM.[ShippingType]                                       
      LEFT JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[SalesPersonAutoId]                                      
      LEFT JOIN [dbo].[EmployeeMaster] AS EM1 ON EM1.[AutoId] = OM.[PackerAutoId]                                       
      INNER JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId] 
	  INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
	  WHERE OM.AutoId = @OrderAutoId     
	  
	  SET @customerType=(select CustomerType from CustomerMaster where AutoId=@CustomerAutoId)
                                      
      SELECT CM.[CategoryName],CM.[CategoryId],DOI.[ProductAutoId],PM.[ProductId],PM.[ProductName],DOI.[UnitAutoId],UM.[UnitType],DOI.[QtyPerUnit],                                      
      (select top 1 requiredQty from OrderItemMaster oim where oim.orderAutoId=doi.OrderAutoId and oim.ProductAutoid=doi.ProductAutoid and oim.UnitTypeAutoId=doi.UnitAutoid )                                      
      as RequiredQty,                                      
      DOI.[UnitPrice],DOI.[Barcode],DOI.[QtyShip],DOI.[QtyDel],DOI.[TotalPieces],DOI.[SRP],DOI.[GP],DOI.[Tax],DOI.[NetPrice]                                      
      ,convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice,ISNULL(FreshReturnQty,0) as FreshReturnQty,FreshReturnUnitAutoId,                                      
      ISNULL(DamageReturnQty,0) as DamageReturnQty,DamageReturnUnitAutoId,                                      
      IsExchange,isnull(TaxValue,0.00) as TaxValue,ISNULL(isFreeItem,0) AS isFreeItem,                                      
      ISNULL(UnitMLQty,0) as UnitMLQty,ISNULL(TotalMLQty,0) as TotalMLQty,
	  (case @customertype when 2 then PD.whminprice when 3 then PD.costprice else [minprice] end) as [MinPrice],PD.Price as BasePrice,                                      
      DOI.Item_TotalMLTax as TotalMLTax ,isnull(DOI.[Del_CostPrice],0.00) as CostPrice                          
      FROM [dbo].[Delivered_Order_Items] AS DOI                                       
      INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = DOI.[ProductAutoId] 
	  INNER JOIN [dbo].[PackingDetails] AS PD ON PD.ProductAutoId = DOI.ProductAutoId and DOI.UnitAutoId=pd.UnitType
      INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = DOI.UnitAutoId                                       
      INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId] WHERE [OrderAutoId]=@OrderAutoId                                      
                                      
      SET @CustomerAutoId = (SELECT [CustomerAutoId] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId)                                      
                                      
      SELECT OM.[AutoId],[OrderNo],CONVERT(VARCHAR(20),[OrderDate],101) AS OrderDate,DO.[AmtDue],DO.[GrandTotal],DO.[AmtPaid] ,                                    
    
	  otm.OrderType,DATEDIFF(dd,OrderDate,GETDATE()) as Aging, orderdate as orderdateSort                                      
      FROM [dbo].[OrderMaster] As OM                            
      INNER JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId]   
	   INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
      WHERE [CustomerAutoId] = @CustomerAutoId AND ([Status] = 6 or [Status] = 11) AND DO.[AmtDue] > 0.00                                      
      order by orderdateSort                                       
      
	  
                                            
     END                                      
    ELSE                                
     BEGIN                                      
      SELECT OM.[AutoId],OM.[OrderNo],CONVERT(VARCHAR(20),OM.[OrderDate], 101) AS OrderDate,                                     
      CONVERT(VARCHAR(20), OM.[DeliveryDate],101)--CONVERT(VARCHAR(20),OM.[DeliveryDate], 131)                                
      AS DeliveryDate,                                      
      CM.[AutoId] As CustAutoId,CM.[CustomerId],CM.[CustomerName],CM.[Contact1] + '/' + CM.[Contact2] As Contact,CTy.CustomerType,CT.[TermsDesc],ST.[ShippingType],                                      
      BA.[Address] As BillAddr,S.[StateName] AS State1,BA.[City] AS City1,BA.[Zipcode] As Zipcode1,SA.[Address] As ShipAddr,S1.[StateName] As State2,                                      
      SA.[City] AS City2,SA.[Zipcode] As Zipcode2,OM.[Status] As StatusCode,SM.[StatusType] AS Status,OM.[TotalAmount],OM.[OverallDisc],OM.[OverallDiscAmt],                                 
      OM.[ShippingCharges],OM.[TotalTax],OM.[GrandTotal],OM.[PackedBoxes],EM.[FirstName] + ' ' + EM.[LastName] As SalesPerson,                                      
      EM1.[FirstName] + ' ' + EM1.[LastName] As PackerName,OM.[DrvRemarks],[CommentType],[Comment]                                      
      ,isnull(DeductionAmount,0.00) as DeductionAmount,                                      
            isnull(CreditAmount,0.00)as CreditAmount,isnull(PayableAmount,0.00) as PayableAmount                                      
            ,ISNULL((select t.CreditAmount from CustomerCreditMaster as t  where t.CustomerAutoId=om.CustomerAutoid),0.00) as CustomerCredit                                      
            ,(select TaxableType from TaxTypeMaster as ttm where ttm.Autoid=om.taxType) as TaxType,
			
			otm.OrderType,OrderRemarks ,                                      
            ISNULL(MLQty,0) as MLQty,isnull(MLTax,0) as MLTax,CM.ContactPersonName,ISNULL(AdjustmentAmt,0) as AdjustmentAmt,                                      
            CM.BusinessName,OM.Driver,isMLManualyApply,isnull(OM.Weigth_OZQty,0) as WeightOzQty,isnull(OM.Weigth_OZTaxAmount,0) as WeightOzTotalTax,dbo.UDF_CheckMLTax(OM.AutoId) as MLTaxStatus         
    FROM [dbo].[OrderMaster] As OM                                      
      INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.Status AND SM.[Category] = 'OrderMaster'                                      
      INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[AutoId] = OM.[BillAddrAutoId]                                      
      INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = OM.[ShipAddrAutoId]                           
      INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                                       
      INNER JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                                       
      INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                       
      INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]                                      
      LEFT JOIN [dbo].[CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                                      
      LEFT JOIN [dbo].[ShippingType] AS ST ON ST.[AutoId] = OM.[ShippingType]                                       
      left JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[SalesPersonAutoId]                                             
      left JOIN [dbo].[EmployeeMaster] AS EM1 ON EM1.[AutoId] = OM.[PackerAutoId] 
	     INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
	  WHERE OM.AutoId = @OrderAutoId                                      
                            
		SET @customerType=(select CustomerType from CustomerMaster where AutoId=@CustomerAutoId)
		
      SELECT CM.[CategoryName],CM.[CategoryId],OIM.ProductAutoId,PM.[ProductId],PM.[ProductName],PM.[ProductLocation],                                      
      OIM.[UnitTypeAutoId] AS UnitAutoId,UM.[UnitType],OIM.[QtyPerUnit],OIM.[UnitPrice],OIM.[TotalPieces],OIM.[SRP],OIM.[GP],OIM.[Tax],                                      
      OIM.[NetPrice],OIM.[Barcode],OIM.[QtyShip],OIM.[RequiredQty] ,convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice,                        
      OIM.IsExchange,isnull(TaxValue,0.00) as TaxValue,ISNULL(isFreeItem,0) AS isFreeItem,UnitMLQty,
	  (case @customertype when 2 then PD.whminprice when 3 then PD.costprice else [minprice] end) as [MinPrice],PD.Price as BasePrice,                                       
      ISNULL(UnitMLQty,0) as UnitMLQty,ISNULL(TotalMLQty,0) as TotalMLQty,                                      
      oim.Item_TotalMLTax as TotalMLTax ,isnull(OIM.[OM_CostPrice],0.00) as CostPrice                                      
      FROM [dbo].[OrderItemMaster] AS OIM                                       
      INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId] 
	   INNER JOIN [dbo].[PackingDetails] AS PD ON PD.ProductAutoId = OIM.ProductAutoId and OIM.UnitTypeAutoId=pd.UnitType
      INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitTypeAutoId                                       
      INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId] WHERE [OrderAutoId] = @OrderAutoId                                       
      ORDER BY CM.[CategoryName] ASC                                      
                                      
      SET @CustomerAutoId = (SELECT [CustomerAutoId] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId)                                      
                                      
      SELECT OM.[AutoId],[OrderNo],CONVERT(VARCHAR(20),[OrderDate],101) AS OrderDate,DO.[AmtDue],DO.[GrandTotal],DO.[AmtPaid] ,                                      
    

	 otm.OrderType, DATEDIFF(dd,OrderDate,GETDATE()) as Aging, orderdate as orderdateSort                                      
      FROM [dbo].[OrderMaster] As OM                                      
      INNER JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId]   
	      INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
      WHERE [CustomerAutoId] = @CustomerAutoId AND ([Status] = 6 or [Status] = 11 ) AND do.[AmtDue] > 0.00                                      
      order by orderdateSort                                       
                                              
     END                                           
                                          
    select OrderAutoId,CreditAutoId,CreditNo,CONVERT(VARCHAR(10),CreditDate,101) AS CreditDate,ISNULL(TotalAmount,0.00) as ReturnValue,ISNULL(PaidAmount,0.00)  as amtDeducted,                                      
    ISNULL(BalanceAmount,0.00) as amtDue                                      
                from CreditMemoMaster AS CM  where Status =3 --AND CustomerAutoId=@CustomerAutoId                                       
                AND OrderAutoId=@OrderAutoId                                       
                                      
  SELECT * FROM (                                                                                                                    
  SELECT 'Driver' as EmpType,DrvRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om                                                                        
  inner join EmployeeMaster as emp on emp.AutoId=om.Driver                                                              
  where om.AutoId=@OrderAutoId   and ISNULL(DrvRemarks,'')!=''    
  UNION                                                                                             
  SELECT ' Account' as EmpType,CancelRemark as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om                            
  inner join EmployeeMaster as emp on emp.AutoId=om.AccountAutoId where om.AutoId=@OrderAutoId                                      
  and ISNULL(CancelRemark,'')!=''  
  UNION                                                                                             
  SELECT 'Account' as EmpType,AcctRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om                            
  inner join EmployeeMaster as emp on emp.AutoId=om.AccountAutoId where om.AutoId=@OrderAutoId                                      
  and ISNULL(AcctRemarks,'')!=''                                                                                                               
                                                                                                                                                        
  UNION                                                                                                                
  SELECT 'Sales Person' as EmpType,OrderRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName  from OrderMaster as om                                  
  inner join EmployeeMaster as emp on emp.AutoId=om.SalesPersonAutoId where                                                                                                                 
  om.AutoId=@OrderAutoId            and ISNULL(OrderRemarks,'')!=''                       
  UNION                                                                                                                
  SELECT 'Packer' as EmpType,PackerRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om                                                                                             
  inner join EmployeeMaster as emp on emp.AutoId=om.PackerAutoId                                                                                                           
  where om.AutoId=@OrderAutoId          and ISNULL(PackerRemarks,'')!=''                                                                                               
  UNION                      
  SELECT 'Sales Manager' as EmpType,ManagerRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName  from OrderMaster as om                                                                                                                
  inner join EmployeeMaster as emp on emp.AutoId=om.ManagerAutoId                                      
  where om.AutoId=@OrderAutoId     and ISNULL(ManagerRemarks,'')!=''                                                                                                                
  UNION  
  SELECT 'Sales Manager' as EmpType,MLTaxRemark as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName  from OrderMaster as om                                                                                                                
  inner join EmployeeMaster as emp on emp.AutoId=om.ManagerAutoId                                      
  where om.AutoId=@OrderAutoId  and ISNULL(MLTaxRemark,'')!='' 
  UNION
  SELECT 'Warehouse Manager' as EmpType,WarehouseRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName as EmpName  from OrderMaster as om                                                                                                                
  inner join EmployeeMaster as emp on emp.AutoId=om.WarehouseAutoId                                                                                                                 
  where om.AutoId=@OrderAutoId             and ISNULL(WarehouseRemarks,'')!=''                                                                                                  
  ) AS T                                       
             SELECT [PackingId],CONVERT(VARCHAR,[PackingDate],101) As PkgDate,EM.[FirstName] + ' ' + EM.[LastName] As Packer                                       
    FROM [dbo].[GenPacking] AS GP                                      
    INNER JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId]=GP.[PackerAutoId] WHERE [OrderAutoId]=@OrderAutoId                                      
                                      
    select CreditAutoId,CreditNo,CONVERT(VARCHAR(10),CreditDate,101) AS CreditDate,                                      
    (SELECT COUNT(*) FROM CreditItemMaster AS CIM WHERE CIM.CreditAutoId=CM.CreditAutoId) AS NoofItem,                                      
    (SELECT SUM(NetAmount) FROM CreditItemMaster AS CIM WHERE CIM.CreditAutoId=CM.CreditAutoId) AS NetAmount                                      
    from CreditMemoMaster AS CM  where Status =1 AND CustomerAutoId=@CustomerAutoId  
 END                                      
  END TRY                                      
  BEGIN CATCH                                      
    Set @isException=1                                      
    Set @exceptionMessage=ERROR_MESSAGE()                                     
  END CATCH                         
END   