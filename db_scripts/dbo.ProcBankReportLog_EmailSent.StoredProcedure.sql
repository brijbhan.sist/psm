ALTER PROCEDURE [dbo].[ProcBankReportLog_EmailSent]
@count int=null,
@i int = 1,
@html varchar(max)=null,
@tr varchar(max)=null, 
@copy_to varchar(max)=null,
@PaymentDate datetime=null,
@CreditAmount decimal(18,2)=null,
@TotalNoOfCustomer int=null, 
@noOfOrder int=null,
@TotalCash decimal(18,2)=null,
@TotalCheck decimal(18,2)=null,
@TotalMoneyOrder decimal(18,2)=null,
@TotalElectronicTransfer decimal(18,2)=null,
@TotalCreditCard decimal(18,2)=null,
@TotalStoreCredit decimal(18,2)=null,
@TotalPaid decimal(18,2)=null,
@Short decimal(18,2)=null, 
@Expense decimal(18,2)=null, 
@Depositamount decimal(18,2)=null, 
@AutoId int=null,
@Noofcount int=null,
@TotalAmount decimal(18,2)=null, 
@CurrenctName varchar(100)=null,
@CustomerName varchar(500)=null,
@OrderNo varchar(max)=null,
@Cash decimal(18,2)=null, 
@Check decimal(18,2)=null, 
@MoneyOrder decimal(18,2)=null,                                              
@CreditCard decimal(18,2)=null, 
@StoreCredit decimal(18,2)=null, 
@ElectronicTransfer decimal(18,2)=null,   
@STime varchar(30)=null,
@CheckNo varchar(50)=null,
@PaymentMode varchar(50)=null,
@ExpenseDescription varchar(max)=null,
@ExpenseAmount decimal(18,2)=null, 
@ExpenseBy varchar(250)=null,
@PaymentId varchar(50)=null,
@ReceivedAmount decimal(18,2)=null,   
@SortAmount decimal(18,2)=null,   
@ReceivedBy varchar(250)=null,                         
@SettlementBy varchar(250)=null,
@CustomerId varchar(50)=null,
@SettlementDate datetime=null,
@Remarks varchar(max)=null,
@CheckDate datetime=null,
@FromEmailId varchar(300)=null, 
@FromName varchar(300)=null, 
@smtp_userName varchar(300)=null,
@Password varchar(max)=null, 
@SMTPServer varchar(300)=null, 
@Port int=null, 
@SSL bit=null, 
@ToEmailId  varchar(500)=null, 
@CCEmailId  varchar(500)=null, 
@BCCEmailId  varchar(500)=null, 
@Subject  varchar(max)=null
AS
BEGIN
	declare  @DriveLocation varchar(150)
	SET @DriveLocation='D:\DailyBankReport\'+UPPER((select CompanyId from dbo.CompanyDetails))+'_BanklogReport_'+convert(varchar(50),Format(GetDate(),'MM_dd_yyyy_hh_mm_tt'))+'.html'
	select top 1  Row_Number() over (order by AutoId) as rownumber
	
	,FORMAT([ReportDate],'MM/dd/yyyy') as PaymentDate, 
	FORMAT([ReportDate],'MM/dd/yyyy') + ':' + FORMAT([ReportDate],'dddd') as Dates,
	[StoreCreditGenerated] as CreditAmount,                                      
	[TotalCustomer] as TotalNoOfCustomer,
	[TotalOrder]  as noOfOrder, 
	[CASH] as TotalCash, 
	[Check] as TotalCheck                                   
	,[MoneyOrder] as TotalMoneyOrder,
	[ElectronicTransfer] as TotalElectronicTransfer,AutoId ,                                                
	[CreditCard] as TotalCreditCard,
	[StoreCreditApply] as TotalStoreCredit,
	[TotalTransaction] as TotalPaid,                   
	[Short] as Short,
	[Expense] as Expense,
	DepositAmount into #t1 
	from  [dbo].[Tbl_DailyBankReport] as t                             
	where  convert(date,ReportDate)= convert(date,getdate()) order by 
	Convert(date,[ReportDate]) desc

			select 
			    @AutoId=AutoId,
				@CustomerName=Dates,
				@PaymentDate = PaymentDate,
				@CreditAmount = CreditAmount,
				@TotalNoOfCustomer = TotalNoOfCustomer,
				@noOfOrder = noOfOrder ,
				@TotalCash = TotalCash,
				@TotalCheck = TotalCheck,
				@TotalMoneyOrder = TotalMoneyOrder,
				@TotalElectronicTransfer = TotalElectronicTransfer,
				@TotalCreditCard = TotalCreditCard,
				@TotalStoreCredit = TotalStoreCredit,
				@TotalPaid = TotalPaid,
				@Short = Short,
				@Expense = Expense,
				@Depositamount=DepositAmount
			from #t1 

			set @tr ='<tr>' 
											+	'<td style=''text-align:center;''>'+ format(@PaymentDate,'MM/dd/yyy') + '</td>' 
											+	'<td style=''text-align:center;''>'+ convert(varchar(50), @TotalNoOfCustomer) + '</td>' 
											+	'<td style=''text-align:center;''>'+ convert(varchar(50), @noOfOrder) + '</td>' 
											+	'<td style=''text-align:right;''>'+ convert(varchar(50), @TotalCash) + '</td>' 
											+	'<td style=''text-align:right;''>'+ convert(varchar(50), @TotalCheck) + '</td>' 
											+	'<td style=''text-align:right;''>'+ convert(varchar(50), @TotalCreditCard) + '</td>' 
											+	'<td style=''text-align:right;''>'+ convert(varchar(50), @TotalElectronicTransfer) + '</td>' 
											+	'<td style=''text-align:right;''>'+ convert(varchar(50), @TotalMoneyOrder) + '</td>' 
											+	'<td style=''text-align:right;''>'+ convert(varchar(50), @TotalStoreCredit) + '</td>' 
											+	'<td style=''text-align:right;''>'+ convert(varchar(50), @CreditAmount) + '</td>' 
											+	'<td style=''text-align:right;''>'+ convert(varchar(50), @TotalPaid) + '</td>' 
											+	'<td style=''text-align:right;''>'+ convert(varchar(50), @Short) + '</td>' 
											+	'<td style=''text-align:right;''>'+ convert(varchar(50), @Expense) + '</td>'
											+	'<td style=''text-align:right;''>'+ convert(varchar(50), @Depositamount) + '</td>'
									+ '</tr>'

set @html = '<html><head><style>
td{padding: 3px 8px;}table {width: 100%;}
table, th, td {border: 1px solid black;border-collapse: collapse;}
body {font-size: 14px;font-family: monospace;}
.center {text-align: center;}.right {text-align: right;}
thead > tr {background: #fff;color: #000;
font-weight: 700;border-top: 1px solid #5f7c8a;}
thead > tr > td {text-align: center !important;}
.InvContent {width: 100%;}.tdLabel {font-weight: 700;}
@media print {#btnPrint {display: none;}}
legend {border-bottom: 1px solid #5f7c8a;}.tableCSS > tbody > tr > td,.tableCSS > thead > tr > td,.tableCSS > tfoot > tr > td {border: 1px solid #5f7c8a;}#tblReportPaymentDailyReport tbody tr td .ElectronicTransfer {color: black}table {border-collapse: collapse;}table,td,th {border: 1px solid black;}</style></head> 
<div class="InvContent" style="width: 100%;">
<table border="1" style="width:100%;border:1px solid black;border-collapse:collapse;">
<tbody><tr>
<td><table class="table tableCSS" border="1" style="width:100%;border: 1px solid black;border-collapse:collapse;" id="tblPaymentLogHeader">
        <thead>
            <tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
                <td style="text-align: center !important;">
                    <h4 style="margin:5px;">Bank Report</h4><h4 style="margin:0 0 10px 0">'+(select CompanyDistributerName from CompanyDetails)+'</h4>
                </td>
            </tr>
            <tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
                <td id="date" style="font-size:17px;font-weight:600;text-align: center;">'+convert(varchar(30),@CustomerName)+'</td>
            </tr>
        </thead>
        <tbody></tbody>
    </table><p style="page-break-before: always"></p>
<table border="1" style="width:100%;border: 1px solid black;border-collapse:collapse;"><thead><tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">	
				<th style="text-align: center !important;"> Date </th>
				<th style="text-align: center !important;"> Total Customer </th>
				<th style="text-align: center !important;"> Total Order </th>
				<th style="text-align: center !important;"> Cash </th>
				<th style="text-align: center !important;"> Check </th>
				<th style="text-align: center !important;"> Credit Card </th>
				<th style="text-align: center !important;"> Electronic Transfer </th>
				<th style="text-align: center !important;"> Money Order </th>
				<th style="text-align: center !important;"> Store Credit Applied </th>
				<th style="text-align: center !important;"> Store Credit Generated </th>
				<th style="text-align: center !important;"> Total Transaction </th>
				<th style="text-align: center !important;"> Short </th>
				<th style="text-align: center !important;"> Expense </th>
				<th style="text-align: center !important;"> Deposit Amount </th>
				</tr>' +   
				'</thead><tbody>' + @tr + '</tbody></table>'

EXEC dbo.spWriteToFile @DriveLocation, @html

-------------------------------------------------------Cash Details--------------------------------------------------------------------------------
		 declare @i1 int=1
		 set @tr=''  
		 select Row_Number() over (order by AutoId) as rownumber, CurrencyName,
		 [Noofcount] as NoOfValue,[TotalAmount] as TotalAmount 
		 into #currency from [dbo].[Tbl_DailyBankReport_CashDetails] where ReportAutoId=@AutoId
		 declare @count1 int= (select count(1) from #currency)
		 if(@count1>0)
		 begin
					 set @html='<table class="table tableCSS" border="1" style="width:100%;border: 1px solid black;border-collapse:collapse;" id="CashDetailHeader">
					<thead>
						<tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
							<td style="text-align: center !important;">
								<h4 style="margin:10px;">Cash - Details</h4>
							</td>
						</tr>
					</thead>
					<tbody></tbody>
					</table>

					<table class="table tableCSS" border="1" style="width:100%;border: 1px solid black;border-collapse:collapse;" id="tblCashDetai">  <thead> <tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
						<td style="text-align: center !important;" class="CurrencyName text-center">CASH</td>
						<td style="text-align: center !important;" class="NoOfValue text-center">Total Count</td>
						<td style="text-align: center !important;" class="TotalAmount text-right">Total Amount</td>
					</tr>' +   
						'</thead> <tbody>'
					EXEC dbo.spWriteToFile @DriveLocation, @html
					SET @html=''
				 while(@i1<=@count1)
				 BEGIN 
										 select 
										 @CurrenctName=CurrencyName,
										 @Noofcount=NoOfValue,
										 @TotalAmount=TotalAmount
										 from #currency where rownumber = @i1 
													set @tr ='<tr>' 
																					+	'<td style=''text-align:center;''>'+ @CurrenctName + '</td>' 
																					+	'<td style=''text-align:center;''>'+ convert(varchar(50), @Noofcount) + '</td>' 
																					+	'<td style=''text-align:right;''>'+ convert(varchar(50), @TotalAmount) + '</td>' 
																			+ '</tr>' 
													EXEC dbo.spWriteToFile @DriveLocation, @tr
													set @i1 = @i1 + 1
				 END
			set @html =' 
			<tfoot>
				<tr>
					<td colspan="2" style="text-align:right"><b>Total</b></td>
					<td id="CashTotal" style="text-align:right"><b>'+convert(varchar(50),(select sum(isnull(TotalAmount,'0.00')) from #currency))+'</b></td>
				</tr>
			</tfoot>
			</table><p style="page-break-before: always"></p>'
			 EXEC dbo.spWriteToFile @DriveLocation, @html 
			end
---------------------------------------------------------------Order Details------------------------------------------------------------
declare @i3 int=1
	 set @tr=''
	 select Row_Number() over (order by AutoId) as rownumber, CustomerName,OrderNo,Cash,[Check] as tCheck,[MoneyOrder] as MoneyOrder,                                              
	  [CreditCard] as CreditCard,[StoreCredit] as StoreCredit,[ElectronicTransfer] as ElectronicTransfer,                                        
	  [TotalAmount] as TotalAmount,CONVERT(varchar(15),CAST([SettlementTime] AS TIME),100) as STime,ISNULL(CheckNO,'') as CheckNO                                           
	  into #OrdDetail from [Tbl_DailybankReport_OrderSettlmentLog]  where ReportAutoId=@AutoId
 
	 declare @count3 int= (select count(1) from #OrdDetail)
	 if(@count3>0)
	 begin

			 SET @html=''
			 SET @html ='<table class="table tableCSS" border="1" style="width:100%;border: 1px solid black;border-collapse:collapse;" id="CashDetailHeader">
			<thead>
				<tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
					<td style="text-align: center !important;">
						<h4 style="margin:10px;">Order Details</h4>
					</td>
				</tr>
			</thead>
			<tbody></tbody>
			</table>
			 <table class="table tableCSS" border="1" style="width:100%;border: 1px solid black;border-collapse:collapse;" id="tblPaymentDailyReportLog"> 
			<thead><tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
				<td style="text-align: center !important;" class="CustomerName text-left">Customer Name</td>
				<td style="text-align: center !important;" class="OrderNo text-center">Order No</td>
				<td style="text-align: center !important;" class="Stime text-center">Settlement Time</td>
				<td style="text-align: center !important;" class="Cash text-right">Cash</td>
				<td style="text-align: center !important;" class="tCheck text-right">Check</td>
				<td style="text-align: center !important;" class="ChequeNo text-right">Check No</td>
				<td style="text-align: center !important;" class="CreditCard text-right">Credit Card</td>
				<td style="text-align: center !important;" class="ElectronicTransfer text-right">Electronic Transfer</td>
				<td style="text-align: center !important;" class="MoneyOrder text-right">Money Order</td>
				<td style="text-align: center !important;" class="StoreCredit text-right">Store Credit</td>
				<td style="text-align: center !important;" class="TotalAmount text-right">Total Amount</td>
			</tr>' + '</thead>
			<tbody>'

			EXEC dbo.spWriteToFile @DriveLocation,@html

				while(@i3<=@count3)
			BEGIN
			select 
			@CustomerName= CustomerName,
			@OrderNo= OrderNo,
			@Cash=Cash,
			@Check=ISNULL(tCheck,0),
			@MoneyOrder=MoneyOrder,                                              
			@CreditCard= CreditCard,
			@StoreCredit= StoreCredit,
			@ElectronicTransfer= ElectronicTransfer,                                        
			@TotalAmount= TotalAmount,
			@STime=STime
			,@CheckNo=CheckNO
			from #OrdDetail where rownumber = @i3

			set @tr ='<tr>' 
										+	'<td style=''text-align:center;''>'+ @CustomerName + '</td>' 
										+	'<td style=''text-align:center;''>'+ @OrderNo + '</td>' 
										+	'<td style=''text-align:center;''>'+ @STime + '</td>' 
										+	'<td style=''text-align:right;''>'+ convert(varchar(50), @Cash) + '</td>' 
										+	'<td style=''text-align:right;''>'+ convert(varchar(50), @Check) + '</td>' 
										+	'<td style=''text-align:center;''>'+@CheckNo + '</td>' 
										+	'<td style=''text-align:right;''>'+ convert(varchar(50), @CreditCard) + '</td>' 
										+	'<td style=''text-align:right;''>'+ convert(varchar(50), @ElectronicTransfer) + '</td>' 
										+	'<td style=''text-align:right;''>'+ convert(varchar(50), @MoneyOrder) + '</td>' 
										+	'<td style=''text-align:right;''>'+ convert(varchar(50), @StoreCredit) + '</td>'
										+	'<td style=''text-align:right;''>'+ convert(varchar(50), @TotalAmount) + '</td>' 
							
									+ '</tr>' 
			EXEC dbo.spWriteToFile @DriveLocation,@tr
			set @i3 = @i3 + 1
			END
			SET @html=''
			set  @html= '</tbody>
			<tfoot>
				<tr>
					<td colspan="3" style="text-align:right"><b>Total</b></td>
					<td id="Cash" style=''text-align:right;''><b>'+convert(varchar(50),(select sum(isnull(Cash,'0.00')) from #OrdDetail))+'</b></td>
					<td id="Check" style=''text-align:right;''><b>'+convert(varchar(50),(select sum(isnull(tCheck,'0.00')) from #OrdDetail))+'</b></td>
					<td></td>
					<td id="CreditCard" style=''text-align:right;''><b>'+convert(varchar(50),(select sum(isnull(CreditCard,'0.00')) from #OrdDetail))+'</b></td>
					<td id="ElectronicTransfer" style=''text-align:right;''><b>'+convert(varchar(50),(select sum(isnull(ElectronicTransfer,'0.00')) from #OrdDetail))+'</b></td>
					<td id="MoneyOrder" style=''text-align:right;''><b>'+convert(varchar(50),(select sum(isnull(MoneyOrder,'0.00')) from #OrdDetail))+'</b></td>
					<td id="StoreCredit" style=''text-align:right;''><b>'+convert(varchar(50),(select sum(isnull(StoreCredit,'0.00')) from #OrdDetail))+'</b></td>
					<td id="TotalAmount" style=''text-align:right;''><b>'+convert(varchar(50),(select sum(isnull(TotalAmount,'0.00')) from #OrdDetail))+'</b></td>
				</tr>
			</tfoot>
			</table><p style="page-break-before: always"></p>'
			EXEC dbo.spWriteToFile @DriveLocation,@html
		end
------------------------------------------------------------Expense Details---------------------------------------------------------------
	declare @i2 int=1
	 set @tr=''
	 select Row_Number() over (order by AutoId) as rownumber, [PaymentMode] as PaymentMode,[Expensedescription] as ExpenseDescription,[ExpenseAmount] as ExpenseAmount, 
	 [ExpenseBy] as ExpenseBy into #ExpDetail from                 
		[dbo].[Tbl_DailyBankReport_ExpenseDetails] where ReportAutoId=@AutoId

	 declare @count2 int= (select count(1) from #ExpDetail)
	 if(@count2>0)
	 begin
			 SET @html=''
			 SET @html='<div id="expDetail"><table class="table tableCSS" border="1" style="width:100%;border: 1px solid black;border-collapse:collapse;" id="tblExpenseDetailHeader">
			<thead>
				<tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
					<td style="text-align: center !important;">
						<h4 style="margin:10px;">Expense - Details</h4>
					</td>
				</tr>
			</thead>
			<tbody></tbody>
			</table>
			<table class="table tableCSS" border="1" style="width:100%;border:1px solid black;border-collapse:collapse;" id="tblPaymentDailyReportLog"> 
			<thead>
			<tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
				<td style="text-align: center !important;" class="ePaymentMode text-center">Payment Mode</td>
				<td style="text-align: center !important;" class="ExpenseAmount text-right">Expense Amount</td>
				<td style="text-align: center !important;" class="ExpenseBy">Expense By</td>
				<td style="text-align: center !important;" class="ExpenseDescription">Expense Description</td>
			</tr>' + '</thead>
			<tbody>'

			EXEC dbo.spWriteToFile @DriveLocation,@html

				 if(@count2>0)
				 begin
						 while(@i2<=@count2)
						BEGIN
							select 
							@PaymentMode= PaymentMode,
							@ExpenseDescription= ExpenseDescription,
							@ExpenseAmount=ExpenseAmount,
							@ExpenseBy=ExpenseBy
							from #ExpDetail where rownumber = @i2

							set @tr ='<tr>' 
														+	'<td style=''text-align:center;''>'+ @PaymentMode + '</td>' 
														+	'<td style=''text-align:right;''>'+ convert(varchar(50), @ExpenseAmount) + '</td>' 
														+	'<td style=''text-align:center;''>'+ convert(varchar(50), @ExpenseBy) + '</td>' 
														+	'<td style=''text-align:center;''>'+ @ExpenseDescription + '</td>' 
												   + '</tr>' 
							EXEC dbo.spWriteToFile @DriveLocation,@tr
							set @i2 = @i2 + 1
						END

				set @html= '' 
				SET @html='</tbody>
				<tfoot>
					<tr>
						<td colspan="1" style="text-align:right"><b>Total<b></td>
						<td id="ExpAmount" style="text-align:right"><b>'+convert(varchar(50),(select sum(isnull(ExpenseAmount,'0.00')) from #ExpDetail))+'</b></td>
						<td></td>
						<td></td>
					</tr>
				</tfoot>
				</table></div><p style="page-break-before: always"></p>'
				EXEC dbo.spWriteToFile @DriveLocation,@html
				end
			end
-------------------------------------------------------SortDetail------------------------------------------------------------------
declare @i5 int=1
 set @tr=''
 select Row_Number() over (order by AutoId) as rownumber, [SettlementId] as PaymentId,SettlementDate as PaymentDate,[PaymentMode] as PaymentMode,ReceivedAmount,[ShortAmount] as SortAmount,[ReceivedBy] as ReceivedBy,                          
  [SettlementBy] as SettlementBy,CustomerId,CustomerName into #SortDetail                     
  from [dbo].[Tbl_DailyBankReport_ShortAmountDetails]  where ReportAutoId=@AutoId

 declare @count5 int= (select count(1) from #SortDetail)
 if(@count5>0)
 begin
 set @html=''
SET @html='<table class="table tableCSS" border="1" style="width:100%;border: 1px solid black;border-collapse:collapse;" id="tblSortDetailHeader">
<thead>
    <tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
        <td style="text-align: center !important;">
            <h4  style="margin:10px;">Short Amount - Details</h4>
        </td>
    </tr>
</thead>
<tbody></tbody>
</table>
<table class="table tableCSS" border="1" style="width:100%;border: 1px solid black;border-collapse:collapse;" id="tblSortDetail">
<thead>
<tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
	<td style="text-align: center !important;" class="PaymentID text-center">Settlement ID</td>
	<td style="text-align: center !important;" class="SettlementDate text-center">Settlement Date</td>
	<td style="text-align: center !important;" class="CustomerId text-center">Customer ID</td>
	<td style="text-align: center !important;" class="CustomerName">Customer Name</td>
	<td style="text-align: center !important;" class="ReceivedBy">Received By</td>
	<td style="text-align: center !important;" class="SettlementBy">Settlement By</td>
	<td style="text-align: center !important;" class="sPaymentMode text-right">Payment Mode</td>
	<td style="text-align: center !important;" class="ReceivedAmount text-right">Received Amount</td>
	<td style="text-align: center !important;" class="SortAmount text-right">Short Amount</td>
</tr>' + '</thead>
<tbody>'

EXEC dbo.spWriteToFile @DriveLocation,@html

  if(@count5>0)
 begin
 while(@i5<=@count5)
BEGIN
select 
@PaymentId= PaymentId,
@PaymentDate=PaymentDate,
@PaymentMode=PaymentMode,
@ReceivedAmount=ReceivedAmount,
@SortAmount=SortAmount,
@ReceivedBy=ReceivedBy,                          
@SettlementBy=SettlementBy,
@CustomerId=CustomerId,
@CustomerName=CustomerName
from #SortDetail where rownumber = @i5

set @tr ='<tr>' 
							+	'<td style=''text-align:center;''>'+ @PaymentId + '</td>' 
							+	'<td style=''text-align:center;''>'+ FORMAT(@PaymentDate,'MM/dd/yyy hh:mm tt') + '</td>' 
							+	'<td style=''text-align:center;''>'+ @CustomerId + '</td>' 
							+	'<td style=''text-align:right;''>'+@CustomerName + '</td>'
							+	'<td style=''text-align:right;''>'+ @ReceivedBy + '</td>'  
							+	'<td style=''text-align:center;''>'+@SettlementBy  + '</td>' 
							+	'<td style=''text-align:center;''>'+ @PaymentMode + '</td>' 
							+	'<td style=''text-align:right;''>'+ convert(varchar(50), @ReceivedAmount) + '</td>' 
							+	'<td style=''text-align:right;''>'+ convert(varchar(50), @SortAmount) + '</td>' 
                       + '</tr>' 
EXEC dbo.spWriteToFile @DriveLocation,@tr
set @i5 = @i5 + 1
END

set @html=''
SET @html='</tbody>
<tfoot>
	<tr>
		<td colspan="7" style="text-align:right"><b>Total</b></td>
		<td id="ReceivedAllAmount" style=''text-align:right;''><b>'+convert(varchar(50),(select isnull(sum(ReceivedAmount),'0.00') from #SortDetail))+'</b></td>
		<td id="SortAllAmount" style=''text-align:right;''><b>'+convert(varchar(50),(select isnull(sum(SortAmount),'0.00') from #SortDetail))+'</b></td>

	</tr>
</tfoot>
</table><p style="page-break-before: always"></p>'
EXEC dbo.spWriteToFile @DriveLocation,@html
end
end
------------------------------------------Check Cancelled [Settled] - Details---------------------------------------------------------------------------------
declare @i6 int=1
 set @tr=''
 select Row_Number() over (order by AutoId) as rownumber, [SettlementId] as SettlementId,CustomerName, CheckDate as ChequeDate,[CheckNo] as ChequeNo,      
[CheckAmount] as ReceivedAmount,[Remark] as CancelRemarks,SettlementDate as SettlementDate,      
CancelDate as CancelDate,[CancelBy] as CancelBy,      
[ReferenceOrderNo] as OrderNo into #CheckDetail from [dbo].[Tbl_DailyBankReport_CheckcancelledDetails]  where ReportAutoId=@AutoId 

 declare @count6 int= (select count(1) from #CheckDetail)
 if(@count6>0)
 begin
 set @html =''
SET @html='<table class="table tableCSS" border="1" style="width:100%;border: 1px solid black;border-collapse:collapse;" id="tblCheckDetailsHeader">
<thead>
	<tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
		<td  style="text-align: center !important;">
			<h4 style="margin:10px;">Check Cancelled [Settled] - Details</h4>
		</td>
	</tr>
</thead>
<tbody></tbody>
</table>
<table class="table tableCSS" border="1" style="width:100%;border: 1px solid black;border-collapse:collapse;" id="tblCheckDetails">
<thead><tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
	<td style="text-align: center !important;" class="SettlementID text-center">Settlement Id</td>
    <td style="text-align: center !important;" class="SettlementDate text-center">Settlement Date</td>
    <td style="text-align: center !important;" class="CustemerName">Customer Name</td>
    <td style="text-align: center !important;" class="RefOrderNo text-center">Reference Order No</td>
    <td style="text-align: center !important;" class="CheckNo text-center">Check No</td>
    <td style="text-align: center !important;" class="CheckDate text-center">Check Date</td>
    <td style="text-align: center !important;" class="CheckAmount text-right">Check Amount</td>
    <td style="text-align: center !important;" class="CancelDate text-center">Cancel Date</td>
    <td style="text-align: center !important;" class="CancelBy">Cancel By</td>
    <td style="text-align: center !important;" class="CancelRemark">Remark</td>
</tr>' + '</thead>
<tbody>'

EXEC dbo.spWriteToFile @DriveLocation,@html
end
  if(@count6>0)
  begin
 while(@i6<=@count6)
BEGIN
select 
@PaymentId=SettlementId,
@CustomerName=CustomerName, 
@CheckDate=ChequeDate,
@CheckNo=ChequeNo,      
@ReceivedAmount=ReceivedAmount,
@Remarks=CancelRemarks,
@SettlementDate=SettlementDate,      
@PaymentDate=CancelDate,
@ReceivedBy=CancelBy,      
@OrderNo=OrderNo
from #CheckDetail where rownumber = @i6

set @tr ='<tr>' 
							+	'<td style=''text-align:center;''>'+ @PaymentId + '</td>' 
							+	'<td style=''text-align:center;''>'+ FORMAT(@SettlementDate,'MM/dd/yyy hh:mm tt') + '</td>' 
							+	'<td style=''text-align:center;''>'+ @CustomerName + '</td>' 
							+	'<td style=''text-align:center;''>'+ @OrderNo + '</td>' 
							+	'<td style=''text-align:center;''>'+ @CheckNo + '</td>' 
							+	'<td style=''text-align:center;''>'+ FORMAT(@CheckDate,'MM/dd/yyy') + '</td>' 
							+	'<td style=''text-align:right;''>'+ convert(varchar(50), @ReceivedAmount) + '</td>' 
							
							+	'<td style=''text-align:center;''>'+ FORMAT(@PaymentDate,'MM/dd/yyy hh:mm tt') + '</td>' 
							+	'<td style=''text-align:center;''>'+ @ReceivedBy + '</td>' 
							+	'<td style=''text-align:center;''>'+ @Remarks + '</td>' 

                       + '</tr>' 
					   EXEC dbo.spWriteToFile @DriveLocation,@tr
set @i6 = @i6 + 1
END

set @html =''
SET @html='</tbody>
<tfoot>
	<tr>
		<td colspan="6" style="text-align:right"><b>Total</b></td>
        <td id="CheckAmount" style=''text-align:right;''><b>'+convert(varchar(50),(select isnull(sum(ReceivedAmount),'0.00') from #CheckDetail))+'</b></td>
        <td></td>
        <td></td>
        <td></td>
	</tr>
</tfoot>
</table><p style="page-break-before: always"></p>'

EXEC dbo.spWriteToFile @DriveLocation,@html

end

-----------------------------------------------------Generate Store Credit - Details----------------------------------------------------------------------
declare @i7 int=1
 set @tr=''
 select Row_Number() over (order by AutoId) as rownumber, [StoreCredit] as CreditAmount,[PaymentMode] as PaymentMode,[SettlementId] as PaymentId,[SettlementDate] as PaymentDate,                        
  [ReceivedBy] as ReceivedBY,CustomerId,CustomerName ,[SettlementBy] as SettleBy into #Storecredit                             
  from [dbo].[Tbl_DailyBankReport_GenerateCreditDetails]  where ReportAutoId=@AutoId         

 declare @count7 int= (select count(1) from #Storecredit)
 if(@count7>0)
 begin
 set @html =''
SET @html='<table class="table tableCSS" border="1" style="width:100%;border: 1px solid black;border-collapse:collapse;" id="tblGENERATESTORECREDITHeader">
<thead>
    <tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
        <td  style="text-align: center !important;">
            <h4 style="margin:10px;">Generate Store Credit - Details</h4>
        </td>
    </tr>
</thead>
<tbody></tbody>
</table>
<table class="table tableCSS" border="1" style="width:100%;border: 1px solid black;border-collapse:collapse;" id="tblGENERATESTORECREDIT">
<thead><tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
	<td style="text-align: center !important;" class="PaymentId text-center">Settlement Id</td>
    <td style="text-align: center !important;" class="PaymentDate text-center">Settlement Date</td>
    <td style="text-align: center !important;" class="CustomerId text-center">Customer Id</td>
    <td style="text-align: center !important;" class="CustomerName">Customer Name</td>
    <td style="text-align: center !important;" class="PaymentMode text-center">Payment Mode</td>
    <td style="text-align: center !important;" class="StoreCredit text-right">Store Credit</td>
    <td style="text-align: center !important;" class="ReceivedBy">Received By</td>
    <td style="text-align: center !important;" class="SettlementBy">Settlement By</td>
</tr>' + '</thead>
<tbody>'

EXEC dbo.spWriteToFile @DriveLocation,@html

   if(@count7>0)
  begin
 while(@i7<=@count7)
BEGIN
	select 
	@PaymentId=PaymentId,
	@PaymentDate= PaymentDate,  
	@CustomerId=CustomerId,
	@CustomerName=CustomerName ,
	@PaymentMode=PaymentMode,
	@CreditAmount=CreditAmount,                 
	@ReceivedBy=ReceivedBY,
	@SettlementBy=SettleBy
	from #Storecredit where rownumber = @i7

	set @tr = '<tr>' 
	+	'<td style=''text-align:center;''>'+ @PaymentId + '</td>' 
	+	'<td style=''text-align:center;''>'+ FORMAT(@PaymentDate,'MM/dd/yyy hh:mm tt') + '</td>' 
	+	'<td style=''text-align:center;''>'+ @CustomerId + '</td>' 
	+	'<td style=''text-align:center;''>'+ @CustomerName + '</td>' 
	+	'<td style=''text-align:center;''>'+ @PaymentMode + '</td>' 
	+	'<td style=''text-align:right;''>'+ convert(varchar(50), @CreditAmount) + '</td>' 
	+	'<td style=''text-align:center;''>'+ @ReceivedBy + '</td>' 
	+	'<td style=''text-align:center;''>'+ @SettlementBy + '</td>' 
	+   '</tr>' 
	EXEC dbo.spWriteToFile @DriveLocation,@tr
	set @i7 = @i7 + 1
END

set @html =''
SET @html='</tbody>
<tfoot>
	<tr>
		<td colspan="5" style="text-align:right"><b>Total</b></td>
        <td id="G_Credit" style=''text-align:right;''><b>'+convert(varchar(50),(select isnull(sum(CreditAmount),'0.00') from #Storecredit))+'</b></td>
        <td></td>
        <td></td>
	</tr>
</tfoot>
</table><p style="page-break-before: always"></p>'

EXEC dbo.spWriteToFile @DriveLocation,@html
		end
 end
---------------------------------------------------------------------------------------------------------------------------
 set @html =''
 set @html='<table border="1" style="width:100%;border: 1px solid black;border-collapse:collapse;">
<tr>
    <td><b style="color:red">Note : -</b></td>
</tr>
<tr>
    <td><b>Transaction Total :</b> Cash <b>+</b> Check <b>+</b> Credit Card <b>+</b> Electronic Transfer <b>+</b> Money Order <b>+</b> Store Credit Applied <b>+</b> Store Credit Generated </td>
</tr>
<tr>
    <td><b>Deposit Amount :</b>Total Transaction -(Short <b>+</b> expense <b>+</b> Store Credit Applied <b>+</b> Electronic Transfer)</td>
</tr>
</table>
</td>
</tr>
</tbody>
</table>
</div> </html>'
EXEC dbo.spWriteToFile @DriveLocation,@html

	set @Subject = 'Bank Report Log for ' +UPPER((select CompanyDistributerName from dbo.CompanyDetails))+' : ' + format(GETDATE(),'MM/dd/yyy hh:mm tt');

	SELECT @FromEmailId=FromEmail,@FromName=DisplayName,@smtp_userName=SMTP_USERNAME,@Password=SMTP_PASSWORD,
	@SMTPServer=HOST,@Port=SMTPPORT,@SSL=EnableSsl from 
	EmailServerMaster_AWS  

	set @BCCEmailId=(select BCCEmail from Tbl_ReportEmailReceiver where Autoid=5);
	set @ToEmailId=(select ToEmail from Tbl_ReportEmailReceiver where Autoid=5)
	SET @CCEmailId=(select CCEmail from Tbl_ReportEmailReceiver where Autoid=5)
	IF(select count(1) from #t1 )>0
	BEGIN
		 EXEC [Emailing_DB].[dbo].[ProcSubmitEmail] 
			@Opcode=11,
			@FromEmailId =@FromEmailId,
			@FromName = @FromName,
			@smtp_userName=@smtp_userName,
			@Password = @Password,
			@SMTPServer = @SMTPServer,
			@Port =@Port,
			@SSL =@SSL,
			@ToEmailId =@ToEmailId,
			@CCEmailId =@CCEmailId,
			@BCCEmailId =@BCCEmailId,  
			@Subject =@Subject,
			@EmailBody = '',
			@SentDate ='',
			@Status =0,
			@SourceApp ='PSM',
			@SubUrl ='Bank Report Log',
			@attachment=@DriveLocation,
			@isException=0,
			@exceptionMessage=''  
	END
END

