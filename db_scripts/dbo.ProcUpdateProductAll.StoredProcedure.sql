
ALTER PROCEDURE [dbo].[ProcUpdateProductAll]  
@OldProductId int,@NewProductId int,  
@Message varchar(20) out  
AS  
BEGIN  
Declare @OldProductAutoId int,@NewProductAutoId int  
SET @OldProductAutoId=(SELECT AutoId FROM ProductMaster WHERE ProductId=@OldProductId)  
SET @NewProductAutoId=(SELECT AutoId FROM ProductMaster WHERE ProductId=@NewProductId)  
 SET @Message=''  
 IF NOT EXISTS(SELECT * FROM PackingDetails AS PD where ProductAutoid=@OldProductAutoId  
 and unitType not in (  
 SELECT UnitType FROM PackingDetails AS PD where ProductAutoid=@NewProductAutoId  
 ))  
 BEGIN  
  
  BEGIN TRY
		BEGIN TRAN  
			update AllowQtyPiece set ProductAutoId=@NewProductAutoId where ProductAutoId=@OldProductAutoId    
			update BillItems   set ProductAutoId=@NewProductAutoId where ProductAutoId=@OldProductAutoId   
			update CreditItemMaster    set ProductAutoId=@NewProductAutoId where ProductAutoId=@OldProductAutoId  
			update CustomerWisePricing  set ProductAutoId=@NewProductAutoId where ProductAutoId=@OldProductAutoId   
			update Delivered_Order_Items set ProductAutoId=@NewProductAutoId where ProductAutoId=@OldProductAutoId     
			update Draft_StockItemMaster  set ProductAutoId=@NewProductAutoId where ProductAutoId=@OldProductAutoId    
			update DraftItemMaster  set ProductAutoId=@NewProductAutoId where ProductAutoId=@OldProductAutoId  
			update DraftPurchaseProductsMaster  set ProductAutoId=@NewProductAutoId where ProductAutoId=@OldProductAutoId  
			update InvItemsList  set ProductAutoId=@NewProductAutoId where ProductAutoId=@OldProductAutoId  
			update ItemBarcode  set ProductAutoId=@NewProductAutoId where ProductAutoId=@OldProductAutoId  
			update OrderItemMaster  set ProductAutoId=@NewProductAutoId where ProductAutoId=@OldProductAutoId  
			update OrderItems_Original  set ProductAutoId=@NewProductAutoId where ProductAutoId=@OldProductAutoId  
			update PackingDetailsUpdateLog  set ProductAutoId=@NewProductAutoId where ProductAutoId=@OldProductAutoId   
			DELETE FROM ProductPricingInPriceLevel   where ProductAutoId=@OldProductAutoId  
			DELETE FROM ProductPricingInPriceLevel_log  where ProductAutoId=@OldProductAutoId  
			update ProductStockUpdateLog set ProductId=@NewProductId where ProductId=@OldProductId  
			update PurchaseProductsMaster  set ProductAutoId=@NewProductAutoId where ProductAutoId=@OldProductAutoId   
			DELETE FROM  PackingDetails  where ProductAutoId=@OldProductAutoId  
			DELETE FROM  ProductMaster  where AutoId=@OldProductAutoId   
		COMMIT TRAN
   END TRY
   BEGIN CATCH
	ROLLBACK TRAN
   END CATCH
 END  
 else  
 begin  
 SET @Message= 'Invalid Request'  
 end  
END
GO
