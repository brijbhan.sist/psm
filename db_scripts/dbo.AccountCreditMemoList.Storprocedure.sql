alter PROCEDURE [dbo].[ProcAccountCreditMemoList]                                    
@Opcode INT=NULL,                                                                  
@CreditNo varchar(50)=null,
@CustomerAutoId INT=NULL,    
@CreditAutoId INT=NULL,
@sortInCode INT=NULL,
@EmpAutoId INT=NULL,                                     
@CreditType INT=NULL,                                    
@FromDate DATE=NULL,                                    
@ToDate DATE=NULL,                                                                                                         
@Status INT=NULL,                                    
@EmpType  INT=NULL,                                    
@SalesPerson int=null,
@PageIndex INT = 1,                                    
@PageSize INT = 10,                                    
@RecordCount INT =null,                                    
@isException BIT OUT,                                    
@exceptionMessage VARCHAR(max) OUT                                    
AS                                    
BEGIN                                    
 BEGIN TRY                                    
  SET @isException=0                                    
  SET @exceptionMessage='Success'                                    
  DECLARE @STATE INT                                     
                                  
 If @Opcode=41                                     
   BEGIN                                    
  SET @EmpType=(SELECT EmpType FROM EmployeeMaster WHERE AutoId=@EmpAutoId)  
  select 
  (
  SELECT AutoId,StatusType FROM StatusMaster WHERE Category='CreditMaster' ORDER BY StatusType asc  
   for json path
  ) as Status,
  (
  select AutoId,OrderType from OrderTypeMaster where Type='CREDIT' order by OrderType
  for json path
  ) as OrderType,
  (
  SELECT FirstName+' '+LastName as SalesPerson,AutoId FROM EmployeeMaster WHERE EmpType=2 and Status=1 order by SalesPerson
  for json path
  ) as salesPerson
  for json path
  END 
   ELSE IF @Opcode=42                                                               
  BEGIN   
  SELECT AutoId AS CuId,CustomerId+' '+CustomerName as CUN FROM CustomerMaster   WHERE            
  status=1                                                   
  and (SalesPersonAutoId=@SalesPerson or ISNULL(@SalesPerson,0)=0)
  order by replace(CustomerId+' '+CustomerName,' ','') ASC     
   for json path   
  END  
ELSE IF @Opcode=44                                    
   BEGIN                                    
    SET @EmpType =(SELECT EmpType FROM  EmployeeMaster WHERE AutoId=@EmpAutoId)                                    
    SELECT ROW_NUMBER() OVER(ORDER BY OrderNo desc) AS RowNumber, * INTO #Results from                      
    (                                       
    SELECT OM.CreditAutoId AS [AutoId],OM.CreditNo AS OrderNo,CONVERT(VARCHAR(20), OM.CreditDate, 101) AS OrderDate,CM.[CustomerName] AS CustomerName,                   
    SM.StatusType AS Status,OM.[Status] As StatusCode,                                    
    TotalAmount AS [GrandTotal],                                    
    (SELECT COUNT(distinct [ProductAutoId]) from [dbo].CreditItemMaster WHERE CreditAutoId=OM.CreditAutoId) AS NoOfItems ,                                    
    OT.OrderType as CreditType,                                    
    (SELECT OrderNo from OrderMaster as mo where mo.AutoId=OM.OrderAutoId) as referenceorderno,emp.FirstName+' '+LastName as SalesPerson                                 
    FROM [dbo].CreditMemoMaster As OM                                    
    INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.AutoId = OM.CustomerAutoId  
	INNER JOIN [dbo].OrderTypeMaster AS OT ON OT.AutoId = OM.CreditType AND OT.Type='CREDIT' 
	INNER JOIN EmployeeMaster AS emp ON emp.AutoId = OM.SalesPersonAutoId
    LEFT JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.[Status] AND SM.Category = 'CreditMaster'                                               
    WHERE                                     
    (@CustomerAutoId=0 or @CustomerAutoId is null or OM.CustomerAutoId=@CustomerAutoId)
	and (@SalesPerson=0 or @SalesPerson is null or OM.SalesPersonAutoId=@SalesPerson)
    and(@CreditType=0 or @CreditType is null or OM.CreditType=@CreditType)                                    
    and (@CreditNo is null or @CreditNo ='' or OM.CreditNo like '%' + @CreditNo + '%')                                    
    and (@FromDate is null or @FromDate = '' or @Todate is null or @Todate = '' or (convert(date,CreditDate) between @FromDate and @Todate))                                        
    and (@Status is null or @Status=0 or OM.[Status]=@Status) 
	and (@EmpAutoId = 0 or  @EmpType=1 or @EmpType=6)                                    
      )as t order by OrderNo                                    
                 
    SELECT COUNT(RowNumber) AS RecordCount,case when @PageSize=0 then COUNT(OrderNo) else @PageSize end AS PageSize,@PageIndex as PageIndex FROM #Results           
                                    
    SELECT * FROM #Results                                    
    WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))                                     
      select @EmpType as EmpType                                 
   END  
    ELSE IF @Opcode=47                                    
   BEGIN                                    
       SELECT CreditNo,format(CreditDate,'MM/dd/yyyy hh:mm tt')  AS CreditDate FROM CreditMemoMaster WHERE CreditAutoId=@CreditAutoId                                    
       SELECT (EM.FirstName+' '+ EM.LastName ) AS EmpName, ETM.TypeName AS EmpType,format(LogDate,'MM/dd/yyyy hh:mm tt') LogDate,                                                                  
    AM.[Action],Remarks FROM CreditMemoLog AS CLOG                                    
    INNER JOIN EmployeeMaster AS EM ON EM.AutoId=CLOG.EmpAutoId               
    INNER JOIN [dbo].[tbl_ActionMaster] As AM ON AM.[AutoId] = CLOG.[ActionAutoId]                                   
    inner join EmployeeTypeMaster As ETM on ETM.AutoId=EM.EmpType                                  
    WHERE CLOG.CreditAutoId=@CreditAutoId order by 
	(CASE WHEN @sortInCode=1 THEN LogDate END)ASC,
	(CASE WHEN @sortInCode=2 THEN LogDate END) DESC
   END   
 END TRY                                    
 BEGIN CATCH                                    
   SET @isException=1                                    
   SET @exceptionMessage=ERROR_MESSAGE()                                    
 END CATCH                                    
END 
GO
