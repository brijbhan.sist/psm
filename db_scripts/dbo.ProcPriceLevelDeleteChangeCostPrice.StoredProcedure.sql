USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcEmailReceiverMaster]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
 --Need to delete Product Price Level which is less that Cost price.
ALTER PROCEDURE [dbo].[ProcPriceLevelDeleteChangeCostPrice]    
@EmpAutoId int
AS      
BEGIN 
	BEGIN TRY
			INSERT INTO [dbo].[ProductPricingInPriceLevel_log]
			([AutoId],[PriceLevelAutoId],[ProductAutoId],[UnitAutoId],[CustomPrice],[createDated],[InsertcreateDated],[UpdatedBy])
			SELECT [AutoId],[PriceLevelAutoId],[ProductAutoId],[UnitAutoId],[CustomPrice],[createDated],GETDATE(),@EmpAutoId  from [dbo].ProductPricingInPriceLevel where AutoId in
			(
			select ppl.AutoId  from [dbo].ProductPricingInPriceLevel as ppl
			inner join [dbo].PackingDetails as pd on pd.ProductAutoId=ppl.ProductAutoId and pd.UnitType=ppl.UnitAutoId
			where ppl.CustomPrice < convert(decimal(10,2),pd.CostPrice)
			)    
			--Delete on db
			delete from [dbo].ProductPricingInPriceLevel where AutoId in
			(
			select ppl.AutoId  from [dbo].ProductPricingInPriceLevel as ppl
			inner join [dbo].PackingDetails as pd on pd.ProductAutoId=ppl.ProductAutoId and pd.UnitType=ppl.UnitAutoId
			where ppl.CustomPrice < convert(decimal(10,2),pd.CostPrice)
			)
	 END TRY
	 BEGIN CATCH
	 END CATCH
END      
GO
