ALTER PROCEDURE [dbo].[ProcVendorMaster]                                      
@Opcode int=Null,                                     
@AutoId  int=Null,                       
@Location varchar (20) = null,                      
@LocationName int = null,                       
@LocationType int = null,                                   
@VendorId VARCHAR(12)=NULL,                                     
@VendorName VARCHAR(50)=NULL,                                    
@Address VARCHAR(200)=NULL,                                     
@Country INT=NULL,                                      
@State VARCHAR(50)=NULL,                                     
@City VARCHAR(50)=NULL,                                     
@Zipcode VARCHAR(50)=NULL,                                      
@ContactPerson VARCHAR(50)=NULL,                                     
@Cell VARCHAR(20)=NULL,                                      
@Office1 VARCHAR(20)=NULL,                                      
@Office2 VARCHAR(20)=NULL,                                    
@Email VARCHAR(50)=NULL,                                     
@Status int=null,                       
@Type int=null,    
@EmpAutoId INT=NULL,
@PageIndex INT = 1,                                    
@PageSize INT = 10,                                     
@RecordCount INT =null,                                    
@isException bit out,                                     
@exceptionMessage varchar(max) out                                    
AS                                      
BEGIN                                      
 BEGIN TRY                                     
		SET @isException=0                                      
		SET @exceptionMessage='Success'                                     
	IF @Opcode=11                                     
		BEGIN                                      
			SET @VendorId = (SELECT dbo.SequenceCodeGenerator('VendorId'))                                     
		IF EXISTS(SELECT [VendorName] FROM [dbo].[VendorMaster] WHERE [VendorName]=@VendorName)                                     
		BEGIN                                      
			SET @isException=1                                      
			SET @exceptionMessage='Vendor details already exist.';                                     
			END                        
		else IF (SELECT count(LocationAutoId) FROM [dbo].[VendorMaster] where LocationAutoId = @LocationName and LocationTypeAutoId = 1) > 0                                     
		BEGIN                                      
			SET @isException=1                        
			set @Location = (select Location from LocationMaster where AutoId = @LocationName)                                    
			SET @exceptionMessage= @Location +' '+ 'Vendor already exist.';                                     
		END                                          
		ELSE                                      
		BEGIN TRY                                      
		BEGIN TRAN              
			INSERT INTO [dbo].[VendorMaster] ([VendorId],[VendorName],[Address],[Country],[State],[City],[Zipcode],[ContactPerson],[Cell],                                     
			[Office1],[Office2],[Email],[Status],LocationTypeAutoId,LocationAutoId,CreateBy,CreateDate,UpdateBy,UpdateDate)                                      
			VALUES(@VendorId,@VendorName,@Address,@Country,@State,@City,@Zipcode,@ContactPerson,@Cell,@Office1,@Office2,@Email,@Status,@LocationType,@LocationName,@EmpAutoId,GETDATE(),@EmpAutoId,GETDATE())                                     
			UPDATE SequenceCodeGeneratorMaster SET currentSequence=currentSequence+1 WHERE SequenceCode='VendorId'                                    
		COMMIT TRANSACTION                                      
		END TRY                                      
		BEGIN CATCH                                      
		ROLLBACK TRAN                          
			SET @isException=1                                      
			SET @exceptionMessage='Oops! Something went wrong.Please try later.'                                      
		END CATCH                     
	END                                       
  ELSE IF @Opcode=21                                      
   BEGIN                                     
     if exists(select VendorName from VendorMaster where LocationAutoId = @LocationName and LocationTypeAutoId = 1 and [VendorId]!=@VendorId  )                      
  begin                      
		SET @isException=1                   
		set @Location = (select lm.Location from VendorMaster vm inner join LocationMaster lm on lm.AutoId = vm.LocationAutoId where LocationAutoId = @LocationName)                                     
		SET @exceptionMessage= @Location +' vendor already exist.';                       
  end                      
  else if exists(select VendorName from VendorMaster where VendorName = @VendorName and [VendorId]!=@VendorId  )                      
  begin                     
   SET @isException=1                                      
      SET @exceptionMessage='Vendor details already exist.';                       
  end                      
  else                      
  begin                                         
    UPDATE [dbo].[VendorMaster] SET [VendorName]=@VendorName,[Address]=@Address,[Country]=@Country,[State]=@State,[City]=@City,[Zipcode]=@Zipcode,                                      
    [ContactPerson]=@ContactPerson,[Cell]=@Cell,[Office1]=@Office1,[Office2]=@Office2,[Email]=@Email,[Status]=@Status,LocationTypeAutoId=@LocationType,
	LocationAutoId=@LocationName,UpdateBy=@EmpAutoId,UpdateDate=@EmpAutoId                                      
    WHERE [VendorId]=@VendorId                       
 end                                     
   END                                      
  ELSE IF @Opcode=31                                      
   BEGIN                                    
   -----Modify on 08-09-2019 By Rizwan Ahmad                                       
        IF EXISTS(Select * from StockEntry WHERE VendorAutoId = (Select AutoId from VendorMaster Where VendorId=@VendorId))                                   
  BEGIN                                  
      SET @isException=1                                  
   SET @exceptionMessage='Vendor has been used in purchased order.'                                  
  END                                  
  ELSE                                  
  BEGIN                                     
            DELETE FROM [dbo].[VendorMaster] WHERE [VendorId]=@VendorId                                     
  END                                  
   END                                      
                                      
  ELSE IF @Opcode = 41                                      
   BEGIN                                        
    SET NOCOUNT ON;                                        
    SELECT ROW_NUMBER() OVER(ORDER BY  VendorId  desc) AS RowNumber, * INTO #Results FROM                                        
    (                                        
    SELECT VM. VendorId , VendorName , ContactPerson , Cell ,                                    
    case when Office1='' and Office2!='' then Office2                                    
    when Office2='' and Office1!='' then Office1                                    
    when Office1='' and Office2='' then ''                                    
    else Office1  + ' / ' +  Office2  end as Office,                                     
    Email ,SM. StatusType  As Status                                         
    FROM  dbo . VendorMaster  AS VM INNER JOIN StatusMaster As SM ON SM.AutoId=VM.Status AND  Category  is NULL                                        
    WHERE (@VendorName IS NULL OR @VendorName='' OR VM.VendorName LIKE '%' + @VendorName + '%')                                     
    and   (@Cell IS NULL OR @Cell='' OR VM.Cell=@Cell)                                
                                   
    ) AS t ORDER BY VendorId                                       
    SELECT case when isnull(@PageSize,0)=0 then 0 else  COUNT( VendorId )end AS RecordCount, @PageSize AS PageSize, @PageIndex AS PageIndex FROM #Results                                     
    SELECT * FROM #Results                                     
    WHERE (isnull(@PageSize,0)=0 or ( RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1  ))                                    
   END                                      
  ELSE IF @Opcode = 42    --Updated on  11/23/2019  By Rizwan Ahmad                                  
   BEGIN                                      
  SELECT [AutoId],[StateCode] + ' - ' + [StateName] As StateName FROM [dbo].[State]                                     
  SELECT [AutoId],[StatusType] FROM [dbo].[StatusMaster] WHERE [Category] is NULL                                      
  SELECT [AutoId],[CountryCode] + ' - ' + [Country] AS Country FROM [dbo].[Country]                                 
  select distinct [Zipcode] from ZipMaster                               
   END                                      
  ELSE IF @Opcode = 43   --Updated on  11/02/2019 02:04 AM By Rizwan Ahmad                                                 
   BEGIN      
  SELECT  VendorId , VendorName , Address , vm.Country as AutoId , Zipcode , ContactPerson , Cell , Office1 , Office2 , Email ,vm.Status,isnull(LocationTypeAutoId,0) as LocationType,                  
  isnull(LocationAutoId,0)as LocationName,sm.StateName,City,C.Country,sm.AutoId as StateId                   
  FROM   VendorMaster  as vm                                    
  left join State as sm on                                    
  vm.State=sm.AutoId        
  left join Country as C on    
  vm.Country=C.AutoId                               
  WHERE [VendorId]=@VendorId                
   END                                      
ELSE IF @Opcode = 44                                  
BEGIN 
	 Select * from 
	(
	Select(SELECT AutoId,[VendorName] FROM [dbo].[VendorMaster] WHERE [Status]=1 
	order by [VendorName]  ASC  for json path)Vendor,  
	(SELECT [AutoId],Convert(varchar(10),[ProductId]) + '--' + [ProductName] as [ProductName] 
	FROM [dbo].[ProductMaster]  order by [ProductName] ASC  for json path) Product
	) as t
	for json path
END                                      
ELSE IF @Opcode = 45                                     
BEGIN                                      
	Select CityId,cm.CityName,sm.AutoId as StateId,sm.StateName,zm.Zipcode from ZipMaster as zm                     
	inner join CityMaster as cm on                                    
	zm.CityId=cm.AutoId                                    
	inner join State as sm on                                    
	cm.StateId=sm.AutoId                                     
	WHERE zm.Zipcode=@Zipcode                                     
END                      
if @Opcode = 46                      
begin                      
	select * from LocationMaster where Location != @Location                      
end  
END TRY                                     
BEGIN CATCH                                      
	SET @isException=1                                      
	SET @exceptionMessage='Oops! Something went wrong.Please try later.'                                  
END CATCH                                      
                                      
END 
GO
