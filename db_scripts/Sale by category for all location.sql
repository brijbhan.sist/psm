--Cat / Sub Cat Sales by location
--Order Date + Product Net Sales (Line Item) + Approved Credit Memo Date


declare @orderfrom date = '02/01/2020'
declare @todate date = '02/29/2020'

use [psmct.a1whm.com]
select cm.CategoryName,scm.SubcategoryName,
sum(Net_Sale_Rev) as TotalSales into ##tempCT
from
(
	select em.FirstName,t1.ProductId,t1.ProductName,t1.UnitType,t1.Qty as [Pc per unit],
	isnull(pppl.CustomPrice,0) as CustomPrice,
	convert(decimal(18,2),(t1.QtyDel/t1.Qty)) as [Sold_Default_Qty],
	t1.QtyDel as [Sold_Pc],
	t1.NetPrice as [Sale Rev],
	convert(decimal(18,2),((t1.QtyDel-isnull(t2.QtyDel,0))/t1.Qty)) as [Net_Sold_Default_Qty],
	(t1.QtyDel-isnull(t2.QtyDel,0)) as [Net_Sold_Pc],
	(t1.NetPrice - isnull(t2.NetPrice,0)) as Net_Sale_Rev,
	t1.Del_CostPrice -isnull(((case when t1.QtyDel != 0 then (t1.Del_CostPrice/t1.QtyDel) else 0 end)*t2.QtyDel),0) as costP
	from (
		select om.SalesPersonAutoId, pd.ProductAutoId, pm.ProductId, pm.ProductName, pm.PackingAutoId, um.UnitType, pd.Qty,
		sum(doi.QtyDel) as QtyDel, sum(doi.NetPrice) as NetPrice, sum((doi.Del_CostPrice/doi.QtyPerUnit)*doi.QtyDel) as Del_CostPrice
		from OrderMaster as om
		inner join Delivered_Order_Items as doi on doi.OrderAutoId=om.AutoId
		inner join ProductMaster as pm on pm.AutoId=doi.ProductAutoId
		inner join PackingDetails as pd on pd.ProductAutoId=pm.AutoId and pd.UnitType=pm.PackingAutoId
		inner join UnitMaster as um on um.AutoId=pm.PackingAutoId
		where convert(date,OrderDate) between  @orderfrom and @todate and om.Status=11
		group by om.SalesPersonAutoId, pm.ProductId, pm.ProductName, um.UnitType, pd.Qty,pd.ProductAutoId, pm.PackingAutoId
	) as t1
	left join (
		select cmm.CreatedBy, pd.ProductAutoId, pm.ProductId, pm.ProductName, pm.PackingAutoId, um.UnitType, pd.Qty,
		sum(cim.TotalPeice) as QtyDel, sum(cim.NetAmount) as NetPrice
		from CreditMemoMaster as cmm
		inner join CreditItemMaster as cim on cim.CreditAutoId=cmm.CreditAutoId
		inner join ProductMaster as pm on pm.AutoId=cim.ProductAutoId
		inner join PackingDetails as pd on pd.ProductAutoId=pm.AutoId and pd.UnitType=pm.PackingAutoId
		inner join UnitMaster as um on um.AutoId=pm.PackingAutoId
		where convert(date,cmm.ApprovalDate) between  @orderfrom and @todate and cmm.Status=3
		group by cmm.CreatedBy, pm.ProductId, pm.ProductName, um.UnitType, pd.Qty,pd.ProductAutoId, pm.PackingAutoId
	) as t2 on t1.SalesPersonAutoId=t2.CreatedBy and t1.ProductAutoId=t2.ProductAutoId and t1.PackingAutoId=t2.PackingAutoId
	inner join EmployeeMaster as em on em.AutoId=t1.SalesPersonAutoId
	left join ProductPricingInPriceLevel as pppl on pppl.ProductAutoId=t1.ProductAutoId and pppl.UnitAutoId=t1.PackingAutoId and pppl.PriceLevelAutoId=2838
) as t
inner join ProductMaster as pm on pm.ProductId = t.ProductId
inner join CategoryMaster as cm on cm.AutoId = pm.CategoryAutoId
inner join SubCategoryMaster  as scm on scm.AutoId=pm.SubcategoryAutoId
group by  cm.CategoryName,scm.SubcategoryName
order by  cm.CategoryName,scm.SubcategoryName

use [psmnpa.a1whm.com]
select cm.CategoryName,scm.SubcategoryName,
sum(Net_Sale_Rev) as TotalSales into ##tempNPA
from
(
	select em.FirstName,t1.ProductId,t1.ProductName,t1.UnitType,t1.Qty as [Pc per unit],
	isnull(pppl.CustomPrice,0) as CustomPrice,
	convert(decimal(18,2),(t1.QtyDel/t1.Qty)) as [Sold_Default_Qty],
	t1.QtyDel as [Sold_Pc],
	t1.NetPrice as [Sale Rev],
	convert(decimal(18,2),((t1.QtyDel-isnull(t2.QtyDel,0))/t1.Qty)) as [Net_Sold_Default_Qty],
	(t1.QtyDel-isnull(t2.QtyDel,0)) as [Net_Sold_Pc],
	(t1.NetPrice - isnull(t2.NetPrice,0)) as Net_Sale_Rev,
	t1.Del_CostPrice -isnull(((case when t1.QtyDel != 0 then (t1.Del_CostPrice/t1.QtyDel) else 0 end)*t2.QtyDel),0) as costP
	from (
		select om.SalesPersonAutoId, pd.ProductAutoId, pm.ProductId, pm.ProductName, pm.PackingAutoId, um.UnitType, pd.Qty,
		sum(doi.QtyDel) as QtyDel, sum(doi.NetPrice) as NetPrice, sum((doi.Del_CostPrice/doi.QtyPerUnit)*doi.QtyDel) as Del_CostPrice
		from OrderMaster as om
		inner join Delivered_Order_Items as doi on doi.OrderAutoId=om.AutoId
		inner join ProductMaster as pm on pm.AutoId=doi.ProductAutoId
		inner join PackingDetails as pd on pd.ProductAutoId=pm.AutoId and pd.UnitType=pm.PackingAutoId
		inner join UnitMaster as um on um.AutoId=pm.PackingAutoId
		where convert(date,OrderDate) between  @orderfrom and @todate and om.Status=11
		group by om.SalesPersonAutoId, pm.ProductId, pm.ProductName, um.UnitType, pd.Qty,pd.ProductAutoId, pm.PackingAutoId
	) as t1
	left join (
		select cmm.CreatedBy, pd.ProductAutoId, pm.ProductId, pm.ProductName, pm.PackingAutoId, um.UnitType, pd.Qty,
		sum(cim.TotalPeice) as QtyDel, sum(cim.NetAmount) as NetPrice
		from CreditMemoMaster as cmm
		inner join CreditItemMaster as cim on cim.CreditAutoId=cmm.CreditAutoId
		inner join ProductMaster as pm on pm.AutoId=cim.ProductAutoId
		inner join PackingDetails as pd on pd.ProductAutoId=pm.AutoId and pd.UnitType=pm.PackingAutoId
		inner join UnitMaster as um on um.AutoId=pm.PackingAutoId
		where convert(date,cmm.ApprovalDate) between  @orderfrom and @todate and cmm.Status=3
		group by cmm.CreatedBy, pm.ProductId, pm.ProductName, um.UnitType, pd.Qty,pd.ProductAutoId, pm.PackingAutoId
	) as t2 on t1.SalesPersonAutoId=t2.CreatedBy and t1.ProductAutoId=t2.ProductAutoId and t1.PackingAutoId=t2.PackingAutoId
	inner join EmployeeMaster as em on em.AutoId=t1.SalesPersonAutoId
	left join ProductPricingInPriceLevel as pppl on pppl.ProductAutoId=t1.ProductAutoId and pppl.UnitAutoId=t1.PackingAutoId and pppl.PriceLevelAutoId=2838
) as t
inner join ProductMaster as pm on pm.ProductId = t.ProductId
inner join CategoryMaster as cm on cm.AutoId = pm.CategoryAutoId
inner join SubCategoryMaster  as scm on scm.AutoId=pm.SubcategoryAutoId
group by  cm.CategoryName,scm.SubcategoryName
order by  cm.CategoryName,scm.SubcategoryName

use [psmpa.a1whm.com]
select cm.CategoryName,scm.SubcategoryName,
sum(Net_Sale_Rev) as TotalSales into ##tempPA
from
(
	select em.FirstName,t1.ProductId,t1.ProductName,t1.UnitType,t1.Qty as [Pc per unit],
	isnull(pppl.CustomPrice,0) as CustomPrice,
	convert(decimal(18,2),(t1.QtyDel/t1.Qty)) as [Sold_Default_Qty],
	t1.QtyDel as [Sold_Pc],
	t1.NetPrice as [Sale Rev],
	convert(decimal(18,2),((t1.QtyDel-isnull(t2.QtyDel,0))/t1.Qty)) as [Net_Sold_Default_Qty],
	(t1.QtyDel-isnull(t2.QtyDel,0)) as [Net_Sold_Pc],
	(t1.NetPrice - isnull(t2.NetPrice,0)) as Net_Sale_Rev,
	t1.Del_CostPrice -isnull(((case when t1.QtyDel != 0 then (t1.Del_CostPrice/t1.QtyDel) else 0 end)*t2.QtyDel),0) as costP
	from (
		select om.SalesPersonAutoId, pd.ProductAutoId, pm.ProductId, pm.ProductName, pm.PackingAutoId, um.UnitType, pd.Qty,
		sum(doi.QtyDel) as QtyDel, sum(doi.NetPrice) as NetPrice, sum((doi.Del_CostPrice/doi.QtyPerUnit)*doi.QtyDel) as Del_CostPrice
		from OrderMaster as om
		inner join Delivered_Order_Items as doi on doi.OrderAutoId=om.AutoId
		inner join ProductMaster as pm on pm.AutoId=doi.ProductAutoId
		inner join PackingDetails as pd on pd.ProductAutoId=pm.AutoId and pd.UnitType=pm.PackingAutoId
		inner join UnitMaster as um on um.AutoId=pm.PackingAutoId
		where convert(date,OrderDate) between  @orderfrom and @todate and om.Status=11
		group by om.SalesPersonAutoId, pm.ProductId, pm.ProductName, um.UnitType, pd.Qty,pd.ProductAutoId, pm.PackingAutoId
	) as t1
	left join (
		select cmm.CreatedBy, pd.ProductAutoId, pm.ProductId, pm.ProductName, pm.PackingAutoId, um.UnitType, pd.Qty,
		sum(cim.TotalPeice) as QtyDel, sum(cim.NetAmount) as NetPrice
		from CreditMemoMaster as cmm
		inner join CreditItemMaster as cim on cim.CreditAutoId=cmm.CreditAutoId
		inner join ProductMaster as pm on pm.AutoId=cim.ProductAutoId
		inner join PackingDetails as pd on pd.ProductAutoId=pm.AutoId and pd.UnitType=pm.PackingAutoId
		inner join UnitMaster as um on um.AutoId=pm.PackingAutoId
		where convert(date,cmm.ApprovalDate) between  @orderfrom and @todate and cmm.Status=3
		group by cmm.CreatedBy, pm.ProductId, pm.ProductName, um.UnitType, pd.Qty,pd.ProductAutoId, pm.PackingAutoId
	) as t2 on t1.SalesPersonAutoId=t2.CreatedBy and t1.ProductAutoId=t2.ProductAutoId and t1.PackingAutoId=t2.PackingAutoId
	inner join EmployeeMaster as em on em.AutoId=t1.SalesPersonAutoId
	left join ProductPricingInPriceLevel as pppl on pppl.ProductAutoId=t1.ProductAutoId and pppl.UnitAutoId=t1.PackingAutoId and pppl.PriceLevelAutoId=2838
) as t
inner join ProductMaster as pm on pm.ProductId = t.ProductId
inner join CategoryMaster as cm on cm.AutoId = pm.CategoryAutoId
inner join SubCategoryMaster  as scm on scm.AutoId=pm.SubcategoryAutoId
group by  cm.CategoryName,scm.SubcategoryName
order by  cm.CategoryName,scm.SubcategoryName

use [psmwpa.a1whm.com]
select cm.CategoryName,scm.SubcategoryName,
sum(Net_Sale_Rev) as TotalSales into ##tempWPA
from
(
	select em.FirstName,t1.ProductId,t1.ProductName,t1.UnitType,t1.Qty as [Pc per unit],
	isnull(pppl.CustomPrice,0) as CustomPrice,
	convert(decimal(18,2),(t1.QtyDel/t1.Qty)) as [Sold_Default_Qty],
	t1.QtyDel as [Sold_Pc],
	t1.NetPrice as [Sale Rev],
	convert(decimal(18,2),((t1.QtyDel-isnull(t2.QtyDel,0))/t1.Qty)) as [Net_Sold_Default_Qty],
	(t1.QtyDel-isnull(t2.QtyDel,0)) as [Net_Sold_Pc],
	(t1.NetPrice - isnull(t2.NetPrice,0)) as Net_Sale_Rev,
	t1.Del_CostPrice -isnull(((case when t1.QtyDel != 0 then (t1.Del_CostPrice/t1.QtyDel) else 0 end)*t2.QtyDel),0) as costP
	from (
		select om.SalesPersonAutoId, pd.ProductAutoId, pm.ProductId, pm.ProductName, pm.PackingAutoId, um.UnitType, pd.Qty,
		sum(doi.QtyDel) as QtyDel, sum(doi.NetPrice) as NetPrice, sum((doi.Del_CostPrice/doi.QtyPerUnit)*doi.QtyDel) as Del_CostPrice
		from OrderMaster as om
		inner join Delivered_Order_Items as doi on doi.OrderAutoId=om.AutoId
		inner join ProductMaster as pm on pm.AutoId=doi.ProductAutoId
		inner join PackingDetails as pd on pd.ProductAutoId=pm.AutoId and pd.UnitType=pm.PackingAutoId
		inner join UnitMaster as um on um.AutoId=pm.PackingAutoId
		where convert(date,OrderDate) between  @orderfrom and @todate and om.Status=11
		group by om.SalesPersonAutoId, pm.ProductId, pm.ProductName, um.UnitType, pd.Qty,pd.ProductAutoId, pm.PackingAutoId
	) as t1
	left join (
		select cmm.CreatedBy, pd.ProductAutoId, pm.ProductId, pm.ProductName, pm.PackingAutoId, um.UnitType, pd.Qty,
		sum(cim.TotalPeice) as QtyDel, sum(cim.NetAmount) as NetPrice
		from CreditMemoMaster as cmm
		inner join CreditItemMaster as cim on cim.CreditAutoId=cmm.CreditAutoId
		inner join ProductMaster as pm on pm.AutoId=cim.ProductAutoId
		inner join PackingDetails as pd on pd.ProductAutoId=pm.AutoId and pd.UnitType=pm.PackingAutoId
		inner join UnitMaster as um on um.AutoId=pm.PackingAutoId
		where convert(date,cmm.ApprovalDate) between  @orderfrom and @todate and cmm.Status=3
		group by cmm.CreatedBy, pm.ProductId, pm.ProductName, um.UnitType, pd.Qty,pd.ProductAutoId, pm.PackingAutoId
	) as t2 on t1.SalesPersonAutoId=t2.CreatedBy and t1.ProductAutoId=t2.ProductAutoId and t1.PackingAutoId=t2.PackingAutoId
	inner join EmployeeMaster as em on em.AutoId=t1.SalesPersonAutoId
	left join ProductPricingInPriceLevel as pppl on pppl.ProductAutoId=t1.ProductAutoId and pppl.UnitAutoId=t1.PackingAutoId and pppl.PriceLevelAutoId=2838
) as t
inner join ProductMaster as pm on pm.ProductId = t.ProductId
inner join CategoryMaster as cm on cm.AutoId = pm.CategoryAutoId
inner join SubCategoryMaster  as scm on scm.AutoId=pm.SubcategoryAutoId
group by  cm.CategoryName,scm.SubcategoryName
order by  cm.CategoryName,scm.SubcategoryName

use [psmnj.a1whm.com]
select cm.CategoryName,scm.SubcategoryName,
sum(Net_Sale_Rev) as TotalSales into ##tempNJ
from
(
	select em.FirstName,t1.ProductId,t1.ProductName,t1.UnitType,t1.Qty as [Pc per unit],
	isnull(pppl.CustomPrice,0) as CustomPrice,
	convert(decimal(18,2),(t1.QtyDel/t1.Qty)) as [Sold_Default_Qty],
	t1.QtyDel as [Sold_Pc],
	t1.NetPrice as [Sale Rev],
	convert(decimal(18,2),((t1.QtyDel-isnull(t2.QtyDel,0))/t1.Qty)) as [Net_Sold_Default_Qty],
	(t1.QtyDel-isnull(t2.QtyDel,0)) as [Net_Sold_Pc],
	(t1.NetPrice - isnull(t2.NetPrice,0)) as Net_Sale_Rev,
	t1.Del_CostPrice -isnull(((case when t1.QtyDel != 0 then (t1.Del_CostPrice/t1.QtyDel) else 0 end)*t2.QtyDel),0) as costP
	from (
		select om.SalesPersonAutoId, pd.ProductAutoId, pm.ProductId, pm.ProductName, pm.PackingAutoId, um.UnitType, pd.Qty,
		sum(doi.QtyDel) as QtyDel, sum(doi.NetPrice) as NetPrice, sum((doi.Del_CostPrice/doi.QtyPerUnit)*doi.QtyDel) as Del_CostPrice
		from OrderMaster as om
		inner join Delivered_Order_Items as doi on doi.OrderAutoId=om.AutoId
		inner join ProductMaster as pm on pm.AutoId=doi.ProductAutoId
		inner join PackingDetails as pd on pd.ProductAutoId=pm.AutoId and pd.UnitType=pm.PackingAutoId
		inner join UnitMaster as um on um.AutoId=pm.PackingAutoId
		where convert(date,OrderDate) between  @orderfrom and @todate and om.Status=11
		group by om.SalesPersonAutoId, pm.ProductId, pm.ProductName, um.UnitType, pd.Qty,pd.ProductAutoId, pm.PackingAutoId
	) as t1
	left join (
		select cmm.CreatedBy, pd.ProductAutoId, pm.ProductId, pm.ProductName, pm.PackingAutoId, um.UnitType, pd.Qty,
		sum(cim.TotalPeice) as QtyDel, sum(cim.NetAmount) as NetPrice
		from CreditMemoMaster as cmm
		inner join CreditItemMaster as cim on cim.CreditAutoId=cmm.CreditAutoId
		inner join ProductMaster as pm on pm.AutoId=cim.ProductAutoId
		inner join PackingDetails as pd on pd.ProductAutoId=pm.AutoId and pd.UnitType=pm.PackingAutoId
		inner join UnitMaster as um on um.AutoId=pm.PackingAutoId
		where convert(date,cmm.ApprovalDate) between  @orderfrom and @todate and cmm.Status=3
		group by cmm.CreatedBy, pm.ProductId, pm.ProductName, um.UnitType, pd.Qty,pd.ProductAutoId, pm.PackingAutoId
	) as t2 on t1.SalesPersonAutoId=t2.CreatedBy and t1.ProductAutoId=t2.ProductAutoId and t1.PackingAutoId=t2.PackingAutoId
	inner join EmployeeMaster as em on em.AutoId=t1.SalesPersonAutoId
	left join ProductPricingInPriceLevel as pppl on pppl.ProductAutoId=t1.ProductAutoId and pppl.UnitAutoId=t1.PackingAutoId and pppl.PriceLevelAutoId=2838
) as t
inner join ProductMaster as pm on pm.ProductId = t.ProductId
inner join CategoryMaster as cm on cm.AutoId = pm.CategoryAutoId
inner join SubCategoryMaster  as scm on scm.AutoId=pm.SubcategoryAutoId
group by  cm.CategoryName,scm.SubcategoryName
order by  cm.CategoryName,scm.SubcategoryName

select cm.CategoryName,scm.SubcategoryName,isnull(nj.TotalSales,0) as NJTotalSales,isnull(ct.TotalSales,0) as CTTotalSales,
isnull(npa.TotalSales,0) as NPATotalSales,isnull(pa.TotalSales,0) as PATotalSales,isnull(wpa.TotalSales,0) as WPATotalSales from 
CategoryMaster as cm 
inner join SubCategoryMaster as scm on cm.AutoId = scm.CategoryAutoId 
left join ##tempNJ as nj on cm.CategoryName=nj.CategoryName and scm.SubcategoryName=nj.SubcategoryName
Left join ##tempCT as ct on cm.CategoryName=ct.CategoryName and scm.SubcategoryName=ct.SubcategoryName
Left join ##tempNPA as npa on cm.CategoryName=npa.CategoryName and scm.SubcategoryName=npa.SubcategoryName
Left join ##tempPA as pa on cm.CategoryName=pa.CategoryName and scm.SubcategoryName=pa.SubcategoryName
Left join ##tempWPA as wpa on cm.CategoryName=wpa.CategoryName and scm.SubcategoryName=wpa.SubcategoryName
order by cm.CategoryName, scm.SubcategoryName


drop table ##tempNJ, ##tempCT, ##tempNPA, ##tempPA, ##tempWPA