select FirstName,ProductId,ProductName,um.UnitType,isnull(ppl.CustomPrice,0.00) as CustomPrice,Qty as [pc per unit],isFreeItem,IsExchange,
convert(decimal(18,2),(convert(decimal(18,2),QtyDel)/Qty)) as qty_in_def_Unit,QtyDel,
convert(decimal(18,2),netCost)netCost,NetPrice from(
select emp.FirstName,pm.AutoId as ProductAutoId,pm.ProductId,pm.ProductName,pm.PackingAutoId,doi.isFreeItem,doi.IsExchange,
sum(doi.QtyDel) QtyDel,sum((doi.Del_CostPrice/doi.QtyPerUnit)*doi.QtyDel) as netCost,sum(doi.NetPrice) as NetPrice,pd.Qty
from EmployeeMaster as emp
inner join CustomerMaster as cm on emp.AutoId=cm.SalesPersonAutoId
inner join OrderMaster as om on om.CustomerAutoId=cm.AutoId
inner join Delivered_Order_Items as doi on doi.OrderAutoId=om.AutoId
inner join ProductMaster as pm on doi.ProductAutoId=pm.AutoId
inner join PackingDetails as pd on pd.ProductAutoId=pm.AutoId and pd.UnitType=pm.PackingAutoId
where emp.AutoId in(1205,1206,1207)
and om.Status = 11
and doi.QtyDel>0
group by emp.FirstName,pm.ProductId,pm.ProductName,pm.PackingAutoId,doi.isFreeItem,doi.IsExchange,pd.Qty,pm.AutoId
) as t
inner join UnitMaster as um on um.AutoId=t.PackingAutoId
left join ProductPricingInPriceLevel as ppl on ppl.ProductAutoId=t.ProductAutoId and ppl.PriceLevelAutoId=2838 and ppl.UnitAutoId=t.PackingAutoId
