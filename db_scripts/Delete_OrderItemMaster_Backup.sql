USE [psmnj.a1whm.com]
GO

/****** Object:  Table [dbo].[Delete_OrderItemMaster_Backup]    Script Date: 04/14/2020 22:12:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Delete_OrderItemMaster_Backup](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[OrderAutoId] [int] NULL,
	[ProductAutoId] [int] NULL,
	[UnitTypeAutoId] [int] NULL,
	[QtyPerUnit] [int] NULL,
	[UnitPrice] [decimal](8, 2) NULL,
	[RequiredQty] [int] NULL,
	[SRP] [decimal](8, 2) NULL,
	[GP] [decimal](8, 2) NULL,
	[Tax] [int] NULL,
	[Status] [int] NULL,
	[Barcode] [varchar](50) NULL,
	[QtyShip] [int] NULL,
	[RemainQty] [int] NULL,
	[IsExchange] [int] NULL,
	[TaxValue] [decimal](10, 2) NULL,
	[UnitMLQty] [decimal](10, 2) NULL,
	[isFreeItem] [int] NULL,
	[Item_TotalMLTax]  AS ([dbo].[FN_Order_MLTaxItem]([AutoId],[OrderAutoId])),
	[ManualScan] [bit] NULL,
	[NetPrice]  AS ([dbo].[FN_Order_Netmount]([AutoId],[OrderAutoId])),
	[TotalPieces]  AS ([dbo].[FN_Order_TotalPieces]([AutoId],[OrderAutoId])),
	[TotalMLQty]  AS ([dbo].[FN_Order_IsMLTotalQty]([AutoId],[OrderAutoId])),
	[Weight_Oz] [decimal](15, 2) NULL,
	[Weight_Oz_TotalQty]  AS ([dbo].[FN_Order_Weight_Oz_Qty]([autoid])),
	[Original_UnitType] [int] NULL,
	[AddOnQty] [int] NULL,
 CONSTRAINT [PK_Delete_OrderItemMaster_Backup] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Delete_OrderItemMaster_Backup] ADD  CONSTRAINT [DF_Delete_OrderItemMaster_Backup_SRP]  DEFAULT ((0)) FOR [SRP]
GO

ALTER TABLE [dbo].[Delete_OrderItemMaster_Backup] ADD  CONSTRAINT [DF_Delete_OrderItemMaster_Backup_isFreeItem]  DEFAULT ((0)) FOR [isFreeItem]
GO

ALTER TABLE [dbo].[Delete_OrderItemMaster_Backup] ADD  CONSTRAINT [DF_Delete_OrderItemMaster_Backup_ManualScan]  DEFAULT ((0)) FOR [ManualScan]
GO

ALTER TABLE [dbo].[Delete_OrderItemMaster_Backup] ADD  CONSTRAINT [DF_Delete_OrderItemMaster_Backup_Weight_Oz]  DEFAULT ((0.00)) FOR [Weight_Oz]
GO

ALTER TABLE [dbo].[Delete_OrderItemMaster_Backup] ADD  CONSTRAINT [DF_Delete_OrderItemMaster_Backup_AddOnQty]  DEFAULT ((0)) FOR [AddOnQty]
GO


