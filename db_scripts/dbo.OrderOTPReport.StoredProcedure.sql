USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[OrderOTPReport]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrderOTPReport]              
                    
@Opcode INT=NULL,   
@DateFrom date =NULL,  
@DateTo date =NULL,  
@PageIndex INT=1,  
@PageSize INT=10,  
@RecordCount INT=null,  
@isException bit out,  
@exceptionMessage varchar(max) out             
AS                  
BEGIN   
 BEGIN TRY  
  Set @isException=0  
  Set @exceptionMessage='Success'         
  
if @Opcode = 41  
begin 
	
	select convert(date,om.OrderDate) as [Invoice Date], om.OrderNo as [Invoice Number],bm.BrandName as [Name of Common Carrier], 
	cm.CustomerName as [Seller Name],ba.Address, ba.City, ba.Zipcode, st.StateName,
	cm.OPTLicence as [OTP License Number],bm.BrandName as [Brand Family],scm.SubcategoryName as [Product Description], 
	Sum(doi.QtyDel) as [Quantity of Item Sold],
	sum(doi.NetPrice) as [Purchase Price],
	om.TotalTax  as [Total Tax Collected],
	'Yes' as [Tax Collected/Paid Y/N]
	INTO #TEMP
	from Delivered_Order_Items as doi
	inner join OrderMaster as om on doi.OrderAutoId = om.AutoId and doi.Tax=1
	inner join ProductMaster as pm on doi.ProductAutoId = pm.AutoId
	inner join CustomerMaster as cm on om.CustomerAutoId = cm.AutoId
	inner join SubCategoryMaster as scm on pm.SubcategoryAutoId = scm.AutoId
	inner join BrandMaster as bm on pm.BrandAutoId = bm.AutoId
	inner join BillingAddress as ba on cm.DefaultBillAdd = ba.AutoId
	inner join State as st on st.AutoId = ba.State
	where om.TotalTax > 0 and convert(date,om.OrderDate) between @DateFrom and @DateTo
	group by convert(date,om.OrderDate),om.OrderNo,bm.BrandName,cm.CustomerName,ba.Address, ba.City, ba.Zipcode, st.StateName,
	cm.OPTLicence,bm.BrandName,scm.SubcategoryName,om.TotalTax
	order by om.OrderNo


	SELECT DISTINCT  [Invoice Date], [Invoice Number],
	STUFF((SELECT DISTINCT  '/' + [Name of Common Carrier] FROM #TEMP AS T2 WHERE T1. [Invoice Number]=T2.[Invoice Number]	FOR XML PATH('')), 1, 1,'') [Name of Common Carrier], 
	[Seller Name],Address, City, Zipcode, StateName,
	[OTP License Number],
	STUFF((SELECT DISTINCT '/' + [Brand Family] FROM #TEMP AS T2 WHERE T1. [Invoice Number]=T2.[Invoice Number]	FOR XML PATH('')), 1, 1,'') [Brand Family],
	STUFF((SELECT DISTINCT '/' + [Product Description] FROM #TEMP AS T2 WHERE T1. [Invoice Number]=T2.[Invoice Number]	FOR XML PATH('')), 1, 1,'') [Product Description], 
	[Quantity of Item Sold], [Purchase Price], [Total Tax Collected], [Tax Collected/Paid Y/N]
	INTO #TEMP1 FROM #TEMP AS T1


	SELECT   ROW_NUMBER() OVER(ORDER BY [Invoice Date] desc) AS RowNumber,[Invoice Date], [Invoice Number],[Name of Common Carrier], [Seller Name],Address, City, Zipcode, StateName,[OTP License Number],[Brand Family],
	REPLACE([Product Description],'&AMP;','&')[Product Description], 
	sum([Quantity of Item Sold]) as [Quantity of Item Sold], sum([Purchase Price]) as [Purchase Price], [Total Tax Collected], [Tax Collected/Paid Y/N]
	FROM #TEMP1 AS T1
	group by 
	[Invoice Date], [Invoice Number],[Name of Common Carrier], [Seller Name],Address, City, Zipcode, StateName,[OTP License Number],[Brand Family],
	REPLACE([Product Description],'&AMP;','&'),  [Total Tax Collected], [Tax Collected/Paid Y/N]



	
    
	SELECT COUNT(RowNumber) AS RecordCount, @PageSize AS PageSize, @PageIndex AS PageIndex FROM #TEMP1  
      
	SELECT * FROM #TEMP1  
	WHERE @PageSize = 0 or RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1  
	
	
	DROP TABLE #TEMP,#TEMP1 
end                 
END TRY  
 BEGIN CATCH  
  Set @isException=1  
  Set @exceptionMessage=ERROR_MESSAGE()  
 END CATCH             
END   
GO
