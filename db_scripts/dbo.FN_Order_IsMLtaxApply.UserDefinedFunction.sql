USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Order_IsMLtaxApply]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION  [dbo].[FN_Order_IsMLtaxApply]  
(   
  @orderAutoId int
   
)  
RETURNS INT  
AS  
BEGIN  
 DECLARE @Isapply int=0  
 if exists(select * from MLTaxMaster where TaxState in (select State from  BillingAddress where AutoId in (select BillAddrAutoId from OrderMaster where AutoId=@orderAutoId)))  
 begin  
 set @Isapply=1   
 END 
	DECLARE @isMLManualyApply int=ISNULL((select isMLManualyApply from OrderMaster where AutoId=@orderAutoId),1) 
	if @isMLManualyApply=0
	begin
		set @Isapply=0
	end
 RETURN @Isapply  
END  
GO
