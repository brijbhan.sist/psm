USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Delivered_Weight_Oz_Qty]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION  [dbo].[FN_Delivered_Weight_Oz_Qty]
(
	 @AutoId int
)
RETURNS decimal(10,2)
AS
BEGIN
	DECLARE @Weight_Oz decimal(10,2)	
	SET @Weight_Oz=isnull((select ISNULL(Del_Weight_Oz ,0)*ISNULL(QtyDel,0) from [Delivered_Order_Items]  where AutoId=@AutoId),0.00)
	RETURN @Weight_Oz
END
GO
