USE [psmnj.a1whm.com]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProductStockUpdateLog]') AND type in (N'U'))
DROP TABLE [dbo].[ProductStockUpdateLog]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ProductStockUpdateLog](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NULL,
	[OledStock] [int] NULL,
	[NewStock] [int] NULL,
	[UserAutoId] [int] NULL,
	[DateTime] [datetime] NULL,
	[ReferenceId] [int] NULL,
	[ActionRemark] [varchar](250) NULL,
	[ReferenceType] [varchar](50) NULL,
	[AffectedStock]  AS (isnull(isnull([NewStock],(0))-isnull([oledStock],(0)),(0))),
	[DefaultAffectedStock]  AS ([dbo].[FN_CalculateDefaultAffectedStock]([AutoId])),
 CONSTRAINT [PK_ProductStockUpdateLog] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


