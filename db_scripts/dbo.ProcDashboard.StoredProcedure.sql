ALTER PROCEDURE [dbo].[ProcDashboard]
@Opcode INT=NULL,
@EmpAutoId INT=NULL,

@PageIndex INT = 1,                                                                                                                  
@PageSize INT = 10,                                                                                                                                                      
@RecordCount INT =null, 
@isException bit out,
@exceptionMessage varchar(max) out
AS
BEGIN	
BEGIN TRY
Set @isException=0
Set @exceptionMessage='Success'

If @Opcode=401	
BEGIN
	
	Select top 25 PM.ProductId,ProductName,sum(DO.QtyDel) as Stock from ProductMaster as PM
	inner join Delivered_Order_Items as DO on
	PM.AutoId=DO.ProductAutoId
	Where PM.ProductStatus=1 group by PM.ProductId,ProductName,DO.QtyShip
	order by sum(DO.QtyDel) desc

	Select * from CompanyDetails
END
If @Opcode=402	
BEGIN
	SELECT ROW_NUMBER() OVER(ORDER BY OrderDate,AmtDue) AS RowNumber, * INTO #Results from                      
	( 
	SELECT OM.SalesPersonAutoId,OM.[AutoId],[OrderNo],CONVERT(VARCHAR(20),[OrderDate],101) AS OrderDate,DO.[AmtDue],DO.PayableAmount as GrandTotal,DO.[AmtPaid],
	CM.CustomerId,CM.CustomerName FROM [dbo].[OrderMaster] As OM
	INNER JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId]
	INNER JOIN [dbo].[CustomerMaster] AS CM ON OM.CustomerAutoId=CM.AutoId
	WHERE cm.SalesPersonAutoId = @EmpAutoId AND 
	(om.[Status] = 11) AND DO.[AmtDue] > 0.00
	--order by [OrderDate],DO.[AmtDue] desc
	) as t order by OrderDate,AmtDue

	SELECT COUNT(RowNumber) AS RecordCount,case when @PageSize=0 then COUNT(OrderDate) else @PageSize end AS PageSize,
	@PageIndex as PageIndex FROM #Results           
                                    
	SELECT * FROM #Results                                    
	WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) *
	@PageSize + 1) + @PageSize) - 1))
END
If @Opcode=403	
BEGIN
	SELECT CustomerName,CT.CustomerType,(format(CreatedOn,'MM/dd/yyyy')) as CreatedDate  from DraftCustomerMaster as DCM
	inner join CustomerType as CT on CT.AutoId=DCM.CustomerType	
END
IF @opcode=404
Begin
    select Row_Number() over (order by PM.AutoId) as rownumber,ProductId,ProductName,
	(SELECT top 1 ImageUrl FROM ProductImageUrl)+Case when ISNUll(PM.ThumbnailImageUrl,'')=''
	then 'productThumbnailImage/100_100_Thumbnail_default_pic.png' else PM.ThumbnailImageUrl end
	as ImageUrl,ISNULL(FORMAT(PM.CreateDate,'MM/dd/yyyy hh:mm tt'),'') as CreateDate,
	ISNULL(Format(ModifiedDate,'MM/dd/yyyy hh:mm tt'),'') as LastUpdateDate,	
	ISNUll((Select FirstName+' '+LastName FROM EmployeeMaster where AutoId=pm.CreateBy),'') as CreateBy
	from ProductMaster as pm where pm.AutoId not in
	(
	Select pd.productAutoId from PackingDetails pd where pd.ProductAutoId=pm.AutoId
	and pd.UnitType=pm.PackingAutoId
	) order by CreateDate desc
EnD
END TRY
BEGIN CATCH
Set @isException=1
Set @exceptionMessage=ERROR_MESSAGE()
END CATCH
END	
GO
