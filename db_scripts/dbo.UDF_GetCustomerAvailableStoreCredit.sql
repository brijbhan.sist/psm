Alter table CustomerCreditMaster drop column AvailStoreCredit

GO
ALTER function DBO.UDF_GetCustomerAvailableStoreCredit
(
   @CustomerAutoId INT
)
returns decimal(18,2)
AS
BEGIN
     Declare @StoreCredit decimal(18,2)
     SET @StoreCredit=ISNULL((SELECT sum(Amount) FROM tbl_Custumor_StoreCreditLog where CustomerAutoId=@CustomerAutoId),0)
	 SET @StoreCredit=@StoreCredit*(-1)   
	 return  @StoreCredit
END

GO

Alter table CustomerCreditMaster add AvailStoreCredit as (DBO.UDF_GetCustomerAvailableStoreCredit(CustomerCreditMaster.CustomerAutoId))
