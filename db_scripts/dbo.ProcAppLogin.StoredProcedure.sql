USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcAppLogin]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE ProcEDURE [dbo].[ProcAppLogin]    
@Opcode int=NULL,    
@EmpAutoId int=null,      
@UserName varchar(100)=null,    
@Password varchar(max)=null,    
@NewPassword varchar(max)=null,    
@DeviceId varchar(max)=null,  
@isException bit out,    
@exceptionMessage varchar(max) out    
AS    
BEGIN    
 BEGIN TRY    
  SET @exceptionMessage= 'Success'    
  SET @isException=0     
     
  IF @Opcode=21     
  BEGIN    
   IF EXISTS(SELECT [Password] from EmployeeMaster where AutoId=@EmpAutoId and CONVERT(varchar(100),DecryptByPassphrase('WHM',[Password]))=@Password )    
   BEGIN    
    UPDATE EmployeeMaster set Password=EncryptByPassPhrase('WHM',@NewPassword) where AutoId=@EmpAutoId   
   END    
   else    
   BEGIN    
    SET @isException=1    
    SET @exceptionMessage= 'Old Password Not Match !!!'    
   END    
  END    
     
  IF @Opcode=41     
  BEGIN    
	if Exists(select 1 from CompanyDetails where CONVERT(date,SubscriptionExpiryDate)>=CONVERT(date,GETDATE()))
	BEGIN

	   SELECT 'success' as response,EM.AutoId,EM.[EmpType] As EmpTypeNo,ETM.TypeName AS [EmpType],[FirstName]+' '+[LastName] AS Name,[UserName]    
	   into #ResultTemp from [dbo].[EmployeeMaster] as EM INNER JOIN [dbo].[EmployeeTypeMaster] AS ETM on EM.EmpType=ETM.AutoId    
	   WHERE EM.[EmpType] in (2,5,9,11) and [UserName]=@UserName 
	   and CONVERT(varchar(100),DecryptByPassphrase('WHM',[Password]))=@Password 
	   and Status=1 and ISNULL(IsAppLogin,0)=1
	   select * from #ResultTemp  
	   select rm.AutoId,SalesPersonAutoId,RouteId,RouteName from RouteMaster as rm   
	   inner join #ResultTemp as temp on temp.AutoId=rm.SalesPersonAutoId  
  
           
	   if exists(SELECT * from [dbo].[EmployeeMaster] as EM  WHERE [UserName]=@UserName        
	   and CONVERT(varchar(100),DecryptByPassphrase('WHM',[Password]))=@Password and Status=1)        
	   begin        
		set @EmpAutoId=(SELECT top 1 AutoId from [dbo].[EmployeeMaster] as EM  WHERE [UserName]=@UserName        
		and CONVERT(varchar(100),DecryptByPassphrase('WHM',[Password]))=@Password        
	   and Status=1)        
		insert into ipaddress(EmpAutoId,IPAddress,LoginDate,Remarks)         
		values(@EmpAutoId,@DeviceId,GETDATE(),'login successfully.')        
	   end 
	END
	ELSE
	BEGIN
	    SET @isException=1    
		SET @exceptionMessage= 'Your subscription expired on' +' '+ (select FORMAT(SubscriptionExpiryDate,'dd MMMM yyyy')+'.' from CompanyDetails)    
	END    
  END    
 END TRY    
 BEGIN CATCH    
  SET @isException=1    
  SET @exceptionMessage= ERROR_MESSAGE()    
 END CATCH    
END
GO
