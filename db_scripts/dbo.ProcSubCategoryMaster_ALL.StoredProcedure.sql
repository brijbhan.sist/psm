ALTER PROCEDURE [dbo].[ProcSubCategoryMaster_ALL]    
@SubCategoryId  VARCHAR(50)=Null    
AS    
BEGIN    
  --------------------------------INSERT  AND UPDATE FOR PSMCT DB---------------------------------------  
  IF EXISTS(SELECT * FROM [psmct.a1whm.com].[dbo].[SubCategoryMaster] WHERE SubcategoryId=@SubCategoryId)    
  BEGIN    
   UPDATE [psmct.a1whm.com].[dbo].[SubCategoryMaster]     
   SET SubcategoryName = (SELECT SubcategoryName FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),    
   Description = (SELECT Description FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),     
   Status = (SELECT Status FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),  
   CategoryAutoId=(SELECT C.AutoId FROM [psmct.a1whm.com].[dbo].[CategoryMaster] AS C WHERE C.CategoryId=(  
   SELECT CM.CategoryId FROM SubCategoryMaster AS SCM   
   INNER JOIN CategoryMaster AS CM ON CM.AutoId=SCM.CategoryAutoId  WHERE SCM.SubcategoryId=@SubCategoryId  
   )),  
   SeqNo=(SELECT SeqNo FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),  
   IsShow=(SELECT IsShow FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),
   SubcategoryTax=(SELECT SubcategoryTax FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),
   UpdateDate=GETDATE()  
   WHERE SubcategoryId=@SubCategoryId    
  END    
  ELSE    
  BEGIN    
   INSERT INTO [psmct.a1whm.com].[dbo].[SubCategoryMaster]([SubcategoryId], SubcategoryName,Description,Status,CreatedDate,UpdateDate,CategoryAutoId,  
   SeqNo,IsShow,SubcategoryTax )    
   SELECT [SubcategoryId], SubcategoryName,SCM.Description,SCM.Status,GETDATE(),GETDATE(),  
   (SELECT C.AutoId FROM [psmCT.a1whm.com].[dbo].[CategoryMaster] AS C WHERE C.CategoryId=CM.CategoryId),  
   scm.SeqNo,scm.IsShow,SubcategoryTax   
   from [SubCategoryMaster] AS SCM   
   INNER JOIN CategoryMaster AS CM ON CM.AutoId=SCM.CategoryAutoId WHERE SubcategoryId=@SubCategoryId    
  END   
  
  --------------------------------INSERT  AND UPDATE FOR PSMPA DB---------------------------------------  
  
  IF EXISTS(SELECT * FROM [psmpa.a1whm.com].[dbo].[SubCategoryMaster] WHERE SubcategoryId=@SubCategoryId)    
  BEGIN    
   UPDATE [psmpa.a1whm.com].[dbo].[SubCategoryMaster]     
   SET SubcategoryName = (SELECT SubcategoryName FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),    
   Description = (SELECT Description FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),     
   Status = (SELECT Status FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId)  ,     
   CategoryAutoId=(SELECT C.AutoId FROM [psmpa.a1whm.com].[dbo].[CategoryMaster] AS C WHERE C.CategoryId=(  
   SELECT CM.CategoryId FROM SubCategoryMaster AS SCM   
   INNER JOIN CategoryMaster AS CM ON CM.AutoId=SCM.CategoryAutoId  WHERE SCM.SubcategoryId=@SubCategoryId  
   )),  
   SeqNo=(SELECT SeqNo FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),  
   IsShow=(SELECT IsShow FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),  
   SubcategoryTax=(SELECT SubcategoryTax FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),
   UpdateDate=GETDATE()  
   WHERE SubcategoryId=@SubCategoryId    
  END    
  ELSE    
  BEGIN    
   INSERT INTO [psmpa.a1whm.com].[dbo].[SubCategoryMaster]([SubcategoryId], SubcategoryName,Description,Status,CreatedDate,UpdateDate,CategoryAutoId,  
   SeqNo,IsShow,SubcategoryTax )    
   SELECT [SubcategoryId], SubcategoryName,SCM.Description,SCM.Status,GETDATE(),GETDATE(),  
   (SELECT C.AutoId FROM [psmpa.a1whm.com].[dbo].[CategoryMaster] AS C WHERE C.CategoryId=CM.CategoryId),  
   scm.SeqNo,scm.IsShow,SubcategoryTax   
   from [SubCategoryMaster] AS SCM   
   INNER JOIN CategoryMaster AS CM ON CM.AutoId=SCM.CategoryAutoId WHERE SubcategoryId=@SubCategoryId    
  END  
  --------------------------------INSERT  AND UPDATE FOR PSMNPA DB---------------------------------------  
  IF EXISTS(SELECT * FROM [psmnpa.a1whm.com].[dbo].[SubCategoryMaster] WHERE SubcategoryId=@SubCategoryId)    
  BEGIN    
   UPDATE [psmnpa.a1whm.com].[dbo].[SubCategoryMaster]     
   SET SubcategoryName = (SELECT SubcategoryName FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),    
   Description = (SELECT Description FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),     
   Status = (SELECT Status FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),  
   CategoryAutoId=(SELECT C.AutoId FROM [psmnpa.a1whm.com].[dbo].[CategoryMaster] AS C WHERE C.CategoryId=(  
   SELECT CM.CategoryId FROM SubCategoryMaster AS SCM   
   INNER JOIN CategoryMaster AS CM ON CM.AutoId=SCM.CategoryAutoId  WHERE SCM.SubcategoryId=@SubCategoryId  
   )),  
   SeqNo=(SELECT SeqNo FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),  
   IsShow=(SELECT IsShow FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),  
   SubcategoryTax=(SELECT SubcategoryTax FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),
   UpdateDate=GETDATE()    
   WHERE SubcategoryId=@SubCategoryId    
  END    
  ELSE    
  BEGIN    
   INSERT INTO [psmnpa.a1whm.com].[dbo].[SubCategoryMaster]([SubcategoryId], SubcategoryName,Description,Status,CreatedDate,UpdateDate,CategoryAutoId,  
   SeqNo,IsShow,SubcategoryTax )    
   SELECT [SubcategoryId], SubcategoryName,SCM.Description,SCM.Status,GETDATE(),GETDATE(),  
   (SELECT C.AutoId FROM [psmnpa.a1whm.com].[dbo].[CategoryMaster] AS C WHERE C.CategoryId=CM.CategoryId),  
   scm.SeqNo,scm.IsShow,SubcategoryTax   
   from [SubCategoryMaster] AS SCM   
   INNER JOIN CategoryMaster AS CM ON CM.AutoId=SCM.CategoryAutoId WHERE SubcategoryId=@SubCategoryId    
  END  
  
  --------------------------------INSERT  AND UPDATE FOR PSMWPA DB---------------------------------------  
  IF EXISTS(SELECT * FROM [psmwpa.a1whm.com].[dbo].[SubCategoryMaster] WHERE SubcategoryId=@SubCategoryId)    
  BEGIN    
   UPDATE [psmwpa.a1whm.com].[dbo].[SubCategoryMaster]     
   SET SubcategoryName = (SELECT SubcategoryName FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),    
   Description = (SELECT Description FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),     
   Status = (SELECT Status FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),  
   CategoryAutoId=(SELECT C.AutoId FROM [psmwpa.a1whm.com].[dbo].[CategoryMaster] AS C WHERE C.CategoryId=(  
   SELECT CM.CategoryId FROM SubCategoryMaster AS SCM   
   INNER JOIN CategoryMaster AS CM ON CM.AutoId=SCM.CategoryAutoId  WHERE SCM.SubcategoryId=@SubCategoryId  
   )),  
   SeqNo=(SELECT SeqNo FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),  
   IsShow=(SELECT IsShow FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),  
   SubcategoryTax=(SELECT SubcategoryTax FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),
   UpdateDate=GETDATE()  
   WHERE SubcategoryId=@SubCategoryId    
  END    
  ELSE    
  BEGIN    
   INSERT INTO [psmwpa.a1whm.com].[dbo].[SubCategoryMaster]([SubcategoryId], SubcategoryName,Description,Status,CreatedDate,UpdateDate,CategoryAutoId,  
   SeqNo,IsShow,SubcategoryTax )    
   SELECT [SubcategoryId], SubcategoryName,SCM.Description,SCM.Status,GETDATE(),GETDATE(),  
   (SELECT C.AutoId FROM [psmwpa.a1whm.com].[dbo].[CategoryMaster] AS C WHERE C.CategoryId=CM.CategoryId),  
   scm.SeqNo,scm.IsShow,SubcategoryTax   
   from [SubCategoryMaster] AS SCM   
   INNER JOIN CategoryMaster AS CM ON CM.AutoId=SCM.CategoryAutoId WHERE SubcategoryId=@SubCategoryId    
  END  
  
   --------------------------------INSERT  AND UPDATE FOR PSMNY DB---------------------------------------  
  IF EXISTS(SELECT * FROM [psmny.a1whm.com].[dbo].[SubCategoryMaster] WHERE SubcategoryId=@SubCategoryId)    
  BEGIN    
   UPDATE [psmny.a1whm.com].[dbo].[SubCategoryMaster]     
   SET SubcategoryName = (SELECT SubcategoryName FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),    
   Description = (SELECT Description FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),     
   Status = (SELECT Status FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),  
   CategoryAutoId=(SELECT C.AutoId FROM [psmny.a1whm.com].[dbo].[CategoryMaster] AS C WHERE C.CategoryId=(  
   SELECT CM.CategoryId FROM SubCategoryMaster AS SCM   
   INNER JOIN CategoryMaster AS CM ON CM.AutoId=SCM.CategoryAutoId  WHERE SCM.SubcategoryId=@SubCategoryId  
   )),  
   SeqNo=(SELECT SeqNo FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),  
   IsShow=(SELECT IsShow FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),  
   SubcategoryTax=(SELECT SubcategoryTax FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),
   UpdateDate=GETDATE()  
   WHERE SubcategoryId=@SubCategoryId    
  END    
  ELSE    
  BEGIN    
   INSERT INTO [psmny.a1whm.com].[dbo].[SubCategoryMaster]([SubcategoryId], SubcategoryName,Description,Status,CreatedDate,UpdateDate,CategoryAutoId,  
   SeqNo,IsShow,SubcategoryTax )    
   SELECT [SubcategoryId], SubcategoryName,SCM.Description,SCM.Status,GETDATE(),GETDATE(),  
   (SELECT C.AutoId FROM [psmny.a1whm.com].[dbo].[CategoryMaster] AS C WHERE C.CategoryId=CM.CategoryId),  
   scm.SeqNo,scm.IsShow,SubcategoryTax   
   from [SubCategoryMaster] AS SCM   
   INNER JOIN CategoryMaster AS CM ON CM.AutoId=SCM.CategoryAutoId WHERE SubcategoryId=@SubCategoryId    
  END  
  --------------------------------INSERT  AND UPDATE FOR EASYWHM DB---------------------------------------  
  IF EXISTS(SELECT * FROM [psmnj.easywhm.com].[dbo].[SubCategoryMaster] WHERE SubcategoryId=@SubCategoryId)    
  BEGIN    
   UPDATE [psmnj.easywhm.com].[dbo].[SubCategoryMaster]     
   SET SubcategoryName = (SELECT SubcategoryName FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),    
   Description = (SELECT Description FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),     
   Status = (SELECT Status FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),  
   CategoryAutoId=(SELECT C.AutoId FROM [psmnj.easywhm.com].[dbo].[CategoryMaster] AS C WHERE C.CategoryId=(  
   SELECT CM.CategoryId FROM SubCategoryMaster AS SCM   
   INNER JOIN CategoryMaster AS CM ON CM.AutoId=SCM.CategoryAutoId  WHERE SCM.SubcategoryId=@SubCategoryId  
   )),  
   SeqNo=(SELECT SeqNo FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),  
   IsShow=(SELECT IsShow FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId), 
   SubcategoryTax=(SELECT SubcategoryTax FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),
   UpdateDate=GETDATE()  
   WHERE SubcategoryId=@SubCategoryId    
  END    
  ELSE    
  BEGIN    
   INSERT INTO [psmnj.easywhm.com].[dbo].[SubCategoryMaster]([SubcategoryId], SubcategoryName,Description,Status,CreatedDate,UpdateDate,CategoryAutoId,  
   SeqNo,IsShow,SubcategoryTax )    
   SELECT [SubcategoryId], SubcategoryName,SCM.Description,SCM.Status,GETDATE(),GETDATE(),  
   (SELECT C.AutoId FROM [psmnj.easywhm.com].[dbo].[CategoryMaster] AS C WHERE C.CategoryId=CM.CategoryId),  
   scm.SeqNo,scm.IsShow,SubcategoryTax   
   from [SubCategoryMaster] AS SCM   
   INNER JOIN CategoryMaster AS CM ON CM.AutoId=SCM.CategoryAutoId WHERE SubcategoryId=@SubCategoryId    
  END  
  
  IF EXISTS(SELECT * FROM [psmct.easywhm.com].[dbo].[SubCategoryMaster] WHERE SubcategoryId=@SubCategoryId)    
  BEGIN    
   UPDATE [psmct.easywhm.com].[dbo].[SubCategoryMaster]     
   SET SubcategoryName = (SELECT SubcategoryName FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),    
   Description = (SELECT Description FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),     
   Status = (SELECT Status FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),  
   CategoryAutoId=(SELECT C.AutoId FROM [psmct.easywhm.com].[dbo].[CategoryMaster] AS C WHERE C.CategoryId=(  
   SELECT CM.CategoryId FROM SubCategoryMaster AS SCM   
   INNER JOIN CategoryMaster AS CM ON CM.AutoId=SCM.CategoryAutoId  WHERE SCM.SubcategoryId=@SubCategoryId  
   )),  
   SeqNo=(SELECT SeqNo FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),  
   IsShow=(SELECT IsShow FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId), 
   SubcategoryTax=(SELECT SubcategoryTax FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),
   UpdateDate=GETDATE()  
   WHERE SubcategoryId=@SubCategoryId    
  END    
  ELSE    
  BEGIN    
   INSERT INTO [psmct.easywhm.com].[dbo].[SubCategoryMaster]([SubcategoryId], SubcategoryName,Description,Status,CreatedDate,UpdateDate,CategoryAutoId,  
   SeqNo,IsShow,SubcategoryTax )    
   SELECT [SubcategoryId], SubcategoryName,SCM.Description,SCM.Status,GETDATE(),GETDATE(),  
   (SELECT C.AutoId FROM [psmct.easywhm.com].[dbo].[CategoryMaster] AS C WHERE C.CategoryId=CM.CategoryId),  
   scm.SeqNo,scm.IsShow,SubcategoryTax   
   from [SubCategoryMaster] AS SCM   
   INNER JOIN CategoryMaster AS CM ON CM.AutoId=SCM.CategoryAutoId WHERE SubcategoryId=@SubCategoryId    
  END  
  
  IF EXISTS(SELECT * FROM [psmnpa.easywhm.com].[dbo].[SubCategoryMaster] WHERE SubcategoryId=@SubCategoryId)    
  BEGIN    
   UPDATE [psmnpa.easywhm.com].[dbo].[SubCategoryMaster]     
   SET SubcategoryName = (SELECT SubcategoryName FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),    
   Description = (SELECT Description FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),     
   Status = (SELECT Status FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),  
   CategoryAutoId=(SELECT C.AutoId FROM [psmnpa.easywhm.com].[dbo].[CategoryMaster] AS C WHERE C.CategoryId=(  
   SELECT CM.CategoryId FROM SubCategoryMaster AS SCM   
   INNER JOIN CategoryMaster AS CM ON CM.AutoId=SCM.CategoryAutoId  WHERE SCM.SubcategoryId=@SubCategoryId  
   )),  
   SeqNo=(SELECT SeqNo FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),  
   IsShow=(SELECT IsShow FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),  
   SubcategoryTax=(SELECT SubcategoryTax FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),
   UpdateDate=GETDATE()  
   WHERE SubcategoryId=@SubCategoryId    
  END    
  ELSE    
  BEGIN    
   INSERT INTO [psmnpa.easywhm.com].[dbo].[SubCategoryMaster]([SubcategoryId], SubcategoryName,Description,Status,CreatedDate,UpdateDate,CategoryAutoId,  
   SeqNo,IsShow,SubcategoryTax )    
   SELECT [SubcategoryId], SubcategoryName,SCM.Description,SCM.Status,GETDATE(),GETDATE(),  
   (SELECT C.AutoId FROM [psmnpa.easywhm.com].[dbo].[CategoryMaster] AS C WHERE C.CategoryId=CM.CategoryId),  
   scm.SeqNo,scm.IsShow ,SubcategoryTax  
   from [SubCategoryMaster] AS SCM   
   INNER JOIN CategoryMaster AS CM ON CM.AutoId=SCM.CategoryAutoId WHERE SubcategoryId=@SubCategoryId    
  END  
  
  IF EXISTS(SELECT * FROM [psmpa.easywhm.com].[dbo].[SubCategoryMaster] WHERE SubcategoryId=@SubCategoryId)    
  BEGIN    
   UPDATE [psmpa.easywhm.com].[dbo].[SubCategoryMaster]     
   SET SubcategoryName = (SELECT SubcategoryName FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),    
   Description = (SELECT Description FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),     
   Status = (SELECT Status FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),  
   CategoryAutoId=(SELECT C.AutoId FROM [psmpa.easywhm.com].[dbo].[CategoryMaster] AS C WHERE C.CategoryId=(  
   SELECT CM.CategoryId FROM SubCategoryMaster AS SCM   
   INNER JOIN CategoryMaster AS CM ON CM.AutoId=SCM.CategoryAutoId  WHERE SCM.SubcategoryId=@SubCategoryId  
   )),  
   SeqNo=(SELECT SeqNo FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),  
   IsShow=(SELECT IsShow FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId), 
   SubcategoryTax=(SELECT SubcategoryTax FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),
   UpdateDate=GETDATE()  
   WHERE SubcategoryId=@SubCategoryId    
  END    
  ELSE    
  BEGIN    
   INSERT INTO [psmpa.easywhm.com].[dbo].[SubCategoryMaster]([SubcategoryId], SubcategoryName,Description,Status,CreatedDate,UpdateDate,CategoryAutoId,  
   SeqNo,IsShow,SubcategoryTax)    
   SELECT [SubcategoryId], SubcategoryName,SCM.Description,SCM.Status,GETDATE(),GETDATE(),  
   (SELECT C.AutoId FROM [psmpa.easywhm.com].[dbo].[CategoryMaster] AS C WHERE C.CategoryId=CM.CategoryId),  
   scm.SeqNo,scm.IsShow ,SubcategoryTax  
   from [SubCategoryMaster] AS SCM   
   INNER JOIN CategoryMaster AS CM ON CM.AutoId=SCM.CategoryAutoId WHERE SubcategoryId=@SubCategoryId    
  END  
  IF EXISTS(SELECT * FROM [psmwpa.easywhm.com].[dbo].[SubCategoryMaster] WHERE SubcategoryId=@SubCategoryId)    
  BEGIN    
	   UPDATE [psmwpa.easywhm.com].[dbo].[SubCategoryMaster]     
	   SET SubcategoryName = (SELECT SubcategoryName FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),    
	   Description = (SELECT Description FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),     
	   Status = (SELECT Status FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),  
	   CategoryAutoId=(SELECT C.AutoId FROM [psmwpa.easywhm.com].[dbo].[CategoryMaster] AS C WHERE C.CategoryId=(  
	   SELECT CM.CategoryId FROM SubCategoryMaster AS SCM   
	   INNER JOIN CategoryMaster AS CM ON CM.AutoId=SCM.CategoryAutoId  WHERE SCM.SubcategoryId=@SubCategoryId  
	   )),  
	   SeqNo=(SELECT SeqNo FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),  
	   IsShow=(SELECT IsShow FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId), 
	   SubcategoryTax=(SELECT SubcategoryTax FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),
	   UpdateDate=GETDATE()  
	   WHERE SubcategoryId=@SubCategoryId    
  END    
  ELSE    
  BEGIN    
	   INSERT INTO [psmwpa.easywhm.com].[dbo].[SubCategoryMaster]([SubcategoryId], SubcategoryName,Description,Status,CreatedDate,UpdateDate,CategoryAutoId,  
	   SeqNo,IsShow,SubcategoryTax )    
	   SELECT [SubcategoryId], SubcategoryName,SCM.Description,SCM.Status,GETDATE(),GETDATE(),  
	   (SELECT C.AutoId FROM [psmwpa.easywhm.com].[dbo].[CategoryMaster] AS C WHERE C.CategoryId=CM.CategoryId),  
	   scm.SeqNo,scm.IsShow,SubcategoryTax   
	   from [SubCategoryMaster] AS SCM   
	   INNER JOIN CategoryMaster AS CM ON CM.AutoId=SCM.CategoryAutoId WHERE SubcategoryId=@SubCategoryId    
  END  

  IF EXISTS(SELECT * FROM [psmny.easywhm.com].[dbo].[SubCategoryMaster] WHERE SubcategoryId=@SubCategoryId)    
  BEGIN    
	   UPDATE [psmny.easywhm.com].[dbo].[SubCategoryMaster]     
	   SET SubcategoryName = (SELECT SubcategoryName FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),    
	   Description = (SELECT Description FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),     
	   Status = (SELECT Status FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),  
	   CategoryAutoId=(SELECT C.AutoId FROM [psmny.easywhm.com].[dbo].[CategoryMaster] AS C WHERE C.CategoryId=(  
	   SELECT CM.CategoryId FROM SubCategoryMaster AS SCM   
	   INNER JOIN CategoryMaster AS CM ON CM.AutoId=SCM.CategoryAutoId  WHERE SCM.SubcategoryId=@SubCategoryId  
	   )),  
	   SeqNo=(SELECT SeqNo FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),  
	   IsShow=(SELECT IsShow FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),  
	   SubcategoryTax=(SELECT SubcategoryTax FROM [dbo].[SubCategoryMaster]  WHERE SubcategoryId=@SubCategoryId),
	   UpdateDate=GETDATE()  
	   WHERE SubcategoryId=@SubCategoryId    
  END    
  ELSE    
  BEGIN    
	   INSERT INTO [psmny.easywhm.com].[dbo].[SubCategoryMaster]([SubcategoryId], SubcategoryName,Description,Status,CreatedDate,UpdateDate,CategoryAutoId,  
	   SeqNo,IsShow,SubcategoryTax)    
	   SELECT [SubcategoryId], SubcategoryName,SCM.Description,SCM.Status,GETDATE(),GETDATE(),  
	   (SELECT C.AutoId FROM [psmny.easywhm.com].[dbo].[CategoryMaster] AS C WHERE C.CategoryId=CM.CategoryId),  
	   scm.SeqNo,scm.IsShow,SubcategoryTax  
	   from [SubCategoryMaster] AS SCM   
	   INNER JOIN CategoryMaster AS CM ON CM.AutoId=SCM.CategoryAutoId WHERE SubcategoryId=@SubCategoryId    
  END  

END  