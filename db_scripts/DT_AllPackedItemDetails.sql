USE [psmnj.a1whm.com]
GO
DROP  PROCEDURE [dbo].[Proc_Packer_OrderMaster]
drop TYPE [dbo].[DT_AllPackedItemDetails]


CREATE TYPE [dbo].[DT_AllPackedItemDetails] AS TABLE(
	[ProductAutoId] [int] NULL,
	[UnitAutoId] [int] NULL,
	[Barcode] [varchar](50) NULL,
	[QtyShip] [int] NULL,
	[Pcs] [int] NULL,
	[OldPacked] [int] NULL,
	[RemainQty] [int] NULL,
	[isfreeitem] [int] NULL,
	[IsExchange] [int] NULL
)
GO

CREATE table [dbo].[AllPackedItemDetails](
     AutoId int identity(1,1),
	 MasterAutoId int not null,
	[ProductAutoId] [int] NULL,
	[UnitAutoId] [int] NULL,
	[Barcode] [varchar](50) NULL,
	[QtyShip] [int] NULL,
	[Pcs] [int] NULL,
	[OldPacked] [int] NULL,
	[RemainQty] [int] NULL,
	[isfreeitem] [int] NULL,
	[IsExchange] [int] NULL
)
