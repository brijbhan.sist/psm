USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcProductSummaryReport]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[ProcProductSummaryReport]
@OpCode int=Null,      
@Who int = null,      
@AutoId  int=Null,      
@ProductName VARCHAR(50)=NULL,      
@ProductId VARCHAR(50)=NULL, 
@FromDate datetime=Null,      
@ToDate datetime=Null,      
@Description varchar(250)=NULL,      
@Status int=null,      
@PageIndex INT=1,      
@PageSize INT=10,      
@RecordCount INT=null,      
@isException bit out,      
@exceptionMessage varchar(max) out    
as 
BEGIN      
 BEGIN TRY      
   SET @isException=0      
   SET @exceptionMessage='success'    
   if @OpCode = 41
   begin
   select ROW_NUMBER() over(order by ProductId,ProductName) as RowNumber,
   * into #Result from (
         select pm.ProductId,ProductName,SUM(NetPrice) as TotalNetSales from Delivered_Order_Items as dio
		 inner join OrderMaster as om on om.AutoId=dio.OrderAutoId
		 inner join ProductMaster as pm on pm.AutoId=dio.ProductAutoId
		 WHERE (@ProductName is null or @ProductName='' or pm.ProductName like '%'+ @ProductName +'%')      
		 and (@ProductId is null or @ProductId='' or pm.ProductId like '%'+ @ProductId +'%') 
		 and om.Status=11
		 and (@FromDate is null or @FromDate='' or @ToDate is null or @ToDate='' or (CONVERT(date,om.OrderDate) between @FromDate and @ToDate))
	group by pm.ProductId,ProductName
	having SUM(dio.QtyDel)>0
	
	 ) as t order by ProductId,ProductName
	 SELECT * FROM #Result  
   WHERE @PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)   
   SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize=0 then COUNT(RowNumber) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Result   

   end

  else if @OpCode = 42
   begin
  
         select pm.ProductId,ProductName,SUM(NetPrice) as TotalNetSales from Delivered_Order_Items as dio
		 inner join OrderMaster as om on om.AutoId=dio.OrderAutoId
		 inner join ProductMaster as pm on pm.AutoId=dio.ProductAutoId
		 WHERE (@ProductName is null or @ProductName='' or pm.ProductName like '%'+ @ProductName +'%')      
		 and (@ProductId is null or @ProductId='' or pm.ProductId like '%'+ @ProductId +'%') 
		 and om.Status=11
		 and (@FromDate is null or @FromDate='' or @ToDate is null or @ToDate='' or (CONVERT(date,om.OrderDate) between @FromDate and @ToDate))
		 
	     group by pm.ProductId,ProductName		 
	      having SUM(dio.QtyDel)>0
	     order by ProductId,ProductName

   end

    END TRY      
   BEGIN CATCH          
  SET @isException=1      
  SET @exceptionMessage='Oops! Something went wrong.Please try later.'
 END CATCH      
END 
GO
