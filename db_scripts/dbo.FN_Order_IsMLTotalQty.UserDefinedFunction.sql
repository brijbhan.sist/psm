USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Order_IsMLTotalQty]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

alter table OrderItemMaster drop column TotalMLQty
drop function FN_Order_IsMLTotalQty
go
CREATE FUNCTION  [dbo].[FN_Order_IsMLTotalQty]  
(  
	@AutoId int,  
	@orderAutoId int  
)  
RETURNS decimal(18,2)  
AS  
BEGIN  
	DECLARE @MLTotalQty decimal(18,2)=0.00      
	IF ([dbo].[FN_Order_IsMLtaxApply](@orderAutoId)=1)  
	BEGIN  
		IF EXISTS(SELECT AutoId FROM OrderItemMaster WHERE AutoId=@AutoId AND IsExchange=0)  
		BEGIN  
			SET @MLTotalQty=(SELECT (TotalPieces*UnitMLQty) FROM OrderItemMaster WHERE AutoId=@AutoId)   
		END  
	END  
	RETURN @MLTotalQty  
END  
  
GO

alter table OrderItemMaster add [TotalMLQty]  AS ([dbo].[FN_Order_IsMLTotalQty]([AutoId],[OrderAutoId]))