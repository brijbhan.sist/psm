USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcSubTaxMaster]    Script Date: 07/09/2021 05:56:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--create table SubTaxMaster
--(
--AutoId int identity(1,1),
--TaxName varchar(200),
--Value decimal(18,2),
--Description varchar(500),
--Status int,
--CreatedDate datetime,
--CreatedBy int,
--UpdatedDate datetime,
--UpdatedBy int
--)

ALTER   PROCEDURE [dbo].[ProcSubTaxMaster]        
@OpCode int=Null,        
@AutoId  int=Null,        
@UserAutoId int=NULL,
@TaxName VARCHAR(200)=NULL,        
@Value decimal(18,2)=NULL,        
@Description varchar(500)=NULL,        
@Status int=null,        
@isException bit out,        
@exceptionMessage varchar(max) out        
AS        
BEGIN        
 BEGIN TRY        
  SET @isException=0        
  SET @exceptionMessage='success'        
        
 IF @OpCode=11        
 BEGIN        
  IF exists(SELECT TaxName FROM SubTaxMaster WHERE TaxName = TRIM(@TaxName))        
  BEGIN        
   set @isException=1        
   set @exceptionMessage='Tax already exists'        
  END        
  ELSE       
  BEGIN       
   BEGIN TRY        
	BEGIN TRAN        
			
		INSERT INTO [dbo].SubTaxMaster(TaxName, Value,Description,Status,CreatedDate,CreatedBy)        
		VALUES (TRIM(@TaxName),@Value, @Description,@Status,GETDATE(),@UserAutoId)        
		   
	COMMIT TRANSACTION        
	END TRY         
	BEGIN CATCH        
	ROLLBACK TRAN        
		SET @isException=1        
		SET @exceptionMessage='Oops! Something went wrong.Please try later.'    
	END CATCH        
  END      
 END        
 ELSE IF @Opcode=21        
 BEGIN        
  IF exists(SELECT TaxName FROM SubTaxMaster WHERE TaxName = TRIM(@TaxName) and AutoId != @AutoId)        
  BEGIN        
		SET @isException=1        
		SET @exceptionMessage='Tax already exists'        
  END        
  ELSE        
  BEGIN        
   BEGIN TRY        
	   BEGIN TRAN        
			UPDATE SubTaxMaster SET TaxName=TRIM(@TaxName), Value=@Value,Description=@Description,Status=@Status,
			UpdatedDate=GETDATE(),UpdatedBy=@UserAutoId where AutoId=@AutoId  
			
	   COMMIT TRANSACTION        
   END TRY         
   BEGIN CATCH        
   ROLLBACK TRAN        
		   SET @isException=1        
		   SET @exceptionMessage='Oops! Something went wrong.Please try later'      
   END CATCH        
  END        
 END        
 ELSE IF @Opcode=31        
 BEGIN        
       
IF exists(SELECT SubcategoryTax FROM SubCategoryMaster WHERE  SubcategoryTax = @AutoId)        
  BEGIN        
		SET @isException=1        
		SET @exceptionMessage='Tax has been used in another sub category.'        
  END        
  ELSE        
  BEGIN 
	BEGIN TRY        
	BEGIN TRAN        
		DELETE FROM SubTaxMaster WHERE AutoId=@AutoId 
	COMMIT TRANSACTION    
	END TRY         
	BEGIN CATCH        
	ROLLBACK TRAN        
		SET @isException=1        
		SET @exceptionMessage='Oops! Something went wrong.Please try later'      
	END CATCH    		    
    END    
 END        
 ELSE IF @OpCode=41        
 BEGIN        
	  SELECT TM.AutoId,TM.TaxName, TM.Value,TM.Description, SM.StatusType AS Status FROM  SubTaxMaster AS TM         
	  inner join StatusMaster AS SM ON SM.AutoId=TM.Status and SM.Category is NULL         
	  WHERE (@TaxName is null or @TaxName='' or TaxName like '%'+ @TaxName +'%')        
	  and (@Status=2 or Status=@Status)          
            
 END         
 ELSE IF @OpCode=42        
 BEGIN        
		SELECT AutoId,TaxName, Value,Description,Status FROM SubTaxMaster WHERE AutoId=@AutoId        
 END        
 END TRY        
 BEGIN CATCH            
	   SET @isException=1        
	   SET @exceptionMessage='Oops! Something went wrong.Please try later.'    
 END CATCH        
END        
