

alter table [dbo].[Delivered_Order_Items] drop column IsProductTaxPerUnit
drop function FN_DelOrder_IsProductTaxPerUnit
go
create FUNCTION  [dbo].[FN_DelOrder_IsProductTaxPerUnit]  
(  
	@AutoId int,  
	@orderAutoId int  
)  
RETURNS decimal(18,2)  
AS  
BEGIN  
	DECLARE @Total decimal(18,2)=0.00,    
	 @Tax int=null  
	IF (select tax from Delivered_Order_Items where AutoId=@AutoId)=1
	BEGIN  
		set @Total=(select isnull((oim.UnitPrice*om.TaxValue)/100,0) as Tax from Delivered_Order_Items as oim 
					inner join OrderMaster as om on om.AutoId=oim.OrderAutoId
					where oim.AutoId=@AutoId) 

	END  
	RETURN @Total  
END  
  
GO

alter table Delivered_Order_Items add IsProductTaxPerUnit  AS ([dbo].[FN_DelOrder_IsProductTaxPerUnit]([AutoId],[OrderAutoId]))