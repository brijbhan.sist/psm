ALTER function dbo.UDF_CheckMLTax
(
  @OrderAutoId INT  --Select dbo.UDF_CheckMLTax (14424)
)
returns int
AS
BEGIN
    Declare @isMLManualyApply int=0
   IF EXISTS(Select TaxState from MLTaxMaster Where TaxState=(Select State from BillingAddress Where AutoId=(Select BillAddrAutoId from OrderMaster where AutoId=@OrderAutoId)))
	BEGIN
         IF(SELECT MLQty from OrderMaster WHERE AutoId=@OrderAutoId)>0
		 BEGIN
		      SET @isMLManualyApply=1
		 END
	END
	return @isMLManualyApply
END