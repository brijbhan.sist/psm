                 
ALTER PROCEDURE [dbo].[ProcInsertData_EmployeeMaster]                                        
@Opcode int=Null,        
@EmpAutoId int=Null,            
@AssignDeviceAutoId VARCHAR(500)=NULL,  
@db_name varchar(max)=NULL,
@sql_query varchar(max)='',                          
@UserName VARCHAR(100)=NULL,  
@PageIndex INT = 1,                                      
@PageSize INT = 10,   
@deviceAutoId int=null,
@RecordCount INT =null,                                      
@IsLoginIp INT =null,      
@companyId varchar(50)=null,
@IsAppLogin INT =null,                              
@IPAddress varchar(250)=null  ,                                 
@isException bit out,                                      
@exceptionMessage varchar(max) out                                      
AS                                      
BEGIN                                      
                                     
 SET @isException=0                                      
 SET @exceptionMessage='Success'                                      
                                   
 IF @Opcode=11                                       
 BEGIN  
  set @db_name=(select [ChildDB_Name] from CompanyDetails)
   if @db_name is not null
	begin
      declare @FullName varchar(200),@Pass varchar(max) 
	  set @FullName=@UserName+'@'+@companyId
 
 
	set @sql_query='SET IDENTITY_INSERT ['+@db_name+'].[dbo].[EmployeeMaster] ON;
	insert into ['+@db_name+'].[dbo].[EmployeeMaster] (AutoId,[EmpId],[EmpType],FirstName,LastName,Email,Contact,Address,[State],[City],[Zipcode],                                      
    Status,UserName,Password,CreatedDate,ImageURL,isApplyIP,IP_Address,EmpLoyeeCode,ProfileName,IsAppLogin,OptimotwFrom,OptimotwTo)
	select AutoId,[EmpId],[EmpType],FirstName,LastName,Email,Contact,Address,[State],[City],[Zipcode],                                      
    Status,UserName,EncryptByPassPhrase(''WHM'',CONVERT(varchar(100),DecryptByPassphrase(''WHM'',emp.[Password]))),CreatedDate,ImageURL,isApplyIP,IP_Address,EmpLoyeeCode,ProfileName,IsAppLogin,OptimotwFrom,OptimotwTo 
	from [dbo].[EmployeeMaster] as emp where emp.AutoId='+convert(varchar(10),@EmpAutoId)+';
	SET IDENTITY_INSERT ['+@db_name+'].[dbo].[EmployeeMaster] OFF;'
	exec (@sql_query) 

	if(@AssignDeviceAutoId!='')
	begin		
		set @sql_query='SET IDENTITY_INSERT ['+@db_name+'].[dbo].[AssignDeviceMaster] ON;
		 insert into ['+@db_name+'].[dbo].[AssignDeviceMaster](AutoId,[DeviceAutoId],[EmpAutoId]) select AutoId,[DeviceAutoId],[EmpAutoId]
		 from [dbo].[AssignDeviceMaster] where EmpAutoId='+convert(varchar(10),@EmpAutoId)+';
		 SET IDENTITY_INSERT ['+@db_name+'].[dbo].[AssignDeviceMaster] OFF;'
	    exec (@sql_query) 
	end
                                
 END  
 end
  IF @Opcode=21                                       
 BEGIN   
  
	set @db_name=(select [ChildDB_Name] from CompanyDetails)
	if @db_name is not null
	begin
   set @sql_query='update emp set [EmpType]=em.EmpType,FirstName=em.FirstName,LastName=em.LastName,Email=em.Email,Contact=em.Contact,                                      
  [Address]=em.Address,[State]=em.State,[City]=em.City,[Zipcode]=em.Zipcode,EmployeeCode=em.EmployeeCode,ProfileName=em.ProfileName,                                      
  UserName=em.UserName,[Password]=EncryptByPassPhrase(''WHM'',CONVERT(varchar(100),DecryptByPassphrase(''WHM'',em.[Password]))),UpdateDate=em.UpdateDate, IP_Address=em.IP_Address,isApplyIP=em.isApplyIP, ImageURL=em.ImageURL,
  IsAppLogin=em.IsAppLogin,OptimotwFrom=em.OptimotwFrom,OptimotwTo=em.OptimotwTo from ['+@db_name+'].[dbo].[EmployeeMaster] as 
  emp inner join [dbo].[EmployeeMaster] as em on em.AutoId=emp.AutoId where emp.AutoId='+convert(varchar(10),@EmpAutoId)+';'
   exec (@sql_query)  

    set @sql_query='delete from ['+@db_name+'].[dbo].[AssignDeviceMaster] where EmpAutoId='+convert(varchar(10),@EmpAutoId)+';'
	exec (@sql_query) 

	if(@AssignDeviceAutoId!='')
	begin		
		set @sql_query='SET IDENTITY_INSERT ['+@db_name+'].[dbo].[AssignDeviceMaster] ON;
		 insert into ['+@db_name+'].[dbo].[AssignDeviceMaster](AutoId,[DeviceAutoId],[EmpAutoId]) select AutoId,[DeviceAutoId],[EmpAutoId]
		 from [dbo].[AssignDeviceMaster] where EmpAutoId='+convert(varchar(10),@EmpAutoId)+';
		 SET IDENTITY_INSERT ['+@db_name+'].[dbo].[AssignDeviceMaster] OFF;'
	    exec (@sql_query) 
	end
	
  END   
 
END 
            
end
