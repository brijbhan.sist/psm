ALTER PROCEDURE [dbo].[ProcBrandMaster_ALL]    
@BrandId  VARCHAR(50)=Null    
AS    
BEGIN    
  --------------------------------INSERT  AND UPDATE FOR PSMCT DB---------------------------------------  
  IF EXISTS(SELECT * FROM [psmct.a1whm.com].[dbo].[BrandMaster] WHERE BrandId=@BrandId)    
  BEGIN    
   UPDATE [psmct.a1whm.com].[dbo].[BrandMaster]     
   SET BrandName = (SELECT BrandName FROM [dbo].[BrandMaster]  WHERE BrandId=@BrandId),    
   Description = (SELECT Description FROM [dbo].[BrandMaster]  WHERE BrandId=@BrandId),     
   Status = (SELECT Status FROM [dbo].[BrandMaster]  WHERE BrandId=@BrandId),  
   UpdatedOn=GETDATE()  
   WHERE BrandId=@BrandId    
  END    
  ELSE    
  BEGIN    
   INSERT INTO [psmct.a1whm.com].[dbo].[BrandMaster]([BrandId], BrandName,Description,Status,CreatedOn,UpdatedOn)    
   SELECT [BrandId], BrandName,Description,Status,GETDATE(),GETDATE()  
   from [BrandMaster]  WHERE BrandId=@BrandId    
  END   
  
  --------------------------------INSERT  AND UPDATE FOR PSMPA DB---------------------------------------  
  
  IF EXISTS(SELECT * FROM [psmpa.a1whm.com].[dbo].[BrandMaster] WHERE BrandId=@BrandId)    
  BEGIN    
   UPDATE [psmpa.a1whm.com].[dbo].[BrandMaster]     
   SET BrandName = (SELECT BrandName FROM [dbo].[BrandMaster]  WHERE BrandId=@BrandId),    
   Description = (SELECT Description FROM [dbo].[BrandMaster]  WHERE BrandId=@BrandId),     
   Status = (SELECT Status FROM [dbo].[BrandMaster]  WHERE BrandId=@BrandId)  ,  
   UpdatedOn=GETDATE()  
   WHERE BrandId=@BrandId    
  END    
  ELSE    
  BEGIN    
   INSERT INTO [psmpa.a1whm.com].[dbo].[BrandMaster]([BrandId], BrandName,Description,Status,CreatedOn,UpdatedOn)    
   SELECT [BrandId], BrandName,Description,Status,GETDATE(),GETDATE() from [BrandMaster] WHERE BrandId=@BrandId    
  END  
  --------------------------------INSERT  AND UPDATE FOR PSMNPA DB---------------------------------------  
  IF EXISTS(SELECT * FROM [psmnpa.a1whm.com].[dbo].[BrandMaster] WHERE BrandId=@BrandId)    
  BEGIN    
   UPDATE [psmnpa.a1whm.com].[dbo].[BrandMaster]     
   SET BrandName = (SELECT BrandName FROM [dbo].[BrandMaster]  WHERE BrandId=@BrandId),    
   Description = (SELECT Description FROM [dbo].[BrandMaster]  WHERE BrandId=@BrandId),     
   Status = (SELECT Status FROM [dbo].[BrandMaster]  WHERE BrandId=@BrandId),  
   UpdatedOn=GETDATE()  
   WHERE BrandId=@BrandId    
  END    
  ELSE    
  BEGIN    
   INSERT INTO [psmnpa.a1whm.com].[dbo].[BrandMaster]([BrandId], BrandName,Description,Status,CreatedOn,UpdatedOn)    
   SELECT [BrandId], BrandName,Description,Status,GETDATE(),GETDATE() from [BrandMaster] WHERE BrandId=@BrandId    
  END  
  
  --------------------------------INSERT  AND UPDATE FOR PSMWPA DB---------------------------------------  
  IF EXISTS(SELECT * FROM [psmwpa.a1whm.com].[dbo].[BrandMaster] WHERE BrandId=@BrandId)    
  BEGIN    
   UPDATE [psmwpa.a1whm.com].[dbo].[BrandMaster]     
   SET BrandName = (SELECT BrandName FROM [dbo].[BrandMaster]  WHERE BrandId=@BrandId),    
   Description = (SELECT Description FROM [dbo].[BrandMaster]  WHERE BrandId=@BrandId),     
   Status = (SELECT Status FROM [dbo].[BrandMaster]  WHERE BrandId=@BrandId),  
   UpdatedOn=GETDATE()  
   WHERE BrandId=@BrandId    
  END    
  ELSE    
  BEGIN    
   INSERT INTO [psmwpa.a1whm.com].[dbo].[BrandMaster]([BrandId], BrandName,Description,Status,CreatedOn,UpdatedOn)    
   SELECT [BrandId], BrandName,Description,Status,GETDATE(),GETDATE() from [BrandMaster] WHERE BrandId=@BrandId    
  END  
  
  --------------------------------INSERT  AND UPDATE FOR PSMNY DB---------------------------------------  
  IF EXISTS(SELECT * FROM [psmny.a1whm.com].[dbo].[BrandMaster] WHERE BrandId=@BrandId)    
  BEGIN    
   UPDATE [psmny.a1whm.com].[dbo].[BrandMaster]     
   SET BrandName = (SELECT BrandName FROM [dbo].[BrandMaster]  WHERE BrandId=@BrandId),    
   Description = (SELECT Description FROM [dbo].[BrandMaster]  WHERE BrandId=@BrandId),     
   Status = (SELECT Status FROM [dbo].[BrandMaster]  WHERE BrandId=@BrandId),  
   UpdatedOn=GETDATE()  
   WHERE BrandId=@BrandId    
  END    
  ELSE    
  BEGIN    
   INSERT INTO [psmny.a1whm.com].[dbo].[BrandMaster]([BrandId], BrandName,Description,Status,CreatedOn,UpdatedOn)    
   SELECT [BrandId], BrandName,Description,Status,GETDATE(),GETDATE() from [BrandMaster] WHERE BrandId=@BrandId    
  END 

  
  --------------------------------INSERT  AND UPDATE FOR psmnj.easywhm.com DB---------------------------------------  
  IF EXISTS(SELECT * FROM [psmnj.easywhm.com].[dbo].[BrandMaster] WHERE BrandId=@BrandId)    
  BEGIN    
   UPDATE [psmnj.easywhm.com].[dbo].[BrandMaster]     
   SET BrandName = (SELECT BrandName FROM [dbo].[BrandMaster]  WHERE BrandId=@BrandId),    
   Description = (SELECT Description FROM [dbo].[BrandMaster]  WHERE BrandId=@BrandId),     
   Status = (SELECT Status FROM [dbo].[BrandMaster]  WHERE BrandId=@BrandId),  
   UpdatedOn=GETDATE()  
   WHERE BrandId=@BrandId    
  END    
  ELSE    
  BEGIN    
   INSERT INTO [psmnj.easywhm.com].[dbo].[BrandMaster]([BrandId], BrandName,Description,Status,CreatedOn,UpdatedOn)    
   SELECT [BrandId], BrandName,Description,Status,GETDATE(),GETDATE() from [dbo].[BrandMaster] WHERE BrandId=@BrandId    
  END  
  IF EXISTS(SELECT * FROM [psmnpa.easywhm.com].[dbo].[BrandMaster] WHERE BrandId=@BrandId)    
  BEGIN    
   UPDATE [psmnpa.easywhm.com].[dbo].[BrandMaster]     
   SET BrandName = (SELECT BrandName FROM [dbo].[BrandMaster]  WHERE BrandId=@BrandId),    
   Description = (SELECT Description FROM [dbo].[BrandMaster]  WHERE BrandId=@BrandId),     
   Status = (SELECT Status FROM [dbo].[BrandMaster]  WHERE BrandId=@BrandId),  
   UpdatedOn=GETDATE()  
   WHERE BrandId=@BrandId    
  END    
  ELSE    
  BEGIN    
   INSERT INTO [psmnpa.easywhm.com].[dbo].[BrandMaster]([BrandId], BrandName,Description,Status,CreatedOn,UpdatedOn)    
   SELECT [BrandId], BrandName,Description,Status,GETDATE(),GETDATE() from [dbo].[BrandMaster] WHERE BrandId=@BrandId    
  END  
  
  IF EXISTS(SELECT * FROM [psmpa.easywhm.com].[dbo].[BrandMaster] WHERE BrandId=@BrandId)    
  BEGIN    
   UPDATE [psmpa.easywhm.com].[dbo].[BrandMaster]     
   SET BrandName = (SELECT BrandName FROM [dbo].[BrandMaster]  WHERE BrandId=@BrandId),    
   Description = (SELECT Description FROM [dbo].[BrandMaster]  WHERE BrandId=@BrandId),     
   Status = (SELECT Status FROM [dbo].[BrandMaster]  WHERE BrandId=@BrandId),  
   UpdatedOn=GETDATE()  
   WHERE BrandId=@BrandId    
  END    
  ELSE    
  BEGIN    
   INSERT INTO [psmpa.easywhm.com].[dbo].[BrandMaster]([BrandId], BrandName,Description,Status,CreatedOn,UpdatedOn)    
   SELECT [BrandId], BrandName,Description,Status,GETDATE(),GETDATE() from [dbo].[BrandMaster] WHERE BrandId=@BrandId    
  END  
  
  
  IF EXISTS(SELECT * FROM [psmct.easywhm.com].[dbo].[BrandMaster] WHERE BrandId=@BrandId)    
  BEGIN    
   UPDATE [psmct.easywhm.com].[dbo].[BrandMaster]     
   SET BrandName = (SELECT BrandName FROM [dbo].[BrandMaster]  WHERE BrandId=@BrandId),    
   Description = (SELECT Description FROM [dbo].[BrandMaster]  WHERE BrandId=@BrandId),     
   Status = (SELECT Status FROM [dbo].[BrandMaster]  WHERE BrandId=@BrandId),  
   UpdatedOn=GETDATE()  
   WHERE BrandId=@BrandId    
  END    
  ELSE    
  BEGIN    
   INSERT INTO [psmct.easywhm.com].[dbo].[BrandMaster]([BrandId], BrandName,Description,Status,CreatedOn,UpdatedOn)    
   SELECT [BrandId], BrandName,Description,Status,GETDATE(),GETDATE() from [dbo].[BrandMaster] WHERE BrandId=@BrandId    
  END  
  
  IF EXISTS(SELECT * FROM [psmwpa.easywhm.com].[dbo].[BrandMaster] WHERE BrandId=@BrandId)    
  BEGIN    
   UPDATE [psmwpa.easywhm.com].[dbo].[BrandMaster]     
   SET BrandName = (SELECT BrandName FROM [dbo].[BrandMaster]  WHERE BrandId=@BrandId),    
   Description = (SELECT Description FROM [dbo].[BrandMaster]  WHERE BrandId=@BrandId),     
   Status = (SELECT Status FROM [dbo].[BrandMaster]  WHERE BrandId=@BrandId),  
   UpdatedOn=GETDATE()  
   WHERE BrandId=@BrandId    
  END    
  ELSE    
  BEGIN    
   INSERT INTO [psmwpa.easywhm.com].[dbo].[BrandMaster]([BrandId], BrandName,Description,Status,CreatedOn,UpdatedOn)    
   SELECT [BrandId], BrandName,Description,Status,GETDATE(),GETDATE() from [dbo].[BrandMaster] WHERE BrandId=@BrandId    
  END 
  

    IF EXISTS(SELECT * FROM [psmny.easywhm.com].[dbo].[BrandMaster] WHERE BrandId=@BrandId)    
  BEGIN    
   UPDATE [psmny.easywhm.com].[dbo].[BrandMaster]     
   SET BrandName = (SELECT BrandName FROM [dbo].[BrandMaster]  WHERE BrandId=@BrandId),    
   Description = (SELECT Description FROM [dbo].[BrandMaster]  WHERE BrandId=@BrandId),     
   Status = (SELECT Status FROM [dbo].[BrandMaster]  WHERE BrandId=@BrandId),  
   UpdatedOn=GETDATE()  
   WHERE BrandId=@BrandId    
  END    
  ELSE    
  BEGIN    
   INSERT INTO [psmny.easywhm.com].[dbo].[BrandMaster]([BrandId], BrandName,Description,Status,CreatedOn,UpdatedOn)    
   SELECT [BrandId], BrandName,Description,Status,GETDATE(),GETDATE() from [dbo].[BrandMaster] WHERE BrandId=@BrandId    
  END 
END  