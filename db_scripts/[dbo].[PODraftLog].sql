USE [psmnj.a1whm.com]
GO

/****** Object:  Table [dbo].[PODraftLog]    Script Date: 01/25/2020 17:40:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PODraftLog](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[RemarkType] [varchar](100) NULL,
	[Remark] [varchar](250) NULL,
	[POAutoId] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CraetedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_PODraftLog] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


