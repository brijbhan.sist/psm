USE [psmnj.a1whm.com]
GO

/****** Object:  UserDefinedTableType [dbo].[SaveRuteDt]    Script Date: 6/29/2020 5:33:52 PM ******/
DROP TYPE [dbo].[SaveRuteDt]
GO

/****** Object:  UserDefinedTableType [dbo].[SaveRuteDt]    Script Date: 6/29/2020 5:33:52 PM ******/
CREATE TYPE [dbo].[SaveRuteDt] AS TABLE(
	[OrderNo] [varchar](25) NULL,
	[Driver] [int] NULL,
	[CarId] [int] NULL,
	[Stop] [int] NULL,
	[Remark] [varchar](500) NULL,
	[ScheduleDate] [datetime] NULL,
	[Lat] [varchar](20) NULL,
	[Long] [varchar](20) NULL,
	[DeliveryFrom] [varchar](20) NULL,
	[DeliveryTo] [varchar](20) NULL,
	[ScheduleAt] [varchar](20) NULL,
	[DeliveredAddress] [varchar](500) NULL
)
GO

Alter table 


