--in this table already has forigh key 
--ALTER TABLE [dbo].[CustomerCreditMaster]  WITH CHECK ADD  CONSTRAINT [FK_CustomerCreditMaster] FOREIGN KEY([CustomerAutoId])
--REFERENCES [dbo].[CustomerMaster] ([AutoId])
---------------------------------------------------
---------------------------------------------------
--ALTER TABLE [dbo].[CustomerDocumentMaster]  WITH CHECK ADD  CONSTRAINT [FK_CustomerDocumentMaster] FOREIGN KEY([CustomerAutoId])
--REFERENCES [dbo].[CustomerMaster] ([AutoId])

--ALTER TABLE [dbo].[AllowQtyPiece]  WITH CHECK ADD  CONSTRAINT [FK_AllowQtyPiece_ProductMaster] FOREIGN KEY([ProductAutoId])
--REFERENCES [dbo].[ProductMaster] ([AutoId])

--ALTER TABLE [dbo].[AllowQtyPiece_Log]  WITH CHECK ADD  CONSTRAINT [FK_AllowQtyPiece_Log_ProductMaster] FOREIGN KEY([ProductAutoId])
--REFERENCES [dbo].[ProductMaster] ([AutoId])

--ALTER TABLE [dbo].[DamageStockItems]  WITH CHECK ADD  CONSTRAINT [FK_DamageStockItems_ProductMaster] FOREIGN KEY([ProductAutoId])
--REFERENCES [dbo].[ProductMaster] ([AutoId])

--ALTER TABLE [dbo].[DamageStockItems]  WITH CHECK ADD  CONSTRAINT [FK_DamageStockItems_UnitMaster] FOREIGN KEY([UnitTypeAutoId])
--REFERENCES [dbo].[UnitMaster] ([AutoId])

ALTER TABLE [dbo].[Delete_Delivered_Order_Items_Backup]  WITH CHECK ADD  CONSTRAINT [FK_Delete_Delivered_Order_Items_Backup_OrderMaster] FOREIGN KEY([OrderAutoId])
REFERENCES [dbo].[OrderMaster] ([AutoId])

ALTER TABLE [dbo].[Delete_Delivered_Order_Items_Backup]  WITH CHECK ADD  CONSTRAINT [FK_Delete_Delivered_Order_Items_Backup_ProductMaster] FOREIGN KEY([ProductAutoId])
REFERENCES [dbo].[ProductMaster] ([AutoId])

ALTER TABLE [dbo].[Delete_Delivered_Order_Items_Backup]  WITH CHECK ADD  CONSTRAINT [FK_Delete_Delivered_Order_Items_Backup_UnitMaster] FOREIGN KEY([UnitAutoId])
REFERENCES [dbo].[UnitMaster] ([AutoId])

--ALTER TABLE [dbo].[ManagerItemDelete]  WITH CHECK ADD  CONSTRAINT [FK_ManagerItemDelete_ProductMaster] FOREIGN KEY([ProductAutoId])
--REFERENCES [dbo].[ProductMaster] ([AutoId])

--ALTER TABLE [dbo].[ManagerItemDelete]  WITH CHECK ADD  CONSTRAINT [FK_ManagerItemDelete_UnitMaster] FOREIGN KEY([UnitTypeAutoId])
--REFERENCES [dbo].[UnitMaster] ([AutoId])

--ALTER TABLE [dbo].[MessageAssignTo]  WITH CHECK ADD  CONSTRAINT [FK_MessageAssignTo_EmployeeMaster] FOREIGN KEY([EmpAutoId])
--REFERENCES [dbo].[EmployeeMaster] ([AutoId])

--ALTER TABLE [dbo].[MessageAssignTo]  WITH CHECK ADD  CONSTRAINT [FK_MessageAssignTo_EmployeeTypeMaster] FOREIGN KEY([EmpType])
--REFERENCES [dbo].[EmployeeTypeMaster] ([AutoId])

ALTER TABLE [dbo].[Draft_StockEntry]  WITH CHECK ADD  CONSTRAINT [FK_Draft_StockEntry_VendorMaster] FOREIGN KEY([VendorAutoId])
REFERENCES [dbo].[VendorMaster] ([AutoId])

--ALTER TABLE [dbo].[Draft_StockItemMaster]  WITH CHECK ADD  CONSTRAINT [FK_Draft_StockItemMaster_UnitMaster] FOREIGN KEY([UnitAutoId])
--REFERENCES [dbo].[UnitMaster] ([AutoId])

--ALTER TABLE [dbo].[ExpensiveCurrency]  WITH CHECK ADD  CONSTRAINT [FK_ExpensiveCurrency_ExpensiveCurrency] FOREIGN KEY([CurrencyAutoId])
--REFERENCES [dbo].[CurrencyMaster] ([AutoId])

--------10/21/2020---------------
update [dbo].[PageTrackerMaster] set UserAutoId=1104
ALTER TABLE [dbo].[PageTrackerMaster]  WITH CHECK ADD  CONSTRAINT [FK_PageTrackerMaster_EmployeeMaster] FOREIGN KEY([UserAutoId])
REFERENCES [dbo].[EmployeeMaster] ([AutoId])

delete  from App_ApiRequestLog where UserAutoId not in (select AutoId from EmployeeMaster)
ALTER TABLE [dbo].[App_ApiRequestLog]  WITH CHECK ADD  CONSTRAINT [FK_App_ApiRequestLog_App_ApiRequestLog] FOREIGN KEY([AutoId])
REFERENCES [dbo].[App_ApiRequestLog] ([AutoId])

delete  from  PaymentOrderDetails where OrderAutoId not in (select AutoId from OrderMaster)
ALTER TABLE [dbo].[PaymentOrderDetails]  WITH CHECK ADD  CONSTRAINT [FK_PaymentOrderDetails_OrderMaster] FOREIGN KEY([OrderAutoId])
REFERENCES [dbo].[OrderMaster] ([AutoId])


delete  from  PageAction where AutoId not in (select AutoId from PageMaster)
ALTER TABLE [dbo].[PageAction]  WITH CHECK ADD  CONSTRAINT [FK_PageAction_PageMaster] FOREIGN KEY([PageAutoId])
REFERENCES [dbo].[PageMaster] ([AutoId])

delete  from  ipaddress where EmpAutoId not in (select AutoId from EmployeeMaster)
ALTER TABLE [dbo].[ipaddress]  WITH CHECK ADD  CONSTRAINT [FK_ipaddress_EmployeeMaster] FOREIGN KEY([EmpAutoId])
REFERENCES [dbo].[EmployeeMaster] ([AutoId])

delete  from  [dbo].[ManagePageAccess] where ModuleAutoId not in (select AutoId from Module)
ALTER TABLE Module
ADD PRIMARY KEY (AutoId);
ALTER TABLE [dbo].[ManagePageAccess]  WITH CHECK ADD  CONSTRAINT [FK_ManagePageAccess_Module] FOREIGN KEY([ModuleAutoId])
REFERENCES [dbo].[Module] ([AutoId])

delete from VenderPaymentOrderDetails where PaymentAutoId not in (select AutoId from VendorPayMaster)
delete from VenderPaymentOrderDetails where OrderAutoId not in (select AutoId from OrderMaster)
ALTER TABLE [dbo].[VenderPaymentOrderDetails]  WITH CHECK ADD  CONSTRAINT [FK_VenderPaymentOrderDetails_OrderMaster] FOREIGN KEY([OrderAutoId])
REFERENCES [dbo].[OrderMaster] ([AutoId])

ALTER TABLE [dbo].[VenderPaymentOrderDetails]  WITH CHECK ADD  CONSTRAINT [FK_VenderPaymentOrderDetails_VendorPayMaster] FOREIGN KEY([PaymentAutoId])
REFERENCES [dbo].[VendorPayMaster] ([AutoId])


delete from [ProductPricingInPriceLevel_log] where [PriceLevelAutoId]  not in (select AutoId from PriceLevelMaster)
delete from [ProductPricingInPriceLevel_log] where [ProductAutoId]  not in (select AutoId from ProductMaster)
ALTER TABLE [dbo].[ProductPricingInPriceLevel_log]  WITH CHECK ADD  CONSTRAINT [FK_ProductPricingInPriceLevel_log_PriceLevelMaster] FOREIGN KEY([PriceLevelAutoId])
REFERENCES [dbo].[PriceLevelMaster] ([AutoId])

ALTER TABLE [dbo].[ProductPricingInPriceLevel_log]  WITH CHECK ADD  CONSTRAINT [FK_ProductPricingInPriceLevel_log_ProductMaster] FOREIGN KEY([ProductAutoId])
REFERENCES [dbo].[ProductMaster] ([AutoId])

ALTER TABLE [dbo].[ProductPricingInPriceLevel_log]  WITH CHECK ADD  CONSTRAINT [FK_ProductPricingInPriceLevel_log_UnitMaster] FOREIGN KEY([UnitAutoId])
REFERENCES [dbo].[UnitMaster] ([AutoId])
 
