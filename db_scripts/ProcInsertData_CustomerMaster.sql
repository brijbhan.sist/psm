USE [psmct.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcInsertData_CustomerMaster]    Script Date: 12/2/2020 4:33:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[ProcInsertData_CustomerMaster]                                                                                                  
@Opcode INT=Null,                                                                                                  
@CustomerAutoId INT=NULL,
@db_name varchar(max)=null,
@sql_query varchar(max)='',
@isException BIT OUT,                                                                                                  
@exceptionMessage VARCHAR(max) OUT                                                                      
AS                                                                                                  
BEGIN                                                                                                  
 BEGIN TRY                                                                               
 SET @isException=0                                                                                                  
 SET @exceptionMessage='Success!!'     
  
 IF @Opcode=11                                                                                 
  BEGIN          
   set @db_name=(select [ChildDB_Name] from CompanyDetails)
   if @db_name is not null
	begin
 
	 set @sql_query='SET IDENTITY_INSERT ['+@db_name+'].[dbo].[CustomerMaster] ON;
	 insert into ['+@db_name+'].[dbo].[CustomerMaster](AutoId,[CustomerId],[CustomerName],[CustomerType],[DefaultBillAdd],[DefaultShipAdd],[SalesPersonAutoId],[Status],[Terms]
	,[TaxId],[LastOrderDate],[BusinessName],[OPTLicence],[CreatedBy],[CreatedOn],[updatedBy],[UpdateOn],[WebStatus],[LocationAutoId],[UserName],[Password],[IsAppLogin]
	,[OptimotwFrom],[OptimotwTo],CustomerRemarks) 
	select AutoId,[CustomerId],[CustomerName],[CustomerType],[DefaultBillAdd],[DefaultShipAdd],[SalesPersonAutoId],[Status],[Terms]
	,[TaxId],[LastOrderDate],[BusinessName],[OPTLicence],[CreatedBy],[CreatedOn],[updatedBy],[UpdateOn],[WebStatus],[LocationAutoId],[UserName],[Password],[IsAppLogin]
	,[OptimotwFrom],[OptimotwTo],CustomerRemarks from [dbo].[CustomerMaster] where AutoId='+convert(varchar(10),@CustomerAutoId)+'
	SET IDENTITY_INSERT ['+@db_name+'].[dbo].[CustomerMaster] OFF;'
	 
	exec (@sql_query) 
	
	set @sql_query='SET IDENTITY_INSERT ['+@db_name+'].[dbo].[ShippingAddress] ON;
	insert into ['+@db_name+'].[dbo].[ShippingAddress] (AutoId,[CustomerAutoId],[Address],[State],[Zipcode],[IsDefault],[SCityAutoId],[ZipcodeAutoid],[SA_Lat],[SA_Long],[Address2])
	select AutoId,[CustomerAutoId],[Address],[State],[Zipcode],[IsDefault],[SCityAutoId],[ZipcodeAutoid],[SA_Lat],[SA_Long],[Address2] from [dbo].[ShippingAddress]
	where CustomerAutoId='+convert(varchar(10),@CustomerAutoId)+'
	SET IDENTITY_INSERT ['+@db_name+'].[dbo].[ShippingAddress] OFF;'
	exec (@sql_query)
           

	set @sql_query='SET IDENTITY_INSERT ['+@db_name+'].[dbo].[BillingAddress] ON;
	insert into ['+@db_name+'].[dbo].[BillingAddress] ([AutoId],CustomerAutoId,[Address],[State],[BCityAutoId],[Zipcode],[IsDefault],ZipcodeAutoid,BillSA_Lat,BillSA_Long,[Address2])
	select [AutoId],CustomerAutoId,[Address],[State],[BCityAutoId],[Zipcode],[IsDefault],ZipcodeAutoid,BillSA_Lat,BillSA_Long,[Address2] from [BillingAddress]
	where CustomerAutoId='+convert(varchar(10),@CustomerAutoId)+'
    SET IDENTITY_INSERT ['+@db_name+'].[dbo].[BillingAddress] OFF;'
	exec (@sql_query)
	 
	 -------------------------------------------[CustomerContactPerson]--------------------------------------------------------------------- 
    set @sql_query='SET IDENTITY_INSERT ['+@db_name+'].[dbo].[CustomerContactPerson] ON;
	insert into ['+@db_name+'].[dbo].[CustomerContactPerson] (AutoId,[ContactPerson],[Type],[MobileNo],[Landline],[Landline2],[Fax],[Email],[AlternateEmail],[CustomerAutoId],[ISDefault])
	select AutoId,[ContactPerson],[Type],[MobileNo],[Landline],[Landline2],[Fax],[Email],[AlternateEmail],[CustomerAutoId],[ISDefault] from [CustomerContactPerson]
	where CustomerAutoId='+convert(varchar(10),@CustomerAutoId)+'
    SET IDENTITY_INSERT ['+@db_name+'].[dbo].[CustomerContactPerson] OFF;'
	exec (@sql_query)



	set @sql_query='SET IDENTITY_INSERT ['+@db_name+'].[dbo].[PriceLevelMaster] ON;
	INSERT INTO ['+@db_name+'].[dbo].[PriceLevelMaster](AutoId,[PriceLevelId],[PriceLevelName],[Status],[CreatedDate],[CreatedBy],[PCustomerType])
	SELECT AutoId,[PriceLevelId],[PriceLevelName],[Status],[CreatedDate],[CreatedBy],[PCustomerType] from [dbo].[PriceLevelMaster]
	where AutoId in  (
	SELECT CPL.PriceLevelAutoId FROM [dbo].CustomerPriceLevel AS CPL WHERE CPL.CustomerAutoId ='+convert(varchar(10),@CustomerAutoId)+'
	) 
	AND AutoId NOT IN (SELECT AUTOID FROM ['+@db_name+'].[DBO].PriceLevelMaster)
	SET IDENTITY_INSERT ['+@db_name+'].[dbo].[PriceLevelMaster] OFF;'
	exec (@sql_query)                                                            
  
	set @sql_query='SET IDENTITY_INSERT ['+@db_name+'].[dbo].[CustomerPriceLevel] ON;
	insert into ['+@db_name+'].[dbo].[CustomerPriceLevel](AutoId,[CustomerAutoId],[PriceLevelAutoId],[AssignedOn],[AssignedBy]) select 
	AutoId,[CustomerAutoId],[PriceLevelAutoId],[AssignedOn],[AssignedBy] from [dbo].[CustomerPriceLevel] 
	where CustomerAutoId='+convert(varchar(10),@CustomerAutoId)+'
	SET IDENTITY_INSERT ['+@db_name+'].[dbo].[CustomerPriceLevel] OFF;'
	exec (@sql_query)

  
	set @sql_query='SET IDENTITY_INSERT ['+@db_name+'].[dbo].[CustomerCreditMaster] ON;
	insert into ['+@db_name+'].[dbo].[CustomerCreditMaster](AutoId,CustomerAutoId,CreditAmount) select AutoId,CustomerAutoId,CreditAmount from 
	[dbo].[CustomerCreditMaster] where CustomerAutoId='+convert(varchar(10),@CustomerAutoId)+'
	SET IDENTITY_INSERT ['+@db_name+'].[dbo].[CustomerCreditMaster] OFF;'
	exec (@sql_query)
  END                               
  END                                                                                                       
    IF @Opcode=21                                                                                                  
  BEGIN                  
    set @db_name=(select [ChildDB_Name] from CompanyDetails)
   if @db_name is not null
	begin                                
	 
		set @sql_query='update cmm set [CustomerName]=cm.CustomerName,[CustomerType]=cm.CustomerType                                                                                                  
		,[Status]=cm.Status, [SalesPersonAutoId]=cm.SalesPersonAutoId, [Terms]=cm.Terms,                                                   
		[BusinessName]=cm.BusinessName,[TaxId]=cm.TaxId, OPTLicence=cm.OPTLicence,CustomerRemarks=cm.CustomerRemarks,
		LocationAutoId=cm.LocationAutoId,UpdateOn=cm.UpdateOn,UpdatedBy=cm.UpdatedBy,OptimotwFrom=cm.OptimotwFrom,OptimotwTo=cm.OptimotwTo
		from ['+@db_name+'].[dbo].[CustomerMaster] as cmm inner join [dbo].[CustomerMaster] as cm on cmm.AutoId=cm.AutoId  
		where cm.AutoId='+convert(varchar(10),@CustomerAutoId)+';'
		exec (@sql_query)


		set @sql_query='update baa set [Address] = ba.Address,[State] = ba.State,[BCityAutoId]=ba.BCityAutoId,[Zipcode]=ba.Zipcode,
		ZipcodeAutoid=ba.ZipcodeAutoid,BillSA_Lat=ba.BillSA_Lat,BillSA_Long=ba.BillSA_Long from ['+@db_name+'].[dbo].[BillingAddress] as baa 
		inner join [dbo].[BillingAddress] as ba on baa.AutoId=ba.AutoId 
		where ba.CustomerAutoId='+convert(varchar(10),@CustomerAutoId)+''
		exec (@sql_query) 
        
 
		set @sql_query='update saa set [Address]=sa.Address,[State]=sa.State,[SCityAutoId]=sa.SCityAutoId,[Zipcode]=sa.Zipcode,ZipcodeAutoid=sa.ZipcodeAutoid,
		SA_Lat=sa.SA_Lat,SA_Long=sa.SA_Long from '+@db_name+'.[dbo].[ShippingAddress] as saa 
		inner join [psmnj.a1whm.com].[dbo].[ShippingAddress] as sa on saa.CustomerAutoId=sa.CustomerAutoId 
		where sa.CustomerAutoId='+convert(varchar(10),@CustomerAutoId)+''
		exec (@sql_query) 
	                                                                                             
 END                                                                           
 end
  END TRY                                                                                                  
  BEGIN CATCH                                                                                                  
   SET @isException=1                                                           
   SET @exceptionMessage='Oops! Something went wrong.Please try later.'                                                  
  END CATCH                                                                                                                                                                    
END 
