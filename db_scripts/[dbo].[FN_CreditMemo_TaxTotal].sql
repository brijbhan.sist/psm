USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Order_TaxTotal]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

alter table CreditMemoMaster drop column TotalTax
GO
CREATE or ALTER FUNCTION  [dbo].[FN_CreditMemo_TaxTotal]
(
	@CreditAutoId int
)
RETURNS decimal(18,2)
AS
BEGIN
	DECLARE @TaxTotal decimal(18,2)	=0.00
	DECLARE @CreditMemoType int=(SELECT CreditMemoType from CreditMemoMaster where CreditAutoId=@CreditAutoId)
	DECLARE @TaxValue DECIMAL(18,2)=(SELECT TaxValue FROM CreditMemoMaster WHERE CreditAutoId=@CreditAutoId)
	DECLARE @discount DECIMAL(18,2)=(ISNULL((SELECT OverallDisc FROM CreditMemoMaster WHERE CreditAutoId=@CreditAutoId),0)*.01)
	if EXISTS(SELECT CreditAutoId FROM CreditMemoMaster WHERE CreditAutoId=@CreditAutoId)
	BEGIN
		if EXISTS(SELECT CreditAutoId FROM CreditMemoMaster WHERE CreditAutoId=@CreditAutoId and ISNULL(TotalTax_Old,0)>0)
		BEGIN
			SET @TaxTotal=ISNULL((SELECT TotalTax_Old FROM CreditMemoMaster WHERE CreditAutoId=@CreditAutoId),0) 
		END	
		else if EXISTS(select CreditMemoType from CreditMemoMaster where CreditAutoId=@CreditAutoId and ISnull(CreditMemoType,1)=2)
		BEGIN
			SET @TaxTotal=ISNULL((select  ISNULL(CAST(SUM((NetAmount-(NetAmount * @discount))*@TaxValue/100) AS decimal(18,2)),0) from CreditItemMaster where TaxRate=1 
			and CreditAutoId=@CreditAutoId),0)
		END		
	END

RETURN @TaxTotal
END
GO
alter table CreditMemoMaster add [TotalTax]  AS ([dbo].[FN_CreditMemo_TaxTotal]([CreditAutoId]))
GO