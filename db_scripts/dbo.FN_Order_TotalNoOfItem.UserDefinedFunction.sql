Create FUNCTION  [dbo].[FN_Order_TotalNoOfItem]  
(  
  @AutoId int  
)  
RETURNS int  
AS  
BEGIN  
 DECLARE @Total int  
  SET @Total=(select count(AutoId) from OrderItemMaster where orderAutoid=@AutoId)   
 RETURN @Total  
END 