USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Order_IsMLtaxApply_shipping]    Script Date: 04-24-2021 05:38:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION  [dbo].[FN_Order_IsMLtaxApply_shipping]  
(   
  @orderAutoId int,
  @EnabledTax int
   
)  
RETURNS INT  
AS  
BEGIN  
 DECLARE @Isapply int=0  
	IF @EnabledTax=1
	BEGIN
			if exists(select * from MLTaxMaster where TaxState in (select State from  BillingAddress where AutoId in (select BillAddrAutoId from OrderMaster where AutoId=@orderAutoId)))  
			begin  
				set @Isapply=1   
			END 
			DECLARE @isMLManualyApply int=ISNULL((select isMLManualyApply from OrderMaster where AutoId=@orderAutoId),1) 
			if @isMLManualyApply=0
			begin
				set @Isapply=0
			end
	END
 RETURN @Isapply  
END
