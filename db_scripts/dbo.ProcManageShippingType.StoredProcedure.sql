ALTER PROCEDURE [dbo].[ProcManageShippingType]                                                                                                      
@Opcode INT=NULL,                                                                                                      
@AutoId INT=NULL,      
@Status INT=NULL,      
@ShippingType varchar(max)=NULL,                            
@EnableTax INT=NULL,                                                                                  
@isException bit out,           
@BulkOrderAutoId varchar(max)=null,                                                                                                     
@exceptionMessage varchar(max) out                                                                                                      
AS                                                                                                      
BEGIN                                                                                                     
  BEGIN TRY                                                                                                      
    Set @isException=0                                                                                                      
    Set @exceptionMessage='Success'                        
 IF @Opcode=41                          
	BEGIN                       
	select * from ShippingType             
	END
	ELSE IF @Opcode=11
	BEGIN
		IF exists(SELECT ShippingType FROM ShippingType WHERE ShippingType = @ShippingType)        
		BEGIN        
		set @isException=1        
		set @exceptionMessage='Shipping Type Already Exists'        
		END        
		ELSE
		BEGIN                       
		INSERT INTO ShippingType  (ShippingType,EnabledTax,Shippingstatus) VALUES(@ShippingType,@EnableTax,@Status)  ;   
		END
	END
	ELSE IF @Opcode=21
	BEGIN
		IF exists(SELECT ShippingType FROM ShippingType WHERE ShippingType = @ShippingType AND AutoId != @AutoId)        
		BEGIN        
		set @isException=1        
		set @exceptionMessage='Shipping Type Already Exists'        
		END        
		ELSE
		BEGIN                       
		UPDATE ShippingType SET ShippingType=@ShippingType,Shippingstatus=@Status where AutoId=@AutoId ;   
		END
	END
	ELSE IF @Opcode=31
	BEGIN
	select * from ShippingType where AutoId=@AutoId
	END
	--ELSE IF @Opcode=42
	--BEGIN
	--DELETE from ShippingType where AutoId=@AutoId
	--END
  END TRY                                                                                                      
  BEGIN CATCH                                              
  Set @isException=1                                                                     
  Set @exceptionMessage=ERROR_MESSAGE()                                                                                                   
  END CATCH                                                                                           
END 