USE [psmnj.a1whm.com]
GO

/****** Object:  UserDefinedTableType [dbo].[DT_SalesOrderItemList]    Script Date: 11-30-2020 23:41:12 ******/
DROP PROCEDURE [dbo].[ProcOrderMaster]     
DROP TYPE [dbo].[DT_SalesOrderItemList]
GO

/****** Object:  UserDefinedTableType [dbo].[DT_SalesOrderItemList]    Script Date: 11-30-2020 23:41:12 ******/
CREATE TYPE [dbo].[DT_SalesOrderItemList] AS TABLE(
	[ProductAutoId] [int] NULL,
	[UnitAutoId] [int] NULL,
	[QtyPerUnit] [int] NULL,
	[RequiredQty] [int] NULL,
	[Barcode] [varchar](50) NULL,
	[QtyShip] [int] NULL,
	[TotalPieces] [int] NULL,
	[UnitPrice] [decimal](18, 2) NULL,
	[SRP] [decimal](18, 2) NULL,
	[GP] [decimal](18, 2) NULL,
	[Tax] [int] NULL,
	[IsExchange] [int] NULL,
	[IsFreeItem] [int] NULL,
	[NetPrice] [decimal](18, 2) NULL,
	[OM_MinPrice] [decimal](18, 2) NULL,
	[OM_CostPrice] [decimal](18, 2) NULL,
	[OM_BasePrice] [decimal](18, 2) NULL,
	Oim_Discount [decimal](18, 2) NULL,
	Oim_DiscountAmount [decimal](18, 2) NULL
)
GO


