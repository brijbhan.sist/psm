USE [psmnj.a1whm.com]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create FUNCTION  [dbo].[FN_CalculateDefaultAffectedStock]
(
	 @AutoID int
)
RETURNS varchar(50)
AS
BEGIN
	DECLARE @Stock decimal(10,2)	
	SET @Stock=(
	Select    ISNULL((PSU.NewStock-PSU.OledStock)/ISNULL(PD.Qty,0),0) from ProductMaster as PM 
	INNER JOIN PackingDetails as PD on PD.ProductAutoId=PM.AutoId AND PD.UnitType=PM.PackingAutoId
	INNER JOIN ProductStockUpdateLog as PSU on PSU.ProductId=PM.ProductId
	INNER JOIN UnitMaster as UM on UM.AutoId=PD.UnitType
	where PSU.AutoId=@AutoID 
	)
	RETURN @Stock
END
Drop function FN_CalculateDefaultAffectedStock
Alter table ProductStockUpdateLog drop column DefaultAffectedStock
Alter table ProductStockUpdateLog add DefaultAffectedStock AS ([dbo].[FN_CalculateDefaultAffectedStock](AutoId))