   




ALTER TABLE [dbo].[Delivered_Order_Items]  drop column Del_ItemTotal
Alter table Delivered_Order_Items Alter column UnitPrice decimal(18,2)
go
Drop function FN_Delivered_ItemTotal
go
CREATE FUNCTION  [dbo].[FN_Delivered_ItemTotal]
(
	 @AutoId int 
)
RETURNS decimal(18,2)
AS
BEGIN
	DECLARE @NetAmount decimal(18,2)		 	 
	SET @NetAmount=cast((SELECT ((UnitPrice/QtyPerUnit) * QtyDel) FROM [Delivered_Order_Items] WHERE AutoId=@AutoId) as decimal	(18,2))	 
	RETURN @NetAmount
END

GO

ALTER TABLE [dbo].[Delivered_Order_Items] ADD  Del_ItemTotal  AS ([dbo].[FN_Delivered_ItemTotal]([AutoId]))
Alter table Delivered_Order_Items Alter column SRP decimal(18,2)
Alter table Delivered_Order_Items Alter column GP decimal(18,2)
Alter table Delivered_Order_Items Alter column TaxValue decimal(18,2)
Alter table Delivered_Order_Items Alter column UnitMLQty decimal(18,2)
Alter table Delivered_Order_Items Alter column Del_CostPrice decimal(18,2)
Alter table Delivered_Order_Items Alter column Del_CommCode decimal(18,2)
Alter table Delivered_Order_Items Alter column Del_Weight_Oz decimal(18,2)
Alter table Delivered_Order_Items Alter column Del_MinPrice decimal(18,2)
Alter table Delivered_Order_Items Alter column BasePrice decimal(18,2)
Alter table Delivered_Order_Items Alter column Del_discount decimal(18,2)
Alter table Delivered_Order_Items Alter column StaticNetPrice decimal(18,2)


Alter table DraftOrderMaster
Alter column OverallDisc decimal(18,2)

Alter table DraftItemMaster
Alter column UnitMLQty decimal(18,2)

Alter table DraftItemMaster
Alter column TotalMLQty decimal(18,2)

ALTER TABLE [dbo].[ProductPricingInPriceLevel_log] alter column CustomPrice decimal(18,2)
ALTER TABLE [dbo].[ProductPricingInPriceLevel] alter column CustomPrice decimal(18,2)
