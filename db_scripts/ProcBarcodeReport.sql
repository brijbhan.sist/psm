Create or alter procedure ProcBarcodeReport --exec ProcBarcodeReport

AS
BEGIN   
   
	select 'NJ vs CT' as db,njpm.ProductId,njpm.productname,njb.UnitAutoId,njb.Barcode,njb.BarcodeType,
	Format(njb.CreatedDate,'mm/dd/yyyy hh:mm tt') as CreatedDate,ctpm.ProductId as ProductId1,ctb.UnitAutoId as Unit1,ctb.Barcode as Barcode1,ctb.BarcodeType as BarcodeType1
	from [psmnj.a1whm.com].dbo.ProductMaster as njpm
	inner join [psmnj.a1whm.com].dbo.ItemBarcode as njb on njpm.AutoId=njb.ProductAutoId
	left join  [psmct.a1whm.com].dbo.ProductMaster as ctpm on njpm.ProductId=ctpm.ProductId
	left join [psmct.a1whm.com].dbo.ItemBarcode as ctb on ctpm.AutoId=ctb.ProductAutoId 
	and njb.UnitAutoId=ctb.UnitAutoId and njb.Barcode=ctb.Barcode and njb.BarcodeType=ctb.BarcodeType
	where ctb.ProductAutoId is null

	Union All
	
	select 'CT vs NJ' as db,njpm.ProductId,njpm.productname,njb.UnitAutoId,njb.Barcode,njb.BarcodeType,
	Format(njb.CreatedDate,'mm/dd/yyyy hh:mm tt') as CreatedDate,ctpm.ProductId as ProductId1,ctb.UnitAutoId as Unit1,ctb.Barcode as Barcode1,ctb.BarcodeType as BarcodeType1
	from [psmct.a1whm.com].dbo.ProductMaster as njpm
	inner join [psmct.a1whm.com].dbo.ItemBarcode as njb on njpm.AutoId=njb.ProductAutoId
	left join  [psmnj.a1whm.com].dbo.ProductMaster as ctpm on njpm.ProductId=ctpm.ProductId
	left join [psmnj.a1whm.com].dbo.ItemBarcode as ctb on ctpm.AutoId=ctb.ProductAutoId 
	and njb.UnitAutoId=ctb.UnitAutoId and njb.Barcode=ctb.Barcode and njb.BarcodeType=ctb.BarcodeType
	where ctb.ProductAutoId is null 

	Union All
	
	select 'NJ vs PA' as db,njpm.ProductId,njpm.productname,njb.UnitAutoId,njb.Barcode,njb.BarcodeType,
	Format(njb.CreatedDate,'mm/dd/yyyy hh:mm tt') as CreatedDate,ctpm.ProductId as ProductId1,ctb.UnitAutoId as Unit1,ctb.Barcode as Barcode1,ctb.BarcodeType as BarcodeType1
	from [psmnj.a1whm.com].dbo.ProductMaster as njpm
	inner join [psmnj.a1whm.com].dbo.ItemBarcode as njb on njpm.AutoId=njb.ProductAutoId
	left join  [psmpa.a1whm.com].dbo.ProductMaster as ctpm on njpm.ProductId=ctpm.ProductId
	left join [psmpa.a1whm.com].dbo.ItemBarcode as ctb on ctpm.AutoId=ctb.ProductAutoId 
	and njb.UnitAutoId=ctb.UnitAutoId and njb.Barcode=ctb.Barcode and njb.BarcodeType=ctb.BarcodeType
	where ctb.ProductAutoId is null

	Union All
	
	select 'PA vs NJ' as db,njpm.ProductId,njpm.productname,njb.UnitAutoId,njb.Barcode,njb.BarcodeType,
	Format(njb.CreatedDate,'mm/dd/yyyy hh:mm tt') as CreatedDate,ctpm.ProductId as ProductId1,ctb.UnitAutoId as Unit1,ctb.Barcode as Barcode1,ctb.BarcodeType as BarcodeType1
	from [psmpa.a1whm.com].dbo.ProductMaster as njpm
	inner join [psmpa.a1whm.com].dbo.ItemBarcode as njb on njpm.AutoId=njb.ProductAutoId
	left join  [psmnj.a1whm.com].dbo.ProductMaster as ctpm on njpm.ProductId=ctpm.ProductId
	left join [psmnj.a1whm.com].dbo.ItemBarcode as ctb on ctpm.AutoId=ctb.ProductAutoId 
	and njb.UnitAutoId=ctb.UnitAutoId and njb.Barcode=ctb.Barcode and njb.BarcodeType=ctb.BarcodeType
	where ctb.ProductAutoId is null

	Union All
	
	select 'NJ vs NPA' as db,njpm.ProductId,njpm.productname,njb.UnitAutoId,njb.Barcode,njb.BarcodeType,
	Format(njb.CreatedDate,'mm/dd/yyyy hh:mm tt') as CreatedDate,ctpm.ProductId as ProductId1,ctb.UnitAutoId as Unit1,ctb.Barcode as Barcode1,ctb.BarcodeType as BarcodeType1
	from [psmnj.a1whm.com].dbo.ProductMaster as njpm
	inner join [psmnj.a1whm.com].dbo.ItemBarcode as njb on njpm.AutoId=njb.ProductAutoId
	left join  [psmnpa.a1whm.com].dbo.ProductMaster as ctpm on njpm.ProductId=ctpm.ProductId
	left join [psmnpa.a1whm.com].dbo.ItemBarcode as ctb on ctpm.AutoId=ctb.ProductAutoId 
	and njb.UnitAutoId=ctb.UnitAutoId and njb.Barcode=ctb.Barcode and njb.BarcodeType=ctb.BarcodeType
	where ctb.ProductAutoId is null

	Union All
	
	select 'NPA vs NJ' as db,njpm.ProductId,njpm.productname,njb.UnitAutoId,njb.Barcode,njb.BarcodeType,
	Format(njb.CreatedDate,'mm/dd/yyyy hh:mm tt') as CreatedDate,ctpm.ProductId as ProductId1,ctb.UnitAutoId as Unit1,ctb.Barcode as Barcode1,ctb.BarcodeType as BarcodeType1
	from [psmnpa.a1whm.com].dbo.ProductMaster as njpm
	inner join [psmnpa.a1whm.com].dbo.ItemBarcode as njb on njpm.AutoId=njb.ProductAutoId
	left join  [psmnj.a1whm.com].dbo.ProductMaster as ctpm on njpm.ProductId=ctpm.ProductId
	left join [psmnj.a1whm.com].dbo.ItemBarcode as ctb on ctpm.AutoId=ctb.ProductAutoId 
	and njb.UnitAutoId=ctb.UnitAutoId and njb.Barcode=ctb.Barcode and njb.BarcodeType=ctb.BarcodeType
	where ctb.ProductAutoId is null

	Union All
	
	select 'NJ vs WPA' as db,njpm.ProductId,njpm.productname,njb.UnitAutoId,njb.Barcode,njb.BarcodeType,
	Format(njb.CreatedDate,'mm/dd/yyyy hh:mm tt') as CreatedDate,ctpm.ProductId as ProductId1,ctb.UnitAutoId as Unit1,ctb.Barcode as Barcode1,ctb.BarcodeType as BarcodeType1
	from [psmnj.a1whm.com].dbo.ProductMaster as njpm
	inner join [psmnj.a1whm.com].dbo.ItemBarcode as njb on njpm.AutoId=njb.ProductAutoId
	left join  [psmwpa.a1whm.com].dbo.ProductMaster as ctpm on njpm.ProductId=ctpm.ProductId
	left join [psmwpa.a1whm.com].dbo.ItemBarcode as ctb on ctpm.AutoId=ctb.ProductAutoId 
	and njb.UnitAutoId=ctb.UnitAutoId and njb.Barcode=ctb.Barcode and njb.BarcodeType=ctb.BarcodeType
	where ctb.ProductAutoId is null

	Union All
	
	select 'WPA vs NJ' as db,njpm.ProductId,njpm.productname,njb.UnitAutoId,njb.Barcode,njb.BarcodeType,
	Format(njb.CreatedDate,'mm/dd/yyyy hh:mm tt') as CreatedDate,ctpm.ProductId as ProductId1,ctb.UnitAutoId as Unit1,ctb.Barcode as Barcode1,ctb.BarcodeType as BarcodeType1
	from [psmWpa.a1whm.com].dbo.ProductMaster as njpm
	inner join [psmwpa.a1whm.com].dbo.ItemBarcode as njb on njpm.AutoId=njb.ProductAutoId
	left join  [psmnj.a1whm.com].dbo.ProductMaster as ctpm on njpm.ProductId=ctpm.ProductId
	left join [psmnj.a1whm.com].dbo.ItemBarcode as ctb on ctpm.AutoId=ctb.ProductAutoId 
	and njb.UnitAutoId=ctb.UnitAutoId and njb.Barcode=ctb.Barcode and njb.BarcodeType=ctb.BarcodeType
	where ctb.ProductAutoId is null

	Union All
	
	select 'NJ vs NY' as db,njpm.ProductId,njpm.productname,njb.UnitAutoId,njb.Barcode,njb.BarcodeType,
	Format(njb.CreatedDate,'mm/dd/yyyy hh:mm tt') as CreatedDate,ctpm.ProductId as ProductId1,ctb.UnitAutoId as Unit1,ctb.Barcode as Barcode1,ctb.BarcodeType as BarcodeType1
	from [psmnj.a1whm.com].dbo.ProductMaster as njpm
	inner join [psmnj.a1whm.com].dbo.ItemBarcode as njb on njpm.AutoId=njb.ProductAutoId
	left join  [psmny.a1whm.com].dbo.ProductMaster as ctpm on njpm.ProductId=ctpm.ProductId
	left join [psmny.a1whm.com].dbo.ItemBarcode as ctb on ctpm.AutoId=ctb.ProductAutoId 
	and njb.UnitAutoId=ctb.UnitAutoId and njb.Barcode=ctb.Barcode and njb.BarcodeType=ctb.BarcodeType
	where ctb.ProductAutoId is null

	Union All
	
	select 'NY vs NJ' as db,njpm.ProductId,njpm.productname,njb.UnitAutoId,njb.Barcode,njb.BarcodeType,
	Format(njb.CreatedDate,'mm/dd/yyyy hh:mm tt') as CreatedDate,ctpm.ProductId as ProductId1,ctb.UnitAutoId as Unit1,ctb.Barcode as Barcode1,ctb.BarcodeType as BarcodeType1
	from [psmny.a1whm.com].dbo.ProductMaster as njpm
	inner join [psmny.a1whm.com].dbo.ItemBarcode as njb on njpm.AutoId=njb.ProductAutoId
	left join  [psmnj.a1whm.com].dbo.ProductMaster as ctpm on njpm.ProductId=ctpm.ProductId
	left join [psmnj.a1whm.com].dbo.ItemBarcode as ctb on ctpm.AutoId=ctb.ProductAutoId 
	and njb.UnitAutoId=ctb.UnitAutoId and njb.Barcode=ctb.Barcode and njb.BarcodeType=ctb.BarcodeType
	where ctb.ProductAutoId is null
END