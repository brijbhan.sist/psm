USE [psmpa.a1whm.com]
GO
SET IDENTITY_INSERT [dbo].[Module] ON 
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (1, N'MOD00001', N'Manage Application', 0, 0, N'la la-gear', 1, CAST(N'2020-02-11' AS Date), 1, CAST(N'2020-03-11' AS Date), 1, 2)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (2, N'MOD00002', N'Email Receiver Master', 1, 1, N'', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (3, N'MOD00003', N'Manage Car', 1, 1, N'', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (4, N'MOD00004', N'Manage Commission Code', 1, 1, N'', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (5, N'MOD00005', N'Manage Company Profile', 1, 1, N'', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (6, N'MOD00006', N'Manage Currency', 1, 1, N'', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (7, N'MOD00007', N'Manage Employee', 1, 1, N'', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (8, N'MOD00008', N'Manage Module', 1, 1, N'', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (9, N'MOD00009', N'Manage Page', 1, 1, N'', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (10, N'MOD00010', N'Manage Page Access', 1, 1, N'', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (11, N'MOD00011', N'Manage Route', 1, 1, N'', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (12, N'MOD00012', N'Manage Terms', 1, 1, N'', 1, CAST(N'2020-02-11' AS Date), 1, CAST(N'2020-02-11' AS Date), 1, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (13, N'MOD00013', N'Manage Zone', 1, 1, N'', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (14, N'MOD00014', N'Parameter Setting', 1, 1, N'', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (15, N'MOD00015', N'Manage Credit Memo', 0, 0, N'la la-file-text', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (16, N'MOD00016', N'Credit Memo List', 1, 15, N'', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (17, N'MOD00017', N'Manage Customer', 0, 0, N'la la-user', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (18, N'MOD00018', N'Customer List', 1, 17, N'la la-server', 1, CAST(N'2020-02-11' AS Date), 1, CAST(N'2020-03-10' AS Date), 1, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (19, N'MOD00019', N'Manage Price Level', 1, 17, N'', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (20, N'MOD00020', N'Manage Inventory', 0, 0, N'la la-taxi', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (21, N'MOD00021', N'Update Stock', 1, 20, N'', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (22, N'MOD00022', N'Manage Order', 0, 0, N'la la-taxi', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (24, N'MOD00024', N'Manage Message', 1, 22, N'', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (26, N'MOD00026', N'By Internal', 1, 127, N'', 1, CAST(N'2020-02-11' AS Date), 1, CAST(N'2020-02-24' AS Date), 1, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (30, N'MOD00030', N'By Vendor(New)', 1, 127, N'', 1, CAST(N'2020-02-11' AS Date), 1, CAST(N'2020-02-19' AS Date), 1, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (35, N'MOD00035', N'Manage Product', 0, 0, N'la la-taxi', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (36, N'MOD00036', N'Manage Brand', 1, 35, N'', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (37, N'MOD00037', N'Manage Category', 1, 35, N'', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (38, N'MOD00038', N'Manage ML Quantity', 1, 35, N'', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (39, N'MOD00039', N'Manage Sub-Category', 1, 35, N'', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (40, N'MOD00040', N'Product Allocation', 1, 35, N'', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (41, N'MOD00041', N'Product List', 1, 35, N'', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (42, N'MOD00042', N'Product Request List', 1, 35, N'', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (44, N'MOD00044', N'Manage ML Tax', 1, 1, N'', 1, CAST(N'2020-02-11' AS Date), 1, CAST(N'2020-02-17' AS Date), 1, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (45, N'MOD00045', N'Manage Weight Tax', 1, 1, N'', 1, CAST(N'2020-02-11' AS Date), 1, CAST(N'2020-02-17' AS Date), 1, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (46, N'MOD00046', N'Manage Vendor', 0, 0, N'la la-taxi', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (47, N'MOD00047', N'Vendor List', 1, 46, N'', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (48, N'MOD00048', N'Vendor Payment', 1, 46, N'', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (49, N'MOD00049', N'Reports', 0, 0, N'la la-eye', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (50, N'MOD00050', N'Account Report', 1, 49, N'', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (51, N'MOD00051', N'By Bank Report', 1, 50, N'', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (52, N'MOD00052', N'By Customer Credit ', 1, 50, N'', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (53, N'MOD00053', N'By Customer Statement', 1, 50, N'', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (54, N'MOD00054', N'By Daily Order Payment', 1, 50, N'', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (55, N'MOD00055', N'By Order Payment', 1, 50, N'', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (56, N'MOD00056', N'By Payment History', 1, 50, N'', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (57, N'MOD00057', N'By Received Check', 1, 50, N'', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (58, N'MOD00058', N'Employee Commission', 1, 49, N'', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (59, N'MOD00059', N'Details', 1, 58, N'', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (60, N'MOD00060', N'summary', 1, 58, N'', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (61, N'MOD00061', N'Inventory Report', 1, 49, N'', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (62, N'MOD00062', N'Product Receive', 1, 61, N'', 1, CAST(N'2020-02-11' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (64, N'MOD00064', N'ML Tax Report', 1, 49, N'', 1, CAST(N'2020-02-13' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (65, N'MOD00065', N'By Customer', 1, 64, N'', 1, CAST(N'2020-02-13' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (66, N'MOD00066', N'By Product', 1, 64, N'', 1, CAST(N'2020-02-13' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (67, N'MOD00067', N'Not Shipped Report', 1, 49, N'', 1, CAST(N'2020-02-13' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (68, N'MOD00068', N'By Order', 1, 67, N'', 1, CAST(N'2020-02-13' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (69, N'MOD00069', N'By Product', 1, 67, N'', 1, CAST(N'2020-02-13' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (70, N'MOD00070', N'Open Balance Report ', 1, 49, N'', 1, CAST(N'2020-02-13' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (71, N'MOD00071', N'By Customer', 1, 70, N'', 1, CAST(N'2020-02-13' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (72, N'MOD00072', N'By Due ', 1, 70, N'', 1, CAST(N'2020-02-13' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (73, N'MOD00073', N'By Order', 1, 70, N'', 1, CAST(N'2020-02-13' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (74, N'MOD00074', N'OPT Report', 1, 49, N'', 1, CAST(N'2020-02-13' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (75, N'MOD00075', N'By VAPE', 1, 74, N'', 1, CAST(N'2020-02-13' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (76, N'MOD00076', N'By Weight', 1, 74, N'', 1, CAST(N'2020-02-13' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (77, N'MOD00077', N'Order Submit', 1, 49, N'', 1, CAST(N'2020-02-13' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (78, N'MOD00078', N'By Date And Time', 1, 77, N'', 1, CAST(N'2020-02-13' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (79, N'MOD00079', N'Packer Report', 1, 49, N'', 1, CAST(N'2020-02-13' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (80, N'MOD00080', N'By Order', 1, 79, N'', 1, CAST(N'2020-02-13' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (81, N'MOD00081', N'By Pick Product', 1, 79, N'', 1, CAST(N'2020-02-13' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (82, N'MOD00082', N'By Summary', 1, 79, N'', 1, CAST(N'2020-02-13' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (83, N'MOD00083', N'Ready to Ship', 1, 79, N'', 1, CAST(N'2020-02-13' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (84, N'MOD00084', N'Product Report', 1, 49, N'', 1, CAST(N'2020-02-13' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (85, N'MOD00085', N'By Barcode', 1, 84, N'', 1, CAST(N'2020-02-13' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (86, N'MOD00086', N'By Catalog', 1, 84, N'', 1, CAST(N'2020-02-13' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (87, N'MOD00087', N'By Driver Return', 1, 84, N'', 1, CAST(N'2020-02-13' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (88, N'MOD00088', N'By Exchange', 1, 84, N'', 1, CAST(N'2020-02-13' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (89, N'MOD00089', N'P&L Report', 1, 49, N'', 1, CAST(N'2020-02-13' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (90, N'MOD00090', N'By Customer', 1, 89, N'', 1, CAST(N'2020-02-13' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (91, N'MOD00091', N'By Product', 1, 89, N'', 1, CAST(N'2020-02-13' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (92, N'MOD00092', N'Sales Report', 1, 49, N'', 1, CAST(N'2020-02-13' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (93, N'MOD00093', N'By Product', 1, 92, N'', 1, CAST(N'2020-02-13' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (94, N'MOD00094', N'By Product Details', 1, 92, N'', 1, CAST(N'2020-02-13' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (95, N'MOD00095', N'By Sales Person', 1, 92, N'', 1, CAST(N'2020-02-13' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (96, N'MOD00096', N'By Top Selling', 1, 92, N'', 1, CAST(N'2020-02-13' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (97, N'MOD00097', N'Customer Sales Person', 1, 92, N'', 1, CAST(N'2020-02-13' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (98, N'MOD00098', N'Order Average', 1, 92, N'', 1, CAST(N'2020-02-13' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (99, N'MOD00099', N'By Customer', 1, 92, N'', 1, CAST(N'2020-02-13' AS Date), 1, CAST(N'2020-03-18' AS Date), 1, 0)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (100, N'MOD00100', N'Developer Help', 0, 0, N'la la-eye', 1, CAST(N'2020-02-13' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (101, N'MOD00101', N'API Request Log', 1, 100, N'', 1, CAST(N'2020-02-13' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (102, N'MOD00102', N'Invalid Barcode', 1, 100, N'', 1, CAST(N'2020-02-13' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (103, N'MOD00103', N'Ticket List', 1, 100, N'', 1, CAST(N'2020-02-13' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (104, N'MOD00104', N'Wrong Product Price List', 1, 100, N'', 1, CAST(N'2020-02-13' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (105, N'MOD00105', N'Email Server Setting', 1, 1, N'', 1, CAST(N'2020-02-13' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (106, N'MOD00106', N'Manage Tax', 1, 1, N'', 1, CAST(N'2020-02-13' AS Date), 1, CAST(N'2020-02-17' AS Date), 1, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (110, N'MOD00110', N'Inventory Onhand', 1, 61, N'', 1, CAST(N'2020-02-17' AS Date), 1, CAST(N'2020-02-24' AS Date), 1, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (111, N'MOD00111', N'Manage Check', 0, 0, N'la la-tablet', 1, CAST(N'2020-02-17' AS Date), 1, CAST(N'2020-02-17' AS Date), 1, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (112, N'MOD00112', N'Receive Payment', 0, 0, N'la la-tablet', 1, CAST(N'2020-02-17' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (113, N'MOD00113', N'My Dashboard', 0, 0, N'la la-home', 1, CAST(N'2020-02-17' AS Date), 1, CAST(N'2020-03-17' AS Date), 1, 1)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (114, N'MOD00114', N'New Order', 1, 22, N'', 1, CAST(N'2020-02-17' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (115, N'MOD00115', N'Draft Order List', 1, 22, N'', 1, CAST(N'2020-02-17' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (116, N'MOD00116', N' New Credit Memo', 1, 15, N'', 1, CAST(N'2020-02-17' AS Date), 1, CAST(N'2020-03-18' AS Date), 1, 0)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (117, N'MOD00117', N'Assigned Driver', 1, 22, N'', 1, CAST(N'2020-02-17' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (118, N'MOD00118', N'By Vendor', 1, 25, N'', 1, CAST(N'2020-02-18' AS Date), 1, CAST(N'2020-02-18' AS Date), 1, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (124, N'MOD00124', N'Order List(WM)', 1, 22, N'', 1, CAST(N'2020-02-18' AS Date), 1, CAST(N'2020-02-24' AS Date), 1, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (125, N'MOD00125', N' Order List(SM)', 1, 22, N'', 1, CAST(N'2020-02-19' AS Date), 1, CAST(N'2020-02-24' AS Date), 1, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (126, N'MOD00126', N'Order List(SP)', 1, 22, N'', 1, CAST(N'2020-02-19' AS Date), 1, CAST(N'2020-02-24' AS Date), 1, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (127, N'MOD00127', N'Manage PO', 0, 0, N'la la-taxi', 1, CAST(N'2020-02-19' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (128, N'MOD00128', N'R-Draft PO List', 1, 30, N'', 1, CAST(N'2020-02-19' AS Date), 1, CAST(N'2020-02-19' AS Date), 1, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (129, N'MOD00129', N'R-Generate PO', 1, 30, N'', 1, CAST(N'2020-02-19' AS Date), 1, CAST(N'2020-02-19' AS Date), 1, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (130, N'MOD00130', N'R-PO List', 1, 30, N'', 1, CAST(N'2020-02-19' AS Date), 1, CAST(N'2020-02-19' AS Date), 1, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (131, N'MOD00131', N'R-Receive Stock List', 1, 30, N'', 1, CAST(N'2020-02-19' AS Date), 1, CAST(N'2020-02-19' AS Date), 1, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (132, N'MOD00132', N'Draft PO List', 1, 30, N'', 1, CAST(N'2020-02-19' AS Date), 1, CAST(N'2020-02-19' AS Date), 1, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (133, N'MOD00133', N'Generate PO', 1, 30, N'', 1, CAST(N'2020-02-19' AS Date), 1, CAST(N'2020-02-19' AS Date), 1, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (134, N'MOD00134', N'PO List', 1, 30, N'', 1, CAST(N'2020-02-19' AS Date), 1, CAST(N'2020-02-19' AS Date), 1, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (135, N'MOD00135', N'Receive Stock List', 1, 30, N'', 1, CAST(N'2020-02-19' AS Date), 1, CAST(N'2020-02-19' AS Date), 1, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (136, N'MOD00136', N'Status Master', 1, 100, N'', 1, CAST(N'2020-02-19' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (137, N'MOD00137', N'My Dashboard -sp', 0, 0, N'la la-home', 1, CAST(N'2020-02-22' AS Date), 1, CAST(N'2020-03-18' AS Date), 1, 1)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (139, N'MOD00139', N'Order List (PK)', 1, 22, N'', 1, CAST(N'2020-02-24' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (140, N'MOD00140', N'Order List(D)', 1, 22, N'', 1, CAST(N'2020-02-24' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (141, N'MOD00141', N'Order List (AC)', 1, 22, N'', 1, CAST(N'2020-02-24' AS Date), 1, CAST(N'2020-02-24' AS Date), 1, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (144, N'MOD00144', N'Manage Expense', 0, 0, N'la la-repeat', 1, CAST(N'2020-02-24' AS Date), 1, CAST(N'2020-03-10' AS Date), 1, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (145, N'MOD00145', N'Manage Petty CASH', 0, 0, N'la la-tablet', 1, CAST(N'2020-02-24' AS Date), 1, CAST(N'2020-03-04' AS Date), 1, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (146, N'MOD00146', N'Product List (sp)', 1, 20, N'', 1, CAST(N'2020-02-24' AS Date), 1, CAST(N'2020-03-06' AS Date), 1, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (147, N'MOD00147', N'New Order(SP)', 1, 22, N'', 1, CAST(N'2020-02-24' AS Date), 1, CAST(N'2020-02-24' AS Date), 1, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (148, N'MOD00148', N'I-Draft PO List', 1, 26, N'', 1, CAST(N'2020-02-24' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (149, N'MOD00149', N'I-Generate PO', 1, 26, N'', 1, CAST(N'2020-02-24' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (150, N'MOD00150', N'I-PO List', 1, 26, N'', 1, CAST(N'2020-02-24' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (151, N'MOD00151', N'By Vendor(OLD)', 1, 127, N'', 1, CAST(N'2020-02-24' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (152, N'MOD00152', N'O-Draft PO List', 1, 151, N'', 1, CAST(N'2020-02-24' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (153, N'MOD00153', N'O-PO List', 1, 151, N'', 1, CAST(N'2020-02-24' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (154, N'MOD00154', N'Wrong Packing Details', 1, 100, N'', 1, CAST(N'2020-02-24' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (155, N'MOD00155', N'Device Master', 1, 1, N'', 1, CAST(N'2020-02-25' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (156, N'MOD00156', N'Draft Order List(POS)', 1, 22, N'', 1, CAST(N'2020-02-28' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (158, N'MOD00158', N'New Route', 1, 11, N'la la-terminal', 1, CAST(N'2020-03-06' AS Date), 1, CAST(N'2020-03-06' AS Date), 1, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (159, N'MOD00159', N'Route List', 1, 11, N'la la-terminal', 1, CAST(N'2020-03-06' AS Date), 1, NULL, NULL, NULL)
GO
INSERT [dbo].[Module] ([AutoId], [ModuleId], [ModuleName], [HasParent], [ParentId], [IconClass], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [SequenceNo]) VALUES (160, N'MOD00160', N'Order List (POS)', 1, 22, N'', 1, CAST(N'2020-03-10' AS Date), 1, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Module] OFF
GO
SET IDENTITY_INSERT [dbo].[PageAction] ON 
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (1, N'View', 1, 1, 0, CAST(N'2020-02-13' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (2, N'View', 2, 1, 0, CAST(N'2020-02-13' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (3, N'Veiw', 3, 1, 0, CAST(N'2020-02-13' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (4, N'Veiw', 4, 1, 0, CAST(N'2020-02-13' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (5, N'View', 5, 1, 0, CAST(N'2020-02-13' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (6, N'View', 6, 1, 0, CAST(N'2020-02-13' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (7, N'View', 7, 1, 0, CAST(N'2020-02-13' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (8, N'view', 8, 1, 0, CAST(N'2020-02-13' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (9, N'View', 9, 1, 2, CAST(N'2020-02-13' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (10, N'View', 10, 1, 0, CAST(N'2020-02-13' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (11, N'View', 11, 1, 0, CAST(N'2020-02-13' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (12, N'View', 12, 1, 0, CAST(N'2020-02-13' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (13, N'View', 13, 1, 0, CAST(N'2020-02-13' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (14, N'View', 14, 1, 0, CAST(N'2020-02-13' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (15, N'View', 15, 1, 0, CAST(N'2020-02-13' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (16, N'View', 16, 1, 0, CAST(N'2020-02-13' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (17, N'View', 17, 1, 0, CAST(N'2020-02-13' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (18, N'View', 18, 1, 0, CAST(N'2020-02-13' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (19, N'View', 19, 1, 0, CAST(N'2020-02-13' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (20, N'View', 20, 1, 0, CAST(N'2020-02-13' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (21, N'View', 21, 1, 0, CAST(N'2020-02-13' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (22, N'View', 22, 1, 0, CAST(N'2020-02-13' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (23, N'View', 23, 1, 0, CAST(N'2020-02-13' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (24, N'View', 24, 1, 0, CAST(N'2020-02-13' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (25, N'View', 25, 1, 0, CAST(N'2020-02-13' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (26, N'View', 26, 1, 0, CAST(N'2020-02-13' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (27, N'View', 27, 1, 0, CAST(N'2020-02-13' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (28, N'View', 28, 1, 0, CAST(N'2020-02-13' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (29, N'View', 29, 1, 0, CAST(N'2020-02-13' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (30, N'View', 30, 1, 0, CAST(N'2020-02-13' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (31, N'View', 31, 1, 0, CAST(N'2020-02-13' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (32, N'View', 32, 1, 0, CAST(N'2020-02-13' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (33, N'View', 33, 1, 0, CAST(N'2020-02-13' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (34, N'View', 34, 1, 0, CAST(N'2020-02-13' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (35, N'View', 35, 1, 0, CAST(N'2020-02-13' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (36, N'View', 36, 1, 0, CAST(N'2020-02-13' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (37, N'View', 37, 1, 0, CAST(N'2020-02-13' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (38, N'View', 38, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (39, N'View', 39, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (40, N'View', 40, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (41, N'View', 41, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (42, N'View', 42, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (43, N'View', 43, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (44, N'View', 44, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (45, N'View', 45, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (46, N'View', 46, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (47, N'View', 47, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (48, N'View', 48, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (49, N'View', 49, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (50, N'View', 50, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (51, N'View', 51, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (52, N'View', 52, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (53, N'View', 53, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (54, N'View', 54, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (55, N'View', 55, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (56, N'View', 56, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (57, N'View', 57, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (58, N'View', 58, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (59, N'View', 59, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (60, N'View', 60, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (61, N'View', 61, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (62, N'View', 62, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (63, N'View', 63, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (64, N'View', 64, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (65, N'View', 65, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (66, N'View', 66, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (67, N'View', 67, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (68, N'View', 68, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (69, N'View', 69, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (70, N'View', 70, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (71, N'View', 71, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (72, N'View', 72, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (73, N'View', 73, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (74, N'View', 74, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (75, N'View', 75, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (76, N'View', 76, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (77, N'View', 77, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (78, N'View', 78, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (79, N'View', 79, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (80, N'View', 80, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (81, N'View', 81, 1, 0, CAST(N'2020-02-14' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (82, N'View', 82, 1, 0, CAST(N'2020-02-17' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (83, N'View', 83, 1, 0, CAST(N'2020-02-17' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (84, N'View', 84, 1, 0, CAST(N'2020-02-17' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (85, N'view', 85, 1, 0, CAST(N'2020-02-17' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (86, N'View', 86, 1, 0, CAST(N'2020-02-17' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (87, N'View', 87, 1, 0, CAST(N'2020-02-17' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (88, N'View', 88, 1, 0, CAST(N'2020-02-17' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (89, N'View', 89, 1, 0, CAST(N'2020-02-17' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (90, N'View', 90, 1, 0, CAST(N'2020-02-18' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (91, N'View', 91, 1, 0, CAST(N'2020-02-18' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (92, N'View', 92, 0, 7, CAST(N'2020-02-18' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (93, N'View', 93, 1, 0, CAST(N'2020-02-19' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (94, N'View', 94, 1, 0, CAST(N'2020-02-19' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (95, N'View', 95, 0, 11, CAST(N'2020-02-19' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (96, N'View', 96, 0, 11, CAST(N'2020-02-19' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (97, N'View', 97, 0, 11, CAST(N'2020-02-19' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (98, N'View', 98, 0, 11, CAST(N'2020-02-19' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (99, N'View', 99, 0, 9, CAST(N'2020-02-19' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (100, N'View', 100, 0, 9, CAST(N'2020-02-19' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (101, N'View', 101, 0, 9, CAST(N'2020-02-19' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (102, N'View', 102, 0, 9, CAST(N'2020-02-19' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (103, N'View', 103, 0, 11, CAST(N'2020-02-19' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (104, N'View', 104, 1, 0, CAST(N'2020-02-19' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (105, N'View', 105, 1, 0, CAST(N'2020-02-22' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (106, N'View', 106, 0, 3, CAST(N'2020-02-24' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (107, N'View', 108, 1, 0, CAST(N'2020-02-24' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (108, N'View', 107, 1, 2, CAST(N'2020-02-24' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (109, N'View', 109, 1, 0, CAST(N'2020-02-24' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (110, N'View', 110, 1, 0, CAST(N'2020-02-24' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (111, N'View', 111, 1, 0, CAST(N'2020-02-24' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (112, N'View', 112, 1, 0, CAST(N'2020-02-24' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (113, N'View', 113, 1, 0, CAST(N'2020-02-24' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (114, N'View', 114, 1, 0, CAST(N'2020-02-24' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (115, N'View', 115, 1, 0, CAST(N'2020-02-24' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (116, N'View', 116, 1, 0, CAST(N'2020-02-24' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (117, N'View', 117, 1, 0, CAST(N'2020-02-24' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (118, N'View', 118, 1, 0, CAST(N'2020-02-24' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (119, N'View', 119, 1, 0, CAST(N'2020-02-24' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (120, N'View', 120, 1, 0, CAST(N'2020-02-24' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (121, N'View', 121, 1, 0, CAST(N'2020-02-24' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (122, N'View', 122, 1, 0, CAST(N'2020-02-24' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (123, N'View', 123, 0, 1, CAST(N'2020-02-25' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (124, N'View', 124, 1, 0, CAST(N'2020-02-28' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (125, N'Veiw', 125, 1, 0, CAST(N'2020-02-28' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (126, N'View', 126, 1, 0, CAST(N'2020-03-03' AS Date), 1)
GO
INSERT [dbo].[PageAction] ([AutoId], [ActionName], [PageAutoId], [ActionType], [UserType], [CreateOn], [CreatedBy]) VALUES (127, N'View', 127, 1, 0, CAST(N'2020-03-10' AS Date), 1)
GO
SET IDENTITY_INSERT [dbo].[PageAction] OFF
GO
SET IDENTITY_INSERT [dbo].[PageMaster] ON 
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (2, N'PD00002', N'Email Receiver', 2, N'/EmailReceiverMaster.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (3, N'PD00003', N'Email Server Setting', 105, N'/admin/EmailServerMaster.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (4, N'PD00004', N'Company Profile', 5, N'/Admin/CompanyProfile.aspx', N'', 0, 1, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (5, N'PD00005', N'Car List', 3, N'/admin/CarDetailsList.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (6, N'PD00006', N'Manage Commission Code', 4, N'/admin/CommissionMaster.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (7, N'PD00007', N'Manage Currency', 6, N'/admin/CurencyMaster.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (8, N'PD00008', N'Manage Employee', 7, N'/Admin/Employee.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (9, N'PD00009', N'Manage Route', 159, N'/admin/RouteList.aspx', N'', 1, 2, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (10, N'PD00010', N'Manage Terms', 12, N'/admin/TermsMaster.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (11, N'PD00011', N'Zone List', 13, N'/admin/ZoneList.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (12, N'PD00012', N'Parameter Setting', 14, N'/admin/ParameterSetting.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (13, N'PD00013', N'Credit Memo List', 16, N'/Sales/CreditMemoList.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (14, N'PD00014', N'Customer List', 18, N'/Sales/NewCustomerList.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (15, N'PD00015', N'Price Level List', 19, N'/Admin/PriceLevelList.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (16, N'PD00016', N'Manage Stock', 21, N'/Admin/UpdateStock.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (18, N'PD00018', N'Manage Message', 24, N'/Admin/CreateMessage.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (21, N'PD00021', N'Purchase Order List', 150, N'/Purchase/PurchaseOrderList.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (26, N'PD00026', N'Manage Brand', 36, N'/admin/BrandMaster.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (27, N'PD00027', N'Manage Category', 37, N'/Admin/categoryMaster.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (28, N'PD00028', N'Manage ML Quantity', 38, N'/Admin/ManageMLQuantity.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (29, N'PD00029', N'Manage Subcategory', 39, N'/Admin/subCategoryMaster.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (30, N'PD00030', N'Product Allocation', 40, N'/Admin/AllowQtyToSalesPerson.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (31, N'PD00031', N'Product List', 41, N'/Admin/productList.aspx', N'', 0, 1, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (32, N'PD00032', N'Product Request List', 42, N'/Admin/RequestProductList.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (33, N'PD00033', N'Manage ML Tax', 44, N'/Admin/MLTaxMaster.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (34, N'PD00034', N'Manage Tax', 106, N'/Admin/TaxMaster.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (35, N'PD00035', N'Manage Weight Tax', 45, N'/Admin/WeightTaxMaster.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (36, N'PD00036', N'Vendor List', 47, N'/Admin/VendorList.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (37, N'PD00037', N'Vendor Payment', 48, N'/Vendor/VendorPayMaster.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (40, N'PD00040', N'Customer Statement', 53, N'/Reports/customerStatementReport.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (41, N'PD00041', N'Daily Order Payment', 54, N'/Reports/Daily_Order_Payment_Status.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (42, N'PD00042', N'Payment By Order', 55, N'/Reports/PaymentReportDateWise.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (43, N'PD00043', N'By Payment History', 56, N'/Warehouse/PaymentLog.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (44, N'PD00044', N'By Received Check', 57, N'/Reports/CheckReceivedpayment.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (45, N'PD00045', N'Details', 59, N'/Reports/ProductWiseCommissionReport.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (46, N'PD00046', N'Summary', 60, N'/Reports/Commission_Summary_Report.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (47, N'PD00047', N'Product Receive', 62, N'/Reports/ProductWiseReceiveReport.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (48, N'PD00048', N'By Customer', 65, N'/Manager/MLQtyReport1.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (49, N'PD00049', N'By Product', 66, N'/Manager/MLQtySummaryReport.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (50, N'PD00050', N'By Order', 68, N'/Reports/NotShippedReport.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (51, N'PD00051', N'By Product', 69, N'/Reports/NotShippedReportbyProduct.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (52, N'PD00052', N'By Customer', 71, N'/Reports/OpenBalanceReportSummary.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (53, N'PD00053', N'By Due', 72, N'/Reports/OpenBalanceReportDetail.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (54, N'PD00054', N'By Order', 73, N'/Reports/OrderWiseOpenBalanceReport.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (55, N'PD00055', N'By VAPE ', 75, N'/Reports/OtpReport.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (56, N'PD00056', N'By Weight', 76, N'/Reports/WeightReport.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (57, N'PD00057', N'By Date And Time', 78, N'/Reports/OrderSubmitTimeReport.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (58, N'PD00058', N'By Order', 80, N'/Reports/PackerWiseOrderReport.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (59, N'PD00059', N'By Pick Product', 81, N'/Reports/ItemWiseReport.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (60, N'PD00060', N'By Summary', 82, N'/Reports/PackerReport_Summary.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (62, N'PD00062', N'Barcode', 85, N'/Admin/BarCodeReport.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (64, N'PD00064', N'By Driver Return', 87, N'/Reports/ProudctReturnReport.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (65, N'PD00065', N'By Exchange', 88, N'/Reports/ProductExchangeReport.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (66, N'PD00066', N'By Customer', 90, N'/Reports/PLCustomerWiseReport.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (67, N'PD00067', N' By Product', 91, N'/Reports/PLProductWiseReport.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (68, N'PD00068', N'By Product', 93, N'/Reports/DetailProductSalesReport.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (69, N'PD00069', N'By Product Details', 94, N'/Reports/ProductWiseSalesReport.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (70, N'PD00070', N'By Sales Person', 95, N'/Reports/SalesBySalesPerson.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (71, N'PD00071', N'By Top Selling', 96, N'/Reports/TopSellingReport.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (72, N'PD00072', N'Customer Sales Person', 97, N'/Reports/SalesBy_customer_SalesPerson.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (73, N'PD00073', N'Order Average', 98, N'/Reports/OrderAverageReport.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (77, N'PD00077', N'Ticket List', 103, N'/admin/ErrorTicketLog.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (78, N'PD00078', N'Wrong Product Price List', 104, N'/Admin/WrongProductPrice.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (79, N'PD00079', N'Page List', 9, N'/admin/PageList.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (80, N'PD00080', N'Manage Page Access', 10, N'/Admin/ManagePageAccess.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (81, N'PD00081', N'Manage Module', 8, N'/admin/ManageModule.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (83, N'PD00083', N'Manage Check', 111, N'/Warehouse/CheckMaster.aspx', N'', 1, 1, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (84, N'PD00084', N'Receive Payment', 112, N'/Sales/PayMaster.aspx', N'', 0, 5, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (85, N'PD00085', N'My Dashboard', 113, N'/Admin/mydashboard.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (87, N'PD00087', N'Incomplete Order List', 115, N'/Sales/DraftList.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (88, N'PD00088', N'Credit Memo', 116, N'/Sales/CreditMemo.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (89, N'PD00089', N'Assigned Driver', 117, N'/Manager/assignedOrders.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (90, N'PD00090', N'Stock Receive', 120, N'/warehouse/stockEntry.aspx', N'', 0, 11, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (91, N'PD00091', N'Purchase Order Management', 121, N'/warehouse/stockEntryList.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (92, N'PD00092', N'Order List(WM)', 124, N'/Warehouse/WarehouseOrderList.aspx', N'', 0, 7, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (93, N'PD00093', N'Order List (SM)', 125, N'/Manager/Manager_orderList.aspx', N'', 0, 8, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (94, N'PD00094', N'New Order(POS)', 114, N'/Warehouse/OrderNew.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (95, N'PD00095', N'Draft PO List', 128, N'/POVendor/PODraftList.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (96, N'PD00096', N'Generate PO', 129, N'  /POVendor/GeneratePOV.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (97, N'PD00097', N'PO List', 130, N'/POVendor/PoListReceiver.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (99, N'PD00099', N'Draft PO List', 132, N' /POVendor/SubmitDraftList.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (100, N'PD00100', N'Generate PO', 133, N'/POVendor/GeneratePOV.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (101, N'PD00101', N'PO List', 134, N'  /POVendor/POListInventoryManager.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (102, N'PD00102', N'Receive Stock List', 135, N'/POVendor/POStockListInventoryManager.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (103, N'PD00103', N'Receive Stock List', 131, N'/POVendor/ReceiveStockList.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (104, N'PD00104', N'Status Master', 136, N'/Admin/StatusMaster.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (105, N'PD00105', N'My Dashboard', 137, N'/Sales/mydashboard_sales.aspx', N'', 0, 2, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (106, N'PD00106', N'Packer Order List', 139, N'/Packer/Packer_OrderList.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (107, N'PD00107', N'Manage Route', 158, N'/admin/RouteEntry.aspx', N'', 0, 2, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (108, N'PD00108', N'Driver Order List', 140, N'/Driver/TodaysOrders.aspx', N'order list', 0, 5, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (109, N'PD00109', N'Account Order List', 141, N'/Account/deliveredOrdersList.aspx', N'order list', 0, 6, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (110, N'PD00110', N'Manage Expense', 144, N'/admin/ExpenseMaster.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (111, N'PD00111', N'Manage Petty CASH', 145, N'/Account/PattyCash.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (112, N'PD00112', N'By Bank Report', 51, N'/Reports/ReportPaymentDailyReport.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (113, N'PD00113', N'Customer Credit Report', 52, N'/Reports/Customer_Credit_Report.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (114, N'PD00114', N'Ready to Ship Report', 83, N'/Reports/ReadytoShipReport.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (115, N'PD00115', N'Product Catalog Report', 86, N'/Reports/ProductCatalogueReport.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (116, N'PD00116', N'Order List(SP)', 126, N'/Sales/orderList.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (117, N'PD00117', N'New Order(SP)', 147, N'/Sales/orderMaster.aspx', N'', 0, 2, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (118, N'PD00118', N'I-Draft PO List', 148, N'/Purchase/DraftList.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (119, N'PD00119', N'I-Generate PO', 149, N'/Purchase/PurchaseOrder.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (120, N'PD00120', N'D-Draft PO List', 152, N'/warehouse/INVstockEntryList.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (121, N'PD00121', N'O-Po List', 153, N'/Admin/stockEntryList.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (122, N'PD00122', N'Inventory Onhand', 110, N'/Reports/InventoryHandReport.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (123, N'PD00123', N'Device Master', 155, N'/admin/DeviceMaster.aspx', N'', 0, 1, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (124, N'PD00124', N'Api Request logs Report', 101, N'/Admin/ApiLogReport.aspx', N'', 0, 1, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (125, N'PD00125', N'Draft Order List(POS)', 156, N'/Warehouse/DraftList.aspx', N'', 0, 10, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (126, N'PD00126', N'By Customer', 99, N'/Reports/OrderSaleReport.aspx', N'', 1, 0, 1)
GO
INSERT [dbo].[PageMaster] ([AutoId], [PageId], [PageName], [ParentModuleAutoId], [PageUrl], [Description], [Type], [Role], [Status]) VALUES (127, N'PD00127', N'Order List', 160, N'/Warehouse/NewOrderList.aspx', N'', 1, 0, 1)
GO
SET IDENTITY_INSERT [dbo].[PageMaster] OFF
GO
 
SET IDENTITY_INSERT [dbo].[ManagePageAccess] ON 
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (1, 63, N'1', 11, 1, 1, CAST(N'2020-02-13T08:01:53.727' AS DateTime), 11, CAST(N'2020-02-13T08:05:50.130' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (2, 2, N'2', 1, 1, 1, CAST(N'2020-02-13T15:29:30.063' AS DateTime), 1, CAST(N'2020-02-13T15:29:30.063' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (3, 3, N'5', 1, 1, 1, CAST(N'2020-02-13T15:53:19.927' AS DateTime), 1, CAST(N'2020-02-13T15:53:19.927' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (4, 105, N'3', 1, 1, 1, CAST(N'2020-02-13T15:53:49.430' AS DateTime), 1, CAST(N'2020-02-13T15:53:49.430' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (5, 5, N'4', 1, 1, 1, CAST(N'2020-02-13T15:54:10.133' AS DateTime), 1, CAST(N'2020-02-13T15:54:10.133' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (6, 4, N'6', 1, 1, 1, CAST(N'2020-02-13T16:03:26.770' AS DateTime), 1, CAST(N'2020-02-13T16:03:26.770' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (7, 6, N'7', 1, 1, 1, CAST(N'2020-02-13T16:05:46.603' AS DateTime), 1, CAST(N'2020-02-13T16:05:46.603' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (8, 11, N'9', 1, 1, 1, CAST(N'2020-02-13T16:20:19.887' AS DateTime), 1, CAST(N'2020-02-13T16:20:19.887' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (9, 12, N'10', 1, 1, 1, CAST(N'2020-02-13T16:23:12.043' AS DateTime), 1, CAST(N'2020-02-13T16:23:12.043' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (10, 13, N'11', 1, 1, 1, CAST(N'2020-02-13T16:25:46.173' AS DateTime), 1, CAST(N'2020-02-13T16:25:46.173' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (11, 14, N'12', 1, 1, 1, CAST(N'2020-02-13T16:34:05.497' AS DateTime), 1, CAST(N'2020-02-13T16:34:05.497' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (12, 16, N'13', 1, 1, 1, CAST(N'2020-02-13T17:18:15.280' AS DateTime), 1, CAST(N'2020-02-13T17:18:15.280' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (13, 18, N'14', 1, 1, 1, CAST(N'2020-02-13T17:20:09.503' AS DateTime), 1, CAST(N'2020-02-13T17:20:09.503' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (14, 19, N'15', 1, 1, 1, CAST(N'2020-02-13T17:22:08.317' AS DateTime), 1, CAST(N'2020-02-13T17:22:08.317' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (15, 21, N'16', 1, 1, 1, CAST(N'2020-02-13T17:24:24.350' AS DateTime), 1, CAST(N'2020-02-13T17:24:24.350' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (17, 23, N'17', 1, 1, 1, CAST(N'2020-02-13T17:27:02.543' AS DateTime), 1, CAST(N'2020-02-13T17:27:02.543' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (20, 27, N'19', 9, 1, 1, CAST(N'2020-02-13T17:32:51.187' AS DateTime), 1, CAST(N'2020-02-13T17:32:51.187' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (21, 28, N'20', 1, 1, 1, CAST(N'2020-02-13T17:34:24.910' AS DateTime), 1, CAST(N'2020-02-13T17:34:24.910' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (22, 28, N'20', 9, 1, 1, CAST(N'2020-02-13T17:34:45.090' AS DateTime), 1, CAST(N'2020-02-13T17:34:45.090' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (24, 29, N'21', 9, 1, 1, CAST(N'2020-02-13T17:36:37.503' AS DateTime), 1, CAST(N'2020-02-13T17:36:37.503' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (25, 31, N'22', 1, 1, 1, CAST(N'2020-02-13T17:38:43.023' AS DateTime), 1, CAST(N'2020-02-13T17:38:43.023' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (26, 31, N'22', 9, 1, 1, CAST(N'2020-02-13T17:39:00.760' AS DateTime), 1, CAST(N'2020-02-13T17:39:00.760' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (27, 32, N'23', 1, 1, 1, CAST(N'2020-02-13T17:41:17.360' AS DateTime), 1, CAST(N'2020-02-13T17:41:17.360' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (28, 32, N'23', 9, 1, 1, CAST(N'2020-02-13T17:41:37.480' AS DateTime), 1, CAST(N'2020-02-13T17:41:37.480' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (29, 33, N'24', 1, 1, 1, CAST(N'2020-02-13T17:43:27.940' AS DateTime), 1, CAST(N'2020-02-13T17:43:27.940' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (30, 33, N'24', 9, 1, 1, CAST(N'2020-02-13T17:43:48.453' AS DateTime), 1, CAST(N'2020-02-13T17:43:48.453' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (31, 34, N'25', 1, 1, 1, CAST(N'2020-02-13T17:46:00.740' AS DateTime), 1, CAST(N'2020-02-13T17:46:00.740' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (32, 34, N'25', 9, 1, 1, CAST(N'2020-02-13T17:46:18.680' AS DateTime), 1, CAST(N'2020-02-13T17:46:18.680' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (33, 36, N'26', 1, 1, 1, CAST(N'2020-02-13T17:55:32.650' AS DateTime), 1, CAST(N'2020-02-13T17:55:32.650' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (34, 37, N'27', 1, 1, 1, CAST(N'2020-02-13T17:57:24.123' AS DateTime), 1, CAST(N'2020-02-13T17:57:24.123' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (35, 38, N'28', 1, 1, 1, CAST(N'2020-02-13T18:06:13.263' AS DateTime), 1, CAST(N'2020-02-13T18:06:13.263' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (36, 39, N'29', 1, 1, 1, CAST(N'2020-02-13T18:08:07.993' AS DateTime), 1, CAST(N'2020-02-13T18:08:07.993' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (37, 40, N'30', 1, 1, 1, CAST(N'2020-02-13T18:20:44.183' AS DateTime), 1, CAST(N'2020-02-13T18:20:44.183' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (38, 41, N'31', 1, 1, 1, CAST(N'2020-02-13T18:23:05.363' AS DateTime), 1, CAST(N'2020-02-13T18:23:05.363' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (39, 42, N'32', 1, 1, 1, CAST(N'2020-02-13T18:26:17.280' AS DateTime), 1, CAST(N'2020-02-13T18:26:17.280' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (40, 44, N'33', 1, 1, 1, CAST(N'2020-02-13T18:30:17.223' AS DateTime), 1, CAST(N'2020-02-13T18:30:17.223' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (41, 106, N'34', 1, 1, 1, CAST(N'2020-02-13T18:56:34.073' AS DateTime), 1, CAST(N'2020-02-13T18:56:34.073' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (42, 45, N'35', 1, 1, 1, CAST(N'2020-02-13T18:58:19.227' AS DateTime), 1, CAST(N'2020-02-13T18:58:19.227' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (43, 47, N'36', 1, 1, 1, CAST(N'2020-02-13T19:13:30.977' AS DateTime), 1, CAST(N'2020-02-13T19:13:30.977' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (44, 48, N'37', 1, 1, 1, CAST(N'2020-02-13T19:15:05.857' AS DateTime), 1, CAST(N'2020-02-13T19:15:05.857' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (45, 51, N'38', 1, 1, 1, CAST(N'2020-02-14T12:11:45.337' AS DateTime), 1, CAST(N'2020-02-14T12:11:45.337' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (46, 52, N'39', 1, 1, 1, CAST(N'2020-02-14T12:14:39.530' AS DateTime), 1, CAST(N'2020-02-14T12:14:39.530' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (47, 53, N'40', 1, 1, 1, CAST(N'2020-02-14T12:16:51.010' AS DateTime), 1, CAST(N'2020-02-14T12:16:51.010' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (48, 54, N'41', 1, 1, 1, CAST(N'2020-02-14T12:28:04.010' AS DateTime), 1, CAST(N'2020-02-14T12:28:04.010' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (49, 55, N'42', 1, 1, 1, CAST(N'2020-02-14T12:30:02.440' AS DateTime), 1, CAST(N'2020-02-14T12:30:02.440' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (50, 56, N'43', 1, 1, 1, CAST(N'2020-02-14T12:33:18.790' AS DateTime), 1, CAST(N'2020-02-14T12:33:18.790' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (51, 57, N'44', 1, 1, 1, CAST(N'2020-02-14T12:39:06.120' AS DateTime), 1, CAST(N'2020-02-14T12:39:06.120' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (52, 59, N'45', 1, 1, 1, CAST(N'2020-02-14T12:41:23.303' AS DateTime), 1, CAST(N'2020-02-14T12:41:23.303' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (53, 60, N'46', 1, 1, 1, CAST(N'2020-02-14T12:46:49.130' AS DateTime), 1, CAST(N'2020-02-14T12:46:49.130' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (54, 62, N'47', 1, 1, 1, CAST(N'2020-02-14T12:49:47.463' AS DateTime), 1, CAST(N'2020-02-14T12:49:47.463' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (55, 65, N'48', 1, 1, 1, CAST(N'2020-02-14T12:53:40.447' AS DateTime), 1, CAST(N'2020-02-14T12:53:40.447' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (56, 66, N'49', 1, 1, 1, CAST(N'2020-02-14T12:56:10.210' AS DateTime), 1, CAST(N'2020-02-14T12:56:10.210' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (57, 68, N'50', 1, 1, 1, CAST(N'2020-02-14T12:58:04.727' AS DateTime), 1, CAST(N'2020-02-14T12:58:04.727' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (58, 69, N'51', 1, 1, 1, CAST(N'2020-02-14T12:59:50.380' AS DateTime), 1, CAST(N'2020-02-14T12:59:50.380' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (59, 71, N'52', 1, 1, 1, CAST(N'2020-02-14T13:01:43.497' AS DateTime), 1, CAST(N'2020-02-14T13:01:43.497' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (60, 72, N'53', 1, 1, 1, CAST(N'2020-02-14T13:03:53.160' AS DateTime), 1, CAST(N'2020-02-14T13:03:53.160' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (61, 73, N'54', 1, 1, 1, CAST(N'2020-02-14T13:05:39.793' AS DateTime), 1, CAST(N'2020-02-14T13:05:39.793' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (62, 75, N'55', 1, 1, 1, CAST(N'2020-02-14T13:07:37.603' AS DateTime), 1, CAST(N'2020-02-14T13:07:37.603' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (63, 76, N'56', 1, 1, 1, CAST(N'2020-02-14T13:10:48.943' AS DateTime), 1, CAST(N'2020-02-14T13:10:48.943' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (64, 78, N'57', 1, 1, 1, CAST(N'2020-02-14T13:18:22.313' AS DateTime), 1, CAST(N'2020-02-14T13:18:22.313' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (65, 80, N'58', 1, 1, 1, CAST(N'2020-02-14T13:19:50.490' AS DateTime), 1, CAST(N'2020-02-14T13:19:50.490' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (66, 81, N'59', 1, 1, 1, CAST(N'2020-02-14T13:21:15.263' AS DateTime), 1, CAST(N'2020-02-14T13:21:15.263' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (67, 82, N'60', 1, 1, 1, CAST(N'2020-02-14T13:23:42.687' AS DateTime), 1, CAST(N'2020-02-14T13:23:42.687' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (68, 83, N'61', 1, 1, 1, CAST(N'2020-02-14T13:25:42.790' AS DateTime), 1, CAST(N'2020-02-14T13:25:42.790' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (69, 85, N'62', 1, 1, 1, CAST(N'2020-02-14T14:48:01.400' AS DateTime), 1, CAST(N'2020-02-14T14:48:01.400' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (70, 86, N'63', 1, 1, 1, CAST(N'2020-02-14T14:56:38.110' AS DateTime), 1, CAST(N'2020-02-14T14:56:38.110' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (71, 87, N'64', 1, 1, 1, CAST(N'2020-02-14T15:12:03.333' AS DateTime), 1, CAST(N'2020-02-14T15:12:03.333' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (72, 88, N'65', 1, 1, 1, CAST(N'2020-02-14T15:15:47.227' AS DateTime), 1, CAST(N'2020-02-14T15:15:47.227' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (73, 90, N'66', 1, 1, 1, CAST(N'2020-02-14T15:18:31.213' AS DateTime), 1, CAST(N'2020-02-14T15:18:31.213' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (74, 91, N'67', 1, 1, 1, CAST(N'2020-02-14T15:20:41.593' AS DateTime), 1, CAST(N'2020-02-14T15:20:41.593' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (75, 93, N'68', 1, 1, 1, CAST(N'2020-02-14T15:22:57.310' AS DateTime), 1, CAST(N'2020-02-14T15:22:57.310' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (76, 94, N'69', 1, 1, 1, CAST(N'2020-02-14T15:24:36.820' AS DateTime), 1, CAST(N'2020-02-14T15:24:36.820' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (77, 95, N'70', 1, 1, 1, CAST(N'2020-02-14T15:26:25.583' AS DateTime), 1, CAST(N'2020-02-14T15:26:25.583' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (78, 96, N'71', 1, 1, 1, CAST(N'2020-02-14T15:29:13.923' AS DateTime), 1, CAST(N'2020-02-14T15:29:13.923' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (79, 97, N'72', 1, 1, 1, CAST(N'2020-02-14T15:30:58.277' AS DateTime), 1, CAST(N'2020-02-14T15:30:58.277' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (80, 98, N'73', 1, 1, 1, CAST(N'2020-02-14T15:32:51.987' AS DateTime), 1, CAST(N'2020-02-14T15:32:51.987' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (81, 101, N'124', 1, 1, 1, CAST(N'2020-02-14T15:36:30.383' AS DateTime), 1, CAST(N'2020-02-28T15:21:09.967' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (82, 102, N'76', 1, 1, 1, CAST(N'2020-02-14T15:38:31.610' AS DateTime), 1, CAST(N'2020-02-14T15:38:31.610' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (83, 103, N'77', 1, 1, 1, CAST(N'2020-02-14T15:40:18.713' AS DateTime), 1, CAST(N'2020-02-14T15:40:18.713' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (84, 104, N'78', 1, 1, 1, CAST(N'2020-02-14T15:41:49.503' AS DateTime), 1, CAST(N'2020-02-14T15:41:49.503' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (85, 9, N'79', 1, 1, 1, CAST(N'2020-02-14T15:48:21.843' AS DateTime), 1, CAST(N'2020-02-14T15:48:21.843' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (86, 10, N'80', 1, 1, 1, CAST(N'2020-02-14T15:51:56.157' AS DateTime), 1, CAST(N'2020-02-14T15:51:56.157' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (87, 8, N'81', 1, 1, 1, CAST(N'2020-02-14T15:53:32.753' AS DateTime), 1, CAST(N'2020-02-14T15:53:32.753' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (88, 110, N'82', 1, 1, 1, CAST(N'2020-02-17T17:03:29.140' AS DateTime), 1, CAST(N'2020-02-17T17:03:29.140' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (89, 111, N'83', 1, 1, 1, CAST(N'2020-02-17T17:13:24.933' AS DateTime), 1, CAST(N'2020-02-17T17:13:24.933' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (90, 112, N'84', 5, 1, 1, CAST(N'2020-02-17T17:19:41.080' AS DateTime), 1, CAST(N'2020-02-17T17:19:41.080' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (93, 18, N'14', 2, 1, 1, CAST(N'2020-02-17T17:31:32.220' AS DateTime), 1, CAST(N'2020-02-17T17:31:32.220' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (95, 115, N'87', 2, 1, 1, CAST(N'2020-02-17T17:44:46.890' AS DateTime), 1, CAST(N'2020-02-17T17:44:46.890' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (96, 112, N'84', 2, 1, 1, CAST(N'2020-02-17T17:45:53.043' AS DateTime), 1, CAST(N'2020-02-17T17:45:53.043' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (97, 116, N'88', 2, 1, 1, CAST(N'2020-02-17T17:48:20.437' AS DateTime), 1, CAST(N'2020-02-17T17:48:20.437' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (98, 16, N'13', 2, 1, 1, CAST(N'2020-02-17T17:48:37.947' AS DateTime), 1, CAST(N'2020-02-17T17:48:37.947' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (100, 113, N'85', 7, 1, 1, CAST(N'2020-02-17T17:53:07.527' AS DateTime), 1, CAST(N'2020-02-17T17:53:07.527' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (102, 80, N'58', 7, 1, 1, CAST(N'2020-02-17T17:57:25.960' AS DateTime), 1, CAST(N'2020-02-17T17:57:25.960' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (103, 81, N'59', 7, 1, 1, CAST(N'2020-02-17T17:57:44.917' AS DateTime), 1, CAST(N'2020-02-17T17:57:44.917' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (104, 82, N'60', 7, 1, 1, CAST(N'2020-02-17T17:58:02.010' AS DateTime), 1, CAST(N'2020-02-17T17:58:02.010' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (106, 113, N'85', 3, 1, 1, CAST(N'2020-02-17T18:08:08.163' AS DateTime), 1, CAST(N'2020-02-17T18:08:08.163' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (107, 23, N'17', 3, 1, 1, CAST(N'2020-02-17T18:08:28.610' AS DateTime), 1, CAST(N'2020-02-17T18:08:28.610' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (108, 113, N'85', 8, 1, 1, CAST(N'2020-02-17T18:09:25.197' AS DateTime), 1, CAST(N'2020-02-17T18:09:25.197' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (109, 106, N'34', 8, 1, 1, CAST(N'2020-02-17T18:17:11.960' AS DateTime), 1, CAST(N'2020-02-17T18:17:11.960' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (110, 3, N'5', 8, 1, 1, CAST(N'2020-02-17T18:17:27.987' AS DateTime), 1, CAST(N'2020-02-17T18:17:27.987' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (111, 13, N'11', 8, 1, 1, CAST(N'2020-02-17T18:17:49.907' AS DateTime), 1, CAST(N'2020-02-17T18:17:49.907' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (112, 14, N'12', 8, 1, 1, CAST(N'2020-02-17T18:18:11.740' AS DateTime), 1, CAST(N'2020-02-17T18:18:11.740' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (113, 11, N'9', 8, 1, 1, CAST(N'2020-02-17T18:18:30.597' AS DateTime), 1, CAST(N'2020-02-17T18:18:30.597' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (114, 16, N'13', 8, 1, 1, CAST(N'2020-02-17T18:19:13.020' AS DateTime), 1, CAST(N'2020-02-17T18:19:13.020' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (115, 18, N'14', 8, 1, 1, CAST(N'2020-02-17T18:19:43.057' AS DateTime), 1, CAST(N'2020-02-17T18:19:43.057' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (116, 19, N'15', 8, 1, 1, CAST(N'2020-02-17T18:20:11.223' AS DateTime), 1, CAST(N'2020-02-17T18:20:11.223' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (118, 117, N'89', 8, 1, 1, CAST(N'2020-02-17T18:24:17.663' AS DateTime), 1, CAST(N'2020-02-17T18:24:17.663' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (119, 40, N'30', 8, 1, 1, CAST(N'2020-02-17T18:24:48.300' AS DateTime), 1, CAST(N'2020-02-17T18:24:48.300' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (120, 71, N'52', 8, 1, 1, CAST(N'2020-02-17T18:25:17.220' AS DateTime), 1, CAST(N'2020-02-17T18:25:17.220' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (121, 72, N'53', 8, 1, 1, CAST(N'2020-02-17T18:25:35.430' AS DateTime), 1, CAST(N'2020-02-17T18:25:35.430' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (122, 73, N'54', 8, 1, 1, CAST(N'2020-02-17T18:25:52.443' AS DateTime), 1, CAST(N'2020-02-17T18:25:52.443' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (123, 56, N'43', 8, 1, 1, CAST(N'2020-02-17T18:26:22.510' AS DateTime), 1, CAST(N'2020-02-17T18:26:22.510' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (124, 93, N'68', 8, 1, 1, CAST(N'2020-02-17T18:26:43.320' AS DateTime), 1, CAST(N'2020-02-17T18:26:43.320' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (125, 94, N'69', 8, 1, 1, CAST(N'2020-02-17T18:27:01.490' AS DateTime), 1, CAST(N'2020-02-17T18:27:01.490' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (126, 95, N'70', 8, 1, 1, CAST(N'2020-02-17T18:27:16.783' AS DateTime), 1, CAST(N'2020-02-17T18:27:16.783' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (127, 96, N'71', 8, 1, 1, CAST(N'2020-02-17T18:27:37.473' AS DateTime), 1, CAST(N'2020-02-17T18:27:37.473' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (128, 97, N'72', 8, 1, 1, CAST(N'2020-02-17T18:27:56.033' AS DateTime), 1, CAST(N'2020-02-17T18:27:56.033' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (129, 98, N'73', 8, 1, 1, CAST(N'2020-02-17T18:28:14.123' AS DateTime), 1, CAST(N'2020-02-17T18:28:14.123' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (130, 99, N'74', 8, 1, 1, CAST(N'2020-02-17T18:28:31.153' AS DateTime), 1, CAST(N'2020-02-17T18:28:31.153' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (131, 80, N'58', 8, 1, 1, CAST(N'2020-02-17T18:28:50.147' AS DateTime), 1, CAST(N'2020-02-17T18:28:50.147' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (132, 81, N'59', 8, 1, 1, CAST(N'2020-02-17T18:29:06.940' AS DateTime), 1, CAST(N'2020-02-17T18:29:06.940' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (133, 82, N'60', 8, 1, 1, CAST(N'2020-02-17T18:29:22.847' AS DateTime), 1, CAST(N'2020-02-17T18:29:22.847' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (134, 83, N'114', 8, 1, 1, CAST(N'2020-02-17T18:29:39.710' AS DateTime), 1, CAST(N'2020-02-24T13:28:48.707' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (135, 68, N'50', 8, 1, 1, CAST(N'2020-02-17T18:30:00.830' AS DateTime), 1, CAST(N'2020-02-17T18:30:00.830' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (136, 69, N'51', 8, 1, 1, CAST(N'2020-02-17T18:30:17.553' AS DateTime), 1, CAST(N'2020-02-17T18:30:17.553' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (137, 85, N'62', 8, 1, 1, CAST(N'2020-02-17T18:30:44.040' AS DateTime), 1, CAST(N'2020-02-17T18:30:44.040' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (138, 86, N'115', 8, 1, 1, CAST(N'2020-02-17T18:32:25.273' AS DateTime), 1, CAST(N'2020-02-24T13:31:24.283' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (139, 88, N'65', 8, 1, 1, CAST(N'2020-02-17T18:32:48.547' AS DateTime), 1, CAST(N'2020-02-17T18:32:48.547' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (140, 78, N'57', 8, 1, 1, CAST(N'2020-02-17T18:33:04.393' AS DateTime), 1, CAST(N'2020-02-17T18:33:04.393' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (142, 120, N'90', 11, 1, 1, CAST(N'2020-02-18T12:08:14.410' AS DateTime), 1, CAST(N'2020-02-18T12:08:14.410' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (143, 121, N'91', 11, 1, 1, CAST(N'2020-02-18T12:09:34.510' AS DateTime), 1, CAST(N'2020-02-18T12:09:34.510' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (144, 31, N'22', 11, 1, 1, CAST(N'2020-02-18T12:14:45.440' AS DateTime), 1, CAST(N'2020-02-18T12:14:45.440' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (145, 33, N'24', 11, 1, 1, CAST(N'2020-02-18T12:15:11.787' AS DateTime), 1, CAST(N'2020-02-18T12:15:11.787' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (146, 123, N'23', 11, 1, 1, CAST(N'2020-02-18T12:29:55.317' AS DateTime), 1, CAST(N'2020-02-18T12:29:55.317' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (147, 123, N'23', 9, 1, 1, CAST(N'2020-02-18T12:51:02.777' AS DateTime), 1, CAST(N'2020-02-18T12:51:02.777' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (148, 116, N'88', 10, 1, 1, CAST(N'2020-02-19T10:14:57.450' AS DateTime), 1, CAST(N'2020-02-19T10:14:57.450' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (149, 16, N'13', 10, 1, 1, CAST(N'2020-02-19T10:15:16.640' AS DateTime), 1, CAST(N'2020-02-19T10:15:16.640' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (152, 112, N'84', 10, 1, 1, CAST(N'2020-02-19T10:19:35.363' AS DateTime), 1, CAST(N'2020-02-19T10:19:35.363' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (153, 93, N'68', 10, 1, 1, CAST(N'2020-02-19T10:20:58.840' AS DateTime), 1, CAST(N'2020-02-19T10:20:58.840' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (154, 41, N'31', 10, 1, 1, CAST(N'2020-02-19T10:21:55.797' AS DateTime), 1, CAST(N'2020-02-19T10:21:55.797' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (155, 125, N'93', 8, 1, 1, CAST(N'2020-02-19T12:04:51.273' AS DateTime), 1, CAST(N'2020-02-19T12:04:51.273' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (156, 114, N'94', 10, 1, 1, CAST(N'2020-02-19T14:57:26.403' AS DateTime), 1, CAST(N'2020-02-19T14:57:26.403' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (157, 130, N'97', 11, 1, 1, CAST(N'2020-02-19T15:33:07.707' AS DateTime), 1, CAST(N'2020-02-19T15:33:07.707' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (158, 135, N'102', 9, 1, 1, CAST(N'2020-02-19T15:33:22.210' AS DateTime), 1, CAST(N'2020-02-19T15:33:22.210' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (159, 132, N'99', 9, 1, 1, CAST(N'2020-02-19T15:33:45.393' AS DateTime), 1, CAST(N'2020-02-19T15:33:45.393' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (160, 133, N'100', 9, 1, 1, CAST(N'2020-02-19T15:33:56.940' AS DateTime), 1, CAST(N'2020-02-19T15:33:56.940' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (161, 134, N'101', 9, 1, 1, CAST(N'2020-02-19T15:34:10.260' AS DateTime), 1, CAST(N'2020-02-19T15:34:10.260' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (162, 128, N'95', 11, 1, 1, CAST(N'2020-02-19T15:34:28.963' AS DateTime), 1, CAST(N'2020-02-19T15:34:28.963' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (163, 129, N'96', 11, 1, 1, CAST(N'2020-02-19T15:34:41.100' AS DateTime), 1, CAST(N'2020-02-19T15:34:41.100' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (164, 131, N'103', 11, 1, 1, CAST(N'2020-02-19T15:37:12.990' AS DateTime), 1, CAST(N'2020-02-19T15:37:12.990' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (165, 137, N'105', 2, 1, 1, CAST(N'2020-02-22T12:23:47.797' AS DateTime), 1, CAST(N'2020-02-22T12:23:47.797' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (166, 11, N'9', 2, 1, 1, CAST(N'2020-02-24T12:54:03.413' AS DateTime), 1, CAST(N'2020-02-24T12:54:03.413' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (167, 141, N'109', 6, 1, 1, CAST(N'2020-02-24T12:54:57.530' AS DateTime), 1, CAST(N'2020-02-24T12:54:57.530' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (168, 18, N'14', 6, 1, 1, CAST(N'2020-02-24T12:55:16.027' AS DateTime), 1, CAST(N'2020-02-24T12:55:16.027' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (169, 47, N'36', 6, 1, 1, CAST(N'2020-02-24T12:55:37.773' AS DateTime), 1, CAST(N'2020-02-24T12:55:37.773' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (170, 48, N'37', 6, 1, 1, CAST(N'2020-02-24T12:55:50.487' AS DateTime), 1, CAST(N'2020-02-24T12:55:50.487' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (171, 111, N'83', 6, 1, 1, CAST(N'2020-02-24T12:57:57.817' AS DateTime), 1, CAST(N'2020-02-24T12:57:57.817' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (172, 143, N'9', 2, 1, 1, CAST(N'2020-02-24T12:59:02.767' AS DateTime), 1, CAST(N'2020-02-24T12:59:02.767' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (173, 145, N'111', 6, 1, 1, CAST(N'2020-02-24T13:01:11.623' AS DateTime), 1, CAST(N'2020-02-24T13:01:11.623' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (174, 144, N'110', 6, 1, 1, CAST(N'2020-02-24T13:01:25.270' AS DateTime), 1, CAST(N'2020-02-24T13:01:25.270' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (175, 51, N'112', 6, 1, 1, CAST(N'2020-02-24T13:04:04.493' AS DateTime), 1, CAST(N'2020-02-24T13:04:04.493' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (176, 52, N'113', 6, 1, 1, CAST(N'2020-02-24T13:05:16.413' AS DateTime), 1, CAST(N'2020-02-24T13:05:16.413' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (177, 142, N'108', 2, 1, 1, CAST(N'2020-02-24T13:05:35.193' AS DateTime), 1, CAST(N'2020-02-24T13:05:35.193' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (178, 53, N'40', 6, 1, 1, CAST(N'2020-02-24T13:05:39.917' AS DateTime), 1, CAST(N'2020-02-24T13:05:39.917' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (179, 54, N'41', 6, 1, 1, CAST(N'2020-02-24T13:06:09.853' AS DateTime), 1, CAST(N'2020-02-24T13:06:09.853' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (180, 55, N'42', 6, 1, 1, CAST(N'2020-02-24T13:06:35.020' AS DateTime), 1, CAST(N'2020-02-24T13:06:35.020' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (181, 56, N'43', 6, 1, 1, CAST(N'2020-02-24T13:06:51.810' AS DateTime), 1, CAST(N'2020-02-24T13:06:51.810' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (182, 57, N'44', 6, 1, 1, CAST(N'2020-02-24T13:07:09.593' AS DateTime), 1, CAST(N'2020-02-24T13:07:09.593' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (183, 75, N'55', 6, 1, 1, CAST(N'2020-02-24T13:07:29.107' AS DateTime), 1, CAST(N'2020-02-24T13:07:29.107' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (184, 76, N'56', 6, 1, 1, CAST(N'2020-02-24T13:07:40.633' AS DateTime), 1, CAST(N'2020-02-24T13:07:40.633' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (185, 88, N'65', 6, 1, 1, CAST(N'2020-02-24T13:08:11.873' AS DateTime), 1, CAST(N'2020-02-24T13:08:11.873' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (186, 87, N'64', 6, 1, 1, CAST(N'2020-02-24T13:08:25.773' AS DateTime), 1, CAST(N'2020-02-24T13:08:25.773' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (187, 140, N'107', 5, 1, 1, CAST(N'2020-02-24T13:13:59.373' AS DateTime), 1, CAST(N'2020-02-24T13:13:59.373' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (188, 139, N'106', 3, 1, 1, CAST(N'2020-02-24T13:15:07.150' AS DateTime), 1, CAST(N'2020-02-24T13:15:07.150' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (189, 143, N'9', 8, 1, 1, CAST(N'2020-02-24T13:17:42.107' AS DateTime), 1, CAST(N'2020-02-24T13:17:42.107' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (190, 126, N'116', 2, 1, 1, CAST(N'2020-02-24T13:36:50.203' AS DateTime), 1, CAST(N'2020-02-24T13:36:50.203' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (191, 147, N'117', 2, 1, 1, CAST(N'2020-02-24T13:41:23.110' AS DateTime), 1, CAST(N'2020-02-24T13:41:23.110' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (192, 148, N'118', 9, 1, 1, CAST(N'2020-02-24T14:23:28.897' AS DateTime), 1, CAST(N'2020-02-24T14:23:28.897' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (193, 149, N'119', 9, 1, 1, CAST(N'2020-02-24T14:23:41.043' AS DateTime), 1, CAST(N'2020-02-24T14:23:41.043' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (194, 150, N'21', 9, 1, 1, CAST(N'2020-02-24T14:23:56.073' AS DateTime), 1, CAST(N'2020-02-24T14:23:56.073' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (195, 152, N'120', 9, 1, 1, CAST(N'2020-02-24T14:27:24.760' AS DateTime), 1, CAST(N'2020-02-24T14:27:24.760' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (196, 153, N'121', 9, 1, 1, CAST(N'2020-02-24T14:27:35.757' AS DateTime), 1, CAST(N'2020-02-24T14:27:35.757' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (197, 37, N'27', 9, 1, 1, CAST(N'2020-02-24T14:28:17.960' AS DateTime), 1, CAST(N'2020-02-24T14:28:17.960' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (198, 39, N'29', 9, 1, 1, CAST(N'2020-02-24T14:28:30.710' AS DateTime), 1, CAST(N'2020-02-24T14:28:30.710' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (199, 36, N'26', 9, 1, 1, CAST(N'2020-02-24T14:28:50.563' AS DateTime), 1, CAST(N'2020-02-24T14:28:50.563' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (200, 42, N'32', 9, 1, 1, CAST(N'2020-02-24T14:29:07.573' AS DateTime), 1, CAST(N'2020-02-24T14:29:07.573' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (201, 41, N'31', 9, 1, 1, CAST(N'2020-02-24T14:29:20.150' AS DateTime), 1, CAST(N'2020-02-24T14:29:20.150' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (202, 21, N'16', 9, 1, 1, CAST(N'2020-02-24T14:29:40.673' AS DateTime), 1, CAST(N'2020-02-24T14:29:40.673' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (203, 47, N'36', 9, 1, 1, CAST(N'2020-02-24T14:30:03.083' AS DateTime), 1, CAST(N'2020-02-24T14:30:03.083' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (204, 48, N'37', 9, 1, 1, CAST(N'2020-02-24T14:30:17.300' AS DateTime), 1, CAST(N'2020-02-24T14:30:17.300' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (205, 62, N'47', 9, 1, 1, CAST(N'2020-02-24T14:30:40.963' AS DateTime), 1, CAST(N'2020-02-24T14:30:40.963' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (206, 110, N'122', 9, 1, 1, CAST(N'2020-02-24T14:33:28.090' AS DateTime), 1, CAST(N'2020-02-24T14:33:28.090' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (207, 153, N'121', 11, 1, 1, CAST(N'2020-02-24T14:35:24.583' AS DateTime), 1, CAST(N'2020-02-24T14:35:24.583' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (208, 155, N'123', 1, 1, 1, CAST(N'2020-02-25T11:25:24.137' AS DateTime), 1, CAST(N'2020-02-25T11:25:24.137' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (209, 156, N'125', 10, 1, 1, CAST(N'2020-02-28T17:55:32.927' AS DateTime), 1, CAST(N'2020-02-28T17:55:32.927' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (211, 124, N'92', 7, 1, 1, CAST(N'2020-03-02T10:30:23.523' AS DateTime), 1, CAST(N'2020-03-02T10:30:23.523' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (212, 113, N'85', 13, 1, 1, CAST(N'2020-03-04T12:05:31.333' AS DateTime), 1, CAST(N'2020-03-04T12:05:31.333' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (213, 7, N'8', 13, 1, 1, CAST(N'2020-03-04T12:07:13.987' AS DateTime), 1, CAST(N'2020-03-04T12:07:13.987' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (214, 145, N'111', 13, 1, 1, CAST(N'2020-03-04T12:08:33.447' AS DateTime), 1, CAST(N'2020-03-04T12:08:33.447' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (215, 158, N'108', 2, 1, 1, CAST(N'2020-03-06T12:48:42.013' AS DateTime), 1, CAST(N'2020-03-06T12:48:42.013' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (216, 159, N'9', 2, 1, 1, CAST(N'2020-03-06T12:48:57.970' AS DateTime), 1, CAST(N'2020-03-06T12:48:57.970' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (217, 51, N'112', 13, 1, 1, CAST(N'2020-03-10T15:46:29.700' AS DateTime), 1, CAST(N'2020-03-10T15:46:29.700' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (218, 52, N'113', 13, 1, 1, CAST(N'2020-03-10T15:49:33.457' AS DateTime), 1, CAST(N'2020-03-10T15:49:33.457' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (219, 53, N'40', 13, 1, 1, CAST(N'2020-03-10T15:50:25.853' AS DateTime), 1, CAST(N'2020-03-10T15:50:25.853' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (220, 54, N'41', 13, 1, 1, CAST(N'2020-03-10T15:50:58.667' AS DateTime), 1, CAST(N'2020-03-10T15:50:58.667' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (221, 55, N'42', 13, 1, 1, CAST(N'2020-03-10T15:51:27.463' AS DateTime), 1, CAST(N'2020-03-10T15:51:27.463' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (222, 57, N'44', 13, 1, 1, CAST(N'2020-03-10T15:51:54.840' AS DateTime), 1, CAST(N'2020-03-10T15:51:54.840' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (223, 56, N'43', 13, 1, 1, CAST(N'2020-03-10T15:52:21.603' AS DateTime), 1, CAST(N'2020-03-10T15:52:21.603' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (224, 59, N'45', 13, 1, 1, CAST(N'2020-03-10T15:52:52.520' AS DateTime), 1, CAST(N'2020-03-10T15:52:52.520' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (225, 60, N'46', 13, 1, 1, CAST(N'2020-03-10T15:53:07.840' AS DateTime), 1, CAST(N'2020-03-10T15:53:07.840' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (226, 110, N'122', 13, 1, 1, CAST(N'2020-03-10T15:53:33.510' AS DateTime), 1, CAST(N'2020-03-10T15:53:33.510' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (227, 62, N'47', 13, 1, 1, CAST(N'2020-03-10T15:53:53.660' AS DateTime), 1, CAST(N'2020-03-10T15:53:53.660' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (228, 65, N'48', 13, 1, 1, CAST(N'2020-03-10T15:54:19.230' AS DateTime), 1, CAST(N'2020-03-10T15:54:19.230' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (229, 66, N'49', 13, 1, 1, CAST(N'2020-03-10T15:54:35.633' AS DateTime), 1, CAST(N'2020-03-10T15:54:35.633' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (230, 68, N'50', 13, 1, 1, CAST(N'2020-03-10T15:55:28.473' AS DateTime), 1, CAST(N'2020-03-10T15:55:28.473' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (231, 69, N'51', 13, 1, 1, CAST(N'2020-03-10T15:55:44.090' AS DateTime), 1, CAST(N'2020-03-10T15:55:44.090' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (232, 71, N'52', 13, 1, 1, CAST(N'2020-03-10T15:56:04.120' AS DateTime), 1, CAST(N'2020-03-10T15:56:04.120' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (233, 72, N'53', 13, 1, 1, CAST(N'2020-03-10T15:56:20.097' AS DateTime), 1, CAST(N'2020-03-10T15:56:20.097' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (234, 73, N'54', 13, 1, 1, CAST(N'2020-03-10T15:56:39.697' AS DateTime), 1, CAST(N'2020-03-10T15:56:39.697' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (235, 75, N'55', 13, 1, 1, CAST(N'2020-03-10T15:56:59.017' AS DateTime), 1, CAST(N'2020-03-10T15:56:59.017' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (236, 76, N'56', 13, 1, 1, CAST(N'2020-03-10T15:57:14.413' AS DateTime), 1, CAST(N'2020-03-10T15:57:14.413' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (237, 78, N'57', 13, 1, 1, CAST(N'2020-03-10T15:57:39.140' AS DateTime), 1, CAST(N'2020-03-10T15:57:39.140' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (238, 90, N'66', 13, 1, 1, CAST(N'2020-03-10T15:58:10.227' AS DateTime), 1, CAST(N'2020-03-10T15:58:10.227' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (239, 91, N'67', 13, 1, 1, CAST(N'2020-03-10T15:58:32.370' AS DateTime), 1, CAST(N'2020-03-10T15:58:32.370' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (240, 80, N'58', 13, 1, 1, CAST(N'2020-03-10T16:23:48.923' AS DateTime), 1, CAST(N'2020-03-10T16:23:48.923' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (241, 81, N'59', 13, 1, 1, CAST(N'2020-03-10T16:24:07.233' AS DateTime), 1, CAST(N'2020-03-10T16:24:07.233' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (242, 82, N'60', 13, 1, 1, CAST(N'2020-03-10T16:24:26.990' AS DateTime), 1, CAST(N'2020-03-10T16:24:26.990' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (243, 83, N'114', 13, 1, 1, CAST(N'2020-03-10T16:24:44.060' AS DateTime), 1, CAST(N'2020-03-10T16:24:44.060' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (244, 85, N'62', 13, 1, 1, CAST(N'2020-03-10T16:25:11.103' AS DateTime), 1, CAST(N'2020-03-10T16:25:11.103' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (245, 86, N'115', 13, 1, 1, CAST(N'2020-03-10T16:25:28.350' AS DateTime), 1, CAST(N'2020-03-10T16:25:28.350' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (246, 87, N'64', 13, 1, 1, CAST(N'2020-03-10T16:25:45.693' AS DateTime), 1, CAST(N'2020-03-10T16:25:45.693' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (247, 88, N'65', 13, 1, 1, CAST(N'2020-03-10T16:26:02.270' AS DateTime), 1, CAST(N'2020-03-10T16:26:02.270' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (248, 99, N'126', 13, 1, 1, CAST(N'2020-03-10T16:26:42.153' AS DateTime), 1, CAST(N'2020-03-10T16:26:42.153' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (249, 93, N'68', 13, 1, 1, CAST(N'2020-03-10T16:26:58.707' AS DateTime), 1, CAST(N'2020-03-10T16:26:58.707' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (250, 94, N'69', 13, 1, 1, CAST(N'2020-03-10T16:27:17.130' AS DateTime), 1, CAST(N'2020-03-10T16:27:17.130' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (251, 95, N'70', 13, 1, 1, CAST(N'2020-03-10T16:27:36.127' AS DateTime), 1, CAST(N'2020-03-10T16:27:36.127' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (252, 96, N'71', 13, 1, 1, CAST(N'2020-03-10T16:27:55.933' AS DateTime), 1, CAST(N'2020-03-10T16:27:55.933' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (253, 97, N'72', 13, 1, 1, CAST(N'2020-03-10T16:28:19.200' AS DateTime), 1, CAST(N'2020-03-10T16:28:19.200' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (254, 98, N'73', 13, 1, 1, CAST(N'2020-03-10T16:28:38.610' AS DateTime), 1, CAST(N'2020-03-10T16:28:38.610' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (255, 113, N'85', 9, 1, 1, CAST(N'2020-03-10T16:33:03.887' AS DateTime), 1, CAST(N'2020-03-10T16:33:03.887' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (256, 111, N'83', 9, 1, 1, CAST(N'2020-03-10T16:33:44.407' AS DateTime), 1, CAST(N'2020-03-10T16:33:44.407' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (257, 113, N'85', 11, 1, 1, CAST(N'2020-03-10T16:36:14.920' AS DateTime), 1, CAST(N'2020-03-10T16:36:14.920' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (258, 21, N'16', 11, 1, 1, CAST(N'2020-03-10T16:37:21.810' AS DateTime), 1, CAST(N'2020-03-10T16:37:21.810' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (259, 113, N'85', 5, 1, 1, CAST(N'2020-03-10T16:39:56.407' AS DateTime), 1, CAST(N'2020-03-10T16:39:56.407' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (260, 113, N'85', 10, 1, 1, CAST(N'2020-03-10T16:41:35.923' AS DateTime), 1, CAST(N'2020-03-10T16:41:35.923' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (261, 160, N'127', 10, 1, 1, CAST(N'2020-03-10T16:45:37.990' AS DateTime), 1, CAST(N'2020-03-10T16:45:37.990' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (262, 158, N'108', 8, 1, 1, CAST(N'2020-03-10T16:51:33.400' AS DateTime), 1, CAST(N'2020-03-10T16:51:33.400' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (263, 159, N'9', 8, 1, 1, CAST(N'2020-03-10T16:51:51.850' AS DateTime), 1, CAST(N'2020-03-10T16:51:51.850' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (264, 113, N'85', 6, 1, 1, CAST(N'2020-03-10T16:57:21.110' AS DateTime), 1, CAST(N'2020-03-10T16:57:21.110' AS DateTime), NULL)
GO
INSERT [dbo].[ManagePageAccess] ([AutoId], [ModuleAutoId], [AssignedAction], [Role], [Status], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [PageId]) VALUES (265, 41, N'31', 2, 1, 1, CAST(N'2020-03-18T16:10:04.890' AS DateTime), 1, CAST(N'2020-03-18T16:10:04.890' AS DateTime), NULL)
GO
SET IDENTITY_INSERT [dbo].[ManagePageAccess] OFF
GO

