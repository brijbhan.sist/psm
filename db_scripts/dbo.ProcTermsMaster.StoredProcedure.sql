USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcTermsMaster]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE PROCEDURE [dbo].[ProcTermsMaster]  
@OpCode int=Null,  
@TermsAutoId  int=Null,  
@TermsId VARCHAR(12)=NULL,  
@TermsName VARCHAR(50)=NULL,  
@Description varchar(250)=NULL,  
@Status int=null,  
  
@isException bit out,  
@exceptionMessage varchar(max) out  
AS  
BEGIN  
 BEGIN TRY  
	  SET @isException=0  
	  SET @exceptionMessage='success' 
  IF @OpCode=11  
  BEGIN  
    IF exists(SELECT TermsDesc FROM CustomerTerms WHERE TermsDesc = @TermsName)  
     BEGIN  
		  set @isException=1  
		  set @exceptionMessage='Terms already exists.'  
     END  
    ELSE  
     BEGIN TRY  
      BEGIN TRAN         
		  INSERT INTO [dbo].[CustomerTerms]([TermsDesc],Description,Status)  
		  VALUES (@TermsName,@Description,@Status)   
		  COMMIT TRANSACTION  
     END TRY   
     BEGIN CATCH  
		  ROLLBACK TRAN  
		  SET @isException=1  
		  SET @exceptionMessage='Oops! Something went wrong.Please try later.'  
     END CATCH  
  END  
  ELSE IF @Opcode=21  
   BEGIN  
    IF exists(SELECT TermsDesc FROM CustomerTerms WHERE TermsDesc = @TermsName and TermsId != @TermsId)  
     BEGIN  
		  SET @isException=1  
		  SET @exceptionMessage='Terms already exists.'  
     END  
    ELSE  
     BEGIN  
		  UPDATE CustomerTerms SET TermsDesc = @TermsName, Description = @Description, Status = @Status  
		  WHERE TermsID = @TermsId  
     END  
   END  
  ELSE IF @Opcode=31  
   BEGIN  
     
     IF exists(SELECT * FROM CustomerMaster WHERE Terms =@TermsId)  
     BEGIN  
		  SET @isException=1  
		  SET @exceptionMessage='Terms has been used for another customer.'  
     END  
     ELSE  
     BEGIN  
         DELETE FROM CustomerTerms WHERE TermsId = @TermsId  
     END  
   END  
  ELSE IF @OpCode=41  
   BEGIN  
		 SELECT CM.TermsId, CM.TermsDesc, CM.Description, SM.StatusType AS Status FROM  CustomerTerms AS CM   
		 inner join StatusMaster AS SM ON SM.AutoId=CM.Status and SM.Category is NULL   
		 WHERE (@TermsId is null or @TermsId='' or TermsId like '%'+ @TermsId +'%')  
		 and (@TermsName is null or @TermsName='' or TermsDesc like '%'+ @TermsName +'%')  
		 and (@Status=2 or Status=@Status)   
   END   
  ELSE IF @OpCode=42  
    BEGIN  
         SELECT TermsId, TermsDesc, Description,Status FROM CustomerTerms WHERE TermsId=@TermsId  
   END  
 END TRY  
 BEGIN CATCH      
	    SET @isException=1  
	    SET @exceptionMessage='Oops! Something went wrong.Please try later.'  
 END CATCH  
END  
  
  
  
  
GO
