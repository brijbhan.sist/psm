ALTER PROCEDURE [dbo].[ProcSalesByProductDetailsReport]                                                                    
@Opcode INT=NULL,                                                                  
@CustomerAutoId INT=NULL,                                                                     
@CustomerType int=null,                                                                            
@ProductAutoId INT=NULL,                               
@ProductAutoIdSring varchar(200)=NULL,                                
@BrandAutoIdSring varchar(200)=NULL,                                                                   
@CategoryAutoId  INT=NULL,                                                                    
@SubCategoryId INT=NULL,                                                                    
@EmpAutoId  INT=NULL,                                                                    
@DriverAutoId INT=null,                                                                    
@OrderStatus INT=NULL,                                                
@SalesPersonAutoId int =null,                                                              
@PaymentStatus VARCHAR(100)=NULL,                                                                    
@SalesPerson  VARCHAR(500)=NULL,                                                         
@FromDate date =NULL,                                                                    
@ToDate date =NULL,                             
@CloseOrderFromDate date =NULL,                                                                    
@CloseOrderToDate date =NULL,     
@OrdFromDate date =NULL,                                                                    
@OrdToDate date =NULL,   
@SearchBy INT=NULL,  
@PageIndex INT=1,                                                                    
@PageSize INT=10,                                                                    
@RecordCount INT=null,                                                                    
@isException bit out,                                                                    
@exceptionMessage varchar(max) out                                                                    
AS                                                                    
BEGIN                                                                     
 BEGIN TRY                                                                    
  Set @isException=0                                                                    
  Set @exceptionMessage='Success' 
  IF @Opcode=40                                                                  
  BEGIN                                                                  
	select(SELECT AutoId AS CustomerAutoId,CustomerId+' '+CustomerName as CustomerName FROM CustomerMaster   WHERE            
	status=1 and (CustomerType=@CustomerType OR ISNULL(@CustomerType,0)=0) and (SalesPersonAutoId=@SalesPersonAutoId
	or ISNULL(@SalesPersonAutoId,0)=0) order by replace(CustomerId+' '+CustomerName,' ','') ASC for json path, INCLUDE_NULL_VALUES) as CustomerList                                                             
  END  
  ELSE IF @Opcode=41                                                                    
  BEGIN                                                                    
	Select (select 
	(SELECT AutoId,CategoryName from CategoryMaster where Status=1  order by CategoryName ASC for json path, INCLUDE_NULL_VALUES)  as CategoryList ,                                                              
	(SELECT AutoId,CustomerType from CustomerType order by CustomerType ASC for json path, INCLUDE_NULL_VALUES ) 
	as CustomerType  for json path, INCLUDE_NULL_VALUES ) as  
	ddlCategoryCustomerType                                                             
  END    
  ELSE IF @Opcode=42               
  BEGIN                                                                    
	Select (Select 
	(SELECT AutoId,SubcategoryName from SubCategoryMaster where (isnull(@CategoryAutoId,0)=0 or CategoryAutoId=@CategoryAutoId )and Status=1                           
	order by SubcategoryName ASC for json path, INCLUDE_NULL_VALUES ) as SubCategoryList,
	(select AutoId, CONVERT(varchar(50), ProductId) + ' - ' + ProductName as ProductName from ProductMaster AS PM   
	Where ISNULL(@CategoryAutoId,0)=0 or CategoryAutoId=@CategoryAutoId  and
	(PM.ProductStatus=1 or (Select count(ProductAutoId) from Delivered_Order_Items as OIM where OIM.ProductAutoId=PM.AutoId AND PM.ProductStatus=0)>0)  
	order by CONVERT(varchar(50), ProductId) + ' - ' + ProductName  asc for json path, INCLUDE_NULL_VALUES ) as  ProductList 
	for json path, INCLUDE_NULL_VALUES ) as DropDownList                                                               
  END IF @OPCODE=43                                                                    
  BEGIN            
	SELECT  AutoId,CONVERT(VARCHAR(10),ProductId )+' '+ ProductName AS ProductName FROM ProductMaster AS PM                                                                   
	WHERE  ((ISNULL(@BrandAutoIdSring,'0')='0')  or (PM.BrandAutoId in (select * from dbo.fnSplitString(@BrandAutoIdSring,','))))               
	AND (ISNULL(@CategoryAutoId,0)=0 OR pm.CategoryAutoId =@CategoryAutoId)                      
	AND (ISNULL(@SubCategoryId,0)=0 OR pm.SubcategoryAutoId =@SubCategoryId)  and
	(PM.ProductStatus=1 or (Select count(ProductAutoId) from Delivered_Order_Items as OIM where OIM.ProductAutoId=PM.AutoId AND PM.ProductStatus=0)>0)
	order by CONVERT(VARCHAR(10),ProductId )+' '+ ProductName   for json path, INCLUDE_NULL_VALUES                                                                  
  END     
  ELSE IF @Opcode=44                                                                    
  BEGIN                                 
    Select( Select 
	(SELECT AutoId AS EmpAutoId,FirstName + ' ' + LastName as EmpName FROM EmployeeMaster where EmpType=2 and Status=1 order by EmpName ASC   for json path, INCLUDE_NULL_VALUES) as EmpList,                                                              
    (SELECT AutoId,StatusType AS StatusName FROM StatusMaster WHERE Category='OrderMaster' order by StatusName ASC  for json path, INCLUDE_NULL_VALUES) as StatusList,                                 
    (select AutoId, BrandName from BrandMaster where status = 1  order by trim(BrandName) asc  for json path, INCLUDE_NULL_VALUES) as BrandList for json path, INCLUDE_NULL_VALUES ) as DropDownList                                                           
  END 
  
  ELSE IF @OPCODE=45                                                                   
  BEGIN                                                   
                                                                    
	SELECT ROW_NUMBER() OVER(ORDER BY  ProductId,CustomerName asc, ProductName asc,DateO asc) AS RowNumber, * into #Results48                                                                      
	FROM                     
	(                                                                    
	select CONVERT(varchar(10),OrderDate,101) as OrderDate,OrderDate as DateO,ProductId,ProductName                    
	,OrderNo,om.CustomerAutoId,(emp.FirstName+' '+ISNULL(LastName,'')) as SalesPerson,CustomerName,                    
	(ba.Address + ', ' + ba.City + ', ' + st.StateName + ', ' + ba.Zipcode) as BillingAddress,                    
	case when (QtyShip*QtyPerUnit)!=QtyDel then                  
	cast(UnitPrice/QtyPerUnit as decimal(10,2)) else UnitPrice end as UnitPrice,                  
	(case when UnitAutoId=3 or ((QtyShip*QtyPerUnit)!=QtyDel)                  
	then CONVERT(VARCHAR(10),SUM(oim.QtyDel)) else                   
	0  end) as TotalPiece,                                             
	(case when UnitAutoId=2 and ((QtyShip*QtyPerUnit)=QtyDel) then CONVERT(VARCHAR(10),SUM(QtyShip)) else '0' end) as TotalBox,                                                                    
	(case when UnitAutoId=1 and ((QtyShip*QtyPerUnit)=QtyDel) then CONVERT(VARCHAR(10),SUM(QtyShip)) else '0' end) as TotalCase,            
	0 as R_TotalPiece,0 as R_TotalBox,0 as R_TotalCase,                    
	cast (SUM(oim.QtyDel*ISNULL(oim.UnitPrice/QtyPerUnit,0)) as decimal(10,2))  as PayableAmount,                    
	'' as CreditNo,'' as CreditDate,                    
	(SELECT UM.UnitType FROM UnitMaster AS UM  WHERE UM.AutoId=UnitAutoId) AS UnitName                    
	from Delivered_Order_Items as oim                     
	inner join OrderMaster as om on om.AutoId=oim.OrderAutoId  and oim.QtyDel>0                    
	inner join CustomerMaster as cm on cm.AutoId=om.CustomerAutoId                     
	inner join ProductMaster as pm on pm.AutoId=oim.ProductAutoId                                                    
	inner join EmployeeMaster as emp on emp.AutoId=om.SalesPersonAutoId                    
	inner join BillingAddress as ba on om.BillAddrAutoId=ba.AutoId                                                                    
	inner join State as st on ba.State=st.AutoId                           
	where                           
	((@ProductAutoIdSring='0' or @ProductAutoIdSring='0,') or (oim.ProductAutoId in (select * from dbo.fnSplitString(@ProductAutoIdSring,','))))                                   
	AND ((@BrandAutoIdSring='0' or @BrandAutoIdSring='0,') or (pm.BrandAutoId in (select * from dbo.fnSplitString(@BrandAutoIdSring,','))))  
	AND (ISNULL(@CustomerAutoId,0) =0 or om.CustomerAutoId=@CustomerAutoId)                                                                    
	AND (ISNULL(@EmpAutoId,0) =0 or OM.SalesPersonAutoId=@EmpAutoId)                                                                    
	AND (ISNULL(@CategoryAutoId,0)=0 OR pm.CategoryAutoId =@CategoryAutoId)                                                                    
	AND (ISNULL(@SubCategoryId,0)=0 OR pm.SubcategoryAutoId =@SubCategoryId)                               
	AND (ISNULL(@CustomerType,0)=0 OR CM.CustomerType=@CustomerType)                    
	and OM.Status=11                                                            

	and (@CloseOrderFromDate is null or @CloseOrderToDate is null or (CONVERT(date,Order_Closed_Date)                                                  
	between convert(date,@CloseOrderFromDate) and CONVERT(date,@CloseOrderToDate) and @SearchBy=2)  
	or (CONVERT(date,OrderDate)                                                  
	between convert(date,@CloseOrderFromDate) and CONVERT(date,@CloseOrderToDate) and @SearchBy=1))
    
	group by om.AutoID,OrderDate,ProductId,ProductName,OrderNo,CustomerName,UnitPrice,om.CustomerAutoId,UnitAutoId,                                                                    
	ba.Address ,ba.City,st.StateName ,ba.Zipcode,ProductAutoId,                                                                
	(emp.FirstName+' '+ISNULL(LastName,'')) ,oim.isFreeItem,oim.IsExchange,oim.Tax,QtyPerUnit ,                  
	QtyShip,QtyDel                           
                      
	UNION ALL                
                    
	SELECT '' as OrderDate,CreditDate as DateO,ProductId,ProductName,'' as OrderNo,om.CustomerAutoId,                                                                    
	(emp.FirstName+' '+ISNULL(LastName,'')) as SalesPerson,                                                                    
	CustomerName,'-' as BillingAddress,                                                                    
	UnitPrice,                    
	'0' TotalPiece,                                                                
	'0' as TotalBox,                                                                
	'0' as TotalCase,                                                                  
	(case when UnitAutoId=3 then CONVERT(VARCHAR(10),SUM(ISNULL(oim.AcceptedQty,oim.RequiredQty))) else '0' end) as R_TotalPiece,                                             
	(case when UnitAutoId=2 then CONVERT(VARCHAR(10),SUM(ISNULL(oim.AcceptedQty,oim.RequiredQty))) else '0' end) as R_TotalBox,                                                                    
	(case when UnitAutoId=1 then CONVERT(VARCHAR(10),SUM(ISNULL(oim.AcceptedQty,oim.RequiredQty))) else '0' end) as R_TotalCase,                                                                    
                              
	cast (SUM(ISNULL(oim.AcceptedQty,oim.RequiredQty)*ISNULL(oim.UnitPrice,0)) as decimal(10,2))  as PayableAmount                    
	,CreditNo,CONVERT(varchar(10),CreditDate,101) as CreditDate,                                                        
	(SELECT UM.UnitType FROM UnitMaster AS UM  WHERE UM.AutoId=UnitAutoId) AS UnitName                                  
	from CreditMemoMaster om                       
	inner join CreditItemMaster as oim on om.CreditAutoId=oim.CreditAutoId                                                                 
	inner join ProductMaster as pm on pm.AutoId=oim.ProductAutoId        
	left join BrandMaster as bm on bm.AutoId = pm.BrandAutoId                           
	inner join CustomerMaster as cm on cm.AutoId=om.CustomerAutoId                                                                    
	left join DeliveredOrders as do on do.OrderAutoId=om.OrderAutoId                                                                    
	inner join EmployeeMaster as emp on emp.AutoId=om.SalesPersonAutoId           
	where                           
	((@ProductAutoIdSring='0' or @ProductAutoIdSring='0,') or (oim.ProductAutoId in (select * from dbo.fnSplitString(@ProductAutoIdSring,','))))                                   
	AND ((@BrandAutoIdSring='0' or @BrandAutoIdSring='0,') or (pm.BrandAutoId in (select * from dbo.fnSplitString(@BrandAutoIdSring,','))))  
	AND (ISNULL(@CustomerAutoId,0) =0 or om.CustomerAutoId=@CustomerAutoId)                                                                    
	AND (ISNULL(@EmpAutoId,0) =0 or cm.SalesPersonAutoId=@EmpAutoId)                                                                    
	AND (ISNULL(@CategoryAutoId,0)=0 OR pm.CategoryAutoId =@CategoryAutoId)                                                                    
	AND (ISNULL(@SubCategoryId,0)=0 OR pm.SubcategoryAutoId =@SubCategoryId)                               
	AND (ISNULL(@CustomerType,0)=0 OR CM.CustomerType=@CustomerType)                  
	and OM.Status=3
	and (@CloseOrderFromDate is null or @CloseOrderToDate is null or (CONVERT(date,CreditDate)                                      
	between convert(date,@CloseOrderFromDate) and CONVERT(date,@CloseOrderToDate)))                                                                    
	group by om.CreditAutoId,CreditDate,ProductId,ProductName,CreditNo,CustomerName,UnitPrice,om.CustomerAutoId,UnitAutoId, 
	ProductAutoId,                                                                
	(emp.FirstName+' '+ISNULL(LastName,''))                          
                    
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------                                                                
	) AS T ORDER BY  ProductId,CustomerName asc, ProductName asc,DateO asc                                                        
                                               
                                                                         
	SELECT * FROM #Results48                                                                    
	WHERE ISNULL(@PageSize,0)=0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)                                    
                                                                       
	SELECT case when ISNULL(@PageSize,0)=0 then 0 else COUNT(OrderDate) end  AS RecordCount, @PageSize AS PageSize, @PageIndex AS PageIndex,Format(getdate(),'MM/dd/yyyy hh:mm tt') as PrintDate FROM #Results48                       
                        
	SELECT ISNULL(sum(convert(int,TotalPiece)),0) AS TotalPiece,ISNULL(sum(convert(int,TotalBox)),0) AS TotalBox                      
	,ISNULL(sum(convert(int,TotalCase)),0) AS TotalCase,
	ISNULL(sum(case when CreditNo!='' then 0  else PayableAmount end),0.00) as PayableAmount, 
	ISNULL(sum(case when CreditNo!='' then PayableAmount  else 0 end),0.00) as TotalCreditAmt,
	ISNULL(sum(convert(int,TotalBox)),0) AS TotalBox,ISNULL(sum(convert(int,TotalCase)),0) AS TotalCase,ISNULL(sum(convert(int,R_TotalPiece)),0) AS R_TotalPiece,
	ISNULL(sum(convert(int,R_TotalBox)),0) AS R_TotalBox,ISNULL(sum(convert(int,R_TotalCase)),0) AS R_TotalCase                       
	FROM #Results48                                                                  
                                                                       
  END                                          
 END TRY                                                    
 BEGIN CATCH                                                                    
	Set @isException=1                                                                    
	Set @exceptionMessage=ERROR_MESSAGE()                                                                    
 END CATCH                                                                    
END 
GO
