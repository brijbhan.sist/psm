select AutoId,OrderDate into #result from [demo.a1whm.com].[dbo].OrderMaster where OrderNO  in (
select BillNo from [dbo].StockEntry where VendorAutoId=1054 and BillNo like '%ORD%'
) order by OrderDate desc
select   OrderDate,ProductAutoId,t.UnitPrice,pm.ProductId,UnitAutoId into #result2 from [demo.a1whm.com].[dbo].ProductMaster as pm
inner join (
	select OrderDate,ProductAutoId,UnitPrice,pm.ProductId,UnitAutoId from 
	[demo.a1whm.com].[dbo].Delivered_Order_Items AS DOI
	Inner join [demo.a1whm.com].[dbo].ProductMaster as PM ON
	DOI.ProductAutoId=PM.AutoId
	inner join   #result as t on t.AutoId=DOI.OrderAutoId
) as t on t.ProductAutoId=pm.AutoId order by ProductId 
select row_number() OVER (PARTITION BY ProductAutoId,UnitAutoId ORDER BY ProductAutoId,OrderDate desc) as part ,* into #re from #result2
 
order by ProductAutoId,OrderDate


select t.OrderDate,t.ProductId,pm.ProductName,um.UnitType,t.UnitPrice as PurchasePrice,pd.CostPrice,pd.MinPrice,pd.WHminPrice,pd.Price from #re as t 
inner join ProductMaster as pm on
t.ProductId=pm.ProductId
inner join PackingDetails as PD on
pm.AutoId=pd.ProductAutoId and pd.UnitType=t.UnitAutoId
inner join UnitMaster as um on
pd.UnitType=um.AutoId
where part=1
and pd.CostPrice!=t.UnitPrice
and pd.CostPrice<=pd.MinPrice
and pd.CostPrice<=pd.WHminPrice

drop table #result,#re
drop table #result2


--Select * from PackingDetails