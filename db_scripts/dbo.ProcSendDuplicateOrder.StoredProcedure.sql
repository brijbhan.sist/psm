ALTER Proc [dbo].[ProcSendDuplicateOrder]
(
 @Opcode int =null,
 @FromEmailId varchar(100)=null, 
 @smtp_userName varchar(100)=null,
 @Password varchar(max)=null, 
 @SMTPServer varchar(50)=null, 
 @Port int=null, 
 @SSL bit=null, 
 @ToEmailId  varchar(100)=null, 
 @CCEmailId  varchar(100)=null, 
 @BCCEmailId  varchar(100)=null, 
 @Subject  varchar(max)=null, 
 @EmailBody  varchar(max)=null, 
 @Status int=null, 
 @SentDate datetime=null, 
 @SourceApp  varchar(20)=null, 
 @SubUrl  varchar(100)=null, 
 @isException BIT OUT,
 @exceptionMessage VARCHAR(max) OUT,
 @OrderAutoId int =null,
 @CustomerAutoId int=null,
 @CustomerName varchar(300)=null,
 @Orderdate varchar(20)=null,
 @payableamount varchar(20)=null

)
as begin 
	SET @isException=0
	SET @exceptionMessage='Success!!'
	declare @FromName varchar(250)
	SELECT @FromEmailId=FromEmail,@FromName=DisplayName,@smtp_userName=SMTP_USERNAME,@Password=SMTP_PASSWORD,
	@SMTPServer=HOST,@Port=SMTPPORT,@SSL=EnableSsl from EmailServerMaster_AWS  
  declare @i int=1,@rowNumber int=0,@noofOrders varchar(10)='0'
  if @Opcode = 11
  begin
	  
		select row_Number()over(order by customerName) as RowNumber,count(1) as [Count], cm.CustomerName as CustomerName, Format(convert(date,OrderDate),'MM/dd/yyyy')as OrderDate,payableamount into #Result from ordermaster as om
		inner join customermaster as cm on cm.AutoId=om.CustomerAutoId
		where convert(date,OrderDate)>=CONVERT(date,GETDATE()-1)
		group by cm.CustomerName,payableamount,om.TotalNOI,OrderDate
		having count(1)>1

		
		SET @EmailBody='<table><thead><tr><td>Customer Name</td><td>Orderdate</td><td>Payable Amount</td><td> no of Orders</td></tr></thead>';
		set @EmailBody+='<tbody>';
		set @rowNumber=(select max(RowNumber) from #result)
		while(@i<=@rowNumber)
		begin
			set @CustomerName=(SELECT CustomerName FROM #result WHERE RowNumber=@i)
			set @Orderdate=(SELECT OrderDate FROM #result WHERE RowNumber=@i)
			set @payableamount=(SELECT payableamount FROM #result WHERE RowNumber=@i)		
			set @noofOrders=(SELECT [Count] FROM #result WHERE RowNumber=@i)
			SET @EmailBody+='<tr><td>'+@CustomerName+'</td><td>'+@Orderdate+'</td><td>'+@payableamount+'</td><td>'+@noofOrders+'</td></tr>'
			set @i=@i+1
		end
		set @EmailBody+='</tbody></table>';
		set @Subject='duplicate order received'
    
  end 
	 if(@rowNumber>0)
	 begin
	   
			EXEC [Emailing_DB].[dbo].[ProcSubmitEmail] 

			@Opcode=11,
			@FromEmailId =@FromEmailId,
			@FromName = @FromName,
			@smtp_userName=@FromEmailId,
			@Password = @Password,
			@SMTPServer = @SMTPServer,
			@Port =@Port,
			@SSL =@SSL,
			@ToEmailId ='uneel913@gmail.com',
			@CCEmailId ='brijbhan.sist@gmail.com',
			@BCCEmailId ='naim.uddeen.786@gmail.com',  
			@Subject =@Subject,
			@EmailBody = @EmailBody,
			@SentDate ='',
			@Status =0,
			@SourceApp ='PSM',
			@SubUrl ='Status Changed'	,
			@isException=0,
			@exceptionMessage=''  
	END
end 