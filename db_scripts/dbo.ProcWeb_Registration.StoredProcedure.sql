 
ALTER Procedure [dbo].[ProcWeb_Registration]              
  @Opcode INT=NULL,              
  @Autoid int =null,             
  @FirstName varchar(50)=null,            
  @LastName varchar(50)=null,             
  @CompanyName varchar(50)=null,              
  @CustomerName varchar(50)=null,        
  @OPTLicense varchar(50)=null,            
  @TaxId varchar(50)=null,              
  @BillAddress varchar(MAX)=null,        
  @BillCity varchar(50)=null,          
  @BillZipCode varchar(50)=null,         
  @BillState varchar(50)=null,         
  @ShippingAddress  varchar(50)=null,        
  @ShipCity  varchar(50)=null,        
  @ShippingZipCode  varchar(50)=null,   
  @IsDefault int=null,  
  @Type int=null,       
  @ShipState  varchar(50)=null,        
  @ContactPerson varchar(50)=null,              
  @Email varchar(80)=null,           
  @AlternateEmail varchar(50)=null,        
  @FaxNo varchar(50)=null,        
  @MainPhone varchar(12)=null,        
  @WorkPhone varchar(12)=null,        
  @Phone varchar(12)=null,             
  @UserId varchar(50)=null,         
  @Password varchar(MAX)=null,   
  @StoreId int=null,                               
  @IsException bit out,                                                    
  @ExceptionMessage varchar(max) out                    
AS              
BEGIN              
   SET @IsException=0              
   SET @ExceptionMessage=''              
   IF @Opcode=11              
   BEGIN          
  BEGIN TRY              
     BEGIN TRANSACTION tr2              
    DECLARE @CustomerId varchar(15),@StorId int,@BillAutoId int,@ShipAutoId int        
    SET @CustomerId=(SELECT [dbo].[SequenceCodeGenerator]('CustomerId'))                 
        
    INSERT INTO CustomerMaster (CustomerId,CustomerName,CustomerType,Email,AltEmail,Contact1,Contact2,SalesPersonAutoId        
    ,Status,MobileNo,FaxNo,TaxId,ContactPersonName,BusinessName,OPTLicence,WebStatus)        
    VALUES (@CustomerId,@CustomerName,1,@Email,@AlternateEmail,@MainPhone,@WorkPhone,1187        
    ,0,@Phone,@FaxNo,@TaxId,@ContactPerson,@CompanyName,@OPTLicense,1)        
        
    SET @StorId=SCOPE_IDENTITY()        
         
    Select @ShipCity=CityId from ZipMaster Where Zipcode=@ShippingZipCode         
    Select @BillCity=CityId from ZipMaster Where Zipcode=@BillZipCode                 
    Select @ShipState=StateId from CityMaster Where AutoId=@ShipCity          
    Select @BillState=StateId from CityMaster Where AutoId=@BillCity          
        
    IF @ShippingZipCode=0  --Updated on 10/18/2019 02:14 AM  
    BEGIN  
        INSERT INTO BillingAddress (CustomerAutoId,Address,State,Zipcode,IsDefault,BCityAutoId)        
     VALUES (@StorId,@BillAddress,@BillState,@BillZipCode,1,@BillCity)        
     SET @BillAutoId=SCOPE_IDENTITY()  
     SET @ShipAutoId=@BillAutoId  
    END  
    ELSE  
    BEGIN  
     INSERT INTO BillingAddress (CustomerAutoId,Address,State,Zipcode,IsDefault,BCityAutoId)        
     values (@StorId,@ShippingAddress,@ShipState,@ShippingZipCode,1,@ShipCity)        
     SET @ShipAutoId=SCOPE_IDENTITY()        
        
     INSERT INTO BillingAddress (CustomerAutoId,Address,State,Zipcode,IsDefault,BCityAutoId)        
     VALUES (@StorId,@BillAddress,@BillState,@BillZipCode,1,@BillCity)        
     SET @BillAutoId=SCOPE_IDENTITY()   
    END       
        
    UPDATE CustomerMaster SET DefaultBillAdd=@BillAutoId,DefaultShipAdd=@ShipAutoId Where AutoId=@StorId        
        
    INSERT INTO CustomerCreditMaster (CustomerAutoId,CreditAmount) VALUES (@StorId,0.00)        
        
    INSERT INTO WebUserStoreRelMaster (StoreId,UserId) VALUES (@StorId,@UserId)     
  
     UPDATE [dbo].[SequenceCodeGeneratorMaster] SET [currentSequence]=[currentSequence]+1 WHERE [SequenceCode]='CustomerId'             
                 
   COMMIT TRANSACTION tr2              
     END TRY              
     BEGIN CATCH              
   ROLLBACK TRANSACTION tr2              
   SET @IsException=1              
   SET @ExceptionMessage=ERROR_MESSAGE()              
  END CATCH              
    END              
   IF @Opcode=12            
   BEGIN            
  BEGIN TRY            
   BEGIN TRAN            
   IF EXISTS(SELECT * FROM WebUserRegistration WHERE Email=@Email)            
   BEGIN            
    SET @IsException=1              
    SET @ExceptionMessage='EmailId already exists '+'('+@Email+').'+' Try another.'              
    END            
    ELSE            
    BEGIN                   
     INSERT INTO WebUserRegistration (FirstName,LastName,Mobile,Email,Password) VALUES (@FirstName,@LastName,@Phone,@Email,EncryptByPassPhrase('WHM',@Password))  
    END            
   COMMIT TRAN            
  END TRY            
  BEGIN CATCH            
  ROLLBACK TRANSACTION             
   SET @IsException=1              
   SET @ExceptionMessage=ERROR_MESSAGE()              
  END CATCH            
  END     
  IF @Opcode=13           
  BEGIN            
   BEGIN TRY            
   BEGIN TRAN         
     Select @ShipCity=CityId from ZipMaster Where Zipcode=@ShippingZipCode         
     Select @BillCity=CityId from ZipMaster Where Zipcode=@BillZipCode                 
     Select @ShipState=StateId from CityMaster Where AutoId=@ShipCity          
     Select @BillState=StateId from CityMaster Where AutoId=@BillCity            
    IF @Type=0            
    BEGIN   
     INSERT INTO BillingAddress (CustomerAutoId,Address,State,Zipcode,IsDefault,BCityAutoId)        
     VALUES (@StoreId,@BillAddress,@BillState,@BillZipCode,@IsDefault,@BillCity)        
     SET @BillAutoId=SCOPE_IDENTITY()    
    END            
    ELSE            
    BEGIN                   
     INSERT INTO BillingAddress (CustomerAutoId,Address,State,Zipcode,IsDefault,BCityAutoId)        
     values (@StoreId,@ShippingAddress,@ShipState,@ShippingZipCode,@IsDefault,@ShipCity)        
     SET @ShipAutoId=SCOPE_IDENTITY()    
    END            
   COMMIT TRAN            
   END TRY            
  BEGIN CATCH            
   ROLLBACK TRANSACTION             
    SET @IsException=1              
    SET @ExceptionMessage=ERROR_MESSAGE()              
  END CATCH            
  END    
  IF @Opcode=14  
  BEGIN           
       IF @Type=0 --Updated on 10/24/2019 05:51 AM  
    BEGIN  
        UPDATE WebCartMaster SET BillAddrId=@Autoid Where StorId=@StoreId   
    END  
    ELSE  
    BEGIN  
        UPDATE WebCartMaster SET ShippAddrId=@Autoid Where StorId=@StoreId  
    END  
  END  
  IF @Opcode=15  
  BEGIN   
  BEGIN TRY              
  BEGIN TRANSACTION tr3         
        
  UPDATE CustomerMaster SET CustomerName=@CustomerName,Email=@Email,AltEmail=@AlternateEmail,Contact1=@MainPhone,Contact2=@WorkPhone   
  ,MobileNo=@Phone,FaxNo=@FaxNo,TaxId=@TaxId,ContactPersonName=@ContactPerson,BusinessName=@CompanyName,OPTLicence=@OPTLicense        
  WHERE AutoId=@Autoid          
         
  Select @ShipCity=CityId from ZipMaster Where Zipcode=@ShippingZipCode         
  Select @BillCity=CityId from ZipMaster Where Zipcode=@BillZipCode                 
  Select @ShipState=StateId from CityMaster Where AutoId=@ShipCity          
  Select @BillState=StateId from CityMaster Where AutoId=@BillCity          
        
    Declare @DefShipID int,@DefBilID int  
    SELECT @DefShipID=DefaultShipAdd,@DefBilID=DefaultBillAdd FROM CustomerMaster WHERE AutoId=@Autoid    
       
    IF @ShippingZipCode=0  
    BEGIN  
         UPDATE BillingAddress SET Address=@BillAddress,State=@BillState,Zipcode=@BillZipCode,BCityAutoId=@BillCity  WHERE AutoId=@DefBilID   
    END  
    ELSE  
    BEGIN  
        UPDATE BillingAddress SET Address=@BillAddress,State=@BillState,Zipcode=@BillZipCode,BCityAutoId=@BillCity  WHERE AutoId=@DefBilID   
     UPDATE BillingAddress SET Address=@ShippingAddress,State=@ShipState,Zipcode=@ShippingZipCode,BCityAutoId=@ShipCity  WHERE AutoId=@DefShipID  
       END  
    
  
 COMMIT TRANSACTION tr3              
  END TRY              
  BEGIN CATCH              
 ROLLBACK TRANSACTION tr3              
  SET @IsException=1              
  SET @ExceptionMessage=ERROR_MESSAGE()              
 END CATCH       
   END            
   IF @Opcode=40            
   BEGIN   
		Declare @UserAutoId int=1                
		IF EXISTS(SELECT * from WebUserRegistration where Email=@UserId and CONVERT(varchar(100),DecryptByPassphrase('WHM',Password))=@Password and Status=1)                                      
		BEGIN   
			SELECT AutoId as UserID,FirstName,LastName,Email,  
			ISNULL(( 
			Select * from (
			(SELECT WSD.AutoId,StoreName,Status as StoreStatus FROM WebStoreDraft as WSD
			INNER JOIN WebDraftUserStoreRelMaster as WSR on  
			WSR.AutoId=WSR.StoreId and WSD.Status=1 and WSR.UserId=ur.AutoId
			Union
            SELECT CM.AutoId,CustomerName as StoreName,cm.Status as StoreStatus from CustomerMaster as CM  
			INNER JOIN WebUserStoreRelMaster as WSR on 
			CM.AutoId=WSR.StoreId and WSR.UserId=ur.AutoId)) as t for json path,include_null_values  
			),0) as  TotalStore from WebUserRegistration as ur Where Email=@UserId and CONVERT(varchar(100),DecryptByPassphrase('WHM',Password))=@Password  
			and Status=1   
			for json path,include_null_values   
		END              
		ELSE              
		BEGIN              
			SET @IsException=1                                     
			SET @ExceptionMessage='Username And / Or Password Incorrect.'               
		END              
   END              
   IF @Opcode=41              
   BEGIN              
        Select Zipcode from ZipMaster Where Status=1              
   END              
   IF @Opcode=42              
   BEGIN              
   SELECT c.Country,s.StateName,cm.CityName from Country c              
   Inner join State as s on              
   c.AutoId=s.CountryId              
   Inner join CityMaster cm on              
   s.AutoId=cm.StateId              
   Inner join ZipMaster on              
   ZipMaster.CityId=cm.AutoId              
   Where ZipMaster.Zipcode=@BillZipCode               
   END             
   IF @Opcode=43      --For Dashboard details      
   BEGIN        
    IF EXISTS(SELECT * FROM WebUserStoreRelMaster Where UserId=@UserId)            
    BEGIN            
   IF EXISTS(SELECT * FROM WebCustomerRegistration WHERE Autoid=@StoreId )            
   BEGIN                     
   SELECT  Autoid , CompanyName  , CustomerName , Address1 , Address2 , Zipcode , City             
   , State , Country, Email, Phone, Website , Stores, Distributer, TaxId , Comments             
   , StoreStatus , DocumentSatus FROM  WebCustomerRegistration Where Autoid=@StoreId            
   END            
  END            
   END           
   IF @Opcode=44  --For Billing          
 BEGIN         
    Select cmm.DefaultBillAdd,cmm.DefaultShipAdd ,(  
    SELECT BA.AutoId,Address,S.StateName,Zipcode,IsDefault,CM.CityName as City FROM BillingAddress as BA          
    INNER JOIN CityMaster AS CM ON          
    CM.AutoId=BA.BCityAutoId          
    INNER JOIN State AS S ON          
    S.AutoId=CM.StateId        
    WHERE CustomerAutoId=@StoreId   
    for json path,include_null_values   ) as BillingShippingList  
    from CustomerMaster as cmm Where cmm.AutoId=@StoreId  for json path,include_null_values          
   END           
  IF @Opcode=45 --For Shipping   Currently Not Using        
  BEGIN     
    SELECT SA.AutoId,Address,State,Zipcode,IsDefault,S.StateName,CM.CityName as City FROM ShippingAddress as SA          
    INNER JOIN CityMaster AS CM ON          
    CM.AutoId=SA.SCityAutoId          
    INNER JOIN State AS S ON          
    S.AutoId=CM.StateId          
    WHERE CustomerAutoId= @StoreId          
   END    
   IF @Opcode=46  
   BEGIN  
        SELECT cm.AutoId,CustomerId,CustomerName,Email,Contact1,SM.StatusType as Status,SM.ColorCode,  
       MobileNo,FaxNo,BusinessName,OPTLicence,SA.Address+' ,'+SA.City+' ,'+SA.Zipcode as ShippingAddress,BA.Address+' ,'+BA.City+' ,'+BA.Zipcode as BillingAddress  
    FROM CustomerMaster  as cm  
    INNER JOIN WebUserStoreRelMaster as WSR on  
    cm.AutoId=WSR.StoreId  
    INNER JOIN BillingAddress as SA on  
    cm.DefaultShipAdd=SA.AutoId  
    INNER JOIN BillingAddress as BA on  
    cm.DefaultBillAdd=BA.AutoId  
    INNER JOIN StatusMaster as SM on  
    cm.Status=SM.AutoId and SM.Category='Store'  
     Where WSR.UserId=@UserId --Updated on 10/18/2019 01:35 AM  
    for Json path  
   END       
    IF @Opcode=47  
   BEGIN  
        SELECT CM.AutoId,CustomerId,CustomerName,Email,AltEmail,Contact1,Contact2,SM.StatusType as Status,MobileNo,FaxNo,TaxId,ContactPersonName,BusinessName,OPTLicence     
  ,(SELECT CustomerAutoId,Address,S.StateName,Zipcode,BCityAutoId,City FROM BillingAddress AS SA  
  INNER JOIN State AS S on   
  SA.State=S.AutoId  
  INNER JOIN CustomerMaster as CM on  
  SA.AutoId=CM.DefaultShipAdd  
   Where CustomerAutoId=@Autoid  for json path,include_null_values) as ShippingAddress,    
  (  
  SELECT BA.AutoId,CustomerAutoId,Address,State,Zipcode,IsDefault,BCityAutoId,City FROM BillingAddress as BA  
  INNER JOIN State AS S on   
  BA.State=S.AutoId  
  INNER JOIN CustomerMaster as CMM on  
  BA.AutoId=CMM.DefaultBillAdd  
   Where CustomerAutoId=@Autoid  for Json path,include_null_values) as BillingAddress  
     FROM CustomerMaster as CM  
  INNER JOIN StatusMaster as SM on  
  CM.Status=SM.AutoId  
  Where WebStatus=1 and CM.AutoId=@Autoid  
  for Json path,include_null_values  
   END     
    IF @Opcode=48 --Address List  
   BEGIN  
       SELECT BA.AutoId,CustomerAutoId,Address,S.StateName,Zipcode,IsDefault,BCityAutoId,City FROM BillingAddress AS BA  
    INNER JOIN State S on  
    BA.State=S.AutoId  
    Where CustomerAutoId=@StoreId for Json path  
   END       
END  
GO
