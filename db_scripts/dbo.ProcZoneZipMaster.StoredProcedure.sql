USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcZoneZipMaster]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
CREATE PROCEDURE [dbo].[ProcZoneZipMaster]              
@Opcode INT =null,               
@AutoId Int =null,               
@Name nvarchar(100) =null,              
@ZipCode nvarchar(max) =null,              
@City nvarchar(50) =null,              
@DriverId int=null,              
@CarId int=null,               
@PageIndex int=null,               
@PageSize  int=10,               
@RecordCount int=null,               
@Status  varchar(100)=null,              
@isException bit out,                
@exceptionMessage varchar(max) out                
AS               
BEGIN              
 BEGIN TRY                
    SET @exceptionMessage= 'Success'                
    SET @isException=0                 
   IF @Opcode=11              
   BEGIN       
  if NOT EXISTS(select * from ZoneMaster where [ZoneName]=@Name)              
  BEGIN              
     Insert into ZoneMaster([ZoneName], [DriverId], [CarId], [Status])              
     values(@Name, case when @DriverId=0 then null else @DriverId end ,case when @CarId=0 then null else @CarId end ,@Status)              
     set @AutoId=SCOPE_IDENTITY()              
     Select @AutoId as AutoId              
  END              
  ELSE              
  BEGIN              
     SET @exceptionMessage= 'Zone already exists'                
     SET @isException=1                
  END         
   END              
   IF @Opcode=21              
   BEGIN              
    if NOT EXISTS(select * from ZoneMaster where [ZoneName]=@Name AND AutoId!=@AutoId)              
    BEGIN              
    Update ZoneMaster set [ZoneName]=@Name, [DriverId]=CASE WHEN @DriverId=0 THEN NULL ELSE @DriverId END , [CarId]=CASE WHEN @CarId=0 THEN NULL ELSE @CarId END, [Status]=@Status where AutoId=@AutoId              
          Select Zm.AutoId, zm.Zipcode, cm.CityName from ZipMaster zm               
    inner join CityMaster cm on(cm.[AutoId]=zm.[CityId])                
    where zm.ZoneId is not null and zm.Status=1 and cm.Status=1              
    END              
    ELSE              
    BEGIN              
    SET @exceptionMessage= 'Zone already exists'                
    SET @isException=1                
    END              
   END               
   IF @Opcode=22  --Updated By Rizwan Ahmad on 10/28/2019 09:34 PM            
   BEGIN              
    BEGIN TRY                              
          BEGIN TRAN     
		if exists(select * from zipmaster where zipcode=@ZipCode and zoneid is null)  
		begin  
	   Update ZipMaster set [ZoneId]=@AutoId  where zipcode=@ZipCode  
		end  
		else  
		begin  
	   Update ZipMaster set [ZoneId]=Null  where zipcode=@ZipCode  
		end  
		 --Update ZipMaster set [ZoneId]=Null  where ZoneId=@AutoId              
		 --Update ZipMaster set [ZoneId]=@AutoId where [AutoId] in (select splitData from [dbo].[fnSplitString](@ZipCode,','))   
			  COMMIT TRANSACTION                              
			  END TRY                              
			  BEGIN CATCH                              
			  ROLLBACK TRAN                              
		 Set @isException=1                              
		 Set @exceptionMessage='Oops! Something went wrong.Please try later.'                              
   End Catch                        
   END               
   IF @Opcode=41              
   BEGIN              
    Select CarAutoId,CarName as  CarName from CarDetailsMaster where status=1 and  [CarAutoId] not in (Select  ISNULL([CarId],0)  from ZoneMaster )              
    Select AutoId, FirstName+' '+LastName  as EmpName from EmployeeMaster where status=1 and  [EmpType]=5 and [AutoId] not in (Select  ISNULL([DriverId],0) from ZoneMaster) order by EmpName asc              
   END               
   IF @Opcode=42              
   BEGIN              
         Select CarAutoId,CarId +' '+CarName as  CarName from CarDetailsMaster where [CarAutoId] not in (Select ISNULL([CarId],0) from ZoneMaster ) or CarAutoId=(Select [CarId] from ZoneMaster where [AutoId]=@AutoId)              
          Select AutoId, EmpId +' '+FirstName+' '+LastName as EmpName from EmployeeMaster where ([EmpType]=5 and [AutoId]        
       not in (Select ISNULL([DriverId],0) from ZoneMaster)) or (AutoId=(Select ISNULL([DriverId],0) from ZoneMaster where [AutoId]=@AutoId))       
    Select AutoId,ZoneName,ISNULL(driverId,0) as DriverId,ISNULL(CarId,0) as CarId,Status from ZoneMaster where [AutoId]=@AutoId        
		Select zm.[AutoId],zm.[Zipcode],cm.[CityName],isnull(zm.[ZoneId],0) as SelectStatus from ZipMaster zm inner join CityMaster cm on             
		   (zm.[CityId]=cm.[AutoId]) where zm.[Status]=1 and cm.[Status]=1 and (zm.[ZoneId] is null or zm.[ZoneId]=@AutoId) and (@ZipCode is null or            
		  @ZipCode ='' or zm.[Zipcode] like '%' + @ZipCode + '%') and (@City is null or @City ='' or cm.[CityName] like '%' + @City + '%') order             
		by zm.[ZoneId] desc,zm.[Zipcode] asc              
   END              
   IF @Opcode=43              
   BEGIN              
    Select  CarAutoId,CarName  as  CarName from CarDetailsMaster where Status=1 ORDER BY CarName ASC             
    Select AutoId, FirstName+' '+LastName as  EmpName from EmployeeMaster where [EmpType]=5  and Status=1 ORDER BY EmpName ASC               
   end              
   IF @Opcode=44              
   BEGIN              
    Select ROW_NUMBER() OVER(ORDER BY zm.[ZoneName]) AS RowNumber ,zm.[AutoId], zm.[ZoneName], CarName+'/'+cdm.CarId as  CarName,FirstName+' '+LastName+'/'+EmpId as EmpName,            
    (Select count(*) from ZipMaster where [ZoneId]=zm.AutoId) countzip, CASE WHEN zm.Status=0 THEN 'Inactive' ELSE 'Active' END AS STATUS INTO #RESULT             
    from  ZoneMaster zm left join [dbo].[CarDetailsMaster] cdm on (cdm.CarAutoId=zm.[CarId]) left join               
    [dbo].[EmployeeMaster] em on (em.AutoId=zm.[DriverId]) WHERE  (@Name is null or @Name ='' or [ZoneName] like '%' + @Name + '%') and (@CarId is null or @CarId =0            
    or zm.[CarId] =@CarId) and (@DriverId is null or @DriverId =0 or zm.[DriverId] =@DriverId) and            
    (@Status is null or @Status =2 or zm.Status =@Status)        
    SELECT COUNT(AutoId) AS RecordCount, @PageSize AS PageSize, @PageIndex AS PageIndex FROM #RESULT         
    SELECT * FROM #RESULT              
    WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize)         
   end              
 END TRY              
 BEGIN CATCH              
    SET @isException=1              
    SET @exceptionMessage='Oops! Something went wrong.Please try later.'                 
 END CATCH              
              
END 
GO
