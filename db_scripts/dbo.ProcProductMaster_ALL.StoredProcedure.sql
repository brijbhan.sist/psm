ALTER PROCEDURE [dbo].[ProcProductMaster_ALL]           
@ProductId VARCHAR(12)=NULL         
AS          
BEGIN         
 Declare @BrandID varchar(15)  
 Select @BrandID=BrandId from BrandMaster Where AutoId in (Select BrandAutoId from ProductMaster Where ProductId=@ProductId)  
   
 Declare @CategoryId varchar(15)      
 Select @CategoryId=CategoryId from CategoryMaster Where AutoId in (Select CategoryAutoId from ProductMaster Where ProductId=@ProductId)  
  
 Declare @SubCategoryId varchar(15)      
 Select @SubCategoryId=SubcategoryId from SubCategoryMaster Where AutoId in (Select SubcategoryAutoId from ProductMaster Where ProductId=@ProductId)      
      
 IF NOT EXISTS(SELECT [AutoId] FROM [psmct.a1whm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)          
 BEGIN       
            
  INSERT INTO [psmct.a1whm.com].[dbo].[ProductMaster]        
  (        
  [ProductId],[ProductName],[ProductLocation],[ImageUrl],[CategoryAutoId],[SubcategoryAutoId],[Stock],[ProductType],[TaxRate],[PackingAutoId]        
  ,P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],[ProductStatus],[CreateBy],[CreateDate],[ModifiedBy],  
  [ModifiedDate],IsApply_Oz,WeightOz,BrandAutoId,NewSRP,ThirdImage,ThumbnailImageUrl,OriginalImageUrl  )       
          
  select [ProductId],[ProductName],[ProductLocation],[ImageUrl],  
  (Select AutoId from [psmct.a1whm.com].[dbo].[CategoryMaster] Where CategoryId=@CategoryId),    
  (Select AutoId from [psmct.a1whm.com].[dbo].[SubCategoryMaster] Where SubCategoryId=@SubCategoryId)  
  ,[Stock],[ProductType],[TaxRate],[PackingAutoId] ,P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],
  (select isnull(Status,0)  from [dbo].[RealProductLocation] where ProductAutoId=PM.AutoId and LocationId=2) as ProductStatus,
  [CreateBy],[CreateDate],[ModifiedBy],[ModifiedDate],IsApply_Oz,WeightOz,      
  (Select BM.AutoId from [psmct.a1whm.com].[dbo].[BrandMaster] as BM Where BrandId=@BrandID),NewSRP ,ThirdImage,ThumbnailImageUrl,OriginalImageUrl     
  FROM [dbo].[ProductMaster] as PM where ProductId =@ProductId         
 END        
 ELSE        
 BEGIN        
  UPDATE CT SET CT.[ProductName]=NJ.[ProductName],CT.[ImageUrl]=NJ.ImageUrl,  
  CT.[CategoryAutoId]=(Select AutoId from [psmct.a1whm.com].[dbo].[CategoryMaster] Where CategoryId=@CategoryId),          
  CT.[SubcategoryAutoId]=(Select AutoId from [psmct.a1whm.com].[dbo].[SubCategoryMaster] Where SubcategoryId=@SubCategoryId),        
  CT.ModifiedDate=GETDATE(),        
  CT.P_CommCode=NJ.P_CommCode,BrandAutoId =  (Select AutoId from [psmct.a1whm.com].[dbo].[BrandMaster] Where BrandId=@BrandID),  
  ct.WeightOz=nj.WeightOz,ct.MLQty=nj.MLQty,--ct.NewSRP=nj.NewSRP ,
  ct.ThirdImage=nj.ThirdImage,ct.ThumbnailImageUrl=nj.ThumbnailImageUrl,ct.OriginalImageUrl=nj.OriginalImageUrl
  FROM [psmct.a1whm.com].[dbo].[ProductMaster] AS CT INNER JOIN ProductMaster AS NJ        
  ON CT.ProductId=NJ.ProductId where NJ.ProductId =@ProductId        
 END        
        
 IF NOT EXISTS(SELECT [AutoId] FROM [psmpa.a1whm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)          
 BEGIN         
   INSERT INTO [psmpa.a1whm.com].[dbo].[ProductMaster]        
   (        
   [ProductId],[ProductName],[ProductLocation],[ImageUrl],[CategoryAutoId],[SubcategoryAutoId],[Stock],[ProductType],[TaxRate],[PackingAutoId]        
   ,P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],[ProductStatus],[CreateBy],[CreateDate],[ModifiedBy],  
   [ModifiedDate],IsApply_Oz,WeightOz,BrandAutoId,NewSRP,ThirdImage,ThumbnailImageUrl,OriginalImageUrl)         
   select [ProductId],[ProductName],[ProductLocation],[ImageUrl],     
  (Select AutoId from [psmpa.a1whm.com].[dbo].[CategoryMaster] Where CategoryId=@CategoryId),    
  (Select AutoId from [psmpa.a1whm.com].[dbo].[SubCategoryMaster] Where SubCategoryId=@SubCategoryId)  
  ,[Stock],[ProductType],[TaxRate],[PackingAutoId],P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],
  (select isnull(Status,0)  from [dbo].[RealProductLocation] where ProductAutoId=PM.AutoId and LocationId=4) as ProductStatus,
  [CreateBy],[CreateDate],[ModifiedBy],[ModifiedDate] ,IsApply_Oz,WeightOz ,      
   (Select BM.AutoId from [psmpa.a1whm.com].[dbo].[BrandMaster] as BM Where BrandId=@BrandID),NewSRP,
   ThirdImage,ThumbnailImageUrl,OriginalImageUrl
   FROM [dbo].[ProductMaster] as PM where ProductId =@ProductId            
 END        
 ELSE        
 BEGIN        
   UPDATE CT SET CT.[ProductName]=NJ.[ProductName],CT.[ImageUrl]=NJ.ImageUrl  
   ,CT.[CategoryAutoId]=(Select AutoId from [psmpa.a1whm.com].[dbo].[CategoryMaster] Where CategoryId=@CategoryId),          
   CT.[SubcategoryAutoId]=(Select AutoId from [psmpa.a1whm.com].[dbo].[SubCategoryMaster] Where SubcategoryId=@SubCategoryId),        
   CT.ModifiedDate=GETDATE(),        
   CT.P_CommCode=NJ.P_CommCode,BrandAutoId= (Select AutoId from [psmpa.a1whm.com].[dbo].[BrandMaster] Where BrandId=@BrandID),  
   ct.WeightOz=nj.WeightOz,ct.MLQty=nj.MLQty,--CT.NewSRP=NJ.NewSRP, 
   ct.ThirdImage=nj.ThirdImage,ct.ThumbnailImageUrl=nj.ThumbnailImageUrl,ct.OriginalImageUrl=nj.OriginalImageUrl
   FROM [psmpa.a1whm.com].[dbo].[ProductMaster] AS CT INNER JOIN ProductMaster AS NJ        
   ON CT.ProductId=NJ.ProductId where NJ.ProductId =@ProductId        
 END        
        
 IF NOT EXISTS(SELECT [AutoId] FROM [psmnpa.a1whm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)          
 BEGIN         
   INSERT INTO [psmnpa.a1whm.com].[dbo].[ProductMaster]        
   (        
   [ProductId],[ProductName],[ProductLocation],[ImageUrl],[CategoryAutoId],[SubcategoryAutoId],[Stock],[ProductType],[TaxRate],[PackingAutoId]        
   ,P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],[ProductStatus],[CreateBy],[CreateDate],[ModifiedBy],  
   [ModifiedDate],IsApply_Oz,WeightOz,BrandAutoId,NewSRP,ThirdImage,ThumbnailImageUrl,OriginalImageUrl)       
           
   select [ProductId],[ProductName],[ProductLocation],[ImageUrl],  
     
  (Select AutoId from [psmnpa.a1whm.com].[dbo].[CategoryMaster] Where CategoryId=@CategoryId),    
  (Select AutoId from [psmnpa.a1whm.com].[dbo].[SubCategoryMaster] Where SubCategoryId=@SubCategoryId)  
  ,[Stock],[ProductType],[TaxRate],[PackingAutoId],P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],
  (select isnull(Status,0)  from [dbo].[RealProductLocation] where ProductAutoId=PM.AutoId and LocationId=5) as ProductStatus,
  [CreateBy],[CreateDate],[ModifiedBy],GETDATE(),IsApply_Oz,WeightOz,      
  (Select BM.AutoId from [psmnpa.a1whm.com].[dbo].[BrandMaster] as BM Where BrandId=@BrandID),NewSRP ,
  ThirdImage,ThumbnailImageUrl,OriginalImageUrl
   FROM [dbo].[ProductMaster] as PM  where ProductId =@ProductId            
 END        
 ELSE        
 BEGIN        
   UPDATE CT SET CT.[ProductName]=NJ.[ProductName],CT.[ImageUrl]=NJ.ImageUrl,  
   CT.[CategoryAutoId]=(Select AutoId from [psmnpa.a1whm.com].[dbo].[CategoryMaster] Where CategoryId=@CategoryId) ,          
   CT.[SubcategoryAutoId]=(Select AutoId from [psmnpa.a1whm.com].[dbo].[SubCategoryMaster] Where SubCategoryId=@SubCategoryId) ,        
   CT.ModifiedDate=GETDATE(),        
   CT.P_CommCode=NJ.P_CommCode,BrandAutoId= (Select AutoId from [psmnpa.a1whm.com].[dbo].[BrandMaster] Where BrandId=@BrandID)  ,  
   ct.WeightOz=nj.WeightOz,ct.MLQty=nj.MLQty,--CT.NewSRP=NJ.NewSRP ,
   ct.ThirdImage=nj.ThirdImage,ct.ThumbnailImageUrl=nj.ThumbnailImageUrl,ct.OriginalImageUrl=nj.OriginalImageUrl
   FROM [psmnpa.a1whm.com].[dbo].[ProductMaster] AS CT INNER JOIN ProductMaster AS NJ        
   ON CT.ProductId=NJ.ProductId where NJ.ProductId =@ProductId        
 END        
        
    
  IF NOT EXISTS(SELECT [AutoId] FROM [psmwpa.a1whm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)          
 BEGIN         
   INSERT INTO [psmwpa.a1whm.com].[dbo].[ProductMaster]        
   (        
   [ProductId],[ProductName],[ProductLocation],[ImageUrl],[CategoryAutoId],[SubcategoryAutoId],[Stock],[ProductType],[TaxRate],[PackingAutoId]        
   ,P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],[ProductStatus],[CreateBy],[CreateDate],[ModifiedBy],  
   [ModifiedDate],IsApply_Oz,WeightOz,BrandAutoId,NewSRP,ThirdImage,ThumbnailImageUrl,OriginalImageUrl)       
           
   select [ProductId],[ProductName],[ProductLocation],[ImageUrl],     
  (Select AutoId from [psmwpa.a1whm.com].[dbo].[CategoryMaster] Where CategoryId=@CategoryId),    
  (Select AutoId from [psmwpa.a1whm.com].[dbo].[SubCategoryMaster] Where SubCategoryId=@SubCategoryId)  
  ,[Stock],[ProductType],[TaxRate],[PackingAutoId],P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],
  (select isnull(Status,0)  from [dbo].[RealProductLocation] where ProductAutoId=PM.AutoId and LocationId=9) as ProductStatus,
  [CreateBy],[CreateDate],[ModifiedBy],GETDATE(),IsApply_Oz,WeightOz,      
     (Select BM.AutoId from [psmwpa.a1whm.com].[dbo].[BrandMaster] as BM Where BrandId=@BrandID),NewSRP  ,
	 ThirdImage,ThumbnailImageUrl,OriginalImageUrl
   FROM [dbo].[ProductMaster] as PM  where ProductId =@ProductId            
 END        
 ELSE        
 BEGIN        
   UPDATE WPA SET WPA.[ProductName]=NJ.[ProductName],WPA.[ImageUrl]=NJ.ImageUrl,  
   WPA.[CategoryAutoId]=(Select AutoId from [psmwpa.a1whm.com].[dbo].[CategoryMaster] Where CategoryId=@CategoryId),          
   WPA.[SubcategoryAutoId]=(Select AutoId from [psmwpa.a1whm.com].[dbo].[SubCategoryMaster] Where SubCategoryId=@SubCategoryId),  
   WPA.ModifiedDate=GETDATE(),        
   WPA.P_CommCode=NJ.P_CommCode,BrandAutoId= (Select AutoId from [psmwpa.a1whm.com].[dbo].[BrandMaster] Where BrandId=@BrandID),  
   WPA.WeightOz=nj.WeightOz,WPA.MLQty=nj.MLQty,--WPA.NewSRP=NJ.NewSRP ,
   WPA.ThirdImage=nj.ThirdImage,WPA.ThumbnailImageUrl=nj.ThumbnailImageUrl,WPA.OriginalImageUrl=nj.OriginalImageUrl
   FROM [psmwpa.a1whm.com].[dbo].[ProductMaster] AS WPA INNER JOIN ProductMaster AS NJ        
   ON WPA.ProductId=NJ.ProductId where NJ.ProductId =@ProductId        
 END   
   
 IF NOT EXISTS(SELECT [AutoId] FROM [psmny.a1whm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)          
 BEGIN         
   INSERT INTO [psmny.a1whm.com].[dbo].[ProductMaster]        
   (        
   [ProductId],[ProductName],[ProductLocation],[ImageUrl],[CategoryAutoId],[SubcategoryAutoId],[Stock],[ProductType],[TaxRate],[PackingAutoId]        
   ,P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],[ProductStatus],[CreateBy],[CreateDate],[ModifiedBy],  
   [ModifiedDate],IsApply_Oz,WeightOz,BrandAutoId,NewSRP,ThirdImage,ThumbnailImageUrl,OriginalImageUrl)       
           
   select [ProductId],[ProductName],[ProductLocation],[ImageUrl],     
  (Select AutoId from [psmny.a1whm.com].[dbo].[CategoryMaster] Where CategoryId=@CategoryId),    
  (Select AutoId from [psmny.a1whm.com].[dbo].[SubCategoryMaster] Where SubCategoryId=@SubCategoryId)  
  ,[Stock],[ProductType],[TaxRate],[PackingAutoId],P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],
  (select isnull(Status,0)  from [dbo].[RealProductLocation] where ProductAutoId=PM.AutoId and LocationId=10) as ProductStatus,
  [CreateBy],[CreateDate],[ModifiedBy],GETDATE(),IsApply_Oz,WeightOz,      
     (Select BM.AutoId from [psmny.a1whm.com].[dbo].[BrandMaster] as BM Where BrandId=@BrandID),NewSRP ,
	 ThirdImage,ThumbnailImageUrl,OriginalImageUrl
   FROM [dbo].[ProductMaster] as PM where ProductId =@ProductId            
 END        
 ELSE        
 BEGIN        
   UPDATE WPA SET WPA.[ProductName]=NJ.[ProductName],WPA.[ImageUrl]=NJ.ImageUrl,  
   WPA.[CategoryAutoId]=(Select AutoId from [psmny.a1whm.com].[dbo].[CategoryMaster] Where CategoryId=@CategoryId),          
   WPA.[SubcategoryAutoId]=(Select AutoId from [psmny.a1whm.com].[dbo].[SubCategoryMaster] Where SubCategoryId=@SubCategoryId),  
   WPA.ModifiedDate=GETDATE(),        
   WPA.P_CommCode=NJ.P_CommCode,BrandAutoId= (Select AutoId from [psmny.a1whm.com].[dbo].[BrandMaster] Where BrandId=@BrandID),  
   WPA.WeightOz=nj.WeightOz,WPA.MLQty=nj.MLQty ,--WPA.NewSRP=NJ.NewSRP  ,
   WPA.ThirdImage=nj.ThirdImage,WPA.ThumbnailImageUrl=nj.ThumbnailImageUrl,wpa.OriginalImageUrl=nj.OriginalImageUrl
   FROM [psmny.a1whm.com].[dbo].[ProductMaster] AS WPA INNER JOIN ProductMaster AS NJ        
   ON WPA.ProductId=NJ.ProductId where NJ.ProductId =@ProductId        
 END 

 IF NOT EXISTS(SELECT [AutoId] FROM [psmnj.easywhm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)          
 BEGIN         
  INSERT INTO [psmnj.easywhm.com].[dbo].[ProductMaster]        
  (        
  [ProductId],[ProductName],[ProductLocation],[ImageUrl],[CategoryAutoId],[SubcategoryAutoId],[Stock],[ProductType],[TaxRate],[PackingAutoId]        
  ,P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],[ProductStatus],[CreateBy],[CreateDate],[ModifiedBy],  
  [ModifiedDate],IsApply_Oz,WeightOz,BrandAutoId,NewSRP,ThirdImage,ThumbnailImageUrl,OriginalImageUrl)       
           
  select [ProductId],[ProductName],[ProductLocation],[ImageUrl],     
  (Select AutoId from [psmnj.easywhm.com].[dbo].[CategoryMaster] Where CategoryId=@CategoryId),    
  (Select AutoId from [psmnj.easywhm.com].[dbo].[SubCategoryMaster] Where SubCategoryId=@SubCategoryId)  
  ,[Stock],[ProductType],[TaxRate],[PackingAutoId],P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],
  (select isnull(Status,0)  from [dbo].[RealProductLocation] where ProductAutoId=PM.AutoId and LocationId=1) as ProductStatus,
  [CreateBy],[CreateDate],[ModifiedBy],GETDATE(),IsApply_Oz,WeightOz,      
  (Select BM.AutoId from [psmnj.easywhm.com].[dbo].[BrandMaster] as BM Where BrandId=@BrandID),NewSRP    
  ,ThirdImage,ThumbnailImageUrl,OriginalImageUrl
  FROM [dbo].[ProductMaster] as PM where ProductId =@ProductId            
 END        
 ELSE        
 BEGIN        
  UPDATE ESY SET ESY.[ProductName]=NJ.[ProductName],ESY.[ImageUrl]=NJ.ImageUrl,  
  ESY.[CategoryAutoId]=(Select AutoId from [psmnj.easywhm.com].[dbo].[CategoryMaster] Where CategoryId=@CategoryId),          
  ESY.[SubcategoryAutoId]=(Select AutoId from [psmnj.easywhm.com].[dbo].[SubCategoryMaster] Where SubCategoryId=@SubCategoryId),  
  ESY.ModifiedDate=GETDATE(),        
  ESY.P_CommCode=NJ.P_CommCode,BrandAutoId= (Select AutoId from [psmnj.easywhm.com].[dbo].[BrandMaster] Where BrandId=@BrandID),  
  ESY.WeightOz=nj.WeightOz,ESY.MLQty=nj.MLQty ,--ESY.NewSRP=NJ.NewSRP  ,
  ESY.ThirdImage=nj.ThirdImage,ESY.ThumbnailImageUrl=nj.ThumbnailImageUrl,ESY.OriginalImageUrl=nj.OriginalImageUrl
  FROM [psmnj.easywhm.com].[dbo].[ProductMaster] AS ESY INNER JOIN ProductMaster AS NJ        
  ON ESY.ProductId=NJ.ProductId where NJ.ProductId =@ProductId        
 END   
  
 IF NOT EXISTS(SELECT [AutoId] FROM [psmct.easywhm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)          
 BEGIN         
  INSERT INTO [psmct.easywhm.com].[dbo].[ProductMaster]        
  (        
  [ProductId],[ProductName],[ProductLocation],[ImageUrl],[CategoryAutoId],[SubcategoryAutoId],[Stock],[ProductType],[TaxRate],[PackingAutoId]        
  ,P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],[ProductStatus],[CreateBy],[CreateDate],[ModifiedBy],  
  [ModifiedDate],IsApply_Oz,WeightOz,BrandAutoId,NewSRP,ThirdImage,ThumbnailImageUrl,OriginalImageUrl)       
           
  select [ProductId],[ProductName],[ProductLocation],[ImageUrl],     
  (Select AutoId from [psmct.easywhm.com].[dbo].[CategoryMaster] Where CategoryId=@CategoryId),    
  (Select AutoId from [psmct.easywhm.com].[dbo].[SubCategoryMaster] Where SubCategoryId=@SubCategoryId)  
  ,[Stock],[ProductType],[TaxRate],[PackingAutoId],P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],
  (select isnull(Status,0)  from [dbo].[RealProductLocation] where ProductAutoId=PM.AutoId and LocationId=2) as ProductStatus,
  [CreateBy],[CreateDate],[ModifiedBy],GETDATE(),IsApply_Oz,WeightOz,      
  (Select BM.AutoId from [psmct.easywhm.com].[dbo].[BrandMaster] as BM Where BrandId=@BrandID),NewSRP,
  ThirdImage,ThumbnailImageUrl,OriginalImageUrl
  FROM [dbo].[ProductMaster] as PM where ProductId =@ProductId            
 END        
 ELSE        
 BEGIN        
  UPDATE ESY SET ESY.[ProductName]=NJ.[ProductName],ESY.[ImageUrl]=NJ.ImageUrl,  
  ESY.[CategoryAutoId]=(Select AutoId from [psmct.easywhm.com].[dbo].[CategoryMaster] Where CategoryId=@CategoryId),          
  ESY.[SubcategoryAutoId]=(Select AutoId from [psmct.easywhm.com].[dbo].[SubCategoryMaster] Where SubCategoryId=@SubCategoryId),  
  ESY.ModifiedDate=GETDATE(),        
  ESY.P_CommCode=NJ.P_CommCode,BrandAutoId= (Select AutoId from [psmct.easywhm.com].[dbo].[BrandMaster] Where BrandId=@BrandID),  
  ESY.WeightOz=nj.WeightOz,ESY.MLQty=nj.MLQty  ,--ESY.NewSRP=NJ.NewSRP,
  ESY.ThirdImage=nj.ThirdImage,ESY.ThumbnailImageUrl=nj.ThumbnailImageUrl,ESY.OriginalImageUrl=nj.OriginalImageUrl
  FROM [psmct.easywhm.com].[dbo].[ProductMaster] AS ESY INNER JOIN ProductMaster AS NJ        
  ON ESY.ProductId=NJ.ProductId where NJ.ProductId =@ProductId        
 END   
  
 IF NOT EXISTS(SELECT [AutoId] FROM [psmnpa.easywhm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)          
 BEGIN         
  INSERT INTO [psmnpa.easywhm.com].[dbo].[ProductMaster]        
  (        
  [ProductId],[ProductName],[ProductLocation],[ImageUrl],[CategoryAutoId],[SubcategoryAutoId],[Stock],[ProductType],[TaxRate],[PackingAutoId]        
  ,P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],[ProductStatus],[CreateBy],[CreateDate],[ModifiedBy],  
  [ModifiedDate],IsApply_Oz,WeightOz,BrandAutoId,NewSRP,ThirdImage,ThumbnailImageUrl,OriginalImageUrl)       
           
  select [ProductId],[ProductName],[ProductLocation],[ImageUrl],     
  (Select AutoId from [psmnpa.easywhm.com].[dbo].[CategoryMaster] Where CategoryId=@CategoryId),    
  (Select AutoId from [psmnpa.easywhm.com].[dbo].[SubCategoryMaster] Where SubCategoryId=@SubCategoryId)  
  ,[Stock],[ProductType],[TaxRate],[PackingAutoId],P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],
  (select isnull(Status,0)  from [dbo].[RealProductLocation] where ProductAutoId=PM.AutoId and LocationId=5) as ProductStatus,
  [CreateBy],[CreateDate],[ModifiedBy],GETDATE(),IsApply_Oz,WeightOz,      
  (Select BM.AutoId from [psmnpa.easywhm.com].[dbo].[BrandMaster] as BM Where BrandId=@BrandID),NewSRP ,
  ThirdImage,ThumbnailImageUrl,OriginalImageUrl
  FROM [dbo].[ProductMaster] as PM where ProductId =@ProductId            
 END        
 ELSE        
 BEGIN        
  UPDATE ESY SET ESY.[ProductName]=NJ.[ProductName],ESY.[ImageUrl]=NJ.ImageUrl,  
  ESY.[CategoryAutoId]=(Select AutoId from [psmnpa.easywhm.com].[dbo].[CategoryMaster] Where CategoryId=@CategoryId),          
  ESY.[SubcategoryAutoId]=(Select AutoId from [psmnpa.easywhm.com].[dbo].[SubCategoryMaster] Where SubCategoryId=@SubCategoryId),  
  ESY.ModifiedDate=GETDATE(),        
  ESY.P_CommCode=NJ.P_CommCode,BrandAutoId= (Select AutoId from [psmnpa.easywhm.com].[dbo].[BrandMaster] Where BrandId=@BrandID),  
  ESY.WeightOz=nj.WeightOz,ESY.MLQty=nj.MLQty,--ESY.NewSRP=NJ.NewSRP ,
  ESY.ThirdImage=nj.ThirdImage,ESY.ThumbnailImageUrl=nj.ThumbnailImageUrl,ESY.OriginalImageUrl=nj.OriginalImageUrl
  FROM [psmnpa.easywhm.com].[dbo].[ProductMaster] AS ESY INNER JOIN ProductMaster AS NJ        
  ON ESY.ProductId=NJ.ProductId where NJ.ProductId =@ProductId        
 END   
 IF NOT EXISTS(SELECT [AutoId] FROM [psmpa.easywhm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)          
 BEGIN         
  INSERT INTO [psmpa.easywhm.com].[dbo].[ProductMaster]        
  (        
  [ProductId],[ProductName],[ProductLocation],[ImageUrl],[CategoryAutoId],[SubcategoryAutoId],[Stock],[ProductType],[TaxRate],[PackingAutoId]        
  ,P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],[ProductStatus],[CreateBy],[CreateDate],[ModifiedBy],  
  [ModifiedDate],IsApply_Oz,WeightOz,BrandAutoId,NewSRP,ThirdImage,ThumbnailImageUrl,OriginalImageUrl)       
           
  select [ProductId],[ProductName],[ProductLocation],[ImageUrl],     
  (Select AutoId from [psmpa.easywhm.com].[dbo].[CategoryMaster] Where CategoryId=@CategoryId),    
  (Select AutoId from [psmpa.easywhm.com].[dbo].[SubCategoryMaster] Where SubCategoryId=@SubCategoryId)  
  ,[Stock],[ProductType],[TaxRate],[PackingAutoId],P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],
  (select isnull(Status,0)  from [dbo].[RealProductLocation] where ProductAutoId=PM.AutoId and LocationId=4) as ProductStatus,
  [CreateBy],[CreateDate],[ModifiedBy],GETDATE(),IsApply_Oz,WeightOz,      
  (Select BM.AutoId from [psmpa.easywhm.com].[dbo].[BrandMaster] as BM Where BrandId=@BrandID),NewSRP ,
  ThirdImage,ThumbnailImageUrl,OriginalImageUrl
  FROM [dbo].[ProductMaster] as PM where ProductId =@ProductId            
 END        
 ELSE        
 BEGIN        
  UPDATE ESY SET ESY.[ProductName]=NJ.[ProductName],ESY.[ImageUrl]=NJ.ImageUrl,  
  ESY.[CategoryAutoId]=(Select AutoId from [psmpa.easywhm.com].[dbo].[CategoryMaster] Where CategoryId=@CategoryId),          
  ESY.[SubcategoryAutoId]=(Select AutoId from [psmpa.easywhm.com].[dbo].[SubCategoryMaster] Where SubCategoryId=@SubCategoryId),  
  ESY.ModifiedDate=GETDATE(),        
  ESY.P_CommCode=NJ.P_CommCode,BrandAutoId= (Select AutoId from [psmpa.easywhm.com].[dbo].[BrandMaster] Where BrandId=@BrandID),  
  ESY.WeightOz=nj.WeightOz,ESY.MLQty=nj.MLQty ,--ESY.NewSRP=NJ.NewSRP ,
  ESY.ThirdImage=nj.ThirdImage,ESY.ThumbnailImageUrl=nj.ThumbnailImageUrl,ESY.OriginalImageUrl=nj.OriginalImageUrl
  FROM [psmpa.easywhm.com].[dbo].[ProductMaster] AS ESY INNER JOIN ProductMaster AS NJ        
  ON ESY.ProductId=NJ.ProductId where NJ.ProductId =@ProductId        
 END   
  
 IF NOT EXISTS(SELECT [AutoId] FROM [psmwpa.easywhm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)          
 BEGIN         
  INSERT INTO [psmwpa.easywhm.com].[dbo].[ProductMaster]        
  (        
  [ProductId],[ProductName],[ProductLocation],[ImageUrl],[CategoryAutoId],[SubcategoryAutoId],[Stock],[ProductType],[TaxRate],[PackingAutoId]        
  ,P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],[ProductStatus],[CreateBy],[CreateDate],[ModifiedBy],  
  [ModifiedDate],IsApply_Oz,WeightOz,BrandAutoId,NewSRP,ThirdImage,ThumbnailImageUrl,OriginalImageUrl)       
           
  select [ProductId],[ProductName],[ProductLocation],[ImageUrl],     
  (Select AutoId from [psmwpa.easywhm.com].[dbo].[CategoryMaster] Where CategoryId=@CategoryId),    
  (Select AutoId from [psmwpa.easywhm.com].[dbo].[SubCategoryMaster] Where SubCategoryId=@SubCategoryId)  
  ,[Stock],[ProductType],[TaxRate],[PackingAutoId],P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],
  (select isnull(Status,0)  from [dbo].[RealProductLocation] where ProductAutoId=PM.AutoId and LocationId=9) as ProductStatus,
  [CreateBy],[CreateDate],[ModifiedBy],GETDATE(),IsApply_Oz,WeightOz,      
  (Select BM.AutoId from [psmwpa.easywhm.com].[dbo].[BrandMaster] as BM Where BrandId=@BrandID),NewSRP   ,
  ThirdImage,ThumbnailImageUrl,OriginalImageUrl
  FROM [dbo].[ProductMaster] as PM where ProductId =@ProductId            
 END        
 ELSE        
 BEGIN        
  UPDATE ESY SET ESY.[ProductName]=NJ.[ProductName],ESY.[ImageUrl]=NJ.ImageUrl,  
  ESY.[CategoryAutoId]=(Select AutoId from [psmwpa.easywhm.com].[dbo].[CategoryMaster] Where CategoryId=@CategoryId),          
  ESY.[SubcategoryAutoId]=(Select AutoId from [psmwpa.easywhm.com].[dbo].[SubCategoryMaster] Where SubCategoryId=@SubCategoryId),  
  ESY.ModifiedDate=GETDATE(),        
  ESY.P_CommCode=NJ.P_CommCode,BrandAutoId= (Select AutoId from [psmwpa.easywhm.com].[dbo].[BrandMaster] Where BrandId=@BrandID),  
  ESY.WeightOz=nj.WeightOz,ESY.MLQty=nj.MLQty ,--ESY.NewSRP=NJ.NewSRP ,
  ESY.ThirdImage=nj.ThirdImage,ESY.ThumbnailImageUrl=nj.ThumbnailImageUrl,ESY.OriginalImageUrl=nj.OriginalImageUrl
  FROM [psmwpa.easywhm.com].[dbo].[ProductMaster] AS ESY INNER JOIN ProductMaster AS NJ        
  ON ESY.ProductId=NJ.ProductId where NJ.ProductId =@ProductId        
 END   

 IF NOT EXISTS(SELECT [AutoId] FROM [psmny.easywhm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)          
 BEGIN         
  INSERT INTO [psmny.easywhm.com].[dbo].[ProductMaster]        
  (        
  [ProductId],[ProductName],[ProductLocation],[ImageUrl],[CategoryAutoId],[SubcategoryAutoId],[Stock],[ProductType],[TaxRate],[PackingAutoId]        
  ,P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],[ProductStatus],[CreateBy],[CreateDate],[ModifiedBy],  
  [ModifiedDate],IsApply_Oz,WeightOz,BrandAutoId,NewSRP,ThirdImage,ThumbnailImageUrl,OriginalImageUrl)       
           
  select [ProductId],[ProductName],[ProductLocation],[ImageUrl],     
  (Select AutoId from [psmny.easywhm.com].[dbo].[CategoryMaster] Where CategoryId=@CategoryId),    
  (Select AutoId from [psmny.easywhm.com].[dbo].[SubCategoryMaster] Where SubCategoryId=@SubCategoryId)  
  ,[Stock],[ProductType],[TaxRate],[PackingAutoId],P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],
  (select isnull(Status,0)  from [dbo].[RealProductLocation] where ProductAutoId=PM.AutoId and LocationId=10) as ProductStatus,
  [CreateBy],[CreateDate],[ModifiedBy],GETDATE(),IsApply_Oz,WeightOz,      
  (Select BM.AutoId from [psmny.easywhm.com].[dbo].[BrandMaster] as BM Where BrandId=@BrandID),NewSRP     ,
  ThirdImage,ThumbnailImageUrl,OriginalImageUrl
  FROM [dbo].[ProductMaster] as PM where ProductId =@ProductId            
 END        
 ELSE        
 BEGIN        
  UPDATE ESY SET ESY.[ProductName]=NJ.[ProductName],ESY.[ImageUrl]=NJ.ImageUrl,  
  ESY.[CategoryAutoId]=(Select AutoId from [psmny.easywhm.com].[dbo].[CategoryMaster] Where CategoryId=@CategoryId),          
  ESY.[SubcategoryAutoId]=(Select AutoId from [psmny.easywhm.com].[dbo].[SubCategoryMaster] Where SubCategoryId=@SubCategoryId),  
  ESY.ModifiedDate=GETDATE(),        
  ESY.P_CommCode=NJ.P_CommCode,BrandAutoId= (Select AutoId from [psmny.easywhm.com].[dbo].[BrandMaster] Where BrandId=@BrandID),  
  ESY.WeightOz=nj.WeightOz,ESY.MLQty=nj.MLQty ,--ESY.NewSRP=NJ.NewSRP ,
  ESY.ThirdImage=nj.ThirdImage,ESY.ThumbnailImageUrl=nj.ThumbnailImageUrl,ESY.OriginalImageUrl=nj.OriginalImageUrl
  FROM [psmny.easywhm.com].[dbo].[ProductMaster] AS ESY INNER JOIN ProductMaster AS NJ        
  ON ESY.ProductId=NJ.ProductId where NJ.ProductId =@ProductId        
 END   
END        