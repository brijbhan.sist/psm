 Alter procedure ProcInvalidPackigDetailsEmail 
 AS                 
 BEGIN

	Declare @html varchar(Max)='',@Location varchar(50),@tr varchar(max),@td varchar(max)='',@num int,@query nvarchar(MAX),@count int,
	@ProductId varchar(15),@ProductName varchar(50),@ProductImage nvarchar(max),@CreateBy varchar(20),@CreateDate varchar(20),@LastUpdateDate varchar(20)
	

	select Row_Number() over (order by PM.AutoId) as rownumber,ProductId,ProductName,
	(SELECT top 1 ImageUrl FROM ProductImageUrl)+Case when ISNUll(PM.ThumbnailImageUrl,'')=''
	then 'productThumbnailImage/100_100_Thumbnail_default_pic.png' else PM.ThumbnailImageUrl end
	as ImageUrl,ISNULL(FORMAT(PM.CreateDate,'MM/dd/yyyy hh:mm tt'),'') as CreateDate,
	ISNULL(Format(ModifiedDate,'MM/dd/yyyy hh:mm tt'),'') as LastUpdateDate,	
	ISNUll((Select FirstName+' '+LastName FROM EmployeeMaster where AutoId=pm.CreateBy),'') as CreateBy
	into #Result 
	from ProductMaster as pm where pm.AutoId not in
	(
	Select pd.productAutoId from PackingDetails pd where pd.ProductAutoId=pm.AutoId
	and pd.UnitType=pm.PackingAutoId
	) 

	set @html='
	    <table class="table tableCSS" border="1" style="width:100%;border: 1px solid black;border-collapse:collapse;">
		<thead>
			<tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
				<td style="text-align: center !important;">
					<h4 style="margin:10px;">Invalid Packing Details</h4>
				</td>
				</tr>
				<tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
				<td style="text-align: center !important;">
					<h4 style="margin:2px">'+(select CompanyDistributerName from CompanyDetails)+'</h4>
				</td>
				</tr>
				<tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
				<td style="text-align: center !important;">
					<h4 style="margin:2px">'+Convert(varchar(50),FORMAT(GetDate(),'MM/dd/yyyy hh:mm tt'))+'</h4>
				</td>
			</tr>
		</thead>
		<tbody></tbody>
		</table>
		<br>
		<table class="table tableCSS" border="1" style="width:100%;border: 1px solid black;border-collapse:collapse;"> 
		<thead>
		<tr>
			<td style=''text-align:center;''><b>Product Id</b></td>
			<td style=''text-align:center;''><b>Product Name</b></td>
			<td style=''text-align:center;''><b>Image</b></td>
			<td style=''text-align:center;''><b>Created By</b></td>
			<td style=''text-align:center;''><b>Created Date</b></td>
			<td style=''text-align:center;''><b>Last Update Date</b></td>
       </tr>
	   </thead>
		<tbody>'
		SET @count= (select count(1) from #Result)
		SET @num=1
		while(@num<=@Count)
		BEGIN 
				select 
				@ProductId=ProductId,
				@ProductName=ProductName,
				@ProductImage= ImageUrl,
				@CreateBy=CreateBy,
				@CreateDate=CreateDate,
				@LastUpdateDate=LastUpdateDate
				from #Result where rownumber = @num 
				set @tr ='<tr>' 
								+	'<td style=''text-align:center;''>'+ @ProductId + '</td>'
								+	'<td style=''text-align:left;''>'+ @ProductName + '</td>'
								+	'<td style=''text-align:center;''><img src='+@ProductImage+' alt="" style="width:50px;height:50px" class="CToWUd"></td>'
								+	'<td style=''text-align:center;''>'+ @CreateBy + '</td>'
								+	'<td style=''text-align:center;''>'+ @CreateDate + '</td>'
								+	'<td style=''text-align:center;''>'+ @LastUpdateDate + '</td>'

						+ '</tr>'
		set @num = @num + 1
		SET @html=@html+@tr
		END
		SET @html=@html+'</tbody></table>'
	--EXEC dbo.spWriteToFile @DriveLocation, @html 

	-----------------------------------------Code For Email---------------------------------------------------------------
	Declare  @Subject varchar(250),@FromName varchar(500),@FromEmailId varchar(500),@Port int,@SMTPServer varchar(150),@smtp_userName varchar(150),
	@Password varchar(50),@SSL  int,@BCCEmailId varchar(max),@ToEmailId varchar(500),@CCEmailId varchar(max)
	set @Subject = 'A1WHM -' +UPPER((select CompanyDistributerName from CompanyDetails ))+' -  Invalid Packing Details' 
	
	SELECT @FromEmailId=FromEmail,@FromName=DisplayName,@smtp_userName=SMTP_USERNAME,@Password=SMTP_PASSWORD,
	@SMTPServer=HOST,@Port=SMTPPORT,@SSL=EnableSsl from EmailServerMaster_AWS 

	set @BCCEmailId=(select BCCEmail from Tbl_ReportEmailReceiver where Autoid=14)
	set @ToEmailId=(select ToEmail from Tbl_ReportEmailReceiver where Autoid=14)
	SET @CCEmailId=(select CCEmail from Tbl_ReportEmailReceiver where Autoid=14)
    
	if(select COUNT(1) from #Result)>0
	BEGIN
			EXEC [Emailing_DB].[dbo].[ProcSubmitEmail] 
			@Opcode=11,
			@FromEmailId =@FromEmailId,
			@FromName = @FromName,
			@smtp_userName=@smtp_userName,
			@Password = @Password,
			@SMTPServer = @SMTPServer,
			@Port =@Port,
			@SSL =@SSL,
			@ToEmailId =@ToEmailId,
			@CCEmailId =@CCEmailId,
			@BCCEmailId =@BCCEmailId,  
			@Subject =@Subject,
			@EmailBody = @html,
			@SentDate ='',
			@Status =0,
			@SourceApp ='PSM',
			@SubUrl ='Invalid Packing Details', 
			@isException=0,
			@exceptionMessage=''  
	END
  END