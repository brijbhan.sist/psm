--ALTER TABLE CreditMemoLog ADD ActionIPAddress VARCHAR(100)
alter PROCEDURE [dbo].[ProcManagerCreditMemo]          
@Opcode INT=NULL,     
@IPAddress VARCHAR(100)=NULL,
@CreditAutoId INT=NULL,          
@CustomerAutoId INT=NULL,          
@CreditNo varchar(200)=null,          
@EmpAutoId INT=NULL,           
@CreditType INT=NULL,          
@FromDate DATE=NULL,          
@ToDate DATE=NULL,          
@ProductAutoId INT=NULL,          
@Remark varchar(500)=NULL,          
@Qty INT=NULL,          
@UnitAutoId INT=NULL,          
@Status INT=NULL,          
@EmpType  INT=NULL,          
@TableValue DT_ManagerCreditItem readonly, 
@TotalAmount decimal(18,2) =NULL,          
@OverallDisc  decimal(18,2) =NULL,          
@OverallDiscAmt  decimal(18,2) =NULL,          
@TotalTax  decimal(18,2) =NULL,          
@GrandTotal  decimal(18,2) =NULL,          
@TaxType  int =NULL,          
@TaxValue  decimal(18,2) =NULL,          
@MLQty  decimal(18,2) =NULL,          
@MLTax  decimal(18,2) =NULL,          
@AdjustmentAmt  decimal(18,2) =NULL,          
@ManagerRemark varchar(500)=null,        
@SecurityKey varchar(50)=null,         
@CancelRemarks varchar(250)=null,  
@PageIndex INT = NULL,
@CreditMemoType INT = NULL,
@ReferenceOrderAutoId INT = NULL,          
@PageSize INT = 10,          
@RecordCount INT =null,          
@isException BIT OUT,          
@exceptionMessage VARCHAR(max) OUT          
AS          
BEGIN          
 BEGIN TRY          
  SET @isException=0          
  SET @exceptionMessage='Success'          
  DECLARE @STATE INT           
   If @Opcode=41           
   BEGIN          
     SET @EmpType=(SELECT EmpType FROM EmployeeMaster WHERE AutoId=@EmpAutoId)          
     SELECT [AutoId],[CustomerId] + ' - ' + [CustomerName] As Customer FROM [dbo].[CustomerMaster]          
     WHERE @EmpType!=2 OR [SalesPersonAutoId]=@EmpAutoId          
    SELECT [AutoId],convert(varchar(10),[ProductId]) + '--' + [ProductName] as [ProductName] FROM [dbo].[ProductMaster]          
    SELECT convert(varchar(10),GETDATE(),101)  as CurrentDate,OrderType AS CreditType FROM OrderTypeMaster WHERE Type='CREDIT' AND case when @EmpType =10 then  2 else 1 end=AutoId

   END          
  ELSE If @Opcode=42           
   BEGIN          
     SELECT UM.AutoId AS AutoId, UM.[UnitType] AS UnitType, [Qty] FROM [dbo].[PackingDetails] AS PD          
      INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = PD.UnitType WHERE PD.[ProductAutoId] = @ProductAutoId           
     SELECT PackingAutoId AS AutoId FROM ProductMaster  WHERE  AutoId=@ProductAutoId          
   END          
  ELSE If @Opcode=43           
   BEGIN          
             
    SELECT PM.AutoId AS ProductAutoId,PD.UnitType AS UnitAutoId,Qty AS QtyPerUnit,@Qty as ReturnQty,10000 AS Qty           
    ,PM.ProductId,PM.ProductName,UM.UnitType,PM.TaxRate,MinPrice,ISNULL(PPL.[CustomPrice],          
    [Price]) as CostPrice,Price,0.00 as SRP,ISNULL(PPL.[CustomPrice],[Price]) * @Qty AS NetPrice,MLQty FROM  ProductMaster AS PM           
    INNER JOIN PackingDetails AS PD ON PM.AutoId =PD.ProductAutoId          
    INNER JOIN UnitMaster AS UM ON PD.UnitType=UM.AutoId          
    LEFT JOIN [dbo].[ProductPricingInPriceLevel] AS PPL ON PPL.[ProductAutoId] = PM.[AutoId] AND PPL.[UnitAutoId] = PD.[UnitType]          
    AND [PriceLevelAutoId] = (SELECT [PriceLevelAutoId] FROM [dbo].[CustomerPriceLevel] WHERE [CustomerAutoId]=@CustomerAutoId)          
    WHERE PM.AutoId=@ProductAutoId AND PD.UnitType=@UnitAutoId             
               
   END          
   ELSE IF @Opcode=44          
   BEGIN          
       SET @EmpType =(SELECT EmpType FROM  EmployeeMaster WHERE AutoId=@EmpAutoId)          
       SELECT ROW_NUMBER() OVER(ORDER BY OrderNo desc) AS RowNumber, * INTO #Results from          
    (             
    SELECT OM.CreditAutoId AS [AutoId],OM.CreditNo AS OrderNo,CONVERT(VARCHAR(20), OM.CreditDate, 101) AS OrderDate,CM.[CustomerName] AS CustomerName,          
    SM.StatusType AS Status,OM.[Status] As StatusCode,          
    (SELECT SUM(NetAmount) from [dbo].CreditItemMaster WHERE CreditAutoId=OM.CreditAutoId) AS [GrandTotal],          
    (SELECT COUNT(distinct [ProductAutoId]) from [dbo].CreditItemMaster WHERE CreditAutoId=OM.CreditAutoId) AS NoOfItems ,          
    OT.OrderType as CreditType,          
    (SELECT OrderNo from OrderMaster as mo where mo.AutoId=OM.OrderAutoId) as referenceorderno          
    FROM [dbo].CreditMemoMaster As OM          
    INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.AutoId = OM.CustomerAutoId 
	INNER JOIN [dbo].OrderTypeMaster AS OT ON OT.AutoId = OM.CreditType AND OT.Type='CREDIT' 
    LEFT JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.[Status] AND SM.Category = 'CreditMaster'                     
    WHERE           
    (@CustomerAutoId=0 or @CustomerAutoId is null or CustomerAutoId=@CustomerAutoId)          
    and(@CreditType=0 or @CreditType is null or CreditType=@CreditType)          
    and (@CreditNo is null or @CreditNo ='' or CreditNo like '%' + @CreditNo + '%')          
    and (@FromDate is null or @FromDate = '' or @Todate is null or @Todate = '' or (convert(date,CreditDate) between @FromDate and @Todate))              
    and (@Status is null or @Status=0 or OM.[Status]=@Status) and           
    (@EmpAutoId = 0 or ((@EmpType=6 and OM.Status!=1 and OM.Status!=5)           
    or @EmpType=7 or @EmpType=8)  OR ( @EmpType=2 AND OM.CreatedBy=@EmpAutoId) or @EmpType=1 or @EmpType=10)          
    )as t order by OrderNo          
          
    SELECT COUNT(*) as RecordCount,@PageSize as PageSize,@PageIndex as PageIndex FROM #Results          
    SELECT * FROM #Results          
    WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1          
    select @EmpType as EmpType          
   END   
   ELSE IF @Opcode=45          
   BEGIN          
   SELECT * FROM StatusMaster WHERE Category='CreditMaster'          
   END          
   ELSE IF @Opcode=46          
   BEGIN          
	SET @EmpType=(SELECT EmpType FROM EmployeeMaster WHERE AutoId=@EmpAutoId)          
	SET @Status=(SELECT Status FROM CreditMemoMaster as cmm  WHERE CreditAutoId=@CreditAutoId)          
	SET @CustomerAutoId=(SELECT CustomerAutoId FROM CreditMemoMaster as cmm         
	WHERE CreditAutoId=@CreditAutoId)          
           
	SELECT CreditNo,convert(varchar(10),CreditDate,101) as CreditDate,cmm.Status as StatusCode,StatusType AS STATUS,    
	CMM.CustomerAutoId,Remarks,ISNULL(OverallDisc,0 ) as OverallDisc,CM.CustomerName,          
	ISNULL(OverallDiscAmt,0) as OverallDiscAmt,ISNULL(TotalTax,0) as TotalTax,ISNULL(GrandTotal,TotalAmount) as GrandTotal,          
	ISNULL(MLQty,0) as MLQty,ISNULL(MLTax,0) as MLTax,ISNULL(AdjustmentAmt,0) as AdjustmentAmt,TotalAmount,
	ISNULL(MLTaxPer,0) as MLTaxPer,
	ISNULL(ApplyMLQty,0) as ApplyMLQty,ManagerRemark,ISNULL((cmm.WeightTotalQuantity),0) as WeightTotalQuantity,        
	isnull((cmm.WeightTaxAmount),0) as WeightTaxAmount,ISNULL((WeightTax),0) as WeightTax,cmm.TaxTypeAutoId,--  TaxTypeAutoId Added By Rizwan Ahmad on 11/09/2019 02:20 AM      
	(SELECT OrderNo from OrderMaster as mo where mo.AutoId=cmm.OrderAutoId) as referenceorderno,      
	OT.OrderType as CreditType,isnull(cmm.ReferenceOrderAutoId,0) as ReferenceOrderAutoId,isnull(CreditMemoType,0) as CreditMemoType --updated by satish 11/04/2019        
	FROM CreditMemoMaster as cmm          
	INNER JOIN StatusMaster AS SM ON SM.AutoId=CMM.Status and Category='CreditMaster'           
	INNER JOIN CustomerMaster as CM on CMM.CustomerAutoId=cm.AutoId
	INNER JOIN [dbo].OrderTypeMaster AS OT ON OT.AutoId = cmm.CreditType AND OT.Type='CREDIT' 
	Left JOIN TaxTypeMaster as TTM ON cmm.TaxTypeAutoId=TTM.AutoId      
	WHERE CreditAutoId=@CreditAutoId          
                  
	SELECT ItemAutoId,PM.ProductId,PM.ProductName,UM.UnitType,CIM.ItemAutoId,CIM.ProductAutoId,CIM.UnitAutoId,CIM.RequiredQty,
	CIM.TotalPeice,CIM.AcceptedQty,CIM.AcceptedTotalQty,CIM.QtyPerUnit,CIM.UnitPrice,CIM.ManagerUnitPrice,          
	CIM.SRP,CIM.TaxRate,CIM.NetAmount,isnull(CIM.UnitMLQty,0) as UnitMLQty,
	isnull(CIM.TotalMLTax,0) as TotalMLTax,10000 as MAXQty,
	isnull((PM.WeightOz),0) as WeightOz
	,CIM.QtyPerUnit_Fresh,ISNULL(QtyReturn_Fresh,0)  as QtyReturn_Fresh,QtyPerUnit_Damage,isnull(QtyReturn_Damage,0)QtyReturn_Damage,
	QtyPerUnit_Missing,isnull(QtyReturn_Missing,0)QtyReturn_Missing
	FROM CreditItemMaster as CIM           
	INNER JOIN ProductMaster AS PM ON PM.AutoId=CIM.ProductAutoId          
	INNER JOIN UnitMaster AS UM ON UM.AutoId=CIM.UnitAutoId 
	
	WHERE CreditAutoId=@CreditAutoId          
              
              
	SELECT EmpType FROM EmployeeMaster WHERE AutoId=@EmpAutoId          
                     
	SELECT 'Sales Manager' as EmpType,ManagerRemark as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName,* from CreditMemoMaster as cmm                   
	left join EmployeeMaster as emp on emp.AutoId=cmm.ApprovedBy                   
	where CreditAutoId=@CreditAutoId        
        
	Declare @StateId int,@DefaultShipAdd int  --New Statement Added By Rizwan Ahmad on 11/09/2019 02:20 AM      
	Select @CustomerAutoId=CustomerAutoId FROM CreditMemoMaster WHERE CreditAutoId=@CreditAutoId      
	Select @DefaultShipAdd=DefaultShipAdd from CustomerMaster where AutoId=@CustomerAutoId      
	Select @StateId=State from ShippingAddress Where AutoId=@DefaultShipAdd      
	Select AutoId,TaxableType,Value from TaxTypeMaster Where State=@StateId and Status=1    
 
	select ProductId,um.UnitType,pd.UnitType as UnitAutoId,Qty from CreditItemMaster as CIM                                                                                                                                                    
	inner join ProductMaster as pm on pm.AutoId=CIM.ProductAutoId                                                                                
	inner join PackingDetails as pd on pd.ProductAutoId=pm.AutoId                                                                                                             
	inner join UnitMaster as um on um.AutoId=pd.UnitType 
	WHERE CreditAutoId=@CreditAutoId  

	SELECT CustomerAutoId,OM.AutoId,[OrderNo],CONVERT(VARCHAR(20),[OrderDate],101)
	AS OrderDate,DO.[AmtDue],DO.[GrandTotal],DO.[AmtPaid]                                                                    
	FROM [dbo].[OrderMaster] As OM                                                                    
	INNER JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId]                                                                         
	WHERE [CustomerAutoId] = @CustomerAutoId AND DO.[AmtDue] > 0.00 

	SELECT AutoId,CreditType FROM CreditMemoTypeMaster	
   END          
   ELSE IF @Opcode=21          
   BEGIN          
      BEGIN TRY          
        BEGIN TRAN          
       DELETE FROM CreditItemMaster WHERE CreditAutoId=@CreditAutoId          
                      INSERT INTO CreditItemMaster(CreditAutoId,ProductAutoId,UnitAutoId,RequiredQty,AcceptedQty,QtyPerUnit,UnitPrice,ManagerUnitPrice,SRP,TaxRate,UnitMLQty)          
       SELECT @CreditAutoId,ProductAutoId,UnitAutoId,RequiredQty,RequiredQty,QtyPerUnit,UnitPrice,UnitPrice,SRP,Tax,MLQty FROM @TableValue          
       as temp inner join ProductMaster as pm on pm.AutoId=temp.ProductAutoId                
                      UPDATE CreditMemoMaster SET            
                      OverallDiscAmt=@OverallDiscAmt,                                        
                      SalesAmount=@GrandTotal,PaidAmount=0,TaxTypeAutoId=@TaxType,TaxValue=@TaxValue          
                      WHERE CreditAutoId=@CreditAutoId          
       SET @Remark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=9), '[MemoNo]', (SELECT CreditNo FROM CreditMemoMaster where CreditAutoId = @CreditAutoId))          
       INSERT INTO CreditMemoLog(CreditAutoId,ActionAutoId,EmpAutoId,Remarks,LogDate)          
       VALUES(@CreditAutoId,9,@EmpAutoId, @Remark,GETDATE())          
                 
     COMMIT TRAN          
     END TRY          
     BEGIN CATCH          
       ROLLBACK TRAN          
    SET @isException=1          
    SET @exceptionMessage='Oops, Some things went wrong;'          
     END CATCH          
   END          
   ELSE IF @Opcode=24          
   BEGIN          
      BEGIN TRY          
        BEGIN TRAN          
				UPDATE CreditMemoMaster SET ManagerRemark=@ManagerRemark,ReferenceOrderAutoId=@ReferenceOrderAutoId,CreditMemoType=@CreditMemoType,
				OverallDiscAmt=@OverallDiscAmt WHERE CreditAutoId=@CreditAutoId          
                
				UPDATE CIM SET AcceptedQty=temp.RequiredQty,ManagerUnitPrice=temp.UnitPrice,      
				QtyPerUnit_Fresh=TEMP.QtyPerUnit_Fresh,QtyPerUnit_Damage=TEMP.QtyPerUnit_Damage,QtyPerUnit_Missing=TEMP.QtyPerUnit_Missing,
				QtyReturn_Fresh=TEMP.QtyReturn_Fresh,QtyReturn_Damage=TEMP.QtyReturn_Damage,QtyReturn_Missing=TEMP.QtyReturn_Missing
				FROM CreditItemMaster AS CIM INNER JOIN  @TableValue AS TEMP ON TEMP.ProductAutoId=CIM.ProductAutoId AND           
				TEMP.UnitAutoId=CIM.UnitAutoId and TEMP.Tax=CIM.TaxRate WHERE CreditAutoId=@CreditAutoId               
                
				SET @Remark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=9), '[MemoNo]', (SELECT CreditNo FROM 
				CreditMemoMaster where CreditAutoId = @CreditAutoId))          
				INSERT INTO CreditMemoLog(CreditAutoId,ActionAutoId,EmpAutoId,Remarks,LogDate)          
				VALUES(@CreditAutoId,9,@EmpAutoId, @Remark,GETDATE())          
     COMMIT TRAN          
     END TRY          
     BEGIN CATCH          
       ROLLBACK TRAN          
    SET @isException=1          
    SET @exceptionMessage='Oops, Some things went wrong;'          
     END CATCH          
   END          
   ELSE IF @Opcode=31          
   BEGIN          
      BEGIN TRY          
        BEGIN TRAN          
     DELETE FROM CreditItemMaster WHERE CreditAutoId=@CreditAutoId          
     DELETE FROM CreditMemoLog WHERE CreditAutoId=@CreditAutoId          
     DELETE FROM CreditMemoMaster WHERE CreditAutoId=@CreditAutoId           
     COMMIT TRAN          
     END TRY          
     BEGIN CATCH          
       ROLLBACK TRAN          
    SET @isException=1          
    SET @exceptionMessage='Oops, Some things went wrong;'          
     END CATCH          
   END          
   ELSE IF @Opcode=47          
   BEGIN          
  SELECT CreditNo,convert(varchar(10),CreditDate,101)  AS CreditDate FROM CreditMemoMaster WHERE CreditAutoId=@CreditAutoId          
  SELECT (EM.FirstName+' '+ EM.LastName ) AS EmpName, ETM.TypeName AS EmpType,format(LogDate,'MM/dd/yyyy hh:mm tt' ) LogDate,          
  AM.[Action],Remarks FROM CreditMemoLog AS CLOG          
  INNER JOIN EmployeeMaster AS EM ON EM.AutoId=CLOG.EmpAutoId          
  INNER JOIN [dbo].[tbl_ActionMaster] As AM ON AM.[AutoId] = CLOG.[ActionAutoId]         
  inner join EmployeeTypeMaster As ETM on ETM.AutoId=EM.EmpType        
  WHERE CLOG.CreditAutoId=@CreditAutoId order by LogDate desc          
   END          
   ELSE IF @Opcode=22          
    BEGIN      
		select cim.ProductAutoId,cim.UnitAutoId,cim.CreditAutoId,pd.CostPrice  into #tmp22 from CreditItemMaster as cim inner join PackingDetails as pd on pd.ProductAutoId=cim.ProductAutoId and pd.UnitType=cim.UnitAutoId
		where CreditAutoId=@CreditAutoId
		update cm set cm.CostPrice=tmp.CostPrice from CreditItemMaster as cm inner join #tmp22 as tmp on tmp.ProductAutoId=cm.ProductAutoId and tmp.UnitAutoId=cm.UnitAutoId
		where cm.CreditAutoId=@CreditAutoId

		UPDATE CreditMemoMaster SET Status=@Status,ApprovedBy=@EmpAutoId,ApprovalDate=GETDATE(),ManagerRemark=@ManagerRemark WHERE CreditAutoId=@CreditAutoId          
		SET @Remark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=case when @Status=3 then 10 else 20 end), '[MemoNo]',  (SELECT CreditNo FROM CreditMemoMaster where CreditAutoId = @CreditAutoId))          
		
		INSERT INTO CreditMemoLog(CreditAutoId,ActionAutoId,EmpAutoId,Remarks,LogDate,ActionIPAddress)          
		VALUES(@CreditAutoId,(case when @Status=3 then 10 else 20 end),@EmpAutoId, @Remark+'<br>'+'<b>'+'Remark: '+'</b>'+@ManagerRemark,GETDATE(),@IPAddress)          
        
		
              
		if (@Status=3)          
		begin    
			UPDATE CIM SET QtyPerUnit_Fresh=TEMP.QtyPerUnit_Fresh,QtyPerUnit_Damage=TEMP.QtyPerUnit_Damage,QtyPerUnit_Missing=TEMP.QtyPerUnit_Missing,
			QtyReturn_Fresh=TEMP.QtyReturn_Fresh,QtyReturn_Damage=TEMP.QtyReturn_Damage,QtyReturn_Missing=TEMP.QtyReturn_Missing
			FROM CreditItemMaster AS CIM INNER JOIN  @TableValue AS TEMP ON TEMP.ProductAutoId=CIM.ProductAutoId AND           
			TEMP.UnitAutoId=CIM.UnitAutoId and TEMP.Tax=CIM.TaxRate WHERE CreditAutoId=@CreditAutoId  	
			
		
			SELECT TBL.ProductAutoId,QtyReturn_Fresh*PD.Qty as QtyReturn_Fresh into #Result FROM @TableValue as TBL
			inner join PackingDetails  as PD on PD.ProductAutoId=TBL.ProductAutoId
			where PD.ProductAutoId=TBL.ProductAutoId and PD.UnitType=TBL.QtyPerUnit_Fresh
			  

			select * from	#Result

			--insert into [dbo].[ProductStockUpdateLog]([ProductId],[OledStock],[NewStock],[UserAutoId],[DateTime],ReferenceId,
			--ActionRemark,ReferenceType)
			--SELECT pm.ProductId,isnull([pm].Stock,0),(isnull(pm.Stock,0)-ISNULL(RT.QtyReturn_Fresh,0)),@EmpAutoId,GETDATE(),dt.CreditAutoId,
			--'Credit Momo No -(' + om.CreditNo + ') has been approved by '+(Select FirstName+' '+LastName 
			--from Employeemaster where autoid=@EmpAutoId)+'.','CreditMemoMaster' 
			--FROM CreditItemMaster as dt
			--inner join productmaster pm on dt.[ProductAutoId] = PM.[AutoId]
			--left join #Result as RT on RT.ProductAutoId=pm.AutoId
			--inner join CreditMemoMaster om on dt.CreditAutoId=om.CreditAutoId
			--where dt.CreditAutoId=@CreditAutoId and isnull(RT.QtyReturn_Fresh,0)>0   

      
			update pm set pm.Stock= isnull(pm.Stock,0)+ ISNULL(rt.QtyReturn_Fresh,0) from  ProductMaster as pm 
			inner join CreditItemMaster as cim on pm.AutoId=cim.ProductAutoId 
			left join #Result as rt on rt.ProductAutoId=pm.AutoId
			where CreditAutoId=@CreditAutoId      
			
			insert into [dbo].[ProductStockUpdateLog]([ProductId],[OledStock],[NewStock],[UserAutoId],[DateTime],ReferenceId,
			ActionRemark,ReferenceType)
			SELECT pm.ProductId,(isnull(pm.Stock,0)-ISNULL(RT.QtyReturn_Fresh,0)),isnull([pm].Stock,0),@EmpAutoId,GETDATE(),dt.CreditAutoId,
			'Credit Momo No -(' + om.CreditNo + ') has been approved by '+(Select FirstName+' '+LastName 
			from Employeemaster where autoid=@EmpAutoId)+'.','CreditMemoMaster' 
			FROM CreditItemMaster as dt
			inner join productmaster pm on dt.[ProductAutoId] = PM.[AutoId]
			left join #Result as RT on RT.ProductAutoId=pm.AutoId
			inner join CreditMemoMaster om on dt.CreditAutoId=om.CreditAutoId
			where dt.CreditAutoId=@CreditAutoId and isnull(RT.QtyReturn_Fresh,0)>0  
		end          
    END          
    ELSE IF @Opcode=23          
    BEGIN          
      BEGIN TRY          
      BEGIN TRAN          
     SET @CustomerAutoId=(SELECT CustomerAutoId  FROM  CreditMemoMaster WHERE CreditAutoId=@CreditAutoId)          
          
     UPDATE CreditMemoMaster SET Status=4,CompletedBy=@EmpAutoId,CompletionDate=GETDATE() WHERE CreditAutoId=@CreditAutoId          
     SET @Remark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=12), '[MemoNo]',  (SELECT CreditNo FROM CreditMemoMaster where CreditAutoId = @CreditAutoId))          
     
	 INSERT INTO CreditMemoLog(CreditAutoId,ActionAutoId,EmpAutoId,Remarks,LogDate)          
     VALUES(@CreditAutoId,12,@EmpAutoId, @Remark,GETDATE())          
     INSERT INTO CreditMoneyLog([CustomerAutoId],[RefType],[RefId],TransactionType,[Amount],[EmpAutoId],[LogDate],[Status])          
     VALUES(@CustomerAutoId,'Credit Memo',@CreditAutoId,'CR',(SELECT SUM(NetAmount) FROM CreditItemMaster WHERE CreditAutoId=@CreditAutoId),@EmpAutoId,GETDATE(),0)          
      COMMIT TRAN          
   END TRY          
   BEGIN CATCH          
     ROLLBACK TRAN          
      SET @isException=1          
      SET @exceptionMessage='Oops some thing went wrong.'          
   END CATCH          
    END          
    ELSE IF @Opcode=48          
    BEGIN          
    Declare @DefBill int  
    SELECT [AutoId],Convert(varchar(10),[ProductId]) + '--' + [ProductName] as [ProductName],MLQty FROM [dbo].[ProductMaster]          
    where ProductStatus=1  order by [ProductName]
	    
	SELECT @DefBill=DefaultBillAdd FROM CustomerMaster WHERE AutoId=@CustomerAutoId	--Modified on 12/14/2019 By Rizwan Ahmad	  
    set @STATE  =(SELECT State from  BillingAddress where AutoId=@DefBill)    
	      
    SELECT TaxRate FROM MLTaxMaster WHERE TaxState=@STATE          
    SELECT * FROM TaxTypeMaster WHERE State=@STATE          
    --WHERE AutoId IN(SELECT ProductAutoId FROM #RESULTP1)          
    END          
    ELSE IF @Opcode=49          
    BEGIN          
    SELECT CompanyName,EmailAddress, [Address],[Website],[MobileNo],[FaxNo],TermsCondition,Logo FROM CompanyDetails        
         
    SELECT OM.[CreditNo] as OrderNo,CONVERT(VARCHAR(20), OM.[CreditDate], 101) AS OrderDate,               
    CM.[AutoId] As CustAutoId,CM.[CustomerId],CM.[CustomerName],CM.[Contact1] + '/' + CM.[Contact2] As Contact,CTy.CustomerType,CT.[TermsDesc],          
    BA.[Address] As BillAddr,          
    S.[StateName] AS State1,BA.[City] AS City1,BA.[Zipcode] As Zipcode1,SA.[Address] As ShipAddr,S1.[StateName] As State2,SA.[City] AS City2,          
    SA.[Zipcode] As Zipcode2,OM.[TotalAmount],ISNULL(OM.[OverallDisc],0) as OverallDisc1,ISNULL(OM.[OverallDiscAmt],0) as OverallDisc,ISNULL(OM.[TotalTax],0) as TotalTax,isnull(OM.[GrandTotal],OM.[TotalAmount]) as GrandTotal,          
    ISNULL(MLQty,0) as MLQty,isnull(MLTax,0) as MLTax,CM.ContactPersonName,ISNULL(AdjustmentAmt,0) as AdjustmentAmt,          
    CM.BusinessName,EM.[FirstName] + ' ' + EM.[LastName] As SalesPerson FROM [dbo].[CreditMemoMaster] As OM          
    INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                     
  INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]          
    INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[CustomerAutoId] = OM.[CustomerAutoId] AND BA.IsDefault=1          
    INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[CustomerAutoId] = OM.[CustomerAutoId] AND SA.IsDefault=1          
    LEFT JOIN [dbo].[CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                 
    INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]           
    INNER JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]           
    left JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.CreatedBy          
    WHERE OM.CreditAutoId=@CreditAutoId          
              
    SELECT CM.[CategoryName],CM.[CategoryId],OIM.ProductAutoId,PM.[ProductId],PM.[ProductName],PM.[ProductLocation],          
    OIM.[UnitAutoId] AS UnitAutoId,UM.[UnitType],OIM.[QtyPerUnit],OIM.[UnitPrice],          
    OIM.NetAmount AS  [NetPrice],OIM.AcceptedQty AS QtyShip,'' AS BarCode          
    FROM [dbo].[CreditItemMaster] AS OIM           
    INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]          
    INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitAutoId           
    INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId] WHERE CreditAutoId=@CreditAutoId           
    ORDER BY CM.[CategoryName] ASC,PM.ProductId asc         
    END          
 if @Opcode = 50        
 begin        
    IF EXISTS(SELECT TOP 1 CreditAutoId FROM  CreditMemoMaster where CreditAutoId = @CreditAutoId AND OrderAutoId is null)
	BEGIN

		IF EXISTS(SELECT TOP 1 CreditAutoId FROM  CreditMemoMaster where CreditAutoId = @CreditAutoId AND STATUS=3)
		BEGIN
			insert into [dbo].[ProductStockUpdateLog]([ProductId],[OledStock],[NewStock],[UserAutoId],[DateTime],ReferenceId,ActionRemark,ReferenceType)
			SELECT pm.ProductId,isnull([Stock],0),(isnull(pm.Stock,0)-ISNULL(AcceptedTotalQty,0)),@EmpAutoId,GETDATE(),dt.CreditAutoId,
			'Credit Momo No -(' + om.CreditNo + ') has been cancelled by '+(Select FirstName+' '+LastName from Employeemaster where autoid=@EmpAutoId)+'.','CreditMemoMaster' 
			FROM (
			select cim.CreditAutoId,cim.ProductAutoId,SUM(ISNULL(QtyReturn_Fresh,0)*Qty) as AcceptedTotalQty  from CreditItemMaster as cim
					inner join PackingDetails as pd on pd.ProductAutoId=cim.ProductAutoId and 
					cim.QtyPerUnit_Fresh=pd.UnitType
					where CreditAutoId=@CreditAutoId
					group by cim.ProductAutoId,cim.CreditAutoId
			) as dt
			inner join productmaster pm on dt.[ProductAutoId] = PM.[AutoId]
			inner join CreditMemoMaster om on dt.CreditAutoId=om.CreditAutoId
			where dt.CreditAutoId=@CreditAutoId and isnull(dt.AcceptedTotalQty,0)>0   

			UPDATE PM SET [Stock] = isnull([Stock],0) - (isnull(AcceptedTotalQty,0) ) FROM [dbo].[ProductMaster] AS PM                                                                                                   
			INNER JOIN (
					select cim.ProductAutoId,SUM(ISNULL(QtyReturn_Fresh,0)*Qty) as AcceptedTotalQty  from CreditItemMaster as cim
					inner join PackingDetails as pd on pd.ProductAutoId=cim.ProductAutoId and 
					cim.QtyPerUnit_Fresh=pd.UnitType
					where CreditAutoId=@CreditAutoId
					group by cim.ProductAutoId
			) as dt ON dt.[ProductAutoId] = PM.[AutoId] 
				
		END

		update CreditMemoMaster set Status = 6,CancelRemark = @CancelRemarks,UpdatedBy = @EmpAutoId,UpdatedDate = GETDATE() where CreditAutoId = @CreditAutoId        
		SET @Remark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=20), '[MemoNo]', (SELECT CreditNo FROM CreditMemoMaster where CreditAutoId = @CreditAutoId))          
		INSERT INTO CreditMemoLog(CreditAutoId,ActionAutoId,EmpAutoId,Remarks,LogDate,ActionIPAddress)          
		VALUES(@CreditAutoId,20,@EmpAutoId, @Remark + '<br><b>Remark:</b> ' + @CancelRemarks ,GETDATE(),@IPAddress)   
	END  
	ELSE
	BEGIN
	    declare @OrderAutoId int =(SELECT top 1 OrderAutoId FROM  CreditMemoMaster where CreditAutoId = @CreditAutoId)
		SET @isException=1          
		SET @exceptionMessage='You can not cancel this credit memo becuase this already attached in <b>'+ (select OrderNO from OrderMaster where AutoId=@OrderAutoId)+' </b>Order no.'
	END
	     
 end        
 if @Opcode = 51        
 begin        
 select * from TBL_MST_SecurityMaster where SecurityValue = @SecurityKey and SecurityType = @EmpAutoId and typeEvent = 6        
 end        
 END TRY          
 BEGIN CATCH          
   SET @isException=1          
   SET @exceptionMessage=ERROR_MESSAGE()          
 END CATCH          
END            
GO
