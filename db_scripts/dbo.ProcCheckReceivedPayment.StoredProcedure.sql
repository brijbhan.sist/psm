ALTER procedure [dbo].[ProcCheckReceivedPayment]      
@OpCode int=Null,      
@Customer int = null,                                                                        
@ReceivedBy int = null,      
@Status int = null,                                                                                                             
@FromDate date=NULL,                                          
@ToDate date=NULL,      
@DepositFromDate date=NULL,    
@DepositToDate date=NULL,    
@PageIndex INT =1,                
@PageSize INT=10,                                                                                                                                                   
@isException bit out,                                          
@exceptionMessage varchar(max) out       
as      
BEGIN                                          
 BEGIN TRY                                          
   SET @isException=0                                                           
   SET @exceptionMessage='success'       
   if @OpCode = 41      
   begin      
		SELECT ROW_NUMBER() OVER(ORDER BY convert(date,tdate) desc ) AS RowNumber, * INTO #Results FROM      
		(      
		select spm.PayId,format(spm.ReceivedDate,'MM/dd/yyy') as ReceivedDate,ReceivedDate as tdate,cm.CustomerId,cm.CustomerName,spm.ReceivedAmount,      
		(case when trim(isnull(spm.ChequeNo,'')) ='' then case when isnull(spm.ReferenceId,'')='' then  'N/A' else spm.ReferenceId end            
		else spm.ChequeNo end) as CheckNo ,(em.FirstName + ' ' + em.LastName) as ReceivedBy,(em2.FirstName + ' ' + em2.LastName) as SettelledBy,    
		FORMAT(cpd.PaymentDate, 'MM/dd/yyy') as PaymentDate,sm.StatusType,spm.Status,FORMAT(spm.ChequeDate,'MM/dd/yyy') as CheckDepositDate --11/26/2019 By Rizwan Ahmad        
		from SalesPaymentMaster spm      
		inner join CustomerMaster cm on cm.AutoId = spm.CustomerAutoId      
		inner join EmployeeMaster em on em.AutoId = spm.ReceiveBy      
		left join CustomerPaymentDetails cpd on cpd.refPaymentId = spm.PaymentAutoId      
		left join EmployeeMaster em2 on em2.AutoId = cpd.EmpAutoId      
		inner join StatusMaster sm on sm.AutoId = spm.Status and sm.Category = 'Pay'      
		where spm.PaymentMode = 2      
		and(@FromDate is null or @FromDate = '' or @Todate is null or @Todate = '' or (ReceivedDate between @FromDate and @Todate))      
		and(@DepositFromDate is null or @DepositFromDate='' or   @DepositToDate is null or @DepositToDate=''     
		or(spm.ChequeDate between @DepositFromDate and @DepositToDate)) --11/26/2019 By Rizwan Ahmad      
		and(@Status is null or @Status=0 or spm.Status in (@Status))      
		and(@Customer is null or @Customer=0 or spm.CustomerAutoId=@Customer )      
		and(@ReceivedBy is null or @ReceivedBy=0 or spm.ReceiveBy=@ReceivedBy )      
		) as t  order by convert(date,tdate)  desc    
          
		SELECT COUNT(PayId) AS RecordCount, case when @PageSize=0 then COUNT(PayId) else @PageSize end AS PageSize, @PageIndex AS PageIndex,Format(getdate(),'MM/dd/yyyy hh:mm tt') as PrintDate FROM #Results      
		SELECT * FROM #Results      
		WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))    
		order by convert(date,tdate)  desc

		SELECT ISNULL(SUM(ReceivedAmount),0.00) as ReceivedAmount FROM #Results      
   end      
   if @OpCode = 42      
   begin     
   select 
   (
   SELECT AutoId AS CID,CustomerId+' '+CustomerName as CUN FROM CustomerMaster   WHERE            
   status=1 order by replace(CustomerId+' '+CustomerName,' ','') ASC     
   for json path
   ) as CUstomer,
   (
    select distinct AutoId as ReceivedBy,(FirstName + ' ' + LastName) as EmployeeName from EmployeeMaster cpd      
    where Status = 1 and EmpType in (2,10,5) order by EmployeeName    
	for json path
    ) as EType,
	(
    select AutoId as SId,StatusType as Stype from StatusMaster where Category = 'Pay'  order by Stype   
	for json path
    ) as Stype
	for json path
   end      

 END TRY                                          
 BEGIN CATCH                                              
    SET @isException=1                                          
    SET @exceptionMessage='Oops! Something went wrong.Please try later.'                                         
 END CATCH                                          
END 
GO
