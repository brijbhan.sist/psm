USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcSubCategoryMaster_ALL_Delete]    Script Date: 08-09-2020 03:39:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[ProcSubCategoryMaster_ALL_Delete]      
@SubCategoryId  VARCHAR(50)=Null      
AS      
BEGIN       
    
 DECLARE @SubCategoryAutoId INT= (SELECT AutoId FROM [psmct.a1whm.com].[dbo].[SubCategoryMaster] WHERE SubcategoryId=@SubCategoryId)    
 IF NOT EXISTS(SELECT * FROM [psmct.a1whm.com].[dbo].[ProductMaster] WHERE SubcategoryAutoId=@SubCategoryAutoId)      
 BEGIN      
  DELETE FROM [psmct.a1whm.com].[dbo].[SubCategoryMaster] WHERE SubcategoryId=@SubCategoryId    
 END      
   
 SET @SubCategoryAutoId= (SELECT AutoId FROM [psmpa.a1whm.com].[dbo].[SubCategoryMaster] WHERE SubcategoryId=@SubCategoryId)    
 IF NOT EXISTS(SELECT * FROM [psmpa.a1whm.com].[dbo].[ProductMaster] WHERE SubcategoryAutoId=@SubCategoryAutoId)      
 BEGIN      
  DELETE FROM [psmpa.a1whm.com].[dbo].[SubCategoryMaster] WHERE SubcategoryId=@SubCategoryId    
 END   
  
 SET @SubCategoryAutoId= (SELECT AutoId FROM [psmnpa.a1whm.com].[dbo].[SubCategoryMaster] WHERE SubcategoryId=@SubCategoryId)    
 IF NOT EXISTS(SELECT * FROM [psmnpa.a1whm.com].[dbo].[ProductMaster] WHERE SubcategoryAutoId=@SubCategoryAutoId)      
 BEGIN      
  DELETE FROM [psmnpa.a1whm.com].[dbo].[SubCategoryMaster] WHERE SubcategoryId=@SubCategoryId    
 END  
  
 SET @SubCategoryAutoId= (SELECT AutoId FROM [psmwpa.a1whm.com].[dbo].[SubCategoryMaster] WHERE SubcategoryId=@SubCategoryId)    
 IF NOT EXISTS(SELECT * FROM [psmwpa.a1whm.com].[dbo].[ProductMaster] WHERE SubcategoryAutoId=@SubCategoryAutoId)      
 BEGIN      
  DELETE FROM [psmwpa.a1whm.com].[dbo].[SubCategoryMaster] WHERE SubcategoryId=@SubCategoryId    
 END  
  
  SET @SubCategoryAutoId= (SELECT AutoId FROM [psmny.a1whm.com].[dbo].[SubCategoryMaster] WHERE SubcategoryId=@SubCategoryId)    
 IF NOT EXISTS(SELECT * FROM [psmny.a1whm.com].[dbo].[ProductMaster] WHERE SubcategoryAutoId=@SubCategoryAutoId)      
 BEGIN      
  DELETE FROM [psmny.a1whm.com].[dbo].[SubCategoryMaster] WHERE SubcategoryId=@SubCategoryId    
 END

 SET @SubCategoryAutoId= (SELECT AutoId FROM [psmnj.easywhm.com].[dbo].[SubCategoryMaster] WHERE SubcategoryId=@SubCategoryId)    
 IF NOT EXISTS(SELECT * FROM [psmnj.easywhm.com].[dbo].[ProductMaster] WHERE SubcategoryAutoId=@SubCategoryAutoId)      
 BEGIN      
  DELETE FROM [psmnj.easywhm.com].[dbo].[SubCategoryMaster] WHERE SubcategoryId=@SubCategoryId    
 END 
 
 SET @SubCategoryAutoId= (SELECT AutoId FROM [psmpa.easywhm.com].[dbo].[SubCategoryMaster] WHERE SubcategoryId=@SubCategoryId)    
 IF NOT EXISTS(SELECT * FROM [psmpa.easywhm.com].[dbo].[ProductMaster] WHERE SubcategoryAutoId=@SubCategoryAutoId)      
 BEGIN      
  DELETE FROM [psmpa.easywhm.com].[dbo].[SubCategoryMaster] WHERE SubcategoryId=@SubCategoryId    
 END
 
 SET @SubCategoryAutoId= (SELECT AutoId FROM [psmnpa.easywhm.com].[dbo].[SubCategoryMaster] WHERE SubcategoryId=@SubCategoryId)    
 IF NOT EXISTS(SELECT * FROM [psmnpa.easywhm.com].[dbo].[ProductMaster] WHERE SubcategoryAutoId=@SubCategoryAutoId)      
 BEGIN      
  DELETE FROM [psmnpa.easywhm.com].[dbo].[SubCategoryMaster] WHERE SubcategoryId=@SubCategoryId    
 END

 SET @SubCategoryAutoId= (SELECT AutoId FROM [psmwpa.easywhm.com].[dbo].[SubCategoryMaster] WHERE SubcategoryId=@SubCategoryId)    
 IF NOT EXISTS(SELECT * FROM [psmwpa.easywhm.com].[dbo].[ProductMaster] WHERE SubcategoryAutoId=@SubCategoryAutoId)      
 BEGIN      
  DELETE FROM [psmwpa.easywhm.com].[dbo].[SubCategoryMaster] WHERE SubcategoryId=@SubCategoryId    
 END

 SET @SubCategoryAutoId= (SELECT AutoId FROM [psmct.easywhm.com].[dbo].[SubCategoryMaster] WHERE SubcategoryId=@SubCategoryId)    
 IF NOT EXISTS(SELECT * FROM [psmct.easywhm.com].[dbo].[ProductMaster] WHERE SubcategoryAutoId=@SubCategoryAutoId)      
 BEGIN      
  DELETE FROM [psmct.easywhm.com].[dbo].[SubCategoryMaster] WHERE SubcategoryId=@SubCategoryId    
 END

 SET @SubCategoryAutoId= (SELECT AutoId FROM [psmwpa.easywhm.com].[dbo].[SubCategoryMaster] WHERE SubcategoryId=@SubCategoryId)    
 IF NOT EXISTS(SELECT * FROM [psmwpa.easywhm.com].[dbo].[ProductMaster] WHERE SubcategoryAutoId=@SubCategoryAutoId)      
 BEGIN      
  DELETE FROM [psmwpa.easywhm.com].[dbo].[SubCategoryMaster] WHERE SubcategoryId=@SubCategoryId    
 END

	SET @SubCategoryAutoId= (SELECT AutoId FROM [psmny.easywhm.com].[dbo].[SubCategoryMaster] WHERE SubcategoryId=@SubCategoryId)    
	IF NOT EXISTS(SELECT * FROM [psmny.easywhm.com].[dbo].[ProductMaster] WHERE SubcategoryAutoId=@SubCategoryAutoId)      
	BEGIN      
	DELETE FROM [psmny.easywhm.com].[dbo].[SubCategoryMaster] WHERE SubcategoryId=@SubCategoryId    
	END


END  