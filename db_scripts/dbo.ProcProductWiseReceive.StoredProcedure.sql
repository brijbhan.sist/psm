ALTER procedure [dbo].[ProcProductWiseReceive]        
@Opcode INT=NULL,        
@CustomerAutoId INT=NULL,        
@ProductAutoId INT=NULL,        
@CategoryAutoId  INT=NULL,        
@SubCategoryId INT=NULL,        
@EmpAutoId  INT=NULL,        
@CustomerTypeAutoId int = null,        
@OrderStatus INT=NULL,        
@SalesPerson  VARCHAR(500)=NULL,        
@FromDate date =NULL,        
@ToDate date =NULL,       
@BillNo varchar(150)=null,    
@PageIndex INT=1,        
@PageSize INT=10,        
@RecordCount INT=null,        
@isException bit out,        
@exceptionMessage varchar(max) out        
AS        
BEGIN         
 BEGIN TRY        
  Set @isException=0        
  Set @exceptionMessage='Success'        
        
  IF @Opcode=41        
  BEGIN        
  select
  (
  SELECT AutoId as CID,CategoryName as CN from CategoryMaster  where Status=1 order by CN ASC     
  for json path
  )as Category,
  (
  SELECT AutoId as VID ,VendorName as VN FROM VendorMaster   order by VN ASC
  for json path
  )as Vendor
  for json path
  END 
  
  ELSE IF @Opcode=42        
  BEGIN        
  SELECT AutoId as SCID,SubcategoryName as SCN from SubCategoryMaster    
  where (isnull(@CategoryAutoId,0)=0 or CategoryAutoId=@CategoryAutoId )and Status=1     
  order by SCN  ASC  
  for json path
  END          
      
       
  ELSE IF @OPCODE=43        
  BEGIN        
     SELECT  AutoId as PID,CONVERT(VARCHAR(10),ProductId )+' '+ ProductName AS PN FROM ProductMaster        
     WHERE         
     (isnull(@SubCategoryId,0)=0 or SubcategoryAutoId=@SubCategoryId )and ProductStatus=1 order by PN ASC
	 for json path    
  END        
  ELSE IF @OPCODE=44        
  BEGIN        
        
  SELECT ROW_NUMBER() OVER(ORDER BY VendorName asc, ProductName asc,DateO asc) AS RowNumber, * into #Results48        
  FROM      
  (      
  SELECT CONVERT(varchar(10),BillDate,101) as BillDate,BillDate as DateO,ProductId,ProductName,BillNo,om.VendorAutoId,      
  VendorName, cim.Price as UnitPrice,      
  (case when UnitAutoId=3 then CONVERT(VARCHAR(10),SUM(ISNULL(cim.Quantity,0))) else '0' end) as TotalPiece,      
  (case when UnitAutoId=2 then CONVERT(VARCHAR(10),SUM(ISNULL(cim.Quantity,0))) else '0' end) as TotalBox,      
  (case when UnitAutoId=1 then CONVERT(VARCHAR(10),SUM(ISNULL(cim.Quantity,0))) else '0' end) as TotalCase,      
  cast (SUM(isnull(cim.Quantity,0)*ISNULL(cim.Price,0)) as decimal(10,2))  as PayableAmount,      
  (SELECT UM.UnitType FROM UnitMaster AS UM  WHERE UM.AutoId=UnitAutoId) AS UnitName,  
  cast(cim.price/cim.QtyPerUnit as decimal(10,2)) as PricePerPiece,CAST(pd1.Price as decimal(10,2)) as sellingprice  
  from          
  StockEntry om inner join [BillItems] as cim on cim.BillAutoId=om.AutoId      
  inner join ProductMaster as pm on pm.AutoId=cim.ProductAutoId          
  inner join VendorMaster as vm on vm.AutoId=om.VendorAutoId      
  inner join PackingDetails as pd1 on pd1.ProductAutoId=pm.AutoId and pm.PackingAutoId=pd1.UnitType  
  where (ISNULL(@ProductAutoId,0) =0 or cim.ProductAutoId=@ProductAutoId)   
  and (@BillNo is null or @BillNo='' or @BillNo like '%'+BillNo+'%')  
  AND (ISNULL(@CustomerAutoId,0) =0 or om.VendorAutoId=@CustomerAutoId)      
  AND (ISNULL(@CategoryAutoId,0)=0 OR pm.CategoryAutoId =@CategoryAutoId)      
  AND (ISNULL(@SubCategoryId,0)=0 OR pm.SubcategoryAutoId =@SubCategoryId)      
  and (@FromDate is null or @ToDate is null or (CONVERT(date,om.BillDate)       
  between convert(date,@FromDate) and CONVERT(date,@ToDate)))      
       
  group by om.BillDate,ProductId,ProductName,om.BillNo,VendorName,UnitAutoId,om.VendorAutoId,  
  cim.Price,cim.QtyPerUnit,pd1.Price   
      
  ) AS T ORDER BY  VendorName asc, ProductName asc,DateO asc      
      
           
  SELECT * FROM #Results48      
  WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))      
         
  SELECT COUNT(BillDate) AS RecordCount, case when @PageSize=0 then COUNT(RowNumber) else @PageSize end AS PageSize, @PageIndex AS PageIndex,FORMAT(getdate(),'MM/dd/yyyy hh:mm tt') as PrintDate FROM #Results48      
           
    SELECT ISNULL(SUM(convert(int,TotalPiece)),0) as TotalPiece,ISNULL(SUM(convert(int,TotalBox)),0) as TotalBox,ISNULL(SUM(convert(int,TotalCase)),0) as TotalCase,ISNULL(SUM(PayableAmount),0.00) as PayableAmount FROM #Results48         
  END        
        
          
 END TRY        
 BEGIN CATCH        
  Set @isException=1        
  Set @exceptionMessage=ERROR_MESSAGE()        
 END CATCH        
END 
GO
