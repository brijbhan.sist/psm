alter procedure [dbo].[ProcSalesByBasePrice]                      
@Opcode INT=NULL,                      
@ProductAutoId int = null,              
@BrandAutoIdSring varchar(200) = null,      
@SalesPersonId varchar(200) = null, 
@CategoryAutoId int = null,      
@CustomerAutoId  int = null,                    
@SubCategoryId int = null,                      
@OrderStatus int = null,                      
@EmpAutoId int = null,                      
@SalesPerson  int=NULL,                            
@FromDate date =NULL,                      
@ToDate date =NULL,                      
@PageIndex INT=1,                      
@PageSize INT=10,                      
@RecordCount INT=null,                      
@isException bit out,                      
@exceptionMessage varchar(max) out                      
as                   
BEGIN                       
 BEGIN TRY                      
  Set @isException=0                      
  Set @exceptionMessage='Success'                      
    Declare @ParentLocation varchar(25),@ChildLocation varchar(25),@Query nvarchar(max)
   SET @ParentLocation=(Select DB_NAME())
   SET @ChildLocation=(Select ChildDB_Name from CompanyDetails)
   
  IF @Opcode=41                      
  BEGIN                          
   SELECT  PM.AutoId as PID,CONVERT(VARCHAR(10),PM.ProductId )+' - '+ PM.ProductName AS PN FROM ProductMaster AS PM              
   WHERE ((ISNULL(@BrandAutoIdSring,'')='')  or (PM.BrandAutoId in (select * from dbo.fnSplitString(@BrandAutoIdSring,','))))               
   AND (ISNULL(@CategoryAutoId,0)=0 OR pm.CategoryAutoId =@CategoryAutoId)                      
   AND (ISNULL(@SubCategoryId,0)=0 OR pm.SubcategoryAutoId =@SubCategoryId)               
   AND PM.ProductStatus=1 order by PN ASC    
   for json path
  END                     

   IF @OPCODE=42                      
  BEGIN                      
 SET @Query='
 select EM.FirstName+'' ''+ISNULL(EM.LastName,'''')SalesRep,pm.ProductId,pm.ProductName,um.UnitType ,doi.BasePrice  ,doi.QtyDel,doi.QtyPerUnit as  Qty	,
		doi.UnitPrice,doi.NetPrice 		 
		into #t1 from ['+@ParentLocation+'].[dbo].OrderMaster as om
		inner join ['+@ParentLocation+'].[dbo].Delivered_Order_Items as doi on doi.OrderAutoId=om.AutoId
		inner join ['+@ParentLocation+'].[dbo].ProductMaster as pm on pm.AutoId=doi.ProductAutoId
		inner join ['+@ParentLocation+'].[dbo].PackingDetails as pd on pd.ProductAutoId=pm.AutoId and doi.UnitAutoId=pd.UnitType
		inner join ['+@ParentLocation+'].[dbo].UnitMaster as um on um.AutoId=doi.UnitAutoId
		inner join ['+@ParentLocation+'].[dbo].EmployeeMaster as em on em.AutoId=om.SalesPersonAutoId
		where  om.Status=11';
		IF(Convert(varchar(25),@FromDate)!=null or Convert(varchar(25),@FromDate)!='')
			BEGIN
			SET @Query=@Query+' and (Convert(date,OM.OrderDate)  between '''+Convert(varchar(25),@FromDate)+'''
			And '''+Convert(varchar(25),@ToDate)+''')'
			END
		IF(ISNULL(@SalesPersonId,'0,')!='0,' AND ISNULL(@SalesPersonId,'0')!='0')
			BEGIN
			SET @Query=@Query+' AND em.AutoId in 
			(select * from dbo.fnSplitString('''+Convert(varchar(15),@SalesPersonId)+''','',''))'
			END
			SET @Query=@Query+'and ('+Convert(varchar(25),@ProductAutoId)+' is null or '+Convert(varchar(25),@ProductAutoId)+'=0 
		                or doi.ProductAutoId='+Convert(varchar(25),@ProductAutoId)+')';

            SET @Query=@Query+'and ('+Convert(varchar(25),@CategoryAutoId)+' is null or '+Convert(varchar(25),@CategoryAutoId)+'=0 
		                or pm.CategoryAutoId='+Convert(varchar(25),@CategoryAutoId)+')';

	        SET @Query=@Query+'and ('+Convert(varchar(25),@SubCategoryId)+' is null or '+Convert(varchar(25),@SubCategoryId)+'=0 
		                or pm.SubcategoryAutoId='+Convert(varchar(25),@SubCategoryId)+')';

			IF(ISNULL(@BrandAutoIdSring,'0,')!='0,' AND ISNULL(@BrandAutoIdSring,'0')!='0')
				BEGIN
				SET @Query=@Query+' AND pm.BrandAutoId in 
				(select * from dbo.fnSplitString('''+Convert(varchar(15),@BrandAutoIdSring)+''','',''))'
				END   
				
       	SET @Query=@Query+'
	
UNION ALL
		select	EM.FirstName+'' ''+ISNULL(EM.LastName,'''')SalesRep
				,pm.ProductId,pm.ProductName,um.UnitType ,oim.BasePrice  ,oim.TotalPieces,oim.QtyPerUnit as Qty ,oim.UnitPrice,oim.NetPrice 		 
		from ['+@ParentLocation+'].[dbo].OrderMaster as om
		inner join ['+@ParentLocation+'].[dbo].OrderItemMaster as oim on oim.OrderAutoId=om.AutoId
		inner join ['+@ParentLocation+'].[dbo].ProductMaster as pm on pm.AutoId=oim.ProductAutoId
		inner join ['+@ParentLocation+'].[dbo].PackingDetails as pd on pd.ProductAutoId=pm.AutoId and oim.UnitTypeAutoId=pd.UnitType
		inner join ['+@ParentLocation+'].[dbo].UnitMaster as um on um.AutoId=oim.UnitTypeAutoId
		inner join ['+@ParentLocation+'].[dbo].EmployeeMaster as em on em.AutoId=om.SalesPersonAutoId
		where  om.Status not in (11,8)' 
		IF(Convert(varchar(25),@FromDate)!=null or Convert(varchar(25),@FromDate)!='')
			BEGIN
			SET @Query=@Query+' and (Convert(date,OM.OrderDate)  between '''+Convert(varchar(25),@FromDate)+'''
			And '''+Convert(varchar(25),@ToDate)+''')'
			END
		IF(ISNULL(@SalesPersonId,'0,')!='0,' AND ISNULL(@SalesPersonId,'0')!='0')
			BEGIN
			SET @Query=@Query+' AND em.AutoId in 
			(select * from dbo.fnSplitString('''+Convert(varchar(15),@SalesPersonId)+''','',''))'
			END

			SET @Query=@Query+'and ('+Convert(varchar(25),@ProductAutoId)+' is null or '+Convert(varchar(25),@ProductAutoId)+'=0 
		                or oim.ProductAutoId='+Convert(varchar(25),@ProductAutoId)+')';

            SET @Query=@Query+'and ('+Convert(varchar(25),@CategoryAutoId)+' is null or '+Convert(varchar(25),@CategoryAutoId)+'=0 
		                or pm.CategoryAutoId='+Convert(varchar(25),@CategoryAutoId)+')';

	        SET @Query=@Query+'and ('+Convert(varchar(25),@SubCategoryId)+' is null or '+Convert(varchar(25),@SubCategoryId)+'=0 
		                or pm.SubcategoryAutoId='+Convert(varchar(25),@SubCategoryId)+')';

		IF(ISNULL(@BrandAutoIdSring,'0,')!='0,' AND ISNULL(@BrandAutoIdSring,'0')!='0')
			BEGIN
			SET @Query=@Query+' AND pm.BrandAutoId in 
			(select * from dbo.fnSplitString('''+Convert(varchar(15),@BrandAutoIdSring)+''','',''))'
			END          
     SET @Query=@Query+'

UNION ALL
		select EM.FirstName+'' ''+ISNULL(EM.LastName,'''')SalesRep
		,pm.ProductId,pm.ProductName,um.UnitType ,doi.BasePrice  ,doi.QtyDel,doi.QtyPerUnit as Qty	,doi.UnitPrice,doi.NetPrice 		 
		from ['+@ChildLocation+'].dbo.OrderMaster as om
		inner join ['+@ChildLocation+'].[dbo].Delivered_Order_Items as doi on doi.OrderAutoId=om.AutoId
		inner join ['+@ChildLocation+'].[dbo].ProductMaster as pm on pm.AutoId=doi.ProductAutoId
		inner join ['+@ChildLocation+'].[dbo].PackingDetails as pd on pd.ProductAutoId=pm.AutoId and doi.UnitAutoId=pd.UnitType
		inner join ['+@ChildLocation+'].[dbo].UnitMaster as um on um.AutoId=doi.UnitAutoId
		inner join ['+@ChildLocation+'].[dbo].EmployeeMaster as em on em.AutoId=om.SalesPersonAutoId
		where  om.Status=11';
		IF(Convert(varchar(25),@FromDate)!=null or Convert(varchar(25),@FromDate)!='')
			BEGIN
			SET @Query=@Query+' and (Convert(date,OM.OrderDate)  between '''+Convert(varchar(25),@FromDate)+'''
			And '''+Convert(varchar(25),@ToDate)+''')'
			END
		IF(ISNULL(@SalesPersonId,'0,')!='0,' AND ISNULL(@SalesPersonId,'0')!='0')
			BEGIN
			SET @Query=@Query+' AND em.AutoId in 
			(select * from dbo.fnSplitString('''+Convert(varchar(15),@SalesPersonId)+''','',''))'
			END
			SET @Query=@Query+'and ('+Convert(varchar(25),@ProductAutoId)+' is null or '+Convert(varchar(25),@ProductAutoId)+'=0 
		                or doi.ProductAutoId='+Convert(varchar(25),@ProductAutoId)+')';

            SET @Query=@Query+'and ('+Convert(varchar(25),@CategoryAutoId)+' is null or '+Convert(varchar(25),@CategoryAutoId)+'=0 
		                or pm.CategoryAutoId='+Convert(varchar(25),@CategoryAutoId)+')';

	        SET @Query=@Query+'and ('+Convert(varchar(25),@SubCategoryId)+' is null or '+Convert(varchar(25),@SubCategoryId)+'=0 
		                or pm.SubcategoryAutoId='+Convert(varchar(25),@SubCategoryId)+')';

		IF(ISNULL(@BrandAutoIdSring,'0,')!='0,' AND ISNULL(@BrandAutoIdSring,'0')!='0')
			BEGIN
			SET @Query=@Query+' AND pm.BrandAutoId in 
			(select * from dbo.fnSplitString('''+Convert(varchar(15),@BrandAutoIdSring)+''','',''))'
			END          

	SET @Query=@Query+'  SELECT ROW_NUMBER() OVER(ORDER BY ProductId asc) AS RowNumber, * into #t11                       
		   FROM                      
		   ( 
		select 
				 SalesRep,ProductId,ProductName,UnitType,BasePrice  
				 ,convert(decimal (18,2), sum(convert(decimal (18,2),QtyDel)/Qty))QtySold
				,sum(NetPrice)TotalSales_In_This_BasePrice
				,sum((case when (BasePrice = UnitPrice) then NetPrice else 0 end ))Sales_At_BasePrice 		
				,sum((case when (BasePrice != UnitPrice) then NetPrice else 0 end ))Sales_At_PriceLeavel,
				Qty
		from  #t1
		group by SalesRep  ,ProductId,ProductName,UnitType,BasePrice,Qty
		 ) AS T   
		
		select RowNumber,SalesRep,ProductId,ProductName,UnitType,BasePrice,QtySold,TotalSales_In_This_BasePrice,Sales_At_BasePrice
		,Sales_At_PriceLeavel,Qty from #t11 
			WHERE '+Convert(varchar(25),@PageSize)+' = 0 or (RowNumber BETWEEN('+Convert(varchar(10),@PageIndex)+' -1) * '+Convert(varchar(25),@PageSize)+'
		+ 1 AND((('+Convert(varchar(25),@PageIndex)+' -1) * '+Convert(varchar(25),@PageSize)+' + 1) + '+Convert(varchar(25),@PageSize)+') - 1)
		 order by  ProductId ' 

		
        	SET @Query=@Query+'SELECT COUNT(*) AS RecordCount, case when '+Convert(varchar(25),@PageSize)+'=0 then COUNT(*) else
		'+Convert(varchar(25),@PageSize)+' end AS PageSize,
		'+Convert(varchar(10),@PageIndex)+' AS PageIndex,format(getdate(),''MM/dd/yyyy hh:mm tt'') as PrintDate FROM #t11 '

			SET @Query=@Query+'select ISNULL(SUM(QtySold),0.00) as QtySold,ISNULL(SUM(TotalSales_In_This_BasePrice),0.00) as TotalSales_In_This_BasePrice,
		ISNULL(SUM(Sales_At_BasePrice),0.00) as Sales_At_BasePrice,
		ISNULL(SUM(Sales_At_PriceLeavel),0.00) as Sales_At_PriceLeavel from #t11'
	
  EXEC sp_executesql @Query
  END    
  IF @Opcode=43                      
  BEGIN                      
  SELECT AutoId as CId,CategoryName as CN from CategoryMaster where Status=1  order by CN ASC   
  for json path
  END                      
    IF @Opcode=44                      
  BEGIN                      
 SELECT AutoId as  SCID,SubcategoryName as SCN from SubCategoryMaster where               
 (isnull(@CategoryAutoId,0)=0 or CategoryAutoId=@CategoryAutoId )and Status=1 order by SCN ASC     
    for json path 
END                      
  IF @Opcode=45                      
  BEGIN 
	  select
	  (
	  SELECT AutoId AS EID,FirstName + ' ' + LastName as EN FROM EmployeeMaster where EmpType=2 and status=1 order by EN ASC                              
	  for json path
	  )as SalesPer,
	  (
	  select AutoId as BID,BrandName as BN from BrandMaster where status = 1 order by BN      
	  for json path
	  )as Brand 
	  for json path          
  END                 
  IF @Opcode=46                      
  BEGIN   
  select AutoId as CUID,CustomerName as CUN from CustomerMaster as cm where (ISNULL(@SalesPerson,0)=0 OR cm.SalesPersonAutoId =@SalesPerson) AND Status=1 order by CUN asc      
  for json path
  END                  
  END TRY                      
 BEGIN CATCH                      
  Set @isException=1                      
  Set @exceptionMessage=ERROR_MESSAGE()                      
 END CATCH                      
END 
