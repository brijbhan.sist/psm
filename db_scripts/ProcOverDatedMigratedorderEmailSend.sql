 Create or alter procedure ProcOverDatedMigratedorderEmailSend --exec ProcOverDatedMigratedorderEmailSend
 
 AS
 BEGIN 
	Declare @html varchar(Max)='',@tr varchar(max),
	@OrderNo varchar(15),@OrderDate varchar(25),@LastPaymentDate varchar(25)

	--declare  @DriveLocation varchar(150)
	--SET @DriveLocation='D:\ProductReport\'+UPPER((select CompanyId from dbo.CompanyDetails))+'_OverDatedMigrantOrder_'+
	--convert(varchar(50),Format(GetDate(),'MM_dd_yyyy_hh_mm_tt'))+'.html'

	declare @StartDate date='2020-07-16'---as per disccussion with Nilay

	select ROW_NUMBER()over(order by om.AutoId asc) as NoOfPayment,om.AutoId,OrderNo,Format(OrderDate,'mm/dd/yyyy hh:mm tt') as OrderDate,SM.StatusType as Status,
	(select top 1 PaymentDate from CustomerPaymentDetails as cpd
	inner join PaymentOrderDetails as pod on pod.PaymentAutoId=cpd.PaymentAutoId and pod.OrderAutoId=om.AutoId
	and cpd.Status=0
	and cpd.PaymentMode!=5
	order by PaymentDate desc) as LastPaymentDate	
	into #ordtemp
	from OrderMaster as om
	inner join DeliveredOrders as do on do.OrderAutoId=om.AutoId and do.AmtDue=0
	inner join ShippingType as st on st.AutoId=om.ShippingType and st.MigrateStatus=1
	INNER join StatusMaster as SM on SM.AutoId=om.Status AND SM.Category='OrderMaster'
	where om.Status=11 and CONVERT(date,OrderDate)>=CONVERT(date,@StartDate)
	and ISNULL((datediff(day,(
	select top 1 PaymentDate from CustomerPaymentDetails as cpd
	inner join PaymentOrderDetails as pod on pod.PaymentAutoId=cpd.PaymentAutoId and pod.OrderAutoId=om.AutoId
	and cpd.Status=0
	and cpd.PaymentMode!=5
	order by PaymentDate desc
	),getdate())),0)>3
	and exists(
	select top 1 cpd.CustomerAutoId from CustomerPaymentDetails as cpd
	inner join PaymentOrderDetails as pod on pod.PaymentAutoId=cpd.PaymentAutoId and pod.OrderAutoId=om.AutoId
	where cpd.Status=0
	and (select count(1) from PaymentOrderDetails as pd where pd.PaymentAutoId=cpd.PaymentAutoId)=1
	and cpd.PaymentMode!=5
	)
	
		declare @count int= (select count(1) from #ordtemp),@num int=1	    

		If(@count>0)
		BEGIN

			set @html='<table class="table tableCSS" border="1" style="width:100%;border: 1px solid black;border-collapse:collapse;" id="OverDatedMigrantOrder">
			<thead>
				<tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
					<td style="text-align: center !important;">
						<h4 style="margin:10px;">Not Migrated Orders</h4>
					</td>
					</tr>
					<tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
					<td style="text-align: center !important;">
						<h4 style="margin:2px">'+(select CompanyDistributerName from CompanyDetails)+'</h4>
					</td>
					</tr>
					<tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
					<td style="text-align: center !important;">
						<h4 style="margin:2px">'+Convert(varchar(50),FORMAT(GetDate(),'MM/dd/yyyy hh:mm tt'))+'</h4>
					</td>
				</tr>
			</thead>
			<tbody></tbody>
			</table>
			<br>'

			set @html=@html+'			
			<table class="table tableCSS" border="1" style="width:100%;border: 1px solid black;border-collapse:collapse;"> 
			<thead>
			<tr>
				<td style=''text-align:center;''><b>Order No</b></td>
				<td style=''text-align:center;''><b>Order Date</b></td>
				<td style=''text-align:center;''><b>Last Payment Date</b></td>
		   </tr>
		   </thead>
			<tbody>'
			while(@num<=@Count)
			BEGIN 
				select 
				@OrderNo=ISNULL(OrderNo,''),
				@OrderDate=ISNULL(OrderDate,''),
				@LastPaymentDate=ISNULL(Format(LastPaymentDate,'mm/dd/yyyy hh:mm tt'),'')			
				from #ordtemp where NoOfPayment = @num 
				set @tr ='<tr>' 
									+	'<td style=''text-align:center;''>'+ ISNULL(convert(varchar(50),@OrderNo),'') + '</td>'
									+	'<td style=''text-align:center;''>'+ ISNULL(convert(varchar(50),@OrderDate),'') + '</td>'
									+	'<td style=''text-align:center;''>'+ ISNULL(convert(varchar(50),@LastPaymentDate),'') + '</td>'
							+ '</tr>'
			set @num = @num + 1
			SET @html=@html+@tr
			END
			SET @html=@html+'</tbody></table>'
		END

		--EXEC dbo.spWriteToFile @DriveLocation, @html 
	-------------------------------------------Code For Email---------------------------------------------------------------
	Declare  @Subject varchar(250),@FromName varchar(500),@FromEmailId varchar(500),@Port int,@SMTPServer varchar(150),
	@Password varchar(50),@SSL  int,@BCCEmailId varchar(max),@ToEmailId varchar(500),@CCEmailId varchar(max)
	set @Subject = 'A1WHM -' +UPPER((select CompanyDistributerName from dbo.CompanyDetails))+' - Not Migrated Orders' 
	SELECT @FromEmailId=EmailId,@Port=port,@SMTPServer=server,@Password=convert(varchar(50), DecryptByPassPhrase('ESS',Pass)),
	@SSL=ssl from EmailServerMaster 
	where  SendTo='Developer' 
	set @BCCEmailId=(select BCCEmail from Tbl_ReportEmailReceiver where Autoid=8);
	set @ToEmailId=(select ToEmail from Tbl_ReportEmailReceiver where Autoid=8)
	SET @CCEmailId=(select CCEmail from Tbl_ReportEmailReceiver where Autoid=8)
    
	EXEC [Emailing_DB].[dbo].[ProcSubmitEmail] 
	@Opcode=11,
	@FromEmailId =@FromEmailId,
	@FromName = @FromName,
	@smtp_userName=@FromEmailId,
	@Password = @Password,
	@SMTPServer = @SMTPServer,
	@Port =@Port,
	@SSL =@SSL,
	@ToEmailId =@ToEmailId,
	@CCEmailId =@CCEmailId,
	@BCCEmailId =@BCCEmailId,  
	@Subject =@Subject,
	@EmailBody = @html,
	@SentDate ='',
	@Status =0,
	@SourceApp ='PSM',
	@SubUrl ='Not Migrated Orders', 
	@isException=0,
	@exceptionMessage=''  
  END