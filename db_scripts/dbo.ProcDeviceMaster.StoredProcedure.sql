 ALTER procedure [dbo].[ProcDeviceMaster]        
  @Opcode int=null,        
  @AutoId int=null,        
  @DeviceName varchar(150)=null,        
  @Createdby int=null,        
  @Status int=null,   
  @IsLocationRequired int=null,   
  @DeviceId varchar(150)=null,  
  @PageIndex INT = 1,    
  @PageSize INT = 10,                                    
  @RecordCount INT =null,                                                   
  @isException bit out,                                
  @exceptionMessage varchar(max) out          
as        
BEGIN                           
BEGIN TRY                          
  SET @isException=0                          
  SET @exceptionMessage='Success'
  Declare @ChildDbLocation varchar(50),@sql nvarchar(max)  
  Select @ChildDbLocation=ChildDB_Name from CompanyDetails
 IF @Opcode=11        
 BEGIN        
  IF exists(select * from DeviceMaster where DeviceId=@DeviceId)         
  BEGIN        
    SET @isException=1        
    SET @exceptionMessage='Device ID already exist.'        
  END        
  ELSE        
	BEGIN
	  Begin Transaction
	    Begin try
	     insert into DeviceMaster([DeviceId],[DeviceName],[Status],[CreatedBy],[CreatedDate],[IsLocationRequired]) 
		 values (@DeviceId,@DeviceName,@Status,@Createdby,GETDATE(),@IsLocationRequired) 

		 SET @AutoId=SCOPE_IDENTITY()
		 SET @sql='
		 SET IDENTITY_INSERT ['+@ChildDbLocation+'].[dbo].[DeviceMaster] ON;
		 insert into ['+@ChildDbLocation+'].[dbo].DeviceMaster 
		 (AutoId,[DeviceId],[DeviceName],[Status],[CreatedBy],[CreatedDate],[IsLocationRequired]) 
		 SELECT [AutoId],[DeviceId],[DeviceName],[Status],[CreatedBy],[CreatedDate],[IsLocationRequired] 
		 FROM DeviceMaster where Autoid='+Convert(varchar(20),@AutoId)+'
		 SET IDENTITY_INSERT ['+@ChildDbLocation+'].[dbo].[DeviceMaster] OFF;'
		 EXEC sp_executesql @sql
		 Commit transaction
		End try
		Begin Catch
		  Rollback transaction
		  SET @isException=1        
          SET @exceptionMessage=ERROR_MESSAGE() 
		End Catch
  END        
 END     
 ELSE IF @Opcode=21        
 BEGIN        
	IF  exists(select *from DeviceMaster where DeviceId=@DeviceId  and AutoId!=@AutoId)        
	BEGIN        
		SET @isException=1        
		SET @exceptionMessage='Device ID already exist.'        
	END  
	else
	BEGIN        
		UPDATE DeviceMaster         
		SET DeviceName=@DeviceName,DeviceId=@DeviceId,Status=@Status,UpdatedBy=@Createdby,UpdatedDate=GETDATE(),
		IsLocationRequired=@IsLocationRequired WHERE Autoid=@AutoId 
		SET @sql='
		UPDATE CDM SET CDM.DeviceName=DM.DeviceId,CDM.DeviceId=DM.DeviceId,CDM.Status=DM.Status,CDM.UpdatedDate=DM.UpdatedDate,
		CDM.CreatedBy=DM.CreatedBy,CDM.UpdatedBy=DM.UpdatedBy,CDM.IsLocationRequired=DM.IsLocationRequired from DeviceMaster as DM
		INNER JOIN ['+@ChildDbLocation+'].[dbo].[DeviceMaster] as CDM on DM.AutoId=CDM.AutoId
		Where DM.Autoid='+Convert(varchar(20),@AutoId)+'
		'
		EXEC sp_executesql @sql

	END         
 END    
 ELSE IF @Opcode=31        
 BEGIN        
	SELECT ROW_NUMBER() OVER(order by AutoId desc) AS RowNumber, * INTO #Results from                      
	(  
	SELECT AutoId,DeviceName,DeviceId,IsLocationRequired,(case when Status=1 then 'Active' ELSE  'Inactive' END)
	as Status,AttachedEmp=STUFF  
	(  
	(  
	SELECT DISTINCT ', ' + CAST(FirstNAme AS VARCHAR(MAX))  
	FROM AssignDeviceMaster as t2   
	inner join EmployeeMaster as em on em.AutoId=t2.EmpAutoId
	WHERE t2.DeviceAutoId =t1.AutoId
	FOR XML PATH('')  
	),1,1,'' 
	) FROM DeviceMaster as t1           
	WHERE  (@DeviceName is null or @DeviceName='' OR DeviceName like '%'+@DeviceName+'%')   
	and (@DeviceId is null or @DeviceId='' OR DeviceId like '%'+@DeviceId+'%')   
	and (@Status=2 or Status=@Status)   
	) as t3 order by AutoId desc

	SELECT COUNT(RowNumber) AS RecordCount,case when @PageSize=0 then COUNT(AutoId) 
	else @PageSize end AS PageSize,@PageIndex as PageIndex FROM #Results           
                                    
	SELECT * FROM #Results                                    
	WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))              
 END     
 ELSE IF @OpCode=41        
 BEGIN        
      SELECT AutoId,DeviceName,DeviceId,Status,IsLocationRequired FROM DeviceMaster WHERE AutoId=@AutoId        
 END        
 ELSE IF @Opcode=42        
 BEGIN        
  BEGIN try        
          DELETE FROM DeviceMaster WHERE AutoId =@AutoId   
		  
		  SET @sql='
		  DELETE FROM ['+@ChildDbLocation+'].[dbo].DeviceMaster WHERE AutoId ='+Convert(varchar(20),@AutoId)+''
		  EXEC sp_executesql @sql
  END try        
  BEGIN CATCH                          
   SET @isException=1                          
   SET @exceptionMessage='Device ID has been used in application.'                            
  END CATCH          
 END         
END TRY                          
BEGIN CATCH                          
   SET @isException=1                          
   SET @exceptionMessage=ERROR_MESSAGE()--'Oops! Something went wrong.Please try later.'                         
END CATCH                          
END 
