USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Delivered_MLTaxItem]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION  [dbo].[FN_Delivered_MLTaxItem]
(
	@AutoId int,
	@orderAutoId int
)
RETURNS decimal(10,2)
AS
BEGIN
	DECLARE @MLTax decimal(10,2)=0.00	
	declare @taxPer decimal(10,2)=isnull((select MLTaxPer from OrderMaster where AutoId=@orderAutoId),0.00)	 
	SET @MLTax=(SELECT TotalMLQty*@taxPer FROM Delivered_Order_Items WHERE AutoId=@AutoId)	 
	RETURN @MLTax
END
GO
