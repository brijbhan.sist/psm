alter PROCEDURE [dbo].[Proc_App_OrderMaster]                                      
@Opcode INT=NULL,                                      
@OrderAutoId INT=NULL,                                       
@CustomerAutoId  int=null,                                                                          
@LoginEmpType int =null,      
@OrderNo VARCHAR(15)=NULL,                                       
@OrderDate DATETIME=NULL,                                                                                                                                                                                                                              
@EmpAutoId INT=NULL,                                                                          
@SalesPersonAutoId INT=NULL,   
@SalesPerson varchar(max)=null,                                
@Todate DATETIME=NULL,                                                                        
@Fromdate DATETIME=NULL,  
@PageIndex INT = 1,                                      
@PageSize INT = 10,                                      
@RecordCount INT =null,                                       
@isException bit out,                                      
@exceptionMessage varchar(max) out,                                                                              
@TypeShipping VARCHAR(max)=NULL,
@customerType varchar(50)= NULL 
AS                                      
BEGIN                                       
  BEGIN TRY                                      
  Set @isException=0                                      
  Set @exceptionMessage='Success'                                       
  IF @Opcode=41                                      
   BEGIN 
    execute [dbo].[WaitForDelay]

    SELECT ROW_NUMBER() OVER(ORDER BY  OrderNo DESC                                      
    ) AS RowNumber, * INTO #Results from                                      
    (                                         
    SELECT OM.[AutoId],OM.OrderNo,CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,CM.AutoId as CustomerAutoId,CM.[CustomerName] AS CustomerName,                           
	OM.[GrandTotal],(emp.FirstName + ' '+ emp.LastName) as SalesPerson,                                     
    (SELECT COUNT(distinct [ProductAutoId]) from [dbo].[OrderItemMaster] WHERE OrderAutoId=OM.AutoId) AS NoOfItems ,                                      
                                           
    (select COUNT(1) from CreditMemoMaster as cmm where (Status=1 or Status=3)  and OrderAutoId is null                                      
    and cmm.CustomerAutoId = OM.CustomerAutoId ) as CreditMemo ,                  
    (Select ShippingType from ShippingType Where AutoId=om.ShippingType) as ShippingType,OM.ShippingType  as ShipId                                       
    FROM [dbo].[App_OrderMaster] As OM                                      
    INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.AutoId = OM.CustomerAutoId                                       
    INNER JOIN [dbo].[EmployeeMaster] AS emp ON emp.AutoId = OM.SalesPersonAutoId                                                                                     
    WHERE (@OrderNo is null or @OrderNo ='' or OrderNo like '%' + @OrderNo + '%')                                      
    and (@CustomerAutoId is null or @CustomerAutoId =0 or CustomerAutoId =@CustomerAutoId)                                      
    and (@FromDate is null or @FromDate = '' or @Todate is null or @Todate = '' or                                       
    (CONVERT(date,OM.[OrderDate]) between CONVERT(date,@FromDate) and CONVERT(date,@Todate)))                                                  
	 and (ISNULL(@SalesPerson,'0')='0' or (@SalesPerson = '0,') or OM.SalesPersonAutoId in (select * from dbo.fnSplitString(@SalesPerson,',')))                    
    and (ISNULL(@TypeShipping,'0')='0' or (@TypeShipping = '0,') or OM.ShippingType in (select * from dbo.fnSplitString(@TypeShipping,',')))                      
    )as t order by [OrderNo] DESC                                    
     SELECT COUNT(1) AS RecordCount, case when @PageSize=0 then COUNT(1) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Results                                               
    SELECT * FROM #Results                                      
    WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))                                      
   END                        
  
  ELSE IF @Opcode=43                                      
  BEGIN                                                               
  SELECT [AutoId],[CustomerName] As Customer FROM [dbo].[CustomerMaster] ORDER BY replace(CustomerName,' ','') ASC                                     
  SELECT AutoId,(FirstName +' '+ISNULL(LastName,'')) AS EmpName FROM EmployeeMaster WHERE EmpType =2 ORDER BY EmpName                                      
  SELECT AutoId,(FirstName +' '+ISNULL(LastName,'')) AS EmpName FROM EmployeeMaster WHERE EmpType =5    order by EmpName                 
                  
  SELECT AutoId,ShippingType FROM ShippingType  order by   ShippingType             
                           
  END                                          
  END TRY                                      
  BEGIN CATCH                                      
    Set @isException=1                                      
    Set @exceptionMessage='Opps Some thing went wrong.Please try later'--ERROR_MESSAGE()                                      
  END CATCH                         
END   