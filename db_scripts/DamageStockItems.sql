USE [psmnj.a1whm.com]
GO

/****** Object:  Table [dbo].[DamageStockItems]    Script Date: 06/08/2020 19:25:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DamageStockItems](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[DamageStockAutoId] [int] NULL,
	[ProductAutoId] [int] NULL,
	[UnitTypeAutoId] [int] NULL,
	[QtyPerUnit] [int] NULL,
	[UnitPrice] [decimal](18, 2) NULL,
	[RequiredQty] [int] NULL,
	[QtyShip] [int] NULL,
	[RemainQty] [int] NULL,
	[NetPrice] [decimal](18, 2) NULL,
	[OM_CostPrice] [decimal](18, 2) NULL,
	[OM_MinPrice] [decimal](18, 2) NULL,
 CONSTRAINT [PK_DamageStockItems] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


