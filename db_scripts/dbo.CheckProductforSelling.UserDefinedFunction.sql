ALTER FUNCTION  [dbo].[CheckProductforSelling]  
(  
  @productId VARCHAR(12)=NULL,  
  @UnitAutoid int=null  
)  
RETURNS int  
AS  
BEGIN  
   DECLARE @Result int=0 
   DECLARE @ProductAutoId int
   IF DB_NAME() IN ('psm.a1whm.com','demo.a1whm.com')
   BEGIN
		SET @ProductAutoId=(SELECT Autoid FROM [dbo].[ProductMaster] where ProductId=@ProductId) 
       IF  EXISTS(SELECT AutoId FROM [dbo].[OrderItemMaster] WHERE ProductAutoId=@ProductAutoId and UnitTypeAutoId=@UnitAutoid)                            
	   BEGIN                            
			SET @Result =1  
	   END 
   END
   ELSE
   BEGIN        
       SET @ProductAutoId=(SELECT Autoid FROM [psmct.a1whm.com].[dbo].[ProductMaster] where ProductId=@ProductId)     
	   IF  EXISTS(SELECT AutoId FROM [psmct.a1whm.com].[dbo].[OrderItemMaster] WHERE ProductAutoId=@ProductAutoId and UnitTypeAutoId=@UnitAutoid)                            
	   BEGIN                            
			SET @Result =1  
	   END              
        
	   SET @ProductAutoId=(SELECT Autoid FROM [psmpa.a1whm.com].[dbo].[ProductMaster] where ProductId=@ProductId)                  
	   IF  EXISTS(SELECT AutoId FROM [psmpa.a1whm.com].[dbo].[OrderItemMaster] WHERE ProductAutoId=@ProductAutoId and UnitTypeAutoId=@UnitAutoid)                            
	   BEGIN    
			SET @Result =1                
	   END             
        
	   SET @ProductAutoId=(SELECT Autoid FROM [psmnpa.a1whm.com].[dbo].[ProductMaster] where ProductId=@ProductId)             
	   IF  EXISTS(SELECT AutoId FROM [psmnpa.a1whm.com].[dbo].[OrderItemMaster] WHERE ProductAutoId=@ProductAutoId and UnitTypeAutoId=@UnitAutoid)                            
	   BEGIN         
			SET @Result =1        
	   END     
  
  
	   SET @ProductAutoId=(SELECT Autoid FROM [psmnj.a1whm.com].[dbo].[ProductMaster] where ProductId=@ProductId)         
	   IF  EXISTS(SELECT AutoId FROM [psmnj.a1whm.com].[dbo].[OrderItemMaster] WHERE ProductAutoId=@ProductAutoId and UnitTypeAutoId=@UnitAutoid)                            
	   BEGIN   
	  SET @Result =1     
	   END    
        
	   SET @ProductAutoId=(SELECT Autoid FROM [psmwpa.a1whm.com].[dbo].[ProductMaster] where ProductId=@ProductId)         
	   IF  EXISTS(SELECT AutoId FROM [psmwpa.a1whm.com].[dbo].[OrderItemMaster] WHERE ProductAutoId=@ProductAutoId and UnitTypeAutoId=@UnitAutoid)                            
	   BEGIN   
	  SET @Result =1                    
   END   
  END
 RETURN @Result  
END