ALTER PROCEDURE [dbo].[ProcPrintOrderMaster]                                                                                                      
@Opcode INT=NULL,                                                                                                      
@OrderAutoId INT=NULL,                                                                                             
@LoginEmpType INT=NULL,                            
@CustomerAutoId INT=NULL,        
@CreditAutoId int=null,                                                                                           
@EmpAutoId INT=NULL,                                                                                    
@isException bit out,           
@BulkOrderAutoId varchar(max)=null,                                                                                                     
@exceptionMessage varchar(max) out     
AS                                                                                                      
BEGIN                                                                                                     
  BEGIN TRY                                                                                                      
    Set @isException=0                                                                                                      
    Set @exceptionMessage='Success'                        
 IF @Opcode=41                          
 BEGIN                       
  SELECT CompanyName,EmailAddress, [Address],[Website],[MobileNo],[FaxNo],TermsCondition, [dbo].[udf_StripHTML](TermsCondition)  as tc,               
   Logo,('http://'+DB_NAME()+'/img/logo/'+Logo) as LogoforEmail FROM CompanyDetails               
                                                                 
  SET @CustomerAutoId = (SELECT [CustomerAutoId] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId) 
  
  SELECT OM.[AutoId],OM.[OrderNo],CONVERT(VARCHAR(20),OM.[OrderDate], 101) AS OrderDate,                                                                                                      
  CONVERT(VARCHAR(20), OM.[DeliveryDate],101) + ' ' + convert(varchar(10), getdate(), 108)                          
  AS DeliveryDate,CM.[CustomerId],CM.[CustomerName],
  (case when Contact1=Contact2 then Contact1 when isnull(Contact2,'')!='' then CM.[Contact1] + '/ ' + CM.[Contact2] else Contact1 end) As Contact,
  CTy.CustomerType,CT.[TermsDesc],ST.[ShippingType], 
  [dbo].[ConvertFirstLetterinCapital](BA.[Address]+(case when isnull(BA.Address2,'')!='' then ', '+BA.Address2 else '' end)+', '+BA.City)+', '+UPper(S.StateCode)+' - '+BA.Zipcode As BillAddr, 
  S.StateCode AS State1,BA.[City] AS City1,BA.[Zipcode] As Zipcode1,
  [dbo].[ConvertFirstLetterinCapital](SA.[Address]+(case when isnull(SA.Address2,'')!='' then ', '+SA.Address2 else '' end)+', '+SA.City)+', '+S1.StateCode+' - '+SA.Zipcode As ShipAddr, 
 S1.StateCode AS State2,SA.[City] AS City2,SA.[Zipcode] As Zipcode2,
  OM.[TotalAmount],OM.[OverallDisc],OM.[OverallDiscAmt],                                                                                                      
  OM.[ShippingCharges],OM.[TotalTax],OM.[GrandTotal],EM.[FirstName] + ' ' + EM.[LastName] As SalesPerson,                                                                                                      
  EM1.[FirstName] + ' ' + EM1.[LastName] As PackerName,                                                                        
  isnull(DeductionAmount,0.00) as DeductionAmount,                                                                                                      
  isnull(CreditAmount,0.00)as CreditAmount,Round(isnull(PayableAmount,0.00),0) as PayableAmount                                                                                                    
  ,ISNULL((select t.CreditAmount from CustomerCreditMaster as t  where t.CustomerAutoId=om.CustomerAutoid),0.00) as CustomerCredit                                                                                                      
  ,OrderRemarks ,                                               
  ISNULL(MLQty,0) as MLQty,isnull(MLTax,0) as MLTax,OM.Weigth_OZTaxAmount as WeightTax,CM.ContactPersonName,
  ISNULL(AdjustmentAmt,0) as AdjustmentAmt,           
  CM.BusinessName,om.Status,CM.Email as ReceiverEmail,TaxValue  ,
   ISNULL((select top 1 PrintLabel from MLTaxMaster as mlt where mlt.TaxState=BA.State),'') as MLTaxPrintLabel, 
   TTM.PrintLabel AS TaxPrintLabel,(SELECT top 1 PrintLabel from [dbo].[Tax_Weigth_OZ]) AS WeightTaxPrintLabel
   
    FROM [dbo].[OrderMaster] As OM   --  WeightTax added by Rizwan Ahmad on 17-09-2019                                             
  left JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.Status AND SM.[Category] = 'OrderMaster'                                                                                 
 INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[AutoId] = OM.[BillAddrAutoId]                                                                                                      
  INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = OM.[ShipAddrAutoId]                                                   
  INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                                                                                                       
  LEFT JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                                                                                           
  INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                                                                                       
  INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]                                                                                                      
  LEFT JOIN [dbo].[CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                                                                                    
  LEFT JOIN [dbo].[ShippingType] AS ST ON ST.[AutoId] = OM.[ShippingType]                                                                                                       
  left JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[SalesPersonAutoId]                                                                                                             
  left JOIN [dbo].[EmployeeMaster] AS EM1 ON EM1.[AutoId] = OM.[PackerAutoId]    
  LEFT JOIN [dbo].[TaxTypeMaster] AS TTM ON TTM.[AutoId] = OM.[TaxType]  
  WHERE OM.AutoId = @OrderAutoId                                      
                                                                                                       
  IF EXISTS(SELECT [AutoId] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId AND Status=11)                                                                                                      
  BEGIN                                                                                 
   SELECT CM.[CategoryName],CM.[CategoryId],DOI.[ProductAutoId],PM.[ProductId],PM.[ProductName],DOI.[UnitAutoId],UM.[UnitType],DOI.[QtyPerUnit],                                                                                                      
   DOI.[UnitPrice],DOI.[Barcode],DOI.[QtyShip],DOI.[QtyDel],DOI.[TotalPieces],DOI.[SRP],DOI.[GP], 
   case when DOI.GP <= 0 then '---' else convert(varchar(15),DOI.[GP]) end as PrintGP  
   ,DOI.[Tax],DOI.[NetPrice]                                                                                                      
   ,convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice,ISNULL(FreshReturnQty,0) as FreshReturnQty,FreshReturnUnitAutoId, 
   ISNULL(DamageReturnQty,0) as DamageReturnQty,DamageReturnUnitAutoId,                                                                                                      
   IsExchange,isnull(TaxValue,0.00) as TaxValue,ISNULL(isFreeItem,0) AS isFreeItem,                                                                                                      
   ISNULL(UnitMLQty,0) as UnitMLQty,ISNULL(TotalMLQty,0) as TotalMLQty,                                                                                                      
   DOI.Item_TotalMLTax as TotalMLTax ,ISNULL(DOI.Del_discount,0) as Del_discount ,isnull(UnitPrice+IsProductTaxPerUnit,0) as IsProductTaxPerUnit,IsProductTaxPerUnit   as IsProductTax                            
   FROM [dbo].[Delivered_Order_Items] AS DOI                                                 
   INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = DOI.[ProductAutoId]                                                         
   INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = DOI.UnitAutoId                                                             
   INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId]                             
   INNER JOIN [dbo].[PackingDetails] AS PD ON PD.[ProductAutoId] = DOI.[ProductAutoId] AND PD.[UnitType] = DOI.UnitAutoId                                    
   WHERE [OrderAutoId]=@OrderAutoId                   
   ORDER BY CM.[CategoryName],pm.ProductName ASC                                                    
  END                                                                            
  ELSE                                                                                                      
  BEGIN                                                                         
		SELECT CM.[CategoryName],CM.[CategoryId],OIM.ProductAutoId,PM.[ProductId],PM.[ProductName],PM.[ProductLocation],                                                          
		OIM.[UnitTypeAutoId] AS UnitAutoId,UM.[UnitType],OIM.[QtyPerUnit],OIM.[UnitPrice],OIM.[TotalPieces],OIM.[SRP],OIM.[GP],  
		(case when OIM.GP <= 0 then '---' else convert(varchar(15),OIM.[GP]) end) as PrintGP  
		,OIM.[Tax],                                                                       
		OIM.[NetPrice],OIM.[Barcode], 
		OIM.[QtyShip]     
		,OIM.[RequiredQty] ,                                                       
		convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice,                                                                                                      
		OIM.IsExchange,isnull(TaxValue,0.00) as TaxValue,ISNULL(isFreeItem,0) AS isFreeItem,UnitMLQty,                                                                                                      
		ISNULL(UnitMLQty,0) as UnitMLQty,ISNULL(TotalMLQty,0) as TotalMLQty,                                                                                                      
		oim.Item_TotalMLTax as TotalMLTax,isnull(Oim_Discount,0) as  Del_discount,isnull(UnitPrice+IsProductTaxPerUnit,0) as IsProductTaxPerUnit,IsProductTaxPerUnit  as IsProductTax                                                                                                                                                           
		FROM [dbo].[OrderItemMaster] AS OIM                                                                                                       
		INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                                                                                                      
		INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitTypeAutoId                                 
		INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId]                             
		INNER JOIN [dbo].[PackingDetails] AS PD ON PD.[ProductAutoId] = OIM.[ProductAutoId] AND PD.[UnitType] = OIM.UnitTypeAutoId                                                            
		WHERE [OrderAutoId] = @OrderAutoId                                                                       
		ORDER BY CM.[CategoryName],pm.ProductName ASC                                                           
                                                
  END                          
		SELECT OM.[AutoId],[OrderNo],CONVERT(VARCHAR(20),[OrderDate],101) AS OrderDate,DO.[AmtDue],DO.[GrandTotal],DO.[AmtPaid] ,                       
		otm.OrderType AS OrderType ,                                                                  
		DATEDIFF(dd,OrderDate,GETDATE()) as Aging, orderdate as orderdateSort                                                                               
		FROM [dbo].[OrderMaster] As OM                                                                                                      
		INNER JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId]    
		INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
		WHERE [CustomerAutoId] = @CustomerAutoId AND ([Status] = 11 ) AND isnull(do.[AmtDue],0) > 0.00                                                                                                      
		order by orderdateSort  
		
		SELECT @LoginEmpType AS LoginEmpType                           
                     
		select OrderAutoId,CreditAutoId,CreditNo,CONVERT(VARCHAR(10),CreditDate,101) AS CreditDate,ISNULL(TotalAmount,0.00) as ReturnValue                    
		,ISNULL(PaidAmount,0.00)  as amtDeducted,                                                                                                    
		ISNULL(BalanceAmount,0.00) as amtDue                                  
		from CreditMemoMaster AS CM  where OrderAutoId=@OrderAutoId  


		select CONVERT(VARCHAR(20), spm.ReceivedDate,101) as ReceivedDate,spm.ChequeNo,spm.ReceivedAmount,CONVERT(VARCHAR(20), spm.ChequeDate,101) as DepositeDate 
		from   SalesPaymentMaster spm  			
		where spm.PaymentMode=2 and spm.Status=4 AND spm.CustomerAutoId=@CustomerAutoId

		if(select Status from OrderMaster where AutoId=@OrderAutoId)=11
		begin
			select isnull(sum(om.IsDel_ProductTax),0) as P_Tax from DeliveredOrders as oim
			inner join Delivered_Order_Items as om on om.AutoId=oim.OrderAutoId
			where oim.OrderAutoId=@OrderAutoId
		end
		else
		begin
		    select isnull(sum(IsProductTax),0) as P_Tax from OrderItemMaster as oim
		inner join OrderMaster as om on om.AutoId=oim.OrderAutoId
		where oim.OrderAutoId=@OrderAutoId and om.Status!=11
		end

  END                           
 ELSE IF @Opcode=42                          
 BEGIN                          
                          
   SELECT CompanyName,EmailAddress, [Address],[Website],[MobileNo],[FaxNo],TermsCondition,Logo FROM CompanyDetails            
                            
   SET @CustomerAutoId = (SELECT [CustomerAutoId] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId)               
                         
   SELECT OM.[AutoId],OM.[OrderNo],CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,                                                                                                      
   ISNULL(CONVERT(VARCHAR(20), OM.[DeliveryDate],101),CONVERT(VARCHAR(20), OM.[DelDate],101) + ' ' +convert(varchar(10), OM.[DelDate], 108)) AS DeliveryDate,
   CM.[AutoId] As CustAutoId,CM.[CustomerId],CM.[CustomerName],
   (case when Contact1=Contact2 then Contact1 when isnull(Contact2,'')!='' then CM.[Contact1] + '/ ' + CM.[Contact2] else Contact1 end) As Contact,CTy.CustomerType,CT.[TermsDesc],ST.[ShippingType], 
   [dbo].[ConvertFirstLetterinCapital](BA.[Address]+(case when isnull(BA.Address2,'')!='' then ', '+BA.Address2 else '' end)) As BillAddr,                                                                                                      
   UPPER(S.[StateCode]) AS State1,ba.State as [stateid],
   [dbo].[ConvertFirstLetterinCapital](BA.[City]) AS City1,BA.[Zipcode] As Zipcode1,
   [dbo].[ConvertFirstLetterinCapital](SA.[Address]+(case when isnull(SA.Address2,'')!='' then ', '+SA.Address2 else '' end)) As ShipAddr,
   UPPER(S1.[StateCode]) As State2,
   [dbo].[ConvertFirstLetterinCapital](SA.[City]) AS City2, 
   SA.[Zipcode] As Zipcode2,OM.[Status] As StatusCode,SM.[StatusType] AS Status,OM.[PackedBoxes],ISNULL(DO.[TotalAmount],om.[TotalAmount]) as [TotalAmount],
   ISNULL(DO.[OverallDisc],om.[OverallDisc]) as [OverallDisc],ISNULL(DO.[OverallDiscAmt],om.[OverallDiscAmt]) as [OverallDiscAmt],                                                                                                      
   ISNULL(DO.[ShippingCharges],om.[ShippingCharges]) as [ShippingCharges],ISNULL(DO.[TotalTax],om.[TotalTax]) as [TotalTax],
   ISNULL(DO.[GrandTotal],om.[GrandTotal]) as [GrandTotal],ISNULL(DO.[AmtPaid],0) as [AmtPaid],ISNULL(DO.[AmtDue],0) as [AmtDue],
   OM.[PaymentRecev],OM.[RecevAmt],OM.[AmtValue],OM.[ValueChanged],                                                                    
   OM.[DiffAmt],OM.[NewTotal],OM.[PayThru],OM.CheckNo,OM.[DrvRemarks],OM.[AcctRemarks],EM.[FirstName] + ' ' + EM.[LastName] As SalesPerson,                                                                                                      
   EM1.[FirstName] + ' ' + EM1.[LastName] As PackerName,[CommentType],[Comment],isnull(do.CreditMemoAmount,om.Deductionamount) as DeductionAmount,                                                                                                      
   ISNULL(DO.CreditAmount,om.CreditAmount)as CreditAmount,isnull(do.PayableAmount,OM.PayableAmount) as PayableAmount,                             
   ISNULL((select t.CreditAmount from CustomerCreditMaster as t  where t.CustomerAutoId=om.CustomerAutoid),0.00) as CustomerCredit                                   
   ,om.TaxType,otm.OrderType AS OrderType,OrderRemarks,                                                                                                      
   ISNULL(OM.MLQty,0) as MLQty,isnull(DO.MLTax,om.MLTax) as MLTax,CM.ContactPersonName,ISNULL(om.AdjustmentAmt,0) as AdjustmentAmt,                                                                 
   CM.BusinessName,TTM.PrintLabel AS TaxPrintLabel,(SELECT top 1 PrintLabel from [dbo].[Tax_Weigth_OZ]) AS WeightTaxPrintLabel,
   ISNULL((select top 1 PrintLabel from MLTaxMaster as mlt where mlt.TaxState=BA.State),'') as MLTaxPrintLabel  ,
   ISNULL(UnitMLTax,0) as UnitMLTax,Weigth_OZTaxAmount as WeightTax,TaxValue  FROM [dbo].[OrderMaster] As OM             
   INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.Status AND SM.[Category] = 'OrderMaster'                                                                                                
   INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[AutoId] = OM.[BillAddrAutoId]                                                                             
   INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = OM.[ShipAddrAutoId]                                                                                                
   INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                                                 
   left JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                                                                                                       
   INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                                                                                                 
   INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]                                                                       
   LEFT JOIN [dbo].[CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                                                                      
   LEFT JOIN [dbo].[ShippingType] AS ST ON ST.[AutoId] = OM.[ShippingType]                                                                                             
   LEFT JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[SalesPersonAutoId]                                                                                                      
   LEFT JOIN [dbo].[EmployeeMaster] AS EM1 ON EM1.[AutoId] = OM.[PackerAutoId]   
   LEFT JOIN [dbo].[TaxTypeMaster] AS TTM ON TTM.[AutoId] = OM.[TaxType]  
   LEFT JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId]
   INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
   WHERE OM.AutoId = @OrderAutoId                           
                                                                                                         
   IF EXISTS(SELECT [AutoId] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId AND Status=11)                                                                                                      
   BEGIN                                                                     
                                                                                                      
    SELECT CM.[CategoryName],CM.[CategoryId],DOI.[ProductAutoId],PM.[ProductId],PM.[ProductName],DOI.[UnitAutoId],UM.[UnitType],DOI.[QtyPerUnit],                                                                                                      
    DOI.[UnitPrice],DOI.[Barcode],DOI.[QtyShip],DOI.[QtyDel],DOI.[TotalPieces],DOI.[SRP],DOI.[GP],  
 case when DOI.[GP] <= 0 then '---' else Convert(varchar(15),DOI.[GP]) end as PrintGP  
 ,DOI.[Tax],DOI.[NetPrice]                                                                                                      
    ,convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice,ISNULL(FreshReturnQty,0) as FreshReturnQty,FreshReturnUnitAutoId,                                  
    ISNULL(DamageReturnQty,0) as DamageReturnQty,DamageReturnUnitAutoId,                                                                                                      
    IsExchange,isnull(TaxValue,0.00) as TaxValue,ISNULL(isFreeItem,0) AS isFreeItem,                                    
    ISNULL(UnitMLQty,0) as UnitMLQty,ISNULL(TotalMLQty,0) as TotalMLQty,                                                                                                     
    DOI.Item_TotalMLTax as TotalMLTax                                                                      
    FROM [dbo].[Delivered_Order_Items] AS DOI                                                                                                       
    INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = DOI.[ProductAutoId]                                        
    INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = DOI.UnitAutoId                                                  
    INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId] WHERE [OrderAutoId]=@OrderAutoId                                                                                                   
    AND ISNULL(QtyShip,0)>0                       
    ORDER BY CM.CategoryName,pm.ProductName ASC --11/21/2019 By Rizwan Ahmad                                                                                 
   END           
   ELSE                                                                      
   BEGIN                                                    
                              
    SELECT CM.[CategoryName],CM.[CategoryId],OIM.ProductAutoId,PM.[ProductId],PM.[ProductName],PM.[ProductLocation],                                                                                                      
    OIM.[UnitTypeAutoId] AS UnitAutoId,UM.[UnitType],OIM.[QtyPerUnit],OIM.[UnitPrice],OIM.[TotalPieces],OIM.[SRP],OIM.[GP],  
 case when OIM.[GP] <= 0 then '---' else convert(varchar(15),OIM.[GP]) end as PrintGP  
 ,OIM.[Tax],                                                     
    OIM.[NetPrice],OIM.[Barcode],OIM.[QtyShip],OIM.[RequiredQty] ,convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice,                          
    OIM.IsExchange,isnull(TaxValue,0.00) as TaxValue,ISNULL(isFreeItem,0) AS isFreeItem,UnitMLQty,                                                                                                      
    ISNULL(UnitMLQty,0) as UnitMLQty,ISNULL(TotalMLQty,0) as TotalMLQty,                                                          
    oim.Item_TotalMLTax as TotalMLTax                                                                             
    FROM [dbo].[OrderItemMaster] AS OIM                                                                               
    INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                                                            
    INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitTypeAutoId                                                                                                       
    INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId] WHERE [OrderAutoId] = @OrderAutoId                                                                                            
    AND ISNULL(QtyShip,0)>0                              
    ORDER BY CM.CategoryName,pm.ProductName ASC  --11/21/2019 By Rizwan Ahmad                                                                  
  END                            
  END         
   ELSE IF @Opcode=43  --New opcode added for packing slip category wise 11/21/2019 By Rizwan Ahmad                          
 BEGIN                          
                          
   SELECT CompanyName,EmailAddress, [Address],[Website],[MobileNo],[FaxNo],TermsCondition,Logo FROM CompanyDetails            
                            
   SET @CustomerAutoId = (SELECT [CustomerAutoId] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId)               
                         
   SELECT OM.[AutoId],OM.[OrderNo],CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,                                                                                                      
   ISNULL(CONVERT(VARCHAR(20), OM.[DeliveryDate],101),CONVERT(VARCHAR(20), OM.[DelDate],101) + ' ' +convert(varchar(10), OM.[DelDate],
   108)) AS DeliveryDate,CM.[AutoId] As CustAutoId,CM.[CustomerId],CM.[CustomerName],
   (case when Contact1=Contact2 then Contact1 when isnull(Contact2,'')!='' then CM.[Contact1] + '/ ' + CM.[Contact2] else Contact1 end) As Contact,
   CTy.CustomerType,CT.[TermsDesc],ST.[ShippingType],BA.[Address] As BillAddr,                                                                                                      
   S.[StateName] AS State1,ba.State as [stateid],BA.[City] AS City1,BA.[Zipcode] As Zipcode1,SA.[Address] As ShipAddr,
   S1.[StateName] As State2,SA.[City] AS City2, SA.[Zipcode] As Zipcode2,
   [dbo].[ConvertFirstLetterinCapital](BA.[Address]+(case when isnull(BA.Address2,'')!='' then ', '+BA.Address2 else '' end)+', '+BA.City)+', '+UPper(S.StateCode)+' - '+BA.Zipcode  As BillAddress,                                                                                                         
   [dbo].[ConvertFirstLetterinCapital](SA.[Address]+(case when isnull(SA.Address2,'')!='' then ', '+SA.Address2 else '' end)+', '+SA.City)+', '+Upper(S1.StateCode)+' - '+SA.Zipcode as ShipAddress,                                                                   
   OM.[Status] As StatusCode,SM.[StatusType] AS Status,OM.[PackedBoxes],DO.[TotalAmount],DO.[OverallDisc],DO.[OverallDiscAmt],                                                                                                      
   DO.[ShippingCharges],DO.[TotalTax],DO.[GrandTotal],DO.[AmtPaid],DO.[AmtDue],OM.[PaymentRecev],OM.[RecevAmt],OM.[AmtValue],OM.[ValueChanged],                                                                    
   OM.[DiffAmt],OM.[NewTotal],OM.[PayThru],OM.CheckNo,OM.[DrvRemarks],OM.[AcctRemarks],EM.[FirstName] + ' ' + EM.[LastName] As SalesPerson,                                                                                                      
   EM1.[FirstName] + ' ' + EM1.[LastName] As PackerName,[CommentType],[Comment],isnull(DeductionAmount,0.00) as DeductionAmount,                                                                                                      
   ISNULL(DO.CreditAmount,0.00)as CreditAmount,isnull(do.PayableAmount,0.00) as PayableAmount,                             
   ISNULL((select t.CreditAmount from CustomerCreditMaster as t  where t.CustomerAutoId=om.CustomerAutoid),0.00) as CustomerCredit                                                                                                 
   ,om.TaxType,otm.OrderType AS OrderType,OrderRemarks,                                                                                                      
   ISNULL(OM.MLQty,0) as MLQty,isnull(DO.MLTax,0) as MLTax,CM.ContactPersonName,ISNULL(om.AdjustmentAmt,0) as AdjustmentAmt,                                                                    
   CM.BusinessName,ISNULL(UnitMLTax,0) as UnitMLTax  FROM [dbo].[OrderMaster] As OM                                                                          
   INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.Status AND SM.[Category] = 'OrderMaster'                                                                                                
   INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[AutoId] = OM.[BillAddrAutoId]                                                                             
   INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = OM.[ShipAddrAutoId]                                                                                                
   INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                                                 
   left JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                                                                                                       
   INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                                                                                                 
   INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]                                                                       
   LEFT JOIN [dbo].[CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                                                                      
   LEFT JOIN [dbo].[ShippingType] AS ST ON ST.[AutoId] = OM.[ShippingType]                                                                                             
   LEFT JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[SalesPersonAutoId]                                                                                                      
   LEFT JOIN [dbo].[EmployeeMaster] AS EM1 ON EM1.[AutoId] = OM.[PackerAutoId]                                                                                    
   LEFT JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId]
    INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
   WHERE OM.AutoId = @OrderAutoId                           
                                                                                                         
   IF EXISTS(SELECT [AutoId] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId AND Status=11)                                                                                                      
   BEGIN                                                                  
                                                                                                      
    SELECT CM.[CategoryName],CM.[CategoryId],DOI.[ProductAutoId],PM.[ProductId],PM.[ProductName],DOI.[UnitAutoId],UM.[UnitType],DOI.[QtyPerUnit],                                                                                                      
   DOI.[UnitPrice],DOI.[Barcode],DOI.[QtyShip],DOI.[QtyDel],DOI.[TotalPieces],DOI.[SRP],DOI.[GP],  
   case when DOI.[GP] <= 0 then '---' else Convert(varchar(15),DOI.[GP]) end as PrintGP  
   ,DOI.[Tax],DOI.[NetPrice]                                                                                                      
    ,convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice,ISNULL(FreshReturnQty,0) as FreshReturnQty,FreshReturnUnitAutoId,                                  
    ISNULL(DamageReturnQty,0) as DamageReturnQty,DamageReturnUnitAutoId,                                                                                                      
    IsExchange,isnull(TaxValue,0.00) as TaxValue,ISNULL(isFreeItem,0) AS isFreeItem,                                                                                                      
    ISNULL(UnitMLQty,0) as UnitMLQty,ISNULL(TotalMLQty,0) as TotalMLQty,                                                                                                      
    DOI.Item_TotalMLTax as TotalMLTax                                                                      
    FROM [dbo].[Delivered_Order_Items] AS DOI                                                                                                       
    INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = DOI.[ProductAutoId]                                        
    INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = DOI.UnitAutoId                                                                                                       
    INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId] WHERE [OrderAutoId]=@OrderAutoId                                                                                                   
    AND ISNULL(QtyShip,0)>0                             
    ORDER BY CM.[CategoryName],PM.ProductName ASC --11/21/2019 By Rizwan Ahmad                                                                                 
   END           
   ELSE                                                                      
   BEGIN                                                    
                              
    SELECT CM.[CategoryName],CM.[CategoryId],OIM.ProductAutoId,PM.[ProductId],PM.[ProductName],PM.[ProductLocation],                                                                                                      
    OIM.[UnitTypeAutoId] AS UnitAutoId,UM.[UnitType],OIM.[QtyPerUnit],OIM.[UnitPrice],OIM.[TotalPieces],OIM.[SRP],OIM.[GP],  
 case when OIM.[GP] <= 0 then '---' else Convert(varchar(15),OIM.[GP]) end as PrintGP  
 ,OIM.[Tax],          
    OIM.[NetPrice],OIM.[Barcode],OIM.[QtyShip],OIM.[RequiredQty] ,convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice,                          
    OIM.IsExchange,isnull(TaxValue,0.00) as TaxValue,ISNULL(isFreeItem,0) AS isFreeItem,UnitMLQty,                                                                                                      
    ISNULL(UnitMLQty,0) as UnitMLQty,ISNULL(TotalMLQty,0) as TotalMLQty,                                                          
    oim.Item_TotalMLTax as TotalMLTax                                                                             
    FROM [dbo].[OrderItemMaster] AS OIM                                                                               
    INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                                                            
    INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitTypeAutoId                                                                                                       
    INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId] WHERE [OrderAutoId] = @OrderAutoId                                                                       
    AND ISNULL(QtyShip,0)>0                              
    ORDER BY CM.[CategoryName],PM.ProductName ASC  --11/21/2019 By Rizwan Ahmad                                                                  
  END                            
  END          
  IF @Opcode=46                       
 BEGIN           
    SELECT OM.[AutoId],OM.[OrderNo] from OrderMaster as OM where AutoId in (SELECT * FROM [dbo].[fnSplitString](@BulkOrderAutoId, ','))        
 end        
  IF @Opcode=45                        
 BEGIN        
   DECLARE @Location varchar(50)=Replace(DB_Name(),'.a1whm.com','')
	select(select CompanyName,EmailAddress, [Address],[Website],[MobileNo],[FaxNo],TermsCondition,              
	Logo,('http://'+DB_NAME()+'/img/logo/'+Logo) as LogoforEmail,        
	(SELECT OM.[AutoId],OM.[OrderNo],CONVERT(VARCHAR(20),OM.[OrderDate], 101) AS OrderDate,                                                                                                    
	CONVERT(VARCHAR(20), OM.[DeliveryDate],101) + ' ' + convert(varchar(10), getdate(), 108)                        
	AS DeliveryDate,                                  
	CM.[CustomerId],CM.[CustomerName],
	(case when Contact1=Contact2 then Contact1 when isnull(Contact2,'')!='' then CM.[Contact1] + '/ ' + CM.[Contact2] else Contact1 end)
	As Contact,CTy.CustomerType,CT.[TermsDesc],ST.[ShippingType],                                                                                                    
	BA.[Address]+' '+BA.City+' '+S.StateName+' '+BA.Zipcode As BillAddr,S.[StateName] AS State1,BA.[City] AS City1,BA.[Zipcode] As Zipcode1,
	SA.[Address]+' '+SA.City+' '+s1.StateName+' '+SA.Zipcode As ShipAddr,S1.[StateName] As State2,SA.[City] AS City2,SA.[Zipcode] As Zipcode2,
	[dbo].[ConvertFirstLetterinCapital](BA.[Address]+(case when isnull(BA.Address2,'')!='' then ', '+BA.Address2 else '' end)+', '+BA.City)+', '+UPper(S.StateCode)+' - '+BA.Zipcode  As BillAddress,                                                                                                         
   [dbo].[ConvertFirstLetterinCapital](SA.[Address]+(case when isnull(SA.Address2,'')!='' then ', '+SA.Address2 else '' end)+', '+SA.City)+', '+Upper(S1.StateCode)+' - '+SA.Zipcode as ShipAddress,                                                                   
  OM.[TotalAmount],OM.[OverallDisc],OM.[OverallDiscAmt],                                                                                                    
	OM.[ShippingCharges],OM.[TotalTax],OM.[GrandTotal],EM.[FirstName] + ' ' + EM.[LastName] As SalesPerson,                                                                                                    
	EM1.[FirstName] + ' ' + EM1.[LastName] As PackerName,                                                                      
	isnull(DeductionAmount,0.00) as DeductionAmount,                                                                                                    
	isnull(CreditAmount,0.00)as CreditAmount,isnull(PayableAmount,0.00) as PayableAmount                                                                                                  
	,ISNULL((select t.CreditAmount from CustomerCreditMaster as t  where t.CustomerAutoId=om.CustomerAutoid),0.00) as CustomerCredit                                                                                                    
	,OrderRemarks ,                                                                                                   
	ISNULL(MLQty,0) as MLQty,isnull(MLTax,0) as MLTax,OM.Weigth_OZTaxAmount as WeightTax,CM.ContactPersonName,ISNULL(AdjustmentAmt,0) as AdjustmentAmt,                                                                                                    
	-- CM.BusinessName,om.Status,CM.Email as ReceiverEmail,        
	isnull((
		SELECT CM.[CategoryName],CM.[CategoryId],DOI.[ProductAutoId],PM.[ProductId],PM.[ProductName],DOI.[UnitAutoId],UM.[UnitType],DOI.[QtyPerUnit],                                                                                                    
		DOI.[UnitPrice],DOI.[Barcode],DOI.[QtyShip],DOI.[QtyDel],DOI.[TotalPieces],DOI.[SRP],
		case when DOI.[GP] < 0 then '---' else Convert(varchar(15),DOI.[GP]) end as  [GP],  
		case when DOI.[GP] < 0 then '---' else convert(varchar(15),DOI.[GP]) end as PrintGP  
		,DOI.[Tax],DOI.[NetPrice]                                                                                                    
		,convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice,ISNULL(FreshReturnQty,0) as FreshReturnQty,FreshReturnUnitAutoId,                         
		ISNULL(DamageReturnQty,0) as DamageReturnQty,DamageReturnUnitAutoId,                                                                                                    
		IsExchange,isnull(TaxValue,0.00) as TaxValue,ISNULL(isFreeItem,0) AS isFreeItem,                                                                                                    
		ISNULL(UnitMLQty,0) as UnitMLQtys,ISNULL(TotalMLQty,0) as TotalMLQty,                                                                                                    
		DOI.Item_TotalMLTax as TotalMLTax                               
		FROM [dbo].[Delivered_Order_Items] AS DOI                                               
		INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = DOI.[ProductAutoId]                                                                                                    
		INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = DOI.UnitAutoId                                                                                                     
		INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId]                                                    
		INNER JOIN [dbo].[PackingDetails] AS PD ON PD.[ProductAutoId] = DOI.[ProductAutoId] AND PD.[UnitType] = DOI.UnitAutoId                                  
		WHERE [OrderAutoId] =OM.AutoId and OM.Status=11          
		ORDER BY CM.[CategoryName] asc,PM.ProductName ASC for json path, INCLUDE_NULL_VALUES        
	),'[]') as Item1,        
	isnull((
		SELECT CM.[CategoryName],CM.[CategoryId],OIM.ProductAutoId,PM.[ProductId],PM.[ProductName],PM.[ProductLocation],                                                        
		OIM.[UnitTypeAutoId] AS UnitAutoId,UM.[UnitType],OIM.[QtyPerUnit],OIM.[UnitPrice],OIM.[TotalPieces],OIM.[SRP],
		case when OIM.[GP] <= 0 then '---' else Convert(varchar(15),OIM.[GP]) end as [GP],OIM.[Tax],                                                                     
		OIM.[NetPrice],OIM.[Barcode],                        
                          
		isnull(OIM.[QtyShip],OIM.[RequiredQty]) as QtyShip                       
                          
		,OIM.[RequiredQty] ,                                                     
		convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice,                                                                                                    
		OIM.IsExchange,isnull(TaxValue,0.00) as TaxValue,ISNULL(isFreeItem,0) AS isFreeItem,UnitMLQty,                                                                                                    
		ISNULL(UnitMLQty,0) as UnitMLQtyf,ISNULL(TotalMLQty,0) as TotalMLQty,                                                                                                    
		oim.Item_TotalMLTax as TotalMLTax                                                                                                  
		FROM [dbo].[OrderItemMaster] AS OIM                                                                                                     
		INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                                                                                                    
		INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitTypeAutoId                               
		INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId]                           
		INNER JOIN [dbo].[PackingDetails] AS PD ON PD.[ProductAutoId] = OIM.[ProductAutoId] AND PD.[UnitType] = OIM.UnitTypeAutoId                                                          
		WHERE [OrderAutoId] =OM.AutoId and OM.Status!=11
		and ((@Location in ('psmpa','psmnpa','psmwpa') and oim.TotalPieces>0) or @Location in ('psmnj','psmct','psm','psmny'))
		ORDER BY CM.[CategoryName] asc,PM.ProductName ASC for json path, INCLUDE_NULL_VALUES        
	),'[]') as Item2,       
	 isnull((        
		SELECT OM.[AutoId],[OrderNo],CONVERT(VARCHAR(20),[OrderDate],101) AS OrderDate,DO.[AmtDue],DO.[GrandTotal],DO.[AmtPaid] ,                     
		otm.OrderType AS OrderType ,                                                                
		DATEDIFF(dd,OrderDate,GETDATE()) as Aging, orderdate as orderdateSort                                                                                                    
		FROM [dbo].[OrderMaster] As OM1                                                                                                  
		INNER JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM1.[AutoId]       
		 INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
		WHERE [CustomerAutoId] = OM.CustomerAutoId AND ([Status] = 11 ) AND isnull(do.[AmtDue],0) > 0.00                                                                                                    
		order by orderdateSort ASC for json path, INCLUDE_NULL_VALUES               
	 ),'[]') as DueOrderList,  
	   isnull((        
		  select CMM.[CreditNo] as OrderNo,CONVERT(VARCHAR(20), CMM.[CreditDate], 101) AS OrderDate,                                         
    CM.[AutoId] As CustAutoId,CM.[CustomerId],CM.[CustomerName],(case when Contact1=Contact2 then Contact1 when isnull(Contact2,'')!='' then CM.[Contact1] + '/ ' + CM.[Contact2] else Contact1 end) As Contact,
	CTy.CustomerType,CT.[TermsDesc],                                    
    BA.[Address] As BillAddr,                                    
    S.[StateName] AS State1,BA.[City] AS City1,BA.[Zipcode] As Zipcode1,SA.[Address] As ShipAddr,S1.[StateName] As State2,SA.[City] AS City2,                                    
    SA.[Zipcode] As Zipcode2,
	[dbo].[ConvertFirstLetterinCapital](BA.[Address]+(case when isnull(BA.Address2,'')!='' then ', '+BA.Address2 else '' end)+', '+BA.City)+', '+UPper(S.StateCode)+' - '+BA.Zipcode  As BillAddress,                                                                                                         
   [dbo].[ConvertFirstLetterinCapital](SA.[Address]+(case when isnull(SA.Address2,'')!='' then ', '+SA.Address2 else '' end)+', '+SA.City)+', '+Upper(S1.StateCode)+' - '+SA.Zipcode as ShipAddress,                                                                   
 OM.[TotalAmount],ISNULL(OM.[OverallDisc],0) as OverallDisc1,ISNULL(OM.[OverallDiscAmt],0) as OverallDisc,ISNULL(OM.[TotalTax],0) as TotalTax,
	isnull(OM.[GrandTotal],OM.[TotalAmount]) as GrandTotal,                              
  ISNULL((select top 1 PrintLabel from MLTaxMaster where TaxState=BA.State),'') as MLTaxPrintLabel,
		ISNULL((select top 1 PrintLabel from Tax_Weigth_OZ),'') as WeightTaxPrintLabel,
		ISNULL((select top 1 PrintLabel from [TaxTypeMaster] as tt  where tt.State=BA.State),'') as TaxPrintLabel,
    ISNULL(MLQty,0) as MLQty,isnull(MLTax,0) as MLTax,CM.ContactPersonName,ISNULL(AdjustmentAmt,0) as AdjustmentAmt,                            
    ISNULL(CMM.WeightTaxAmount,0) as WeightTaxAmount,  --  TaxTypeAutoId Added By Rizwan Ahmad on 11/09/2019 04:00 AM                                  
    CM.BusinessName,EM.[FirstName] + ' ' + EM.[LastName] As SalesPerson
		 ,isnull((
		SELECT CM.[CategoryName],CM.[CategoryId],OIM.ProductAutoId,PM.[ProductId],PM.[ProductName],PM.[ProductLocation],                                  
		OIM.[UnitAutoId] AS UnitAutoId,UM.[UnitType],OIM.[QtyPerUnit],convert(decimal(10,2),OIM.[UnitPrice]) as UnitPrice,                                    
		convert(decimal(10,2),OIM.NetAmount) AS  [NetPrice],OIM.AcceptedQty AS QtyShip,'' AS BarCode                                    
		FROM [dbo].[CreditItemMaster] AS OIM                                     
		INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                                    
		INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitAutoId                                     
		INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId]    
		WHERE OIM.CreditAutoId=CMM.CreditAutoId                                  
		ORDER BY CM.[CategoryName] ASC for json path, INCLUDE_NULL_VALUES
    ),'[]') as CreditItems      
		  from CreditMemoMaster AS CMM  
			INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = CMM.[CustomerAutoId]                                               
			INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]                                    
			INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[CustomerAutoId] = OM.[CustomerAutoId] AND BA.IsDefault=1                                    
			INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[CustomerAutoId] = OM.[CustomerAutoId] AND SA.IsDefault=1                                    
			LEFT JOIN [dbo].[CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                                           
			INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                                     
			INNER JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                                     
			left JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = CMM.CreatedBy  where CMM.OrderAutoId =OM.AutoId 
		  
		  for json path, INCLUDE_NULL_VALUES        
	   ),'[]') as CreditDetails,
		ISNULL((select top 1 PrintLabel from MLTaxMaster where TaxState=BA.State),'') as MLTaxPrintLabel ,
		ISNULL((select top 1 PrintLabel from Tax_Weigth_OZ),'') as WeightTaxPrintLabel,
		ISNULL((select top 1 PrintLabel from [TaxTypeMaster] as tt  where tt.State=BA.State),'') as TaxPrintLabel
		 FROM [dbo].[OrderMaster] As OM   --  WeightTax added by Rizwan Ahmad on 17-09-2019                                           
		 INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.Status AND SM.[Category] = 'OrderMaster'                                                                               
		 INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[AutoId] = OM.[BillAddrAutoId]                                                                                                    
		 INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = OM.[ShipAddrAutoId]                                                 
		 INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                                                                                                     
		 LEFT JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                                                                                         
		 INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                                                                                     
		 INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]                                                                                                    
		 LEFT JOIN [dbo].[CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                                                                                
		 LEFT JOIN [dbo].[ShippingType] AS ST ON ST.[AutoId] = OM.[ShippingType]                                                                                                     
		 left JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[SalesPersonAutoId]                                                                                                           
		 left JOIN [dbo].[EmployeeMaster] AS EM1 ON EM1.[AutoId] = OM.[PackerAutoId]                         
		  WHERE OM.AutoId in (SELECT * FROM [dbo].[fnSplitString](@BulkOrderAutoId, ',')) order by OM.Stoppage asc for json path, INCLUDE_NULL_VALUES) as OrderDetails        
		 from CompanyDetails for json path, INCLUDE_NULL_VALUES) as CompanyD    
 --    DECLARE @Location varchar(50)=Replace(DB_Name(),'.a1whm.com','')
	--select(select CompanyName,EmailAddress, [Address],[Website],[MobileNo],[FaxNo],TermsCondition,              
	--Logo,('http://'+DB_NAME()+'/img/logo/'+Logo) as LogoforEmail,        
	--(SELECT OM.[AutoId],OM.[OrderNo],CONVERT(VARCHAR(20),OM.[OrderDate], 101) AS OrderDate,                                                                                                    
	--CONVERT(VARCHAR(20), OM.[DeliveryDate],101) + ' ' + convert(varchar(10), getdate(), 108)                        
	--AS DeliveryDate,                                  
	--CM.[CustomerId],CM.[CustomerName],CM.[Contact1] + '/' + CM.[Contact2] As Contact,CTy.CustomerType,CT.[TermsDesc],ST.[ShippingType],                                                                                                    
	--BA.[Address]+' '+BA.City+' '+S.StateName+' '+BA.Zipcode As BillAddr,S.[StateName] AS State1,BA.[City] AS City1,BA.[Zipcode] As Zipcode1,
	--SA.[Address]+' '+SA.City+' '+s1.StateName+' '+SA.Zipcode As ShipAddr,S1.[StateName] As State2,   
	--SA.[City] AS City2,SA.[Zipcode] As Zipcode2,OM.[TotalAmount],OM.[OverallDisc],OM.[OverallDiscAmt],                                                                                                    
	--OM.[ShippingCharges],OM.[TotalTax],OM.[GrandTotal],EM.[FirstName] + ' ' + EM.[LastName] As SalesPerson,                                                                                                    
	--EM1.[FirstName] + ' ' + EM1.[LastName] As PackerName,                                                                      
	--isnull(DeductionAmount,0.00) as DeductionAmount,                                                                                                    
	--isnull(CreditAmount,0.00)as CreditAmount,isnull(PayableAmount,0.00) as PayableAmount                                                                                                  
	--,ISNULL((select t.CreditAmount from CustomerCreditMaster as t  where t.CustomerAutoId=om.CustomerAutoid),0.00) as CustomerCredit                                                                                                    
	--,OrderRemarks ,                                                                                                   
	--ISNULL(MLQty,0) as MLQty,isnull(MLTax,0) as MLTax,OM.Weigth_OZTaxAmount as WeightTax,CM.ContactPersonName,ISNULL(AdjustmentAmt,0) as AdjustmentAmt,                                                                                                    
	---- CM.BusinessName,om.Status,CM.Email as ReceiverEmail,        
	--isnull((
	--	SELECT CM.[CategoryName],CM.[CategoryId],DOI.[ProductAutoId],PM.[ProductId],PM.[ProductName],DOI.[UnitAutoId],UM.[UnitType],DOI.[QtyPerUnit],                                                                                                    
	--	DOI.[UnitPrice],DOI.[Barcode],DOI.[QtyShip],DOI.[QtyDel],DOI.[TotalPieces],DOI.[SRP],
	--	case when DOI.[GP] < 0 then '---' else Convert(varchar(15),DOI.[GP]) end as  [GP],  
	--	case when DOI.[GP] < 0 then '---' else convert(varchar(15),DOI.[GP]) end as PrintGP  
	--	,DOI.[Tax],DOI.[NetPrice]                                                                                                    
	--	,convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice,ISNULL(FreshReturnQty,0) as FreshReturnQty,FreshReturnUnitAutoId,                         
	--	ISNULL(DamageReturnQty,0) as DamageReturnQty,DamageReturnUnitAutoId,                                                                                                    
	--	IsExchange,isnull(TaxValue,0.00) as TaxValue,ISNULL(isFreeItem,0) AS isFreeItem,                                                                                                    
	--	ISNULL(UnitMLQty,0) as UnitMLQtys,ISNULL(TotalMLQty,0) as TotalMLQty,                                                                                                    
	--	DOI.Item_TotalMLTax as TotalMLTax                               
	--	FROM [dbo].[Delivered_Order_Items] AS DOI                                               
	--	INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = DOI.[ProductAutoId]                                                                                                    
	--	INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = DOI.UnitAutoId                                                                                                     
	--	INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId]                                                    
	--	INNER JOIN [dbo].[PackingDetails] AS PD ON PD.[ProductAutoId] = DOI.[ProductAutoId] AND PD.[UnitType] = DOI.UnitAutoId                                  
	--	WHERE [OrderAutoId] =OM.AutoId and OM.Status=11          
	--	ORDER BY CM.[CategoryName] asc,PM.ProductName ASC for json path, INCLUDE_NULL_VALUES        
	--),'[]') as Item1,        
	--isnull((
	--	SELECT CM.[CategoryName],CM.[CategoryId],OIM.ProductAutoId,PM.[ProductId],PM.[ProductName],PM.[ProductLocation],                                                        
	--	OIM.[UnitTypeAutoId] AS UnitAutoId,UM.[UnitType],OIM.[QtyPerUnit],OIM.[UnitPrice],OIM.[TotalPieces],OIM.[SRP],
	--	case when OIM.[GP] <= 0 then '---' else Convert(varchar(15),OIM.[GP]) end as [GP],OIM.[Tax],                                                                     
	--	OIM.[NetPrice],OIM.[Barcode],                        
                          
	--	isnull(OIM.[QtyShip],OIM.[RequiredQty]) as QtyShip                       
                          
	--	,OIM.[RequiredQty] ,                                                     
	--	convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice,                                                                                                    
	--	OIM.IsExchange,isnull(TaxValue,0.00) as TaxValue,ISNULL(isFreeItem,0) AS isFreeItem,UnitMLQty,                                                                                                    
	--	ISNULL(UnitMLQty,0) as UnitMLQtyf,ISNULL(TotalMLQty,0) as TotalMLQty,                                                                                                    
	--	oim.Item_TotalMLTax as TotalMLTax                                                                                                  
	--	FROM [dbo].[OrderItemMaster] AS OIM                                                                                                     
	--	INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                                                                                                    
	--	INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitTypeAutoId                               
	--	INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId]                           
	--	INNER JOIN [dbo].[PackingDetails] AS PD ON PD.[ProductAutoId] = OIM.[ProductAutoId] AND PD.[UnitType] = OIM.UnitTypeAutoId                                                          
	--	WHERE [OrderAutoId] =OM.AutoId and OM.Status!=11
	--	and ((@Location in ('psmpa','psmnpa','psmwpa') and oim.TotalPieces>0) or @Location in ('psmnj','psmct','psm'))
	--	ORDER BY CM.[CategoryName],PM.ProductName ASC for json path, INCLUDE_NULL_VALUES        
	--),'[]') as Item2,       
	-- isnull((        
	--	SELECT OM.[AutoId],[OrderNo],CONVERT(VARCHAR(20),[OrderDate],101) AS OrderDate,DO.[AmtDue],DO.[GrandTotal],DO.[AmtPaid] ,                     
	--	otm.OrderType AS OrderType ,                                                                
	--	DATEDIFF(dd,OrderDate,GETDATE()) as Aging, orderdate as orderdateSort                                                                                                    
	--	FROM [dbo].[OrderMaster] As OM1                                                                                                  
	--	INNER JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM1.[AutoId]       
	--	 INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
	--	WHERE [CustomerAutoId] = OM.CustomerAutoId AND ([Status] = 11 ) AND isnull(do.[AmtDue],0) > 0.00                                                                                                    
	--	order by orderdateSort ASC for json path, INCLUDE_NULL_VALUES               
	-- ),'[]') as DueOrderList,  
	--   isnull((        
	--	  select OrderAutoId,CreditAutoId,CreditNo,CONVERT(VARCHAR(10),CreditDate,101) AS CreditDate,ISNULL(TotalAmount,0.00) as ReturnValue                  
	--	  ,ISNULL(PaidAmount,0.00)  as amtDeducted,                                                                                                  
	--	  ISNULL(BalanceAmount,0.00) as amtDue                             
	--	  from CreditMemoMaster AS CM  where OrderAutoId =OM.AutoId  for json path, INCLUDE_NULL_VALUES        
	--   ),'[]') as CreditDetails,
	--	ISNULL((select top 1 PrintLabel from MLTaxMaster where TaxState=BA.State),'') as MLTaxPrintLabel ,
	--	ISNULL((select top 1 PrintLabel from Tax_Weigth_OZ),'') as WeightTaxPrintLabel,
	--	ISNULL((select top 1 PrintLabel from [TaxTypeMaster] as tt  where tt.State=BA.State),'') as TaxPrintLabel
	--	 FROM [dbo].[OrderMaster] As OM   --  WeightTax added by Rizwan Ahmad on 17-09-2019                                           
	--	 INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.Status AND SM.[Category] = 'OrderMaster'                                                                               
	--	 INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[AutoId] = OM.[BillAddrAutoId]                                                                                                    
	--	 INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = OM.[ShipAddrAutoId]                                                 
	--	 INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                                                                                                     
	--	 LEFT JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                                                                                         
	--	 INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                                                                                     
	--	 INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]                                                                                                    
	--	 LEFT JOIN [dbo].[CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                                                                                
	--	 LEFT JOIN [dbo].[ShippingType] AS ST ON ST.[AutoId] = OM.[ShippingType]                                                                                                     
	--	 left JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[SalesPersonAutoId]                                                                                                           
	--	 left JOIN [dbo].[EmployeeMaster] AS EM1 ON EM1.[AutoId] = OM.[PackerAutoId]                         
	--	  WHERE OM.AutoId in (SELECT * FROM [dbo].[fnSplitString](@BulkOrderAutoId, ',')) order by OM.Stoppage asc for json path, INCLUDE_NULL_VALUES) as OrderDetails        
	--	 from CompanyDetails for json path, INCLUDE_NULL_VALUES) as CompanyD        
  END    
  -----------------------------------------------------------------------Multi Credit Memo-----------------------------
   IF @Opcode=522                          
 BEGIN 
    
		select (SELECT CompanyName,EmailAddress, [Address],[Website],[MobileNo],[FaxNo],TermsCondition, [dbo].[udf_StripHTML](TermsCondition)  as tc,               
		Logo,('http://'+DB_NAME()+'/img/logo/'+Logo) as LogoforEmail,
		(SELECT OM.[AutoId],OM.[OrderNo],CONVERT(VARCHAR(20),OM.[OrderDate], 101) AS OrderDate,                                                                                                      
		CONVERT(VARCHAR(20), OM.[DeliveryDate],101) + ' ' + convert(varchar(10), getdate(), 108) AS DeliveryDate,                                    
		CM.[CustomerId],CM.[CustomerName],
		(case when Contact1=Contact2 then Contact1 when isnull(Contact2,'')!='' then CM.[Contact1] + '/ ' + CM.[Contact2] else Contact1 end) As Contact,CTy.CustomerType,CT.[TermsDesc],ST.[ShippingType],
		[dbo].[ConvertFirstLetterinCapital](BA.[Address]+(case when isnull(BA.Address2,'')!='' then ', '+BA.Address2 else '' end)+', '+BA.City)+', '+UPper(S.StateCode)+' - '+BA.Zipcode  as BillingAddress,
		[dbo].[ConvertFirstLetterinCapital](SA.[Address]+(case when isnull(SA.Address2,'')!='' then ', '+SA.Address2 else '' end)+', '+SA.City)+', '+S1.StateCode+' - '+SA.Zipcode as ShippingAddress,
		BA.[Address] As BillAddr,S.[StateName] AS State1,BA.[City] AS City1,BA.[Zipcode] As Zipcode1,SA.[Address] As ShipAddr,S1.[StateName] As State2,                                                                                                      
		SA.[City] AS City2,SA.[Zipcode] As Zipcode2,convert(varchar(30),convert(decimal(18,2),OM.[TotalAmount])) as TotalAmount,convert(varchar(30),convert(decimal(18,2),OM.[OverallDisc])) as OverallDisc,convert(varchar(30),convert(decimal(18,2),OM.[OverallDiscAmt])) as OverallDiscAmt,                                                                                                      
		convert(varchar(30),convert(decimal(18,2),OM.[ShippingCharges])) as ShippingCharges,convert(varchar(30),convert(decimal(18,2),OM.[TotalTax])) as TotalTax,convert(varchar(30),convert(decimal(18,2),OM.[GrandTotal])) as GrandTotal,EM.[FirstName] + ' ' + EM.[LastName] As SalesPerson,                                                                                                      
		EM1.[FirstName] + ' ' + EM1.[LastName] As PackerName,                                                                        
		isnull(convert(varchar(30),convert(decimal(18,2),DeductionAmount)),0.00) as DeductionAmount,                                                                                                      
		isnull(convert(varchar(30),convert(decimal(18,2),CreditAmount)),0.00)as CreditAmount,isnull(convert(varchar(30),convert(decimal(18,2),PayableAmount)),0.00) as PayableAmount                                                                                                    
		,ISNULL((select t.CreditAmount from CustomerCreditMaster as t  where t.CustomerAutoId=om.CustomerAutoid),0.00) as CustomerCredit                                                                                                      
		,OrderRemarks ,                                               
		ISNULL(MLQty,0) as MLQty,isnull(MLTax,0) as MLTax,isnull(convert(varchar(30),convert(decimal(18,2),OM.Weigth_OZTaxAmount)),0.00) as WeightTax,CM.ContactPersonName,
		ISNULL(convert(varchar(30),convert(decimal(18,2),AdjustmentAmt)),0.00) as AdjustmentAmt,               
		CM.BusinessName,om.Status,CM.Email as ReceiverEmail, 
		isnull((
			SELECT CM.[CategoryName],CM.[CategoryId],DOI.[ProductAutoId],PM.[ProductId],PM.[ProductName],DOI.[UnitAutoId],UM.[UnitType],DOI.[QtyPerUnit],                                                                                                    
			convert(varchar(30),convert(decimal(18,2),DOI.[UnitPrice])) as UnitPrice,DOI.[Barcode],DOI.[QtyShip],DOI.[QtyDel],DOI.[TotalPieces],
			convert(varchar(30),convert(decimal(18,2),DOI.[SRP])) as SRP,
			case when DOI.[GP] <= 0 then '---' else Convert(varchar(15),DOI.[GP]) end as  [GP],  
			case when DOI.[GP] <= 0 then '---' else convert(varchar(15),DOI.[GP]) end as PrintGP  
			,DOI.[Tax],convert(varchar(30),convert(decimal(18,2),DOI.[NetPrice])) as NetPrice                                                                                                    
			, convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice,ISNULL(FreshReturnQty,0) as FreshReturnQty,FreshReturnUnitAutoId,                         
			ISNULL(DamageReturnQty,0) as DamageReturnQty,DamageReturnUnitAutoId,                                                                                                    
			ISNULL(IsExchange,0) as IsExchange,isnull(TaxValue,0.00) as TaxValue,ISNULL(isFreeItem,0) AS isFreeItem,                                                                                                    
			ISNULL(UnitMLQty,0) as UnitMLQtys,ISNULL(TotalMLQty,0) as TotalMLQty,                                                                                                    
			convert(decimal(18,2),DOI.Item_TotalMLTax) as TotalMLTax                               
			FROM [dbo].[Delivered_Order_Items] AS DOI                                               
			INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = DOI.[ProductAutoId]                                                                                                    
			INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = DOI.UnitAutoId                                                                                                     
			INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId]                                                    
			INNER JOIN [dbo].[PackingDetails] AS PD ON PD.[ProductAutoId] = DOI.[ProductAutoId] AND PD.[UnitType] = DOI.UnitAutoId                                  
			WHERE [OrderAutoId] =OM.AutoId and OM.Status=11          
			ORDER BY CM.[CategoryName] asc,PM.ProductName ASC for json path, INCLUDE_NULL_VALUES        
		),'[]') as Item1,  
		isnull((
			SELECT CM.[CategoryName],CM.[CategoryId],OIM.ProductAutoId,PM.[ProductId],PM.[ProductName],PM.[ProductLocation],                                                        
			OIM.[UnitTypeAutoId] AS UnitAutoId,UM.[UnitType],OIM.[QtyPerUnit],convert(varchar(30),convert(decimal(18,2),OIM.[UnitPrice])) as UnitPrice,OIM.[TotalPieces],convert(varchar(30),convert(decimal(18,2),OIM.[SRP])) as SRP,
			case when OIM.[GP] <= 0 then '---' else Convert(varchar(15),OIM.[GP]) end as [GP],convert(varchar(30),convert(decimal(18,2),OIM.[Tax])) as Tax,                                                                     
			convert(varchar(30),convert(decimal(18,2),OIM.[NetPrice])) as NetPrice,OIM.[Barcode],
			isnull(OIM.[QtyShip],OIM.[RequiredQty]) as QtyShip    
			,OIM.[RequiredQty] ,                                                     
			convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice,                                                                                                    
			ISNULL(IsExchange,0) as IsExchange,isnull(TaxValue,0.00) as TaxValue,ISNULL(isFreeItem,0) AS isFreeItem,UnitMLQty,                                                                                                    
			ISNULL(UnitMLQty,0) as UnitMLQtyf,ISNULL(TotalMLQty,0) as TotalMLQty,                                                                                                    
			convert(decimal(18,2),oim.Item_TotalMLTax) as TotalMLTax                                                                                                  
			FROM [dbo].[OrderItemMaster] AS OIM                                                                                                     
			INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                                                                                                    
			INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitTypeAutoId                               
			INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId]                           
			INNER JOIN [dbo].[PackingDetails] AS PD ON PD.[ProductAutoId] = OIM.[ProductAutoId] AND PD.[UnitType] = OIM.UnitTypeAutoId                                                          
			WHERE [OrderAutoId] =OM.AutoId and OM.Status!=11                                                                    
			ORDER BY CM.[CategoryName],PM.ProductName ASC for json path, INCLUDE_NULL_VALUES        
		),'[]') as Item2,
		isnull((        
			SELECT OM.[AutoId],[OrderNo],CONVERT(VARCHAR(20),[OrderDate],101) AS OrderDate,
			convert(varchar(30),convert(decimal(18,2),ISNULL(DO.[AmtDue],0))) as AmtDue,
			convert(varchar(30),convert(decimal(18,2),DO.[PayableAmount])) as GrandTotal,
			convert(varchar(30),convert(decimal(18,2),ISNULL(DO.[AmtPaid],0))) as  AmtPaid,                     
			otm.OrderType AS OrderType ,                                                                
			DATEDIFF(dd,OrderDate,GETDATE()) as Aging, orderdate as orderdateSort                                                                                                    
			FROM [dbo].[OrderMaster] As OM1                                                                                                  
			INNER JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM1.[AutoId] 
			INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
			WHERE [CustomerAutoId] = OM.CustomerAutoId AND ([Status] = 11 ) AND isnull(do.[AmtDue],0) > 0.00                                                                                                    
			order by orderdateSort ASC for json path, INCLUDE_NULL_VALUES               
		),'[]') as DueOrderList,  
		isnull(( 
			SELECT OMM.[CreditNo] as CreditNo,CONVERT(VARCHAR(20), OMM.[CreditDate], 101) AS CreditDate,                                    
			CM.[AutoId] As CustAutoId,CM.[CustomerId],CM.[CustomerName],(case when Contact1=Contact2 then Contact1 when isnull(Contact2,'')!='' then CM.[Contact1] + '/ ' + CM.[Contact2] else Contact1 end) As Contact,
			CTy.CustomerType,CT.[TermsDesc],                            
			BA.[Address] As BillAddr,        
			[dbo].[ConvertFirstLetterinCapital](BA.[Address]+(case when isnull(BA.Address2,'')!='' then ', '+BA.Address2 else '' end)+', '+BA.City)+', '+UPper(S.StateCode)+' - '+BA.Zipcode  as BillingAddress,
		    [dbo].[ConvertFirstLetterinCapital](SA.[Address]+(case when isnull(SA.Address2,'')!='' then ', '+SA.Address2 else '' end)+', '+SA.City)+', '+S1.StateCode+' - '+SA.Zipcode as ShippingAddress,
			S.[StateName] AS State1,BA.[City] AS City1,BA.[Zipcode] As Zipcode1,SA.[Address] As ShipAddr,S1.[StateName] As State2,SA.[City] AS City2,                            
			SA.[Zipcode] As Zipcode2,convert(varchar(30),convert(decimal(18,2),OMM.[TotalAmount])) as TotalAmount,ISNULL(OM.[OverallDisc],0) as OverallDisc1,
			ISNULL(convert(varchar(30),convert(decimal(18,2),OM.[OverallDiscAmt])),0) as OverallDisc,ISNULL(convert(varchar(30),convert(decimal(18,2),OMM.[TotalTax])),0) as TotalTax,isnull(convert(varchar(30),convert(decimal(18,2),OMM.[GrandTotal])),convert(varchar(30),convert(decimal(18,2),OMM.[TotalAmount]))) as GrandTotal,                            
			ISNULL(MLQty,0) as MLQty,isnull(MLTax,0) as MLTax,CM.ContactPersonName,ISNULL(AdjustmentAmt,0) as AdjustmentAmt,                    
			ISNULL(convert(varchar(30),convert(decimal(18,2),OMM.WeightTaxAmount)),0.00) as WeightTaxAmount,  --  TaxTypeAutoId Added By Rizwan Ahmad on 11/09/2019 04:00 AM                          
			CM.BusinessName,EM.[FirstName] + ' ' + EM.[LastName] As SalesPerson,
			ISNULL((
			SELECT CM.[CategoryName],CM.[CategoryId],OIM.ProductAutoId,PM.[ProductId],PM.[ProductName],PM.[ProductLocation],                          
			OIM.[UnitAutoId] AS UnitAutoId,UM.[UnitType],OIM.[QtyPerUnit],convert(varchar(30),convert(decimal(18,2),OIM.[UnitPrice])) as UnitPrice,                            
			convert(varchar(30),convert(decimal(18,2),OIM.NetAmount)) AS  [NetPrice],OIM.AcceptedQty AS QtyShip,'' AS BarCode                            
			FROM [dbo].[CreditItemMaster] AS OIM                             
			INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                            
			INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitAutoId                             
			INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId]  
			where OIM.CreditAutoId=OMM.CreditAutoId                           
			ORDER BY CM.[CategoryName] 
			
			ASC for json path, INCLUDE_NULL_VALUES
			),'[]') as CreditItems
			FROM [dbo].[CreditMemoMaster] As OMM                         
			INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OMM.[CustomerAutoId]                                       
			INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]                                    
			LEFT JOIN [dbo].[CustomerTerms] AS CT1 ON CT1.[TermsId] = CM.[Terms]                                   
			INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                             
			INNER JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                             
			left JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OMM.CreatedBy                            
			WHERE OMM.CreditAutoId in (select CreditAutoId from CreditMemoMaster where OrderAutoId =OM.AutoId)  for json path, INCLUDE_NULL_VALUES
	
			),'[]') as CreditDetails, 
			ISNULL((select top 1 PrintLabel from MLTaxMaster where TaxState=BA.State),'') as MLTaxPrintLabel      
			FROM [dbo].[OrderMaster] As OM   --  WeightTax added by Rizwan Ahmad on 17-09-2019                                             
			INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.Status AND SM.[Category] = 'OrderMaster'                                                                                 
			INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[AutoId] = OM.[BillAddrAutoId]                                                                                                      
			INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = OM.[ShipAddrAutoId]                                                   
			INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                                                                                                       
			LEFT JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                                                                                           
			INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                                                                                       
			INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]                                                                                                      
			LEFT JOIN [dbo].[CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                                                                                    
			LEFT JOIN [dbo].[ShippingType] AS ST ON ST.[AutoId] = OM.[ShippingType]                                                                                                       
			left JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[SalesPersonAutoId]                                                                                                             
			left JOIN [dbo].[EmployeeMaster] AS EM1 ON EM1.[AutoId] = OM.[PackerAutoId]                           
			WHERE OM.AutoId=@OrderAutoId for json path, INCLUDE_NULL_VALUES)as OrderDetails 
			FROM CompanyDetails for json path, INCLUDE_NULL_VALUES) as CompanyD   
  END 

   IF @Opcode=47                         
 BEGIN                                                                                 
      DECLARE @Status int
	  SELECT @Status=Status FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId              
	  SELECT cm.CustomerId,cm.CustomerName,om.OrderNo,om.PayableAmount FROM [dbo].[OrderMaster] as om 
	  inner join CustomerMaster as cm on cm.AutoId=om.CustomerAutoId
	  where om.AutoId=@OrderAutoId
	  IF @status=11                                                                                                     
	  BEGIN                                                                                 
		   SELECT CM.[CategoryName],CM.[CategoryId],DOI.[ProductAutoId],PM.[ProductId],PM.[ProductName],DOI.[UnitAutoId],UM.[UnitType],DOI.[QtyPerUnit],                                                                                                       
		   DOI.[Tax],DOI.[NetPrice]  ,[UnitPrice] ,[QtyShip]                                                                                                  
		   ,convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice  ,
		    DOI.IsExchange,isFreeItem         
		   FROM [dbo].[Delivered_Order_Items] AS DOI                                                 
		   INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = DOI.[ProductAutoId]                                                         
		   INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = DOI.UnitAutoId                                                             
		   INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId]                             
		   INNER JOIN [dbo].[PackingDetails] AS PD ON PD.[ProductAutoId] = DOI.[ProductAutoId] AND PD.[UnitType] = DOI.UnitAutoId                                    
		   WHERE [OrderAutoId]=@OrderAutoId ORDER BY PM.[ProductId] ASC                                                    
	  END                                                                           
	  ELSE                                                                                                      
	  BEGIN    
		
			SELECT CM.[CategoryName],CM.[CategoryId],OIM.ProductAutoId,PM.[ProductId],PM.[ProductName],PM.[ProductLocation],                                                          
			OIM.[UnitTypeAutoId] AS UnitAutoId,UM.[UnitType],OIM.[QtyPerUnit],OIM.[UnitPrice],OIM.[TotalPieces],                                                                     
			OIM.[NetPrice],
		    CASE WHEN @Status>2 THEN OIM.[QtyShip] ELSE OIM.RequiredQty END AS [QtyShip],OIM.[RequiredQty] ,                                                       
			convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice                                                                                                                                                                                                       
		     ,oim.IsExchange,isFreeItem                                                                                                                                                  
			FROM [dbo].[OrderItemMaster] AS OIM                                                                                                       
			INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                                                                                                      
			INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitTypeAutoId                                 
			INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId]                             
			INNER JOIN [dbo].[PackingDetails] AS PD ON PD.[ProductAutoId] = OIM.[ProductAutoId] AND PD.[UnitType] = OIM.UnitTypeAutoId                                                            
			WHERE [OrderAutoId] = @OrderAutoId                                                                       
			ORDER BY PM.[ProductId] ASC         
	  END      
  END 
 IF @Opcode=48                         
 BEGIN                                                                                 
      
	  SELECT @Status=Status,@CustomerAutoId=CustomerAutoId FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId  
	  
	   SELECT cm.CustomerId,cm.CustomerName,om.OrderNo,om.PayableAmount,om.AdjustmentAmt,ISNULL(om.Deductionamount,0.00) as CreditAmount,om.OverallDiscAmt,
	   sa.Address,sa.City,sa.Zipcode,st.StateName,st.StateCode FROM [dbo].[OrderMaster] as om 
	   inner join CustomerMaster as cm on cm.AutoId=om.CustomerAutoId
	   inner join ShippingAddress sa on sa.AutoId=cm.DefaultShipAdd 
	  inner join State st on st.AutoId=sa.State 
	   where om.AutoId=@OrderAutoId

	  IF @status=11                                                                                                     
	  BEGIN                                                                                 
		   SELECT CM.[CategoryName],CM.[CategoryId],DOI.[ProductAutoId],PM.[ProductId],PM.[ProductName],DOI.[UnitAutoId],UM.[UnitType],DOI.[QtyPerUnit],                                                                                                       
		   DOI.[Tax],DOI.[NetPrice]  ,[UnitPrice] ,[QtyShip]                                                                                                  
		   ,convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice  
		   ,DOI.IsExchange,isFreeItem                               
		   FROM [dbo].[Delivered_Order_Items] AS DOI                                                 
		   INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = DOI.[ProductAutoId]                                                         
		   INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = DOI.UnitAutoId                                                             
		   INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId]                             
		   INNER JOIN [dbo].[PackingDetails] AS PD ON PD.[ProductAutoId] = DOI.[ProductAutoId] AND PD.[UnitType] = DOI.UnitAutoId                                    
		   WHERE [OrderAutoId]=@OrderAutoId ORDER BY PM.[ProductId] ASC                                                    
	  END                                                                           
	  ELSE                                                                                                      
	  BEGIN    
		
			SELECT CM.[CategoryName],CM.[CategoryId],OIM.ProductAutoId,PM.[ProductId],PM.[ProductName],PM.[ProductLocation],                                                          
			OIM.[UnitTypeAutoId] AS UnitAutoId,UM.[UnitType],OIM.[QtyPerUnit],OIM.[UnitPrice],OIM.[TotalPieces],                                                                     
			OIM.[NetPrice],
		    CASE WHEN @Status>2 THEN OIM.[QtyShip] ELSE OIM.RequiredQty END AS [QtyShip],OIM.[RequiredQty] ,                                                       
			convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice                                                                                                                                                                                                       
		      ,oim.IsExchange,isFreeItem                                                                                                                                             
			FROM [dbo].[OrderItemMaster] AS OIM                                                                                                       
			INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                                                                                                      
			INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitTypeAutoId                                 
			INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId]                             
			INNER JOIN [dbo].[PackingDetails] AS PD ON PD.[ProductAutoId] = OIM.[ProductAutoId] AND PD.[UnitType] = OIM.UnitTypeAutoId                                                            
			WHERE [OrderAutoId] = @OrderAutoId                                                                       
			ORDER BY PM.[ProductId] ASC                                                           
                                                
	  END                          
	  SELECT CreditNo FROM CreditMemoMaster Where CustomerAutoId=@CustomerAutoId AND OrderAutoId=@OrderAutoId AND Status=3
  END
	ELSE IF @Opcode=49                          
	BEGIN                          
                          
	SELECT CompanyName,EmailAddress, [Address],[Website],[MobileNo],[FaxNo],TermsCondition,Logo FROM CompanyDetails            
                            
	SET @CustomerAutoId = (SELECT [CustomerAutoId] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId)               
                         
	SELECT OM.[AutoId],OM.[OrderNo],CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,                                                                                                      
	ISNULL(CONVERT(VARCHAR(20), OM.[DeliveryDate],101),CONVERT(VARCHAR(20), OM.[DelDate],101) + ' ' +convert(varchar(10), OM.[DelDate], 108)) AS DeliveryDate,
	CM.[AutoId] As CustAutoId,CM.[CustomerId],CM.[CustomerName],
	(case when Contact1=Contact2 then Contact1 when isnull(Contact2,'')!='' then CM.[Contact1] + '/ ' + CM.[Contact2] else Contact1 end) As Contact,
	CTy.CustomerType,CT.[TermsDesc],ST.[ShippingType],

	[dbo].[ConvertFirstLetterinCapital](BA.[Address]+(case when isnull(BA.Address2,'')!='' then ', '+BA.Address2 else '' end)+', '+BA.City)+', '+UPper(S.StateCode)+' - '+BA.Zipcode As BillAddr, 
	S.StateCode AS State1,BA.[City] AS City1,BA.[Zipcode] As Zipcode1,

	[dbo].[ConvertFirstLetterinCapital](SA.[Address]+(case when isnull(SA.Address2,'')!='' then ', '+SA.Address2 else '' end)+', '+SA.City)+', '+S1.StateCode+' - '+SA.Zipcode As ShipAddr, 
	S1.StateCode AS State2,SA.[City] AS City2,SA.[Zipcode] As Zipcode2,
	
	OM.[Status],OM.[PackedBoxes],ISNULL(DO.[TotalAmount],om.[TotalAmount]) as [TotalAmount],
	ISNULL(DO.[OverallDisc],om.[OverallDisc]) as [OverallDisc],ISNULL(DO.[OverallDiscAmt],om.[OverallDiscAmt]) as [OverallDiscAmt],                                                                                                      
	ISNULL(DO.[ShippingCharges],om.[ShippingCharges]) as [ShippingCharges],ISNULL(DO.[TotalTax],om.[TotalTax]) as [TotalTax],
	ISNULL(DO.[GrandTotal],om.[GrandTotal]) as [GrandTotal],ISNULL(DO.[AmtPaid],0) as [AmtPaid],ISNULL(DO.[AmtDue],0) as [AmtDue],
	OM.[PaymentRecev],OM.[RecevAmt],OM.[AmtValue],OM.[ValueChanged],                                                                    
	OM.[DiffAmt],OM.[NewTotal],OM.[PayThru],OM.CheckNo,OM.[DrvRemarks],OM.[AcctRemarks],EM.[FirstName] + ' ' + EM.[LastName] As SalesPerson,                                                                                                      
	EM1.[FirstName] + ' ' + EM1.[LastName] As PackerName,[CommentType],[Comment],isnull(do.CreditMemoAmount,om.Deductionamount) as DeductionAmount,                                                                                                      
	ISNULL(DO.CreditAmount,om.CreditAmount)as CreditAmount,isnull(do.PayableAmount,OM.PayableAmount) as PayableAmount,                             
	ISNULL((select t.CreditAmount from CustomerCreditMaster as t  where t.CustomerAutoId=om.CustomerAutoid),0.00) as CustomerCredit                                   
	,om.TaxType,otm.OrderType AS OrderType,OrderRemarks,                                                                                                      
	ISNULL(OM.MLQty,0) as MLQty,isnull(DO.MLTax,om.MLTax) as MLTax,CM.ContactPersonName,ISNULL(om.AdjustmentAmt,0) as AdjustmentAmt,                                                                 
	CM.BusinessName,TTM.PrintLabel AS TaxPrintLabel,(SELECT top 1 PrintLabel from [dbo].[Tax_Weigth_OZ]) AS WeightTaxPrintLabel,
	ISNULL((select top 1 PrintLabel from MLTaxMaster as mlt where mlt.TaxState=BA.State),'') as MLTaxPrintLabel  ,
	ISNULL(UnitMLTax,0) as UnitMLTax,Weigth_OZTaxAmount as WeightTax,TaxValue  FROM [dbo].[OrderMaster] As OM             
	INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.Status AND SM.[Category] = 'OrderMaster'                                                                                                
	INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[AutoId] = OM.[BillAddrAutoId]                                                                             
	INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = OM.[ShipAddrAutoId]                                                                                                
	INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                                                 
	left JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                                                                                                       
	INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                                                                                                 
	INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]                                                                       
	LEFT JOIN [dbo].[CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                                                                      
	LEFT JOIN [dbo].[ShippingType] AS ST ON ST.[AutoId] = OM.[ShippingType]                                                                                             
	LEFT JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[SalesPersonAutoId]                                                                                                      
	LEFT JOIN [dbo].[EmployeeMaster] AS EM1 ON EM1.[AutoId] = OM.[PackerAutoId]   
	LEFT JOIN [dbo].[TaxTypeMaster] AS TTM ON TTM.[AutoId] = OM.[TaxType]  
	LEFT JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId]
	INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
	WHERE OM.AutoId = @OrderAutoId                           
                                                                                                         
	IF EXISTS(SELECT [AutoId] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId AND Status=11)                                                                                                      
	BEGIN                                                                     
		SELECT CM.[CategoryName],CM.[CategoryId],DOI.[ProductAutoId],PM.[ProductId],PM.[ProductName],DOI.[UnitAutoId],UM.[UnitType],DOI.[QtyPerUnit],                                                                                                      
		DOI.[UnitPrice],DOI.[Barcode],DOI.[QtyShip],DOI.[QtyDel],DOI.[TotalPieces],DOI.[SRP],DOI.[GP],  
		case when DOI.[GP] <= 0 then '---' else Convert(varchar(15),DOI.[GP]) end as PrintGP  
		,DOI.[Tax],DOI.[NetPrice]                                                                                                      
		,convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice,ISNULL(FreshReturnQty,0) as FreshReturnQty,FreshReturnUnitAutoId,                                  
		ISNULL(DamageReturnQty,0) as DamageReturnQty,DamageReturnUnitAutoId,                                                                                                      
		IsExchange,isnull(TaxValue,0.00) as TaxValue,ISNULL(isFreeItem,0) AS isFreeItem,                                    
		ISNULL(UnitMLQty,0) as UnitMLQty,ISNULL(TotalMLQty,0) as TotalMLQty,                                                                                                     
		DOI.Item_TotalMLTax as TotalMLTax,ISNULL(DOI.Del_discount,0) as Del_discount                                                                         
		FROM [dbo].[Delivered_Order_Items] AS DOI                                                                                                       
		INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = DOI.[ProductAutoId]                                        
		INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = DOI.UnitAutoId                                                  
		INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId] WHERE [OrderAutoId]=@OrderAutoId                                                                                                   
		AND ISNULL(QtyShip,0)>0                       
		ORDER BY CM.CategoryName,PM.ProductName ASC --11/21/2019 By Rizwan Ahmad                                                                                 
	END           
	ELSE                                                                      
	BEGIN                    
		SELECT CM.[CategoryName],CM.[CategoryId],OIM.ProductAutoId,PM.[ProductId],PM.[ProductName],PM.[ProductLocation],                                                                                                      
		OIM.[UnitTypeAutoId] AS UnitAutoId,UM.[UnitType],OIM.[QtyPerUnit],OIM.[UnitPrice],OIM.[TotalPieces],OIM.[SRP],OIM.[GP],  
		case when OIM.[GP] <= 0 then '---' else convert(varchar(15),OIM.[GP]) end as PrintGP  
		,OIM.[Tax],                                                     
		OIM.[NetPrice],OIM.[Barcode],OIM.[QtyShip],OIM.[RequiredQty] ,convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice,                          
		OIM.IsExchange,isnull(TaxValue,0.00) as TaxValue,ISNULL(isFreeItem,0) AS isFreeItem,UnitMLQty,                                                                                                      
		ISNULL(UnitMLQty,0) as UnitMLQty,ISNULL(TotalMLQty,0) as TotalMLQty,                                                          
		oim.Item_TotalMLTax as TotalMLTax,ISNULL(OIM.Oim_Discount,0) as Del_discount                                                                                
		FROM [dbo].[OrderItemMaster] AS OIM                                                                               
		INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                                                            
		INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitTypeAutoId                                                                                                       
		INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId] WHERE [OrderAutoId] = 100674                                                                                            
		ORDER BY CM.CategoryName,PM.ProductName ASC                                                                
	END    
	Select CreditNo from CreditMemoMaster where CustomerAutoId=@CustomerAutoId AND OrderAutoId=@OrderAutoId AND Status=3
END 

	 IF @Opcode=51                      
 BEGIN                                                                                 
                     
	   SELECT cm.CustomerId,cm.CustomerName,om.OrderNo,om.PayableAmount,om.AdjustmentAmt,ISNULL(om.Deductionamount,0.00) as CreditAmount,om.OverallDiscAmt,
	   sa.Address,sa.City,sa.Zipcode,st.StateName,(
	    SELECT CM.[CategoryName],CM.[CategoryId],DOI.[ProductAutoId],PM.[ProductId],PM.[ProductName],DOI.[UnitAutoId],UM.[UnitType],DOI.[QtyPerUnit],                                                                                                       
		   DOI.[Tax],DOI.[NetPrice]  ,[UnitPrice] ,[QtyShip]                                                                                                  
		   ,convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice                                 
		   FROM [dbo].[Delivered_Order_Items] AS DOI                                                 
		   INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = DOI.[ProductAutoId]                                                         
		   INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = DOI.UnitAutoId                                                             
		   INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId]                             
		   INNER JOIN [dbo].[PackingDetails] AS PD ON PD.[ProductAutoId] = DOI.[ProductAutoId] AND PD.[UnitType] = DOI.UnitAutoId                                    
		   WHERE [OrderAutoId]=@OrderAutoId ORDER BY PM.[ProductId] ASC    
		   for json path
	   ) as DOList,(
	   SELECT CM.[CategoryName],CM.[CategoryId],OIM.ProductAutoId,PM.[ProductId],PM.[ProductName],PM.[ProductLocation],                                                          
			OIM.[UnitTypeAutoId] AS UnitAutoId,UM.[UnitType],OIM.[QtyPerUnit],OIM.[UnitPrice],OIM.[TotalPieces],                                                                     
			OIM.[NetPrice],
		    CASE WHEN om.Status>2 THEN OIM.[QtyShip] ELSE OIM.RequiredQty END AS [QtyShip],OIM.[RequiredQty] ,                                                       
			convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice                                                                                                                                                                                                       
		                                                                                                                                                   
			FROM [dbo].[OrderItemMaster] AS OIM                                                                                                       
			INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                                                                                                      
			INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitTypeAutoId                                 
			INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId]                             
			INNER JOIN [dbo].[PackingDetails] AS PD ON PD.[ProductAutoId] = OIM.[ProductAutoId] AND PD.[UnitType] = OIM.UnitTypeAutoId                                                            
			WHERE [OrderAutoId] = @OrderAutoId                                                                       
			ORDER BY PM.[ProductId] ASC  
			for json path
	   ) as OrderList FROM [dbo].[OrderMaster] as om 
	   inner join CustomerMaster as cm on cm.AutoId=om.CustomerAutoId
	   inner join ShippingAddress sa on sa.AutoId=cm.DefaultShipAdd 
	  inner join State st on st.AutoId=sa.State 
	   where om.AutoId in (SELECT * FROM [dbo].[fnSplitString](@BulkOrderAutoId, ',')) 
	   for json path
	                           
		
  END

  IF @Opcode=52                         
 BEGIN                                                                                 
     
	  SELECT @Status=Status FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId              
	  SELECT cm.CustomerId,cm.CustomerName,om.OrderNo,om.PayableAmount FROM [dbo].[OrderMaster] as om 
	  inner join CustomerMaster as cm on cm.AutoId=om.CustomerAutoId
	  where om.AutoId in (SELECT * FROM [dbo].[fnSplitString](@BulkOrderAutoId, ','))
	  IF @status=11                                                                                                     
	  BEGIN                                                                                 
		   SELECT CM.[CategoryName],CM.[CategoryId],DOI.[ProductAutoId],PM.[ProductId],PM.[ProductName],DOI.[UnitAutoId],UM.[UnitType],DOI.[QtyPerUnit],                                                                                                       
		   DOI.[Tax],DOI.[NetPrice]  ,[UnitPrice] ,[QtyShip]                                                                                                  
		   ,convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice                                 
		   FROM [dbo].[Delivered_Order_Items] AS DOI                                                 
		   INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = DOI.[ProductAutoId]                                                         
		   INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = DOI.UnitAutoId                                                             
		   INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId]                             
		   INNER JOIN [dbo].[PackingDetails] AS PD ON PD.[ProductAutoId] = DOI.[ProductAutoId] AND PD.[UnitType] = DOI.UnitAutoId                                    
		   WHERE [OrderAutoId] in (SELECT * FROM [dbo].[fnSplitString](@BulkOrderAutoId, ',')) ORDER BY PM.[ProductId] ASC                                                    
	  END                                                                           
	  ELSE                                                                                                      
	  BEGIN    
		
			SELECT CM.[CategoryName],CM.[CategoryId],OIM.ProductAutoId,PM.[ProductId],PM.[ProductName],PM.[ProductLocation],                                                          
			OIM.[UnitTypeAutoId] AS UnitAutoId,UM.[UnitType],OIM.[QtyPerUnit],OIM.[UnitPrice],OIM.[TotalPieces],                                                                     
			OIM.[NetPrice],
		    CASE WHEN @Status>2 THEN OIM.[QtyShip] ELSE OIM.RequiredQty END AS [QtyShip],OIM.[RequiredQty] ,                                                       
			convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice                                                                                                                                                                                                       
		                                                                                                                                                   
			FROM [dbo].[OrderItemMaster] AS OIM                                                                                                       
			INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                                                                                                      
			INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitTypeAutoId                                 
			INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId]                             
			INNER JOIN [dbo].[PackingDetails] AS PD ON PD.[ProductAutoId] = OIM.[ProductAutoId] AND PD.[UnitType] = OIM.UnitTypeAutoId                                                            
			WHERE [OrderAutoId] in (SELECT * FROM [dbo].[fnSplitString](@BulkOrderAutoId, ','))                                                                       
			ORDER BY PM.[ProductId] ASC         
	  END      
  END 

	IF @Opcode=50
	BEGIN
		SELECT CompanyName,EmailAddress, [Address],[Website],[MobileNo],[FaxNo],TermsCondition,Logo FROM CompanyDetails                                                                                                                                              
		SELECT @Status=Status from OrderMaster WHERE AutoId=@OrderAutoId
        SELECT OM.[AutoId],OM.[OrderNo],CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,                                                                                                                                    
		ISNULL(CONVERT(VARCHAR(20), OM.[DeliveryDate],101),CONVERT(VARCHAR(20), OM.[DelDate],101) + ' ' +convert(varchar(10), OM.[DelDate], 108)) AS DeliveryDate,  CM.[AutoId] As CustAutoId,CM.[CustomerId]              
		,CM.[CustomerName],
		(case when Contact1=Contact2 then Contact1 when isnull(Contact2,'')!='' then CM.[Contact1] + '/ ' + CM.[Contact2] else Contact1 end) As Contact,
		CTy.CustomerType,CT.[TermsDesc],ST.[ShippingType],                                                                                                                                         
		[dbo].[ConvertFirstLetterinCapital](BA.[Address]+(case when isnull(BA.Address2,'')!='' then ', '+BA.Address2 else '' end)+', '+BA.City)+', '+UPper(S.StateCode)+' - '+BA.Zipcode As BillAddr, 
		S.StateCode AS State1,BA.[City] AS City1,BA.[Zipcode] As Zipcode1,
		[dbo].[ConvertFirstLetterinCapital](SA.[Address]+(case when isnull(SA.Address2,'')!='' then ', '+SA.Address2 else '' end)+', '+SA.City)+', '+S1.StateCode+' - '+SA.Zipcode As ShipAddr, 
		[dbo].[ConvertFirstLetterinCapital](SA.[Address]+(case when isnull(SA.Address2,'')!='' then ', '+SA.Address2 else '' end)+',<br> '+SA.City)+', '+S1.StateCode+' - '+SA.Zipcode As NJMShipAddr, 
		S1.StateCode AS State2,SA.[City] AS City2,SA.[Zipcode] As Zipcode2,
		OM.[Status] As StatusCode,SM.[StatusType] AS Status,OM.[PackedBoxes],om.[TotalAmount],om.[OverallDisc],om.[OverallDiscAmt],                                                                                        
		om.[ShippingCharges],om.[TotalTax],om.[GrandTotal],om.paidAmount,om.[AmtDue],OM.[PaymentRecev],OM.[RecevAmt],OM.[AmtValue],OM.[ValueChanged],                                                                                                                 
 
		OM.[DiffAmt],OM.[NewTotal],OM.[PayThru],OM.CheckNo,OM.[DrvRemarks],OM.[AcctRemarks],EM.[FirstName] + ' ' + EM.[LastName] As SalesPerson,                                                                          
		EM1.[FirstName] + ' ' + EM1.[LastName] As PackerName,[CommentType],[Comment],isnull(DO.CreditMemoAmount,om.DeductionAmount) as DeductionAmount,                                                                                                                                
 
		ISNULL(om.CreditAmount,om.CreditAmount)as CreditAmount,isnull(om.PayableAmount,OM.PayableAmount) as PayableAmount,                                                                           
		ISNULL((select t.CreditAmount from CustomerCreditMaster as t  where t.CustomerAutoId=om.CustomerAutoid),0.00) as CustomerCredit                                                                                                                              
		,ISNULL((select top 1 PrintLabel from MLTaxMaster as mlt where mlt.TaxState=BA.State),'') as MLTaxPrintLabel
		,om.TaxType,otm.OrderType AS OrderType,OrderRemarks,                                                                                                                                                    
		ISNULL(OM.MLQty,0) as MLQty,isnull(om.MLTax,om.MLTax) as MLTax,CM.ContactPersonName,ISNULL(om.AdjustmentAmt,0) as AdjustmentAmt, 
		TTM.PrintLabel AS TaxPrintLabel,(SELECT top 1 PrintLabel from [dbo].[Tax_Weigth_OZ]) AS WeightTaxPrintLabel,
		CM.BusinessName,ISNULL(UnitMLTax,0) as UnitMLTax,om.Weigth_OZTaxAmount as  WeightTax,OM.TaxValue,om.Status as stautoid  FROM [dbo].[OrderMaster] As OM                                                                                                                        
		INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.Status AND SM.[Category] = 'OrderMaster'                                                                                                                                              
		INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[AutoId] = OM.[BillAddrAutoId]                                                                 
		INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = OM.[ShipAddrAutoId]                           
		INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                                                                                                                                                     
		INNER JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                                                                                                                                     
		INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                                                                                                                                               
		INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]                                                                                             
		LEFT JOIN [dbo].[CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                                                                                                                    
		LEFT JOIN [dbo].[ShippingType] AS ST ON ST.[AutoId] = OM.[ShippingType]                                                                                                                        
		LEFT JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[SalesPersonAutoId]                                                                              
		LEFT JOIN [dbo].[EmployeeMaster] AS EM1 ON EM1.[AutoId] = OM.[PackerAutoId]      
		LEFT JOIN [dbo].[TaxTypeMaster] AS TTM ON TTM.[AutoId] = OM.[TaxType]  
		left JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId] 
		INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
		WHERE OM.AutoId = @OrderAutoId                                                                                                                                                 
        
		IF EXISTS(SELECT [AutoId] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId AND Status=11)                                                                               
                              
		BEGIN                                                                                                                   
		                                                                                                                              
			SELECT CM.[CategoryName],CM.[CategoryId],DOI.[ProductAutoId],PM.[ProductId],PM.[ProductName],DOI.[UnitAutoId],UM.[UnitType],DOI.[QtyPerUnit],
			DOI.[UnitPrice],DOI.[Barcode],DOI.[QtyShip],DOI.[QtyDel],DOI.[TotalPieces],DOI.[SRP],DOI.[GP],  
			case when DOI.[GP] < 0 then '---' else Convert(varchar(15),DOI.[GP]) end as PrintGP  
			,DOI.[Tax],DOI.[NetPrice]                                                                                                                                         
			,convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice,ISNULL(FreshReturnQty,0) as FreshReturnQty,FreshReturnUnitAutoId,                                                                                           
			ISNULL(DamageReturnQty,0) as DamageReturnQty,DamageReturnUnitAutoId,                                                                                         
			IsExchange,isnull(TaxValue,0.00) as TaxValue,ISNULL(isFreeItem,0) AS isFreeItem,                                                                   
			ISNULL(UnitMLQty,0) as UnitMLQty,ISNULL(TotalMLQty,0) as TotalMLQty,                                                                                                                                                    
			DOI.Item_TotalMLTax as TotalMLTax,DOI.Del_discount as discount               
			FROM [dbo].[Delivered_Order_Items] AS DOI                                                                                                                                      
			INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = DOI.[ProductAutoId]                                                                                      
			INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = DOI.UnitAutoId                                    
			INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId] WHERE [OrderAutoId]=@OrderAutoId                                                              
			AND QtyShip>0                                                             
			ORDER BY CM.[CategoryName] ASC                          
		END                                                          
		ELSE                                                                                                   
		BEGIN 
			
			SELECT CM.[CategoryName],CM.[CategoryId],OIM.ProductAutoId,PM.[ProductId],PM.[ProductName],PM.[ProductLocation],                                                                 
			OIM.[UnitTypeAutoId] AS UnitAutoId,UM.[UnitType],OIM.[QtyPerUnit],OIM.[UnitPrice],OIM.[TotalPieces],OIM.[SRP],OIM.[GP],  
			case when OIM.[GP] < 0 then '---' else convert(varchar(15),OIM.[GP]) end as PrintGP  
			,OIM.[Tax],                                                 
			OIM.[NetPrice],OIM.[Barcode], OIM.[QtyShip],OIM.[RequiredQty] ,convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice,                                                                                                        
  
			OIM.IsExchange,isnull(TaxValue,0.00) as TaxValue,ISNULL(isFreeItem,0) AS isFreeItem,UnitMLQty,                                                                                                                                                    
			ISNULL(UnitMLQty,0) as UnitMLQty,ISNULL(TotalMLQty,0) as TotalMLQty,                                                                                     
			oim.Item_TotalMLTax as TotalMLTax,oim.Oim_Discount as discount                                                                   
			FROM [dbo].[OrderItemMaster] AS OIM                                                  
			INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                                               
			INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitTypeAutoId                                                                        
			INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId] WHERE [OrderAutoId] = @OrderAutoId                                                                                
			AND (case when @Status=1 or @Status=2 then RequiredQty  else QtyShip end) >0                                                                         
			ORDER BY CM.[CategoryName] ASC                                                                      
		END                                                                                                                                                         
			SET @CustomerAutoId = (SELECT [CustomerAutoId] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId)         
			SELECT OM.[AutoId],[OrderNo],CONVERT(VARCHAR(20),[OrderDate],101) AS OrderDate,DO.[AmtDue],DO.[GrandTotal],DO.[AmtPaid] ,                                                                                                                             
			otm.OrderType AS OrderType ,DATEDIFF(dd,OrderDate,GETDATE()) as Aging, orderdate as orderdateSort 
			FROM [dbo].[OrderMaster] As OM                                                                                                                         
			INNER JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId]     
			INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
			WHERE [CustomerAutoId] = @CustomerAutoId AND ([Status] = 6 or [Status] = 11 ) AND do.[AmtDue] > 0.00                                                                                   
			order by orderdateSort                                                                                            
			select OrderAutoId,CreditAutoId,CreditNo,CONVERT(VARCHAR(10),CreditDate,101) AS CreditDate,ISNULL(TotalAmount,0.00) as ReturnValue,ISNULL(PaidAmount,0.00)  
			as amtDeducted,        
			ISNULL(BalanceAmount,0.00) as amtDue                                                                                                            
			from CreditMemoMaster AS CM where  OrderAutoId=@OrderAutoId               
	END
  END TRY                                                                                                      
  BEGIN CATCH                                              
  Set @isException=1                                                                     
  Set @exceptionMessage='Oops, Something went wrong.Please try later.'                                                                                                      
  END CATCH                                                                                           
END 