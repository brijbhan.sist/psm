ALTER PROCEDURE [dbo].[ProcApp_Customer_PaymentMaster]      
@Opcode int=NULL,      
@CustomerAutoId int=null,  
@isException bit out,      
@exceptionMessage varchar(max) out      
AS      
BEGIN      
BEGIN TRY      
	SET @exceptionMessage= 'Success'      
	SET @isException=0       
       
		IF @Opcode=41       
		BEGIN 
			select DraftNo as draftNo,AutoId as draftAId,CustomerAutoId as cusAId,  
			dbo.[ConvertUTCTimeStamp](DraftDate) as delDate,  
			DefaultBillAutoId as billAID,  
			billAddr,DefaultShipAutoId as shipAID,shipAddr,  
			ISNULL((  
			select pm.AutoId as pAID,ProductId as productId,productName,um.AutoId as uAID,um.UnitType as unitName,  
			Quantity as qty,Price as price,NetPrice from App_DraftCartItemMasterCust as dcm   
			inner join ProductMaster as pm on pm.AutoId=dcm.ProductAutoId  
			inner join UnitMaster as um on um.AutoId=dcm.UnitType  
			where dcm.CartAutoId=do.AutoId  
			for json path  , INCLUDE_NULL_VALUES  
  
			),'[]')  
			as ProductDetails,  
			SubTotal  
			from App_DraftCartMasterCust as do  
			where AutoId=@CustomerAutoId  
			for json path , INCLUDE_NULL_VALUES  
		END      
  
		 
  
END TRY      
BEGIN CATCH      
	SET @isException=1      
	SET @exceptionMessage= ERROR_MESSAGE()      
	END CATCH      
END