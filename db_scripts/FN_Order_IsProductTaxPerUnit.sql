
alter table OrderItemMaster drop column IsProductTaxPerUnit
drop function FN_Order_IsProductTaxPerUnit
go
create FUNCTION  [dbo].[FN_Order_IsProductTaxPerUnit]  
(  
	@AutoId int,  
	@orderAutoId int  
)  
RETURNS decimal(18,2)  
AS  
BEGIN  
	DECLARE @Total decimal(18,2)=0.00      
	IF (select tax from OrderItemMaster where AutoId=@AutoId)=1
	BEGIN  
		set @Total=(select isnull((oim.UnitPrice*om.TaxValue)/100,0) as Tax from OrderItemMaster as oim 
					inner join OrderMaster as om on om.AutoId=oim.OrderAutoId
					where oim.AutoId=@AutoId) 
		
	END  
	RETURN @Total  
END  
  
GO

alter table OrderItemMaster add IsProductTaxPerUnit  AS ([dbo].[FN_Order_IsProductTaxPerUnit]([AutoId],[OrderAutoId]))