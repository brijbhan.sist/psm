USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcCarDetails]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
         
Alter PROCEDURE [dbo].[ProcCarDetails]        
@Opcode INT =null,         
@CarAutoId INT =null,         
@CarId varchar(100) =null,         
@CarName  varchar(100)=null,         
@YearName  INT=null,         
@Make  varchar(100)=null,         
@Model  varchar(100)=null,         
@VINNumber  varchar(100)=null,         
@TAGNumber  varchar(100)=null,         
@InspectionExpDate  DATE=null,         
@StartMilage  varchar(100)=null,         
@Description  varchar(500)=null,         
@PageIndex int=null,         
@PageSize  int=10,         
@RecordCount int=null,         
@Status  varchar(100)=null,        
@isException bit out,          
@exceptionMessage varchar(max) out          
AS         
BEGIN        
 BEGIN TRY          
    SET @exceptionMessage= 'Success'          
    SET @isException=0 

	DECLARE @Childdb varchar(50),@SqlQuery nvarchar(MAX)
    SET @Childdb=(select [ChildDB_Name] from CompanyDetails)

   IF @Opcode=11        
   BEGIN        
		SET @CarId=(SELECT dbo.SequenceCodeGenerator('CarDetails'))        
		INSERT INTO [CarDetailsMaster]([CarId],[CarName],[YearName],[Make],[Model],[VINNumber],[TAGNumber],        
		[InspectionExpDate],[StartMilage],[Description],[Status])        
		VALUES(@CarId,@CarName,@YearName,@Make,@Model,@VINNumber,@TAGNumber,@InspectionExpDate,@StartMilage,@Description,        
		@Status)        
		SET @CarAutoId=SCOPE_IDENTITY()        
		UPDATE SequenceCodeGeneratorMaster SET currentSequence=currentSequence+1 WHERE SequenceCode='CarDetails'  
		Declare @ChildCarId nvarchar(20)
		 SET @SqlQuery='Select @ChildCarId=['+@Childdb+'].[dbo].SequenceCodeGenerator(''CarDetails'')'
		 EXEC sp_executesql @SqlQuery, N'@ChildCarId nvarchar(max) OUTPUT',@ChildCarId OUTPUT

		 SET @SqlQuery='
		 SET IDENTITY_INSERT ['+@Childdb+'].[dbo].CarDetailsMaster ON
		 INSERT INTO ['+@Childdb+'].[dbo].[CarDetailsMaster](CarAutoId,[CarId],[CarName],[YearName],[Make],[Model],[VINNumber],
		 [TAGNumber],[InspectionExpDate],[StartMilage],[Description],[Status])     
		 SELECT CarAutoId,'''+Convert(varchar(25),@ChildCarId)+''',[CarName],[YearName],[Make],[Model],[VINNumber],[TAGNumber],        
		 [InspectionExpDate],[StartMilage],[Description],[Status] FROM CarDetailsMaster where CarAutoId='+Convert(varchar(25),@CarAutoId)+'
		 SET IDENTITY_INSERT ['+@Childdb+'].[dbo].CarDetailsMaster OFF
		 UPDATE SequenceCodeGeneratorMaster SET currentSequence=currentSequence+1 WHERE SequenceCode=''CarDetails''
		 '
		 EXEC sp_executesql @SqlQuery

		 SELECT @CarAutoId AS CarAutoId,@CarId as CarId        
   END        
   ELSE IF @Opcode=21        
   BEGIN        
	  UPDATE [CarDetailsMaster] SET [CarName]=@CarName,[YearName]=@YearName,Make=@Make,Model=@Model        
	  ,VINNumber=@VINNumber,TAGNumber=@TAGNumber,InspectionExpDate=@InspectionExpDate,StartMilage=@StartMilage,        
	  Description=@Description,Status=@Status WHERE CarAutoId=@CarAutoId 
	  
	  SET @SqlQuery='
	  UPDATE CD SET CD.CarName=CDM.CarName,CD.YearName=CDM.YearName,CD.Make=CDM.Make,CD.Model=CDM.Model,CD.VINNumber=CDM.VINNumber,
	  CD.TAGNumber=CDM.TAGNumber,CD.InspectionExpDate=CDM.InspectionExpDate,CD.StartMilage=CDM.StartMilage,
	  CD.Description=CDM.Description,CD.Status=CDM.Status
	  FROM CarDetailsMaster AS CD
	  INNER JOIN CarDetailsMaster AS CDM ON CD.CarAutoId=CDM.CarAutoId WHERE CDM.CarAutoId='+Convert(varchar(20),@CarAutoId)+'
	  '
	  EXEC sp_executesql @SqlQuery
   END        
   ELSE IF @Opcode=41        
   BEGIN        
           
    SELECT ROW_NUMBER() OVER(ORDER BY CARID) AS RowNumber ,CarAutoId,CarId,CarName,YearName,Make,        
    Model,VINNumber,TAGNumber,Format(InspectionExpDate,'MM/dd/yyyy') as InspectionExpDate,StartMilage,        
    CASE WHEN Status=0 THEN 'INACTIVE' ELSE 'ACTIVE' END AS STATUS        
    INTO #RESULT FROM CarDetailsMaster        
    WHERE  ISNULL(@CarName,'')='' OR CarName LIKE '%'+@CarName+'%'        
           
    SELECT COUNT(CARID) AS RecordCount, @PageSize AS PageSize, @PageIndex AS PageIndex FROM #RESULT        
        
    SELECT * FROM #RESULT        
    WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1        
           
   END        
   ELSE IF @Opcode=42        
   BEGIN           
    SELECT CarAutoId,CarId,CarName,YearName,Make,Description,           
    Model,VINNumber,TAGNumber,Format(InspectionExpDate,'MM/dd/yyyy') as InspectionExpDate,StartMilage,    
    --Format Date by Rizwan Ahmad 25-08-2019        
    Status    FROM CarDetailsMaster  WHERE  CarAutoId =@CarAutoId        
   END        
 ELSE IF @Opcode=43      
 BEGIN         
  Begin Try     
       DELETE FROM CarDetailsMaster WHERE  CarAutoId=@CarAutoId  
	   SET @SqlQuery='
	   DELETE FROM ['+@Childdb+'].[dbo].[CarDetailsMaster] WHERE  CarAutoId='+Convert(varchar(15),@CarAutoId)+'
	   '
	   EXEC sp_executesql @SqlQuery
  End Try    
  Begin Catch    
    SET @isException=1    
    SET @exceptionMessage='Exists'     
  ENd Catch    
      
 END      
 END TRY        
 BEGIN CATCH        
    SET @isException=1        
    SET @exceptionMessage='Oops! Something went wrong.Please try later.'           
 END CATCH        
        
END 
GO
