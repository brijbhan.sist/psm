ALTER TABLE [dbo].[OrderMaster] DROP COLUMN 
[OverallDisc] ,
[MLTax],
[Deductionamount],
[CreditAmount], 
[IsMLTaxApply], 
[TotalTax],
[TotalAmount], 
[Weigth_OZQty],
[Weigth_OZTaxAmount],
[GrandTotal],
[PayableAmount],
[AdjustmentAmt],
[TotalNOI]

GO

ALTER FUNCTION  [dbo].[FN_Order_TotalAmount]
(
	 @orderAutoId int
)
RETURNS decimal(10,2) WITH SCHEMABINDING
AS
BEGIN
	DECLARE @TotalAmount decimal(10,2)	 
	IF EXISTS(SELECT AutoId FROM [dbo].OrderMaster WHERE AutoId=@orderAutoId AND Status not IN (11))
	BEGIN
		SET @TotalAmount=(SELECT SUM(NetPrice) FROM [dbo].OrderItemMaster WHERE OrderAutoId=@orderAutoId)	
	END
	ELSE 
	BEGIN
		SET @TotalAmount=(SELECT SUM(NetPrice) FROM [dbo].Delivered_Order_Items WHERE OrderAutoId=@orderAutoId)	
	END

	RETURN @TotalAmount
END
GO

ALTER TABLE [dbo].[OrderMaster] ADD  [TotalAmount]  AS ([dbo].[FN_Order_TotalAmount]([AutoId])) 
 GO
ALTER FUNCTION  [dbo].[FN_OrderOverallDisc]
(
	 @orderAutoId int
)
RETURNS decimal(10,2) WITH SCHEMABINDING
AS
BEGIN
	DECLARE @OverallDisc decimal(10,2)
	set @OverallDisc=isnull((select (case when TotalAmount=0 then 0 else (OverallDiscAmt/TotalAmount) end)  from [dbo].OrderMaster where AutoId=@orderAutoId)	,0.00)*100
	RETURN  convert(decimal(10,2),@OverallDisc)
END
GO

ALTER TABLE [dbo].[OrderMaster] ADD 
[OverallDisc]  AS ([dbo].[FN_OrderOverallDisc]([AutoId]))
GO
ALTER FUNCTION  [dbo].[FN_Order_MLTax]
( 
	@orderAutoId int,
	@status int
)
RETURNS decimal(10,2) WITH SCHEMABINDING
AS
BEGIN
	DECLARE @MLTax decimal(10,2)=0.00	
	if(@status=11)
	begin	
	SET @MLTax=(SELECT sum(Item_TotalMLTax) FROM [dbo].Delivered_Order_Items WHERE OrderAutoId=@orderAutoId)	
	end
	else
	begin 	 
	SET @MLTax=(SELECT sum(Item_TotalMLTax) FROM [dbo].OrderItemMaster WHERE OrderAutoId=@orderAutoId)	
	end
	RETURN @MLTax
END
GO
ALTER TABLE [dbo].[OrderMaster] ADD  
[MLTax]  AS ([dbo].[FN_Order_MLTax]([AutoId],[Status]))
GO
ALTER FUNCTION  [dbo].[FN_Order_DeductionAmount]
(
	 @OrderAutoId int
)
RETURNS decimal(10,2) WITH SCHEMABINDING
AS
BEGIN
	DECLARE @DeductionAmount decimal(10,2)	
	SET @DeductionAmount=isnull((select SUM(TotalAmount) from [dbo].CreditMemoMaster where OrderAutoId=@OrderAutoId),0.00)
	RETURN @DeductionAmount
END
GO
ALTER TABLE [dbo].[OrderMaster] ADD   
[Deductionamount]  AS ([dbo].[FN_Order_DeductionAmount]([AutoId]))
GO
ALTER FUNCTION  [dbo].[FN_Order_IsMLtaxApply]  
(   
  @orderAutoId int
   
)  
RETURNS INT   WITH SCHEMABINDING
AS  
BEGIN  
 DECLARE @Isapply int=0  
 if exists(select AutoId from [dbo].MLTaxMaster where TaxState in (select State from  [dbo].BillingAddress where AutoId in (select BillAddrAutoId from [dbo].OrderMaster where AutoId=@orderAutoId)))  
 begin  
 set @Isapply=1   
 END 
	DECLARE @isMLManualyApply int=ISNULL((select isMLManualyApply from [dbo].OrderMaster where AutoId=@orderAutoId),1) 
	if @isMLManualyApply=0
	begin
		set @Isapply=0
	end
 RETURN @Isapply  
END 
GO

ALTER TABLE [dbo].[OrderMaster] ADD  
[IsMLTaxApply]  AS ([dbo].[FN_Order_IsMLtaxApply]([AutoId]))
GO
ALTER FUNCTION  [dbo].[FN_Order_TaxTotal]
(
	@orderAutoId int
)
RETURNS decimal(10,2) WITH SCHEMABINDING
AS
BEGIN
	DECLARE @TaxTotal decimal(10,2)	=0.00
	DECLARE @TaxValue DECIMAL(10,2)=(SELECT TaxValue FROM [dbo].OrderMaster WHERE AutoId=@orderAutoId)
	IF EXISTS(SELECT AutoId FROM [dbo].OrderMaster WHERE AutoId=@orderAutoId AND IsTaxApply=1)
	BEGIN
		if EXISTS(SELECT AutoId FROM [dbo].OrderMaster WHERE AutoId=@orderAutoId AND Status NOT IN (11))
		BEGIN
			SET @TaxTotal=(select ISNULL(CAST(SUM(NetPrice*@TaxValue/100) AS decimal(10,2)),0) from [dbo].OrderItemMaster where Tax=1
			AND OrderAutoId=@orderAutoId)
		END
		ELSE
		BEGIN
			SET @TaxTotal=(select ISNULL(CAST(SUM(NetPrice*@TaxValue/100) AS decimal(10,2)),0) from [dbo].Delivered_Order_Items where Tax=1
			AND OrderAutoId=@orderAutoId)
		END
	 END
RETURN @TaxTotal
END
GO

ALTER TABLE [dbo].[OrderMaster] ADD     
[TotalTax]  AS ([dbo].[FN_Order_TaxTotal]([AutoId]))
GO

GO
ALTER FUNCTION  [dbo].[FN_Order_Weigth_OZQty]
(
@OrderAutoId int
)
RETURNS decimal(10,2) WITH SCHEMABINDING
AS
BEGIN
DECLARE @Weight_Oz decimal(10,2)	
	IF EXISTS(SELECT 1 FROM [dbo].OrderMaster WHERE AutoId=@OrderAutoId AND Status=11)
	BEGIN
		SET @Weight_Oz=isnull((select SUM(ISNULL(Del_Weight_Oz_TotalQty ,0)) from [dbo].[Delivered_Order_Items] where OrderAutoId=@OrderAutoId),0.00)
	END
	ELSE
	BEGIN
		SET @Weight_Oz=isnull((select SUM(ISNULL(Weight_Oz_TotalQty ,0)) from [dbo].orderItemMaster where OrderAutoId=@OrderAutoId),0.00)
	END
RETURN @Weight_Oz
END
GO
ALTER TABLE [dbo].[OrderMaster] ADD       
[Weigth_OZQty]  AS ([dbo].[FN_Order_Weigth_OZQty]([autoid]))
GO
ALTER FUNCTION  [dbo].[FN_Order_Weigth_OZTaxAmount]
(
	 @AutoId int
)
RETURNS decimal(10,2) WITH SCHEMABINDING
AS
BEGIN
	DECLARE @Weight_Oz decimal(10,2)	
	SET @Weight_Oz=isnull((select  ISNULL(Weigth_OZQty,0)*ISNULL(Weigth_OZTax,0) from [dbo].OrderMaster where AutoId=@AutoId),0.00)
	RETURN @Weight_Oz
END
GO
ALTER TABLE [dbo].[OrderMaster] ADD    
[Weigth_OZTaxAmount]  AS ([dbo].[FN_Order_Weigth_OZTaxAmount]([autoid]))
GO
ALTER FUNCTION  [dbo].[FN_Order_GrandTotal]  
(  
  @orderAutoId int  
)  
RETURNS decimal(10,2)   WITH SCHEMABINDING
AS  
BEGIN  
 DECLARE @GrandTotal decimal(10,2)   
  SET @GrandTotal=(SELECT (round(((([TotalAmount]-[OverallDiscAmt])+[ShippingCharges])+[TotalTax])+[MLTax]+
  ISNULL(Weigth_OZTaxAmount,0),(0))) FROM [dbo].OrderMaster WHERE AutoId=@orderAutoId)   
 RETURN @GrandTotal  
END
GO
ALTER TABLE [dbo].[OrderMaster] ADD       
[GrandTotal]  AS ([dbo].[FN_Order_GrandTotal]([autoid]))
GO
ALTER FUNCTION  [dbo].[FN_StoreCredit]
(
	 @OrderAutoId int 
)
RETURNS decimal(10,2) WITH SCHEMABINDING
AS
BEGIN
	DECLARE @StoreCredit decimal(10,2)		 	 
	SET @StoreCredit=cast((SELECT SUM(ISNULL(Amount,0.00)) FROM [dbo].tbl_Custumor_StoreCreditLog WHERE ReferenceType='OrderMaster' and
	ReferenceNo=CONVERT(varchar(10), @OrderAutoId)) as decimal	(10,2))	 
	RETURN @StoreCredit
END
GO

ALTER TABLE [dbo].[OrderMaster] ADD    
[CreditAmount]  AS ([dbo].[FN_StoreCredit]([Autoid]))
GO
ALTER FUNCTION  [dbo].[FN_Order_PayableAmount]  
(  
  @orderAutoId int  
)  
RETURNS decimal(10,2)  WITH SCHEMABINDING 
AS  
BEGIN  
 DECLARE @PayableAmount decimal(10,2)   
  SET @PayableAmount=(SELECT  ((GrandTotal-isnull([Deductionamount],(0)))-isnull([CreditAmount],(0)))FROM [dbo].OrderMaster WHERE AutoId=@orderAutoId)   
 RETURN @PayableAmount  
END
GO
ALTER TABLE [dbo].[OrderMaster] ADD     
[PayableAmount]  AS ([dbo].[FN_Order_PayableAmount]([autoid]))
GO
 ALTER FUNCTION  [dbo].[FN_Order_AdjustmentAmt]    
(    
  @orderAutoId int    
)    
RETURNS decimal(10,2) WITH SCHEMABINDING   
AS    
BEGIN    
 DECLARE @AdjustmentAmt decimal(10,2)     
   declare @GrandTotal decimal(12,2)=(SELECT ((([TotalAmount]-[OverallDiscAmt])+[ShippingCharges])+[TotalTax]+[MLTax]+ISNULL(Weigth_OZTaxAmount,0)) FROM [dbo].OrderMaster WHERE AutoId=@orderAutoId)  
  SET @AdjustmentAmt=((round(@GrandTotal,0))-(@GrandTotal))     
     
    
 RETURN @AdjustmentAmt    
END 
GO
ALTER TABLE [dbo].[OrderMaster] ADD      
[AdjustmentAmt]  AS ([dbo].[FN_Order_AdjustmentAmt]([autoid]))
GO
ALTER FUNCTION  [dbo].[FN_Order_TotalNoOfItem]    
(    
  @AutoId int    
)    
RETURNS int  WITH SCHEMABINDING  
AS    
BEGIN    
 DECLARE @Total int  
 if exists(Select AutoId from [dbo].OrderMaster where AutoId=@AutoId and Status not in(11))
 begin
    SET @Total=(select count(AutoId) from [dbo].OrderItemMaster where orderAutoid=@AutoId) 
  end
  else
  begin
    SET @Total=(select count(AutoId) from [dbo].Delivered_Order_Items where orderAutoid=@AutoId)
  end
 RETURN @Total    
END
GO
ALTER TABLE [dbo].[OrderMaster] ADD       
[TotalNOI]  AS ([dbo].[FN_Order_TotalNoOfItem]([AutoId]))
