CREATE PROCEDURE [dbo].[spWriteToFile]
(
  @PATH_TO_FILE nvarchar(MAX),
  @TEXT_TO_WRITE nvarchar(MAX)
)
AS
BEGIN
  DECLARE @OLE int 
  DECLARE @FileID int

  EXECUTE sp_OACreate 'Scripting.FileSystemObject', @OLE OUT 
  EXECUTE sp_OAMethod @OLE, 'OpenTextFile', @FileID OUT, @PATH_TO_FILE, 8, 1 
  EXECUTE sp_OAMethod @FileID, 'WriteLine', NULL, @TEXT_TO_WRITE
  EXECUTE sp_OADestroy @FileID
  EXECUTE sp_OADestroy @OLE 
END