USE [psmnj.a1whm.com]
GO

/****** Object:  Table [dbo].[DriverOptimoRouteMasterLog]    Script Date: 07-14-2020 02:45:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DriverOptimoRouteMasterLog]') AND type in (N'U'))
DROP TABLE [dbo].[DriverOptimoRouteMasterLog]
GO

/****** Object:  Table [dbo].[DriverOptimoRouteMasterLog]    Script Date: 07-14-2020 02:45:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DriverOptimoRouteMasterLog](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[PanningId] [int] NOT NULL,
	[OrderAutoId] [int] NOT NULL
) ON [PRIMARY]
GO


