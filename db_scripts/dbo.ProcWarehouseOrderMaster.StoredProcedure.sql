alter PROCEDURE [dbo].[ProcWarehouseOrderMaster]                                                                                                                                              
 @Opcode INT=NULL,                                                                                                                                              
 @OrderAutoId INT=NULL,                                                                                                                                               
 @OrderNo VARCHAR(15)=NULL,                                                                                                                                           
 @PaymentId VARCHAR(20)=NULL,                                                                                                                                              
 @PaymentAutoId INT=NULL,                                                                                                                                              
 @OrderDate DATETIME=NULL,                                                                                                                                              
 @DeliveryDate DATETIME=NULL,                                                                                                                                              
 @CustomerAutoId INT=NULL,                                                                                                                                      
 @Times INT=NULL,         
 @custType int=null,                                                                                                                                          
 @ProductAutoId INT=NULL,                                                                                                                                              
 @UnitAutoId INT=NULL,                                      
 @SalesPersonAutoId INT=NULL,                  
 @LoginEmpType INT=NULL,                                  
 @PackerAutoId INT=NULL,                                
 @EmpAutoId INT=NULL,                                                     
 @OrderStatus INT=NULL,                               
 @Remarks VARCHAR(max)=NULL,                                                              
 @CommentType INT=NULL,                                                                                                                    
 @Comment VARCHAR(max)=NULL,                                              
 @AddressAutoId INT=NULL,                                                 
 @OrderItemAutoId INT=NULL,  
 @TableValueMultiOrder DT_MultiOrderAssign readonly,                                                                                                                            
 @Todate DATETIME=NULL,                                                                                                                                              
 @Fromdate DATETIME=NULL,                                                                                                                                             
 @QtyShip INT=NULL,                                                                                                                                        
 @FromDelDate DATE=NULL,                                                                               
 @ToDelDate DATE=NULL,                                                                     
 @DriverAutoId INT=NULL,                                                                                                                                       
 @AsgnDate DATE=NULL,                                                                                                                                                
 @Stoppage VARCHAR(10)=NULL,                                                                                                                                              
 @PkgAutoId INT=NULL,                                                                                                                                              
 @PkgId VARCHAR(12)=NULL,                                                                                                                                              
 @PkgDt DATETIME=NULL,                                                                                                                             
 @PriceLevelAutoId INT=NULL,                                                                                                                
 @PageIndex INT = 1,                                        
 @PageSize INT = 10,                                                                                                                      
 @RecordCount INT =null,                     
 @LastBasePrice DECIMAL(18,2)=NULL,                                                                                                                              
 @CurrentPrice DECIMAL(18,2)=NULL,                                              
 @LogRemark varchar(200) = NULL,                                                                                                                                              
 @DraftAutoId int =null out,                                                
 @TypeShipping  VARCHAR(250)=NULL,                                                                                                                         
 @isException bit out,                                     
 @exceptionMessage varchar(max) out                                                                                
AS                                                                                                                                              
BEGIN                                                                                                                         
  BEGIN TRY                                                                                                                                      
   Set @isException=0                                                                                                                     
   Set @exceptionMessage='Success'                                                                                                   
   declare @customerType varchar(50)                                                                                                                                          
    IF @Opcode = 2021                                                                                                                                              
 BEGIN                                                                                          
  BEGIN TRY                                                                                                                                              
  BEGIN TRAN 
    IF EXISTS(Select AutoId FROM EmployeeMaster where AutoId=@EmpAutoId and EmpType=7)
	BEGIN
	  IF EXISTS(SELECT Autoid FROM EmployeeMaster Where Autoid=@PackerAutoId AND EmpType=3)
		BEGIN
		UPDATE om SET PackerAutoId = @PackerAutoId,PackerAssignDate=getdate(),                                                                                                             
		WarehouseAutoId=@EmpAutoId,WarehouseRemarks=@Remarks,                                                                              
		PackerAssignStatus =1,Times =@Times,                                                                                        
		[Status] =(case when [Status]=1 then 2 else Status end)     
		from  OrderMaster as om     
		inner join @TableValueMultiOrder as tbl on tbl.OrderAutoId=om.AutoId                                                                                
		--WHERE [AutoId] = @OrderAutoId                                                                                                          
		SET @LogRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=18), '[PackerName]',
		(SELECT [FirstName] + ' ' + [LastName] AS packername FROM [dbo].[EmployeeMaster] WHERE [AutoId] = @PackerAutoId)) 
		INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                                                                     
		select 18,@EmpAutoId,getdate(),@LogRemark,OrderAutoId from @TableValueMultiOrder as am                                                                                                                                            
		IF(@OrderStatus=1)                                                                                                                                              
		BEGIN                                                                                                                                                   
			SET @LogRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=3),'[Status1]','New')                                                             
			SET @LogRemark = REPLACE(@LogRemark,'[Status2]','Processed.')                                                          
			INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                                                                                                                              
			select 3,@EmpAutoId,getdate(),@LogRemark,em.OrderAutoId from @TableValueMultiOrder as em                                                                    
		END        
        
		DECLARE @m INT=0                                                                                                                                              
		SELECT ROW_NUMBER() OVER (ORDER BY OrderAutoId DESC) AS ROWNo,* INTO #Ckeck  FROM @TableValueMultiOrder                                                                                                             
		SET @m=1                                                                                                                                              
		WHILE @m<=(SELECT COUNT(*) FROM @TableValueMultiOrder)                                                                                   
		BEGIN                                                                                                                               
			SET @OrderAutoId=(SELECT OrderAutoId FROM #Ckeck WHERE ROWNo=@m)                                                                                                       
			BEGIN                                                                                                                                              
				--SET @m=(SELECT COUNT(*) FROM @TableValueMultiOrder)                                                                                                                             
				SELECT @CustomerAutoId=CustomerAutoId FROM [dbo].[OrderMaster]           
				WHERE [AutoId] = @OrderAutoId 
				if((select CustomerType from CustomerMaster where AutoId=@CustomerAutoId)=3)                
				begin                
					exec [dbo].[ProcUpdatePOStatus_All]                
					@CustomerId=@CustomerAutoId,                
					@OrderAutoId=@OrderAutoId,                
					@Status=2                    
				end                                                                                                                                                    
			END                                                                     
			SET @m=@m+1          
		END  
		END
	 ELSE
	 BEGIN
		  Set @isException=1                                                                                      
		  Set @exceptionMessage='Unauthorized Access.' 
	 END
	 END
	 ELSE
	 BEGIN
		  Set @isException=1                                                                                      
		  Set @exceptionMessage='Unauthorized Access.' 
	 END
  COMMIT TRANSACTION                                                                                                                                              
  END TRY                                                                    
  BEGIN CATCH                                                                           
  ROLLBACK TRAN                                                   
	  Set @isException=1                                                                                      
	  Set @exceptionMessage='Oops! Something went wrong.Please try later.'                                                                                  
  END CATCH                                                                                          
 END                                                                                                                                            
                                                                                                                             
  ELSE IF @Opcode=301                                                                                                                                              
   BEGIN                                                                                                                                             
                                                                                              
    DELETE FROM  [dbo].[OrderItems_Original] WHERE  [OrderAutoId] = @OrderAutoId                                  
    DELETE FROM  [dbo].[Order_Original] WHERE [AutoId] = @OrderAutoId                                                                                                             
    DELETE FROM [dbo].[OrderItemMaster] WHERE [OrderAutoId] = @OrderAutoId               
    DELETE FROM  [dbo].[GenPacking] WHERE [OrderAutoId] = @OrderAutoId                                                                                                                                              
    DELETE FROM [dbo].[DrvLog] WHERE [OrderAutoId] = @OrderAutoId                                                                                                                                              
    DELETE FROM [dbo].[OrderMaster] WHERE [AutoId] = @OrderAutoId                                                                                                                                              
   END                                              
                                                                                                                                                 
  ELSE IF @Opcode=405                                                                                                                                         
   BEGIN                                                                         
		SELECT ROW_NUMBER() OVER(ORDER BY [AutoId] DESC) AS RowNumber, * INTO #Results from                                                         
		(  SELECT OM.[AutoId],OM.OrderNo,CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,CM.[CustomerName] AS CustomerName,             
		SM.StatusType AS Status,OM.[Status] As StatusCode,OM.[GrandTotal],(emp.FirstName + ' '+ emp.LastName) as SalesPerson,  
		(Select ShippingType from ShippingType Where AutoId=om.ShippingType) as ShippingType,OM.ShippingType as ShipId,       
		(SELECT COUNT(distinct [ProductAutoId]) from [dbo].[OrderItemMaster] WHERE OrderAutoId=OM.AutoId) AS NoOfItems ,                                                                                                                           
		'' as CreditMemo                                                                                                   
		FROM [dbo].[OrderMaster] As OM                                                                                                                                              
		INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.AutoId = OM.CustomerAutoId  AND CM.Status=1                                       
		INNER JOIN [dbo].[EmployeeMaster] AS emp ON emp.AutoId = OM.SalesPersonAutoId                          
		INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.[Status] AND SM.Category = 'OrderMaster'                                                                                                                                                         
		WHERE (@OrderNo is null or @OrderNo ='' or OrderNo like '%' + @OrderNo + '%')                           
		and (@CustomerAutoId is null or @CustomerAutoId =0 or CustomerAutoId =@CustomerAutoId)                                      
		and (@FromDate is null or @FromDate = '' or @Todate is null or @Todate = '' or                                                                                  
		(CONVERT(date,OM.[OrderDate])  between CONVERT(date,@FromDate) and CONVERT(date,@Todate)))                                                                                                                                              
		and (@OrderStatus is null or @OrderStatus=0 or OM.[Status]=@OrderStatus)                          
		and OM.[Status] in (1,2,9)                                                                                                                                         
		and (@SalesPersonAutoId = 0 or om.[SalesPersonAutoId]=@SalesPersonAutoId)                                                                                              
		and (ISNULL(@TypeShipping,'0')='0' or ISNULL(@TypeShipping,'0,')='0,' or om.ShippingType in (select * from dbo.fnSplitString(@TypeShipping,',')))                                                                                                     
		)as t order by [AutoId] DESC                                                                                                                           
                          
		SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize=0 then COUNT(RowNumber) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Results                                                                                                   
        
		SELECT * FROM #Results                                                                                                                                              
		WHERE (ISNULL(@PageSize,0)=0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))                                                                    
   END                                                                                                                          
  ELSE IF @Opcode=407                                                                                                                            
     BEGIN    
		SELECT [AutoId],[StatusType] FROM [dbo].[StatusMaster] WHERE  [Category]='OrderMaster'                                                  
		AND [AutoId] IN(1,2,9)  order by  [StatusType]                                                       
                                                                                                                         
		SELECT AutoId,(FirstName +' '+ISNULL(LastName,'')) AS EmpName FROM EmployeeMaster   
		WHERE EmpType =2 and Status=1 order by (FirstName +' '+ISNULL(LastName,''))   
		
		SELECT AutoId,ShippingType FROM ShippingType  order by ShippingType                                
 END                                                                                                                             
                                                                                          
  ELSE IF @Opcode=412                                                                                                                                              
   BEGIN                                                                                           
		SELECT [AutoId],[FirstName] + ' ' + [LastName] As Name FROM [dbo].[EmployeeMaster] WHERE [EmpType]=3  AND Status=1 order by [FirstName] + ' ' + [LastName] asc                                                                                           
   END                                                                                                                                          
   ELSE IF @Opcode=413                                                                                                                                            
   BEGIN                                                                                           
		SELECT [AutoId],[FirstName] + ' ' + [LastName] As Name FROM [dbo].[EmployeeMaster] WHERE [EmpType]=3  AND Status=1 order by [FirstName] + ' ' + [LastName] asc 
		SELECT PackerAutoId,WarehouseRemarks  FROM OrderMaster WHERE AutoId=@OrderAutoId   
   END                                                                                                                                             
                               
  -- if @Opcode = 213                                                  
  -- begin                                  
  --set @ProductAutoId=(select ProductAutoId from OrderItemMaster where AutoId=@DraftAutoId)                                        
  --set @UnitAutoId=(select UnitTypeAutoId from OrderItemMaster where AutoId=@DraftAutoId)                                              
  --set @CustomerAutoId=(SELECT CustomerAutoId FROM OrderMaster WHERE AutoId=@OrderAutoId)                                  
  --set @customerType=(SELECT CustomerType FROM CustomerMaster WHERE AutoId=@CustomerAutoId)             
  --SELECT (CASE                                                                                                                                               
  --WHEN @custType=2 THEN ISNULL(WHminPrice,0)                                    
  --WHEN @custType=3  THEN ISNULL(CostPrice,0)                                                                                                             
  --ELSE [MinPrice] END) AS [MinPrice] ,[CostPrice],(case  WHEN  @custType=3 then ISNULL(CostPrice,0) else [Price] end)                                  
  --[Price],CONVERT(DECIMAL(10,2),((PM.[P_SRP]-([Price]/(CASE WHEN [Qty]=0 then 1 ELSE [Qty] END)))/(CASE WHEN PM.[P_SRP]=0 then 1 ELSE PM.[P_SRP] END )) * 100) AS GP,                                                                                   
  --isnull((select Top 1 [CustomPrice] from [dbo].[ProductPricingInPriceLevel] AS PPL where PPL.[ProductAutoId] = PD.[ProductAutoId] AND PPL.[UnitAutoId] = PD.[UnitType]                                                                                       
  --AND [PriceLevelAutoId] = (SELECT [PriceLevelAutoId] FROM [dbo].[CustomerPriceLevel] WHERE [CustomerAutoId]=@CustomerAutoId)                                                                                
  --Order by [CustomPrice] desc),null) as [CustomPrice],PM.[Stock]/(CASE WHEN [Qty]=0 then 1 ELSE [Qty] END)  as Stock,PM.[TaxRate],PM.[P_SRP] as  [SRP]               
  --FROM [dbo].[PackingDetails] As PD                  
  --INNER JOIN [dbo].[ProductMaster] AS PM ON PD.[ProductAutoId]=PM.[AutoId]                                                                                                                            
  --WHERE PD.[ProductAutoId] = @ProductAutoId and PD.[UnitType] = @UnitAutoId                                    
  -- end                                                  
     ELSE IF @Opcode=435  
  BEGIN  
  select Autoid as CustomerAutoId,CustomerId+' '+CustomerName as CustomerName from CustomerMaster  
   where ISNULL(@SalesPersonAutoId,0)=0 or SalesPersonAutoId=@SalesPersonAutoId  
   order by CustomerId+' '+CustomerName  
  END                                                                                                                                         
  END TRY                                                                
  BEGIN CATCH                                                 
  Set @isException=1                                    
  Set @exceptionMessage='Oops! Something went wrong.Please try later.'                                                                                                                                              
  END CATCH                                                                                 
END 
GO
