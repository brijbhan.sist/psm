ALTER Proc [dbo].[ProcUpdatePOStatus_All]    
@CustomerId int =null,    
@OrderAutoId int =null,    
@Status int=null    
as    
begin    
Declare @LocationId int =(Select LocationAutoId from CustomerMaster where AutoId=@CustomerId)     
Declare @SqlStr nvarchar(max)=null,@OrderNo varchar(20),@TotalAmount Decimal(18,2),@TOI int

   Select @OrderNo=OrderNo,@TotalAmount=PayableAmount,@TOI=TotalNOI from ordermaster where AutoId=@OrderAutoId   
    
  set @SqlStr ='Update ['+(select Location from LocationMaster where AutoId=@LocationId)+'.a1whm.com].[dbo].[PurchaseOrderMaster] set Status=@status,
  TotalOrderAmount=@TotalAmount,TotalNoOfItem=@TOI,OrderNo=@OrderNo,DeliveryDate=GETDATE() where PONo in (Select referenceOrderNumber from     
  [dbo].[OrderMaster]  where AutoId=@OrderAutoId)'    
      
  Execute Sp_executesql @SqlStr,    
  N'@OrderAutoId int,@status int,@OrderNo varchar(20),@TotalAmount Decimal(10,2),@TOI int',
  @OrderAutoId,@status,@OrderNo,@TotalAmount,@TOI
     
end
