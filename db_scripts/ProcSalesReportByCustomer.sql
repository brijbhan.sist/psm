USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcSalesReportByCustomer]    Script Date: 04/07/2020 23:03:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER  PROCEDURE [dbo].[ProcSalesReportByCustomer]                                                                    
@Opcode INT=NULL,                                                                    
@CustomerAutoId INT=NULL,                                                                     
@CustomerType int=null,                                                                                                                          
@EmpAutoId  INT=NULL,                                                                                                 
@SalesPersonAutoId int =null,  
@SalesPerson  VARCHAR(500)=NULL,                                                         
@PageIndex INT=1,                                                                    
@PageSize INT=10,                                                                    
@RecordCount INT=null,                                                                    
@isException bit out,                                                                    
@exceptionMessage varchar(max) out                                                                    
AS                                                                    
BEGIN                                                                     
 BEGIN TRY                                                                    
  Set @isException=0                                                                    
  Set @exceptionMessage='Success'                                                                    
                                                                                                                       
 IF @Opcode=41                                                                 
  BEGIN                                                                    
  select 
  (SELECT AutoId  as CId,CustomerType as Ct from CustomerType order by CustomerType ASC
  for json path
  ) as CType,
  (
  Select em.AutoId as SID ,(em.FirstName + ' ' + em.LastName) as SP from EmployeeMaster em                                           
  inner join EmployeeTypeMaster etm on etm.AutoId = em.EmpType                                          
  where EmpType = 2 and Status=1 order by SP ASC 
  for json path
  ) as SalesList
   for JSON path
 END                                                                  
  ELSE IF @Opcode=42                                                               
  BEGIN   
  SELECT AutoId AS CuId,CustomerId+' '+CustomerName as CUN FROM CustomerMaster   WHERE            
  status=1 and             
   (CustomerType=@CustomerType                                       
  OR ISNULL(@CustomerType,0)=0) and (SalesPersonAutoId=@SalesPersonAutoId or ISNULL(@SalesPersonAutoId,0)=0)
  order by replace(CustomerId+' '+CustomerName,' ','') ASC     
   for json path
   
  END                                                

ELSE  If @Opcode=43                                                                     
  BEGIN                                                      
		SELECT ROW_NUMBER() OVER(ORDER BY [CustomerName]) AS RowNumber, * INTO #Results43 FROM                                                                    
		(                                                                    
		select CustomerName,COUNT(CustomerName) as NoofOrder,isnull(sum(do.PayableAmount),0.00) as TotalOrderAmount,                                                                    
		isnull(sum(do.AmtPaid),0.00) as TotalPaid,sum((isnull(do.PayableAmount,0.00)-isnull(do.AmtPaid,0.00))) as AmtDue from OrderMaster as om                                                                    
		inner join DeliveredOrders as do on om.AutoId=do.OrderAutoId and ISNULL(dueOrderStatus,0)=0                                                      
		inner join  CustomerMaster as cm on cm.AutoId=om.CustomerAutoId                                                                    
		where                       
		om.Status=11 and                                                             
		(isnull(do.PayableAmount,0.00)-isnull(do.AmtPaid,0.00))> 0                                                   
		AND (ISNULL(@CustomerAutoId,0)=0 or cm.AutoId=@CustomerAutoId)                                    
		AND (isnull(@SalesPersonAutoId,0)=0 or cm.SalesPersonAutoId=@SalesPersonAutoId)                                                   
		AND(ISNULL(@CustomerType,0)=0 or cm.CustomerType=@CustomerType)                                                                        
		group by CustomerName                                             
					) AS t ORDER BY [CustomerName]                                                                                                               
		SELECT COUNT(CustomerName) AS RecordCount, case when @PageSize=0 then COUNT(CustomerName) else @PageSize end AS PageSize, @PageIndex AS PageIndex,Format(getdate(),'MM/dd/yyyy hh:mm tt') as PrintDate FROM #Results43                                                                    
		SELECT * FROM #Results43                                                                    
		WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))                                                                          
                          
		SELECT ISNULL(SUM(TotalOrderAmount),0.00) AS TotalOrderAmount,ISNULL(SUM(TotalPaid),0.00) AS TotalPaid, isnull(Sum(NoofOrder),0) as NoofOrder,                                                                   
		ISNULL(SUM(AmtDue),0.00) AS AmtDue FROM #Results43 
		Select FORMAT(GETDATE(),'MM/dd/yyyy hh:mm tt') as [PrintTime]
  END            
	
 END TRY                                                    
 BEGIN CATCH                                                                    
  Set @isException=1                                                                    
  Set @exceptionMessage=ERROR_MESSAGE()                                                                    
 END CATCH                                                                    
END 
