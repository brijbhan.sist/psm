ALTER PROCEDURE [dbo].[ProcApp_CustomerOrderMaster]                                                                                             
@Opcode INT=NULL,                                                                                                                                 
@CustomerAutoId INT=NULL,      
@DraftAutoId int =null,
@OrderAutoId int=null,
@Remarks VARCHAR(500)=NULL,
@isException bit out,                                           
@exceptionMessage varchar(max) out   
AS
BEGIN                                                                                                                               
	If @Opcode=101                                                                                                                       
	BEGIN                                                               
		BEGIN TRY                                                                                                                                                    
		BEGIN TRAN
			DECLARE @OrderNo VARCHAR(25)= (SELECT DBO.SequenceCodeGenerator('OrderNumber'))                                                                       
			DECLARE @BillAddrAutoId INT=(SELECT DefaultBillAutoId FROM App_DraftCartMasterCust WHERE AutoId=@DraftAutoId)     

			declare @State INT=(SELECT TOP 1 State FROM BillingAddress WHERE AutoId =@BillAddrAutoId)                                                                         		                                                                            
			DECLARE @Terms INT=(SELECT Terms  FROM CustomerMaster WHERE AutoId=@CustomerAutoId)                                                                                                                  
			DECLARE @IsTaxApply INT=0                                                                                                                    
	                                                             
			INSERT INTO [dbo].[OrderMaster] ([OrderNo],[OrderDate],[DeliveryDate],[CustomerAutoId],[Terms],[BillAddrAutoId],[ShipAddrAutoId],                                                                                                                      
			[SalesPersonAutoId],OverallDiscAmt,[ShippingCharges],[Status],[ShippingType],                                                                                                                                     
			TaxType,OrderRemarks,OrderType,TaxValue,mlTaxPer,IsTaxApply,Weigth_OZTax)                 
			SELECT @OrderNo,getdate(),getdate(),CustomerAutoId,@Terms,DefaultBillAutoId,DefaultShipAutoId,                                                                                                                  
			(SELECT [SalesPersonAutoId] FROM [dbo].[CustomerMaster] WHERE [AutoId] = @CustomerAutoId),                                
			0,0,1,1,0,@Remarks,3,0,ISNULL((SELECT TaxRate FROM MLTaxMaster where TaxState=@STATE),0.00),0,0
			FROM App_DraftCartMasterCust  WHERE AutoId=@DraftAutoId 
			
			SET @OrderAutoId =SCOPE_IDENTITY()

			INSERT INTO [dbo].[OrderItemMaster]([OrderAutoId],[ProductAutoId],[UnitTypeAutoId],[QtyPerUnit],[UnitPrice],[RequiredQty],
			[SRP],[GP],[Tax],IsExchange,TaxValue,UnitMLQty,isFreeItem,Weight_Oz)                                                                                                                                                  
			SELECT @OrderAutoId,tb.ProductAutoId,tb.UnitType,pd.[Qty],tb.Price,tb.Quantity ,pm.[P_SRP],                                                          
			CONVERT(DECIMAL(10,2),((PM.[P_SRP]-(tb.Price/(CASE WHEN Qty=0 THEN 1 ELSE [Qty] END)))/(CASE WHEN PM.[P_SRP]=0 THEN 1 ELSE PM.[P_SRP] END)) * 100)			
			,0,0,0,0, 0,0                              
			FROM App_DraftCartItemMasterCust AS tb                                     
			inner join ProductMaster as pm on pm.AutoId=tb.ProductAutoId
			inner join PackingDetails as pd on pd.ProductAutoId=tb.ProductAutoId and tb.UnitType=pd.UnitType WHERE DraftAutoId=@DraftAutoId                                                                                              
             
			                                                                      
                                                                                                                                                  
			DELETE FROM App_DraftCartMasterCust WHERE AutoId=@DraftAutoId                                                                                                     
			DELETE FROM App_DraftCartItemMasterCust WHERE DraftAutoId=@DraftAutoId                                                                                                                                                  
                                                                      
			update CustomerMaster set LastOrderDate = GETDATE() where AutoId =@CustomerAutoId                                                                                            
                                                                                                                      
			select @OrderAutoId as OrderAutoId,@OrderNo as OrderNo     

			UPDATE SequenceCodeGeneratorMaster SET currentSequence = currentSequence + 1 WHERE SequenceCode='OrderNumber'    

		COMMIT TRAN
		END TRY
		BEGIN CATCH
			ROLLBACK TRAN
			Set @isException=1                                                                                                   
			Set @exceptionMessage='Server error'             
		END CATCH
	END
END                                                                                                 
  

