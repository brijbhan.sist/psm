--ALTER TABLE ShippingType add ShippingStatus int 
ALTER PROCEDURE [dbo].[Proc_Manager_OrderMaster]                                      
@Opcode INT=NULL,  
@IPAddress varchar(100)=null,
@OrderAutoId INT=NULL,                                       
@CustomerAutoId  int=null,                                      
@DriverAutoId int=null,                                      
@LoginEmpType int =null,      
@DeliveryTodate DATETIME=NULL,  
@DeliveryFromdate DATETIME=NULL,  
@OrderNo VARCHAR(15)=NULL,                                       
@OrderDate DATETIME=NULL,                                       
@AsgnDate datetime=null,                                      
@OrderStatus INT=NULL,                                      
@IsTaxable int =NULL,                                       
@ProductAutoId INT=NULL,                                      
@UnitAutoId INT=NULL,                                       
@EmpAutoId INT=NULL,                                       
@ReqQty  INT=NULL,                                      
@QtyPerUnit INT=NULL,                                      
@SalesPersonAutoId INT=NULL,   
@SalesPerson varchar(max)=null,
@UnitPrice DECIMAL(8,2)=NULL,                                         
@Remarks VARCHAR(max)=NULL,                                        
@Todate DATETIME=NULL,                                      
@CheckSecurity  VARCHAR(max)=NULL,                                        
@LogRemark   VARCHAR(max)=NULL,                                        
@Fromdate DATETIME=NULL,  
@MLTaxRemark varchar(MAX)=null,
@PageIndex INT = 1,                                      
@PageSize INT = 10,                                      
@RecordCount INT =null, 
@CurrentStatus INT =null, 
@isException bit out,                                      
@exceptionMessage varchar(max) out,                                                                              
@TypeShipping VARCHAR(max)=NULL,
@customerType varchar(50)= NULL,
@OrdLat varchar(25)=NULL,
@OrdLong varchar(25)=NULL, @OrderType int =NULL,    
@OrdCity varchar(200)=NULL,
@OrdState varchar(200)=NULL,
@OrdAddress varchar(300)=NULL,
@OrdZipcode varchar(50)=NULL,
@ShippingAddress2 varchar(100)=NULL,
@OrderBy int=null,
@OrdStatus varchar(max)=null,
@StateFilter varchar(max)=null
AS                                      
BEGIN                                       
  BEGIN TRY                                      
  Set @isException=0                                      
  Set @exceptionMessage='Success'  
  Declare @StateCode varchar(5)
  IF @Opcode=41                                      
   BEGIN 
    execute [dbo].[WaitForDelay]

	 SELECT ROW_NUMBER() OVER(ORDER BY  OrderNo desc) AS RowNumber, * INTO #Results from                                      
    (                                         
		SELECT OM.[AutoId],OM.OrderNo,OM.[OrderDate]OrderDate,CM.AutoId as CustomerAutoId,CM.[CustomerName] AS CustomerName,isnull(OM.Stoppage,'') as StopNo,                           
		SM.StatusType AS Status,OM.[Status] As StatusCode,isnull(OM.[GrandTotal],0.00) as GrandTotal,(emp.FirstName + ' '+ emp.LastName) as SalesPerson,isnull(OM.[PayableAmount],0.00) as PayableAmount,                                      
		(SELECT COUNT(distinct [ProductAutoId]) from [dbo].[OrderItemMaster] WHERE OrderAutoId=OM.AutoId) AS NoOfItems ,CONVERT(VARCHAR(20), OM.[DeliveryDate], 101) AS DeliveryDate,
		(select COUNT(1) from CreditMemoMaster as cmm where (Status=1 or Status=3)  and OrderAutoId is null                                      
		and cmm.CustomerAutoId = OM.CustomerAutoId ) as CreditMemo ,(Select ShippingType from ShippingType Where AutoId=om.ShippingType) as ShippingType,OM.ShippingType  as ShipId,
		(case when OrdShippingAddress!='' then OM.SA_Lat else SA.SA_Lat end) as SA_Lat,(case when OrdShippingAddress!='' then OM.SA_Long else SA.SA_Long end) as SA_Long
		FROM [dbo].[OrderMaster] As OM                                      
		INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.AutoId = OM.CustomerAutoId                                       
		INNER JOIN [dbo].[EmployeeMaster] AS emp ON emp.AutoId = OM.SalesPersonAutoId                                       
		INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.[Status] AND SM.Category = 'OrderMaster'
		INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
		INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = OM.[ShipAddrAutoId]   
		WHERE (@OrderNo is null or @OrderNo ='' or OrderNo like '%' + @OrderNo + '%')                                      
		and (@CustomerAutoId is null or @CustomerAutoId =0 or om.CustomerAutoId =@CustomerAutoId)   
		AND (@OrderType =0  or @OrderType is null or  OM.OrderType=@OrderType)  
		and (@FromDate is null or @FromDate = '' or @Todate is null or @Todate = '' or                                       
		(CONVERT(date,OM.[OrderDate]) between CONVERT(date,@FromDate) and CONVERT(date,@Todate)))   
	
		and (@DeliveryFromdate is null or @DeliveryFromdate = '' or @DeliveryToDate is null or @DeliveryToDate = '' or                                       
		(CONVERT(date,OM.[DeliveryDate]) between CONVERT(date,@DeliveryFromdate) and CONVERT(date,@DeliveryToDate)))
  
		and (ISNULL(@OrdStatus,'0')='0' or (@OrdStatus = '0,') or OM.Status in (select * from dbo.fnSplitString(@OrdStatus,',')))     
	
		and (ISNULL(@SalesPerson,'0')='0' or (@SalesPerson = '0,') or OM.SalesPersonAutoId in (select * from dbo.fnSplitString(@SalesPerson,',')))     
		and (@DriverAutoId IS NULL OR @DriverAutoId = 0 or om.Driver=@DriverAutoId)                  
		and (ISNULL(@TypeShipping,'0')='0' or (@TypeShipping = '0,') or OM.ShippingType in (select * from dbo.fnSplitString(@TypeShipping,',')))      
		and (ISNULL(@StateFilter,'0')='0' or (@StateFilter = '0,') or SA.State in (select * from dbo.fnSplitString(@StateFilter,',')))    
    )
	as t order by  OrderNo desc

    SELECT ROW_NUMBER() OVER(ORDER BY (CASE WHEN @OrderBy=1 then CustomerName end) asc,(CASE WHEN @OrderBy=2 then OrderDate end) desc) AS 
	RowNumber, * INTO #Results1 from                                      
    (                                         
		SELECT AutoId, OrderNo,OrderDate,CustomerAutoId,CustomerName,StopNo,Status,StatusCode, [GrandTotal],SalesPerson, [PayableAmount],                                      
		NoOfItems ,DeliveryDate,CreditMemo ,ShippingType,ShipId,SA_Lat,SA_Long
		FROM #Results                       
    )
	as t order by  (CASE WHEN @OrderBy=1 then CustomerName end) asc,(CASE WHEN @OrderBy=2 then OrderDate end) desc
	
		SELECT COUNT(1) AS RecordCount, case when @PageSize=0 then COUNT(1) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Results1                                               
		SELECT AutoId, OrderNo,Format(OrderDate,'MM/dd/yyyy') as OrderDate,CustomerAutoId,CustomerName,StopNo,Status,StatusCode, [GrandTotal],SalesPerson, [PayableAmount],                                      
		NoOfItems ,DeliveryDate,CreditMemo ,ShippingType,ShipId,SA_Lat,SA_Long FROM #Results1                                      
		WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)) 
   END    
   
  IF @Opcode=42                                      
   BEGIN                                         
    SET @CustomerAutoId=(SELECT CustomerAutoId FROM OrderMaster WHERE AutoId=@OrderAutoId)    
	IF EXISTS(SELECT AUTOID FROM CustomerMaster WHERE AutoId=@CustomerAutoId AND CustomerType=3)
	BEGIN
		UPDATE OrderMaster SET isMLManualyApply=0,IsTaxApply=0,Weigth_OZTax=0 WHERE AutoId=@OrderAutoId
	END

    IF EXISTS(SELECT [AutoId] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId AND status=11)                                      
     BEGIN                                      
      SELECT OM.[AutoId],OM.[OrderNo],CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,                                      
      ISNULL(CONVERT(VARCHAR(20), OM.[DeliveryDate],101),CONVERT(VARCHAR(20), OM.[DelDate],101)) AS DeliveryDate,                                
      CM.[AutoId] As CustAutoId,CM.[CustomerId],CM.[CustomerName],CM.[Contact1] + '/' + CM.[Contact2] As Contact,CTy.CustomerType,CT.[TermsDesc],ST.[ShippingType],                                      
      [dbo].[ConvertFirstLetterinCapital](BA.[Address]+(case when isnull(BA.Address2,'')!='' then ', '+BA.Address2 else '' end)+', '+BA.City)+', '+UPper(S.StateCode)+' - '+BA.Zipcode As BillAddr,
	  ST.[AutoId] as ShipAutoId,                                
      [dbo].[ConvertFirstLetterinCapital](SA.[Address]+(case when isnull(SA.Address2,'')!='' then ', '+SA.Address2 else '' end)+', '+SA.City)+', '+S1.StateCode+' - '+SA.Zipcode As ShipAddr,                                            
	  OM.[Status] As StatusCode,SM.[StatusType] AS Status,OM.[PackedBoxes],DO.[TotalAmount],DO.[OverallDisc],DO.[OverallDiscAmt],                                      
      DO.[ShippingCharges],DO.[TotalTax],DO.[GrandTotal],DO.[AmtPaid],DO.[AmtDue],OM.[PaymentRecev],OM.[RecevAmt],OM.[AmtValue],OM.[ValueChanged],                                      
      OM.[DiffAmt],OM.[NewTotal],OM.[PayThru],OM.CheckNo,OM.[DrvRemarks],OM.[AcctRemarks],EM.[FirstName] + ' ' + EM.[LastName] As SalesPerson,                                      
      EM1.[FirstName] + ' ' + EM1.[LastName] As PackerName,[CommentType],[Comment],isnull(DeductionAmount,0.00) as DeductionAmount,                                      
            ISNULL(DO.CreditAmount,0.00)as CreditAmount,isnull(do.PayableAmount,0.00) as PayableAmount,                                      
            ISNULL((select t.CreditAmount from CustomerCreditMaster as t  where t.CustomerAutoId=om.CustomerAutoid),0.00) as CustomerCredit                                      
          
			,otm.OrderType,OrderRemarks,ST.EnabledTax,                                     
            ISNULL(OM.MLQty,0) as MLQty,isnull(DO.MLTax,0) as MLTax,CM.ContactPersonName,ISNULL(om.AdjustmentAmt,0) as AdjustmentAmt,                                      
            CM.BusinessName,ISNULL(UnitMLTax,0) as UnitMLTax,ISNULL(DO.AmtPaid,0) as AmtPaid,ISNULL(do.AmtDue,0) as AmtDue,                                      
      (select TaxableType from TaxTypeMaster as ttm where ttm.Autoid=om.taxType) as TaxType ,OM.Driver ,isMLManualyApply,        
      isnull(OM.Weigth_OZQty,0) as WeightOzQty,isnull(OM.Weigth_OZTaxAmount,0) as WeightOzTotalTax,dbo.UDF_CheckMLTax(OM.AutoId) as MLTaxStatus       
                                     
      FROM [dbo].[OrderMaster] As OM                                           
      INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                     
      INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.Status AND SM.[Category] = 'OrderMaster'                                      
      INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[AutoId] = CM.DefaultBillAdd                                      
      INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = CM.DefaultShipAdd                      
      INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                                       
      INNER JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                                              
      INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]                                      
      LEFT JOIN [dbo].[CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                                      
      LEFT JOIN [dbo].[ShippingType] AS ST ON ST.[AutoId] = OM.[ShippingType]                                       
      LEFT JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[SalesPersonAutoId]                                      
      LEFT JOIN [dbo].[EmployeeMaster] AS EM1 ON EM1.[AutoId] = OM.[PackerAutoId]                                       
      INNER JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId] 
	  INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
	  WHERE OM.AutoId = @OrderAutoId     
	  
	  SET @customerType=(select CustomerType from CustomerMaster where AutoId=@CustomerAutoId)
                                      
      SELECT CM.[CategoryName],CM.[CategoryId],DOI.[ProductAutoId],PM.[ProductId],PM.[ProductName],DOI.[UnitAutoId],UM.[UnitType],DOI.[QtyPerUnit],                                      
      (select top 1 requiredQty from OrderItemMaster oim where oim.orderAutoId=doi.OrderAutoId and oim.ProductAutoid=doi.ProductAutoid and oim.UnitTypeAutoId=doi.UnitAutoid )                                      
      as RequiredQty,                                      
      DOI.[UnitPrice],DOI.[Barcode],DOI.[QtyShip],DOI.[QtyDel],DOI.[TotalPieces],DOI.[SRP],DOI.[GP],DOI.[Tax],DOI.[NetPrice]                                      
      ,convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice,
	  ISNULL(FreshReturnQty,0) as FreshReturnQty,FreshReturnUnitAutoId,                                      
      ISNULL(DamageReturnQty,0) as DamageReturnQty,DamageReturnUnitAutoId,  
	  ISNULL(MissingItemQty,0) as MissingItemQty,FreshReturnUnitAutoId,
      IsExchange,isnull(TaxValue,0.00) as TaxValue,ISNULL(isFreeItem,0) AS isFreeItem,                                      
      ISNULL(UnitMLQty,0) as UnitMLQty,ISNULL(TotalMLQty,0) as TotalMLQty,
	  (DOI.Del_MinPrice) as [MinPrice],DOI.BasePrice as BasePrice,                                      
      DOI.Item_TotalMLTax as TotalMLTax,isnull(Del_discount,0.00) as Discount,isnull(Del_ItemTotal,0.00) as ItemTotal                                    
      FROM [dbo].[Delivered_Order_Items] AS DOI                                       
      INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = DOI.[ProductAutoId] 
	  INNER JOIN [dbo].[PackingDetails] AS PD ON PD.ProductAutoId = DOI.ProductAutoId and DOI.UnitAutoId=pd.UnitType
      INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = DOI.UnitAutoId                                       
      INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId] WHERE [OrderAutoId]=@OrderAutoId                                      
                                      
      SET @CustomerAutoId = (SELECT [CustomerAutoId] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId)                                      
                                      
      SELECT OM.[AutoId],[OrderNo],CONVERT(VARCHAR(20),[OrderDate],101) AS OrderDate,DO.[AmtDue],DO.[GrandTotal],DO.[AmtPaid] ,                                    
    
	  otm.OrderType,DATEDIFF(dd,OrderDate,GETDATE()) as Aging, orderdate as orderdateSort                                      
      FROM [dbo].[OrderMaster] As OM                            
      INNER JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId]   
	   INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
      WHERE [CustomerAutoId] = @CustomerAutoId AND ([Status] = 6 or [Status] = 11) AND DO.[AmtDue] > 0.00                                      
      order by orderdateSort                                       
      
	  
                                            
     END                                      
    ELSE                                
     BEGIN                                      
      SELECT OM.[AutoId],OM.[OrderNo],CONVERT(VARCHAR(20),OM.[OrderDate], 101) AS OrderDate,                                     
      CONVERT(VARCHAR(20), OM.[DeliveryDate],101)--CONVERT(VARCHAR(20),OM.[DeliveryDate], 131)                                
      AS DeliveryDate,                                      
      CM.[AutoId] As CustAutoId,CM.[CustomerId],CM.[CustomerName],CM.[Contact1] + '/' + CM.[Contact2] As Contact,CTy.CustomerType,CT.[TermsDesc],ST.[ShippingType],ST.AutoId as ShipAutoId,                                     
       [dbo].[ConvertFirstLetterinCapital](BA.[Address]+(case when isnull(BA.Address2,'')!='' then ', '+BA.Address2 else '' end)+', '+BA.City)+', '+UPper(S.StateCode)+' - '+BA.Zipcode As BillAddr,                                
      [dbo].[ConvertFirstLetterinCapital](SA.[Address]+(case when isnull(SA.Address2,'')!='' then ', '+SA.Address2 else '' end)+', '+SA.City)+', '+S1.StateCode+' - '+SA.Zipcode As ShipAddr,                                            
	  OM.[Status] As StatusCode,SM.[StatusType] AS Status,OM.[TotalAmount],OM.[OverallDisc],OM.[OverallDiscAmt],                                 
      OM.[ShippingCharges],OM.[TotalTax],OM.[GrandTotal],OM.[PackedBoxes],EM.[FirstName] + ' ' + EM.[LastName] As SalesPerson,                                      
      EM1.[FirstName] + ' ' + EM1.[LastName] As PackerName,OM.[DrvRemarks],[CommentType],[Comment]                                      
      ,isnull(DeductionAmount,0.00) as DeductionAmount,                                      
            isnull(CreditAmount,0.00)as CreditAmount,isnull(PayableAmount,0.00) as PayableAmount                                      
            ,ISNULL((select t.CreditAmount from CustomerCreditMaster as t  where t.CustomerAutoId=om.CustomerAutoid),0.00) as CustomerCredit                                      
            ,(select TaxableType from TaxTypeMaster as ttm where ttm.Autoid=om.taxType) as TaxType,
			
			otm.OrderType,OrderRemarks ,ST.EnabledTax,                                   
            ISNULL(MLQty,0) as MLQty,isnull(MLTax,0) as MLTax,CM.ContactPersonName,ISNULL(AdjustmentAmt,0) as AdjustmentAmt,                                      
            CM.BusinessName,OM.Driver,isMLManualyApply,isnull(OM.Weigth_OZQty,0) as WeightOzQty,isnull(OM.Weigth_OZTaxAmount,0) as WeightOzTotalTax,
			dbo.UDF_CheckMLTax(OM.AutoId) as MLTaxStatus         
    FROM [dbo].[OrderMaster] As OM                                      
      INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.Status AND SM.[Category] = 'OrderMaster'                                      
      INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[AutoId] = OM.[BillAddrAutoId]                                      
      INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = OM.[ShipAddrAutoId]                           
      INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                                       
      INNER JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                                       
      INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                       
      INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]                                      
      LEFT JOIN [dbo].[CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                                      
      LEFT JOIN [dbo].[ShippingType] AS ST ON ST.[AutoId] = OM.[ShippingType]                                       
      left JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[SalesPersonAutoId]                                             
      left JOIN [dbo].[EmployeeMaster] AS EM1 ON EM1.[AutoId] = OM.[PackerAutoId] 
	     INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
	  WHERE OM.AutoId = @OrderAutoId                                      
                            
		SET @customerType=(select CustomerType from CustomerMaster where AutoId=@CustomerAutoId)
		
      SELECT CM.[CategoryName],CM.[CategoryId],OIM.ProductAutoId,PM.[ProductId],PM.[ProductName],PM.[ProductLocation],                                      
      OIM.[UnitTypeAutoId] AS UnitAutoId,UM.[UnitType],OIM.[QtyPerUnit],OIM.[UnitPrice],OIM.[TotalPieces],OIM.[SRP],OIM.[GP],OIM.[Tax],                                      
      OIM.[NetPrice],OIM.[Barcode],OIM.[QtyShip],OIM.[RequiredQty] ,convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice,                        
      OIM.IsExchange,isnull(TaxValue,0.00) as TaxValue,ISNULL(isFreeItem,0) AS isFreeItem,UnitMLQty,
	  (case @customertype when 2 then PD.whminprice when 3 then PD.costprice else [minprice] end) as [MinPrice],PD.Price as BasePrice,                                       
      ISNULL(UnitMLQty,0) as UnitMLQty,ISNULL(TotalMLQty,0) as TotalMLQty,                                      
      oim.Item_TotalMLTax as TotalMLTax,isnull(Oim_Discount,0.00) as Discount,isnull(Oim_ItemTotal,0.00) as ItemTotal                                      
      FROM [dbo].[OrderItemMaster] AS OIM                                       
      INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId] 
	   INNER JOIN [dbo].[PackingDetails] AS PD ON PD.ProductAutoId = OIM.ProductAutoId and OIM.UnitTypeAutoId=pd.UnitType
      INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitTypeAutoId                                       
      INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId] WHERE [OrderAutoId] = @OrderAutoId                                       
      ORDER BY CM.[CategoryName] ASC                                      
                                      
      SET @CustomerAutoId = (SELECT [CustomerAutoId] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId)                                      
                                      
      SELECT OM.[AutoId],[OrderNo],CONVERT(VARCHAR(20),[OrderDate],101) AS OrderDate,DO.[AmtDue],DO.[GrandTotal],DO.[AmtPaid] ,                                      
    

	 otm.OrderType, DATEDIFF(dd,OrderDate,GETDATE()) as Aging, orderdate as orderdateSort                                      
      FROM [dbo].[OrderMaster] As OM                                      
      INNER JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId]   
	      INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
      WHERE [CustomerAutoId] = @CustomerAutoId AND ([Status] = 6 or [Status] = 11 ) AND do.[AmtDue] > 0.00                                      
      order by orderdateSort                                       
                                              
     END                                           
                                          
    select OrderAutoId,CreditAutoId,CreditNo,CONVERT(VARCHAR(10),CreditDate,101) AS CreditDate,ISNULL(TotalAmount,0.00) as ReturnValue,ISNULL(PaidAmount,0.00)  as amtDeducted,                                      
    ISNULL(BalanceAmount,0.00) as amtDue                                      
                from CreditMemoMaster AS CM  where Status =3 --AND CustomerAutoId=@CustomerAutoId                                       
                AND OrderAutoId=@OrderAutoId                                       
                                      
  SELECT * FROM (                                                                                                                    
  SELECT 'Driver' as EmpType,DrvRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om                                                                        
  inner join EmployeeMaster as emp on emp.AutoId=om.Driver                                                              
  where om.AutoId=@OrderAutoId   and ISNULL(DrvRemarks,'')!=''    
  UNION                                                                                             
  SELECT ' Account' as EmpType,CancelRemark as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om                            
  inner join EmployeeMaster as emp on emp.AutoId=om.AccountAutoId where om.AutoId=@OrderAutoId                                      
  and ISNULL(CancelRemark,'')!=''  
  UNION                                                                                             
  SELECT 'Account' as EmpType,AcctRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om                            
  inner join EmployeeMaster as emp on emp.AutoId=om.AccountAutoId where om.AutoId=@OrderAutoId                                      
  and ISNULL(AcctRemarks,'')!=''                                                                                                               
                                                                                                                                                        
  UNION                                                                                                                
  SELECT 'Sales Person' as EmpType,OrderRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName  from OrderMaster as om                                  
  inner join EmployeeMaster as emp on emp.AutoId=om.SalesPersonAutoId where                                                                                                                 
  om.AutoId=@OrderAutoId            and ISNULL(OrderRemarks,'')!=''                       
  UNION                                                                                                                
  SELECT 'Packer' as EmpType,PackerRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om                                                                                             
  inner join EmployeeMaster as emp on emp.AutoId=om.PackerAutoId                                                                                                           
  where om.AutoId=@OrderAutoId          and ISNULL(PackerRemarks,'')!=''                                                                                               
  UNION                      
  SELECT 'Sales Manager' as EmpType,ManagerRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName  from OrderMaster as om                                                                                                                
  inner join EmployeeMaster as emp on emp.AutoId=om.ManagerAutoId                                      
  where om.AutoId=@OrderAutoId     and ISNULL(ManagerRemarks,'')!=''                                                                                                                
  UNION  
  SELECT 'Sales Manager' as EmpType,MLTaxRemark as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName  from OrderMaster as om                                                                                                                
  inner join EmployeeMaster as emp on emp.AutoId=om.ManagerAutoId                                      
  where om.AutoId=@OrderAutoId  and ISNULL(MLTaxRemark,'')!='' 
  UNION
  SELECT 'Warehouse Manager' as EmpType,WarehouseRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName as EmpName  from OrderMaster as om                                                                                                                
  inner join EmployeeMaster as emp on emp.AutoId=om.WarehouseAutoId                                                                                                                 
  where om.AutoId=@OrderAutoId             and ISNULL(WarehouseRemarks,'')!=''                                                                                                  
  ) AS T                                       
             SELECT [PackingId],CONVERT(VARCHAR,[PackingDate],101) As PkgDate,EM.[FirstName] + ' ' + EM.[LastName] As Packer                                       
    FROM [dbo].[GenPacking] AS GP                                      
    INNER JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId]=GP.[PackerAutoId] WHERE [OrderAutoId]=@OrderAutoId                                      
                                      
    select CreditAutoId,CreditNo,CONVERT(VARCHAR(10),CreditDate,101) AS CreditDate,                                      
    (SELECT COUNT(*) FROM CreditItemMaster AS CIM WHERE CIM.CreditAutoId=CM.CreditAutoId) AS NoofItem,                                      
    (SELECT SUM(NetAmount) FROM CreditItemMaster AS CIM WHERE CIM.CreditAutoId=CM.CreditAutoId) AS NetAmount                                      
    from CreditMemoMaster AS CM  where Status =1 AND CustomerAutoId=@CustomerAutoId  
 END                                      
                                        
  ELSE IF @Opcode=43                                      
  BEGIN                                      
		SELECT [AutoId],[StatusType] FROM [dbo].[StatusMaster] WHERE   [Category]='OrderMaster'   ORDER BY [StatusType] ASC                             
		SELECT [AutoId],CustomerId+'-'+[CustomerName] As Customer FROM [dbo].[CustomerMaster] ORDER BY CustomerId,replace([CustomerName],' ','') ASC                                     
		SELECT AutoId,(FirstName +' '+ISNULL(LastName,'')) AS EmpName FROM EmployeeMaster WHERE EmpType =2 ORDER BY EmpName                                      
		SELECT AutoId,(FirstName +' '+ISNULL(LastName,'')) AS EmpName FROM EmployeeMaster WHERE EmpType =5    order by EmpName 
		SELECT AutoId,ShippingType FROM ShippingType where Shippingstatus=1 order by   ShippingType  
		Select AutoId,StateName from State where Status=1
  END                                      
  ELSE IF @Opcode=44          
   BEGIN                      
		SELECT em.[AutoId],[FirstName] + ' ' + [LastName] + ' [' + etm.TypeName + ']' as Name, 
		(select count (1) from OrderMaster om where Status in (4,5) and em.AutoId = om.Driver) as AssignOrders FROM [dbo].[EmployeeMaster] em            
		inner join EmployeeTypeMaster etm on etm.AutoId =  em.EmpType            
		WHERE ([EmpType]=5 or em.autoid in (SELECT SalesPersonAutoId            
		FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId                    
		) ) AND Status=1               
		order by em.EmpType,FirstName               
                            
		SELECT [OrderNo],[Driver],PackerAutoId,convert(varchar(10),isnull(AssignDate,getdate()),101) as AssignDate,OrderDate                                       
		FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId                                      
   END
  ELSE IF @Opcode=45
  BEGIN
    SELECT ROW_NUMBER() OVER(ORDER BY  OrderNo DESC) AS RowNumber, * INTO #TemResults from                                      
    (Select FORMAT(OM.OrderDate,'MM/dd/yyyy') as OrderDate,OM.OrderNo,UM.UnitType,ISNULL(OIM.QtyShip,OIM.RequiredQty) as QtyShip,OIM.UnitPrice,SM.StatusType,SM.AutoId as StatusCode, 
		  NetPrice as  TotalPrice,isnull(isFreeItem,0) as isFreeItem,ISNULL(IsExchange,0) as IsExchange from OrderMaster as OM
		  INNER JOIN OrderItemMaster AS OIM ON OM.AutoId=OIM.OrderAutoId
		  INNER JOIN UnitMaster AS UM ON OIM.UnitTypeAutoId=UM.AutoId
		  INNER JOIN StatusMaster AS SM ON OM.Status=SM.AutoId  and sm.Category='OrderMaster'
		  Where OM.Status NOT IN (7,8,11) AND OIM.ProductAutoId=@ProductAutoId AND OM.CustomerAutoId=@CustomerAutoId
		  Union
		  Select FORMAT(OM.OrderDate,'MM/dd/yyyy') as OrderDate,OM.OrderNo,UM.UnitType,OIM.QtyShip,OIM.UnitPrice,SM.StatusType,SM.AutoId,  
		  NetPrice as TotalPrice,isnull(isFreeItem,0) as isFreeItem,ISNULL(IsExchange,0) as IsExchange from OrderMaster as OM
		  INNER JOIN Delivered_Order_Items AS OIM ON OM.AutoId=OIM.OrderAutoId
		  INNER JOIN UnitMaster AS UM ON OIM.UnitAutoId=UM.AutoId
		  INNER JOIN StatusMaster AS SM ON OM.Status=SM.AutoId and sm.Category='OrderMaster'
		  Where OM.Status=11 AND OIM.ProductAutoId=@ProductAutoId AND OM.CustomerAutoId=@CustomerAutoId
     )as t order by [OrderNo] DESC                                    
     SELECT COUNT(1) AS RecordCount, case when @PageSize=0 then COUNT(1) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #TemResults                                               
     SELECT * FROM #TemResults                                      
     WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)) 
	  Select SUM(UnitPrice) AS OverAllUnitPrice,SUM(TotalPrice) AS OverALllTotalPrice FROM #TemResults
  END
    ELSE IF @Opcode=46
  BEGIN
   SELECT * FROM (                                                                                                                    
	SELECT 'Driver' as EmpType,DrvRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om                                                                        
	inner join EmployeeMaster as emp on emp.AutoId=om.Driver                                                              
	where om.AutoId=@OrderAutoId  and ISNULL(DrvRemarks,'')!=''    
   
	union                                                                                        
	SELECT ' Account' as EmpType,CancelRemark as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om                            
	inner join EmployeeMaster as emp on emp.AutoId=om.AccountAutoId 
	where om.AutoId=@OrderAutoId  and ISNULL(CancelRemark,'')!=''  
  
	union                                                                                          
	SELECT 'Account' as EmpType,AcctRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om                            
	inner join EmployeeMaster as emp on emp.AutoId=om.AccountAutoId
	where om.AutoId=@OrderAutoId  and ISNULL(AcctRemarks,'')!=''                                                                                                               
                                                                                                                                                        
	union                                                                                                         
	SELECT 'Sales Person' as EmpType,OrderRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName  from OrderMaster as om                                  
	inner join EmployeeMaster as emp on emp.AutoId=om.SalesPersonAutoId where                                                                                                                 
	om.AutoId=@OrderAutoId  and ISNULL(OrderRemarks,'')!=''                       
   
	union                                                                                                           
	SELECT 'Packer' as EmpType,PackerRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om                                                                                             
	inner join EmployeeMaster as emp on emp.AutoId=om.PackerAutoId                                                                                                           
	where om.AutoId=@OrderAutoId and ISNULL(PackerRemarks,'')!=''                                                                                               
  
	union                
	SELECT 'Sales Manager' as EmpType,ManagerRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName  from OrderMaster as om                                                                                                                
	inner join EmployeeMaster as emp on emp.AutoId=om.ManagerAutoId                                      
	where om.AutoId=@OrderAutoId and ISNULL(ManagerRemarks,'')!=''                                                                                                                
  
	union
	SELECT 'Sales Manager' as EmpType,MLTaxRemark as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName  from OrderMaster as om                                                                                                                
	inner join EmployeeMaster as emp on emp.AutoId=om.ManagerAutoId                                      
	where om.AutoId=@OrderAutoId and ISNULL(MLTaxRemark,'')!='' 
  
	union
	SELECT 'Warehouse Manager' as EmpType,WarehouseRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName as EmpName  from OrderMaster as om                                                                                                                
	inner join EmployeeMaster as emp on emp.AutoId=om.WarehouseAutoId                                                                                                                 
	where om.AutoId=@OrderAutoId and ISNULL(WarehouseRemarks,'')!=''                                                                                                  
  ) AS T     
  
  END
  Else If @Opcode=21                                      
  BEGIN
		Declare @OldStatus varchar(50)=null
		Select @OldStatus=sm.StatusType,@SalesPersonAutoId=om.SalesPersonAutoId from OrderMaster as om 
		inner join statusmaster sm on sm.AutoId=om.Status and sm.Category='OrderMaster'
		where om.AutoId=@OrderAutoId  
		IF NOT EXISTS(SELECT AutoId FROM OrderMaster WHERE AutoId=@orderAutoId AND Status IN (1,2,8,6,11))  
		BEGIN 
				IF NOT EXISTS(SELECT AutoId FROM OrderMaster WHERE AutoId=@orderAutoId AND Status IN (9))  
				BEGIN 
			 
					IF EXISTS(Select * from AllocatedPackingDetails where OrderAutId=@OrderAutoId)
					BEGIN
						 DELETE FROM AllocatedPackingDetails where OrderAutId=@OrderAutoId
					END
				END
				insert into [dbo].[ProductStockUpdateLog]([ProductId],[OledStock],[NewStock],[UserAutoId],[DateTime],ReferenceId,ActionRemark,ReferenceType)
				SELECT pm.ProductId,isnull([Stock],0),(isnull(pm.Stock,0)+(isnull(TotalPieces,0))),@EmpAutoId,GETDATE(),@OrderAutoId,
				'Order No-(' + (select OrderNo from OrderMaster as om where om.AutoId=@OrderAutoId) + ') has set as processed by '+(Select FirstName+' '+LastName from Employeemaster where autoid=@EmpAutoId)+'.','OrderMaster'
				FROM [dbo].[ProductMaster] AS PM                                                                                                  
				INNER JOIN (
				SELECT sum(TotalPieces) as TotalPieces,ProductAutoId FROM OrderItemMaster
				where OrderAutoId=@OrderAutoId AND isnull(QtyShip,0)>0
				Group by ProductAutoId
				) AS dt
				ON dt.[ProductAutoId] = PM.[AutoId]

				UPDATE PM SET [Stock] = isnull([Stock],0) + isnull(TotalPieces,0)
				FROM [dbo].[ProductMaster] AS PM                                                                                                   
				INNER JOIN (
				SELECT sum(TotalPieces) as TotalPieces,ProductAutoId FROM OrderItemMaster
				where OrderAutoId=@OrderAutoId AND isnull(QtyShip,0)>0
				Group by ProductAutoId) AS dt 
				ON dt.[ProductAutoId] = PM.[AutoId]	       

				SET @LogRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=25), '@OldStatus', @OldStatus) 
				SET @LogRemark = REPLACE(@LogRemark, '@NewStatus', 'Processed')  
				SET @LogRemark = REPLACE(@LogRemark, '@Emp', (Select FirstName+' '+LastName from Employeemaster where autoid=@EmpAutoId))                                                                                                                                                                                                            
				INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                                                                                      
				VALUES(25,@EmpAutoId,getdate(),@LogRemark,@OrderAutoId)  
                                        
				UPDATE [OrderMaster] SET Status=2,UPDATEDATE=GETDATE(),UPDATEDBY=@EmpAutoId ,
				PackedBoxes=NULL,AddonPackedQty=NULL
				WHERE AutoId=@OrderAutoId
				--For Po Order ---
				SELECT @CustomerAutoId=CustomerAutoId FROM [dbo].[OrderMaster]  WHERE [AutoId] = @OrderAutoId 
				set @CustomerAutoId=(select @CustomerAutoId from  [OrderMaster] WHERE AutoId=@OrderAutoId)              
				if((select CustomerType from CustomerMaster where AutoId=@CustomerAutoId)=3)        
				begin        
					exec [dbo].[ProcUpdatePOStatus_All]        
					@CustomerId=@CustomerAutoId,        
					@OrderAutoId=@OrderAutoId,        
					@Status=2            
				end 
				
		end
		ELSE
		BEGIN
			SET @isException=1                                      
			SET @exceptionMessage='You can changed status on '+@OldStatus+ ' Status.'
		END

		----End-----
		  
  END                                      
  ELSE IF @Opcode = 22                                      
 BEGIN                                       
  BEGIN TRY    
  BEGIN TRAN 
     IF EXISTS(SELECT AutoId FROM EmployeeMaster Where AutoId=@EmpAutoId AND EmpType=8)
	 BEGIN
        IF EXISTS(SELECT AutoId FROM EmployeeMaster Where AutoId=@DriverAutoId AND EmpType In(2,5))
		BEGIN
			SET @CustomerAutoId=(SELECT CUSTOMERAUTOID FROM OrderMaster WHERE AUTOID=@OrderAutoId)                            
			SET @customerType =(select CustomerType from CustomerMaster where AutoId=@CustomerAutoId)     
			SET @CurrentStatus = (SELECT Status FROM OrderMaster WHERE AutoId=@OrderAutoId)
			if( (select COUNT(1) from OrderItemMaster as oim                             
			inner join PackingDetails as pd on oim.ProductAutoId=pd.ProductAutoId and oim.UnitTypeAutoId=pd.UnitType                            
			where OrderAutoId=@OrderAutoId and isFreeItem=0 and IsExchange=0                            
			and oim.UnitPrice<OM_MinPrice)=0)                            
			BEGIN                            
					UPDATE [dbo].[OrderMaster] SET [Driver] = @DriverAutoId,[AssignDate]=ISNULL(convert(date,@AsgnDate),GETDATE()),[Status]=4,[Stoppage] = NULL,                                      
					ManagerAutoId=@EmpAutoId WHERE [AutoId] = @OrderAutoId                                      
					INSERT INTO [dbo].[DrvLog] ([DrvAutoId],[OrderAutoId],[AssignDate]) 
					VALUES(@DriverAutoId,@OrderAutoId,Convert(date,getdate()))                                               
                    
					
					IF(@CurrentStatus = 3)
					BEGIN
					SET @LogRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=3),'[Status1]','Packed')   
					                                   
					SET @LogRemark = REPLACE(@LogRemark,'[Status2]','Ready to ship')  
						INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                      
					VALUES(3,@EmpAutoId,getdate(),@LogRemark,@OrderAutoId)
					END
					 IF(@CurrentStatus = 10)
					BEGIN
					SET @LogRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=3),'[Status1]','Add On Packed')   
					                                   
					SET @LogRemark = REPLACE(@LogRemark,'[Status2]','Ready to ship')  
						INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                      
					VALUES(3,@EmpAutoId,getdate(),@LogRemark,@OrderAutoId)
					END
					                                    
				                                      
                                      
					SET @LogRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=5), '[DriverName]', (SELECT [FirstName] + ' ' + [LastName] AS DrvName FROM [dbo].[EmployeeMaster] WHERE [AutoId] = @DriverAutoId))                             
  
					INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                      
					VALUES(5,@EmpAutoId,getdate(),@LogRemark,@OrderAutoId)                               
                              
					SELECT EM.[FirstName] + ' ' + Em.[LastName] AS Name,CONVERT(VARCHAR(20),OM.[AssignDate],101) AS AssignDate,SM.[StatusType],OM.[Status]                                        
					FROM [dbo].[OrderMaster] AS OM                                       
					INNER JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[Driver]                                        
					INNER JOIN [dbo].[StatusMaster] AS SM ON SM.[AutoId] = OM.[Status] and SM.Category='OrderMaster' WHERE OM.[AutoId] = @OrderAutoId                           
					if((select CustomerType from CustomerMaster where AutoId=@CustomerAutoId)=3)        
					begin        
					exec [dbo].[ProcUpdatePOStatus_All]        
					@CustomerId=@CustomerAutoId,        
					@OrderAutoId=@OrderAutoId,        
					@Status=4            
					end  
 
					Select @OrderAutoId as AutoId      
					Select 'Driver has been assigned' as msg       
					EXEC [dbo].[ProcAutomaticSendEmail]  @Opcode=13,@CustomerAutoId=@CustomerAutoId, 
					@OrderAutoId=@OrderAutoId,@isException=0,@exceptionMessage=''
			END                            
			ELSE                            
			BEGIN                            
				Set @isException=1                                      
				Set @exceptionMessage='Some Product have unit price less than min Price.Please fix it and try again.'                            
			END 
		END
		ELSE
		BEGIN
			Set @isException=1                                      
			Set @exceptionMessage='Unauthorized Access.'
		END
		END
		ELSE
		BEGIN
			Set @isException=1                                      
			Set @exceptionMessage='Unauthorized Access.'
		END
  COMMIT TRAN                                                       
  END TRY                                      
  BEGIN CATCH   
  ROLLBACK TRAN                                        
   Set @isException=1                                      
   Set @exceptionMessage='Oops,some thing went wrong .Please try again.'                            
  END CATCH                                       
 END                                      
  ELSE IF @Opcode=23                                      
   BEGIN                                    
				declare @CreditAmount decimal(10,2), @DeductAmount decimal(10,2) , @GrandTotal decimal(10,2)                               
                                
				SET @CreditAmount=ISNULL((SELECT CreditAmount FROM OrderMaster WHERE AutoId=@OrderAutoId),0)                                                                   
				SET @CustomerAutoId=(SELECT CustomerAutoId FROM OrderMaster WHERE AutoId=@OrderAutoId)                               
                SET @SalesPersonAutoId=(SELECT SalesPersonAutoId FROM OrderMaster WHERE AutoId=@OrderAutoId)      
				IF(ISNULL((SELECT CreditAmount FROM OrderMaster WHERE AutoId=@OrderAutoId),0)!=0)                                                                
				BEGIN                                                                    
				UPDATE CustomerCreditMaster SET CreditAmount=ISNULL(CreditAmount,0)+@CreditAmount WHERE CustomerAutoId=@CustomerAutoId                                                                    
				END                               
                                                                     
				SET @DeductAmount =ISNULL((SELECT Deductionamount FROM OrderMaster WHERE AutoId=@OrderAutoId),0)                                   
                                                               
				SET @GrandTotal=ISNULL((SELECT GrandTotal FROM OrderMaster WHERE AutoId=@OrderAutoId),0)                                                                   
                              
				if(@DeductAmount>@GrandTotal )                                                                  
				BEGIN                                                                  
				UPDATE CustomerCreditMaster SET CreditAmount=ISNULL(CreditAmount,0)-ISNULL((@DeductAmount-@GrandTotal),0) WHERE CustomerAutoId=@CustomerAutoId                                                                  
				END                                
                                                                  
				DELETE from tbl_Custumor_StoreCreditLog where ReferenceNo=CONVERT(varchar(50),@OrderAutoId) and ReferenceType='OrderMaster' 
				
				IF NOT EXISTS(SELECT AutoId FROM OrderMaster WHERE AutoId=@orderAutoId AND Status IN (1,2,8))  
		        BEGIN 
					insert into [dbo].[ProductStockUpdateLog]([ProductId],[OledStock],[NewStock],[UserAutoId],[DateTime],ReferenceId,ActionRemark,ReferenceType)
					SELECT pm.ProductId,isnull([Stock],0),(isnull(pm.Stock,0)+(isnull(TotalPieces,0))),@EmpAutoId,GETDATE(),@OrderAutoId,
					'Order No-(' + (select OrderNo from OrderMaster as om where om.AutoId=@OrderAutoId) + ') has cancelled by '+(Select FirstName+' '+LastName from Employeemaster where autoid=@EmpAutoId)+'.','OrderMaster'
					FROM [dbo].[ProductMaster] AS PM                                                                                                  
					INNER JOIN (
					SELECT sum(TotalPieces) as TotalPieces,ProductAutoId FROM OrderItemMaster
					where OrderAutoId=@OrderAutoId AND isnull(QtyShip,0)>0
					Group by ProductAutoId
					) AS dt
					ON dt.[ProductAutoId] = PM.[AutoId]

					UPDATE PM SET [Stock] = isnull([Stock],0) +isnull(TotalPieces,0) FROM [dbo].[ProductMaster] AS PM                                                                                                   
					INNER JOIN (SELECT sum(TotalPieces) as TotalPieces,ProductAutoId FROM OrderItemMaster 
					where OrderAutoId=@OrderAutoId group by ProductAutoId) AS dt 
					ON dt.[ProductAutoId] = PM.[AutoId]
					
				End    
				
				IF EXISTS(SELECT AutoId FROM OrderMaster WHERE AutoId=@orderAutoId AND Status=9)  
		        BEGIN 
					insert into [dbo].[ProductStockUpdateLog]([ProductId],[OledStock],[NewStock],[UserAutoId],[DateTime],ReferenceId,ActionRemark,ReferenceType)
					SELECT pm.ProductId,isnull([Stock],0),(isnull(pm.Stock,0)+(isnull(AddOnPackedPeice,0))),@EmpAutoId,GETDATE(),@OrderAutoId,
					'Order No-(' + (select OrderNo from OrderMaster as om where om.AutoId=@OrderAutoId) + ') has cancelled by '+(Select FirstName+' '+LastName from Employeemaster where autoid=@EmpAutoId)+'.','OrderMaster'
					FROM [dbo].[ProductMaster] AS PM                                                                                                  
					INNER JOIN (
					SELECT sum(TotalPieces) as TotalPieces,ProductAutoId,sum(AddOnPackedPeice) as AddOnPackedPeice FROM OrderItemMaster
					where OrderAutoId=@OrderAutoId AND isnull(QtyShip,0)>0
					Group by ProductAutoId
					) AS dt
					ON dt.[ProductAutoId] = PM.[AutoId]

					

					UPDATE PM SET [Stock] = isnull([Stock],0) + isnull(AddOnPackedPeice,0)
					
					FROM [dbo].[ProductMaster] AS PM                                                                                                   
					INNER JOIN (SELECT sum(TotalPieces) as TotalPieces,ProductAutoId,sum(AddOnPackedPeice) as AddOnPackedPeice FROM OrderItemMaster 
					where OrderAutoId=@OrderAutoId group by ProductAutoId) AS dt 
					ON dt.[ProductAutoId] = PM.[AutoId]
					
				

				End     
				
                                      
				UPDATE [dbo].[OrderMaster] SET [Status]=8,ManagerRemarks=@Remarks,ManagerAutoId=@EmpAutoId,UPDATEDATE=GETDATE(),UPDATEDBY=@EmpAutoId,
				CancelledBy=@EmpAutoId,CancelledDate=GETDATE(),CancelRemark=@Remarks                               
				WHERE [AutoId]=@OrderAutoId   
				                 
				INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId],ActionIPAddress)                                      
				VALUES(7,@EmpAutoId,getdate(),'Cancelled',@OrderAutoId,@IPAddress)   
                                 
                                      
				UPDATE CreditMemoMaster set OrderAutoId=null where OrderAutoId=@OrderAutoId                              
                              
				SELECT OM.[Status],SM.[StatusType] FROM [dbo].[OrderMaster] AS OM                                      
				INNER JOIN [dbo].[StatusMaster] As SM ON SM.[AutoId] = OM.[Status] WHERE OM.[AutoId]=@OrderAutoId
                
				if((select CustomerType from CustomerMaster where AutoId=@CustomerAutoId)=3)        
				begin        
				exec [dbo].[ProcUpdatePOStatus_All]        
				@CustomerId=@CustomerAutoId,        
				@OrderAutoId=@OrderAutoId,        
				@Status=8            
				end    				
				
			 

				IF EXISTS(Select * from AllocatedPackingDetails where OrderAutId=@OrderAutoId)
				BEGIN
					 DELETE FROM AllocatedPackingDetails where OrderAutId=@OrderAutoId
				END
   END       
     
  ELSE IF @Opcode=25                                      
  BEGIN                                      
	SELECT [OrderNo],PayableAmount,Convert(varchar(30),Convert(date,[OrderDate]),101) As OrderDate,cm.CustomerName,Email FROm OrderMaster as om  
	inner join CustomerMaster as cm on cm.AutoId=om.CustomerAutoId  
	WHERE om.[AutoId] = @OrderAutoId     
	select PrintLabel as PrintLabel from MLTaxMaster where TaxState in (select State from  BillingAddress where AutoId in (select BillAddrAutoId from OrderMaster where AutoId=@orderAutoId))       
  END    
                                    
   ELSE IF @Opcode=24                                  
   BEGIN                                  
  IF EXISTS(SELECT [SecurityValue] FROM [dbo].[TBL_MST_SecurityMaster] WHERE [SecurityValue]=@CheckSecurity                                   
   and SecurityType=8 and typeEvent=1)                                  
   BEGIN                                  
                                   
		SET @isException=0
		SET @exceptionMessage='Key match'
                                       
   END                                  
   ELSE                                  
   BEGIN                                  
    Set @isException=1                                      
    Set @exceptionMessage='Invalid Security Key'                                  
   END                                  
   END                                  
  ELSE IF @Opcode=26                                      
  BEGIN                                      
	  select AutoId,FirstName+' '+LastName as Driver from [dbo].[EmployeeMaster] where EmpType=5 and Status=1  
	  order by Driver     
  END
  ELSE IF @Opcode=27
  BEGIN
        Declare @Status int
        UPDATE OrderMaster SET isMLManualyApply=case when isMLManualyApply=1 then 0 else 1 end WHERE AutoId=@OrderAutoId  

		SET @Status=(Select isMLManualyApply FROM OrderMaster WHERE AutoId=@OrderAutoId  )

		Update OrderMaster SET MLTaxRemark=@MLTaxRemark Where [AutoId] = @OrderAutoId

		SET @LogRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=(case when 
		@Status=0 then 19 else 31 end)),'[OrderNo]',(SELECT [OrderNo] FROM OrderMaster where [AutoId] = @OrderAutoId)) 
		
		INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                                
		VALUES(case when @Status=0 then 19 else 31 end,@EmpAutoId,getdate(),@LogRemark,@OrderAutoId)  
  END
  ELSE IF @Opcode=28
  BEGIN
        SET @StateCode=(Select StateCode FROM State where trim(StateName)=trim(@OrdState))

        UPDATE OrderMaster SET OrdShippingAddress=[dbo].[ConvertFirstLetterinCapital](@TypeShipping)+',<br/>'+Upper(@StateCode)+' - '+@OrdZipcode,SA_Lat=@OrdLat,SA_Long=@OrdLong,SA_City=@OrdCity,SA_State=Upper(@StateCode),
		SA_Zipcode=@OrdZipcode,SA_Address=@OrdAddress,SA_Address2=@ShippingAddress2 WHERE AutoId=@OrderAutoId  
		
		INSERT INTO tbl_OrderLog (ActionTaken,EmpAutoId,ActionDate,Remarks,OrderAutoId) VALUES (43,@EmpAutoId,Getdate(),@TypeShipping,@OrderAutoId)
  END
  ELSE IF @Opcode=29
  BEGIN
	select OrderNo,CM.CustomerName,OrdShippingAddress,(SA.[Address]+', '+SA.[City]+', '+S1.[StateName]+', '+SA.[Zipcode]) As shipAdd, 
	(case when OM.SA_Lat is NULL  then SA.SA_Lat else OM.SA_Lat  end) as SA_Lat  ,
	(case when OM.SA_Long is NULL  then SA.SA_Long else OM.SA_Long  end) as SA_Long ,
	(case when OM.SA_City is NULL  then SA.City else OM.SA_City  end) as SA_City,
	(case when OM.SA_Zipcode is NULL  then SA.Zipcode else OM.SA_Zipcode  end) as SA_Zipcode,
	(case when OM.SA_State is NULL  then S1.StateName else OM.SA_State  end) as SA_State,
	(case when OM.SA_Address is NULL  then SA.Address else OM.SA_Address end) as SA_Address,
	(case when OM.SA_Address2  is NULL  then SA.Address2 else OM.SA_Address2  end) as SA_Address2 
	from OrderMaster as OM
	INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = OM.[ShipAddrAutoId]     
	INNER JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State] 
	inner join CustomerMaster as CM on CM.AutoId=OM.CustomerAutoId
	WHERE OM.AutoId=@OrderAutoId  
  END
  ELSE IF @Opcode=30
  BEGIN
       select AutoId,EmpId,[FirstName] + ' ' + [LastName] As SalesPerson from EmployeeMaster where EmpType=2  and status=1
	   order by SalesPerson
  END
  ELSE IF @Opcode=31
  BEGIN
    
	UPDATE [dbo].[OrderMaster] SET [SalesPersonAutoId] = @SalesPersonAutoId WHERE [AutoId] = @OrderAutoId                                      
                                   
    INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                      
    VALUES(45,@EmpAutoId,getdate(),@Remarks,@OrderAutoId)    
  END
  ELSE IF @Opcode=32
  BEGIN
		UPDATE OrderMaster SET DriverRemarks=@Remarks WHERE AutoId=@OrderAutoId 
		INSERT INTO tbl_OrderLog (ActionTaken,EmpAutoId,ActionDate,Remarks,OrderAutoId) VALUES (47,@EmpAutoId,Getdate(),@Remarks,@OrderAutoId)
  END
   ELSE IF @Opcode=33
  BEGIN
    set @OldStatus=(SELECT sm.StatusType FROM OrderMaster as om inner join StatusMaster as sm on sm.AutoId=om.Status and SM.Category = 'OrderMaster' WHERE om.[AutoId] = @OrderAutoId)
    SET @LogRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=3),'[Status1]',@OldStatus)   
  
    SET @LogRemark = REPLACE(@LogRemark,'[Status2]','Packed')                                      
    INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                      
    VALUES(3,@EmpAutoId,getdate(),@LogRemark+' ('+ @Remarks +')' ,@OrderAutoId)                             
    
	DELETE from DriverLog_OrdDetails where orderno=(select top 1 orderno from OrderMaster as om WHERE om.AutoId=@OrderAutoId)

	UPDATE OrderMaster SET Status=@OrderStatus WHERE AutoId=@OrderAutoId 

  END
  ELSE IF @Opcode=34
  BEGIN
		SELECT DriverRemarks FROm OrderMaster WHERE AutoId=@OrderAutoId 
  END
  END TRY                                      
  BEGIN CATCH                                      
    Set @isException=1                                      
    Set @exceptionMessage='Opps Some thing went wrong.Please try later'--ERROR_MESSAGE()                                
  END CATCH                         
END   