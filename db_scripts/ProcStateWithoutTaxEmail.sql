 Alter procedure ProcStateWithoutTaxEmail --Exec ProcStateWithoutTaxEmail
 AS                 
 BEGIN
	--declare  @DriveLocation varchar(250)
	--SET @DriveLocation='D:\DraftProduct\'+UPPER((select CompanyId from dbo.CompanyDetails))+'_Created'+
	--convert(varchar(50),Format(GetDate(),'MM_dd_yyyy_hh_mm_tt'))+'.html'

	Declare @html varchar(Max)='',@Location varchar(50),@tr varchar(max),@td varchar(max)='',@num int,@query nvarchar(MAX),@count int,
	@StateName varchar(50),@StateCode varchar(15),@Country varchar(150),@Status varchar(20)
	

	select Row_Number() over (order by S.AutoId) as rownumber,S.StateName,S.StateCode,C.Country,
	Case when S.Status=1 then 'Active' else 'Inactive' end as Status into #Result  from State AS S
	INNER JOIN Country AS C ON S.CountryId=C.AutoId
	where S.AutoId NOT IN (Select TaxState FROM MLTaxMaster)

	set @html='
	    <table class="table tableCSS" border="1" style="width:100%;border: 1px solid black;border-collapse:collapse;">
		<thead>
			<tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
				<td style="text-align: center !important;">
					<h4 style="margin:10px;">State Without Tax</h4>
				</td>
				</tr>
				<tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
				<td style="text-align: center !important;">
					<h4 style="margin:2px">'+(select CompanyDistributerName from CompanyDetails)+'</h4>
				</td>
				</tr>
				<tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
				<td style="text-align: center !important;">
					<h4 style="margin:2px">'+Convert(varchar(50),FORMAT(GetDate(),'MM/dd/yyyy hh:mm tt'))+'</h4>
				</td>
			</tr>
		</thead>
		<tbody></tbody>
		</table>
		<br>
		<table class="table tableCSS" border="1" style="width:100%;border: 1px solid black;border-collapse:collapse;"> 
		<thead>
		<tr>
			<td style=''text-align:center;''><b>State Name</b></td>
			<td style=''text-align:center;''><b>State Code</b></td>
			<td style=''text-align:center;''><b>Country</b></td>
			<td style=''text-align:center;''><b>Status</b></td>
       </tr>
	   </thead>
		<tbody>'
		SET @count= (select count(1) from #Result)
		SET @num=1
		while(@num<=@Count)
		BEGIN 
				select 
				@StateName=StateName,
				@StateCode=StateCode,
				@Country= Country,
				@Status=Status
				from #Result where rownumber = @num 
				set @tr ='<tr>' 
								+	'<td style=''text-align:left;''>'+ @StateName + '</td>'
								+	'<td style=''text-align:center;''>'+ @StateCode + '</td>'
								+	'<td style=''text-align:left;''>'+ @Country + '</td>'
								+	'<td style=''text-align:center;''>'+ @Status + '</td>'

						+ '</tr>'
		set @num = @num + 1
		SET @html=@html+@tr
		END
		SET @html=@html+'</tbody></table>'
	--EXEC dbo.spWriteToFile @DriveLocation, @html 

	-----------------------------------------Code For Email---------------------------------------------------------------
	Declare  @Subject varchar(250),@FromName varchar(500),@FromEmailId varchar(500),@Port int,@SMTPServer varchar(150),
	@Password varchar(50),@SSL  int,@BCCEmailId varchar(max),@ToEmailId varchar(500),@CCEmailId varchar(max)
	set @Subject = 'A1WHM -' +UPPER((select CompanyDistributerName from CompanyDetails ))+' -  State Without Tax' 
	SELECT @FromEmailId=EmailId,@Port=port,@SMTPServer=server,@Password=convert(varchar(50), DecryptByPassPhrase('ESS',Pass)),
	@SSL=ssl from EmailServerMaster 
	where  SendTo='Developer' 
	set @BCCEmailId=(select BCCEmail from Tbl_ReportEmailReceiver where Autoid=15)
	set @ToEmailId=(select ToEmail from Tbl_ReportEmailReceiver where Autoid=15)
	SET @CCEmailId=(select CCEmail from Tbl_ReportEmailReceiver where Autoid=15)
    
	if(select COUNT(1) from #Result)>0
	BEGIN
			EXEC [Emailing_DB].[dbo].[ProcSubmitEmail] 
			@Opcode=11,
			@FromEmailId =@FromEmailId,
			@FromName = @FromName,
			@smtp_userName=@FromEmailId,
			@Password = @Password,
			@SMTPServer = @SMTPServer,
			@Port =@Port,
			@SSL =@SSL,
			@ToEmailId =@ToEmailId,
			@CCEmailId =@CCEmailId,
			@BCCEmailId =@BCCEmailId,  
			@Subject =@Subject,
			@EmailBody = @html,
			@SentDate ='',
			@Status =0,
			@SourceApp ='PSM',
			@SubUrl ='State Without Tax', 
			@isException=0,
			@exceptionMessage=''  
	END
  END