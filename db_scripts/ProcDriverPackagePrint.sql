CREATE OR
ALTER PROCEDURE [dbo].[ProcDriverPackagePrint]                                                                                                                                                    
 @Opcode INT=NULL,    
 @BulkOrderAutoId varchar(max)=null,  
 @status int=null,
 @OrderAutoId INT=NULL,                                                                                                                       
 @OrderNo VARCHAR(15)=NULL,                                                    
 @DriverAutoId INT=NULL,  
 @LoginEmpType INT=NULL,                            
 @EmpAutoId INT=NULL,  
 @CreditAutoId INT=NULL, 
 @AsgnDate DATE=NULL,        
 @PageIndex INT = 1,                
 @PageSize INT = 10,                                                                                                                            
 @RecordCount INT =null,                                                                                                                       
 @isException bit out,                                           
 @exceptionMessage varchar(max) out          
 
AS                                                                                                                                                    
BEGIN                                                                                                                                    
  BEGIN TRY                                                                                                                                            
   Set @isException=0                                                                                                                           
   Set @exceptionMessage='Success'                                                                                                         
   declare @customerType varchar(50)  ,@CustomerAutoId int                                                                                                                                               
                                                                                                                                        
 If @Opcode=101                                                                                                                          
   Begin                                                    
     SELECT [Address],[Website],[MobileNo],[FaxNo],TermsCondition,Logo FROM CompanyDetails                                            
     SELECT OM.[OrderNo],CONVERT(VARCHAR(20),OM.[OrderDate],101) As OrderDate,CONVERT(VARCHAR(20),OM.[AssignDate],101) as AssignDate ,
	 (emp.FirstName + ' '+ emp.LastName) as Driver,(SELECT emp.FirstName + ' '+ emp.LastName FROM EmployeeMaster as emp where 
	 emp.AutoId=OM.SalesPersonAutoId)as SalesPerson,       
     CM.[CustomerName]+
	 ' <br/> '+ISNULL((case when OM.DeliveredAddress is null then  +[dbo].[ConvertFirstLetterinCapital](sa.Address+', '+sa.City)+', '+(Select StateCode from State where AutoId=sa.State)+
	' - '+sa.Zipcode else OM.DeliveredAddress end+' <br/> '+
	case when ScheduleAt is null then '' else	'Schedule At: '+CONVERT(varchar(15),CAST(ScheduleAt AS TIME),100) end + case when DeliveryFromTime is null then '' else
	' ('+CONVERT(varchar(15),CAST(DeliveryFromTime AS TIME),100)+' - '+CONVERT(varchar(15),CAST(DeliveryToTime AS TIME),100)+')' end
	),'') as  [CustomerName],
	PayableAmount as [GrandTotal],[Stoppage],RootName,DriverRemarks,ScheduleAt,
	 om.DeliveryFromTime,DeliveryToTime FROM [dbo].[OrderMaster] AS OM                                                            
     INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                                                                                               
     INNER JOIN [dbo].[EmployeeMaster] AS emp ON emp.AutoId = OM.Driver                                            
     INNER JOIN [dbo].[StatusMaster] AS SM ON SM.[AutoId] = OM.[Status]  
	 INNER JOIN [dbo].[ShippingAddress] AS SA ON OM.ShipAddrAutoId=SA.AutoId
     AND SM.[Category] = 'OrderMaster'   
	 WHERE OM.AutoId IN (SELECT * FROM [dbo].[fnSplitString](@BulkOrderAutoId, ','))
	  and OM.Status in (4,5)                                                                                                                                                
     Order by (emp.FirstName + ' '+ emp.LastName), convert(int,[Stoppage]),OM.[OrderNo]                                                                                                                                                     
     select @DriverAutoId,@AsgnDate                                                                                                                                                        
   End                                                                                                                                                       
  IF @Opcode=102                        
 BEGIN        
   DECLARE @Location varchar(50)=Replace(DB_Name(),'.a1whm.com','')
	select(select CompanyName,EmailAddress, [Address],[Website],[MobileNo],[FaxNo],TermsCondition,              
	Logo,('http://'+DB_NAME()+'/img/logo/'+Logo) as LogoforEmail,        
	(SELECT OM.[AutoId],OM.[OrderNo],CONVERT(VARCHAR(20),OM.[OrderDate], 101) AS OrderDate,                                                                                                    
	CONVERT(VARCHAR(20), OM.[DeliveryDate],101) + ' ' + convert(varchar(10), getdate(), 108)                        
	AS DeliveryDate,                                  
	CM.[CustomerId],CM.[CustomerName],
	(case when Contact1=Contact2 then Contact1 when isnull(Contact2,'')!='' then CM.[Contact1] + '/ ' + CM.[Contact2] else Contact1 end) As Contact,
	CTy.CustomerType,CT.[TermsDesc],ST.[ShippingType],                                                                                                    
	[dbo].[ConvertFirstLetterinCapital](ba.Address+', '+BA.City+', '+S.StateName+' - '+BA.Zipcode) As BillAddr,S.[StateName] AS State1,BA.[City] AS City1,BA.[Zipcode] As Zipcode1,
	[dbo].[ConvertFirstLetterinCapital](SA.[Address]+', '+SA.City+', '+s1.StateName+' - '+SA.Zipcode) As ShipAddr,S1.[StateName] As State2,   
	SA.[City] AS City2,SA.[Zipcode] As Zipcode2,OM.[TotalAmount],OM.[OverallDisc],OM.[OverallDiscAmt],                                                                                                    
	OM.[ShippingCharges],OM.[TotalTax],OM.[GrandTotal],EM.[FirstName] + ' ' + EM.[LastName] As SalesPerson,                                                                                                    
	EM1.[FirstName] + ' ' + EM1.[LastName] As PackerName,                                                                      
	isnull(DeductionAmount,0.00) as DeductionAmount,                                                                                                    
	isnull(CreditAmount,0.00)as CreditAmount,isnull(PayableAmount,0.00) as PayableAmount                                                                                                  
	,ISNULL((select t.CreditAmount from CustomerCreditMaster as t  where t.CustomerAutoId=om.CustomerAutoid),0.00) as CustomerCredit                                                                                                    
	,OrderRemarks , [ScheduleAt] as Schedule,                                                                                             
	ISNULL(MLQty,0) as MLQty,isnull(MLTax,0) as MLTax,OM.Weigth_OZTaxAmount as WeightTax,isnull(CM.ContactPersonName,'') as ContactPersonName,ISNULL(AdjustmentAmt,0) as AdjustmentAmt,                                                                                                    
	-- CM.BusinessName,om.Status,CM.Email as ReceiverEmail,        
	isnull((
		SELECT CM.[CategoryName],CM.[CategoryId],DOI.[ProductAutoId],PM.[ProductId],PM.[ProductName],DOI.[UnitAutoId],UM.[UnitType],DOI.[QtyPerUnit],                                                                                                    
		DOI.[UnitPrice],DOI.[Barcode],DOI.[QtyShip],DOI.[QtyDel],DOI.[TotalPieces],DOI.[SRP],
		case when DOI.[GP] < 0 then '---' else Convert(varchar(15),DOI.[GP]) end as  [GP],  
		case when DOI.[GP] < 0 then '---' else convert(varchar(15),DOI.[GP]) end as PrintGP  
		,DOI.[Tax],DOI.[NetPrice]                                                                                                    
		,convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice,ISNULL(FreshReturnQty,0) as FreshReturnQty,FreshReturnUnitAutoId,                         
		ISNULL(DamageReturnQty,0) as DamageReturnQty,DamageReturnUnitAutoId,                                                                                                    
		IsExchange,isnull(TaxValue,0.00) as TaxValue,ISNULL(isFreeItem,0) AS isFreeItem,                                                                                                    
		ISNULL(UnitMLQty,0) as UnitMLQtys,ISNULL(TotalMLQty,0) as TotalMLQty,                                                                                                    
		DOI.Item_TotalMLTax as TotalMLTax,  Isnull(Del_discount,0.00)  as Del_discount                           
		FROM [dbo].[Delivered_Order_Items] AS DOI                                               
		INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = DOI.[ProductAutoId]                                                                                                    
		INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = DOI.UnitAutoId                                                                                                     
		INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId]                                                    
		INNER JOIN [dbo].[PackingDetails] AS PD ON PD.[ProductAutoId] = DOI.[ProductAutoId] AND PD.[UnitType] = DOI.UnitAutoId                                  
		WHERE [OrderAutoId] =OM.AutoId and OM.Status=11          
		ORDER BY PM.ProductName ASC,CM.[CategoryName] ASC  for json path, INCLUDE_NULL_VALUES        
	),'[]') as Item1,        
	isnull((
		SELECT CM.[CategoryName],CM.[CategoryId],OIM.ProductAutoId,PM.[ProductId],PM.[ProductName],PM.[ProductLocation],                                                        
		OIM.[UnitTypeAutoId] AS UnitAutoId,UM.[UnitType],OIM.[QtyPerUnit],OIM.[UnitPrice],OIM.[TotalPieces],OIM.[SRP],
		case when OIM.[GP] <= 0 then '---' else Convert(varchar(15),OIM.[GP]) end as [GP],OIM.[Tax],                                                                     
		OIM.[NetPrice],OIM.[Barcode],                        
                          
		isnull(OIM.[QtyShip],OIM.[RequiredQty]) as QtyShip                       
                          
		,OIM.[RequiredQty] ,                                                     
		convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice,                                                                                                    
		OIM.IsExchange,isnull(TaxValue,0.00) as TaxValue,ISNULL(isFreeItem,0) AS isFreeItem,UnitMLQty,                                                                                                    
		ISNULL(UnitMLQty,0) as UnitMLQtyf,ISNULL(TotalMLQty,0) as TotalMLQty,                                                                                                    
		oim.Item_TotalMLTax as TotalMLTax,ISNULL(Oim_Discount,0) as Del_discount                                                                                                  
		FROM [dbo].[OrderItemMaster] AS OIM                                                                                                     
		INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                                                                                                    
		INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitTypeAutoId                               
		INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId]                           
		INNER JOIN [dbo].[PackingDetails] AS PD ON PD.[ProductAutoId] = OIM.[ProductAutoId] AND PD.[UnitType] = OIM.UnitTypeAutoId                                                          
		WHERE [OrderAutoId] =OM.AutoId and OM.Status!=11
		and ((@Location in ('psmpa','psmnpa','psmwpa') and oim.TotalPieces>0) or @Location in ('psmnj','psmct','psm','psmny'))
		ORDER BY PM.ProductName ASC,CM.[CategoryName] ASC for json path, INCLUDE_NULL_VALUES        
	),'[]') as Item2,       
	 isnull((        
		SELECT OM.[AutoId],[OrderNo],CONVERT(VARCHAR(20),[OrderDate],101) AS OrderDate,DO.[AmtDue],DO.[GrandTotal],DO.[AmtPaid] ,                     
		otm.OrderType AS OrderType ,                                                                
		DATEDIFF(dd,OrderDate,GETDATE()) as Aging, orderdate as orderdateSort                                                                                                    
		FROM [dbo].[OrderMaster] As OM1                                                                                                  
		INNER JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM1.[AutoId]       
		 INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
		WHERE [CustomerAutoId] = OM.CustomerAutoId AND ([Status] = 11 ) AND isnull(do.[AmtDue],0) > 0.00                                                                                                    
		order by orderdateSort ASC for json path, INCLUDE_NULL_VALUES               
	 ),'[]') as DueOrderList,  
	   isnull((        
		  select CMM.[CreditNo] as OrderNo,CONVERT(VARCHAR(20), CMM.[CreditDate], 101) AS OrderDate,                                         
    CM.[AutoId] As CustAutoId,CM.[CustomerId],CM.[CustomerName],
	(case when Contact1=Contact2 then Contact1 when isnull(Contact2,'')!='' then CM.[Contact1] + '/ ' + CM.[Contact2] else Contact1 end) As Contact,
	CTy.CustomerType,CT.[TermsDesc],                                    
    [dbo].[ConvertFirstLetterinCapital](BA.[Address]) As BillAddr,                                    
    [dbo].[ConvertFirstLetterinCapital](S.[StateName]) AS State1,
	[dbo].[ConvertFirstLetterinCapital](BA.[City]) AS City1,
	[dbo].[ConvertFirstLetterinCapital](BA.[Zipcode]) As Zipcode1,
	[dbo].[ConvertFirstLetterinCapital](SA.[Address]) As ShipAddr,
	[dbo].[ConvertFirstLetterinCapital](S1.[StateName]) As State2,
	[dbo].[ConvertFirstLetterinCapital](SA.[City]) AS City2,                                    
    SA.[Zipcode] As Zipcode2,CMM.[TotalAmount],ISNULL(CMM.[OverallDisc],0) as OverallDisc1,ISNULL(CMM.[OverallDiscAmt],0) as OverallDisc,ISNULL(CMM.[TotalTax],0) as TotalTax,
	CMM.[GrandTotal] as GrandTotal,                              
  ISNULL((select top 1 PrintLabel from MLTaxMaster where TaxState=BA.State),'') as MLTaxPrintLabel,
		ISNULL((select top 1 PrintLabel from Tax_Weigth_OZ),'') as WeightTaxPrintLabel,
		ISNULL((select top 1 PrintLabel from [TaxTypeMaster] as tt  where tt.State=BA.State),'') as TaxPrintLabel,
    ISNULL(cmm.MLQty,0) as MLQty,isnull(cmm.MLTax,0) as MLTax,CM.ContactPersonName,ISNULL(cmm.AdjustmentAmt,0) as AdjustmentAmt,                            
    ISNULL(CMM.WeightTaxAmount,0) as WeightTaxAmount,  --  TaxTypeAutoId Added By Rizwan Ahmad on 11/09/2019 04:00 AM                                  
    CM.BusinessName,EM.[FirstName] + ' ' + EM.[LastName] As SalesPerson
		 ,isnull((
		SELECT CM.[CategoryName],CM.[CategoryId],OIM.ProductAutoId,PM.[ProductId],PM.[ProductName],PM.[ProductLocation],                                  
		OIM.[UnitAutoId] AS UnitAutoId,UM.[UnitType],OIM.[QtyPerUnit],convert(decimal(10,2),OIM.[UnitPrice]) as UnitPrice,                                    
		convert(decimal(10,2),OIM.NetAmount) AS  [NetPrice],OIM.AcceptedQty AS QtyShip,'' AS BarCode                                    
		FROM [dbo].[CreditItemMaster] AS OIM                                     
		INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                                    
		INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitAutoId                                     
		INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId]    
		WHERE OIM.CreditAutoId=CMM.CreditAutoId                                  
		ORDER BY CM.[CategoryName] ASC for json path, INCLUDE_NULL_VALUES
    ),'[]') as CreditItems      
		  from CreditMemoMaster AS CMM  
			INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = CMM.[CustomerAutoId]                                               
			INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]                                    
			INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[CustomerAutoId] = OM.[CustomerAutoId] AND BA.IsDefault=1                                    
			INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[CustomerAutoId] = OM.[CustomerAutoId] AND SA.IsDefault=1                                    
			LEFT JOIN [dbo].[CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                                           
			INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                                     
			INNER JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                                     
			left JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = CMM.CreatedBy  where CMM.OrderAutoId =OM.AutoId 
		  
		  for json path, INCLUDE_NULL_VALUES        
	   ),'[]') as CreditDetails,
		ISNULL((select top 1 PrintLabel from MLTaxMaster where TaxState=BA.State),'') as MLTaxPrintLabel ,
		ISNULL((select top 1 PrintLabel from Tax_Weigth_OZ),'') as WeightTaxPrintLabel,
		ISNULL((select top 1 PrintLabel from [TaxTypeMaster] as tt  where tt.State=BA.State),'') as TaxPrintLabel
		 FROM [dbo].[OrderMaster] As OM   --  WeightTax added by Rizwan Ahmad on 17-09-2019                                           
		 INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.Status AND SM.[Category] = 'OrderMaster'                                                                               
		 INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[AutoId] = OM.[BillAddrAutoId]                                                                                                    
		 INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = OM.ShipAddrAutoId                                                 
		 INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                                                                                                     
		 LEFT JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                                                                                         
		 INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                                                                                     
		 INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]                                                                                                    
		 LEFT JOIN [dbo].[CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                                                                                
		 LEFT JOIN [dbo].[ShippingType] AS ST ON ST.[AutoId] = OM.[ShippingType]                                                                                                     
		 left JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[SalesPersonAutoId]                                                                                                           
		 left JOIN [dbo].[EmployeeMaster] AS EM1 ON EM1.[AutoId] = OM.[PackerAutoId]                         
		  WHERE OM.AutoId in (SELECT * FROM [dbo].[fnSplitString](@BulkOrderAutoId, ',')) order by OM.Stoppage asc for json path, INCLUDE_NULL_VALUES) as OrderDetails        
		 from CompanyDetails for json path, INCLUDE_NULL_VALUES) as CompanyD    
 
   end
     else IF @Opcode=103                        
 BEGIN                                                                                 
     
	  SELECT @Status=Status FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId              
	  SELECT cm.CustomerId,cm.CustomerName,om.OrderNo,om.PayableAmount FROM [dbo].[OrderMaster] as om 
	  inner join CustomerMaster as cm on cm.AutoId=om.CustomerAutoId
	  where om.AutoId in (SELECT * FROM [dbo].[fnSplitString](@BulkOrderAutoId, ','))
	  IF @status=11                                                                                                     
	  BEGIN                                                                                 
		   SELECT CM.[CategoryName],CM.[CategoryId],DOI.[ProductAutoId],PM.[ProductId],PM.[ProductName],DOI.[UnitAutoId],UM.[UnitType],DOI.[QtyPerUnit],                                                                                                       
		   DOI.[Tax],DOI.[NetPrice]  ,[UnitPrice] ,[QtyShip]                                                                                                  
		   ,convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice,isnull(Del_discount,0.00) as Del_discount                                 
		   FROM [dbo].[Delivered_Order_Items] AS DOI                                                 
		   INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = DOI.[ProductAutoId]                                                         
		   INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = DOI.UnitAutoId                                                             
		   INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId]                             
		   INNER JOIN [dbo].[PackingDetails] AS PD ON PD.[ProductAutoId] = DOI.[ProductAutoId] AND PD.[UnitType] = DOI.UnitAutoId                                    
		   WHERE [OrderAutoId] in (SELECT * FROM [dbo].[fnSplitString](@BulkOrderAutoId, ',')) ORDER BY PM.[ProductId] ASC                                                    
	  END                                                                           
	  ELSE                                                                                                      
	  BEGIN    
		
			SELECT CM.[CategoryName],CM.[CategoryId],OIM.ProductAutoId,PM.[ProductId],PM.[ProductName],PM.[ProductLocation],                                                          
			OIM.[UnitTypeAutoId] AS UnitAutoId,UM.[UnitType],OIM.[QtyPerUnit],OIM.[UnitPrice],OIM.[TotalPieces],                                                                     
			OIM.[NetPrice],
		    CASE WHEN @Status>2 THEN OIM.[QtyShip] ELSE OIM.RequiredQty END AS [QtyShip],OIM.[RequiredQty] ,                                                       
			convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice,isnull(Oim_Discount,0.00) as Del_discount                                                                                                                                                                                                       
		                                                                                                                                                   
			FROM [dbo].[OrderItemMaster] AS OIM                                                                                                       
			INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                                                                                                      
			INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitTypeAutoId                                 
			INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId]                             
			INNER JOIN [dbo].[PackingDetails] AS PD ON PD.[ProductAutoId] = OIM.[ProductAutoId] AND PD.[UnitType] = OIM.UnitTypeAutoId                                                            
			WHERE [OrderAutoId] in (SELECT * FROM [dbo].[fnSplitString](@BulkOrderAutoId, ','))                                                                       
			ORDER BY PM.[ProductId] ASC         
	  END      
  END 
 else IF @Opcode=104                        
 BEGIN   
    select
   (
   select (
    ISNULL(( select Stoppage as StopNo,AutoId from OrderMaster where AutoId in (SELECT * FROM [dbo].[fnSplitString](@BulkOrderAutoId, ',')) order by convert(int,Stoppage) asc
   for json path, INCLUDE_NULL_VALUES),'[]')
   ) as Orderlist
 
   for json path, INCLUDE_NULL_VALUES
   )as test 
 end
 ELSE IF @Opcode=105                       
	BEGIN
		SELECT CompanyName,EmailAddress, [Address],[Website],[MobileNo],[FaxNo],TermsCondition,Logo FROM [psmpa.a1whm.com].[dbo].CompanyDetails                                                                                                                                              
		SELECT @Status=Status from OrderMaster WHERE AutoId=@OrderAutoId
        SELECT OM.[AutoId],OM.[OrderNo],CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,                                                                                                                                    
		ISNULL(CONVERT(VARCHAR(20), OM.[DeliveryDate],101),CONVERT(VARCHAR(20), OM.[DelDate],101) + ' ' +convert(varchar(10), OM.[DelDate], 108)) AS DeliveryDate,  CM.[AutoId] As CustAutoId,CM.[CustomerId]              
		,CM.[CustomerName],
		(case when Contact1=Contact2 then Contact1 when isnull(Contact2,'')!='' then CM.[Contact1] + '/ ' + CM.[Contact2] else Contact1 end) As Contact,
		CTy.CustomerType,CT.[TermsDesc],ST.[ShippingType],                                                                                                                                         
    
    	[dbo].[ConvertFirstLetterinCapital](BA.[Address]+(case when isnull(BA.Address2,'')!='' then ', '+BA.Address2 else '' end)+', '+BA.City)+', '+UPper(S.StateCode)+' - '+BA.Zipcode As BillAddr,
		S.[StateName] AS State1,ba.State as [stateid],BA.[City] AS City1,BA.[Zipcode] As Zipcode1,
		
		[dbo].[ConvertFirstLetterinCapital](SA.[Address]+(case when isnull(SA.Address2,'')!='' then ', '+SA.Address2 else '' end)+', '+SA.City)+', '+S1.StateCode+' - '+SA.Zipcode As ShipAddr,
		S1.[StateName] As State2,SA.[City] AS City2,                                              
		SA.[Zipcode] As Zipcode2,OM.[Status] As StatusCode,SM.[StatusType] AS Status,OM.[PackedBoxes],om.[TotalAmount],om.[OverallDisc],om.[OverallDiscAmt],                                                                                        
		om.[ShippingCharges],om.[TotalTax],om.[GrandTotal],om.paidAmount,om.[AmtDue],OM.[PaymentRecev],OM.[RecevAmt],OM.[AmtValue],OM.[ValueChanged],                                                                                                                 
 
		OM.[DiffAmt],OM.[NewTotal],OM.[PayThru],OM.CheckNo,OM.[DrvRemarks],OM.[AcctRemarks],EM.[FirstName] + ' ' + EM.[LastName] As SalesPerson,                                                                          
		EM1.[FirstName] + ' ' + EM1.[LastName] As PackerName,[CommentType],[Comment],isnull(DO.CreditMemoAmount,om.DeductionAmount) as DeductionAmount,                                                                                                                                
 
		ISNULL(om.CreditAmount,om.CreditAmount)as CreditAmount,isnull(om.PayableAmount,OM.PayableAmount) as PayableAmount,                                                                           
		ISNULL((select t.CreditAmount from CustomerCreditMaster as t  where t.CustomerAutoId=om.CustomerAutoid),0.00) as CustomerCredit                                                                                                                              
		,ISNULL((select top 1 PrintLabel from MLTaxMaster as mlt where mlt.TaxState=BA.State),'') as MLTaxPrintLabel
		,om.TaxType,otm.OrderType AS OrderType,OrderRemarks,                                                                                                                                                    
		ISNULL(OM.MLQty,0) as MLQty,isnull(om.MLTax,om.MLTax) as MLTax,CM.ContactPersonName,ISNULL(om.AdjustmentAmt,0) as AdjustmentAmt, 
		TTM.PrintLabel AS TaxPrintLabel,(SELECT top 1 PrintLabel from [dbo].[Tax_Weigth_OZ]) AS WeightTaxPrintLabel,
		CM.BusinessName,ISNULL(UnitMLTax,0) as UnitMLTax,om.Weigth_OZTaxAmount as  WeightTax,OM.TaxValue,om.Status as stautoid  FROM [dbo].[OrderMaster] As OM                                                                                                                        
		INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.Status AND SM.[Category] = 'OrderMaster'                                                                                                                                              
		INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[AutoId] = OM.[BillAddrAutoId]                                                                 
		INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = OM.[ShipAddrAutoId]                           
		INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                                                                                                                                                     
		INNER JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                                                                                                                                     
		INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                                                                                                                                               
		INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]                                                                                             
		LEFT JOIN [dbo].[CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                                                                                                                    
		LEFT JOIN [dbo].[ShippingType] AS ST ON ST.[AutoId] = OM.[ShippingType]                                                                                                                        
		LEFT JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[SalesPersonAutoId]                                                                              
		LEFT JOIN [dbo].[EmployeeMaster] AS EM1 ON EM1.[AutoId] = OM.[PackerAutoId]      
		LEFT JOIN [dbo].[TaxTypeMaster] AS TTM ON TTM.[AutoId] = OM.[TaxType]  
		left JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId] 
		INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
		WHERE OM.AutoId = @OrderAutoId                                                                                                                                                 
        
		IF EXISTS(SELECT [AutoId] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId AND Status=11) 
		BEGIN                                                                                                                         
			SELECT CM.[CategoryName],CM.[CategoryId],DOI.[ProductAutoId],PM.[ProductId],PM.[ProductName],DOI.[UnitAutoId],UM.[UnitType],DOI.[QtyPerUnit],
			DOI.[UnitPrice],DOI.[Barcode],DOI.[QtyShip],DOI.[QtyDel],DOI.[TotalPieces],DOI.[SRP],DOI.[GP],  
			case when DOI.[GP] < 0 then '---' else Convert(varchar(15),DOI.[GP]) end as PrintGP  
			,DOI.[Tax],DOI.[NetPrice]                                                                                                                                         
			,convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice,ISNULL(FreshReturnQty,0) as FreshReturnQty,FreshReturnUnitAutoId,                                                                                           
			ISNULL(DamageReturnQty,0) as DamageReturnQty,DamageReturnUnitAutoId,                                                                                         
			IsExchange,isnull(TaxValue,0.00) as TaxValue,ISNULL(isFreeItem,0) AS isFreeItem,                                                                   
			ISNULL(UnitMLQty,0) as UnitMLQty,ISNULL(TotalMLQty,0) as TotalMLQty,                                                                                                                                                    
			DOI.Item_TotalMLTax as TotalMLTax,Isnull(Del_discount,0.00) as Del_discount              
			FROM [dbo].[Delivered_Order_Items] AS DOI                                                                                                                                      
			INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = DOI.[ProductAutoId]                                                                                      
			INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = DOI.UnitAutoId                                    
			INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId] WHERE [OrderAutoId]=@OrderAutoId                                                              
			AND QtyShip>0                                                             
			ORDER BY CM.[CategoryName] ASC                          
		END                                                          
		ELSE                                                                                                   
		BEGIN 
			SELECT CM.[CategoryName],CM.[CategoryId],OIM.ProductAutoId,PM.[ProductId],PM.[ProductName],PM.[ProductLocation],                                                                 
			OIM.[UnitTypeAutoId] AS UnitAutoId,UM.[UnitType],OIM.[QtyPerUnit],OIM.[UnitPrice],OIM.[TotalPieces],OIM.[SRP],OIM.[GP],  
			case when OIM.[GP] < 0 then '---' else convert(varchar(15),OIM.[GP]) end as PrintGP  
			,OIM.[Tax],                                                 
			OIM.[NetPrice],OIM.[Barcode], OIM.[QtyShip],OIM.[RequiredQty] ,convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice,                                                                                                        
  
			OIM.IsExchange,isnull(TaxValue,0.00) as TaxValue,ISNULL(isFreeItem,0) AS isFreeItem,UnitMLQty,                                                                                                                                                    
			ISNULL(UnitMLQty,0) as UnitMLQty,ISNULL(TotalMLQty,0) as TotalMLQty,                                                                                     
			oim.Item_TotalMLTax as TotalMLTax,Isnull(OIM_Discount,0.00) as Del_discount                                                                 
			FROM [dbo].[OrderItemMaster] AS OIM                                                  
			INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                                               
			INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitTypeAutoId                                                                        
			INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId] WHERE [OrderAutoId] = @OrderAutoId                                                                                
			AND (case when @Status=1 or @Status=2 then RequiredQty  else QtyShip end) >0                                                                         
			ORDER BY CM.[CategoryName] ASC                                                                      
		END                                                                                                                                                         
			SET @CustomerAutoId = (SELECT [CustomerAutoId] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId)         
			SELECT OM.[AutoId],[OrderNo],CONVERT(VARCHAR(20),[OrderDate],101) AS OrderDate,DO.[AmtDue],DO.[GrandTotal],DO.[AmtPaid] ,                                                                                                                             
			otm.OrderType AS OrderType ,DATEDIFF(dd,OrderDate,GETDATE()) as Aging, orderdate as orderdateSort 
			FROM [dbo].[OrderMaster] As OM                                                                                                                         
			INNER JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId]     
			INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
			WHERE [CustomerAutoId] = @CustomerAutoId AND ([Status] = 6 or [Status] = 11 ) AND do.[AmtDue] > 0.00                                                                                   
			order by orderdateSort                                                                                            
			select OrderAutoId,CreditAutoId,CreditNo,CONVERT(VARCHAR(10),CreditDate,101) AS CreditDate,ISNULL(TotalAmount,0.00) as ReturnValue,ISNULL(PaidAmount,0.00)  
			as amtDeducted,        
			ISNULL(BalanceAmount,0.00) as amtDue                                                                                                            
			from CreditMemoMaster AS CM where  OrderAutoId=@OrderAutoId               
	END
	ELSE IF @Opcode=106                                    
    BEGIN                                 
		Declare @LocationCode varchar(100)                        
		SELECT @LocationCode=DB_NAME()                       
		SELECT CompanyName,EmailAddress, [Address],[Website],[MobileNo],[FaxNo],TermsCondition,Logo,case when @LocationCode='psmct.a1whm.com' then 'CT' else 'NJ' end as LocationCode  
		FROM CompanyDetails                            
                           
		SELECT OM.[CreditNo] as OrderNo,CONVERT(VARCHAR(20), OM.[CreditDate], 101) AS OrderDate,                                         
		CM.[AutoId] As CustAutoId,CM.[CustomerId],CM.[CustomerName],(case when Contact1=Contact2 then Contact1 when isnull(Contact2,'')!='' then CM.[Contact1] + '/ ' + CM.[Contact2] else 
		Contact1 end) As Contact,CTy.CustomerType,CT.[TermsDesc],                                    
		[dbo].[ConvertFirstLetterinCapital](BA.[Address]+(case when isnull(BA.Address2,'')!='' then ', '+BA.Address2 else '' end)+', '+BA.City)+', '+UPper(S.StateCode)+' - '+BA.Zipcode As BillAddr,                                  
		S.[StateName] AS State1,BA.[City] AS City1,BA.[Zipcode] As Zipcode1,
		[dbo].[ConvertFirstLetterinCapital](SA.[Address]+(case when isnull(SA.Address2,'')!='' then ', '+SA.Address2 else '' end)+', '+SA.City)+', '+S1.StateCode+' - '+SA.Zipcode As ShipAddr,
		S1.[StateName] As State2,SA.[City] AS City2,                                    
		SA.[Zipcode] As Zipcode2,OM.[TotalAmount],ISNULL(OM.[OverallDisc],0) as OverallDisc1,ISNULL(OM.[OverallDiscAmt],0) as OverallDisc,ISNULL(OM.[TotalTax],0) as TotalTax,
		isnull(OM.[GrandTotal],OM.[TotalAmount]) as GrandTotal,ISNULL((select top 1 PrintLabel from MLTaxMaster where TaxState=BA.State),'') as MLTaxPrintLabel, 
		ISNULL(MLQty,0) as MLQty,isnull(MLTax,0) as MLTax,CM.ContactPersonName,ISNULL(AdjustmentAmt,0) as AdjustmentAmt,                            
		ISNULL(OM.WeightTaxAmount,0) as WeightTaxAmount,  --  TaxTypeAutoId Added By Rizwan Ahmad on 11/09/2019 04:00 AM                                  
		CM.BusinessName,EM.[FirstName] + ' ' + EM.[LastName] As SalesPerson FROM [dbo].[CreditMemoMaster] As OM                                    
		INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                               
		INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]                                    
		INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[CustomerAutoId] = OM.[CustomerAutoId] AND BA.IsDefault=1                                    
		INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[CustomerAutoId] = OM.[CustomerAutoId] AND SA.IsDefault=1                                    
		LEFT JOIN [dbo].[CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                                           
		INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                                     
		INNER JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                                     
		left JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.CreatedBy                                    
		WHERE OM.CreditAutoId=@CreditAutoId                                    
                                        
		SELECT CM.[CategoryName],CM.[CategoryId],OIM.ProductAutoId,PM.[ProductId],PM.[ProductName],PM.[ProductLocation],                                  
		OIM.[UnitAutoId] AS UnitAutoId,UM.[UnitType],OIM.[QtyPerUnit],OIM.ManagerUnitPrice as  [UnitPrice],                                    
		OIM.NetAmount AS  [NetPrice],OIM.AcceptedQty AS QtyShip,'' AS BarCode                                    
		FROM [dbo].[CreditItemMaster] AS OIM                                     
		INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                                    
		INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitAutoId                                     
		INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId] WHERE CreditAutoId=@CreditAutoId                                     
		ORDER BY CM.[CategoryName] ASC                       
    END 
 ELSE IF @Opcode=107                                   
    BEGIN
	SELECT * from ( SELECT CustomerName,OrderNo,sa.CompleteAddress as ShipAddr,om.Status,
	 ISNULL((
		select ProductId,ProductName,um.UnitType,ISNULL(QtyShip,0) as QtyShip,NetPrice,UnitPrice,IsExchange,isFreeItem from OrderItemMaster as oim
		INNER JOIN ProductMaster as pm on pm.AutoId=oim.ProductAutoId
		INNER JOIN UnitMaster as um on um.AutoId=oim.UnitTypeAutoId
		where oim.OrderAutoId=om.AutoId and om.Status!=11
		order by ProductId
		for json path
	 ),'[]') as notDelItem,
	 ISNULL((
		select ProductId,ProductName,um.UnitType,QtyShip,NetPrice,UnitPrice,IsExchange,isFreeItem from Delivered_Order_Items as oim
		INNER JOIN ProductMaster as pm on pm.AutoId=oim.ProductAutoId
		INNER JOIN UnitMaster as um on um.AutoId=oim.UnitAutoId
		where oim.OrderAutoId=om.AutoId and om.Status=11
		order by ProductId
		for json path
	 ),'[]') as DelItem,
	 OverallDiscAmt,DeductionAmount,AdjustmentAmt,PayableAmount
	 FROM OrderMaster AS OM
	 INNER JOIN CustomerMaster as cm on cm.autoid=om.CustomerAutoId
	 INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = OM.[ShipAddrAutoId]        
	 WHERE om.[AutoId] in (SELECT * FROM [dbo].[fnSplitString](@BulkOrderAutoId, ','))  
	 ) as OrderDetails
	 for json path
	END
  END TRY                                                                      
  BEGIN CATCH                               
  Set @isException=1                                          
  Set @exceptionMessage='Oops! Something went wrong.Please try later.'                                                                                                                                                    
  END CATCH                                                                                       
END 