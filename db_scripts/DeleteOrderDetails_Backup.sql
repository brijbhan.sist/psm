USE [psmnj.a1whm.com]
GO

/****** Object:  Table [dbo].[DeleteOrderDetails_Backup]    Script Date: 05/22/2020 16:04:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DeleteOrderDetails_Backup](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[OrderNo] [varchar](25) NULL,
	[OrderDate] [datetime] NULL,
	[DeletedBy] [varchar](20) NOT NULL,
	[DeleteDate] [datetime] NULL,
	[DeleteRemark] [varchar](250) NOT NULL
	)