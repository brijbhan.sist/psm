USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcManageMLQuantity]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProcManageMLQuantity]            
@Opcode int=null,                    
@ProductAutoId INT=NULL,                     
@CategoryAutoId INT= NULL,            
@SubcategoryAutoId INT= NULL,            
@ProductName varchar(50)=NULL,  
@Xml xml=NULL,   
@UpdatedBy int=null,                   
@isException bit out,            
@exceptionMessage varchar(max) out                   
AS            
BEGIN            
 BEGIN TRY            
  Set @isException=0            
  Set @exceptionMessage='Success'            
 IF @Opcode = 41            
   BEGIN      
   select AutoId,ProductName,ProductId,IsApply_ML,MLQty,isnull(WeightOz,0.00)as WeightOz,IsApply_Oz from ProductMaster PM  
   where  (@CategoryAutoId IS NULL OR @CategoryAutoId=0 OR PM.[CategoryAutoId] = @CategoryAutoId)             
      AND (@SubcategoryAutoId IS NULL OR @SubcategoryAutoId=0 OR PM.[SubcategoryAutoId] = @SubcategoryAutoId)                  
      AND (@ProductName IS NULL OR @ProductName='' OR replace(PM.ProductName,' ','') LIKE '%' + replace(@ProductName,' ','') + '%')           
  AND(@ProductAutoId IS NULL OR @ProductAutoId=0 or PM.ProductId like '%'+CONVERT(varchar(250),@ProductAutoId)+'%')     
  order by AutoId Asc                      
 END    
   
 if @Opcode = 42            
   BEGIN   
  update tab1 set MLQty=Tab.MLQty,IsApply_ML=Tab.IsApply_ML,WeightOz=Tab.WeightOz ,IsApply_Oz= Tab.IsApply_Oz,  
  UpdateDate=GETDATE(),ModifiedDate=GETDATE(),ModifiedBy=@UpdatedBy  
  from ProductMaster as tab1  
  inner join (  
    select tr.td.value('AutoId[1]','int') as AutoId,          
    tr.td.value('IsApply_ML[1]','bit') as IsApply_ML,          
 tr.td.value('MLQty[1]','decimal(10,2)') as MLQty,  
 tr.td.value('IsApply_Oz[1]','bit') as IsApply_Oz,  
 tr.td.value('WeightOz[1]','decimal(10,2)') as WeightOz  
 from  @Xml.nodes('/Xml') tr(td)  
    ) as tab on tab.AutoId=Tab1.AutoId  
        
   End     
   END TRY            
 BEGIN CATCH            
  Set @isException=1            
  Set @exceptionMessage='Oops! Something went wrong.Please try later.'      
 END CATCH  
 END 
GO
