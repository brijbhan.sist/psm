USE [psmnj.a1whm.com]
GO

/****** Object:  Table [dbo].[PurchaseProductsMaster]    Script Date: 02/11/2020 16:52:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PurchaseProductsMaster](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[ProductAutoId] [int] NULL,
	[Unit] [int] NULL,
	[QtyPerUnit] [int] NULL,
	[Qty] [int] NULL,
	[TotalPieces] [int] NULL,
	[UnitPrice] [decimal](18, 2) NULL,
	[CostPrice] [decimal](18, 2) NULL,
	[POAutoId] [int] NULL,
	[ReceivedPieces]  AS ([dbo].[FN_ReceivedPieces]([POAutoId],[ProductAutoId],[Unit])),
	[RemainPieces]  AS ([dbo].[FN_PORemainPieces]([POAutoId],[ProductAutoId],[Unit])),
 CONSTRAINT [PK__Purchase__6B23290596CEB969] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


