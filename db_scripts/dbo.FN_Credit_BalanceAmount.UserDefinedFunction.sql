USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Credit_BalanceAmount]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION  [dbo].[FN_Credit_BalanceAmount]
(
	 @CreditAutoId int
)
RETURNS decimal(18,2)
AS
BEGIN
	DECLARE @GrandTotal decimal(18,2)	
	SET @GrandTotal=(SELECT (TotalAmount-PaidAmount) FROM CreditMemoMaster WHERE CreditAutoId=@CreditAutoId)
	RETURN @GrandTotal
END
Alter table CreditMemoMaster drop column BalanceAmount
drop function FN_Credit_BalanceAmount
Alter table CreditMemoMaster add BalanceAmount  AS ([dbo].[FN_Credit_BalanceAmount]([CreditAutoId]))
GO
