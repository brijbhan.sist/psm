Select convert(date,GETDATE()-6*30)
Select * from (
select PM.AutoId,ProductId,ProductName,CM.CategoryName,SCM.SubcategoryName,BM.BrandName,Format(PM.CreateDate,'MM/dd/yyyy') as CreateDate,
			(
			select max(orderdate) from OrderMaster as om 
			inner join Delivered_Order_Items as do on do.OrderAutoId=om.AutoId
			where om.Status=11 and do.ProductAutoId=pm.AutoId

			) as lastdate,PM.ProductStatus
			from ProductMaster as PM
			inner join CategoryMaster CM on CM.AutoId=PM.CategoryAutoId  
			inner join SubCategoryMaster SCM on SCM.AutoId=PM.SubcategoryAutoId  
			left join BrandMaster BM on BM.AutoId=PM.BrandAutoId  

			where pm.AutoId not in (
			select distinct OIM.ProductAutoId from  OrderMaster  as OM
			inner join Delivered_Order_Items  as OIM on OM.AutoId=OIM.OrderAutoId
			where  convert(date,OrderDate)>=convert(date,GETDATE()-6*30)
			and om.Status=11
			)
)
as t1 order by lastdate desc


