
  
ALTER Proc [dbo].[ProcApiLogReport]  
@Opcode int=null,  
@User varchar(200)=null,  
@DeviceName varchar(max)=null,  
@FromDate datetime=null,  
@ToDate datetime=null,  
@Method int=null,  
@AppV Varchar(50)=null,  
@PageIndex INT=1,      
@PageSize INT=10,      
@isException bit out,      
@exceptionMessage varchar(max) out  
as   
begin  
BEGIN TRY      
  Set @isException=0      
  Set @exceptionMessage='Success'      
  IF @Opcode=41      
  BEGIN  
  select (FirstName +' '+LastName)as UserName,AutoId,EmpId  from EmployeeMaster where EmpType in(2,5) and Status=1 order by UserName ASC  
  Select AutoId,Url,MathodName as MethodName from App_ApiMethods order by MathodName ASC  
  Select DISTINCT appVersion from App_ApiRequestLog  
  end    
  IF @Opcode=42     
  BEGIN  
   SELECT ROW_NUMBER() OVER(ORDER BY tc DESC) AS RowNumber, * INTO #Results from                      
  (  
  select al.AutoId,(al.accessToken) as AccessToken,al.CreatedOn as tc,(al.appVersion)as AppVersion,(al.deviceID) as DeviceId,ISNULL((Select DeviceName from DeviceMaster Where DeviceId=al.deviceID),'') as DeviceName,
  format(al.CreatedOn,'MM/dd/yyyy hh:mm tt')as CreatedOn,(al.latLong)as LatLong,aa.MathodName as MethodName,format(al.utcTimeStamp,'MM/dd/yyyy hh:mm tt')as UtcTimeStamp,  
  isnull(rm.RouteName,'N/A')as RouteName,(em.FirstName+' '+em.LastName) as UserName from App_ApiRequestLog as al  
  inner join EmployeeMaster em on em.AutoId=al.userAutoId  
  left join RouteMaster rm on rm.AutoId=al.RouteAutoId  
  inner join App_ApiMethods aa on aa.Url=al.MethodName  
   Where (@FromDate is null or @FromDate='' or @ToDate is null or @ToDate='' or convert(date,al.CreatedOn) between Convert(date,@FromDate)  
    and Convert(date,@ToDate)) and (ISNULL(@User,0)=0 or al.userAutoId=@User)  and   
	 (@AppV is null or @AppV='' or al.appVersion=@AppV) and (@Method is null or @Method='' or aa.AutoId=@Method)  
	 and (@DeviceName is null or @DeviceName='' or ISNULL((Select DeviceName from DeviceMaster Where DeviceId=al.deviceID),'') like '%'+@DeviceName+'%')
  ) as t order by tc DESC  
  SELECT COUNT(*) AS RecordCount, case when @PageSize=0 then COUNT(*) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Results  
  Select * from #Results  
  WHERE @PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)   
  
  end    
 END TRY      
 BEGIN CATCH      
  Set @isException=1      
  Set @exceptionMessage=ERROR_MESSAGE()      
 END CATCH       
end  
GO
