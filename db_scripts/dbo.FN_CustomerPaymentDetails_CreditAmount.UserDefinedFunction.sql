CREATE FUNCTION  [dbo].[FN_CustomerPaymentDetails_CreditAmount]
(
	 @PaymentAutoId int 
)
RETURNS decimal(10,2)
AS
BEGIN
	DECLARE @ReceivedAmount decimal(10,2)=0,@SettledAmount DECIMAL(12,2),@CreditAmount DECIMAL(12,2)		 	 
	SET @ReceivedAmount=ISNULL((SELECT 
	CASE WHEN PaymentMode=1 and TotalCurrencyAmount>0  THEN TotalCurrencyAmount ELSE 
	ReceivedAmount END FROM CustomerPaymentDetails WHERE PaymentAutoId=@PaymentAutoId),0)
	
	SET @SettledAmount=ISNULL((SELECT sum(ReceivedAmount) FROM PaymentOrderDetails WHERE PaymentAutoId=@PaymentAutoId),0)
	SET @CreditAmount=@ReceivedAmount-@SettledAmount
	RETURN @CreditAmount
END