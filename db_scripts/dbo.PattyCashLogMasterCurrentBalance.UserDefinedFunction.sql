USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[PattyCashLogMasterCurrentBalance]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[PattyCashLogMasterCurrentBalance](@AutoId int)  
Returns decimal(12,2)  
as  
Begin  
	declare @CrTransationAmount decimal(10,2),@DrTrnsactionAmount decimal(10,2),@CheckDate datetime,@Result decimal(10,2)  
	set @CheckDate=(select TransactionDate from PattyCashLogMaster where AutoId=@AutoId)  
	set @CrTransationAmount= (select sum(TransactionAmount) from PattyCashLogMaster where
	TransactionDate<=@CheckDate and   (case 
	when TransactionDate=@CheckDate and AutoId<@AutoId then 1 
	when TransactionDate<@CheckDate then 1 else 0 end)=1 and TransactionType='CR')  
 
	set @DrTrnsactionAmount= (select sum(TransactionAmount) from PattyCashLogMaster 
	where TransactionDate<=@CheckDate and  (case 
	when TransactionDate=@CheckDate and AutoId<@AutoId then 1 
	when TransactionDate<@CheckDate then 1 else 0 end)=1 and TransactionType='DR')  
	set @Result=(Isnull(@CrTransationAmount,0)-Isnull(@DrTrnsactionAmount,0))  
	Return @Result  
END  

GO
