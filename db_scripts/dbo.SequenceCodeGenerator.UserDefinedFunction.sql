USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[SequenceCodeGenerator]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[SequenceCodeGenerator](@SequenceCode nvarchar(50))
returns nvarchar(25)
as begin
 declare @NewSequence nvarchar(25)

 select 
@NewSequence=(
PreSample
+
substring(PostSample,1,(len(PostSample)-len(currentSequence+1)))
+
convert(nvarchar,currentSequence+1)
)
from SequenceCodeGeneratorMaster 
where SequenceCode=@SequenceCode
 return @NewSequence
end



GO
