USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedTableType [dbo].[DT_OrderAsgn]    Script Date: 11/20/2019 5:05:52 AM ******/
CREATE TYPE [dbo].[DT_OrderAsgn] AS TABLE(
	[OrderAutoId] [int] NULL,
	[Stoppage] [varchar](10) NULL,
	[Remarks] [varchar](500) NULL
)
GO
