ALTER Procedure ProcPurchaseOrderVENDorMaster                          
 @Opcode int=Null,                         
 @EmpId INT=0,                          
 @POVNumber varchar(15)='', 
 @POAutoId int=null,                        
 @ProductAutoId INT=0,                        
 @UnitAutoId INT=0,      
 @RecAutoId INT=0,    
 @QtyPerUnit INT=0,                        
 @RequiredQty INT=0,     
 @ReceivedQty INT=0,                       
 @POVDraftAutoId INT=0,                                                
 @VendorId INT=0,                         
 @Status INT=0,          
 @RecUnitAutoId INT=0,
 @Remark varchar(max)='',     
 @ReceiverRemark varchar(max)='',                       
 @Barcode varchar(25)='',      
 @PODraftId varchar(20)='', 
 @Draftitems xml=null,       
 @POFromDate DATETIME=NULL,      
 @POToDate DATETIME=NULL,                 
 @PageIndex INT = 1,                                                              
 @PageSize INT = 10,                                                               
 @RecordCount INT =null,                                                              
 @isException bit out,                                                               
 @exceptionMessage varchar(max) out,
 @Price decimal(18,2)=null,
 @BillNo varchar(40)='', 
 @BillDate DATETIME=NULL,  
 @RecDate DATETIME=NULL
AS                          
BEGIN                          
    SET @isException=0                          
    SET @exceptionMessage='Success'                          
 IF @Opcode=401                          
 BEGIN                          
  Select AutoId,VendorName from VENDorMaster --Where LocationTypeAutoId=0                          
  order by VendorName                           
  Select AutoId,ProductId,Convert(varchar,ProductId)+'--'+ProductName as ProductName,VendorAutoId from ProductMaster AS PM Where ProductStatus=1 and                      
  (select count(1) from PackingDetails where  ProductAutoId=PM.AutoId)>0 order by ProductName       
       
 END   
  IF @Opcode=4011                         
 BEGIN                          
                     
  select(select(select AutoId,ProductId,Convert(varchar,ProductId)+'--'+ProductName as ProductName,VendorAutoId from ProductMaster AS PM Where ProductStatus=1 and                      
  (select count(1) from PackingDetails where  ProductAutoId=PM.AutoId)>0 order by ProductName for json path, INCLUDE_NULL_VALUES) as Product for json path, INCLUDE_NULL_VALUES)as ProductList  
       
 END   
 IF @Opcode=402                        
 BEGIN                        
  SELECT pm.ProductName,UM.AutoId AS AutoId, UM.[UnitType] AS UnitType, [Qty],(Stock /(CASE WHEN pd.Qty=0 then 1 ELSE PD.Qty END) )as AvailableQty,                                                                                              
  Price,EligibleforFree,CostPrice  FROM [dbo].[PackingDetails] AS PD                                                                                              
  INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = PD.UnitType                                                                                      
  INNER JOIN ProductMaster as pm on pm.AutoId =PD.[ProductAutoId] WHERE PD.[ProductAutoId] = @ProductAutoId          
  order by UM.[UnitType]                                                                                                                       
  SELECT PackingAutoId AS AutoId FROM ProductMaster  WHERE  AutoId=@ProductAutoId                            
 END       
 IF @Opcode=403                        
 BEGIN                
		Select ROW_NUMBER() OVER(Order By POM.AutoId desc) AS RowNumber,POM.AutoId,PONo,FORMAT(PODate,'MM/dd/yyyy') as PODate,VM.VendorName,NoofItems,
		SM.StatusType AS Status,Sm.ColorCode,PORemarks,BillNo,FORMAT(BillDate,'MM/dd/yyyy') as BillDate,(em.FirstName+' '+em.LastName) as CreateBy INTO #POResult FROM PurchaseOrderMaster AS POM       
		INNER JOIN VendorMaster AS VM ON POM.VenderAutoId=VM.AutoId      
		INNER JOIN StatusMaster AS SM ON POM.Status=SM.AutoId AND SM.Category='POOrd'      
		inner join EmployeeMaster as em on em.AutoId=POM.CreatedBy
		WHERE POM.VendorType=0 and  --POM.AssignedBy=@EmpId and    
		(ISNULL(@VendorId,0)=0 OR POM.VenderAutoId =@VendorId) AND (ISNULL(@POVNumber,'')='' or POM.PONo=@POVNumber) AND POM.VendorType=0 AND       
		(@POFromDate is null or @POFromDate='' or @POToDate is null or @POToDate ='' or Convert(date,POM.PODate) between Convert(date,@POFromDate) AND Convert(date,@POToDate)) 
		and (ISNULL(@Status,0)=0 OR POM.Status =@Status)
		order by POM.AutoId DESC      
		SELECT COUNT(*) as RecordCount,@PageSize as PageSize,@PageIndex as PageIndex FROM #POResult        
		SELECT * FROM #POResult                           
		WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1                      
 END       
 IF @Opcode=404    
 BEGIN   
	select distinct POM.DraftAutoId,FORMAT(PODate,'MM/dd/yyyy') as PODate,sm.StatusType as Status,sm.ColorCode,PODraftId,   
	(Select count(*) from DraftPurchaseProductsMaster where DraftAutoId=POM.DraftAutoId)NoofItems,PORemarks into #temp404 from DraftPurchaseProductsMaster as PPM 
	inner join DraftPurchaseOrderMaster as POM on POM.DraftAutoId=PPM.DraftAutoId
	inner join StatusMaster as sm on sm.AutoId=POM.Status and sm.category='PODraft'
	where POM.CreatedBy=@EmpId and (ISNULL(@VendorId,0)=0 OR PPM.VendorAutoId= @VendorId) 
	AND  POM.VendorType=0 AND (@POFromDate is null or @POFromDate='' or @POToDate is null or @POToDate ='' or Convert(date,POM.PODate) between Convert(date,@POFromDate) AND Convert(date,@POToDate))             
		and (@PODraftId IS NULL OR @PODraftId='' OR POM.PODraftId= @PODraftId) 
	and POM.Status in (1,3)

	 Select  ROW_NUMBER() OVER(Order By DraftAutoId) AS RowNumber,* INTO #DraftPOResult FROM #temp404  
	
	 SELECT COUNT(*) as RecordCount,@PageSize as PageSize,@PageIndex as PageIndex FROM #DraftPOResult        
                                                                       
	 SELECT * FROM #DraftPOResult                                                                        
	 WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1  
	 order by RowNumber DESC     
 END      
 IF @Opcode=405    
 BEGIN    
 Select DraftAutoId,Format(PODate,'MM/dd/yyyy') as PODate,PORemarks,sm.StatusType as Status,PODraftId,DPO.Status as SAutoId from DraftPurchaseOrderMaster as DPO
 inner join StatusMaster as sm on sm.AutoId=DPO.Status and sm.Category='PODraft' 
 where DraftAutoId=@POVDraftAutoId AND VendorType=0    
    
 Select ProductAutoId,PM.ProductId,PM.ProductName,Unit,QtyPerUnit,DraftAutoId,Qty,(QtyPerUnit*Qty) as TotalPeice,PPM.VendorAutoId,Price from DraftPurchaseProductsMaster AS PPM    
 INNER JOIN ProductMaster AS PM ON    
 PPM.ProductAutoId=PM.AutoId    
 where DraftAutoId=@POVDraftAutoId    
    
 SELECT PD.ProductAutoId,UM.AutoId AS AutoId, UM. UnitType  AS UnitType, PD. Qty ,ISNULL(EligibleforFree,0) as EligibleforFree,CostPrice            
    FROM  PackingDetails  AS PD                                                                                                     
    INNER JOIN  UnitMaster  AS UM ON UM.AutoId = PD.UnitType                
    WHERE PD.ProductAutoId  in (select ProductAutoId from DraftPurchaseProductsMaster where DraftAutoId=@POVDraftAutoId)            
 END       
 IF @Opcode=406    
 BEGIN      
      
   Select POM.AutoId,PONo,FORMAT(PODate,'MM/dd/yyyy') as PODate,VM.VendorName,PORemarks,RecieveStockRemark,sm.AutoId as SAutoId,(select top 1 AutoId from POReceiveMaster where POAutoId=@POAutoId and Status=1) as RecAutoId,
   sm.StatusType as Status,BillNo,FORMAT(BillDate,'MM/dd/yyyy') as BillDate from PurchaseOrderMaster AS POM    
   INNER JOIN VendorMaster VM ON POM.VenderAutoId=VM.AutoId  
   INNER JOIN StatusMaster sm on sm.AutoId=POM.Status and Category='POOrd'   
   where POM.vendortype=0 AND POM.AutoId=@POAutoId   
    
   Select PPM.AutoId,PPM.ProductAutoId,ProductId,PM.ProductName,UM.UnitType+' ('+Convert(varchar,PPM.QtyPerUnit)+') ' as UnitType, PPM.Unit,PRP.RecUnitAutoId,
   PPM.QtyPerUnit,Qty,TotalPieces,
   ISNULL(PRP.ReceiveQty,0) as RecivedQty,    
   (ISNULL(PPM.ReceivedPieces,0)-ISNULL(PRP.QtyPerUnit,0)*ISNULL(PRP.ReceiveQty,0)) as OtherRecivedPieces,  
   --ISNULL(PRP.ReceiveQty,0)*ISNULL(prp.QtyPerUnit,0) as OtherRecivedPieces,  
  (ISNULL(PRP.QtyPerUnit,0)*ISNULL(PRP.ReceiveQty,0)) as ReceivedPiece,PPM.RemainPieces as RemainQty
   from PurchaseProductsMaster AS PPM    
   INNER JOIN ProductMaster AS PM ON PPM.ProductAutoId=PM.AutoId    
   INNER JOIN UnitMaster AS UM ON PPM.Unit=UM.AutoId  
   left join POReceiveMaster as PMS on PMS.POAutoId=PPM.POAutoId and PMS.AutoId=@RecAutoId
   left join POReceiveProduct as PRP on PRP.ProductAutoId=PPM.ProductAutoId and PPM.Unit=PRP.UnitAutoId and PMS.AutoId=PRP.POReceiveAutoId 
   where PPM.POAutoId=@POAutoId   

    SELECT PD.ProductAutoId,UM.AutoId AS AutoId, UM. UnitType  AS UnitType, PD. Qty ,CostPrice            
    FROM  PackingDetails  AS PD                                                                                                     
    INNER JOIN  UnitMaster  AS UM ON UM.AutoId = PD.UnitType                
    WHERE PD.ProductAutoId  in (select ProductAutoId from PurchaseProductsMaster where POAutoId=@POAutoId)   
	
	select PORecieveId,StatusType as RecStatus,FORMAT(ReceiveDate,'MM/dd/yyyy') as ReceiveDate,FirstName+' '+LastName as ReceivedBy,prm.Status,prm.Remarks from POReceiveMaster as prm 
	inner join StatusMaster as sm on sm.AutoId=prm.Status and Category='PORec' 
	inner join EmployeeMaster as em on em.AutoId=prm.ReceiveBy
	where prm.AutoId=@RecAutoId 
 END                
 IF @Opcode=101                        
 BEGIN                        
  BEGIN TRY                      
    BEGIN transaction           
                      
 IF NOT EXISTS(SELECT * FROM DraftPurchaseOrderMaster WHERE DraftAutoId=ISNULL(@POVDraftAutoId,0))                        
 BEGIN           
     set @PODraftId=(SELECT DBO.SequenceCodeGenerator('PODraftId')) 
   
  INSERT INTO DraftPurchaseOrderMaster (PODate,PORemarks,CreatedBy,CreatedOn,VENDorType,Status,PODraftId)                        
  VALUES (GETDATE(),@Remark,@EmpId,GETDATE(),0,1,@PODraftId)                      
  SET @POVDraftAutoId=Scope_Identity()      
  
   UPDATE SequenceCodeGeneratorMaster SET currentSequence = currentSequence + 1 WHERE SequenceCode='PODraftId' 

   SET @ReceiverRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=32), '[Draft No]', @PODraftId)     

  insert into PODraftLog(RemarkType,Remark,POAutoId,CreatedOn,CraetedBy,ActionTaken)values
  (@ReceiverRemark,@Remark,@POVDraftAutoId,GETDATE(),@EmpId,32)    
 END                     
                        
 IF EXISTS(SELECT * FROM DraftPurchaseProductsMaster Where DraftAutoId=@POVDraftAutoId and (ProductAutoId=@ProductAutoId AND Unit=@UnitAutoId))                    
 BEGIN     
 
  UPDATE DraftPurchaseProductsMaster SET Qty=Qty+@RequiredQty Where DraftAutoId=@POVDraftAutoId and (ProductAutoId=@ProductAutoId AND Unit=@UnitAutoId)                    
              
  SELECT PM.AutoId as ProductAutoId,PM.ProductId,PM.ProductName,PD.Qty as UnitQty,PD.UnitType,CostPrice,               
  ISNULL((SELECT TOP 1 UnitType +  ' ('+ CONVERT(VARCHAR(20),PD.QTY)+' pcs)' from UnitMaster AS UM WHERE UM.AutoId=PD.UnitType),'') AS UnitName,                                                                                                              
  PM.[Stock]/PD.Qty  as Stock,DPPM.Qty as RequiredQty,(DPPM.QtyPerUnit*DPPM.Qty) as TotalPeice                            
  FROM [dbo].[PackingDetails] As PD                                                                                         
  INNER JOIN [dbo].[ProductMaster] AS PM ON PD.[ProductAutoId]=PM.[AutoId]                
  INNER JOIN DraftPurchaseProductsMaster AS DPPM ON PD.ProductAutoId=DPPM.ProductAutoId AND PD.UnitType=DPPM.Unit   
  and DraftAutoId=@POVDraftAutoId    
  WHERE PD.ProductAutoId = @ProductAutoId AND PD.UnitType=@UnitAutoId  order by DPPM.ItemAutoId DESC                           
                                    
  SELECT UM.AutoId AS AutoId, UM.[UnitType] AS UnitType, [Qty],ISNULL(EligibleforFree,0) as EligibleforFree,CostPrice into #UnitDetails FROM                       
  [dbo].[PackingDetails] AS PD                                                                                                                     
  INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = PD.UnitType WHERE PD.[ProductAutoId] = @ProductAutoId                       
                           
  Select * from #UnitDetails                      
 END                    
 ELSE                    
 BEGIN                    
  IF EXISTS(SELECT * FROM DraftPurchaseProductsMaster Where ProductAutoId=@ProductAutoId AND DraftAutoId=@POVDraftAutoId)                    
  BEGIN                    
    SET @isException=1                      
    SET @exceptionMessage='ambiguity'--'You cannot add multiple unit of a product.'                      
  END                    
  ELSE                    
  BEGIN   
    update DraftPurchaseOrderMaster set PORemarks=@Remark where DraftAutoId=@POVDraftAutoId

   INSERT INTO DraftPurchaseProductsMaster  (ProductAutoId,Unit,QtyPerUnit,Qty,DraftAutoId,VendorAutoId,Price) VALUES                         
   (@ProductAutoId,@UnitAutoId,@QtyPerUnit,@RequiredQty,@POVDraftAutoId,@VendorId,@Price)                     
                    
                 
  SELECT PM.AutoId as ProductAutoId,PM.ProductId,PM.ProductName,PD.Qty as UnitQty,PD.UnitType,               
  ISNULL((SELECT TOP 1 UnitType +  ' ('+ CONVERT(VARCHAR(20),PD.QTY)+' pcs)' from UnitMaster AS UM WHERE UM.AutoId=PD.UnitType),'') AS UnitName,                                                                                                               
  PM.[Stock]/PD.Qty  as Stock,DPPM.Qty as RequiredQty,(DPPM.QtyPerUnit*DPPM.Qty) as TotalPeice                            
  FROM [dbo].[PackingDetails] As PD                                                                                         
  INNER JOIN [dbo].[ProductMaster] AS PM ON PD.[ProductAutoId]=PM.[AutoId]                
  INNER JOIN DraftPurchaseProductsMaster AS DPPM ON PD.ProductAutoId=DPPM.ProductAutoId AND PD.UnitType=DPPM.Unit   
 and DraftAutoId=@POVDraftAutoId
   WHERE PD.ProductAutoId = @ProductAutoId AND PD.UnitType=@UnitAutoId  order by DPPM.ItemAutoId DESC                                 
                                    
   SELECT UM.AutoId AS AutoId, UM.[UnitType] AS UnitType, [Qty],ISNULL(EligibleforFree,0) as EligibleforFree,CostPrice into #UnitDetailsUpd FROM                       
   [dbo].[PackingDetails] AS PD                     
   INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = PD.UnitType WHERE PD.[ProductAutoId] = @ProductAutoId                       
                           
  Select * from #UnitDetailsUpd               
                        
  END                
  END 
                     
  SELECT @POVDraftAutoId as DraftPOVId                    
  Commit Transaction                       
   END TRY                      
   BEGIN CATCH                      
           RollBack transaction                      
           SET @isException=1                      
           SET @exceptionMessage='Oops something went wrong.'                      
   END Catch                      
 END                        
 IF @Opcode=102                      
 BEGIN                      
 BEGIN TRY                      
 BEGIN Transaction                      
  IF EXISTS(SELECT * FROM ItemBarcode WHERE Barcode=@Barcode)                                                                                                                                                                
 BEGIN                                                                                                                                                                
  SET @ProductAutoId=(SELECT ProductAutoId FROM ItemBarcode WHERE Barcode=@Barcode)                                                                                      
  SET @UnitAutoId=(SELECT UnitAutoId FROM ItemBarcode WHERE Barcode=@Barcode)        
  set @Price=(select CostPrice from PackingDetails where ProductAutoId=@ProductAutoId and UnitType=@UnitAutoId)
                          
 IF NOT EXISTS(SELECT * FROM DraftPurchaseOrderMaster WHERE DraftAutoId=ISNULL(@POVDraftAutoId,0))                      
 BEGIN        
  set @PODraftId=(SELECT DBO.SequenceCodeGenerator('PODraftId')) 

  INSERT INTO  DraftPurchaseOrderMaster ( PODate , PORemarks, CreatedBy, CreatedOn ,VENDorType,Status,PODraftId) VALUES (GetDate(),@Remark,@EmpId,GetDate(),0,1,@PODraftId)                       
  Set @POVDraftAutoId = SCOPE_IDENTITY()     
  
   UPDATE SequenceCodeGeneratorMaster SET currentSequence = currentSequence + 1 WHERE SequenceCode='PODraftId' 

   SET @ReceiverRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=32), '[Draft No]', @PODraftId)     

  insert into PODraftLog(RemarkType,Remark,POAutoId,CreatedOn,CraetedBy,ActionTaken)values
  (@ReceiverRemark,@Remark,@POVDraftAutoId,GETDATE(),@EmpId,32)    

 END                      
 IF NOT EXISTS(SELECT * FROM [DraftPurchaseProductsMaster] WHERE DraftAutoId= @POVDraftAutoId AND (ProductAutoId=@ProductAutoId AND Unit=@UnitAutoId))                             
 BEGIN                      
 IF EXISTS(SELECT * FROM DraftPurchaseProductsMaster WHERE DraftAutoId=@POVDraftAutoId AND  ProductAutoId=@ProductAutoId)                    
 BEGIN                    
  SET @isException=1                      
  SET @exceptionMessage='ambiguity'--'You cannot add multiple unit of a product.'                          
 END                    
 ELSE                    
  BEGIN                
                    
   SELECT PM.AutoId as ProductAutoId,PM.ProductId,PM.ProductName,PD.Qty as UnitQty,PD.UnitType,CostPrice,pm.VendorAutoId,                
   ISNULL((SELECT TOP 1 UnitType +  ' ('+ CONVERT(VARCHAR(20),PD.QTY)+' pcs)' from UnitMaster AS UM WHERE UM.AutoId=PD.UnitType),'') AS UnitName,      
   PM.[Stock]/PD.Qty  as Stock                          
   FROM [dbo].[PackingDetails] As PD                                                                                         
   INNER JOIN [dbo].[ProductMaster] AS PM ON PD.[ProductAutoId]=PM.[AutoId]                                                                                                                                                               
   WHERE PD.ProductAutoId = @ProductAutoId AND PD.UnitType=@UnitAutoId                     
                                    
   SELECT UM.AutoId AS AutoId, UM.UnitType AS UnitType, Qty,ISNULL(EligibleforFree,0) as EligibleforFree,CostPrice INTO #BracodeUnitDetails FROM                       
   PackingDetails AS PD                                                                                                                     
   INNER JOIN UnitMaster AS UM ON UM.AutoId = PD.UnitType WHERE PD.ProductAutoId = @ProductAutoId                        
                              
   Select * From #BracodeUnitDetails                 
                
   Select @QtyPerUnit=Qty from #BracodeUnitDetails UD  Where UD.AutoId=@UnitAutoId               
  
    update DraftPurchaseOrderMaster set PORemarks=@Remark where DraftAutoId=@POVDraftAutoId
   INSERT INTO  DraftPurchaseProductsMaster( ProductAutoId , Unit, QtyPerUnit, Qty, DraftAutoId ,VendorAutoId,Price)                            
   VALUES (@ProductAutoId ,@UnitAutoId,@QtyPerUnit,@RequiredQty,@POVDraftAutoId,@VendorId,@Price)                     
  END                      
 END                            
 ELSE                            
  BEGIN                           
   SELECT PM.AutoId as ProductAutoId,PM.ProductId,PM.ProductName,PD.Qty as UnitQty,PD.UnitType,CostPrice  ,pm.VendorAutoId,             
   ISNULL((SELECT TOP 1 UnitType +  ' ('+ CONVERT(VARCHAR(20),PD.QTY)+' pcs)' from UnitMaster AS UM WHERE UM.AutoId=PD.UnitType),'') AS UnitName,       
   PM.[Stock]/PD.Qty  as Stock              
   FROM [dbo].[PackingDetails] As PD                                                                                         
   INNER JOIN [dbo].[ProductMaster] AS PM ON PD.[ProductAutoId]=PM.[AutoId]                                                                                                                                                                
   WHERE PD.ProductAutoId = @ProductAutoId AND PD.UnitType=@UnitAutoId                
                                    
   SELECT UM.AutoId AS AutoId, UM.UnitType AS UnitType, Qty,ISNULL(EligibleforFree,0) as EligibleforFree,CostPrice INTO #BracodeUnitDetailsUpd FROM                       
   PackingDetails AS PD                                               
   INNER JOIN UnitMaster AS UM ON UM.AutoId = PD.UnitType WHERE PD.ProductAutoId = @ProductAutoId                        
                              
   Select * From #BracodeUnitDetailsUpd               
                 
   Select @QtyPerUnit=Qty from #BracodeUnitDetailsUpd UD  Where UD.AutoId=@UnitAutoId               
                           
   Update [dbo].[DraftPurchaseProductsMaster] Set [Qty] = [Qty] + @RequiredQty where DraftAutoId=@POVDraftAutoId AND ProductAutoId=@ProductAutoId AND Unit=@UnitAutoId                            
                     
  END                      
 END                      
  ELSE                      
  BEGIN                      
       SET @isException=1                      
       SET @exceptionMessage='Barcode does not exist.'                      
  END                     
  Select @POVDraftAutoId AS DraftPOVId                       
                    
 Commit Transaction                      
 END TRY                      
 BEGIN CATCH                      
    Rollback Transaction                      
    SET @isException=0                      
    SET @exceptionMessage='Oops something went wrong.'                      
 END CATCH                            
 END                               
 IF @Opcode=201                        
 BEGIN                        
     DELETE FROM DraftPurchaseProductsMaster WHERE ProductAutoId=@ProductAutoId AND DraftAutoId=@POVDraftAutoId --AND Unit=@UnitAutoId                      
 END                 
 IF @Opcode=301              
 BEGIN              
     IF EXISTS (SELECT * FROM DraftPurchaseOrderMaster WHERE DraftAutoId=@POVDraftAutoId)              
  BEGIN              
  UPDATE DraftPurchaseOrderMaster SET PORemarks=@Remark, CreatedBy=@EmpId,UpdatedOn=GETDATE() WHERE DraftAutoId=@POVDraftAutoId                
              
  DELETE FROM DraftPurchaseProductsMaster WHERE DraftAutoId=@POVDraftAutoId              
              
  INSERT INTO DraftPurchaseProductsMaster ( ProductAutoId , Unit, QtyPerUnit, Qty, DraftAutoId ,VendorAutoId)              
  SELECT               
  tr.td.value('ProductAutoId[1]','Int') as ProductAutoId,              
  tr.td.value('Unit[1]','Int') as Unit,              
  tr.td.value('QtyPerUnit[1]','Int') as QtyPerUnit,              
  tr.td.value('Qty[1]','Int') as Qty,@POVDraftAutoId,@VendorId              
  FROM @Draftitems.nodes('/DraftItems') tr(td)              
  END              
 END                     
 IF @Opcode=302              
 BEGIN           
  Declare @NoOfItems int      
  select distinct tr.td.value('Vendor[1]','Int') as VendorAId into #tmx FROM @Draftitems.nodes('/DraftItems') tr(td)
  select  *,ROW_NUMBER() over(order by VendorAId desc) as ROWNo into #tm from #tmx
  
  DECLARE @i INT= 1;

 WHILE @i<=(SELECT COUNT(*) FROM #tm)        
  BEGIN
     declare @VenderA_Id int=(SELECT VendorAId FROM #tm WHERE ROWNo=@i)
	  select @NoOfItems=count(*) FROM @Draftitems.nodes('/DraftItems') tr(td) where tr.td.value('Vendor[1]','Int')= @VenderA_Id 
	  SET @POVNumber = (SELECT DBO.SequenceCodeGenerator('NormalPurchaseOrder'))          
   
	  insert into  PurchaseOrderMaster (PONo ,PODate ,VenderAutoId ,Status ,NoofItems,CreatedBy,CreatedOn,VendorType,AssignedBy,PORemarks)                 
	  select @POVNumber,GETDATE(),@VenderA_Id,1,@NoOfItems,@EmpId,GETDATE(),0,@EmpId,@Remark 
       
	  SET @PoAutoId=SCOPE_IDENTITY()

	  UPDATE SequenceCodeGeneratorMaster SET currentSequence = currentSequence + 1 WHERE SequenceCode='NormalPurchaseOrder' 
  
	   insert into PORemarkLogs(RemarkType,Remark,POAutoId,CreatedOn,CraetedBy,ActionTaken)
	   select PD.RemarkType,PD.Remark,@PoAutoId,PD.CreatedOn,PD.CraetedBy,PD.ActionTaken from PODraftLog as PD where PD.POAutoId=@POVDraftAutoId

	   SET @ReceiverRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=33), '[PONo]', @POVNumber)
	  insert into PORemarkLogs(RemarkType,Remark,POAutoId,CreatedOn,CraetedBy,ActionTaken)values
	  (@ReceiverRemark,@Remark,@PoAutoId,GETDATE(),@EmpId,33)         
      
	  INSERT INTO PurchaseProductsMaster (ProductAutoId,Unit,QtyPerUnit,Qty,TotalPieces,POAutoId,UnitPrice)      
	  SELECT               
	  tr.td.value('ProductAutoId[1]','Int') as ProductAutoId,              
	  tr.td.value('Unit[1]','Int') as Unit,              
	  tr.td.value('QtyPerUnit[1]','Int') as QtyPerUnit,              
	  tr.td.value('Qty[1]','Int') as Qty,      
	  tr.td.value('QtyPerUnit[1]','Int')*tr.td.value('Qty[1]','Int'),      
	  @POAutoId ,
	  tr.td.value('CostPrice[1]','decimal') as UnitPrice
	  FROM @Draftitems.nodes('/DraftItems') tr(td)  where tr.td.value('Vendor[1]','Int')=@VenderA_Id

      SET @i = @i + 1;
 END
 
  DELETE FROM DraftPurchaseProductsMaster WHERE DraftAutoId=@POVDraftAutoId     
  DELETE FROM DraftPurchaseOrderMaster WHERE DraftAutoId=@POVDraftAutoId      
  delete From PODraftLog WHERE POAutoId=@POVDraftAutoId      
    select count(*) as TotalPO from  #tm
 END          
 IF @Opcode=303    
 BEGIN                        
  BEGIN TRY                      
    BEGIN transaction           

	SELECT @PoAutoId=AutoId from PurchaseOrderMaster Where PONo=@POVNumber                
 IF NOT EXISTS(SELECT * FROM POReceiveMaster WHERE [AutoId]=ISNULL(@RecAutoId,0))                        
	BEGIN   
		declare @RecId nvarchar(20)=null
		set @POVNumber=(select PONo from PurchaseOrderMaster where AutoId=@POAutoId)
        set @RecId=(select count(*)+1 from POReceiveMaster where POAutoId=@PoAutoId)

		insert into [dbo].[POReceiveMaster]([ReceiveDate],[ReceiveBy],[POAutoId],[Status],PORecieveId,Remarks) values(GETDATE(),@EmpId,@PoAutoId,1,@POVNumber+'/'+@RecId,@Remark)                  
		SET @RecAutoId=SCOPE_IDENTITY()  

		update PurchaseOrderMaster set Status=2 WHERE AutoId=@PoAutoId

		  SET @Remark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=34), '[RecNo]', @POVNumber+'/'+@RecId)
		 insert into PORemarkLogs(RemarkType,Remark,POAutoId,CreatedOn,CraetedBy,ActionTaken)values
         (@Remark,@ReceiverRemark,@PoAutoId,GETDATE(),@EmpId,34)    
	END                     
                        
 IF EXISTS(SELECT * FROM POReceiveProduct Where [POReceiveAutoId]=@RecAutoId and (ProductAutoId=@ProductAutoId AND UnitAutoId=@UnitAutoId))                    
	 BEGIN   
	  UPDATE POReceiveProduct SET ReceiveQty=@ReceivedQty,RecUnitAutoId=@RecUnitAutoId,QtyPerUnit=@QtyPerUnit Where POReceiveAutoId=@RecAutoId and (ProductAutoId=@ProductAutoId AND UnitAutoId=@UnitAutoId)  
	 END                    
 ELSE                    
 BEGIN                    
	  IF EXISTS(SELECT * FROM POReceiveProduct Where ProductAutoId=@ProductAutoId AND [POReceiveAutoId]=@RecAutoId)                    
		  BEGIN                    
			SET @isException=1                      
			SET @exceptionMessage='ambiguity'--'You cannot add multiple unit of a product.'       
			  --UPDATE POReceiveProduct SET ReceiveQty=@ReceivedQty Where POReceiveAutoId=@RecAutoId and (ProductAutoId=@ProductAutoId AND UnitAutoId=@UnitAutoId)
		  END                    
	  ELSE                    
		  BEGIN  
		  
		   insert into [dbo].[POReceiveProduct]([ProductAutoId],[UnitAutoId],POReceiveAutoId) select                         
			ProductAutoId,Unit,@RecAutoId from PurchaseProductsMaster where POAutoId=@PoAutoId  

			UPDATE POReceiveProduct SET ReceiveQty=@ReceivedQty,RecUnitAutoId=@RecUnitAutoId,QtyPerUnit=@QtyPerUnit Where POReceiveAutoId=@RecAutoId and (ProductAutoId=@ProductAutoId AND UnitAutoId=@UnitAutoId)
			--update POReceiveProduct set ReceiveQty=@RequiredQty 
		   --insert into [dbo].[POReceiveProduct]([ProductAutoId],[UnitAutoId],[ReceiveQty],POReceiveAutoId) VALUES                         
		   --(@ProductAutoId,@UnitAutoId,@ReceivedQty,@RecAutoId)                     
                    
		  END                
  END 
        select @RecAutoId as RecAutoId        
  Commit Transaction                       
   END TRY                      
   BEGIN CATCH                      
           RollBack transaction                      
           SET @isException=1                      
           SET @exceptionMessage='Oops something went wrong.'                      
   END Catch                      
 END      
 IF @Opcode=304    
 BEGIN    
  update PurchaseOrderMaster set BillNo=@BillNo,BillDate=@BillDate where AutoId=@POVNumber


      IF(@Status=1)    
   BEGIN   
		  if(@RecAutoId is not null)
		  begin
		  update POReceiveMaster set Status=1,Remarks=@ReceiverRemark,CreatedDate=GETDATE(),ReceiveDate=@RecDate where AutoId=@RecAutoId and POAutoId=@POVNumber
		  end
   END    
   ELSE IF (@Status=2)    
   BEGIN    
       update POReceiveMaster set Status=2,Remarks=@ReceiverRemark,CreatedDate=GETDATE(),ReceiveDate=@RecDate where AutoId=@RecAutoId and POAutoId=@POVNumber

	   set @RecId=(select PORecieveId from POReceiveMaster where AutoId=@RecAutoId)
	   SET @Remark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=35), '[RecNo]', @RecId)
	   insert into PORemarkLogs(RemarkType,Remark,POAutoId,CreatedOn,CraetedBy,ActionTaken)values
       (@Remark,@ReceiverRemark,@POVNumber,GETDATE(),@EmpId,35)    
   END    
     
 END
 If @Opcode=409  
 Begin  
  delete DraftPurchaseOrderMaster where DraftAutoId=@POVDraftAutoId   
  delete DraftPurchaseProductsMaster where DraftAutoId=@POVDraftAutoId  
 End 
IF @Opcode=410    
	BEGIN    
		update DraftPurchaseOrderMaster set Status=2,PORemarks=@Remark where DraftAutoId=@POVDraftAutoId  

       set @PODraftId=(select PODraftId from DraftPurchaseOrderMaster where DraftAutoId=@POVDraftAutoId)
	   SET @ReceiverRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=36), '[Draft No]', @PODraftId)
		insert into PODraftLog(RemarkType,Remark,POAutoId,CreatedOn,CraetedBy,ActionTaken)values
		(@ReceiverRemark,@Remark,@POVDraftAutoId,GETDATE(),@EmpId,36)    
	END  
IF @Opcode=411    
	BEGIN    
	select distinct POM.DraftAutoId,FORMAT(PODate,'MM/dd/yyyy') as PODate,sm.StatusType as Status,sm.ColorCode, PODraftId,  
	(Select count(*) from DraftPurchaseProductsMaster where DraftAutoId=POM.DraftAutoId)NoofItems,PORemarks into #temp411 from DraftPurchaseProductsMaster as PPM 
	inner join DraftPurchaseOrderMaster as POM on POM.DraftAutoId=PPM.DraftAutoId
	inner join StatusMaster as sm on sm.AutoId=POM.Status and sm.category='PODraft'
	where (ISNULL(@VendorId,0)=0 OR PPM.VendorAutoId= @VendorId) 
	AND  POM.VendorType=0 AND (@POFromDate is null or @POFromDate='' or @POToDate is null or @POToDate ='' or Convert(date,POM.PODate) between Convert(date,@POFromDate) AND Convert(date,@POToDate))             
	and (@PODraftId IS NULL OR @PODraftId='' OR POM.PODraftId= @PODraftId) 
	and POM.Status in (2)

		Select ROW_NUMBER() OVER(Order By DraftAutoId) AS RowNumber,* INTO #DraftPOResult411 FROM #temp411    
		
		SELECT COUNT(*) as RecordCount,@PageSize as PageSize,@PageIndex as PageIndex FROM #DraftPOResult411        
                                                                       
		SELECT * FROM #DraftPOResult411                       
		

		WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1  
		order by RowNumber DESC     
	END    
IF @Opcode=412    
	BEGIN    
		update DraftPurchaseOrderMaster set Status=3,PORemarks=@Remark where DraftAutoId=@POVDraftAutoId  

	   set @PODraftId=(select PODraftId from DraftPurchaseOrderMaster where DraftAutoId=@POVDraftAutoId)
	   SET @ReceiverRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=37), '[Draft No]', @PODraftId)
		insert into PODraftLog(RemarkType,Remark,POAutoId,CreatedOn,CraetedBy,ActionTaken)values
		(@ReceiverRemark,@Remark,@POVDraftAutoId,GETDATE(),@EmpId,37)   
END  
IF @Opcode=413   
	BEGIN    
		update PurchaseOrderMaster set Status=3 where AutoId=@PoAutoId

	   set @POVNumber=(select PONo from PurchaseOrderMaster where AutoId=@PoAutoId)
	   SET @ReceiverRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=38), '[PONo]', @POVNumber)
		insert into PORemarkLogs(RemarkType,POAutoId,CreatedOn,CraetedBy,Remark,ActionTaken)values
		(@ReceiverRemark,@PoAutoId,GETDATE(),@EmpId,@Remark,38)   
	END  
	 Else If @Opcode=414  
 Begin  
    update POReceiveMaster set Status=4 where POAutoId=@POAutoId  and AutoId=@RecAutoId

	  set @RecId=(select PORecieveId from POReceiveMaster where AutoId=@RecAutoId)
	  SET @ReceiverRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=39), '[RecNo]', @RecId)
	insert into PORemarkLogs(RemarkType,POAutoId,CreatedOn,CraetedBy,Remark,ActionTaken)values
		(@ReceiverRemark,@POAutoId,GETDATE(),@EmpId,@Remark,39)   
	
 End    
 IF @Opcode=415    
	BEGIN    
	select Action+' '+RemarkType as RemarkType,Remark,FORMAT(CreatedOn,'MM/dd/yyyy') as PODate,em.FirstName+' '+LastName as EmpName  from PODraftLog as POD 
	inner join EmployeeMaster as em on em.AutoId=POD.CraetedBy 
	inner join tbl_ActionMaster as am on am.AutoId=pod.ActionTaken
	where POD.POAutoId=@POVDraftAutoId  
END  
IF @Opcode=416    
	BEGIN    
	select Action+' '+RemarkType as RemarkType,Remark,FORMAT(CreatedOn,'MM/dd/yyyy hh:mm:ss tt') as PODate,em.FirstName+' '+LastName as EmpName  from PoRemarkLogs as POD 
	inner join EmployeeMaster as em on em.AutoId=POD.CraetedBy 
	inner join tbl_ActionMaster as am on am.AutoId=pod.ActionTaken
	where POD.POAutoId=@POAutoId  order by POD.AutoId desc
END  
IF @Opcode=417    
	BEGIN   
	update DraftPurchaseProductsMaster set ProductAutoId=@ProductAutoId,Unit=@UnitAutoId,QtyPerUnit=@QtyPerUnit,Qty=@RequiredQty,VendorAutoId=@VendorId,Price=@Price
	where DraftAutoId=@POVDraftAutoId and ProductAutoId=@ProductAutoId 
END
IF @Opcode=418    
	BEGIN    
	update DraftPurchaseOrderMaster set PORemarks=@Remark where DraftAutoId=@POVDraftAutoId
END
 IF @Opcode=419    
 BEGIN      
      
   Select POM.AutoId,PONo,FORMAT(PODate,'MM/dd/yyyy') as PODate,VM.VendorName,PORemarks,RecieveStockRemark,sm.AutoId as SAutoId,
   sm.StatusType as Status,BillNo,FORMAT(BillDate,'MM/dd/yyyy') as BillDate,emp.FirstName+' '+LastName as CreateBy from PurchaseOrderMaster AS POM    
   INNER JOIN VendorMaster VM ON POM.VenderAutoId=VM.AutoId  
   INNER JOIN StatusMaster sm on sm.AutoId=POM.Status and Category='POOrd'   
   inner join EmployeeMaster as emp on emp.AutoId=POM.CreatedBy
   where POM.vendortype=0 AND POM.AutoId=@POAutoId   
    
   Select PPM.AutoId,PPM.ProductAutoId,ProductId,PM.ProductName,UM.UnitType+' ('+Convert(varchar,PPM.QtyPerUnit)+') ' as UnitType, PPM.Unit,ISNULL(RemainPieces,0) RemainPieces,
   PPM.QtyPerUnit,Qty,TotalPieces,ISNULL(PPM.ReceivedPieces,0) as ReceivedPieces,    
   isnull(ReceivedPieces,0) as ReceivedPiece from PurchaseProductsMaster AS PPM    
   INNER JOIN ProductMaster AS PM ON PPM.ProductAutoId=PM.AutoId    
   INNER JOIN UnitMaster AS UM ON PPM.Unit=UM.AutoId    
   --left join POReceiveMaster as prm on prm.POAutoId=PPM.POAutoId
   --left join POReceiveProduct as prp on prp.POReceiveAutoId=prm.AutoId and prp.ProductAutoId=PPM.ProductAutoId and prp.UnitAutoId=PPM.Unit
   where PPM.POAutoId=@POAutoId   
 END        
END

 

