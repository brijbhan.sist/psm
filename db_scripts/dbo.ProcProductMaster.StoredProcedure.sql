ALTER PROCEDURE [dbo].[ProcProductMaster]                                                                                                                                                                           
@OpCode int=Null,                                                                                                                                                                                
@AutoId  int=Null,                                                                                                                                                                                
@CategoryAutoId int=Null,                                                                                                                                                                                
@SubcategoryAutoId  int=Null,                                                                                                                                                                                
@ProductId VARCHAR(12)=NULL,                                                                                                                                                                                
@ProductName VARCHAR(50)=NULL,                                                                                                                                                                                
@ProductLocation VARCHAR(20)=NULL,                                                                                                                                                                                
@SearchBy VARCHAR(20)=NULL,                                                                                                                                                                               
@EligibleforFree int =NULL,                                                                                                                                                                                
@ImageUrl varchar(250)=NULL,  
@ThumbnailImageUrl varchar(250)=NULL,                                                                                                                                                                                
@VendorAutoId int=NULL,                                                                                                                                                                                
@BrandAutoId int =null,                                                                                                                                                                            
@ReOrderMark int=NULL,                                                                                                                                                                                
@WHminPrice decimal(10,2)=null,                                                                                                                                                                                
@PackingDetailAutoId int=NULL,                                                                                                                                                                                
@EmpAutoId int=NULL,                                                                                                                                                                               
@UnitType INT=NULL,                                                                                                                                                                                
@Qty int=NULL,                                                                                                                                                                                
@Status int=NULL,                   
@MinPrice DECIMAL(8,2)=NULL,                       
@CostPrice DECIMAL(8,2)=NULL,                       
@Price DECIMAL(8,2)=NULL,                        
@SRP DECIMAL(8,2) =NULL,                             
@PreDefinedBarcode VARCHAR(50)= NULL,                                                   
@CommCode DECIMAL(10,4)= NULL,                                                                      
@BarcodeAutoId INT= NULL,                                                                                        
@IsAppyMLQty int = null,                                                                    
@IsAppyWeightQty int = null,                                                                                           
@MLQty DECIMAL(8,2) =NULL,                                                                                         
@WeightOz DECIMAL(8,2) =NULL,                                                                                                          
@FourImageUrl varchar(250)=null,
@IsShowOnWebsite INT= NULL,
@CheckSecurity varchar(250)=NULL,
@IsOutOfStock INT= NULL,
@txtDescription VARCHAR(max)= NULL,
@defaultImg VARCHAR(500)= NULL,
@ImageAutoId INT= NULL,
@OriginalImageUrl varchar(250)=NULL,

@PageIndex INT = 1,                                                                                                                  
@PageSize INT = 10,                                                                                                
@PackingAutoId INT=0,                                                                                                                                                    
@RecordCount INT =null,                                                                                                                               
@dtbulkUnit [DT_dtbulkUnit] readonly,      
@dtbulkImageUrl [DT_dtbulkImageUrl] readonly,  
@LocationStatus DT_LocationStatus readonly,
@isException bit out,                                                                                                                                                       
@exceptionMessage varchar(max) out                                                                                                                           
AS                                                                                                                                     
BEGIN                                                                                                                                             
 BEGIN TRY                                                                                                                                                       
		  SET @isException=0                                                                                                                                                                       
		  SET @exceptionMessage='Success'   
		  Declare @BarcodeType int  
 IF @OpCode=11                                                                                                                                                                           
 BEGIN                                                                                                                                                                             
	   IF EXISTS(SELECT [AutoId] FROM [dbo].[ProductMaster] WHERE [ProductId] = @ProductId)                                                                                                                                                        
	   BEGIN                                                                                                                                        
			 SET @isException=1                                                                                                                                        
			 SET @exceptionMessage='Product ID already exists.'      
	   END                            
	   ELSE if exists(SELECT [AutoId] FROM [dbo].[ProductMaster] WHERE replace(replace(ProductName,'  ',' '),'  ',' ') = @ProductName)                
	   BEGIN                                              
			 SET @isException=1                             
			 SET @exceptionMessage='Product name already exists.'                                                                             
	   END                            
	   ELSE                                           
	   BEGIN                                                                     
		   BEGIN TRY                                                                                                                        
		    BEGIN TRAN                                                                                                          
			INSERT INTO [dbo].[ProductMaster]([ProductId], [ProductName], [ProductLocation], [ImageUrl], [CategoryAutoId],                                                                                                               
			[SubcategoryAutoId],[Stock],VendorAutoId,ReOrderMark,MLQty,ProductStatus,CreateBy,CreateDate,ModifiedBy,ModifiedDate,                                                                                                                    
			BrandAutoId,P_CommCode,WeightOz,IsApply_ML,IsApply_Oz,NewSRP,[ThumbnailImageUrl],ThirdImage,OriginalImageUrl)                                                                                                                                          
			VALUES (@ProductId, TRIM(@ProductName), (Select top 1 CompanyId from CompanyDetails), @ImageUrl, @CategoryAutoId, @SubcategoryAutoId,                                                                                                                                                    
			0,@VendorAutoId,@ReOrderMark,@MLQty,(select ststus from @LocationStatus where LocationId=1) ,@EmpAutoId,GETDATE(),@EmpAutoId,GETDATE(),                                                                                                                                                                
			(CASE WHEN ISNULL(@BrandAutoId,0)=0 THEN NULL ELSE @BrandAutoId END),@CommCode,@WeightOz,@IsAppyMLQty,@IsAppyWeightQty,@SRP,
			@ThumbnailImageUrl,@FourImageUrl,@OriginalImageUrl)                                                                                     
			SET @AutoId = (SELECT SCOPE_IDENTITY()) 
			
			insert into RealProductLocation(ProductAutoId,LocationId,Status)
			select @AutoId,LocationId,ststus from @LocationStatus
			IF(DB_NAME()='psmnj.a1whm.com')                                                                                                                                                                             
			BEGIN                                                                                                                                         
				EXEC ProcProductMaster_ALL @ProductId=@ProductId                                                                                                                                                                            
			END                                                                                                     
                Exec dbo.ManageProductEmailSending @ProductId=@ProductId,@Status=1   
				select @AutoId as ProductId
			COMMIT TRANSACTION                                                                                                                                   
		  END TRY                        
		  BEGIN CATCH                                                                         
			ROLLBACK TRAN                                                       
						SET @isException=1                                                        
						SET @exceptionMessage='Oops! Something went wrong.Please try later.'                                            
			END CATCH                                         
          END                                                                                                             
 END                                                                                                               
 ELSE IF @OpCode=12                                                
 BEGIN                                                                                                   
	  IF EXISTS(SELECT [UnitType] FROM dbo.PackingDetails WHERE [UnitType] = @UnitType AND [ProductAutoId]=@AutoId)              
	  BEGIN                                                                                                   
			SET @isException=1                                                                                       
			SET @exceptionMessage='Packing details already added.'                                                                                                   
	  END                                                                                                                                               
	  ELSE                                                                                                                               
	  BEGIN                                                                                                                                                                           
	  BEGIN TRY                                                                                                                                          
		 BEGIN TRAN                                                                                                                 
			IF @PreDefinedBarcode is null or ltrim(rtrim(@PreDefinedBarcode))=''                                                                                                                                                          
			BEGIN                                                                                                                                        
				SET @PreDefinedBarcode = (SELECT CONVERT(VARCHAR(50),CONVERT(NUMERIC(12,0),RAND() * 999999999999))) 
				SET @BarcodeType=1
			END  
			ELSE
			BEGIN
				SET @BarcodeType=2
			END
			IF NOT EXISTS(SELECT [Barcode] FROM [dbo].[ItemBarcode] WHERE [Barcode]=@PreDefinedBarcode)  
			BEGIN                                                                                                                                                                                        
				INSERT INTO [dbo].[PackingDetails] (WHminPrice,[UnitType],[Qty],[MinPrice],[CostPrice],[Price],                                                                                                                   
				[ProductAutoId],EligibleforFree,CreateBy,CreateDate,ModifiedBy,UpdateDate)                                                                                                                                                              
				VALUES (@WHminPrice,@UnitType,(case when @UnitType=3 then 1 else @Qty end), @MinPrice, @CostPrice, @Price,                                                                                                                     
				@AutoId,@EligibleforFree,@EmpAutoId,GETDATE(),@EmpAutoId,GETDATE())        
				
				SET @PackingAutoId=SCOPE_IDENTITY()                        
                                                                                                                    
				insert into PackingDetailsUpdateLog(ProductAutoId,UnitType,MinPrice,CostPrice,Price,WHminPrice,UpdatedByAutoId,DateTime)                                                                                                               
				select @AutoId,pd.UnitType,pd.MinPrice,pd.CostPrice,pd.Price,pd.WHminPrice,@EmpAutoId,GETDATE() from PackingDetails pd                                           
				where ProductAutoId = @AutoId and UnitType = @UnitType  
				
				if NOT EXISTS(select AutoId from ProductMaster where AutoId=@AutoId AND PackingAutoId=@UnitType)              
				BEGIN                                                                           
					UPDATE ProductMaster SET PackingAutoId=@UnitType WHERE AutoId=@AutoId                                                                                                                                                       
				END                                                                                                                      
				IF(DB_NAME()='psmnj.a1whm.com')                                                                                                                                                                             
				BEGIN                                                                                           
					EXEC ProcPackingtMaster_CT @ProductAutoId=@AutoId,@EmpAutoId=@EmpAutoId                                                                                                                                    
					EXEC ProcPackingtMaster_PA @ProductAutoId=@AutoId,@EmpAutoId=@EmpAutoId                                                                                                                                                      
					EXEC ProcPackingtMaster_NPA @ProductAutoId=@AutoId,@EmpAutoId=@EmpAutoId         
					EXEC ProcPackingtMaster_WPA @ProductAutoId=@AutoId,@EmpAutoId=@EmpAutoId
					EXEC ProcPackingtMaster_NY @ProductAutoId=@AutoId,@EmpAutoId=@EmpAutoId
				    EXEC ProcPackingtMaster_EasyNJ @ProductAutoId=@AutoId,@EmpAutoId=@EmpAutoId
				END                                                                                                                                                                                                                                                           
				set @ProductId=(select ProductId from ProductMaster  where AutoId=@AutoId)                                                                                                                  
				EXEC [dbo].[Proc_BarCodeMaster_ALLDB]                                                                                                                                                             
				@ProductId=@ProductId,                                                                             
				@UnitAutoId=@UnitType,                                                                                                                                                            
				@Barcode=@PreDefinedBarcode,
				@BarcodeType=@BarcodeType
			END                                                                                                                               
			ELSE                                                                                        
			BEGIN                                                                                            
				SET @isException=1                       
				SET @exceptionMessage='Barcode already assigned to a product.'                                                 
			END                                                                                              
		COMMIT TRANSACTION                                                                                  
		END TRY        
		BEGIN CATCH                                                                                                                                    
		ROLLBACK TRAN                                                                                         
			SET @isException=1                                                                                                     
			SET @exceptionMessage='Oops, Something went wrong .Please try again.'                                                                                           
		END CATCH                                                                   
		END                                                                                                         
			Exec [dbo].[ProcProductMaster] @Opcode=401,@ProductId=@ProductId,@EmpAutoId=@EmpAutoId,@PackingAutoId=@PackingAutoId,@isException=1 ,@exceptionMessage=''                                                         
 END                                                                                                                                                                 
 ELSE IF @Opcode=13                                                                                                                                                   
 BEGIN                                                                                                                       
	--set @PreDefinedBarcode=[dbo].[funItemBarCode](@PreDefinedBarcode)                                                                                                                                                    
	IF EXISTS(SELECT [Barcode] FROM [dbo].[ItemBarcode] WHERE [Barcode]=@PreDefinedBarcode)                                                                                                     
	BEGIN  
		SET @isException=1                                                 
		SET @exceptionMessage='Barcode already exists to a product.' 
	END
	ELSE
	BEGIN
		BEGIN TRY                                                                                                                                                                                
			BEGIN TRAN                                                                                      
			SET @UnitType = (SELECT [UnitType] FROM [dbo].[PackingDetails] WHERE [AutoId]=@PackingDetailAutoId)                                                                                                                        
			set @ProductId=(select ProductId from ProductMaster  where AutoId=@AutoId)
			
			EXEC [dbo].[Proc_BarCodeMaster_ALLDB]                                                                                                                            
			@ProductId=@ProductId,                                                                                                                                                            
			@UnitAutoId=@UnitType,                                                                              
			@Barcode=@PreDefinedBarcode
			,@BarcodeType=2
			Exec [dbo].[ProcProductMaster] @Opcode=401,@ProductId=@ProductId,@PreDefinedBarcode=@PreDefinedBarcode,            
			@EmpAutoId=@EmpAutoId,@PackingAutoId=@PackingDetailAutoId,@isException=1 ,@exceptionMessage=''     
			
			SELECT DISTINCT IB.[AutoId],IB.[ProductAutoId],IB.[UnitAutoId],IB.[Barcode],um.UnitType as UnitName,pm.ProductId
			,pm.ProductName,ISNULL((
			select top 1 isnull(format(OrderDate,'MM/dd/yyyy'),'') as LastUseDate from OrderItemMaster as oim 
			inner join OrderMaster as om on om.AutoId=oim.OrderAutoId
			where oim.Barcode=ib.Barcode
			order by om.OrderDate desc
			),'') as LastUsedDate,ISNULL((
			select count(Barcode) from OrderItemMaster as oim 
			inner join OrderMaster as om on om.AutoId=oim.OrderAutoId
			where oim.Barcode=ib.Barcode),0)
			 as Totalcount FROM [dbo].[ItemBarcode] As IB
			inner join UnitMaster um on um.AutoId=IB.UnitAutoId
			inner join ProductMaster pm on pm.AutoId=IB.ProductAutoId
			WHERE IB.Barcode=@PreDefinedBarcode AND [UnitAutoId]=@UnitType 

			COMMIT TRAN                                                                          
		END TRY                                                                                        
		BEGIN CATCH                                                                                         
		ROLLBACK TRAN                                                                      
			SET @isException=1                                                 
			SET @exceptionMessage='Oops, Something went wrong .Please try again.'                        
		END CATCH  
	END                                                
 END                                                                                    
 ELSE IF @OpCode=21                                                                                                                                            
 BEGIN                                                                                                                                              
	IF EXISTS(SELECT [AutoId] FROM [dbo].[ProductMaster] WHERE (replace(replace(ProductName,'  ',' '),'  ',' ') = @ProductName
	AND AutoId!=@AutoId )) 
	BEGIN                                                                                                                                                                                
		SET @isException=1                                                                                             
		SET @exceptionMessage='Product name already exists.'                                                                                                                                                                                
	END                                                                                        
	ELSE IF EXISTS(SELECT [AutoId] FROM [dbo].[ProductMaster] WHERE (ProductId=@ProductId AND AutoId!=@AutoId ))                                                                                                         
	BEGIN                                                                                                                                
		SET @isException=1                                                                                                                                                            
		SET @exceptionMessage='Product ID already exists.'                                                                                                                                                                                
	END                                                                                         
	ELSE                                                                                                                                                                                 
	BEGIN   
	    Declare @BasePrice decimal(18,2)
	    IF EXISTS(Select Price from PackingDetails where ProductAutoId=@AutoId AND UnitType=3)
		BEGIN
			 SELECT @BasePrice=ISNULL(Price,0) from PackingDetails where ProductAutoId=@AutoId AND UnitType=3
		END
		IF (ISNULL(@SRP,0)>=ISNULL(@BasePrice,0))
		BEGIN

			declare @location varchar(10),@locationId int 
			set @location=( select REPLACE(DB_NAME(),'.a1whm.com',''))
			set @locationId=(select AutoId from LocationMaster where Location=@location)
			select @locationId

			Insert into ProductMasterLog
			(ProductId,ProductName,ProductLocation,ImageUrl,CategoryAutoId,SubcategoryAutoId,Stock,ProductType,TaxRate,PackingAutoId,
			VendorAutoId,ReOrderMark,MLQty,UpdateDate,ProductStatus,CreateBy,CreateDate,ModifiedBy,ModifiedDate,BrandAutoId,P_CommCode,
			WeightOz,IsApply_ML,IsApply_Oz,ThumbnailImageUrl,ThirdImage,NewSRP,ShowOnWebsite,IsOutOfStock,Description,P_SRP,CurrentStock,
			CheckImageURLExistense,CheckThirdImageExistense,Product_ShowOnWebsite,OriginalImageUrl) 
			Select ProductId,ProductName,ProductLocation,ImageUrl,CategoryAutoId,SubcategoryAutoId,Stock,ProductType,TaxRate,
			PackingAutoId,VendorAutoId,ReOrderMark,MLQty,UpdateDate,ProductStatus,CreateBy,CreateDate,ModifiedBy,ModifiedDate,
			BrandAutoId,P_CommCode,WeightOz,IsApply_ML,IsApply_Oz,ThumbnailImageUrl,ThirdImage,NewSRP,ShowOnWebsite,IsOutOfStock,
			Description,P_SRP,CurrentStock,CheckImageURLExistense,CheckThirdImageExistense,Product_ShowOnWebsite,OriginalImageUrl
			from ProductMaster WHERE AutoId=@AutoId

			UPDATE [dbo].[ProductMaster] SET ProductId=@ProductId,[ProductName]=TRIM(@ProductName),[ImageUrl]=@ImageUrl,
			[CategoryAutoId]=@CategoryAutoId,ReOrderMark=ISNULL((Case when ISNULL(@Qty,0)=0 then @ReOrderMark else  @ReOrderMark*ISNULL(@Qty,1) end),0)
			,[SubcategoryAutoId]=@SubcategoryAutoId,
			VendorAutoId=@VendorAutoId  ,                           
			MLQty=@MLQty, ProductStatus=(select ststus from @LocationStatus where LocationId=@locationId),
			BrandAutoId=(CASE WHEN ISNULL(@BrandAutoId,0)=0 THEN NULL ELSE @BrandAutoId END),                                                                      
			ModifiedBy=@EmpAutoId,ModifiedDate=GETDATE(),P_CommCode=@CommCode,WeightOz=@WeightOz,IsApply_ML=@IsAppyMLQty,
			IsApply_Oz=@IsAppyWeightQty,NewSRP=@SRP,
			[ThumbnailImageUrl]=@ThumbnailImageUrl,ThirdImage=@FourImageUrl,CreateBy=@EmpAutoId,CreateDate=GetDate(),
			OriginalImageUrl=@OriginalImageUrl
			WHERE AutoId=@AutoId 
			
			IF EXISTS(select status from RealProductLocation where ProductAutoId=@AutoId)
			BEGIN
				update RealProductLocation set Status=(select ststus from @LocationStatus where LocationId=@locationId)
				where ProductAutoId=@AutoId and LocationId=@locationId
			END
			ELSE
			BEGIN
				INSERT RealProductLocation(LocationId,ProductAutoId,Status)
				select LocationId,@AutoId,ststus from @LocationStatus
			END
			IF(DB_NAME()='psmnj.a1whm.com')                                                           
			BEGIN                             
				EXEC ProcProductMaster_ALL @ProductId=@ProductId                                      
			END 
			 Exec dbo.ManageProductEmailSending @ProductId=@ProductId,@Status=2     
		END 
		ELSE
		BEGIN
		    SET @isException=1                                                                                                                                                            
		    SET @exceptionMessage='SRP can''t be less than  base price of piece.'  
		END
	END                                  
 END                                                                     
 ELSE IF @OpCode=22                                                                                                                                                
 BEGIN                                                                                                   
   IF  (SELECT count(*) FROM ProductPricingInPriceLevel WHERE ProductAutoId=@AutoId AND UnitAutoId=@UnitType AND                                                                                   
   CustomPrice<@CostPrice)=0                                                                                                                                         
   BEGIN                                                                                                                                                                 
   UPDATE [dbo].[PackingDetails] SET WHminPrice=@WHminPrice, [UnitType]=@UnitType,[Qty]=(case when @UnitType=3 then 1 else @Qty end ), 
   [MinPrice]=@MinPrice,[CostPrice]=@CostPrice,[Price]=@Price,                                                 
   [ProductAutoId]=@AutoId,UpdateDate=GETDATE(),EligibleforFree=@EligibleforFree,                                                        
   ModifiedBy=@EmpAutoId WHERE [AutoId] = @PackingDetailAutoId                                                                                                                        
   update ProductMaster set UpdateDate=GETDATE() where [AutoId]=@AutoId                                                                                                        
                                                                                                      
   SELECT @ProductId=ProductId from ProductMaster Where [AutoId]=@AutoId                                                                                                        
   Exec [dbo].[ProcProductMaster] @Opcode=401,@ProductId=@ProductId,@EmpAutoId=@EmpAutoId,@PackingAutoId=@PackingDetailAutoId,@isException=1 ,@exceptionMessage=''                                                                                                                                                                     
  IF(DB_NAME()='psmnj.a1whm.com')                                                                           
  BEGIN                                                                                                                                                                            
   EXEC ProcPackingtMaster_CT @ProductAutoId=@AutoId,@EmpAutoId=@EmpAutoId                                                                                                                                    
   EXEC ProcPackingtMaster_PA @ProductAutoId=@AutoId,@EmpAutoId=@EmpAutoId                                                                                                                                                      
   EXEC ProcPackingtMaster_NPA @ProductAutoId=@AutoId,@EmpAutoId=@EmpAutoId         
   EXEC ProcPackingtMaster_WPA @ProductAutoId=@AutoId,@EmpAutoId=@EmpAutoId 
   EXEC ProcPackingtMaster_NY @ProductAutoId=@AutoId,@EmpAutoId=@EmpAutoId                                        
   EXEC ProcPackingtMaster_EasyNJ @ProductAutoId=@AutoId,@EmpAutoId=@EmpAutoId                                        
   
  END                                                                 
  END                                                                            
  ELSE                            
  BEGIN                                                       
    SET @isException=1                                                                                          
    SET @exceptionMessage='EXISTS'                     
 select distinct pm.ProductId,pm.ProductName,(select UnitType from unitMaster as um where um.Autoid=pl.UnitAutoId) as UnitType,pll.PriceLevelName,                                          
    pl.CustomPrice,CostPrice                                                                            
    from ProductPricingInPriceLevel as pl                                    
    inner join PackingDetails as pd on pd.ProductAutoId=pl.ProductAutoId and pd.UnitType=pl.UnitAutoId                                                  
    inner join PriceLevelMaster as pll on pl.PriceLevelAutoId=pll.AutoId                                                                                                                                                                         
    inner join ProductMaster as pm on pm.AutoId=pl.ProductAutoId                                                                                                                                   
   where pl.ProductAutoId=@AutoId  AND UnitAutoId=@UnitType                                                                                                                                                                        
    AND ISNULL(CustomPrice,0)<@CostPrice                                                                               
    and CustomPrice is not null                                                                                                                  
  END                                                                                                                                                                       
                                                      
 END                                                                                                                                                                                
 ELSE IF @Opcode=23                                                                                                                                                                            
 BEGIN                                                                                                                      
  IF NOT EXISTS(SELECT [Barcode] FROM [dbo].[ItemBarcode] WHERE [Barcode]=@PreDefinedBarcode and AutoId!=@BarcodeAutoId)              
  BEGIN                                                                                                                                       
 BEGIN TRY                                                                                                                        
  BEGIN TRAN                                                                                                                             
  UPDATE [dbo].[ItemBarcode] SET [Barcode]=@PreDefinedBarcode WHERE [AutoId]=@BarcodeAutoId                                                                                                                                                  
   SET @AutoId=(SELECT ProductAutoId from [dbo].[ItemBarcode] WHERE [AutoId]=@BarcodeAutoId)                                                                                                  
   SET @UnitType=(SELECT UnitAutoId from [dbo].[ItemBarcode] WHERE [AutoId]=@BarcodeAutoId)                                       
   set @ProductId=(select ProductId from ProductMaster  where AutoId=@AutoId)    
                                                                                                                                               
   EXEC [dbo].[Proc_BarCodeMaster_ALLDB]         
   @ProductId=@ProductId,                                 
   @UnitAutoId=@UnitType,                                                                                               
   @Barcode=@PreDefinedBarcode                                                                                   
                                                                   
      COMMIT TRAN                                                  
  END TRY                                             
  BEGIN CATCH                                                                                                                                                      
  ROLLBACK TRAN                
   SET @isException=1                                                                               
   SET @exceptionMessage='Oops, Something went wrong .Please try again.'                                                                                                                                                                 
  END CATCH                                                                                                                                                     
  END                                                                 
  ELSE                                                                                                                                                                   
  BEGIN                                                                                                                                                                                
   SET @isException=1                                                                                                                    
   SET @exceptionMessage='Barcode already exists. Enter another barcode.'                                                                                                                                            
 END                                             
 END                                                                                                     
	Else IF @Opcode=31                                                      
	BEGIN                                                                                                                       
		                                                                                                                                                                
			SET @isException=0                                                                                           
			Exec [dbo].[ProcProductMaster] @Opcode=401,@ProductId=@ProductId,@EmpAutoId=@EmpAutoId,@isException=1 ,@exceptionMessage=''                        
			SET @AutoId = (SELECT [AutoId] FROM [dbo].[ProductMaster] WHERE [ProductId]=@ProductId)                                                                      
			if(DB_NAME()='psmnj.a1whm.com')                                                                                                                 
			BEGIN                    
				EXEC [dbo].[ProcProductDelete_ALL] @ProductId=@ProductId,@Status = @Status OUTPUT
				if(@Status=1)   
				begin
				SET @isException=1                                                                           
				SET @exceptionMessage='This product cannot be deleted because Orders for this product is received.' 
				end
				                   
			END                      
			ELSE                      
			BEGIN 
				BEGIN TRY                                                                                  
				BEGIN TRAN  
					DELETE FROM [dbo].[ItemBarcode] WHERE [ProductAutoId]=@AutoId                                                                                                                                                  
					DELETE FROM [dbo].[PackingDetails] WHERE [ProductAutoId]=@AutoId                                                                                                                     
					DELETE FROM [dbo].[ProductMaster] WHERE [AutoId]=@AutoId
				COMMIT TRANSACTION                                                                                                                            
				END TRY               
				BEGIN CATCH                                                                                                                                                                                
				ROLLBACK TRAN                                                                                                             
					SET @isException=1                                                                           
					SET @exceptionMessage='Oops, Something went wrong .Please try again.'                                                                                    
				END CATCH  
			END                                                                                                                                                            
		                                                                                                                  
	END                                                                                                                                                 
 Else IF @Opcode=33                                                                                     
 BEGIN                                                          
	SELECT @PreDefinedBarcode=Barcode,@UnitType=UnitAutoId,@AutoId=ProductAutoId FROM [dbo].[ItemBarcode] WHERE [AutoId]=@BarcodeAutoId                                                     
	Select @ProductId=ProductId from ProductMaster Where AutoId=@AutoId                                                                                                    
	Exec [dbo].[ProcProductMaster] @Opcode=401,@ProductId=@ProductId,@PreDefinedBarcode=@PreDefinedBarcode,@EmpAutoId=@EmpAutoId,@UnitType=@UnitType,@isException=1 ,@exceptionMessage=''                                                                        
  
                                                                                                       
	Exec [dbo].[Proc_BarCodeDelete_ALLDB]                                                                                                 
	@Barcode=@PreDefinedBarcode                                                                          
 END                                                               
 ELSE IF @OpCode=41                                                                                       
 BEGIN                                       
   SELECT [AutoId], [CategoryName] FROM [dbo].[CategoryMaster] WHERE [Status]=1 ORDER BY  CategoryName ASC                                                                                                                    
   SELECT AutoId,VendorName FROM VENDORMASTER WHERE Status=1     order by     VendorName asc                               
   SELECT AutoId,BrandName FROM BrandMaster WHERE Status=1     order by     BrandName asc                                                                                        
   SELECT AutoId,CommissionCode,C_DisplayName from Tbl_CommissionMaster where Status=1   order by CommissionCode                            
 END                                                 
 ELSE IF @Opcode=42                                                                                                                                                                                
 BEGIN                                                                         
 SELECT [AutoId], [SubcategoryName] FROM [dbo].[SubCategoryMaster] WHERE [Status]=1 AND [CategoryAutoId]=@CategoryAutoId  ORDER BY  SubcategoryName ASC  
 END                                                                                                                                                                                
 ELSE IF @Opcode=43                                             
 BEGIN                                                                                                       
  SELECT * FROM [dbo].[UnitMaster]                                                                                        
 END                                                                                   
 ELSE IF @Opcode=44                                                                                                                                                                                
 BEGIN                                                                                        
                                                                    
   SELECT ROW_NUMBER() OVER(ORDER BY [ProductId] asc) AS RowNumber, * into #Results2  FROM                                                                                                                                                                                
   (                                                                               
   SELECT distinct PM.[AutoId],[ProductId],[ProductName],CM.CategoryName AS Category,SCM.SubcategoryName    
   AS Subcategory,isnull(Qty,1) as Qty,um.UnitType,   
   ISNULL(convert(varchar,(Stock/(case (select Qty from PackingDetails as temp1                                                                                                                                                 
   where temp1.productAutoId = pm.AutoId and temp1.UnitType =pm.PackingAutoId) when 0 then 1 else (select Qty from PackingDetails as temp1                                            
   where temp1.productAutoId = pm.AutoId and temp1.UnitType =pm.PackingAutoId) end)))                                                                               
   +' ('+um.UnitType+')'                                                                                                        
   ,'') as [Stock] ,case when ISNULL(Qty,1)=0 then pm.ReOrderMark else CAST((isnull(pm.ReOrderMark,0)) as int)/ISnull(Qty,1) end 
   as ReOrderMark,
   
   ImageUrl,ThumbnailImageUrl,(case when pm.ProductStatus=1 then 'Active' else 'Inactive' end) as Status,    
   (select BrandName from BrandMaster as bm where bm.autoid=pm.Brandautoid) as  BrandName ,                                                                                                   
   (select top 1 cm.C_DisplayName from Tbl_CommissionMaster as cm where cm.CommissionCode=pm.P_CommCode)  as  CommCode                                                                                                                                    
   FROM ProductMaster AS PM                                                                                                                                   
   INNER JOIN [dbo].[CategoryMaster] AS CM ON PM.CategoryAutoId = CM.AutoId                                                                                                                                          
   INNER JOIN [dbo].[SubCategoryMaster] AS SCM ON PM.SubcategoryAutoId = SCM.AutoId                                                                                                                         
     
	left join dbo.UnitMaster as um on PM.PackingAutoId=um.AutoId                                                                                            
	left join ItemBarcode as bc on bc.ProductAutoId=pm.AutoId                                                                                                                                    
	left join PackingDetails as pd on pd.ProductAutoId=pm.AutoId and pd.UnitType=pm.PackingAutoId
	and pm.AutoId=pd.ProductAutoId                                                                            
	AND (@ProductLocation IS NULL OR @ProductLocation='' OR pd.Location LIKE '%' + @ProductLocation + '%')                                                                                            
	WHERE (@CategoryAutoId IS NULL OR @CategoryAutoId='' OR PM.[CategoryAutoId] = @CategoryAutoId)                                                                                     
	and (@Status IS NULL OR @Status=2 OR PM.ProductStatus = @Status)           
	AND (@BrandAutoId IS NULL OR @BrandAutoId=0 OR PM.BrandAutoId = @BrandAutoId) 
	AND (@SubcategoryAutoId IS NULL OR @SubcategoryAutoId='' OR PM.[SubcategoryAutoId] = @SubcategoryAutoId)    
	AND (@ProductId IS NULL OR @ProductId='' OR PM.[ProductId] LIKE '%'+ @ProductId +'%')            
	AND (@ProductName IS NULL OR @ProductName='' OR PM.ProductName LIKE '%' + @ProductName + '%')                                                                                                                                             
	AND (@PreDefinedBarcode IS NULL OR @PreDefinedBarcode='' OR bc.Barcode LIKE '%' + @PreDefinedBarcode + '%')     
	AND (@ProductLocation IS NULL OR @ProductLocation='' OR pd.Location LIKE '%' + @ProductLocation + '%')                                                                          
	AND (ISNULL(@SearchBy,'0')='0'  OR (@SearchBy ='=' AND Stock=@Qty) OR (@SearchBy ='>' AND Stock>@Qty)                                                                                         
	OR (@SearchBy ='<' AND Stock<@Qty))   	 
   ) AS t ORDER BY [ProductId] asc                                                                                    
                                                                                                          
  SELECT COUNT([ProductId]) AS RecordCount, case when @PageSize=0 then COUNT(*) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Results2                              
                                                                                                                                                            
  SELECT * FROM #Results2                                             
  WHERE @PageSize=0 or  RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1                                        
 END                                                                                                                                             
 ELSE IF @Opcode=45                                                                                                                          
 BEGIN                                                                                                                   
 SELECT PD.AutoId,Location,WHminPrice,ISNULL(pm.PackingAutoId,0) AS PackingAutoId,UM.UnitType AS UnitType 
 ,[Qty],[MinPrice],[CostPrice],[Price],case when PD.EligibleforFree=1 then 'Yes' else 'No'                                                                  
  end  as EligibleforFree FROM [dbo].[PackingDetails] AS PD  --EligibleForFree column added by Rizwan Ahmad on 11/29/2019                                                                                                        
 INNER JOIN [dbo].[UnitMaster] AS UM ON UM.[AutoId] = PD.[UnitType]                                                                                                                 
 LEFT JOIN ProductMaster as pm on pm.AutoId=pd.ProductAutoId and pd.UnitType=pm.PackingAutoId          
 WHERE PD.[ProductAutoId]=@AutoId                                                  
 END                                                                                                                
 ELSE IF @Opcode=46                                                                                                  
 BEGIN                                                                                                                                                                 
    SET @CategoryAutoId=(select CategoryAutoId from ProductMaster where AutoId=@AutoId)                                                                
    SELECT [AutoId], [SubcategoryName] FROM [dbo].[SubCategoryMaster] WHERE [Status]=1 AND [CategoryAutoId]=@CategoryAutoId  
    SELECT [AutoId], [CategoryName] FROM [dbo].[CategoryMaster] WHERE [Status]=1                                                                
    SELECT * FROM [dbo].[UnitMaster]         
	
	set @Qty=(select isnull(Qty,1) as Qty from ProductMaster as pm 
	inner join PackingDetails as pd on pd.ProductAutoId=pm.AutoId and pd.UnitType=pm.PackingAutoId
	and pm.AutoId=pd.ProductAutoId 
	and pm.AutoId=@AutoId)

    SELECT ISNULL((select top 1 pm.AutoId from ProductMaster as pm WHERE pm.ProductId < pm1.ProductId  
	order by pm.ProductId desc  ),0 ) as PreviousProduct 
    , ISNULL((select top 1 pm.AutoId from ProductMaster as pm WHERE pm.ProductId > pm1.ProductId 
	order by pm.ProductId asc),0 ) as NextProduct                                                              
    ,[AutoId],[ProductId],[ProductName],[ProductLocation],OriginalImageUrl as [ImageUrl],[CategoryAutoId],[SubcategoryAutoId],[Stock],VendorAutoId,
   case when ISNULL(@Qty,0)=0 then pm1.ReOrderMark else CAST((isnull(pm1.ReOrderMark,0)) as int)/ISnull(@Qty,0) end as ReOrderMark,pm1.ReOrderMark as PieceReOrderMark,MLQty,ProductStatus as Status,BrandAutoId,cast(isnull(pm1.P_CommCode,0) as decimal(10,4))  as 
	CommCode,WeightOz,isnull(IsApply_ML,0)                                                         
    as IsApply_ML,isnull(IsApply_Oz,0) as IsApply_Oz,    ISNULL(NewSRP,0) as SRP,ShowOnWebsite,IsOutOfStock,Description,
	pm1.CurrentStock ,(select UnitType from UnitMaster as UM  where UM.AutoId=pm1.PackingAutoId ) as DefaultUnitType,
	(select UnitType from UnitMaster as UM  where UM.AutoId=pm1.PackingAutoId),@Qty as Qty
    FROM [dbo].[ProductMaster] as pm1 WHERE AutoId=@AutoId                                                           
                                                     
    SELECT                                                                                                                                                       
    PD.AutoId,Location,WHminPrice,UM.UnitType AS UnitType,ISNULL(pm.PackingAutoId,0) AS PackingAutoId,                                                                   
    PD.Qty, PD.MinPrice, PD.[CostPrice], PD.Price, case when EligibleforFree=1 then 'Yes' else 'No' end as EligibleforFree
    FROM [dbo].[PackingDetails] AS PD                                                                                                                                                                                 
    INNER JOIN [dbo].[UnitMaster] AS UM ON PD.UnitType= UM.[AutoId]                                                              
    LEFT JOIN ProductMaster as pm on pm.AutoId=pd.ProductAutoId and pd.UnitType=pm.PackingAutoId                                                         
    WHERE PD.ProductAutoId = @AutoId                                                                                                                   
    order by UM.Autoid asc                                    
                                                                                      
    SELECT AutoId,VendorName FROM VENDORMASTER WHERE Status=1  order by VendorName asc                                                                                             
    SELECT AutoId,BrandName FROM BrandMaster WHERE Status=1    order by BrandName asc                                             
    SELECT AutoId,CommissionCode,C_DisplayName from Tbl_CommissionMaster where Status=1       order by CommissionCode asc       
	SELECT AutoId,ProductAutoId,ImageUrl, 
	(case when isDefault=0 then '' else 'defaultImage' end) as isDefault,
	Thumbnail_100,Thumbnaill_400
	FROM Tbl_ProductImage WHERE ProductAutoId=@AutoId

 END                                                 
 ELSE IF @Opcode=47                       
 BEGIN                                                                                                 
   SELECT [UnitType],Location,WHminPrice,[Qty],[MinPrice],[CostPrice],[Price],EligibleforFree FROM [dbo].[PackingDetails] AS PD   
   WHERE PD.[AutoId] = @PackingDetailAutoId                                         
 END                                                                                       
 ELSE IF @Opcode=48                                                                                               
 BEGIN                                      
   SET @UnitType = (SELECT [UnitType] FROM [dbo].[PackingDetails] WHERE [AutoId]=@PackingDetailAutoId)  
   SET @AutoId = (SELECT ProductAutoId FROM [dbo].[PackingDetails] WHERE [AutoId]=@PackingDetailAutoId)                  
   select @UnitType as UnitType,@AutoId as ProductAutoId
   
   IF EXISTS(SELECT * FROM ItemBarcode Where [ProductAutoId]=@AutoId AND [UnitAutoId]=@UnitType)
   BEGIN
	   SELECT DISTINCT IB.[AutoId],IB.[ProductAutoId],IB.[UnitAutoId],IB.[Barcode],um.UnitType as UnitName,pm.ProductId
	   ,pm.ProductName,(
		select top 1 isnull(format(OrderDate,'MM/dd/yyyy'),'') as LastUseDate from OrderItemMaster as oim 
		inner join OrderMaster as om on om.AutoId=oim.OrderAutoId
		where oim.Barcode=ib.Barcode
		order by om.OrderDate desc
		) as LastUsedDate,(
		select count(Barcode) from OrderItemMaster as oim 
		inner join OrderMaster as om on om.AutoId=oim.OrderAutoId
		where oim.Barcode=ib.Barcode
		) as Totalcount FROM [dbo].[ItemBarcode] As IB
	   inner join UnitMaster um on um.AutoId=IB.UnitAutoId
	   inner join ProductMaster pm on pm.AutoId=IB.ProductAutoId
	   WHERE IB.ProductAutoId=@AutoId AND [UnitAutoId]=@UnitType 
   END
   ELSE
   BEGIN
	   Select um.UnitType as UnitName,pm.ProductId
	   ,pm.ProductName FROM ProductMaster as PM
	   INNER JOIN UnitMaster as UM on UM.AutoId=@UnitType
	   Where PM.AutoId=@AutoId AND  UM.AutoId=@UnitType
   END
                        
                         
   SELECT [UnitAutoId], COUNT([Barcode]) AS BarcodeCount FROM [dbo].[ItemBarcode] WHERE [ProductAutoId]=@AutoId AND
   [UnitAutoId]=@UnitType  
                                                             
   GROUP BY [UnitAutoId]                                                                                                                                                                                 
 END                                                            
 ELSE IF @Opcode=24                                                                                                                                 
 BEGIN                                                                                                                                                                                
   DECLARE @ProductAutoid int  =(Select ProductAutoId from [PackingDetails] where AutoId=@AutoId)                                                                                                                                     
   SET @ProductId = (SELECT ProductId FROM [dbo].[ProductMaster] WHERE AutoId=@ProductAutoid)                                                                     
   SET @UnitType = (select UnitType from [PackingDetails] where AutoId=@AutoId)                                         
   EXEC [dbo].[ProcSetDefaultUnitALL]  @ProductId=@ProductId,  @UnitAutoID=@UnitType                                                                                        
                                                                                                                                                                                 
 END                                                                                                                                                  
 ELSE IF @Opcode=49                             
 BEGIN                               
   SET NOCOUNT ON;                                                               
   SELECT ROW_NUMBER() OVER(ORDER BY ProductName asc) AS RowNumber, * INTO #Results49 FROM                              
   (                                                                                                                                                                                
  SELECT PM.[AutoId],[ProductId],[ProductName],[ProductLocation],CM.CategoryName AS Category,SCM.SubcategoryName                                                          
   AS Subcategory,PD.LatestBarcode as barcode,um.UnitType as Unit                                                                      
   FROM ProductMaster AS PM                                                                                                                                     
   INNER JOIN [dbo].[CategoryMaster] AS CM ON PM.CategoryAutoId = CM.AutoId                                                           
   INNER JOIN [dbo].[SubCategoryMaster] AS SCM ON PM.SubcategoryAutoId = SCM.AutoId            
   INNER JOIN PackingDetails as PD on PD.ProductAutoId=PM.AutoId           
   INNER JOIN UnitMaster AS UM on UM.AutoId=PD.UnitType                                                                                          
   WHERE (@CategoryAutoId IS NULL OR @CategoryAutoId='' OR PM.[CategoryAutoId] = @CategoryAutoId)       
   AND (@SubcategoryAutoId IS NULL OR @SubcategoryAutoId='' OR PM.[SubcategoryAutoId] = @SubcategoryAutoId)                                  
   AND (@ProductId IS NULL OR @ProductId='' OR PM.[ProductId] LIKE '%'+ @ProductId +'%')                                                                                                                     
   AND (@ProductName IS NULL OR @ProductName='' OR  replace(replace(PM.ProductName,'  ',' '),'  ',' ') LIKE '%'+ @ProductName+'%')          
   AND ISNULL(PD.LatestBarcode,'')!=''          
   ) AS t order by ProductName asc                                                                                                                                                     
                                                                             
   SELECT COUNT([ProductId]) AS RecordCount, case when @PageSize=0 then COUNT([ProductId]) else @PageSize end AS PageSize,                                                              
    @PageIndex AS PageIndex FROM #Results49                                                               
   SELECT * FROM #Results49                                                                                                                                                                                
   WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))          
   select * from #Results49                                                                                                                        
 END                                                           
 Else if @OpCode=50                                                                   
 Begin                                                             
   SELECT PM.[AutoId],[ProductId],[ProductName],[ProductLocation],CM.CategoryName AS Category,SCM.SubcategoryName                                                          
   AS Subcategory,PD.LatestBarcode as barcode--,um.UnitType as Unit                                                                      
   FROM ProductMaster AS PM                                                                                                                   
   INNER JOIN [dbo].[CategoryMaster] AS CM ON PM.CategoryAutoId = CM.AutoId                                                           
   INNER JOIN [dbo].[SubCategoryMaster] AS SCM ON PM.SubcategoryAutoId = SCM.AutoId            
   INNER JOIN PackingDetails as PD on PD.ProductAutoId=PM.AutoId           
   INNER JOIN UnitMaster AS UM on UM.AutoId=PD.UnitType                                                                                          
   WHERE (@CategoryAutoId IS NULL OR @CategoryAutoId='' OR PM.[CategoryAutoId] = @CategoryAutoId)                                                                                                                                            
   AND (@SubcategoryAutoId IS NULL OR @SubcategoryAutoId='' OR PM.[SubcategoryAutoId] = @SubcategoryAutoId)                                  
   AND (@ProductId IS NULL OR @ProductId='' OR PM.[ProductId] LIKE '%'+ @ProductId +'%')                         
   AND (@ProductName IS NULL OR @ProductName='' OR replace(replace(PM.ProductName,'  ',' '),'  ',' ') LIKE '%'+ @ProductName+'%')          
   AND ISNULL(PD.LatestBarcode,'')!=''          
                  
 End                                                                                                    
	ELSE IF @OpCode=51                                                                                                
	BEGIN                                                    
		select @ProductId=ProductId from ProductMaster where AutoId=@AutoId                                                                                    
		IF DB_NAME() IN ('psmnj.a1whm.com','psm.a1whm.com','demo.a1whm.com')  --demo added on 12/05/2019 By Rizwan Ahmad                                                                                                                
		BEGIN                                                                                                                                                      
			SELECT um.AutoId as UnitType,ISNULL(Qty,1) AS Qty,isnull(MinPrice,0.00)as MinPrice,isnull(CostPrice,0.00) as CostPrice,                                           
			isnull(Price,0)as Price,isnull(WHminPrice,0) as WHminPrice,PD.Location,PackingStatus as Status,                                        
			case when ISNULL(PM.PackingAutoId,0)=PD.UnitType then 1 else 0 end as DefaultPack,                                                                                          
			[dbo].[CheckProductforSelling](@ProductId,UM.AutoId) as OrderAttachment,                                                         
			UM.UnitType AS UnitName,isnull(PD.EligibleforFree,0) as Free,isnull(PD.AutoId,0) as PackingId,
			Rack,Section,Row,BoxNo
			FROM UnitMaster AS UM
			LEFT JOIN PackingDetails AS PD ON UM.AutoId=PD.UnitType                                          
			AND PD.ProductAutoId=@AutoId                                          
			LEFT JOIN ProductMaster as pm on pm.AutoId=pd.ProductAutoId  order by UM.AutoId DESC                                                                                             
		END                                                                                                                  
		ELSE                                                        
		BEGIN                                                                                                              
			SELECT um.AutoId as UnitType,ISNULL(Qty,1) AS Qty,isnull(MinPrice,0.00)as MinPrice,isnull(CostPrice,0.00) as CostPrice,                                                                                                         
			isnull(Price,0)as Price,isnull(WHminPrice,0) as WHminPrice,PD.Location,PackingStatus as Status,                             
			case when ISNULL(PM.PackingAutoId,0)=PD.UnitType then 1 else 0 end as DefaultPack,                                         
			[dbo].[CheckProductforSelling](@ProductId,UM.AutoId) as OrderAttachment,                                        
			UM.UnitType AS UnitName,isnull(PD.EligibleforFree,0) as Free,isnull(PD.AutoId,0) as PackingId,
			Rack,Section,Row,BoxNo
			FROM UnitMaster AS UM                                                                           
			inner JOIN PackingDetails AS PD ON UM.AutoId=PD.UnitType                                          
			AND PD.ProductAutoId=@AutoId                                          
			inner JOIN ProductMaster as pm on pm.AutoId=pd.ProductAutoId  order by UM.AutoId DESC                                                                                
		END                                                                 
	END                                                                                 
    ELSE IF @OpCode=25                                                               
    BEGIN                                                                                                                      
	  BEGIN TRY                                                                                                                    
	  BEGIN TRAN  tr1

				SELECT @SRP=ISNULL(NewSRP,0) FROM ProductMaster WHERE AutoId=(select top 1 bu.ProductAutoId from @dtbulkUnit bu)
				SELECT @BasePrice=(select top 1 bu.BasePrice from @dtbulkUnit bu where UnitAutoId=3)
				IF(@SRP>=@BasePrice)
				BEGIN
				Declare @PrdctId int                                                            
				Declare @TempTable TABLE (ID INT IDENTITY(1, 1),ProductAutoId INT,UnitType INT)                                            
				IF EXISTS(Select ProductAutoId,UnitType from PackingDetails Where ProductAutoId IN 
				(select top 1 bu.ProductAutoId from @dtbulkUnit as bu)AND UnitType NOT IN  (SELECT bu.UnitAutoId FROM @dtbulkUnit as bu))                                          
				BEGIN                                                       
					INSERT into @TempTable                                                          
					Select ProductAutoId,UnitType from PackingDetails Where ProductAutoId IN 
					(select top 1 bu.ProductAutoId from @dtbulkUnit as bu)                                                         
					AND UnitType NOT IN  (SELECT bu.UnitAutoId FROM @dtbulkUnit as bu)                                                           
				END  
				
				IF EXISTS(Select AutoId from @dtbulkUnit as pd
				INNER JOIN PackingDetails as b on pd.ProductAutoId=b.ProductAutoId AND pd.UnitAutoId=b.UnitType
				where pd.BasePrice!=b.Price)
				BEGIN 
					insert into ProductPricingInPriceLevel_log([AutoId],[PriceLevelAutoId],[ProductAutoId],[UnitAutoId],[CustomPrice],
					[createDated],[UpdatedBy])
					SELECT [AutoId],[PriceLevelAutoId],[ProductAutoId],[UnitAutoId],[CustomPrice],GETDATE(),@EmpAutoId FROM
					ProductPricingInPriceLevel WHERE ProductAutoId=(select top 1 bu.ProductAutoId from @dtbulkUnit as bu) 

					DELETE FROM [ProductPricingInPriceLevel] WHERE ProductAutoId=(select top 1 bu.ProductAutoId from @dtbulkUnit as bu)
				END
		
				insert into PackingDetailsUpdateLog(ProductAutoId,UnitType,MinPrice,CostPrice,Price,WHminPrice,
				UpdatedByAutoId,DateTime,Reff_Code,Rack,Section,Row,BoxNo)                                                                             
				select pd.ProductAutoId,pd.UnitType,pd.MinPrice,pd.CostPrice,pd.Price,pd.WHminPrice,@EmpAutoId,
				GETDATE(),'ProcProductMaster-25',upper(Rack),Section,Row,BoxNo from PackingDetails pd                                                                                                                  
				inner join UnitMaster um on um.AutoId = pd.UnitType                                                                                      
				where ProductAutoId in (select top 1 bu.ProductAutoId from @dtbulkUnit bu)                                                                          
                                                                                                                                                                
				UPDATE PD SET  PD.UnitType=UM.UnitAutoId,pd.Qty=(case when UM.UnitAutoId=3 then 1 else UM.QTY end),
				pd.CostPrice=UM.COSTPRICE,pd.Price=UM.BASEPRICE,                     
				pd.EligibleforFree=UM.SETFree,pd.PackingStatus=UM.Status,pd.minprice=UM.RetailPrice,
				pd.WHminPrice=UM.WHPRICE,UpdateDate=GETDATE(),ModifiedBy=@EmpAutoId,
				pd.Rack=upper(UM.Rack),pd.Section=UM.Section,pd.Row=UM.Row,pd.BoxNo=UM.BoxNo
				FROM PackingDetails AS PD                                              
				INNER JOIN @dtbulkUnit AS UM ON UM.ProductAutoId=PD.ProductAutoId and UM.UnitAutoId=PD.UnitType                                                                                                                                                            
                                                                                        
				INSERT INTO [dbo].[PackingDetails] (WHminPrice,[UnitType],[Qty],[MinPrice],[CostPrice],[Price],EligibleforFree,
				[ProductAutoId],CreateBy,CreateDate,ModifiedBy,UpdateDate,PackingStatus,Rack,Section,Row,BoxNo)                                                                                                                                  
				SELECT WHPRICE,UnitAutoId, case when UnitAutoId=3 then 1 else Qty end, RetailPrice, COSTPRICE,
				BasePrice, SETFree, PRODUCTAUTOID,@EmpAutoId,GETDATE(),@EmpAutoId,GETDATE(),Status,upper(Rack),Section,Row,BoxNo                                                                                                                                                                      
				FROM @dtbulkUnit as temp                                                                                                                                                                      
				where UnitAutoId not in (select unittype from PackingDetails as t where t.ProductAutoId=temp.ProductAutoId)                                                   
           
		

				SET @AutoId=(SELECT TOP 1 PRODUCTAUTOID FROM @dtbulkUnit)                                                                  
				--SET @PreDefinedBarcode = (SELECT CONVERT(VARCHAR(50),CONVERT(NUMERIC(12,0),RAND() * 99999999999)))      
				

				
				set @productId=(select ProductId from ProductMaster where AutoId=@AutoId)                                                                                                                                     
				declare @Barcode varchar(250) 
				IF EXISTS(SELECT * FROM @dtbulkUnit WHERE UnitAutoId=1)
				BEGIN
					IF NOT EXISTS(Select * from ItemBarcode where ProductAutoId = @AutoId AND UnitAutoId =1)                    
					BEGIN                                                                             
						SET @Barcode=(select dbo.FN_GenerateUPCAbarcode('1'))                                                                                                      
						EXEC Proc_BarCodeMaster_ALLDB @ProductId=@productId,@UnitAutoId=1,@Barcode=@Barcode,@BarcodeType=1
					END 
				END
				IF EXISTS(SELECT * FROM @dtbulkUnit WHERE UnitAutoId=2)
				BEGIN
					IF NOT EXISTS(Select * from ItemBarcode where ProductAutoId = @AutoId AND UnitAutoId =2)                    
					BEGIN                                                                             
						SET @Barcode=(select dbo.FN_GenerateUPCAbarcode('2'))                                                                                                            
						EXEC Proc_BarCodeMaster_ALLDB @ProductId=@productId,@UnitAutoId=2,@Barcode=@Barcode,@BarcodeType=1
					END 
				END
			
				IF EXISTS(SELECT * FROM @dtbulkUnit WHERE UnitAutoId=3)
				BEGIN 
					IF NOT EXISTS(Select * from ItemBarcode where ProductAutoId = @AutoId AND UnitAutoId =3)                    
					BEGIN                                                                          
						SET @Barcode=(select dbo.FN_GenerateUPCAbarcode('3'))                                                                                                 
						EXEC Proc_BarCodeMaster_ALLDB @ProductId=@productId,@UnitAutoId=3,@Barcode=@Barcode,@BarcodeType=1
					END  
				END                                                   
				SELECT @PackingAutoId= bu.UnitAutoId from @dtbulkUnit bu where bu.SETDefault=1                                                 
		
				IF (@PackingAutoId is not null  and  @PackingAutoId!='')                                              
				BEGIN                                             
					Update ProductMaster SET PackingAutoId=@PackingAutoId WHERE AutoId=(select top 1 bu.ProductAutoId from @dtbulkUnit bu)                                                        
				END                              
                                                                                                                                                            
				IF(DB_NAME()='psmnj.a1whm.com')																					                      
				BEGIN                                                                 
					EXEC ProcPackingtMaster_CT @ProductAutoId=@AutoId,@EmpAutoId=@EmpAutoId                                                                                       
					EXEC ProcPackingtMaster_PA @ProductAutoId=@AutoId,@EmpAutoId=@EmpAutoId                                                                                                          
					EXEC ProcPackingtMaster_NPA @ProductAutoId=@AutoId,@EmpAutoId=@EmpAutoId                              
					EXEC ProcPackingtMaster_WPA @ProductAutoId=@AutoId,@EmpAutoId=@EmpAutoId
					EXEC ProcPackingtMaster_NY @ProductAutoId=@AutoId,@EmpAutoId=@EmpAutoId                              
					EXEC ProcPackingtMaster_EasyNJ @ProductAutoId=@AutoId,@EmpAutoId=@EmpAutoId
					EXEC [dbo].[ProcPriceLevelDeleteALL]
					Declare @i int=1,@Count int=0                                            
					SELECT @Count=COUNT(*) from @TempTable                                            
					IF (@Count>0)                                         
					BEGIN                                                       
						WHILE @i <= @Count                                                          
						BEGIN                                                          
							Select @ProductAutoID=ProductAutoId,@UnitType=UnitType from @TempTable where ID=@i                                                          
							Select @PrdctId=ProductId from ProductMaster Where AutoId=@ProductAutoID                                                           
							Exec ProcDeleteUnitofProduct @ProductId=@PrdctId,@UnitAutoid=@UnitType   --Modified on 12/04/2019 For deleting uncheck unit of product of all location                                                           
							SET @i=@i+1                                                          
						END                                                          
					END 

				END   
				ELSE
				BEGIN
					delete from [dbo].ProductPricingInPriceLevel where AutoId in
					(
					select ppl.AutoId  from [dbo].ProductPricingInPriceLevel as ppl
					inner join [dbo].PackingDetails as pd on pd.ProductAutoId=ppl.ProductAutoId and pd.UnitType=ppl.UnitAutoId
					where ppl.CustomPrice < pd.CostPrice
					)
				END
                                                          
		                                                         
		SET @ProductAutoid=(select top 1 bu.ProductAutoId from @dtbulkUnit bu) 
		exec ProcUpdatePriceEmailSend @ProductAutoid=@ProductAutoid                                                               
		SELECT @ProductId=ProductId from ProductMaster Where [AutoId]=@AutoId 
		END
		ELSE
		BEGIN
		     SET @isException=1                                                                                                                                                            
		     SET @exceptionMessage='Base price of Piece can''t be greater than  SRP.' 
		END
   COMMIT TRAN tr1                                                                                            
END TRY                                                                                            
BEGIN CATCH                                                             
		ROLLBACK TRANSACTION  tr1                                                                    
		SET @isException=1                                                                                                                   
		SET @exceptionMessage= 'Oops, Something went wrong .Please try again.'                                                                                                                                                                      
END CATCH                                                  
END                           
                                                                                                                   
 Else If @OpCode = 401    --Add By Rizwan Ahmad on 11/05/2019 05:52 AM                                                                                           
 begin                                                                                   
   Declare @ProductAId int,@TotalUnit int,@EmpName varchar(200)='',@LocationName varchar(100),@UnitName varchar(50),@ProperLocation varchar(100)--,@DefUnit varchar(25)                   
                          
   SELECT @ProductAId=AutoId from ProductMaster Where ProductId=@ProductId                                                                                                        
   SELECT  @EmpName=ISNULL(FirstName,'')+' '+ISNULL(LastName,'') FROM EmployeeMaster Where AutoId=@EmpAutoId                                                             
   Select @UnitName=ISNULL(UnitType,'') from UnitMaster Where AutoId=@UnitType                                                                                                    
   SELECT @LocationName=DB_NAME()                                                                  
   SELECT @ProperLocation='A1WHM - '+ UPPER(Replace(@LocationName,'.a1whm.com',''))                                                                                                 
   SELECT @TotalUnit=COUNT(*) from UnitMaster AS UM                                                                                                        
   INNER JOIN PackingDetails AS PD On                                                                                                        
   UM.AutoId=PD.UnitType where PD.ProductAutoId=@ProductAId 
   
   Select PM.AutoId as ProductId,PM.ProductId as Prdtid,PM.ProductName,@LocationName+PM.ImageUrl as ImageUrl,ISNULL(P_SRP,0) as SRP, --Updated on 11/24/2019 02:09 AM                                                                                    
   CM.CategoryName,SM.SubcategoryName,VM.VendorName,BM.BrandName,PM.ReOrderMark,MLQty,WeightOz,case when ProductStatus=1 then 'Active' else 'In Active' end as ProductStatus,Stock,--@DefUnit as DefaultPack,                                                                                    
   ISNUll(P_CommCode,0) as P_CommCode,case when IsApply_Oz=0 then 'No' else 'Yes' end as IsApply_Oz,case when IsApply_ML=0 then 'No' else 'Yes' end as IsApply_ML,                                                                                            
   GETDATE() as CreateDate,@ProperLocation as Location,@TotalUnit as TotalUnit, GETDATE() as UpdateDate,GETDATE() as DeleteDate,ISNULL(@EmpName,'')                                                                                                 
   as EmpName ,GETDATE() as DeleteDate,ISNULL(@PreDefinedBarcode,'') as Barcode,ISNULL(@UnitName,'') as UnitName from ProductMaster AS PM                                                             
   INNER JOIN CategoryMaster AS CM on PM.CategoryAutoId=CM.AutoId                                                                                    
   INNER JOIN SubCategoryMaster AS SM on PM.SubcategoryAutoId=SM.AutoId                                                                                    
   LEFT JOIN VendorMaster AS VM on PM.VendorAutoId=VM.AutoId                                                                                    
   LEFT JOIN BrandMaster AS BM on PM.BrandAutoId=BM.AutoId                                                                                    
   Where PM.AutoId=@ProductAId                            
   Select distinct UnitAutoId from ItemBarcode as IBC                                                                                                          
   Where IBC.ProductAutoId=@ProductAId                                        
                                                          
	IF (@PackingAutoId is not null and @PackingAutoId!=0)                             
	BEGIN                                                                                                      
		Select um.UnitType,CostPrice,WHminPrice,Qty,Price,IBC.Barcode as Barcode,MinPrice as RetailMinPrice,PD.ProductAutoId,um.AutoId AS UnitId,                                                             
		case when EligibleforFree=0 then 'No' else 'Yes' end as EligibleforFree,pd.Location      --Add By Rizwan Ahmad on 11/26/2019                                                                       
		from PackingDetails as PD                                                                                                                 
		Inner Join UnitMaster as um on um.AutoId=PD.UnitType                                                                                               
		left Join  ItemBarcode as IBC on  PD.ProductAutoId=IBC.ProductAutoId and ibc.UnitAutoId=PD.UnitType                                                                                                              
		Where PD.AutoId=@PackingAutoId                                                                                                       
	END                                                                      
	IF (@PackingAutoId='0')                                                                                                      
	BEGIN                                                                                                      
		Select um.UnitType,CostPrice,WHminPrice,Qty,Price,MinPrice as RetailMinPrice,PD.ProductAutoId,um.AutoId AS UnitId,pd.productAutoId,                  
		case when PM.PackingAutoId=1 then 'Case' when PM.PackingAutoId=2 then 'Box' when PM.PackingAutoId=3 then 'Peice' when PM.PackingAutoId=0 then '' end as DefPacking,                   
		case when EligibleforFree=0 then 'No' else 'Yes' end as EligibleforFree,pd.Location      --Add By Rizwan Ahmad on 11/26/2019                                                                                                                       
		from PackingDetails as PD                                                                                                                 
		Inner Join UnitMaster as um on um.AutoId=PD.UnitType                   
		Left JOIN ProductMaster AS PM ON                  
		PM.AutoId=PD.ProductAutoId AND PM.PackingAutoId=um.AutoId                                                                                                 
		Where PD.ProductAutoId=@BrandAutoId                                                                                              
	END                                                   
	Select EmailID as Email from EmailReceiverMaster Where Category='Developer'                                                                                                                     
 end           
 Else if @OpCode=26                                                                   
 Begin                                                             
	select ProductId,ProductName,PackingAutoId,(
	select pd.Location from PackingDetails as pd where pd.ProductAutoId=pm.autoid and pm.PackingAutoId=pd.UnitType
	) as Location from ProductMaster as pm where AutoId=@AutoId          
	select LatestBarcode as Barcode,um.UnitType,um.AutoId as UnitAutoId from PackingDetails as ibc 
	inner join UnitMaster as um on um.AutoId=ibc.UnitType           
	where ProductAutoId=@AutoId    and LatestBarcode!=''       
	order by um.AutoId desc      
 End      
 Else if @OpCode=27                                                                   
 Begin 
 select
 (
  select ProductId as PID,ProductName as PN,pd.LatestBarcode as BC,um.UnitType as UT,PM.PackingAutoId as PAI,um.AutoId as  UID,    
  (Select COUNT(ProductAutoId) from PackingDetails where ProductAutoId=PM.AutoId)  as TU,
  (
	select pdi.Location from PackingDetails as pdi where pdi.ProductAutoId=pm.autoid and pm.PackingAutoId=pdi.UnitType
	) as LOC
  from ProductMaster AS PM    
  INNER JOIN [dbo].[CategoryMaster] AS CM ON PM.CategoryAutoId = CM.AutoId                                                                                                                                          
  INNER JOIN [dbo].[SubCategoryMaster] AS SCM ON PM.SubcategoryAutoId = SCM.AutoId                                                        
  INNER join PackingDetails as pd on pd.ProductAutoId=pm.AutoId                                                                                                                                                                                
  INNER join dbo.UnitMaster as um on pd.UnitType=um.AutoId                                                                                                     
  AND (@ProductLocation IS NULL OR @ProductLocation='' OR pd.Location LIKE '%' + @ProductLocation + '%')                                                                                            
  WHERE (@CategoryAutoId IS NULL OR @CategoryAutoId='' OR PM.[CategoryAutoId] = @CategoryAutoId)                                                                                     
  and (@Status IS NULL OR @Status=2 OR PM.ProductStatus = @Status)  
  and (@BrandAutoId IS NULL OR @BrandAutoId=0 OR PM.BrandAutoId = @BrandAutoId)  
  AND (@SubcategoryAutoId IS NULL OR @SubcategoryAutoId='' OR PM.[SubcategoryAutoId] = @SubcategoryAutoId)    
  AND (@ProductId IS NULL OR @ProductId='' OR PM.[ProductId] LIKE '%'+ @ProductId +'%')            
  AND (@ProductName IS NULL OR @ProductName='' OR PM.ProductName LIKE '%' + @ProductName + '%')                                                                                                                                             
  AND (@PreDefinedBarcode IS NULL OR @PreDefinedBarcode='' OR pd.LatestBarcode LIKE '%' + @PreDefinedBarcode + '%')     
  AND (@ProductLocation IS NULL OR @ProductLocation='' OR pd.Location LIKE '%' + @ProductLocation + '%')                                                                          
  AND (ISNULL(@SearchBy,'0')='0'  OR (@SearchBy ='=' AND Stock=@Qty) OR (@SearchBy ='>' AND Stock>@Qty)                                                                                         
  OR (@SearchBy ='<' AND Stock<@Qty))          
  order by ProductId,um.AutoId desc
  for json path
  ) as AllBarcode
  for json path
 End   
 ELSE IF @OpCode = 900
BEGIN
SELECT AutoId,ProductId,Replace(ImageUrl,'/Attachments/','') AS ImageUrl,ThumbnailImageUrl,ThirdImage FROM ProductMaster pm WHERE pm.ThumbnailImageUrl IS NULL  AND pm.ImageUrl IS NOT NULL AND pm.ThirdImage IS NULL
END  

ELSE IF @OpCode = 901
BEGIN
UPDATE ProductMaster set ThumbnailImageUrl=@ThumbnailImageUrl,ThirdImage=@FourImageUrl WHERE AutoId=@AutoId
END
ELSE IF @OpCode = 902
BEGIN
    SELECT ROW_NUMBER() OVER(ORDER BY AutoId desc) AS RowNumber, * INTO #Results FROM                                
    (
	SELECT psl.AutoId,psl.OledStock as oldStcok,psl.NewStock,FORMAT(psl.DateTime,'MM/dd/yyyy hh:mm tt') AS ActionDate, 
	CASE
	    WHEN psl.ReferenceType='OrderMaster'  then  (SELECT OrderNo FROM OrderMaster om WHERE om.AutoId=psl.ReferenceId)
		WHEN psl.ReferenceType='Delivered_Order_Items'  then  (SELECT OrderNo FROM OrderMaster om WHERE om.AutoId=psl.ReferenceId)
		WHEN psl.ReferenceType='OrderItemMaster'  then  (SELECT OrderNo FROM OrderMaster om WHERE om.AutoId=psl.ReferenceId)
		WHEN (select EmpType from employeeMaster as emp where emp.autoid=psl.UserAutoId)=9
		or (select EmpType from employeeMaster as emp where emp.autoid=psl.UserAutoId)=1
		THEN (SELECT BillNo FROM StockEntry se WHERE se.AutoId=psl.ReferenceId)
	ELSE
	(
	case when psl.ReferenceType='CreditMemoMaster' 
	then (Select CreditNo from CreditMemoMaster where CreditAutoId=psl.ReferenceId) 
	else (SELECT OrderNo FROM OrderMaster om WHERE om.AutoId=psl.ReferenceId) end) 
	end
	as ReferenceId,psl.ActionRemark as remark,pm.ProductName,psl.AffectedStock,
	Convert(varchar(50),psl.DefaultAffectedStock)+' '+(Select UnitType FROM UnitMaster where AutoId=PM.PackingAutoId)
	as DefaultAffectedStock FROM ProductStockUpdateLog psl
	INNER JOIN ProductMaster pm ON pm.ProductId=psl.ProductId
	WHERE psl.ProductId=@ProductId
	)AS t ORDER BY t.ActionDate desc

	SELECT COUNT([AutoId]) AS RecordCount, @PageSize AS PageSize, @PageIndex AS PageIndex FROM #Results                                
                                    
    SELECT * FROM #Results 
	WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))  ORDER BY [AutoId] desc

	Select ProductId,ProductName FROM ProductMaster where ProductId=@ProductId
END 

ELSE IF @OpCode=903                                                                                                                                                                                      
		BEGIN
		UPDATE [dbo].[ProductMaster] SET IsOutOfStock=@IsOutOfStock,Description=@txtDescription,
			ModifiedBy=@EmpAutoId,ModifiedDate=GETDATE(),ShowOnWebsite=@IsShowOnWebsite                                                                                                  
			WHERE AutoId=@AutoId                                                        
			END   
			
	ELSE IF @OpCode=904                                                                                                                                                                                    
		BEGIN	
		    SET @ProductId=(Select ProductId FROM ProductMaster Where AutoId=@AutoId)
			UPDATE Tbl_ProductImage SET isDefault=case when imageurl=@defaultImg then 1 else 0 end where ProductAutoId=@AutoId
			IF(DB_NAME()='psmnj.a1whm.com')                                                                                                                                                                             
			BEGIN                                                                                                                                         
				EXEC [dbo].[Proc_AllImage] @ProductId=@ProductId,@defaultImg=@defaultImg                                                                                                                                                                            
			END
		END 
ELSE IF @OpCode=905                                                                                                                                                                                   
		BEGIN	
			DELETE FROM Tbl_ProductImage where AutoId=@ImageAutoId AND ProductAutoId=@AutoId
			SET @ProductId=(Select ProductId FROM ProductMaster Where AutoId=@AutoId)
			IF(DB_NAME()='psmnj.a1whm.com')                                                                                                                                                                             
			BEGIN
				EXEC [dbo].[Proc_DeleteAllImage] @ProductId=@ProductId,@ImageUrl=@ImageUrl
			END
		END
ELSE IF @OpCode=906                                                                                                                                                                                     
		BEGIN		
			BEGIN TRY
				INSERT INTO Tbl_ProductImage (ProductAutoId,ImageUrl,Thumbnail_100,Thumbnaill_400) 
				SELECT @AutoId,[URL],[Thumb100],[Thumb400] FROM @dtbulkImageUrl WHERE Status=0 
				UPDATE Tbl_ProductImage SET isDefault=case when imageurl=@defaultImg then 1 else 0 end where ProductAutoId=@AutoId  
				IF(DB_NAME()='psmnj.a1whm.com')                                                                                                                                                                             
				BEGIN
		            SET @ProductId=(Select ProductId FROM ProductMaster Where AutoId=@AutoId)  
					EXEC [dbo].[ProcProductMaster_ALL] @ProductId=@ProductId                 
					EXEC [dbo].[Proc_AllImage] @ProductId=@ProductId,@defaultImg=@defaultImg                                                                                                                                                            
				END
			END TRY
			BEGIN CATCH
				 SET @isException=1
				 SET @exceptionMessage='Oops, Something went wrong .Please try again.'
			END CATCH
		END 	
ELSE IF @Opcode=907                                                              
   BEGIN                                                                                                 
	DECLARE @EmpType INT =(SELECT EmpType FROM EMPLOYEEMASTER WHERE AUTOID=@EmpAutoId)
    SELECT [SecurityValue] FROM [dbo].[TBL_MST_SecurityMaster] WHERE [SecurityValue]=@CheckSecurity AND SecurityType=@EmpType and typeevent=10                                                                                                                  
   END  
ELSE IF @Opcode=52                             
			BEGIN
			set @ProductId =(select ProductId from productmaster where autoid=isnull(@autoid,0))			
			select LM.AutoId,Location, (isnull([dbo].[FN_LocationStstus](@ProductId,LM.AutoId),0)) as Status,
			(case when REPLACE(DB_NAME(),'.a1whm.com','')=Location then  1 else 0 end) as DbLoction  from locationmaster as LM      
			where lm.Status=1 order by L_sequanceNO
			for json path
			END
 END TRY                                                    
 BEGIN CATCH                                                                                                               
    SET @isException=1                                                                                                                                                
    SET @exceptionMessage='Oops,some thing went wrong.Please try later.'                                                                                        
 END CATCH                                                                                                                                                                         
END 
