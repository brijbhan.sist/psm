ALTER PROCEDURE [dbo].[ProcPOS_CreditMemo]    
@IPAddress varchar(100)=null,
@Opcode INT=NULL,                                
@Barcode varchar(250) = null,                                    
@CreditAutoId INT=NULL,
@sortInCode INT=NULL,
@CustomerAutoId INT=NULL,                                    
@CreditNo varchar(200)=null,                                    
@EmpAutoId INT=NULL,                                     
@CreditType INT=NULL,                                    
@FromDate DATE=NULL,                                    
@ToDate DATE=NULL,                                    
@ProductAutoId INT=NULL,                                    
@Remark varchar(500)=NULL, 
@CancelRemarks varchar(500)=null,
@Qty INT=NULL,                                    
@UnitAutoId INT=NULL,                                    
@Status INT=NULL,                                    
@EmpType  INT=NULL,                                    
@TableValue [DT_CreditItem_POS] readonly,                                    
@TotalAmount decimal(18,2) =NULL,                                    
@OverallDisc  decimal(18,2) =NULL,                                    
@OverallDiscAmt  decimal(18,2) =NULL,                                    
@TotalTax  decimal(18,2) =NULL,                                    
@GrandTotal  decimal(18,2) =NULL,                                    
@TaxType  int =NULL,                                    
@TaxValue  decimal(18,2) =NULL,                                    
@MLQty  decimal(18,2) =NULL,                                    
@MLTax  decimal(18,2) =NULL,                                    
@AdjustmentAmt  decimal(18,2) =NULL,                                    
@ManagerRemark varchar(500)=null,     
@SecurityKey varchar(50)=null,     
@OrderAutoId INT=NULL,  
@CheckSecurity  VARCHAR(max)=NULL,  
@MLTaxRemark varchar(MAX)=null,
@SalesPerson int=null,
@CreditMemoType int=null,
@ReferenceOrderAutoId int=null,
@PageIndex INT = 1,                                    
@PageSize INT = 10,                                    
@RecordCount INT =null,                                    
@isException BIT OUT,                                    
@exceptionMessage VARCHAR(max) OUT                                    
AS                                    
BEGIN                                    
 BEGIN TRY                                    
  SET @isException=0                                    
  SET @exceptionMessage='Success'                                    
  DECLARE @STATE INT,@custType int                                     
  If @Opcode=11                                      
   BEGIN                                    
    BEGIN TRY                                    
     BEGIN TRAN                                    
       SET @CreditNo = (SELECT DBO.SequenceCodeGenerator('CreditNo'))                                    
       set @EmpType=(select EmpType from EmployeeMaster where AutoId=@EmpAutoId)                                    
       declare @IsApply int =0,@MLtaxper DECIMAL(10,2)=0.00,@DefBill INT,@SalesPersonAutoId int    
        
	   SELECT @DefBill=DefaultBillAdd,@SalesPersonAutoId=SalesPersonAutoId FROM CustomerMaster Where AutoId=@CustomerAutoId                                        
       SET @STATE =(select State from BillingAddress where AutoId =@DefBill)     
                               
       IF EXISTS(SELECT * FROM MLTaxMaster WHERE TaxState=@state)                                    
       BEGIN                                    
       IF @CreditMemoType=1
		   BEGIN
				SET @IsApply=0                                    
				SET @MLtaxper=0.00
		   END
		   ELSE
		   BEGIN
				SET @IsApply=1                                    
				SET @MLtaxper=(SELECT TaxRate FROM MLTaxMaster WHERE TaxState=@state)  
		   END                              
       END                                    
       INSERT INTO CreditMemoMaster(CreditNo,CustomerAutoId,SalesPersonAutoId,CreatedBy,CreditDate,Status,Remarks,creditType,                                    
          OverallDiscAmt,SalesAmount,PaidAmount,IsMLTaxApply,MLTAXPER,TaxTypeAutoId,TaxValue,WeightTax,CreditMemoType,
		  ReferenceOrderAutoId,ApprovalDate,ApprovedBy                                  
       )                                    
       VALUES(@CreditNo,@CustomerAutoId,@SalesPersonAutoId,@EmpAutoId,GETDATE(),case when @EmpType=10 then 3 else 1 end ,@Remark,                                    
       case when @EmpType=10 then 2 else 1 end,                   
       @OverallDiscAmt,@GrandTotal,0,@IsApply,@MLtaxper,@TaxType,@TaxValue,              
       isnull((select Value from Tax_Weigth_OZ),0),@CreditMemoType,@ReferenceOrderAutoId,
	   case when @EmpType=10 then GETDATE() else '' end,case when @EmpType=10 then @EmpAutoId else '' end)                                   
                                    
       SET @CreditAutoId=SCOPE_IDENTITY()        
                                    
       INSERT INTO CreditItemMaster(CreditAutoId,ProductAutoId,UnitAutoId,RequiredQty,AcceptedQty,QtyPerUnit,                                    
       UnitPrice,ManagerUnitPrice,SRP,TaxRate,UnitMLQty,Weight_OZ,CostPrice,QtyReturn_Fresh,QtyPerUnit_Fresh,
	   QtyReturn_Damage,QtyPerUnit_Damage,OM_MinPrice,OM_CostPrice,OM_BasePrice)                           
       SELECT @CreditAutoId,temp.ProductAutoId,temp.UnitAutoId,RequiredQty,RequiredQty,QtyPerUnit,UnitPrice,                         
       UnitPrice,pm.P_SRP,Tax,
	  (case when @CreditMemoType=1 then 0 when IsApply_ML=1 then ISNULL(MLQty,0) else 0 end ) as MLQty,
		(case when @CreditMemoType=1 then 0 when IsApply_Oz=1 then  ISNULL(WeightOz,0) else 0 end) as WeightOz,
		pd.CostPrice,RequiredQty,UnitAutoId,0,UnitAutoId,OM_MinPrice,OM_CostPrice,OM_BasePrice 
		FROM @TableValue                              
       as temp inner join ProductMaster as pm on pm.AutoId=temp.ProductAutoId    
	   inner join PackingDetails as pd on pd.ProductAutoId=temp.ProductAutoId and pd.UnitType=temp.UnitAutoId
                             
       SET @Remark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=8), '[MemoNo]', @CreditNo)                                 
       INSERT INTO CreditMemoLog(CreditAutoId,ActionAutoId,EmpAutoId,Remarks,LogDate)                                    
       VALUES(@CreditAutoId,8,@EmpAutoId, @Remark,GETDATE())                                    
       UPDATE [dbo].[SequenceCodeGeneratorMaster] SET currentSequence=currentSequence+1 WHERE [SequenceCode]='CreditNo'        
	   
	   IF @EmpType=10
	   BEGIN 
			--SELECT SUM(TotalPeice) as TotalPeice ,ProductAutoId  FROM CreditItemMaster where 
			--AND isnull(TotalPeice,0)>0 group by ProductAutoId
			SELECT TBL.ProductAutoId,QtyReturn_Fresh*PD.Qty as TotalPeice into #stock FROM @TableValue as TBL
			inner join PackingDetails  as PD on PD.ProductAutoId=TBL.ProductAutoId
			where PD.ProductAutoId=TBL.ProductAutoId and PD.UnitType=TBL.QtyPerUnit_Fresh

			insert into [dbo].[ProductStockUpdateLog]([ProductId],[OledStock],[NewStock],[UserAutoId],[DateTime],ReferenceId,ActionRemark,ReferenceType)
			SELECT pm.ProductId,isnull([Stock],0),(isnull(pm.Stock,0)+(t.TotalPeice)),@EmpAutoId,GETDATE(),@CreditAutoId,
			'Credit Memo No -(' + @CreditNo + ') has approved by '+(Select FirstName+' '+LastName from Employeemaster where autoid=@EmpAutoId)+'.','CreditMemoMaster' 
			FROM #stock as t inner join ProductMaster as pm on pm.AutoId=t.ProductAutoId
			
			UPDATE PM SET [Stock] = isnull([Stock],0) + (TotalPeice) FROM [dbo].[ProductMaster] AS PM                                                                                                   
			INNER JOIN (SELECT TotalPeice ,ProductAutoId FROM #stock) AS dt 
			ON dt.[ProductAutoId] = PM.[AutoId]     
	   END
     COMMIT TRANSACTION                                    
    END TRY                                    
    BEGIN CATCH                                    
     ROLLBACK TRAN                                    
     Set @isException=1                                    
     Set @exceptionMessage=ERROR_MESSAGE()                                    
    END CATCH                                    
   END  
    ELSE If @Opcode=41                                     
   BEGIN                                    
  SET @EmpType=(SELECT EmpType FROM EmployeeMaster WHERE AutoId=@EmpAutoId)                                    
  SELECT [AutoId],CustomerId+' - '+[CustomerName] As Customer FROM [dbo].[CustomerMaster]                                    
  WHERE @EmpType!=2 OR [SalesPersonAutoId]=@EmpAutoId AND Status=1  ORDER BY replace(CustomerId+' - '+[CustomerName],' ','') ASC       
                                  
  SELECT [AutoId],convert(varchar(10),[ProductId]) + '--' + [ProductName] as [ProductName] FROM [dbo].[ProductMaster] ORDER BY convert(varchar(10),[ProductId]) + '--' + [ProductName] ASC                                   
  SELECT convert(varchar(10),GETDATE(),101)  as CurrentDate,OrderType AS CreditType FROM OrderTypeMaster WHERE Type='CREDIT' AND case when @EmpType =10 then  2 else 1 end=AutoId                                    
  
  select AutoId,OrderType from OrderTypeMaster where Type='CREDIT' 

  SELECT FirstName+' '+LastName as SalesPerson,AutoId FROM EmployeeMaster WHERE EmpType=2 and Status=1

  END     
  ELSE If @Opcode=415                                     
   BEGIN                                    
  SET @EmpType=(SELECT EmpType FROM EmployeeMaster WHERE AutoId=@EmpAutoId)                                    
  
 select(
	select
		isnull(( SELECT [AutoId],CustomerId+' - '+[CustomerName] As Customer FROM [dbo].[CustomerMaster] WHERE  Status=1 ORDER BY replace(CustomerId+' - '+[CustomerName],' ','') ASC for json path, INCLUDE_NULL_VALUES),'[]') as Customer,
		isnull((SELECT [AutoId],convert(varchar(10),[ProductId]) + '--' + [ProductName] as [ProductName] FROM [dbo].[ProductMaster] ORDER BY convert(varchar(10),[ProductId]) + '--' + [ProductName] ASC for json path, INCLUDE_NULL_VALUES),'[]') as Product,
		isnull((SELECT convert(varchar(10),GETDATE(),101)  as CurrentDate,OrderType AS CreditType FROM OrderTypeMaster WHERE Type='CREDIT' AND AutoId=2 for json path, INCLUDE_NULL_VALUES),'[]') as CreditType,
		isnull((select AutoId,OrderType from OrderTypeMaster where Type='CREDIT' and  AutoId=2  for json path, INCLUDE_NULL_VALUES),'[]') as OrderType,
		isnull((SELECT FirstName+' '+LastName as SalesPerson,AutoId FROM EmployeeMaster WHERE EmpType=2 and Status=1 ORDER BY SalesPerson for json path, INCLUDE_NULL_VALUES),'[]') as SalesPerson
	
	for json path, INCLUDE_NULL_VALUES
)as DropDownList
  END     
   ELSE If @Opcode=411                                     
   BEGIN                                    
		SET @EmpType=(SELECT EmpType FROM EmployeeMaster WHERE AutoId=@EmpAutoId) 
		select(
		select
		(
		SELECT [AutoId],CustomerId+' - '+[CustomerName] As Customer FROM [dbo].[CustomerMaster]                                    
		WHERE CustomerType!=3 AND Status=1 AND  (@EmpType!=2 OR [SalesPersonAutoId]=@EmpAutoId) ORDER BY replace(CustomerId+' - '+[CustomerName],' ','')
		ASC for json path, INCLUDE_NULL_VALUES
		) as Customer,
		(
		SELECT pd.[AutoId],Convert(varchar(10),pd.[ProductId]) + '--' + pd.[ProductName] as [ProductName],pd.MLQty,ISNULL(pd.WeightOz,0)                  
		AS WeightOz FROM [dbo].[ProductMaster] as pd
		inner join PackingDetails pkd ON pkd.ProductAutoId=pd.AutoId
		where ProductStatus=1 and PackingAutoId!='' group by pd.AutoId,pd.[ProductId],pd.[ProductName],pd.MLQty,pd.WeightOz order by Convert(varchar(10),pd.[ProductId]) + '--' + pd.[ProductName]
		for json path, INCLUDE_NULL_VALUES) as Product,
		(
		SELECT convert(varchar(10),GETDATE(),101)  as CurrentDate,case when @EmpType =10 then  'POS CREDIT' else 'WEB CREDIT' end as CreditType 
		for json path, INCLUDE_NULL_VALUES
		) as CreditType,
		(SELECT AutoId,CreditType FROM CreditMemoTypeMaster	for json path, INCLUDE_NULL_VALUES) as CreditMemoType
		for json path, INCLUDE_NULL_VALUES
		)as DropDownList                                  
   END  
ELSE IF @Opcode=412                                                               
  BEGIN   
		SELECT AutoId,CustomerId+' '+CustomerName as Customer FROM CustomerMaster   WHERE            
		status=1 and (SalesPersonAutoId=@SalesPerson or ISNULL(@SalesPerson,0)=0)
		order by replace(CustomerId+' '+CustomerName,' ','') ASC
		for json path
  END     
 ELSE If @Opcode=42                                     
   BEGIN                                    
		SELECT UM.AutoId AS AutoId, UM.[UnitType] AS UnitType, [Qty] FROM [dbo].[PackingDetails] AS PD                                    
		INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = PD.UnitType WHERE PD.[ProductAutoId] = @ProductAutoId    ORDER BY UM.[UnitType] ASC                                   
		SELECT PackingAutoId AS AutoId FROM ProductMaster  WHERE  AutoId=@ProductAutoId                                  
   END                                    
  ELSE If @Opcode=43                                     
   BEGIN          
   SET @custType=(Select CustomerType from CustomerMaster where AutoId=@CustomerAutoId)
	SELECT PM.AutoId AS ProductAutoId,PD.UnitType AS UnitAutoId,Qty AS QtyPerUnit,@Qty as ReturnQty,10000 AS Qty                                     
	,PM.ProductId,PM.ProductName,UM.UnitType,isnull(PM.TaxRate,0) as TaxRate,convert(decimal(10,2),MinPrice) as MinPrice ,convert(decimal(10,2),ISNULL(PPL.[CustomPrice],                                    
	[Price])) as CostPrice,convert(decimal(10,2),Price) as Price,0.00 SRP,ISNULL(PPL.[CustomPrice],[Price]) * @Qty AS NetPrice,
	(case when IsApply_ML=1 then ISNULL(MLQty,0) else 0 end ) as MLQty,
	( case when IsApply_Oz=1 then  ISNULL(WeightOz,0) else 0 end) as WeightOz,
	(CASE                                                                                                                                                     
	WHEN @custType=2 THEN ISNULL(WHminPrice,0)                                                                                                                                                    
	WHEN @custType=3  THEN ISNULL(CostPrice,0)                                                                                                                   
	ELSE [MinPrice] END) AS OM_MinPrice ,[CostPrice] as OM_CostPrice,Price as OM_BasePrice
	FROM  ProductMaster AS PM                                     
	INNER JOIN PackingDetails AS PD ON PM.AutoId =PD.ProductAutoId                                    
	INNER JOIN UnitMaster AS UM ON PD.UnitType=UM.AutoId                                    
	LEFT JOIN [dbo].[ProductPricingInPriceLevel] AS PPL ON PPL.[ProductAutoId] = PM.[AutoId] AND PPL.[UnitAutoId] = PD.[UnitType]                                    
	AND [PriceLevelAutoId] = (SELECT [PriceLevelAutoId] FROM [dbo].[CustomerPriceLevel] WHERE [CustomerAutoId]=@CustomerAutoId)                                    
	WHERE PM.AutoId=@ProductAutoId AND PD.UnitType=@UnitAutoId                                       
    
	select AutoId,UnitType from UnitMaster                                    
   END                                    
   ELSE IF @Opcode=44                                    
   BEGIN                                    
    SET @EmpType =(SELECT EmpType FROM  EmployeeMaster WHERE AutoId=@EmpAutoId)                                    
    SELECT ROW_NUMBER() OVER(ORDER BY OrderNo desc) AS RowNumber, * INTO #Results from                      
    (                                       
    SELECT OM.CreditAutoId AS [AutoId],OM.CreditNo AS OrderNo,FORMAT(OM.CreditDate, 'MM/dd/yyyy') AS OrderDate,CM.[CustomerName] AS CustomerName,                   
    SM.StatusType AS Status,OM.[Status] As StatusCode,                                    
    TotalAmount AS [GrandTotal],                                    
    (SELECT COUNT(distinct [ProductAutoId]) from [dbo].CreditItemMaster WHERE CreditAutoId=OM.CreditAutoId) AS NoOfItems ,                                    
    OT.OrderType as CreditType,                                    
    (SELECT OrderNo from OrderMaster as mo where mo.AutoId=OM.OrderAutoId) as referenceorderno,emp.FirstName+' '+LastName as SalesPerson                                 
    FROM [dbo].CreditMemoMaster As OM                                    
    INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.AutoId = OM.CustomerAutoId  
	INNER JOIN [dbo].OrderTypeMaster AS OT ON OT.AutoId = OM.CreditType AND OT.Type='CREDIT' 
	INNER JOIN EmployeeMaster AS emp ON emp.AutoId = OM.SalesPersonAutoId
    LEFT JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.[Status] AND SM.Category = 'CreditMaster'                                               
    WHERE                                     
    (@CustomerAutoId=0 or @CustomerAutoId is null or CustomerAutoId=@CustomerAutoId)
	and (@SalesPerson=0 or @SalesPerson is null or OM.SalesPersonAutoId=@SalesPerson)
    and(@CreditType=0 or @CreditType is null or CreditType=@CreditType)                                    
    and (@CreditNo is null or @CreditNo ='' or CreditNo like '%' + @CreditNo + '%')                                    
    and (@FromDate is null or @FromDate = '' or @Todate is null or @Todate = '' or (convert(date,CreditDate) between @FromDate and @Todate))                                        
    and (@Status is null or @Status=0 or OM.[Status]=@Status) and   
	OM.CreatedBy=@EmpAutoId                                    
    )as t order by OrderNo                                    
                 
   
	select(
	select
		isnull((SELECT COUNT(RowNumber) AS RecordCount,case when @PageSize=0 then COUNT(OrderNo) else @PageSize end AS PageSize,@PageIndex as PageIndex FROM #Results for json path, INCLUDE_NULL_VALUES),'[]') as Paging,
		isnull((SELECT * FROM #Results                                    
    WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)) for json path, INCLUDE_NULL_VALUES),'[]') as OrderList,
		isnull((select @EmpType as EmpType   for json path, INCLUDE_NULL_VALUES),'[]') as EmpType 
	for json path, INCLUDE_NULL_VALUES
)as DropDownList 
   END                              
   ELSE IF @Opcode=45                                    
   BEGIN                                    
  	select(
		isnull((SELECT * FROM StatusMaster WHERE Category='CreditMaster' and AutoId not in(1,5)  ORDER BY StatusType desc for json path, INCLUDE_NULL_VALUES),'[]')
		
)as DropDownList                                    
   END                                    
   ELSE IF @Opcode=46                                    
   BEGIN                                    
		SET @EmpType=(SELECT EmpType FROM EmployeeMaster WHERE AutoId=@EmpAutoId)                                    
		SET @Status=(SELECT Status FROM CreditMemoMaster as cmm  WHERE CreditAutoId=@CreditAutoId)                                    
		SET @CustomerAutoId=(SELECT CustomerAutoId FROM CreditMemoMaster as cmm                                   
		WHERE CreditAutoId=@CreditAutoId)  

		IF @EmpType=6 AND @Status=3                                    
		BEGIN                                       
			SET @Remark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=11), '[MemoNo]', (SELECT CreditNo FROM CreditMemoMaster where CreditAutoId = @CreditAutoId))                          
			INSERT INTO CreditMemoLog(CreditAutoId,ActionAutoId,EmpAutoId,Remarks,LogDate)                                    
			VALUES(@CreditAutoId,11,@EmpAutoId, @Remark,GETDATE())                                    
		END  
		
		SELECT IsMLTaxApply,CreditNo,convert(varchar(10),CreditDate,101) as CreditDate,cmm.Status as StatusCode,StatusType AS STATUS,                                    
		CMM.CustomerAutoId,Remarks,ISNULL(OverallDisc,0 ) as OverallDisc,cm.CustomerName,                                    
		ISNULL(OverallDiscAmt,0) as OverallDiscAmt,ISNULL(TotalTax,0) as TotalTax,ISNULL(GrandTotal,TotalAmount) as GrandTotal,                                    
		ISNULL(MLQty,0) as MLQty,ISNULL(MLTax,0) as MLTax,ISNULL(AdjustmentAmt,0) as AdjustmentAmt,TotalAmount,                              
		ISNULL(ApplyMLQty,0) as ApplyMLQty,ManagerRemark,ISNULL((cmm.WeightTotalQuantity),0) as WeightTotalQuantity,                                  
		isnull((cmm.WeightTaxAmount),0) as WeightTaxAmount,ISNULL((WeightTax),0) as WeightTax ,(SELECT OrderNo from OrderMaster as mo where mo.AutoId=cmm.OrderAutoId) as referenceorderno,                              
		ISNULL(MLTaxPer,0) as MLTaxPer,OrderType as CreditType,cmm.MLTaxRemark,dbo.UDF_CreditMemo_CheckMLTax(CMM.CreditAutoId,CMM.CustomerAutoId) as CreditMemo_CheckMLTax,OrderAutoId
		,isnull(CreditMemoType,0) as CreditMemoType,isnull(ReferenceOrderAutoId,0) as ReferenceOrderAutoId
		FROM CreditMemoMaster as cmm                                    
		INNER JOIN StatusMaster AS SM ON SM.AutoId=CMM.Status and Category='CreditMaster'   
		INNER JOIN OrderTypeMaster AS OTM ON OTM.AutoId=CMM.CreditType and Type='Credit'                                    
		INNER JOIN CustomerMaster as CM on CMM.CustomerAutoId=cm.AutoId  WHERE CreditAutoId=@CreditAutoId                                    
                                            
		SELECT PM.ProductId,PM.ProductName,UM.UnitType,CIM.ItemAutoId,CIM.ProductAutoId,CIM.UnitAutoId,CIM.RequiredQty,CIM.TotalPeice,CIM.AcceptedQty,
		CIM.AcceptedTotalQty,CIM.QtyPerUnit,CIM.UnitPrice,CIM.ManagerUnitPrice,                                    
		CIM.SRP,CIM.TaxRate,CIM.NetAmount,isnull(CIM.UnitMLQty,0) as UnitMLQty,isnull(CIM.TotalMLTax,0) as TotalMLTax,10000 as MAXQty,
		(case when pm.IsApply_Oz=1 then  ISNULL(PM.WeightOz,0) else 0 end) as WeightOz,                                     
		CIM.QtyPerUnit_Fresh,ISNULL(QtyReturn_Fresh,0)  as QtyReturn_Fresh,QtyPerUnit_Damage,isnull(QtyReturn_Damage,0)QtyReturn_Damage,
		QtyPerUnit_Missing,isnull(QtyReturn_Missing,0)QtyReturn_Missing,isnull(OM_MinPrice,0) as OM_MinPrice
		,isnull(OM_CostPrice,0) as OM_CostPrice,isnull(OM_BasePrice,0) as OM_BasePrice
		FROM CreditItemMaster as CIM                                  
		INNER JOIN ProductMaster AS PM ON PM.AutoId=CIM.ProductAutoId                                    
		INNER JOIN UnitMaster AS UM ON UM.AutoId=CIM.UnitAutoId                                        
		WHERE CreditAutoId=@CreditAutoId                         
                                        
		SELECT EmpType FROM EmployeeMaster WHERE AutoId=@EmpAutoId                                    
                                               
		SELECT 'Sales Manager' as EmpType,ManagerRemark as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName,* from CreditMemoMaster as cmm                                             
		left join EmployeeMaster as emp on emp.AutoId=cmm.ApprovedBy                                             
		where CreditAutoId=@CreditAutoId    
	
		select ProductId,um.UnitType,pd.UnitType as UnitAutoId,Qty from CreditItemMaster as CIM                                                                                                                                                    
		inner join ProductMaster as pm on pm.AutoId=CIM.ProductAutoId                                                                                
		inner join PackingDetails as pd on pd.ProductAutoId=pm.AutoId                                                                                                             
		inner join UnitMaster as um on um.AutoId=pd.UnitType 
		WHERE CreditAutoId=@CreditAutoId  


   END                                    
   ELSE IF @Opcode=21                                    
   BEGIN                                    
      BEGIN TRY                                    
        BEGIN TRAN                                    
			   DELETE FROM CreditItemMaster WHERE CreditAutoId=@CreditAutoId                                    
			   INSERT INTO CreditItemMaster(CreditAutoId,ProductAutoId,UnitAutoId,RequiredQty,AcceptedQty,QtyPerUnit,UnitPrice,ManagerUnitPrice,SRP,TaxRate,UnitMLQty,CostPrice
			   ,QtyReturn_Fresh,QtyPerUnit_Fresh,QtyReturn_Damage,QtyPerUnit_Damage,OM_MinPrice,OM_CostPrice,OM_BasePrice)                                    
			   SELECT @CreditAutoId,temp.ProductAutoId,UnitAutoId,RequiredQty,RequiredQty,QtyPerUnit,UnitPrice,UnitPrice,Pm.P_SRP,Tax,MLQty,CostPrice,
			   QtyReturn_Fresh,QtyPerUnit_Fresh,QtyReturn_Damage,QtyPerUnit_Damage,OM_MinPrice,OM_CostPrice,OM_BasePrice FROM @TableValue                                    
			   as temp inner join ProductMaster as pm on pm.AutoId=temp.ProductAutoId    
			   inner join PackingDetails as pd on pd.ProductAutoId=temp.ProductAutoId and pd.UnitType=temp.UnitAutoId
			   UPDATE CreditMemoMaster SET                                      
			   OverallDiscAmt=@OverallDiscAmt,                         
			   SalesAmount=@GrandTotal,PaidAmount=0,TaxTypeAutoId=@TaxType,TaxValue=@TaxValue                                    
			   WHERE CreditAutoId=@CreditAutoId                                    
			   
			   SET @Remark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=9), '[MemoNo]', (SELECT CreditNo FROM CreditMemoMaster where CreditAutoId = @CreditAutoId))                                    
			   INSERT INTO CreditMemoLog(CreditAutoId,ActionAutoId,EmpAutoId,Remarks,LogDate)                                    
			   VALUES(@CreditAutoId,9,@EmpAutoId, @Remark,GETDATE())                                    
                                           
     COMMIT TRAN                                    
     END TRY                                    
     BEGIN CATCH                                    
       ROLLBACK TRAN                                    
    SET @isException=1                                    
    SET @exceptionMessage='Oops, Some things went wrong;'                                    
     END CATCH                                    
   END                           
   ELSE IF @Opcode=24                                    
   BEGIN                                    
      BEGIN TRY                                    
        BEGIN TRAN                                    
				 UPDATE CreditMemoMaster SET ManagerRemark=@ManagerRemark WHERE CreditAutoId=@CreditAutoId                                    
                 UPDATE CIM SET AcceptedQty=temp.RequiredQty,                                     
                 ManagerUnitPrice=temp.UnitPrice,
				 CostPrice= pd.CostPrice                                      
                 FROM CreditItemMaster AS CIM 
				 INNER JOIN  @TableValue AS TEMP ON TEMP.ProductAutoId=CIM.ProductAutoId AND TEMP.UnitAutoId=CIM.UnitAutoId
				 INNER JOIN  PackingDetails AS pd ON pd.ProductAutoId=CIM.ProductAutoId AND pd.UnitType=CIM.UnitAutoId 
				 WHERE CreditAutoId=@CreditAutoId                                         
                                                     
				 SET @Remark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=9), '[MemoNo]', (SELECT CreditNo FROM CreditMemoMaster where CreditAutoId = @CreditAutoId))                                    
				 INSERT INTO CreditMemoLog(CreditAutoId,ActionAutoId,EmpAutoId,Remarks,LogDate)                                    
				 VALUES(@CreditAutoId,9,@EmpAutoId, @Remark,GETDATE())                                    
     COMMIT TRAN                                    
     END TRY                               
     BEGIN CATCH                                    
       ROLLBACK TRAN                                    
    SET @isException=1                                    
    SET @exceptionMessage='Oops, Some things went wrong;'                                    
     END CATCH                                    
   END                                    
   ELSE IF @Opcode=31                                    
   BEGIN                                    
      BEGIN TRY                                    
        BEGIN TRAN                                    
     DELETE FROM CreditItemMaster WHERE CreditAutoId=@CreditAutoId                                    
     DELETE FROM CreditMemoLog WHERE CreditAutoId=@CreditAutoId                                    
     DELETE FROM CreditMemoMaster WHERE CreditAutoId=@CreditAutoId                                     
     COMMIT TRAN                                    
     END TRY                                    
     BEGIN CATCH                                    
       ROLLBACK TRAN                        
    SET @isException=1                                    
    SET @exceptionMessage='Oops, Some things went wrong;'                                    
     END CATCH                                    
   END                                    
   ELSE IF @Opcode=47                                    
   BEGIN                                    
       SELECT CreditNo,format(CreditDate,'MM/dd/yyyy hh:mm tt')  AS CreditDate FROM CreditMemoMaster WHERE CreditAutoId=@CreditAutoId                                    
       SELECT (EM.FirstName+' '+ EM.LastName ) AS EmpName, ETM.TypeName AS EmpType,format(LogDate,'MM/dd/yyyy hh:mm tt') LogDate,                                
    --Convert(varchar(30),LogDate) LogDate,                                    
    AM.[Action],Remarks FROM CreditMemoLog AS CLOG                                    
    INNER JOIN EmployeeMaster AS EM ON EM.AutoId=CLOG.EmpAutoId               
    INNER JOIN [dbo].[tbl_ActionMaster] As AM ON AM.[AutoId] = CLOG.[ActionAutoId]                                   
    inner join EmployeeTypeMaster As ETM on ETM.AutoId=EM.EmpType                                  
    WHERE CLOG.CreditAutoId=@CreditAutoId order by 
	(CASE WHEN @sortInCode=1 THEN LogDate END)ASC,
	(CASE WHEN @sortInCode=2 THEN LogDate END) DESC
   END                                    
   ELSE IF @Opcode=22                                    
    BEGIN                                    
    UPDATE CreditMemoMaster SET Status=@Status,ApprovedBy=@EmpAutoId,ApprovalDate=GETDATE(),ManagerRemark=@ManagerRemark WHERE CreditAutoId=@CreditAutoId                                    
    SET @Remark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=case when @Status=3 then 10 else 11 end), '[MemoNo]',  (SELECT CreditNo FROM CreditMemoMaster where CreditAutoId = @CreditAutoId))                                
  
    
    INSERT INTO CreditMemoLog(CreditAutoId,ActionAutoId,EmpAutoId,Remarks,LogDate)                                
    VALUES(@CreditAutoId,(case when @Status=3 then 10 else 11 end),@EmpAutoId, @Remark+'<br>'+'<b>'+'Remark: '+'</b>'+@ManagerRemark,GETDATE())                                    
                                        
                                        
    if (@Status=3)                                    
    begin                                    
    update pm set pm.Stock= isnull(pm.Stock,0)+ ISNULL(cim.AcceptedTotalQty,0) from  ProductMaster as pm
	inner join CreditItemMaster as cim                                    
    on pm.AutoId=cim.ProductAutoId where CreditAutoId=@CreditAutoId                                    
                                        
    end                                    
    END                                    
    ELSE IF @Opcode=23                                
    BEGIN                                    
      BEGIN TRY                                    
      BEGIN TRAN                                    
     SET @CustomerAutoId=(SELECT CustomerAutoId  FROM  CreditMemoMaster WHERE CreditAutoId=@CreditAutoId)                                    
                                    
     UPDATE CreditMemoMaster SET Status=4,CompletedBy=@EmpAutoId,CompletionDate=GETDATE() WHERE CreditAutoId=@CreditAutoId                                    
     SET @Remark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=12), '[MemoNo]',  (SELECT CreditNo FROM CreditMemoMaster where CreditAutoId = @CreditAutoId))                                    
     INSERT INTO CreditMemoLog(CreditAutoId,ActionAutoId,EmpAutoId,Remarks,LogDate)                                    
     VALUES(@CreditAutoId,12,@EmpAutoId, @Remark,GETDATE())                                    
     INSERT INTO CreditMoneyLog([CustomerAutoId],[RefType],[RefId],TransactionType,[Amount],[EmpAutoId],[LogDate],[Status])                                    
     VALUES(@CustomerAutoId,'Credit Memo',@CreditAutoId,'CR',(SELECT SUM(NetAmount) FROM CreditItemMaster WHERE CreditAutoId=@CreditAutoId),@EmpAutoId,GETDATE(),0)                                    
      COMMIT TRAN                                    
   END TRY                                    
   BEGIN CATCH                                    
     ROLLBACK TRAN                                    
      SET @isException=1                                    
      SET @exceptionMessage='Oops some thing went wrong.'                                    
   END CATCH                                    
    END                                    
    ELSE IF @Opcode=48                                    
   BEGIN                
	   Declare @DefaultBillAdd INT   
	   Select @DefaultBillAdd=DefaultBillAdd from CustomerMaster Where AutoId=@CustomerAutoId   
	   set @STATE  =(SELECT State from  BillingAddress where AutoId =@DefaultBillAdd)           
	   
	SELECT TaxRate FROM MLTaxMaster WHERE TaxState=@STATE                                 
	SELECT AutoId,Value,TaxableType FROM TaxTypeMaster WHERE State=@STATE                             
	select Value from Tax_Weigth_OZ  

	SELECT CustomerAutoId,OM.AutoId,[OrderNo],CONVERT(VARCHAR(20),[OrderDate],101)
	AS OrderDate,DO.[AmtDue],DO.[GrandTotal],DO.[AmtPaid]                                                                    
	FROM [dbo].[OrderMaster] As OM                                                                    
	INNER JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId]                                                                         
	WHERE [CustomerAutoId] = @CustomerAutoId AND                                   
	[Status] = 11 AND DO.[AmtDue] > 0.00 
	  
    END                                   
    ELSE IF @Opcode=49                                    
    BEGIN                                 
	Declare @LocationCode varchar(100)                        
	SELECT @LocationCode=DB_NAME() -- LocationCode Added By Rizwan Ahmad on 11/14/2019 to show on print page                        
	SELECT CompanyName,EmailAddress, [Address],[Website],[MobileNo],[FaxNo],TermsCondition,Logo,case when @LocationCode='psmct.a1whm.com' then 'CT' else 'NJ' end as LocationCode  
	FROM CompanyDetails                            
                           
    SELECT OM.[CreditNo] as OrderNo,CONVERT(VARCHAR(20), OM.[CreditDate], 101) AS OrderDate,                                         
    CM.[AutoId] As CustAutoId,CM.[CustomerId],CM.[CustomerName],CM.[Contact1] + '/' + CM.[Contact2] As Contact,CTy.CustomerType,CT.[TermsDesc],                                    
    BA.[Address] As BillAddr,                                    
    S.[StateName] AS State1,BA.[City] AS City1,BA.[Zipcode] As Zipcode1,SA.[Address] As ShipAddr,S1.[StateName] As State2,SA.[City] AS City2,                                    
    SA.[Zipcode] As Zipcode2,OM.[TotalAmount],ISNULL(OM.[OverallDisc],0) as OverallDisc1,ISNULL(OM.[OverallDiscAmt],0) as OverallDisc,ISNULL(OM.[TotalTax],0) as TotalTax,
	isnull(OM.[GrandTotal],OM.[TotalAmount]) as GrandTotal,ISNULL((select top 1 PrintLabel from MLTaxMaster where TaxState=BA.State),'') as MLTaxPrintLabel, 
    ISNULL(MLQty,0) as MLQty,isnull(MLTax,0) as MLTax,CM.ContactPersonName,ISNULL(AdjustmentAmt,0) as AdjustmentAmt,                            
    ISNULL(OM.WeightTaxAmount,0) as WeightTaxAmount,  --  TaxTypeAutoId Added By Rizwan Ahmad on 11/09/2019 04:00 AM                                  
    CM.BusinessName,EM.[FirstName] + ' ' + EM.[LastName] As SalesPerson FROM [dbo].[CreditMemoMaster] As OM                                    
    INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                               
    INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]                                    
    INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[CustomerAutoId] = OM.[CustomerAutoId] AND BA.IsDefault=1                                    
    INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[CustomerAutoId] = OM.[CustomerAutoId] AND SA.IsDefault=1                                    
    LEFT JOIN [dbo].[CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                                           
    INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                                     
    INNER JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                                     
    left JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.CreatedBy                                    
    WHERE OM.CreditAutoId=@CreditAutoId                                    
                                        
    SELECT CM.[CategoryName],CM.[CategoryId],OIM.ProductAutoId,PM.[ProductId],PM.[ProductName],PM.[ProductLocation],                                  
    OIM.[UnitAutoId] AS UnitAutoId,UM.[UnitType],OIM.[QtyPerUnit],OIM.ManagerUnitPrice as  [UnitPrice],                                    
    OIM.NetAmount AS  [NetPrice],OIM.AcceptedQty AS QtyShip,'' AS BarCode                                    
    FROM [dbo].[CreditItemMaster] AS OIM                                     
    INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                                    
    INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitAutoId                                     
    INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId] WHERE CreditAutoId=@CreditAutoId                                     
    ORDER BY CM.[CategoryName] ASC                       
    END        
 ELSE IF @Opcode=401                                                  
 BEGIN                                                  
	select @ProductAutoId = ProductAutoId ,@UnitAutoId = UnitAutoId from ItemBarcode where Barcode = CONVERT(varchar(50),@Barcode)     
	if isnull(@ProductAutoId,0)=0
	Begin	
	    SET @isException=1
		SET @exceptionMessage='BarcodeDoesNotExist'
	END
	ELSE IF ((Select ProductStatus FROM ProductMaster where AutoId=@ProductAutoId)=1)
	BEGIN
		SET @custType=(SELECT CustomerType FROM CustomerMaster WHERE AutoId=@CustomerAutoId)                                                
		SELECT PM.AutoId AS ProductAutoId,PD.UnitType AS UnitAutoId,Qty AS QtyPerUnit,1 as ReturnQty,10000 AS Qty                                                     
		,PM.ProductId,PM.ProductName,UM.UnitType,PM.TaxRate,MinPrice,CONVERT(DECIMAL(10,2),ISNULL(PPL.[CustomPrice],                                                    
		[Price])) as CostPrice,Price,0.00 as SRP,ISNULL(PPL.[CustomPrice],[Price]) * 1 AS NetPrice,
		(case when IsApply_ML=1 then ISNULL(MLQty,0) else 0 end ) as MLQty,
		(case when IsApply_Oz=1 then  ISNULL(WeightOz,0) else 0 end) as WeightOz,
		(CASE                                                                                                                                                     
		WHEN @custType=2 THEN ISNULL(WHminPrice,0)                                                                                                                                                    
		WHEN @custType=3  THEN ISNULL(CostPrice,0)                                                                                                                   
		ELSE [MinPrice] END) AS OM_MinPrice ,[CostPrice] as OM_CostPrice,Price as OM_BasePrice FROM  ProductMaster AS PM                                                     
		INNER JOIN PackingDetails AS PD ON PM.AutoId =PD.ProductAutoId                                                    
		INNER JOIN UnitMaster AS UM ON PD.UnitType=UM.AutoId                                 
		LEFT JOIN [dbo].[ProductPricingInPriceLevel] AS PPL ON PPL.[ProductAutoId] = PM.[AutoId] AND PPL.[UnitAutoId] = PD.[UnitType]                                                    
		AND [PriceLevelAutoId] = (SELECT [PriceLevelAutoId] FROM [dbo].[CustomerPriceLevel] WHERE [CustomerAutoId]=@CustomerAutoId)                                                    
		WHERE PM.AutoId=@ProductAutoId AND PD.UnitType=@UnitAutoId  

		select AutoId,UnitType from UnitMaster where  AutoId=@UnitAutoId
	END
	ELSE
	BEGIN
	    SET @isException=1
		SET @exceptionMessage='Inactive'
	END
 END     
 ELSE IF @Opcode=402  
 BEGIN  
	 SET @custType =(SELECT CustomerType FROM CustomerMaster WHERE AutoId=@CustomerAutoId)                             
	 SELECT (CASE                                                                                                                                                 
	 WHEN @custType=2 THEN ISNULL(WHminPrice,0)                                                                                                                                                
	 WHEN @custType=3  THEN ISNULL(CostPrice,0)                                                                                                               
	 ELSE [MinPrice] END) AS [MinPrice] ,[CostPrice],(case  WHEN  @custType=3 then ISNULL(CostPrice,0) else [Price] end)                                    
	 [Price],CONVERT(DECIMAL(10,2),((PM.[P_SRP]-([Price]/(CASE WHEN Qty=0 THEN 1 ELSE [Qty] END)))/(CASE WHEN PM.[P_SRP]=0 THEN 1 ELSE PM.[P_SRP] END)) * 100) AS GP,                                                                                     
	 isnull((select Top 1 [CustomPrice] from [dbo].[ProductPricingInPriceLevel] AS PPL where PPL.[ProductAutoId] = PD.[ProductAutoId]                   
	 AND PPL.[UnitAutoId] = PD.[UnitType]                                                                                           
	 AND [PriceLevelAutoId] = (SELECT [PriceLevelAutoId] FROM [dbo].[CustomerPriceLevel] WHERE [CustomerAutoId]=@CustomerAutoId)                                                                              
	 Order by [CustomPrice] desc),null) as [CustomPrice],PM.[Stock]/(CASE WHEN Qty=0 THEN 1 ELSE Qty END) as Stock,PM.[TaxRate],PM.[P_SRP] AS [SRP] FROM [dbo].[PackingDetails] As PD                    
	 INNER JOIN [dbo].[ProductMaster] AS PM ON PD.[ProductAutoId]=PM.[AutoId]                                                                                                                                                                   
	 WHERE PD.[ProductAutoId] = @ProductAutoId and PD.[UnitType] = @UnitAutoId      
  END    
  ELSE IF @Opcode=403                                   
  BEGIN 
  SELECT @LocationCode=DB_NAME()
  select(SELECT CompanyName,EmailAddress, [Address],[Website],[MobileNo],[FaxNo],TermsCondition,Logo,case when @LocationCode='psmct.a1whm.com' then 'CT' else 'NJ' end as LocationCode,
  ( 
    SELECT OM.[CreditNo] as OrderNo,CONVERT(VARCHAR(20), OM.[CreditDate], 101) AS OrderDate,                                         
    CM.[AutoId] As CustAutoId,CM.[CustomerId],CM.[CustomerName],CM.[Contact1] + '/' + CM.[Contact2] As Contact,CTy.CustomerType,CT.[TermsDesc],                                    
    BA.[Address] As BillAddr,                                    
    S.[StateName] AS State1,BA.[City] AS City1,BA.[Zipcode] As Zipcode1,SA.[Address] As ShipAddr,S1.[StateName] As State2,SA.[City] AS City2,                                    
    SA.[Zipcode] As Zipcode2,OM.[TotalAmount],ISNULL(OM.[OverallDisc],0) as OverallDisc1,ISNULL(OM.[OverallDiscAmt],0) as OverallDisc,ISNULL(OM.[TotalTax],0) as TotalTax,
	isnull(OM.[GrandTotal],OM.[TotalAmount]) as GrandTotal,                              
  ISNULL((select top 1 PrintLabel from MLTaxMaster where TaxState=BA.State),'') as MLTaxPrintLabel,
		ISNULL((select top 1 PrintLabel from Tax_Weigth_OZ),'') as WeightTaxPrintLabel,
		ISNULL((select top 1 PrintLabel from [TaxTypeMaster] as tt  where tt.State=BA.State),'') as TaxPrintLabel,
    ISNULL(MLQty,0) as MLQty,isnull(MLTax,0) as MLTax,CM.ContactPersonName,ISNULL(AdjustmentAmt,0) as AdjustmentAmt,                            
    ISNULL(OM.WeightTaxAmount,0) as WeightTaxAmount,  --  TaxTypeAutoId Added By Rizwan Ahmad on 11/09/2019 04:00 AM                                  
    CM.BusinessName,EM.[FirstName] + ' ' + EM.[LastName] As SalesPerson,
	 isnull((
		SELECT CM.[CategoryName],CM.[CategoryId],OIM.ProductAutoId,PM.[ProductId],PM.[ProductName],PM.[ProductLocation],                                  
		OIM.[UnitAutoId] AS UnitAutoId,UM.[UnitType],OIM.[QtyPerUnit],convert(decimal(10,2),OIM.[UnitPrice]) as UnitPrice,                                    
		convert(decimal(10,2),OIM.NetAmount) AS  [NetPrice],OIM.AcceptedQty AS QtyShip,'' AS BarCode                                    
		FROM [dbo].[CreditItemMaster] AS OIM                                     
		INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                                    
		INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitAutoId                                     
		INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId]    
		WHERE CreditAutoId=@CreditAutoId                                  
		ORDER BY CM.[CategoryName] ASC for json path, INCLUDE_NULL_VALUES
    ),'[]') as CreditItems      
	FROM [dbo].[CreditMemoMaster] As OM                                    
    INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                               
    INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]                                    
    INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[CustomerAutoId] = OM.[CustomerAutoId] AND BA.IsDefault=1                                    
    INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[CustomerAutoId] = OM.[CustomerAutoId] AND SA.IsDefault=1                                    
    LEFT JOIN [dbo].[CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                                           
    INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                                     
    INNER JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                                     
    left JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.CreatedBy                                    
    WHERE OM.CreditAutoId=@CreditAutoId for json path, INCLUDE_NULL_VALUES
  ) as CreditDetails
  FROM CompanyDetails for json path, INCLUDE_NULL_VALUES) as company                    
    END  
	
	ELSE IF @Opcode=404                               
   BEGIN                                  
  IF EXISTS(SELECT [SecurityValue] FROM [dbo].[TBL_MST_SecurityMaster] WHERE [SecurityValue]=@CheckSecurity                                   
   and SecurityType=10 and typeEvent=1)                                  
   BEGIN                                  
                                   
		SET @isException=0
		SET @exceptionMessage='Key match'
                                       
   END                                  
   ELSE                                  
   BEGIN                                  
    Set @isException=1                                      
    Set @exceptionMessage='Invalid Security Key'                                  
   END                                  
   END          

     ELSE IF @Opcode=405
  BEGIN
        Declare @LogRemark varchar(500)
        UPDATE [CreditMemoMaster] SET IsMLTaxApply=case when IsMLTaxApply=1 then 0 else 1 end WHERE CreditNo=@CreditNo  

		SET @Status=(Select IsMLTaxApply FROM [CreditMemoMaster] WHERE CreditNo=@CreditNo  )

		SET @CreditAutoId=(Select CreditAutoId FROM [CreditMemoMaster] WHERE CreditNo=@CreditNo  )

		Update [CreditMemoMaster] SET MLTaxRemark=@MLTaxRemark Where CreditNo= @CreditNo

		SET @LogRemark = REPLACE((SELECT [Action]+' for '+[ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=(case when
		@Status=0 then 47 else 46 end)),'[MemoNo]',@CreditNo)

		INSERT INTO [dbo].[CreditMemoLog] ([ActionAutoId],[EmpAutoId],[LogDate],[Remarks],[CreditAutoId])                                                
		VALUES(case when @Status=0 then 47 else 46 end,@EmpAutoId,getdate(),@LogRemark,@CreditAutoId)  
  END
   if @Opcode = 51        
 begin        
	 select * from TBL_MST_SecurityMaster where SecurityValue = @SecurityKey and SecurityType = @EmpAutoId and typeEvent = 1        
 end  
 if @Opcode = 50        
 begin        
    IF EXISTS(SELECT TOP 1 CreditAutoId FROM  CreditMemoMaster where CreditAutoId = @CreditAutoId AND OrderAutoId is null)
	BEGIN

		IF EXISTS(SELECT TOP 1 CreditAutoId FROM  CreditMemoMaster where CreditAutoId = @CreditAutoId AND STATUS=3)
		BEGIN
			insert into [dbo].[ProductStockUpdateLog]([ProductId],[OledStock],[NewStock],[UserAutoId],[DateTime],ReferenceId,ActionRemark,ReferenceType)
			SELECT pm.ProductId,isnull([Stock],0),(isnull(pm.Stock,0)-(ISNULL(AcceptedTotalQty,0))),@EmpAutoId,GETDATE(),dt.CreditAutoId,
			'Credit Momo No -(' + om.CreditNo + ') has been cancelled by '+(Select FirstName+' '+LastName from Employeemaster where autoid=@EmpAutoId)+'.','CreditMemoMaster' 
			FROM (
			select cim.CreditAutoId,cim.ProductAutoId,SUM(ISNULL(QtyReturn_Fresh,0)*Qty) as AcceptedTotalQty  from CreditItemMaster as cim
					inner join PackingDetails as pd on pd.ProductAutoId=cim.ProductAutoId and 
					cim.QtyPerUnit_Fresh=pd.UnitType
					where CreditAutoId=@CreditAutoId
					group by cim.ProductAutoId,cim.CreditAutoId
			) as dt
			inner join productmaster pm on dt.[ProductAutoId] = PM.[AutoId]
			inner join CreditMemoMaster om on dt.CreditAutoId=om.CreditAutoId
			where dt.CreditAutoId=@CreditAutoId and isnull(dt.AcceptedTotalQty,0)>0   

			UPDATE PM SET [Stock] = isnull([Stock],0) - (isnull(AcceptedTotalQty,0)) FROM [dbo].[ProductMaster] AS PM                                                                                                   
			INNER JOIN (
					select cim.ProductAutoId,SUM(ISNULL(QtyReturn_Fresh,0)*Qty) as AcceptedTotalQty  from CreditItemMaster as cim
					inner join PackingDetails as pd on pd.ProductAutoId=cim.ProductAutoId and 
					cim.QtyPerUnit_Fresh=pd.UnitType
					where CreditAutoId=@CreditAutoId
					group by cim.ProductAutoId
			) as dt ON dt.[ProductAutoId] = PM.[AutoId] 
		END

		update CreditMemoMaster set Status = 6,CancelRemark = @CancelRemarks,UpdatedBy = @EmpAutoId,UpdatedDate = GETDATE() where CreditAutoId = @CreditAutoId        
		SET @Remark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=20), '[MemoNo]', (SELECT CreditNo FROM CreditMemoMaster where CreditAutoId = @CreditAutoId))          
	
	INSERT INTO CreditMemoLog(CreditAutoId,ActionAutoId,EmpAutoId,Remarks,LogDate,ActionIPAddress)          
		VALUES(@CreditAutoId,47,@EmpAutoId, @Remark + '<br><b>Remark:</b> ' + @CancelRemarks ,GETDATE(),@IPAddress)   
	END  
	ELSE
	BEGIN
	    set  @OrderAutoId  =(SELECT top 1 OrderAutoId FROM  CreditMemoMaster where CreditAutoId = @CreditAutoId)
		SET @isException=1          
		SET @exceptionMessage='You can not cancel this credit memo becuase It is already attached in this '+ (select OrderNO from OrderMaster where AutoId=@OrderAutoId)+' order.'
	END	     
 end        
 END TRY                                    
 BEGIN CATCH                                    
   SET @isException=1                                    
   SET @exceptionMessage=ERROR_MESSAGE()                                    
 END CATCH                                    
END 
GO
