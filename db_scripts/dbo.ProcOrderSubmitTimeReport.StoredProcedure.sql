USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcOrderSubmitTimeReport]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
alter procedure [dbo].[ProcOrderSubmitTimeReport]    
@Opcode INT=NULL,    
@CustomerAutoId varchar (10) = null,   
@SalePersonAutoId int = null,    
@DateFrom date =NULL,    
@DateTo date =NULL,    
@PageIndex INT=1,    
@PageSize INT=10,    
@RecordCount INT=null,    
@RouteStatus int=null,  
@isException bit out,    
@exceptionMessage varchar(max) out     
as    
    
BEGIN     
 BEGIN TRY    
  Set @isException=0    
  Set @exceptionMessage='Success'       
  if @Opcode = 41     
  begin    
  Select ROW_NUMBER() OVER(ORDER BY OrderDate) AS RowNumber, format(OrderDate,'MM/dd/yyyy hh:mm tt') as OrderDate
  ,cm.CustomerName,OrderNo,GrandTotal,sm.StatusType as Status,om.Status as StatusCode   
 , case when routestatus=1 then 'ON' else 'OFF' end RouteStatus,em.FirstName+' '+LastName as SalePerson into #RESULT from OrderMaster om    
  inner join CustomerMaster cm on cm.AutoId = om.CustomerAutoId    
  inner join EmployeeMaster em on em.AutoId=cm.SalesPersonAutoId  
  inner join StatusMaster sm on sm.AutoId = om.Status and  sm.Category = 'OrderMaster'  
  where  
   (@SalePersonAutoId is null or @SalePersonAutoId = 0 or cm.SalesPersonAutoId = @SalePersonAutoId)    
        and (@DateFrom is null or @DateFrom = '' or @DateTo is null or @DateTo = '' or           
        (CONVERT(date,OrderDate)  between CONVERT(date,@DateFrom) and CONVERT(date,@DateTo)))    
  and (isnull(om.RouteStatus,0)=@RouteStatus or @RouteStatus=2)  
  and om.Status!=8  
  SELECT COUNT(RowNumber) AS RecordCount, @PageSize AS PageSize, @PageIndex AS PageIndex,Format(getdate(),'MM/dd/yyyy hh:mm tt') as PrintDate FROM #RESULT    
    
   SELECT * FROM #RESULT    
   WHERE @PageSize = 0 or RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize)-1       
    
	 SELECT ISNULL(SUM(GrandTotal),0.00) AS GrandTotal from  #RESULT    
  end    
  if @Opcode = 42    
  begin    
   
      select AutoId as SID,FirstName+' '+LastName as SP from EmployeeMaster where EmpType=2 and Status=1 order by Sp ASC 
	  for json path
  end    
    
    
  END TRY    
 BEGIN CATCH    
  Set @isException=1    
  Set @exceptionMessage=ERROR_MESSAGE()    
 END CATCH      
 end 
GO
