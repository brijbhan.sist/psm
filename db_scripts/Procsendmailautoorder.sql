USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[Procsendmailautoorder]    Script Date: 8/12/2021 4:26:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER   PROCEDURE [dbo].[Procsendmailautoorder]  
AS
BEGIN
declare @Subject varchar(250)='Ticket Report',@html varchar(Max)=null;

select * into #t1 from
(
select 1 as row,  'PSM NJ' as Location,

(select count(1) from [psmnj.a1whm.com].[dbo].ErrorTicketMaster where DeveloperStatus='Open') as [Open],
(select count(1) from [psmnj.a1whm.com].[dbo].ErrorTicketMaster where DeveloperStatus='Under Process') as [Under Process],
(select count(1) from [psmnj.a1whm.com].[dbo].ErrorTicketMaster where DeveloperStatus='Close' and convert(date,ticketDate)=convert(date,getdate())) as [Close]
UNION
select 5 as row, 'PSM NPA' as Location,
(select count(1) from [psmnpa.a1whm.com].[dbo].ErrorTicketMaster where DeveloperStatus='Open') as [Open],
(select count(1) from [psmnpa.a1whm.com].[dbo].ErrorTicketMaster where DeveloperStatus='Under Process') as [Under Process],
(select count(1) from [psmnpa.a1whm.com].[dbo].ErrorTicketMaster where DeveloperStatus='Close' and convert(date,ticketDate)=convert(date,getdate())) as [Close]
UNION
select 4 as row, 'PSM PA' as Location,

(select count(1) from [psmpa.a1whm.com].[dbo].ErrorTicketMaster where DeveloperStatus='Open') as [Open],
(select count(1) from [psmpa.a1whm.com].[dbo].ErrorTicketMaster where DeveloperStatus='Under Process') as [Under Process],
(select count(1) from [psmpa.a1whm.com].[dbo].ErrorTicketMaster where DeveloperStatus='Close' and convert(date,ticketDate)=convert(date,getdate())) as [Close]
UNION
select 6 as row, 'PSM WPA' as Location,

(select count(1) from [psmwpa.a1whm.com].[dbo].ErrorTicketMaster where DeveloperStatus='Open') as [Open],
(select count(1) from [psmwpa.a1whm.com].[dbo].ErrorTicketMaster where DeveloperStatus='Under Process') as [Under Process],
(select count(1) from [psmwpa.a1whm.com].[dbo].ErrorTicketMaster where DeveloperStatus='Close' and convert(date,ticketDate)=convert(date,getdate())) as [Close]
UNION
select 3 as row, 'PSM CT' as Location,

(select count(1) from [psmct.a1whm.com].[dbo].ErrorTicketMaster where DeveloperStatus='Open') as [Open],
(select count(1) from [psmct.a1whm.com].[dbo].ErrorTicketMaster where DeveloperStatus='Under Process') as [Under Process],
(select count(1) from [psmct.a1whm.com].[dbo].ErrorTicketMaster where DeveloperStatus='Close' and convert(date,ticketDate)=convert(date,getdate())) as [Close]
UNION
select  2 as row, 'PSM NY' as Location,

(select count(1) from [psmny.a1whm.com].[dbo].ErrorTicketMaster where DeveloperStatus='Open') as [Open],
(select count(1) from [psmny.a1whm.com].[dbo].ErrorTicketMaster where DeveloperStatus='Under Process') as [Under Process],
(select count(1) from [psmny.a1whm.com].[dbo].ErrorTicketMaster where DeveloperStatus='Close' and convert(date,ticketDate)=convert(date,getdate())) as [Close]

) as TX ORDER BY row
	 	

SELECT ROW_NUMBER() OVER (ORDER BY [row]) AS RowNumber,
'<tr><td>' +  [Location] + '</td><td style=" text-align:center;">' +convert(varchar(50),[Open]) + '</td><td style=" text-align:center;">' +convert(varchar(50),[Under Process]) + '</td><td style=" text-align:center;">'+ convert(varchar(50),[Close])+ '</td></tr>' AS HtmlData
into #result
FROM  #t1

set @html =('<html><head><title></title><style>
#customers {
  font-family: Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 400px; 
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
}
</style>
</head></head>
    <body><br><br><table id="customers"><tr>
    <th>Location</th><th>Open</th><th>Under Process</th><th>Close</th></tr>')
declare @i int=1;
while(@i<=6)
begin
	set @html+=(select t.HtmlData from #result as t where t.RowNumber=@i)
	SET @i=@I+1;
End
set @html+='</table></body></html>';
select @html

	Declare  @FromName varchar(500),@FromEmailId varchar(500),@Port int,@SMTPServer varchar(150),@smtp_userName varchar(150),
	@Password varchar(50),@SSL  int,@BCCEmailId varchar(max),@ToEmailId varchar(500),@CCEmailId varchar(max)

	
	SELECT @FromEmailId=FromEmail,@FromName=DisplayName,@smtp_userName=SMTP_USERNAME,@Password=SMTP_PASSWORD,
	@SMTPServer=HOST,@Port=SMTPPORT,@SSL=EnableSsl from EmailServerMaster_AWS 

	select @ToEmailId=ToEmail,@BCCEmailId=BCCEmail,@CCEmailId=CCEmail from Tbl_ReportEmailReceiver where ReportId='R00013'
	EXEC [Emailing_DB].[dbo].[ProcSubmitEmail] 
			@Opcode=11,
			@FromEmailId =@FromEmailId,
			@FromName = @FromName,
			@smtp_userName=@smtp_userName,
			@Password = @Password,
			@SMTPServer = @SMTPServer,
			@Port =@Port,
			@SSL =@SSL,
			@ToEmailId =@ToEmailId,
			@CCEmailId =@CCEmailId,
			@BCCEmailId =@BCCEmailId,  
			@Subject =@Subject,
			@EmailBody = @html,
			@SentDate ='',
			@Status =0,
			@SourceApp ='PSM',
			@SubUrl ='Total Order', 
			@isException=0,
			@exceptionMessage=''  
	

end