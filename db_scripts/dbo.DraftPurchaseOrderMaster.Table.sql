USE [psmnj.a1whm.com]
GO
/****** Object:  Table [dbo].[DraftPurchaseOrderMaster]    Script Date: 12/03/2019 5.44.05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DraftPurchaseOrderMaster](
	[DraftAutoId] [int] IDENTITY(1,1) NOT NULL,
	[PODate] [datetime] NULL,
	[VenderAutoId] [int] NULL,
	[PORemarks] [varchar](500) NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL
) ON [PRIMARY]
GO
