USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_DeliveredAmtDue]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION  [dbo].[FN_DeliveredAmtDue]
(
	 @orderAutoId int
)
RETURNS decimal(10,2)
AS
BEGIN
	DECLARE @AmtDue decimal(10,2)	
	set @AmtDue=isnull((select isnull(PayableAmount,0)-isnull(AmtPaid,0) from DELIVEREDORDERS where oRDERAutoId=@orderAutoId),0.00)
	RETURN @AmtDue
END

--alter table deliveredOrders drop column  AmtDue 
--alter table deliveredOrders add  AmtDue AS DBO.FN_DeliveredAmtDue(OrderAutoId)



--SELECT * FROM deliveredOrders---AmtDue
GO
