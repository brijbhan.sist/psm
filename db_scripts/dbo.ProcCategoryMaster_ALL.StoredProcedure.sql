ALTER PROCEDURE [dbo].[ProcCategoryMaster_ALL]    
@CategoryId  VARCHAR(50)=Null    
AS    
BEGIN    
  --FOR CT DB  
  IF EXISTS(SELECT * FROM [psmct.a1whm.com].[dbo].[CategoryMaster] WHERE CategoryId=@CategoryId)    
  BEGIN    
   UPDATE [psmct.a1whm.com].[dbo].[CategoryMaster]     
   SET CategoryName = (SELECT CategoryName FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId),    
   Description = (SELECT Description FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId),     
   Status = (SELECT Status FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId),    
   SeqNo= (SELECT SeqNo FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId) ,  
   IsShow= (SELECT IsShow FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId) ,   
   UpdateDate=GETDATE()  
   WHERE CategoryID = @CategoryId    
  END    
  ELSE    
  BEGIN    
   INSERT INTO [psmct.a1whm.com].[dbo].[CategoryMaster]([CategoryId], [CategoryName],Description,Status,CreatedDate,UpdateDate,SeqNo,IsShow)    
   select [CategoryId], [CategoryName],Description,Status,GETDATE(),GETDATE(),SeqNo,IsShow from [CategoryMaster] WHERE CategoryID = @CategoryId    
  END   
  --FOR PA DB  
  IF EXISTS(SELECT * FROM [psmpa.a1whm.com].[dbo].[CategoryMaster] WHERE CategoryId=@CategoryId)    
  BEGIN    
   UPDATE [psmpa.a1whm.com].[dbo].[CategoryMaster]     
   SET CategoryName = (SELECT CategoryName FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId),    
   Description = (SELECT Description FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId),     
   Status = (SELECT Status FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId)  ,  
   SeqNo= (SELECT SeqNo FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId) ,  
   IsShow= (SELECT IsShow FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId) ,   
   UpdateDate=GETDATE()  
   WHERE CategoryID = @CategoryId    
  END    
  ELSE    
  BEGIN    
   INSERT INTO [psmpa.a1whm.com].[dbo].[CategoryMaster]([CategoryId], [CategoryName],Description,Status,CreatedDate,UpdateDate,SeqNo,IsShow)    
   select [CategoryId], [CategoryName],Description,Status,GETDATE(),GETDATE(),SeqNo,IsShow from [CategoryMaster] WHERE CategoryID = @CategoryId    
  END  
  --FOR NPA DB  
  IF EXISTS(SELECT * FROM [psmnpa.a1whm.com].[dbo].[CategoryMaster] WHERE CategoryId=@CategoryId)    
  BEGIN    
   UPDATE [psmnpa.a1whm.com].[dbo].[CategoryMaster]     
   SET CategoryName = (SELECT CategoryName FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId),    
   Description = (SELECT Description FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId),     
   Status = (SELECT Status FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId),    
   SeqNo= (SELECT SeqNo FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId) ,  
   IsShow= (SELECT IsShow FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId) ,   
   UpdateDate=GETDATE()   
   WHERE CategoryID = @CategoryId    
  END    
  ELSE    
  BEGIN    
   INSERT INTO [psmnpa.a1whm.com].[dbo].[CategoryMaster]([CategoryId], [CategoryName],Description,Status,CreatedDate,UpdateDate,SeqNo,IsShow)    
   select [CategoryId], [CategoryName],Description,Status,GETDATE(),GETDATE(),SeqNo,IsShow from [CategoryMaster] WHERE CategoryID = @CategoryId    
  END  
  
  --FOR WPA DB  
  IF EXISTS(SELECT * FROM [psmwpa.a1whm.com].[dbo].[CategoryMaster] WHERE CategoryId=@CategoryId)    
  BEGIN    
   UPDATE [psmwpa.a1whm.com].[dbo].[CategoryMaster]     
   SET CategoryName = (SELECT CategoryName FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId),    
   Description = (SELECT Description FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId),     
   Status = (SELECT Status FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId),    
   SeqNo= (SELECT SeqNo FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId) ,  
   IsShow= (SELECT IsShow FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId) ,   
   UpdateDate=GETDATE()  
   WHERE CategoryID = @CategoryId    
  END    
  ELSE    
  BEGIN    
   INSERT INTO [psmwpa.a1whm.com].[dbo].[CategoryMaster]([CategoryId], [CategoryName],Description,Status,CreatedDate,UpdateDate,SeqNo,IsShow)    
   select [CategoryId], [CategoryName],Description,Status,GETDATE(),GETDATE(),SeqNo,IsShow from [CategoryMaster] WHERE CategoryID = @CategoryId    
  END  
  
  --FOR NY DB  
  IF EXISTS(SELECT * FROM [psmny.a1whm.com].[dbo].[CategoryMaster] WHERE CategoryId=@CategoryId)    
  BEGIN    
   UPDATE [psmny.a1whm.com].[dbo].[CategoryMaster]     
   SET CategoryName = (SELECT CategoryName FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId),    
   Description = (SELECT Description FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId),     
   Status = (SELECT Status FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId),    
   SeqNo= (SELECT SeqNo FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId) ,  
   IsShow= (SELECT IsShow FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId) ,   
   UpdateDate=GETDATE()  
   WHERE CategoryID = @CategoryId    
  END    
  ELSE    
  BEGIN    
   INSERT INTO [psmny.a1whm.com].[dbo].[CategoryMaster]([CategoryId], [CategoryName],Description,Status,CreatedDate,UpdateDate,SeqNo,IsShow)    
   select [CategoryId], [CategoryName],Description,Status,GETDATE(),GETDATE(),SeqNo,IsShow from [CategoryMaster] WHERE CategoryID = @CategoryId    
  END  
  

  --FOR ESYWHM DB  
  IF EXISTS(SELECT * FROM [psmnj.easywhm.com].[dbo].[CategoryMaster] WHERE CategoryId=@CategoryId)    
  BEGIN    
   UPDATE [psmnj.easywhm.com].[dbo].[CategoryMaster]     
   SET CategoryName = (SELECT CategoryName FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId),    
   Description = (SELECT Description FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId),     
   Status = (SELECT Status FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId),    
   SeqNo= (SELECT SeqNo FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId) ,  
   IsShow= (SELECT IsShow FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId) ,   
   UpdateDate=GETDATE()  
   WHERE CategoryID = @CategoryId    
  END    
  ELSE    
  BEGIN    
   INSERT INTO [psmnj.easywhm.com].[dbo].[CategoryMaster]([CategoryId], [CategoryName],Description,Status,CreatedDate,UpdateDate,SeqNo,IsShow)    
   select [CategoryId], [CategoryName],Description,Status,GETDATE(),GETDATE(),SeqNo,IsShow from [CategoryMaster] WHERE CategoryID = @CategoryId    
  END  
  
  IF EXISTS(SELECT * FROM [psmct.easywhm.com].[dbo].[CategoryMaster] WHERE CategoryId=@CategoryId)    
  BEGIN    
   UPDATE [psmct.easywhm.com].[dbo].[CategoryMaster]     
   SET CategoryName = (SELECT CategoryName FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId),    
   Description = (SELECT Description FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId),     
   Status = (SELECT Status FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId),    
   SeqNo= (SELECT SeqNo FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId) ,  
   IsShow= (SELECT IsShow FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId) ,   
   UpdateDate=GETDATE()  
   WHERE CategoryID = @CategoryId    
  END    
  ELSE    
  BEGIN    
   INSERT INTO [psmct.easywhm.com].[dbo].[CategoryMaster]([CategoryId], [CategoryName],Description,Status,CreatedDate,UpdateDate,SeqNo,IsShow)    
   select [CategoryId], [CategoryName],Description,Status,GETDATE(),GETDATE(),SeqNo,IsShow from [CategoryMaster] WHERE CategoryID = @CategoryId    
  END  
  
  IF EXISTS(SELECT * FROM [psmnpa.easywhm.com].[dbo].[CategoryMaster] WHERE CategoryId=@CategoryId)    
  BEGIN    
   UPDATE [psmnpa.easywhm.com].[dbo].[CategoryMaster]     
   SET CategoryName = (SELECT CategoryName FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId),    
   Description = (SELECT Description FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId),     
   Status = (SELECT Status FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId),    
   SeqNo= (SELECT SeqNo FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId) ,  
   IsShow= (SELECT IsShow FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId) ,   
   UpdateDate=GETDATE()  
   WHERE CategoryID = @CategoryId    
  END    
  ELSE    
  BEGIN    
   INSERT INTO [psmnpa.easywhm.com].[dbo].[CategoryMaster]([CategoryId], [CategoryName],Description,Status,CreatedDate,UpdateDate,SeqNo,IsShow)    
   select [CategoryId], [CategoryName],Description,Status,GETDATE(),GETDATE(),SeqNo,IsShow from [CategoryMaster] WHERE CategoryID = @CategoryId    
  END  
  
  IF EXISTS(SELECT * FROM [psmpa.easywhm.com].[dbo].[CategoryMaster] WHERE CategoryId=@CategoryId)    
  BEGIN    
   UPDATE [psmpa.easywhm.com].[dbo].[CategoryMaster]     
   SET CategoryName = (SELECT CategoryName FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId),    
   Description = (SELECT Description FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId),     
   Status = (SELECT Status FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId),    
   SeqNo= (SELECT SeqNo FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId) ,  
   IsShow= (SELECT IsShow FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId) ,   
   UpdateDate=GETDATE()  
   WHERE CategoryID = @CategoryId    
  END    
  ELSE    
  BEGIN    
   INSERT INTO [psmpa.easywhm.com].[dbo].[CategoryMaster]([CategoryId], [CategoryName],Description,Status,CreatedDate,UpdateDate,SeqNo,IsShow)    
   select [CategoryId], [CategoryName],Description,Status,GETDATE(),GETDATE(),SeqNo,IsShow from [CategoryMaster] WHERE CategoryID = @CategoryId    
  END  
  
  
  IF EXISTS(SELECT * FROM [psmwpa.easywhm.com].[dbo].[CategoryMaster] WHERE CategoryId=@CategoryId)    
  BEGIN    
   UPDATE [psmwpa.easywhm.com].[dbo].[CategoryMaster]     
   SET CategoryName = (SELECT CategoryName FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId),    
   Description = (SELECT Description FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId),     
   Status = (SELECT Status FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId),    
   SeqNo= (SELECT SeqNo FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId) ,  
   IsShow= (SELECT IsShow FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId) ,   
   UpdateDate=GETDATE()  
   WHERE CategoryID = @CategoryId    
  END    
  ELSE    
  BEGIN    
   INSERT INTO [psmwpa.easywhm.com].[dbo].[CategoryMaster]([CategoryId], [CategoryName],Description,Status,CreatedDate,UpdateDate,SeqNo,IsShow)    
   select [CategoryId], [CategoryName],Description,Status,GETDATE(),GETDATE(),SeqNo,IsShow from [CategoryMaster] WHERE CategoryID = @CategoryId    
  END  

   IF EXISTS(SELECT * FROM [psmny.easywhm.com].[dbo].[CategoryMaster] WHERE CategoryId=@CategoryId)    
  BEGIN    
   UPDATE [psmny.easywhm.com].[dbo].[CategoryMaster]     
   SET CategoryName = (SELECT CategoryName FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId),    
   Description = (SELECT Description FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId),     
   Status = (SELECT Status FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId),    
   SeqNo= (SELECT SeqNo FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId) ,  
   IsShow= (SELECT IsShow FROM [dbo].CategoryMaster  WHERE CategoryID = @CategoryId) ,   
   UpdateDate=GETDATE()  
   WHERE CategoryID = @CategoryId    
  END    
  ELSE    
  BEGIN    
   INSERT INTO [psmny.easywhm.com].[dbo].[CategoryMaster]([CategoryId], [CategoryName],Description,Status,CreatedDate,UpdateDate,SeqNo,IsShow)    
   select [CategoryId], [CategoryName],Description,Status,GETDATE(),GETDATE(),SeqNo,IsShow from [CategoryMaster] WHERE CategoryID = @CategoryId    
  END  
  
END