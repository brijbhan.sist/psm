alter PROCEDURE [dbo].[ProcStoreCreditAmount_Issues_EmailSent] 
AS      
BEGIN     
	Declare @html varchar(max)='',@tr varchar(max)='',@Count int,@Num int=1,@CustomerId varchar(50),
	@CustomerName varchar(250),@CreditAmount varchar(250),@AvailStoreCredit varchar(250)
	 
	select  ROW_NUMBER() OVER(order by CustomerAutoId) AS RowNumber, CustomerId,CustomerName,CreditAmount,AvailStoreCredit
	into #Result from CustomerCreditMaster AS CCM
	INNER JOIN CustomerMaster AS CM ON CM.AutoId=CCM.CustomerAutoId where CreditAmount!=AvailStoreCredit

	set @html = '<html><head><style>
td{padding: 3px 8px;}table {width: 100%;}
table, th, td {border: 1px solid black;border-collapse: collapse;}
body {font-size: 14px;font-family: monospace;}
.center {text-align: center;}.right {text-align: right;}
thead > tr {background: #fff;color: #000;
font-weight: 700;border-top: 1px solid #5f7c8a;}
thead > tr > td {text-align: center !important;}
.InvContent {width: 100%;}.tdLabel {font-weight: 700;}
@media print {#btnPrint {display: none;}}
legend {border-bottom: 1px solid #5f7c8a;}.tableCSS > tbody > tr > td,.tableCSS > thead > tr > td,.tableCSS > tfoot > tr > td {border: 1px solid #5f7c8a;}#tblReportPaymentDailyReport tbody tr td .ElectronicTransfer {color: black}table {border-collapse: collapse;}table,td,th {border: 1px solid black;}</style></head> 
<div class="InvContent" style="width: 100%;">
<table border="1" style="width:100%;border: 1px solid black;border-collapse:collapse;"><thead><tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">	
				<th style="text-align: center !important;"> Customer Id </th>
				<th style="text-align: center !important;">  Customer Name </th>
				<th style="text-align: center !important;"> Credit Amount </th>
				<th style="text-align: center !important;"> Available Credit Amount </th> 
				</tr>' +   
				'</thead><tbody>'; 
	SET @Count=(select count(1) from #Result)
	
	if(@count>0)
	begin	
		while(@num<=@Count)
	    BEGIN 
		    SELECT @CustomerId=CustomerId,@CustomerName=CustomerName,@CreditAmount=CreditAmount,@AvailStoreCredit=AvailStoreCredit FROM #Result	where RowNumber = @num 
			set @tr+='<tr><td>'+convert(varchar(50), @CustomerId)+'</td><td>'+@CustomerName+'</td><td style="text-align:right">'+convert(varchar(50), @CreditAmount)+'</td><td style="text-align:right">'+convert(varchar(50), @AvailStoreCredit)+'</td></tr>';
			set @num = @num + 1
		END
		SET @html+=@tr+'</tbody></table>'
		
	
	
			--------------Email Code---------------

			Declare  @Subject varchar(max),@FromName varchar(1000),@FromEmailId varchar(1000),@Port int,@SMTPServer varchar(1000),@smtp_userName varchar(1000),@Password varchar(1000),@SSL  int,
			@BCCEmailId varchar(max),@MigrateEmailId varchar(1000),@CCEmailId varchar(max)
			set @Subject = 'Store Credit Bugs from ' +UPPER((select CompanyDistributerName from dbo.CompanyDetails))+' : ' + 
			format(GETDATE(),'MM/dd/yyy hh:mm tt');
			
			SELECT @FromEmailId=FromEmail,@FromName=DisplayName,@smtp_userName=SMTP_USERNAME,@Password=SMTP_PASSWORD,
			@SMTPServer=HOST,@Port=SMTPPORT,@SSL=EnableSsl from 
			EmailServerMaster_AWS 

			set @BCCEmailId=(select top 1 BCCEmail from [dbo].[Tbl_ReportEmailReceiver] where autoid=11);
			set @MigrateEmailId=(select top 1 ToEmail from [dbo].[Tbl_ReportEmailReceiver] where autoid=11)
			SET @CCEmailId=(select top 1 CCEmail from [dbo].[Tbl_ReportEmailReceiver] where autoid=11)
   
		    EXEC [Emailing_DB].[dbo].[ProcSubmitEmail] 
			@Opcode=11,
			@FromEmailId =@FromEmailId,
			@FromName = @FromName,
			@smtp_userName=@smtp_userName,
			@Password = @Password,
			@SMTPServer = @SMTPServer,
			@Port =@Port,
			@SSL =@SSL,
			@ToEmailId =@MigrateEmailId,
			@CCEmailId =@CCEmailId,
			@BCCEmailId =@BCCEmailId,  
			@Subject =@Subject,
			@EmailBody = @html,
			@SentDate ='',
			@Status =0,
			@SourceApp ='PSM',
			@SubUrl ='Store Credit Bugs', 
			@isException=0,
			@exceptionMessage=''  
	
	END
END