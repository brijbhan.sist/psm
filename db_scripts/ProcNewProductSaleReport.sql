alter procedure [dbo].[ProcNewProductSaleReport]                      
@Opcode INT=NULL,                      
@ProductAutoId int = null,
@PriceLevelAutoId int = null,
@BrandAutoIdSring varchar(200) = null,                     
@CategoryAutoId int = null,      
@CustomerAutoId  int = null,                    
@SubCategoryId int = null,                      
@OrderStatus int = null,                      
@EmpAutoId int = null,                      
@SalesPerson  int=NULL, 
@SalesPersonString varchar(200) = null,  
@FromDate date =NULL,                      
@ToDate date =NULL,                      
@PageIndex INT=1,                      
@PageSize INT=10,                      
@RecordCount INT=null,                      
@isException bit out,                      
@exceptionMessage varchar(max) out                      
as                   
BEGIN                       
 BEGIN TRY                      
  Set @isException=0                      
  Set @exceptionMessage='Success'                      
                      
                 
   IF @OPCODE=42                      
  BEGIN                      
  SELECT ROW_NUMBER() OVER(ORDER BY productid,ProductName asc) AS RowNumber, * into #Results48        
  FROM      
  (  
	select em.FirstName,t1.ProductId,t1.ProductName,t1.UnitType,t1.Qty as [Pcperunit],
	isnull(pppl.CustomPrice,0) as CustomPrice,
	convert(decimal(18,2),(t1.QtyDel/t1.Qty)) as [Sold_Default_Qty],
	t1.QtyDel as [Sold_Pc],
	t1.NetPrice as [SaleRev],
	convert(decimal(18,2),((t1.QtyDel-isnull(t2.QtyDel,0))/t1.Qty)) as [Net_Sold_Default_Qty],
	(t1.QtyDel-isnull(t2.QtyDel,0)) as [Net_Sold_Pc],
	(t1.NetPrice - isnull(t2.NetPrice,0)) as Net_Sale_Rev,
	t1.Del_CostPrice-isnull(((case when t1.QtyDel != 0 then (t1.Del_CostPrice/t1.QtyDel) else 0 end)*t2.QtyDel),0) as costP
	from (
		select om.SalesPersonAutoId, pd.ProductAutoId, pm.ProductId, pm.ProductName, pm.PackingAutoId, um.UnitType, pd.Qty,
		sum(doi.QtyDel) as QtyDel, sum(doi.NetPrice) as NetPrice, sum((doi.Del_CostPrice/doi.QtyPerUnit)*doi.QtyDel) as Del_CostPrice 
		--sum(doi.QtyDel) as QtyDel, sum(doi.NetPrice) as NetPrice, sum(doi.Del_CostPrice) as Del_CostPrice
		from OrderMaster as om
		inner join Delivered_Order_Items as doi on doi.OrderAutoId=om.AutoId
		inner join ProductMaster as pm on pm.AutoId=doi.ProductAutoId
		inner join PackingDetails as pd on pd.ProductAutoId=pm.AutoId and pd.UnitType=pm.PackingAutoId
		inner join UnitMaster as um on um.AutoId=pm.PackingAutoId
		where (@FromDate is null or @ToDate is null or (CONVERT(date,OrderDate)       
        between convert(date,@FromDate) and CONVERT(date,@ToDate))) and 
		om.Status=11
		group by om.SalesPersonAutoId, pm.ProductId, pm.ProductName, um.UnitType, pd.Qty,pd.ProductAutoId, pm.PackingAutoId
		) as t1
	left join (
		select cmm.CreatedBy, pd.ProductAutoId, pm.ProductId, pm.ProductName, pm.PackingAutoId, um.UnitType, pd.Qty,
		sum(cim.TotalPeice) as QtyDel, sum(cim.NetAmount) as NetPrice
		from CreditMemoMaster as cmm
		inner join CreditItemMaster as cim on cim.CreditAutoId=cmm.CreditAutoId
		inner join ProductMaster as pm on pm.AutoId=cim.ProductAutoId
		inner join PackingDetails as pd on pd.ProductAutoId=pm.AutoId and pd.UnitType=pm.PackingAutoId
		inner join UnitMaster as um on um.AutoId=pm.PackingAutoId
		where (@FromDate is null or @ToDate is null or (CONVERT(date,cmm.ApprovalDate)       
          between convert(date,@FromDate) and CONVERT(date,@ToDate)))
		and cmm.Status=3 group by cmm.CreatedBy, pm.ProductId, pm.ProductName, um.UnitType, pd.Qty,pd.ProductAutoId, pm.PackingAutoId
		) as t2 on t1.SalesPersonAutoId=t2.CreatedBy and t1.ProductAutoId=t2.ProductAutoId and t1.PackingAutoId=t2.PackingAutoId
	inner join EmployeeMaster as em on em.AutoId=t1.SalesPersonAutoId
	left join ProductPricingInPriceLevel as pppl on pppl.ProductAutoId=t1.ProductAutoId and pppl.UnitAutoId=t1.PackingAutoId and pppl.PriceLevelAutoId=@PriceLevelAutoId
	where ((ISNULL(@SalesPersonString,'0')='0') or (SalesPersonAutoId in (select * from dbo.fnSplitString(@SalesPersonString,','))))     
	and (ISNULL(@ProductAutoId,0) =0 or t1.ProductId=@ProductAutoId)  
	 ) AS T ORDER BY t.ProductId,t.ProductName asc

	  SELECT * FROM #Results48      
	WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))      
         
	SELECT COUNT(ProductId) AS RecordCount, case when @PageSize=0 then COUNT(RowNumber) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Results48      
           
	SELECT ISNULL(SUM(convert(int,Sold_Pc)),0) as Sold_Pc,ISNULL(SUM(Net_Sold_Default_Qty),0.00) as Net_Sold_Default_Qty,ISNULL(SUM(Net_Sold_Pc),0.00) as Net_Sold_Pc
	,ISNULL(SUM(Net_Sale_Rev),0.00) as Net_Sale_Rev,ISNULL(SUM(costP),0.00) as costP FROM #Results48         
  END                      
                    
  IF @Opcode=45                      
  BEGIN  
     select(
   select
   (
   SELECT AutoId AS EmpAutoId,FirstName + ' ' + LastName as EmpName FROM EmployeeMaster where EmpType=2 and status=1 order by EmpName ASC
   for json path, INCLUDE_NULL_VALUES) as SalePerson,
	(SELECT  PM.ProductId as AutoId,CONVERT(VARCHAR(10),PM.ProductId )+' - '+ PM.ProductName AS ProductName FROM ProductMaster AS PM              
  WHERE  PM.ProductStatus=1 order by CONVERT(VARCHAR(10),PM.ProductId )+' - '+ PM.ProductName ASC
	 for json path, INCLUDE_NULL_VALUES) as Product,
	 
	 (select plm.AutoId,plm.PriceLevelName from PriceLevelMaster plm 
	 INNER JOIN CustomerPriceLevel cpl ON cpl.PriceLevelAutoId=plm.AutoId GROUP BY plm.AutoId,plm.PriceLevelName ORDER BY plm.PriceLevelName asc for json path ,INCLUDE_NULL_VALUES) as PriceLevel
	 for json path, INCLUDE_NULL_VALUES
	 )as Dropdown 


  END                              
  END TRY                      
 BEGIN CATCH                      
  Set @isException=1                      
  Set @exceptionMessage=ERROR_MESSAGE()                      
 END CATCH                      
END 
