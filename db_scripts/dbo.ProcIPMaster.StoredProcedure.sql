
ALTER PROCEDURE [dbo].[ProcIPMaster]    
@OpCode int=Null,    
@EmpAutoId  int=Null, 
@AutoId  int=Null, 
@isDefault  int=Null,  
@userIpAddress VARCHAR(100)=NULL,
@description VARCHAR(500)=NULL,

@PageIndex INT=1,          
@PageSize INT=10,          
@RecordCount INT=null,
@isException bit out,    
@exceptionMessage varchar(max) out    
AS    
BEGIN    
 BEGIN TRY    
   SET @isException=0    
   SET @exceptionMessage='success'    
    
  IF @OpCode=11    
  BEGIN   
  IF exists(SELECT AutoId FROM Tbl_ipaddress WHERE IpAddress = @userIpAddress)    
     BEGIN    
		set @isException=1    
		set @exceptionMessage='IP address already exist.'    
     END 
    ELSE    
     BEGIN TRY    
         
     INSERT INTO Tbl_ipaddress(IpAddress,description,isDefault,CreateDate,CreatedBy)    
     VALUES (@userIpAddress,@description,@isDefault,getDate(),@EmpAutoId)           
  
     END TRY     
     BEGIN CATCH      
    SET @isException=1    
    SET @exceptionMessage='Oops! Something went wrong.Please try later'    
     END CATCH    
  END    
  ELSE IF @Opcode=21    
   BEGIN     
    IF exists(SELECT AutoId FROM Tbl_ipaddress WHERE IpAddress = @userIpAddress and AutoId != @AutoId)    
    BEGIN    
   SET @isException=1    
   SET @exceptionMessage='IP already exists.'    
    END    
    ELSE       
    BEGIN    
   UPDATE Tbl_ipaddress SET IpAddress = @userIpAddress,description = @description, isDefault = @isDefault, UpdateDate = getDate(),UpdatedBy=@EmpAutoId WHERE AutoId = @AutoId    
    END    
	end   
  ELSE IF @Opcode=31    
    BEGIN     
    IF exists(SELECT AutoId FROM EmployeeIpAddress WHERE IPAutoId = @AutoId)    
    BEGIN    
   SET @isException=1    
   SET @exceptionMessage='Assigned'    
    END    
    ELSE       
    BEGIN    
	DELETE FROM  Tbl_ipaddress WHERE AutoId=@AutoId  
    END 
   END     
  ELSE IF @OpCode=41    
    BEGIN    
   select ROW_NUMBER() over(order by AutoId Desc) as RowNumber,    
   * into #Result from(
   SELECT AutoId,IpAddress,description,isDefault,Format(CreateDate,'MM/dd/yyyy') as CreateDate FROM Tbl_ipaddress where (ISNULL(@userIpAddress,'')='' or IpAddress=@userIpAddress)
   ) as t  ORDER BY AutoId  desc 

   SELECT * FROM #Result      
   WHERE @PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)       
   SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize=0 then COUNT(RowNumber) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Result 

   END    
  ELSE IF @OpCode=42    
     BEGIN    
         SELECT AutoId,IpAddress,description,isDefault FROM Tbl_ipaddress WHERE AutoId=@AutoId;   
     END     
 END TRY    
 BEGIN CATCH        
    SET @isException=1    
    SET @exceptionMessage='Oops! Something went wrong.Please try later.'    
 END CATCH    
END    
    
    
    
GO
