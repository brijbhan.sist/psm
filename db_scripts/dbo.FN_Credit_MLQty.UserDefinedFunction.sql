USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Credit_MLQty]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 

 
--alter table CreditMemoMaster drop column MLQty
--drop function FN_Credit_MLQty
--alter table CreditMemoMaster add  MLQty AS [dbo].[FN_Credit_MLQty](CreditAutoId)

CREATE FUNCTION  [dbo].[FN_Credit_MLQty]
(
	 @AutoId int
)
RETURNS decimal(18,2)
AS
BEGIN
	DECLARE @MLQty decimal(18,2)
	SET @MLQty=isnull((SELECT SUM(TotalMLTax) FROM CreditItemMaster WHERE CreditAutoId=@AutoId),0)
	RETURN @MLQty
END


GO
