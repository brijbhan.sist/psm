-----------------------Add new column for PO Log
alter table [dbo].[PODraftLog] add ActionTaken int
alter table [dbo].[PoRemarkLogs] add ActionTaken int



-----------Action table for PO Log -----------------------------------------------------

select * into #temp1 from [dbo].[tbl_ActionMaster] where AutoId between 32 and 41

insert into [dbo].[tbl_ActionMaster] (Action,ActionDesc) select temp.Action,temp.ActionDesc from #temp1 as temp



------------------------------Add column in Vender Bank Details and Customer Bank Details-----------------------------

alter table [dbo].[VendorBankDetails] add BankRemark varchar(max)
alter table [dbo].[VendorBankDetails] add CardRemark varchar(max)

alter table [dbo].[CustomerBankDetails] add [BankRemark] varchar(max)
alter table [dbo].[CustomerBankDetails] add CardRemark varchar(max)

----------------------------------------------24/07/2020--------------------------------------------------------------

alter table [dbo].[Driver_Log] add [DriverPlanningName] varchar(20)

----------------------------------------------27/07/2020--------------------------------------------------------------

alter table [dbo].[CompanyDetails] add [MigrateEmail] varchar(500)

----------------------------------------------31/07/2020--------------------------------------------------------------

alter table [dbo].[MessageMaster] add [IsRead] int

----------------------------------------------06/08/2020--------------------------------------------------------------

alter table [dbo].[Driver_Log] add [PlanningId] int

----------------------------------------------07/08/2020--------------------------------------------------------------

alter table [dbo].[DriverLog_OrdDetails] add [DriverAutoId] int

alter table [dbo].[DriverLog_OrdDetails] add [PlanningId] int