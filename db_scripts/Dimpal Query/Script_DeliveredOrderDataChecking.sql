-------------------------------------------------------DeliveredOrders-------------------------------------------------------------------------------
declare @OrderAutoId_Old int=32866
declare @OrderAutoId_easy int=4125
--SELECT * FROM [psmnpa_old.a1whm.com].[dbo].[tbl_OrderLog] where OrderAutoId=@OrderAutoId_Old
--select * from [psmnpa.easywhm.com].[dbo].[tbl_OrderLog] where OrderAutoId=@OrderAutoId_easy

--SELECT om.OrderNo,do.* FROM [psmnpa_old.a1whm.com].[dbo].[DeliveredOrders] as do
--inner join [psmnpa_old.a1whm.com].[dbo].[OrderMaster] as om on om.AutoId=do.OrderAutoId
--where OrderAutoId=@OrderAutoId_Old 

--SELECT om.OrderNo,do.* FROM [psmnpa.easywhm.com].[dbo].[DeliveredOrders] as do
--inner join [psmnpa.easywhm.com].[dbo].[OrderMaster] as om on om.AutoId=do.OrderAutoId
--where OrderAutoId=@OrderAutoId_easy 


SELECT om.OrderNo,do.* into #tm FROM [psmnpa_old.a1whm.com].[dbo].[DeliveredOrders] as do
inner join [psmnpa_old.a1whm.com].[dbo].[OrderMaster] as om on om.AutoId=do.OrderAutoId
where OrderAutoId=@OrderAutoId_Old 

SELECT om.orderNo,tm.OrderNo,do.* FROM [psmnpa.easywhm.com].[dbo].[DeliveredOrders] as do
inner join [psmnpa.easywhm.com].dbo.OrderMaster as om on om.AutoId=do.OrderAutoId
inner join #tm as tm on tm.OrderNo=om.OrderNo
where do.OrderAutoId=@OrderAutoId_easy 

--drop table #tm

and isnull(do.[PaymentAutoId],0)=isnull(tm.[PaymentAutoId],0)
and isnull(do.[PayId]			  ,0)=isnull(tm.[PayId]			  ,0)
and isnull(do.[AdjustmentAmt]	  ,0)=isnull(tm.[AdjustmentAmt]	  ,0)
and isnull(do.[GrandTotal]	  ,0)=    isnull(tm.[GrandTotal]	  ,0)
and isnull(do.[PayableAmount]	  ,0)=isnull(tm.[PayableAmount]	  ,0)
and isnull(do.[AmtDue]			  ,0)=isnull(tm.[AmtDue]			  ,0)
and isnull(do.[TotalAmount]	  ,0)=    isnull(tm.[TotalAmount]	  ,0)
and isnull(do.[OverallDiscAmt]	  ,0)=isnull(tm.[OverallDiscAmt]	  ,0)
and isnull(do.[ShippingCharges]  ,0)=isnull(tm.[ShippingCharges]  ,0)
and isnull(do.[TotalTax]		  ,0)=isnull(tm.[TotalTax]		  ,0)
and isnull(do.[CreditMemoAmount] ,0)=isnull(tm.[CreditMemoAmount] ,0)
and isnull(do.[CreditAmount]	  ,0)=isnull(tm.[CreditAmount]	  ,0)
and isnull(do.[OverallDisc]	  ,0)=isnull(tm.[OverallDisc]	  ,0)
and isnull(do.[MLTax]			  ,0)=isnull(tm.[MLTax]			  ,0)
and isnull(do.[OldPaidAmount]	  ,0)=isnull(tm.[OldPaidAmount]	  ,0)
and isnull(do.[AdjestOrderAmount],0)=isnull(tm.[AdjestOrderAmount],0)
and isnull(do.[AmtPaid]		  ,0)=isnull(tm.[AmtPaid]		  ,0)
and isnull(do.[dueOrderStatus]   ,0)=isnull(tm.[dueOrderStatus]   ,0)

-----------------------------------------------------------------------------------------------------------------------------------

declare @OrderAutoId_Old1 int=38396
declare @OrderAutoId_easy1 int=6283

--SELECT om.OrderNo,do.* FROM [psmnpa_old.a1whm.com].[dbo].[Delivered_Order_Items] as do
--inner join [psmnpa_old.a1whm.com].[dbo].[OrderMaster] as om on om.AutoId=do.OrderAutoId
--where OrderAutoId=@OrderAutoId_Old1 

--SELECT om.OrderNo,do.* FROM [psmnpa.easywhm.com].[dbo].[Delivered_Order_Items] as do
--inner join [psmnpa.easywhm.com].[dbo].[OrderMaster] as om on om.AutoId=do.OrderAutoId
--where OrderAutoId=@OrderAutoId_easy1 


SELECT om.OrderNo,pm.ProductId,do.* into #tm1 FROM [psmnpa_old.a1whm.com].[dbo].Delivered_Order_Items as do
inner join [psmnpa_old.a1whm.com].[dbo].[OrderMaster] as om on om.AutoId=do.OrderAutoId
inner join [psmnpa_old.a1whm.com].[dbo].ProductMaster as pm on pm.AutoId=do.ProductAutoId
where OrderAutoId=@OrderAutoId_Old1 

--select * from #tm1

SELECT pm.ProductId,tm.ProductId,do.* FROM [psmnpa.easywhm.com].[dbo].Delivered_Order_Items as do
inner join [psmnpa.easywhm.com].dbo.OrderMaster as om on om.AutoId=do.OrderAutoId
inner join #tm1 as tm on tm.OrderNo=om.OrderNo
inner join [psmnpa_old.a1whm.com].[dbo].ProductMaster as pm on pm.ProductId=tm.ProductId
where do.OrderAutoId=@OrderAutoId_easy1 and pm.ProductId=tm.ProductId and do.UnitAutoId=tm.UnitAutoId

drop table #tm1

------------------------------------------------------- Drv Log ----------------------------------------------------------------------------

declare @OrderAutoId_Old2 int=38396
declare @OrderAutoId_easy2 int=6283
SELECT [AutoId],[DrvAutoId],[OrderAutoId] ,[AssignDate] FROM [psmnpa_old.a1whm.com].[dbo].[DrvLog] where OrderAutoId=@OrderAutoId_Old2

SELECT [AutoId],[DrvAutoId],[OrderAutoId] ,[AssignDate] FROM [psmnpa.easywhm.com].[dbo].[DrvLog] where OrderAutoId=@OrderAutoId_easy2


------------------------------------------------------ GenPacking --------------------------------------------------------------------------------

declare @OrderAutoId_Old3 int=38396
declare @OrderAutoId_easy3 int=6283

SELECT [AutoId],[PackingId],[OrderAutoId],[PackingDate],[PackerAutoId] FROM [psmnpa_old.a1whm.com].[dbo].[GenPacking] where OrderAutoId=@OrderAutoId_Old3

SELECT [AutoId],[PackingId],[OrderAutoId],[PackingDate],[PackerAutoId] FROM [psmnpa.easywhm.com].[dbo].[GenPacking] where OrderAutoId=@OrderAutoId_easy3

----------------------------------------------------AllocatedPackingDetails----------------------------------------------------------------

SELECT [AutoId] ,[OrderAutId] ,[ProductAutoId] ,[AllocatedQty] ,[PackingType],[AllocateDate]  
FROM [psmnpa_old.a1whm.com].[dbo].[AllocatedPackingDetails] where OrderAutId=@OrderAutoId_Old3

SELECT [AutoId] ,[OrderAutId] ,[ProductAutoId] ,[AllocatedQty] ,[PackingType],[AllocateDate]  
FROM [psmnpa.easywhm.com].[dbo].[AllocatedPackingDetails] where OrderAutId=@OrderAutoId_easy3

---------------------------------------------------------DamageStockMaster--------------------------------------------------------------------


SELECT [AutoId],[OrderId],[Status],[Type],[CreatedBy],[CreatedDate] FROM [psmnpa_old.a1whm.com].[dbo].[DamageStockMaster] where OrderId='ORD323326'

SELECT [AutoId],[OrderId],[Status],[Type],[CreatedBy],[CreatedDate] FROM [psmnpa.easywhm.com].[dbo].[DamageStockMaster] where OrderId='ORD323326'

---------------------------------------------------------------------------------------------------------------------------------------------

select * from [psmnpa_old.a1whm.com].dbo.CustomerPaymentDetails where PaymentAutoId=27611

select * from [psmnpa.easywhm.com].dbo.CustomerPaymentDetails where PaymentAutoId=5573



----------------------------------------------------------------------------------------------------------------------------------------------
declare @OrderAutoId_Old4 int=38396
declare @OrderAutoId_easy4 int=6283

SELECT  [ItemAutoId],[PaymentAutoId] ,[OrderAutoId],[ReceivedAmount] FROM [psmnpa_old.a1whm.com].[dbo].[PaymentOrderDetails] where OrderAutoId=@OrderAutoId_Old4

SELECT  [ItemAutoId],[PaymentAutoId] ,[OrderAutoId],[ReceivedAmount] FROM [psmnpa.easywhm.com].[dbo].[PaymentOrderDetails] where OrderAutoId=@OrderAutoId_easy4

---------------------------------------------------------------------------------------------------------------------------------------------------

SELECT [AutoId],[CurrencyAutoId] ,[PaymentAutoId],[NoofValue] ,[OldTotalAmount] ,[P_CurrencyValue],[TotalAmount]
FROM [psmnpa_old.a1whm.com].[dbo].[PaymentCurrencyDetails] where PaymentAutoId=27611

SELECT [AutoId],[CurrencyAutoId] ,[PaymentAutoId],[NoofValue] ,[OldTotalAmount] ,[P_CurrencyValue],[TotalAmount]
FROM [psmnpa.easywhm.com].[dbo].[PaymentCurrencyDetails] where PaymentAutoId=5573

----------------------------------------------------------------------------------------------------------------------------------------------------

