declare @OrderAutoId_Old int=38396
declare @OrderAutoId_easy int=6283

SELECT [CreditAutoId],[CreditNo],[CustomerAutoId],[CreatedBy],[CreditDate],[Remarks],[ApprovedBy],[CompletedBy],[ApprovalDate],[CompletionDate],[Status],
[PaidAmount],[OrderAutoId]
,[SalesAmount],[CreditType],[OverallDiscAmt],[ApplyMLQty] ,[MLTaxPer],[IsMLTaxApply] ,[CompletionRemarks] ,[referenceCreditMemoNumber] ,[TaxTypeAutoId],[TaxValue]
,[ManagerRemark] ,[WeightTax] ,[UpdatedBy] ,[UpdatedDate],[CancelRemark],[SalesPersonAutoId] ,[MLTaxRemark],[GrandTotal] ,[MLQty],[BalanceAmount]
,[OverallDisc],[WeightTotalQuantity],[TotalAmount] ,[NetAmount] ,[CreditMemoType],[ReferenceOrderAutoId] ,[MLTax] ,[WeightTaxAmount] ,[AdjustmentAmt],
[TotalTax_Old],[TotalTax] FROM [psmnpa_old.a1whm.com].[dbo].[CreditMemoMaster] where OrderAutoId=@OrderAutoId_Old

SELECT [CreditAutoId],[CreditNo],[CustomerAutoId],[CreatedBy],[CreditDate],[Remarks],[ApprovedBy],[CompletedBy],[ApprovalDate],[CompletionDate],[Status],
[PaidAmount],[OrderAutoId]
,[SalesAmount],[CreditType],[OverallDiscAmt],[ApplyMLQty] ,[MLTaxPer],[IsMLTaxApply] ,[CompletionRemarks] ,[referenceCreditMemoNumber] ,[TaxTypeAutoId],[TaxValue]
,[ManagerRemark] ,[WeightTax] ,[UpdatedBy] ,[UpdatedDate],[CancelRemark],[SalesPersonAutoId] ,[MLTaxRemark],[GrandTotal] ,[MLQty],[BalanceAmount]
,[OverallDisc],[WeightTotalQuantity],[TotalAmount] ,[NetAmount] ,[CreditMemoType],[ReferenceOrderAutoId] ,[MLTax] ,[WeightTaxAmount] ,[AdjustmentAmt],
[TotalTax_Old],[TotalTax] FROM [psmnpa.easywhm.com].[dbo].[CreditMemoMaster] where OrderAutoId=@OrderAutoId_easy

select cm.CreditMemoType,cmm.CreditMemoType,* from [psmnpa_old.a1whm.com].dbo.CreditMemoMaster as cm 
inner join [psmnpa.easywhm.com].dbo.CreditMemoMaster as cmm on cm.CreditNo=cmm.CreditNo
where cmm.CreditNo='CRM00865'
and isnull(cm.[CustomerAutoId],0)=             isnull(cmm.[CustomerAutoId],0)
and isnull(cm.[CreatedBy]                ,0)  =isnull(cmm.[CreatedBy]                ,0)
and isnull(cm.[CreditDate]				 ,'') = isnull(cmm.[CreditDate]				 ,'')
and isnull(cm.[Remarks]					 ,0) = isnull(cmm.[Remarks]					 ,0)
and isnull(cm.[ApprovedBy]				 ,0) = isnull(cmm.[ApprovedBy]				 ,0)
and isnull(cm.[CompletedBy]				 ,0) = isnull(cmm.[CompletedBy]				 ,0)
and isnull(cm.[ApprovalDate]			 ,'')  =isnull(cmm.[ApprovalDate]			 ,'')
and isnull(cm.[CompletionDate]			 ,'') = isnull(cmm.[CompletionDate]			 ,'')
and isnull(cm.[Status]					 ,0) = isnull(cmm.[Status]					 ,0)
and isnull(cm.[PaidAmount]				 ,0) = isnull(cmm.[PaidAmount]				 ,0)
--and isnull(cm.[OrderAutoId]				 ,0) = isnull(cmm.[OrderAutoId]				 ,0)
and isnull(cm.[SalesAmount]				 ,0) = isnull(cmm.[SalesAmount]				 ,0)
and isnull(cm.[CreditType]				 ,0) = isnull(cmm.[CreditType]				 ,0)
and isnull(cm.[OverallDiscAmt]			 ,0) = isnull(cmm.[OverallDiscAmt]			 ,0)
and isnull(cm.[ApplyMLQty]				 ,0) = isnull(cmm.[ApplyMLQty]				 ,0)
and isnull(cm.[MLTaxPer]				 ,0) = isnull(cmm.[MLTaxPer]				 ,0)
and isnull(cm.[IsMLTaxApply]			 ,0) = isnull(cmm.[IsMLTaxApply]			 ,0)
and isnull(cm.[CompletionRemarks]		 ,0) = isnull(cmm.[CompletionRemarks]		 ,0)
and isnull(cm.[referenceCreditMemoNumber],0) = isnull(cmm.[referenceCreditMemoNumber],0)
and isnull(cm.[TaxTypeAutoId]			 ,0) = isnull(cmm.[TaxTypeAutoId]			 ,0)
and isnull(cm.[TaxValue]				 ,0) = isnull(cmm.[TaxValue]				 ,0)
and isnull(cm.[ManagerRemark]			 ,0) = isnull(cmm.[ManagerRemark]			 ,0)
and isnull(cm.[WeightTax]				 ,0) = isnull(cmm.[WeightTax]				 ,0)
and isnull(cm.[UpdatedBy]				 ,0) = isnull(cmm.[UpdatedBy]				 ,0)
and isnull(cm.[UpdatedDate]				 ,'') = isnull(cmm.[UpdatedDate]				 ,'')
and isnull(cm.[CancelRemark]			 ,0) = isnull(cmm.[CancelRemark]			 ,0)
and isnull(cm.[SalesPersonAutoId]		 ,0) = isnull(cmm.[SalesPersonAutoId]		 ,0)
and isnull(cm.[MLTaxRemark]				 ,0) = isnull(cmm.[MLTaxRemark]				 ,0)
and isnull(cm.[GrandTotal]				 ,0) = isnull(cmm.[GrandTotal]				 ,0)
and isnull(cm.[MLQty]					 ,0) = isnull(cmm.[MLQty]					 ,0)
and isnull(cm.[BalanceAmount]			 ,0) = isnull(cmm.[BalanceAmount]			 ,0)
and isnull(cm.[OverallDisc]				 ,0) = isnull(cmm.[OverallDisc]				 ,0)
and isnull(cm.[WeightTotalQuantity]		 ,0) = isnull(cmm.[WeightTotalQuantity]		 ,0)
and isnull(cm.[TotalAmount]				 ,0) = isnull(cmm.[TotalAmount]				 ,0)
and isnull(cm.[NetAmount]				 ,0) = isnull(cmm.[NetAmount]				 ,0)
and isnull(cm.[CreditMemoType]			 ,0) = isnull(cmm.[CreditMemoType]			 ,0)
--and isnull(cm.[ReferenceOrderAutoId]	 ,0) = isnull(cmm.[ReferenceOrderAutoId]	 ,0)
and isnull(cm.[MLTax]					 ,0) = isnull(cmm.[MLTax]					 ,0)
and isnull(cm.[WeightTaxAmount]			 ,0) = isnull(cmm.[WeightTaxAmount]			 ,0)
and isnull(cm.[AdjustmentAmt]			 ,0) = isnull(cmm.[AdjustmentAmt]			 ,0)
and isnull(cm.[TotalTax_Old]			 ,0) = isnull(cmm.[TotalTax_Old]			 ,0)
and isnull(cm.[TotalTax]				 ,0) =isnull(cmm.[TotalTax]				 ,0)


--------------------------------------------------------  CreditItemMaster  -------------------------------------------------
declare @CreditAutoId_Old int=1481
declare @CreditAutoId_easy int=78

SELECT [ItemAutoId],[CreditAutoId],[ProductAutoId],[UnitAutoId]
,[UnitPrice],[SRP],[TaxRate],[RequiredQty],[QtyPerUnit],[AcceptedQty] ,[ManagerUnitPrice],[UnitMLQty],[Weight_OZ],[CostPrice] ,[QtyPerUnit_Fresh]
,[QtyPerUnit_Damage],[QtyPerUnit_Missing],[QtyReturn_Fresh],[QtyReturn_Damage] ,[QtyReturn_Missing],[OM_MinPrice],[OM_CostPrice],[OM_BasePrice]
,[NetAmount] ,[AcceptedTotalQty] ,[TotalMLTax] ,[Item_TotalMLTax],[WeightQuantity] ,[TotalPeice] FROM [psmnpa_old.a1whm.com].[dbo].[CreditItemMaster] where CreditAutoId=@CreditAutoId_Old

SELECT [ItemAutoId],[CreditAutoId],[ProductAutoId],[UnitAutoId]
,[UnitPrice],[SRP],[TaxRate],[RequiredQty],[QtyPerUnit],[AcceptedQty] ,[ManagerUnitPrice],[UnitMLQty],[Weight_OZ],[CostPrice] ,[QtyPerUnit_Fresh]
,[QtyPerUnit_Damage],[QtyPerUnit_Missing],[QtyReturn_Fresh],[QtyReturn_Damage] ,[QtyReturn_Missing],[OM_MinPrice],[OM_CostPrice],[OM_BasePrice]
,[NetAmount] ,[AcceptedTotalQty] ,[TotalMLTax] ,[Item_TotalMLTax],[WeightQuantity] ,[TotalPeice] FROM [psmnpa.easywhm.com].[dbo].[CreditItemMaster] 
where CreditAutoId=@CreditAutoId_easy