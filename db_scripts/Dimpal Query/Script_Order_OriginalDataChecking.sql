declare @OrderNo varchar(50)='ORD323326'
--SELECT [AutoId],[OrderNo],[OrderDate]  ,[DeliveryDate],[CustomerAutoId] ,[Terms],[BillAddrAutoId],[ShipAddrAutoId],[SalesPersonAutoId]
--,[TotalAmount],[OverallDisc],[OverallDiscAmt],[ShippingCharges] ,[TotalTax],[GrandTotal],[ShippingType],[TaxType],[MLQty],[MLTax]
--FROM [psmct.a1whm.com].[dbo].[Order_Original] where OrderNo=@OrderNo

--SELECT [AutoId],[OrderNo],[OrderDate]  ,[DeliveryDate],[CustomerAutoId] ,[Terms],[BillAddrAutoId],[ShipAddrAutoId],[SalesPersonAutoId]
--,[TotalAmount],[OverallDisc],[OverallDiscAmt],[ShippingCharges] ,[TotalTax],[GrandTotal],[ShippingType],[TaxType],[MLQty],[MLTax]
--FROM [psmct.easywhm.com].[dbo].[Order_Original] where OrderNo=@OrderNo

--SELECT [AutoId],[OrderNo],[OrderDate]  ,[DeliveryDate],[CustomerAutoId] ,[Terms],[BillAddrAutoId],[ShipAddrAutoId],[SalesPersonAutoId]
--,[TotalAmount],[OverallDisc],[OverallDiscAmt],[ShippingCharges] ,[TotalTax],[GrandTotal],[ShippingType],[TaxType],[MLQty],[MLTax]
--FROM [psmct_old.a1whm.com].[dbo].[Order_Original] where OrderNo=@OrderNo

select * from [psmnpa_old.a1whm.com].[dbo].[Order_Original] as oo
inner join [psmnpa.easywhm.com].[dbo].[Order_Original] as ot on ot.OrderNo=oo.OrderNo
where ot.OrderNo=@OrderNo
and isnull(oo.[OrderDate],'')= isnull(ot.OrderDate,'') 
and isnull(oo.[DeliveryDate],'')    	    =isnull(ot.[DeliveryDate],'')
and isnull(oo.[CustomerAutoId],0) 		=isnull(ot.[CustomerAutoId],0) 
and isnull(oo.[Terms],0) 				=isnull(ot.[Terms],0) 
and isnull(oo.[BillAddrAutoId],0)	=isnull(ot.[BillAddrAutoId] ,0)
and isnull(oo.[ShipAddrAutoId] 	  ,0)	=isnull(ot.[ShipAddrAutoId] 	  ,0)
and isnull(oo.[SalesPersonAutoId] ,0)	=isnull(ot.[SalesPersonAutoId] ,0)
and isnull(oo.[TotalAmount] 	  ,0)	=isnull(ot.[TotalAmount] 	  ,0)
and isnull(oo.[OverallDisc] 	  ,0)	=isnull(ot.[OverallDisc] 	  ,0)
and isnull(oo.[OverallDiscAmt] 	  ,0)	=isnull(ot.[OverallDiscAmt] 	  ,0)
and isnull(oo.[ShippingCharges]   ,0)	=isnull(ot.[ShippingCharges]   ,0)
and isnull(oo.[TotalTax] 		  ,0)	=isnull(ot.[TotalTax] 		  ,0)
and isnull(oo.[GrandTotal] 		  ,0)	=isnull(ot.[GrandTotal] 		  ,0)
and isnull(oo.[ShippingType] 	  ,0)	=isnull(ot.[ShippingType] 	  ,0)
and isnull(oo.[TaxType] 		  ,0)	=isnull(ot.[TaxType] 		  ,0)
and isnull(oo.[MLQty]			  ,0)	=isnull(ot.[MLQty]			  ,0)
and isnull(oo.[MLTax]			  ,0)	=isnull(ot.[MLTax]			  ,0)

-----------------------------------------------------Order_OriginalItem------------------------------------------------------------------------------------------

select * from [psmnpa.easywhm.com].dbo.OrderItems_Original as oim
inner join [psmnpa.easywhm.com].dbo.OrderItemMaster as om 
on om.OrderAutoId=oim.OrderAutoId --and om.OrderAutoId=4125
where Om.OrderAutoId=6283

select * from [psmnpa.easywhm.com].dbo.OrderItems_Original where OrderAutoId=6283
select * from [psmnpa.easywhm.com].dbo.OrderItemMaster where OrderAutoId=6283

select * from [psmnpa_old.a1whm.com].dbo.OrderItems_Original where OrderAutoId=38396