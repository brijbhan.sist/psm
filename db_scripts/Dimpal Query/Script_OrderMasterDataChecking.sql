--select * from [psmct_old.a1whm.com].[dbo].[OrderMaster] where AutoId=32866
declare @OrderNo varchar(100)='ORD323326'
select * from [psmnpa_old.a1whm.com].[dbo].[OrderMaster] where OrderNo=@OrderNo

select * from [psmnpa.easywhm.com].[dbo].[OrderMaster] where OrderNo=@OrderNo

select om.* from [dbo].[OrderMaster] as om
inner join [dbo].[OrderMaster]  as om1
on om1.OrderNo=om.OrderNo
where om.OrderNo=@OrderNo and om.OrderDate=om1.OrderDate and om.DeliveryDate=om1.DeliveryDate and om.CustomerAutoId=om1.CustomerAutoId
and om.Terms=om1.Terms and om.BillAddrAutoId=om1.BillAddrAutoId and om.ShipAddrAutoId=om1.ShipAddrAutoId and om.SalesPersonAutoId=om1.SalesPersonAutoId
and om.OverallDiscAmt=om1.OverallDiscAmt and om.[ShippingCharges]=om1.[ShippingCharges]
and om.[Status]=om1.[Status]
and isnull(om.[PackerAutoId],0)=isnull(om1.[PackerAutoId],0)
and isnull(om.[Driver],0)=isnull(om1.[Driver],0)
and isnull(om.[AssignDate],0)=isnull(om1.[AssignDate],0)
and isnull(om.[Stoppage],0)= isnull(om1.[Stoppage],0)
and isnull(om.[DelDate],0)= isnull(om1.[DelDate],0)
and om.[PaymentRecev]= om1.[PaymentRecev]
and isnull(om.[RecevAmt],0)= isnull(om1.[RecevAmt],0)
and isnull(om.[AmtValue],0)=isnull(om1.[AmtValue],0)
and isnull(om.[ValueChanged],0)=isnull(om1.[ValueChanged],0)
and isnull(om.[DiffAmt],0)=isnull(om1.[DiffAmt],0)
and isnull(om.[NewTotal],0)=isnull(om1.[NewTotal],0)
and isnull(om.[PayThru],0)=isnull(om1.[PayThru],0)
and isnull(om.[DrvRemarks],0)=isnull(om1.[DrvRemarks],0)
and isnull(om.[AmtDue],0)=isnull(om1.[AmtDue],0)
and isnull(om.[PackedBoxes],0)=isnull(om1.[PackedBoxes],0)
and isnull(om.[AcctRemarks],0)=isnull(om1.[AcctRemarks],0)
and isnull(om.[ShippingType],0)=isnull(om1.[ShippingType],0)
and isnull(om.[CommentType],0)=isnull(om1.[CommentType],0)
and isnull(om.[Comment],0)=isnull(om1.[Comment],0)
and isnull(om.[CheckNo],0)=isnull(om1.[CheckNo],0)
and isnull(om.[AddonPackedQty],0)=isnull(om1.[AddonPackedQty],0)
and isnull(om.[TaxType],0)=isnull(om1.[TaxType],0)
and isnull(om.[ManagerAutoId],0)=isnull(om1.[ManagerAutoId],0)
and isnull(om.[AccountAutoId],0)=isnull(om1.[AccountAutoId],0)
and isnull(om.[PackerRemarks],0)=isnull(om1.[PackerRemarks],0)
and isnull(om.[OrderRemarks],0)=isnull(om1.[OrderRemarks],0)
and isnull(om.[RootName],0)=isnull(om1.[RootName],0)
and isnull(om.[OrderType],0)=isnull(om1.[OrderType],0)
and isnull(om.[paidAmount],0)=isnull(om1.[paidAmount],0)
and isnull(om.[paymentMode],0)=isnull(om1.[paymentMode],0)
and isnull(om.[referNo],0)=isnull(om1.[referNo],0)
and isnull(om.[POSAutoId],0)=isnull(om1.[POSAutoId],0)
and isnull(om.[TaxValue],0)=isnull(om1.[TaxValue],0)
and isnull(om.[PastDue],0)=isnull(om1.[PastDue],0)
and isnull(om.[referenceOrderNumber],0)=isnull(om1.[referenceOrderNumber],0)
and isnull(om.[ManagerRemarks],0)=isnull(om1.[ManagerRemarks],0)
and isnull(om.[PackerAssignDate],0)=isnull(om1.[PackerAssignDate],0)
and isnull(om.[PackerAssignStatus],0)=isnull(om1.[PackerAssignStatus],0)
and isnull(om.[Times],0)=isnull(om1.[Times],0)
and isnull(om.[UPDATEDATE],0)=isnull(om1.[UPDATEDATE],0)
and isnull(om.[UPDATEDBY],0)=isnull(om1.[UPDATEDBY],0)
and isnull(om.[WarehouseAutoId],0)=isnull(om1.[WarehouseAutoId],0)
and isnull(om.[WarehouseRemarks],0)=isnull(om1.[WarehouseRemarks],0)
and isnull(om.[UnitMLTax],0)=isnull(om1.[UnitMLTax],0)
and isnull(om.[MLTaxPer],0)=isnull(om1.[MLTaxPer],0)
and isnull(om.[CancelledDate],0)		=	  isnull(om1.[CancelledDate],0)
and isnull(om.[CancelledBy],0)		=	  isnull(om1.[CancelledBy],0)
and isnull(om.[DriverRemarks],0)		=	  isnull(om1.[DriverRemarks],0)
and isnull(om.[isMLManualyApply],0)	=	  isnull(om1.[isMLManualyApply],0)
and isnull(om.[IsTaxApply],0)			=	  isnull(om1.[IsTaxApply],0)
and isnull(om.[RouteAutoId],0)		=	  isnull(om1.[RouteAutoId],0)
and isnull(om.[RouteStatus],0)		=	  isnull(om1.[RouteStatus],0)
and isnull(om.[DeviceID],0)			=	  isnull(om1.[DeviceID],0)
and isnull(om.[appVersion],0)			=	  isnull(om1.[appVersion],0)
and isnull(om.[latLong],0)			=	  isnull(om1.[latLong],0)
and isnull(om.[AppOrderdate],0)		=	  isnull(om1.[AppOrderdate],0)
and isnull(om.[CancelRemark],0)		=	  isnull(om1.[CancelRemark],0)
and isnull(om.[Weigth_OZTax],0)		=	  isnull(om1.[Weigth_OZTax],0)
and isnull(om.[Order_Closed_Date],0)	=	  isnull(om1.[Order_Closed_Date],0)
and isnull(om.[TotalNOI],0)			=	  isnull(om1.[TotalNOI],0)
and isnull(om.[MLTaxRemark],0)		=	  isnull(om1.[MLTaxRemark],0)
and isnull(om.[OrdShippingAddress] ,0) = isnull(om1.[OrdShippingAddress] ,0)
and isnull(om.[DraftOrderDate]	 ,0)= isnull(om1.[DraftOrderDate]	 ,0)
and isnull(om.[SA_Lat]			 ,0)= isnull(om1.[SA_Lat]			 ,0)
and isnull(om.[SA_Long]			 ,0)= isnull(om1.[SA_Long]			 ,0)
and isnull(om.[DriverCarId]		 ,0)= isnull(om1.[DriverCarId]		 ,0)
and isnull(om.[ShippingTaxEnabled] ,0)= isnull(om1.[ShippingTaxEnabled] ,0)
and isnull(om.[IsMLTaxApply]		 ,0)= isnull(om1.[IsMLTaxApply]		 ,0)
and isnull(om.[MigrateRemarks]	 ,0)= isnull(om1.[MigrateRemarks]	 ,0)
and isnull(om.[SA_City]			 ,0)= isnull(om1.[SA_City]			 ,0)
and isnull(om.[SA_State]			 ,0)= isnull(om1.[SA_State]			 ,0)
and isnull(om.[SA_Zipcode]		 ,0)= isnull(om1.[SA_Zipcode]		 ,0)
and isnull(om.[SA_Address]		 ,0)= isnull(om1.[SA_Address]		 ,0)
and isnull(om.[DeliveryFromTime]	 ,'')= isnull(om1.[DeliveryFromTime]	 ,'')
and isnull(om.[DeliveryToTime]	 ,'')= isnull(om1.[DeliveryToTime]	 ,'')
and isnull(om.[ScheduleAt]		 ,0)= isnull(om1.[ScheduleAt]		 ,0)
and isnull(om.[DeliveredAddress]	 ,0)= isnull(om1.[DeliveredAddress]	 ,0)
and isnull(om.[SA_Address2]		 ,0)= isnull(om1.[SA_Address2]		 ,0)
and isnull(om.[CustDefaultFromTime],'')= isnull(om1.[CustDefaultFromTime],'')
and isnull(om.[CustDefaultToTime],'')= isnull(om1.[CustDefaultToTime],'')
and isnull(om.[MLQty]				 ,0)= isnull(om1.[MLQty]				 ,0)
and isnull(om.[OverallDisc]		 ,0)= isnull(om1.[OverallDisc]		 ,0)
and isnull(om.[Deductionamount]	 ,0)= isnull(om1.[Deductionamount]	 ,0)
and isnull(om.[CreditAmount]		 ,0)= isnull(om1.[CreditAmount]		 ,0)
and isnull(om.[TotalTax]			 ,0)= isnull(om1.[TotalTax]			 ,0)
and isnull(om.[Weigth_OZQty]		 ,0)= isnull(om1.[Weigth_OZQty]		 ,0)
and isnull(om.[GrandTotal]		 ,0)= isnull(om1.[GrandTotal]		 ,0)
and isnull(om.[PayableAmount]		 ,0)= isnull(om1.[PayableAmount]		 ,0)
and isnull(om.[AdjustmentAmt]		 ,0)= isnull(om1.[AdjustmentAmt]		 ,0)
and isnull(om.[MLTax]				 ,0)= isnull(om1.[MLTax]				 ,0)
and isnull(om.[TotalCostPrice]	 ,0)= isnull(om1.[TotalCostPrice]	 ,0)
and isnull(om.[Weigth_OZTaxAmount] ,0)= isnull(om1.[Weigth_OZTaxAmount] ,0)
and isnull(om.[staticpayableAmount],0)= isnull(om1.[staticpayableAmount],0)
and isnull(om.[TotalAmount]		 ,0)= isnull(om1.[TotalAmount]		 ,0)
and isnull(om.[OM_MigrationType]	 ,0)= isnull(om1.[OM_MigrationType]	 ,0)