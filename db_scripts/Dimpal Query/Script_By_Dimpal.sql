 USE [psmwpa.easywhm.com]
 go 

 alter table ordermaster add staticpayableAmount decimal(12,2),
OM_MigrationType int

alter table [dbo].[CustomerPaymentDetails] add MigrationType int

GO


---------------------------------------------------------------------------------------------------------------------

create or ALTER FUNCTION  [dbo].[FN_Order_IsProductTaxPerUnit]  
(  
	@AutoId int,  
	@orderAutoId int  
)  
RETURNS decimal(18,2)  
AS  
BEGIN  
	DECLARE @Total decimal(18,2)=0.00      
	IF (select tax from OrderItemMaster where AutoId=@AutoId)=1
	BEGIN  
		set @Total=(select isnull((oim.UnitPrice*om.TaxValue)/100,0) as Tax from OrderItemMaster as oim 
					inner join OrderMaster as om on om.AutoId=oim.OrderAutoId
					where oim.AutoId=@AutoId) 
		
	END  
	RETURN @Total  
END  
GO
------------------------------------------------------------------------------------------------------------------------


create or ALTER   FUNCTION [dbo].[FN_Order_OIMItemtotalAmount]
(
	 @AutoId int,
	 @orderAutoId int
)
RETURNS decimal(18,2)
AS
BEGIN
	DECLARE @NetAmount decimal(18,2)	
	--CAL TOTAL AMOUNT FOR NEW ,PROCESSED AND CANCELLED ORDER 
	IF EXISTS(SELECT AutoId FROM OrderMaster WHERE AutoId=@orderAutoId AND Status IN (0,1,2,8))
	BEGIN
		SET @NetAmount=ISNULL((SELECT (UnitPrice*	RequiredQty) FROM OrderItemMaster WHERE AutoId=@AutoId),0)	
	END
	ELSE 
	BEGIN	 
		SET @NetAmount=ISNULL((SELECT (UnitPrice*	QtyShip) FROM OrderItemMaster WHERE AutoId=@AutoId),0)	
	END

	RETURN @NetAmount
END
GO
-------------------------------------------------------------------------------------------------------------------------
create FUNCTION  [dbo].[FN_Order_IsProductTax]  
(  
	@AutoId int,  
	@orderAutoId int  
)  
RETURNS decimal(18,2)  
AS  
BEGIN  
	DECLARE @Total decimal(18,2)=0.00      
	IF (select tax from OrderItemMaster where AutoId=@AutoId)=1
	BEGIN  
		set @Total=ISNULL((select isnull((oim.NetPrice*om.TaxValue)/100,0) as Tax from OrderItemMaster as oim 
					inner join OrderMaster as om on om.AutoId=oim.OrderAutoId
					where oim.AutoId=@AutoId),0) 
		
	END  
	RETURN @Total  
END  
GO
---------------------------------------------------------------------------------------------------------------------

alter table [dbo].[OrderItemMaster] add
[AddedItemKey] [varchar](50),
[AddOnPackedPeice] [int],[Oim_Discount] [decimal](18, 2) ,
[Oim_ItemTotal]  AS ([dbo].[FN_Order_OIMItemtotalAmount]([autoid],[OrderAutoid])),
[IsProductTax]  AS ([dbo].[FN_Order_IsProductTax]([AutoId],[OrderAutoId])),
[IsProductTaxPerUnit]  AS ([dbo].[FN_Order_IsProductTaxPerUnit]([AutoId],[OrderAutoId])),
[Oim_DiscountAmount] [decimal](18, 2)

------------------------------------------------------------------------------------------------------------------------------
alter table [dbo].[tbl_OrderLog] add [ActionIPAddress] varchar(100)

alter table [dbo].[CreditMemoLog] add [ActionIPAddress] varchar(100) 

alter table [dbo].[PaymentCurrencyDetails] add [OldTotalAmount] [decimal](18, 2),[P_CurrencyValue] [decimal](18, 2)


GO
----------------------------------------------------------------------------------------------------------------------------------
CREATE or alter FUNCTION  [dbo].[FN_Delivered_ItemTotal]
(
	 @AutoId int 
)
RETURNS decimal(18,2)
AS
BEGIN
	DECLARE @NetAmount decimal(18,2)		 	 
	SET @NetAmount=cast((SELECT ((UnitPrice/QtyPerUnit) * QtyDel) FROM [Delivered_Order_Items] WHERE AutoId=@AutoId) as decimal	(18,2))	 
	RETURN @NetAmount
END

GO
--

ALTER TABLE [dbo].[Delivered_Order_Items] ADD  Del_ItemTotal  AS ([dbo].[FN_Delivered_ItemTotal]([AutoId]))
------------------------------------------------------------171------------------------------------------------------------
go
create or alter FUNCTION  [dbo].[FN_DelOrder_IsProductTax]  
(  
	@AutoId int,  
	@orderAutoId int  
)  
RETURNS decimal(18,2)  
AS  
BEGIN  
	DECLARE @Total decimal(18,2)=0.00,    
	 @Tax int=null  
	IF (select tax from Delivered_Order_Items where AutoId=@AutoId)=1
	BEGIN  
		set @Total=ISNULL((select isnull((oim.NetPrice*om.TaxValue)/100,0) as Tax from Delivered_Order_Items as oim 
					inner join OrderMaster as om on om.AutoId=oim.OrderAutoId
					where oim.AutoId=@AutoId),0) 

	END  
	RETURN @Total  
END  
GO
-------------------------------------------------
 
go
create FUNCTION  [dbo].[FN_DelOrder_IsProductTaxPerUnit]  
(  
	@AutoId int,  
	@orderAutoId int  
)  
RETURNS decimal(18,2)  
AS  
BEGIN  
	DECLARE @Total decimal(18,2)=0.00,    
	 @Tax int=null  
	IF (select tax from Delivered_Order_Items where AutoId=@AutoId)=1
	BEGIN  
		set @Total=(select isnull((oim.UnitPrice*om.TaxValue)/100,0) as Tax from Delivered_Order_Items as oim 
					inner join OrderMaster as om on om.AutoId=oim.OrderAutoId
					where oim.AutoId=@AutoId) 

	END  
	RETURN @Total  
END  
  
GO

alter table Delivered_Order_Items add IsProductTaxPerUnit  AS ([dbo].[FN_DelOrder_IsProductTaxPerUnit]([AutoId],[OrderAutoId]))
--------------------------
-----------------------------------------------------------------------------------------------------------------------------
 

alter table [dbo].[Delivered_Order_Items] add
[Del_discount] [decimal](18, 2), 
[IsDel_ProductTax]  AS ([dbo].[FN_DelOrder_IsProductTax]([AutoId],[OrderAutoId])), 
[StaticDelQty] [int],
[StaticNetPrice] [decimal](18, 2)

-----------------------------------------------------------------------------------------------------------------------------

alter table [dbo].[CreditMemoMaster] add [CreditMemoType] int,
[ReferenceOrderAutoId] int,
[TotalTax_Old] decimal(18,2)
  
------------------------------------------------------------------------------------------------------------------------------

alter table [dbo].[CreditItemMaster] add 
[OM_MinPrice] decimal(18,2),
[OM_CostPrice] decimal(18,2),
[OM_BasePrice] decimal(18,2)
GO 
------------------------------------------------------------------------------------------------------------------------------

ALTER TABLE CreditMemoMaster DROP COLUMN MLTax
GO
ALTER FUNCTION  [dbo].[FN_Credit_MLTax]
( 
@CreditAutoId int
)
RETURNS decimal(18,2)
AS
BEGIN
	DECLARE @MLTax decimal(18,2)=0.00
	IF EXISTS(select 1 from CreditMemoMaster where CreditAutoId=@CreditAutoId and isnull(creditmemoType,0)=2)
	Begin
	SET @MLTax=isnull((SELECT sum(Item_TotalMLTax) FROM CreditItemMaster WHERE CreditAutoId=@CreditAutoId),0)	
	END
	RETURN @MLTax
END

GO
ALTER TABLE CreditMemoMaster add MLTax as [dbo].[FN_Credit_MLTax](CreditAutoId)
go

update CreditMemoMaster set creditmemoType=2 where creditmemoType is null


   
