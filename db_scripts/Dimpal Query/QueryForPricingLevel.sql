delete from ProductPricingInPriceLevel where AutoId in (
	select AutoId  from (
		select AutoId,PriceLevelAutoId,ProductAutoId,UnitAutoId,createDated,
		ROW_NUMBER() over(partition by PriceLevelAutoId,ProductAutoId,UnitAutoId order by PriceLevelAutoId,ProductAutoId,UnitAutoId,createDated desc,AutoId desc)  as rn
		from ProductPricingInPriceLevel as t 
		where exists(
			select PriceLevelAutoId,ProductAutoId,UnitAutoId,COUNT(1) from ProductPricingInPriceLevel 
			where PriceLevelAutoId=t.PriceLevelAutoId and ProductAutoId=t.ProductAutoId and UnitAutoId=t.UnitAutoId
			group by PriceLevelAutoId,ProductAutoId,UnitAutoId 
			having COUNT(1) >1
		)
		--order by ProductAutoId,UnitAutoId
	) as t1
	where rn!=1
)

ALTER table ProductPricingInPriceLevel
ADD CONSTRAINT CompKey_PriceLevel UNIQUE (PriceLevelAutoId,ProductAutoId,UnitAutoId);