USE [psmnj.a1whm.com]
GO

/****** Object:  Table [dbo].[Delete_OrderMaster_Backup]    Script Date: 04/14/2020 22:12:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Delete_OrderMaster_Backup](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[OrderNo] [varchar](15) NULL,
	[OrderDate] [datetime] NULL,
	[DeliveryDate] [datetime] NULL,
	[CustomerAutoId] [int] NULL,
	[Terms] [int] NULL,
	[BillAddrAutoId] [int] NULL,
	[ShipAddrAutoId] [int] NULL,
	[SalesPersonAutoId] [int] NULL,
	[OverallDiscAmt] [decimal](8, 2) NULL,
	[ShippingCharges] [decimal](8, 2) NULL,
	[Status] [int] NULL,
	[PackerAutoId] [int] NULL,
	[Driver] [int] NULL,
	[AssignDate] [datetime] NULL,
	[Stoppage] [varchar](10) NULL,
	[DelDate] [datetime] NULL,
	[PaymentRecev] [varchar](20) NULL,
	[RecevAmt] [varchar](20) NULL,
	[AmtValue] [decimal](8, 2) NULL,
	[ValueChanged] [varchar](20) NULL,
	[DiffAmt] [decimal](8, 2) NULL,
	[NewTotal] [decimal](8, 2) NULL,
	[PayThru] [int] NULL,
	[DrvRemarks] [varchar](200) NULL,
	[AmtDue] [decimal](8, 2) NULL,
	[PackedBoxes] [int] NULL,
	[AcctRemarks] [varchar](200) NULL,
	[ShippingType] [int] NULL,
	[CommentType] [int] NULL,
	[Comment] [varchar](max) NULL,
	[CheckNo] [varchar](150) NULL,
	[AddonPackedQty] [int] NULL,
	[TaxType] [int] NULL,
	[ManagerAutoId] [int] NULL,
	[AccountAutoId] [int] NULL,
	[PackerRemarks] [varchar](500) NULL,
	[OrderRemarks] [varchar](500) NULL,
	[RootName] [varchar](100) NULL,
	[OrderType] [int] NULL,
	[paidAmount] [decimal](10, 2) NULL,
	[paymentMode] [int] NULL,
	[referNo] [varchar](50) NULL,
	[POSAutoId] [int] NULL,
	[TaxValue] [decimal](10, 2) NULL,
	[PastDue] [decimal](10, 2) NULL,
	[referenceOrderNumber] [varchar](50) NULL,
	[ManagerRemarks] [varchar](500) NULL,
	[PackerAssignDate] [datetime] NULL,
	[PackerAssignStatus] [int] NULL,
	[Times] [int] NULL,
	[UPDATEDATE] [datetime] NULL,
	[UPDATEDBY] [int] NULL,
	[WarehouseAutoId] [int] NULL,
	[WarehouseRemarks] [varchar](500) NULL,
	[UnitMLTax] [decimal](10, 2) NULL,
	[MLTaxPer] [decimal](10, 2) NULL,
	[MLQty]  AS ([dbo].[FN_Order_MLQty]([Autoid],[Status])),
	[OverallDisc]  AS ([dbo].[FN_OrderOverallDisc]([AutoId])),
	[MLTax]  AS ([dbo].[FN_Order_MLTax]([AutoId],[Status])),
	[Deductionamount]  AS ([dbo].[FN_Order_DeductionAmount]([AutoId])),
	[CreditAmount]  AS ([dbo].[FN_StoreCredit]([Autoid])),
	[CancelledDate] [datetime] NULL,
	[CancelledBy] [int] NULL,
	[DriverRemarks] [varchar](500) NULL,
	[isMLManualyApply] [int] NULL,
	[IsMLTaxApply]  AS ([dbo].[FN_Order_IsMLtaxApply]([AutoId])),
	[IsTaxApply] [int] NULL,
	[RouteAutoId] [int] NULL,
	[RouteStatus] [int] NULL,
	[DeviceID] [varchar](500) NULL,
	[appVersion] [varchar](50) NULL,
	[latLong] [varchar](max) NULL,
	[AppOrderdate] [datetime] NULL,
	[CancelRemark] [varchar](1000) NULL,
	[TotalTax]  AS ([dbo].[FN_Order_TaxTotal]([AutoId])),
	[TotalAmount]  AS ([dbo].[FN_Order_TotalAmount]([AutoId])),
	[Weigth_OZTax] [decimal](10, 2) NULL,
	[Weigth_OZTaxAmount]  AS ([dbo].[FN_Order_Weigth_OZTaxAmount]([autoid])),
	[GrandTotal]  AS ([dbo].[FN_Order_GrandTotal]([autoid])),
	[PayableAmount]  AS ([dbo].[FN_Order_PayableAmount]([autoid])),
	[AdjustmentAmt]  AS ([dbo].[FN_Order_AdjustmentAmt]([autoid])),
	[Weigth_OZQty]  AS ([dbo].[FN_Order_Weigth_OZQty]([autoid])),
	[Order_Closed_Date] [datetime] NULL,
	[IsAttached] [bit] NULL,
	[AttachedDate] [datetime] NULL,
	[TotalNOI]  AS ([dbo].[FN_Order_TotalNoOfItem]([AutoId])),
	[MLTaxRemark] [varchar](500) NULL,
	[OrdShippingAddress] [varchar](max) NULL,
	[DraftCreateDate] [datetime] NULL,
	[DraftOrderDate] [datetime] NULL,
	[ReferenceNo] [varchar](30) NULL,
	[DeleteRemarks] [varchar](500) NULL,
 CONSTRAINT [PK_Delete_OrderMaster_Backup] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[Delete_OrderMaster_Backup] ADD  CONSTRAINT [DF_Delete_OrderMaster_Backup_PackerAssignStatus]  DEFAULT ((0)) FOR [PackerAssignStatus]
GO

ALTER TABLE [dbo].[Delete_OrderMaster_Backup] ADD  CONSTRAINT [DF_Delete_OrderMaster_Backup_Times]  DEFAULT ((0)) FOR [Times]
GO

ALTER TABLE [dbo].[Delete_OrderMaster_Backup] ADD  CONSTRAINT [DF_Delete_OrderMaster_Backup_UnitMLTax]  DEFAULT ((0)) FOR [UnitMLTax]
GO

ALTER TABLE [dbo].[Delete_OrderMaster_Backup] ADD  CONSTRAINT [DF_Delete_OrderMaster_Backup_MLTaxPer]  DEFAULT ((0.00)) FOR [MLTaxPer]
GO

ALTER TABLE [dbo].[Delete_OrderMaster_Backup] ADD  CONSTRAINT [DF_Delete_OrderMaster_Backup_isMLManualyApply]  DEFAULT ((1)) FOR [isMLManualyApply]
GO

ALTER TABLE [dbo].[Delete_OrderMaster_Backup] ADD  CONSTRAINT [DF_Delete_OrderMaster_Backup_IsTaxApply]  DEFAULT ((0)) FOR [IsTaxApply]
GO

ALTER TABLE [dbo].[Delete_OrderMaster_Backup] ADD  CONSTRAINT [DF_Delete_OrderMaster_Backup_Weigth_OZTax]  DEFAULT ((0.00)) FOR [Weigth_OZTax]
GO

ALTER TABLE [dbo].[Delete_OrderMaster_Backup] ADD  CONSTRAINT [DF_Delete_OrderMaster_Backup_IsAttached]  DEFAULT ('FALSE') FOR [IsAttached]
GO

ALTER TABLE [dbo].[Delete_OrderMaster_Backup] ADD  CONSTRAINT [DF_Delete_OrderMaster_Backup_DraftOrderDate]  DEFAULT (getdate()) FOR [DraftOrderDate]
GO


