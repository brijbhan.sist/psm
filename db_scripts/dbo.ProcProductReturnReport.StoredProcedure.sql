ALTER procedure [dbo].[ProcProductReturnReport]            
@Opcode INT=NULL,              
@SalesPersonAutoId int = null,              
@CustomerAutoId int=null,              
@FromDate datetime = null,              
@ToDate datetime = null,        
@ClosedFromDate datetime = null,              
@ClosedToDate datetime = null,              
@Status int = null,                 
@PageIndex INT=1,                
@PageSize INT=10,                
@RecordCount INT=null,                
@isException bit out,                
@exceptionMessage varchar(max) out               
as            
BEGIN                                                                          
 BEGIN TRY                                                                          
  SET @isException=0                                                                          
  SET @exceptionMessage='Success'            
            
  if @OpCode = 42            
  begin            
  SELECT ROW_NUMBER() OVER(ORDER BY OrderDate,OrderNo, [CustomerName]) AS RowNumber, * INTO #Results FROM                
  (              
  select cm.CustomerId, cm.CustomerName,(em.FirstName +' ' +ISNULL(em.LastName,'')) as SalesPerson,        
  (em1.FirstName +' ' +ISNULL(em1.LastName,'')) as Driver,        
  om.OrderNo,FORMAT(om.OrderDate,'MM/dd/yyyy') As OrderDate,isnull(FORMAT(om.Order_Closed_Date,'MM/dd/yyyy'),'N/A') as ClosedDate ,pm.ProductId,pm.ProductName,        
  um.UnitType,sum(doi.FreshReturnQty) as FreshReturnQty,sum(doi.DamageReturnQty) as DamageReturnQty         
  FROM Delivered_Order_Items  doi         
  inner join OrderMaster om on om.AutoId = doi.OrderAutoId            
  LEFT join EmployeeMaster em on om.SalesPersonAutoId = em.AutoId        
  LEFT join EmployeeMaster em1 on om.Driver = em1.AutoId            
  inner join CustomerMaster cm on cm.AutoId = om.CustomerAutoId            
  inner join ProductMaster pm on pm.AutoId = doi.ProductAutoId            
  inner join UnitMaster um on um.AutoId = doi.FreshReturnUnitAutoId            
  inner join UnitMaster um2 on um2.AutoId = doi.DamageReturnUnitAutoId            
  where om.status = 11 and (doi.FreshReturnQty > 0 or doi.DamageReturnQty > 0)            
  and (@CustomerAutoId = '0' or @CustomerAutoId is null or om.CustomerAutoId = @CustomerAutoId )              
  and (@SalesPersonAutoId = '0' or @SalesPersonAutoId is null or OM.SalesPersonAutoId = @SalesPersonAutoId)            
  and (@FromDate = '' or @FromDate is null or @ToDate = '' or @ToDate is null or CONVERT(date, OrderDate) between @FromDate and @ToDate)         
  and (@ClosedFromDate = '' or @ClosedFromDate is null or @ClosedToDate = '' or @ClosedToDate is null or CONVERT(date, om.Order_Closed_Date) between @ClosedFromDate and @ClosedToDate)        
  Group by cm.CustomerId, cm.CustomerName,om.OrderNo,om.OrderDate,pm.ProductId,pm.ProductName,um.UnitType,om.Order_Closed_Date ,       
  (em.FirstName +' ' +ISNULL(em.LastName,'')),(em1.FirstName +' ' +ISNULL(em1.LastName,''))            
  ) as t ORDER BY OrderDate,OrderNo, [CustomerName]            
            
  SELECT COUNT(CustomerName) AS RecordCount,case when @PageSize=0 then COUNT(RowNumber) else  @PageSize end AS PageSize, @PageIndex AS PageIndex,Format(getdate(),'MM/dd/yyyy hh:mm tt') as PrintDate FROM #Results                       
  SELECT * FROM #Results                
  WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))             
            
  SELECT ISNULL(SUM(DamageReturnQty),0) AS DamageReturnQty,ISNULL(SUM(FreshReturnQty),0) AS FreshReturnQty from  #Results     
  end            
            
  if @Opcode = 41            
  begin            
  SELECT AutoId,(CustomerId + ' - ' + CustomerName) as CustomerName from CustomerMaster where status = 1 order by (CustomerId + ' - ' + CustomerName)             
  SELECT AutoId,FirstName +' '+ LastName as SalesPerson  from EmployeeMaster where EmpType = 2 and status = 1 order by SalesPerson asc                
  select AutoId, StatusType from StatusMaster where Category = 'OrderMaster' and AutoId != 8             
  SELECT AutoId,FirstName + ' ' + LastName as DriverName from EmployeeMaster where EmpType in (2,5) and status = 1  
  order by DriverName           
  end            
  if @Opcode = 43            
  begin  
              
  SELECT AutoId,(CustomerId + ' - ' + CustomerName) as CustomerName from CustomerMaster where status = 1
   AND ( ISNULL(@SalesPersonAutoId,'')='' OR SalesPersonAutoId =@SalesPersonAutoId) order by  (CustomerId + ' - ' + CustomerName)                       
  end                                                
  END TRY                                                       
  BEGIN CATCH                                                                          
  SET @isException=1                                                                          
  SET @exceptionMessage= 'Oops, Something went wrong .Please try again.'                                                                          
  END CATCH                                                                          
END   
GO
