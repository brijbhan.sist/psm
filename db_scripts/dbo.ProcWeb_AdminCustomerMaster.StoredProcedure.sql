USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcWeb_AdminCustomerMaster]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[ProcWeb_AdminCustomerMaster]  --By Rizwan Ahmad on 10/19/2019 04:03 AM          
    @Opcode INT=NULL,  

	@PayAutoId int=null,
	@WebPaymentAutoId int=null,
	@ShipAutoId int=null,
	@WebShipAutoId int=null,
	@ShippingCharge decimal(18,2)=null,
	@Type Int=null,

	@Document int=null,
	@DocumentUrl varchar(MAX)='',

	@WeightTaxId int=null,
	@WeightTax decimal(18,2)=null,
	@MLTaxId int=null,
	@MLTax decimal(18,2)=null,

	@StoreId varchar(50)='',
	@StoreName varchar(50)='',
	@SalesPersonAutoId int=null,
	@StateId int=null,
	@CityId int=null,
	@Status int=null,
	                                         
	@PageIndex INT=1,                                                                    
	@PageSize INT=10,                                                                    
	@RecordCount INT=null,                       
    @IsException bit out,                                                  
    @ExceptionMessage varchar(max) out                  
AS            
BEGIN            
	   SET @IsException=0            
	   SET @ExceptionMessage=''  
   IF @Opcode=10 --Assign/Remove Payment Mode
   BEGIN
        IF @Type=1
		BEGIN
		     INSERT INTO WebCustomerPaymentMode (CustomerAutoId,PaymentAutoId,IsDefault) 
			 (Select @StoreId,AutoID,1 FROM PAYMENTModeMaster Where AutoID=@PayAutoId)
		END
		ELSE
		BEGIN
		     DELETE FROM WebCustomerPaymentMode Where CustomerAutoId=@StoreId and PaymentAutoId=@PayAutoId
		END
   END
   IF @Opcode=11 --Set Default Payment Mode
   BEGIN
		BEGIN
		     UPDATE WebCustomerPaymentMode SET IsDefault=0 where CustomerAutoId=@StoreId
			 Update WebCustomerPaymentMode SET IsDefault=1 where CustomerAutoId=@StoreId and  AutoId=@WebPaymentAutoId
		END
   END
   IF @Opcode=12 --Assign/Remove Shipping Mode
   BEGIN
        IF @Type=1
		BEGIN
		     IF Exists(SELECT * FROM WebCustomerShippingType Where ShippingAutoId=@ShipAutoId and CustomerAutoId=@StoreId)
			 BEGIN
			      IF @ShippingCharge!=0
				  BEGIN
				       UPDATE WebCustomerShippingType SET ShippingCharge=@ShippingCharge Where ShippingAutoId=@ShipAutoId and CustomerAutoId=@StoreId
				  END
			 END
			 ELSE
			 BEGIN
			      INSERT INTO WebCustomerShippingType(CustomerAutoId,ShippingAutoId,ShippingCharge,IsDefault) 
				 (Select @StoreId,AutoID,ISNULL(@ShippingCharge,0),1 FROM ShippingType Where AutoID=@ShipAutoId)
			 END
		END
		ELSE
		BEGIN
		     DELETE FROM WebCustomerShippingType Where CustomerAutoId=@StoreId and ShippingAutoId=@WebShipAutoId
		END
   END
   IF @Opcode=13 --Set Default Shipping Mode
   BEGIN
		UPDATE WebCustomerShippingType SET IsDefault=0 where CustomerAutoId=@StoreId
		Update WebCustomerShippingType SET IsDefault=1 where CustomerAutoId=@StoreId and  AutoId=@WebShipAutoId
   END
   IF @Opcode=14 --Update Tax Details
   BEGIN
      IF(@WeightTaxId=0 and @MLTaxId=0)
		BEGIN
				INSERT INTO WebCustomerTaxMaster (StoreId,TaxType,TaxValue) VALUES (@StoreId,'WeightTax',@WeightTax)
				INSERT INTO WebCustomerTaxMaster (StoreId,TaxType,TaxValue) VALUES (@StoreId,'MLTax',@MLTax)
		END
		ELSE
		BEGIN
				UPDATE WebCustomerTaxMaster SET TaxValue=@MLTax WHERE StoreId=@StoreId and AutoId=@MLTaxId
				UPDATE WebCustomerTaxMaster SET TaxValue=@WeightTax WHERE StoreId=@StoreId and AutoId=@WeightTaxId
		END
   END
   IF @Opcode=15 --Approve Store
   BEGIN
		UPDATE CustomerMaster SET Status=1 WHERE AutoId=@StoreId
   END
   IF @Opcode=16 --Upload Document
   BEGIN
		Insert into WebCustomerDocumentMaster (StoreId,WebAdminDocumentId,DocumentUrl,VerifyStatus) VALUES (@StoreId,@Document,@DocumentUrl,1)
   END
   IF @Opcode=17 --Upload Document
   BEGIN
        IF @Type=0
		BEGIN
		     UPDATE WebCustomerDocumentMaster SET VerifyStatus=0 WHERE AutoId=@StoreId
		END
		ELSE
		BEGIN
		     UPDATE WebCustomerDocumentMaster SET VerifyStatus=1 WHERE AutoId=@StoreId
		END
   END
   IF @Opcode=40 --Bind DropDown
   BEGIN
       SELECT AutoId,StateName FROM State WHERE Status=1 order by StateName
	   SELECT AutoId,CityName FROM CityMaster WHERE Status=1 order by CityName
	   SELECT AutoId,FirstName+' '+LastName as Name FROM EmployeeMaster Where EmpType=2 and Status=1 order by Name
	   SELECT AutoId,StatusType FROM StatusMaster Where Category='Store' 
   END
   IF @Opcode=41 --Store List
   BEGIN
       SELECT ROW_NUMBER() OVER(ORDER BY AutoId desc) AS RowNumber, * INTO #Results FROM                                                                    
       (    
       SELECT cm.AutoId,CustomerId,CustomerName,Email,Contact1,SM.StatusType as Status,SM.ColorCode,
       MobileNo,FaxNo,BusinessName,OPTLicence,SA.Address+' ,'+SA.City+' ,'+S.StateName+' ,'+SA.Zipcode as ShippingAddress,BA.Address+' ,'+BA.City+' ,'+SC.StateName+' ,'+BA.Zipcode as BillingAddress
	   FROM CustomerMaster  as cm
	   INNER JOIN WebUserStoreRelMaster as WSR on
	   cm.AutoId=WSR.StoreId
	   INNER JOIN BillingAddress as SA on
	   cm.DefaultShipAdd=SA.AutoId
	   INNER JOIN BillingAddress as BA on
	   cm.DefaultBillAdd=BA.AutoId
	   INNER JOIN StatusMaster as SM on
	   cm.Status=SM.AutoId and SM.Category='Store'
	   INNER JOIN State as S on
	   SA.State=S.AutoId
	   INNER JOIN State as SC on
	   BA.State=SC.AutoId
	    Where cm.WebStatus=1 
		and (ISNULL(@StoreName,'')='' or cm.CustomerName=@StoreName)
		and (ISNULL(@StoreId,'')='' or cm.CustomerId=@StoreId)
		and (ISNULL(@SalesPersonAutoId,0)=0 or cm.SalesPersonAutoId=@SalesPersonAutoId)
		and (ISNULL(@StateId,0)=0 or BA.State=@StateId)
		and (ISNULL(@CityId,0)=0 or BA.BCityAutoId=@CityId)
		and (ISNULL(@Status,2)=2 or cm.Status=@Status)
		) AS t ORDER BY AutoId 
	   SELECT COUNT(RowNumber) AS RecordCount, @PageSize AS PageSize, @PageIndex AS PageIndex FROM #Results  
       SELECT * FROM #Results                                                                    
       WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1    
   END 
   IF @Opcode=42 --Store Details
   BEGIN    
		Declare @DocumentStatus int=0,@PaymentMode int=0,@ShippingStatus int=0,@StoreStatus int=0,@ApproveStatus varchar(50)='Pending'

		IF EXISTS(SELECT WCD.VerifyStatus FROM WebAdminDocumentMaster AS WAD
		LEFT JOIN WebCustomerDocumentMaster AS WCD ON
		WAD.AutoId=WCD.WebAdminDocumentId
		and WCD.StoreId=@StoreId and WAD.Status=1
		Where (WCD.VerifyStatus is null or WCD.VerifyStatus=0) and WAD.IsRequired=1)
		BEGIN
			SET @DocumentStatus=0
		END
		ELSE
		BEGIN
		   SET @DocumentStatus=1
		END
		IF EXISTS( SELECT * FROM WebCustomerPaymentMode  Where CustomerAutoId=@StoreId)
		BEGIN
			SET @PaymentMode=1
		END
		IF EXISTS( SELECT * FROM WebCustomerShippingType  Where CustomerAutoId=@StoreId)
		BEGIN
			SET @ShippingStatus=1
		END

	   IF ((@DocumentStatus=1 and @PaymentMode=1) and @ShippingStatus=1)
	   BEGIN
	        SET @StoreStatus=1
			SET @ApproveStatus='Approved'
	   END	     
       SELECT CM.AutoId,CM.CustomerId as StoreId,CustomerName as StoreName,@ApproveStatus as StatusType,@StoreStatus as Status,EM.FirstName+' '+EM.LastName as SalesPerson,CM.ContactPersonName,
	   CM.BusinessName FROM CustomerMaster AS CM
	   INNER JOIN StatusMaster AS SM ON
	   CM.Status=SM.AutoId and SM.Category='Store'
	   INNER JOIN EmployeeMaster AS EM ON
	   CM.SalesPersonAutoId=EM.AutoId and EM.EmpType=2
	   WHERE WebStatus=1 and CM.AutoId=@StoreId 
   END   
   IF @Opcode=43 --Address List
   BEGIN
       SELECT BA.AutoId,CustomerAutoId,Address,S.StateName,Zipcode,IsDefault,BCityAutoId,City,S.StateName FROM BillingAddress AS BA
	   INNER JOIN State S on
	   BA.State=S.AutoId
	   Where CustomerAutoId=@StoreId
   END  
   IF @Opcode=44 --Payment Method
   BEGIN
      Select PMM.AutoID as PaymentAutoId,PaymentMode,WCPM.IsDefault,ISNULL(WCPM.AutoId,0) as WebAutoId from PAYMENTModeMaster as PMM
	  LEFT JOIN WebCustomerPaymentMode WCPM ON
	  PMM.AutoID=WCPM.PaymentAutoId
	  and WCPM.CustomerAutoId=@StoreId
   END
   IF @Opcode=45 --Shipping Method
   BEGIN
      Select ST.AutoId AS ShipAutoId,ST.ShippingType,WST.IsDefault,ISNULL(WST.AutoId,0) AS WebShipAutoId,ISNULL(WST.ShippingCharge,0.00) as ShippingCharge from ShippingType AS ST
	  LEFT JOIN WebCustomerShippingType AS WST ON
      ST.AutoId=WST.ShippingAutoId and WST.CustomerAutoId=@StoreId
   END
   IF @Opcode=46 --Tax
   BEGIN
      Select AutoId,StoreId,TaxType,ISNULL(TaxValue,0.00) as TaxValue from WebCustomerTaxMaster Where StoreId=@StoreId
   END
   IF @Opcode=47 --Document Details
   BEGIN      
      SELECT WAD.AutoId,WAD.DocumentName,WAD.Description,Case when WAD.IsRequired=1 then 'Yes' when WAD.IsRequired=0 then 'No' end as IsRequired,
	  WCD.DocumentUrl,WCD.VerifyStatus,WCD.AutoId as WebAutoId FROM WebAdminDocumentMaster AS WAD
	  LEFT JOIN WebCustomerDocumentMaster AS WCD ON
	  WAD.AutoId=WCD.WebAdminDocumentId
	  and WCD.StoreId=@StoreId and WAD.Status=1

	  Select AutoId,DocumentName+' - '+case when isRequired=1 then 'Required' end as DocumentName from WebAdminDocumentMaster
   END
END
GO
