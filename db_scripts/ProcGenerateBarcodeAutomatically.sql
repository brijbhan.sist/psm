Create or alter Procedure ProcGenerateBarcodeAutomatically -- exec ProcGenerateBarcodeAutomatically
AS
BEGIN		
  BEGIN TRY
	BEGIN TRAN
			Declare @productId int,@PreDefinedBarcode varchar(25),@Barcode varchar(25),@UnitType int
			Select ROW_NUMBER() OVER(ORDER BY ProductId) as ID,ProductId,UnitType into #Result from ProductMaster as Pm
			INNER JOIN PackingDetails as Pd on Pm.AutoId=Pd.ProductAutoId
			where not exists(select ib.autoid from ItemBarcode as IB where IB.UnitAutoId=Pd.UnitType AND IB.ProductAutoId=Pd.ProductAutoId)

			Declare @i int=1,@Count int=0                                            
			SELECT @Count=COUNT(*) from #Result                                            
			IF (@Count>0)                                         
			BEGIN                                                       
				WHILE @i <= @Count                                                          
				BEGIN           
					Select @ProductID=ProductId,@UnitType=UnitType from #Result where ID=@i 
					SET @Barcode=(select dbo.FN_GenerateUPCAbarcode(@UnitType))                                                                                                      
					EXEC Proc_BarCodeMaster_ALLDB @ProductId=@ProductID,@UnitAutoId=@UnitType,@Barcode=@Barcode,@BarcodeType=1                                                  
					SET @i=@i+1                                                          
				END                                                          
			END 
	COMMIT TRAN
	END TRY
	BEGIN CATCH
	ROLLBACK TRAN
	SELECT ERROR_mESSAGE()
	END CATCH
END

