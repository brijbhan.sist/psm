USE [psmnj.a1whm.com]
GO

/****** Object:  Table [dbo].[DamageStockMaster]    Script Date: 06/08/2020 19:26:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DamageStockMaster](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[OrderId] [varchar](30) NULL,
	[Status] [int] NULL,
	[Type] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_DamageStockMaster] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


