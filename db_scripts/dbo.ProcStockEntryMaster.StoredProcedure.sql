CREATE or Alter PROCEDURE [dbo].[ProcStockEntryMaster]                                
@Opcode INT=NULL,                            
@BillAutoId INT=NULL,                            
@VendorAutoId int=null,                            
@BillNo VARCHAR(20)=NULL,                            
@BillDate DATETIME=NULL,                            
@Remarks VARCHAR(200)=NULL,                            
@BillItemsAutoId INT=NULL,                            
@ProductAutoId INT=NULL,                            
@UnitAutoId INT=NULL,                            
@Quantity INT=NULL,                            
@TotalPieces INT=NULL,                            
@QtyPerUnit INT=NULL,                            
@PageIndex INT = 1,                            
@PageSize INT = 10,                            
@RecordCount INT =null,                            
@FromBillDate DATETIME=null,                            
@ToBillDate DATETIME=null,                            
@BasePrice decimal(18,4)= null,                            
@RetailMIN decimal(18,4)= null,                            
@CostPrice decimal(18,4)= null,                            
@SRP decimal(18,2)= null,                            
@WholesaleMinPrice decimal(18,4)= null,                            
@ItemAutoId int= null,                            
@TableValue DT_BillItemList readonly,                            
@TblUpdateItems DT_UpdateBillItems readonly,                            
@Barcode VARCHAR(50)=NULL,                            
@BarcodeCount INT=NULL,                 
@EmpAutoId int=null,                           
@isException bit out,                            
@exceptionMessage varchar(max) out                            
AS                            
BEGIN                            
  Begin Try                            
  Set @isException=0                            
  Set @exceptionMessage='Success'                            
                            
  If @Opcode=11                              
  BEGIN                            
   Begin Try                            
    BEGIN TRAN                   
    IF NOT EXISTS(SELECT AutoId FROM [dbo].[StockEntry] WHERE [BillNo]=@BillNo)                  
    BEGIN                    
  INSERT INTO [dbo].[StockEntry] ([BillNo],[BillDate],[Remarks], [VendorAutoId],CreatedBy)   
  VALUES(@BillNo,@BillDate,@Remarks,@VendorAutoId,@EmpAutoId)      
                       
  SET @BillAutoId = SCOPE_IDENTITY()                            
                            
  INSERT INTO [dbo].[BillItems] ([BillAutoId],[ProductAutoId],[UnitAutoId],[Quantity],[QtyPerUnit],[Price])                            
  SELECT @BillAutoId,tb.[ProductAutoId],tb.[UnitAutoId],tb.[Quantity],tb.[QtyPerUnit], tb.Price FROM @TableValue AS tb                              
    
  insert into [dbo].[ProductStockUpdateLog]([ProductId],[OledStock],[NewStock],[UserAutoId],[DateTime],ReferenceId,ActionRemark,ReferenceType)      
  SELECT pm.ProductId,Stock,(ISNULL(stock,0)+ISNULL(newStock,0)) as [NewStock],@EmpAutoId,GETDATE(),@BillAutoId,('Stock received by Inventory Manager direct.'),'[BillItems]'  
  from (  
  SELECT ProductAutoId,sum(TotalPieces) AS NewStock FROM [BillItems] WHERE BillAutoId=@BillAutoId  
  GROUP BY ProductAutoId  
  ) as t  inner join productMaster as pm on pm.AutoId=t.ProductAutoId  
                                     
  UPDATE PM SET [Stock]= isnull([Stock],0)+isnull(t.NewStock,0) FROM [ProductMaster] AS PM   
  INNER JOIN (  
  SELECT ProductAutoId,sum(TotalPieces) AS NewStock FROM [BillItems] WHERE BillAutoId=@BillAutoId  
  GROUP BY ProductAutoId  
  ) as t on t.ProductAutoId=pm.AutoId           
    END                        
    ELSE                  
    BEGIN                  
    Set @isException=1                            
    Set @exceptionMessage='This bill no already proccesed'                  
    END                  
    COMMIT TRANSACTION                            
   End Try                            
   Begin Catch                            
       ROLLBACK TRAN                
    Set @isException=1                            
    Set @exceptionMessage='Oops! Something went wrong.Please try later.'                         
   End Catch                             
  END        
  If @Opcode=21                              
  BEGIN                            
   BEGIN TRY                            
    BEGIN TRAN   
		IF NOT EXISTS(SELECT AutoId FROM [dbo].[StockEntry] WHERE [BillNo]=@BillNo and AutoId!=@BillAutoId)                  
		BEGIN                  
			UPDATE PM SET [Stock]= [Stock]-CS.TotalPieces FROM [ProductMaster] AS PM                             
			INNER JOIN (  
			SELECT [ProductAutoId], sum([TotalPieces]) AS [TotalPieces] FROM [dbo].[BillItems]      
			where BillAutoId=@BillAutoId    
			GROUP BY [ProductAutoId]  
			) AS CS                            
			ON PM.AutoId=CS.ProductAutoId                          
                              
			UPDATE [dbo].[StockEntry] SET [BillNo]=@BillNo,[BillDate]=@BillDate,                            
			[Remarks]=@Remarks,vendorAutoId=@VendorAutoId,UpdatedBy=@EmpAutoId,UpdateDate=GETDATE() WHERE [AutoId]=@BillAutoId  
                                 
			delete from [BillItems] where BillAutoId=@BillAutoId                             
			INSERT INTO [dbo].[BillItems] ([BillAutoId],[ProductAutoId],[UnitAutoId],[Quantity],[QtyPerUnit],[Price])                            
			SELECT @BillAutoId,tb.[ProductAutoId],tb.[UnitAutoId],tb.[Quantity],tb.[QtyPerUnit], tb.Price FROM @TableValue AS tb                              
           
			INSERT INTO ProductStockUpdateLog (ProductId, OledStock, NewStock, UserAutoId , DateTime, ReferenceId, ActionRemark,ReferenceType )                
			SELECT PM.ProductId,PM.Stock,isnull(PM.Stock,0)+isnull([TotalPieces],0),@EmpAutoId,GETDATE(),@BillAutoId,
			'Update Bill by Inventory Manager  Bill No:-'+ @BillNo,'[BillItems]' FROM [ProductMaster] AS PM  
			INNER JOIN (  
			SELECT [ProductAutoId], sum([TotalPieces]) AS [TotalPieces] FROM [dbo].[BillItems]      
			where BillAutoId=@BillAutoId    
			GROUP BY [ProductAutoId]  
			) AS CS                            
			ON PM.AutoId=CS.ProductAutoId  
    
			UPDATE PM SET [Stock]= [Stock]+CS.TotalPieces FROM [ProductMaster] AS PM                             
			INNER JOIN (  
			SELECT [ProductAutoId], sum([TotalPieces]) AS [TotalPieces] FROM [dbo].[BillItems]      
			where BillAutoId=@BillAutoId    
			GROUP BY [ProductAutoId]  
			) AS CS                            
			ON PM.AutoId=CS.ProductAutoId   
		END                  
		ELSE                  
		BEGIN                  
			Set @isException=1                            
			Set @exceptionMessage='This bill no already proccesed.'                  
		END                  
                  
    COMMIT TRANSACTION                            
   END TRY                            
   BEGIN CATCH                            
		ROLLBACK TRAN                            
		Set @isException=1                            
		Set @exceptionMessage='Oops! Something went wrong.Please try later.'                          
   End Catch                             
  END                            
  If @Opcode=31                              
   BEGIN                            
    DELETE FROM [dbo].[BillItems] WHERE [BillAutoId]=@BillAutoId                            
    DELETE FROM [dbo].[StockEntry] WHERE [AutoId]=@BillAutoId                            
   END                            
  ELSE IF @Opcode=41                             BEGIN                            
    SELECT [AutoId],convert(varchar(10),[ProductId]) + '--' + [ProductName] as [ProductName] FROM [dbo].[ProductMaster]              
 where ProductStatus=1                        
   END                            
  ELSE IF @Opcode=42                            
   BEGIN                     
       --Start                     
    SELECT UM.AutoId AS AutoId, UM.UnitType AS UnitType, [Qty] FROM [dbo].[PackingDetails] AS PD                           
    INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = PD.UnitType              
    WHERE ProductAutoId = @ProductAutoId            
            
    SELECT PM.PackingAutoId from [dbo].[ProductMaster] AS PM             
    WHERE AutoId = @ProductAutoId order by PM.PackingAutoId             
       --End by RIZWAN AHMAD ON 12-09-2019              
   END                              
  ELSE IF @Opcode=43                          
   BEGIN     
                      
  select ROW_NUMBER() over(order by bd desc) as RowNumber,* into #Result from       
  ( select  se.[AutoId],vm.VendorName,[BillNo],      
  [BillDate] as bd,      
  CONVERT(VARCHAR(20), SE.[BillDate], 101) AS BillDate,                          
  (SELECT COUNT(distinct [ProductAutoId]) from [dbo].[BillItems] WHERE BillAutoId=SE.AutoId) AS NoOfItems,                           
  isnull((SELECT sum(Quantity*price) from [dbo].[BillItems] WHERE BillAutoId=SE.AutoId),0.00) AS TotalAmount,                          
  [Remarks]  FROM [dbo].[StockEntry] AS SE                           
  left join vendormaster as vm on vm.autoid=se.vendorautoid                           
  WHERE (@FromBillDate is null or @FromBillDate='' or @ToBillDate is null or @ToBillDate=''or (SE.[BillDate] between @FromBillDate and @ToBillDate))                          
  and (@BillNo is null or @BillNo='' or [BillNo] LIKE '%' + @BillNo + '%')                          
  and(@VendorAutoId=0 or VendorAutoId=@VendorAutoId)       
  ) as t                        
  order by bd desc                      
                       
                     
  SELECT * FROM #Result                    
  WHERE ISNULL(@PageSize,0) = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)                     
  SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize=0 then COUNT(RowNumber) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Result                    
                     
                         
   END                            
                              
  ELSE IF @Opcode=44                            
   BEGIN                            
		SELECT [AutoId],[BillNo],CONVERT(VARCHAR(20), SE.[BillDate], 101) AS BillDate,[Remarks],[VendorAutoId],
		(select top 1 VendorName from VendorMaster where AutoId=se.VendorAutoId) as VendorName
		FROM [dbo].[StockEntry] AS SE WHERE [AutoId]=@BillAutoId                            
                            
		SELECT BI.[AutoId],[ProductAutoId],PM.ProductId,PM.ProductName,[UnitAutoId],UM.UnitType,[Quantity],[TotalPieces],[QtyPerUnit], [Price],                
		convert(decimal(18,2),([Quantity]*[Price])) as[TotalPrice] FROM [dbo].[BillItems] AS BI                             
		INNER JOIN [dbo].[ProductMaster] As PM ON PM.AutoId = BI.[ProductAutoId]                            
		INNER JOIN [dbo].[UnitMaster] AS UM ON UM.[AutoId] = BI.[UnitAutoId]                             
		WHERE [BillAutoId]=@BillAutoId Order By BI.[AutoId] asc                            
		SELECT [AutoId],convert(varchar(10),[ProductId]) + '--' + [ProductName] as [ProductName] FROM [dbo].[ProductMaster]                            
   END                            
  ELSE IF @Opcode=45                            
   BEGIN  
		IF Not Exists (SELECT [ProductAutoId] FROM [dbo].[ItemBarcode] WHERE [Barcode]=@Barcode)                                                                                                                                      
		BEGIN   
			Set @isException=1                                                                                                                           
			Set @exceptionMessage='Invalid Barcode'
		END
        Else IF NOT EXISTS(SELECT * FROM [dbo].[ItemBarcode] as ib 
		inner join ProductMaster as pm on pm.AutoId=ib.ProductAutoId and ProductStatus=1
		WHERE [Barcode]=@Barcode)                                                                                                                                                    
		BEGIN   
			Set @isException=1                                                                                                                           
			Set @exceptionMessage='InactiveProduct'
		END
		ELSE
		BEGIN
			SET @ProductAutoId= (SELECT [ProductAutoId] FROM [dbo].[ItemBarcode] WHERE [Barcode]=@Barcode)                              
			SET @UnitAutoId=(SELECT [UnitAutoId] FROM [dbo].[ItemBarcode] WHERE [Barcode]=@Barcode)                              
			SELECT [AutoId],[ProductAutoId],[UnitAutoId] FROM [dbo].[ItemBarcode] WHERE [Barcode]=@Barcode                            
			SELECT UM.AutoId AS AutoId, UM.[UnitType] AS UnitType, [Qty],Price FROM [dbo].[PackingDetails] AS PD                            
			INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = PD.UnitType WHERE PD.[ProductAutoId] = @ProductAutoId                                
			AND PD.UnitType =@UnitAutoId    
   
			Exec [dbo].[ProcBarcodeScanning_Log] @Barcode=@Barcode,@ScannedBy=@EmpAutoId,@ActionRemarks='New Stock Entry',  
			@ModuleName='Manage PO > By Vendor > New Stock Entry (Inventory Manager)' 
		END
   END                            
  ELSE IF @Opcode=46                            
   BEGIN                               
   IF NOT EXISTS(SELECT [Barcode] FROM [dbo].[ItemBarcode] WHERE [Barcode]=@Barcode)                            
    BEGIN                            
     INSERT INTO [dbo].[ItemBarcode] ([ProductAutoId],[UnitAutoId],[Barcode]) Values(@ProductAutoId,@UnitAutoId,@Barcode)                            
    END                            
   END                            
  ELSE IF @Opcode=47                            
   BEGIN                            
  SELECT COUNT([Barcode]) AS BarcodeCount FROM [dbo].[ItemBarcode] WHERE [ProductAutoId]=@ProductAutoId AND [UnitAutoId]=@UnitAutoId                                 
  SELECT UM.AutoId AS AutoId,Price FROM [dbo].[PackingDetails] AS PD                            
  INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = PD.UnitType WHERE PD.[ProductAutoId] = @ProductAutoId                                
   END                            
  ELSE IF @Opcode=48                            
  BEGIN                            
    SELECT PD.AutoId as ItemAutoId,pm.ProductId,pm.ProductName,PD.Price,UM.UnitType,                            
    PD.Qty,P_SRP as SRP,CostPrice,MinPrice,pm.P_CommCode as CommCode,Location,WHminPrice FROM PackingDetails AS PD                             
    INNER JOIN UnitMaster AS UM ON UM.AutoId=PD.UnitType                            
    INNER JOIN ProductMaster As pm on pm.AutoId=PD.ProductAutoId                            
    WHERE ProductAutoId=@ProductAutoId AND PD.UnitType=@UnitAutoId                            
  END                            
  ELSE IF @Opcode=23                            
  BEGIN    
       BEGIN TRY
	    BEGIN TRANSACTION

			SELECT @ProductAutoId=ProductAutoId,@QtyPerUnit=Qty,@UnitAutoId=UnitType FROM PackingDetails where AutoId =@ItemAutoId 

			IF EXISTS(SELECT PD.AutoId FROM PackingDetails as PD  
			INNER JOIN ProductMaster as PM on PD.ProductAutoId=PM.AutoId 
			WHERE PM.AutoId = @ProductAutoId and PD.UnitType=@UnitAutoId AND @SRP<cast(@BasePrice/PD.Qty as decimal(10,4)))  
			BEGIN  
				 SET @isException=1  
				 SET @exceptionMessage='SRP'  
			END  
			ELSE
			BEGIN
				insert into PackingDetailsUpdateLog(ProductAutoId,UnitType,MinPrice,CostPrice,Price,WHminPrice,UpdatedByAutoId,DateTime)      
				select @ProductAutoId,pd.UnitType,pd.MinPrice,pd.CostPrice,pd.Price,pd.WHminPrice,@EmpAutoId,GETDATE() from PackingDetails pd      
				inner join UnitMaster um on um.AutoId = pd.UnitType      
				where ProductAutoId = @ProductAutoId  
                       
				UPDATE PackingDetails SET                         
				Price=CAST(((@BasePrice/@QtyPerUnit)*Qty) AS decimal(18,4))
				,MinPrice=CAST(((@RetailMIN/@QtyPerUnit)*Qty) AS decimal(18,4))
				,CostPrice=CAST(((@CostPrice/@QtyPerUnit)*Qty) AS decimal(18,4))
				,WHminPrice=CAST(((@WholesaleMinPrice/@QtyPerUnit)*Qty) AS decimal(18,4)) 
				where ProductAutoId =@ProductAutoId   

				Update ProductMaster SET NewSRP=@SRP where AutoId=@ProductAutoId
       
				EXEC [dbo].[ProcPriceLevelDeleteChangeCostPrice] @EmpAutoId=@EmpAutoId  
			END
		COMMIT TRANSACTION
	   END TRY
	   BEGIN CATCH
	    ROLLBACK TRANSACTION
	        SET @isException=1
			SET @exceptionMessage=ERROR_MESSAGE()
	   END CATCH
  END                            
  END TRY                            
  BEGIN CATCH                            
    Set @isException=1                     
    Set @exceptionMessage=ERROR_MESSAGE()--'Oops! Something went wrong.Please try later.'                            
  END CATCH                            
END 