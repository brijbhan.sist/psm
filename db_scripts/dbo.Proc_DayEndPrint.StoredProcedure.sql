USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[Proc_DayEndPrint]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Proc_DayEndPrint]  
		@OpCode int=null,  
		@OrderAutoId int =null,  
		@EndAutoId int =null  
 AS  
 BEGIN  
  
 IF @OpCode=41  
 BEGIN  
	  SELECT Autoid FROM OrderMaster WHERE Status IN (1,2,9)  
	  AND OrderDate<=(SELECT  CAST(EndDate AS datetime)+CAST(EndTime AS datetime) FROM tbl_DayEndReport AS RE WHERE  RE.AutoID=@EndAutoId)
	  AND SalesPersonAutoId=(SELECT RE.SalesPersonAutoId FROM tbl_DayEndReport AS RE WHERE  RE.AutoID=@EndAutoId)

	  Insert Into tbl_DayEndLogReport  ( ReportAutoId , OrderAutoID , PrintDate) 
	  SELECT @EndAutoId,Autoid,GETDATE() FROM OrderMaster WHERE Status IN (1,2,9)  
	  AND OrderDate<=(SELECT  CAST(EndDate AS datetime)+CAST(EndTime AS datetime) FROM tbl_DayEndReport AS RE WHERE  RE.AutoID=@EndAutoId)
	  AND SalesPersonAutoId=(SELECT RE.SalesPersonAutoId FROM tbl_DayEndReport AS RE WHERE  RE.AutoID=@EndAutoId)

 END  
 ELSE IF @OpCode=42  
 BEGIN  
  
	  SELECT OM.[AutoId],OM.[OrderNo],CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,                                                                  
	  CM.[CustomerId],CM.[CustomerName],                                                                        
	  EM.[FirstName] + ' ' + EM.[LastName] As SalesPerson,                                                     
	  (select EM1.[FirstName] + ' ' + EM1.[LastName] from [dbo].[EmployeeMaster] AS EM1 where EM1.[AutoId]                                                                   
	  = OM.PackerAutoId) AS PackerName,                                                                  
	  (select EM1.[FirstName] + ' ' + EM1.[LastName] from [dbo].[EmployeeMaster] AS EM1 where EM1.[AutoId]                                                                   
	  = OM.warehouseAutoid) AS WarehouseName,OrderRemarks,WarehouseRemarks  FROM [dbo].[OrderMaster] As OM                                                                  
	  INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.Status AND SM.[Category] = 'OrderMaster'                                                                  
	  INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[AutoId] = OM.[BillAddrAutoId]                                                                  
	  INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = OM.[ShipAddrAutoId]                                                                  
	  INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                                                                   
	  INNER JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                                                                   
	  INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                                                   
	  INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]                                                                  
	  LEFT JOIN [dbo].[CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                                                                  
	  LEFT JOIN [dbo].[ShippingType] AS ST ON ST.[AutoId] = OM.[ShippingType]                                                                      
	  INNER JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[SalesPersonAutoId] WHERE OM.AutoId = @OrderAutoId                                                                  
                                                                      
	  SELECT * FROM (                                                                  
	  SELECT PM.[ProductId],PM.[ProductName],(                                                                  
	  select top 1 Location  from PackingDetails as pd where pd.ProductAutoId=PM.AutoId and                                                                  
	  pd.UnitType=OIM.UnitTypeAutoId) AS [ProductLocation],                                                                  
	  UM.[UnitType],OIM.[TotalPieces],                                                                  
	  OIM.[RequiredQty] FROM [dbo].[OrderItemMaster] AS OIM                                                                   
	  INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                                                               
	  INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitTypeAutoId        
	 WHERE [OrderAutoId] = @OrderAutoId                                                                   
	  ) AS T ORDER BY  ISNULL(ProductLocation,'') ASC,ProductId       
	  
	  
	     
  
 END  
END                                                           
GO
