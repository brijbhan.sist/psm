USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[Proc_Order_EmailBody]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Proc_Order_EmailBody] --- Proc_Order_EmailBody @OrderAutoId=11438   
   
  @OrderAutoId int  
AS                    
BEGIN
SELECT  OrderNo,FORMAT(OrderDate,'MM/dd/yyyy')as OrderDate,CustomerId,cm.CustomerName,  
CM.ContactPersonName,(CM.[Contact1] + '/' + CM.[Contact2])as Contact,  
CT.[TermsDesc]as TermsDesc,ST.[ShippingType]as ShippingType,(EM.[FirstName] + ' ' + EM.[LastName])as SalesPerson,  
CM.BusinessName as BusinessName,(SA.[Address]+','+S1.[StateName]+','+SA.[City]+','+SA.[Zipcode])as shipAddr,  
(BA.[Address]+','+S.[StateName]+','+BA.[City]+','+BA.[Zipcode])as BillAddr,TotalAmount   
FROM [dbo].[OrderMaster] As OM                                                                                
INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.Status AND SM.[Category] = 'OrderMaster'                                                               
INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[AutoId] = OM.[BillAddrAutoId]                                                                                    
INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = OM.[ShipAddrAutoId]                                 
INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                                                                                     
LEFT JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                                                                         
INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                                                                   
LEFT JOIN [dbo].[CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                                                                  
LEFT JOIN [dbo].[ShippingType] AS ST ON ST.[AutoId] = OM.[ShippingType]                                                                                     
left JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[SalesPersonAutoId]       
WHERE OM.AutoId = @OrderAutoId 

 SELECT ROW_NUMBER()  over(order by CM.[CategoryName]) as RowNo,CM.[CategoryName],PM.[ProductId],PM.[ProductName],                                                                                    
 UM.[UnitType],OIM.[UnitPrice],OIM.[TotalPieces],OIM.[SRP],OIM.[GP],OIM.[Tax],                                                                                    
 OIM.[NetPrice],OIM.[Barcode],OIM.[QtyShip],OIM.[RequiredQty] ,   
 OIM.IsExchange,ISNULL(isFreeItem,0) AS isFreeItem  
 into #OrderItem FROM [dbo].[OrderItemMaster] AS OIM                                                             
 INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                                          
 INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitTypeAutoId                                                                                     
 INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId]  
 WHERE [OrderAutoId] = @OrderAutoId                                                                          
 ORDER BY CM.[CategoryName] ASC
 select * from #OrderItem
 select Sum(RequiredQty) as TotalQty from #OrderItem
End
GO
