ALTER FUNCTION  [dbo].[FN_GETPaidAmount]
(
	 @OrderAutoId int
)
RETURNS decimal(10,2)
AS
BEGIN
	DECLARE @PaidAmount decimal(10,2)=0.00
	IF EXISTS(SELECT 1 FROM PaymentOrderDetails AS POD WHERE POD.OrderAutoId=@OrderAutoId)
	BEGIN
		IF EXISTS(SELECT 1 FROM PaymentOrderDetails AS POD INNER JOIN CustomerPaymentDetails AS CPD ON CPD.PaymentAutoId=POD.PaymentAutoId 
		AND POD.OrderAutoId=@OrderAutoId AND CPD.Status=0)
		BEGIN
			SET @PaidAmount=ISNULL((SELECT SUM(POD.ReceivedAmount) FROM PaymentOrderDetails AS POD INNER JOIN CustomerPaymentDetails AS CPD ON CPD.PaymentAutoId=POD.PaymentAutoId 
							 AND POD.OrderAutoId=@OrderAutoId AND CPD.Status=0),0)
			SET @PaidAmount=@PaidAmount+ISNULL((SELECT ISNULL(AdjestOrderAmount,0) FROM DeliveredOrders where OrderAutoId=@OrderAutoId),0)
		END
	END	
	ELSE
	BEGIN
		SET @PaidAmount=(SELECT ISNULL(OldPaidAmount,0)+ISNULL(AdjestOrderAmount,0) FROM DeliveredOrders where OrderAutoId=@OrderAutoId)
	END
	RETURN @PaidAmount
END