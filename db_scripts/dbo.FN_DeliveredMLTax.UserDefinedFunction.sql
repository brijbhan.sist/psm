USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_DeliveredMLTax]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION  [dbo].[FN_DeliveredMLTax]
(
	 @orderAutoId int
)
RETURNS decimal(10,2)
AS
BEGIN
	DECLARE @MLTax decimal(10,2)
	set @MLTax=isnull((select MLTax FROM OrderMaster where AutoId=@orderAutoId)	,0.00)
	RETURN @MLTax
END
GO
