USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_CreditMemo_Weight_Oz_Qty]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION  [dbo].[FN_CreditMemo_Weight_Oz_Qty]
(
	 @AutoId int
)
RETURNS decimal(18,2)
AS
BEGIN
	DECLARE @Weight_Oz decimal(18,2)	
	SET @Weight_Oz=isnull((select ISNULL(Weight_OZ,0)*ISNULL(TotalPeice,0) from CreditItemMaster where ItemAutoId=@AutoId),0.00)
	RETURN @Weight_Oz
END
GO
Alter table CreditItemMaster drop column [WeightQuantity]
Drop function [FN_CreditMemo_Weight_Oz_Qty]
Alter table CreditItemMaster Add [WeightQuantity]  AS ([dbo].[FN_CreditMemo_Weight_Oz_Qty]([ItemAutoId]))