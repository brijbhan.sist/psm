USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcCurrencyMaster]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[ProcCurrencyMaster]        
  @Opcode int=null,        
  @AutoId int=null,        
  @CurrencyName varchar(25)=null,        
  @Createdby int=null,        
  @Status int=null,        
  @CurrencyXml xml=null,                                                    
  @isException bit out,                                
  @exceptionMessage varchar(max) out          
as        
BEGIN                           
BEGIN TRY                          
  SET @isException=0                          
  SET @exceptionMessage='Success'        
 IF @Opcode=11        
 BEGIN        
  IF exists(select *from CurrencyMaster where  CurrencyName=(SELECT tr.td.value('CurrencyName[1]','varchar(25)') as CurrencyName FROM @CurrencyXml.nodes('/Xmlcurrency') tr(td) ))         
  BEGIN        
    SET @isException=1        
    SET @exceptionMessage='Currency name already exist.'        
  END        
  ELSE        
  BEGIN        
    insert into CurrencyMaster(CurrencyName,CurrencyValue,Status,Craetedby,CreatedDate,UpdatedBy,UpdatedDate)        
    select tr.td.value('CurrencyName[1]','varchar(25)') as CurrencyName,        
    tr.td.value('CurrencyValue[1]','decimal(10,2)') as CurrencyValue,        
    tr.td.value('Status[1]','int') as Status,        
    @Createdby,        
    GETDATE(),        
    @Createdby,        
    GETDATE() from  @CurrencyXml.nodes('/Xmlcurrency') tr(td)        
  END        
 END     
 ELSE IF @Opcode=21        
 BEGIN        
  IF  exists(select *from CurrencyMaster where          
  CurrencyName=(SELECT tr.td.value('CurrencyName[1]','varchar(25)') as CurrencyName FROM @CurrencyXml.nodes('/Xmlcurrency') tr(td) ) and         
  Autoid!=(SELECT tr.td.value('Autoid[1]','int') as Autoid FROM @CurrencyXml.nodes('/Xmlcurrency') tr(td)))        
  BEGIN        
    SET @isException=1        
    SET @exceptionMessage='Currency already exist.'        
  END        
  BEGIN        
    UPDATE CurrencyMaster         
    SET CurrencyName=(SELECT tr.td.value('CurrencyName[1]','varchar(25)') as CurrencyName FROM @CurrencyXml.nodes('/Xmlcurrency') tr(td) ),        
    CurrencyValue=(SELECT tr.td.value('CurrencyValue[1]','decimal(10,2)') as CurrencyValue FROM @CurrencyXml.nodes('/Xmlcurrency') tr(td)),        
    Status=(SELECT tr.td.value('Status[1]','int') as Status FROM @CurrencyXml.nodes('/Xmlcurrency') tr(td)),        
    Craetedby=@Createdby,        
    CreatedDate=GETDATE(),        
    UpdatedBy=@Createdby,        
    UpdatedDate=GETDATE() WHERE         
    Autoid=(SELECT tr.td.value('Autoid[1]','int') as Autoid FROM @CurrencyXml.nodes('/Xmlcurrency') tr(td))        
  END         
 END    
 ELSE IF @Opcode=31        
 BEGIN        
   SELECT AutoId,CurrencyName,CurrencyValue,(case when Status=1 then 'Active' ELSE  'Inactive' END) as Status FROM CurrencyMaster            
   WHERE  (@CurrencyName is null or @CurrencyName='' OR CurrencyName like '%'+@CurrencyName+'%')        
   and (@Status=2 or Status=@Status)             
 END     
 ELSE IF @OpCode=41        
 BEGIN        
      SELECT AutoId,CurrencyName,CurrencyValue,Status FROM CurrencyMaster WHERE AutoId=@AutoId        
 END        
 ELSE IF @Opcode=42        
 BEGIN        
  BEGIN try        
      DELETE FROM CurrencyMaster WHERE AutoId =@AutoId        
  END try        
  BEGIN CATCH                          
   SET @isException=1                          
   SET @exceptionMessage='Currency has been used in application.'                            
  END CATCH          
 END         
END TRY                          
BEGIN CATCH                          
   SET @isException=1                          
   SET @exceptionMessage='Oops! Something went wrong.Please try later.'                         
END CATCH                          
END 
GO
