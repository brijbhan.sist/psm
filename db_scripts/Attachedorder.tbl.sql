USE [psmnj.a1whm.com]
GO

/****** Object:  Table [dbo].[AttachedOrder_ReceivePayment]    Script Date: 8/6/2021 12:25:59 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AttachedOrder_ReceivePayment](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[OrderAutoId] [int] NULL,
	[PaymentAutoId] [int] NULL,
 CONSTRAINT [PK_AttachedOrderForPayment] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


