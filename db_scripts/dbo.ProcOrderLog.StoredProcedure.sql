
ALTER PROCEDURE [dbo].[ProcOrderLog]    
	 @Opcode INT=NULL,    
	 @OrderAutoId INT=NULL,    
    
	 @isException bit out,    
	 @exceptionMessage varchar(max) out   
AS    
BEGIN    
 BEGIN TRY    
	Set @isException=0    
	Set @exceptionMessage='Success'    
    
	 If @Opcode=401    
	  BEGIN    
		SELECT EM.[FirstName] + ' ' + Em.[LastName] AS EmpName,AM.[Action],[Remarks],Format(ActionDate,'MM/dd/yyyy hh:mm tt') As ActionDate,OM.[OrderNo]
		,Format(Om.[OrderDate],'MM/dd/yyyy hh:mm tt') As OrderDate FROM [dbo].[tbl_OrderLog] AS OL    
		LEFT JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OL.[EmpAutoId]    
		INNER JOIN [dbo].[tbl_ActionMaster] As AM ON AM.[AutoId] = OL.[ActionTaken]    
		INNER JOIN [dbo].[OrderMaster] AS OM ON OM.[AutoId] = OL.[OrderAutoId]    
		WHERE [OrderAutoId] = @OrderAutoId ORDER BY OL.[AutoId] desc      
		SELECT [OrderNo],Convert(varchar(30),Convert(date,[OrderDate]),101) As OrderDate FROm OrderMaster WHERE [AutoId] = @OrderAutoId    
	  END     
 END TRY    
 BEGIN CATCH    
	Set @isException=1    
	Set @exceptionMessage=ERROR_MESSAGE()    
 END CATCH    
END    
GO



