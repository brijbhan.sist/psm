--ALTER TABLE VendorPayMaster add PaymentType int 
alter PROCEDURE [dbo].[ProcVendorPaymentLog]                                                
@Opcode INT =null,                                                  
@VendorName varchar(20) = null,                                                
@PaymentType int = null,                                                
@PageIndex int=null,   
@VendorAutoId int=null,
@PaymentTypeName varchar(50) = null,   
@PayAutoId int=null,
@AutoId int=null,
@PageSize  int=10,    
@FromDate date=NULL,                                        
@ToDate date=NULL, 
@RecordCount int=null,                                                   
@isException bit out,                                                    
@exceptionMessage varchar(max) out                                                  
as                                                
begin                                                  
begin try                                                  
SET @exceptionMessage= 'Success'                                                    
   SET @isException=0      
    if @Opcode = 41                                                
   begin                                                
      select * from PAYMENTModeMaster where Status=1
   end     
   if @Opcode = 42                                                
   begin                                                
		SELECT ROW_NUMBER() OVER(ORDER BY PaymentDate desc) AS RowNumber,vm.AutoId ,(vpm.AutoId) as PaymentAutoId,
		vpm.PayId,vm.VendorName,Format(vpm.PaymentDate,'MM/dd/yyyy') as PaymentDate,vpm.PaymentAmount,     
		pm.PaymentMode,(em.FirstName+''+em.LastName) as payby,ptm.Type, (select count(AutoId)  from VenderPaymentOrderDetails
		where PaymentAutoId=vpm.AutoId) as NoOfBill,   
		vpm.Remark,vpm.ReferenceID,vpm.Status,sm.StatusType into #RESULT561  from VendorPayMaster vpm    
		inner join VendorMaster vm on vm.AutoId = vpm.VendorAutoId 
		inner join EmployeeMaster em on em.AutoId=vpm.CreatedBy
		inner join StatusMaster sm on sm.AutoId = vpm.Status and sm.Category = 'Pay'    
		inner join PAYMENTModeMaster pm on pm.AutoID = vpm.PaymentMode
		left join PaymentTypeMaster ptm on ptm.AutoId=vpm.PaymentType    
		WHERE (@VendorName is null or @VendorName ='' or vm.VendorName like '%' + @VendorName + '%') 
		and (@PaymentType is null or @PaymentType=0 or vpm.PaymentMode=@PaymentType)            
		and (@FromDate is null or @ToDate is null or (CONVERT(date,vpm.PaymentDate) between convert(date,@FromDate) and CONVERT(date,@ToDate))) 
 
		order by PaymentDate desc   
		SELECT COUNT(PaymentAutoId) AS RecordCount, @PageSize AS PageSize, @PageIndex AS PageIndex,FORMAT(getdate(),'MM/dd/yyyy hh:mm tt') as PrintDate FROM #RESULT561          
          
		SELECT * FROM #RESULT561          
		WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))   
	    order by PayId desc 
	
		SELECT ISNULL(SUM(PaymentAmount),0.00) AS TotalAmount FROM #RESULT561      
   end   
if @OpCode = 43    
 begin  
	select PayId,VendorAutoId,FORMAT(PaymentDate, 'MM/dd/yyyy') as PaymentDate,VendorName,PaymentAmount,pmm.PaymentMode,Remark,ReferenceID, 
	ISNULL((select CheckNO from CheckMaster where [PaymentReferenceNo]=@PayAutoId),'NA') as CheckNumber 
	from VendorPayMaster as VPM
	inner join VendorMaster as VM on VM.AutoId=VPM.VendorAutoId
	inner join PAYMENTModeMaster as pmm on pmm.AutoID=VPM.PaymentMode 
	where VPM.AutoId = @PayAutoId    
         
	select CurrencyName,CurrencyValue,CurrencyValue,NoofValue,TotalAmount   from CurrencyMaster as cm                                             
	INNER join VendorPaymentCurrencyDetails  as pcd on pcd.CurrencyAutoId=cm.AutoId                                                             
	where  PaymentAutoId=@PayAutoId                                                                      
	order by cm.CurrencyValue  desc     
   
	SELECT em.BillNo,Format(em.BillDate,'MM/dd/yyyy') as BillDate,em.OrderAmount,PaymentAmount FROM VenderPaymentOrderDetails  as od
	inner join StockEntry as em on em.AutoId=od.OrderAutoId
	WHERE  PaymentAutoId=@PayAutoId 
 end                                              
   end try                                                  
  begin catch                             
   SET @isException=1                                                  
   SET @exceptionMessage= ERROR_MESSAGE()                                                   
end catch                                                  
end 
