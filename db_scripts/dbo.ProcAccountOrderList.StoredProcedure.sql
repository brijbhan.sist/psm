ALTER PROCEDURE [dbo].[ProcAccountOrderList]                                                                                                                                                    
 @Opcode INT=NULL,                                                                                                                                                                                                                                                                                                    
 @OrderNo VARCHAR(15)=NULL,                                                                                                                                                                                                                                                                                                      
 @CustomerAutoId INT=NULL,                                                                                                                                                                                                                                                                                                       
 @OrderType int =NULL,       
  @LoginEmpType INT=NULL,                                                                                                                                                     
 @PayableAmount decimal(10,2)=null,                                                                                                                                                              
 @SalesPersonAutoId INT=NULL,                                                               
 @OrderStatus INT=NULL,                                                                                                                                                                                                                                               
 @FromDelDate DATE=NULL,                                                                                     
 @ToDelDate DATE=NULL,
 @ShippingType INT=NULL,
 @DriverAutoId INT=NULL,                                                                                                                                                          
 @CustomerTypeAutoId int=null,          
 @PageIndex INT = 1,                
 @PageSize INT = 10,                                                                                                                            
 @RecordCount INT =null,                                                                                                                                                         
 @isException bit out,                                           
 @exceptionMessage varchar(max) out                                                                                      
AS                                                                                                                                                    
BEGIN                                                                                                                               
  BEGIN TRY                                                                                                                                            
   Set @isException=0                                                                                                                           
   Set @exceptionMessage='Success'                                                                                                         
   declare @customerType varchar(50)  ,@ShippingTaxEnabled int                                                                                                                                               
If @Opcode=419                                                                                                                      
  BEGIN                                                         
     SELECT ROW_NUMBER() OVER(ORDER BY [AutoId] desc) AS RowNumber, * INTO #Values1 from                                                                                                                           
     (                                           
		SELECT OM.[AutoId],sm.StatusType as Status,OM.[OrderNo],CONVERT(VARCHAR(20),OM.[OrderDate],101) As OrderDate,CM.[CustomerName],CM.CustomerType,OM.[GrandTotal],                                                                                          
        
		CONVERT(VARCHAR(20),OM.[DelDate],101) As DelDate,(emp.FirstName + ' '+ emp.LastName) as SalesPerson,                                                                                                                              
		(emp1.FirstName + ' '+ emp1.LastName) as DriverName,om.Status as StatusCode,ST.ShippingType,                                                                                                                                             
		otm.OrderType AS OrderType  FROM [dbo].[OrderMaster] AS OM                                                                                                      
		INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId] AND CM.Status=1                         
		INNER JOIN [dbo].[EmployeeMaster] AS emp ON emp.AutoId = OM.SalesPersonAutoId                            
		left JOIN [dbo].[EmployeeMaster] AS emp1 ON emp1.AutoId = OM.Driver                                                                                                                                                     
		inner join StatusMaster as sm on sm.AutoId= OM.Status and Category='OrderMaster'     
		   INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
		   INNER JOIN ShippingType as ST ON ST.AutoId=OM.ShippingType
		WHERE ((@OrderStatus =0  and OM.[Status] in (6,11,8)) or (OM.[Status] = @OrderStatus and AccountAutoId is not null) or (@OrderStatus!=8 and om.Status=@OrderStatus))                                                                                    
             
		AND (@OrderType =0  or @OrderType is null or  OM.OrderType=@OrderType)                                                                                 
		and(@SalesPersonAutoId = 0 or om.[SalesPersonAutoId]=@SalesPersonAutoId)               
		and(@CustomerTypeAutoId = 0 or CM.CustomerType=@CustomerTypeAutoId) 
		and(@ShippingType = 0 or om.ShippingType=@ShippingType) 
		and(@DriverAutoId = 0 or OM.Driver =@DriverAutoId)                                        
		and(@PayableAmount = 0 or OM.[GrandTotal] =@PayableAmount)                                                                                                                          
		AND (@CustomerAutoId is null or @CustomerAutoId=0 or CustomerAutoId=@CustomerAutoId)and                                                                                                                                                    
		(@OrderNo = '' OR @OrderNo IS NULL OR OM.[OrderNo] LIKE '%' + @OrderNo + '%')                                                    
		AND                                                                             
		(@FromDelDate = '' OR @FromDelDate IS NULL OR @ToDelDate = '' OR @ToDelDate IS NULL OR                                                                                                                                                     
		(CONVERT(date,OM.[OrderDate]) BETWEEN CONVERT(date,@fromDelDate) AND CONVERT(date,@ToDelDate)))                                                                                         
     )AS t                                                                                       
                                     
     SELECT CASE WHEN ISNULL(@PageSize,0)=0 then 0 else COUNT(*) end  as RecordCount,@PageSize as PageSize,@PageIndex as PageIndex FROM #Values1 
	 SELECT * FROM #Values1                                          
     WHERE ISNULL(@PageSize,0)=0 or                                                                     
  (RowNumber BETWEEN(@PageIndex - 1) * @PageSize + 1 AND (((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)                                                                   
    END  
	
	ELSE IF @Opcode=407                                                                                                                                  
     BEGIN          
			if @LoginEmpType= 7                                                                                                                                       
			BEGIN                                                                                                                                                
				SELECT [AutoId],[StatusType] FROM [dbo].[StatusMaster] WHERE  [Category]='OrderMaster'                                                        
				AND [AutoId] IN(1,2,9)      order by [StatusType] ASC                                                                                      
			END                                                                                                                                                
			ELSE IF(@LoginEmpType= 3 )                                                                             
			BEGIN                                                                                                                      
				SELECT [AutoId],[StatusType] FROM [dbo].[StatusMaster] WHERE  [Category]='OrderMaster'                                             
				AND [AutoId] IN(1,2,3,9,10)       order by [StatusType]  ASC                                                              
			END                                                                                                                                                    
			ELSE IF(@LoginEmpType= 6)                                                                                                            
			BEGIN                                                            
				SELECT [AutoId],[StatusType] FROM [dbo].[StatusMaster] WHERE  [Category]='OrderMaster'                                                                                     
				AND [AutoId] IN(6,11,8)          order by [StatusType]  ASC                                                                                                                                                      
			END            
			ELSE IF(@LoginEmpType= 5)                                                                                                       
			BEGIN                                                                                                        
				SELECT [AutoId],[StatusType] FROM [dbo].[StatusMaster] WHERE  [Category]='OrderMaster'                                                                                                                                                     
				AND [AutoId] IN(4,5,6)          order by [StatusType]  ASC                                                                                                                                                      
			END                                                                
			ELSE                                                                                                                                                    
			BEGIN                                                                                                                                             
				SELECT [AutoId],[StatusType] FROM [dbo].[StatusMaster] WHERE   [Category]='OrderMaster'          
				order by [StatusType]                                                                                                                                
			END                                                                                               
                                                                                                
  IF(@LoginEmpType != 2)                                       
  BEGIN                                       
   SELECT [AutoId],[CustomerId] + ' - ' + [CustomerName] As Customer FROM [dbo].[CustomerMaster]        
    order by [CustomerName] ASC,[CustomerId] ASC                                                                                              
  END                                                                       
  ELSE                                                                        
  BEGIN                                                                                                                                                    
   SELECT [AutoId],[CustomerId] + ' - ' + [CustomerName] As Customer FROM [dbo].[CustomerMaster]                                                                                                                                                    
   WHERE [SalesPersonAutoId]=@SalesPersonAutoId order by replace([CustomerName],' ','')    asc                                                                                                                                       
  END                                                                                                                                 
  SELECT AutoId,(FirstName +' '+ISNULL(LastName,'')) AS EmpName         
  FROM EmployeeMaster WHERE EmpType =2  order By (FirstName +' '+ISNULL(LastName,''))-- Updated on 11/19/2019 By Rizwan Ahmad EmpId removed                                                                                                          
                                                
  SELECT AutoId,(FirstName +' '+ISNULL(LastName,'')) AS EmpName FROM        
   EmployeeMaster WHERE EmpType =5  order by (FirstName +' '+ISNULL(LastName,''))-- Updated on 11/19/2019 By Rizwan Ahmad EmpId removed                                                                                                           
                                                    
  SELECT AutoId,ShippingType FROM ShippingType where Shippingstatus=1  ORDER BY ShippingType ASC         
  select AutoId,CustomerType FROM CustomerType  ORDER BY CustomerType ASC        
     select AutoId, OrderType from OrderTypeMaster where Type='Order'  order by OrderType asc  
	 

 END 
   ELSE IF @Opcode=435                                    
  BEGIN                                                      
  SELECT AutoId AS CustomerAutoId,CustomerId + ' ' + CustomerName as CustomerName FROM CustomerMaster  WHERE (CustomerType=@CustomerTypeAutoId                                     
  OR ISNULL(@CustomerTypeAutoId,0)=0) and (SalesPersonAutoId=@SalesPersonAutoId or ISNULL(@SalesPersonAutoId,0)=0) order by CustomerName ASC                                                    
  END 


  END TRY                                                                      
  BEGIN CATCH                               
  Set @isException=1                                          
  Set @exceptionMessage='Oops! Something went wrong.Please try later.'                                                                                                                                                    
  END CATCH                                                                                       
END 