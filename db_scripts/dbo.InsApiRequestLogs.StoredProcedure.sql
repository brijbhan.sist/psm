USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[InsApiRequestLogs]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsApiRequestLogs]   
@MethodName  varchar(250)=null,  
@accessToken    nvarchar(250)=null,  
@appVersion  varchar(15)=null,  
@deviceID  varchar(50)=null,  
@latLong  nvarchar(50)=null,  
@RouteAutoId varchar(50)=null,  
@userAutoId  varchar(50)=null,  
@utcTimeStamp datetime=null  
AS  
BEGIN  
Insert into App_ApiRequestLog(MethodName,accessToken,appVersion,deviceID,latLong,RouteAutoId,  
userAutoId,utcTimeStamp,CreatedOn) values(@MethodName,@accessToken,@appVersion,@deviceID,@latLong  
,@RouteAutoId,@userAutoId,@utcTimeStamp,GETDATE())  
END  
GO
