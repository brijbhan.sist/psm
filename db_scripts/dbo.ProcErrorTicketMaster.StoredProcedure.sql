ALTER PROCEDURE [dbo].[ProcErrorTicketMaster]        
 @Who nvarchar(25)=NULL,        
 @isException bit out,        
 @exceptionMessage nvarchar(500) out,        
 @TicketID nvarchar(50)=null out,        
 @PageURL nvarchar(50)=null,        
 @Priority nvarchar(50)=null,        
 @FromDate datetime=Null,        
 @ToDate datetime=Null,        
 @Type nvarchar(50)=null,        
 @ByEmployee nvarchar(50)=null,        
 @Subject nvarchar(50)=null,        
 @Description nvarchar(1000)=null,        
 @Status nvarchar(50)=null,        
 @TicketCloseDate Datetime=null,    
 @DeveloperStatus varchar(250)=null,  
 @CustomerType varchar(250)=null,      
 @Attach nvarchar(max)=null,        
 @TicketDate Datetime=null,        
 @opCode int=Null         
AS        
BEGIN        
 BEGIN TRY         
  BEGIN TRANSACTION        
           
        
   if @opCode=11        
   begin        
    set @TicketID=(select dbo.SequenceCodeGenerator('DTicket'))        
    insert into ErrorTicketMaster(TicketID,PageURL,Priority,Type,ByEmployee,Subject,Description,Status,Attach,TicketDate)        
    values (@TicketID,@PageURL,@Priority,@Type,@ByEmployee,@Subject,@Description,@Status,@Attach,GETDATE());           
    update SequenceCodeGeneratorMaster set currentSequence=currentSequence+1 where SequenceCode='DTicket'        
       
    SET @isException=0          
    end        
    if @opCode=41        
    begin        
                     
     select *,(select top 1 upper(Companyid) from CompanyDetails) as Companyid from EmailReceiverMaster    
  --union    
  --select 0 as id,EmpId as EmployeeId,FirstName+' '+ISNULL(LastName,'') as EmployeeName,Email as EmailID,'Client' as Category,    
  --(select top 1 upper(Companyid) from CompanyDetails) as Companyid from EmployeeMaster  where AutoId=@ByEmployee                
     SET @isException=0        
    end         
    if @opCode=61        
    begin        
  declare @Query varchar(max)            
  SELECT etm.TicketID,TicketDate,em.EmpId,em.FirstName + ' ' + LastName as fullname,etm.Priority,etm.Type,etm.Status,etm.Subject  
  ,etm.Description,etm.DeveloperStatus,TicketCloseDate from EmployeeMaster as em   
  inner join ErrorTicketMaster as etm on em.AutoId = etm.ByEmployee   
  where        
  (@Type is null or @type='' or etm.Type like '%'+@Type+'%' )    
  and (@TicketID is null or @TicketID='' or etm.TicketID like '%'+@TicketID+'%')      
  and (@Status is null or @Status='' or etm.Status like '%'+@Status+'%')        
  and (@CustomerType is null or @CustomerType='0' or em.EmpType like '%'+@CustomerType+'%')  
  and (@DeveloperStatus is null or @DeveloperStatus='0' or etm.DeveloperStatus like '%'+@DeveloperStatus+'%')           
  and (@Subject is null or @Status='' or Subject like '%'+@Subject+'%')        
  and (@ByEmployee is null or @Status='' or ByEmployee like '%'+@ByEmployee+'%')        
  and (@FromDate is null or ltrim(rtrim(@FromDate)) !='' or convert(nvarchar,@FromDate,101) != '01/01/2001' or @ToDate is null       
  or ltrim(rtrim(@ToDate)) !='' or convert(nvarchar,@ToDate,101) != '01/01/2001' or CONVERT(DATE,TicketDate)     
  between convert(DATE,@FromDate,101) and convert(DATE,@ToDate,101)  ) order by  TicketDate DESC        
           
             
      SET @isException=0        
    end         
                
        
           
  COMMIT TRAN        
 END TRY        
 BEGIN CATCH        
  ROLLBACK TRAN        
  SET @isException=1        
  SET @exceptionMessage= ERROR_MESSAGE()        
 END CATCH;        
END 
GO
