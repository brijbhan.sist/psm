Create or alter  function [dbo].[FN_ImproveBarcode] -- select dbo.FN_ImproveBarcode('361371384538')
(@Barcode varchar(20))
Returns varchar(15)
BEGIN 
    Declare @UseBarcode varchar(15)

	SET @UseBarcode=@Barcode

	IF ISNULL(@UseBarcode,'')!=''
	BEGIN
		IF @UseBarcode Not LIKE '%[^0-9]%'
		BEGIN
			SET @UseBarcode=(Select right(@UseBarcode,11))
			Declare @i int=1,@Count int=11,@num int=0,@multiplier int=0,@Even int=0,@Odd int=0,@lastDigit int=0  
                                                       
			WHILE @i <= @Count                                                          
			BEGIN   
				SET @num=(Select SUBSTRING(@UseBarcode, @i, 1))
				If(ISNULL(@i,0)%2=0)
				BEGIN
					SET @Even=ISNULL(@Even,0)+ISNULL(@num,0);
				END
				ELSE
				BEGIN
					SET @Odd=ISNULL(@Odd,0)+ISNULL(@num,0);
				END
				SET @i=@i+1  
			END 
			SET @lastDigit=@Even+(@Odd*3);
			SET @lastDigit=@lastDigit%10
			SET @lastDigit=10-@lastDigit

			IF EXISTS(Select AutoId FROM ItemBarcode Where Barcode=@UseBarcode+CONVERT(VARCHAR(10),@lastDigit))
			BEGIN
				SET @UseBarcode=@Barcode
			END
			ELSE
			BEGIN
				SET @UseBarcode=@UseBarcode+CONVERT(VARCHAR(10),@lastDigit)  
			END
		END
		ELSE
		BEGIN
			SET @UseBarcode=@Barcode
		END
	END
	return @UseBarcode
END



