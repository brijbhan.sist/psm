CREATE OR Alter PROCEDURE [dbo].[ProcBarcodeScanning_Log]           
@Barcode VARCHAR(12)=NULL,
@ScannedBy int=null,
@ActionRemarks varchar(500)=null,
@ModuleName varchar(200)=null
AS          
BEGIN         
	insert into [dbo].[BarcodeScanning_Log]
	([Barcode],[ScanningDate],[ScannedBy],[ActionRemarks],[ModuleName]) values
	(@Barcode,GETDATE(),@ScannedBy,@ActionRemarks,@ModuleName)
END        