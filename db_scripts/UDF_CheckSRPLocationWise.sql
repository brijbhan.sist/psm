Create function UDF_CheckSRPLocationWise--DROP function UDF_CheckSRPLocationWise
(
@ProductId INT,
@SRP decimal(19,2),
@BasePrice decimal(18,2),
@Type int
)
returns varchar(MAX)  
AS
BEGIN
    Declare @OutPut varchar(MAX)='',@Check varchar(max)=''
	IF EXISTS(SELECT PD.AutoId FROM [psmnj.a1whm.com].[dbo].PackingDetails as PD
	INNER JOIN [psmnj.a1whm.com].[dbo].ProductMaster as PM
	on PD.ProductAutoId=PM.AutoId AND PD.UnitType=PM.PackingAutoId
	WHERE PM.ProductId=@ProductId AND @SRP<(case when @Type=0 then @BasePrice else cast(@BasePrice/PD.Qty as decimal(10,4)) end))
	BEGIN
		SET @Check='PSMNJ'
	END
	IF EXISTS(SELECT PD.AutoId FROM [psmct.a1whm.com].[dbo].PackingDetails as PD
	INNER JOIN [psmct.a1whm.com].[dbo].ProductMaster as PM
	on PD.ProductAutoId=PM.AutoId AND PD.UnitType=PM.PackingAutoId
	WHERE PM.ProductId=@ProductId AND @SRP<(case when @Type=0 then @BasePrice else cast(@BasePrice/PD.Qty as decimal(10,4)) end))
	BEGIN
	    IF @Check!=''
		BEGIN
			SET @Check=@Check+',PSMCT'
		END
		ELSE
		BEGIN
		    SET @Check='PSMCT'
		END
	END
	IF EXISTS(SELECT PD.AutoId FROM [psmpa.a1whm.com].[dbo].PackingDetails as PD
	INNER JOIN [psmpa.a1whm.com].[dbo].ProductMaster as PM
	on PD.ProductAutoId=PM.AutoId AND PD.UnitType=PM.PackingAutoId
	WHERE PM.ProductId=@ProductId AND @SRP<(case when @Type=0 then @BasePrice else cast(@BasePrice/PD.Qty as decimal(10,4)) end))
	BEGIN
	    IF @Check!=''
		BEGIN
			SET @Check=@Check+',PSMPA'
		END
		ELSE
		BEGIN
		    SET @Check='PSMPA'
		END
	END
	IF EXISTS(SELECT PD.AutoId FROM [psmnpa.a1whm.com].[dbo].PackingDetails as PD
	INNER JOIN [psmnpa.a1whm.com].[dbo].ProductMaster as PM 
	on PD.ProductAutoId=PM.AutoId AND PD.UnitType=PM.PackingAutoId
	WHERE PM.ProductId=@ProductId AND @SRP<(case when @Type=0 then @BasePrice else cast(@BasePrice/PD.Qty as decimal(10,4)) end))
	BEGIN
	    IF @Check!=''
		BEGIN
			SET @Check=@Check+',PSMNPA'
		END
		ELSE
		BEGIN
		    SET @Check='PSMNPA'
		END
	END
	IF EXISTS(SELECT PD.AutoId FROM [psmwpa.a1whm.com].[dbo].PackingDetails as PD
	INNER JOIN [psmwpa.a1whm.com].[dbo].ProductMaster as PM 
	on PD.ProductAutoId=PM.AutoId AND PD.UnitType=PM.PackingAutoId
	WHERE PM.ProductId=@ProductId AND @SRP<(case when @Type=0 then @BasePrice else cast(@BasePrice/PD.Qty as decimal(10,4)) end))
	BEGIN
	    IF @Check!=''
		BEGIN
			SET @Check=@Check+',PSMWPA'
		END
		ELSE
		BEGIN
		    SET @Check='PSMWPA'
		END
	END
	IF EXISTS(SELECT PD.AutoId FROM [psmny.a1whm.com].[dbo].PackingDetails as PD
	INNER JOIN [psmny.a1whm.com].[dbo].ProductMaster as PM 
	on PD.ProductAutoId=PM.AutoId AND PD.UnitType=PM.PackingAutoId
	WHERE PM.ProductId=@ProductId AND @SRP<(case when @Type=0 then @BasePrice else cast(@BasePrice/PD.Qty as decimal(10,4)) end))
	BEGIN
	    IF @Check!=''
		BEGIN
			SET @Check=@Check+',PSMNY'
		END
		ELSE
		BEGIN
		    SET @Check='PSMNY'
		END
	END
	IF @Check=''
	BEGIN
	     SET @OutPut='ValidSRP'
	END
	ELSE
	BEGIN
	     SET @OutPut='Product SRP is less than base price.('+@Check+')'
	END
	return @OutPut
END