USE [psmnj.a1whm.com]
GO

/****** Object:  Table [dbo].[POReceiveProduct]    Script Date: 02/11/2020 16:58:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[POReceiveProduct](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[ProductAutoId] [int] NULL,
	[UnitAutoId] [int] NULL,
	[ReceiveQty] [int] NULL,
	[POReceiveAutoId] [int] NULL,
	[RecUnitAutoId] [int] NULL,
	[QtyPerUnit] [int] NULL,
 CONSTRAINT [PK_POReceiveProduct] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


