USE [psmnj.a1whm.com]
GO
Drop procedure ProcOrderMaster
Drop procedure ProcManagerEditOrderMaster
DROP type DT_PayAgainstOrder
go
CREATE TYPE [dbo].[DT_PayAgainstOrder] AS TABLE(
	[OrderAutoId] [int] NULL,
	[AmountPaying] [decimal](18, 2) NULL,
	[Remarks] [varchar](100) NULL
)
GO
