   
ALTER PROCEDURE [dbo].[ProcCustomerStatementReport]                               
@Opcode INT=Null,                                
@CustomerAutoId INT=NULL,        
@CustomerList  VARCHAR(500)=NULL,  
@FromDate date =NULL,                                  
@ToDate date =NULL,                               
@PageIndex INT=1,                                
@PageSize INT=10,                                
@RecordCount INT=null,                                                      
@isException BIT OUT,                                
@exceptionMessage VARCHAR(max) OUT                                
AS                                
BEGIN                                
 BEGIN TRY                                
 SET @isException=0                                
 SET @exceptionMessage='Success!!'                                
                            
if @Opcode=41                          
   Begin                          
	   select AutoId as CUId,Customerid+' '+CustomerName as CUN from CustomerMaster where Status=1 
	   order by Customerid+' '+CustomerName  for json path
   End                          
   else if @Opcode=42                          
   Begin                          
		select ROW_NUMBER() OVER(ORDER BY OrderDate asc) AS RowNumber,* into #Results48 from  (
		
			select OrderDate,FORMAT(convert(datetime,OrderDate),'MM/dd/yyyy hh:mm tt') as TransactionDate,                          
			'New Order Id :'+OrderNo+case when CreditAmount>0 or Deductionamount>0 then '*' else '' end as
			TransactionType,PayableAmount as OrderAmount                          
			,0.00 as PaidAmount from OrderMaster                           
			where Status not in (7,8)                      
			and (@FromDate is null or @ToDate is null or (CONVERT(date,OrderDate)                                   
			between convert(date,@FromDate) and CONVERT(date,@ToDate)))                            
			and (ISNULL(@CustomerList,'0')='0' OR CustomerAutoId                                  
			in (select * from dbo.fnSplitString(@CustomerList,',')))
			
			union  
			
			select PaymentDate,format(PaymentDate,'MM/dd/yyyy hh:mm tt') as TransactionDate,                          
			'Payment ID :'+cpd.PaymentId+' ('+      
			(SELECT PMM.PaymentMode FROM PAYMENTModeMaster AS PMM WHERE PMM.AUTOID=cpd.PaymentMode)+' )' 
			as TransactionType,0.00 as OrderAmount,cpd.ReceivedAmount as PaidAmount                          
			from CustomerPaymentDetails as cpd                          
			where      
			cpd.status=0 and PaymentMode!=5 and    
			(@FromDate is null or @ToDate is null or (CONVERT(date,PaymentDate)                                   
			between convert(date,@FromDate) and CONVERT(date,@ToDate)))                           
			and (ISNULL(@CustomerList,'0')='0' OR cpd.CustomerAutoId                                  
			in (select * from dbo.fnSplitString(@CustomerList,',')))
			
		) as  t order by OrderDate 
		
		SELECT * FROM #Results48 WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 
		AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))    
		
		SELECT COUNT(OrderDate) AS RecordCount, @PageSize AS PageSize, @PageIndex AS PageIndex,FORMAT(getdate(),'MM/dd/yyyy hh:mm tt') as PrintDate FROM #Results48 
		
		select * from #Results48                            
                       
		if(@FromDate is not null)                     
		Begin                          
			select Isnull((select sum(PayableAmount) from OrderMaster where CustomerAutoId in 
			(select * from dbo.fnSplitString(@CustomerList,',')) and  Status not in (7,8)  and 
			(CONVERT(date,OrderDate)<convert(date,@FromDate))),0) as OrderAmount,  

			Isnull((select sum(ReceivedAmount) from CustomerPaymentDetails where CustomerAutoId in
			(select * from dbo.fnSplitString(@CustomerList,',')) and PaymentMode!=5 and  Status=0 and
			(CONVERT(date,PaymentDate)<convert(date,@FromDate))),0) as PaidAmount,  
			
			ISNULL((Isnull((select sum(PayableAmount) from OrderMaster where CustomerAutoId in 
			(select * from dbo.fnSplitString(@CustomerList,',')) and  Status not in (7,8)  and 
			(CONVERT(date,OrderDate)<convert(date,@FromDate))),0)-  
			
			isnull((select sum(ReceivedAmount) from CustomerPaymentDetails where 
			PaymentMode!=5 and
			CustomerAutoId 
			in (select * from dbo.fnSplitString(@CustomerList,',')) and Status=0 and
			(CONVERT(date,PaymentDate)<convert(date,@FromDate))),0)),0) as BalanceForwardAmount                       
		End                          
		else                          
		Begin                  
			select 0.00 as BalanceForwardAmount                          
		End                            
   End                          
 END TRY                                
 BEGIN CATCH                                
	  SET @isException=1                                
	  SET @exceptionMessage=ERROR_MESSAGE()                                
 END CATCH                                
END 
