USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcProductBulkUpload]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ProcProductBulkUpload]
@Opcode int=Null,
@TableValue DT_BulkUpload readonly,
@ProductAutoId varchar(50)=NULL,
@ProductId varchar(50)=null,
@Category varchar(50)=null,
@Subcategory varchar(50)=null,
@ProductName varchar(100)=null,
@Location varchar(50)=null,
@Taxable varchar(50)=null,
@TaxRate varchar(50)=null,
@P_Barcode varchar(50)=null,
@P_MinPrice varchar(50)=null,
@P_CostPrice varchar(50)=null,
@P_BasePrice varchar(50)=null,
@P_SRP varchar(50)=null,
@P_CommCode varchar(50)=null,
@B_Qty varchar(50)=null,
@B_Barcode varchar(50)=null,
@B_MinPrice varchar(50)=null,
@B_CostPrice varchar(50)=null,
@B_BasePrice varchar(50)=null,
@B_SRP varchar(50)=null,
@B_CommCode varchar(50)=null,
@C_Qty varchar(50)=null,
@C_Barcode varchar(50)=null,
@C_MinPrice varchar(50)=null,
@C_CostPrice varchar(50)=null,
@C_BasePrice varchar(50)=null,
@C_SRP varchar(50)=null,
@C_CommCode varchar(50)=null,

@PageIndex INT = 1,
@PageSize INT = 15,
@RecordCount INT =null,

@isException bit out,
@exceptionMessage varchar(max) out
AS
BEGIN
	BEGIN TRY
		SET @isException = 0
		SET @exceptionMessage = 'Success'
		IF @Opcode = 11
			BEGIN		
				DELETE FROM [dbo].[tempProductDetails]
				INSERT INTO [dbo].[tempProductDetails] ([ProductId],[Category],[Subcategory],[ProductName],PreferVendor,ReOrderMarkBox,P_Qty,P_MinPrice,P_WholeSale,P_BasePrice,P_CostPrice,P_SRP,P_CommCode,P_Location,
                P_Barcode,B_Qty,B_MinPrice,B_WholeSale,B_BasePrice,B_CostPrice,B_SRP,B_CommCode,B_Location,B_Barcode,C_Qty,
                C_MinPrice,C_WholeSale,C_BasePrice,C_CostPrice,C_SRP,C_CommCode,C_Location,C_Barcode,D_Selling)					
				SELECT [ProductId],ltrim(rtrim([Category])),ltrim(rtrim([Subcategory])),[ProductName],PreferVendor,ReOrderMarkBox,P_Qty,P_MinPrice,P_WholeSale,P_BasePrice,P_CostPrice,P_SRP,P_CommCode,P_Location,
                P_Barcode,B_Qty,B_MinPrice,B_WholeSale,B_BasePrice,B_CostPrice,B_SRP,B_CommCode,B_Location,B_Barcode,C_Qty,
                C_MinPrice,C_WholeSale,C_BasePrice,C_CostPrice,C_SRP,C_CommCode,C_Location,C_Barcode,D_Selling FROm @TableValue AS t
               WHERE [ProductName]<>''
				
				update [tempProductDetails] set [ProductId]='PRD'+convert(varchar,productautoid) where productautoid not in 
				(SELECT productautoid FROM [dbo].[tempProductDetails] WHERE [ProductId] != '' )
				update [tempProductDetails] set [ProductId]='PRD'+[ProductId]+convert(varchar,productautoid) where productautoid not in 
				(
					SELECT productautoid FROM [dbo].[tempProductDetails] 
					WHERE [ProductId] NOT IN (SELECT [ProductId] FROM [dbo].[tempProductDetails] group by [ProductId] having count(ProductId) > 1)				
				)
				
			END
		ELSE IF @Opcode = 41
			BEGIN
				SELECT ROW_NUMBER() OVER(ORDER BY [ProductAutoId] asc) AS RowNumber, * INTO #Results from
				(			
					SELECT *,(CASE when [Category] in (SELECT [CategoryName] FROM CategoryMaster WHERE [Status] = 1) then 'Ok' else 'Not Ok' end) as chkCategory,
					(CASE when [Subcategory] in (SELECT [SubcategoryName] FROM SubCategoryMaster WHERE CategoryAutoId = (SELECT [AutoId] FROM CategoryMaster WHERE [CategoryName] = [Category] AND [Status] = 1)) then 'Ok' else 'Not Ok' end) as chkSubcategory,
					(CASE WHEN (B_Qty != '' AND [B_BasePrice] != '') OR ([B_Barcode] = '' AND B_Qty = '' AND [B_BasePrice] = '') THEN 'Ok' ELSE 'Not Ok' END) AS chkBox,
					(CASE WHEN (C_Qty != '' AND [C_BasePrice] != '') OR ([C_Barcode] = '' AND C_Qty = '' AND [C_BasePrice] = '') THEN 'Ok' ELSE 'Not Ok' END) AS chkCase
					 FROM [dbo].[tempProductDetails]
				) as t

				SELECT COUNT(*) as RecordCount,@PageSize as PageSize,@PageIndex as PageIndex FROM #Results
				SELECT * FROM #Results
			END
		ELSE IF @Opcode = 12
			BEGIN TRY
				BEGIN TRAN

					INSERT INTO [dbo].[ProductMaster]([ProductId],[CategoryAutoId],[SubcategoryAutoId],[ProductName],
					[ImageUrl],[Stock],PackingAutoId,ReOrderMark,UpdateDate,VendorAutoId)
					SELECT [ProductId],(SELECT [AutoId] FROM CategoryMaster WHERE [CategoryName] = [Category]),
					(SELECT [AutoId] FROM SubCategoryMaster WHERE [CategoryAutoId] = (SELECT [AutoId] FROM CategoryMaster WHERE [CategoryName] =                        [Category]) AND [SubcategoryName] = [Subcategory]),
					[ProductName],'/images/default_pic.png',0	
					,(SELECT AutoId FROM UnitMaster WHERE UnitName = D_Selling),ReOrderMarkBox,getdate(),
					(SELECT AutoId FROM VendorMaster WHERE VendorId = t.PreferVendor) FROM [dbo].[tempProductDetails] as t 
					WHERE ProductName<>'' AND [Category] in (SELECT [CategoryName] FROM CategoryMaster WHERE [Status] = 1) AND 
					[Subcategory] in (SELECT [SubcategoryName] FROM SubCategoryMaster WHERE CategoryAutoId = (SELECT [AutoId] FROM CategoryMaster                       WHERE [CategoryName] = [Category]) 
					AND [Status] = 1) 

					select pm.autoid as productmasterautoid, tpd.* into #tempPRD 
					FROM [dbo].[tempProductDetails] AS tPD 
					INNER JOIN [dbo].[ProductMaster] AS pm ON pm.[ProductId] = tPD.[ProductId]

					INSERT INTO [dbo].[PackingDetails] ([UnitType],[Qty],[MinPrice],[CostPrice],[Price],[SRP],[CommCode],[ProductAutoId])
					SELECT 3,1,(CASE WHEN [P_MinPrice] = '' THEN 0.00 ELSE Convert(decimal(8,2),[P_MinPrice]) END),
				    (CASE WHEN [P_CostPrice] = '' THEN Convert(decimal(8,2),[P_BasePrice]) ELSE
					Convert(decimal(8,2),[P_CostPrice]) END),Convert(decimal(8,2),[P_BasePrice]),
					(CASE WHEN [P_SRP] is null or [P_SRP] = '' THEN Convert(decimal(8,2),
					(case when [P_BasePrice]='0' then 1 else [P_BasePrice] end)) 
					ELSE Convert(decimal(8,2),(case when [P_SRP]='0' then 1 else Convert(decimal(8,2),[P_SRP]) end)) END),
					(CASE WHEN [P_CommCode] = '' THEN NULL ELSE Convert(decimal(8,4),[P_CommCode]) END),
					productmasterautoid FROM #tempPRD WHERE [P_BasePrice] != ''

					INSERT INTO [dbo].[PackingDetails] ([UnitType],[Qty],[MinPrice],[CostPrice],[Price],[SRP],[CommCode],[ProductAutoId])
					SELECT 2,case when B_Qty is null or B_Qty= '' Or B_Qty='0' then 1 else  Convert(int,B_Qty) end,
					(CASE WHEN [B_MinPrice] = '' THEN NULL ELSE Convert(decimal(8,2),[B_MinPrice]) END),
					(CASE WHEN [B_CostPrice] = '' THEN Convert(decimal(8,2),[B_BasePrice]) ELSE 
					Convert(decimal(8,2),[B_CostPrice]) END),Convert(decimal(8,2),[B_BasePrice]),
					(CASE WHEN [B_SRP] is null or [B_SRP] = '' 
					THEN Convert(decimal(8,2),(case when [B_BasePrice]='0' then 1 else  Convert(decimal(8,2),[B_BasePrice]) end))
					 ELSE Convert(decimal(8,2),(case when [B_SRP]='0' then 1 else [B_SRP] end)) END),
					(CASE WHEN [B_CommCode] = '' THEN NULL ELSE Convert(decimal(8,4),[B_CommCode]) END),
					productmasterautoid FROM #tempPRD WHERE [B_BasePrice] != '' AND B_Qty != ''
				
					INSERT INTO [dbo].[PackingDetails] ([UnitType],[Qty],[MinPrice],[CostPrice],[Price],[SRP],[CommCode],[ProductAutoId])
					SELECT 1,case when C_Qty is null or C_Qty= '' or C_Qty= 0 then 1 else  Convert(int,C_Qty) end,
					(CASE WHEN [C_MinPrice] = '' THEN 0.00 ELSE Convert(decimal(8,2),[C_MinPrice]) END),
					(CASE WHEN [C_CostPrice] = '' THEN Convert(decimal(8,2),[C_BasePrice]) ELSE Convert(decimal(8,2),[C_CostPrice]) END),
					Convert(decimal(8,2),[C_BasePrice]),
					(CASE WHEN [C_SRP] is null or [C_SRP] = '' or convert(decimal(10,2),[C_SRP]) = 0.0 then 1.0 ELSE Convert(decimal(8,2),(Convert(decimal(8,2),[C_SRP]))) END),			
					(CASE WHEN [C_CommCode] = '' THEN NULL ELSE Convert(decimal(8,4),[C_CommCode]) END),
					productmasterautoid FROM #tempPRD WHERE [C_BasePrice] != '' AND C_Qty != ''
					
					
					SELECT DISTINCT productmasterautoid,(CASE WHEN [P_Barcode] = '' THEN (SELECT [dbo].[RandomNumber]()) ELSE [P_Barcode] END) 
					as BarCode
					into #PeiceCode FROM #tempPRD WHERE [P_BasePrice] != '' AND [P_Barcode] NOT IN (SELECT [Barcode] FROM [dbo].[ItemBarcode])
              

					INSERT INTO [dbo].[ItemBarcode]([ProductAutoId],[UnitAutoId],[Barcode])
					select productmasterautoid,3,BarCode from #PeiceCode
					where [Barcode] IN (SELECT [Barcode] FROM #PeiceCode 
					group by [Barcode] having COUNT(BarCode)=1)
					
                    SELECT DISTINCT productmasterautoid,
				   (CASE WHEN [B_Barcode] = '' THEN (SELECT [dbo].[RandomNumber]()) ELSE [B_Barcode] END) AS BarCode INTO #TEMP FROM #tempPRD 
					WHERE [B_BasePrice] != '' AND B_Qty != '' AND [B_Barcode] NOT IN (SELECT [Barcode] FROM [dbo].[ItemBarcode])

					INSERT INTO [dbo].[ItemBarcode]([ProductAutoId],[UnitAutoId],[Barcode])
					SELECT distinct  productmasterautoid,2,BarCode FROM #TEMP 
					where [Barcode] IN (SELECT [Barcode] FROM #TEMP 
					group by [Barcode] having COUNT(BarCode)=1)
                    
                    SELECT DISTINCT productmasterautoid,(CASE WHEN [C_Barcode] = '' THEN (SELECT [dbo].[RandomNumber]()) ELSE [C_Barcode] END)
                    as BarCode into #CBarCode FROM #tempPRD 
					WHERE [C_BasePrice] != '' AND C_Qty != '' AND [C_Barcode] NOT IN (SELECT [Barcode] FROM [dbo].[ItemBarcode])

					INSERT INTO [dbo].[ItemBarcode]([ProductAutoId],[UnitAutoId],[Barcode])
					select productmasterautoid,1,BarCode from #CBarCode
					where [Barcode] IN (SELECT [Barcode] FROM #CBarCode 
					group by [Barcode] having COUNT(BarCode)=1)
					
					
					
					SELECT MIN([AutoId]) as autoid into #tempMinPD FROM [dbo].[PackingDetails] group by [ProductAutoId],[UnitType]
					delete FROM [dbo].[PackingDetails] WHERE [AutoId] NOT IN (select autoid from #tempMinPD)

					SELECT MIN([AutoId]) as autoid into #tempMinIBC FROM [dbo].[ItemBarcode] group by [ProductAutoId],[UnitAutoId],[Barcode]
					delete FROM [dbo].[ItemBarcode] WHERE [AutoId] NOT IN (select autoid from #tempMinIBC)
					UPDATE ProductMaster set PackingAutoId=3  from ProductMaster where PackingAutoId is null
					DELETE FROM [dbo].[tempProductDetails]



				COMMIT TRANSACTION
			END TRY
			BEGIN CATCH
				ROLLBACK TRAN
				SET @isException = 1
				SET @exceptionMessage = ERROR_MESSAGE()
			END CATCH 	
	END TRY
	BEGIN CATCH
		SET @isException = 1
		SET @exceptionMessage = ERROR_MESSAGE()
	END CATCH
END


GO
