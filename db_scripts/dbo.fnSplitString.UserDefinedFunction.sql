USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[fnSplitString]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnSplitString] 
( 
  @string  varchar(MAX), 
  @delimiter CHAR(1) 
) 
RETURNS @output TABLE(splitdata  varchar(MAX) 
) 
BEGIN 
  DECLARE @start INT, @end INT 
  SELECT @start = 1, @end = CHARINDEX(@delimiter, @string) 
  WHILE @start < LEN(@string) + 1 BEGIN 
      IF @end = 0  
          SET @end = LEN(@string) + 1
     
      INSERT INTO @output (splitdata)  
      VALUES(SUBSTRING(@string, @start, @end - @start)) 
      SET @start = @end + 1 
      SET @end = CHARINDEX(@delimiter, @string, @start)
      
  END 
  RETURN 
  END

GO
