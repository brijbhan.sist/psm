create or alter PROCEDURE [dbo].[ProcinsertData_Cityzipcode_ALL]           
@City varchar(50)=null,
@State varchar(50)=null,
@Zipcode1 varchar(10)=null,
@CityAutoId int =null,
@StateAutoId int =null
AS          
BEGIN         
	declare @sql_query varchar(max)='',@sql_query1 varchar(max)='',@db_name varchar(max)=null,@ZipcodeAutoId int;
	set @db_name=(select [ChildDB_Name] from CompanyDetails)

 IF NOT EXISTS(SELECT [AutoId] FROM [dbo].CityMaster where CityName=trim(@City) and StateId=(select AutoId from State where StateName=trim(@State)))          
 BEGIN 
		insert into  CityMaster(CityName,Status,StateId)values(trim(@City),1,(select AutoId from State where StateName=trim(@State)))
		set @CityAutoId=SCOPE_IDENTITY()
	IF @db_name is not null 
	BEGIN
		set @sql_query='SET IDENTITY_INSERT ['+@db_name+'].[dbo].[CityMaster] ON;
		insert into ['+@db_name+'].[dbo].CityMaster(AutoId,StateId,CityName,Status,MIReferenceID,MILocation)
		select AutoId,StateId,CityName,Status,MIReferenceID,MILocation from [dbo].[CityMaster]
		where AutoId='+convert(varchar(10),@CityAutoId)+'
		SET IDENTITY_INSERT ['+@db_name+'].[dbo].[CityMaster] OFF;' 	
		exec (@sql_query) 
	END         
	
	IF NOT EXISTS(SELECT ZM.AutoId FROM ZipMaster AS ZM 
	INNER JOIN CityMaster AS CM ON CM.AutoId=ZM.CityId
	INNER JOIN State AS SM ON SM.AutoId=CM.StateId
	WHERE  Zipcode=@Zipcode1 AND CityName=@City AND SM.StateName=@State)                                                          
	BEGIN

		insert into ZipMaster(Zipcode,CityId,Status)values(@Zipcode1,@CityAutoId,1)  
		set @ZipcodeAutoId=SCOPE_IDENTITY()
		if @db_name is not null 
		begin   
			set @sql_query1='SET IDENTITY_INSERT ['+@db_name+'].[dbo].[ZipMaster] ON;
			insert into ['+@db_name+'].[dbo].ZipMaster(AutoId,CityId,ZoneId,Zipcode,Status,MIReferenceID,MILocation)
			select AutoId,CityId,ZoneId,Zipcode,Status,MIReferenceID,MILocation from [dbo].[ZipMaster]
			where AutoId='+convert(varchar(10),@ZipcodeAutoId)+'
			SET IDENTITY_INSERT ['+@db_name+'].[dbo].[ZipMaster] OFF;' 
			exec (@sql_query1) 
		END         
	END                  

END 
ELSE
BEGIN
     IF NOT EXISTS(SELECT ZM.AutoId FROM ZipMaster AS ZM 
		INNER JOIN CityMaster AS CM ON CM.AutoId=ZM.CityId
		INNER JOIN State AS SM ON SM.AutoId=CM.StateId
		WHERE  Zipcode=@Zipcode1 AND CityName=@City AND SM.StateName=@State)                                                          
		BEGIN
			set @CityAutoId=(Select AutoId from CityMaster where CityName=trim(@City))
			insert into ZipMaster(Zipcode,CityId,Status)values(@Zipcode1,@CityAutoId,1)  
			set @ZipcodeAutoId=SCOPE_IDENTITY()
			if @db_name is not null 
			begin   
				set @sql_query1='SET IDENTITY_INSERT ['+@db_name+'].[dbo].[ZipMaster] ON;
				insert into ['+@db_name+'].[dbo].ZipMaster(AutoId,CityId,ZoneId,Zipcode,Status,MIReferenceID,MILocation)
				select AutoId,CityId,ZoneId,Zipcode,Status,MIReferenceID,MILocation from [dbo].[ZipMaster]
				where AutoId='+convert(varchar(10),@ZipcodeAutoId)+'
				SET IDENTITY_INSERT ['+@db_name+'].[dbo].[ZipMaster] OFF;' 
				exec (@sql_query1) 
			END         
		END
END
END        