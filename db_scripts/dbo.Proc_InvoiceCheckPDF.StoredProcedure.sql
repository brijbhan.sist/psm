 
ALTER procedure [dbo].[Proc_InvoiceCheckPDF]
@OpCode int=Null,      
@Who int = null,      
@AutoId  int=Null,      
@BrandId VARCHAR(12)=NULL,      
@BrandName VARCHAR(50)=NULL,      
@Description varchar(250)=NULL,      
@Status int=null,      
@PageIndex INT=1,      
@PageSize INT=10,      
@RecordCount INT=null,      
@isException bit out,      
@exceptionMessage varchar(max) out      
AS 
BEGIN      
 BEGIN TRY      
   SET @isException=0      
   SET @exceptionMessage='success' 

   If @OpCode = 41
   begin
   select top 2 AutoId from OrderMaster where IsAttached is null
   end

   END TRY      
   BEGIN CATCH          
  SET @isException=1      
  SET @exceptionMessage='Oops! Something went wrong.Please try later.'
 END CATCH      
END 
GO
