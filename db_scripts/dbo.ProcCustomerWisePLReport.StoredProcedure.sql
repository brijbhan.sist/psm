USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcProductWisePLReport]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                   
alter PROCEDURE [dbo].[ProcCustomerWisePLReport]                        
@Opcode INT=NULL,                    
@SalesPerson int=null,                    
@CustomerAutoId INT=NULL,                    
@ProductAutoId INT=NULL,                    
@CategoryAutoId int=null,                    
@SubCategoryAutoId int=null,                    
@FromDate date =NULL,                    
@ToDate date =NULL,                    
@PageIndex INT=1,                    
@PageSize INT=10,                    
@RecordCount INT=null,                    
@isException bit out,                    
@exceptionMessage varchar(max) out                            
AS                                    
BEGIN                       
 BEGIN TRY                    
  Set @isException=0                    
  Set @exceptionMessage='Success'                    
  IF @Opcode=41                    
  BEGIN  
  select
  (
    SELECT AutoId as CAID, CategoryName as CAN FROM CategoryMaster order by CAN asc 
	for json path
  ) as Category,
  (
  Select em.AutoId as SID ,(em.FirstName + ' ' + em.LastName) as SP from EmployeeMaster em                                           
  inner join EmployeeTypeMaster etm on etm.AutoId = em.EmpType                                          
  where EmpType = 2 and Status=1 order by SP ASC 
  for json path
  ) as SalesPerson
   for JSON path                
  END                    
                
 ELSE IF @Opcode=42        
  BEGIN        
  SELECT AutoId as SCID,SubcategoryName as SCN from SubCategoryMaster    
  where (isnull(@CategoryAutoId,0)=0 or CategoryAutoId=@CategoryAutoId )and Status=1     
  order by SCN  ASC  
  for json path
  END          
      
       
  ELSE IF @OPCODE=43        
  BEGIN        
     SELECT  AutoId as PID,CONVERT(VARCHAR(10),ProductId )+' '+ ProductName AS PN FROM ProductMaster        
     WHERE         
     (isnull(@SubCategoryAutoId,0)=0 or SubcategoryAutoId=@SubCategoryAutoId )and ProductStatus=1 order by PN ASC
	 for json path    
  END                             
   
 ELSE if(@Opcode = 44)                  
   begin                  
    SELECT AutoId as CUID,CustomerId + ' ' + CustomerName as CUN FROM CustomerMaster  where (SalesPersonAutoId=@SalesPerson or      
  ISNULL(@SalesPerson,0)=0) order by replace(CustomerName,' ','') ASC 
  for json path
   end            
                   
 ELSE  if @Opcode = 45                  
   begin                  
   select ROW_NUMBER() over(order by FirstOrderDate) as RowNumber, t.*,                  
 case when CostPrice=0 then Profit*100 else  (Profit*100/CostPrice)end as ProfitPer                  
  into #Result44 from (                    
                  
   select cm.autoid,cm.CustomerId,cm.CustomerName,FORMAT(convert(date,min(OrderDate)),'MM/dd/yyyy') as FirstOrderDate,format(convert(date,max(OrderDate)),'MM/dd/yyyy') as LastOrderDate,                  
   (select count(*) from OrderMaster as om2 where om2.CustomerAutoId = cm.AutoId and om2.Status=11) as totalNoOfOrders,                  
   cast(SUM(QtyPerUnit*(QtyDel/cast(QtyPerUnit as decimal(10,2)))) as decimal(10,2)) AS TotalQty,                  
 case when IsExchange>0 then 0 else  sum(NetPrice) end SoldPrice,                  
 case when IsExchange>0 then 0 else cast(Sum(Del_CostPrice*(QtyDel/cast(QtyPerUnit as decimal(10,2)))) as decimal(10,2)) end CostPrice,                      
 case when IsExchange>0 then 0 else  (sum(NetPrice)-Sum(Del_CostPrice*(QtyDel/cast(QtyPerUnit as decimal(10,2))))) end Profit             
   --sum(NetPrice) as SoldPrice,                  
   --cast(Sum(Del_CostPrice*(QtyDel/cast(QtyPerUnit as decimal(10,2)))) as decimal(10,2)) as CostPrice,                      
   --(sum(NetPrice)-Sum(Del_CostPrice*(QtyDel/cast(QtyPerUnit as decimal(10,2))))) as Profit                   
   from  [dbo].[Delivered_Order_Items]   as dim                        
   inner join OrderMaster as OM on OM.AutoId=dim.OrderAutoId                      
   inner join ProductMaster as pm on pm.AutoId=dim.ProductAutoId                      
   inner join CustomerMaster as CM on CM.AutoId=OM.CustomerAutoId                       
   WHERE                       
   om.Status=11 and                  
   (ISNULL(@FromDate,'')='' OR ISNULL(@ToDate,'') = '' OR                                     
   (CONVERT(date,OM.OrderDate) BETWEEN CONVERT(date,@fromDate) AND CONVERT(date,@ToDate)))                      
   AND        
   (ISNULL(@CustomerAutoId,0)=0 OR OM.CustomerAutoId=@CustomerAutoId)                      
   AND                      
   (ISNULL(@ProductAutoId,0)=0 OR PM.AutoId=@ProductAutoId)                      
   AND              
   (ISNULL(@SalesPerson,0)=0 OR CM.SalesPersonAutoId=@SalesPerson)                      
   AND                      
   (ISNULL(@CategoryAutoId,0)=0 OR PM.CategoryAutoId=@CategoryAutoId)                      
   AND                      
   (ISNULL(@SubCategoryAutoId,0)=0 OR PM.SubcategoryAutoId=@SubCategoryAutoId)                      
   and ISNULL(QtyDel,0)>0           
                   
  group by cm.AutoId, cm.CustomerId,cm.CustomerName,om.CustomerAutoId ,IsExchange                  
   ) as t                   
                    
   inner join CustomerMaster as CM on CM.AutoId=t.AutoId                  
   order by cm.AutoId                  
                  
   SELECT * FROM #Result44                  
  WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))                   
  SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize = 0 then COUNT(RowNumber) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Result44                  
            
	SELECT isnull(SUM(TotalQty),0) as TotalQty,isnull(SUM(totalNoOfOrders),0) as totalNoOfOrders,isnull(SUM(SoldPrice),0.00) as SoldPrice,isnull(SUM(CostPrice),0.00) as CostPrice,isnull(SUM(Profit),0.00) as Profit                
	,convert (decimal(18,2),(isnull(SUM(Profit),0.00)/isnull(SUM(CostPrice),0.00))*100) as ProfitPer FROM #Result44       
	
	select FORMAT(getdate(),'MM/dd/yyyy hh:mm tt') as PrintDate
end                  
 END TRY                    
 BEGIN CATCH                    
  Set @isException=1                    
  Set @exceptionMessage=ERROR_MESSAGE()                    
 END CATCH                                 
END 
GO
