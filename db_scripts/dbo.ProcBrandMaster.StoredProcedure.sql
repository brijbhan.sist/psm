USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcBrandMaster]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
alter procedure [dbo].[ProcBrandMaster]      
@OpCode int=Null,      
@Who int = null,      
@AutoId  int=Null,      
@BrandId VARCHAR(12)=NULL,      
@BrandName VARCHAR(50)=NULL,      
@Description varchar(250)=NULL,      
@Status int=null,      
@PageIndex INT=1,      
@PageSize INT=10,      
@RecordCount INT=null,      
@isException bit out,      
@exceptionMessage varchar(max) out      
AS      
BEGIN      
 BEGIN TRY      
   SET @isException=0      
   SET @exceptionMessage='success'      
      
   IF @OpCode=11      
   BEGIN      
    IF exists(SELECT BrandName FROM BrandMaster WHERE BrandName = TRIM(@BrandName))      
    BEGIN      
     set @isException=1      
     set @exceptionMessage='Brand already exists.'      
    END      
    ELSE      
    BEGIN      
     BEGIN TRY      
     BEGIN TRAN      
     SET @BrandId=(SELECT dbo.SequenceCodeGenerator('BrandId'))      
     INSERT INTO [dbo].[BrandMaster]([BrandId], [BrandName], [Description], [Status],[CreatedBy],[CreatedOn],[UpdatedBy],[UpdatedOn])      
     VALUES (@BrandId, TRIM(@BrandName),@Description,@Status,@Who,GETDATE(),@Who,GETDATE())      
     Update SequenceCodeGeneratorMaster SET currentSequence=currentSequence+1 WHERE SequenceCode='BrandId'      
     IF(DB_NAME()='psmnj.a1whm.com')      
     BEGIN      
      EXEC [ProcBrandMaster_ALL] @BrandId=@BrandId      
     END      
     COMMIT TRANSACTION      
     END TRY       
     BEGIN CATCH      
     ROLLBACK TRAN      
     SET @isException=1      
     SET @exceptionMessage=ERROR_MESSAGE()
     END CATCH      
    END      
   END      
   IF @Opcode=21      
   BEGIN      
    IF exists(SELECT BrandName FROM BrandMaster WHERE BrandName = TRIM(@BrandName) and BrandId != @BrandId)      
    BEGIN      
     SET @isException=1      
     SET @exceptionMessage='Brand already exists.'      
    END      
    ELSE      
    BEGIN      
     BEGIN TRY      
     BEGIN TRAN      
      UPDATE BrandMaster SET BrandName = TRIM(@BrandName), Description = @Description, Status = @Status, UpdatedBy = @Who, UpdatedOn = GETDATE()      
      WHERE AutoId = @AutoId      
      
      IF(DB_NAME()='psmnj.a1whm.com')      
      BEGIN      
       EXEC [ProcBrandMaster_ALL] @BrandId=@BrandId      
      END      
     COMMIT TRANSACTION      
     END TRY       
     BEGIN CATCH      
     ROLLBACK TRAN      
     SET @isException=1      
     SET @exceptionMessage='Oops! Something went wrong.Please try later.'  
     END CATCH      
    END      
   END      
   IF @Opcode=31      
   BEGIN   
        
		IF NOT EXISTS(SELECT  1 FROM ProductMaster WHERE BrandAutoId = @AutoId)
		BEGIN
			BEGIN TRY      
			BEGIN TRAN 
				SET @BrandId=(SELECT BrandId FROM BrandMaster WHERE AutoId = @AutoId)      
				DELETE FROM BrandMaster WHERE AutoId = @AutoId      
			IF(DB_NAME()='psmnj.a1whm.com')      
			BEGIN      
				EXEC [ProcBrandMaster_ALL_Delete] @BrandId=@BrandId      
			END      
			COMMIT TRANSACTION      
			END TRY       
			BEGIN CATCH      
			ROLLBACK TRAN      
				SET @isException=1      
				SET @exceptionMessage='Oops! Something went wrong.Please try later.'  
			END CATCH   
			END
		ELSE 
		BEGIN
			SET @isException=1      
			SET @exceptionMessage='Brand already assigned to product.' 
		END   
   END      
   IF @OpCode=41      
   BEGIN       
   SELECT ROW_NUMBER() OVER(ORDER BY BrandId desc) AS RowNumber, * INTO #Results from                        
		( 
     SELECT Bm.AutoId, BM.BrandId, BM.BrandName, BM.Description, SM.StatusType AS Status FROM  BrandMaster AS BM       
     inner join StatusMaster AS SM ON SM.AutoId=BM.Status and SM.Category is NULL       
     WHERE (@BrandId is null or @BrandId='' or BrandId like '%'+ @BrandId +'%')      
     and (@BrandName is null or @BrandName='' or BrandName like '%'+ @BrandName +'%')      
     and (@Status=2 or Status=@Status)  
	 ) as t order by BrandId desc
	 SELECT COUNT(RowNumber) AS RecordCount,case when @PageSize=0 then COUNT(AutoId) else @PageSize end AS PageSize,@PageIndex as PageIndex FROM #Results             
	    SELECT * FROM #Results                                      
		WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))                                                                        

   END       
   IF @OpCode=42      
      BEGIN      
    SELECT AutoId, BrandId, BrandName, Description,Status FROM BrandMaster WHERE AutoId=@AutoId      
   END      
 END TRY      
 BEGIN CATCH          
  SET @isException=1      
  SET @exceptionMessage='Oops! Something went wrong.Please try later.'
 END CATCH      
END 
GO
