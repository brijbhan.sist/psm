USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcCategoryMaster_ALL_Delete]    Script Date: 08-09-2020 06:02:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[ProcCategoryMaster_ALL_Delete]    
@CategoryId  VARCHAR(50)=Null    
AS    
BEGIN     
  
	DECLARE @CategoryAutoId INT= (SELECT AutoId FROM [psmct.a1whm.com].[dbo].[CategoryMaster] WHERE CategoryId=@CategoryId)  
	IF NOT EXISTS(SELECT * FROM [psmct.a1whm.com].[dbo].[SubCategoryMaster] WHERE CategoryAutoId=@CategoryAutoId)    
	BEGIN    
		DELETE FROM [psmct.a1whm.com].[dbo].[CategoryMaster] WHERE CategoryId=@CategoryId  
	END    
 
	SET @CategoryAutoId= (SELECT AutoId FROM [psmpa.a1whm.com].[dbo].[CategoryMaster] WHERE CategoryId=@CategoryId)  
	IF NOT EXISTS(SELECT * FROM [psmpa.a1whm.com].[dbo].[SubCategoryMaster] WHERE CategoryAutoId=@CategoryAutoId)    
	BEGIN    
		DELETE FROM [psmpa.a1whm.com].[dbo].[CategoryMaster] WHERE CategoryId=@CategoryId  
	END 

	SET @CategoryAutoId= (SELECT AutoId FROM [psmnpa.a1whm.com].[dbo].[CategoryMaster] WHERE CategoryId=@CategoryId)  
	IF NOT EXISTS(SELECT * FROM [psmnpa.a1whm.com].[dbo].[SubCategoryMaster] WHERE CategoryAutoId=@CategoryAutoId)    
	BEGIN    
		DELETE FROM [psmnpa.a1whm.com].[dbo].[CategoryMaster] WHERE CategoryId=@CategoryId  
	END

	SET @CategoryAutoId= (SELECT AutoId FROM [psmwpa.a1whm.com].[dbo].[CategoryMaster] WHERE CategoryId=@CategoryId)  
	IF NOT EXISTS(SELECT * FROM [psmwpa.a1whm.com].[dbo].[SubCategoryMaster] WHERE CategoryAutoId=@CategoryAutoId)    
	BEGIN    
		DELETE FROM [psmwpa.a1whm.com].[dbo].[CategoryMaster] WHERE CategoryId=@CategoryId  
	END

	SET @CategoryAutoId= (SELECT AutoId FROM [psmnj.a1whm.com].[dbo].[CategoryMaster] WHERE CategoryId=@CategoryId)  
	IF NOT EXISTS(SELECT * FROM [psmnj.a1whm.com].[dbo].[SubCategoryMaster] WHERE CategoryAutoId=@CategoryAutoId)    
	BEGIN    
		DELETE FROM [psmnj.a1whm.com].[dbo].[CategoryMaster] WHERE CategoryId=@CategoryId  
	END

	SET @CategoryAutoId= (SELECT AutoId FROM [psmny.a1whm.com].[dbo].[CategoryMaster] WHERE CategoryId=@CategoryId)  
	IF NOT EXISTS(SELECT * FROM [psmny.a1whm.com].[dbo].[SubCategoryMaster] WHERE CategoryAutoId=@CategoryAutoId)    
	BEGIN    
		DELETE FROM [psmny.a1whm.com].[dbo].[CategoryMaster] WHERE CategoryId=@CategoryId  
	END

	SET @CategoryAutoId= (SELECT AutoId FROM [psmnj.easywhm.com].[dbo].[CategoryMaster] WHERE CategoryId=@CategoryId)  
	IF NOT EXISTS(SELECT * FROM [psmnj.easywhm.com].[dbo].[SubCategoryMaster] WHERE CategoryAutoId=@CategoryAutoId)    
	BEGIN    
		DELETE FROM [psmnj.easywhm.com].[dbo].[CategoryMaster] WHERE CategoryId=@CategoryId  
	END

	SET @CategoryAutoId= (SELECT AutoId FROM [psmpa.easywhm.com].[dbo].[CategoryMaster] WHERE CategoryId=@CategoryId)  
	IF NOT EXISTS(SELECT * FROM [psmpa.easywhm.com].[dbo].[SubCategoryMaster] WHERE CategoryAutoId=@CategoryAutoId)    
	BEGIN    
		DELETE FROM [psmpa.easywhm.com].[dbo].[CategoryMaster] WHERE CategoryId=@CategoryId  
	END

	SET @CategoryAutoId= (SELECT AutoId FROM [psmnpa.easywhm.com].[dbo].[CategoryMaster] WHERE CategoryId=@CategoryId)  
	IF NOT EXISTS(SELECT * FROM [psmnpa.easywhm.com].[dbo].[SubCategoryMaster] WHERE CategoryAutoId=@CategoryAutoId)    
	BEGIN    
		DELETE FROM [psmnpa.easywhm.com].[dbo].[CategoryMaster] WHERE CategoryId=@CategoryId  
	END

	SET @CategoryAutoId= (SELECT AutoId FROM [psmwpa.easywhm.com].[dbo].[CategoryMaster] WHERE CategoryId=@CategoryId)  
	IF NOT EXISTS(SELECT * FROM [psmwpa.easywhm.com].[dbo].[SubCategoryMaster] WHERE CategoryAutoId=@CategoryAutoId)    
	BEGIN    
		DELETE FROM [psmwpa.easywhm.com].[dbo].[CategoryMaster] WHERE CategoryId=@CategoryId  
	END

	SET @CategoryAutoId= (SELECT AutoId FROM [psmct.easywhm.com].[dbo].[CategoryMaster] WHERE CategoryId=@CategoryId)  
	IF NOT EXISTS(SELECT * FROM [psmct.easywhm.com].[dbo].[SubCategoryMaster] WHERE CategoryAutoId=@CategoryAutoId)    
	BEGIN    
		DELETE FROM [psmct.easywhm.com].[dbo].[CategoryMaster] WHERE CategoryId=@CategoryId  
	END

	SET @CategoryAutoId= (SELECT AutoId FROM [psmny.easywhm.com].[dbo].[CategoryMaster] WHERE CategoryId=@CategoryId)  
	IF NOT EXISTS(SELECT * FROM [psmny.easywhm.com].[dbo].[SubCategoryMaster] WHERE CategoryAutoId=@CategoryAutoId)    
	BEGIN    
		DELETE FROM [psmny.easywhm.com].[dbo].[CategoryMaster] WHERE CategoryId=@CategoryId  
	END
END
