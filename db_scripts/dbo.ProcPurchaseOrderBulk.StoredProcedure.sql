CREATE OR Alter PROCEDURE [dbo].[ProcPurchaseOrderBulk]                                                                                                                                     
 @Opcode INT=NULL,     
 @POType INT=NULL,
 @POAutoId INT=NULL,              
 @ProductAutoId  int =null, 
 @ProductId  int =null, 
 @OrderNo nvarchar(15)=null,               
 @EmpId int=null,                                                                                                                                                                                                                                              
 @DraftAutoId int=null out,   
 @DeliveryDate DATETIME=NULL,      
 @RecieveStockRemark varchar(max)=null,  
 @VendorName varchar(100)=null,                                                                                                                                      
 @VenderAutoId INT=NULL,               
 @NoofItems int=null,                                                                                                                                      
 @UnitAutoId INT=NULL,   
 @QtyPerUnit int=null,                                                                                                                                                                   
 @OrderStatus INT=NULL,      
 @PoStatus varchar(10)=null,                                                                                                      
 @PORemarks VARCHAR(500)=NULL,           
 @Barcode varchar(50)=NULL,        
 @FromDate DateTime=null,        
 @ToDate DateTime=null,          
 @ReqQty int=null, 
 @RefillType int=null,
 @dtbulkUnit DT_dtPOMManagePrice readonly,                                                                                            
 @TableValue xml=null,                                                                   
 @PageIndex INT = 1,                                                                                                                  
 @PageSize INT = 10,                                                                                                                                                                                                                                           
 @isException bit out,                             
 @exceptionMessage varchar(max) out                                                                        
AS                                                                                                                                      
 BEGIN                                                                                                               
  BEGIN TRY                                                                                                                              
  Set @isException=0                                                                                                             
  Set @exceptionMessage='Success'          
    declare @Location varchar(50)=Replace(DB_Name(),'.a1whm.com','')        
    declare @CurrentLocationId int=(select AutoId from LocationMaster where Location=@Location )       
    declare @SqlStr nvarchar(max)=null      
    declare @vender int=null,@PNo varchar(20)=null,@LocationID int=null,@Location1 varchar(20)=null       
                                                                                        
  if @Opcode=40              
  begin              
  BEGIN TRY                                                                                                                                      
  BEGIN TRAN         
  if(@POAutoId is null or @POAutoId=0)          
  begin           
    declare @CustomerAutoId int=(select CustomerAutoId from         
    [dbo].[fn_GetCustomerAndSalesPerson](@VenderAutoId,@CurrentLocationId))        
        
    if(@CustomerAutoId is not null)        
		begin         
			SET @OrderNo = (SELECT DBO.SequenceCodeGenerator('PurchaseOrder'))                
     
			insert into [dbo].[PurchaseOrderMaster]([PONo],[PODate],[VenderAutoId],[Status],[NoofItems],[PORemarks],
			[CreatedBy],[CreatedOn],[StockStatus],VendorType)values(@OrderNo,GETDATE(),
			@VenderAutoId,1,@NoofItems,@PORemarks,@EmpId,GETDATE(),'Pending',1)
  
   
			SET @POAutoId = (SELECT SCOPE_IDENTITY())         
                
			Insert into [dbo].[PurchaseProductsMaster]([ProductAutoId],[Unit],[QtyPerUnit],[Qty],[TotalPieces],[POAutoId])          
			select tr.td.value('ProductAutoId[1]','int') as ProductAutoId,tr.td.value('Unit[1]','int') as Unit,tr.td.value('QtyPerUnit[1]','int') as QtyPerUnit,tr.td.value('Qty[1]','int') as Qty,            
			tr.td.value('TotalPieces[1]','int') as TotalPieces,@POAutoId  from  @TableValue.nodes('/Xml') tr(td)                 
			UPDATE SequenceCodeGeneratorMaster SET currentSequence = currentSequence + 1 WHERE SequenceCode='PurchaseOrder'   
    
			delete DraftPurchaseOrderMaster where DraftAutoId=@DraftAutoId   
			delete DraftPurchaseProductsMaster where DraftAutoId=@DraftAutoId        
        
			exec [dbo].[Proc_GenerateOrder_all]        
			@VenderAutoId=@VenderAutoId,        
			@POAutoId=@POAutoId,        
			@Remark=@PORemarks,        
			@PONo=@OrderNo        
        
			Set @isException=0                                                                                                             
			Set @exceptionMessage='Success'        
		End        
		else         
		begin         
			Set @isException=1                                   
			Set @exceptionMessage='NotExist'        
		end        
		end        
  else        
   begin        
	if((Select Status from PurchaseOrderMaster where AutoId=@POAutoId)=1)        
	begin        
		Update [dbo].[PurchaseOrderMaster] set VenderAutoId=@VenderAutoId ,PORemarks=@PORemarks,        
		NoofItems=@NoofItems ,UpdatedBy=@EmpId ,UpdatedOn=GetDate() Where AutoId=@POAutoId        
        
		Delete [dbo].[PurchaseProductsMaster] where POAutoId=@POAutoId         
        
		Insert into [dbo].[PurchaseProductsMaster]([ProductAutoId],[Unit],[QtyPerUnit],[Qty],[TotalPieces],[POAutoId])          
		select tr.td.value('ProductAutoId[1]','int') as ProductAutoId,tr.td.value('Unit[1]','int') as Unit,tr.td.value('QtyPerUnit[1]','int') as QtyPerUnit,tr.td.value('Qty[1]','int') as Qty,            
		tr.td.value('TotalPieces[1]','int') as TotalPieces,@POAutoId  from  @TableValue.nodes('/Xml') tr(td)          
		Set @OrderNo=(Select PONo from PurchaseOrderMaster where AutoId=@POAutoId)        
		
		exec [dbo].[Proc_UpdateOrder_all]        
		@VenderAutoId=@VenderAutoId,        
		@POAutoId=@POAutoId,        
		@Remark=@PORemarks,        
		@PONo=@OrderNo        
           
		Set @isException=0                                                                                                             
		Set @exceptionMessage='Updated' 
		       
    end        
	else        
		begin        
			Set @isException=1                                                                                                             
			Set @exceptionMessage='ChangeStatus'        
        
	end        
 end        
                                    
  COMMIT TRANSACTION                                                                                                                                      
 END TRY                                                                                                                                      
  BEGIN CATCH                                                                                                                  
 ROLLBACK TRAN                                                                                                           
 Set @isException=1                                                                                     
 Set @exceptionMessage=ERROR_MESSAGE()--'Oops! Something went wrong.Please try later.'                                                                                 
 End Catch      
  End                                                                                                                                  
  else If @Opcode=42                                                                                                         
 BEGIN                
  select VendorName,AutoId from VendorMaster where LocationTypeAutoId=1  and Status=1  ORDER BY  VendorName ASC
  SELECT [AutoId],convert(varchar(10),[ProductId]) + '--' + [ProductName] as [ProductName],ISNULL(MLQty,0) AS MLQty, isnull(WeightOz,0) as WeightOz     ,
  CurrentStock
  FROM [dbo].[ProductMaster] as pm where ProductStatus=1                                                                 
  and (select count(1) from PackingDetails where  ProductAutoId=pm.AutoId)>0                                                 
  order by convert(varchar(10),[ProductId]) + '--' + [ProductName] asc                                             
 end               
                
 ELSE IF @Opcode=41                                                                                                                      
 BEGIN 
	Declare @DefPacking varchar(20)
	SET @DefPacking=(Select um.UnitType from ProductMaster as PM
	Inner Join PackingDetails as PD on pm.AutoId=pd.ProductAutoId And PM.PackingAutoId=PD.UnitType
	Inner Join UnitMaster as Um on um.AutoId=pd.UnitType
	where pm.AutoId=@ProductAutoId                                                                                   
	)
	SELECT UM.AutoId AS AutoId, UM.[UnitType] AS UnitType, [Qty],ISNULL(EligibleforFree,0) as EligibleforFree
	FROM [dbo].[PackingDetails] AS PD                                                                                           
	INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = PD.UnitType WHERE PD.[ProductAutoId] = @ProductAutoId  ORDER BY UM.[UnitType] ASC                                                                                            
	SELECT PackingAutoId AS AutoId,@DefPacking as DefPacking FROM ProductMaster  WHERE  AutoId=@ProductAutoId                                          
 END                
                
 ELSE IF @Opcode=43                                                                                                                                      
 BEGIN 
	--DECLARE @ParmDefinition NVARCHAR(500);  
	--DECLARE @max_title VARCHAR(30)

	--SET @LocationID=(Select LocationAutoId From VendorMaster where AutoId=@VenderAutoId)
	--SET @Location=(Select Location FROM LocationMaster where AutoId=@LocationID)
 
	--SET @SqlStr = N'SELECT @Id=IBC.AutoId FROM [dbo].[ItemBarcode] as IBC
	--INNER JOIN ProductMaster as PM on IBC.ProductAutoId=PM.AutoId
	--INNER JOIN ['+@Location+'.a1whm.com].[dbo].[ProductMaster] as PMM on PM.ProductId=PMM.ProductId
	--WHERE IBC.Barcode='''+@Barcode+''' AND PM.ProductStatus=1 AND PMM.ProductStatus=1';  
	--SET @ParmDefinition = N'@Id VARCHAR(30) OUTPUT';  
 
	--EXECUTE sp_executesql @SqlStr, @ParmDefinition, @Id=@max_title OUTPUT; 

	IF NOT EXISTS(SELECT AutoId FROM [dbo].[ItemBarcode] WHERE [Barcode]=@Barcode)    
	BEGIN
		SET @isException=1
		SET @exceptionMessage='BarcodeDoesNotExist'
	END
	ELSE IF NOT EXISTS(select IBC.AutoId from ItemBarcode as IBC INNER JOIN ProductMaster as PM ON PM.AutoId=IBC.ProductAutoId where Barcode=@Barcode AND PM.ProductStatus=1) 
	BEGIN
		 SET @isException=1
		 SET @exceptionMessage='ProductisInactive'
	END
	ELSE 
	  BEGIN                                                                                                                                      
		  SET @ProductAutoId=(SELECT [ProductAutoId] FROM [dbo].[ItemBarcode] WHERE [Barcode]=@Barcode)                                                            
		  SET @UnitAutoId=(SELECT UnitAutoId FROM [dbo].[ItemBarcode] WHERE [Barcode]=@Barcode) 
  
		  SET @DefPacking=(Select um.UnitType from ProductMaster as PM
		  Inner Join PackingDetails as PD on pm.AutoId=pd.ProductAutoId And PM.PackingAutoId=PD.UnitType
		  Inner Join UnitMaster as Um on um.AutoId=pd.UnitType
		  where pm.AutoId=@ProductAutoId)
  
		  SELECT PM.AutoId as ProductAutoId,PM.ProductId,PM.ProductName,PD.Qty as UnitQty,PD.UnitType,  
		  ISNULL((SELECT TOP 1 UnitType +  ' ('+ CONVERT(VARCHAR(20),PD.QTY)+' pcs)' from UnitMaster AS UM WHERE UM.AutoId=PD.UnitType),'') AS UnitName,                                                                                              
		  PM.[Stock]/Qty  as Stock,CurrentStock,@DefPacking as DefPacking,PM.VendorAutoId                                                                                 
		  FROM [dbo].[PackingDetails] As PD                                                                                                                                       
		  INNER JOIN [dbo].[ProductMaster] AS PM ON PD.[ProductAutoId]=PM.[AutoId]  
		  WHERE PD.[ProductAutoId] = @ProductAutoId and PD.[UnitType] = @UnitAutoId  
  
		  SELECT UM.AutoId AS AutoId, UM.[UnitType] AS UnitType, [Qty],ISNULL(EligibleforFree,0) as EligibleforFree 
		  into #TEMP11 FROM [dbo].[PackingDetails] AS PD                                                                                           
		  INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = PD.UnitType WHERE PD.[ProductAutoId] = @ProductAutoId 
		  
		  Select * From #TEMP11  

		  Select AutoId,VendorName from VendorMaster  where Status=1
		  --set @QtyPerUnit=(Select TOP 1 Qty from #TEMP11)  
  
		 --  IF ISNULL(@DraftAutoId,0)=0                                                                                                                                              
		 --  BEGIN  
			--   INSERT INTO   [dbo].[DraftPurchaseOrderMaster] ([PODate] , [VenderAutoId] , [PORemarks] , [CreatedBy] , [CreatedOn]) 
			--	VALUES   ( GetDate() , @VenderAutoId , @PORemarks , @EmpId , GetDate() )  
			--   Set @DraftAutoId = SCOPE_IDENTITY() 
		 --  End 
		 --  Else
		 --  begin  
			--update [dbo].[DraftPurchaseOrderMaster] set VenderAutoId=@VenderAutoId, PORemarks=@PORemarks  where DraftAutoId=@DraftAutoId  
		 --  End  
  
		--IF NOT EXISTS(SELECT * FROM [DraftPurchaseProductsMaster] WHERE DraftAutoId= @DraftAutoId AND ProductAutoId=@ProductAutoId)   
		--Begin  
		--	INSERT INTO  [dbo].[DraftPurchaseProductsMaster] ([ProductAutoId] , [Unit] , [QtyPerUnit] , [Qty] , [DraftAutoId]) 
		--	VALUES  ( @ProductAutoId , @UnitAutoId , @QtyPerUnit , @ReqQty , @DraftAutoId )
		--End  
		--Else  
		--Begin  
		--	Update [dbo].[DraftPurchaseProductsMaster] Set [Qty] = [Qty] + @ReqQty where DraftAutoId=@DraftAutoId AND ProductAutoId=@ProductAutoId AND Unit=@UnitAutoId  
		--End  
			  --Select @DraftAutoId AS DraftAutoId   
	  End                        
 END   
                                                                                                                                               
 ELSE IF @Opcode=44                            
 BEGIN                                                                                                                                      
  SELECT UM.AutoId AS AutoId, UM.[UnitType] AS UnitType,[Qty],ISNULL(EligibleforFree,0) as EligibleforFree FROM [dbo].[PackingDetails] AS PD                                        
  INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = PD.UnitType WHERE PD.[ProductAutoId] = @ProductAutoId                                                                                              
  SELECT PackingAutoId AS AutoId FROM ProductMaster  WHERE  AutoId=@ProductAutoId                                          
 END                                         
 ELSE IF @Opcode=45                                                                                                                      
 BEGIN        
  SELECT ROW_NUMBER() OVER(ORDER BY AutoId DESC) AS RowNumber, * INTO #Results FROM      
 (                                                                              
  Select PO.AutoId,format(PO.PODate,'MM/dd/yyyy hh:mm tt')as PODate,vm.VendorName,sm.StatusType,sm.ColorCode,NoofItems,isnull(PORemarks,'No Remark') as PORemarks,        
  isnull(format(PO.DeliveryDate,'MM/dd/yyyy'),'N/A')as DeliveryDate,PONo,StockStatus as POStatus,TotalOrderAmount,TotalNoOfItem,OrderNo from [dbo].[PurchaseOrderMaster] as Po          
  left join VendorMaster vm on vm.AutoId=Po.VenderAutoId           
  left join StatusMaster sm on sm.AutoId=Po.Status and sm.category='OrderMaster'        
  where Po.VendorType=1 and (@OrderNo is null or @OrderNo ='' or PO.PONo like '%' + @OrderNo + '%')                                                                                                          
  and (@FromDate is null or @FromDate = '' or @Todate is null or @Todate = '' or (convert(date,PO.PODate) between Convert(date,@FromDate) and Convert(date,@Todate)))                                                                                          
  and (@PoStatus is null or @PoStatus='' or po.StockStatus=@PoStatus)                
  and (@OrderStatus is null or @OrderStatus=0 or po.Status=@OrderStatus)        
  and (@VenderAutoId is null or @VenderAutoId=0 or po.VenderAutoId=@VenderAutoId)          
 ) as t order by AutoId  DESC      
      
 SELECT COUNT(AutoId) AS RecordCount, case when @PageSize=0 then COUNT(AutoId) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Results      
 SELECT * FROM #Results      
 WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))        
 END        
         
 ELSE IF @Opcode=46                                                                                                                      
 BEGIN       
	declare @Status int=(select status from PurchaseOrderMaster where AutoId=@POAutoId)
		  if(@Status=1)      
		  begin                                                                                                                                     
				Select Po.AutoId,PONo,format(PODate,'MM/dd/yyyy')as PODate,format(DeliveryDate,'MM/dd/yyyy') as DeliveryDate,
				VenderAutoId,vm.VendorName,(sm.StatusType) as Status,TotalOrderAmount       
				,sm.AutoId as StatusAutoId,NoofItems,PORemarks,StockStatus,RecieveStockRemark,po.OrderNo from [dbo].[PurchaseOrderMaster]as po       
				inner join StatusMaster sm on sm.AutoId=po.Status and  sm.category='OrderMaster'      
				inner join VendorMaster vm on vm.AutoId=po.VenderAutoId 
				where po.AutoId=@POAutoId      
      
				Select pp.*,pm.ProductName,pm.ProductId,um.UnitType,pm.CurrentStock,t.UnitType as DefUnit from PurchaseProductsMaster as pp        
				inner join ProductMaster pm on pm.AutoId=pp.ProductAutoId        
				inner join UnitMaster um on um.AutoId=pp.Unit 
				left join (
				Select ums.UnitType,PMS.AutoId from ProductMaster as PMS
				Inner Join PackingDetails as PDS on pms.AutoId=pds.ProductAutoId And PMs.PackingAutoId=PDs.UnitType
				Inner Join UnitMaster as UmS on ums.AutoId=pds.UnitType
				where pms.AutoId in (Select ProductAutoId FROM PurchaseProductsMaster where POAutoId=@POAutoId)    

				) as t on t.AutoId=pm.AutoId
				where pp.POAutoId=@POAutoId 
      
				SELECT PD.ProductAutoId,UM.AutoId AS AutoId, UM.[UnitType] AS UnitType, PD.[Qty],ISNULL(EligibleforFree,0) 
				as EligibleforFree FROM [dbo].[PackingDetails] AS PD                                                                                           
				INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = PD.UnitType      
				WHERE PD.[ProductAutoId] in (select ProductAutoId from PurchaseProductsMaster where POAutoId=@POAutoId)  
			  End      
	  else if(@Status=11)      
	  begin      
			Select @vender=VenderAutoId,@PNo=PONo from PurchaseOrderMaster where AutoId=@POAutoId      
			select @LocationID=LocationAutoId from VendorMaster where AutoId=@vender      
			select @Location1=Location from LocationMaster where AutoId=@LocationID      
      
			Select Po.AutoId,PONo,format(PODate,'MM/dd/yyyy')as PODate,format(DeliveryDate,'MM/dd/yyyy') as DeliveryDate,VenderAutoId,      
			vm.VendorName,(sm.StatusType) as Status,TotalOrderAmount,        
			sm.AutoId as StatusAutoId,NoofItems,PORemarks,StockStatus,RecieveStockRemark,po.OrderNo from [dbo].[PurchaseOrderMaster]as po       
			inner join StatusMaster sm on sm.AutoId=po.Status and  sm.category='OrderMaster'      
			inner join VendorMaster vm on vm.AutoId=po.VenderAutoId        
			where po.AutoId=@POAutoId    
      
		     set @SqlStr='
			select ISNULL(t.ProductId,tl.ProductId) as ProductId,ISNULL(t.ProductName,tl.ProductName) as ProductName,
			t.UnitType,isnull(t.Qty,0) as Qty,isnull(t.TotalPieces,0) as TotalPieces,t.Unit,isnull(tl.L_ProductAutoId,0) as  L_ProductAutoId,  
			isnull(t.QtyPerUnit,0) as QtyPerUnit,tl.L_UnitType,isnull(tl.L_Qty,0) as L_Qty,isnull(tl.L_QtyPerUnit,0) as L_QtyPerUnit,
			tl.L_Unit, isnull(pd.CostPrice,0.00) as CurrentCostPrice,isnull(tl.PackedQty,0) as PackedQty,isnull(tl.TotalPackedPieces,0) as TotalPackedPieces,
			isnull(tl.NewCostPrice,0.00) as NewCostPrice,isnull(tl.isFreeItem,0) as isFreeItem,
			isnull(tl.IsExchange,0) as IsExchange from (     select pm.ProductId,um.UnitType,pm.ProductName,isnull(ppm.Qty,0) as Qty,isnull(ppm.TotalPieces,0)  
			as TotalPieces,ppm.Unit,ppm.QtyPerUnit     ,0 as IsExchange,0 as isFreeItem     from PurchaseProductsMaster as ppm  
			inner join ProductMaster as pm on pm.AutoId=ppm.ProductAutoId    
			inner join UnitMaster as um on um.AutoId=ppm.Unit     where ppm.POAutoId='+convert(varchar(10),@POAutoId)+'     ) as t  
			full join 
			(     select vpm.ProductId,vum.UnitType as L_UnitType,vpm.ProductName,isnull(oim.QtyShip,0) as L_Qty,isnull(oim.QtyPerUnit,0) as L_QtyPerUnit,     
			oim.UnitAutoId as L_Unit,oim.ProductAutoId as L_ProductAutoId,oim.UnitAutoId as Original_UnitType,    
			isnull(oim.QtyShip,0) as PackedQty,isnull((oim.QtyPerUnit*oim.QtyShip),0) as TotalPackedPieces,   
			ISNULL(oim.UnitPrice,0.00) as NewCostPrice,isFreeItem,IsExchange from ['+@Location1+'.a1whm.com].dbo.Delivered_Order_Items as oim  
			inner join ['+@Location1+'.a1whm.com].dbo.OrderMaster as vom on vom.AutoId=oim.OrderAutoId     
			inner join ['+@Location1+'.a1whm.com].dbo.ProductMaster as vpm on vpm.AutoId=oim.ProductAutoId    
			inner join ['+@Location1+'.a1whm.com].dbo.UnitMaster as vum on vum.AutoId=oim.UnitAutoId    
			inner join ['+@Location1+'.a1whm.com].dbo.CustomerMaster as vcmx on vom.CustomerAutoId=vcmx.AutoId     
			where referenceOrderNumber=@PNo and vcmx.LocationAutoId='+convert(varchar(10),@CurrentLocationId)+'     ) 

			as tl on t.ProductId=tl.ProductId 
			and t.isFreeItem=tl.isFreeItem and t.IsExchange=tl.IsExchange    
			left join ProductMaster as pml on ISNULL(t.ProductId,tl.ProductId)=pml.ProductId
			left join PackingDetails as pd on pd.UnitType=tl.L_Unit and pml.AutoId=pd.ProductAutoId
			Order by ProductID,ProductName asc
			'
			Execute Sp_executesql @SqlStr,          
			N'@PNo varchar(20)',@PNo      
		 End      
		 else      
		 begin  
				Select @vender=VenderAutoId,@PNo=PONo from PurchaseOrderMaster where AutoId=@POAutoId      
				select @LocationID=LocationAutoId from VendorMaster where AutoId=@vender      
				select @Location1=Location from LocationMaster where AutoId=@LocationID      
      
				Select Po.AutoId,PONo,format(PODate,'MM/dd/yyyy')as PODate,format(DeliveryDate,'MM/dd/yyyy') as DeliveryDate,VenderAutoId,vm.VendorName,(sm.StatusType) as Status        
				,sm.AutoId as StatusAutoId,NoofItems,PORemarks,StockStatus,RecieveStockRemark,TotalOrderAmount,po.OrderNo from [dbo].[PurchaseOrderMaster]as po       
				inner join StatusMaster sm on sm.AutoId=po.Status and  sm.category='OrderMaster'      
				inner join VendorMaster vm on vm.AutoId=po.VenderAutoId        
				where po.AutoId=@POAutoId      
      
				set @SqlStr='
				select ISNULL(t.ProductId,tl.ProductId) as ProductId,ISNULL(t.ProductName,tl.ProductName) as ProductName,
				t.UnitType,isnull(t.Qty,0) as Qty,isnull(t.TotalPieces,0) as TotalPieces,t.Unit,isnull(tl.L_ProductAutoId,0) as  L_ProductAutoId, 
				isnull(t.QtyPerUnit,0) as QtyPerUnit,tl.L_UnitType,isnull(tl.L_Qty,0) as L_Qty,isnull(tl.L_QtyPerUnit,0) as L_QtyPerUnit,tl.L_Unit,
				isnull(pd.CostPrice,0.00) as CurrentCostPrice,isnull(tl.PackedQty,0) as PackedQty,isnull(tl.TotalPackedPieces,0) as TotalPackedPieces ,
				isnull(tl.NewCostPrice,0.00) as NewCostPrice,isnull(tl.isFreeItem,0) as isFreeItem,
				isnull(tl.IsExchange,0) as IsExchange from (     select pm.ProductId,um.UnitType,pm.ProductName,isnull(ppm.Qty,0) as Qty,isnull(ppm.TotalPieces,0)  
				as TotalPieces,ppm.Unit,ppm.QtyPerUnit     ,0 as IsExchange,0 as isFreeItem     from PurchaseProductsMaster as ppm  
				inner join ProductMaster as pm on pm.AutoId=ppm.ProductAutoId    
				inner join UnitMaster as um on um.AutoId=ppm.Unit     where ppm.POAutoId='+convert(varchar(10),@POAutoId)+'     ) as t  
				full join 
				(     select vpm.ProductId,vum.UnitType as L_UnitType,vpm.ProductName,isnull(oim.RequiredQty,0) as L_Qty,isnull(oim.QtyPerUnit,0) as L_QtyPerUnit,     
				oim.UnitTypeAutoId as L_Unit,oim.ProductAutoId as L_ProductAutoId,oim.Original_UnitType,    
				case when vom.Status>=3 then isnull(oim.QtyShip,0) else isnull(oim.RequiredQty,0) end as PackedQty,
				isnull((oim.QtyPerUnit*oim.QtyShip),0) as TotalPackedPieces,   
				ISNULL(oim.UnitPrice,0.00) as NewCostPrice,isFreeItem,IsExchange from ['+@Location1+'.a1whm.com].dbo.OrderItemMaster as oim  
				inner join ['+@Location1+'.a1whm.com].dbo.OrderMaster as vom on vom.AutoId=oim.OrderAutoId     
				inner join ['+@Location1+'.a1whm.com].dbo.ProductMaster as vpm on vpm.AutoId=oim.ProductAutoId    
				inner join ['+@Location1+'.a1whm.com].dbo.UnitMaster as vum on vum.AutoId=oim.UnitTypeAutoId    
				inner join ['+@Location1+'.a1whm.com].dbo.CustomerMaster as vcmx on vom.CustomerAutoId=vcmx.AutoId     
				where referenceOrderNumber=@PNo and vcmx.LocationAutoId='+convert(varchar(10),@CurrentLocationId)+'     ) 

				as tl on t.ProductId=tl.ProductId 
				and t.isFreeItem=tl.isFreeItem and t.IsExchange=tl.IsExchange    
				left join ProductMaster as pml on ISNULL(t.ProductId,tl.ProductId)=pml.ProductId
				left join PackingDetails as pd on pd.UnitType=tl.L_Unit and pml.AutoId=pd.ProductAutoId
				Order by ProductID,ProductName asc
				'
				Execute Sp_executesql @SqlStr,          
				N'@PNo varchar(20)',@PNo   
		  End  
  
		SELECT ProductId,ProductName,'Inactive' as ProductStatus  FROM ProductMaster      
		WHERE AutoId in (select ProductAutoId from PurchaseProductsMaster 
		where POAutoId=@POAutoId)  AND ProductStatus=0     
 END        
        
 ELSE IF @Opcode=48                                                                                                                      
 BEGIN  
   BEGIN TRY
   BEGIN TRAN
   if((Select Status from PurchaseOrderMaster where AutoId=@POAutoId)=1)      
   begin 
		declare @SqlStrDel1 nvarchar(max)
		Select @vender=VenderAutoId,@PNo=PONo from PurchaseOrderMaster where AutoId=@POAutoId      
		select @LocationID=LocationAutoId from VendorMaster where AutoId=@vender      
		select @Location1=Location from LocationMaster where AutoId=@LocationID      
     
		SET  @SqlStrDel1='
		DELETE FROM  ['+@Location1+'.a1whm.com].[dbo].[tbl_OrderLog] WHERE  [OrderAutoId] = 
		(Select omx.AutoId from ['+@Location1+'.a1whm.com].[dbo].[OrderMaster]as omx      
		inner join ['+@Location1+'.a1whm.com].dbo.CustomerMaster as cmx on omx.CustomerAutoId=cmx.AutoId      
		where referenceOrderNumber=@PNo and cmx.LocationAutoId='+convert(varchar(10),@CurrentLocationId)+')  
		DELETE FROM  ['+@Location1+'.a1whm.com].[dbo].[OrderItems_Original] WHERE  [OrderAutoId] = 
		(Select omx.AutoId from ['+@Location1+'.a1whm.com].[dbo].[OrderMaster]as omx      
		inner join ['+@Location1+'.a1whm.com].dbo.CustomerMaster as cmx on omx.CustomerAutoId=cmx.AutoId      
		where referenceOrderNumber=@PNo and cmx.LocationAutoId='+convert(varchar(10),@CurrentLocationId)+')  
		DELETE FROM  ['+@Location1+'.a1whm.com].[dbo].[Order_Original] WHERE [AutoId] = 
		(Select omx.AutoId from ['+@Location1+'.a1whm.com].[dbo].[OrderMaster]as omx      
		inner join ['+@Location1+'.a1whm.com].dbo.CustomerMaster as cmx on omx.CustomerAutoId=cmx.AutoId      
		where referenceOrderNumber=@PNo and cmx.LocationAutoId='+convert(varchar(10),@CurrentLocationId)+')                                                                                                                   
		DELETE FROM ['+@Location1+'.a1whm.com].[dbo].[OrderItemMaster] WHERE [OrderAutoId] = 
		(Select omx.AutoId from ['+@Location1+'.a1whm.com].[dbo].[OrderMaster]as omx      
		inner join ['+@Location1+'.a1whm.com].dbo.CustomerMaster as cmx on omx.CustomerAutoId=cmx.AutoId      
		where referenceOrderNumber=@PNo and cmx.LocationAutoId='+convert(varchar(10),@CurrentLocationId)+')                   
		DELETE FROM  ['+@Location1+'.a1whm.com].[dbo].[GenPacking] WHERE [OrderAutoId] = 
		(Select omx.AutoId from ['+@Location1+'.a1whm.com].[dbo].[OrderMaster]as omx      
		inner join ['+@Location1+'.a1whm.com].dbo.CustomerMaster as cmx on omx.CustomerAutoId=cmx.AutoId      
		where referenceOrderNumber=@PNo and cmx.LocationAutoId='+convert(varchar(10),@CurrentLocationId)+')                                                                                                                                                    
		DELETE FROM ['+@Location1+'.a1whm.com].[dbo].[DrvLog] WHERE [OrderAutoId] = 
		(Select omx.AutoId from ['+@Location1+'.a1whm.com].[dbo].[OrderMaster]as omx      
		inner join ['+@Location1+'.a1whm.com].dbo.CustomerMaster as cmx on omx.CustomerAutoId=cmx.AutoId      
		where referenceOrderNumber=@PNo and cmx.LocationAutoId='+convert(varchar(10),@CurrentLocationId)+')                                                                                                                                                 
		DELETE FROM ['+@Location1+'.a1whm.com].[dbo].[OrderMaster] WHERE [AutoId] = 
		(Select omx.AutoId from ['+@Location1+'.a1whm.com].[dbo].[OrderMaster]as omx      
		inner join ['+@Location1+'.a1whm.com].dbo.CustomerMaster as cmx on omx.CustomerAutoId=cmx.AutoId      
		where referenceOrderNumber=@PNo and cmx.LocationAutoId='+convert(varchar(10),@CurrentLocationId)+')' 
  
		Execute Sp_executesql @SqlStrDel1,          
		N'@PNo varchar(20)',@PNo 

		Delete From PurchaseProductsMaster where POAutoId=@POAutoId                                                                                                                                   
		Delete from [PurchaseOrderMaster] where AutoId=@POAutoId 
		Set @exceptionMessage='Deleted' 
        COMMIT TRAN
   End       
   else      
   begin                                                                                             
        Set @exceptionMessage='Not Deleted'       
   end  
   END TRY
   BEGIN CATCH
        RollBack Tran
        SET @isException=1
		SET @exceptionMessage='Oops, Something went wrong .Please try again.'
   END CATCH
 END    
 ELSE IF @Opcode=50                                                                                                                      
 BEGIN 
    IF EXISTS(SELECT * from PurchaseOrderMaster where AutoId=@POAutoId AND StockStatus='Received')
	BEGIN
	    Set @isException=0                                                                                                             
		Set @exceptionMessage='AlreadyReceived' 
	END
	ELSE
	BEGIN
		Exec Proc_ReceiveStockForPurchase_All        
		@VenderAutoId=@VenderAutoId,        
		@PoNo=@Orderno,        
		@EmpId=@EmpId        
         
		Update PurchaseOrderMaster Set StockReceiveDate=GetDate(),ReceivedBy=@EmpId,StockStatus='Received',
		RecieveStockRemark=@RecieveStockRemark where AutoId=@POAutoId              
          
		Set @isException=0                                                                                                             
		Set @exceptionMessage='Success' 
	END
 END         
   
 --From Draft to save Purchase Order--  
 else If @Opcode=101  
  Begin  
	  --IF ISNULL(@DraftAutoId,0)=0                                                                                                                                              
	  -- BEGIN  
			--INSERT INTO [dbo].[DraftPurchaseOrderMaster] ([PODate] , [VenderAutoId] , [PORemarks] , [CreatedBy] , [CreatedOn]) 
			--VALUES  (GetDate() , @VenderAutoId , @PORemarks , @EmpId , GetDate())
			--Set @DraftAutoId = SCOPE_IDENTITY()   
	  -- End  
	  -- begin  
		 --  update [dbo].[DraftPurchaseOrderMaster] set VenderAutoId=@VenderAutoId, PORemarks=@PORemarks  where DraftAutoId=@DraftAutoId  
	  -- End  
  
	  -- IF NOT EXISTS(SELECT * FROM [DraftPurchaseProductsMaster] WHERE DraftAutoId= @DraftAutoId AND ProductAutoId=@ProductAutoId)   
	  -- Begin  
			--INSERT INTO   [dbo].[DraftPurchaseProductsMaster] ([ProductAutoId] , [Unit] , [QtyPerUnit] , [Qty] , [DraftAutoId]) 
			--VALUES (@ProductAutoId , @UnitAutoId , @QtyPerUnit , @ReqQty , @DraftAutoId)
	  -- End  
	  -- Else  
	  -- Begin  
			--Update [dbo].[DraftPurchaseProductsMaster] Set [Qty] = [Qty] + @ReqQty where DraftAutoId=@DraftAutoId AND ProductAutoId=@ProductAutoId AND Unit=@UnitAutoId  
	  -- End 
		SELECT UM.AutoId AS AutoId, UM.[UnitType] AS UnitType, [Qty],ISNULL(EligibleforFree,0) as EligibleforFree,PM.VendorAutoId
		FROM [dbo].[PackingDetails] AS PD                                                                                           
		INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = PD.UnitType 
		INNER JOIN ProductMaster AS PM ON PM.AutoId=PD.ProductAutoId
		WHERE PD.[ProductAutoId] = @ProductAutoId 

		 Select AutoId,VendorName from VendorMaster  where Status=1

		--SELECT isnull(@DraftAutoId,0) as DraftAutoId 
  End  
  
  else if @Opcode=104  
  begin  
   SELECT ROW_NUMBER() OVER(ORDER BY DraftAutoId DESC) AS RowNumber, * INTO #Results1 FROM      
   (  
   Select vm.VendorName,Dpo.PORemarks,Format(DPO.PODate,'MM/dd/yyyy') as PODate,'Draft' as Status,DPO.DraftAutoId,  
   (Select Count(DraftAutoId) from DraftPurchaseProductsMaster where DraftAutoId=DPO.DraftAutoId) as NoOfItems  
   from DraftPurchaseOrderMaster as DPO  
   left Join VendorMaster as vm on vm.AutoId=DPO.VenderAutoId  
   where (@FromDate is null or @FromDate = '' or @Todate is null or @Todate = ''   
   or (convert(date,DPO.PODate) between Convert(date,@FromDate) and Convert(date,@Todate)))                                                                                            
   and (@VendorName is null or @VendorName='' or vm.VendorName like '%'+ @VendorName +'%')  
   ) as t order by DraftAutoId  DESC      
       
  SELECT COUNT(DraftAutoId) AS RecordCount, case when @PageSize=0 then COUNT(DraftAutoId) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Results1      
  SELECT * FROM #Results1      
  WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))        
    END   
  Else if @Opcode=106  
   Begin  
   
	SET @DefPacking=(Select um.UnitType from ProductMaster as PM
	Inner Join PackingDetails as PD on pm.AutoId=pd.ProductAutoId And PM.PackingAutoId=PD.UnitType
	Inner Join UnitMaster as Um on um.AutoId=pd.UnitType
	where pm.AutoId=@ProductAutoId                                                                                   
	)
    Select DPO.DraftAutoId,format(DPO.PODate,'MM/dd/yyyy')as PODate,VenderAutoId,vm.VendorName,'Incomplete' as Status,       
   (Select Count(DraftAutoId) from DraftPurchaseProductsMaster where DraftAutoId=DPO.DraftAutoId) as NoOfItems  
   ,PORemarks from [dbo].[DraftPurchaseOrderMaster]as DPO           
    inner join VendorMaster vm on vm.AutoId=DPO.VenderAutoId where DPO.DraftAutoId=@DraftAutoId      
      
    Select DPM.*,pm.ProductName,pm.ProductId,um.UnitType,(DPM.Qty*DPM.QtyPerUnit) as TotalPieces,pm.CurrentStock,
	(Select um.UnitType from ProductMaster as PMm
	Inner Join PackingDetails as PDd on pmm.AutoId=pdd.ProductAutoId And PMm.PackingAutoId=PDd.UnitType
	Inner Join UnitMaster as Um on um.AutoId=pdd.UnitType
	where pmm.AutoId=pm.AutoId) as DefUnit
	from DraftPurchaseProductsMaster as DPM        
    inner join ProductMaster pm on pm.AutoId=DPM.ProductAutoId        
    inner join UnitMaster um on um.AutoId=DPM.Unit        
    where DPM.DraftAutoId=@DraftAutoId      
      
    SELECT PD.ProductAutoId,UM.AutoId AS AutoId, UM.[UnitType] AS UnitType, PD.[Qty],ISNULL(EligibleforFree,0) as EligibleforFree  
    FROM [dbo].[PackingDetails] AS PD                                                                                           
    INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = PD.UnitType      
    WHERE PD.[ProductAutoId] in (select ProductAutoId from DraftPurchaseProductsMaster where DraftAutoId=@DraftAutoId)  
  
   End  
 Else If @Opcode=108  
 Begin  
   
 Delete DraftPurchaseProductsMaster where ProductAutoId=@ProductAutoId and Unit=@UnitAutoId and DraftAutoId=@DraftAutoId  
    
 End  
 Else If @Opcode=203  
 Begin  
  delete DraftPurchaseOrderMaster where DraftAutoId=@DraftAutoId   
  delete DraftPurchaseProductsMaster where DraftAutoId=@DraftAutoId  
 End  
 
 ELSE IF @Opcode=206                                                                             
	BEGIN  
	update DraftPurchaseProductsMaster set Unit=@UnitAutoId,Qty=@ReqQty,QtyPerUnit=@QtyPerUnit 
	where DraftAutoId=@DraftAutoId and ProductAutoId=@ProductAutoId                                                                                                                                            
	END 
  
 --End Draft to save Purchase Order--
 
 else if(@Opcode=441)    
 begin  
   SET @ProductAutoId=(Select AutoId from ProductMaster where ProductId=@ProductId)
   SELECT um.AutoId as UnitType,ISNULL(Qty,1) AS Qty,isnull(MinPrice,0.00)as MinPrice,isnull(CostPrice,0.00) as CostPrice,                                                                                       
   isnull(Price,0)as Price,isnull(MinPrice,0) as SRP,P_CommCode,isnull(WHminPrice,0) as WHminPrice,                      
   case when ISNULL(PM.PackingAutoId,0)=PD.UnitType then ISNULL(PM.PackingAutoId,0) else 0 end as DefaultPack,                       
   case when ISNULL((Select COUNT(*) from OrderItemMaster where ProductAutoId=@ProductAutoId AND UnitTypeAutoId=UM.AutoId),0)=0 then 0 else 1  end as OrderAttachment,                      
   UM.UnitType AS UnitName,isnull(PD.EligibleforFree,0) as Free,isnull(PD.AutoId,0) as PackingId FROM PackingDetails AS PD                                                         
   inner JOIN UnitMaster AS UM ON UM.AutoId=PD.UnitType AND PD.ProductAutoId=@ProductAutoId                       
   inner JOIN ProductMaster as pm on pm.AutoId=pd.ProductAutoId  order by UM.AutoId DESC  
    
   Select ('('+convert(varchar(50),ProductId)+' - '+ProductName+')') as ProductName from ProductMaster where AutoId=@ProductAutoId   
  
 end
 
 ELSE IF @OpCode=421                                             
  BEGIN                                                                                                    
    BEGIN TRY                                                                                                  
    BEGIN TRAN   
	 
	 SET @ProductAutoId=(Select AutoId FROM ProductMaster where ProductId=(select top 1 bu.ProductAutoId from @dtbulkUnit bu))
	 
     insert into PackingDetailsUpdateLog(ProductAutoId,UnitType,MinPrice,CostPrice,Price,SRP,WHminPrice,
	 UpdatedByAutoId,DateTime,Reff_Code)                                                           
     select pd.ProductAutoId,pd.UnitType,pd.MinPrice,pd.CostPrice,pd.Price,pm.P_SRP,pd.WHminPrice,
	 @EmpId,GETDATE(),'[dbo].[ProcPurchaseOrder],OpCode=421' from PackingDetails pd                                                                             
     inner join UnitMaster um on um.AutoId = pd.UnitType  
	 inner join ProductMaster pm on pm.AutoId=pd.ProductAutoId
     where ProductId in (select top 1 bu.ProductAutoId from @dtbulkUnit bu) 

	IF EXISTS(Select AutoId from @dtbulkUnit as pd
	INNER JOIN PackingDetails as b on b.ProductAutoId=@ProductAutoId AND pd.UnitAutoId=b.UnitType
	where PD.BasePrice!=b.Price)
	BEGIN 
		insert into ProductPricingInPriceLevel_log([AutoId],[PriceLevelAutoId],[ProductAutoId],[UnitAutoId],[CustomPrice],
		[createDated],[UpdatedBy])
		SELECT [AutoId],[PriceLevelAutoId],[ProductAutoId],[UnitAutoId],[CustomPrice],GETDATE(),@EmpId FROM
		ProductPricingInPriceLevel WHERE ProductAutoId=@ProductAutoId

		DELETE FROM [ProductPricingInPriceLevel] WHERE ProductAutoId=@ProductAutoId 
	END
    
     UPDATE PD SET PD.CostPrice=UM.CostPrice,PD.Price=UM.BasePrice,                                                    
     PD.MinPrice=UM.RetailPrice,PD.WHminPrice=UM.WHPrice,UpdateDate=GETDATE(),ModifiedBy=@EmpId FROM PackingDetails AS PD  
	 INNER JOIN ProductMaster pm on pm.AutoId=pd.ProductAutoId
     INNER JOIN @dtbulkUnit AS UM ON UM.ProductAutoId=pm.ProductId and UM.UnitAutoId=PD.UnitType                                                                                                                                                   
    
	EXEC [dbo].[ProcPriceLevelDeleteChangeCostPrice] @EmpAutoId=@EmpId

    COMMIT TRAN                                                                              
    END TRY                                                                          
    BEGIN CATCH                                           
   ROLLBACK TRANSACTION                                                                                                                                                       
   SET @isException=1                                                                                                 
   SET @exceptionMessage= 'Oops, Something went wrong .Please try again.'                                                                                                                                                    
  END CATCH                                
 END 
 ELSE IF @Opcode=422
 BEGIN
    SET @Location=(Select Location from LocationMaster as LM
	INNER JOIN VendorMaster AS VM ON VM.LocationAutoId=LM.AutoId Where VM.AutoId=@VenderAutoId)
	
    SET @SqlStr='
	select(select(select PMNJ.AutoId,convert(varchar(10),PMNJ.ProductId) + ''--'' + PMNJ.ProductName as P,ISNULL(PMNJ.MLQty,0) AS M, isnull(PMNJ.WeightOz,0) as W     ,
	ISNULL(PMNJ.CurrentStock,0) as C FROM [psmnj.a1whm.com].[dbo].[ProductMaster] as PM INNER JOIN ProductMaster AS PMNJ on PMNJ.ProductId=PM.ProductId
    Where PMNJ.ProductStatus=1  AND PM.ProductStatus=1                                                              
	and (select count(1) from [psmnj.a1whm.com].[dbo].PackingDetails where  ProductAutoId=pm.AutoId)>0                                                 
	order by convert(varchar(10),PMNJ.ProductId) + ''--'' + PMNJ.ProductName for json path, INCLUDE_NULL_VALUES) as ProductList, 	
	(Select AutoId,VendorName from VendorMaster  where Status=1 order by VendorName asc for json path, INCLUDE_NULL_VALUES) as VendorList
	for json path, INCLUDE_NULL_VALUES) as ProductList'  

	

	execute sp_executesql @SqlStr
 END
 else if(@Opcode=423)    
 begin    
	update DraftPurchaseOrderMaster set PORemarks=@PORemarks
	where DraftAutoId=@DraftAutoId         
 end
 ELSE IF @Opcode=424
 BEGIN
     Begin Try
		SELECT distinct ProductAutoId,sum(Qty) as Qty INTO #ProductAutoId FROM                
		(  
		SELECT distinct OIM.ProductAutoId,sum(QtyDel) as Qty FROM          
		Delivered_Order_Items AS OIM              
		INNER JOIN OrderMaster AS OM ON OM.AutoId=OIM.OrderAutoId 
		where om.Status=11 and OIM.QtyDel>0 AND
		DeliveryDate>=(case when @RefillType=2 then DATEADD(MONTH,-1, GETDATE()) else DATEADD(day, -14, GETDATE()) end)
		group BY OIM.ProductAutoId
		Union
		SELECT distinct OIM.ProductAutoId,SUM(ISNULL(OIM.QtyShip,0)*OIM.QtyPerUnit) as Qty FROM          
		OrderItemMaster AS OIM              
		INNER JOIN OrderMaster AS OM ON OM.AutoId=OIM.OrderAutoId 
		where om.Status NOT IN(8,11,1,2) and (ISNULL(OIM.QtyShip,0)*OIM.QtyPerUnit)>0 AND
		OrderDate>=(case when @RefillType=2 then DATEADD(MONTH,-1, GETDATE()) else DATEADD(day, -14, GETDATE()) end)
		group BY OIM.ProductAutoId
		--union
		--select AutoId as ProductAutoId,ReOrderMark as qty from ProductMaster where Cast((ReOrderMark) as int)>=CurrentStock and ProductStatus=1 order by ProductId,ProductName
		) as t group By ProductAutoId
		having SUM(Qty)>0
		

		
		if @POType=2
		begin
		SET @SqlStr='
	       Select (
		   Select 
		   (	
			Select P1.ProductAutoId,PM.ProductId,PM.ProductName,PM.CurrentStock,VM.AutoId,VM.VendorName,
			(Select UnitType from UnitMaster where AutoId=PM.PackingAutoId) as DefaultUnit,PD.Qty as QtyPerUnit,cast(ISNULL(P1.Qty/PD.Qty,1) as INT) as Qty,PM.PackingAutoId
			FROM ProductMaster as PM 
			Inner join ProductMaster as PM1 on PM.ProductId=PM1.ProductId
			INNER JOIN #ProductAutoId as P1 on P1.ProductAutoId=PM.Autoid
			INNER JOIN PackingDetails as PD on PD.ProductAutoId=P1.ProductAutoId AND PD.UnitType=PM.PackingAutoId
			left Join VendorMaster as VM on VM.AutoId=PM.VendorAutoId
			Where PM.ProductStatus=1 AND PM1.ProductStatus=1  and cast(ISNULL(P1.Qty/PD.Qty,1) as INT)>0 order by PM.ProductID asc for json path, INCLUDE_NULL_VALUES
		   ) as ProductList,
		   (
			Select P1.ProductAutoId,um.AutoId,um.UnitType,pd.EligibleforFree,pd.qty
			from  PackingDetails as PD 
			inner join UnitMaster as um on pd.UnitType=um.AutoId
			inner join #ProductAutoId as P1 on P1.ProductAutoId=PD.ProductAutoId
			Group by P1.ProductAutoId,um.AutoId,um.UnitType,pd.qty,pd.EligibleforFree for json path, INCLUDE_NULL_VALUES
		  ) as UnitList,
		  (
			Select AutoId,VendorName from VendorMaster  where Status=1 order by VendorName asc for json path, INCLUDE_NULL_VALUES
		  ) as VendorList for json path, INCLUDE_NULL_VALUES		   
		   ) as ProductDetails'
		   end
		   if @POType=1
		begin

		select AutoId as ProductAutoId,ReOrderMark as qty,VendorAutoId into #tmpProduct
		from ProductMaster where Cast((ReOrderMark) as int)>=CurrentStock and ProductStatus=1 order by ProductId,ProductName
	
	SET @SqlStr='
			   Select (
			   Select 
			   (	
				Select P1.ProductAutoId,PM.ProductId,PM.ProductName,PM.CurrentStock,VM.AutoId,VM.VendorName,
				(Select UnitType from UnitMaster where AutoId=PM.PackingAutoId) as DefaultUnit,PD.Qty as QtyPerUnit,
				cast(ISNULL(P1.Qty/PD.Qty,1) as INT) as Qty,PM.PackingAutoId
				FROM ProductMaster as PM  
				INNER JOIN #tmpProduct as P1 on P1.ProductAutoId=PM.Autoid
				INNER JOIN PackingDetails as PD on PD.ProductAutoId=P1.ProductAutoId AND PD.UnitType=PM.PackingAutoId
				left Join VendorMaster as VM on VM.AutoId=PM.VendorAutoId
				Where PM.ProductStatus=1 and P1.qty>pm.stock order by PM.ProductID asc for json path, INCLUDE_NULL_VALUES
			   ) as ProductList,
			   (
				Select P1.ProductAutoId,um.AutoId,um.UnitType,pd.EligibleforFree,pd.qty
				from  PackingDetails as PD 
				inner join UnitMaster as um on pd.UnitType=um.AutoId
				inner join #tmpProduct as P1 on P1.ProductAutoId=PD.ProductAutoId
				for json path, INCLUDE_NULL_VALUES
			  ) as UnitList,
			  (
				Select AutoId,VendorName from VendorMaster as vm
				inner join #tmpProduct as tm on tm.VendorAutoId=vm.AutoId			
				where Status=1 order by VendorName asc for json path,
				INCLUDE_NULL_VALUES
			  ) as VendorList for json path, INCLUDE_NULL_VALUES		   
			   ) as ProductDetails'
		   end
		  execute sp_executesql @SqlStr	
		  
		  Drop table #ProductAutoId
	End Try
	Begin catch
	     SET @isException=1
		 SET @exceptionMessage=ERROR_MESSAGE()
	End catch
 END
If @Opcode=425             
begin              
	BEGIN TRY                                                                                                                                      
		BEGIN TRAN 
		select ROW_NUMBER() over(order by tr.td.value('ProductAutoId[1]','int')) as ID, tr.td.value('ProductAutoId[1]','int') as ProductAutoId,tr.td.value('Unit[1]','int') as Unit,tr.td.value('QtyPerUnit[1]','int')
		as QtyPerUnit,tr.td.value('Qty[1]','int') as Qty,tr.td.value('TotalPieces[1]','int') as TotalPieces,tr.td.value('VendorAutoId[1]','int') as VendorAutoId 
		into #TempTable  from  @TableValue.nodes('/Xml') tr(td)

		Declare @i int=1,@j int=1,@k int=1,@Count int=0,@TotalVendorItem int,@VendorType int  
		
		SELECT  ROW_NUMBER() over(order by vendorautoid asc) as Rn ,VendorAutoId into #result43
		from ( select distinct VendorAutoId		from #TempTable inner join VendorMaster as vm  on vm.AutoId=VendorAutoId ) as t
		                                        
		SET @Count=(select COUNT(VendorAutoId) from #result43)
		------------
		DECLARE @ParmDefinition NVARCHAR(500)=''
		DECLARE @max_title VARCHAR(30)='' 

		IF (@Count>0)                                         
		BEGIN 
			WHILE @i <= @Count                                                          
			BEGIN  
			    SET @VenderAutoId=(select VendorAutoId from #result43 where Rn=@i ) 
				SET @VendorType=(select Isnull(LocationTypeAutoId,0) from VendorMaster where AutoId=@VenderAutoId)
				SET @LocationID=(Select LocationAutoId From VendorMaster where AutoId=@VenderAutoId)
				SET @Location=(Select Location FROM LocationMaster where AutoId=@LocationID)
				-------
				SET @SqlStr='
				select @Id=PM.ProductId from ProductMaster as PM 
				INNER JOIN #TempTable as TT on TT.ProductAutoId=PM.AutoId
				INNER JOIN ['+@Location+'.a1whm.com].[dbo].ProductMaster as LPM on LPM.ProductId=PM.ProductId 
				Where LPM.ProductStatus=0 AND TT.VendorAutoId='+Convert(varchar(20),@VenderAutoId)+''
				SET @ParmDefinition = N'@Id VARCHAR(max) OUTPUT';  

				EXECUTE sp_executesql @SqlStr, @ParmDefinition, @Id=@max_title OUTPUT; 

				IF @max_title!=''
				Break;

				SET @i=@i+1    
			END
		END
		IF(@max_title='')
		BEGIN
			IF (@Count>0)                                         
			BEGIN 		    

				WHILE @k <= @Count                                                          
				BEGIN  
					SET @VenderAutoId=(select VendorAutoId from #result43 where Rn=@k ) 
					SET @TotalVendorItem=(Select count(*) from #TempTable as t where t.VendorAutoId=@VenderAutoId)
					SET @VendorType=(select Isnull(LocationTypeAutoId,0) from VendorMaster where AutoId=@VenderAutoId)
					SET @CustomerAutoId =(select CustomerAutoId from  [dbo].[fn_GetCustomerAndSalesPerson](@VenderAutoId,@CurrentLocationId))

					if(Isnull(@VendorType,0)=1)
					begin   
						if(@CustomerAutoId is not null)        
						begin             

							SET @OrderNo = (SELECT DBO.SequenceCodeGenerator('PurchaseOrder'))    
     
							insert into [dbo].[PurchaseOrderMaster]
							([PONo],[PODate],[VenderAutoId],[Status],[NoofItems],[PORemarks],[CreatedBy],[CreatedOn],[StockStatus],VendorType)
							values (@OrderNo,GETDATE(),@VenderAutoId,1,@TotalVendorItem,@PORemarks,@EmpId,GETDATE(),'Pending',1)
   
							SET @POAutoId = (SELECT SCOPE_IDENTITY())         
                
							Insert into [dbo].[PurchaseProductsMaster]([ProductAutoId],[Unit],[QtyPerUnit],[Qty],[TotalPieces],[POAutoId])          
							select tbl.ProductAutoId,tbl.Unit,tbl.QtyPerUnit,tbl.Qty,tbl.TotalPieces,@POAutoId  from  #TempTable as tbl 
							where tbl.VendorAutoId=@VenderAutoId
				
							UPDATE SequenceCodeGeneratorMaster SET currentSequence = currentSequence + 1 WHERE SequenceCode='PurchaseOrder'

							exec [dbo].[Proc_GenerateOrder_all]        
							@VenderAutoId=@VenderAutoId,        
							@POAutoId=@POAutoId,        
							@Remark=@PORemarks,        
							@PONo=@OrderNo 
						
							Update PM SET PM.VendorAutoId=tb.VendorAutoId from ProductMaster as PM 
							inner join #TempTable as tb on tb.ProductAutoId=PM.AutoId
							where PM.VendorAutoId!=tb.VendorAutoId AND tb.VendorAutoId=@VenderAutoId

							Set @isException=0                                                                                                             
							Set @exceptionMessage='Success'        
						End        
						else         
						begin         
							Set @isException=1                                   
							Set @exceptionMessage='NotExist'        
						end        
					End   
					else 
					begin
						SET @OrderNo=(select ABS(CHECKSUM(NewId())) % 999999)

						INSERT INTO [dbo].[Draft_StockEntry] ([BillNo],[BillDate],[Remarks], [VendorAutoId],STATUS)                                
						VALUES(@OrderNo,GETDATE(),@PORemarks,@VenderAutoId,2)       
					
						SET @POAutoId = SCOPE_IDENTITY()    
					
						INSERT INTO [dbo].[Draft_StockItemMaster] ([BillAutoId],[ProductAutoId],[UnitAutoId],[Quantity],[TotalPieces],[QtyPerUnit],[Price])      
						select @POAutoId,tbl.ProductAutoId,tbl.Unit,tbl.Qty,tbl.TotalPieces,tbl.QtyPerUnit,
						(Select CostPrice from PackingDetails where ProductAutoId=tbl.ProductAutoId AND UnitType=tbl.Unit)  from  #TempTable
						as tbl where tbl.VendorAutoId=@VenderAutoId  

						Update PM SET PM.VendorAutoId=tb.VendorAutoId from ProductMaster as PM 
						inner join #TempTable as tb on tb.ProductAutoId=PM.AutoId
						where PM.VendorAutoId!=tb.VendorAutoId AND tb.VendorAutoId=@VenderAutoId
					end
					SET @k=@k+1                                                          
				END  
			END   
		END
		ELSE
		BEGIN
		    SET @SqlStr=''
			WHILE @j <= @Count                                                          
			BEGIN  
				SET @VenderAutoId=(select VendorAutoId from #result43 where Rn=@j ) 
				SET @VendorType=(select Isnull(LocationTypeAutoId,0) from VendorMaster where AutoId=@VenderAutoId)
				SET @LocationID=(Select LocationAutoId From VendorMaster where AutoId=@VenderAutoId)
				SET @Location=(Select Location FROM LocationMaster where AutoId=@LocationID)
				-------
				If(Isnull(@VendorType,0)=1)
				Begin
				  IF @SqlStr!=''
				  Begin
				       SET @SqlStr+=' Union all '
				  END
					SET @SqlStr+='
					select PM.ProductId from ProductMaster as PM 
					INNER JOIN #TempTable as TT on TT.ProductAutoId=PM.AutoId
					INNER JOIN ['+@Location+'.a1whm.com].[dbo].ProductMaster as LPM on LPM.ProductId=PM.ProductId 
					Where LPM.ProductStatus=0 AND TT.VendorAutoId='+Convert(varchar(20),@VenderAutoId)+' '
				END
				SET @j=@j+1    
			END
				EXECUTE sp_executesql @SqlStr
		END
		COMMIT TRANSACTION                                                                                                                                      
	END TRY                                                                                                                                      
	BEGIN CATCH                                                                                                                  
		ROLLBACK TRAN                                                                                                           
		Set @isException=1                                                                                     
		Set @exceptionMessage= 'Oops! Something went wrong.Please try later.'                                                                                 
	End Catch      
End    
	END TRY                                                        
	BEGIN CATCH                                         
	Set @isException=1                                   
	Set @exceptionMessage= 'Oops! Something went wrong.Please try later.'                                                                                                                                      
	END CATCH          
                                                                     
END 