USE [psmnj.a1whm.com]
GO

/****** Object:  StoredProcedure [dbo].[WaitForDelay]    Script Date: 01/27/2020 18:33:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[WaitForDelay]
	
AS
BEGIN
	 declare @Time varchar(20)=(select convert(varchar(20),Duration,8) from [dbo].[WaitForCommand])
	 WAITFOR DELAY @Time
END
GO


