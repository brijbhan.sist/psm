USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[Proc_ShortAmountReport]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Proc_ShortAmountReport] 
@FromDate Date=null,
@ToDate Date=null
AS 
BEGIN
	select pd.PaymentId,FORMAT(pd.PaymentDate,'MM/dd/yyyy HH:MM:ss tt') as PaymentDate,pd.ReceivedAmount,ISNULL(SortAmount,0) ShortAmount,PaymentRemarks
	,(emp.FirstName+' '+ISNULL(emp.LastName,'')) as SettledBy,(emp.FirstName+' '+ISNULL(emp.LastName,'')) as CollectedBy
	from CustomerPaymentDetails pd
	left join  SalesPaymentMaster as spd on spd.PaymentAutoId=pd.refPaymentId
	inner join EmployeeMaster as emp on emp.AutoId=pd.EmpAutoId
	LEFT join EmployeeMaster as emp1 on emp1.AutoId=spd.ReceiveBy where 
	ISNULL(SortAmount,0)>0
	and (@ToDate is null or @FromDate is null or (CONVERT(date,pd.PaymentDate) between @FromDate and @ToDate))
END
GO
