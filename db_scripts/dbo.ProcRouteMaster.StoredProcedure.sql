Alter procedure [dbo].[ProcRouteMaster]        
@Opcode INT =null,        
@UserAutoId int = null,        
@RouteId nvarchar(20) = null,         
@AutoId Int =null,         
@RouteName varchar(50) =null,        
@SalesPersonAutoId int =null,        
@CustomerAutoId int = null,        
@Status  int = null,      
@PageIndex int=null,         
@PageSize  int=10,         
@RecordCount int=null,         
@isException bit out,          
@exceptionMessage varchar(max) out        
        
as        
BEGIN        
     BEGIN try        
    SET @exceptionMessage= 'Success'          
    SET @isException=0           
    IF @Opcode=41        
    BEGIN        
		IF @AutoId=2      
		BEGIN      
			SELECT AutoId,  FirstName + ' ' + LastName as SalesPerson from EmployeeMaster where AutoId=@SalesPersonAutoId AND STATUS=1
			ORDER BY SalesPerson
		END      
		ELSE      
		BEGIN      
			SELECT AutoId, FirstName + ' ' + LastName as SalesPerson from EmployeeMaster where EmpType=2 AND STATUS=1 ORDER BY  SalesPerson ASC    --EmpId removed by Rizwan Ahmad 11/20/2019    
		END      
	END      
	IF @Opcode = 42        
	BEGIN       
		select ROW_NUMBER() over (order by RouteName) as RowNumber, rm.AutoId,rm.RouteId, rm.RouteName, em.FirstName + ' ' + LastName as SalesPerson,        
		(case when isnull(rm.Status,0)=0 then 'Inactive' else 'Active' END) as Status,        
		(select count (*)from RouteLog where RouteAutoId = rm.AutoId) as NoOfCustomer into #RESULT42        
		from RouteMaster as rm inner join EmployeeMaster as em on rm.SalesPersonAutoId = em.AutoId where        
		(@RouteName is null or @RouteName = '' or rm.RouteName like '%' + @RouteName + '%')        
		and (@SalesPersonAutoId is null or @SalesPersonAutoId = 0 or rm.SalesPersonAutoId = @SalesPersonAutoId)        
		and (@Status is null or @Status = 2 or rm.Status =@Status)      
		SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize = 0 then COUNT(RowNumber) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #RESULT42     
		SELECT * FROM #RESULT42        
		WHERE @PageSize=0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize))         
	END       
	IF @Opcode = 43        
	BEGIN      
		set @SalesPersonAutoId = (select SalesPersonAutoId  from RouteMaster where AutoId = @AutoId)  
		
		select [AutoId], CustomerId + ' - ' + CustomerName as Customer from CustomerMaster 
		where SalesPersonAutoId = @SalesPersonAutoId ORDER BY CustomerName ASC,CustomerId ASC  
		
		select AutoId,RouteId,RouteName,SalesPersonAutoId,Status,CreatedBy,CreatedOn,UpdatedBy,UpdatedOn,
		(Select EmpType FROM EmployeeMaster where AutoId=@SalesPersonAutoId) as EmpType
		from RouteMaster where AutoId = @AutoId     

		select * from((select cm.AutoId, cm.CustomerId + ' - ' + cm.CustomerName as Customer, 1 as SelectStatus from RouteMaster as rm     
		inner join RouteLog as rl on rm.AutoId = rl.RouteAutoId  left join  CustomerMaster as cm on (rl.CustomerAutoId = cm.AutoId)    
		where rm.AutoId = @AutoId        
		and (@CustomerAutoId is null or @CustomerAutoId = 0 or cm.AutoId = @CustomerAutoId)) 
		
		union         

		(select cm.AutoId, cm.CustomerId + ' - ' + cm.CustomerName as Customer, 0 as SelectStatus        
		from CustomerMaster as cm  where cm.SalesPersonAutoId = @SalesPersonAutoId and        
		cm.AutoId not in (Select [CustomerAutoId] from RouteLog where [RouteAutoId]= @AutoId)        
		and (@CustomerAutoId is null or @CustomerAutoId = 0 or cm.AutoId = @CustomerAutoId)))as t         
               
	END      
	IF @Opcode=44  --By Rizwan Ahmad on 07-09-2019    
	BEGIN    
		select [AutoId], CustomerId + ' - ' + CustomerName as Customer from CustomerMaster  where SalesPersonAutoId = @SalesPersonAutoId  
		AND STATUS=1 ORDER BY Customer

		select * from((select cm.AutoId, cm.CustomerId + ' - ' + cm.CustomerName as Customer, 1 as SelectStatus from RouteMaster as rm     
		inner join RouteLog as rl on rm.AutoId = rl.RouteAutoId  left join  CustomerMaster as cm on (rl.CustomerAutoId = cm.AutoId)    
		where rm.AutoId = @AutoId         
		union         
		(select cm.AutoId, cm.CustomerId + ' - ' + cm.CustomerName as Customer, 0 as SelectStatus        
		from CustomerMaster as cm  where cm.SalesPersonAutoId = @SalesPersonAutoId and        
		cm.AutoId not in (Select [CustomerAutoId] from RouteLog where [RouteAutoId]= @AutoId))))as t    
		
		Select EmpType FROM EmployeeMaster where AutoId=@SalesPersonAutoId
	END    
	IF @Opcode = 11        
	BEGIN           
		IF (select COUNT (*) from RouteMaster where RouteName = @RouteName and [SalesPersonAutoId]=@SalesPersonAutoId)= 0        
		BEGIN              
			SET @RouteId = (SELECT DBO.SequenceCodeGenerator('RouteNo'))        
			insert into RouteMaster([RouteId], [RouteName], [SalesPersonAutoId], [Status],[CreatedBy],[CreatedOn],[UpdatedBy],[UpdatedOn])        
			values (@RouteId,@RouteName,@SalesPersonAutoId,@Status,@UserAutoId,getdate(),@UserAutoId,getdate())        
			select SCOPE_IDENTITY() as routeid, @RouteId as AutoId        
			update SequenceCodeGeneratorMaster set [currentSequence] = [currentSequence] + 1 where [SequenceCode] = 'RouteNo'       
			SET @isException=0          
		END        
		else        
		BEGIN        
			SET @isException=1        
			SET @exceptionMessage='Exists'        
		END        
	END        
	IF @Opcode = 21        
	BEGIN   
	  IF (select COUNT (*) from RouteMaster where RouteName = @RouteName and [SalesPersonAutoId]=@SalesPersonAutoId)> 1        
	  BEGIN       
		SET @isException=1        
		SET @exceptionMessage='Exists'
	  END
	  ELSE
	  BEGIN  
		update RouteMaster set RouteName = @RouteName, SalesPersonAutoId = @SalesPersonAutoId,UpdatedBy = @UserAutoId ,UpdatedOn = getdate(),    
		Status = @Status where AutoId = @AutoId 
	  END
	END           
	IF  @Opcode = 22        
	BEGIN        
		IF exists(select 1 from RouteLog where CustomerAutoId = @CustomerAutoId and RouteAutoId = @AutoId)        
		BEGIN        
			delete from RouteLog where CustomerAutoId = @CustomerAutoId and RouteAutoId = @AutoId        
		END        
		else        
		BEGIN        
			insert into Routelog([RouteAutoId],[CustomerAutoId],[CreatedBy],[CreatedOn]) values(@AutoId,@CustomerAutoId,@UserAutoId,GETDATE())            
		END           
	END  
	IF @Opcode=23
	BEGIN
	     BEGIN TRY
			 Delete from RouteLog where RouteAutoId=@AutoId
			 Delete from RouteMaster where AutoId=@AutoId
			 SET @isException=0        
			 SET @exceptionMessage='Success' 
		 END TRY
		 BEGIN CATCH
		     SET @isException=1        
			 SET @exceptionMessage='Route is using in application so you can''t delete it.'  
		 END CATCH
	END
END try        
BEGIN catch        
    SET @isException=1        
    SET @exceptionMessage='Oops! Something went wrong.Please try later.'         
END catch        
END 
GO
