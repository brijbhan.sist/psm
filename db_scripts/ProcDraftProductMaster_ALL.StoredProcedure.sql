USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcDraftProductMaster_ALL]    Script Date: 08-09-2020 04:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
   
ALTER PROCEDURE [dbo].[ProcDraftProductMaster_ALL]           
@ProductId VARCHAR(12)=NULL         
AS          
BEGIN     
  Declare @BrandID varchar(12),@CategoryId varchar(50),@subCategoryId varchar(50)     
  Declare @cmdstring varchar(1000)   
  Declare @ImagePath VARCHAR(500),@Imgpath100 varchar(250),@Imgpath400 varchar(250),@newpath100 varchar(MAX),@newpath400 varchar(MAX)  
  Select @BrandID=BrandId from BrandMaster Where AutoId in (Select BrandAutoId from [DraftProductMaster] Where ProductId=@ProductId) 
  Select @CategoryId=CategoryId from CategoryMaster Where AutoId in (Select CategoryAutoId from [DraftProductMaster] Where ProductId=@ProductId) 
  Select @subCategoryId=SubcategoryId from SubCategoryMaster Where AutoId in (Select SubcategoryAutoId from [DraftProductMaster] Where ProductId=@ProductId) 
  SET  @ImagePath = replace((select [ImageUrl] FROM [dbo].[DraftProductMaster] where ProductId =@ProductId),'/','\')  
  SET  @Imgpath100 = replace((select [ThumbnailImage100] FROM [dbo].[DraftProductMaster] where ProductId =@ProductId),'/','\')  
  SET  @Imgpath400 = replace((select [ThumbnailImage400] FROM [dbo].[DraftProductMaster] where ProductId =@ProductId),'/','\')  

  declare @newPath varchar(max)=replace(@ImagePath,'DraftProduct','Attachments')  
 SET @newpath100=replace(@Imgpath100,'DraftProduct','productThumbnailImage')  
 SET @newpath400=replace(@Imgpath400,'DraftProduct','productThumbnailImage')  

  set @newPath=REPLACE(@newPath,'/','\')  
   set @newpath100=REPLACE(@newpath100,'/','\')  
   set @newpath400=REPLACE(@newpath400,'/','\')  
  
 if(DB_Name() in ('psmnj.a1whm.com'))  
 begin 
  -- psmct location 
   IF NOT EXISTS(SELECT [AutoId] FROM [psmct.a1whm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)          
   BEGIN       
     INSERT INTO [psmct.a1whm.com].[dbo].[ProductMaster]        
     (        
     [ProductId],[ProductName],[ProductLocation],[ImageUrl],[CategoryAutoId],[SubcategoryAutoId],[Stock],[ProductType],
	 [TaxRate],[PackingAutoId],P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],[ProductStatus],[CreateBy],
	 [CreateDate],[ModifiedBy], [ModifiedDate],IsApply_Oz,WeightOz,BrandAutoId,NewSRP,[ThumbnailImageUrl],ThirdImage,OriginalImageUrl)       
          
     select [ProductId],[ProductName],[ProductLocation],REPLACE(@newPath,'\','/'),
	 (Select cm.AutoId from [psmct.a1whm.com].[dbo].[CategoryMaster] as cm Where CategoryId=@CategoryId),
	 (Select sc.[AutoId] from [psmct.a1whm.com].[dbo].[SubCategoryMaster] as sc Where SubcategoryId=@subCategoryId)
	 ,[Stock],[ProductType],[TaxRate],[PackingAutoId]        
     ,P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],
	 (select isnull(Status,0)  from [dbo].[DraftProductLocation] where ProductAutoId=PM.AutoId and LocationId=2) as ProductStatus,
	 [CreateBy],[CreateDate],[ModifiedBy],[ModifiedDate],IsApply_Oz,WeightOz,      
     (Select BM.AutoId from [psmct.a1whm.com].[dbo].[BrandMaster] as BM Where BrandId=@BrandID),P_SRP,ThumbnailImage100,ThumbnailImage400,      
     @ImagePath FROM [dbo].[DraftProductMaster] as PM where ProductId =@ProductId      
             
   END     
 -- psmnj location  
  
   IF NOT EXISTS(SELECT [AutoId] FROM [psmnj.a1whm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)          
   BEGIN         
    INSERT INTO [psmnj.a1whm.com].[dbo].[ProductMaster]        
    (        
    [ProductId],[ProductName],[ProductLocation],[ImageUrl],[CategoryAutoId],[SubcategoryAutoId],[Stock],[ProductType],
	[TaxRate],[PackingAutoId]        
    ,P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],[ProductStatus],[CreateBy],[CreateDate],[ModifiedBy],  
    [ModifiedDate],IsApply_Oz,WeightOz,BrandAutoId,NewSRP,[ThumbnailImageUrl],ThirdImage,OriginalImageUrl)       
    select [ProductId],[ProductName],[ProductLocation],REPLACE(@newPath,'\','/'),
	(Select cm.AutoId from [psmnj.a1whm.com].[dbo].[CategoryMaster] as cm Where CategoryId=@CategoryId),
	 (Select sc.[AutoId] from [psmnj.a1whm.com].[dbo].[SubCategoryMaster] as sc Where SubcategoryId=@subCategoryId)
	 ,[Stock],[ProductType],[TaxRate],[PackingAutoId]        
    ,P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],
	 (select isnull(Status,0)  from [dbo].[DraftProductLocation] where ProductAutoId=PM.AutoId and LocationId=1) as ProductStatus,
	[ModifiedBy],[CreateDate],[ModifiedBy],[ModifiedDate] ,IsApply_Oz,WeightOz,      
    (Select BM.AutoId from [psmnj.a1whm.com].[dbo].[BrandMaster] as BM Where BrandId=@BrandID),P_SRP,
	REPLACE(@newpath100,'\','/') as ThumbnailImage100,REPLACE(@newpath400,'\','/') as ThumbnailImage400,@ImagePath      
    FROM [dbo].[DraftProductMaster] as PM  where ProductId =@ProductId  

	Declare @prdtId int
	SET @prdtId=SCOPE_IDENTITY()

	INSERT INTO [dbo].[RealProductLocation]([ProductAutoId],[LocationId],[Status])
	SELECT @prdtId,LocationId,Status FROM DraftProductLocation where ProductAutoId =(Select AutoId FROM [DraftProductMaster] where ProductId=@ProductId) 


    set @cmdstring = 'copy D:\PSMSERVER\psmnj.a1whm.com\wwwroot'+@ImagePath+ ' D:\PSMSERVER\psmnj.A1WHM.COM\wwwroot'+@newPath  
    exec master..xp_cmdshell @cmdstring 
	set @cmdstring = 'copy D:\PSMSERVER\psmnj.a1whm.com\wwwroot'+@Imgpath100+ ' D:\PSMSERVER\psmnj.A1WHM.COM\wwwroot'+@newpath100  
    exec master..xp_cmdshell @cmdstring 
	set @cmdstring = 'copy D:\PSMSERVER\psmnj.a1whm.com\wwwroot'+@Imgpath400+ ' D:\PSMSERVER\psmnj.A1WHM.COM\wwwroot'+@newpath400  
    exec master..xp_cmdshell @cmdstring 

	END      
   
 -- psmpa location  
  
  IF NOT EXISTS(SELECT [AutoId] FROM [psmpa.a1whm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)          
  BEGIN         
   INSERT INTO [psmpa.a1whm.com].[dbo].[ProductMaster]        
   (        
   [ProductId],[ProductName],[ProductLocation],[ImageUrl],[CategoryAutoId],[SubcategoryAutoId],[Stock],[ProductType],[TaxRate],[PackingAutoId]        
   ,P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],[ProductStatus],[CreateBy],[CreateDate],[ModifiedBy],  
   [ModifiedDate],IsApply_Oz,WeightOz,BrandAutoId,NewSRP,[ThumbnailImageUrl],ThirdImage,OriginalImageUrl)         
   select [ProductId],[ProductName],[ProductLocation],REPLACE(@newPath,'\','/'),
   (Select cm.AutoId from [psmpa.a1whm.com].[dbo].[CategoryMaster] as cm Where CategoryId=@CategoryId),
   (Select sc.[AutoId] from [psmpa.a1whm.com].[dbo].[SubCategoryMaster] as sc Where SubcategoryId=@subCategoryId)
	,[Stock],[ProductType],[TaxRate],[PackingAutoId]        
   ,P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],
   (select isnull(Status,0)  from [dbo].[DraftProductLocation] where ProductAutoId=PM.AutoId and LocationId=4) as ProductStatus,
   [CreateBy],[CreateDate],[ModifiedBy],[ModifiedDate] ,IsApply_Oz,WeightOz ,      
   (Select BM.AutoId from [psmpa.a1whm.com].[dbo].[BrandMaster] as BM Where BrandId=@BrandID),P_SRP,
   REPLACE(@newpath100,'\','/') as ThumbnailImage100,REPLACE(@newpath400,'\','/') as ThumbnailImage400,@ImagePath
   FROM [dbo].[DraftProductMaster] as PM where ProductId =@ProductId  
       
  END       
      
 --psmnpa location  
  
  IF NOT EXISTS(SELECT [AutoId] FROM [psmnpa.a1whm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)          
  BEGIN         
   INSERT INTO [psmnpa.a1whm.com].[dbo].[ProductMaster]        
   (        
   [ProductId],[ProductName],[ProductLocation],[ImageUrl],[CategoryAutoId],[SubcategoryAutoId],[Stock],[ProductType],[TaxRate],[PackingAutoId]        
   ,P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],[ProductStatus],[CreateBy],[CreateDate],[ModifiedBy],  
   [ModifiedDate],IsApply_Oz,WeightOz,BrandAutoId,NewSRP,[ThumbnailImageUrl],ThirdImage,OriginalImageUrl)          
           
   select [ProductId],[ProductName],[ProductLocation],REPLACE(@newPath,'\','/'),
   (Select cm.AutoId from [psmnpa.a1whm.com].[dbo].[CategoryMaster] as cm Where CategoryId=@CategoryId),
	(Select sc.[AutoId] from [psmnpa.a1whm.com].[dbo].[SubCategoryMaster] as sc Where SubcategoryId=@subCategoryId)
	,[Stock],[ProductType],[TaxRate],[PackingAutoId]        
   ,P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],
    (select isnull(Status,0)  from [dbo].[DraftProductLocation] where ProductAutoId=PM.AutoId and LocationId=5) as ProductStatus,
   [CreateBy],[CreateDate],[ModifiedBy],GETDATE(),IsApply_Oz,WeightOz,      
   (Select BM.AutoId from [psmnpa.a1whm.com].[dbo].[BrandMaster] as BM Where BrandId=@BrandID),P_SRP,
   REPLACE(@newpath100,'\','/') as ThumbnailImage100,REPLACE(@newpath400,'\','/') as ThumbnailImage400,@ImagePath
   FROM [dbo].[DraftProductMaster] as PM where ProductId =@ProductId  
           
  END        
        
 --psmwpa location  
    
  
 IF NOT EXISTS(SELECT [AutoId] FROM [psmwpa.a1whm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)          
  BEGIN         
   INSERT INTO [psmwpa.a1whm.com].[dbo].[ProductMaster]        
   (        
   [ProductId],[ProductName],[ProductLocation],[ImageUrl],[CategoryAutoId],[SubcategoryAutoId],[Stock],[ProductType],[TaxRate],[PackingAutoId]        
   ,P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],[ProductStatus],[CreateBy],[CreateDate],[ModifiedBy],  
   [ModifiedDate],IsApply_Oz,WeightOz,BrandAutoId,NewSRP,[ThumbnailImageUrl],ThirdImage,OriginalImageUrl)      
           
   select [ProductId],[ProductName],[ProductLocation],REPLACE(@newPath,'\','/'),
   (Select cm.AutoId from [psmwpa.a1whm.com].[dbo].[CategoryMaster] as cm Where CategoryId=@CategoryId),
   (Select sc.[AutoId] from [psmwpa.a1whm.com].[dbo].[SubCategoryMaster] as sc Where SubcategoryId=@subCategoryId)
	,[Stock],[ProductType],[TaxRate],[PackingAutoId],P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],
	(select isnull(Status,0)  from [dbo].[DraftProductLocation] where ProductAutoId=PM.AutoId and LocationId=9) as ProductStatus,
	[CreateBy],[CreateDate],[ModifiedBy],GETDATE(),IsApply_Oz,WeightOz,      
   (Select BM.AutoId from [psmwpa.a1whm.com].[dbo].[BrandMaster] as BM Where BrandId=@BrandID),P_SRP,
   REPLACE(@newpath100,'\','/') as ThumbnailImage100,REPLACE(@newpath400,'\','/') as ThumbnailImage400,@ImagePath
   FROM [dbo].[DraftProductMaster] as PM where ProductId =@ProductId  
     
  END   
  

  --psmny location  
    
  
 IF NOT EXISTS(SELECT [AutoId] FROM [psmny.a1whm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)          
  BEGIN         
   INSERT INTO [psmny.a1whm.com].[dbo].[ProductMaster]        
   (        
   [ProductId],[ProductName],[ProductLocation],[ImageUrl],[CategoryAutoId],[SubcategoryAutoId],[Stock],[ProductType],[TaxRate],[PackingAutoId]        
   ,P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],[ProductStatus],[CreateBy],[CreateDate],[ModifiedBy],  
   [ModifiedDate],IsApply_Oz,WeightOz,BrandAutoId,NewSRP,[ThumbnailImageUrl],ThirdImage,OriginalImageUrl)        
           
   select [ProductId],[ProductName],[ProductLocation],REPLACE(@newPath,'\','/'),
   (Select cm.AutoId from [psmny.a1whm.com].[dbo].[CategoryMaster] as cm Where CategoryId=@CategoryId),
   (Select sc.[AutoId] from [psmny.a1whm.com].[dbo].[SubCategoryMaster] as sc Where SubcategoryId=@subCategoryId)
	,[Stock],[ProductType],[TaxRate],[PackingAutoId],P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],
   (select isnull(Status,0)  from [dbo].[DraftProductLocation] where ProductAutoId=PM.AutoId and LocationId=10) as ProductStatus,
   [CreateBy],[CreateDate],[ModifiedBy],GETDATE(),IsApply_Oz,WeightOz,      
   (Select BM.AutoId from [psmny.a1whm.com].[dbo].[BrandMaster] as BM Where BrandId=@BrandID),P_SRP,
   REPLACE(@newpath100,'\','/') as ThumbnailImage100,REPLACE(@newpath400,'\','/') as ThumbnailImage400,@ImagePath
   FROM [dbo].[DraftProductMaster] as PM where ProductId =@ProductId  
     
  END   
   
 --psmnj easywhm location 
 IF NOT EXISTS(SELECT [AutoId] FROM [psmnj.easywhm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)          
  BEGIN         
   INSERT INTO [psmnj.easywhm.com].[dbo].[ProductMaster]        
   (        
   [ProductId],[ProductName],[ProductLocation],[ImageUrl],[CategoryAutoId],[SubcategoryAutoId],[Stock],[ProductType],[TaxRate],[PackingAutoId]        
   ,P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],[ProductStatus],[CreateBy],[CreateDate],[ModifiedBy],  
   [ModifiedDate],IsApply_Oz,WeightOz,BrandAutoId,NewSRP,[ThumbnailImageUrl],ThirdImage,OriginalImageUrl)       
           
   select [ProductId],[ProductName],[ProductLocation],REPLACE(@newPath,'\','/'),
   (Select cm.AutoId from [psmnj.easywhm.com].[dbo].[CategoryMaster] as cm Where CategoryId=@CategoryId),
	 (Select sc.[AutoId] from [psmnj.easywhm.com].[dbo].[SubCategoryMaster] as sc Where SubcategoryId=@subCategoryId)
	 ,[Stock],[ProductType],[TaxRate],[PackingAutoId],P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],
	(select isnull(Status,0)  from [dbo].[DraftProductLocation] where ProductAutoId=PM.AutoId and LocationId=1) as ProductStatus,
	 [CreateBy],[CreateDate],[ModifiedBy],GETDATE(),IsApply_Oz,WeightOz,      
   (Select AutoId from [psmnj.easywhm.com].[dbo].[BrandMaster] Where BrandId=@BrandID),P_SRP ,
   REPLACE(@newpath100,'\','/') as ThumbnailImage100,REPLACE(@newpath400,'\','/') as ThumbnailImage400,@ImagePath
   FROM [dbo].[DraftProductMaster] as PM where ProductId =@ProductId  
     
   set @cmdstring = 'copy D:\PSMSERVER\psmnj.A1WHM.COM\wwwroot'+@ImagePath+ ' D:\PSMSERVER\EASYWHM.COM\psmnj.easywhm.com\wwwroot'+@newPath  
   exec master..xp_cmdshell @cmdstring 
   set @cmdstring = 'copy D:\PSMSERVER\psmnj.a1whm.com\wwwroot'+@Imgpath100+ ' D:\PSMSERVER\EASYWHM.A1WHM.COM\psmnj.easywhm.com\wwwroot'+@newpath100  
    exec master..xp_cmdshell @cmdstring 
	set @cmdstring = 'copy D:\PSMSERVER\psmnj.a1whm.com\wwwroot'+@Imgpath400+ ' D:\PSMSERVER\EASYWHM.A1WHM.COM\psmnj.easywhm.com\wwwroot'+@newpath400  
    exec master..xp_cmdshell @cmdstring 
  END 

  --psmpa easywhm location 
 IF NOT EXISTS(SELECT [AutoId] FROM [psmpa.easywhm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)          
  BEGIN         
   INSERT INTO [psmpa.easywhm.com].[dbo].[ProductMaster]        
   (        
   [ProductId],[ProductName],[ProductLocation],[ImageUrl],[CategoryAutoId],[SubcategoryAutoId],[Stock],[ProductType],[TaxRate],[PackingAutoId]        
   ,P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],[ProductStatus],[CreateBy],[CreateDate],[ModifiedBy],  
   [ModifiedDate],IsApply_Oz,WeightOz,BrandAutoId,NewSRP,[ThumbnailImageUrl],ThirdImage,OriginalImageUrl)     
           
   select [ProductId],[ProductName],[ProductLocation],REPLACE(@newPath,'\','/'),
   (Select cm.AutoId from [psmpa.easywhm.com].[dbo].[CategoryMaster] as cm Where CategoryId=@CategoryId),
	 (Select sc.[AutoId] from [psmpa.easywhm.com].[dbo].[SubCategoryMaster] as sc Where SubcategoryId=@subCategoryId)
	 ,[Stock],[ProductType],[TaxRate],[PackingAutoId],P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],
	  (select isnull(Status,0)  from [dbo].[DraftProductLocation] where ProductAutoId=PM.AutoId and LocationId=4) as ProductStatus,
	 [CreateBy],[CreateDate],[ModifiedBy],GETDATE(),IsApply_Oz,WeightOz,      
   (Select BM.AutoId from [psmpa.easywhm.com].[dbo].[BrandMaster] as BM Where BrandId=@BrandID),P_SRP,
   REPLACE(@newpath100,'\','/') as ThumbnailImage100,REPLACE(@newpath400,'\','/') as ThumbnailImage400,@ImagePath
   FROM [dbo].[DraftProductMaster]as PM where ProductId =@ProductId  
     
  END 

  --psmnpa easywhm location 
 IF NOT EXISTS(SELECT [AutoId] FROM [psmnpa.easywhm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)          
  BEGIN         
   INSERT INTO [psmnpa.easywhm.com].[dbo].[ProductMaster]        
   (        
   [ProductId],[ProductName],[ProductLocation],[ImageUrl],[CategoryAutoId],[SubcategoryAutoId],[Stock],[ProductType],[TaxRate],[PackingAutoId]        
   ,P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],[ProductStatus],[CreateBy],[CreateDate],[ModifiedBy],  
   [ModifiedDate],IsApply_Oz,WeightOz,BrandAutoId,NewSRP,[ThumbnailImageUrl],ThirdImage,OriginalImageUrl)       
           
   select [ProductId],[ProductName],[ProductLocation],REPLACE(@newPath,'\','/'),
   (Select cm.AutoId from [psmnpa.easywhm.com].[dbo].[CategoryMaster] as cm Where CategoryId=@CategoryId),
	 (Select sc.[AutoId] from [psmnpa.easywhm.com].[dbo].[SubCategoryMaster] as sc Where SubcategoryId=@subCategoryId)
	 ,[Stock],[ProductType],[TaxRate],[PackingAutoId],P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],
	 (select isnull(Status,0)  from [dbo].[DraftProductLocation] where ProductAutoId=PM.AutoId and LocationId=5) as ProductStatus,
	 [CreateBy],[CreateDate],[ModifiedBy],GETDATE(),IsApply_Oz,WeightOz,      
   (Select BM.AutoId from [psmnpa.easywhm.com].[dbo].[BrandMaster] as BM Where BrandId=@BrandID),P_SRP,
   REPLACE(@newpath100,'\','/') as ThumbnailImage100,REPLACE(@newpath400,'\','/') as ThumbnailImage400,@ImagePath
   FROM [dbo].[DraftProductMaster] as PM where ProductId =@ProductId  
     
  END 

  --psmwpa easywhm location 
 IF NOT EXISTS(SELECT [AutoId] FROM [psmwpa.easywhm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)          
  BEGIN         
   INSERT INTO [psmwpa.easywhm.com].[dbo].[ProductMaster]        
   (        
   [ProductId],[ProductName],[ProductLocation],[ImageUrl],[CategoryAutoId],[SubcategoryAutoId],[Stock],[ProductType],[TaxRate],[PackingAutoId]        
   ,P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],[ProductStatus],[CreateBy],[CreateDate],[ModifiedBy],  
   [ModifiedDate],IsApply_Oz,WeightOz,BrandAutoId,NewSRP,[ThumbnailImageUrl],ThirdImage,OriginalImageUrl)      
           
   select [ProductId],[ProductName],[ProductLocation],REPLACE(@newPath,'\','/'),
     (Select cm.AutoId from [psmwpa.easywhm.com].[dbo].[CategoryMaster] as cm Where CategoryId=@CategoryId),
	 (Select sc.[AutoId] from [psmwpa.easywhm.com].[dbo].[SubCategoryMaster] as sc Where SubcategoryId=@subCategoryId)
	 ,[Stock],[ProductType],[TaxRate],[PackingAutoId],P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],
	 (select isnull(Status,0)  from [dbo].[DraftProductLocation] where ProductAutoId=PM.AutoId and LocationId=9) as ProductStatus,
	 [CreateBy],[CreateDate],[ModifiedBy],GETDATE(),IsApply_Oz,WeightOz,      
   (Select BM.AutoId from [psmwpa.easywhm.com].[dbo].[BrandMaster] as BM Where BrandId=@BrandID),P_SRP,
   REPLACE(@newpath100,'\','/') as ThumbnailImage100,REPLACE(@newpath400,'\','/') as ThumbnailImage400,@ImagePath
   FROM [dbo].[DraftProductMaster] as PM where ProductId =@ProductId  
     
  END 

 --psmct easywhm location 
 IF NOT EXISTS(SELECT [AutoId] FROM [psmct.easywhm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)          
  BEGIN         
   INSERT INTO [psmct.easywhm.com].[dbo].[ProductMaster]        
   (        
   [ProductId],[ProductName],[ProductLocation],[ImageUrl],[CategoryAutoId],[SubcategoryAutoId],[Stock],[ProductType],[TaxRate],[PackingAutoId]        
   ,P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],[ProductStatus],[CreateBy],[CreateDate],[ModifiedBy],  
   [ModifiedDate],IsApply_Oz,WeightOz,BrandAutoId,NewSRP,[ThumbnailImageUrl],ThirdImage,OriginalImageUrl)      
           
   select [ProductId],[ProductName],[ProductLocation],REPLACE(@newPath,'\','/'),
   (Select cm.AutoId from [psmct.easywhm.com].[dbo].[CategoryMaster] as cm Where CategoryId=@CategoryId),
	 (Select sc.[AutoId] from [psmct.easywhm.com].[dbo].[SubCategoryMaster] as sc Where SubcategoryId=@subCategoryId)	 
	 ,[Stock],[ProductType],[TaxRate],[PackingAutoId],P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],
	 (select isnull(Status,0)  from [dbo].[DraftProductLocation] where ProductAutoId=PM.AutoId and LocationId=2) as ProductStatus,
	 [CreateBy],[CreateDate],[ModifiedBy],GETDATE(),IsApply_Oz,WeightOz,      
   (Select BM.AutoId from [psmct.easywhm.com].[dbo].[BrandMaster] as BM Where BrandId=@BrandID),P_SRP ,
   REPLACE(@newpath100,'\','/') as ThumbnailImage100,REPLACE(@newpath400,'\','/') as ThumbnailImage400,@ImagePath
   FROM [dbo].[DraftProductMaster] as PM where ProductId =@ProductId  
     
  END 

  --demo easywhm location 
 IF NOT EXISTS(SELECT [AutoId] FROM [psmny.easywhm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)          
  BEGIN         
   INSERT INTO [psmny.easywhm.com].[dbo].[ProductMaster]        
   (        
   [ProductId],[ProductName],[ProductLocation],[ImageUrl],[CategoryAutoId],[SubcategoryAutoId],[Stock],[ProductType],[TaxRate],[PackingAutoId]        
   ,P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],[ProductStatus],[CreateBy],[CreateDate],[ModifiedBy],  
   [ModifiedDate],IsApply_Oz,WeightOz,BrandAutoId,NewSRP,[ThumbnailImageUrl],ThirdImage,OriginalImageUrl)        
           
   select [ProductId],[ProductName],[ProductLocation],REPLACE(@newPath,'\','/'),
    (Select cm.AutoId from [psmny.easywhm.com].[dbo].[CategoryMaster] as cm Where CategoryId=@CategoryId),
	(Select sc.[AutoId] from [psmny.easywhm.com].[dbo].[SubCategoryMaster] as sc Where SubcategoryId=@subCategoryId)
	,[Stock],[ProductType],[TaxRate],[PackingAutoId],P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],
	(select isnull(Status,0)  from [dbo].[DraftProductLocation] where ProductAutoId=PM.AutoId and LocationId=8) as ProductStatus,
	[CreateBy],[CreateDate],[ModifiedBy],GETDATE(),IsApply_Oz,WeightOz,      
   (Select BM.AutoId from [psmny.easywhm.com].[dbo].[BrandMaster] as BM Where BrandId=@BrandID),P_SRP ,
   REPLACE(@newpath100,'\','/') as ThumbnailImage100,REPLACE(@newpath400,'\','/') as ThumbnailImage400,@ImagePath
   FROM [dbo].[DraftProductMaster] as PM where ProductId =@ProductId  
      
  END 

 END  
 else  
 BEGIN       
  INSERT INTO [ProductMaster]        
  (        
  [ProductId],[ProductName],[ProductLocation],[ImageUrl],[CategoryAutoId],[SubcategoryAutoId],[Stock],[ProductType],[TaxRate],[PackingAutoId]        
  ,P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],[ProductStatus],[CreateBy],[CreateDate],[ModifiedBy],  
  [ModifiedDate],IsApply_Oz,WeightOz,BrandAutoId,NewSRP,[ThumbnailImageUrl],ThirdImage,OriginalImageUrl)           
  select [ProductId],[ProductName],[ProductLocation],REPLACE(@newPath,'\','/'),
  (Select cm.AutoId from [dbo].[CategoryMaster] as cm Where CategoryId=@CategoryId),
	(Select sc.[AutoId] from [dbo].[SubCategoryMaster] as sc Where SubcategoryId=@subCategoryId),[Stock],[ProductType],[TaxRate],[PackingAutoId]        
  ,P_CommCode,[ReOrderMark],IsApply_ML,[MLQty],[UpdateDate],
  (select isnull(Status,0)  from [dbo].[DraftProductLocation] where ProductAutoId=PM.AutoId and LocationId=1) as ProductStatus,
  [CreateBy],[CreateDate],[ModifiedBy],[ModifiedDate] ,IsApply_Oz,WeightOz ,      
  (Select BM.AutoId from [dbo].[BrandMaster] as BM Where BrandId=@BrandID),P_SRP,
  REPLACE(@newpath100,'\','/') as ThumbnailImage100,REPLACE(@newpath400,'\','/') as ThumbnailImage400,@ImagePath
  FROM [dbo].[DraftProductMaster] as PM where ProductId =@ProductId  
      
  set @cmdstring = 'copy D:\PSMSERVER\psm.a1whm.COM\wwwroot'+@ImagePath+ ' D:\PSMSERVER\psm.a1whm.com\wwwroot'+@newPath  
  exec master..xp_cmdshell @cmdstring  
  set @cmdstring = 'copy D:\PSMSERVER\psm.a1whm.COM\wwwroot'+@Imgpath100+ ' D:\PSMSERVER\psm.a1whm.com\wwwroot'+@newpath100  
  exec master..xp_cmdshell @cmdstring  
  set @cmdstring = 'copy D:\PSMSERVER\psm.a1whm.COM\wwwroot'+@Imgpath400+ ' D:\PSMSERVER\psm.a1whm.com\wwwroot'+@newpath400  
  exec master..xp_cmdshell @cmdstring  

     
  set @cmdstring = 'copy D:\PSMSERVER\demo.A1WHM.COM\wwwroot'+@ImagePath+ ' D:\PSMSERVER\psm.a1whm.com\wwwroot'+@newPath  
  exec master..xp_cmdshell @cmdstring 
  
  set @cmdstring = 'copy D:\PSMSERVER\demo.A1WHM.COM\wwwroot'+@Imgpath100+ ' D:\PSMSERVER\psm.a1whm.com\wwwroot'+@newpath100  
  exec master..xp_cmdshell @cmdstring
  set @cmdstring = 'copy D:\PSMSERVER\demo.A1WHM.COM\wwwroot'+@Imgpath100+ ' D:\PSMSERVER\psm.a1whm.com\wwwroot'+@newpath400  
  exec master..xp_cmdshell @cmdstring
  

     
 END    
END          