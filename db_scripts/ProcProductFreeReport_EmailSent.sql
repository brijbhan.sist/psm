
ALTER PROCEDURE [dbo].[ProcProductFreeReport_EmailSent] 
AS        
BEGIN  
   declare  @DriveLocation varchar(250)
   SET @DriveLocation='D:\ProductReport\'+UPPER((select CompanyId from dbo.CompanyDetails))+'_Product_Report_By_Free'+
   convert(varchar(50),Format(GetDate(),'MM_dd_yyyy_hh_mm_tt'))+'.html'
          
	SELECT ROW_NUMBER() OVER(ORDER BY [CustomerName]) AS RowNumber,CM.CustomerId,CM.CustomerName, OrderNo,FORMAT(OrderDate,'MM/dd/yyyy') as OrderDate,ProductId,ProductName,UM.UnitType,sum(OIM.RequiredQty)      
	AS OrderQty, Em.FirstName +' ' + Em.LastName as SalesPerson,Em2.FirstName +' ' + Em2.LastName as Driver,CAST(pd.Price as decimal(10,2)) As PPrice
	into #tempTbl FROM          
	OrderItemMaster AS OIM              
	INNER JOIN OrderMaster AS OM ON OM.AutoId=OIM.OrderAutoId              
	INNER JOIN CustomerMaster AS CM ON CM.AutoId=OM.CustomerAutoId              
	INNER JOIN ProductMaster AS PM ON PM.AutoId=OIM.ProductAutoId              
	INNER JOIN UnitMaster AS UM ON UM.AutoId=OIM.UnitTypeAutoId            
	inner join EmployeeMaster as Em on em.AutoId = Om.SalesPersonAutoId            
	left join EmployeeMaster as Em2 on em2.AutoId = Om.Driver           
	Inner Join PackingDetails as pd on pd.ProductAutoId=OIM.ProductAutoId and pd.UnitType=OIM.UnitTypeAutoId          
	where oim.isFreeItem=1      
	and om.Status!='8' 
	and CONVERT(date, OrderDate)=CONVERT(date, GETDATE())               
	GROUP BY CM.CustomerId,CM.CustomerName, OrderNo,OrderDate,ProductId,ProductName,UM.UnitType,Em.FirstName +' ' + Em.LastName,Em2.FirstName +' ' + Em2.LastName,pd.Price           
	having sum(OIM.RequiredQty)>0         
	order by [CustomerName]
	Declare @AutoIdint int,@CustomerId VARCHAR(20),@CustomerName VARCHAR(500),@OrderNo VARCHAR(20),
	@OrderDate VARCHAR(20),@ProductId  int,@ProductName varchar(250),@UnitType varchar(50),@SalesPerson varchar(150),@OrderQty int,
	@SellingPrice decimal(18,2),@Count int,@num int=1,@html varchar(max)='',@tr varchar(max)=''

	SET @Count=(select count(1) from #tempTbl)
	if(@count>0)
	begin
	     set @html='<table class="table tableCSS" border="1" style="width:100%;border: 1px solid black;border-collapse:collapse;">
		<thead>
			<tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
				<td style="text-align: center !important;">
					<h4 style="margin:10px;">Product Report By Free</h4>
				</td>
				</tr>
				<tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
				<td style="text-align: center !important;">
					<h4 style="margin:2px">'+(select CompanyDistributerName from CompanyDetails)+'</h4>
				</td>
				</tr>
				<tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
				<td style="text-align: center !important;">
					<h4 style="margin:2px">'+Convert(varchar(50),FORMAT(GetDate(),'MM/dd/yyyy hh:ss tt'))+'</h4>
				</td>
			</tr>
		</thead>
		<tbody></tbody>
		</table>
		<br>
		<table class="table tableCSS" border="1" style="width:100%;border: 1px solid black;border-collapse:collapse;" 
		id="tblNotShippedReportByHeader"> 
		<thead>
		<tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
			<td style="text-align: center !important;" class="text-center">Customer ID</td>
			<td style="text-align: center !important;" class="text-center">Customer Name</td>
			<td style="text-align: center !important;" class="text-center">Order No</td>
			<td style="text-align: center !important;" class="text-center">Order Date</td>
			<td style="text-align: center !important;" class="text-center">Sales Person</td>
			<td style="text-align: center !important;" class="text-center">Product ID</td>
			<td style="text-align: center !important;" class="text-center">Product Name</td>
			<td style="text-align: center !important;" class="text-center">Unit</td>
			<td style="text-align: center !important;" class="text-center">Order Qty</td>
			<td style="text-align: center !important;" class="text-right">Product Price</td>
			<td style="text-align: center !important;" class="text-right">Total Amount</td>
		</tr>' +   
		'</thead> <tbody>'
		EXEC dbo.spWriteToFile @DriveLocation, @html
		SET @html=''
		while(@num<=@Count)
		BEGIN 
		
			select 
			@CustomerId=CustomerId,
			@CustomerName=CustomerName,
			@OrderNo=OrderNo,
			@OrderDate=OrderDate,
			@ProductId=ProductId,
			@ProductName=ProductName,
			@UnitType=UnitType,
			@OrderQty=OrderQty,
			@SalesPerson=SalesPerson,
			@SellingPrice=PPrice
			from #tempTbl where RowNumber = @num 
			 
			set @tr ='<tr>' +	'<td style=''text-align:center;''>'+convert(varchar(50), @CustomerId) + '</td>' 
							+	'<td style=''text-align:center;''>'+ convert(varchar(50), @CustomerName) + '</td>'
							+	'<td style=''text-align:center;''>'+convert(varchar(50), @OrderNo) + '</td>' 
							+	'<td style=''text-align:center;''>'+ convert(varchar(50), @OrderDate) + '</td>' 
							+	'<td style=''text-align:center;''>'+ convert(varchar(50), @SalesPerson) + '</td>'
							+	'<td style=''text-align:center;''>'+convert(varchar(50), @ProductId) + '</td>' 
							+	'<td style=''text-align:center;''>'+ convert(varchar(50), @ProductName) + '</td>' 
							+	'<td style=''text-align:center;''>'+ convert(varchar(50), @UnitType) + '</td>'
							+	'<td style=''text-align:center;''>'+ convert(varchar(50), @OrderQty) + '</td>' 
							+	'<td style=''text-align:right;''>'+ convert(varchar(50), @SellingPrice) + '</td>'
							+	'<td style=''text-align:right;''>'+ convert(varchar(50), cast((@SellingPrice * @OrderQty) as decimal(10,2))) + '</td>'
							+ '</tr>' ;
			EXEC dbo.spWriteToFile @DriveLocation, @tr
			set @num = @num + 1
		END
		set @html =' 
		<tfoot>
			<tr>
				<td colspan="10" style="text-align:right"><b>Total</b></td> 
				<td id="CashTotal" style="text-align:right"><b>'+convert(varchar(50),(select sum(isnull(cast((PPrice*OrderQty) as decimal(10,2)),'0.00')) from #tempTbl))+'</b></td>
			</tr>
		</tfoot>
		</table><p style="page-break-before: always"></p>'
		EXEC dbo.spWriteToFile @DriveLocation, @html 
	END
	--------------Email Code---------------
	Declare  @Subject varchar(max),@FromName varchar(1000),@FromEmailId varchar(1000),@Port int,@SMTPServer varchar(1000),@Password varchar(1000),@SSL  int,
	@BCCEmailId varchar(max),@MigrateEmailId varchar(1000),@CCEmailId varchar(max),@smtp_userName varchar(max)
	set @Subject = 'Product Report By Free ' +UPPER((select CompanyDistributerName from dbo.CompanyDetails))+' : ' + 
	format(GETDATE(),'MM/dd/yyy hh:mm tt');
	
	SELECT @FromEmailId=FromEmail,@FromName=DisplayName,@smtp_userName=SMTP_USERNAME,@Password=SMTP_PASSWORD,
	@SMTPServer=HOST,@Port=SMTPPORT,@SSL=EnableSsl from EmailServerMaster_AWS

	set @BCCEmailId=(select top 1 BCCEmail from [dbo].[Tbl_ReportEmailReceiver] where autoid=3);
	set @MigrateEmailId=(select top 1 ToEmail from [dbo].[Tbl_ReportEmailReceiver] where autoid=3)
	SET @CCEmailId=(select top 1 CCEmail from [dbo].[Tbl_ReportEmailReceiver] where autoid=3)
    IF(select count(1) from #tempTbl )>0
	BEGIN
		 EXEC [Emailing_DB].[dbo].[ProcSubmitEmail] 
			@Opcode=11,
			@FromEmailId =@FromEmailId,
			@FromName = @FromName,
			@smtp_userName=@smtp_userName,
			@Password = @Password,
			@SMTPServer = @SMTPServer,
			@Port =@Port,
			@SSL =@SSL,
			@ToEmailId =@MigrateEmailId,
			@CCEmailId =@CCEmailId,
			@BCCEmailId =@BCCEmailId,  
			@Subject =@Subject,
			@EmailBody = '',
			@SentDate ='',
			@Status =0,
			@SourceApp ='PSM',
			@SubUrl ='Product Report By Free',
			@attachment=@DriveLocation,
			@isException=0,
			@exceptionMessage=''  
	END
END