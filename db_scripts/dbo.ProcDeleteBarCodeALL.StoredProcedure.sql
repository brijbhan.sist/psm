USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcDeleteBarCodeALL]    Script Date: 08-09-2020 05:54:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER Proc [dbo].[ProcDeleteBarCodeALL]  
@PreDefinedBarcode varchar(250)  
as   
BEGIN   
  if(DB_NAME() in ('psmnj.a1whm.com','psmpa.a1whm.com','psmnpa.a1whm.com','psmct.a1whm.com'
  ,'psmwpa.a1whm.com','psmny.a1whm.com'))                      
  BEGIN                     
    DELETE FROM [psmnj.a1whm.com].[dbo].[ItemBarcode] WHERE Barcode=@PreDefinedBarcode   
    DELETE FROM [psmct.a1whm.com].[dbo].[ItemBarcode] WHERE Barcode=@PreDefinedBarcode   
    DELETE FROM [psmpa.a1whm.com].[dbo].[ItemBarcode] WHERE Barcode=@PreDefinedBarcode   
    DELETE FROM [psmnpa.a1whm.com].[dbo].[ItemBarcode] WHERE Barcode=@PreDefinedBarcode  
    DELETE FROM [psmwpa.a1whm.com].[dbo].[ItemBarcode] WHERE Barcode=@PreDefinedBarcode
	DELETE FROM [psmny.a1whm.com].[dbo].[ItemBarcode] WHERE Barcode=@PreDefinedBarcode
	DELETE FROM [psmnj.easywhm.com].[dbo].[ItemBarcode] WHERE Barcode=@PreDefinedBarcode
	DELETE FROM [psmpa.easywhm.com].[dbo].[ItemBarcode] WHERE Barcode=@PreDefinedBarcode
	DELETE FROM [psmnpa.easywhm.com].[dbo].[ItemBarcode] WHERE Barcode=@PreDefinedBarcode
	DELETE FROM [psmwpa.easywhm.com].[dbo].[ItemBarcode] WHERE Barcode=@PreDefinedBarcode
	DELETE FROM [psmct.easywhm.com].[dbo].[ItemBarcode] WHERE Barcode=@PreDefinedBarcode
	DELETE FROM [demo.easywhm.com].[dbo].[ItemBarcode] WHERE Barcode=@PreDefinedBarcode
  END  
  ELSE  
  BEGIN  
   IF NOT EXISTS(SELECT 1 FROM [dbo].[OrderItemMaster] WHERE Barcode=@PreDefinedBarcode)                        
   BEGIN   
     DELETE FROM [dbo].[ItemBarcode] WHERE Barcode=@PreDefinedBarcode                       
   END  
  END  
END  