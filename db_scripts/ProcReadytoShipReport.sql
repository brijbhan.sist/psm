Alter PROCEDURE ProcReadytoShipReport  
@OpCode int,    
@PageIndex INT=1,                                                                      
@PageSize INT=10,                                                                      
@RecordCount INT=null,                                                                      
@isException bit out,                                                                      
@exceptionMessage varchar(max) out   
as   
if @OpCode=41  
BEGIN  
  select ROW_NUMBER() over(order by OrderNo) as RowNumber, * into #tbl FROM (             
  SELECT CONVERT(varchar(10),OrderDate,101) as OrderDate,cm.CustomerName,OrderNo,sa.Address+' ' +sa.City+' '+sa.ZipCode   
  as CustomerAddress,                                                                      
  (em.FirstName+' ' +em.LastName) as DriverName,(em1.FirstName+' ' +em1.LastName) as SalesName,om.SalesPersonAutoId,                                                                      
  Stoppage                                                                      
  FROM  ORDERMASTER AS om                                                                       
  inner join customerMaster as cm on cm.autoid=om.CustomerAutoId                                                                         
  inner join EmployeeMaster as em on em.autoid=om.Driver                                                                           
  inner join EmployeeMaster as em1 on em1.autoid=om.SalesPersonAutoId                                                                    
  inner join shippingAddress as sa on sa.Autoid=om.ShipAddrAutoiD WHERE om.STATUS=4                                                                       
  ) AS Tbl   
   
  SELECT * FROM #tbl                                          
  WHERE @PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)      
  
  SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize=0 then COUNT(RowNumber) else @PageSize end AS PageSize,  
  @PageIndex AS PageIndex,Format(getdate(),'MM/dd/yyyy hh:mm tt') as PrintDate FROM #tbl   
END