

 ALTER PROCEDURE [dbo].[ProcStockReceiverMaster]                                
@Opcode INT=NULL,    
@UserAutoId int = null,                                
@BillAutoId INT=NULL,                                
@VendorAutoId int=null,                                
@BillNo VARCHAR(20)=NULL,                                
@BillDate DATETIME=NULL,                                
@Remarks VARCHAR(200)=NULL,                                
@BillItemsAutoId INT=NULL,                                
@ProductAutoId INT=NULL,                                
@UnitAutoId INT=NULL,                                
@Quantity INT=NULL,                                
@TotalPieces INT=NULL,                                
@QtyPerUnit INT=NULL,                                
@PageIndex INT = 1,                                
@PageSize INT = 10,                                
@RecordCount INT =null,                                
@FromBillDate DATETIME=null,                                
@ToBillDate DATETIME=null,                                
@BasePrice decimal(10,2)= null,                                
@RetailMIN decimal(10,2)= null,                                
@CostPrice decimal(10,2)= null,                                
@SRP decimal(10,2)= null,                                
@WholesaleMinPrice decimal(10,2)= null,                                
@ItemAutoId int= null,                                
@TableValue DT_BillItemList readonly,                                
@TblUpdateItems DT_UpdateBillItems readonly,                                
@Status  int =NULL,                                
@Barcode VARCHAR(50)=NULL,                                
@BarcodeCount INT=NULL,                                
@DraftAutoId int =null out,                                  
@isException bit out,                                
@exceptionMessage varchar(max) out                                
                                
AS                                
BEGIN                                
  Begin Try                                
  Set @isException=0                                
  Set @exceptionMessage='Success'                                
                                
		If @Opcode=11                                  
		BEGIN                                
			Begin Try                                
			BEGIN TRAN                      
				if exists (select AutoId from [dbo].[Draft_StockEntry] where BillNo = trim(@BillNo))                
				begin                 
					Set @isException=1                            
					Set @exceptionMessage='This bill no already processed.'                
				end   
				else if exists (select AutoId from [dbo].[StockEntry] where BillNo = trim(@BillNo))                
				begin                 
					Set @isException=1                            
					Set @exceptionMessage='This bill no already processed.'                
				end   
				else                
				begin                     
					INSERT INTO [dbo].[Draft_StockEntry] ([BillNo],[BillDate],[Remarks], [VendorAutoId],STATUS)                                
					VALUES(@BillNo,@BillDate,@Remarks,@VendorAutoId,@Status)                                
                                
					SET @BillAutoId = SCOPE_IDENTITY()                                
                                
					INSERT INTO [dbo].[Draft_StockItemMaster] ([BillAutoId],[ProductAutoId],[UnitAutoId],[Quantity],[TotalPieces],[QtyPerUnit],[Price])                                
					SELECT @BillAutoId,tb.[ProductAutoId],tb.[UnitAutoId],tb.[Quantity],tb.[TotalPieces],tb.[QtyPerUnit],                                
					isnull((select top 1 costPrice from packingdetails as pd where pd.ProductAutoId=tb.ProductAutoId and pd.UnitType=tb.UnitAutoId),0) FROM @TableValue AS tb                                 
				END                           
			COMMIT TRANSACTION                                
			End Try                                
			Begin Catch                                
			ROLLBACK TRAN                                
			Set @isException=1                                
			Set @exceptionMessage=ERROR_MESSAGE()                                
			End Catch                                 
		END  
		ELSE IF @Opcode=12                                  
		BEGIN                                
			Begin Try                                
			BEGIN TRAN  
			if exists (select AutoId from [dbo].[Draft_StockEntry] where BillNo = trim(@BillNo))                
			begin                 
				Set @isException=1                            
				Set @exceptionMessage='This bill no already processed.'                
			end   
			else if exists (select AutoId from [dbo].[StockEntry] where BillNo = trim(@BillNo))                
			begin                 
				Set @isException=1                            
				Set @exceptionMessage='This bill no already processed.'                
			end   
			else                
			begin  
				IF ISNULL(@DraftAutoId,0)=0                                                                                                                                                    
				BEGIN  
					INSERT INTO [dbo].[Draft_StockEntry] ([BillNo],[BillDate],[Remarks], [VendorAutoId],STATUS)                                
					VALUES(@BillNo,@BillDate,@Remarks,@VendorAutoId,@Status)   

					SET @BillAutoId = SCOPE_IDENTITY()        
					set @DraftAutoId=@BillAutoId    
	
					INSERT INTO [dbo].[Draft_StockItemMaster]([BillAutoId],[ProductAutoId],[UnitAutoId],[Quantity],[TotalPieces],[QtyPerUnit],[Price])
					values(@BillAutoId,@ProductAutoId,@UnitAutoId,@Quantity,@TotalPieces,@QtyPerUnit,isnull((select top 1 costPrice from packingdetails as pd where pd.ProductAutoId=@ProductAutoId and pd.UnitType=@UnitAutoId),0))                                                                       
					select @DraftAutoId      
				end   
				else                
				begin  
					IF NOT EXISTS(SELECT * FROM [Draft_StockItemMaster] WHERE BillAutoId= @DraftAutoId and ProductAutoId=@ProductAutoId )
					BEGIN
						update  [dbo].[Draft_StockEntry] set [BillNo]=@BillNo,[BillDate]=@BillDate,[Remarks]=@Remarks where AutoId=@DraftAutoId                                
     
						INSERT INTO [dbo].[Draft_StockItemMaster]([BillAutoId],[ProductAutoId],[UnitAutoId],[Quantity],[TotalPieces],[QtyPerUnit],[Price])
						values(@DraftAutoId,@ProductAutoId,@UnitAutoId,@Quantity,@TotalPieces,@QtyPerUnit,isnull((select top 1 costPrice from packingdetails as pd where pd.ProductAutoId=@ProductAutoId and pd.UnitType=@UnitAutoId),0))                                                                           
					END
					else
					BEGIN
						update  [dbo].[Draft_StockEntry] set [BillNo]=@BillNo,[BillDate]=@BillDate,[Remarks]=@Remarks where AutoId=@DraftAutoId   
						update [dbo].[Draft_StockItemMaster] set [Quantity]=[Quantity]+@Quantity,
						[TotalPieces]=[TotalPieces]+@TotalPieces
						where BillAutoId= @DraftAutoId and ProductAutoId=@ProductAutoId and UnitAutoId=@UnitAutoId
					END	
				END   
			END
			COMMIT TRANSACTION                                
			End Try                                
			Begin Catch                                
			ROLLBACK TRAN                                
			Set @isException=1                                
			Set @exceptionMessage='Oops! Something went wrong.Please try later'                               
			End Catch                                 
		END 
		else If @Opcode=21            
		BEGIN                                
			BEGIN TRY                                
			BEGIN TRAN    
				if exists (select AutoId from [dbo].[Draft_StockEntry] where BillNo = trim(@BillNo))                
				begin                 
					Set @isException=1                            
					Set @exceptionMessage='This bill no already processed.'                
				end   
				else if exists (select AutoId from [dbo].[StockEntry] where BillNo = trim(@BillNo))                
				begin                 
					Set @isException=1                            
					Set @exceptionMessage='This bill no already processed.'                
				end   
				else                
				begin  
					UPDATE [dbo].[Draft_StockEntry] SET [BillNo]=@BillNo,[BillDate]=@BillDate,                                
					[Remarks]=@Remarks,vendorAutoId=@VendorAutoId,                               
					status=@Status WHERE [AutoId]=@BillAutoId 
                                   
					DELETE from [Draft_StockItemMaster] where BillAutoId=@BillAutoId                                 
                                     
					INSERT INTO [dbo].[Draft_StockItemMaster] ([BillAutoId],[ProductAutoId],[UnitAutoId],[Quantity],[TotalPieces],[QtyPerUnit],[Price])                                
					SELECT @BillAutoId,tb.[ProductAutoId],tb.[UnitAutoId],tb.[Quantity],tb.[TotalPieces],tb.[QtyPerUnit],                         
					case when @Status=4 then tb.Price else                               
					isnull((select top 1 costPrice from packingdetails as pd where pd.ProductAutoId=tb.ProductAutoId and pd.UnitType=tb.UnitAutoId),0)                               
					end                              
					FROM @TableValue AS tb                                 
					IF @Status=4                                
					BEGIN                                
                           
						INSERT INTO [dbo].[StockEntry] ([BillNo],[BillDate],[Remarks], [VendorAutoId],CreatedBy,UpdatedBy,Reference_Code) 
						VALUES(@BillNo,@BillDate,@Remarks,@VendorAutoId,@UserAutoId,@UserAutoId,@BillNo)                              
						SET @BillAutoId = SCOPE_IDENTITY()                                
    
						INSERT INTO [dbo].[BillItems] ([BillAutoId],[ProductAutoId],[UnitAutoId],[Quantity],[QtyPerUnit],[Price])                                
						SELECT @BillAutoId,tb.[ProductAutoId],tb.[UnitAutoId],tb.[Quantity],tb.[QtyPerUnit], tb.Price
						FROM @TableValue AS tb    
    
						insert into [dbo].[ProductStockUpdateLog]([ProductId],[OledStock],[NewStock],[UserAutoId],[DateTime],ReferenceId,ActionRemark,ReferenceType)    
						SELECT pm.ProductId,Stock,(ISNULL(stock,0)+ISNULL(newStock,0)) as [NewStock],@UserAutoId,GETDATE(),@BillAutoId,('Stock received by Inventory Manager'),'[BillItems]'
						from (
						SELECT ProductAutoId,sum(TotalPieces) AS NewStock FROM [BillItems] WHERE BillAutoId=@BillAutoId
						GROUP BY ProductAutoId
						) as t  inner join productMaster as pm on pm.AutoId=t.ProductAutoId
                                   
						UPDATE PM SET [Stock]= isnull([Stock],0)+isnull(t.NewStock,0) FROM [ProductMaster] AS PM 
						INNER JOIN (
						SELECT ProductAutoId,sum(TotalPieces) AS NewStock FROM [BillItems] WHERE BillAutoId=@BillAutoId
						GROUP BY ProductAutoId
						) as t on t.ProductAutoId=pm.AutoId    
                       
					END                                
				END                 
			COMMIT TRANSACTION                                
			END TRY                                
			BEGIN CATCH                                
			ROLLBACK TRAN                                
				Set @isException=1                                
				Set @exceptionMessage='Oops! Something went wrong.Please try later.'   --ERROR_MESSAGE()                                
			End Catch                                 
		END     
		ELSE If @Opcode=22            
		BEGIN                                
			update [dbo].[Draft_StockItemMaster] set [Quantity]=@Quantity,
			[TotalPieces]=@TotalPieces
			where BillAutoId= @DraftAutoId and ProductAutoId=@ProductAutoId and UnitAutoId=@UnitAutoId                                               
		END     
		else if @Opcode=31                                  
		BEGIN                                
			DELETE FROM [dbo].[BillItems] WHERE [BillAutoId]=@BillAutoId                                
			DELETE FROM [dbo].[StockEntry] WHERE [AutoId]=@BillAutoId                                
		END  
		else If @Opcode=32                                  
		BEGIN                                
			DELETE FROM [dbo].Draft_StockItemMaster WHERE [BillAutoId]=@BillAutoId                                
			DELETE FROM [dbo].Draft_StockEntry WHERE [AutoId]=@BillAutoId                                
		END  
		ELSE IF @Opcode=41                                
		BEGIN                                
			SELECT [AutoId],Convert(varchar(10),[ProductId]) + '--' + [ProductName] as [ProductName] FROM [dbo].[ProductMaster]                               
		END 
		ELSE IF @Opcode=42                                
		BEGIN                     
		--Start                         
			SELECT UM.AutoId AS AutoId, UM.[UnitType] AS UnitType, [Qty] FROM [dbo].[PackingDetails] AS PD                    
			INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = PD.UnitType WHERE PD.[ProductAutoId] = @ProductAutoId                  
               
			SELECT PM.PackingAutoId from [dbo].[ProductMaster] AS PM                   
			WHERE AutoId = @ProductAutoId order by PM.PackingAutoId               
		--End By Rizwan Ahmad on 12-09-2019                            
		END             
	   ELSE IF @Opcode=43                                
	   BEGIN                                
			select ROW_NUMBER() over(order by SE.[BillDate] desc,se.[AutoId] desc) as RowNumber, se.[AutoId],vm.VendorName,[BillNo],
			sm.StatusType,CONVERT(VARCHAR(20), SE.[BillDate], 101) AS BillDate,                                
			(SELECT COUNT(distinct [ProductAutoId]) from [dbo].[Draft_StockItemMaster] WHERE BillAutoId=SE.AutoId) AS NoOfItems,                                 
			isnull((SELECT sum(Quantity*price) from [dbo].[Draft_StockItemMaster] WHERE BillAutoId=SE.AutoId),0.00) AS TotalAmount,                                
			[Remarks] into #Result43 FROM [dbo].[Draft_StockEntry] AS SE                                 
			left join vendormaster as vm on vm.autoid=se.vendorautoid                                 
			INNER JOIN StatusMaster as sm on sm.AutoId=SE.Status and sm.Category='inv'                                
			WHERE (@FromBillDate is null or @FromBillDate='' or @ToBillDate is null or @ToBillDate=''or 
			(SE.[BillDate] between @FromBillDate and @ToBillDate))                                
			and (@BillNo is null or @BillNo='' or [BillNo] LIKE '%' + @BillNo + '%')                                
			and(@VendorAutoId=0 or VendorAutoId=@VendorAutoId)  
			order by SE.AutoId desc
             
			SELECT * FROM #Result43            
			WHERE @PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 
			AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)             
			SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize=0 then COUNT(RowNumber) 
			else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Result43            
		END                                
		ELSE IF @Opcode=49                                
		BEGIN             
			SELECT ROW_NUMBER() OVER(ORDER BY  BD desc,[BillNo]) AS RowNumber, * into #Temp                   
			FROM                  
			(                                  
			SELECT se.[AutoId],vm.VendorName,[BillNo],sm.StatusType,SE.[BillDate] AS BD,CONVERT(VARCHAR(20), SE.[BillDate], 101) AS BillDate,                                
			(SELECT COUNT(distinct [ProductAutoId]) from [dbo].[Draft_StockItemMaster] WHERE BillAutoId=SE.AutoId) AS NoOfItems,                                 
			isnull((SELECT sum(Quantity*price) from [dbo].[Draft_StockItemMaster] WHERE BillAutoId=SE.AutoId),0.00) AS TotalAmount,                                
			[Remarks] FROM [dbo].[Draft_StockEntry] AS SE                                 
			left join vendormaster as vm on vm.autoid=se.vendorautoid                                 
			INNER JOIN StatusMaster as sm on sm.AutoId=SE.Status and sm.Category='inv'                                
			WHERE 
			(@FromBillDate is null or @ToBillDate is null or (CONVERT(date,SE.BillDate)                                    
			between convert(date,@FromBillDate) and CONVERT(date,@ToBillDate)))    
			and (@BillNo is null or @BillNo='' or [BillNo] LIKE '%' + @BillNo + '%')                                
			and(@VendorAutoId=0 or VendorAutoId=@VendorAutoId)                                 
			and SE.status> 1 and (ISnull(@Status,0)=0 or SE.STATUS=@Status)                                 
			) AS T ORDER BY  BD desc,[BillNo]              
              
			SELECT * FROM  #Temp                 
			WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))                  
                     
			SELECT COUNT(AutoId) AS RecordCount, case when @PageSize=0 then COUNT(RowNumber) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Temp                  
                                           
		END                                
		ELSE IF @Opcode=44                                
		BEGIN                                
			SELECT SE.[AutoId],[BillNo],CONVERT(VARCHAR(20), SE.[BillDate], 101) AS BillDate,[Remarks],[VendorAutoId],                                
			SM.StatusType AS Status,SM.AutoId as StatusCode                                
			FROM [dbo].[Draft_StockEntry] AS SE                                
			INNER JOIN StatusMaster AS SM ON SM.AutoId=SE.Status AND SM.Category='inv' WHERE SE.[AutoId]=@BillAutoId                                
                                
			SELECT BI.[AutoId],[ProductAutoId],PM.ProductId,PM.ProductName,[UnitAutoId],UM.UnitType,[Quantity],[TotalPieces],[QtyPerUnit],
			[Price], convert(decimal(18,2),([Quantity]*[Price])) as[TotalPrice] ,                                
			(SELECT TOP 1 Price FROM PackingDetails AS PD WHERE BI.ProductAutoId=PD.ProductAutoId AND BI.UnitAutoId=PD.UnitType)                                
			AS BasePrice                                
			FROM [dbo].[Draft_StockItemMaster] AS BI                                 
			INNER JOIN [dbo].[ProductMaster] As PM ON PM.AutoId = BI.[ProductAutoId]                                
			INNER JOIN [dbo].[UnitMaster] AS UM ON UM.[AutoId] = BI.[UnitAutoId]                                 
			WHERE [BillAutoId]=@BillAutoId Order By BI.[AutoId] asc                                
			SELECT [AutoId],CONVERT(VARCHAR(10),[ProductId]) + '--' + [ProductName] as [ProductName] FROM [dbo].[ProductMaster]                                
		END                                
		ELSE IF @Opcode=45                                
		BEGIN  
		 
			if  exists (SELECT [ProductAutoId] FROM [dbo].[ItemBarcode] WHERE [Barcode]=@Barcode)
			BEGIN
				SET @ProductAutoId= (SELECT [ProductAutoId] FROM [dbo].[ItemBarcode] WHERE [Barcode]=@Barcode)                                  
				SET @UnitAutoId=(SELECT [UnitAutoId] FROM [dbo].[ItemBarcode] WHERE [Barcode]=@Barcode)
				set @QtyPerUnit= (select Qty from PackingDetails where UnitType=@UnitAutoId and ProductAutoId=@ProductAutoId) 
                                    
				SELECT pm.AutoId as ProductAutoId,ProductId,ProductName,UM.AutoId AS UnitAutoId, UM.[UnitType] AS UnitType, [Qty],                  
				pd.CostPrice FROM [dbo].[PackingDetails] AS PD                                
				INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = PD.UnitType                                
				INNER JOIN [dbo].[ProductMaster] as pm on pm.AutoId=PD.ProductAutoId                     
				WHERE PD.[ProductAutoId] = @ProductAutoId                                    
				AND PD.UnitType =@UnitAutoId     
	
				IF ISNULL(@DraftAutoId,0)=0                                                                                                                                                    
				BEGIN  
					INSERT INTO [dbo].[Draft_StockEntry] ([BillNo],[BillDate],[Remarks], [VendorAutoId],STATUS)                                
					VALUES(@BillNo,@BillDate,@Remarks,@VendorAutoId,1)   

					SET @BillAutoId = SCOPE_IDENTITY()        
					set @DraftAutoId=@BillAutoId    
	
					INSERT INTO [dbo].[Draft_StockItemMaster]([BillAutoId],[ProductAutoId],[UnitAutoId],[Quantity],[TotalPieces],[QtyPerUnit],[Price])
					values(@BillAutoId,@ProductAutoId,@UnitAutoId,1,1*@QtyPerUnit,@QtyPerUnit,isnull((select top 1 costPrice from packingdetails as pd where pd.ProductAutoId=@ProductAutoId and pd.UnitType=@UnitAutoId),0))                                                                       
					select @DraftAutoId as  DraftAutoId    
				end   
				else                
				begin  
					IF NOT EXISTS(SELECT * FROM [Draft_StockItemMaster] WHERE BillAutoId= @DraftAutoId and ProductAutoId=@ProductAutoId)
					BEGIN    
						update  [dbo].[Draft_StockEntry] set [BillNo]=@BillNo,[BillDate]=@BillDate,[Remarks]=@Remarks where AutoId=@DraftAutoId  
						INSERT INTO [dbo].[Draft_StockItemMaster]([BillAutoId],[ProductAutoId],[UnitAutoId],[Quantity],[TotalPieces],[QtyPerUnit],[Price])
						values(@DraftAutoId,@ProductAutoId,@UnitAutoId,1,1*@QtyPerUnit,@QtyPerUnit,isnull((select top 1 costPrice from packingdetails as pd where pd.ProductAutoId=@ProductAutoId and pd.UnitType=@UnitAutoId),0))                                                                       
						select @DraftAutoId as  DraftAutoId 
					END
					else
					BEGIN
						update  [dbo].[Draft_StockEntry] set [BillNo]=@BillNo,[BillDate]=@BillDate,[Remarks]=@Remarks where AutoId=@DraftAutoId  

						update [dbo].[Draft_StockItemMaster] set [Quantity]=[Quantity]+1,[TotalPieces]=(Quantity+1)*@QtyPerUnit
						where BillAutoId= @DraftAutoId and ProductAutoId=@ProductAutoId and UnitAutoId=@UnitAutoId
						select @DraftAutoId as DraftAutoId 
					END
				END
			END
			else 
			BEGIN
				Set @isException=1                                
				Set @exceptionMessage='Invalid Barcode' 
			END

		END 
		ELSE IF @Opcode=46                                
		BEGIN                                   
			IF NOT EXISTS(SELECT [Barcode] FROM [dbo].[ItemBarcode] WHERE [Barcode]=@Barcode)                   
			BEGIN                                
			INSERT INTO [dbo].[ItemBarcode] ([ProductAutoId],[UnitAutoId],[Barcode]) Values(@ProductAutoId,@UnitAutoId,@Barcode)                                
			END                                
		END                                
		ELSE IF @Opcode=47                                
		BEGIN                                
			SELECT COUNT([Barcode]) AS BarcodeCount FROM [dbo].[ItemBarcode] WHERE [ProductAutoId]=@ProductAutoId                                 
			AND [UnitAutoId]=@UnitAutoId                                     
			SELECT UM.AutoId AS AutoId,PD.CostPrice as Price FROM [dbo].[PackingDetails] AS PD                                
			INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = PD.UnitType                                 
			WHERE PD.[ProductAutoId] = @ProductAutoId AND pd.[UnitType]=@UnitAutoId                                        
		END                    
		ELSE IF @Opcode=48                                
		BEGIN                                
			SELECT PD.AutoId as ItemAutoId,pm.ProductId,pm.ProductName,PD.Price,UM.UnitType,                                
			PD.Qty,P_SRP as SRP,CostPrice,MinPrice,P_CommCode as CommCode,Location,WHminPrice FROM PackingDetails AS PD                                 
			INNER JOIN UnitMaster AS UM ON UM.AutoId=PD.UnitType                                
			INNER JOIN ProductMaster As pm on pm.AutoId=PD.ProductAutoId                                
			WHERE ProductAutoId=@ProductAutoId AND PD.UnitType=@UnitAutoId                                
		END              
		ELSE IF @Opcode = 50                      
		BEGIN 
			select
			(
			SELECT AutoId as AI,[VendorName] as VN FROM [dbo].[VendorMaster] WHERE [Status]=1 order by VN
			for json path
			) as vendor,
			(
			Select AutoId,StatusType from StatusMaster where Category='inv' and AutoId not in (1) 
			for json path
			) as status
			for json path
		END                
		ELSE IF @Opcode=23           
		BEGIN                                
			SELECT @ProductAutoId=ProductAutoId,@UnitAutoId=UnitType,@QtyPerUnit=Qty FROM PackingDetails where AutoId =@ItemAutoId
	 
			insert into PackingDetailsUpdateLog(ProductAutoId,UnitType,MinPrice,CostPrice,Price,WHminPrice,UpdatedByAutoId,DateTime)    
			select @ProductAutoId,pd.UnitType,pd.MinPrice,pd.CostPrice,pd.Price,pd.WHminPrice,@UserAutoId,GETDATE() from PackingDetails pd    
			inner join UnitMaster um on um.AutoId = pd.UnitType    
			where ProductAutoId = @ProductAutoId                               
                        
			IF EXISTS(Select AutoId from PackingDetails as pd where ProductAutoId=@ProductAutoid AND Pd.UnitType=@UnitAutoId
			AND Price!=CAST(@BasePrice AS decimal(10,4)))
			BEGIN 
				insert into ProductPricingInPriceLevel_log([AutoId],[PriceLevelAutoId],[ProductAutoId],[UnitAutoId],[CustomPrice],
				[createDated],[UpdatedBy])
				SELECT [AutoId],[PriceLevelAutoId],[ProductAutoId],[UnitAutoId],[CustomPrice],GETDATE(),@UserAutoId FROM
				ProductPricingInPriceLevel WHERE ProductAutoId=@ProductAutoId order by AutoId desc

				DELETE FROM [ProductPricingInPriceLevel] WHERE ProductAutoId=@ProductAutoId
			END

			UPDATE PackingDetails SET                                 
			Price=CAST((@BasePrice/@QtyPerUnit)*Qty AS decimal(10,4)),MinPrice=CAST((@RetailMIN/@QtyPerUnit)*Qty AS decimal(10,4)),                                    
			CostPrice=CAST((@CostPrice/@QtyPerUnit)*Qty AS decimal(10,4)),                                
			WHminPrice=CAST((@WholesaleMinPrice/@QtyPerUnit)*Qty AS decimal(10,4)) where ProductAutoId=@ProductAutoId                          
                           
			EXEC [dbo].[ProcPriceLevelDeleteChangeCostPrice] @EmpAutoId=@UserAutoId               
		END  
		ELSE IF @Opcode=51                                
		BEGIN              
			if  exists (SELECT [ProductAutoId] FROM [dbo].[ItemBarcode] WHERE [Barcode]=@Barcode)
			BEGIN
				SET @ProductAutoId= (SELECT [ProductAutoId] FROM [dbo].[ItemBarcode] WHERE [Barcode]=@Barcode)                                  
				SET @UnitAutoId=(SELECT [UnitAutoId] FROM [dbo].[ItemBarcode] WHERE [Barcode]=@Barcode)
				set @QtyPerUnit= (select Qty from PackingDetails where UnitType=@UnitAutoId and ProductAutoId=@ProductAutoId) 
                                    
				SELECT pm.AutoId as ProductAutoId,ProductId,ProductName,UM.AutoId AS UnitAutoId, UM.[UnitType] AS UnitType, [Qty],                  
				convert(decimal(13,2),pd.CostPrice) as CostPrice FROM [dbo].[PackingDetails] AS PD                                
				INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = PD.UnitType                                
				INNER JOIN [dbo].[ProductMaster] as pm on pm.AutoId=PD.ProductAutoId                     
				WHERE PD.[ProductAutoId] = @ProductAutoId                                    
				AND PD.UnitType =@UnitAutoId     
	
				IF NOT EXISTS(SELECT * FROM [Draft_StockItemMaster] WHERE BillAutoId= @DraftAutoId and ProductAutoId=@ProductAutoId)
				BEGIN      
					INSERT INTO [dbo].[Draft_StockItemMaster]([BillAutoId],[ProductAutoId],[UnitAutoId],[Quantity],[TotalPieces],[QtyPerUnit],[Price])
					values(@DraftAutoId,@ProductAutoId,@UnitAutoId,1,1*@QtyPerUnit,@QtyPerUnit,isnull((select top 1 costPrice from packingdetails as pd where pd.ProductAutoId=@ProductAutoId and pd.UnitType=@UnitAutoId),0))                                                                       
					select @DraftAutoId as  DraftAutoId 
				END
				else
				BEGIN  

					update [dbo].[Draft_StockItemMaster] set [Quantity]=[Quantity]+1,[TotalPieces]=(Quantity+1)*@QtyPerUnit
					where BillAutoId= @DraftAutoId and ProductAutoId=@ProductAutoId and UnitAutoId=@UnitAutoId
					select @DraftAutoId as DraftAutoId 
				END
	  
				Exec [dbo].[ProcBarcodeScanning_Log] @Barcode=@Barcode,@ScannedBy=@UserAutoId,@ActionRemarks='Scanned product by barcode',
				@ModuleName='Purchase Order > By Vendor > Generate PO (Inventory Manager)'
			END 
		else 
		BEGIN
			Set @isException=1                                
			Set @exceptionMessage='Invalid Barcode' 
		END
	END
  END TRY                                
  BEGIN CATCH                                
    Set @isException=1                                
    Set @exceptionMessage='Oops! Something went wrong.Please try later.'                              
  END CATCH                                
END   
