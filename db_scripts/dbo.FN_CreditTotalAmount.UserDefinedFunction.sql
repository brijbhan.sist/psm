USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_CreditTotalAmount]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Alter table CreditMemoMaster drop column [TotalAmount],[NetAmount]
Drop function [FN_CreditTotalAmount]
go
CREATE or alter FUNCTION  [dbo].[FN_CreditTotalAmount]
(
	 @CreditAutoId int
)
RETURNS decimal(18,2)
AS
BEGIN
	DECLARE @GrandTotal decimal(18,2), @Creditmemotype int;
	set @Creditmemotype=(select isnull(CreditMemoType,0) FROM CreditMemoMaster WHERE CreditAutoId=@CreditAutoId)
	if @Creditmemotype=1
	begin
	SET @GrandTotal=(SELECT (round((([GrandTotal]-[OverallDiscAmt])),(0))) FROM CreditMemoMaster WHERE CreditAutoId=@CreditAutoId)
	end
	else 
	BEGIN 
		SET @GrandTotal=(SELECT (round(((([GrandTotal]-[OverallDiscAmt]))+[TotalTax])+[MLTax]+[WeightTaxAmount],(0))) FROM CreditMemoMaster WHERE CreditAutoId=@CreditAutoId)
	END
	RETURN @GrandTotal
END
------------------------------
GO

Alter table CreditMemoMaster Add [NetAmount]  AS ([dbo].[FN_CreditTotalAmount]([CreditAutoId]))
Alter table CreditMemoMaster Add [TotalAmount]  AS ([dbo].[FN_CreditTotalAmount]([CreditAutoId]))
