USE [psmnj.a1whm.com]
GO
drop procedure ProcPurchaseOrder
drop procedure ProcPOInventoryManager
DROP TYPE [dbo].[DT_dtPOMManagePrice] 
/****** Object:  UserDefinedTableType [dbo].[DT_dtPOMManagePrice]    Script Date: 12-19-2020 05:12:21 ******/
DROP TYPE [dbo].[DT_dtPOMManagePrice]
GO

/****** Object:  UserDefinedTableType [dbo].[DT_dtPOMManagePrice]    Script Date: 12-19-2020 05:12:21 ******/
CREATE TYPE [dbo].[DT_dtPOMManagePrice] AS TABLE(
	[ProductAutoId] [int] NULL,
	[UnitAutoId] [int] NULL,
	[CostPrice] [decimal](8, 3) NULL,
	[BasePrice] [decimal](8, 3) NULL,
	[RetailPrice] [decimal](8, 3) NULL,
	[WHPrice] [decimal](8, 3) NULL
)
GO


