--ALTER TABLE [dbo].[SubCategoryMaster] ADD SubcategoryTax int
--ALTER TABLE [dbo].[SubCategoryMaster] ADD  DEFAULT ((0)) FOR [SubcategoryTax]
ALTER PROCEDURE [dbo].[ProcSubCategoryMaster]
	@Opcode int=NULL,
	@SubcategoryAutoId int=NULL,
	@SubcategoryId varchar(50)=NULL,
	@SubcategoryName varchar(50)=NULL,
	@Description varchar(250)=NULL,
	@Status int=NULL,
	@CategoryAutoId int=NULL,
	@SeqNo int=NULL,
	@IsShow int=NULL,
	@PageIndex INT = 1,                                                                                                                  
	@PageSize INT = 10,                                                                                                                                                   
	@RecordCount INT =null,  
	@SubcategoryTax int=NULL,
	@isException bit out,
	@exceptionMessage varchar(max) out
AS
BEGIN
	BEGIN TRY
		SET @isException=0
		SET @exceptionMessage='Success!!'

			IF @Opcode=11
				BEGIN
					IF EXISTS(SELECT SubcategoryName from SubcategoryMaster WHERE SubcategoryName=TRIM(@SubcategoryName) AND CategoryAutoId=@CategoryAutoId)
						BEGIN
							SET @isException=1
							SET @exceptionMessage='Already Exists'
						END
					ELSE
						BEGIN TRY							
							BEGIN TRAN
								SET @SubcategoryId=(select dbo.SequenceCodeGenerator('SubcategoryId'))
								INSERT INTO SubcategoryMaster (SubcategoryId, SubcategoryName, Description, Status, CategoryAutoId,SeqNo,IsShow,SubcategoryTax) 
								VALUES (@SubcategoryId, TRIM(@SubcategoryName), @Description, @Status, @CategoryAutoId,@SeqNo,@IsShow,@SubcategoryTax)
								UPDATE SequenceCodeGeneratorMaster SET currentSequence=currentSequence+1 WHERE SequenceCode='SubcategoryId'
								IF(DB_NAME()='psmnj.a1whm.com')
								BEGIN
									EXEC [dbo].[ProcSubCategoryMaster_ALL] @SubcategoryId=@SubcategoryId
								END
						COMMIT TRANSACTION
						END TRY
						BEGIN CATCH
							ROLLBACK TRAN
								SET @isException=1
								SET @exceptionMessage=ERROR_MESSAGE()
						END CATCH
				END
				ELSE IF @Opcode=21
					BEGIN
						IF EXISTS(SELECT SubcategoryName from SubcategoryMaster WHERE SubcategoryName=TRIM(@SubcategoryName)  AND CategoryAutoId=@CategoryAutoId 
						AND [SubcategoryId]!= @SubcategoryId)
							BEGIN
								SET @isException=1
								SET @exceptionMessage='Already Exists'
							END
						ELSE 
							BEGIN
								BEGIN TRY							
								BEGIN TRAN
										UPDATE [dbo].[SubCategoryMaster] SET [SubcategoryName]=TRIM(@SubcategoryName), [Description]=@Description,
										[Status]=@Status, [CategoryAutoId]=@CategoryAutoId,SeqNo=@SeqNo,IsShow=@IsShow,SubcategoryTax=@SubcategoryTax WHERE [SubcategoryId]=@SubcategoryId

										IF(DB_NAME()='psmnj.a1whm.com')
										BEGIN
											EXEC [dbo].[ProcSubCategoryMaster_ALL] @SubcategoryId=@SubcategoryId
										END
								COMMIT TRANSACTION
								END TRY
								BEGIN CATCH
								ROLLBACK TRAN
								SET @isException=1
								SET @exceptionMessage=ERROR_MESSAGE()
								END CATCH
							END
					END
				ELSE IF @Opcode=31
				BEGIN TRY
					SET @SubCategoryAutoId= (SELECT AutoId FROM [SubCategoryMaster] WHERE SubcategoryId=@SubCategoryId)    
					IF NOT EXISTS(SELECT * FROM [ProductMaster] WHERE SubcategoryAutoId=@SubCategoryAutoId)      
					BEGIN      
						DELETE FROM [SubCategoryMaster] WHERE SubcategoryId=@SubCategoryId 
						IF(DB_NAME()='psmnj.a1whm.com')
						BEGIN
							EXEC [dbo].[ProcSubCategoryMaster_ALL_Delete] @SubCategoryId=@SubcategoryId
						END
					END 
					ELSE
					BEGIN
						SET @isException = 1
						SET @exceptionMessage = 'can not delete'
					END
				END TRY
				BEGIN CATCH
					SET @isException = 1
					SET @exceptionMessage = 'can not delete'
				END CATCH
				ELSE IF @Opcode=41
					BEGIN
					  SELECT ROW_NUMBER() OVER(ORDER BY SubcategoryId asc) AS RowNumber, * INTO #Results FROM                              
                      (   
						SELECT [SubcategoryId], [SubcategoryName], SCM.[Description],ISNULL(STM.TaxName,'') as TaxName, CM.CategoryName,
						SM.StatusType AS Status,SCM.SeqNo,SCM.IsShow from [dbo].[SubCategoryMaster] AS SCM
						INNER JOIN [dbo].[CategoryMaster] AS CM ON SCM.[CategoryAutoId] = CM.AutoId
						INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId=SCM.Status and SM.[Category] is NULL	
						left join  [dbo].[SubTaxMaster] as STM ON STM.AutoId=SCM.SubcategoryTax 
						WHERE (@SubcategoryId IS NULL OR @SubcategoryId='' OR [SubcategoryID] LIKE '%'+ @SubcategoryId+'%')	
						AND (@SubcategoryTax IS NULL OR @SubcategoryTax=0 or SCM.SubcategoryTax=@SubcategoryTax)
						AND (@SubcategoryName IS NULL OR @SubcategoryName='' OR [SubcategoryName] LIKE '%'+ @SubcategoryName+'%')	
						AND (@CategoryAutoId IS NULL OR @CategoryAutoId=0 OR  [CategoryAutoId] = @CategoryAutoId)	
						AND (@Status IS NULL OR @Status=2 OR SCM.[Status]=@Status)	
					   ) AS t order by SubcategoryId asc                                                                                                                                                     
                                                                             
						SELECT COUNT(SubcategoryId) AS RecordCount, case when @PageSize=0 then COUNT(SubcategoryId) else @PageSize end AS PageSize,                                                              
						@PageIndex AS PageIndex FROM #Results                                                               
						SELECT * FROM #Results                                                                                                                                                                                
						WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)) 

					END
				ELSE IF @Opcode=42
					BEGIN
						SELECT [SubcategoryId], [SubcategoryName], [Description],[Status], [CategoryAutoId],SeqNo,IsShow,ISNULL(SubcategoryTax,0) as SubcategoryTax
						FROM [dbo].[SubCategoryMaster] WHERE [SubcategoryId]=@SubcategoryId
					END
				ELSE IF @Opcode=43
				BEGIN
					SELECT AutoId, CategoryName FROM CategoryMaster WHERE Status = 1 ORDER BY CategoryName ASC 
					SELECT AutoId, TaxName FROM SubTaxMaster WHERE Status = 1 ORDER BY TaxName ASC 
				END
	END TRY
	BEGIN CATCH
		SET @isException=1
		SET @exceptionMessage=ERROR_MESSAGE()
	END CATCH
END



GO
