 
ALTER PROCEDURE [dbo].[ProcManageModule]   
@OpCode int= Null,
@ModuleId VARCHAR(200)= Null,
@Module VARCHAR(200)= Null,
@iconClass VARCHAR(200)= Null,
@HasModule int= Null,
@ParentModule int= Null,
@Status int= Null,
@EmpAutoId int=Null,
@AutoId int=Null,
@SequenceNo int=Null,
@MenuType int=null,
@PageIndex INT = 1,                                                                                                                  
@PageSize INT = 10,                                                                                                                                                   
@RecordCount INT =null,        
@isException bit out,  
@exceptionMessage varchar(max) out  
AS  
BEGIN  
  BEGIN TRY  
  SET @isException = 0  
  SET @exceptionMessage = 'success'
  IF @OpCode = 42  
		BEGIN   
			BEGIN TRY  
			SELECT * FROM Module WHERE Status=1 order by ModuleName asc
			END TRY  
			BEGIN CATCH  
			SET @exceptionMessage = 'Oops! Something went wrong.Please try later.'  
			SET @isException = 1  
			END CATCH  
		END
	ELSE IF @OpCode = 41
			BEGIN
				BEGIN TRY

				  SELECT ROW_NUMBER() OVER(ORDER BY AutoId) AS RowNumber, * INTO #Results FROM                
                  ( 
					SELECT md.AutoId,md.ModuleId,md.ModuleName,md.CreatedBy,iconClass,case when md.MenuType=1 then 'Internal' else 'External' end as MenuType,
					md.CreatedOn,md.HasParent,md.ParentId,md.Status,(SELECT ModuleName FROM Module mo Where md.ParentId=mo.AutoId) AS ParentModule,SequenceNo
					FROM Module md
					WHERE (@Status IS NULL OR @Status=2 OR md.Status = @Status)
					AND (@Module IS NULL OR @Module='' OR md.ModuleName = @Module)
					  ) as t ORDER BY AutoId            
            
				  SELECT COUNT(AutoId) AS RecordCount,case when @PageSize=0 then COUNT(RowNumber) else  @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Results                       
				  SELECT * FROM #Results                
				  WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))      
				END TRY
				BEGIN CATCH
				END CATCH
			END
	ELSE IF @OpCode = 11
		   BEGIN
			   
					   SET @ModuleId=(SELECT [dbo].[SequenceCodeGenerator]('ModuleId'))  
				   
					   INSERT INTO Module (ModuleId,ModuleName,HasParent,ParentId,Status,CreatedOn,CreatedBy,IconClass,SequenceNo,MenuType)
					   VALUES(@ModuleId,@Module,@HasModule,@ParentModule,@Status,GETDATE(),@EmpAutoId,@iconClass,@SequenceNo,@MenuType)

					   UPDATE SequenceCodeGeneratorMaster SET currentSequence=currentSequence+1 WHERE PreSample='MOD' AND AutoID=@AutoId
				   
		   END
 ELSE IF @Opcode=31
   BEGIN  
    BEGIN TRY  
      DELETE FROM Module WHERE AutoId=@AutoId
	END TRY  
    BEGIN CATCH  
	 SET @isException=1  
     SET @exceptionMessage='Module already in used!!!'  
    END CATCH  
   END  
	ELSE IF @OpCode = 21
		   BEGIN
		   IF exists(SELECT ModuleId FROM Module WHERE ModuleName=@Module AND AutoId != @AutoId)    
		   BEGIN      
		   set @isException=1      
		   set @exceptionMessage='ModuleExists'      
	       END

		   ELSE
		   BEGIN
		   UPDATE Module SET ModuleId=@ModuleId,ModuleName=@Module,HasParent=@HasModule,ParentId=@ParentModule,IconClass=@iconClass,
		   Status=@Status,UpdatedOn=GETDATE(),UpdatedBy=@EmpAutoId,SequenceNo=@SequenceNo,MenuType=@MenuType WHERE AutoId=@AutoId
		   END
		   END
		     
		END TRY  
			BEGIN CATCH  
			SET @isException=1          
			SET @exceptionMessage=ERROR_MESSAGE()--'Oops! Something went wrong.Please try later.'  
			END CATCH  
		END  