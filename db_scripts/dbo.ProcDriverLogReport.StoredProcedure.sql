  
Alter Proc [dbo].[ProcDriverLogReport]  
@Opcode int=null,   
@FromDate datetime=null,  
@ToDate datetime=null,   
@PageIndex INT=1,      
@PageSize INT=10,      
@isException bit out,      
@exceptionMessage varchar(max) out  
as   
begin  
BEGIN TRY      
  Set @isException=0      
  Set @exceptionMessage='Success'      
  IF @Opcode=41      
  BEGIN                                                                         
		SELECT ROW_NUMBER() OVER(ORDER BY [AutoId]  DESC                                                                                                                                           
		) AS RowNumber, * INTO #Results from                                                         
		(  SELECT OM.AutoId,OM.PlanningId,OM.DriverAutoId,CONVERT(VARCHAR(20), OM.RouteDate, 101) AS RouteDate,emp.FirstName+SPACE(1)+emp.LastName 
		as ManagerName,emp1.FirstName+SPACE(1)+emp1.LastName  as DriverName,emp1.FirstName+SPACE(1)+emp1.LastName  as PlanningDriverName,CM.CarName,DriverNo,
		NoOfStop,FORMAT(CreateDate,'MM/dd/yyyy') as CreateDate,OM.DriverStartTime,OM.DraverEndTime
		FROM [dbo].DriverOptimoRouteLog As OM 
		INNER JOIN EmployeeMaster Emp ON Emp.AutoId=OM.ManagerAutoId
		INNER JOIN EmployeeMaster Emp1 ON Emp1.AutoId=OM.DriverAutoId
		INNER JOIN EmployeeMaster Emp2 ON Emp2.AutoId=OM.DriverPlanningAutoId
		INNER JOIN CarDetailsMaster CM ON CM.CarAutoId=OM.CarAutoId    
		WHERE (@FromDate is null or @FromDate = '' or @Todate is null or @Todate = '' or                                                                                  
		(CONVERT(date,OM.[RouteDate])  between CONVERT(date,@FromDate) and CONVERT(date,@Todate)))                                                                                                   
                                                                     
		)as t order by [AutoId] DESC                                                                                                                           
                          
		SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize=0 then COUNT(RowNumber) else @PageSize 
		end AS PageSize, @PageIndex AS PageIndex FROM #Results 
        
		SELECT * FROM #Results                                                                                                                                              
		WHERE (ISNULL(@PageSize,0)=0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))                                                                    
 END   
 END TRY      
 BEGIN CATCH      
		Set @isException=1      
		Set @exceptionMessage=ERROR_MESSAGE()      
 END CATCH       
end  
GO
