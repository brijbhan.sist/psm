USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Credit_Netmount]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
Create FUNCTION  [dbo].[FN_Credit_Netmount]
(
	 @AutoId int
)
RETURNS decimal(18,2)
AS
BEGIN
	DECLARE @NetAmount decimal(18,2)
	SET @NetAmount=isnull((SELECT (ManagerUnitPrice*AcceptedQty) FROM CreditItemMaster WHERE ItemAutoId=@AutoId),0)
	RETURN @NetAmount
END

GO
Alter table CreditItemMaster drop column [NetAmount]
Drop function [FN_Credit_Netmount]
Alter table CreditItemMaster Add [NetAmount]  AS ([dbo].[FN_Credit_Netmount]([ItemAutoId]))