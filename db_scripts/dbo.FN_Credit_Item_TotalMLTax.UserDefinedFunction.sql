USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Credit_Item_TotalMLTax]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION  [dbo].[FN_Credit_Item_TotalMLTax]
(
	 @AutoId int,
	 @CreditAutoId int
)
RETURNS decimal(18,2)
AS
BEGIN
	DECLARE @Item_TotalMLTax decimal(18,2)=0.00,@TaxPer decimal(18,2)=0.00
	IF EXISTS(SELECT * FROM CreditMemoMaster WHERE CreditAutoId=@CreditAutoId AND ISmltaxapPly=1)
	BEGIN
		set @TaxPer =ISNULL((SELECT MLTaxPer FROM CreditMemoMaster WHERE CreditAutoId=@CreditAutoId ),0)
		SET @Item_TotalMLTax=isnull((SELECT (TotalMLTax)*@TaxPer FROM CreditItemMaster WHERE ItemAutoId=@AutoId),0)
	END
	RETURN @Item_TotalMLTax
END
----Drop function FN_Credit_Item_TotalMLTax
--alter table CreditItemMaster Drop column Item_TotalMLTax
--alter table CreditItemMaster add  Item_TotalMLTax as  [dbo].FN_Credit_Item_TotalMLTax(ItemAutoid,CreditAutoId)
GO
