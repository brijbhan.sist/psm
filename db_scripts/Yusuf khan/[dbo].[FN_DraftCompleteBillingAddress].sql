USE [psmnj.a1whm.com]

ALTER TABLE [dbo].[DraftBillingAddress] DROP COLUMN [CompleteBillingAddress]
GO
alter FUNCTION  [dbo].[FN_DraftCompleteBillingAddress] 
(   
  @AutoId int   
)  
RETURNS varchar(Max)  
AS  
BEGIN  
	DECLARE @CompleteAddress varchar(MAX)
	set @CompleteAddress=(SELECT [Address]+', '+case when Address2!=null then [Address2]+',' else '' end +'<br/>'+City+', '+state+', '+Zipcode FROM DraftBillingAddress WHere Autoid=@AutoId)
	RETURN @CompleteAddress  
END 
GO
ALTER TABLE [dbo].[DraftBillingAddress] ADD [CompleteBillingAddress]  AS ([dbo].[FN_DraftCompleteBillingAddress]([AutoId]))