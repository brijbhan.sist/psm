--05/01/2021
alter table OrderItemMaster add Oim_Discount decimal(18,2) DEFAULT(0.00),Oim_ItemTotal decimal(18,2) DEFAULT(0.00)
alter table Delivered_Order_Items add Del_discount decimal(18,2) DEFAULT(0.00),Del_ItemTotal decimal(18,2) DEFAULT(0.00)


--04/30/2021
insert into TBL_MST_SecurityMaster(SecurityType,SecurityValue,typeEvent,description)
values(6,12345,6,'Account Check Security For Payment Edit')

Alter table DraftBillingAddress Add CompleteBillingAddress AS ([dbo].[FN_DraftCompleteBillingAddress]([AutoId]))
Alter table DraftShippingAddress Add CompleteShippingAddress AS ([dbo].[FN_DraftCompleteShippingAddress] ([AutoId]))
--08/25/2020

Alter table VendorMaster Add CompleteAddress AS ([dbo].[FN_CompleteVendorAddress] ([AutoId]))
--08/07/2020
alter table CreditItemMaster drop column [TotalPeice] 
alter table CreditItemMaster add [TotalPeice]  AS ([dbo].[FN_Credit_TotalPeice]([ItemAutoId]))

alter table CreditItemMaster add [QtyPerUnit_Fresh] [int] 
alter table CreditItemMaster add [QtyPerUnit_Damage] [int] 
alter table CreditItemMaster add [QtyPerUnit_Missing] [int] 
alter table CreditItemMaster add [QtyReturn_Fresh] [int] 
alter table CreditItemMaster add [QtyReturn_Damage] [int] 
alter table CreditItemMaster add [QtyReturn_Missing] [int] 

--07/30/2020
ALTER TABLE [dbo].[AllowQtyPiece]  WITH CHECK ADD CHECK  (([UsedQty]>=(0)))
GO
--07/29/2020
alter table [dbo].[ExpenseMaster] add PaymentSource int
alter table [dbo].[ExpenseMaster] drop column ExpenseType
--07/27/2020
alter table [dbo].[CompanyDetails] add  CompanyDistributerName varchar(50)
alter table [dbo].[ExpenseMaster] add ExpenseType int
--07/26/2020
Alter table BillingAddress Add CompleteBillingAddress AS ([dbo].[FN_CompleteBillingAddress] ([AutoId]))
Alter table ShippingAddress Add CompleteShippingAddress AS ([dbo].[FN_CompleteShippingAddress] ([AutoId]))
--07/25/2020
insert into Pattycashcategorymaster(CategoryName)values('Vendor Payment')
--07/22/2020
alter table PattyCashLogMaster add ReferenceId int
Alter table VendorPayMaster add PaymentType int
--07/21/2020
alter table [dbo].[PattyCashLogMaster] drop column Category
alter table [dbo].[ExpenseMaster] drop column Category

alter table [dbo].[PattyCashLogMaster] add  Category int
alter table [dbo].[ExpenseMaster] add  Category int
