USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_ShippingCompleteAddress]    Script Date: 07-18-2020 22:33:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Alter table Vendormaster drop column CompleteAddress
Drop function [FN_CompleteVendorAddress]
Create FUNCTION  [dbo].[FN_CompleteVendorAddress]
(   
  @AutoId int   
)  
RETURNS varchar(Max)  
AS  
BEGIN  
	DECLARE @CompleteAddress varchar(MAX)
	set @CompleteAddress=(select Address+'<br/>'+City+', '
	+(SELECT StateCode FROM State as st Where st.AutoId=VM.State)+' - '+VM.Zipcode
	 from VendorMaster as VM WHere VM.AutoId=@AutoId)
	RETURN @CompleteAddress  
END 
Alter table VendorMaster Add [CompleteAddress]  AS ([dbo].[FN_CompleteVendorAddress]([AutoId]))