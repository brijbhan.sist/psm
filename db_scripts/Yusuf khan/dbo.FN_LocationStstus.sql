USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_ShippingCompleteAddress]    Script Date: 07-18-2020 22:33:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION  [dbo].[FN_LocationStstus] 
(   
  @productid int,
  @AutoId int
)  
RETURNS  int
AS  
BEGIN  
 declare @Status int
   set @Status=(  select  
	 case when autoid=1 then (select isnull(ProductStatus,0) from [psmnj.a1whm.com].[dbo].ProductMaster where ProductId=@productid)
	 when autoid=2 then (select isnull(ProductStatus,0) from [psmct.a1whm.com].[dbo].ProductMaster where ProductId=@productid)
	 when autoid=4 then (select isnull(ProductStatus,0) from [psmpa.a1whm.com].[dbo].ProductMaster where ProductId=@productid)
	 when autoid=5 then (select isnull(ProductStatus,0) from [psmnpa.a1whm.com].[dbo].ProductMaster where ProductId=@productid)
	 when autoid=10 then (select isnull(ProductStatus,0) from [psmny.a1whm.com].[dbo].ProductMaster where ProductId=@productid)
	 when autoid=9 then (select isnull(ProductStatus,0) from [psmwpa.a1whm.com].[dbo].ProductMaster where ProductId=@productid)
	 end as Status from locationmaster where AutoId=@AutoId)
	 RETURN @Status
END 

