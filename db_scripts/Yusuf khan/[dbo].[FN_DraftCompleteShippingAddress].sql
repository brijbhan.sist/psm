USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_ShippingCompleteAddress]    Script Date: 07-18-2020 22:33:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
ALTER TABLE [dbo].[DraftShippingAddress] drop COLUMN [CompleteShippingAddress] 
GO
alter FUNCTION  [dbo].[FN_DraftCompleteShippingAddress] 
(   
  @AutoId int   
)  
RETURNS varchar(Max)  
AS  
BEGIN  
	DECLARE @CompleteAddress varchar(MAX)
	set @CompleteAddress=(SELECT [Address]+', '+case when Address2!=null then [Address2]+',' else '' end +'<br/>'+City+', '+state+', '+Zipcode FROM DraftShippingAddress WHere Autoid=@AutoId)
	RETURN @CompleteAddress  
END 
go
ALTER TABLE [dbo].[DraftShippingAddress] ADD [CompleteShippingAddress]  AS ([dbo].[FN_DraftCompleteShippingAddress]([AutoId])) 