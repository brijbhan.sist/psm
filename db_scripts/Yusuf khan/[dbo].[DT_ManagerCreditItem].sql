USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedTableType [dbo].[DT_CreditItem]    Script Date: 11/20/2019 5:05:52 AM ******/

CREATE TYPE [dbo].[DT_ManagerCreditItem] AS TABLE(
	[ProductAutoId] [int] NULL,
	[UnitAutoId] [int] NULL,
	[RequiredQty] [int] NULL,
	[QtyPerUnit] [int] NULL,
	[UnitPrice] [decimal](8, 2) NULL,
	[SRP] [decimal](8, 2) NULL,
	[Tax] [decimal](8, 2) NULL,
	[NetPrice] [decimal](8, 2) NULL,
	[QtyPerUnit_Fresh] [int] NULL, 
	[QtyPerUnit_Damage] [int] NULL,
	[QtyPerUnit_Missing] [int] NULL, 
	[QtyReturn_Fresh] [int] NULL, 
	[QtyReturn_Damage] [int] NULL, 
	[QtyReturn_Missing] [int] NULL
)
GO


