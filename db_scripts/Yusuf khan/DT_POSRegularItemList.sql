
CREATE TYPE [dbo].[DT_POSRegularItemList] AS TABLE(
	[ProductAutoId] [int] NULL,
	[UnitAutoId] [int] NULL,
	[QtyPerUnit] [int] NULL,
	[RequiredQty] [int] NULL,
	[TotalPieces] [int] NULL,
	[UnitPrice] [decimal](18, 2) NULL,
	[SRP] [decimal](18, 2) NULL,
	[GP] [decimal](18, 2) NULL,
	[Tax] [int] NULL,
	[IsExchange] [int] NULL,
	[isFreeItem] [int] NULL,
	[NetPrice] [decimal](18, 2) NULL,
	OM_MinPrice [decimal](18, 2) NULL,
    OM_CostPrice [decimal](18, 2),
	OM_BasePrice [decimal](18, 2)
)
GO