USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_ShippingCompleteAddress]    Script Date: 07-18-2020 22:33:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create FUNCTION  [dbo].[FN_CompleteShippingAddress] 
(   
  @AutoId int   
)  
RETURNS varchar(Max)  
AS  
BEGIN  
	DECLARE @CompleteAddress varchar(MAX)
	set @CompleteAddress=(SELECT [Address]+', '+case when Address2!=null then [Address2]+',' else '' end +'<br/>'+City+', '+(SELECT StateName FROM State Where AutoId=State)+', '+Zipcode FROM ShippingAddress WHere Autoid=@AutoId)
	RETURN @CompleteAddress  
END 