USE [psmnj.a1whm.com]
GO 
alter table ProductStockUpdateLog
drop column DefaultAffectedStock
go
CREATE
or alter
FUNCTION  [dbo].[FN_ ProductStock_CurrentStock]
(
	 @AutoId int
)
RETURNS varchar(100)
AS
BEGIN
	DECLARE @CurrentStock varchar(100)
	SET @CurrentStock=isnull((SELECT   cast((AffectedStock/convert(decimal(10,2),Qty)) as decimal(18,2)) FROM ProductStockUpdateLog as PS 
	inner join ProductMaster as PM on PM.ProductId=PS.ProductId
	inner join PackingDetails as pd on pd.ProductAutoId=pm.AutoId and pm.PackingAutoId=pd.UnitType 
	WHERE ps.AutoId=@AutoId),0)
	
	RETURN @CurrentStock
END
go

Alter table ProductStockUpdateLog add DefaultAffectedStock as [dbo].[FN_ ProductStock_CurrentStock](AutoId)
