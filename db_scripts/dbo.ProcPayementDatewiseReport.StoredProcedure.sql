
alter  procedure [dbo].[ProcPayementDatewiseReport]                
@Opcode INT =null,                   
@PayMentMode int=null,                
@DateFrom DATE=null,                  
@DateTo Date=null,                  
@PageIndex int=null,                   
@PageSize  int=10,                   
@isException bit out,                    
@RecordCount INT=null,                          
@exceptionMessage varchar(max) out                 
AS                  
BEGIN                  
  BEGIN TRY                  
    SET @exceptionMessage= 'Success'                    
 SET @isException=0                  
                 
 IF @Opcode=401                  
    BEGIN                  
    select AutoID as PId,PaymentMode as PM from [dbo].[PAYMENTModeMaster]  ORDER BY PM ASC    
	for json path
    end                
 IF @Opcode=402                  
    BEGIN                  
                
 select cpd.PaymentAutoId,CONVERT(date,PaymentDate) as PaymentDate,cpd.ReceivedAmount as PayAmount,CreditAmount,                
 DueamountAfter='0.00',                
 PaymentMode,ReceivedBy,EmpAutoId,PaymentRemarks, count(PaymentDate) as Numberofspan,cpd.PaymentId into #temp from                 
 CustomerPaymentDetails as cpd                
 inner join PaymentOrderDetails as pod on pod.PaymentAutoId=cpd.PaymentAutoId 
 where cpd.status=0 and (isnull( @PayMentMode,0)=0 OR cpd.PaymentMode=@PayMentMode)                
 and (ISNULL(@DateFrom,'')='' OR ISNULL(@DateTo,'') = '' OR                   
 (CONVERT(date,PaymentDate) BETWEEN CONVERT(date,@DateFrom) AND CONVERT(date,@DateTo)))                    
 group by cpd.PaymentAutoId,CONVERT(date,PaymentDate),cpd.ReceivedAmount,CreditAmount,                
 PaymentMode,ReceivedBy,EmpAutoId,PaymentRemarks,PaymentId        
                
 SELECT ROW_NUMBER() OVER(ORDER BY [PaymentDate] asc,PaymentAutoId asc) AS RowNumber, * INTO #Results from                              
 (                  
 select cpd.*,DueamountBefore=om.PayableAmount-[dbo].[SumOfRecievedAmount](pod.PaymentAutoId,pod.OrderAutoId),pod.ReceivedAmount as ApplyAmount,                
 OrderNo,om.PayableAmount                 
 as orderamount from #temp as cpd                
 inner join PaymentOrderDetails as pod on pod.PaymentAutoId=cpd.PaymentAutoId                 
 inner join OrderMaster as om on om.AutoId=pod.OrderAutoId and om.Status=11        
 )as t order by [PaymentDate] asc,PaymentAutoId asc  
 
 Select count(PaymentAutoId) as noofsp,PaymentAutoId
		 into #Results2 from #Results   
		 WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 
		 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))
		 group by PaymentAutoId,PaymentDate
   
 SELECT COUNT(*) AS RecordCount, case when @PageSize=0 then COUNT(*) else @PageSize end AS PageSize,
 @PageIndex AS PageIndex,Format(getdate(),'MM/dd/yyyy hh:mm tt') as PrintDate FROM #Results 	 

 
 SELECT PaymentAutoId,FORMAT(t.PaymentDate,'MM/dd/yyyy') as PaymentDate,PayAmount,PaymentMode as PayMode,                
 case                   
 when CreditAmount<0 then 0                
 ELSE CreditAmount end as CreditAmount                
 ,DueamountBefore,DueamountAfter=DueamountBefore-ApplyAmount,    
 (select pmm.PaymentMode from PaymentModeMaster as pmm where pmm.Autoid=t.PaymentMode)            
  as PaymentMode,(em.FirstName + ' ' + isnull(em.LastName,'')) as ReceivedBy,
  (em2.FirstName + ' ' + isnull(em2.LastName,'')) as EmpAutoId,
  (SElect noofsp from #Results2 as r2 where r2.PaymentAutoId=t.PaymentAutoId) as Numberofspan,
 PaymentRemarks,--Numberofspan,
 ApplyAmount,OrderNo,orderamount FROM #Results   as t              
 left join EmployeeMaster em on em.AutoId = ReceivedBy                  
 left join EmployeeMaster em2 on em2.AutoId = EmpAutoId  
 WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 
 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))  
               
   select (select sum(PayAmount) from #temp) as PayAmount, (select sum(isnull((case when CreditAmount<0 then 0  ELSE CreditAmount end),0.00)) from #temp) as CreditAmount   ,* from   
 (  
  SELECT sum(isnull(orderamount,0.00)) as orderamount,sum(isnull(DueamountBefore,0.00)) as DueamountBefore,sum(isnull(ApplyAmount,0.00)) as ApplyAmount,sum(isnull(DueamountBefore-ApplyAmount,0.00)) as DueamountAfter     
 , sum(isnull((case when CreditAmount<0 then 0  ELSE CreditAmount end),0.00)) as CreditAmount                
   FROM #Results       
  ) t  
 end                
END TRY                                        
BEGIN CATCH           
 Set @isException=1                                        
 Set @exceptionMessage=ERROR_MESSAGE()                                        
END CATCH                                       
END 
GO
