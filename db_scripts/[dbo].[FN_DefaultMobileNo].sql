USE [psmnj.a1whm.com]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create FUNCTION  [dbo].[FN_DefaultMobileNo] 
(   
  @AutoId int   
)  
RETURNS varchar(100)  
AS  
BEGIN  
	DECLARE @MobileNo varchar(10)
	set @MobileNo=isnull((SELECT TOP 1 MobileNo FROM CustomerContactPerson WHERE CustomerAutoId=@AutoId AND ISDefault=1),'')
	RETURN @MobileNo  
END 
go
Alter table CustomerMaster Drop column MobileNo
Alter table CustomerMaster add MobileNo As ([dbo].[FN_DefaultMobileNo](AutoId))