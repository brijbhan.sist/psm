USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_DeliveredCreditAmount]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 

CREATE FUNCTION  [dbo].[FN_DeliveredCreditAmount]
(
	 @orderAutoId int
)
RETURNS decimal(10,2)
AS
BEGIN
	DECLARE @CreditAmount decimal(10,2)
	set @CreditAmount=isnull((select CreditAmount from OrderMaster where AutoId=@orderAutoId)	,0.00)
	RETURN @CreditAmount
END

GO
