 Alter procedure ProcExceptionDetailsEmail
 AS                 
 BEGIN 

	Declare @html varchar(Max)='',@Location varchar(50),@tr varchar(max),@td varchar(max)='',@num int,@query nvarchar(MAX),@count int,
	@UserName varchar(50),@AppVersion varchar(50),@deviceID varchar(500),@functionName varchar(500),@requestContainer varchar(500)
   ,@errordetails varchar(max),@propertyName varchar(max),@errorDate varchar(50),@Createddate varchar(250),@Status varchar(15)

	select Row_Number() over (order by AutoId) as rownumber,UserName,AppVersion,deviceID,functionName,requestContainer,
	errordetails,propertyName,ISNULL(Format(errorDate,'MM/dd/yyyy hh:tt mm'),'') as errorDate,
	ISNULL(Format(Createddate,'MM/dd/yyyy hh:tt mm'),'') as Createddate
	,case when Status=1 then 'Active' else 'Inactive' end as EStatus
	into #Result from tbl_ExceptionMaster where ISNULL(Status,0)=0

	set @html='
	<table class="table tableCSS" border="1" style="width:100%;border: 1px solid black;border-collapse:collapse;">
		<thead>
			<tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
				<td style="text-align: center !important;">
					<h4 style="margin:10px;">Exception Details</h4>
				</td>
				</tr>
				<tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
				<td style="text-align: center !important;">
					<h4 style="margin:2px">'+(select CompanyDistributerName from CompanyDetails)+'</h4>
				</td>
				</tr>
				<tr style="background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a">
				<td style="text-align: center !important;">
					<h4 style="margin:2px">'+Convert(varchar(50),FORMAT(GetDate(),'MM/dd/yyyy hh:mm tt'))+'</h4>
				</td>
			</tr>
		</thead>
		<tbody></tbody>
		</table>
		<br>
		<table class="table tableCSS" border="1" style="width:100%;border: 1px solid black;border-collapse:collapse;"> 
		<thead>
		<tr>
			<td style=''text-align:center;''><b>User Name</b></td>
			<td style=''text-align:center;''><b>App Version</b></td>
			<td style=''text-align:center;''><b>Device ID</b></td>
			<td style=''text-align:center;''><b>Function Name</b></td>
			<td style=''text-align:center;''><b>Request Container</b></td>
			<td style=''text-align:center;''><b>Error Details</b></td>
			<td style=''text-align:center;''><b>Property Name</b></td>
			<td style=''text-align:center;''><b>Error Date</b></td>
			<td style=''text-align:center;''><b>Created Date</b></td>
       </tr>
	   </thead>
		<tbody>'
		SET @count= (select count(1) from #Result)
		SET @num=1
		while(@num<=@Count)
		BEGIN 
				select 
				@UserName=UserName,
				@AppVersion=AppVersion,
				@deviceID= deviceID,
				@functionName=functionName,
				@requestContainer=requestContainer,
				@errordetails=errordetails,
				@propertyName=propertyName,
				@errorDate=errorDate,
				@Createddate=Createddate
				from #Result where rownumber = @num 
				set @tr ='<tr>' 
								+	'<td style=''text-align:left;''>'+ @UserName + '</td>'
								+	'<td style=''text-align:center;''>'+ @AppVersion + '</td>'
								+	'<td style=''text-align:center;''>'+ @deviceID + '</td>'
								+	'<td style=''text-align:center;''>'+ @functionName + '</td>'
								+	'<td style=''text-align:center;''>'+ @requestContainer + '</td>'
								+	'<td style=''text-align:left;''>'+ @errordetails + '</td>'
								+	'<td style=''text-align:center;''>'+ @propertyName + '</td>'
								+	'<td style=''text-align:center;''>'+ @errorDate + '</td>'
								+	'<td style=''text-align:center;''>'+ @Createddate + '</td>'

						+ '</tr>'
		set @num = @num + 1
		SET @html=@html+@tr
		END
		SET @html=@html+'</tbody></table>' 

	-----------------------------------------Code For Email---------------------------------------------------------------
	Declare  @Subject varchar(250),@FromName varchar(500),@FromEmailId varchar(500),@Port int,@SMTPServer varchar(150),@smtp_userName varchar(150),
	@Password varchar(50),@SSL  int,@BCCEmailId varchar(max),@ToEmailId varchar(500),@CCEmailId varchar(max)
	set @Subject = 'A1WHM -' +UPPER((select CompanyDistributerName from CompanyDetails ))+' -  Exception Details' 

	SELECT @FromEmailId=FromEmail,@FromName=DisplayName,@smtp_userName=SMTP_USERNAME,@Password=SMTP_PASSWORD,
	@SMTPServer=HOST,@Port=SMTPPORT,@SSL=EnableSsl from EmailServerMaster_AWS 

	set @BCCEmailId=(select BCCEmail from Tbl_ReportEmailReceiver where Autoid=15)
	set @ToEmailId=(select ToEmail from Tbl_ReportEmailReceiver where Autoid=15)
	SET @CCEmailId=(select CCEmail from Tbl_ReportEmailReceiver where Autoid=15)
    IF @count>0
	BEGIN
		EXEC [Emailing_DB].[dbo].[ProcSubmitEmail] 
		@Opcode=11,
		@FromEmailId =@FromEmailId,
		@FromName = @FromName,
		@smtp_userName=@smtp_userName,
		@Password = @Password,
		@SMTPServer = @SMTPServer,
		@Port =@Port,
		@SSL =@SSL,
		@ToEmailId =@ToEmailId,
		@CCEmailId =@CCEmailId,
		@BCCEmailId =@BCCEmailId,  
		@Subject =@Subject,
		@EmailBody = @html,
		@SentDate ='',
		@Status =0,
		@SourceApp ='PSM',
		@SubUrl ='Exception Details', 
		@isException=0,
		@exceptionMessage='' 
	END
  END