 
alter PROCEDURE [dbo].[ProcPageMaster]   
@OpCode int= Null,  
@AutoId int= Null,  
@PageId VARCHAR(12) = Null,  
@PageName VARCHAR(100) = Null,  
@PageUrl VARCHAR(100) = Null,  
@PageAction VARCHAR(400) = Null,  
@Description VARCHAR(250) = Null,  
@Status int = Null, 
@UserType int = Null,  
@PageType int = Null,  
@ActionType int = Null,
@EmpTypeNo int = Null,
@ActionAutoId int = Null,
@ModuleAutoId int = Null,
  
@isException bit out,  
@exceptionMessage varchar(max) out  
AS  
BEGIN  
 BEGIN TRY  
  SET @isException = 0  
  SET @exceptionMessage = 'success'  
  
	IF @OpCode = 11  
	BEGIN      
			if exists(SELECT PageUrl FROM PageMaster WHERE PageUrl = @PageUrl)  
			BEGIN  
			SET @exceptionMessage = 'Page url already exist'  
			SET @isException = 1  
			END  
			
			ELSE  
		BEGIN  
			BEGIN TRY  
			SET @PageId=(SELECT dbo.SequenceCodeGenerator('PageId'))  
			  
			INSERT INTO PageMaster (PageId,PageName,PageUrl,Description,Status,Type,Role,ParentModuleAutoId)
			VALUES(@PageId,@PageName,@PageUrl,@Description,@Status,@PageType,@UserType,@ModuleAutoId)  
			Update SequenceCodeGeneratorMaster SET currentSequence=currentSequence+1 WHERE SequenceCode='PageId'  
			set @AutoId=SCOPE_IDENTITY()

			SELECT * FROM PageMaster WHERE AutoId=@AutoId
			END TRY  
			BEGIN CATCH  
			SET @exceptionMessage = 'Oops! Something went wrong.Please try later2.'  
			SET @isException = 1  
			END CATCH  
		END  
	END
	ELSE IF @OpCode = 12  
	BEGIN   
			   
			if exists(SELECT AutoId FROM PageAction WHERE ActionName = @PageAction AND PageAutoId=@AutoId)  
			BEGIN  
			SET @exceptionMessage = 'Page Action already exist'  
			SET @isException = 1  
			END  
			ELSE  
		BEGIN  
			BEGIN TRY    
			INSERT INTO PageAction (ActionName,PageAutoId,ActionType,UserType,CreateOn,CreatedBy)
			VALUES(@PageAction,@AutoId,@ActionType,@UserType,GETDATE(),@EmpTypeNo)  

			SELECT pa.AutoId,pa.ActionName,pa.ActionType,pm.PageName,etm.TypeName,pa.UserType,pa.PageAutoId FROM PageAction pa
			INNER JOIN PageMaster pm ON pm.AutoId=pa.PageAutoId
			LEFT JOIN EmployeeTypeMaster etm ON etm.AutoId = pa.UserType
			WHERE pa.PageAutoId=@AutoId
			END TRY  
			BEGIN CATCH  
			SET @exceptionMessage = 'Oops! Something went wrong.Please try later2.'  
			SET @isException = 1  
			END CATCH  
		END  
	END  
	ELSE IF @OpCode = 22  
	BEGIN      
			if exists(SELECT AutoId FROM PageAction WHERE ActionName = @PageAction AND PageAutoId=@AutoId AND AutoId != @ActionAutoId)  
			BEGIN  
			SET @exceptionMessage = 'Page Action already exist'  
			SET @isException = 1  
			END  
			ELSE  
		BEGIN  
			BEGIN TRY    
			UPDATE PageAction SET ActionName=@PageAction,PageAutoId=@AutoId,ActionType=@ActionType,
			UserType=@UserType WHERE AutoId=@ActionAutoId

			SELECT * FROM PageMaster WHERE PageId=@PageId
			END TRY  
			BEGIN CATCH  
			SET @exceptionMessage = 'Oops! Something went wrong.Please try later2.'  
			SET @isException = 1  
			END CATCH  
		END  
	END
	ELSE IF @OpCode = 41  
		BEGIN   
			BEGIN TRY  
			SELECT *,etm.TypeName FROM PageMaster 
			LEFT JOIN EmployeeTypeMaster etm ON etm.AutoId=PageMaster.Role   
			WHERE   
			( ISNULL(@PageName,'')='' OR PageName like '%'+@PageName+'%')   
			AND (ISNULL(@PageId,'' )='' OR PageId like '%'+@PageId+'%')  
			AND (@Status=2 or Status=@Status)   
			END TRY  
			BEGIN CATCH  
			SET @exceptionMessage = 'Oops! Something went wrong.Please try later.'  
			SET @isException = 1  
			END CATCH  
		END  
  
	ELSE IF @OpCode = 42  
		BEGIN   
			BEGIN TRY  
			SELECT * FROM EmployeeTypeMaster ORDER BY TypeName ASC
			select * from module where ParentId=0 ORDER BY ModuleName Asc
			END TRY  
			BEGIN CATCH  
			SET @exceptionMessage = 'Oops! Something went wrong.Please try later.'  
			SET @isException = 1  
			END CATCH  
		END
	ELSE IF @OpCode = 45 
		BEGIN   
			BEGIN TRY  
			SELECT m.AutoId,m.ModuleId,m.ModuleName,(select mmm.ModuleName from Module mmm WHERE mmm.AutoId=m.ParentId) as ParentModuleName FROM [dbo].[Module] m 
			WHERE Status=1 AND (select count(md.AutoId) from Module md WHERE md.ParentId=m.AutoId) = 0 ORDER BY ParentModuleName ASC  

			END TRY  
			BEGIN CATCH  
			SET @exceptionMessage = 'Oops! Something went wrong.Please try later.'  
			SET @isException = 1  
			END CATCH  
		END
	ELSE IF @OpCode = 43
		BEGIN   
			BEGIN TRY  
			SELECT * FROM PageMaster WHERE AutoId=@AutoId
			END TRY  
			BEGIN CATCH  
			SET @exceptionMessage = 'Oops! Something went wrong.Please try later.'  
			SET @isException = 1  
			END CATCH  
		END
	ELSE IF @OpCode = 44
		BEGIN   
			BEGIN TRY  
			SELECT pa.AutoId,pa.ActionName,pa.ActionType,pm.PageName,etm.TypeName,pa.UserType,pa.PageAutoId FROM PageAction pa
			INNER JOIN PageMaster pm ON pm.AutoId=pa.PageAutoId
			LEFT JOIN EmployeeTypeMaster etm ON etm.AutoId = pa.UserType
			WHERE pa.PageAutoId=@AutoId
			END TRY  
			BEGIN CATCH  
			SET @exceptionMessage = 'Oops! Something went wrong.Please try later.'  
			SET @isException = 1  
			END CATCH  
		END  

    ELSE IF @OpCode = 31  
		BEGIN  
			if exists(SELECT mpa.PageId FROM ManagePageAccess mpa WHERE mpa.PageId = @AutoId)  
			BEGIN  
			SET @exceptionMessage = 'Page already assigned'  
			SET @isException = 1  
			END 

			ELSE
		BEGIN
			BEGIN TRY  
			DELETE FROM PageMaster WHERE AutoId=@AutoId  
			END TRY  
	
			BEGIN CATCH  
			SET @exceptionMessage = 'Oops! Something went wrong.Please try later.'  
			SET @isException = 1  
			END CATCH  
			END 
		END
	ELSE IF @OpCode = 32  
		BEGIN  
			if exists(SELECT mpa.PageId FROM ManagePageAccess mpa WHERE mpa.PageId = @AutoId AND AssignedAction=@PageAction)  
			BEGIN  
			SET @exceptionMessage = 'Error'  
			SET @isException = 1  
			END 

			ELSE
			BEGIN
			SET @exceptionMessage = 'Success'  
			SET @isException = 1 
			END	
		END  
  
	ELSE IF @OpCode = 21  
	BEGIN      
		    if exists(SELECT PageUrl FROM PageMaster WHERE PageUrl = @PageUrl AND AutoId != @AutoId)  
			BEGIN  
			SET @exceptionMessage = 'Page url already exist'  
			SET @isException = 1  
			END  
			
			ELSE 
		BEGIN   
			BEGIN TRY  
			UPDATE PageMaster SET PageName=@PageName,PageUrl=@PageUrl,Description=@Description,
			Status=@Status,Type=@PageType,Role=@UserType,ParentModuleAutoId=@ModuleAutoId WHERE AutoId=@AutoId  
			END TRY  
			BEGIN CATCH  
			SET @exceptionMessage = 'Oops! Something went wrong.Please try later.'  
			SET @isException = 1  
			END CATCH  
		END 
		END
      
			END TRY  
			BEGIN CATCH  
			SET @isException=1          
			SET @exceptionMessage='Oops! Something went wrong.Please try later.'  
			END CATCH  
		END  
  
  
  
GO
