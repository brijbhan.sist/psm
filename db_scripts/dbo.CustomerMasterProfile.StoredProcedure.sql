---alter table CustomerBankDetails add CreatedBy int ,CreatedDate datetime,ModifyDate datetime,ModifyBy int
---alter table BillingAddress add CreatedBy int ,CreatedDate datetime,ModifyDate datetime,ModifyBy int
---alter table ShippingAddress add CreatedBy int ,CreatedDate datetime,ModifyDate datetime,ModifyBy int
Alter Proc [dbo].[CustomerMasterProfile]
@OpCode int =null,
@CustomerAutoId int=null,
@DocumentName VARCHAR(100)=NULL,
@DateSortIn int=NULL,
@DocumentURL VARCHAR(max)=NULL, 
@EmpAutoId INT=NULL,
@FileAutoId INT =NULL,
@RoutingNo varchar(20)=null,                   
@ExpiryDate varchar(10)=null,                    
@BankName varchar(500)=NULL,                  
@BankAcc varchar(20)=NULL,                  
@CardNo varchar(20)=NULL,                  
@CVV varchar(20)=NULL, 
@Zipcode varchar(6)=NULL,   
@StoreCredit decimal(18,2)=NULL, 
@CardType int=NULL,                  
@AutoId int=null,
@Status INT=NULL,  
@PaymentRemarks VARCHAR(max)=NULL,
@Address varchar(200)=NULL,
@Address2 varchar(200)=NULL,
@City varchar(50)=null,
@State varchar(50)=null,
@StateId int =null,
@Zipcode1 varchar(10)=null,
@CityAutoId int =null,
@latitude  varchar(10)=null,
@longitude  varchar(50)=null,
@IsDefault int=null,
@BiliingAutoId int=null,
@ShippingAutoId int=null,
@ZipCodeAutoId int =null,
@PageIndex INT=1,                                                                                                  
@PageSize INT=10,
@isException BIT OUT,                                
@exceptionMessage VARCHAR(max) OUT
As
Begin
BEGIN TRY                                                                               
	SET @isException=0                                                                                                  
	SET @exceptionMessage='Success!!'
	Declare @ChildDbLocation varchar(50),@sql nvarchar(max), 
	@FromPath varchar(MAX),@ToPath varchar(MAX),@ToPath2 varchar(MAX),@cmdstring varchar(1000),@ImagePath VARCHAR(500),@cmdstring2 varchar(1000)  
	select @ChildDbLocation=ChildDB_Name from CompanyDetails
	IF @Opcode = 41                                                                                       
		BEGIN                                                                                                  
			SELECT [CustomerId],[CustomerName],ct.[CustomerType],emp.FirstName +' '+ emp.LastName as EmpName,emp.Email as [AltEmail],
			[Contact1],[Contact2],PLM.[PriceLevelName],CM.[Status],SM.[StatusType],                                                                                                  
			[SalesPersonAutoId],[ContactPersonName],FORMAT (CreatedOn, 'MM/dd/yyyy') as CreatedOn,FORMAT(UpdateOn, 'MM/dd/yyyy') as UpdateOn,  
			(SELECT FirstName+' '+ISNULL(LastName,'') FROM EmployeeMaster AS emp1 WHERE emp1.AutoId=CM.Createdby) as CreatedBy,                                                                                                  
			(SELECT  FirstName+' '+ISNULL(LastName,'')  FROM EmployeeMaster AS emp2 WHERE emp2.AutoId=CM.updatedBy) as UpdatedBy,                                                                                                                     
			ISNULL((select CreditAmount from CustomerCreditMaster where customerAutoid=cm.Autoid),0.00) AS CreditAmount,                                                                                                  
			ISNULL((SELECT ZoneName FROM ZoneMaster AS ZM WHERE AUTOID=(SELECT top 1 ZONEID FROM  ZipMaster ZM1 WHERE                                                                                          
			ZM1.ZIPCODE=(select sa.ZipCode from  shippingAddress as sa where sa.autoid=cm.DefaultShipAdd))) ,'NA')AS Zone1                                            
			FROM [dbo].[CustomerMaster] As CM                                                                                                  
			INNER JOIN [dbo].CustomerType AS ct ON ct.AutoId=CM.CustomerType                                                                                                  
			LEFT JOIN [dbo].EmployeeMaster AS emp ON emp.AutoId=CM.SalesPersonAutoId                                                                                                   
			LEFT JOIN [dbo].[CustomerPriceLevel] AS CPL ON CPL.[CustomerAutoId] = CM.[AutoId]			                           
			LEFT JOIN [dbo].[PriceLevelMaster] AS PLM ON PLM.[AutoId] = CPL.[PriceLevelAutoId]                              
			INNER JOIN [dbo].[StatusMaster] AS SM ON SM.[AutoId] = CM.[Status] AND SM.[Category] IS NULL 
			WHERE CM.AutoId = @CustomerAutoId 
		                                                                                                  
			SELECT EMP.AutoId,ISNULL(FirstName,'')+' ' +ISNULL(LastName,'') AS EmpName,SM.StatusType Status FROM EmployeeMaster AS EMP                                                                                       
			INNER JOIN StatusMaster AS SM ON SM.AUTOID=EMP.STATUS AND Category IS NULL and EmpType in (2,5,6,10)  order by EMP.FirstName 
			
			SELECT Sum(DO.AmtDue) TotalDueAmount FROM OrderMaster AS OM 
			INNER JOIN DeliveredOrders AS DO ON OM.AutoId=DO.OrderAutoId  and DO.AmtDue >0 and Status=11                                                                                  
			where CustomerAutoId =@CustomerAutoId                                                                                              
		END
		ELSE IF @Opcode=42                                                                                                  
		BEGIN                                                      
			SELECT AUTOID INTO #OrderAutoId FROM OrderMaster WHERE CustomerAutoId= @CustomerAutoId                                                                                                  
			SELECT ROW_NUMBER() OVER(ORDER BY(CASE WHEN @DateSortIn=1 THEN AutoId END) ASC,
			(CASE WHEN @DateSortIn=2 THEN AutoId END) DESC) AS RowNumber, * INTO #Results47 FROM                                                                                                  
			(                                                                                                  
			SELECT OL.AutoId, EM.[FirstName] + ' ' + Em.[LastName] AS EmpName,AM.[Action],[Remarks],FORMAT([ActionDate],'MM/dd/yyyy hh:mm:tt') As ActionDate,OM.[OrderNo],
			FORMAT(Om.[OrderDate],'MM/dd/yyyy hh:mm:tt')  As OrderDate                                                                                          
			FROM [dbo].[tbl_OrderLog] AS OL                                                            
			INNER JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OL.[EmpAutoId]                                                                                                  
			INNER JOIN [dbo].[tbl_ActionMaster] As AM ON AM.[AutoId] = OL.[ActionTaken]                                                                                           
			INNER JOIN [dbo].[OrderMaster] AS OM ON OM.[AutoId] = OL.[OrderAutoId]                      
			WHERE OrderAutoId IN (SELECT * FROM #OrderAutoId)                                                                                                  
			) AS t ORDER BY
			(CASE WHEN @DateSortIn=1 THEN AutoId END)ASC,
			(CASE WHEN @DateSortIn=2 THEN AutoId END) DESC
			
			SELECT  COUNT(AutoId)                                                                                                                                                                                
			AS RecordCount, case when @PageSize=0 then COUNT(AutoId) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Results47 as PM 
                                                            
			SELECT *   from #Results47 as PM                                                                   
			WHERE @PageSize=0 or  RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1      
			
		END

		ELSE IF @Opcode=43                                                                   
		BEGIN                                                                                                             
			select CONVERT(VARCHAR(10),lo.CreatedDate,101) AS CreatedDate,(FirstName+' '+LastName) as Receivedby,Amount,                                                                                                
			case                                                                                                   
			when ReferenceType='OrderMaster' then (select OrderNo from OrderMaster om  where om.AutoId=lo.ReferenceNo)                                                                                   
			when ReferenceType='CustomerPaymentDetails' then (select sp.PaymentId from CustomerPaymentDetails sp  where sp.PaymentAutoId=lo.ReferenceNo)                                                                                                  
			else '' end as ReferenceId,CustomerAutoId                                                                                                      
			from tbl_Custumor_StoreCreditLog as lo inner join EmployeeMaster as emp on emp.AutoId=lo.CreatedBy                                                                                                  
			where customerAutoId=@CustomerAutoId  
			order by  convert(datetime, lo.CreatedDate   )
		END 

		ELSE IF @Opcode=11                                                                                 
		BEGIN         
		BEGIN TRY      
    BEGIN TRANSACTION
		INSERT INTO CustomerDocumentMaster(CustomerAutoId,DocumentName,DocumentURL,UploadedDate,ModifyDate                          
		,UploadedBy,ModifyBy)                                                            
		VALUES(@CustomerAutoId,@DocumentName,@DocumentURL,GETDATE(),GETDATE(),@EmpAutoId,@EmpAutoId) 
		SET @FileAutoId = (SELECT SCOPE_IDENTITY())
			 			
		if exists(select IsExistOrNew from CustomerMaster where AutoId=@CustomerAutoId and IsExistOrNew in (1,2))
		begin
		update CustomerDocumentMaster set WebStatus=1 where FileAutoId=@FileAutoId
		--------------Code for copy Document strt
		SET @ImagePath = (select DocumentUrl FROM CustomerDocumentMaster where FileAutoId=@FileAutoId)  
		SET @FromPath=(Select Frompath From DocumentPathMaster where AutoId=1)
		SET @ToPath=(Select TOPath From DocumentPathMaster where AutoId=1)		
		SET @ToPath2=(Select TOPath From [DocumentPathMaster] where AutoId=2)
		SET @cmdstring = 'copy '+@FromPath+@ImagePath+' '+@ToPath+@ImagePath
		SET @cmdstring2 = 'copy '+@FromPath+@ImagePath+' '+@ToPath2+@ImagePath
		exec master..xp_cmdshell @cmdstring2
        exec master..xp_cmdshell @cmdstring
		
		--------------Code for copy Document end
		end
		COMMIT TRANSACTION
		
	END TRY
	BEGIN CATCH
	   ROLLBACK TRANSACTION
	   SET @IsException=1
	   SET @ExceptionMessage=ERROR_MESSAGE()
	END CATCH

		END
		ELSE IF @OpCode=12
		BEGIN
		select DocumentName,case when IsRequired=1 then (DocumentName+' - Required') else DocumentName end as Document from PriceSmart.dbo.WebAdminDocumentMaster
		for json path
		END
		ELSE IF @Opcode=44                                                                                                  
		BEGIN                                             
			SELECT  FileAutoId,DocumentName,DocumentURL FROM CustomerDocumentMaster                                                                                        
			WHERE CustomerAutoId =@CustomerAutoId                                                                                       
		END  

		ELSE IF @Opcode=21 
		BEGIN                    
		BEGIN TRY      
    BEGIN TRANSACTION
		UPDATE CustomerDocumentMaster SET DocumentName =@DocumentName,ModifyDate=GETDATE(),ModifyBy=@EmpAutoId,                                                                                                 
		DocumentURL=case when @DocumentURL is null or @DocumentURL='' then DocumentURL else @DocumentURL end                                                
		WHERE FileAutoId=@FileAutoId  
		if exists(select IsExistOrNew from CustomerMaster where IsExistOrNew in (1,2) and AutoId=@CustomerAutoId)
		begin
		update CustomerDocumentMaster set WebStatus=1 where FileAutoId=@FileAutoId
		--------------Code for copy Document strt
		SET @ImagePath = (select DocumentUrl FROM CustomerDocumentMaster where FileAutoId=@FileAutoId)  
		select @ImagePath,@FileAutoId
		SET @FromPath=(Select Frompath From DocumentPathMaster where AutoId=1)
		SET @ToPath=(Select TOPath From DocumentPathMaster where AutoId=1)		
		SET @ToPath2=(Select TOPath From [DocumentPathMaster] where AutoId=2)
		SET @cmdstring = 'copy '+@FromPath+@ImagePath+' '+@ToPath+@ImagePath
		SET @cmdstring2 = 'copy '+@FromPath+@ImagePath+' '+@ToPath2+@ImagePath
		exec master..xp_cmdshell @cmdstring2
        exec master..xp_cmdshell @cmdstring
		end
		--------------Code for copy Document end
		COMMIT TRANSACTION
		
	END TRY
	BEGIN CATCH
	   ROLLBACK TRANSACTION
	   SET @IsException=1
	   SET @ExceptionMessage=ERROR_MESSAGE()
	END CATCH
		END
		 
		ELSE IF @Opcode=31                                                                              
		BEGIN                                                                                                   
		DELETE FROM  CustomerDocumentMaster  WHERE FileAutoId=@FileAutoId                                                                                                  
		END

		ELSE if @Opcode=13                                
		begin   
		BEGIN TRY                                      
		BEGIN TRAN                 
			if @Status=1  
			begin               
			INSERT INTO  [dbo].[CustomerBankDetails]([CustomerAutoId],[BankName],[BankACC],
			Status,RoutingNo,BankRemark,CreatedBy,CreatedDate,ModifyDate,ModifyBy)                   
			values (@CustomerAutoId,@BankName,@BankAcc,1,@RoutingNo,@PaymentRemarks,
			@EmpAutoId,GETDATE(),GETDATE(),@EmpAutoId)  
			end
			else  
			begin  
			INSERT INTO  [dbo].[CustomerBankDetails]([CustomerAutoId],[CardTypeAutoId],[CardNo],[ExpiryDate],[CVV],Status,Zipcode,CardRemark,CreatedBy,CreatedDate,ModifyDate,ModifyBy)             
			values (@CustomerAutoId,@CardType,@CardNo,@ExpiryDate,@CVV,2,@Zipcode,@PaymentRemarks,@EmpAutoId,GETDATE(),GETDATE(),@EmpAutoId
			)   
			end  
		COMMIT TRAN                                                                                            
		END TRY                                                                         
		BEGIN CATCH                                                                                            
		ROLLBACK TRAN                               
		SET @isException=1                                              
		SET @exceptionMessage=ERROR_MESSAGE()                                                                                            
		END CATCH                       
		end 
		
		else if @Opcode=48                                
		begin     
			select cbd.AutoId,BankName,BankACC,RoutingNo,BankRemark from [dbo].[CustomerBankDetails] as cbd  
			where CustomerAutoId=@CustomerAutoId and Status=1     
   
			select cbd.AutoId,CardNo,CONVERT(VARCHAR(10),ExpiryDate,101) AS ExpiryDate,CVV,CardType,Zipcode,CardRemark from [dbo].[CustomerBankDetails] as cbd            
			inner join [dbo].[CardTypeMaster] as ctm on ctm.AutoId=cbd.CardTypeAutoId            
			where CustomerAutoId=@CustomerAutoId and Status=2     
                     
		end   
		else if @Opcode=221                               
		begin   
            update CustomerCreditMaster set CreditAmount=isnull(CreditAmount,0)+@StoreCredit where CustomerAutoId=@CustomerAutoId	
			insert into tbl_Custumor_StoreCreditLog (ReferenceType,Amount,CreatedDate,CreatedBy,CustomerAutoId,Remarks)
			values('Settled Credit Amount',-1*@StoreCredit,GETDATE(),@EmpAutoId,@CustomerAutoId,@PaymentRemarks)
		end 
		ELSE IF @Opcode=101                                                                                 
			BEGIN                                                                                                           
			BEGIN TRY                                                                                          
			BEGIN TRAN                                                                 
				IF NOT EXISTS(SELECT AutoId FROM State WHERE StateName=@State)                                                                           
				BEGIN                        
					SET @isException=1                                                                                                  
					SET @exceptionMessage='invalid state'                                                                                                  
				END
				ELSE 
				BEGIN
					Exec [dbo].[ProcinsertData_Cityzipcode_ALL]  @State=@State,@City=@City,@Zipcode1=@Zipcode1  
					
					select @StateId=AutoId from State where StateName=@State 
					SET @CityAutoId=(select AutoiD from CityMaster where CityName=@City and StateId=@StateId)
					Select @Zipcode1=Zipcode,@ZipCodeAutoId=AutoId from ZipMaster Where Zipcode= @Zipcode1 and CityId=@CityAutoId

					IF @IsDefault=1
					BEGIN
						update BillingAddress set IsDefault=0 where CustomerAutoId=@CustomerAutoId
						
						INSERT INTO BillingAddress(CustomerAutoId,Address,Address2,IsDefault,BCityAutoId,State,Zipcode,ZipcodeAutoid,BillSA_Lat,BillSA_Long,CreatedBy,CreatedDate,ModifyDate,ModifyBy)                                                            
						VALUES(@CustomerAutoId,@Address,@Address2,@IsDefault,@CityAutoId,@StateId,@Zipcode1,@ZipCodeAutoId,@latitude,@longitude,@EmpAutoId,GETDATE(),GETDATE(),@EmpAutoId) 
						set @BiliingAutoId=SCOPE_IDENTITY() 
						update CustomerMaster set DefaultBillAdd=@BiliingAutoId where AutoId=@CustomerAutoId 
						
						SET @sql='
						update ['+@ChildDbLocation+'].[dbo].BillingAddress set IsDefault=0 
						where CustomerAutoId='+Convert(varchar(20),@CustomerAutoId)+'

						SET IDENTITY_INSERT ['+@ChildDbLocation+'].[dbo].[BillingAddress] ON;

						INSERT INTO ['+@ChildDbLocation+'].[dbo].BillingAddress(AutoId,CustomerAutoId,Address,
						Address2,IsDefault,BCityAutoId,State,Zipcode,ZipcodeAutoid,BillSA_Lat,BillSA_Long)                                                            
						select AutoId,CustomerAutoId,Address,
						Address2,IsDefault,BCityAutoId,State,Zipcode,ZipcodeAutoid,BillSA_Lat,BillSA_Long
						from [dbo].BillingAddress where autoid='+Convert(varchar(20),@BiliingAutoId)+' 
						 

						SET IDENTITY_INSERT ['+@ChildDbLocation+'].[dbo].[BillingAddress] OFF;

						update ['+@ChildDbLocation+'].[dbo].CustomerMaster 
						set DefaultBillAdd='+Convert(varchar(20),@BiliingAutoId)+' where AutoId='+convert(varchar(25),@CustomerAutoId)+''
						
						EXEC sp_executesql @sql 
					END
					else
					BEGIN

						INSERT INTO BillingAddress(CustomerAutoId,Address,Address2,IsDefault,BCityAutoId,State,Zipcode,ZipcodeAutoid,BillSA_Lat,BillSA_Long,CreatedBy,CreatedDate,ModifyDate,ModifyBy)                                                            
						VALUES(@CustomerAutoId,@Address,@Address2,@IsDefault,@CityAutoId,@StateId,@Zipcode1,@ZipCodeAutoId,@latitude,@longitude,@EmpAutoId,GETDATE(),GETDATE(),@EmpAutoId) 
					    
						SET @BiliingAutoId=SCOPE_IDENTITY()

						SET @sql='
						SET IDENTITY_INSERT ['+@ChildDbLocation+'].[dbo].[BillingAddress] ON;

						INSERT INTO ['+@ChildDbLocation+'].[dbo].BillingAddress(AutoId,CustomerAutoId,Address,
						Address2,IsDefault,BCityAutoId,State,Zipcode,ZipcodeAutoid,BillSA_Lat,BillSA_Long)                                                            
						select AutoId,CustomerAutoId,Address,
						Address2,IsDefault,BCityAutoId,State,Zipcode,ZipcodeAutoid,BillSA_Lat,BillSA_Long
						from [dbo].BillingAddress where autoid='+Convert(varchar(20),@BiliingAutoId)+' 
						 

						SET IDENTITY_INSERT ['+@ChildDbLocation+'].[dbo].[BillingAddress] OFF;

						'

						EXEC sp_executesql @sql
					END
				END
				
			COMMIT TRANSACTION                                                                                             
			END TRY                                                    
			BEGIN CATCH                                             
			ROLLBACK TRAN                                                                                                   
			SET @isException=1                                                                       
			SET @exceptionMessage=ERROR_MESSAGE()                                                                                                       
			END CATCH                                                                      
		END
		
		ELSE IF @Opcode=102                                                                                 
			BEGIN                                                                                                           
			BEGIN TRY                                                                                          
			BEGIN TRAN                                                                 
				IF NOT EXISTS(SELECT * FROM State WHERE StateName=@State)                                                                           
				BEGIN                        
					SET @isException=1                                                                                                  
					SET @exceptionMessage='invalid state'                                                                                                  
				END
				ELSE 
				BEGIN
					 Exec [dbo].[ProcinsertData_Cityzipcode_ALL]  @State=@State,@City=@City,@Zipcode1=@Zipcode1

					select @StateId=AutoId from State where StateName=@State 
					SET @CityAutoId=(select AutoiD from CityMaster where CityName=@City and StateId=@StateId)
					Select @Zipcode1=Zipcode,@ZipCodeAutoId=AutoId from ZipMaster Where Zipcode= @Zipcode1 and CityId=@CityAutoId						

				 IF @IsDefault=1
					BEGIN					
						
						update ShippingAddress set IsDefault=0  where CustomerAutoId=@CustomerAutoId

						INSERT INTO ShippingAddress(CustomerAutoId,Address,Address2,IsDefault,SCityAutoId,State,Zipcode,ZipcodeAutoid,SA_Lat,SA_Long,CreatedBy,CreatedDate,ModifyDate,ModifyBy)                                                            
						VALUES(@CustomerAutoId,@Address,@Address2,@IsDefault,@CityAutoId,@StateId,@Zipcode1,@ZipCodeAutoId,@latitude,@longitude,@EmpAutoId,GETDATE(),GETDATE(),@EmpAutoId) 					
					
						set @ShippingAutoId=SCOPE_IDENTITY()
						update CustomerMaster set DefaultShipAdd=@ShippingAutoId where AutoId=@CustomerAutoId
					
					  
						SET @sql='		
						
						update ['+@ChildDbLocation+'].[dbo].ShippingAddress set IsDefault=0 
						where CustomerAutoId='+Convert(varchar(20),@CustomerAutoId)+'

						SET IDENTITY_INSERT ['+@ChildDbLocation+'].[dbo].[ShippingAddress] ON;

						INSERT INTO ['+@ChildDbLocation+'].[dbo].ShippingAddress(AutoId,CustomerAutoId,Address,
						Address2,IsDefault,SCityAutoId,State,Zipcode,ZipcodeAutoid,SA_Lat,SA_Long)                                                            
						select AutoId,CustomerAutoId,Address,
						Address2,IsDefault,SCityAutoId,State,Zipcode,ZipcodeAutoid,SA_Lat,SA_Long 
						from [dbo].ShippingAddress where autoid='+Convert(varchar(25),@ShippingAutoId)+' 

						SET IDENTITY_INSERT ['+@ChildDbLocation+'].[dbo].[ShippingAddress] OFF

						update ['+@ChildDbLocation+'].[dbo].CustomerMaster set DefaultBillAdd='+Convert(varchar(25),
						@ShippingAutoId)+' where AutoId='+convert(varchar(25),@CustomerAutoId)+''

						EXEC sp_executesql @sql
					END
					else
					BEGIN
						select @StateId=AutoId from State where StateName=@State 
						SET @CityAutoId=(select AutoiD from CityMaster where CityName=@City and StateId=@StateId)
						Select @Zipcode1=Zipcode,@ZipCodeAutoId=AutoId from ZipMaster Where Zipcode= @Zipcode1 and CityId=@CityAutoId

						INSERT INTO ShippingAddress(CustomerAutoId,Address,Address2,IsDefault,SCityAutoId,State,Zipcode,ZipcodeAutoid,SA_Lat,SA_Long)                                                            
						VALUES(@CustomerAutoId,@Address,@Address2,@IsDefault,@CityAutoId,@StateId,@Zipcode1,@ZipCodeAutoId,@latitude,@longitude) 	
					
					    set @ShippingAutoId=SCOPE_IDENTITY()

					    SET @sql='
						SET IDENTITY_INSERT ['+@ChildDbLocation+'].[dbo].[ShippingAddress] ON;

						INSERT INTO ['+@ChildDbLocation+'].[dbo].ShippingAddress(AutoId,CustomerAutoId,Address,
						Address2,IsDefault,SCityAutoId,State,Zipcode,ZipcodeAutoid,SA_Lat,SA_Long)                                                            
						select AutoId,CustomerAutoId,Address,
						Address2,IsDefault,SCityAutoId,State,Zipcode,ZipcodeAutoid,SA_Lat,SA_Long 
						from [dbo].ShippingAddress where autoid='+Convert(varchar(25),@ShippingAutoId)+' 

						SET IDENTITY_INSERT ['+@ChildDbLocation+'].[dbo].[ShippingAddress] OFF;
						'
						EXEC sp_executesql @sql
					END
				END
				
			COMMIT TRANSACTION                                                                                             
			END TRY                                                    
			BEGIN CATCH                                             
			ROLLBACK TRAN                                                                                                   
			SET @isException=1                                                                       
			SET @exceptionMessage=ERROR_MESSAGE()                                                                                                         
			END CATCH                                                                      
		END
		ELSE IF @Opcode=201                                                                                 
			BEGIN                                                                                                           
			BEGIN TRY                                                                                          
			BEGIN TRAN
			IF NOT EXISTS(select * from State where StateName=@State)
			BEGIN
			  SET @isException=1                                                                            
			  SET @exceptionMessage='Invalid state'
			END
			        			                      
				ELSE
				BEGIN
					 Exec [dbo].[ProcinsertData_Cityzipcode_ALL]  @State=@State,@City=@City,@Zipcode1=@Zipcode1

				IF @IsDefault=1
					BEGIN
						update BillingAddress set IsDefault=0 where CustomerAutoId=@CustomerAutoId
						SELECT @StateId=SM.AutoId,@CityAutoId=CM.AutoId,@ZipCodeAutoId=ZM.AutoId FROM ZipMaster AS ZM 
						INNER JOIN CityMaster AS CM ON CM.AutoId=ZM.CityId
						INNER JOIN State AS SM ON SM.AutoId=CM.StateId
						WHERE  Zipcode=@Zipcode1 AND CityName=@City AND SM.StateName=@State
											 
						update BillingAddress set CustomerAutoId=@CustomerAutoId,Address=@Address,Address2=@Address2,BCityAutoId=@CityAutoId,
						State=@StateId,Zipcode=@Zipcode1,IsDefault=@IsDefault,ZipcodeAutoid=@ZipCodeAutoId,BillSA_Lat=@latitude,BillSA_Long=@longitude,
						ModifyDate=GETDATE(),ModifyBy=@EmpAutoId
						where AutoId=@AutoId

						set @BiliingAutoId=@AutoId
						update CustomerMaster set DefaultBillAdd=@BiliingAutoId where AutoId=@CustomerAutoId 

						SET @sql='
						update ['+@ChildDbLocation+'].[dbo].BillingAddress set IsDefault=0 where 
						CustomerAutoId='+COnvert(varchar(20),@CustomerAutoId)+'	

						UPDATE CBA SET CBA.CustomerAutoId=BA.CustomerAutoId
						,CBA.Address=BA.Address,CBA.Address2=BA.Address2,CBA.BCityAutoId=BA.BCityAutoId,CBA.State=BA.State,CBA.Zipcode=BA.Zipcode,
						CBA.IsDefault=BA.IsDefault,CBA.ZipcodeAutoid=BA.ZipcodeAutoid,CBA.BillSA_Lat=BA.BillSA_Lat,CBA.BillSA_Long=BA.BillSA_Long 
						from BillingAddress as BA 
						INNER JOIN ['+@ChildDbLocation+'].[dbo].[BillingAddress] as CBA 
						ON BA.AutoId=CBA.AutoId WHERE BA.AutoId='+Convert(varchar(20),@AutoId)+'


						update ['+@ChildDbLocation+'].[dbo].CustomerMaster set DefaultBillAdd='+Convert(varchar(20),@AutoId)+'
						where AutoId='+Convert(varchar(20),@CustomerAutoId)+'
						'
						EXEC sp_executesql @sql
					END   
					else 
					BEGIN
					    SELECT @StateId=SM.AutoId,@CityAutoId=CM.AutoId,@ZipCodeAutoId=ZM.AutoId FROM ZipMaster AS ZM 
						INNER JOIN CityMaster AS CM ON CM.AutoId=ZM.CityId
						INNER JOIN State AS SM ON SM.AutoId=CM.StateId
						WHERE  Zipcode=@Zipcode1 AND CityName=@City AND SM.StateName=@State

						Declare @count int=(Select count(AutoId) from BillingAddress where CustomerAutoId=@CustomerAutoId)

						update BillingAddress set CustomerAutoId=@CustomerAutoId,Address=@Address,Address2=@Address2,BCityAutoId=@CityAutoId,
						State=@StateId,Zipcode=@Zipcode1,IsDefault=(case when @count=1 then 1 else @IsDefault end),ZipcodeAutoid=@ZipCodeAutoId,BillSA_Lat=@latitude,BillSA_Long=@longitude
						where AutoId=@AutoId

						SET @sql='  

						UPDATE CBA SET CBA.CustomerAutoId=BA.CustomerAutoId
						,CBA.Address=BA.Address,CBA.Address2=BA.Address2,CBA.BCityAutoId=BA.BCityAutoId,CBA.State=BA.State,CBA.Zipcode=BA.Zipcode,
						CBA.IsDefault=BA.IsDefault,CBA.ZipcodeAutoid=BA.ZipcodeAutoid,CBA.BillSA_Lat=BA.BillSA_Lat,CBA.BillSA_Long=BA.BillSA_Long 
						from BillingAddress as BA 
						INNER JOIN ['+@ChildDbLocation+'].[dbo].[BillingAddress] as CBA 
						ON BA.AutoId=CBA.AutoId WHERE BA.AutoId='+Convert(varchar(20),@AutoId)+''

						EXEC sp_executesql @sql
					END
				END				
			COMMIT TRANSACTION                                                                                             
			END TRY                                                    
			BEGIN CATCH                                             
			ROLLBACK TRAN                                                                                                   
			SET @isException=1                                                                       
			SET @exceptionMessage=ERROR_MESSAGE()                                                                                                        
			END CATCH                                                                      
		END
		
		ELSE IF @Opcode=202                                                                                 
			BEGIN                                                                                                           
			BEGIN TRY                                                                                          
			BEGIN TRAN                                                                 
				IF NOT EXISTS(SELECT * FROM State WHERE StateName=@State)                                                                           
				BEGIN                        
					SET @isException=1                                                                                                  
					SET @exceptionMessage='invalid state'                                                                                                  
				END
				ELSE 
				BEGIN
					Exec [dbo].[ProcinsertData_Cityzipcode_ALL]  @State=@State,@City=@City,@Zipcode1=@Zipcode1 
				IF @IsDefault=1
					BEGIN
						update ShippingAddress set IsDefault=0 where CustomerAutoId=@CustomerAutoId
						
						SELECT @Zipcode1=Zipcode,@ZipCodeAutoId=ZM.AutoId,@CityAutoId=CM.AutoId FROM ZipMaster AS ZM 
						INNER JOIN CityMaster AS CM ON CM.AutoId=ZM.CityId WHERE  Zipcode=@Zipcode1 AND CityName=@City
						select @StateId=AutoId from State where StateName=@State 
						
						update ShippingAddress set CustomerAutoId=@CustomerAutoId,Address=@Address,Address2=@Address2,SCityAutoId=@CityAutoId,State=@StateId,
						Zipcode=@Zipcode1,IsDefault=@IsDefault,ZipcodeAutoid=@ZipCodeAutoId,SA_Lat=@latitude,SA_Long=@longitude 
						where AutoId=@AutoId					
					
						set @ShippingAutoId=@AutoId
						update CustomerMaster set DefaultShipAdd=@ShippingAutoId where AutoId=@CustomerAutoId
					
					    SET @sql='
						update ['+@ChildDbLocation+'].[dbo].ShippingAddress set IsDefault=0 where 
						CustomerAutoId='+COnvert(varchar(20),@CustomerAutoId)+'

						UPDATE CBA SET CBA.CustomerAutoId=BA.CustomerAutoId
						,CBA.Address=BA.Address,CBA.Address2=BA.Address2,CBA.SCityAutoId=BA.SCityAutoId,CBA.State=BA.State,CBA.Zipcode=BA.Zipcode,
						CBA.IsDefault=BA.IsDefault,CBA.ZipcodeAutoid=BA.ZipcodeAutoid,CBA.SA_Lat=BA.SA_Lat,CBA.SA_Long=BA.SA_Long 
						from ShippingAddress as BA 
						INNER JOIN ['+@ChildDbLocation+'].[dbo].[ShippingAddress] as CBA 
						ON BA.AutoId=CBA.AutoId WHERE BA.AutoId='+Convert(varchar(20),@AutoId)+'

						update ['+@ChildDbLocation+'].[dbo].CustomerMaster set DefaultBillAdd='+Convert(varchar(20),@AutoId)+'
						where AutoId='+Convert(varchar(20),@CustomerAutoId)+'
						'
						EXEC sp_executesql @sql
					END
				else
				BEGIN
				    select @StateId=AutoId from State where StateName=@State 
					SET @CityAutoId=(select AutoiD from CityMaster where CityName=@City and StateId=@StateId)
					Select @Zipcode1=Zipcode,@ZipCodeAutoId=AutoId from ZipMaster Where Zipcode= @Zipcode1 and CityId=@CityAutoId

					SET @count=(Select count(AutoId) from ShippingAddress where CustomerAutoId=@CustomerAutoId)

					update ShippingAddress set CustomerAutoId=@CustomerAutoId,Address=@Address,Address2=@Address2,SCityAutoId=@CityAutoId,State=@StateId,
					Zipcode=@Zipcode1,IsDefault=(case when @count=1 then 1 else @IsDefault end),ZipcodeAutoid=@ZipCodeAutoId,SA_Lat=@latitude,SA_Long=@longitude 
					where AutoId=@AutoId

					SET @sql='
						UPDATE CBA SET CBA.CustomerAutoId=BA.CustomerAutoId
						,CBA.Address=BA.Address,CBA.Address2=BA.Address2,CBA.SCityAutoId=BA.SCityAutoId,CBA.State=BA.State,CBA.Zipcode=BA.Zipcode,
						CBA.IsDefault=BA.IsDefault,CBA.ZipcodeAutoid=BA.ZipcodeAutoid,CBA.SA_Lat=BA.SA_Lat,CBA.SA_Long=BA.SA_Long 
						from ShippingAddress as BA 
						INNER JOIN ['+@ChildDbLocation+'].[dbo].[ShippingAddress] as CBA 
						ON BA.AutoId=CBA.AutoId WHERE BA.AutoId='+Convert(varchar(20),@AutoId)+''
						EXEC sp_executesql @sql
				END
				END
			COMMIT TRANSACTION                                                                                             
			END TRY                                                    
			BEGIN CATCH                                             
			ROLLBACK TRAN                                                                                                   
			SET @isException=1                                                                       
			SET @exceptionMessage='Invalid Address'                                                                                                         
			END CATCH                                                                      
		END
		else if @OpCode=301
		BEGIN
			IF NOT EXISTS (select  * from OrderMaster where BillAddrAutoId=@AutoId)
			BEGIN
			delete from BillingAddress where AutoId=@AutoId and IsDefault!=1
			END
			ELSE
			BEGIN
			SET @isException=1                                                                       
			SET @exceptionMessage='Address is already used'   
			END
		
		END
		else if @OpCode=302
		BEGIN
		   IF NOT EXISTS (select  * from OrderMaster where ShipAddrAutoId=@AutoId)
			BEGIN
			 delete from ShippingAddress where AutoId=@AutoId and IsDefault!=1
			END
			ELSE
			BEGIN
			SET @isException=1                                                                       
			SET @exceptionMessage='Address is already used'   
			END		
		END
		else if @OpCode=401
		BEGIN
		 select BA.AutoId,Address,Address2,City,Zipcode,StateName,BillSA_Lat,BillSA_Long,IsDefault from BillingAddress as  BA
         inner join State SM on sm.AutoId=BA.State
		 where BA.AutoId=@AutoId 
		END
		else if @OpCode=402
		BEGIN
		   select SA.AutoId,Address,Address2,City,Zipcode,StateName,SA_Lat,SA_Long,IsDefault from ShippingAddress as  SA
         inner join State SM on sm.AutoId=SA.State
		 where SA.AutoId=@AutoId 
		END
		else if @OpCode=403
		BEGIN
		 select AutoId,CompleteBillingAddress,IsDefault from BillingAddress
		 where CustomerAutoId=@CustomerAutoId
		END
		else if @OpCode=404
		BEGIN
		  select AutoId,CompleteShippingAddress,IsDefault from ShippingAddress 
		 where CustomerAutoId=@CustomerAutoId
		END
END TRY                                                                                                  
BEGIN CATCH                                                                                                  
	SET @isException=1                                                           
	SET @exceptionMessage='Oops! Something went wrong.Please try later.'                                                  
END CATCH 
End
GO
