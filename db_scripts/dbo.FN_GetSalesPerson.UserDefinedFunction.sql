USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GetSalesPerson]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--SELECT dbo.FN_GetSalesPerson('ORD100001',10866,2,1106)  
alter FUNCTION [dbo].[FN_GetSalesPerson] (  
@OrderNo INT,  
@ProductAutoId INT,  
@UnitAutoId INT,  
@ReqQty INT,
@IsFreeItem INT,
@IsExchange INT
)   
RETURNS Bit AS  
BEGIN     
	Declare @SalesPerson INT,@Qty INT,@AllowQtyInPieces INT,@OutPara BIT=1     
    
	SELECT @SalesPerson=SalesPersonAutoId From OrderMaster WHERE AutoId=@OrderNo  
	SELECT @Qty=SUM((ISNULL(QtyShip,0)*QtyPerUnit)+(@ReqQty*QtyPerUnit)) FROM OrderItemMaster WHERE ProductAutoId=@ProductAutoId AND UnitTypeAutoId=@UnitAutoId
	and OrderAutoId=@OrderNo and isFreeItem=@IsFreeItem and IsExchange=@IsExchange   
	IF EXISTS(SELECT * FROM AllowQtyPiece WHERE SalesPersonAutoId=@SalesPerson and ProductAutoId=@ProductAutoId)  
	BEGIN  
		IF EXISTS(select * from AllowQtyPiece where CAST(AllowDate AS date) <= CAST(getdate() AS DATE) AND SalesPersonAutoId=@SalesPerson AND ProductAutoId=@ProductAutoId)  
		BEGIN  
			SELECT @AllowQtyInPieces=(AllowQtyInPieces-UsedQty) FROM AllowQtyPiece WHERE SalesPersonAutoId=@SalesPerson and ProductAutoId=@ProductAutoId  
			IF (@AllowQtyInPieces>=@Qty)  
			BEGIN  
				SET @OutPara=1  
			END  
		ELSE  
		BEGIN  
			SET @OutPara=0  
		END  
	END  
END  
RETURN @OutPara  
END  
  
  
  
  
  
  
  
  
  
GO
