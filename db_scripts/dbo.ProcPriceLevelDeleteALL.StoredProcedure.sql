USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcEmailReceiverMaster]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[ProcPriceLevelDeleteALL]    
as      
begin 

	--Delete on psmnj
	delete from [psmnj.a1whm.com].[dbo].ProductPricingInPriceLevel where AutoId in
	(
	select ppl.AutoId  from [psmnj.a1whm.com].[dbo].ProductPricingInPriceLevel as ppl
	inner join [psmnj.a1whm.com].[dbo].PackingDetails as pd on pd.ProductAutoId=ppl.ProductAutoId and pd.UnitType=ppl.UnitAutoId
	where ppl.CustomPrice < pd.CostPrice
	)
	--Delete on psmny
	delete from [psmny.a1whm.com].[dbo].ProductPricingInPriceLevel where AutoId in
	(
	select ppl.AutoId  from [psmny.a1whm.com].[dbo].ProductPricingInPriceLevel as ppl
	inner join [psmny.a1whm.com].[dbo].PackingDetails as pd on pd.ProductAutoId=ppl.ProductAutoId and pd.UnitType=ppl.UnitAutoId
	where ppl.CustomPrice < pd.CostPrice
	)
	--Delete on psmnpa
	delete from [psmnpa.a1whm.com].[dbo].ProductPricingInPriceLevel where AutoId in
	(
	select ppl.AutoId  from [psmnpa.a1whm.com].[dbo].ProductPricingInPriceLevel as ppl
	inner join [psmnpa.a1whm.com].[dbo].PackingDetails as pd on pd.ProductAutoId=ppl.ProductAutoId and pd.UnitType=ppl.UnitAutoId
	where ppl.CustomPrice < pd.CostPrice
	)
	--Delete on psmpa
	delete from [psmpa.a1whm.com].[dbo].ProductPricingInPriceLevel where AutoId in
	(
	select ppl.AutoId  from [psmpa.a1whm.com].[dbo].ProductPricingInPriceLevel as ppl
	inner join [psmpa.a1whm.com].[dbo].PackingDetails as pd on pd.ProductAutoId=ppl.ProductAutoId and pd.UnitType=ppl.UnitAutoId
	where ppl.CustomPrice < pd.CostPrice
	)
	--Delete on psmwpa
	delete from [psmwpa.a1whm.com].[dbo].ProductPricingInPriceLevel where AutoId in
	(
	select ppl.AutoId  from [psmwpa.a1whm.com].[dbo].ProductPricingInPriceLevel as ppl
	inner join [psmwpa.a1whm.com].[dbo].PackingDetails as pd on pd.ProductAutoId=ppl.ProductAutoId and pd.UnitType=ppl.UnitAutoId
	where ppl.CustomPrice < pd.CostPrice
	)
	--Delete on psmct
	delete from [psmct.a1whm.com].[dbo].ProductPricingInPriceLevel where AutoId in
	(
	select ppl.AutoId  from [psmct.a1whm.com].[dbo].ProductPricingInPriceLevel as ppl
	inner join [psmct.a1whm.com].[dbo].PackingDetails as pd on pd.ProductAutoId=ppl.ProductAutoId and pd.UnitType=ppl.UnitAutoId
	where ppl.CustomPrice < pd.CostPrice
	)
	
end      
GO
