USE [psmnj.a1whm.com]
GO

/****** Object:  Table [dbo].[Delete_DrvLog_Backup]    Script Date: 04/14/2020 22:11:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Delete_DrvLog_Backup](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[DrvAutoId] [int] NULL,
	[OrderAutoId] [int] NULL,
	[AssignDate] [datetime] NULL,
 CONSTRAINT [PK_Delete_DrvLog_Backup] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


