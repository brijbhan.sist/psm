
ALTER PROCEDURE [dbo].[ProcErrorTicketResponseLog]  
 @Who nvarchar(25)=NULL,  
 @isException bit out,  
 @exceptionMessage varchar(500) out,  
 @TicketID varchar(50)=NULL,  
 @ActionNo varchar(50)=NULL,  
 @DeveloperStatus varchar(50)=NULL,  
 @Action varchar(MAX)=NULL,  
 @ActionDate datetime=NULL,  
 @Status varchar(50)=NULL,  
 @ByEmployee varchar(50)=NULL,  
 @opCode int=Null  
AS  
BEGIN  
     SET @isException=0
	 SET @exceptionMessage=''
 BEGIN TRY   
   BEGIN TRANSACTION     
		if @opCode=11  
		begin     
			insert into ErrorTicketResponseLog(TicketID,ActionNo,[Action],ActionDate,ByEmployee,Status,DeveloperStatus)  
			values(@TicketID,@ActionNo,@Action,getdate(),@ByEmployee,@Status,@DeveloperStatus); 
			update ErrorTicketMaster set 
			Status=(case when @Status is null then status else @Status END), 
			DeveloperStatus=(case when @DeveloperStatus is null then DeveloperStatus else @DeveloperStatus END), 
			TicketCloseDate = GETDATE() where TicketID=@TicketID         
		end    
	   if @opCode=41    
			begin     
			select TicketID,ActionNo,[Action], ActionDate,em.EmpId as ByEmployee ,etrl.Status,isnull(DeveloperStatus,'Open') DeveloperStatus,  
			(em.FirstName+' '+em.LastName) as EmpName from ErrorTicketResponseLog as etrl inner join EmployeeMaster as em on em.AutoId = etrl.ByEmployee  
			where TicketID=@TicketID order by Convert(date,ActionDate)  desc 
	   end  
  
	   if @opCode=42  
			begin  
			select attach from ErrorTicketMaster where TicketID=@TicketID  
	   end  
	   if @opCode=43  
			begin  
			select TicketID , Format(TicketDate,'MM/dd/yyyy') as TicketDate , PageUrl, Priority, Type, ByEmployee, Subject, 
			Description, Status, DeveloperStatus,Format(TicketCloseDate,'MM/dd/yyyy') as TicketCloseDate, attach ,
			(select FirstName+' '+LastName from EmployeeMaster as em  where [AutoId] = [ByEmployee]) as Fullname from ErrorTicketMaster as em   
			where TicketID=@TicketID 

			select TicketID,ActionNo,[Action], Format(ActionDate,'MM/dd/yyyy hh:mm tt') as ActionDate,em.EmpId as ByEmployee ,etrl.Status,isnull(DeveloperStatus,'Open') DeveloperStatus,  
			(em.FirstName+' '+em.LastName) as EmpName from ErrorTicketResponseLog as etrl inner join EmployeeMaster as em on em.AutoId = etrl.ByEmployee  
			where TicketID=@TicketID order by Convert(date,ActionDate)  desc 
			end  
       if @opCode=44
	   BEGIN
	         select *,(select top 1 upper(Companyid) from CompanyDetails) as Companyid from EmailReceiverMaster  
	   END
  COMMIT TRAN     
  END TRY  
  BEGIN CATCH  
	  ROLLBACK TRAN  
	  SET @isException=1  
	  SET @exceptionMessage= ERROR_MESSAGE()  
  END CATCH;  
END  
  
  
  
  
  
GO
