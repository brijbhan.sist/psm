ALTER Procedure [dbo].[ProcAppCartmaster]  
 @CustomerAutoId INT=NULL,   
 @DraftId INT=NULL,   
 @ProductId INT=NULL,   
 @UnitType INT=NULL,    
 @Quantity INT=NULL,    
 @Price decimal(18,2)=NULL,    
 @timeStamp datetime =null,    
 @Opcode INT=NULL,                       
 @isException bit out,                            
 @exceptionMessage varchar(max) out         
 AS  
 BEGIN  
      SET @isException=0  
   SET @exceptionMessage='Success'  
      IF @Opcode=11  
   BEGIN  
       BEGIN TRY  
    BEGIN TRANSACTION 
     Declare @DefaultShipAutoId INT,@DefaultBillAutoId INT,@CartAutoId INT,@DraftSequence varchar(25)=null
     SELECT @DefaultShipAutoId=DefaultShipAdd,@DefaultBillAutoId=DefaultBillAdd FROM CustomerMaster Where AutoId=@CustomerAutoId  
  
     IF @DraftId=0  
     BEGIN  
		  SET @DraftSequence = (SELECT DBO.SequenceCodeGenerator('DraftCartMaster'))  

		  INSERT INTO App_DraftCartMasterCust (CustomerAutoId,DefaultShipAutoId,DefaultBillAutoId,DraftDate,DraftId)   
		  VALUES (@CustomerAutoId,@DefaultShipAutoId,@DefaultBillAutoId,GETDATE(),@DraftSequence)  
		  
		  SET @CartAutoId=SCOPE_IDENTITY() 

		  UPDATE SequenceCodeGeneratorMaster SET currentSequence = currentSequence + 1 WHERE SequenceCode='DraftCartMaster'  
     END

	 IF EXISTS(Select * from App_DraftCartItemMasterCust where ProductId=@ProductId and CartAutoId=@DraftId)
	 begin
	      update App_DraftCartItemMasterCust set UnitType=@UnitType,Quantity=@Quantity where ProductId=@ProductId and CartAutoId=@DraftId
	 End
	 else
	 begin
	 INSERT INTO App_DraftCartItemMasterCust (CartAutoId,ProductId,UnitType,Quantity,Price) values (@CartAutoId,@ProductId,@UnitType,@Quantity,@Price) 
	 end

	 Select  @CartAutoId AS DraftAutoId 

		  
      
   COMMIT TRANSACTION  
   END TRY  
   BEGIN CATCH  
           ROLLBACK TRANSACTION  
           SET @isException=1  
           SET @exceptionMessage='Error'  
   END CATCH  
   END  
 END