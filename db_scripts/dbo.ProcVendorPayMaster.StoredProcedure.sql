ALTER procedure [dbo].[ProcVendorPayMaster]    
	@OpCode int=Null,                                                 
	@PaymentMode  int=Null,    
	@PayAutoId int = null,     
	@ReferenceID varchar(20)=null,    
	@PayId varchar(20)=null,                                         
	@Remark varchar(250)=null,                                     
	@VendorAutoId int=NULL,
	@PaymentType int =null,
	@PaymentAutoId INT=NULL,                                          
	@PaymentAmount decimal(10,2)=NULL,                                        
	@PaymentDate datetime=NULL,                                        
	@FromDate date=NULL,                                        
	@ToDate date=NULL,                                                                           
	@EmpAutoId int=NULL,  
	@Category int=null,
	@CheckAutoId int=null,    
	@UpdateCheckAutoId int=null,    
	@DocumentName VARCHAR(100)=NULL,                                                                                              
	@DocumentURL VARCHAR(max)=NULL,              
	@TableValue  VenderPayment readonly,              
	@PaymentCurrencyXml xml=null,                                                                                  
	@SortAmount decimal(18,2)=null,                                                                            
	@TotalCurrencyAmount decimal(10,2)=null,                                                      
	@isException bit out,                                        
	@exceptionMessage varchar(max) out  
	
as    
BEGIN                                        
 BEGIN TRY                                        
   SET @isException=0                                                         
   SET @exceptionMessage='success'     
	if @OpCode = 11    
	begin 
		BEGIN TRY  
		begin tran  
				Set @PayId = (select dbo.SequenceCodeGenerator('VendorPayment'))     
				insert into VendorPayMaster(PayId,VendorAutoId,PaymentDate,PaymentAmount,PaymentMode,PaymentType,Remark,CreatedBy,CreateDate,ReferenceID,Status)    
				values(@PayId,@VendorAutoId,@PaymentDate,@PaymentAmount,@PaymentMode,@PaymentType,@Remark,@EmpAutoId,GETDATE(),@ReferenceID,2)  --As per discussion with Brijbhan Sir TI101513  
				SET @PaymentAutoId=SCOPE_IDENTITY()       
                if @PaymentType=1 
				BEGIN
					set @Category=(select AutoId from Pattycashcategorymaster Where CategoryName='Vendor Payment')
					insert into PattyCashLogMaster(TransactionType,TransactionAmount,Remark,AutoIdCreatedBy,CreatedDate,AutoIdUpdatedBy,UpdatedDate,TransactionDate,Category,ReferenceId)
					values('DR',@PaymentAmount,@Remark,@EmpAutoId,GETDATE(),@EmpAutoId,GETDATE(),GETDATE(),@Category,@PaymentAutoId)
				END
				INSERT INTO VenderPaymentOrderDetails(PaymentAutoId,OrderAutoId,PaymentAmount)                                                                                              
				SELECT @PaymentAutoId,OrderAutoId,PaymentAmount FROM @TableValue            
     
				update se set PaidAmount=ISNULL(PaidAmount,0)+ISNULL(tmp.PaymentAmount,0),
				ReceivedBy= @EmpAutoId,ReceivedDate=GETDATE()
				from StockEntry as se inner join @TableValue as tmp on tmp.OrderAutoId=se.AutoId     
   
				update CheckMaster set [PaymentReferenceNo]=@PaymentAutoId where CheckAutoId=@CheckAutoId                                                                         
                                                                                 
				INSERT INTO VendorPaymentCurrencyDetails(PaymentAutoId,CurrencyAutoId,NoOfValue,TotalAmount)                                                                                    
				select @PaymentAutoId,tr.td.value('CurrencyAutoId[1]','int') as CurrencyAutoId,                                                              
				tr.td.value('NoOfValue[1]','int') as NoOfValue,                                                                                    
				tr.td.value('TotalAmount[1]','decimal(10,2)') as TotalAmount                                                                                    
				from  @PaymentCurrencyXml.nodes('/PaymentCurrencyXml') tr(td)          
                       
				update SequenceCodeGeneratorMaster set currentSequence=currentSequence+1 where SequenceCode='VendorPayment'    
  
				declare @Remarks varchar(200)=null  
				SET @Remarks = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=21), '[PaymentId]',@PayId)     
  
				insert into [dbo].[VendorLog](ActionDate,[ActionBy],[Remarks],[VendorAutoId],ActionTaken) 
				values(GETDATE(),@EmpAutoId,@Remarks,@VendorAutoId,21)  
		commit tran  
		END try  
		begin catch  
			rollback tran  
			SET @isException=0  
			SET @exceptionMessage='Oops,something went wrong.Please try again'  
		end catch  
	end     
   if @OpCode = 21    
   begin    
		update VendorPayMaster set VendorAutoId = @VendorAutoId, PaymentDate = @PaymentDate, PaymentAmount = @PaymentAmount,ReferenceID = @ReferenceID,    
		PaymentMode = @PaymentMode,PaymentType=@PaymentType, Remark = @Remark, UpdatedBy = @EmpAutoId, UpdatedDate = GETDATE() where AutoId = @PayAutoId 
		
		
   end      
   if @OpCode = 31    
   begin    
        delete VendorPayMaster where AutoId = @PayAutoId    
   end      
   if @OpCode = 41    
   begin    
	   select AutoId,VendorName from VendorMaster where Status = 1 order by VendorName ASC   
	   select AutoId,PaymentMode from PAYMENTModeMaster where AutoID!=5  order by PaymentMode ASC   
   end
   if @OpCode = 44    
   begin    
	   select * from PaymentTypeMaster
	   for json path
   end
	if @OpCode = 42    
	begin    
		select (vpm.AutoId) as PaymentAutoId,vpm.PayId,vm.VendorName,Format(vpm.PaymentDate,'MM/dd/yyyy') as PaymentDate,vpm.PaymentAmount,     
		pm.PaymentMode,    
		vpm.Remark,vpm.ReferenceID,vpm.Status,sm.StatusType from VendorPayMaster vpm    
		inner join VendorMaster vm on vm.AutoId = vpm.VendorAutoId    
		inner join StatusMaster sm on sm.AutoId = vpm.Status and sm.Category = 'Pay'    
		inner join PAYMENTModeMaster pm on pm.AutoID = vpm.PaymentMode    
		where (@PayId = '' or @PayId is null or vpm.PayId = @PayId )    
		and (@VendorAutoId = 0 or @VendorAutoId is null or vpm.VendorAutoId = @VendorAutoId )    
		and (@FromDate is null or @ToDate is null or (CONVERT(date,vpm.PaymentDate) between convert(date,@FromDate) and CONVERT(date,@ToDate)))     
	end    
   if @OpCode = 43    
 begin    
	set @VendorAutoId=(select VendorAutoId from VendorPayMaster where AutoId=@PayAutoId)  
	SELECT OrderAutoId,PaymentAmount INTO #RESULT43 FROM VenderPaymentOrderDetails                                                                                                   
	WHERE  PaymentAutoId=@PayAutoId     
  
	select PayId,VendorAutoId,FORMAT(PaymentDate, 'MM/dd/yyyy') as PaymentDate,PaymentAmount,PaymentMode,PaymentType,Remark,ReferenceID,(select CheckAutoId from 
	CheckMaster where [PaymentReferenceNo]=@PayAutoId) as CheckAutoId from VendorPayMaster where AutoId = @PayAutoId    
         
	select CurrencyName,CurrencyValue,cm.AutoId,CurrencyValue,case when NoofValue is null then 0 else NoofValue end as NoofValue,                                                             
	case when TotalAmount is null then 0 else TotalAmount end as TotalAmount   from CurrencyMaster as cm                                             
	left join VendorPaymentCurrencyDetails  as pcd on pcd.CurrencyAutoId=cm.AutoId and PaymentAutoId=@PayAutoId                                                             
	where Status=1                                                                           
	order by cm.CurrencyValue  desc     
   
	SELECT om.AutoId as OrderAutoId,BillNo as OrderNo,(convert(varchar(10),BillDate,101)) as PODate,VendorAutoId,isnull(PaidAmount,0.00) as PaidAmount,temp.PaymentAmount    
	,isnull(OrderAmount,0.00) as OrderAmount, isnull(DueAmount,0.00) as DueAmount,0 AS Pay   FROM StockEntry AS OM        
	inner join #RESULT43 as temp on temp.OrderAutoId=OM.AutoId                                                                       
	UNION                                                                                                    
	SELECT om.AutoId as OrderAutoId,BillNo as OrderNo,(convert(varchar(10),BillDate,101) )PODate,VendorAutoId,isnull(PaidAmount,0.00) as PaidAmount, 0 as PaymentAmount,                                                      
	isnull(OrderAmount,0.00) as OrderAmount, isnull(DueAmount,0.00) as DueAmount ,1 AS Pay                         
	FROM StockEntry AS OM                                                                                               
	where VendorAutoId =@VendorAutoId and om.DueAmount  >0                                                                                                    
	And om.AutoId not in (select OrderAutoId from #RESULT43)    
  
	select ISNULL(sum(DueAmount),0.00) as TotalDueAmount from StockEntry where VendorAutoId=(select VendorAutoId from VendorPayMaster where AutoId = @PayAutoId) and (isnull(DueAmount,0.00)>0)    
      
	select CheckAutoId as AutoId,CheckNO as CheckNO,CheckAmount from CheckMaster where VendorAutoId=@VendorAutoId 
	union   
	select CheckAutoId as AutoId,CheckNO as CheckNO,CheckAmount from CheckMaster where [PaymentReferenceNo]=@PayAutoId order by  CheckNO 
  
  
 end    
    if @OpCode = 32    
 begin       
    
 select SE.AutoId as OrderAutoId,BillNo as OrderNo,FORMAT(BillDate, 'MM/dd/yyyy') as PODate,VendorAutoId,isnull(PaidAmount,0.00) as PaidAmount--,'0.00' as DueAmount    
 ,isnull(OrderAmount,0.00) as OrderAmount, isnull(DueAmount,0.00) as DueAmount from StockEntry as SE    
 --inner join BillItems as BI on BI.BillAutoId=SE.AutoId    
 where VendorAutoId=@VendorAutoId and (isnull(DueAmount,0.00)>0)  order by BillDate asc  
    
   select Autoid,Currencyname,CurrencyValue from Currencymaster where Status=1  order by   CurrencyValue desc       
  
   select ISNULL(sum(DueAmount),0.00) as TotalDueAmount from StockEntry where VendorAutoId=@VendorAutoId and (isnull(DueAmount,0.00)>0)    
 end   
  if @OpCode = 33    
   begin    
		select CheckAutoId as AutoId,CheckNO as CheckNO,CheckAmount from CheckMaster where (VendorAutoId=@VendorAutoId 
		and Type=1 and [PaymentReferenceNo] is  null) or (PaymentReferenceNo=@PayAutoId) --and [PaymentReferenceNo] is not null  
   end   
   if @OpCode = 34   
   begin    
   --------------------------------------------------------------update payment settlement------------------------------------------------------  
    BEGIN TRY  
   begin tran  
   update VendorPayMaster set VendorAutoId=@VendorAutoId,PaymentDate=@PaymentDate,PaymentAmount=@PaymentAmount,PaymentMode=@PaymentMode,Remark=@Remark,UpdatedBy=@EmpAutoId,  
   UpdatedDate=GETDATE(),PaymentType=@PaymentType,ReferenceID=@ReferenceID where AutoId=@PayAutoId  
   
   if @PaymentType=1 
		BEGIN
		set @Category=(select AutoId from Pattycashcategorymaster Where CategoryName='Vendor Payment')
		update PattyCashLogMaster set TransactionType='DR',TransactionAmount=@PaymentAmount, Remark=@Remark,AutoIdUpdatedBy=@EmpAutoId,
		UpdatedDate=GETDATE(),TransactionDate=GETDATE(),Category=@Category where ReferenceId=@PayAutoId and Category=(select t.AutoId from Pattycashcategorymaster as t Where CategoryName='Vendor Payment')
		END
	update se set PaidAmount=ISNULL(PaidAmount,0)-ISNULL(tmp.PaymentAmount,0)	 from StockEntry as se 
	inner join VenderPaymentOrderDetails as tmp on tmp.OrderAutoId=se.AutoId
	where PaymentAutoId=@PayAutoId

   delete from VenderPaymentOrderDetails where PaymentAutoId=@PayAutoId  
   
   INSERT INTO VenderPaymentOrderDetails(PaymentAutoId,OrderAutoId,PaymentAmount)                                                                                              
   SELECT @PayAutoId,OrderAutoId,PaymentAmount FROM @TableValue            
     
    update se set PaidAmount=ISNULL(PaidAmount,0)+ISNULL(tmp.PaymentAmount,0),
	ReceivedBy= @EmpAutoId,ReceivedDate=GETDATE() from StockEntry as se inner join @TableValue as tmp on tmp.OrderAutoId=se.AutoId    
    
   set @UpdateCheckAutoId=(select CheckAutoid from CheckMaster where PaymentReferenceNo=@PayAutoId)  
   update CheckMaster set PaymentReferenceNo=null where CheckAutoId=@UpdateCheckAutoId  
  
   update CheckMaster set [PaymentReferenceNo]=@PayAutoId where CheckAutoId=@CheckAutoId                                                                         
           
	delete from VendorPaymentCurrencyDetails where PaymentAutoId=@PayAutoId       
                                                                    
    INSERT INTO VendorPaymentCurrencyDetails(PaymentAutoId,CurrencyAutoId,NoOfValue,TotalAmount)                                                                                    
    select @PayAutoId,tr.td.value('CurrencyAutoId[1]','int') as CurrencyAutoId,                                                              
    tr.td.value('NoOfValue[1]','int') as NoOfValue,                                                                                    
    tr.td.value('TotalAmount[1]','decimal(10,2)') as TotalAmount                                                                                    
    from  @PaymentCurrencyXml.nodes('/PaymentCurrencyXml') tr(td)          
                       
     
    SET @Remarks = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=27), '[PaymentId]',
	(select payid from VendorPayMaster where AutoId= @PayAutoId))     
  
   insert into [dbo].[VendorLog](ActionDate,[ActionBy],[Remarks],[VendorAutoId],ActionTaken) 
   values(GETDATE(),@EmpAutoId,@Remarks,@VendorAutoId,27)  
   commit tran  
   END try  
   begin catch  
   rollback tran  
   SET @isException=0  
   SET @exceptionMessage='Oops,something went wrong.Please try again'  
   end catch  
    end   
     
       
   END TRY                                        
 BEGIN CATCH                                            
    SET @isException=1                                        
    SET @exceptionMessage='Oops! Something went wrong.Please try later.'                                       
 END CATCH                                        
END 
GO
