ALTER PROCEDURE  [dbo].[ProcCustomer_Credit_Report]         
@Opcode INT=NULL,        
@CustomerAutoId int=null,           
@PageIndex INT=1,          
@PageSize INT=10,          
@RecordCount INT=null,          
@isException bit out,          
@exceptionMessage varchar(max) out          
AS          
BEGIN           
 BEGIN TRY          
  Set @isException=0          
  Set @exceptionMessage='Success'
 IF @Opcode=21                   
  BEGIN           
    SELECT AutoId,CustomerName from CustomerMaster         
    where Status=1 order by CustomerName ASC  for json path, INCLUDE_NULL_VALUES 
  END         
  ELSE IF @Opcode=41          
  BEGIN          
	SELECT ROW_NUMBER() OVER(ORDER BY [CustomerName]) AS RowNumber, * INTO #Results FROM          
	(          
	select cm.CustomerId,cm.CustomerName,ccm.CreditAmount from CustomerMaster as cm        
	inner join  CustomerCreditMaster as ccm on ccm.CustomerAutoId = cm.AutoId        
	where ccm.CreditAmount !=0         
	and(isnull  (@CustomerAutoId,0)=0 or cm.AutoId=@CustomerAutoId)        
	)AS t ORDER BY [CustomerName]        
	SELECT COUNT(CustomerName) AS RecordCount, case when @PageSize=0 then COUNT(CustomerName) else @PageSize end AS PageSize, @PageIndex AS PageIndex,Format(getdate(),'MM/dd/yyyy hh:mm tt') as PrintDate FROM #Results        
	SELECT * FROM #Results          
	WHERE  (@PageSize = 0 or  (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))     
     
	SELECT ISNULL(SUM(CreditAmount),0.00) as CreditAmount  FROM #Results          
  END           
 END TRY          
 BEGIN CATCH          
	Set @isException=1          
	Set @exceptionMessage=ERROR_MESSAGE()          
 END CATCH          
END     
  