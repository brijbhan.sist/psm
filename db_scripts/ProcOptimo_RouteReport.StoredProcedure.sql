USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcOptimo_RouteReport]    Script Date: 10/5/2020 12:58:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[ProcOptimo_RouteReport]                                                
@Opcode INT =null,                                                  
@FromDate Date = null,                                                
@ToDate Date = null,                                                
@PageIndex int=null,   
@ReportAutoId int=null,
@PlanningId int=null,
@PageSize  int=10,    
@AutoId int=null,
@RecordCount int=null,                                                   
@isException bit out,                                                    
@exceptionMessage varchar(max) out                                                  
as                                                
begin                                                  
begin try                                                  
   SET @exceptionMessage= 'Success'                                                    
   SET @isException=0                                                 
   if @Opcode = 41                                                
   begin                                          
		select ROW_NUMBER() over(order by Convert(date,tm) desc) as RowNumber, * into #Result from (
		select  [PlanningId],count(DriverAutoId) as NoOfDriver,convert(date,RouteDate) as tm,FORMAT(RouteDate,'MM/dd/yyyy') as RouteDate,
		(select count(OrderAutoId) from DriverOptimoRouteMasterLog as DO where DO.PanningId=t.PlanningId)  as NoOfOrder ,
		(select (FirstName+' '+LastName) from EmployeeMaster as emp where emp.AutoId=t.ManagerAutoid)  as PlanBy
		,sum(CONVERT(int,NoOfStop)) as NoOfStop
		from DriverOptimoRouteLog as t
		where (ISNULL(@FromDate,'')='' OR ISNULL(@ToDate,'') = '' or (convert(date,RouteDate) BETWEEN (@FromDate) AND( @ToDate)))                     
		group by PlanningId,RouteDate,ManagerAutoid
		) as t
		order by tm desc                                       
		SELECT * FROM #Result                                                
		WHERE @PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)  
                                               
		SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize=0 then COUNT(RowNumber) else @PageSize end AS PageSize, 
		@PageIndex AS PageIndex,Format(getdate(),'MM/dd/yyyy hh:mm tt') as PrintDate FROM #Result                             
              
   end                                        
   if @Opcode = 42                                                
    begin
		--select (select (FirstName+' '+LastName) from EmployeeMaster as em where em.AutoId=t.DriverAutoId) as Driver,
		--DriverStartTime,DraverEndTime,
		--sum(CONVERT(int,NoOfStop)) as NoOfStop,DriverAutoId,
		--(select count(*) from DriverLog_OrdDetails as dl where dl.DriverAutoId=t.DriverAutoId and dl.PlanningId=@PlanningId) as NOOfOrder,
		
		--(select CarName from CarDetailsMaster as cd where cd.CarAutoId=t.CarAutoId) as CarName
		--from DriverOptimoRouteLog as t where PlanningId=@PlanningId
		--group by PlanningId,DriverStartTime,DraverEndTime,DriverAutoId,CarAutoId
		select (select (FirstName+' '+LastName) from EmployeeMaster as em where em.AutoId=t.DriverAutoId) as Driver,
		DriverStartTime,DraverEndTime,
		max(CONVERT(int,NoOfStop)) as NoOfStop,t.DriverAutoId,
		(select count(*) from DriverLog_OrdDetails as dl where dl.DriverAutoId=t.DriverAutoId and dl.PlanningId=@PlanningId) as NOOfOrder,
		
		(select CarName from CarDetailsMaster as cd where cd.CarAutoId=t.CarAutoId) as CarName
		from DriverOptimoRouteLog as t
		inner join DriverLog_OrdDetails as om on om.PlanningId=t.PlanningId
		where t.PlanningId=@PlanningId
		
		group by t.PlanningId,DriverStartTime,DraverEndTime,t.DriverAutoId,CarAutoId
   end     
   if @Opcode = 43                                                
    begin
		select distinct  StopNo,CustomerName+'<br/>'+Address as CustomerName,dl.DriverName,
		do.SalesPerson as SalesPerson,
		CONVERT(varchar,CustOpenTime,100)+'-'+CONVERT(varchar,CustCloseTime,100) as StoreOpenTime ,
		CONVERT(varchar,SetDeliveryFromTime,100)+'-'+CONVERT(varchar,[SetDeliveryToTime],100) as SuggestedTime ,
		CONVERT(varchar,do.ScheduleAt,100) as AssignDeliverytime,OM.AcctRemarks,
		Format(DeliveredOrderTime,'MM/dd/yyyy hh:mm tt') as DelivereTime,
		Remark,(Select count(*) from [DriverLog_OrdDetails] g        
        where g.CustomerName = do.CustomerName and g.PlanningId=do.PlanningId) as NOOfOrder,do.OrderNo,ISNULL(sm.StatusType,'Close') as  Status
		from [dbo].[DriverLog_OrdDetails] as do
		LEFT JOIN OrderMaster as OM On OM.OrderNo=do.OrderNo
		LEFT JOIN StatusMaster as sm On sm.AutoId=om.Status and sm.Category='OrderMaster'
		left join [dbo].[Driver_Log] as dl on dl.AutoId=do.DrvLogAutoId 
		where do.DriverAutoId=@ReportAutoId and dl.PlanningId=@PlanningId 
		order by StopNo
   end      
    if @Opcode = 44                                                
    begin
	if @ReportAutoId!=0
	begin
		select distinct  StopNo,CustomerName+'<br/>'+do.Address as CustomerName,dl.DriverName,
		 do.SalesPerson as SalesPerson,
		CONVERT(varchar,CustOpenTime,100)+'-'+CONVERT(varchar,CustCloseTime,100) as StoreOpenTime ,
		CONVERT(varchar,SetDeliveryFromTime,100)+'-'+CONVERT(varchar,[SetDeliveryToTime],100) as SuggestedTime ,
		CONVERT(varchar,do.ScheduleAt,100) as AssignDeliverytime,OM.AcctRemarks,
		Format(DeliveredOrderTime,'MM/dd/yyyy hh:mm tt') as DelivereTime,
		Remark,(Select count(*) from [DriverLog_OrdDetails] g        
        where g.CustomerName = do.CustomerName and g.PlanningId=do.PlanningId) as NOOfOrder,do.OrderNo,ISNULL(sm.StatusType,'Close') as  Status,
		do.CustomerId
		from [dbo].[DriverLog_OrdDetails] as do
		left JOIN OrderMaster as OM On OM.OrderNo=do.OrderNo
		left JOIN StatusMaster as sm On sm.AutoId=om.Status and sm.Category='OrderMaster'
		left join [dbo].[Driver_Log] as dl on dl.AutoId=do.DrvLogAutoId 
		where dl.PlanningId=@PlanningId and do.DriverAutoId=@ReportAutoId
		order by StopNo

     end
	 else
	 begin
		select StopNo,CustomerName+'<br/>'+do.Address as CustomerName,dl.DriverName,
		do.SalesPerson as SalesPerson,
		CONVERT(varchar,CustOpenTime,100)+'-'+CONVERT(varchar,CustCloseTime,100) as StoreOpenTime ,
		CONVERT(varchar,SetDeliveryFromTime,100)+'-'+CONVERT(varchar,[SetDeliveryToTime],100) as SuggestedTime ,
		CONVERT(varchar,do.ScheduleAt,100) as AssignDeliverytime,OM.AcctRemarks,
		Format(DeliveredOrderTime,'MM/dd/yyyy hh:mm tt') as DelivereTime,
		Remark,(Select count(*) from [DriverLog_OrdDetails] g        
        where g.CustomerName = do.CustomerName and g.PlanningId=do.PlanningId) as NOOfOrder,do.OrderNo,sm.StatusType as  Status,
		do.CustomerId
	     into #tm
		from [dbo].[DriverLog_OrdDetails] as do
		left JOIN OrderMaster as OM On OM.OrderNo=do.OrderNo
		LEFT JOIN StatusMaster as sm On sm.AutoId=om.Status and sm.Category='OrderMaster'
		left join [dbo].[Driver_Log] as dl on dl.AutoId=do.DrvLogAutoId 
		where dl.PlanningId=@PlanningId
		
		if @AutoId=1
		begin
		select * from #tm order by DriverName,StopNo
		end
		else
		begin
		  select * from #tm order by SalesPerson,StopNo
		end
		
	 end
	select Format(GETDATE(),'MM/dd/yyyy hh:mm tt') as PrintTime,CompanyDistributerName from CompanyDetails
   end      

   end try                                                  
  begin catch                             
   SET @isException=1                                                  
   SET @exceptionMessage= ERROR_MESSAGE()                                                   
end catch                                                  
end 

