ALTER FUNCTION [dbo].[SumOfRecievedAmount](    
    @PaymentAutoId int,    
    @OrderAutoID int    
)    
RETURNS decimal(10,2)    
AS     
BEGIN    
   DECLARE @SumofReceivedAmount decimal(10,2)=0.00    
   declare @PaymentDate datetime =(select PaymentDate from CustomerPaymentDetails where PaymentAutoId=@PaymentAutoId)    
   SET @SumofReceivedAmount=isnull((SELECT SUM(pod.ReceivedAmount) from PaymentOrderDetails as pod    
   inner join  CustomerPaymentDetails as cpd on cpd.PaymentAutoId=pod.PaymentAutoId    
   where cpd.Status=0 and PaymentDate<@PaymentDate and pod.OrderAutoId=@OrderAutoID),0)    
 RETURN @SumofReceivedAmount    
END;