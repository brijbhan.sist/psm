
ALTER PROCEDURE [dbo].[PackingDetailUpdateLog]                                                                                                                                                                                
@OpCode int=Null,                                                                                                                                                                                
@ProductAutoId  int=Null,                                                                                     
@PageIndex INT = 1,                                                                                                                  
@PageSize INT = 10,                                                                                                
@isException bit out, 
@RecordCount INT =null, 
@exceptionMessage varchar(max) out                                                                                                                           
AS                                                                                                                                     
BEGIN                                                                                                                                             
 BEGIN TRY                                                                                                                                                       
  SET @isException=0                                                                                                                                                                       
  SET @exceptionMessage='Success'                                                                                                                                                                                
IF @OpCode=41                                                                                       
 BEGIN                                       
   SELECT [AutoId], [ProductId],[ProductName] FROM [dbo].[ProductMaster] ORDER BY  AutoId,ProductName DESC                                                                                                                                              
 END                                                                                                                                                                                                                                                                                                               
 ELSE IF @Opcode=44                                                                                                                                                                                
 BEGIN    
		SELECT ROW_NUMBER() OVER(ORDER BY  AutoId) AS RowNumber, * INTO #Results FROM(
		SELECT pdl.AutoId,pdl.CostPrice,FORMAT(pdl.DateTime,'MM/dd/yyyy HH:mm tt') as createDate,pdl.MinPrice,pdl.Price,pdl.ProductAutoId,pdl.SRP,
		pdl.WHminPrice,pm.ProductName,um.UnitType,pm.ProductId
		FROM PackingDetailsUpdateLog pdl 
		INNER JOIN ProductMaster pm ON pm.AutoId=pdl.ProductAutoId
		INNER JOIN UnitMaster um ON um.AutoId=pdl.UnitType
		where
		(pdl.ProductAutoId=@ProductAutoId OR ISNULL(@ProductAutoId,0)=0)
		) AS t ORDER BY t.AutoId


		SELECT  COUNT(pd.AutoId)                                                                                                                                                                                
		AS RecordCount, case when @PageSize=0 then COUNT(distinct pd.[AutoId]) else @PageSize end AS PageSize,
		@PageIndex AS PageIndex FROM #Results as pd
                                                           
		SELECT *   from #Results as PM                                                                   
		WHERE @PageSize=0 or  RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1      
		ORDER BY AutoId asc                                                                                                                                                      
                                   
 END                                                                                                                                             
                                                                                                                                                                  
 END TRY                                                    
 BEGIN CATCH                                                                                                               
    SET @isException=1                                                                                                                                                
    SET @exceptionMessage='Oops, Something went wrong .Please try again.'                                                                                             
 END CATCH                                                                                                                                                                         
END 
