
ALTER Proc [dbo].[Proc_UpdateOrder_all]      
@VenderAutoId int=null,      
@POAutoId int=null,      
@Remark varchar(500)=null,      
@PONo varchar(50)=null      
as       
begin      
 Declare @OrderNo varchar(50) ,@OrderAutoId int,@PriceLevelAutoId int      
 declare @CustomerAutoId int=null, @SalesPersonAutoId int =null,@CurrentLocationId int =null,@Location varchar(50)=null      
 Select @Location=Replace(DB_Name(),'.a1whm.com','')      
 Select @CurrentLocationId=AutoId from LocationMaster where Location=@Location                  
 declare @VenderLocation int       
 Select @VenderLocation=LocationAutoId ,@CustomerAutoId=CustomerAutoId from       
 [dbo].[fn_GetCustomerAndSalesPerson](@VenderAutoId,@CurrentLocationId)      
      
      
 IF(@VenderLocation=1)      
 BEGIN       
	set @OrderAutoId=(Select omx.AutoId from [psmnj.a1whm.com].[dbo].[OrderMaster] as omx        
	inner join [psmnj.a1whm.com].[dbo].[CustomerMaster] as cmx on omx.CustomerAutoId=cmx.AutoId         
	where referenceOrderNumber=@PoNo and cmx.LocationAutoId=@CurrentLocationId)      
        
  Update [psmnj.a1whm.com].[dbo].[OrderMaster] Set OrderRemarks=@Remark where AutoId=@OrderAutoId      
        
  delete from [psmnj.a1whm.com].[dbo].[OrderItemMaster] where OrderAutoId=@OrderAutoId      
        
 INSERT INTO [psmnj.a1whm.com].[dbo].[OrderItemMaster] ([OrderAutoId], [ProductAutoId],      
 [UnitTypeAutoId], [QtyPerUnit], [UnitPrice], [RequiredQty], [SRP], [GP], [Tax], [IsExchange],UnitMLQty,
 isFreeItem,Weight_Oz,Original_UnitType,OM_MinPrice,OM_CostPrice,BasePrice) 
 
 Select t.OrderAutoId,t.ProductAutoId,t.UnitType,t.Qty,   t.CostPrice,    
 t.ReqQty,t.SRP,t.GP,t.tax,t.IsExchange,t.MlQty,t.IsFree,t.WeightOz,t.OrgUnitType,t.CostPrice,t.MinPrice,t.Price 
 from (SELECT @OrderAutoId as OrderAutoId,              
 nj.AutoId as ProductAutoId, pd.UnitType as UnitType,pd.Qty as Qty, pd.CostPrice as CustomPrice, pd.CostPrice as CostPrice,    
 --(case when pdx.CostPrice > pd.CostPrice then pdx.CostPrice when pdx.CostPrice < pd.CostPrice then pd.CostPrice      
 --else pd.CostPrice End ) as CostPrice,                                 
 pp.Qty as ReqQty,nj.[P_SRP] as  SRP,               
 CONVERT(DECIMAL(10,2),((nj.[P_SRP]-(pd.[Price]/(CASE WHEN pd.[Qty]=0 then 1 else pd.[Qty] end )))/        
 (CASE WHEN ISNULL(nj.[P_SRP],0)=0 then 1 else nj.[P_SRP] END)) * 100) AS GP              
 ,0 as tax,0 as IsExchange,(case when isnull(nj.IsApply_ML,0)=0 then 0 else isnull(nj.MLQty,0) end) as MlQty,        
 0 as IsFree,(case when isnull(nj.IsApply_Oz,0)=0 then 0 else isnull(nj.WeightOz,0) end) as WeightOz,pd.UnitType as OrgUnitType,
 pd.CostPrice as MinPrice,pd.Price
 from PurchaseProductsMaster as pp              
 inner join ProductMaster pm on pp.ProductAutoId=pm.autoid       
 inner join PackingDetails pdx on pdx.ProductAutoId=pm.AutoId and pp.Unit=pdx.unittype              
 inner join [psmnj.a1whm.com].[dbo].[ProductMaster] nj on nj.ProductId=pm.ProductId                
 inner join [psmnj.a1whm.com].[dbo].[PackingDetails] pd on pd.ProductAutoId=nj.AutoId and pp.unit=pd.unittype              
 where pp.POAutoId=@POAutoId ) as t      
        
  Set @PriceLevelAutoId = (SELECT [PriceLevelAutoId] FROM [psmnj.a1whm.com].[dbo].[CustomerPriceLevel]       
  WHERE [CustomerAutoId] = @CustomerAutoId)          
                
  exec [psmnj.a1whm.com].[dbo].[UPDTAE_PRICELEVEL]           
  @OrderAutoId=@OrderAutoId,          
  @PriceLevelAutoId=@PriceLevelAutoId
                 
                       
  exec [psmnj.a1whm.com].[dbo].[ProcUpdatePOStatus_All]                      
		@CustomerId=@CustomerAutoId,                      
		@OrderAutoId=@OrderAutoId,                      
		@Status=1                          
          
                     
     End         
        
  else IF(@VenderLocation=2)      
 BEGIN      
  set @OrderAutoId=(Select omx.AutoId from [psmct.a1whm.com].[dbo].[OrderMaster] as omx        
  inner join [psmct.a1whm.com].[dbo].[CustomerMaster]as cmx on omx.CustomerAutoId=cmx.AutoId         
  where referenceOrderNumber=@PoNo and cmx.LocationAutoId=@CurrentLocationId)         
      
  Update [psmct.a1whm.com].[dbo].[OrderMaster] Set OrderRemarks=@Remark where AutoId=@OrderAutoId      
        
  delete from [psmct.a1whm.com].[dbo].[OrderItemMaster] where OrderAutoId=@OrderAutoId      
        
  INSERT INTO [psmct.a1whm.com].[dbo].[OrderItemMaster] ([OrderAutoId], [ProductAutoId],      
 [UnitTypeAutoId], [QtyPerUnit], [UnitPrice], [RequiredQty], [SRP], [GP], [Tax], [IsExchange],UnitMLQty,
 isFreeItem,Weight_Oz,Original_UnitType,OM_MinPrice,OM_CostPrice,BasePrice)   
 
 Select t.OrderAutoId,t.ProductAutoId,t.UnitType,t.Qty,     t.CostPrice, 
 t.ReqQty,t.SRP,t.GP,t.tax,t.IsExchange,t.MlQty,t.IsFree,t.WeightOz,t.OrgUnitType,t.MinPrice,t.CostPrice,t.Price from (SELECT @OrderAutoId as OrderAutoId,              
 nj.AutoId as ProductAutoId, pd.UnitType as UnitType,pd.Qty as Qty, pd.CostPrice as CustomPrice, pd.CostPrice as CostPrice,        
 --(case when pdx.CostPrice > pd.CostPrice then pdx.CostPrice when pdx.CostPrice < pd.CostPrice then pd.CostPrice      
 --else pd.CostPrice End ) as CostPrice,                                 
 pp.Qty as ReqQty,nj.[P_SRP] as  SRP,               
 CONVERT(DECIMAL(10,2),((nj.[P_SRP]-(pd.[Price]/(CASE WHEN pd.[Qty]=0 then 1 else pd.[Qty] end )))/        
 (CASE WHEN ISNULL(nj.[P_SRP],0)=0 then 1 else nj.[P_SRP] END)) * 100) AS GP              
 ,0 as tax,0 as IsExchange,(case when isnull(nj.IsApply_ML,0)=0 then 0 else isnull(nj.MLQty,0) end) as MlQty,        
 0 as IsFree,(case when isnull(nj.IsApply_Oz,0)=0 then 0 else isnull(nj.WeightOz,0) end) as WeightOz,pd.UnitType as OrgUnitType,
 pd.CostPrice as MinPrice,pd.Price
 from PurchaseProductsMaster pp              
 inner join ProductMaster pm on pp.ProductAutoId=pm.autoid       
 inner join PackingDetails pdx on pdx.ProductAutoId=pm.AutoId and pp.unit=pdx.unittype              
 inner join [psmct.a1whm.com].[dbo].[ProductMaster] nj on nj.ProductId=pm.ProductId                
 inner join [psmct.a1whm.com].[dbo].[PackingDetails] pd on pd.ProductAutoId=nj.AutoId and pp.unit=pd.unittype              
 where pp.POAutoId=@POAutoId ) as t      
        
  Set @PriceLevelAutoId = (SELECT [PriceLevelAutoId] FROM [psmct.a1whm.com].[dbo].[CustomerPriceLevel]       
  WHERE [CustomerAutoId] = @CustomerAutoId)          
                
  exec [psmct.a1whm.com].[dbo].[UPDTAE_PRICELEVEL]           
  @OrderAutoId=@OrderAutoId,          
  @PriceLevelAutoId=@PriceLevelAutoId
  
                      
  exec [psmct.a1whm.com].[dbo].[ProcUpdatePOStatus_All]                      
		@CustomerId=@CustomerAutoId,                      
		@OrderAutoId=@OrderAutoId,                      
		@Status=1                          
          
          
     End       
   
  else IF(@VenderLocation=4)      
  BEGIN      
		set @OrderAutoId=(Select omx.AutoId from [psmpa.a1whm.com].[dbo].[OrderMaster] as omx        
		inner join [psmpa.a1whm.com].[dbo].[CustomerMaster]as cmx on omx.CustomerAutoId=cmx.AutoId         
		where referenceOrderNumber=@PoNo and cmx.LocationAutoId=@CurrentLocationId)        
        
		Update [psmpa.a1whm.com].[dbo].[OrderMaster] Set OrderRemarks=@Remark where AutoId=@OrderAutoId      
        
		delete from [psmpa.a1whm.com].[dbo].[OrderItemMaster] where OrderAutoId=@OrderAutoId      
        
		INSERT INTO [psmpa.a1whm.com].[dbo].[OrderItemMaster] ([OrderAutoId], [ProductAutoId],      
		[UnitTypeAutoId], [QtyPerUnit], [UnitPrice], [RequiredQty], [SRP], [GP], [Tax], [IsExchange],UnitMLQty,isFreeItem,
		Weight_Oz,Original_UnitType,OM_MinPrice,OM_CostPrice,BasePrice)                                  
		Select t.OrderAutoId,t.ProductAutoId,t.UnitType,t.Qty,   t.CostPrice, 
		t.ReqQty,t.SRP,t.GP,t.tax,t.IsExchange,t.MlQty,t.IsFree,t.WeightOz,t.OrgUnitType,t.CostPrice,t.MinPrice,t.Price from (SELECT @OrderAutoId as OrderAutoId,              
		nj.AutoId as ProductAutoId, pd.UnitType as UnitType,pd.Qty as Qty,      
		 pd.CostPrice as CustomPrice, pd.CostPrice as CostPrice,        
		--(case when pdx.CostPrice > pd.CostPrice then pdx.CostPrice when pdx.CostPrice < pd.CostPrice then pd.CostPrice      
		--else pd.CostPrice End ) as CostPrice,                                 
		pp.Qty as ReqQty,nj.[P_SRP] as  SRP,               
		CONVERT(DECIMAL(10,2),((nj.[P_SRP]-(pd.[Price]/(CASE WHEN pd.[Qty]=0 then 1 else pd.[Qty] end )))/        
		(CASE WHEN ISNULL(nj.[P_SRP],0)=0 then 1 else nj.[P_SRP] END)) * 100) AS GP              
		,0 as tax,0 as IsExchange,(case when isnull(nj.IsApply_ML,0)=0 then 0 else isnull(nj.MLQty,0) end) as MlQty,        
		0 as IsFree,(case when isnull(nj.IsApply_Oz,0)=0 then 0 else isnull(nj.WeightOz,0) end) as WeightOz,pd.UnitType as OrgUnitType,
		pd.CostPrice as MinPrice,pd.Price
		from PurchaseProductsMaster pp              
		inner join ProductMaster pm on pp.ProductAutoId=pm.autoid       
		inner join PackingDetails pdx on pdx.ProductAutoId=pm.AutoId and pp.unit=pdx.unittype              
		inner join [psmpa.a1whm.com].[dbo].[ProductMaster] nj on nj.ProductId=pm.ProductId                
		inner join [psmpa.a1whm.com].[dbo].[PackingDetails] pd on pd.ProductAutoId=nj.AutoId and pp.unit=pd.unittype              
		where pp.POAutoId=@POAutoId ) as t      
        
		Set @PriceLevelAutoId = (SELECT [PriceLevelAutoId] FROM [psmpa.a1whm.com].[dbo].[CustomerPriceLevel]       
		WHERE [CustomerAutoId] = @CustomerAutoId)          
                
		exec [psmpa.a1whm.com].[dbo].[UPDTAE_PRICELEVEL]           
		@OrderAutoId=@OrderAutoId,          
		@PriceLevelAutoId=@PriceLevelAutoId  
                        
		exec [psmpa.a1whm.com].[dbo].[ProcUpdatePOStatus_All]                      
			@CustomerId=@CustomerAutoId,                      
			@OrderAutoId=@OrderAutoId,                      
			@Status=1                                    
          
     End                 
        
  else IF(@VenderLocation=5)      
  BEGIN      
     set @OrderAutoId=(Select omx.AutoId from [psmnpa.a1whm.com].[dbo].[OrderMaster] as omx        
   inner join [psmnpa.a1whm.com].[dbo].[CustomerMaster]as cmx on omx.CustomerAutoId=cmx.AutoId         
   where referenceOrderNumber=@PoNo and cmx.LocationAutoId=@CurrentLocationId)        
        
  Update [psmnpa.a1whm.com].[dbo].[OrderMaster] Set OrderRemarks=@Remark where AutoId=@OrderAutoId      
        
  delete from [psmnpa.a1whm.com].[dbo].[OrderItemMaster] where OrderAutoId=@OrderAutoId                    
                       
      
        
  INSERT INTO [psmnpa.a1whm.com].[dbo].[OrderItemMaster] ([OrderAutoId], [ProductAutoId],      
 [UnitTypeAutoId], [QtyPerUnit], [UnitPrice], [RequiredQty], [SRP], [GP], [Tax], [IsExchange],UnitMLQty,isFreeItem,Weight_Oz,
 Original_UnitType,OM_MinPrice,OM_CostPrice,BasePrice
)                                  
 Select t.OrderAutoId,t.ProductAutoId,t.UnitType,t.Qty,   t.CostPrice,  
 t.ReqQty,t.SRP,t.GP,t.tax,t.IsExchange,t.MlQty,t.IsFree,t.WeightOz,t.OrgUnitType,t.CostPrice,t.MinPrice,t.Price
 from (SELECT @OrderAutoId as OrderAutoId,              
 nj.AutoId as ProductAutoId, pd.UnitType as UnitType,pd.Qty as Qty, pd.CostPrice as CustomPrice, pd.CostPrice as CostPrice,        
 --(case when pdx.CostPrice > pd.CostPrice then pdx.CostPrice when pdx.CostPrice < pd.CostPrice then pd.CostPrice      
 --else pd.CostPrice End ) as CostPrice,                                 
 pp.Qty as ReqQty,nj.[P_SRP] as  SRP,               
 CONVERT(DECIMAL(10,2),((nj.[P_SRP]-(pd.[Price]/(CASE WHEN pd.[Qty]=0 then 1 else pd.[Qty] end )))/        
 (CASE WHEN ISNULL(nj.[P_SRP],0)=0 then 1 else nj.[P_SRP] END)) * 100) AS GP              
 ,0 as tax,0 as IsExchange,(case when isnull(nj.IsApply_ML,0)=0 then 0 else isnull(nj.MLQty,0) end) as MlQty,        
 0 as IsFree,(case when isnull(nj.IsApply_Oz,0)=0 then 0 else isnull(nj.WeightOz,0) end) as WeightOz,pd.UnitType as OrgUnitType,
 pd.CostPrice as MinPrice,pd.Price
 from PurchaseProductsMaster pp              
 inner join ProductMaster pm on pp.ProductAutoId=pm.autoid       
 inner join PackingDetails pdx on pdx.ProductAutoId=pm.AutoId and pp.unit=pdx.unittype              
 inner join [psmnpa.a1whm.com].[dbo].[ProductMaster] nj on nj.ProductId=pm.ProductId                
 inner join [psmnpa.a1whm.com].[dbo].[PackingDetails] pd on pd.ProductAutoId=nj.AutoId and pp.unit=pd.unittype              
 where pp.POAutoId=@POAutoId ) as t      
        
  Set @PriceLevelAutoId = (SELECT [PriceLevelAutoId] FROM [psmnpa.a1whm.com].[dbo].[CustomerPriceLevel]       
  WHERE [CustomerAutoId] = @CustomerAutoId)          
                
  exec [psmnpa.a1whm.com].[dbo].[UPDTAE_PRICELEVEL]           
  @OrderAutoId=@OrderAutoId,          
  @PriceLevelAutoId=@PriceLevelAutoId 
  
                     
  exec [psmnpa.a1whm.com].[dbo].[ProcUpdatePOStatus_All]                      
		@CustomerId=@CustomerAutoId,                      
		@OrderAutoId=@OrderAutoId,                      
		@Status=1                          
          
                           
     End      
   else IF(@VenderLocation=7)      
  BEGIN      
 set @OrderAutoId=(Select omx.AutoId from [psm.a1whm.com].[dbo].[OrderMaster] as omx        
 inner join [psm.a1whm.com].[dbo].[CustomerMaster]as cmx on omx.CustomerAutoId=cmx.AutoId         
 where referenceOrderNumber=@PoNo and cmx.LocationAutoId=@CurrentLocationId)       
        
  Update [psm.a1whm.com].[dbo].[OrderMaster] Set OrderRemarks=@Remark where AutoId=@OrderAutoId      
        
  delete from [psm.a1whm.com].[dbo].[OrderItemMaster] where OrderAutoId=@OrderAutoId      
        
  INSERT INTO [psm.a1whm.com].[dbo].[OrderItemMaster] ([OrderAutoId], [ProductAutoId],      
 [UnitTypeAutoId], [QtyPerUnit], [UnitPrice], [RequiredQty], [SRP], [GP], [Tax], [IsExchange],UnitMLQty,isFreeItem,
 Weight_Oz,Original_UnitType,OM_MinPrice,OM_CostPrice,BasePrice
)                                  
 Select t.OrderAutoId,t.ProductAutoId,t.UnitType,t.Qty,   t.CostPrice,   
 t.ReqQty,t.SRP,t.GP,t.tax,t.IsExchange,t.MlQty,t.IsFree,t.WeightOz,t.OrgUnitType,t.CostPrice,t.MinPrice,t.Price
 from (SELECT @OrderAutoId as OrderAutoId,              
 nj.AutoId as ProductAutoId, pd.UnitType as UnitType,pd.Qty as Qty, pd.CostPrice as CustomPrice,  pd.CostPrice as CostPrice,       
 --(case when pdx.CostPrice > pd.CostPrice then pdx.CostPrice when pdx.CostPrice < pd.CostPrice then pd.CostPrice      
 --else pd.CostPrice End ) as CostPrice,                                 
 pp.Qty as ReqQty,nj.[P_SRP] as  SRP,               
 CONVERT(DECIMAL(10,2),((nj.[P_SRP]-(pd.[Price]/(CASE WHEN pd.[Qty]=0 then 1 else pd.[Qty] end )))/        
 (CASE WHEN ISNULL(nj.[P_SRP],0)=0 then 1 else nj.[P_SRP] END)) * 100) AS GP              
 ,0 as tax,0 as IsExchange,(case when isnull(nj.IsApply_ML,0)=0 then 0 else isnull(nj.MLQty,0) end) as MlQty,        
 0 as IsFree,(case when isnull(nj.IsApply_Oz,0)=0 then 0 else isnull(nj.WeightOz,0) end) as WeightOz,pd.UnitType as OrgUnitType
 ,pd.CostPrice as MinPrice,
 pd.Price      
 from PurchaseProductsMaster pp              
 inner join ProductMaster pm on pp.ProductAutoId=pm.autoid       
 inner join PackingDetails pdx on pdx.ProductAutoId=pm.AutoId and pp.unit=pdx.unittype              
 inner join [psm.a1whm.com].[dbo].[ProductMaster] nj on nj.ProductId=pm.ProductId                
 inner join [psm.a1whm.com].[dbo].[PackingDetails] pd on pd.ProductAutoId=nj.AutoId and pp.unit=pd.unittype              
 where pp.POAutoId=@POAutoId ) as t      
        
  Set @PriceLevelAutoId = (SELECT [PriceLevelAutoId] FROM [psm.a1whm.com].[dbo].[CustomerPriceLevel]       
  WHERE [CustomerAutoId] = @CustomerAutoId)          
                
  exec [psm.a1whm.com].[dbo].[UPDTAE_PRICELEVEL]           
  @OrderAutoId=@OrderAutoId,          
  @PriceLevelAutoId=@PriceLevelAutoId  
  
                        
  exec [psm.a1whm.com].[dbo].[ProcUpdatePOStatus_All]                      
		@CustomerId=@CustomerAutoId,                      
		@OrderAutoId=@OrderAutoId,                      
		@Status=1                          
                       
     End      
      
 
		else IF(@VenderLocation=9)      
		BEGIN      
				set @OrderAutoId=(Select omx.AutoId from [psmwpa.a1whm.com].[dbo].[OrderMaster] as omx        
				inner join [psmwpa.a1whm.com].[dbo].[CustomerMaster]as cmx on omx.CustomerAutoId=cmx.AutoId         
				where referenceOrderNumber=@PoNo and cmx.LocationAutoId=@CurrentLocationId)       
        
				Update [psmwpa.a1whm.com].[dbo].[OrderMaster] Set OrderRemarks=@Remark where AutoId=@OrderAutoId      
        
				delete from [psmwpa.a1whm.com].[dbo].[OrderItemMaster] where OrderAutoId=@OrderAutoId      
        
				INSERT INTO [psmwpa.a1whm.com].[dbo].[OrderItemMaster] ([OrderAutoId], [ProductAutoId],      
				[UnitTypeAutoId], [QtyPerUnit], [UnitPrice], [RequiredQty], [SRP], [GP], [Tax], [IsExchange],UnitMLQty
				,isFreeItem,Weight_Oz,Original_UnitType,OM_MinPrice,OM_CostPrice,BasePrice
)                     
				Select t.OrderAutoId,t.ProductAutoId,t.UnitType,t.Qty,  t.CostPrice,    
				t.ReqQty,t.SRP,t.GP,t.tax,t.IsExchange,t.MlQty,t.IsFree,t.WeightOz,t.OrgUnitType,t.CostPrice,t.MinPrice,t.Price
				 from (SELECT @OrderAutoId as OrderAutoId,              
				nj.AutoId as ProductAutoId, pd.UnitType as UnitType,pd.Qty as Qty,      
				 pd.CostPrice as CustomPrice,    pd.CostPrice as CostPrice,     
				pp.Qty as ReqQty,nj.[P_SRP] as  SRP,               
				CONVERT(DECIMAL(10,2),((nj.[P_SRP]-(pd.[Price]/(CASE WHEN pd.[Qty]=0 then 1 else pd.[Qty] end )))/        
				(CASE WHEN ISNULL(nj.[P_SRP],0)=0 then 1 else nj.[P_SRP] END)) * 100) AS GP              
				,0 as tax,0 as IsExchange,(case when isnull(nj.IsApply_ML,0)=0 then 0 else isnull(nj.MLQty,0) end) as MlQty,        
				0 as IsFree,(case when isnull(nj.IsApply_Oz,0)=0 then 0 else isnull(nj.WeightOz,0) end) as WeightOz,
				pd.UnitType as OrgUnitType,pd.CostPrice as MinPrice,pd.Price      
				from PurchaseProductsMaster pp              
				inner join ProductMaster pm on pp.ProductAutoId=pm.autoid       
				inner join PackingDetails pdx on pdx.ProductAutoId=pm.AutoId and pp.unit=pdx.unittype              
				inner join [psmwpa.a1whm.com].[dbo].[ProductMaster] nj on nj.ProductId=pm.ProductId                
				inner join [psmwpa.a1whm.com].[dbo].[PackingDetails] pd on pd.ProductAutoId=nj.AutoId and pp.unit=pd.unittype              
				where pp.POAutoId=@POAutoId ) as t      
        
				Set @PriceLevelAutoId = (SELECT [PriceLevelAutoId] FROM [psmwpa.a1whm.com].[dbo].[CustomerPriceLevel]       
				WHERE [CustomerAutoId] = @CustomerAutoId)          
                
				exec [psmwpa.a1whm.com].[dbo].[UPDTAE_PRICELEVEL]           
				@OrderAutoId=@OrderAutoId,          
				@PriceLevelAutoId=@PriceLevelAutoId 
  
                        
				exec [psmwpa.a1whm.com].[dbo].[ProcUpdatePOStatus_All]                      
				@CustomerId=@CustomerAutoId,                      
				@OrderAutoId=@OrderAutoId,                      
				@Status=1                          
		End
		else IF(@VenderLocation=10)      
		BEGIN      
				set @OrderAutoId=(Select omx.AutoId from [psmny.a1whm.com].[dbo].[OrderMaster] as omx        
				inner join [psmny.a1whm.com].[dbo].[CustomerMaster]as cmx on omx.CustomerAutoId=cmx.AutoId         
				where referenceOrderNumber=@PoNo and cmx.LocationAutoId=@CurrentLocationId)       
        
				Update [psmny.a1whm.com].[dbo].[OrderMaster] Set OrderRemarks=@Remark where AutoId=@OrderAutoId      
        
				delete from [psmny.a1whm.com].[dbo].[OrderItemMaster] where OrderAutoId=@OrderAutoId      
        
				INSERT INTO [psmny.a1whm.com].[dbo].[OrderItemMaster] ([OrderAutoId], [ProductAutoId],      
				[UnitTypeAutoId], [QtyPerUnit], [UnitPrice], [RequiredQty], [SRP], [GP], [Tax], [IsExchange],UnitMLQty,
				isFreeItem,Weight_Oz,Original_UnitType,OM_MinPrice,OM_CostPrice,BasePrice)                     
				Select t.OrderAutoId,t.ProductAutoId,t.UnitType,t.Qty,t.CostPrice,  
				t.ReqQty,t.SRP,t.GP,t.tax,t.IsExchange,t.MlQty,t.IsFree,t.WeightOz,t.OrgUnitType,t.CostPrice,t.MinPrice,t.Price from (SELECT @OrderAutoId as OrderAutoId,              
				nj.AutoId as ProductAutoId, pd.UnitType as UnitType,pd.Qty as Qty,      
				 pd.CostPrice as CustomPrice,    pd.CostPrice as CostPrice,     
				pp.Qty as ReqQty,nj.[P_SRP] as  SRP,               
				CONVERT(DECIMAL(10,2),((nj.[P_SRP]-(pd.[Price]/(CASE WHEN pd.[Qty]=0 then 1 else pd.[Qty] end )))/        
				(CASE WHEN ISNULL(nj.[P_SRP],0)=0 then 1 else nj.[P_SRP] END)) * 100) AS GP              
				,0 as tax,0 as IsExchange,(case when isnull(nj.IsApply_ML,0)=0 then 0 else isnull(nj.MLQty,0) end) as MlQty,        
				0 as IsFree,(case when isnull(nj.IsApply_Oz,0)=0 then 0 else isnull(nj.WeightOz,0) end) as WeightOz,
				pd.UnitType as OrgUnitType,pd.CostPrice as MinPrice,pd.Price      
				from PurchaseProductsMaster pp              
				inner join ProductMaster pm on pp.ProductAutoId=pm.autoid       
				inner join PackingDetails pdx on pdx.ProductAutoId=pm.AutoId and pp.unit=pdx.unittype              
				inner join [psmny.a1whm.com].[dbo].[ProductMaster] nj on nj.ProductId=pm.ProductId                
				inner join [psmny.a1whm.com].[dbo].[PackingDetails] pd on pd.ProductAutoId=nj.AutoId and pp.unit=pd.unittype              
				where pp.POAutoId=@POAutoId ) as t      
        
				Set @PriceLevelAutoId = (SELECT [PriceLevelAutoId] FROM [psmny.a1whm.com].[dbo].[CustomerPriceLevel]       
				WHERE [CustomerAutoId] = @CustomerAutoId)          
                
				exec [psmny.a1whm.com].[dbo].[UPDTAE_PRICELEVEL]           
				@OrderAutoId=@OrderAutoId,          
				@PriceLevelAutoId=@PriceLevelAutoId 
  
                        
				exec [psmny.a1whm.com].[dbo].[ProcUpdatePOStatus_All]                      
				@CustomerId=@CustomerAutoId,                      
				@OrderAutoId=@OrderAutoId,                      
				@Status=1                          
		End 
End 