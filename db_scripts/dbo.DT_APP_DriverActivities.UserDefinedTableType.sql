USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedTableType [dbo].[DT_APP_DriverActivities]    Script Date: 11/20/2019 5:05:52 AM ******/
CREATE TYPE [dbo].[DT_APP_DriverActivities] AS TABLE(
	[utcActivityTimeStamp] [datetime] NULL,
	[orderActivityStatus] [int] NULL,
	[deliveryRemark] [varchar](max) NULL
)
GO
