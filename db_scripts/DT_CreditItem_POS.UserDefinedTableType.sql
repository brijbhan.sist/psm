USE [psmnj.a1whm.com]
GO
Drop PROCEDURE [dbo].[ProcPOS_CreditMemo]  
Drop  TYPE [dbo].[DT_CreditItem_POS]
/****** Object:  UserDefinedTableType [dbo].[DT_CreditItem]    Script Date: 08/15/2020 22:00:13 ******/
CREATE TYPE [dbo].[DT_CreditItem_POS] AS TABLE(
	[ProductAutoId] [int] NULL,
	[UnitAutoId] [int] NULL,
	[RequiredQty] [int] NULL,
	[QtyPerUnit] [int] NULL,
	[UnitPrice] [decimal](18, 2) NULL,
	[SRP] [decimal](18, 2) NULL,
	[Tax] [decimal](18, 2) NULL,
	[NetPrice] [decimal](18, 2) NULL,
	[QtyPerUnit_Fresh] [int] NULL,
	[QtyPerUnit_Damage] [int] NULL,
	[QtyPerUnit_Missing] [int] NULL,
	[QtyReturn_Fresh] [int] NULL,
	[QtyReturn_Damage] [int] NULL,
	[QtyReturn_Missing] [int] NULL,
	OM_MinPrice [decimal](18, 2) NULL,
	OM_CostPrice [decimal](18, 2) NULL,
	OM_BasePrice [decimal](18, 2) NULL

)
GO


