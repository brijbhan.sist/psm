alter Procedure [dbo].[UPDTAE_PRICELEVEL]      
@OrderAutoId INT=NULL,      
@PriceLevelAutoId INT=NULL,
@EmpAutoId INT=NULL      
as       
      
begin   
	Insert into [ProductPricingInPriceLevel_log]([AutoId],[PriceLevelAutoId],[ProductAutoId],[UnitAutoId],[CustomPrice],[createDated],[InsertcreateDated],[UpdatedBy])
	select [AutoId],[PriceLevelAutoId],[ProductAutoId],[UnitAutoId],[CustomPrice],[createDated],GETDATE(),@EmpAutoId FROM [dbo].[ProductPricingInPriceLevel]        
	WHERE [PriceLevelAutoId] = @PriceLevelAutoId      
	and ProductAutoid in (select ProductAutoId from OrderItemMaster where orderautoid=@OrderAutoId and isFreeItem=0 and IsExchange=0 and UnitPrice>0)

   
	 DELETE FROM [dbo].[ProductPricingInPriceLevel]        
	 WHERE [PriceLevelAutoId] = @PriceLevelAutoId      
	 and ProductAutoid in (select ProductAutoId from OrderItemMaster where orderautoid=@OrderAutoId and isFreeItem=0 and IsExchange=0 and UnitPrice>0)           
	 INSERT INTO [dbo].[ProductPricingInPriceLevel] ([PriceLevelAutoId],[ProductAutoId],[UnitAutoId],[CustomPrice],OrderAutoId)      
	 SELECT DISTINCT @PriceLevelAutoId,tb.ProductAutoId,pd.UnitType,       
	 cast (pd.Qty*(TB.UnitPrice/convert(decimal(18,4), (select pdin.Qty from PackingDetails as pdin where pdin.ProductAutoId=tb.ProductAutoId       
	 and pdin.UnitType=tb.UnitTypeAutoId))) as decimal(18,4)),
	 @OrderAutoId
	 FROM OrderItemMaster AS tb       
	 inner join PackingDetails as pd on pd.ProductAutoId=tb.ProductAutoId        
	 where  
	  OrderAutoId=@OrderAutoId  and tb.isFreeItem=0 and tb.IsExchange=0   
	  and UnitPrice>0   



end  