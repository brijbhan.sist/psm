USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedTableType [dbo].[CustomerPayment]    Script Date: 11/20/2019 5:05:52 AM ******/
CREATE TYPE [dbo].[CustomerPayment] AS TABLE(
	[OrderAutoId] [int] NULL,
	[ReceivedAmount] [decimal](10, 2) NULL
)
GO
