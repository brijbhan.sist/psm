USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcWeb_AdminLogin]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
CREATE PROCEDURE [dbo].[ProcWeb_AdminLogin]                        
@Opcode int=NULL,                        
@EmpAutoId int=null,                        
@EmpType varchar(50)=null,                        
@UserName varchar(100)=null,                        
@Password varchar(max)=null,                        
@NewPassword varchar(max)=null,                        
@IPAddress varchar(max)=null,                        
@isException bit out,                        
@exceptionMessage varchar(max) out                        
AS                        
BEGIN                        
 BEGIN TRY                        
     SET @exceptionMessage= 'Success'                        
     SET @isException=0              
  IF @Opcode=21                         
  BEGIN                        
    IF EXISTS(SELECT [Password] from EmployeeMaster where AutoId=@EmpAutoId and CONVERT(varchar(100),DecryptByPassphrase('WHM',[Password]))=@Password )                        
    BEGIN                        
      UPDATE EmployeeMaster set Password=EncryptByPassPhrase('WHM',@NewPassword) where AutoId=@EmpAutoId                        
    END                        
   else                        
    BEGIN                        
     SET @isException=1                        
     SET @exceptionMessage= 'Old Password Not Match !!!'                        
    END                        
  END                        
                        
  IF @Opcode=41       
  BEGIN     
                                                  
     IF EXISTS(SELECT * FROM [dbo].[EmployeeMaster] as EM  WHERE [UserName]=@UserName                      
     and em.Status=1                    
     and CONVERT(varchar(100),DecryptByPassphrase('WHM',[Password]))=@Password                      
     and (isApplyIp=0 or IP_Address like '%'+@IPAddress+'%')  )                      
     BEGIN                       
  SELECT 'success' as response,ProfileName,EM.AutoId,EM.[EmpType] As EmpTypeNo,ETM.TypeName AS [EmpType],                      
  [FirstName] AS Name,[UserName] ,[EmpId] ,Email,(select top 1 Logo from CompanyDetails) as ImageURL,    
  CASE     
  WHEN (select convert(date,SubscriptionExpiryDate) from CompanyDetails) < convert(date,GETDATE()) then     
  'Your subscription expired on' +' '+ (select FORMAT(SubscriptionExpiryDate,'dd MMMM yyyy')+'.' from CompanyDetails)    
  WHEN (select convert(date,SubscriptionAlertDate) from CompanyDetails) <= convert(date,GETDATE()) then     
  'Your subscription will expire on' +' '+ (select FORMAT(SubscriptionExpiryDate,'dd MMMM yyyy')+'.' from CompanyDetails) else     
  '0' end as Submsg,      
  CASE     
        WHEN (select convert(date,SubscriptionExpiryDate) from CompanyDetails) < convert(date,GETDATE()) then 1    
        WHEN (select convert(date,SubscriptionAlertDate) from CompanyDetails) <= convert(date,GETDATE()) then 0 else     
        '1' end as ExpMsg     
  FROM [dbo].[EmployeeMaster] as EM                       
  INNER JOIN [dbo].[EmployeeTypeMaster] AS ETM on EM.EmpType=ETM.AutoId  WHERE [UserName]=@UserName                      
  and em.Status=1                    
  and CONVERT(varchar(100),DecryptByPassphrase('WHM',[Password]))=@Password                      
  and (isApplyIp=0 or IP_Address like '%'+@IPAddress+'%')                      
                  
      SET @EmpAutoId=(SELECT top 1 AutoId from [dbo].[EmployeeMaster] as EM  WHERE [UserName]=@UserName                      
      and CONVERT(varchar(100),DecryptByPassphrase('WHM',[Password]))=@Password                      
      )                      
      insert into ipaddress(EmpAutoId,IPAddress,LoginDate,Remarks)                       
      values(@EmpAutoId,@IPAddress,GETDATE(),'login successfully.')      
                   
      end                     
      ELSE IF EXISTS(SELECT * FROM [dbo].[EmployeeMaster] as EM  WHERE [UserName]=@UserName                      
      and CONVERT(varchar(100),DecryptByPassphrase('WHM',[Password]))=@Password                       
      and em.Status=0                
      )                  
     BEGIN                  
    SET @isException=1                      
    SET @exceptionMessage= 'Access Denied'                  
     end                   
     ELSE IF EXISTS(SELECT * FROM [dbo].[EmployeeMaster] as EM  WHERE [UserName]=@UserName                      
     and em.Status=1                    
     and CONVERT(varchar(100),DecryptByPassphrase('WHM',[Password]))=@Password                      
     and isApplyIp=1)                  
     BEGIN                  
    SET @isException=1                      
    SET @exceptionMessage= 'Access Denied'                  
     end                  
     ELSE                  
     BEGIN                  
    SET @isException=1                      
    SET @exceptionMessage= 'Username And / Or Password Incorrect'                  
     END          
                    
    END                        
  Else if @Opcode=42                        
   Begin                        
          select logo,CompanyId from CompanyDetails                        
   End      
           
               
 END TRY                        
 BEGIN CATCH                        
   SET @isException=1                        
   SET @exceptionMessage= ERROR_MESSAGE()                
 END CATCH                        
END   
GO
