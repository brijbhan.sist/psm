USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_ReceivedPieces]    Script Date: 02/11/2020 16:49:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION  [dbo].[FN_ReceivedPieces]
(
	 @POAutoId int,
	 @ProductAutoId int,
	 @UnitAutoId int
)
RETURNS int
AS
BEGIN
	DECLARE @ReceivedPieces int	 
	
	set @ReceivedPieces=(Select Count(PoAutoId) from PurchaseProductsMaster where POAutoId=@POAutoId)

	set @ReceivedPieces=(select sum(ISNULL(QtyPerUnit,0)* ISNULL(ReceiveQty,0)) as RecPieces from POReceiveProduct as prp inner join POReceiveMaster as prm on prm.AutoId=prp.POReceiveAutoId
	                   where prm.POAutoId=@POAutoId and prp.ProductAutoId=@ProductAutoId and prp.UnitAutoId=@UnitAutoId)

	RETURN @ReceivedPieces
END