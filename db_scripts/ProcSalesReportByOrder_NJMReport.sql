create or
alter procedure ProcSalesReportByOrder_NJMReport                      
@Opcode INT=NULL,                      
@ProductAutoId int = null,              
@BrandAutoIdSring varchar(200) = null,                     
@CategoryAutoId int = null,      
@CustomerAutoId  int = null,                    
@SubCategoryId int = null,                      
@OrderStatus int = null,                      
@EmpAutoId int = null,                      
@SalesPerson  int=NULL,                            
@FromDate date =NULL,    
@CheckDB varchar(max)=null,
@ToDate date =NULL,
@SortBySoldQty int=NULL,
@PageIndex INT=1,                      
@PageSize INT=10,                      
@RecordCount INT=null,                      
@isException bit out,                      
@exceptionMessage varchar(max) out                      
as                   
BEGIN                       
 BEGIN TRY                      
  Set @isException=0                      
  Set @exceptionMessage='Success'                      
                      
  IF @Opcode=41                      
  BEGIN                          
	SELECT  PM.AutoId as PID,CONVERT(VARCHAR(10),PM.ProductId )+' - '+ PM.ProductName AS PN FROM ProductMaster AS PM              
	WHERE ((ISNULL(@BrandAutoIdSring,'0')='0')  or (PM.BrandAutoId in (select * from dbo.fnSplitString(@BrandAutoIdSring,','))))               
	AND (ISNULL(@CategoryAutoId,0)=0 OR pm.CategoryAutoId =@CategoryAutoId)                      
	AND (ISNULL(@SubCategoryId,0)=0 OR pm.SubcategoryAutoId =@SubCategoryId)               
	AND (PM.ProductStatus=1 or (Select count(ProductAutoId) from Delivered_Order_Items as OIM 
	where OIM.ProductAutoId=PM.AutoId AND PM.ProductStatus=0)>0) order by CONVERT(VARCHAR(10),PM.ProductId )+' - '+ PM.ProductName ASC    
	for json path
  END                     
 IF @Opcode=42   
    BEGIN    
	set @CheckDB=(select DB_NAME())

    SELECT ROW_NUMBER() OVER(ORDER BY ProductId) AS RowNumber, * INTO #Results FROM      
     (      

		 select om.OrderNo,FORMAT(om.OrderDate,'MM/dd/yyyy hh:mm tt') as OrderDate,cm.CustomerId,cm.CustomerName,bm.BrandName,
		pm.ProductId,pm.ProductName,um.UnitType+ ' ['+convert(varchar(10),pd.Qty)+' Pieces]' as Unit,
		convert(decimal(10,2),(convert(decimal(10,2),dio.StaticDelQty)/pd.Qty)) as [SoldQty] from Delivered_Order_Items as dio
		inner join OrderMaster as om on om.AutoId=dio.OrderAutoId
		inner join CustomerMaster as cm on cm.AutoId=om.CustomerAutoId
		inner join ProductMaster as pm on pm.AutoId=dio.ProductAutoId
		inner join BrandMaster as bm on pm.BrandAutoId=bm.AutoId
		inner join PackingDetails as pd on pd.ProductAutoId=pm.AutoId and pd.UnitType=pm.PackingAutoId
		inner join UnitMaster as um on um.AutoId=pd.UnitType
		where 
	    ((@CheckDB in ('psmnpa.a1whm.com','psmpa.a1whm.com','psmwpa.a1whm.com') and om.Status=11 and dio.Tax=1) or 
		(@CheckDB in ('psmnj.a1whm.com','psmct.a1whm.com') and dio.Item_TotalMLTax>0 and om.MLTax>0))
		 and StaticDelQty>0
		and (@FromDate is null or @FromDate = '' or @ToDate is null or @ToDate = '' or           
        (CONVERT(date,om.OrderDate)  between CONVERT(date,@FromDate) and CONVERT(date,@ToDate))) 
		
		AND  ((ISNULL(@BrandAutoIdSring,'0')='0') or (pm.BrandAutoId in (select * from dbo.fnSplitString(@BrandAutoIdSring,','))))               
		AND  (ISNULL(@ProductAutoId,0)=0 or dio.ProductAutoId =@ProductAutoId) 
		AND (ISNULL(@CategoryAutoId,0)=0 OR pm.CategoryAutoId =@CategoryAutoId)                      
		AND (ISNULL(@SubCategoryId,0)=0 OR pm.SubcategoryAutoId =@SubCategoryId)   
		AND (ISNULL(@CustomerAutoId,0)=0 OR cm.AutoId=@CustomerAutoId)   
		AND (ISNULL(@SalesPerson,0)=0 OR cm.SalesPersonAutoId=@SalesPerson)

   ) AS t  ORDER BY ProductId 
	SELECT COUNT(ProductId) AS RecordCount, case when @PageSize=0 then COUNT(ProductId) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Results    
	SELECT *  FROM #Results      
	WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))     
	SELECT ISNULL(SUM(SoldQty),0) as TotalSoldQty,
	FORMAT(GETDATE(),'MM/dd/yyyy hh:mm tt') as [PrintDate] 
	FROM #Results  
	end
  IF @Opcode=43                      
  BEGIN                      
  SELECT AutoId as CId,CategoryName as CN from CategoryMaster where Status=1  order by CN ASC   
  for json path
  END                      
    IF @Opcode=44                      
  BEGIN                      
 SELECT AutoId as  SCID,SubcategoryName as SCN from SubCategoryMaster where               
 (isnull(@CategoryAutoId,0)=0 or CategoryAutoId=@CategoryAutoId )and Status=1 order by SCN ASC     
    for json path 
END                      
  IF @Opcode=45                      
  BEGIN 
  select
  (
  SELECT AutoId AS EID,FirstName + ' ' + LastName as EN FROM EmployeeMaster where EmpType=2 and status=1 order by FirstName + ' ' + LastName ASC                              
  for json path
  )as SalesPer,
  (
  select AutoId as BID,BrandName as BN from BrandMaster where status = 1 order by BN asc     
  for json path
  )as Brand 
  for json path
          
  END                 
  IF @Opcode=46                      
  BEGIN   
	select AutoId as CUID,CustomerId+' - '+CustomerName as CUN from CustomerMaster as cm where 
	(ISNULL(@SalesPerson,0)=0 OR cm.SalesPersonAutoId =@SalesPerson) AND Status=1 order by CustomerId+' - '+CustomerName asc      
  for json path
  END                  
  END TRY                      
 BEGIN CATCH                      
  Set @isException=1                      
  Set @exceptionMessage=ERROR_MESSAGE()                      
 END CATCH                      
END 
