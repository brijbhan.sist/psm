USE [psmnj.a1whm.com]
GO

/****** Object:  Table [dbo].[EmployeeMaster_Log]    Script Date: 03-14-2021 03:38:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmployeeMaster_Log]') AND type in (N'U'))
DROP TABLE [dbo].[EmployeeMaster_Log]
GO

/****** Object:  Table [dbo].[EmployeeMaster_Log]    Script Date: 03-14-2021 03:38:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[EmployeeMaster_Log](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[EmpId] [varchar](12) NULL,
	[EmpType] [int] NULL,
	[FirstName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[Email] [varchar](100) NULL,
	[Contact] [varchar](12) NULL,
	[Address] [varchar](200) NULL,
	[State] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[Zipcode] [varchar](50) NULL,
	[Status] [int] NULL,
	[UserName] [varchar](100) NULL,
	[Password] [varchar](150) NULL,
	[CreatedDate] [date] NULL,
	[ImageURL] [varchar](100) NULL,
	[IP_Address] [varchar](max) NULL,
	[isApplyIP] [int] NULL,
	[UpdateDate] [datetime] NULL,
	[updateby] [int] NULL,
	[EmployeeCode] [varchar](30) NULL,
	[ProfileName] [varchar](30) NULL,
	[IsAppLogin] [int] NULL,
	[OptimotwFrom] [time](7) NULL,
	[OptimotwTo] [time](7) NULL,
 CONSTRAINT [PK__Employeee__6B232905C7E00FD4] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


