USE [psmnj.a1whm.com]
GO

/****** Object:  Table [dbo].[DriverLog_OrdDetails]    Script Date: 07/23/2020 21:34:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DriverLog_OrdDetails](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[DrvLogAutoId] [int] NULL,
	[OrderNo] [varchar](30) NULL,
	[OrderDate] [datetime] NULL,
	[CustomerId] [varchar](30) NULL,
	[CustomerName] [varchar](50) NULL,
	[CustOpenTime] [time](7) NULL,
	[CustCloseTime] [time](7) NULL,
	[DeliveredOrderTime] [time](7) NULL,
	[OrderAmount] [decimal](18, 2) NULL,
	[PayableAmount] [decimal](18, 2) NULL,
	[SetDeliveryFromTime] [time](7) NULL,
	[SetDeliveryToTime] [time](7) NULL,
	[ScheduleAt] [time](7) NULL,
	[StopNo] [int] NULL,
	[Remark] [varchar](500) NULL,
	[Address] [varchar](500) NULL,
	[Lat] [varchar](50) NULL,
	[Long] [varchar](50) NULL,
 CONSTRAINT [PK_DriverLog_OrdDetails] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


