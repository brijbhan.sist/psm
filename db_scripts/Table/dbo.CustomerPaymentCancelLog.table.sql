
/****** Object:  Table [dbo].[CustomerPaymentCancelLog]    Script Date: 07/20/2021 06:19:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CustomerPaymentCancelLog](
	[PaymentAutoId] [int] NOT NULL,
	[PaymentId] [varchar](50) NULL,
	[PaymentDate] [datetime] NULL,
	[EmpAutoId] [int] NULL,
	[PaymentRemarks] [varchar](500) NULL,
	[CancelDate] datetime
) ON [PRIMARY]
GO
