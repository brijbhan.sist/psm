USE [psmnj.a1whm.com]
GO

/****** Object:  Table [dbo].[SalesPaymentMaster]    Script Date: 07/20/2021 22:31:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SalesPaymentMasterLog](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[PaymentId] [int] NOT NULL,
	[PayId] [varchar](50) NOT NULL,
	[CustomerAutoId] [int] NOT NULL,
	[ReceivedDate] [date] NULL,
	[ReceivedAmount] [decimal](18, 2) NOT NULL,
	[PaymentMode] [int] NULL,
	[ReferenceId] [varchar](50) NULL,
	[Remarks] [varchar](500) NULL,
	[ReceiveDate] [datetime] NULL,
	[ReceiveBy] [int] NULL,
	[Status] [int] NULL,
	[PayType] [int] NULL,
	[OrderAutoId] [int] NULL,
	[cancelBy] [int] NULL,
	[CancelDate] [datetime] NULL,
	[CancelRemarks] [varchar](500) NULL,
	[referencePaymentNumber] [varchar](100) NULL,
	[DateUpdate] [datetime] NULL,
	[ChequeNo] [varchar](50) NULL,
	[ChequeDate] [datetime] NULL,
	[ChequeRemarks] [varchar](500) NULL,
	[BankCharges] [decimal](10, 2) NULL,
	[HoldBy] [int] NULL,
	[DeviceId] [varchar](500) NULL,
	[AppVersion] [varchar](50) NULL,
	[LatLong] [varchar](max) NULL,
)


