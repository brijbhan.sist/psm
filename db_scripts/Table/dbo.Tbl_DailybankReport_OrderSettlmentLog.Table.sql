USE [psmnj.a1whm.com]
GO
/****** Object:  Table [dbo].[Tbl_DailybankReport_OrderSettlmentLog]    Script Date: 07/21/2020 07:34:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_DailybankReport_OrderSettlmentLog](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[ReportAutoId] [int] NOT NULL,
	[CustomerId] [varchar](50) NULL,
	[CustomerName] [varchar](250) NULL,
	[OrderNo] [varchar](50) NULL,
	[OrderDate] [datetime] NULL,
	[SettlementTime] [time](7) NULL,
	[Cash] [decimal](18, 2) NULL,
	[Check] [decimal](18, 2) NULL,
	[CheckNO] [varchar](50) NULL,
	[CreditCard] [decimal](18, 2) NULL,
	[ElectronicTransfer] [decimal](18, 2) NULL,
	[MoneyOrder] [decimal](18, 2) NULL,
	[StoreCredit] [decimal](18, 2) NULL,
	[TotalAmount] [decimal](18, 2) NULL,
 CONSTRAINT [PK_Tbl_DailybankReport_OrderSettlmentLog] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tbl_DailybankReport_OrderSettlmentLog]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_DailybankReport_OrderSettlmentLog_Tbl_DailyBankReport] FOREIGN KEY([ReportAutoId])
REFERENCES [dbo].[Tbl_DailyBankReport] ([AutoId])
GO
ALTER TABLE [dbo].[Tbl_DailybankReport_OrderSettlmentLog] CHECK CONSTRAINT [FK_Tbl_DailybankReport_OrderSettlmentLog_Tbl_DailyBankReport]
GO
