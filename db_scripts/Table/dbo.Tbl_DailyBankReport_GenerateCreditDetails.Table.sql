USE [psmnj.a1whm.com]
GO
/****** Object:  Table [dbo].[Tbl_DailyBankReport_GenerateCreditDetails]    Script Date: 07/21/2020 07:34:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_DailyBankReport_GenerateCreditDetails](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[ReportAutoId] [int] NULL,
	[SettlementId] [varchar](50) NULL,
	[SettlementDate] [datetime] NULL,
	[CustomerId] [varchar](50) NULL,
	[CustomerName] [varchar](250) NULL,
	[PaymentMode] [varchar](50) NULL,
	[StoreCredit] [decimal](18, 2) NULL,
	[ReceivedBy] [varchar](250) NULL,
	[SettlementBy] [varchar](250) NULL,
 CONSTRAINT [PK_Tbl_DailyBankReport_GenerateCreditDetails] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tbl_DailyBankReport_GenerateCreditDetails]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_DailyBankReport_GenerateCreditDetails_Tbl_DailyBankReport] FOREIGN KEY([ReportAutoId])
REFERENCES [dbo].[Tbl_DailyBankReport] ([AutoId])
GO
ALTER TABLE [dbo].[Tbl_DailyBankReport_GenerateCreditDetails] CHECK CONSTRAINT [FK_Tbl_DailyBankReport_GenerateCreditDetails_Tbl_DailyBankReport]
GO
