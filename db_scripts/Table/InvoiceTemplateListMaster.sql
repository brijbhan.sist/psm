USE [psmnj.a1whm.com]
GO
/****** Object:  Table [dbo].[InvoiceTemplateListMaster]    Script Date: 11/12/2020 06:46:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InvoiceTemplateListMaster](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[InvoiceTemplateName] [varchar](100) NULL,
	[InvoiceTemplateDescription] [varchar](max) NULL,
	[CreationDate] [datetime] NULL,
	[UpdateDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[InvoiceTemplateListMaster] ON 

INSERT [dbo].[InvoiceTemplateListMaster] ([AutoId], [InvoiceTemplateName], [InvoiceTemplateDescription], [CreationDate], [UpdateDate]) VALUES (1, N'PSM Default - Invoice Only', N'PSM Default description', NULL, NULL)
INSERT [dbo].[InvoiceTemplateListMaster] ([AutoId], [InvoiceTemplateName], [InvoiceTemplateDescription], [CreationDate], [UpdateDate]) VALUES (2, N' PSM Default', N'PSM Default description', NULL, NULL)
INSERT [dbo].[InvoiceTemplateListMaster] ([AutoId], [InvoiceTemplateName], [InvoiceTemplateDescription], [CreationDate], [UpdateDate]) VALUES (3, N'Packing Slip', N'Packing Slip description', NULL, NULL)
INSERT [dbo].[InvoiceTemplateListMaster] ([AutoId], [InvoiceTemplateName], [InvoiceTemplateDescription], [CreationDate], [UpdateDate]) VALUES (4, N'Packing Slip - Without Category Breakdown', N'Packing Slip - Without Category Breakdown description', NULL, NULL)
INSERT [dbo].[InvoiceTemplateListMaster] ([AutoId], [InvoiceTemplateName], [InvoiceTemplateDescription], [CreationDate], [UpdateDate]) VALUES (5, N'7 Eleven (Type 1)', N'7 Eleven (Type 1) description', NULL, NULL)
INSERT [dbo].[InvoiceTemplateListMaster] ([AutoId], [InvoiceTemplateName], [InvoiceTemplateDescription], [CreationDate], [UpdateDate]) VALUES (6, N'7 Eleven (Type 2)', N'7 Eleven (Type 2) description', NULL, NULL)
INSERT [dbo].[InvoiceTemplateListMaster] ([AutoId], [InvoiceTemplateName], [InvoiceTemplateDescription], [CreationDate], [UpdateDate]) VALUES (7, N'Template 1', N'Template 1 description', NULL, NULL)
INSERT [dbo].[InvoiceTemplateListMaster] ([AutoId], [InvoiceTemplateName], [InvoiceTemplateDescription], [CreationDate], [UpdateDate]) VALUES (8, N'Template 2', N'Template 2 description', NULL, NULL)
INSERT [dbo].[InvoiceTemplateListMaster] ([AutoId], [InvoiceTemplateName], [InvoiceTemplateDescription], [CreationDate], [UpdateDate]) VALUES (9, N'PSMNPA Default', N'PSMNPA Default Description', NULL, NULL)
INSERT [dbo].[InvoiceTemplateListMaster] ([AutoId], [InvoiceTemplateName], [InvoiceTemplateDescription], [CreationDate], [UpdateDate]) VALUES (10, N'PSMPA Default', N'PSMPA Default Description', NULL, NULL)
SET IDENTITY_INSERT [dbo].[InvoiceTemplateListMaster] OFF
GO
