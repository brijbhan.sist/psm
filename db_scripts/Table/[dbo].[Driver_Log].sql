USE [psmnj.a1whm.com]
GO

/****** Object:  Table [dbo].[Driver_Log]    Script Date: 07/23/2020 21:35:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Driver_Log](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[DriverAutoId] [int] NULL,
	[DriverName] [varchar](30) NULL,
	[DriverId] [varchar](20) NULL,
	[OptimoFromTime] [time](7) NULL,
	[OptimoToTime] [time](7) NULL,
	[ScheduleFromTime] [nchar](10) NULL,
	[ScheduleToTime] [time](7) NULL,
	[ScheduleDate] [datetime] NULL,
	[CarId] [varchar](20) NULL,
	[CarName] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_Driver_Log] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


