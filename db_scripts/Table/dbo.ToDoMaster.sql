
CREATE TABLE [dbo].[ToDoMaster](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](150) NULL,
	[Description] [varchar](max) NULL,
	[Starred] [int] NULL,
	[Important] [int] NULL,
	[IsDeleted] [int] NULL,
	[EmpAutoId] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[IsCompleted] [int] NULL,
 CONSTRAINT [PK_ToDoMaster] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[ToDoMaster]  WITH CHECK ADD  CONSTRAINT [FK_ToDoMaster_UserMaster] FOREIGN KEY([EmpAutoId])
REFERENCES [dbo].[EmployeeMaster] ([AutoId])
GO

ALTER TABLE [dbo].[ToDoMaster] CHECK CONSTRAINT [FK_ToDoMaster_UserMaster]
GO
