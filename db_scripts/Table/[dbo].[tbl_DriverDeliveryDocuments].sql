USE [psmnj.a1whm.com]
GO

/****** Object:  Table [dbo].[tbl_DriverDeliveryDocuments]    Script Date: 05/30/2021 01:59:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tbl_DriverDeliveryDocuments](
[AutoId] [int] IDENTITY(1,1) NOT NULL,
[OrderAutoId] [int] NULL,
[fileType] [varchar](max) NULL,
[fileName] [varchar](max) NULL,
[filepath] [varchar](max) NULL,
[PhysicalPath] [varchar](max) NULL,
[Datetime] [datetime] NULL,
CONSTRAINT [PK__tbl_Driv__6B232905C9D515C4] PRIMARY KEY CLUSTERED
(
[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[tbl_DriverDeliveryDocuments] ADD  CONSTRAINT [DF_tbl_DriverDeliveryDocuments_Datetime]  DEFAULT (getdate()) FOR [Datetime]
GO

ALTER TABLE [dbo].[tbl_DriverDeliveryDocuments]  WITH CHECK ADD  CONSTRAINT [FK_tbl_DriverDeliveryDocuments_OrderMaster] FOREIGN KEY([OrderAutoId])
REFERENCES [dbo].[OrderMaster] ([AutoId])
GO

ALTER TABLE [dbo].[tbl_DriverDeliveryDocuments] CHECK CONSTRAINT [FK_tbl_DriverDeliveryDocuments_OrderMaster]
GO