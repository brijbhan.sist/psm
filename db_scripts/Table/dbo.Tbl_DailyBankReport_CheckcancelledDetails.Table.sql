USE [psmnj.a1whm.com]
GO
/****** Object:  Table [dbo].[Tbl_DailyBankReport_CheckcancelledDetails]    Script Date: 07/21/2020 07:34:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_DailyBankReport_CheckcancelledDetails](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[ReportAutoId] [int] NULL,
	[SettlementId] [varchar](50) NULL,
	[SettlementDate] [datetime] NULL,
	[CustomerId] [varchar](50) NULL,
	[CustomerName] [varchar](250) NULL,
	[ReferenceOrderNo] [varchar](50) NULL,
	[CheckNo] [varchar](50) NULL,
	[CheckDate] [date] NULL,
	[CheckAmount] [decimal](18, 2) NULL,
	[CancelDate] [datetime] NULL,
	[CancelBy] [varchar](250) NULL,
	[Remark] [varchar](max) NULL,
 CONSTRAINT [PK_Tbl_DailyBankReport_CheckcancelledDetails] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tbl_DailyBankReport_CheckcancelledDetails]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_DailyBankReport_CheckcancelledDetails_Tbl_DailyBankReport] FOREIGN KEY([ReportAutoId])
REFERENCES [dbo].[Tbl_DailyBankReport] ([AutoId])
GO
ALTER TABLE [dbo].[Tbl_DailyBankReport_CheckcancelledDetails] CHECK CONSTRAINT [FK_Tbl_DailyBankReport_CheckcancelledDetails_Tbl_DailyBankReport]
GO
