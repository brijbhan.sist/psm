USE [psmnj.a1whm.com]
GO
/****** Object:  Table [dbo].[Tbl_DailyBankReport]    Script Date: 07/21/2020 07:34:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_DailyBankReport](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[ReportDate] [date] NOT NULL,
	[Createddate] [datetime] NULL,
	[TotalCustomer] [int] NULL,
	[TotalOrder] [int] NULL,
	[CASH] [decimal](18, 2) NULL,
	[Check] [decimal](18, 2) NULL,
	[CreditCard] [decimal](18, 2) NULL,
	[ElectronicTransfer] [decimal](18, 2) NULL,
	[MoneyOrder] [decimal](18, 2) NULL,
	[StoreCreditApply] [decimal](18, 2) NULL,
	[StoreCreditGenerated] [decimal](18, 2) NULL,
	[TotalTransaction] [decimal](18, 2) NULL,
	[Short] [decimal](18, 2) NULL,
	[Expense] [decimal](18, 2) NULL,
	[DepositAmount]  AS (([TotalTransaction]+[StoreCreditGenerated])-((([ElectronicTransfer]+[StoreCreditApply])+[Short])+[Expense])),
 CONSTRAINT [PK_Tbl_DailyBankReport] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
