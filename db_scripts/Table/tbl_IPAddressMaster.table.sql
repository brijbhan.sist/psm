create table IpMaster
(
 AutoId int identity(1,1),
 IPAddress varchar(100),
 NameofLocation varchar(100),
 Description varchar(max),
 CreatedDate datetime,
 CreatedBy int,
 UpdatedDate datetime,
 UpdateBy int
)

