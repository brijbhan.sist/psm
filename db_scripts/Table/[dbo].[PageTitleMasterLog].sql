USE [psmnj.a1whm.com]
GO

/****** Object:  Table [dbo].[PageTitleMaster]    Script Date: 12/04/2020 05:38:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PageTitleMasterLog](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[PageId] [int] NULL,
	[PageUrl] [varchar](150) NULL,
	[PageTitle] [varchar](150) NULL,
	[UserDescription] [varchar](max) NULL,
	[AdminDescription] [varchar](max) NULL,
	[Status] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreateDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdateDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[PageTitleMaster] ADD  DEFAULT (getdate()) FOR [CreateDate]
GO


