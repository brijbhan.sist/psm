USE [psmnj.a1whm.com]
GO

/****** Object:  Table [dbo].[IpMaster]    Script Date: 03/16/2021 02:17:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IpMaster](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[IPAddress] [varchar](100) NULL,
	[NameofLocation] [varchar](100) NULL,
	[Description] [varchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdateBy] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


