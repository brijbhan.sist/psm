USE [psmnj.a1whm.com]
GO

/****** Object:  Table [dbo].[DraftCustomerMaster]    Script Date: 09/29/2020 00:17:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DraftCustomerMaster](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerName] [varchar](100) NULL,
	[CustomerType] [int] NULL,
	[SalesPersonAutoId] [int] NULL,
	[Status] [int] NULL,
	[Terms] [int] NULL,
	[TaxId] [varchar](50) NULL,
	[priceLevelId] [int] NULL,
	[BusinessName] [varchar](250) NULL,
	[OPTLicence] [varchar](250) NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[OptimotwFrom] [time](7) NULL,
	[OptimotwTo] [time](7) NULL,
 CONSTRAINT [PK_DraftCustomerMaster] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


GO

USE [psmnj.a1whm.com]
GO

/****** Object:  Table [dbo].[DraftShippingAddress]    Script Date: 09/29/2020 00:17:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DraftShippingAddress](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerAutoId] [int] NULL,
	[Address] [varchar](200) NULL,
	[Address2] [varchar](250) NULL,
	[State] [int] NULL,
	[Zipcode] [varchar](20) NULL,
	[IsDefault] [int] NULL,
	[SCityAutoId] [int] NOT NULL,
	[ZipcodeAutoid] [int] NULL,
	[SA_Lat] [varchar](20) NULL,
	[SA_Long] [varchar](20) NULL,
 CONSTRAINT [PK_DraftShippingAddress] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[DraftShippingAddress]  WITH CHECK ADD  CONSTRAINT [FK_DraftShippingAddress_DraftCustomerMaster] FOREIGN KEY([CustomerAutoId])
REFERENCES [dbo].[DraftCustomerMaster] ([AutoId])
GO

ALTER TABLE [dbo].[DraftShippingAddress] CHECK CONSTRAINT [FK_DraftShippingAddress_DraftCustomerMaster]
GO


USE [psmnj.a1whm.com]
GO

/****** Object:  Table [dbo].[DraftBillingAddress]    Script Date: 09/29/2020 00:18:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DraftBillingAddress](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerAutoId] [int] NULL,
	[Address] [varchar](200) NULL,
	[Address2] [varchar](250) NULL,
	[State] [int] NULL,
	[Zipcode] [varchar](20) NULL,
	[IsDefault] [int] NULL,
	[BCityAutoId] [int] NULL,
	[ZipcodeAutoid] [int] NULL,
	[BillSA_Lat] [varchar](50) NULL,
	[BillSA_Long] [varchar](50) NULL,
 CONSTRAINT [PK_DraftBillingAddress] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[DraftBillingAddress]  WITH CHECK ADD  CONSTRAINT [FK_DraftBillingAddress_DraftCustomerMaster] FOREIGN KEY([CustomerAutoId])
REFERENCES [dbo].[DraftCustomerMaster] ([AutoId])
GO

ALTER TABLE [dbo].[DraftBillingAddress] CHECK CONSTRAINT [FK_DraftBillingAddress_DraftCustomerMaster]
GO


USE [psmnj.a1whm.com]
GO

/****** Object:  Table [dbo].[DraftCustomerContactPerson]    Script Date: 09/29/2020 00:18:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DraftCustomerContactPerson](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerAutoId] [int] NULL,
	[ContactPerson] [varchar](500) NULL,
	[Type] [int] NULL,
	[MobileNo] [varchar](20) NULL,
	[Landline] [varchar](20) NULL,
	[Landline2] [varchar](20) NULL,
	[Fax] [varchar](20) NULL,
	[Email] [varchar](200) NULL,
	[AlternateEmail] [varchar](200) NULL,
	[ISDefault] [int] NULL,
 CONSTRAINT [PK_DraftCustomerContactPerson] PRIMARY KEY CLUSTERED 
(
	[AutoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[DraftCustomerContactPerson]  WITH CHECK ADD  CONSTRAINT [FK_DraftCustomerContactPerson_DraftCustomerMaster] FOREIGN KEY([CustomerAutoId])
REFERENCES [dbo].[DraftCustomerMaster] ([AutoId])
GO

ALTER TABLE [dbo].[DraftCustomerContactPerson] CHECK CONSTRAINT [FK_DraftCustomerContactPerson_DraftCustomerMaster]
GO


