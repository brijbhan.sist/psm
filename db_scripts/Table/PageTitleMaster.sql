Create table PageTitleMaster
(
AutoId int identity(1,1),
PageId int,
PageUrl varchar(150),
PageTitle varchar(150),
UserDescription varchar(max),
AdminDescription varchar(max),
Status int,
CreatedBy int,
CreateDate datetime default(getdate())
PRIMARY KEY (AutoId)
)


