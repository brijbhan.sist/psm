USE [psmnj.a1whm.com]
GO

/****** Object:  Table [dbo].[MessageImageurl]    Script Date: 11/13/2020 06:36:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MessageImageurl](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[MessageAutoId] [int] NULL,
	[messageImageUrl] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


