USE [psmnj.a1whm.com]
GO
/****** Object:  Table [dbo].[Tbl_DailyBankReport_ShortAmountDetails]    Script Date: 07/21/2020 07:34:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_DailyBankReport_ShortAmountDetails](
	[AutoID] [int] IDENTITY(1,1) NOT NULL,
	[ReportAutoId] [int] NULL,
	[SettlementId] [varchar](50) NULL,
	[SettlementDate] [datetime] NULL,
	[CustomerId] [varchar](50) NULL,
	[CustomerName] [varchar](250) NULL,
	[ReceivedBy] [varchar](250) NULL,
	[SettlementBy] [varchar](250) NULL,
	[PaymentMode] [varchar](50) NULL,
	[ReceivedAmount] [decimal](18, 2) NULL,
	[ShortAmount] [decimal](18, 2) NULL,
 CONSTRAINT [PK_Tbl_DailyBankReport_ShortAmountDetails] PRIMARY KEY CLUSTERED 
(
	[AutoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tbl_DailyBankReport_ShortAmountDetails]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_DailyBankReport_ShortAmountDetails_Tbl_DailyBankReport] FOREIGN KEY([ReportAutoId])
REFERENCES [dbo].[Tbl_DailyBankReport] ([AutoId])
GO
ALTER TABLE [dbo].[Tbl_DailyBankReport_ShortAmountDetails] CHECK CONSTRAINT [FK_Tbl_DailyBankReport_ShortAmountDetails_Tbl_DailyBankReport]
GO
