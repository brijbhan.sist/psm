--ItemBarcode
Alter table ItemBarcode Add BarcodeType int

--ProductMasterLog
CREATE TABLE [dbo].[ProductMasterLog](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NULL,
	[ProductName] [varchar](100) NULL,
	[ProductLocation] [varchar](50) NULL,
	[ImageUrl] [varchar](250) NULL,
	[CategoryAutoId] [int] NOT NULL,
	[SubcategoryAutoId] [int] NOT NULL,
	[Stock] [int] NOT NULL,
	[ProductType] [int] NULL,
	[TaxRate] [decimal](8, 2) NULL,
	[PackingAutoId] [int] NULL,
	[VendorAutoId] [int] NULL,
	[ReOrderMark] [int] NULL,
	[MLQty] [decimal](10, 2) NULL,
	[UpdateDate] [datetime] NULL,
	[ProductStatus] [int] NULL,
	[CreateBy] [varchar](100) NULL,
	[CreateDate] [datetime] NULL,
	[ModifiedBy] [varchar](100) NULL,
	[ModifiedDate] [datetime] NULL,
	[BrandAutoId] [int] NOT NULL,
	[P_CommCode] [decimal](12, 4) NULL,
	[WeightOz] [decimal](10, 2) NULL,
	[IsApply_ML] [bit] NULL,
	[IsApply_Oz] [bit] NULL,
	[ThumbnailImageUrl] [varchar](max) NULL,
	[ThirdImage] [varchar](250) NULL,
	[NewSRP] [decimal](18, 2) NULL,
	[ShowOnWebsite] [int] NULL,
	[IsOutOfStock] [int] NULL,
	[Description] [nvarchar](max) NULL,
	[P_SRP] [decimal](18, 2) NULL,
	[CurrentStock] [int] NULL,
	[CheckImageURLExistense]  int null,
	[CheckThirdImageExistense]  int null,
	[Product_ShowOnWebsite]  int null,
	[OriginalImageUrl] [varchar](250) NULL
) 

