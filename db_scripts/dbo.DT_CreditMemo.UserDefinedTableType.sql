USE [psmnj.a1whm.com]
GO
Drop procedure ProcOrderMaster
Drop procedure ProcManagerEditOrderMaster
DROP type DT_CreditMemo
go
CREATE TYPE [dbo].[DT_CreditMemo] AS TABLE(
	[CreditAutoId] [int] NULL,
	[DeductAmount] [decimal](18, 2) NULL,
	[Remarks] [varchar](500) NULL
)
GO
