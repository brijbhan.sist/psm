ALTER PROCEDURE [dbo].[ProcPayReceiveMaster]            
@Opcode int=NULL,            
@PayAutoId int=NULL,            
@CustomerAutoId int=NULL,       
@OrderAutoId varchar(max)=NULL,    
@PayId varchar(50)=NULL,            
@ReceiveDate DATE=NULL,            
@LoginEmpType int =NULL,            
@FromDate DATE=NULL,            
@ToDate DATE=NULL,            
@ReceiveAmount decimal(10,2)=NULL,            
@ReferenceId varchar(50)=NULL,            
@Status int=NULL,            
@PaymentMode int=NULL,            
@Remarks varchar(500)=NULL,  
@PageIndex INT=1,                      
@PageSize INT=10,  
@EmpAutoId int=NULL,            
@isException bit out,            
@exceptionMessage varchar(500) out            
            
AS            
BEGIN            
 BEGIN TRY            
  SET @isException=0            
  SET @exceptionMessage='Success!!'            
            
   IF @Opcode=11            
    BEGIN                 
      BEGIN TRY                   
       BEGIN TRAN            
			SET @PayId=(select dbo.SequenceCodeGenerator('SalesPaymentId'))       
			declare @Empype int=(select EmpType from employeeMaster where AutoId=@EmpAutoId)            
			INSERT INTO SalesPaymentMaster (PayId, CustomerAutoId, ReceivedDate, ReceivedAmount, PaymentMode,            
			ReferenceId,Remarks,ReceiveDate,ReceiveBy,Status,PayType,DateUpdate)             
			VALUES (@PayId,@CustomerAutoId,@ReceiveDate,@ReceiveAmount,@PaymentMode,@ReferenceId,@Remarks,GETDATE(),@EmpAutoId,1
			,case when @Empype=10 then 1 else 2 end,            
			GETDATE())   
			set @PayAutoId=SCOPE_IDENTITY()
			UPDATE SequenceCodeGeneratorMaster SET currentSequence=currentSequence+1 WHERE SequenceCode='SalesPaymentId'     
			if @OrderAutoId!=''
			begin
			insert into [dbo].[AttachedOrder_ReceivePayment] ([OrderAutoId],[PaymentAutoId]) 
			select convert(int,splitdata) as OrderAutoId,@PayAutoId from dbo.fnSplitString(@OrderAutoId,',')
			
			

			end
			COMMIT TRANSACTION            
      END TRY            
      BEGIN CATCH            
       ROLLBACK TRAN            
        SET @isException=1            
        SET @exceptionMessage='Oops! Something went wrong.Please try later.'    
      END CATCH            
    END            
    ELSE IF @Opcode=21            
    BEGIN            
     IF EXISTS(SELECT * FROM  SalesPaymentMaster WHERE PaymentAutoId=@PayAutoId AND Status=1)            
     BEGIN            
      UPDATE SalesPaymentMaster SET CustomerAutoId =@CustomerAutoId,ReceivedDate =@ReceiveDate,            
      PaymentMode=@PaymentMode,ReferenceId =@ReferenceId,Remarks=@Remarks,            
      ReceivedAmount =@ReceiveAmount,DateUpdate=GETDATE()  WHERE PaymentAutoId=@PayAutoId       
	  
	  delete from [AttachedOrder_ReceivePayment] where PaymentAutoId=@PayAutoId
	  if @OrderAutoId!=''
	   begin
		   insert into [dbo].[AttachedOrder_ReceivePayment] ([OrderAutoId],[PaymentAutoId]) 
		   select convert(int,splitdata) as OrderAutoId,@PayAutoId from dbo.fnSplitString(@OrderAutoId,',')
	   end
     END            
     ELSE            
     BEGIN            
      SET @isException = 1            
      SET @exceptionMessage = 'Payment can not be updated.'            
     END               
    END            
    ELSE IF @Opcode=31            
     BEGIN TRY            
        IF EXISTS(SELECT * FROM  SalesPaymentMaster WHERE PaymentAutoId=@PayAutoId AND Status=1)            
        BEGIN            
             DELETE FROM SalesPaymentMaster  WHERE PaymentAutoId=@PayAutoId            
        END            
        ELSE            
        BEGIN            
         SET @isException = 1            
         SET @exceptionMessage = 'Payment can not be deleted becuase payment has been processed.'            
        END            
     END TRY            
     BEGIN CATCH            
      SET @isException = 1            
      SET @exceptionMessage = 'Oops! Something went wrong.Please try later.'             
     END CATCH            
    ELSE IF @Opcode=41            
     BEGIN    
	 SELECT ROW_NUMBER() OVER(ORDER BY PayId) AS RowNumber, * INTO #Results FROM      
     (     
		SELECT  PaymentAutoId,PayId ,CustomerName,CONVERT(varchar(10),ReceivedDate,101) as ReceivedDate,            
		ReceivedAmount, 
		(select pm.paymentMode from paymentModeMaster as pm where pm.Autoid=spm.paymentMode)           
		PaymentMode,ReferenceId,Remarks,StatusType as Status,spm.Status as StatusCode            
		from SalesPaymentMaster AS spm            
		INNER JOIN CustomerMaster AS CM ON spm.customerAutoid = CM.AutoId            
		INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId=spm.Status and SM.[Category]='Pay'             
		where ReceiveBy =@EmpAutoId and
		(@CustomerAutoId=0 or @CustomerAutoId is null or  CustomerAutoId=@CustomerAutoId)            
		and (@PayId='' or @CustomerAutoId is null or  PayId like '%'+ @PayId +'%')         
		and spm.Status in (1)          
		and (@FromDate is null or @FromDate = '' or @Todate is null or @Todate = '' or CONVERT(date,ReceivedDate) between @FromDate and @Todate)                
		--Order by spm.PaymentAutoId desc
		) AS t  ORDER BY  PaymentAutoId desc,PayId desc

		SELECT COUNT(PayId) AS RecordCount, case when @PageSize=0 then COUNT(PayId) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #Results    
		SELECT *  FROM #Results      
		WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))   order by PaymentAutoId desc
     END          
    ELSE IF @Opcode=42            
     BEGIN            
      SELECT  PaymentAutoId,PayId,CustomerAutoId,CONVERT(varchar(10),ReceivedDate,101) as ReceivedDate,            
      ReceivedAmount,PaymentMode,ReferenceId,Remarks, Status            
       from SalesPaymentMaster AS spm where PaymentAutoId=@PayAutoId    

	   set @CustomerAutoId=(SELECT CustomerAutoId from SalesPaymentMaster  where PaymentAutoId=@PayAutoId )

	   SELECT CustomerAutoId,om.[AutoId],[OrderNo],CONVERT(VARCHAR(20),[OrderDate],101) AS OrderDate,DO.[AmtDue],DO.[GrandTotal],DO.[AmtPaid],StatusType            
       FROM [dbo].[OrderMaster] As OM            
      INNER JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId]   
	  inner join StatusMaster as sm on sm.AutoId=om.Status and sm.Category='OrderMaster'
      WHERE [CustomerAutoId] = @CustomerAutoId             
     AND ([Status]! = 8) AND DO.[AmtDue]>0

	  SELECT CustomerAutoId,om.[AutoId],[OrderNo],CONVERT(VARCHAR(20),[OrderDate],101) AS OrderDate,DO.[AmtDue],DO.[GrandTotal],DO.[AmtPaid],StatusType            
       FROM [dbo].[OrderMaster] As OM            
      INNER JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId]   
	  inner join StatusMaster as sm on sm.AutoId=om.Status and sm.Category='OrderMaster'
	  left join AttachedOrder_ReceivePayment as aop on aop.OrderautoId=om.[AutoId]
      WHERE [CustomerAutoId] = @CustomerAutoId and aop.PaymentAutoId=@PayAutoId            
     AND ([Status]! = 8) AND DO.[AmtDue]>0

     END            
                 
    ELSE IF @Opcode=43            
    BEGIN            
    SELECT [AutoId],[StatusType] FROM [dbo].[StatusMaster] WHERE [Category]='Pay'  order by [StatusType]            
    IF(@LoginEmpType != 2)            
     BEGIN                
      SELECT [AutoId],[CustomerId] + ' - ' + [CustomerName] As Customer FROM [dbo].[CustomerMaster]  order by Customer          
     END            
    ELSE            
     BEGIN            
      SELECT [AutoId],[CustomerId] + ' - ' + [CustomerName] As Customer FROM [dbo].[CustomerMaster] WHERE 
	  [SalesPersonAutoId]=@EmpAutoId  order by Customer       ASC           
     END 
	 
	 select AutoId,PaymentMode from PAYMENTModeMaster where status=1 order by PaymentMode
    END            
    ELSE IF @Opcode=44            
    BEGIN            
      SELECT CustomerAutoId,[AutoId],[OrderNo],CONVERT(VARCHAR(20),[OrderDate],101) AS OrderDate,DO.[AmtDue],DO.[GrandTotal],DO.[AmtPaid]            
      FROM [dbo].[OrderMaster] As OM            
      INNER JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId]                 
      WHERE [CustomerAutoId] = @CustomerAutoId AND            
      ( [Status] = 11) AND DO.[AmtDue] > 0.00   
	  
	  SELECT CustomerAutoId,om.[AutoId],[OrderNo],CONVERT(VARCHAR(20),[OrderDate],101) AS OrderDate,DO.[AmtDue],DO.[GrandTotal],DO.[AmtPaid],StatusType            
       FROM [dbo].[OrderMaster] As OM            
      INNER JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId]   
	  inner join StatusMaster as sm on sm.AutoId=om.Status and sm.Category='OrderMaster'
      WHERE [CustomerAutoId] = @CustomerAutoId             
     AND ([Status]! = 8) AND DO.[AmtDue]>0
    END            
 END TRY            
 BEGIN CATCH            
  SET @isException=1            
  SET @exceptionMessage='Oops! Something went wrong.Please try later.'   
 END CATCH            
END 
GO
