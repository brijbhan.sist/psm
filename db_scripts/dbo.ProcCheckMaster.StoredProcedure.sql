 
alter PROCEDURE [dbo].[ProcCheckMaster]                                    
@OpCode int=Null,              
@Type int = null,                    
@SecurityKey VARCHAR(20)=NULL,                  
@EmpType  int=NULL,                                 
@CheckAutoId  int=Null,                                    
@CheckId VARCHAR(12)=NULL,                                    
@VendorAutoId int=NULL,                                    
@CheckAmount decimal(18,2)=NULL,                                    
@CheckDate date=NULL,                                    
@FromDate date=NULL,                                    
@ToDate date=NULL,                                    
@VendorAddress varchar(250)=NULL,                                    
@EmpAutoId int=NULL,    
@PageIndex INT=1,      
@PageSize INT=10,      
@Memo varchar(250)=NULL,                                    
@isException bit out,                                    
@exceptionMessage varchar(max) out                                    
AS                                    
BEGIN                                    
 BEGIN TRY                                    
   SET @isException=0                                    
                 
   SET @exceptionMessage='success'                                 
 IF @OpCode=11                                    
 BEGIN                                    
  BEGIN TRY                                    
  BEGIN TRAN                                    
   if NOT EXISTS(select 1 from [dbo].[CheckMaster] where [CheckNO]=@CheckId)                            
   BEGIN                            
    INSERT INTO [dbo].[CheckMaster]([CheckNO],[VendorAutoId],[VendorAddress],[CheckAmount],[CheckDate],[CreateDate],[CreatedBy],Memo,Type)                                    
    VALUES (@CheckId,@VendorAutoId,@VendorAddress,@CheckAmount,@CheckDate,GETDATE(),@EmpAutoId,@Memo,@Type)                                    
   END                            
   ELSE                            
   BEGIN                            
    SET @isException=1                                    
    SET @exceptionMessage='Check No already exist.'                              
   END                            
  COMMIT TRANSACTION                                    
  END TRY                                     
  BEGIN CATCH                                    
  ROLLBACK TRAN                                    
   SET @isException=1                                    
   SET @exceptionMessage='Oops! Something went wrong.Please try later.'                                   
  END CATCH                                    
 END                                    
 ELSE IF @Opcode=21                                    
 BEGIN                           
  BEGIN TRY                                    
  BEGIN TRAN                                    
   if NOT EXISTS(select 1 from [dbo].[CheckMaster] where [CheckNO]=@CheckId and CheckAutoId != @CheckAutoId)                            
   BEGIN                                   
    UPDATE CheckMaster SET [CheckNO]=@CheckId,[VendorAutoId] = @VendorAutoId, VendorAddress = @VendorAddress,                                     
    CheckAmount = @CheckAmount,CheckDate=@CheckDate,Memo=@Memo,Type=@Type                                    
    WHERE CheckAutoId = @CheckAutoId                         
   END                            
   ELSE                            
   BEGIN                            
   SET @isException=1                                    
   SET @exceptionMessage='Check No already exist.'                              
   END                            
  COMMIT TRANSACTION                                    
  END TRY                                     
  BEGIN CATCH                                    
  ROLLBACK TRAN                                    
   SET @isException=1                                    
   SET @exceptionMessage='Oops! Something went wrong.Please try later.'                                   
  END CATCH                                   
 END                 
 ELSE IF @Opcode=31                                    
   BEGIN                                    
          DELETE FROM CheckMaster WHERE CheckAutoId = @CheckAutoId                                    
   END                                    
 ELSE IF @OpCode=41                                    
 BEGIN                                    
		SELECT ROW_NUMBER() over(order by [CheckDate] desc,CM.CheckAutoId desc) as RowNumber, CM.CheckAutoId,[CheckNO],(case   
		when ISNULL(Type,0) = 1 then vm.VendorName   
		when ISNULL(Type,0) = 2 then (em.FirstName + ' ' + em.LastName) else '' end) as Name              
		,[VendorAddress],[CheckAmount],FORMAT([CheckDate],'MM/dd/yyyy') as CheckDate  ,[CheckDate] as tdate                                  
		,CM.Memo,(case when ISNULL(Type,0) = 1 then 'Vendor' when ISNULL(Type,0) = 2 then 'Employee' else '' end) as Type                                   
		into #Results
		FROM  CheckMaster AS CM                                     
		left join VendorMaster AS vm ON vm.AutoId=CM.VendorAutoId              
		left join EmployeeMaster em on em.AutoId = cm.VendorAutoId                                    
		WHERE (@VendorAutoId is null or @VendorAutoId=0 or VendorAutoId  =@VendorAutoId)                                    
		and (@FromDate is null or @FromDate = '' or @Todate is null or @Todate = '' or ([CheckDate] between @FromDate and @Todate))                                        
		and (@CheckId is null or @CheckId='' or CheckNO like '%'+@CheckId+'%')  
		and (ISNULL(@Type,0)=0 or Type=@Type)                                                                    
                           
		SELECT case when @PageSize=0 then 0 else count(RowNumber) end as RecordCount,@PageSize as PageSize,@PageIndex as PageIndex FROM #Results                   
		SELECT * FROM #Results                                          
		WHERE @PageSize=0 or ( RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1) 
 END                                     
 ELSE IF @OpCode=42                                    
 BEGIN                                    
  SELECT CheckAutoId,[CheckNO],[VendorAutoId],[VendorAddress],[CheckAmount],dbo.[fNumToWords]([CheckAmount]) AS NumofWords,FORMAT([CheckDate],'MM/dd/yyyy')  as CheckDate,Memo,ISNULL(Type,0) as Type                                    
  FROM CheckMaster WHERE CheckAutoId = @CheckAutoId               
                
  set @Type = (SELECT type FROM CheckMaster WHERE CheckAutoId = @CheckAutoId)               
              
  if (@Type = 1)              
  begin              
  select AutoId, VendorName from VendorMaster where Status = 1 
  order by VendorName             
  select 1 as type              
  end              
  else if (@Type = 2)              
  begin              
  select AutoId, (FirstName + ' ' + LastName) as VendorName from EmployeeMaster 
  where EmpType not in (1,12) and status=1 order by (FirstName + ' ' + LastName)          
  select 2 as type              
  end                           
 END                                    
 ELSE IF @OpCode=43                                    
 BEGIN                    
  IF @Type=1          
  BEGIN                         
   SELECT ISNULL(Address,'') as Address,City,(Select top 1 State FROm state where State=vm.State) as StateName
   ,Zipcode,CompleteAddress as VendorAddress                                    
   FROM VendorMaster as vm WHERE vm.AutoId = @VendorAutoId            
  END           
  ELSE          
  BEGIN          
   --Declare @ZipCode varchar(50),@CityId int,@StateId int,@Coutnry varchar(50)       
   --Select @ZipCode=Zipcode from EmployeeMaster Where Autoid=@VendorAutoId          
   --Select @CityId=CityId from ZipMaster Where Zipcode=@ZipCode          
   --Select @StateId=StateId from cITYmASTER wHERE AutoId=@CityId          
   --Select @Coutnry=Country from Country Where AutoId=(Select CountryId from State Where AutoId=@StateId)      
   Select Address,City,State as StateName,Zipcode,ISNULL(Address,'')+'<br> '+ISNULL(City,'')+' '+State+' '+Zipcode as VendorAddress from       
   EmployeeMaster Where AutoId=@VendorAutoId 
  END                            
   END                                    
 ELSE IF @OpCode=44                                    
 BEGIN                                    
  Declare @EType int          
  Select @EType=Type from CheckMaster Where CheckAutoId=@CheckAutoId          
  IF @EType=1           
  BEGIN          
   SELECT CheckAutoId,[CheckNO],vm.VendorName,vm.[Address],vm.City,sm.StateName as State,Zipcode,[CheckAmount],                                    
   FORMAT([CheckDate],'MM/dd/yyyy')  as CheckDate,Memo,dbo.[fNumToWords]([CheckAmount]) AS NumofWords                  
   FROM CheckMaster as cm                                     
   inner join VendorMaster as vm on cm.VendorAutoId=vm.AutoId                  
   left join State as sm on sm.AutoId=vm.State WHERE CheckAutoId = @CheckAutoId             
  END          
  ELSE          
  BEGIN          
   SELECT CheckAutoId,[CheckNO],ISNULL(em.FirstName,'')+' '+ISNULL(em.LastName,'') as VendorName          
   ,cm.[VendorAddress] as [Address],em.City,em.State,Zipcode,[CheckAmount],                                    
   FORMAT([CheckDate],'MM/dd/yyyy')  as CheckDate,Memo,dbo.[fNumToWords]([CheckAmount]) AS NumofWords                  
   FROM CheckMaster as cm                                     
   inner join EmployeeMaster as em on cm.VendorAutoId=em.AutoId            
   WHERE CheckAutoId = @CheckAutoId             
  END                        
  select * from CompanyDetails                                
 END                     
 ELSE if @OpCode = 45                         
 begin                          
  select * from TBL_MST_SecurityMaster where SecurityValue = @SecurityKey and SecurityType = @EmpType and typeEvent = 4                          
 end             
 ELSE IF @Opcode = 46                             
 BEGIN               
  if @Type = 1              
  begin                             
   SELECT AutoId,[VendorName] FROM [dbo].[VendorMaster] WHERE [Status]=1  order by [VendorName]              
   select 1 as type              
  end              
  else --if @EmpType = 2              
  begin              
   select AutoId, (FirstName +' ' + LastName) as VendorName from EmployeeMaster      
   where EmpType not in (1,12) and Status = 1 order by (FirstName +' ' + LastName)  
   select 2 as type              
  end                           
   END 
 ELSE IF @Opcode = 47                             
 BEGIN            
 select  dbo.[fNumToWords](@CheckAmount) AS NumofWords
 End
 END TRY                                    
 BEGIN CATCH                                        
    SET @isException=1                                    
    SET @exceptionMessage='Oops! Something went wrong.Please try later.'                                   
 END CATCH                                    
END 
GO
