SELECT cm.CustomerName,em.FirstName[SalesRep],YEAR(om.OrderDate)[YearName],MONTH(om.OrderDate)[MonthNo],DATENAME(MONTH,om.OrderDate)[MonthName]
,count(1)[Orders],sum(om.PayableAmount)[Total] 
into #t1 FROM [psmnj.a1whm.com].dbo.CustomerMaster as cm
inner join [psmnj.a1whm.com].dbo.OrderMaster as om on om.CustomerAutoId = cm.AutoId
inner join [psmnj.a1whm.com].dbo.EmployeeMaster as em on em.AutoId = cm.SalesPersonAutoId
where YEAR(om.OrderDate) = YEAR(getdate()-1) and cm.CustomerType !=3
and om.Status in (11)  and em.FirstName not in ('Ajay','Palak')
group by cm.CustomerName,em.FirstName,YEAR(om.OrderDate),MONTH(om.OrderDate),DATENAME(MONTH,om.OrderDate)
 --select * from CustomerType
UNION ALL

SELECT cm.CustomerName,em.FirstName[SalesRep],YEAR(om.OrderDate)[YearName],MONTH(om.OrderDate)[MonthNo],DATENAME(MONTH,om.OrderDate)[MonthName]
,count(1)[Orders],sum(om.PayableAmount)[Total] 
 FROM [psmnj.easywhm.com].dbo.CustomerMaster as cm
inner join [psmnj.easywhm.com].dbo.OrderMaster as om on om.CustomerAutoId = cm.AutoId
inner join [psmnj.a1whm.com].dbo.EmployeeMaster as em on em.AutoId = cm.SalesPersonAutoId

where YEAR(om.OrderDate) = YEAR(getdate()-1) and cm.CustomerType !=3
and om.Status in (11)   and em.FirstName not in ('Ajay','Palak')
group by cm.CustomerName,em.FirstName,YEAR(om.OrderDate),MONTH(om.OrderDate),DATENAME(MONTH,om.OrderDate)

order by CustomerName,SalesRep,MonthNo

select CustomerName,SalesRep ,isnull(January,0)January,isnull(February,0)February,isnull(March,0)March,isnull(April,0)April,isnull(May,0)May
							,isnull(June,0)June,isnull(July,0)July,isnull(August,0)August
							,isnull(September,0)September,isnull(October,0)October,isnull(November,0)November,isnull(December,0) December
into #t2 from #t1
				PIVOT
					(
						MAX(Orders) 
						FOR  [MonthName]
						IN (
							January,February,March,April,May,June,July,August,September,October,November,December
							)
					) AS P

select CustomerName,SalesRep,sum(January)JanOrd,sum(February)FebOrd,sum(March)MarOrd,sum(April)AprOrd,sum(May)MayOrd
							,sum(June)JunOrd,sum(July)JulOrd,sum(August)AugOrd,sum(September)SepOrd,sum(October)OctOrd,sum(November)NovOrd,sum(December)DecOrd
into #t22 from #t2 
group by CustomerName,SalesRep


----------------------------

select CustomerName,SalesRep ,isnull(January,0)January,isnull(February,0)February,isnull(March,0)March,isnull(April,0)April,isnull(May,0)May
							,isnull(June,0)June,isnull(July,0)July,isnull(August,0)August
							,isnull(September,0)September,isnull(October,0)October,isnull(November,0)November,isnull(December,0) December
into #t3 from #t1
				PIVOT
					(
						MAX([Total]) 
						FOR  [MonthName]
						IN (
							January,February,March,April,May,June,July,August,September,October,November,December
							)
					) AS P
					
select CustomerName,SalesRep,sum(January)JanTotal,sum(February)FebTotal,sum(March)MarTotal,sum(April)AprTotal,sum(May)MayTotal
							,sum(June)JunTotal,sum(July)JulTotal,sum(August)AugTotal,sum(September)SepTotal,sum(October)OctTotal,sum(November)NovTotal,sum(December)DecTotal
 
into #t33 from #t3 
group by CustomerName,SalesRep

select t2.CustomerName,t2.SalesRep
						,t2.JanOrd
						,t3.JanTotal
						,t2.FebOrd
						,t3.FebTotal
						,t2.MarOrd
						,t3.MarTotal
						,t2.AprOrd
						,t3.AprTotal
						,t2.MayOrd
						,t3.MayTotal
						,t2.JunOrd
						,t3.JunTotal
						,t2.JulOrd
						,t3.JulTotal						
						,t2.AugOrd
						,t3.AugTotal
						,t2.SepOrd
						,t3.SepTotal
						,t2.OctOrd
						,t3.OctTotal
						,t2.NovOrd
						,t3.NovTotal
						,t2.DecOrd
						,t3.DecTotal
						,(t2.JanOrd+t2.FebOrd+t2.MarOrd+t2.AprOrd+t2.MayOrd+t2.JunOrd+t2.JulOrd+t2.AugOrd+t2.SepOrd+t2.OctOrd+NovOrd+DecOrd)[YearOrders]
						,(JanTotal+FebTotal+MarTotal+AprTotal+MayTotal+JunTotal+JulTotal+AugTotal+SepTotal+OctTotal+NovTotal+DecTotal)[YearTotal]

from #t22 as t2 
inner join #t33 as t3
on t2.CustomerName = t3.CustomerName and t2.SalesRep = t3.SalesRep




					 

drop table #t1,#t2 ,#t3,#t22,#t33
