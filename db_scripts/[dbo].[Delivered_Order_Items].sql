USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Order_IsMLTotalQty]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO 
create FUNCTION  [dbo].[FN_DelOrder_IsProductTax]  
(  
	@AutoId int,  
	@orderAutoId int  
)  
RETURNS decimal(18,2)  
AS  
BEGIN  
	DECLARE @Total decimal(18,2)=0.00,    
	 @Tax int=null  
	IF (select tax from Delivered_Order_Items where AutoId=@AutoId)=1
	BEGIN  
		set @Total=(select isnull((oim.NetPrice*om.TaxValue)/100,0) as Tax from Delivered_Order_Items as oim 
					inner join OrderMaster as om on om.AutoId=oim.OrderAutoId
					where oim.AutoId=@AutoId) 

	END  
	RETURN @Total  
END  
  
GO

alter table Delivered_Order_Items add IsDel_ProductTax  AS ([dbo].[FN_DelOrder_IsProductTax]([AutoId],[OrderAutoId]))