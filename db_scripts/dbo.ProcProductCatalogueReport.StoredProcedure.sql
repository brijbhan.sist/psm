ALTER procedure [dbo].[ProcProductCatalogueReport]
@CategoryAutoId int = null,
@ProductName varchar(100) = null,
@SubCategoryAutoId int = null,
@ProductAutoId int = null,
@ProductId varchar(10) = null,
@Who nvarchar(25)=NULL,
@isException bit out,
@exceptionMessage nvarchar(500) out,
@PageIndex INT=1,
@PageSize INT=10,
@opCode int=Null
as
begin  
begin try  
SET @exceptionMessage= 'Success'    
   SET @isException=0  
		if @opCode = 41
		begin
		select AutoId,CategoryName as CategoryName from CategoryMaster ORDER BY REPLACE(CategoryName , ' ','') asc
		end
		if @opCode = 42
		begin
		select scm.AutoId, SubcategoryName as Subcategory from SubCategoryMaster as scm
		inner join CategoryMaster as cm on cm.AutoId = scm.CategoryAutoId
		where CategoryAutoId = @CategoryAutoId ORDER BY REPLACE(SubcategoryName , ' ','') asc
		end
	
		if @opCode = 45
		begin		
			select ROW_NUMBER() OVER(ORDER BY CategoryName desc) AS RowNumber,
			pm.ProductId,cm.CategoryName,scm.SubcategoryName, pm.ProductName,um.UnitType, pd.Qty as [Qty],
			CONVERT(DECIMAL(10,2),pd.CostPrice) AS CostPrice,
			CONVERT(DECIMAL(10,2),pd.WHminPrice) AS [wholesaleminprice], 
			CONVERT(DECIMAL(10,2),pd.MinPrice) as [RetailMinPrice],
			CONVERT(DECIMAL(10,2),pd.Price)  as [BasePrice],pm.P_SRP as SRP  into #Results  from ProductMaster as pm
			inner join PackingDetails as pd on pm.AutoId = pd.ProductAutoId and pm.PackingAutoId=pd.UnitType
			inner join UnitMaster as um on pm.PackingAutoId=um.AutoId
			inner join CategoryMaster as cm on cm.AutoId = pm.CategoryAutoId
			inner join SubCategoryMaster as scm on scm.AutoId = pm.SubcategoryAutoId	
			where pm.ProductStatus = 1
			and	(@ProductId is null or @ProductId = '' or pm.ProductId like '%'+ @ProductId +'%')
			and (@ProductName is null or @ProductName = '' or pm.ProductName like '%'+@ProductName+'%')	
			and (@CategoryAutoId is null or @CategoryAutoId = 0 or cm.AutoId = @CategoryAutoId)
			and (@SubCategoryAutoId is null or @SubCategoryAutoId = 0 or scm.AutoId = @SubCategoryAutoId) 
			and (@ProductAutoId is null or @ProductAutoId = 0 or pm.AutoId = @ProductAutoId)
			order by pm.ProductId

			SELECT * FROM #Results
			WHERE @PageSize=0 or RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1										
			SELECT COUNT(CategoryName) AS RecordCount, case when @PageSize=0 then COUNT(CategoryName) else @PageSize end AS PageSize, @PageIndex AS PageIndex,Format(getdate(),'MM/dd/yyyy hh:mm tt') as PrintDate FROM #Results
				
			SELECT * FROM #Results
		end
		

		end try  
begin catch  
SET @isException=1  
   SET @exceptionMessage= ERROR_MESSAGE()   
end catch  
end  
GO
