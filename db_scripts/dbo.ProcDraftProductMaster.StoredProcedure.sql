--Alter table DraftPackingDetails add Rack varchar(15),Section varchar(15),Row varchar(15),BoxNo varchar(15)
--Alter table DraftPackingDetails drop column Location
--Alter table DraftPackingDetails add Location AS (Rack+'-'+Section+'-'+Row+'-'+BoxNo)  
Alter PROCEDURE [dbo].[ProcDraftProductMaster]                                                                                                                                                                                
@OpCode int=Null,                                                                                                                                                                                
@AutoId  int=Null,                                                                                                                                                                                
@CategoryAutoId int=Null,                                                                                                                                                                                
@SubcategoryAutoId  int=Null,                                                                                                                                                                                
@ProductId VARCHAR(12)=NULL,                                                                                                                                                                                
@ProductName VARCHAR(50)=NULL,                                                                                                                                                                                
@ProductLocation VARCHAR(20)=NULL,                                                                                                                                                                                
@SearchBy VARCHAR(20)=NULL,                                                                                                                                                                               
@EligibleforFree int =NULL,                                                                                                                                                                                
@ImageUrl varchar(250)=NULL, 
@ThumbnailImage100 varchar(250)=NULL,
@ThumbnailImage400 varchar(250)=NULL,
@Barcode varchar(50)=NULL,                                                                                                                                                                                
@VendorAutoId int=NULL,                                                                                                                                                                                
@BrandAutoId int =null,                                                                                                                                                                            
@ReOrderMark int=NULL,                                                                                                                                                                                
@WHminPrice decimal(10,2)=null,                                                                                                                                                                                
@PackingDetailAutoId int=NULL,                                                                                                                                                                                
@EmpAutoId int=NULL,                                                                                                                                                                               
@UnitType INT=NULL,                                                                                                                                                                                
@Qty int=NULL,                                                                                                                                                                                
@Status int=1,                   
@MinPrice DECIMAL(8,2)=NULL,                       
@CostPrice DECIMAL(8,2)=NULL,                       
@Price DECIMAL(8,2)=NULL,                        
@SRP DECIMAL(8,2) =NULL,                             
@PreDefinedBarcode VARCHAR(50)= NULL,                                                   
@CommCode DECIMAL(10,4)= NULL,                                                                      
@BarcodeAutoId INT= NULL,                                                                                        
@IsAppyMLQty int = null,                                                                    
@IsAppyWeightQty int = null,                                                                                           
@MLQty DECIMAL(8,2) =NULL,                                                                                         
@WeightOz DECIMAL(8,2) =NULL,                                                                                                          
@PageIndex INT = 1,                                                                                                                  
@PageSize INT = 10,                                                                                                
@PackingAutoId INT=0,                                                                                                                                                    
@RecordCount INT =null,                                                                                                                               
@dtbulkUnit DT_dtbulkUnitNew readonly,  
@LocationStatus DT_LocationStatus readonly,
@isException bit out,                                                                                                                                                       
@exceptionMessage varchar(max) out,      
@Message varchar(50)=null
AS                                                                                                                                     
BEGIN                                                                                                                                             
BEGIN TRY                                                                                                                                                       
	SET @isException=0                                                                                                                                                                       
	SET @exceptionMessage='Success' 
	Declare @RequestLocation varchar(50),@sql nvarchar(max) 
		IF @OpCode=11                                                                                                                                                                           
			BEGIN                                                                                                                                                                             
			IF EXISTS(SELECT [AutoId] FROM [psmnj.a1whm.com].[dbo].[ProductMaster] WHERE [ProductId] = @ProductId)                                                                                                                                                        
			BEGIN                                                                                                                                        
				SET @isException=1                                                                                                                                        
				SET @exceptionMessage='Product ID already exists.'      
			END                            
				else if exists(SELECT [AutoId] FROM [psmnj.a1whm.com].[dbo].[ProductMaster] WHERE ProductName = LTRIM(RTRIM(@ProductName)))                                  
			BEGIN                                              
				SET @isException=1                             
				SET @exceptionMessage='Product name already exists.'                                                                             
			END 			
			ELSE                                           
			BEGIN                                                                     
			BEGIN TRY                                                                                                                        
			BEGIN TRAN                                                                                                          
			if(DB_Name() not in ('psm.a1whm.com','demo.a1whm.com'))  
			BEGIN                                                                                                                                                                                 
				IF EXISTS(SELECT [AutoId] FROM [psmnj.a1whm.com].[dbo].[DraftProductMaster] WHERE [ProductId] = @ProductId)                                                                                                                                                        
				BEGIN                                                                                                                                        
					SET @isException=1                                                                                                                                        
					SET @exceptionMessage='Product ID already exists in draft request.'      
				END 
				else if exists(SELECT [AutoId] FROM [psmnj.a1whm.com].[dbo].[DraftProductMaster] WHERE ProductName = LTRIM(RTRIM(@ProductName)))                                   
				BEGIN                                              
					SET @isException=1                             
					SET @exceptionMessage='Product name already exists in draft request.'                                                                             
				END  
				ELSE if EXISTS(select Barcode from [psmnj.a1whm.com].[dbo].ItemBarcode where Barcode in (select Barcode  from @dtbulkUnit where Barcode!=''))
				BEGIN
					SELECT   @Message=COALESCE(@Message + ', ', '')+Barcode
					FROM [psmnj.a1whm.com].[dbo].ItemBarcode  where Barcode in (select Barcode from @dtbulkUnit where Barcode!='')
					SET @isException=1                                                                                             
					SET @exceptionMessage=@Message +' Barcode already exists.' 
				END
				ELSE
				BEGIN                                   
					INSERT INTO [psmnj.a1whm.com].[dbo].[DraftProductMaster]([ProductId], [ProductName], [ProductLocation], [ImageUrl], 
					[CategoryAutoId],[SubcategoryAutoId],[Stock],VendorAutoId,ReOrderMark,MLQty,ProductStatus,CreateBy,CreateDate,ModifiedBy,ModifiedDate,                                                                                                                    
					BrandAutoId,P_CommCode,WeightOz,IsApply_ML,IsApply_Oz,P_SRP,ThumbnailImage100,ThumbnailImage400,EmployeeName)                                                                                                                                          
					VALUES (@ProductId, @ProductName, Replace(DB_Name(),'.a1whm.com',''), @ImageUrl, @CategoryAutoId, @SubcategoryAutoId,                                                                                                                                                    
					0,@VendorAutoId,@ReOrderMark,@MLQty,1,@EmpAutoId,GETDATE(),@EmpAutoId,GETDATE(),                                                                                                                                                                
					(CASE WHEN ISNULL(@BrandAutoId,0)=0 THEN NULL ELSE @BrandAutoId END),@CommCode,@WeightOz,@IsAppyMLQty,@IsAppyWeightQty,
					@SRP,@ThumbnailImage100,@ThumbnailImage400,
					(SELECT FirstName+' '+ISNULL(LastName,'') FROM EmployeeMaster where AutoId=@EmpAutoId))
				  
				
					SET @AutoId = (SELECT SCOPE_IDENTITY())   

					insert into [psmnj.a1whm.com].[dbo].DraftProductLocation(ProductAutoId,LocationId,Status)
					select @AutoId,LocationId,ststus from @LocationStatus

					INSERT INTO [psmnj.a1whm.com].[dbo].[DraftPackingDetails] (WHminPrice,[UnitType],[Qty],[Price],[CostPrice],[MinPrice],                                                                                                                   
					[ProductAutoId],EligibleforFree,CreateBy,CreateDate,ModifiedBy,UpdateDate,LatestBarcode,Rack,Section,Row,BoxNo)                                                                                                                                                              
				
					SELECT [WHPrice],[UnitAutoId],[Qty],[BasePrice],[CostPrice],[RetailPrice],@AutoId,[SETFree], @EmpAutoId,
					GETDATE(),@EmpAutoId,GETDATE(),Barcode,Upper(Rack),Section,Row,BoxNo 
					FROM  @dtbulkUnit     
				
					update [psmnj.a1whm.com].[dbo].[DraftProductMaster] set PackingAutoId=(select UnitAutoId from @dtbulkUnit where SETDefault=1)                 
				
					EXEC [psmnj.a1whm.com].[dbo].[DraftProductEmailSending] @ProductId=@ProductId,@Status=@Status
				END
				END
				ELSE 
				BEGIN                                                                                                                                                                               
					IF EXISTS(SELECT [AutoId] FROM [dbo].[DraftProductMaster] WHERE [ProductId] = @ProductId)                                                                                                                                                        
				BEGIN                                                                                                                                        
					SET @isException=1                                                                                                                                        
					SET @exceptionMessage='Product ID already exists.'      
				END 
					else if exists(SELECT [AutoId] FROM [dbo].[DraftProductMaster] WHERE ProductName = LTRIM(RTRIM(@ProductName)))                                   
				BEGIN                                              
					SET @isException=1                             
					SET @exceptionMessage='Product name already exists.'                                                                             
				END     
				ELSE if EXISTS(select LatestBarcode from DraftPackingDetails where LatestBarcode in (select Barcode  from @dtbulkUnit))
				BEGIN
					SELECT   @Message=COALESCE(@Message + ', ', '')+LatestBarcode
					FROM DraftPackingDetails  where LatestBarcode in (select Barcode from @dtbulkUnit)
					SET @isException=1                                                                                             
					SET @exceptionMessage=@Message +' Barcode already exists in draft request.' 
				END
				ELSE
				BEGIN
					INSERT INTO [dbo].[DraftProductMaster]([ProductId], [ProductName], [ProductLocation], [ImageUrl], [CategoryAutoId],                                                                                                               
					[SubcategoryAutoId],[Stock],VendorAutoId,ReOrderMark,MLQty,ProductStatus,CreateBy,CreateDate,ModifiedBy,ModifiedDate,                                                                                                                    
					BrandAutoId,P_CommCode,WeightOz,IsApply_ML,IsApply_Oz,P_SRP,ThumbnailImage100,ThumbnailImage400)                                                                                                                                          
					VALUES (@ProductId, @ProductName, Replace(DB_Name(),'.a1whm.com',''), @ImageUrl, @CategoryAutoId, @SubcategoryAutoId,                                                                                                                                                    
					0,@VendorAutoId,@ReOrderMark,@MLQty,1,@EmpAutoId,GETDATE(),@EmpAutoId,GETDATE(),                                                                                                                                                                
					(CASE WHEN ISNULL(@BrandAutoId,0)=0 THEN NULL ELSE @BrandAutoId END),@CommCode,@WeightOz,
					@IsAppyMLQty,@IsAppyWeightQty,@SRP,@ThumbnailImage100,@ThumbnailImage400)                                                                                                                                                                   
					SET @AutoId = (SELECT SCOPE_IDENTITY())   

					insert into DraftProductLocation(ProductAutoId,LocationId,Status)
					select @AutoId,LocationId,ststus from @LocationStatus

					INSERT INTO [dbo].[DraftPackingDetails] (WHminPrice,[UnitType],[Qty],[Price],[CostPrice],[MinPrice],                                                                                                                   
					[ProductAutoId],EligibleforFree,CreateBy,CreateDate,ModifiedBy,UpdateDate,LatestBarcode,Rack,Section,Row,BoxNo)                                                                                                                                                              
					SELECT [WHPrice],[UnitAutoId],[Qty],[BasePrice],[CostPrice],[RetailPrice],@AutoId,[SETFree], @EmpAutoId,
					GETDATE(),@EmpAutoId,GETDATE(),Barcode,Upper(Rack),Section,Row,BoxNo 
					FROM  @dtbulkUnit  
					update [dbo].[DraftProductMaster] set PackingAutoId=(select UnitAutoId from @dtbulkUnit where SETDefault=1)
				
						EXEC [dbo].[DraftProductEmailSending] @ProductId=@ProductId,@Status=@Status				
				END
				END	                                                                                                                           
				COMMIT TRANSACTION                                                                                                                                   
				END TRY                        
				BEGIN CATCH                                                                         
				ROLLBACK TRAN                                                       
					SET @isException=1                                                        
					SET @exceptionMessage=ERROR_MESSAGE();                                            
				END CATCH                                         
				END                                                                                                             
			END                                                                                                               
	ELSE IF @OpCode=19                                                                          
			BEGIN  
			BEGIN TRY
				BEGIN TRAN           
						IF DB_NAME() not IN ('psm.a1whm.com','demo.a1whm.com')                                                                                                                 
						BEGIN
								SET @ProductName=(SELECT  ProductName FROM [psmnj.a1whm.com].[dbo].[ProductMaster] where ProductId=@ProductId)
								IF EXISTS(SELECT  1 FROM [psmnj.a1whm.com].[dbo].[ProductMaster] where ProductId=@ProductId)
								BEGIN
									SET @isException=1                                                        
									SET @exceptionMessage='Product Id already exists'
								END
								ELSE IF EXISTS(SELECT  1 FROM [psmnj.a1whm.com].[dbo].[ProductMaster] where ProductName=@ProductName)
								BEGIN
									SET @isException=1                                                        
									SET @exceptionMessage='Product Name already exists'
								END
								ELSE IF EXISTS(SELECT Barcode FROM [psmnj.a1whm.com].[dbo].[ItemBarcode]  WHERE ProductAutoId in (SELECT AutoId FROM DraftProductMaster WHERE ProductId=@ProductId))
								BEGIN
								SELECT   @Message=COALESCE(@Message + ', ', '')+Barcode
								FROM [psmnj.a1whm.com].[dbo].[ItemBarcode]  WHERE ProductAutoId in (SELECT AutoId FROM DraftProductMaster WHERE ProductId=@ProductId)						
								SET @isException=1                                                                                             
								SET @exceptionMessage=@Message +' Barcode already exists in draft request.' 
								END
								ELSE
								BEGIN
									Exec [psmnj.a1whm.com].[dbo].[ProcDraftProductMaster_ALL] @ProductId=@ProductId 
									Exec [psmnj.a1whm.com].[dbo].[ProcDraftPackingMaster_All] @ProductId=@ProductId

									INSERT INTO [psmnj.a1whm.com].[dbo].CreateProductFromDraftProductLog (ProductId,RequestFromLocation,RequestedByUserId,
									RequestedDate,ProductCreatedBy,ProductCreatedDate) 
									SELECT @ProductId,[ProductLocation],[CreateBy],[CreateDate],@EmpAutoId,GETDATE() FROM [DraftProductMaster] WHERE ProductId=@ProductId
 
									DELETE FROM [psmnj.a1whm.com].[dbo].[DraftPackingDetails] WHERE ProductAutoId=(SELECT AutoId FROM DraftProductMaster WHERE ProductId=@ProductId)
									DELETE FROM [psmnj.a1whm.com].[dbo].[DraftProductMaster] WHERE ProductId=@ProductId
								END
			 
						END 
						ELSE 
						BEGIN
								SET @ProductName=(SELECT  ProductName FROM [dbo].[ProductMaster] where ProductId=@ProductId)
								IF EXISTS(SELECT  1 FROM [dbo].[ProductMaster] where ProductId=@ProductId)
								BEGIN
									SET @isException=1                                                        
									SET @exceptionMessage='Product Id already exists'
								END
								ELSE IF EXISTS(SELECT  1 FROM [dbo].[ProductMaster] where ProductName=@ProductName)
								BEGIN
									SET @isException=1                                                        
									SET @exceptionMessage='Product Name already exists'
								END
								ELSE IF EXISTS(SELECT Barcode FROM [psmnj.a1whm.com].[dbo].[ItemBarcode]  WHERE ProductAutoId in (SELECT AutoId FROM DraftProductMaster WHERE ProductId=@ProductId))
								BEGIN
									SELECT   @Message=COALESCE(@Message + ', ', '')+Barcode
									FROM [psmnj.a1whm.com].[dbo].[ItemBarcode]  WHERE ProductAutoId in (SELECT AutoId FROM DraftProductMaster WHERE ProductId=@ProductId)						
									SET @isException=1                                                                                             
									SET @exceptionMessage=@Message +' Barcode already exists in draft request.' 
								END
								ELSE
								BEGIN
									Exec [dbo].[ProcDraftProductMaster_ALL] @ProductId=@ProductId 
									Exec [dbo].[ProcDraftPackingMaster_All] @ProductId=@ProductId

									INSERT INTO [dbo].CreateProductFromDraftProductLog (ProductId,RequestFromLocation,RequestedByUserId,
									RequestedDate,ProductCreatedBy,ProductCreatedDate) 
									SELECT @ProductId,[ProductLocation],[CreateBy],[CreateDate],@EmpAutoId,GETDATE() FROM [DraftProductMaster] WHERE ProductId=@ProductId
 
									DELETE FROM [dbo].[DraftPackingDetails] WHERE ProductAutoId=(SELECT AutoId FROM DraftProductMaster WHERE ProductId=@ProductId)
									DELETE FROM [dbo].[DraftProductMaster] WHERE ProductId=@ProductId
								END 			
						END

				COMMIT TRANSACTION
			END TRY
			BEGIN CATCH
			ROLLBACK TRAN
				SET @isException=1                                                        
				SET @exceptionMessage= ERROR_MESSAGE() --'Oops! Something went wrong.Please try later.'     
			END CATCH     
			END  

	ELSE IF @OpCode=51                                                                                                
			BEGIN                                                    
                                                                                                      
				IF DB_NAME() not IN ('psm.a1whm.com','demo.a1whm.com')  --demo added on 12/05/2019 By Rizwan Ahmad                                                                                                                
			BEGIN                                                                                                                                                      
				SELECT um.AutoId as UnitType,ISNULL(Qty,1) AS Qty,isnull(MinPrice,0.00)as MinPrice,isnull(CostPrice,0.00) as CostPrice,                                           
				isnull(Price,0)as Price,isnull(MinPrice,0) as SRP,isnull(WHminPrice,0) as WHminPrice,                                        
				case when ISNULL(PM.PackingAutoId,0)=PD.UnitType then ISNULL(PM.PackingAutoId,0) else 0 end as DefaultPack
				,Rack,Section,Row,BoxNo   ,                                                                                       
				case when ISNULL((Select COUNT(*) from OrderItemMaster where ProductAutoId=@AutoId AND UnitTypeAutoId=UM.AutoId),0)=0 then 0 else 1  end as OrderAttachment,                                                         
				UM.UnitType AS UnitName,isnull(PD.EligibleforFree,0) as Free,isnull(PD.AutoId,0) as PackingId,Rack,Section,Row,BoxNo
				FROM 
				[psmnj.a1whm.com].[dbo].UnitMaster AS UM 
				LEFT JOIN [psmnj.a1whm.com].[dbo].DraftPackingDetails AS PD ON UM.AutoId=PD.UnitType                                          
				AND PD.ProductAutoId=@AutoId                                          
				LEFT JOIN [psmnj.a1whm.com].[dbo].DraftProductMaster as pm on pm.AutoId=pd.ProductAutoId  order by UM.AutoId DESC                                                                                             
			END                                                                                                                  
			ELSE                                                        
			BEGIN                                                                                                              
				SELECT um.AutoId as UnitType,ISNULL(Qty,1) AS Qty,isnull(MinPrice,0.00)as MinPrice,isnull(CostPrice,0.00) as CostPrice,                                                                                                         
				isnull(Price,0)as Price,isnull(MinPrice,0) as SRP,isnull(WHminPrice,0) as WHminPrice,                           
				case when ISNULL(PM.PackingAutoId,0)=PD.UnitType then ISNULL(PM.PackingAutoId,0) else 0 end as DefaultPack,Location,                                         
				case when ISNULL((Select COUNT(*) from OrderItemMaster where ProductAutoId=@AutoId AND UnitTypeAutoId=UM.AutoId),0)=0 then 0 else 1  end as OrderAttachment,                                        
				UM.UnitType AS UnitName,isnull(PD.EligibleforFree,0) as Free,isnull(PD.AutoId,0) as PackingId,Rack,Section,Row,BoxNo
				FROM UnitMaster AS UM                                                                           
				LEFT JOIN DraftPackingDetails AS PD ON UM.AutoId=PD.UnitType                                          
				AND PD.ProductAutoId=@AutoId                                          
				LEFT JOIN DraftProductMaster as pm on pm.AutoId=pd.ProductAutoId  order by UM.AutoId DESC                                                                                
			END                                                                 
			END  
 
                                                                              
	ELSE IF @OpCode=21                                                                                                                                            
		BEGIN  
		 Begin try
		  --Begin transaction                                                     
			if(DB_Name() not in ('psm.a1whm.com','demo.a1whm.com'))                                                        
			BEGIN 
													
					IF EXISTS(SELECT [AutoId] FROM [dbo].[DraftProductMaster] WHERE ProductName = @ProductName AND AutoId!=@AutoId )                                                     
					BEGIN                                                                                                                                                                                
						SET @isException=1                                                                                             
						SET @exceptionMessage='Product name already exists in draft request.'                                                                                                                                                                                
					END 
					ELSE IF EXISTS(SELECT [AutoId] FROM [psmnj.a1whm.com].[dbo].[ProductMaster] WHERE ProductName = @ProductName)                                                    
					BEGIN                                                                                                                                                                                
						SET @isException=1                                                                                             
						SET @exceptionMessage='Product name already exists.'                                                                                                                                                                                
					END                                                                                       
					ELSE IF EXISTS(SELECT [AutoId] FROM [dbo].[DraftProductMaster] WHERE (ProductId=@ProductId AND AutoId!=@AutoId ))                                                                                                         
					BEGIN                                                                                                                                
						SET @isException=1                                                                                                                                                            
						SET @exceptionMessage='Product ID already exists in draft request.'                                                                                                                                                                                
					END
					ELSE IF EXISTS(SELECT [AutoId] FROM [psmnj.a1whm.com].[dbo].[ProductMaster] WHERE (ProductId=@ProductId))                                                                                                         
					BEGIN                                                                                                                                
						SET @isException=1                                                                                                                                                            
						SET @exceptionMessage='Product ID already exists.'                                                                                                                                                                                
					END                                                                                           
					ELSE                                                                                                                                                                                 
					BEGIN
                  
					UPDATE [psmnj.a1whm.com].[dbo].[DraftProductMaster] SET ProductId=@ProductId,[ProductName]=@ProductName,[ImageUrl]=@ImageUrl,[CategoryAutoId]=@CategoryAutoId,                                                                                          
					ReOrderMark =@ReOrderMark,[SubcategoryAutoId]=@SubcategoryAutoId,VendorAutoId=@VendorAutoId  ,                           
					MLQty=@MLQty, ProductStatus=1,  BrandAutoId=(CASE WHEN ISNULL(@BrandAutoId,0)=0 THEN NULL ELSE @BrandAutoId END),                                                                      
					ModifiedBy=@EmpAutoId,ModifiedDate=GETDATE(),P_CommCode=@CommCode,WeightOz=@WeightOz,IsApply_ML=@IsAppyMLQty,
					IsApply_Oz=@IsAppyWeightQty,P_SRP=@SRP,ThumbnailImage100=@ThumbnailImage100 ,ThumbnailImage400=@ThumbnailImage400 ,
					EmployeeName=(SELECT FirstName+' '+ISNULL(LastName,'') FROM EmployeeMaster where AutoId=@EmpAutoId)
					WHERE AutoId=@AutoId  					

					if EXISTS(select Barcode from [psmnj.a1whm.com].[dbo].ItemBarcode where Barcode in (select Barcode  from @dtbulkUnit where Barcode!=''))
					BEGIN
						SELECT   @Message=COALESCE(@Message + ', ', '')+Barcode
						FROM [psmnj.a1whm.com].[dbo].ItemBarcode  where Barcode in (select Barcode from @dtbulkUnit where Barcode!='')
						SET @isException=1                                                                                             
						SET @exceptionMessage=@Message +' Barcode already exists.' 
					END
					else
					BEGIN
					 delete from [psmnj.a1whm.com].[dbo].[DraftPackingDetails] where ProductAutoId=@AutoId 
					 
					 delete from [psmnj.a1whm.com].[dbo].DraftProductLocation where ProductAutoId=@AutoId

			         insert into [psmnj.a1whm.com].[dbo].DraftProductLocation(ProductAutoId,LocationId,Status)
				     select @AutoId,LocationId,ststus from @LocationStatus

					INSERT INTO [psmnj.a1whm.com].[dbo].[DraftPackingDetails] (WHminPrice,[UnitType],[Qty],[Price],[CostPrice],[MinPrice],                                                                                                                   
					[ProductAutoId],EligibleforFree,CreateBy,CreateDate,ModifiedBy,UpdateDate,LatestBarcode,Rack,Section,Row,BoxNo)                                                                                                                                                              
					SELECT [WHPrice],[UnitAutoId],[Qty],[BasePrice],[CostPrice],[RetailPrice],@AutoId,[SETFree], @EmpAutoId,
					GETDATE(),@EmpAutoId,GETDATE(),Barcode,Rack,Section,Row,BoxNo
					FROM  @dtbulkUnit  

					update [psmnj.a1whm.com].[dbo].[DraftProductMaster] set PackingAutoId=(select UnitAutoId from @dtbulkUnit 
					where SETDefault=1)

					if(@Status!=12)
					BEGIN
					    EXEC [psmnj.a1whm.com].[dbo].[DraftProductEmailSending] @ProductId=@ProductId,@Status=@Status
					END
					
					IF(@Status=12)
					BEGIN					    
						Exec [psmnj.a1whm.com].[dbo].[ProcDraftProductMaster] @Opcode=19, @ProductId=@ProductId,
						@isException = @isException OUTPUT,
						@exceptionMessage = @exceptionMessage OUTPUT  
				    	EXEC [psmnj.a1whm.com].[dbo].[RealProductEmailSending] @ProductId=@ProductId,@Status=1
						
					END
				 END
					
				END                                     
			END
			ELSE
			BEGIN
				IF EXISTS(SELECT [AutoId] FROM [dbo].[DraftProductMaster] WHERE ProductName = @ProductName AND AutoId!=@AutoId )                                                     
				BEGIN                                                                                                                                                                                
					SET @isException=1                                                                                             
					SET @exceptionMessage='Product name already exists in draft request.'                                                                                                                                                                                
				END
				ELSE IF EXISTS(SELECT [AutoId] FROM [dbo].[ProductMaster] WHERE ProductName = @ProductName)
				BEGIN                                                                                                                                                                                
					SET @isException=1                                                                                             
					SET @exceptionMessage='Product name already exists.'                                                                                                                                                                                
				END                                                                                         
					ELSE IF EXISTS(SELECT [AutoId] FROM [dbo].[DraftProductMaster] WHERE (ProductId=@ProductId AND AutoId!=@AutoId ))                                                                                                         
				BEGIN                                                                                                                                
					SET @isException=1                                                                                                                                                            
					SET @exceptionMessage='Product ID already exists in draft request.'                                                                                                                                                                                
				END
					ELSE IF EXISTS(SELECT [AutoId] FROM [dbo].[ProductMaster] WHERE (ProductId=@ProductId))
				BEGIN                                                                                                                                
					SET @isException=1                                                                                                                                                            
					SET @exceptionMessage='Product ID already exists.'                                                                                                                                                                                
				END   
			 
				ELSE                                                                                                                                                                                 
				BEGIN
					UPDATE [dbo].[DraftProductMaster] SET ProductId=@ProductId,[ProductName]=@ProductName,[ImageUrl]=@ImageUrl,[CategoryAutoId]=@CategoryAutoId,                                                                                          
					ReOrderMark =@ReOrderMark,ProductStatus=1,[SubcategoryAutoId]=@SubcategoryAutoId,VendorAutoId=@VendorAutoId  ,                           
					MLQty=@MLQty,  BrandAutoId=(CASE WHEN ISNULL(@BrandAutoId,0)=0 THEN NULL ELSE @BrandAutoId END),                                                                      
					ModifiedBy=@EmpAutoId,ModifiedDate=GETDATE(),P_CommCode=@CommCode,WeightOz=@WeightOz,IsApply_ML=@IsAppyMLQty,
					IsApply_Oz=@IsAppyWeightQty,P_SRP=@SRP,ThumbnailImage100=@ThumbnailImage100 ,ThumbnailImage400=@ThumbnailImage400,
					EmployeeName=(SELECT FirstName+' '+ISNULL(LastName,'') FROM EmployeeMaster where AutoId=@EmpAutoId)
					WHERE AutoId=@AutoId 
					delete from DraftProductLocation where ProductAutoId=@AutoId
					insert into DraftProductLocation(ProductAutoId,LocationId,Status)
					select @AutoId,LocationId,ststus from @LocationStatus
					delete from [dbo].[DraftPackingDetails] where ProductAutoId=@AutoId		
					if EXISTS(select LatestBarcode from DraftPackingDetails where LatestBarcode in (select Barcode  from @dtbulkUnit))
						BEGIN
							SELECT   @Message=COALESCE(@Message + ', ', '')+LatestBarcode
							FROM DraftPackingDetails  where LatestBarcode in (select Barcode from @dtbulkUnit)
							SET @isException=1                                                                                             
							SET @exceptionMessage=@Message +' Barcode already exists in draft request.' 
						END
					Else
					BEGIN
					INSERT INTO [dbo].[DraftPackingDetails] (WHminPrice,[UnitType],[Qty],[Price],[CostPrice],[MinPrice],                                                                                                                   
					[ProductAutoId],EligibleforFree,CreateBy,CreateDate,ModifiedBy,UpdateDate,LatestBarcode,Rack,Section,Row,BoxNo)                                                                                                                                                              
					SELECT [WHPrice],[UnitAutoId],[Qty],[BasePrice],[CostPrice],[RetailPrice],@AutoId,[SETFree], @EmpAutoId,
					GETDATE(),@EmpAutoId,GETDATE(),Barcode,Upper(Rack),Section,Row,BoxNo 
					FROM  @dtbulkUnit 
					END
					update [dbo].[DraftProductMaster] set PackingAutoId=(select UnitAutoId from @dtbulkUnit 
					where SETDefault=1)   
					if(@Status!=12)
					BEGIN
							EXEC [dbo].[DraftProductEmailSending] @ProductId=@ProductId,@Status=@Status
					END 
					IF(@Status=12)
					BEGIN				
						Exec [dbo].[ProcDraftProductMaster] @Opcode=19, @ProductId=@ProductId,@isException=1 ,@exceptionMessage='' 
						EXEC [dbo].[RealProductEmailSending] @ProductId=@ProductId,@Status=1
					END   
				--commit transaction
				END      
			END   
		 End try
		 Begin catch
		   --Rollback transaction
		   SET @isException=1
		   SET @exceptionMessage=ERROR_MESSAGE()
		 End catch
		END                                                                                                                                                                                                                                                    
                                                                          
	ELSE IF @OpCode=41                                                                                       
			BEGIN   
				if(DB_Name() not in ('psm.a1whm.com','demo.a1whm.com'))                                                        
			BEGIN                                     
				SELECT [AutoId], [CategoryName] FROM [psmnj.a1whm.com].[dbo].[CategoryMaster] WHERE [Status]=1 ORDER BY  CategoryName ASC                                                                                                                    
				SELECT AutoId,VendorName FROM [psmnj.a1whm.com].[dbo].VENDORMASTER WHERE Status=1                                        
				SELECT AutoId,BrandName FROM [psmnj.a1whm.com].[dbo].BrandMaster WHERE Status=1                                                              
				SELECT AutoId,CommissionCode,C_DisplayName from [psmnj.a1whm.com].[dbo].Tbl_CommissionMaster where Status=1  
			end 
			else
			begin
				SELECT [AutoId], [CategoryName] FROM [dbo].[CategoryMaster] WHERE [Status]=1 ORDER BY  CategoryName ASC                                                                                                                    
				SELECT AutoId,VendorName FROM VENDORMASTER WHERE Status=1                                        
				SELECT AutoId,BrandName FROM BrandMaster WHERE Status=1                                                              
				SELECT AutoId,CommissionCode,C_DisplayName from Tbl_CommissionMaster where Status=1  
			end                            
			END                                                 
	ELSE IF @Opcode=42                                                                                                                                                                                
			BEGIN      
				if(DB_Name() not in ('psm.a1whm.com','demo.a1whm.com'))                                                        
			BEGIN                                                                    
				SELECT [AutoId], [SubcategoryName] FROM [psmnj.a1whm.com].[dbo].[SubCategoryMaster] WHERE [Status]=1 AND [CategoryAutoId]=@CategoryAutoId  ORDER BY  SubcategoryName ASC                                      
			end                          
			else                                                     
			BEGIN                                                                    
				SELECT [AutoId], [SubcategoryName] FROM [dbo].[SubCategoryMaster] WHERE [Status]=1 AND [CategoryAutoId]=@CategoryAutoId  ORDER BY  SubcategoryName ASC                                      
			end                              
			END                                                                                                                                                                                
	ELSE IF @Opcode=43                                             
			BEGIN                                                                  
				SELECT * FROM [dbo].[UnitMaster]                                                                          
			END                                                                                   
	ELSE IF @Opcode=44                                                                                                                                                                                
			BEGIN       
 
			if(DB_Name()  in ('psm.a1whm.com','demo.a1whm.com'))                                                        
			BEGIN 
                                                                                  
				SELECT  COUNT(distinct [ProductId])                                                                                                                                                                                
				AS RecordCount, @PageSize AS PageSize, @PageIndex AS PageIndex FROM DraftProductMaster as PM                                                                                                                                   
				INNER JOIN [dbo].[CategoryMaster] AS CM ON PM.CategoryAutoId = CM.AutoId                                                                                                                                         
				INNER JOIN [dbo].[SubCategoryMaster] AS SCM ON PM.SubcategoryAutoId = SCM.AutoId                                                                                                                                                                      
				left join dbo.UnitMaster as um on PM.PackingAutoId=um.AutoId             
				left join DraftPackingDetails as pd on pd.ProductAutoId=pm.AutoId     
				WHERE (@CategoryAutoId IS NULL OR @CategoryAutoId='' OR PM.[CategoryAutoId] = @CategoryAutoId)                                                          
				and (@Status IS NULL OR @Status=2 OR PM.ProductStatus = @Status)                                                                                                                                                                               
				AND (@SubcategoryAutoId IS NULL OR @SubcategoryAutoId='' OR PM.[SubcategoryAutoId] = @SubcategoryAutoId)                                                                                                                                    
				AND (@ProductId IS NULL OR @ProductId='' OR PM.[ProductId] LIKE '%'+ @ProductId +'%')                                                                   
				AND (@ProductName IS NULL OR @ProductName='' OR PM.ProductName LIKE '%' + @ProductName + '%')                                                            
				AND (@PreDefinedBarcode IS NULL OR @PreDefinedBarcode='' OR pd.LatestBarcode=@PreDefinedBarcode)     
				AND (@ProductLocation IS NULL OR @ProductLocation='' OR pm.ProductLocation LIKE '%' + @ProductLocation + '%')     
				AND (ISNULL(@SearchBy,'0')='0'  OR (@SearchBy ='=' AND Stock=@Qty) OR (@SearchBy ='>' AND Stock>@Qty)     
				OR (@SearchBy ='<' AND Stock<@Qty))       
                                                                                                                                                                               
				select * from (                                                                      
				SELECT ROW_NUMBER() OVER(ORDER BY [ProductId] asc) AS RowNumber, *   FROM                                                                                                                                                                                
				(                                                                               
				SELECT distinct PM.[AutoId],[ProductId],[ProductName],[ProductLocation],
				(SELECT convert(varchar, PM.[CreateDate], 101)) as CreatedOn,
				(select FirstName+' '+LastName from EmployeeMaster as EM where EM.AutoId=PM.CreateBy) as  CreatedBy , 
				CM.CategoryName AS Category,SCM.SubcategoryName AS Subcategory,                                           
				ISNULL(convert(varchar,(Stock/(case (select Qty from DraftPackingDetails as temp1                                                                                                                                                 
				where temp1.productAutoId = pm.AutoId and temp1.UnitType =pm.PackingAutoId) when 0 then 1 else 
				(select Qty from DraftPackingDetails as temp1                                            
				where temp1.productAutoId = pm.AutoId and temp1.UnitType =pm.PackingAutoId) end)))                                                                               
				+' ('+um.UnitType+')'                                                                                                        
				,'') as [Stock] ,ReOrderMark,(SELECT top 1 ImageUrl FROM ProductImageUrl)+ImageUrl as ImageUrl,
				(case when pm.ProductStatus=1 then 'Active' else 'Inactive' end) as Status,    
				(select BrandName from BrandMaster as bm where bm.autoid=pm.Brandautoid) as  BrandName ,                                                                                                   
				cast(isnull(pm.P_CommCode,0) as decimal(10,4))  as  CommCode                                                                                                                                    
				FROM DraftProductMaster AS PM                                                                                                                                   
				INNER JOIN [dbo].[CategoryMaster] AS CM ON PM.CategoryAutoId = CM.AutoId                                                                                                                                          
				INNER JOIN [dbo].[SubCategoryMaster] AS SCM ON PM.SubcategoryAutoId = SCM.AutoId
				                                                                                                                         
     
				left join dbo.UnitMaster as um on PM.PackingAutoId=um.AutoId  
                                                                                                                                      
				left join DraftPackingDetails as pd on pd.ProductAutoId=pm.AutoId
				WHERE (@CategoryAutoId IS NULL OR @CategoryAutoId='' OR PM.[CategoryAutoId] = @CategoryAutoId)                                                                                     
				and (@Status IS NULL OR @Status=2 OR PM.ProductStatus = @Status)                                                                                                                         
				AND (@SubcategoryAutoId IS NULL OR @SubcategoryAutoId='' OR PM.[SubcategoryAutoId] = @SubcategoryAutoId)    
				AND (@ProductId IS NULL OR @ProductId='' OR PM.[ProductId] LIKE '%'+ @ProductId +'%')            
				AND (@ProductName IS NULL OR @ProductName='' OR PM.ProductName LIKE '%' + @ProductName + '%')                                                                                                                                             
				AND (@PreDefinedBarcode IS NULL OR @PreDefinedBarcode='' OR pd.LatestBarcode LIKE '%' + @PreDefinedBarcode + '%')                                                                      
				AND (ISNULL(@SearchBy,'0')='0'  OR (@SearchBy ='=' AND Stock=@Qty) OR (@SearchBy ='>' AND Stock>@Qty)                                                                                         
				OR (@SearchBy ='<' AND Stock<@Qty))                                                                                                                                  
				) AS t                                                                                  
				) as p                                                                                                          
				WHERE @PageSize=0 or  RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1      
				ORDER BY [ProductId] asc    
			END
			ELSE
			BEGIN
				SELECT  COUNT(distinct [ProductId])                                                                                                                                                                                
				AS RecordCount, @PageSize AS PageSize, @PageIndex AS PageIndex FROM [psmnj.a1whm.com].[dbo].DraftProductMaster as PM                                                                                                                                   
				INNER JOIN [psmnj.a1whm.com].[dbo].[CategoryMaster] AS CM ON PM.CategoryAutoId = CM.AutoId                                                                                                                                         
				INNER JOIN [psmnj.a1whm.com].[dbo].[SubCategoryMaster] AS SCM ON PM.SubcategoryAutoId = SCM.AutoId                                                                                                                                                                      
				left join [psmnj.a1whm.com].[dbo].UnitMaster as um on PM.PackingAutoId=um.AutoId             
				left join [psmnj.a1whm.com].[dbo].DraftPackingDetails as pd on pd.ProductAutoId=pm.AutoId     
				WHERE (@CategoryAutoId IS NULL OR @CategoryAutoId='' OR PM.[CategoryAutoId] = @CategoryAutoId)                                                          
				and (@Status IS NULL OR @Status=2 OR PM.ProductStatus = @Status)                                                                                                                                                                               
				AND (@SubcategoryAutoId IS NULL OR @SubcategoryAutoId='' OR PM.[SubcategoryAutoId] = @SubcategoryAutoId)                                                                                                                                    
				AND (@ProductId IS NULL OR @ProductId='' OR PM.[ProductId] LIKE '%'+ @ProductId +'%')                                                                   
				AND (@ProductName IS NULL OR @ProductName='' OR PM.ProductName LIKE '%' + @ProductName + '%')                                                            
				AND (@PreDefinedBarcode IS NULL OR @PreDefinedBarcode='' OR pd.LatestBarcode=@PreDefinedBarcode)     
				AND (@ProductLocation IS NULL OR @ProductLocation='' OR pm.ProductLocation LIKE '%' + @ProductLocation + '%')     
				AND (ISNULL(@SearchBy,'0')='0'  OR (@SearchBy ='=' AND Stock=@Qty) OR (@SearchBy ='>' AND Stock>@Qty)     
				OR (@SearchBy ='<' AND Stock<@Qty))       
                                                                                                                                                                               
				select * from (                                                                      
				SELECT ROW_NUMBER() OVER(ORDER BY [ProductId] asc) AS RowNumber, *   FROM                                                                                                                                                                                
				(                                                                               
				SELECT distinct PM.[AutoId],[ProductId],[ProductName],[ProductLocation],
				(SELECT convert(varchar, PM.[CreateDate], 101)) as CreatedOn,
				CASE 
				WHEN [ProductLocation]='PSMNJ' THEN (select FirstName+' '+LastName from [psmnj.a1whm.com].[dbo].EmployeeMaster as EM where EM.AutoId=PM.CreateBy)
				WHEN [ProductLocation]='PSMPA' THEN (select FirstName+' '+LastName from [psmpa.a1whm.com].[dbo].EmployeeMaster as EM where EM.AutoId=PM.CreateBy)
				WHEN [ProductLocation]='PSMNPA' THEN (select FirstName+' '+LastName from [psmnpa.a1whm.com].[dbo].EmployeeMaster as EM where EM.AutoId=PM.CreateBy)
				WHEN [ProductLocation]='PSMWPA' THEN (select FirstName+' '+LastName from [psmwpa.a1whm.com].[dbo].EmployeeMaster as EM where EM.AutoId=PM.CreateBy)
				WHEN [ProductLocation]='PSMCT' THEN (select FirstName+' '+LastName from [psmct.a1whm.com].[dbo].EmployeeMaster as EM where EM.AutoId=PM.CreateBy)
				else '' end
				as  CreatedBy ,
				CM.CategoryName AS Category,SCM.SubcategoryName AS Subcategory,                                           
				ISNULL(convert(varchar,(Stock/(case (select Qty from [psmnj.a1whm.com].[dbo].DraftPackingDetails as temp1                                                                                                                                                 
				where temp1.productAutoId = pm.AutoId and temp1.UnitType =pm.PackingAutoId) when 0 then 1 else (select Qty from [psmnj.a1whm.com].[dbo].DraftPackingDetails as temp1                                            
				where temp1.productAutoId = pm.AutoId and temp1.UnitType =pm.PackingAutoId) end)))                                                                               
				+' ('+um.UnitType+')'                                                                                                        
				,'') as [Stock] ,ReOrderMark,(SELECT top 1 ImageUrl FROM ProductImageUrl)+ImageUrl as ImageUrl,(case when pm.ProductStatus=1 then 'Active' else 'Inactive' end) as Status,    
				(select BrandName from [psmnj.a1whm.com].[dbo].BrandMaster as bm where bm.autoid=pm.Brandautoid) as  BrandName ,                                                                                                   
				cast(isnull(pm.P_CommCode,0) as decimal(10,4))  as  CommCode                                                                                                                                    
				FROM [psmnj.a1whm.com].[dbo].DraftProductMaster AS PM                                                                                                                                   
				INNER JOIN [psmnj.a1whm.com].[dbo].[CategoryMaster] AS CM ON PM.CategoryAutoId = CM.AutoId                                                                                                                                          
				INNER JOIN [psmnj.a1whm.com].[dbo].[SubCategoryMaster] AS SCM ON PM.SubcategoryAutoId = SCM.AutoId 
				left join [psmnj.a1whm.com].[dbo].UnitMaster as um on PM.PackingAutoId=um.AutoId                                         
				left join [psmnj.a1whm.com].[dbo].DraftPackingDetails as pd on pd.ProductAutoId=pm.AutoId
				WHERE (@CategoryAutoId IS NULL OR @CategoryAutoId='' OR PM.[CategoryAutoId] = @CategoryAutoId)                                                                                     
				and (@Status IS NULL OR @Status=2 OR PM.ProductStatus = @Status)                                                                                                                         
				AND (@SubcategoryAutoId IS NULL OR @SubcategoryAutoId='' OR PM.[SubcategoryAutoId] = @SubcategoryAutoId)    
				AND (@ProductId IS NULL OR @ProductId='' OR PM.[ProductId] LIKE '%'+ @ProductId +'%')            
				AND (@ProductName IS NULL OR @ProductName='' OR PM.ProductName LIKE '%' + @ProductName + '%')                                                                                                                                             
				AND (@PreDefinedBarcode IS NULL OR @PreDefinedBarcode='' OR pd.LatestBarcode LIKE '%' + @PreDefinedBarcode + '%')     
				AND (@ProductLocation IS NULL OR @ProductLocation='' OR pm.ProductLocation LIKE '%' + @ProductLocation + '%')                                                                          
				AND (ISNULL(@SearchBy,'0')='0'  OR (@SearchBy ='=' AND Stock=@Qty) OR (@SearchBy ='>' AND Stock>@Qty)                                                                                         
				OR (@SearchBy ='<' AND Stock<@Qty))                                                                                                                                  
				) AS t                                                                                  
				) as p                                                                                                          
				WHERE @PageSize=0 or  RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1      
				ORDER BY [ProductId] asc    
			END                        
			END                                                                                                                                             
                                                                                                              
	ELSE IF @Opcode=46                                                                                                  
			BEGIN                                                  
				if(DB_Name() not in ('psm.a1whm.com','demo.a1whm.com'))                                                        
				BEGIN                                                                                                     
					SET @CategoryAutoId=(select CategoryAutoId from [psmnj.a1whm.com].[dbo].DraftProductMaster where AutoId=@AutoId) 
				END
				ELSE
				BEGIN
					SET @CategoryAutoId=(select CategoryAutoId from [dbo].DraftProductMaster where AutoId=@AutoId) 
				END
				if(DB_Name() not in ('psm.a1whm.com','demo.a1whm.com'))                                                        
				BEGIN    
					SELECT [AutoId], [SubcategoryName] FROM [psmnj.a1whm.com].[dbo].[SubCategoryMaster] WHERE [Status]=1 AND [CategoryAutoId]=@CategoryAutoId 
					order by  [SubcategoryName]
					SELECT [AutoId], [CategoryName] FROM [psmnj.a1whm.com].[dbo].[CategoryMaster] WHERE [Status]=1   
					order by  [CategoryName]                                                             
					SELECT * FROM [psmnj.a1whm.com].[dbo].[UnitMaster]                                                                                                    
					SELECT [AutoId],[ProductId],[ProductName],[ProductStatus],[ProductLocation],'http://psmnj.a1whm.com' as imgURL,[ImageUrl],[CategoryAutoId],[SubcategoryAutoId],[Stock],ReOrderMark,MLQty,ProductStatus as Status,
					BrandAutoId,cast(isnull(pm1.P_CommCode,0) as decimal(10,4))  as  CommCode,WeightOz,isnull(IsApply_ML,0)                                                         
					as IsApply_ML,isnull(IsApply_Oz,0) as IsApply_Oz,    ISNULL(P_SRP,0) as SRP                                                                                                                  
					FROM [psmnj.a1whm.com].[dbo].[DraftProductMaster] as pm1 WHERE AutoId=@AutoId  
			                                              
					SELECT um.AutoId as UnitType,ISNULL(Qty,1) AS Qty,isnull(MinPrice,0.00)as MinPrice,isnull(CostPrice,0.00) as CostPrice,                                           
					isnull(Price,0)as Price,isnull(WHminPrice,0) as WHminPrice,                                        
					case when ISNULL(PM.PackingAutoId,0)=PD.UnitType then ISNULL(PM.PackingAutoId,0) else 0 end as DefaultPack,                                                                                          
					0 as OrderAttachment,                                                   
					UM.UnitType AS UnitName,isnull(PD.EligibleforFree,0) as Free,isnull(PD.AutoId,0) as PackingId,LatestBarcode
					,Rack,Section,Row,BoxNo
					FROM [psmnj.a1whm.com].[dbo].UnitMaster AS UM                                                                                                                                                       
					LEFT JOIN [psmnj.a1whm.com].[dbo].DraftPackingDetails AS PD ON UM.AutoId=PD.UnitType                                          
					AND PD.ProductAutoId=@AutoId                                      
					LEFT JOIN [psmnj.a1whm.com].[dbo].DraftProductMaster as pm on pm.AutoId=pd.ProductAutoId  order by UM.AutoId DESC   
					SELECT AutoId,VendorName FROM [psmnj.a1whm.com].[dbo].VENDORMASTER WHERE Status=1                                                                                               
					SELECT AutoId,BrandName FROM [psmnj.a1whm.com].[dbo].BrandMaster WHERE Status=1                                               
					SELECT AutoId,CommissionCode,C_DisplayName from [psmnj.a1whm.com].[dbo].Tbl_CommissionMaster where Status=1 
					select * from [psmnj.a1whm.com].[dbo].DraftProductLocation where  ProductAutoId=@AutoId
				END
				ELSE
				BEGIN
					SELECT [AutoId], [SubcategoryName] FROM [dbo].[SubCategoryMaster] WHERE [Status]=1 AND [CategoryAutoId]=@CategoryAutoId 
					order by  [SubcategoryName]
					SELECT [AutoId], [CategoryName] FROM [dbo].[CategoryMaster] WHERE [Status]=1   
					order by  [CategoryName]                                                             
					SELECT * FROM [dbo].[UnitMaster]     
					SELECT [AutoId],[ProductId],[ProductName],[ProductStatus],[ProductLocation],'http://psmnj.a1whm.com' as imgURL,[ImageUrl],[CategoryAutoId],
					[SubcategoryAutoId],[Stock],ReOrderMark,MLQty,ProductStatus as Status,
					BrandAutoId,cast(isnull(pm1.P_CommCode,0) as decimal(10,4))  as  CommCode,WeightOz,isnull(IsApply_ML,0)                                                         
					as IsApply_ML,isnull(IsApply_Oz,0) as IsApply_Oz,    ISNULL(P_SRP,0) as SRP                                                                                                                  
					FROM [dbo].[DraftProductMaster] as pm1 WHERE AutoId=1081  
			                                              
					SELECT um.AutoId as UnitType,ISNULL(Qty,1) AS Qty,isnull(MinPrice,0.00)as MinPrice,isnull(CostPrice,0.00) as CostPrice,                                           
					isnull(Price,0)as Price,isnull(WHminPrice,0) as WHminPrice,                                        
					case when ISNULL(PM.PackingAutoId,0)=PD.UnitType then ISNULL(PM.PackingAutoId,0) else 0 end as DefaultPack,                                                                                          
					0 as OrderAttachment,   Rack,Section,Row,BoxNo,                                                   
					UM.UnitType AS UnitName,isnull(PD.EligibleforFree,0) as Free,isnull(PD.AutoId,0) as PackingId,LatestBarcode FROM [dbo].UnitMaster AS UM                                                                                                                                                       
					LEFT JOIN [dbo].DraftPackingDetails AS PD ON UM.AutoId=PD.UnitType                                         
					AND PD.ProductAutoId=@AutoId                                      
					LEFT JOIN [dbo].DraftProductMaster as pm on pm.AutoId=pd.ProductAutoId  order by UM.AutoId DESC 
					SELECT AutoId,VendorName FROM [dbo].VENDORMASTER WHERE Status=1                                                                                               
					SELECT AutoId,BrandName FROM [dbo].BrandMaster WHERE Status=1                                               
					SELECT AutoId,CommissionCode,C_DisplayName from [dbo].Tbl_CommissionMaster where Status=1 
					select * from DraftProductLocation where  ProductAutoId=@AutoId
				END                                                                                                                                         
			END                                                 
                                                                                                                                               
	ELSE IF @Opcode=49                             
			BEGIN                               
			SET NOCOUNT ON;                                                               
				SELECT ROW_NUMBER() OVER(ORDER BY ProductName asc) AS RowNumber, * INTO #Results49 FROM                              
				(                                                                                                                                                                                
				SELECT PM.[AutoId],[ProductId],[ProductName],[ProductLocation],CM.CategoryName AS Category,SCM.SubcategoryName                                                          
				AS Subcategory,PD.LatestBarcode as barcode,um.UnitType as Unit                                                                      
				FROM DraftProductMaster AS PM                                                                                                                                     
				INNER JOIN [dbo].[CategoryMaster] AS CM ON PM.CategoryAutoId = CM.AutoId                                                           
				INNER JOIN [dbo].[SubCategoryMaster] AS SCM ON PM.SubcategoryAutoId = SCM.AutoId            
				INNER JOIN DraftPackingDetails as PD on PD.ProductAutoId=PM.AutoId           
				INNER JOIN UnitMaster AS UM on UM.AutoId=PD.UnitType                                                                                          
				WHERE (@CategoryAutoId IS NULL OR @CategoryAutoId='' OR PM.[CategoryAutoId] = @CategoryAutoId)       
				AND (@SubcategoryAutoId IS NULL OR @SubcategoryAutoId='' OR PM.[SubcategoryAutoId] = @SubcategoryAutoId)                                  
				AND (@ProductId IS NULL OR @ProductId='' OR PM.[ProductId] LIKE '%'+ @ProductId +'%')                                                                                                                     
				AND (@ProductName IS NULL OR @ProductName='' OR  replace(replace(PM.ProductName,'  ',' '),'  ',' ') LIKE '%'+ @ProductName+'%')          
				AND ISNULL(PD.LatestBarcode,'')!=''          
				) AS t order by ProductName asc                                                                                                                                                     
                                                                             
				SELECT COUNT([ProductId]) AS RecordCount, case when @PageSize=0 then COUNT([ProductId]) else @PageSize end AS PageSize,                                                              
				@PageIndex AS PageIndex FROM #Results49                                                               
				SELECT * FROM #Results49                                                                                                                                                                                
				WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))          
				select * from #Results49                                                                                                                        
			END      
	ELSE IF @Opcode=50                             
			BEGIN

				if(DB_Name() in ('psm.a1whm.com','demo.a1whm.com'))   
				BEGIN 
					select LM.AutoId,Location,isnull(DLM.Status,0) as Status from [dbo].locationmaster as LM
					left join .[dbo].DraftProductLocation as  DLM on LM.AutoId=DLM.LocationId and  DLM.ProductAutoId=isnull(@AutoId,0)	
					order by L_sequanceNO
					for json path
				END
				ELSE
				BEGIN
					select LM.AutoId,Location,isnull(DLM.Status,0) as Status from [psmnj.a1whm.com].[dbo].locationmaster as LM
					left join [psmnj.a1whm.com].[dbo].DraftProductLocation as  DLM on LM.AutoId=DLM.LocationId and  DLM.ProductAutoId=isnull(@AutoId,0)	
					order by L_sequanceNO
					for json path
				END
			END
    ELSE IF @OpCode = 31   
		begin
			select ProductId as Prdtid,ProductName,(SELECT top 1 ImageUrl FROM ProductImageUrl)+dpm.ImageUrl as ImageUrl,FirstName+' '+LastName 
			as EmpName,Format(GETDATE(),'MM/dd/yy hh:mm:ss') as DeleteDate,
			('A1WHM - '+' '+UPPER(ProductLocation)) as Location into #tmp1 from DraftProductMaster as dpm
			inner join EmployeeMaster as em on em.AutoId=@EmpAutoId where ProductId=@ProductId
			
				if(DB_Name() in ('psm.a1whm.com','demo.a1whm.com'))   
				BEGIN  
					 DELETE FROM [dbo].[DraftPackingDetails] WHERE ProductAutoId=(SELECT AutoId FROM DraftProductMaster WHERE ProductId=@ProductId)
					 DELETE FROM [dbo].[DraftProductMaster] WHERE ProductId=@ProductId
				END
				else
				BEGIN  
					 DELETE FROM [psmnj.a1whm.com].[dbo].[DraftPackingDetails] WHERE ProductAutoId=(SELECT AutoId FROM DraftProductMaster WHERE ProductId=@ProductId)
					 DELETE FROM [psmnj.a1whm.com].[dbo].[DraftProductMaster] WHERE ProductId=@ProductId
				END
				select * from #tmp1
			select EmailID as Email from [dbo].[EmailReceiverMaster] where Category='Developer'
		END                                                                                                                 
			END TRY                                                    
			BEGIN CATCH                                                                                                               
			SET @isException=1                                                                                                                                                
			SET @exceptionMessage=Error_Message()--'Oops, Something went wrong .Please try again.'                                                                                             
			END CATCH                                                                                                                                                                         
END 
			GO
