USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedTableType [dbo].[DT_BillItemList]    Script Date: 11/20/2019 5:05:52 AM ******/
CREATE TYPE [dbo].[DT_BillItemList] AS TABLE(
	[ProductAutoId] [int] NULL,
	[UnitAutoId] [int] NULL,
	[QtyPerUnit] [int] NULL,
	[Quantity] [int] NULL,
	[TotalPieces] [int] NULL,
	[Price] [decimal](18, 2) NULL
)
GO
