Alter table PackingDetails drop column LatestBarcode
Alter table PackingDetails add
[LatestBarcode]  AS (ISNULL([dbo].[FN_LatestBarCode]([ProductAutoId],[UnitType]),''))

Create FUNCTION  [dbo].[FN_LatestBarCode]--Drop function FN_LatestBarCode
(
	 @productAutoid int,
	 @UnitAutoId int
)
RETURNS varchar(50)
AS
BEGIN
	declare @BarCode varchar(50)=''
	if exists(select * from OrderItemMaster as OIM
	INNER JOIN ItemBarcode as IM ON OIM.Barcode=IM.Barcode
	where OIM.productAutoid=@productAutoid 
	and UnitTypeAutoId=@UnitAutoId and ISNULL(OIM.Barcode,'')!='')
	begin
		SET @BarCode=ISNULL((select top 1 Barcode from OrderItemMaster oim 
		inner join OrderMaster as om on om.Autoid=oim.OrderAutoid where
		productAutoid=@productAutoid and UnitTypeAutoId=@UnitAutoId and ISNULL(barcode,'')!=''
		order by OrderDate desc),'')
	end
	ELSE
	BEGIN
		SET @BarCode=ISNULL((select top 1 Barcode from ItemBarcode 
		where productAutoid=@productAutoid and UnitAutoId=@UnitAutoId
		order by CreatedDate desc,AutoId desc),'')
	END
	RETURN @BarCode
END

