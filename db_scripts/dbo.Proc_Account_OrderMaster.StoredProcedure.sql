ALTER   PROCEDURE [dbo].[Proc_Account_OrderMaster]           
@Opcode INT=NULL,     
@IPAddress varchar(100)=null,
@OrderAutoId INT=NULL,                       
@CustomerAutoId  int=null,                      
@OrderNo VARCHAR(15)=NULL,                       
@OrderDate DATETIME=NULL,                       
@IsTaxable int =NULL,                       
@ProductAutoId INT=NULL,                      
@UnitAutoId INT=NULL,                       
@EmpAutoId INT=NULL,                       
@LoginEmpType INT =NULL,                      
@ReqQty  INT=NULL,                      
@QtyPerUnit INT=NULL,                      
@UnitPrice DECIMAL(8,2)=NULL,                         
@Remarks VARCHAR(max)=NULL,  
@MLTaxRemark VARCHAR(max)=NULL,  
@Todate DATETIME=NULL,                      
@CheckSecurity  VARCHAR(max)=NULL,                        
@Fromdate DATETIME=NULL,                        
@CancelRemark varchar(150)=null,            
@CreditMemoAmount decimal(10,2)=null,             
@DelItems DT_Internal_Delivered_Order_Items readonly,              
@AmtValue DECIMAL(8,2)=NULL,            
@PaymentRecev VARCHAR(20)=NULL,                                                                                              
@OverallDiscAmt DECIMAL(8,2)=NULL,                                                                                                                           
@ShippingCharges DECIMAL(8,2)=NULL,     
@db_name varchar(max)=NULL,
@PayableAmount decimal(18,2)=null,             
@PaymentAutoId INT=NULL,            
@PriceLevelAutoId INT=NULL,            
@ValueChanged VARCHAR(20)=NULL,                                                                                                                                    
@ManagerRemarks VARCHAR(20)=NULL,                                                                                                                 
@DiffAmt DECIMAL(18,2)=NULL,                                                                                                                                                  
@NewTotal DECIMAL(18,2)=NULL,                      
@PageIndex INT = 1,                      
@PageSize INT = 10,                      
@RecordCount INT =null, 

@Barcode varchar(50)=NULL, 
@IsExchange int=NULL,                                                                                                                                                        
@IsFreeItem  INT=NULL,  
@MLQty DECIMAL(8,2)=NULL,
@minprice  DECIMAL(8,2)=NULL,           
@SRP DECIMAL(8,2)=NULL,                                                                                      
@GP DECIMAL(8,2)=NULL, 

@isException bit out,                      
@exceptionMessage varchar(max) out                      
AS                      
BEGIN     
  BEGIN TRY                      
   Set @isException=0                      
   Set @exceptionMessage='Success'                         
   DECLARE @GrandTotal DECIMAL(10,2)
   
   IF @Opcode=41                      
      BEGIN                         
		  IF EXISTS(SELECT [AutoId] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId AND ([PaymentRecev] IS NOT NULL OR [PaymentRecev] != ''))                      
		   BEGIN                      
			SELECT OM.[AutoId],OM.[OrderNo],CONVERT(VARCHAR(20), OM.[OrderDate], 101) AS OrderDate,                      
			ISNULL(CONVERT(VARCHAR(20), OM.[DeliveryDate],101),CONVERT(VARCHAR(20), OM.[DelDate],101) + ' ' +convert(varchar(10), OM.[DelDate], 108)) AS DeliveryDate,                      
			CM.[AutoId] As CustAutoId,CM.[CustomerId],CM.[CustomerName],CM.[Contact1] + '/' + CM.[Contact2] As Contact,CTy.CustomerType,CT.[TermsDesc],ST.[ShippingType],                      
			[dbo].[ConvertFirstLetterinCapital](BA.[Address]+(case when isnull(BA.Address2,'')!='' then ', '+BA.Address2 else '' end)+', '+BA.City)+', '+UPper(S.StateCode)+' - '+BA.Zipcode As BillAddr,
            [dbo].[ConvertFirstLetterinCapital](SA.[Address]+(case when isnull(SA.Address2,'')!='' then ', '+SA.Address2 else '' end)+', '+SA.City)+', '+S1.StateCode+' - '+SA.Zipcode As ShipAddr,                                      
            OM.[Status] As StatusCode,SM.[StatusType] AS Status,OM.[PackedBoxes],DO.[TotalAmount],DO.[OverallDisc],DO.[OverallDiscAmt],                      
			DO.[ShippingCharges],DO.[TotalTax],DO.[GrandTotal],DO.[AmtPaid],DO.[AmtDue],OM.[PaymentRecev],OM.[RecevAmt],OM.[AmtValue],OM.[ValueChanged],                      
			OM.[DiffAmt],OM.[NewTotal],OM.[PayThru],OM.CheckNo,OM.[DrvRemarks],OM.[AcctRemarks],EM.[FirstName] + ' ' + EM.[LastName] As SalesPerson,                      
			EM1.[FirstName] + ' ' + EM1.[LastName] As PackerName,[CommentType],[Comment],isnull(DeductionAmount,0.00) as DeductionAmount,                      
			ISNULL(DO.CreditAmount,0.00)as CreditAmount,isnull(do.PayableAmount,0.00) as PayableAmount,OM.isMLManualyApply,dbo.UDF_Delivered_CheckMLTax(OM.AutoId) as Delivered_CheckMLTax,                    
			ISNULL((select t.CreditAmount from CustomerCreditMaster as t  where t.CustomerAutoId=om.CustomerAutoid),0.00) as CustomerCredit                 
			,om.TaxType,otm.OrderType AS OrderType,OrderRemarks,ST.EnabledTax,                  
			ISNULL(OM.MLQty,0) as MLQty,isnull(DO.MLTax,0) as MLTax,CM.ContactPersonName,ISNULL(om.AdjustmentAmt,0) as AdjustmentAmt,                      
			CM.BusinessName,ISNULL(UnitMLTax,0) as UnitMLTax,ISNULL(DO.AmtPaid,0) as AmtPaid,ISNULL(do.AmtDue,0) as AmtDue,                      
			(select TaxableType from TaxTypeMaster as ttm where ttm.Autoid=om.taxType) as TaxType,            
		  isnull(OM.Weigth_OZQty,0) as WeightOzQty,isnull(OM.Weigth_OZTaxAmount,0) as WeightOzTotalTax                         
			FROM [dbo].[OrderMaster] As OM                      
			INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.Status AND SM.[Category] = 'OrderMaster'                      
			INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[AutoId] = OM.[BillAddrAutoId]                      
			INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = OM.[ShipAddrAutoId]                      
			INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                       
			INNER JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                       
			INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                 
			INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]                      
			LEFT JOIN [dbo].[CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                      
			LEFT JOIN [dbo].[ShippingType] AS ST ON ST.[AutoId] = OM.[ShippingType]                       
			LEFT JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[SalesPersonAutoId]                      
			LEFT JOIN [dbo].[EmployeeMaster] AS EM1 ON EM1.[AutoId] = OM.[PackerAutoId]                       
			INNER JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId] 
			 INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
			WHERE OM.AutoId = @OrderAutoId                      
                      
			SELECT CM.[CategoryName],CM.[CategoryId],DOI.[ProductAutoId],PM.[ProductId],PM.[ProductName],DOI.[UnitAutoId],UM.[UnitType],DOI.[QtyPerUnit],                      
			DOI.[UnitPrice],DOI.[Barcode],DOI.[QtyShip],DOI.[QtyDel],DOI.[TotalPieces],DOI.[SRP],DOI.[GP],DOI.[Tax],DOI.[NetPrice]                      
			,convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice,
			ISNULL(FreshReturnQty,0) as FreshReturnQty,FreshReturnUnitAutoId,                      
			ISNULL(DamageReturnQty,0) as DamageReturnQty,MissingItemUnitAutoId,
			ISNULL(MissingItemQty,0) as MissingItemQty,FreshReturnUnitAutoId,
			IsExchange,isnull(TaxValue,0.00) as TaxValue,ISNULL(isFreeItem,0) AS isFreeItem,                      
			ISNULL(UnitMLQty,0) as UnitMLQty,ISNULL(TotalMLQty,0) as TotalMLQty,                      
			DOI.Item_TotalMLTax as TotalMLTax,isnull(Del_discount,0) as Discount,isnull(Del_ItemTotal,0) as ItemTotal                      
			FROM [dbo].[Delivered_Order_Items] AS DOI                       
			INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = DOI.[ProductAutoId]                      
			INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = DOI.UnitAutoId                       
			INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId] WHERE [OrderAutoId]=@OrderAutoId                      
			ORDER BY CM.[CategoryName],PM.[ProductId],PM.[ProductName] ASC                 
			SET @CustomerAutoId = (SELECT [CustomerAutoId] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId)                      
                      
			SELECT OM.[AutoId],[OrderNo],CONVERT(VARCHAR(20),[OrderDate],101) AS OrderDate,DO.[AmtDue],DO.[GrandTotal],DO.[AmtPaid] ,                      
		   otm.OrderType AS OrderType ,DATEDIFF(dd,OrderDate,GETDATE()) as Aging, orderdate as orderdateSort                      
			FROM [dbo].[OrderMaster] As OM                      
			INNER JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId]   
			 INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
			WHERE [CustomerAutoId] = @CustomerAutoId AND ([Status] = 6 or [Status] = 11) AND DO.[AmtDue] > 0.00                      
			order by orderdateSort                       
                             
                            
		   END               
		  ELSE                      
		   BEGIN                      
			SELECT OM.[AutoId],OM.[OrderNo],CONVERT(VARCHAR(20),OM.[OrderDate], 101) AS OrderDate,                      
			CONVERT(VARCHAR(20), OM.[DeliveryDate],101) + ' ' + convert(varchar(10), getdate(), 108)--CONVERT(VARCHAR(20),OM.[DeliveryDate], 131)                       
			AS DeliveryDate,                      
			CM.[AutoId] As CustAutoId,CM.[CustomerId],CM.[CustomerName],CM.[Contact1] + '/' + CM.[Contact2] As Contact,CTy.CustomerType,CT.[TermsDesc],ST.[ShippingType],                      
			BA.[Address] As BillAddr,S.[StateName] AS State1,BA.[City] AS City1,BA.[Zipcode] As Zipcode1,SA.[Address] As ShipAddr,S1.[StateName] As State2,                      
			SA.[City] AS City2,SA.[Zipcode] As Zipcode2,OM.[Status] As StatusCode,SM.[StatusType] AS Status,OM.[TotalAmount],OM.[OverallDisc],OM.[OverallDiscAmt],                      
			OM.[ShippingCharges],OM.[TotalTax],OM.[GrandTotal],OM.[PackedBoxes],EM.[FirstName] + ' ' + EM.[LastName] As SalesPerson,                      
			EM1.[FirstName] + ' ' + EM1.[LastName] As PackerName,OM.[DrvRemarks],[CommentType],[Comment]                      
			,isnull(DeductionAmount,0.00) as DeductionAmount, OM.isMLManualyApply,                      
			isnull(CreditAmount,0.00)as CreditAmount,isnull(PayableAmount,0.00) as PayableAmount                      
			,ISNULL((select t.CreditAmount from CustomerCreditMaster as t  where t.CustomerAutoId=om.CustomerAutoid),0.00) as CustomerCredit                      
			,om.TaxType,otm.OrderType AS OrderType,OrderRemarks ,ST.EnabledTax,                      
			ISNULL(MLQty,0) as MLQty,isnull(MLTax,0) as MLTax,CM.ContactPersonName,ISNULL(AdjustmentAmt,0) as AdjustmentAmt,CM.BusinessName,dbo.UDF_Delivered_CheckMLTax(OM.AutoId) as Delivered_CheckMLTax,         
		 isnull(OM.Weigth_OZQty,0) as WeightOzQty,isnull(OM.Weigth_OZTaxAmount,0) as WeightOzTotalTax FROM [dbo].[OrderMaster] As OM                      
			INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.Status AND SM.[Category] = 'OrderMaster'                      
			INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[AutoId] = OM.[BillAddrAutoId]                      
			INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = OM.[ShipAddrAutoId]                      
			INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                       
			INNER JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                       
			INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                       
			INNER JOIN [dbo].[CustomerType] AS CTy ON CTy.[AutoId] = CM.[CustomerType]                      
			LEFT JOIN [dbo].[CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                      
			LEFT JOIN [dbo].[ShippingType] AS ST ON ST.[AutoId] = OM.[ShippingType]                       
			left JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[SalesPersonAutoId]                             
			left JOIN [dbo].[EmployeeMaster] AS EM1 ON EM1.[AutoId] = OM.[PackerAutoId] 
			 INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
			WHERE OM.AutoId = @OrderAutoId                      
                      
			SELECT CM.[CategoryName],CM.[CategoryId],OIM.ProductAutoId,PM.[ProductId],PM.[ProductName],PM.[ProductLocation],                      
			OIM.[UnitTypeAutoId] AS UnitAutoId,UM.[UnitType],OIM.[QtyPerUnit],OIM.[UnitPrice],OIM.[TotalPieces],OIM.[SRP],OIM.[GP],OIM.[Tax],                      
			OIM.[NetPrice],OIM.[Barcode],OIM.[QtyShip],OIM.[RequiredQty] ,convert(decimal(10,2),isnull(UnitPrice,0.00)/isnull(QtyPerUnit,0.00)) as PerpiecePrice,                      
			OIM.IsExchange,isnull(TaxValue,0.00) as TaxValue,ISNULL(isFreeItem,0) AS isFreeItem,UnitMLQty,                      
			ISNULL(UnitMLQty,0) as UnitMLQty,ISNULL(TotalMLQty,0) as TotalMLQty,                      
			oim.Item_TotalMLTax as TotalMLTax ,isnull(Oim_Discount,0) as Discount,isnull(Oim_ItemTotal,0) as ItemTotal                                          
			FROM [dbo].[OrderItemMaster] AS OIM                         
			INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                      
			INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitTypeAutoId                       
			INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId] WHERE [OrderAutoId] = @OrderAutoId                       
			ORDER BY CM.[CategoryName],PM.[ProductId],PM.[ProductName] ASC                      
                      
			SET @CustomerAutoId = (SELECT [CustomerAutoId] FROM [dbo].[OrderMaster] WHERE [AutoId]=@OrderAutoId)                      
             
			SELECT OM.[AutoId],[OrderNo],CONVERT(VARCHAR(20),[OrderDate],101) AS OrderDate,DO.[AmtDue],DO.[GrandTotal],DO.[AmtPaid] ,                      
			otm.OrderType AS OrderType ,DATEDIFF(dd,OrderDate,GETDATE()) as Aging, orderdate as orderdateSort                      
			FROM [dbo].[OrderMaster] As OM                      
			INNER JOIN [dbo].[DeliveredOrders] As DO ON DO.[OrderAutoId] = OM.[AutoId]   
			 INNER join [dbo].[OrderTypeMaster] as otm on otm.AutoId=om.OrderType and Type='Order'
			WHERE [CustomerAutoId] = @CustomerAutoId AND ([Status] = 6 or [Status] = 11 ) AND do.[AmtDue] > 0.00                      
			order by orderdateSort                       
                              
		   END                           
                          
		  select OrderAutoId,CreditAutoId,CreditNo,CONVERT(VARCHAR(10),CreditDate,101) AS CreditDate,ISNULL(TotalAmount,0.00) as ReturnValue,ISNULL(PaidAmount,0.00)  as amtDeducted,                      
		  ISNULL(BalanceAmount,0.00) as amtDue                      
			 from CreditMemoMaster AS CM  where Status =3               
			 AND OrderAutoId=@OrderAutoId                       
                      
		  SELECT * FROM (                          
         SELECT 'Driver' as EmpType,DrvRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om                       
         inner join EmployeeMaster as emp on emp.AutoId=om.Driver                       
         where om.AutoId=@OrderAutoId and  DrvRemarks!=''                       
    UNION                      
         SELECT 'Account' as EmpType,AcctRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om                      
         inner join EmployeeMaster as emp on emp.AutoId=om.AccountAutoId where om.AutoId=@OrderAutoId                      
          and AcctRemarks is not null  and  AcctRemarks!=''                       
    UNION                      
         SELECT 'Sales Person' as EmpType,OrderRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName  from OrderMaster as om                      
         inner join EmployeeMaster as emp on emp.AutoId=om.SalesPersonAutoId where                       
         om.AutoId=@OrderAutoId   and  OrderRemarks is not null  and  OrderRemarks!=''                       
    UNION                      
         SELECT 'Packer' as EmpType,PackerRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName from OrderMaster as om                      
         inner join EmployeeMaster as emp on emp.AutoId=om.PackerAutoId                      
          where om.AutoId=@OrderAutoId AND    PackerRemarks is not null  and  PackerRemarks!=''                       
    UNION                      
         SELECT 'Manager' as EmpType,ManagerRemarks as Remarks,emp.FirstName+SPACE(1)+emp.LastName  as EmpName  from OrderMaster as om                      
         inner join EmployeeMaster as emp on emp.AutoId=om.ManagerAutoId                       
         where om.AutoId=@OrderAutoId AND    ManagerRemarks is not null  and  ManagerRemarks!=''                       
     ) AS T                      
              select AutoId,OrderAutoId,fileType,fileName,filepath,PhysicalPath,Datetime from tbl_DriverDeliveryDocuments where OrderAutoId=@OrderAutoId 
      END                       
      ELSE IF @Opcode=42                      
      BEGIN                      
		SELECT [SecurityValue] FROM [dbo].[TBL_MST_SecurityMaster] WHERE [SecurityValue]=@CheckSecurity and typeEvent=2                      
		and SecurityType=@LoginEmpType                      
      END                      
   ELSE IF @Opcode=43                      
   BEGIN 
   
		IF EXISTS(SELECT [SecurityValue] FROM [dbo].[TBL_MST_SecurityMaster] WHERE [SecurityValue]=@CheckSecurity                       
	  and SecurityType=@LoginEmpType and typeEvent=1)                      
		BEGIN                      
	   Set @isException=0                      
	   Set @exceptionMessage='Successs'         
                      
		END                      
		ELSE                      
		BEGIN                         
	  Set @isException=1                      
	  Set @exceptionMessage='Invalid Security Key.'                      
		END                      
   END                      
                    
   Else If @Opcode=21                    
   BEGIN                    
  IF NOT EXISTS(SELECT od.OrderAutoId from PaymentOrderDetails as od inner join CustomerPaymentDetails as cpd on cpd.PaymentAutoId=od.PaymentAutoId and cpd.Status=0 WHERE OrderAutoId=@OrderAutoId)              
  BEGIN              
              
   DECLARE @CreditAmount DECIMAL(10,2)=ISNULL((SELECT CreditAmount FROM OrderMaster WHERE AutoId=@OrderAutoId),0)                      
   SET @CustomerAutoId=(SELECT CustomerAutoId FROM OrderMaster WHERE AutoId=@OrderAutoId) 
                        
   IF(ISNULL((SELECT CreditAmount FROM OrderMaster WHERE AutoId=@OrderAutoId),0)!=0)                      
   BEGIN                     
    UPDATE CustomerCreditMaster SET CreditAmount=ISNULL(CreditAmount,0)+@CreditAmount WHERE CustomerAutoId=@CustomerAutoId                      
   END                       
   SET @GrandTotal=ISNULL((SELECT GrandTotal FROM OrderMaster WHERE AutoId=@OrderAutoId),0)                     
              
                      
   DELETE from tbl_Custumor_StoreCreditLog where ReferenceNo=CONVERT(varchar(50),@OrderAutoId) and ReferenceType='OrderMaster'
                         
   UPDATE CreditMemoMaster set OrderAutoId=null where OrderAutoId=@OrderAutoId

    if ((Select Status from OrderMaster where AutoId=@OrderAutoId)=11)
	Begin
		insert into [dbo].[ProductStockUpdateLog]([ProductId],[OledStock],[NewStock],[UserAutoId],[DateTime],ReferenceId,ActionRemark,ReferenceType)
		SELECT pm.ProductId,isnull([Stock],0),(isnull(pm.Stock,0)+(isnull(dt.QtyDel,0))),@EmpAutoId,GETDATE(),dt.OrderAutoId,
		'Order No-(' + om.OrderNo + ') has cancelled by '+(Select FirstName+' '+LastName from Employeemaster where autoid=@EmpAutoId)+'.','OrderMaster' 
		FROM Delivered_Order_Items as dt
		inner join productmaster pm on dt.[ProductAutoId] = PM.[AutoId]
		inner join OrderMaster om on dt.OrderAutoId=om.AutoId
		inner join DeliveredOrders dom on dt.OrderAutoId=dom.OrderAutoId
		where dt.OrderAutoId=@OrderAutoId and isnull(dt.QtyShip,0)>0 

		UPDATE PM SET [Stock] = isnull([Stock],0) + (isnull(dt.QtyDel,0)) FROM [dbo].[ProductMaster] AS PM                                                                                                   
		INNER JOIN (SELECT QtyDel,ProductAutoId FROM Delivered_Order_Items where OrderAutoId=@OrderAutoId) AS dt 
		ON dt.[ProductAutoId] = PM.[AutoId]
	 

	End
	Else
	Begin
	
		insert into [dbo].[ProductStockUpdateLog]([ProductId],[OledStock],[NewStock],[UserAutoId],[DateTime],ReferenceId,ActionRemark,ReferenceType)
		SELECT pm.ProductId,isnull([Stock],0),(isnull(pm.Stock,0)+(isnull(dt.QtyPerUnit,0) * isnull(dt.QtyShip,0))),@EmpAutoId,GETDATE(),dt.OrderAutoId,
		'Order No-(' + om.OrderNo + ') has cancelled by '+(Select FirstName+' '+LastName from Employeemaster where autoid=@EmpAutoId)+'.','OrderMaster' 
		FROM OrderItemMaster as dt
		inner join productmaster pm on dt.[ProductAutoId] = PM.[AutoId]
		inner join OrderMaster om on dt.OrderAutoId=om.AutoId
		where dt.OrderAutoId=@OrderAutoId and isnull(dt.QtyShip,0)>0 

		UPDATE PM SET [Stock] = isnull([Stock],0) + (isnull(dt.QtyPerUnit,0)*isnull(dt.QtyShip,0)) FROM [dbo].[ProductMaster] AS PM                                                                                                   
		INNER JOIN (SELECT QtyShip,QtyPerUnit,ProductAutoId FROM OrderItemMaster where OrderAutoId=@OrderAutoId) AS dt 
		ON dt.[ProductAutoId] = PM.[AutoId] 

	End
	
   
                         
   declare @OrderStatus int=( select Status from OrderMaster   where AutoId=@OrderAutoId )                    
   UPDATE OrderMaster SET Status=8,CancelledDate=GETDATE(),AccountAutoId=@EmpAutoId,CancelledBy=@EmpAutoId,CancelRemark=@CancelRemark                     
   where AutoId=@OrderAutoId                      
                      
   DECLARE  @LogRemark VARCHAR(500)= REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=3),'[Status1]',                    
   (select StatusType from  StatusMaster where AutoId=@OrderStatus and Category='OrderMaster'))                      
   SET @LogRemark = REPLACE(@LogRemark,'[Status2]','Cancelled') 
                        
   INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId],ActionIPAddress)                      
   VALUES(3,@EmpAutoId,getdate(),@LogRemark,@OrderAutoId,@IPAddress)  
   
          
		SELECT @CustomerAutoId=CustomerAutoId FROM [dbo].[OrderMaster]             
		WHERE [AutoId] = @OrderAutoId            
		if((select CustomerType from CustomerMaster where AutoId=@CustomerAutoId)=3)                  
		begin                  
		exec [dbo].[ProcUpdatePOStatus_All]                  
		@CustomerId=@CustomerAutoId,                  
		@OrderAutoId=@OrderAutoId,                  
		@Status=8         
		End                             
    END            
             
ELSE              
 BEGIN              
  Set @isException=1                      
     Set @exceptionMessage='Already, Payment has been received so you can not cancel this order.'               
 END               
 END            
  else if(@Opcode=53)             
  begin            
	  SELECT pm.[AutoId],[ProductName] as [PName],MLQty,isnull((WeightOz),0) as WOz,pm.ProductId  as PID                                                                          
	  FROM [dbo].[ProductMaster] as pm             
	  where pm.ProductStatus=1                                                  
	  and (select count(1) from PackingDetails where packingstatus=1 and ProductAutoId=pm.AutoId)>0                                          
	  and ISNULL(pm.PackingAutoId,0)!=0                                                
	  order by pm.[ProductId] for json path           
  end            
              
  else if(@Opcode=54)             
  begin            
    SELECT pm.ProductName,UM.AutoId AS AutoId, UM.[UnitType] AS UnitType, [Qty],(Stock /(CASE WHEN pd.Qty=0 then 1 else PD.Qty end) )as AvailableQty,                                                                                
    Price,EligibleforFree,pm.ProductId,PD.LatestBarcode  FROM [dbo].[PackingDetails] AS PD                                                                                
    INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = PD.UnitType                                                                                
    INNER JOIN ProductMaster as pm on pm.AutoId =PD.[ProductAutoId] WHERE pm.[AutoId] = @ProductAutoId                                                                                                           
    SELECT PackingAutoId AS AutoId FROM ProductMaster  WHERE  AutoId=@ProductAutoId	          
  end            
else if(@Opcode=55)            
  begin        
		Select t.MinPrice,t.Price,(case when t.CustomPrice <= t.CostPrice then t.CostPrice else t.CostPrice end) as CostPrice,t.GP,t.CustomPrice,      
		t.Stock,t.TaxRate,t.SRP,t.MLQty,t.WeightOz      
		from (SELECT ISNULL(pd.[CostPrice],0) as MinPrice ,pd.[Price] as Price,ISNULL(CostPrice,0) as CostPrice,      
		CONVERT(DECIMAL(10,2),((PM.[P_SRP]-(pd.[Price]/(CASE WHEN pd.QTY=0 then 1 else pd.[Qty] end)))/(CASE WHEN PM.[P_SRP]=0 then 1 else PM.[P_SRP] end )) * 100) AS GP,                                                                    
		isnull((select Top 1 [CustomPrice] from [dbo].[ProductPricingInPriceLevel] AS PPL where PPL.[ProductAutoId] = PD.[ProductAutoId] AND PPL.[UnitAutoId] = PD.[UnitType]                                                                             
		AND [PriceLevelAutoId] = (SELECT [PriceLevelAutoId] FROM [dbo].[CustomerPriceLevel] WHERE [CustomerAutoId]=@CustomerAutoId)                                                                   
		Order by [CustomPrice] desc),0) as [CustomPrice],PM.[Stock]/(CASE WHEN pd.Qty=0 then 1 else pd.Qty end)  as Stock,                
		PM.[TaxRate] as TaxRate,PM.[P_SRP] as [SRP],isnull(PM.MLQty,0.0)as MLQty,isnull(PM.WeightOz,0.0)as WeightOz FROM [dbo].[PackingDetails] As PD           
		INNER JOIN [dbo].[ProductMaster] AS PM ON PD.[ProductAutoId]=PM.[AutoId]       
		WHERE PD.[ProductAutoId] =@ProductAutoId  and PD.[UnitType] = @UnitAutoId
		) as t               
  end           
            
  else if(@Opcode=61)            
  begin            
 Select tm.TaxableType,tm.Value,tm.AutoId from TaxTypeMaster as tm            
 inner join ShippingAddress sa on sa.State=tm.State            
 inner join OrderMaster om on om.ShipAddrAutoId=sa.AutoId and om.AutoId=@OrderAutoId            
  end            
  ELSE IF @Opcode=105                                                                                                                     
  BEGIN                                        
		BEGIN TRY                                                                                                                                                  
		BEGIN TRAN 
		IF EXISTS(
			select * from (SELECT tbl.ProductAutoId,SUM(tbl.QtyDel) as SoldQty FROM @DelItems AS TBL
			where tbl.Status=1
			group by ProductAutoId) as t
			INNER JOIN ProductMaster as PM ON PM.AutoId=t.ProductAutoId
			where PM.Stock<t.SoldQty
			Union
			select * from (SELECT tbl.ProductAutoId,SUM(tbl.QtyDel-tbl.AddOnQty) as SoldQty FROM @DelItems AS TBL
			where tbl.Status=2
			group by ProductAutoId) as t
			INNER JOIN ProductMaster as PM ON PM.AutoId=t.ProductAutoId
			where PM.Stock<t.SoldQty
			)
		BEGIN  
			SET @isException=1
			SET @exceptionMessage='Stock not available.'
		END
		ELSE
		Begin
			IF EXISTS(SELECT AutoId FROM EmployeeMaster WHERE AutoId=@EmpAutoId AND EmpType=6)
			BEGIN
			IF EXISTS(SELECT OrderAutoId from [DeliveredOrders] WHERE OrderAutoId=@OrderAutoId)
			BEGIN
				Set @isException=1                                                                                                     
				Set @exceptionMessage='Please contact to support Team.'    
			END
			ELSE
			BEGIN
				SET @CreditAmount=ISNULL((SELECT CreditAmount FROM OrderMaster WHERE AutoId=@OrderAutoId),0)                        
				SET @CreditMemoAmount=ISNULL((SELECT Deductionamount FROM OrderMaster WHERE AutoId=@OrderAutoId),0)                                                                                                               
				SET @CustomerAutoId=(SELECT CustomerAutoId FROM OrderMaster WHERE AutoId=@OrderAutoId)                                                                                                            
                                                                                                        
				IF (@CreditAmount>0)                                                                                           
				BEGIN                               
					update CustomerCreditMaster set CreditAmount=isnull(CreditAmount,0)+Isnull(@CreditAmount,0) where CustomerAutoId= @CustomerAutoId                                                                                      
					DELETE FROM tbl_Custumor_StoreCreditLog WHERE ReferenceType='OrderMaster' AND ReferenceNo=CONVERT(VARCHAR(10),@OrderAutoId)                                                                                 
				END                                                                                                               
				IF (@CreditAmount<0)                                                                                               
				BEGIN                                                                                                       
					update CustomerCreditMaster set CreditAmount=isnull(CreditAmount,0)-Isnull(@CreditAmount,0) where CustomerAutoId= @CustomerAutoId         
					DELETE FROM tbl_Custumor_StoreCreditLog WHERE ReferenceType='OrderMaster' AND ReferenceNo=CONVERT(VARCHAR(10),@OrderAutoId)
				END                                                                    
                                                                                              
				declare @TaxValue1 decimal(10,2)=isnull((select TaxValue from OrderMaster where AutoId=@OrderAutoId),0.00)  
  
				INSERT INTO [dbo].[DeliveredOrders] ([OrderAutoId])                                                                                                                                                  
				VALUES(@OrderAutoId)                                                                                                                   
                                                   
				INSERT INTO [dbo].[Delivered_Order_Items] ([OrderAutoId],[ProductAutoId],[UnitAutoId],[QtyPerUnit],                                                                      
				[UnitPrice],[Barcode],                                                                
				[QtyShip],[SRP],[GP],[Tax],FreshReturnQty,FreshReturnUnitAutoId,DamageReturnQty,DamageReturnUnitAutoId,MissingItemQty,MissingItemUnitAutoId,                     
				IsExchange,TaxValue,isFreeItem,UnitMLQty,QtyPerUnit_Fresh ,QtyPerUnit_Damage,QtyPerUnit_Missing,Del_CommCode,                                                
				Del_Weight_Oz,Del_CostPrice,Del_MinPrice,BasePrice)                                                                                                                                
				SELECT @OrderAutoId,tbl.[ProductAutoId],tbl.[UnitAutoId],tbl.[QtyPerUnit],tbl.[UnitPrice],tbl.[Barcode],                                                                    
				tbl.[QtyShip] ,                                                                                                                                                  
				pm.[P_SRP],tbl.[GP],tbl.[Tax],                                                                                                                           
				FreshReturnQty,FreshReturnUnitAutoId,DamageReturnQty,DamageReturnUnitAutoId,MissingItemQty,MissingItemUnitAutoId,IsExchange,@TaxValue1,                                                                                      
				isFreeItem,isnull(tbl.MLQty,0),                                                                   
				isnull((select top 1 Qty from PackingDetails as pd  where pd.ProductAutoId=tbl.ProductAutoId and pd.UnitType=tbl.FreshReturnUnitAutoId),0),                                                                                                     
				isnull((select top 1 Qty from PackingDetails as pd  where pd.ProductAutoId=tbl.ProductAutoId and pd.UnitType=tbl.DamageReturnUnitAutoId),0), 
				isnull((select top 1 Qty from PackingDetails as pd  where pd.ProductAutoId=tbl.ProductAutoId and pd.UnitType=tbl.MissingItemUnitAutoId),0),
				--isnull((select top 1 CostPrice from PackingDetails as pd  where pd.ProductAutoId=tbl.ProductAutoId and pd.UnitType=tbl.UnitAutoId),0),                                                                                                     
				ISNULL(P_CommCode,0),ISNULL(pm.WeightOz,0),Del_CostPrice,Del_MinPrice,
				isnull((select Price from PackingDetails as pd  where pd.ProductAutoId=tbl.ProductAutoId 
				and pd.UnitType=tbl.UnitAutoId),0)                                                                                                     
			  
				FROM                                                                                                         
				@DelItems As tbl inner join  ProductMaster as pm on pm.AutoId=tbl.ProductAutoId       
      
				--Add For insert into OrderItemMaster add new product by account  
				Update oim                                              
				set QtyShip=t.QtyShip,RequiredQty=(case when t.QtyShip>RequiredQty then RequiredQty+(t.QtyShip-RequiredQty) else RequiredQty end)
				from [OrderItemMaster] as oim                                                                                                                                                    
				inner join (
				select ProductAutoId,QtyShip,isFreeItem,IsExchange,Tax from @DelItems  As tbl where tbl.Status=2  
				)  as t on t.ProductAutoId=oim.ProductAutoId and oim.OrderAutoId=@OrderAutoId 
				and t.isFreeItem=oim.isFreeItem and t.IsExchange=oim.IsExchange and t.Tax=oim.Tax
		
				INSERT INTO [dbo].[OrderItemMaster] ([OrderAutoId],[ProductAutoId],[UnitTypeAutoId],[QtyPerUnit],[UnitPrice],
				[RequiredQty],[Barcode],      
				[SRP],[GP],[Tax],[QtyShip],[RemainQty],IsExchange,TaxValue,isFreeItem,UnitMLQty,Weight_Oz,BasePrice)       
				SELECT @OrderAutoId,tbl.[ProductAutoId],tbl.[UnitAutoId],tbl.[QtyPerUnit],tbl.[UnitPrice],QtyShip,tbl.[Barcode],                                                                    
				pm.[P_SRP],tbl.[GP],tbl.[Tax],tbl.[QtyShip],0,IsExchange,@TaxValue1,isFreeItem,isnull(tbl.MLQty,0),
				ISNULL(pm.WeightOz,0),
				(Select price FROM PackingDetails pd where pd.ProductAutoId=pm.AutoId and PD.UnitType=tbl.UnitAutoId)
				FROM                              
				@DelItems  As tbl       
				inner join ProductMaster as pm on pm.AutoId=tbl.ProductAutoId and tbl.Status=1      
				--End                                                                     
                                                                                                                                      
				UPDATE [dbo].[OrderMaster] SET                                                                                                                                                   
				[PaymentRecev] = @PaymentRecev,                                                                                                                                        
				AccountAutoId=@EmpAutoId,                                                                           
				[OverallDiscAmt]=@OverallDiscAmt,                                                                                                                                          
				[ShippingCharges]=@ShippingCharges,                                                                                                                                    
				status=11,       
				Order_Closed_Date=getdate(),                                                                                                                                    
				[AcctRemarks] = @Remarks                                                                       
				WHERE [AutoId] = @OrderAutoId   
	  
				SET @PayableAmount=ISNULL((SELECT PayableAmount FROM [dbo].[OrderMaster]  WHERE [AutoId] = @OrderAutoId ),0)                                                                                                       
                                                          
				IF(@CreditAmount>0)                                                                                   
				BEGIN                                                                                                            
					IF((@GrandTotal-@CreditMemoAmount)<@CreditAmount)                     
					BEGIN                                                         
						UPDATE CustomerCreditMaster SET CreditAmount=CreditAmount+(@CreditAmount-(@GrandTotal-@CreditMemoAmount)) WHERE CustomerAutoId=@CustomerAutoId                                                                                                            
                                                                                                          
						INSERT INTO tbl_Custumor_StoreCreditLog(CustomerAutoId,CreatedBy,CreatedDate,ReferenceNo,ReferenceType,Amount)                                                                                                                  
						VALUES(@CustomerAutoId,@EmpAutoId,GETDATE(),@OrderAutoId,'OrderMaster', -(@CreditAmount-((@CreditAmount-(@GrandTotal-@CreditMemoAmount)))))                                                                                                            
					END                                                         
					Else                                                                                                            
					BEGIN                                                                                 
						update CustomerCreditMaster set CreditAmount =isnull(CreditAmount,0)-isnull(@CreditAmount,0)                                                         
						where CustomerAutoId=@CustomerAutoId                                                                                                            
						insert into tbl_Custumor_StoreCreditLog(CustomerAutoId,ReferenceType,ReferenceNo,Amount,CreatedDate,CreatedBy)                                                                
						values(@CustomerAutoId,'OrderMaster',@OrderAutoId,@CreditAmount,getdate(),@EmpAutoid)                                                                                                            
					END                                                                       
				END                                                                 
                                                                                                                                  
				IF(@CreditAmount<0)                                                                                                            
				BEGIN                     
					UPDATE CustomerCreditMaster SET CreditAmount=CreditAmount+(@CreditAmount) WHERE CustomerAutoId=@CustomerAutoId                                                                                                            
					INSERT INTO tbl_Custumor_StoreCreditLog(CustomerAutoId,CreatedBy,CreatedDate,ReferenceNo,ReferenceType,Amount)                                                                                                 
					VALUES(@CustomerAutoId,@EmpAutoId,GETDATE(),@OrderAutoId,'OrderMaster', @CreditAmount)                                                                                                             
				END                                                                                                            
  
				SELECT ProductAutoId,Status, 
				(FreshReturnQty * ISNULL((select Qty from PackingDetails where ProductAutoId=TBL.ProductAutoId 
				and UnitType=tbl.FreshReturnUnitAutoId),1))+     
				(MissingItemQty * ISNULL((select Qty from PackingDetails where ProductAutoId=TBL.ProductAutoId 
				and UnitType=tbl.MissingItemUnitAutoId),1)) 
				AS Stock into #Result105
				FROM @DelItems AS TBL

				SELECT ProductAutoId, SUM(QtyDel) AS Stock into #Result106 FROM @DelItems AS TBL where Status=1
				group by ProductAutoId
				
				SELECT tbl.ProductAutoId,SUM(tbl.QtyDel-tbl.AddOnQty) as Stock into #Result107 
				FROM @DelItems AS TBL where tbl.Status=2 
				group by TBL.ProductAutoId

				insert into [dbo].[ProductStockUpdateLog]([ProductId],[OledStock],[NewStock],[UserAutoId],[DateTime],ReferenceId,
				ActionRemark,ReferenceType)
				SELECT pm.ProductId,isnull([Stock],0),isnull([Stock],0) - isnull([Pcs],0),@EmpAutoId,GETDATE(),@OrderAutoId,
				'Order No-(' + (select OrderNO from OrderMaster as  om where om.autoid=@OrderAutoId) + ') has been updated '+(Select 
				FirstName+' '+LastName from Employeemaster where autoid=@EmpAutoId)+'.','OrderMaster' 
				FROM [dbo].[ProductMaster] AS PM                                                                                                   
				INNER JOIN (SELECT [ProductAutoId],ISNULL(Stock,0) as [Pcs] FROM #Result107 as tbl106) AS dt
				ON dt.[ProductAutoId] = PM.[AutoId]
				and ISNULL([Pcs],0)>0
		
				Update pm                                              
				set Stock=isnull(Stock,0)- [Pcs]
				from ProductMaster as pm                                                                                                                                                    
				inner join (SELECT [ProductAutoId],ISNULL(Stock,0) as [Pcs] FROM #Result107 as tbl106
				)  as t on t.ProductAutoId=pm.AutoId and ISNULL([Pcs],0)>0


				insert into [dbo].[ProductStockUpdateLog]([ProductId],[OledStock],[NewStock],[UserAutoId],[DateTime],ReferenceId,
				ActionRemark,ReferenceType)
				SELECT pm.ProductId,isnull([Stock],0),isnull([Stock],0) - isnull([Pcs],0),@EmpAutoId,GETDATE(),@OrderAutoId,
				'Order No-(' + (select OrderNO from OrderMaster as  om where om.autoid=@OrderAutoId) + ') has been updated '+(Select 
				FirstName+' '+LastName from Employeemaster where autoid=@EmpAutoId)+'.','OrderMaster' 
				FROM [dbo].[ProductMaster] AS PM                                                                                                   
				INNER JOIN (SELECT [ProductAutoId],ISNULL(Stock,0) as [Pcs] FROM #Result106 as tbl106) AS dt ON dt.[ProductAutoId] = PM.[AutoId]
				and ISNULL([Pcs],0)>0
		
				Update pm                                              
				set Stock=isnull(Stock,0)- [Pcs]
				from ProductMaster as pm                                                                                                                                                    
				inner join (SELECT [ProductAutoId],ISNULL(Stock,0) as [Pcs] FROM #Result106 as tbl106
				)  as t on t.ProductAutoId=pm.AutoId and ISNULL([Pcs],0)>0

				insert into [dbo].[ProductStockUpdateLog]([ProductId],[OledStock],[NewStock],[UserAutoId],[DateTime],ReferenceId,
				ActionRemark,ReferenceType)
				SELECT pm.ProductId,isnull([Stock],0),isnull([Stock],0) + isnull([Pcs],0),@EmpAutoId,GETDATE(),@OrderAutoId,
				'Order No-(' + (select OrderNO from OrderMaster as  om where om.autoid=@OrderAutoId) + ') has been updated '+(Select 
				FirstName+' '+LastName from Employeemaster where autoid=@EmpAutoId)+'.','OrderMaster' 
				FROM [dbo].[ProductMaster] AS PM                                                                                                   
				INNER JOIN (SELECT [ProductAutoId],SUM(Stock) as [Pcs] FROM #Result105 as tbl105 where tbl105.Status=0 group by
				[ProductAutoId]) AS dt ON dt.[ProductAutoId] = PM.[AutoId] 
				and [Pcs]>0

				Update pm                                              
				set Stock=isnull(Stock,0)+ StockQty
				from ProductMaster as pm                                                                                                                                                    
				inner join (
				select ProductAutoId,SUM(Stock) as StockQty from #Result105 as temp where  temp.Status=0     
				group by ProductAutoId
				)  as t on t.ProductAutoId=pm.AutoId and StockQty>0    
		 
                                                                                                                                        
				SET @PriceLevelAutoId = (SELECT [PriceLevelAutoId] FROM [dbo].[CustomerPriceLevel] WHERE [CustomerAutoId] = @CustomerAutoId)                                                                                                                   
				exec UPDTAE_PRICELEVEL_delivery                                                                                                                                                   
				@OrderAutoId=@OrderAutoId,                             
				@PriceLevelAutoId=@PriceLevelAutoId,
				@EmpAutoId=@EmpAutoId  

				
  				INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                                                              
				select 2,@EmpAutoId,getdate(),CONVERT(varchar(50),pm.productId)+' has been added'+ 
				case when tb.IsExchange=1 then ' as Exchange' when tb.IsFreeItem=1 then ' as Free' else '' end as tp,
				@OrderAutoId from @DelItems as tb                                                                                                            
				inner join ProductMaster as pm on pm.AutoId=tb.ProductAutoId 
				where tb.Status=1 

				INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                                                              
				select 2,@EmpAutoId,getdate(),CONVERT(varchar(50),pm.productId)+' quantity has been updated',@OrderAutoId from @DelItems as tb                                                                                                            
				inner join ProductMaster as pm on pm.AutoId=tb.ProductAutoId 
				where tb.Status=2		
                                                                 
				SET @LogRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=3),'[Status1]','Delivered')                                                                    
				SET @LogRemark = REPLACE(@LogRemark,'[Status2]','Closed')                                                                                                                                                
				INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                                                                                            
				VALUES(3,@EmpAutoId,getdate(),@LogRemark,@OrderAutoId)                 
                
				Update Delivered_Order_Items set StaticDelQty=QtyDel,StaticNetPrice=NetPrice where OrderAutoId=@OrderAutoId

				if((select CustomerType from CustomerMaster where AutoId=@CustomerAutoId)=3)                
				begin
					exec [dbo].[ProcUpdatePOStatus_All]                
					@CustomerId=@CustomerAutoId,                
					@OrderAutoId=@OrderAutoId,                
					@Status=11                    
				end                                                                                                                                                
			END 
		END
		ELSE
		BEGIN                                                                                                   
			Set @isException=1                                                                                                     
			Set @exceptionMessage='Unauthorized Access.' 
		END
		END
		COMMIT TRANSACTION                                                                                              
		END TRY        
		BEGIN CATCH                                                                            
		ROLLBACK TRAN                                                                                                     
			Set @isException=1                                                                                                     
			Set @exceptionMessage='Oops! Something went wrong.Please try later.'                                                                              
		END CATCH 
  END                                                                                                                            
  ELSE IF @Opcode=207                        
  BEGIN                                                                                                                                           
  BEGIN TRY                                                                        
  BEGIN TRAN 
	IF EXISTS(
		select * from (SELECT tbl.ProductAutoId,SUM(tbl.QtyDel) as SoldQty FROM @DelItems AS TBL
		where tbl.Status=1
		group by ProductAutoId) as t
		INNER JOIN ProductMaster as PM ON PM.AutoId=t.ProductAutoId
		where PM.Stock<t.SoldQty
		Union
		select * from (SELECT tbl.ProductAutoId,SUM(tbl.QtyDel-tbl.AddOnQty) as SoldQty FROM @DelItems AS TBL
		where tbl.Status=2
		group by ProductAutoId) as t
		INNER JOIN ProductMaster as PM ON PM.AutoId=t.ProductAutoId
		where PM.Stock<t.SoldQty
	)
	BEGIN  
		SET @isException=1
		SET @exceptionMessage='Stock not available.'
	END
	ELSE
	Begin
		IF EXISTS(SELECT AutoId FROM EmployeeMaster WHERE AutoId=@EmpAutoId AND EmpType in (1,6))
		BEGIN
			SET @CustomerAutoId=(SELECT CustomerAutoId FROM OrderMaster where AutoId=@OrderAutoId )                                                                                                                                 
			SET @CreditAmount=ISNULL((select CreditAmount from OrderMaster where AutoId=@OrderAutoId),0.00)                                                                                                                             
			SET @CreditMemoAmount=ISNULL((select Deductionamount from OrderMaster where AutoId=@OrderAutoId),0.00)                                                                         
			DECLARE @AmtValue1 DECIMAL(10,2)=ISNULL((select AmtPaid from DeliveredOrders where OrderAutoId=@OrderAutoId),0.00)                                          
			DECLARE @AmtValue2 DECIMAL(10,2)=ISNULL((select AmtValue from OrderMaster where AutoId=@OrderAutoId),0.00)                                                                                                                                                  
			DECLARE @CreditAmount1 DECIMAL(10,2)=ISNULL((select CreditAmount from DeliveredOrders where OrderAutoId=@OrderAutoId),0.00)                              
			declare @PaymentRecev1 varchar(20)=(select PaymentRecev from OrderMaster where AutoId=@OrderAutoId)                                                                                
			SET @PaymentAutoId =(select PaymentAutoId from   DeliveredOrders where OrderAutoId=@OrderAutoId)                                                                 
			IF (@CreditAmount>0)                                                                                           
			BEGIN                                                                                                                 
				update CustomerCreditMaster set CreditAmount=isnull(CreditAmount,0)+Isnull(@CreditAmount,0) where CustomerAutoId= @CustomerAutoId
				DELETE FROM tbl_Custumor_StoreCreditLog WHERE ReferenceType='OrderMaster' AND ReferenceNo=CONVERT(VARCHAR(10),@OrderAutoId)
			END                               
			IF (@CreditAmount<0)                                                                                               
			BEGIN                                                                                                                 
				update CustomerCreditMaster set CreditAmount=isnull(CreditAmount,0)-Isnull(@CreditAmount,0) where CustomerAutoId= @CustomerAutoId                                                                                                          
				DELETE FROM tbl_Custumor_StoreCreditLog WHERE ReferenceType='OrderMaster' AND ReferenceNo=CONVERT(VARCHAR(10),@OrderAutoId)                                                                                          
			END                                                                
			UPDATE [dbo].[OrderMaster] SET                                                
			[PaymentRecev] = @PaymentRecev,                                                                                                                                                    
			[AmtDue]  = @GrandTotal,                                                                                                                                                  
			[ValueChanged] = @ValueChanged,                                                         
			[DiffAmt]  = @DiffAmt,                                                                                                                                          
			[NewTotal]  = @NewTotal,                                                                           
			[AcctRemarks] = @Remarks,                                                                                                                           
			[OverallDiscAmt]=@OverallDiscAmt,                                                    
			[ShippingCharges]=@ShippingCharges,                                                                                                                          
			Status=11                                               
			WHERE [AutoId] = @OrderAutoId             
                                                                                                                  
			--select  ProductAutoId,StockValues=(FreshReturnQty* (select  Qty from PackingDetails as t where t.ProductAutoId =tbl.ProductAutoId                                  
			--and UnitType =tbl.UnitAutoId))                                                                                                                                                  
			--into #delivery from  [Delivered_Order_Items] as tbl WHERE [OrderAutoId] = @OrderAutoId   
  
			SELECT ProductAutoId, 
			(FreshReturnQty * ISNULL((select Qty from PackingDetails where ProductAutoId=TBL.ProductAutoId and UnitType=tbl.FreshReturnUnitAutoId),1)) +     
			(MissingItemQty * ISNULL((select Qty from PackingDetails where ProductAutoId=TBL.ProductAutoId and UnitType=tbl.MissingItemUnitAutoId),1) )
			AS StockValues into #delivery
			FROM [Delivered_Order_Items] as tbl WHERE [OrderAutoId] = @OrderAutoId		

			insert into [dbo].[ProductStockUpdateLog]([ProductId],[OledStock],[NewStock],[UserAutoId],[DateTime],ReferenceId,ActionRemark,ReferenceType)
			SELECT pm.ProductId,isnull([Stock],0),isnull([Stock],0) - isnull([Pcs],0),@EmpAutoId,GETDATE(),@OrderAutoId,
			'Order No-(' + (select OrderNO from OrderMaster as  om where om.autoid=@OrderAutoId) + ') has been updated '+(Select FirstName+' '+LastName from Employeemaster where autoid=@EmpAutoId)+'.','OrderMaster' 
			FROM [dbo].[ProductMaster] AS PM                                                                                                   
			INNER JOIN (SELECT [ProductAutoId],SUM(StockValues) as [Pcs] FROM #delivery group by [ProductAutoId]) AS dt 
			ON dt.[ProductAutoId] = PM.[AutoId] and [Pcs]>0

			Update pm                                              
			set Stock=isnull(Stock,0)- StockQty
			from ProductMaster as pm                                                                                                                                                    
			inner join (
			select ProductAutoId,SUM(StockValues) as StockQty from #delivery
			group by ProductAutoId
			)  as t on t.ProductAutoId=pm.AutoId and StockQty>0
          
                                                                                                                                                    
			DELETE FROM [dbo].[Delivered_Order_Items] WHERE [OrderAutoId] = @OrderAutoId    
  
			INSERT INTO [dbo].[Delivered_Order_Items] ([OrderAutoId],[ProductAutoId],[UnitAutoId],[QtyPerUnit],                                                                                                              
			[UnitPrice],[Barcode],         
			[QtyShip],[SRP],[GP],[Tax],FreshReturnQty,FreshReturnUnitAutoId,DamageReturnQty,DamageReturnUnitAutoId,MissingItemQty,MissingItemUnitAutoId,                                                                                                                                                  
			IsExchange,TaxValue,isFreeItem,UnitMLQty,QtyPerUnit_Fresh ,QtyPerUnit_Damage,QtyPerUnit_Missing,Del_CommCode,                                                
			Del_Weight_Oz,Del_CostPrice,Del_MinPrice,BasePrice)                                                                             
			SELECT @OrderAutoId,tbl.[ProductAutoId],tbl.[UnitAutoId],tbl.[QtyPerUnit],tbl.[UnitPrice],tbl.[Barcode],                                                                                                            
			tbl.[QtyShip] ,                                                                    
			pm.[P_SRP],tbl.[GP],tbl.[Tax],                                                                                                                      
			FreshReturnQty,FreshReturnUnitAutoId,DamageReturnQty,DamageReturnUnitAutoId,MissingItemQty,MissingItemUnitAutoId,IsExchange,@TaxValue1,                                                                                      
			isFreeItem,isnull(tbl.MLQty,0),                                                                                                                
			isnull((select top 1 Qty from PackingDetails as pd where pd.ProductAutoId=tbl.ProductAutoId and pd.UnitType=tbl.FreshReturnUnitAutoId),0),                     
			isnull((select top 1 Qty from PackingDetails as pd  where pd.ProductAutoId=tbl.ProductAutoId and pd.UnitType=tbl.DamageReturnUnitAutoId),0), 
			isnull((select top 1 Qty from PackingDetails as pd  where pd.ProductAutoId=tbl.ProductAutoId and pd.UnitType=tbl.MissingItemUnitAutoId),0),   
			--isnull((select top 1 CostPrice from PackingDetails as pd  where pd.ProductAutoId=tbl.ProductAutoId and pd.UnitType=tbl.UnitAutoId),0),                                                                                                      
			ISNULL(P_CommCode,0),ISNULL(pm.WeightOz,0),Del_CostPrice,Del_MinPrice,Del_BasePrice                                                                                                      
	  
			FROM                                
			@DelItems  As tbl    inner join  ProductMaster as pm on pm.AutoId=tbl.ProductAutoId
	  
			SET @PriceLevelAutoId = (SELECT [PriceLevelAutoId] FROM [dbo].[CustomerPriceLevel] WHERE [CustomerAutoId] = @CustomerAutoId)                                                                                                                            
			exec UPDTAE_PRICELEVEL_delivery @OrderAutoId=@OrderAutoId,@PriceLevelAutoId=@PriceLevelAutoId,  @EmpAutoId=@EmpAutoId 
			IF(@CreditAmount>0)
			BEGIN                                                                                                            
				IF((@GrandTotal-@CreditMemoAmount)<@CreditAmount)                                                                                                 
				BEGIN                                                                                                            
					UPDATE CustomerCreditMaster SET CreditAmount=CreditAmount+(@CreditAmount-(@GrandTotal-@CreditMemoAmount)) WHERE CustomerAutoId=@CustomerAutoId                                                                        
					INSERT INTO tbl_Custumor_StoreCreditLog(CustomerAutoId,CreatedBy,CreatedDate,ReferenceNo,ReferenceType,Amount)                                                                  
					VALUES(@CustomerAutoId,@EmpAutoId,GETDATE(),@OrderAutoId,'OrderMaster', -(@CreditAmount-((@CreditAmount-(@GrandTotal-@CreditMemoAmount)))))                                                                            
				END                                                                                                            
				Else                                                                                                            
				BEGIN                                                                                                          
					update CustomerCreditMaster set CreditAmount =isnull(CreditAmount,0)-isnull(@CreditAmount,0)                                                            
					where CustomerAutoId=@CustomerAutoId                               
					insert into tbl_Custumor_StoreCreditLog(CustomerAutoId,ReferenceType,ReferenceNo,Amount,CreatedDate,CreatedBy)                                                                                                                            
					values(@CustomerAutoId,'OrderMaster',@OrderAutoId,@CreditAmount,getdate(),@EmpAutoid)                                                                                                            
				END                                                    
			END                                                                                                            
			IF(@CreditAmount<0)                                                                              
			BEGIN           
				UPDATE CustomerCreditMaster SET CreditAmount=CreditAmount+(@CreditAmount) WHERE CustomerAutoId=@CustomerAutoId                                                                                
				INSERT INTO tbl_Custumor_StoreCreditLog(CustomerAutoId,CreatedBy,CreatedDate,ReferenceNo,ReferenceType,Amount)                                                                                                                                              
				VALUES(@CustomerAutoId,@EmpAutoId,GETDATE(),@OrderAutoId,'OrderMaster', @CreditAmount)           
			END              
   
			SELECT ProductAutoId,Status, 
			((FreshReturnQty * ISNULL((select Qty from PackingDetails where ProductAutoId=TBL.ProductAutoId and UnitType=tbl.FreshReturnUnitAutoId),1))) +     
			((MissingItemQty * ISNULL((select Qty from PackingDetails where ProductAutoId=TBL.ProductAutoId and UnitType=tbl.MissingItemUnitAutoId),1))) 
			AS Stock into #ResultUpdate
			FROM @DelItems AS TBL 

			SELECT ProductAutoId, SUM(QtyDel) AS Stock,tbl.Status into #ResultUpdate1 FROM @DelItems AS TBL where Status=1
			group by ProductAutoId,Status
				
			SELECT tbl.ProductAutoId,SUM(tbl.QtyDel-tbl.AddOnQty) as Stock,tbl.Status into #ResultUpdate2 
			FROM @DelItems AS TBL where tbl.Status=2 
			group by ProductAutoId,Status

			insert into [dbo].[ProductStockUpdateLog]([ProductId],[OledStock],[NewStock],[UserAutoId],[DateTime],ReferenceId,ActionRemark,ReferenceType)
			SELECT pm.ProductId,isnull([Stock],0),isnull([Stock],0) + isnull([Pcs],0),@EmpAutoId,GETDATE(),@OrderAutoId,
			'Order No-(' + (select OrderNO from OrderMaster as  om where om.autoid=@OrderAutoId) + ') has been updated '+(Select FirstName+' '+LastName from Employeemaster where autoid=@EmpAutoId)+'.','OrderMaster' 
			FROM [dbo].[ProductMaster] AS PM                                                                                                   
			INNER JOIN (SELECT [ProductAutoId],SUM(Stock) as [Pcs] FROM #ResultUpdate as rs207 where rs207.Status=0  group by
			[ProductAutoId]) AS dt ON dt.[ProductAutoId] = PM.[AutoId]  and [Pcs]>0

			Update pm                                              
			set Stock=isnull(Stock,0)+ [Pcs]
			FROM [dbo].[ProductMaster] AS PM                                                                                                   
			INNER JOIN (SELECT [ProductAutoId],SUM(Stock) as [Pcs] FROM #ResultUpdate as rs207 where rs207.Status=0  group by
			[ProductAutoId]) AS dt ON dt.[ProductAutoId] = PM.[AutoId]  and [Pcs]>0

			insert into [dbo].[ProductStockUpdateLog]([ProductId],[OledStock],[NewStock],[UserAutoId],[DateTime],ReferenceId,ActionRemark,ReferenceType)
			SELECT pm.ProductId,isnull([Stock],0),isnull([Stock],0) - isnull([Pcs],0),@EmpAutoId,GETDATE(),@OrderAutoId,
			'Order No-(' + (select OrderNO from OrderMaster as  om where om.autoid=@OrderAutoId) + ') has been updated '+(Select FirstName+' '+LastName from Employeemaster where autoid=@EmpAutoId)+'.','OrderMaster' 
			FROM [dbo].[ProductMaster] AS PM                                                                                                   
			INNER JOIN (SELECT [ProductAutoId],SUM(Stock) as [Pcs] FROM #ResultUpdate1 as rs207 where rs207.Status=1  group by
			[ProductAutoId]) AS dt ON dt.[ProductAutoId] = PM.[AutoId]  and [Pcs]>0

			Update pm                                              
			set Stock=isnull(Stock,0)- StockQty
			FROM [dbo].[ProductMaster] AS PM                                                                                                   
			INNER JOIN (SELECT [ProductAutoId],SUM(Stock) as StockQty FROM #ResultUpdate1 as rs207 where rs207.Status=1  group by
			[ProductAutoId]) AS dt ON dt.[ProductAutoId] = PM.[AutoId]  and StockQty>0   
		
			insert into [dbo].[ProductStockUpdateLog]([ProductId],[OledStock],[NewStock],[UserAutoId],[DateTime],ReferenceId,ActionRemark,ReferenceType)
			SELECT pm.ProductId,isnull([Stock],0),isnull([Stock],0) - isnull([Pcs],0),@EmpAutoId,GETDATE(),@OrderAutoId,
			'Order No-(' + (select OrderNO from OrderMaster as  om where om.autoid=@OrderAutoId) + ') has been updated '+(Select FirstName+' '+LastName from Employeemaster where autoid=@EmpAutoId)+'.','OrderMaster' 
			FROM [dbo].[ProductMaster] AS PM                                                                                                   
			INNER JOIN (SELECT [ProductAutoId],SUM(Stock) as [Pcs] FROM #ResultUpdate2 as rs207 where rs207.Status=2  group by
			[ProductAutoId]) AS dt ON dt.[ProductAutoId] = PM.[AutoId]  and [Pcs]>0

			Update pm                                              
			set Stock=isnull(Stock,0)- StockQty
			FROM [dbo].[ProductMaster] AS PM                                                                                                   
			INNER JOIN (SELECT [ProductAutoId],SUM(Stock) as StockQty FROM #ResultUpdate2 as rs207 where rs207.Status=2  group by
			[ProductAutoId]) AS dt ON dt.[ProductAutoId] = PM.[AutoId]  and StockQty>0   
                                                                                                                                                   
			SET @LogRemark = (SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=2)                                                                                               
			SET @LogRemark = REPLACE(@LogRemark,'[OrderNo]',(SELECT oRDERNO FROM OrderMaster WHERE AUTOID=@OrderAutoId))                                                                                                       
			INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                                                                                                       
			VALUES(2,@EmpAutoId,getdate(),@LogRemark,@OrderAutoId)                                                                                                                       
			SET @PayableAmount=ISNULL((SELECT PayableAmount FROM DeliveredOrders WHERE OrderAutoId=@OrderAutoId),0)                                                                      
			SET @AmtValue=ISNULL((SELECT SUM(ReceivedAmount) FROM PaymentOrderDetails WHERE OrderAutoId=@OrderAutoId),0)                                                                                                           
			IF @PayableAmount <@AmtValue                                                                                        
			BEGIN 
				IF EXISTS(SELECT * FROM CustomerCreditMaster WHERE CustomerAutoId=@CustomerAutoId)                                                                                                                     
				BEGIN                                                                                                                 
					UPDATE CustomerCreditMaster SET CreditAmount=isnull((CreditAmount),0)+@AmtValue-@PayableAmount                                                                                                                         
					WHERE CustomerAutoId=@CustomerAutoId                                   
				END                                        
				ELSE                                                                                                                               
				BEGIN                                                                                                                              
					INSERT CustomerCreditMaster VALUES (@CustomerAutoId,@AmtValue-@PayableAmount)                                                                                                                              
				END                                  
				SET @PaymentAutoId=(SELECT TOP 1 PaymentAutoId FROM PaymentOrderDetails WHERE OrderAutoId=@OrderAutoId)                                                      
				insert into tbl_Custumor_StoreCreditLog(CustomerAutoId,ReferenceType,ReferenceNo,Amount,CreatedDate,CreatedBy)                                                                                                                       
				values(@CustomerAutoId,'CustomerPaymentDetails',@PaymentAutoId,-(@AmtValue-@PayableAmount),getdate(),@EmpAutoid)                 
			END 
			Update Delivered_Order_Items set StaticDelQty=QtyDel,StaticNetPrice=NetPrice where OrderAutoId=@OrderAutoId
			if((select CustomerType from CustomerMaster where AutoId=@CustomerAutoId)=3)                
			begin 
				exec [dbo].[ProcUpdatePOStatus_All]                
				@CustomerId=@CustomerAutoId,                
				@OrderAutoId=@OrderAutoId,                
				@Status=11                    
			end   
	ELSE
	BEGIN
		Set @isException=1                                                                                                     
		Set @exceptionMessage='Unauthorized Access.' 
	END
     END  
	 ELSE
	 BEGIN
	    SET @isException=1
		SET @exceptionMessage='Unauthorized Access.'
	 END
  END
 COMMIT TRANSACTION                                                                                                                                                  
 END TRY                                           
 BEGIN CATCH                                                                                
 ROLLBACK TRAN                                                                                                                                          
	Set @isException=1                                        
	Set @exceptionMessage=ERROR_MESSAGE()--'Oops! Something went wrong.Please try later.'                                                                                                                           
 END CATCH                             
 END 
  ELSE IF @Opcode=209
 BEGIN
       Declare @Status int
       UPDATE OrderMaster SET isMLManualyApply=case when isMLManualyApply=1 then 0 else 1 end, MLTaxRemark=@MLTaxRemark WHERE AutoId=@OrderAutoId  

SET @Status=(Select isMLManualyApply FROM OrderMaster WHERE AutoId=@OrderAutoId  )

SET @LogRemark = REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=(case when
@Status=0 then 19 else 31 end)),'[OrderNo]',(SELECT [OrderNo] FROM OrderMaster where [AutoId] = @OrderAutoId))

INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                                
VALUES(case when @Status=0 then 19 else 31 end,@EmpAutoId,getdate(),@LogRemark,@OrderAutoId)  
 END

   ELSE IF @Opcode=106                                                                                                                                                    
  BEGIN           
	IF EXISTS(SELECT * FROM [dbo].[ItemBarcode] WHERE [Barcode]=@Barcode)                                                                                                                                                    
	BEGIN                                                                                                                                         
		SET @ProductAutoId=(SELECT [ProductAutoId] FROM [dbo].[ItemBarcode] WHERE [Barcode]=@Barcode)                                                                          
		SET @UnitAutoId=(SELECT UnitAutoId FROM [dbo].[ItemBarcode] WHERE [Barcode]=@Barcode)    
		
		DECLARE @custType1 int=(SELECT CustomerType FROM CustomerMaster WHERE AutoId=@CustomerAutoId)   
		
		SELECT PM.AutoId as ProductAutoId,PM.ProductId,PM.ProductName,PD.Qty as UnitQty,PD.UnitType,PD.LatestBarcode,                                                        
		ISNULL(CostPrice,0) AS [MinPrice] ,                                                                                                                                                    
		ISNULL((SELECT TOP 1 UnitType +  ' ('+ CONVERT(VARCHAR(20),PD.QTY)+' pcs)' from UnitMaster AS UM WHERE UM.AutoId=PD.UnitType),'') AS UnitName, 
		[CostPrice],                                                                                                                                                    
		(case                                                                                                   
		when @IsExchange=1 or @IsFreeItem=1 then 0.00                                                                                                                                            
		WHEN  @custType1=3 then CONVERT(DECIMAL(10,2),ISNULL(CostPrice,0))                                                                                                                                                
		else CONVERT(DECIMAL(10,2),[Price]) end) as  [Price],PD.Price as BasePrice,                                                     
		CONVERT(DECIMAL(10,2),((PM.[P_SRP]-([Price]/(CASE WHEN [Qty]=0 then 1 else [Qty] end )))/(CASE WHEN ISNULL(PM.[P_SRP],0)=0 then 1 else PM.[P_SRP] END)) * 100) AS GP,                                                   
		isnull((select Top 1 [CustomPrice] from [dbo].[ProductPricingInPriceLevel] AS PPL where                                    
		PPL.[ProductAutoId] = PD.[ProductAutoId] AND PPL.[UnitAutoId] = PD.[UnitType]                                                                                                                                                    
		AND [PriceLevelAutoId] = (SELECT [PriceLevelAutoId] FROM [dbo].[CustomerPriceLevel]                                                       
		WHERE [CustomerAutoId]=@CustomerAutoId)                                     
		Order by [CustomPrice] desc),null) as [CustomPrice],PM.[Stock]/(CASE WHEN Qty=0 THEN 1 ELSE QTY END)  as Stock,PM.[TaxRate],                                                                                                 
		PM.[P_SRP] AS [SRP],                                                                                                                    
		(case when @IsExchange=1 or @IsFreeItem=1 then 0.00 else PM.MLQty end) as MLQty,(case when @IsExchange=1 or @IsFreeItem=1 then 0.00 else isnull(WeightOz,0) end) 
		as WeightOz   
		into #result423 FROM [dbo].[PackingDetails] As PD                                                                                                                                                     
		INNER JOIN [dbo].[ProductMaster] AS PM ON PD.[ProductAutoId]=PM.[AutoId]                                                                                                                                                    
		WHERE PD.[ProductAutoId] = @ProductAutoId and PD.[UnitType] = @UnitAutoId                                                                                          
          
		SELECT * FROM #result423  	                          
	END
	
  END 
  else if @Opcode=208
	begin
			declare @EmpType int
			select @EmpType=EmpType from EmployeeMaster where AutoId=@EmpAutoId
			if exists(select * from TBL_MST_SecurityMaster where SecurityType=@EmpType and typeEvent=9)
			begin
				set @isException=0
			end
			else
			begin
				set @isException=1
			end 
			
	end  
	 else if @Opcode=210
	begin
			
			select @EmpType=EmpType from EmployeeMaster where AutoId=@EmpAutoId
			if exists(select * from TBL_MST_SecurityMaster where [SecurityValue]=@CheckSecurity and SecurityType=@LoginEmpType and typeEvent=1)
			begin
				set @isException=0
			end
			else
			begin
				set @isException=1
			end 
			
	end  
	else if @Opcode=211
	  begin
				   set @db_name=(select [ChildDB_Name] from CompanyDetails)
					IF  @db_name IS NULL
					BEGIN 
						SET @isException=1                                      
						SET @exceptionMessage='System  is not allow migration.';
					END
					ELSE IF EXISTS(select 1 from DeliveredOrders where OrderAutoId=@OrderAutoId and AmtDue>0)
					BEGIN
						SET @isException=1                                      
						SET @exceptionMessage='Order can not be migrate because order have some due amount';      
					END
					ELSE IF EXISTS(select * from (
					SELECT OrderAutoId,SUM(CPD.ReceivedAmount) AS ReceivedAmount,SUM(POD.ReceivedAmount) as SettledAmount FROM CustomerPaymentDetails AS CPD
					INNER JOIN PaymentOrderDetails AS POD ON POD.PaymentAutoId=CPD.PaymentAutoId
					WHERE OrderAutoId=@OrderAutoId AND CPD.Status=0
					GROUP BY OrderAutoId
					) as t where t.ReceivedAmount!=SettledAmount)
					BEGIN
						SET @isException=1                                      
						SET @exceptionMessage='Received and Settled payment should be equal.please check payment history for this order';  
					END
					ELSE 
					BEGIN
						update OrderMaster set MigrateRemarks=@Remarks where AutoId=@OrderAutoId
						exec [dbo].[ProcMigrate_OrderMaster] @OrderAutoId=@OrderAutoId,@isException=@isException out,@exceptionMessage=@exceptionMessage out--,@Remarks=@Remarks
					END
	  end
  ELSE IF @Opcode=212
  BEGIN
		IF EXISTS(SELECT STATUS FROM OrderMaster where AutoId=@OrderAutoId and status=6)
		BEGIN
			Update OIM SET OIM.OM_MinPrice=convert(decimal(12,2),PD.CostPrice),OIM.OM_CostPrice=convert(decimal(12,2),PD.CostPrice),OIM.UnitPrice=convert(decimal(12,2),PD.CostPrice)
			FROM  OrderItemMaster AS OIM
			INNER JOIN PackingDetails AS PD 
			on OIM.ProductAutoId=PD.ProductAutoId 
			AND OIM.UnitTypeAutoId=PD.UnitType
			Where OrderAutoId=@OrderAutoId AND (OIM.OM_CostPrice!=convert(decimal(12,2),PD.CostPrice))

			INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                                                                                            
			VALUES(46,@EmpAutoId,getdate(),@Remarks,@OrderAutoId)   
		END
		ELSE 
		BEGIN
			SET @isException=1                                      
			SET @exceptionMessage='Order has been closed.so Cost Price can not be updated'; 
		END
  END
  END TRY                      
  BEGIN CATCH                      
	  Set @isException=1                      
	  Set @exceptionMessage=ERROR_MESSAGE()                
  END CATCH                      
END 

