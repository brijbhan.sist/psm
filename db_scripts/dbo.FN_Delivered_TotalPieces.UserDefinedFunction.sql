USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Delivered_TotalPieces]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION  [dbo].[FN_Delivered_TotalPieces]
(
	 @AutoId int
)
RETURNS int
AS
BEGIN
	DECLARE @TotalPieces  int
	SET @TotalPieces=(SELECT (QtyPerUnit*	QtyShip) FROM Delivered_Order_Items WHERE AutoId=@AutoId)	
	RETURN @TotalPieces
END
 
GO
