ALTER PROCEDURE [dbo].[ProcWrongProductPrice]
@OpCode int= Null,
@ProductName VARCHAR(12) = Null,
@Unit VARCHAR(100) = Null,
@isException bit out,
@PageIndex INT = 1,  
@PageSize INT = 10,  
@RecordCount INT =null, 
@exceptionMessage varchar(max) out
AS

BEGIN
BEGIN TRY
		SET @isException = 0
		SET @exceptionMessage = 'success'
		IF @OpCode = 41
		BEGIN
		SELECT ROW_NUMBER() OVER(ORDER BY ProductId) AS RowNumber,ProductAutoId, ProductId,ProductName,um.UnitType,Qty,MinPrice,CostPrice,Price,
	P_SRP as SRP,WHminPrice,FORMAT(pd.UpdateDate,'MM/dd/yyyy HH:hh tt') as LastDate   into #RESULT from PackingDetails as pd
			inner join ProductMaster as pm on pm.AutoId=pd.ProductAutoId
			inner join UnitMaster as um on um.AutoId=pd.UnitType
						WHERE 
						( ISNULL(@ProductName,'')='' OR ProductName like '%'+@ProductName+'%') 
						AND (ISNULL(@Unit,'' )='' OR um.UnitType like '%'+@Unit+'%')
						AND InvalidData>0

		SELECT COUNT(ProductId) AS RecordCount, case when @PageSize=0 then COUNT(distinct ProductId) else @PageSize end AS PageSize, @PageIndex AS PageIndex FROM #RESULT        
        
		SELECT * FROM #RESULT        
		WHERE @PageSize=0 or  RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1   
						
		END
		END TRY
		BEGIN CATCH
			SET @exceptionMessage = 'Oops! Something went wrong.Please try later.'
			SET @isException = 1
		END CATCH
END
GO
