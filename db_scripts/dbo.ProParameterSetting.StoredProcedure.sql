ALTER PROCEDURE [dbo].[ProParameterSetting]        
@Opcode INT=Null,        
@StateId INT=NULL,        
@StateName VARCHAR(50)=NULL,        
@Abbreviation VARCHAR(50)=NULL,        
@Status INT=NULL,        
@CityId int = null,        
@CityName Varchar(50)=null,        
@ZipCode nvarchar(20) = null,        
@ZipId int = null,        
@PageIndex INT=1,        
@PageSize INT=10,        
@RecordCount INT=null,        
@isException BIT OUT,        
@exceptionMessage VARCHAR(max) OUT        
AS        
BEGIN        
BEGIN TRY        
   SET @isException=0        
   SET @exceptionMessage='Success'
   Declare @SqlQuery nvarchar(max),@ChildDbLocation varchar(50)
	select @ChildDbLocation=ChildDB_Name from CompanyDetails
 IF @opcode = 11        
 BEGIN         
	 IF exists(select * from State where trim(StateCode) = trim(@Abbreviation))        
	 BEGIN        
	   SET @isException=1        
	   SET @exceptionMessage='Abbreviation name already exist.'        
	 END       
	 ELSE IF (SELECT COUNT(*) FROM State WHERE StateName=@StateName or [StateCode]=@Abbreviation) = 0        
	 BEGIN        
			insert into State ([StateCode], [StateName], [Status]) values (@Abbreviation, @StateName, @Status) 
			SET @StateID=SCOPE_IDENTITY()
			SET @SqlQuery='
			IF NOT EXISTS(Select AutoId from ['+@ChildDbLocation+'].[dbo].State AS ST Where ST.StateName='''+Convert(varchar(150),@StateName)+''') 
			BEGIN    
				SET IDENTITY_INSERT ['+@ChildDbLocation+'].[dbo].State ON
				INSERT INTO ['+@ChildDbLocation+'].[dbo].State (AutoId,StateCode,StateName,Status,CountryId) 
				select AutoId,StateCode,StateName,Status,CountryId from State where autoid='+convert(varchar(10),@StateID)+'
				SET IDENTITY_INSERT ['+@ChildDbLocation+'].[dbo].State OFF
			END'
		
			EXEC sp_executesql @SqlQuery
	 END        
	 ELSE        
	 BEGIN        
	   SET @isException=1        
	   SET @exceptionMessage='State name already exist.'        
	 END     
 END        
 IF @Opcode=41        
 BEGIN      
      Select ROW_NUMBER() OVER(ORDER BY StateName) AS RowNumber,AutoId ,[StateCode], [StateName], case when isnull([Status],0)=0 then 'Inactive' else 'Active' END as Status  into #RESULT41 from State         
      WHERE  (@StateName is null or @StateName ='' or StateName like '%' + @StateName + '%')and (@Status is null or @Status =2 or Status =@Status)      
      SELECT COUNT(RowNumber) AS RecordCount, @PageSize AS PageSize, @PageIndex AS PageIndex FROM #RESULT41       
	SELECT * FROM #RESULT41        
	WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize)         
 END        
 IF @Opcode = 42        
 BEGIN        
     select * from State where [AutoId] = @StateId   order by StateName asc     
 END        
 IF @Opcode = 21        
 BEGIN        
  IF exists(select * from State where StateCode = @Abbreviation and AutoId != @StateId)        
  BEGIN        
   SET @isException=1        
   SET @exceptionMessage='Abbreviation already exist.'        
  END       
  ELSE IF(select count(*) from [dbo].[State] where trim(StateName) = trim(@StateName) and AutoId != @StateId)=0        
  BEGIN        
      update state set [StateName] = @StateName , [StateCode] = @Abbreviation, [Status] = @Status where [AutoId] = @StateId ;
	 set @SqlQuery ='update sc set sc.StateCode=s.StateCode,sc.StateName=s.StateName,sc.Status=s.Status,sc.CountryId=s.CountryId from  ['+@ChildDbLocation+'].[dbo].State as sc 
	 inner join State as s on s.autoid =sc.autoid where s.[AutoId] ='+convert(varchar(90), @StateId); 
	 EXEC sp_executesql @SqlQuery
  END        
  else        
  BEGIN        
     SET @isException=1        
     SET @exceptionMessage='State already exist.'        
  END       
        
 END        
 IF @Opcode = 31        
 BEGIN        
 BEGIN try        
       delete from State where [AutoId] = @StateId
	   SET @SqlQuery='Delete from ['+@ChildDbLocation+'].[dbo].State where AutoId='+Convert(varchar(20),@StateId);
	   Exec sp_executesql @SqlQuery
 END TRY        
 BEGIN catch        
       SET @isException=1        
       SET @exceptionMessage='State used in another city'        
 END catch        
 END        
        
 --here start to City procedure        
 IF @Opcode = 45        
 BEGIN         
       select distinct [StateName],[AutoId] from State   where Status = 1  ORDER BY [StateName]         
 END        
 IF @Opcode = 43        
 BEGIN        
    Select ROW_NUMBER() OVER(ORDER BY StateName) AS RowNumber,dbo.State.StateName, dbo.CityMaster.CityName, dbo.CityMaster.AutoId, case when isnull(CityMaster.Status,0)=0 then 'Inactive' else 'Active' END as Status          
    into #RESULT43         
    FROM dbo.State INNER JOIN dbo.CityMaster ON dbo.State.AutoId = dbo.CityMaster.StateId        
    WHERE  (@StateId is null or @StateId =0 or dbo.CityMaster.StateId =@StateId)and (@Status is null or @Status =2 or dbo.CityMaster.Status =@Status)        
    and (@CityName is null or @CityName = '' or CityName like '%' + @CityName + '%')     
    SELECT COUNT(RowNumber) AS RecordCount, @PageSize AS PageSize, @PageIndex AS PageIndex FROM #RESULT43        
    SELECT * FROM #RESULT43        
    WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize)        
 END        
 IF @Opcode = 44        
 BEGIN        
      SELECT distinct dbo.State.AutoId,dbo.State.StateName FROM dbo.CityMaster INNER JOIN dbo.State ON dbo.CityMaster.StateId = dbo.State.AutoId order by dbo.State.StateName asc        
 END        
 IF @Opcode = 12        
 BEGIN        
 IF(select count(*) from [dbo].[CityMaster] where [StateId]=@StateId and [CityName]=@CityName)=0        
 BEGIN        
     insert into [dbo].[CityMaster]([StateId], [CityName], [Status]) values (@StateId, @CityName, @Status) 
	 

		SET @CityId=SCOPE_IDENTITY()
		SET @SqlQuery=' 
		SET IDENTITY_INSERT ['+@ChildDbLocation+'].[dbo].CityMaster ON
		INSERT INTO ['+@ChildDbLocation+'].[dbo].[CityMaster] (AutoId,[StateId], [CityName], [Status]) 
		select AutoId,[StateId], [CityName], [Status] from [CityMaster] where autoid='+convert(varchar(10),@CityId)+'
		SET IDENTITY_INSERT ['+@ChildDbLocation+'].[dbo].CityMaster OFF
		'
		EXEC sp_executesql @SqlQuery
 END        
  ELSE        
   BEGIN        
     SET @isException=1        
     SET @exceptionMessage='City already exist.'        
   END        
 END        
 IF @Opcode = 32        
 BEGIN        
 BEGIN try        
     delete from CityMaster where [AutoId] = @StateId  
	 SET @SqlQuery='Delete from ['+@ChildDbLocation+'].[dbo].CityMaster where AutoId='+Convert(varchar(20),@StateId);
	 Exec sp_executesql @SqlQuery
 END TRY        
 BEGIN catch        
     SET @isException=1        
     SET @exceptionMessage='City has been used in application.'        
 END catch        
 END        
 IF @Opcode = 46        
 BEGIN        
     select [AutoId], [StateId], [CityName], [Status] from [dbo].[CityMaster] where [AutoId] = @StateId ORDER BY  [CityName] asc      
 END        
 IF @Opcode = 22        
 BEGIN        
 IF(select count(*) from [dbo].[CityMaster] where [StateId]=@StateId and [CityName]=@CityName and AutoId != @CityId)=0        
 BEGIN        
     update [dbo].[CityMaster] set [StateId]=@StateId,[CityName]=@CityName,Status=@Status where [AutoId]=@CityId  
	
		set @SqlQuery ='update sc set sc.[StateId]=s.[StateId],sc.[CityName]=s.[CityName],sc.Status=s.Status 
		from  ['+@ChildDbLocation+'].[dbo].[CityMaster] as sc 
		inner join [CityMaster] as s on s.autoid =sc.autoid where sc.[AutoId] ='+convert(varchar(90), @StateId);
		EXEC sp_executesql @SqlQuery
 END        
 else        
 BEGIN        
  SET @isException=1        
  SET @exceptionMessage='City already exist.'        
 END        
 END     
 --here start Zip Code Procedure     
 IF @Opcode = 47        
 BEGIN        
     select [AutoId], [CityName] from [dbo].[CityMaster]  ORDER BY  CityName     
 END        
 IF @Opcode = 48        
 BEGIN        
  Select ROW_NUMBER() OVER(ORDER BY StateName) AS RowNumber,zm.AutoId,zm.Zipcode,cm.CityName,sm.StateName ,case when isnull(zm.Status,0)=0 then 'Inactive' else 'Active' END as Status          
  into #RESULT44         
  FROM dbo.ZipMaster zm INNER JOIN dbo.CityMaster cm ON (cm.AutoId = zm.CityId) inner join State sm on(sm.AutoId=cm.StateId)        
  WHERE  (@StateId is null or @StateId =0 or sm.AutoId =@StateId)and (@Status is null or @Status =2 or zm.Status =@Status)         
  and (@CityId is null or @CityId =0 or cm.AutoId =@CityId) and (@ZipCode is null or @ZipCode ='' or zm.Zipcode =@ZipCode)     
  SELECT COUNT(RowNumber) AS RecordCount, @PageSize AS PageSize, @PageIndex AS PageIndex FROM #RESULT44        
  SELECT * FROM #RESULT44        
  WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize)        
 END        
 IF @Opcode = 13        
 BEGIN        
 IF(select count(*) from [dbo].[ZipMaster] where [CityId] = @CityId and [Zipcode] = @ZipCode)=0        
 BEGIN        
     insert into [dbo].[ZipMaster]([CityId], [Zipcode], [Status]) values(@CityId, @ZipCode, @Status) 
	
	SET @CityId=SCOPE_IDENTITY()
	SET @SqlQuery=' 
	SET IDENTITY_INSERT ['+@ChildDbLocation+'].[dbo].[ZipMaster] ON
	INSERT INTO ['+@ChildDbLocation+'].[dbo].[ZipMaster] (AutoId,[CityId], [Zipcode], [Status]) 
	select AutoId,[CityId], [Zipcode], [Status] from [ZipMaster] where autoid='+convert(varchar(10),@CityId)+'
	SET IDENTITY_INSERT ['+@ChildDbLocation+'].[dbo].ZipMaster OFF
	'
	EXEC sp_executesql @SqlQuery
 END        
 else        
 BEGIN        
   SET @isException=1        
   SET @exceptionMessage='Zip code already exist.'        
 END        
 END        
 IF @Opcode = 33        
 BEGIN     
      SET @ZipCode=ISNULL((Select Zipcode from ZipMaster WHERE AutoId=@StateId),'')
	  SET @CityId=ISNULL((Select CityId from ZipMaster WHERE AutoId=@StateId),'')
	  SET @CityName=ISNULL((Select CityName from CityMaster WHERE AutoId=@CityId),'')
	  IF @ZipCode!=''
	  BEGIN
            IF EXISTS(Select Zipcode from BillingAddress WHERE Zipcode=@ZipCode and ZipcodeAutoid=@StateId)
			BEGIN  
				SET @isException=1        
				SET @exceptionMessage='Zip code is using in Billing address.'
			END
			ELSE IF EXISTS(Select Zipcode from ShippingAddress WHERE Zipcode=@ZipCode  and ZipcodeAutoid=@StateId)
			BEGIN  
				SET @isException=1        
				SET @exceptionMessage='Zip code is using in Shipping Address.'
			END
			ELSE IF EXISTS(Select * from VendorMaster WHERE Zipcode=@ZipCode and City=@CityName)
			BEGIN  
				SET @isException=1        
				SET @exceptionMessage='Zip code is using in Vendor Address.'
			END
			ELSE
			BEGIN
				delete [ZipMaster] where AutoId = @StateId    
				SET @SqlQuery='Delete from ['+@ChildDbLocation+'].[dbo].[ZipMaster] where AutoId='+Convert(varchar(20),@StateId)+''
				Exec sp_executesql @SqlQuery
			END   
	  END
 END        
 IF @Opcode = 49        
 BEGIN        
  SELECT dbo.State.AutoId AS StateId, dbo.CityMaster.AutoId AS CityId, dbo.ZipMaster.AutoId AS ZipId, dbo.ZipMaster.Zipcode, dbo.ZipMaster.Status        
  FROM dbo.State INNER JOIN        
  dbo.CityMaster ON dbo.State.AutoId = dbo.CityMaster.StateId INNER JOIN        
  dbo.ZipMaster ON dbo.CityMaster.AutoId = dbo.ZipMaster.CityId where ZipMaster.AutoId = @StateId        
 END        
 IF @Opcode = 23        
 BEGIN        
 IF(select count(*) from [dbo].[ZipMaster] where AutoId != @ZipId and [CityId] = @CityId and [Zipcode] = @ZipCode and Status = @Status)=0        
 BEGIN        
     update ZipMaster set [CityId] = @CityId, [Zipcode] = @ZipCode, [Status] = @Status where AutoId = @ZipId;
	
	
	 set @SqlQuery ='update sc set sc.[CityId]=s.[CityId],sc.[Zipcode]=s.[Zipcode],sc.Status=s.Status 
				 from  ['+@ChildDbLocation+'].[dbo].[ZipMaster] as sc 
				 inner join [ZipMaster] as s on s.autoid =sc.autoid where sc.[AutoId] ='+convert(varchar(90), @StateId);
				 EXEC sp_executesql @SqlQuery
 END        
 else        
 BEGIN        
     SET @isException=1        
     SET @exceptionMessage='Zip code already exist.'     
 END        
 END        
 IF @Opcode = 410        
 BEGIN        
     SELECT [AutoId],[CityName] from [dbo].[CityMaster] where [StateId] = @StateId   order by  [CityName] asc    
 END        
END TRY        
BEGIN CATCH        
   SET @isException=1        
   SET @exceptionMessage='Oops! Something went wrong.Please try later.'        
END CATCH        
END 
GO
