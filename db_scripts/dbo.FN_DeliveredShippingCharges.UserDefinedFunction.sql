USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_DeliveredShippingCharges]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 

CREATE FUNCTION  [dbo].[FN_DeliveredShippingCharges]
(
	 @orderAutoId int
)
RETURNS decimal(10,2)
AS
BEGIN
	DECLARE @ShippingCharges decimal(10,2)
	set @ShippingCharges=isnull((select ShippingCharges from OrderMaster where AutoId=@orderAutoId)	,0.00)
	RETURN @ShippingCharges
END

GO
