USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedTableType [dbo].[DT_UpdateBillItems]    Script Date: 11/20/2019 5:05:52 AM ******/
CREATE TYPE [dbo].[DT_UpdateBillItems] AS TABLE(
	[rowAutoId] [int] NULL,
	[Quantity] [int] NULL,
	[TotalPieces] [int] NULL,
	[Price] [decimal](18, 2) NULL
)
GO
