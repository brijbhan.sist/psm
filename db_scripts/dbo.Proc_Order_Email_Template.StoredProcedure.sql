USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[Proc_Order_Email_Template]    Script Date: 12/30/2019 11:59:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
   
CREATE PROCEDURE [dbo].[Proc_Order_Email_Template]     
   
  @OrderAutoId int  
AS                    
BEGIN   
   
declare @OrderNo varchar(50),@OrderDate varchar(20),@CustomerId varchar(20),@CustomerName varchar(200),  
@ContactPersonName varchar(200),@Contact varchar(200),@TermsDesc varchar(200),@shipVia varchar(200),  
@SalesPerson varchar(200),@BusinessName varchar(250),@shipAddr varchar(500),@BillAddr varchar(500),  
@TotalAmount varchar(20)  
   
  
  
SELECT  @OrderNo=OrderNo,@OrderDate=FORMAT(OrderDate,'MM/dd/yyyy'),@CustomerId=CustomerId,@CustomerName=cm.CustomerName,  
@ContactPersonName=CM.ContactPersonName,@Contact=(CM.[Contact1] + '/' + CM.[Contact2]),  
@TermsDesc  =CT.[TermsDesc],@shipVia=ST.[ShippingType],@SalesPerson=(EM.[FirstName] + ' ' + EM.[LastName]),  
@BusinessName=CM.BusinessName,@shipAddr=(SA.[Address]+','+S1.[StateName]+','+SA.[City]+','+SA.[Zipcode]),  
@BillAddr=(BA.[Address]+','+S.[StateName]+','+BA.[City]+','+BA.[Zipcode]),  
@TotalAmount=TotalAmount   
  FROM [dbo].[OrderMaster] As OM                                                                                
  INNER JOIN [dbo].[StatusMaster] AS SM ON SM.AutoId = OM.Status AND SM.[Category] = 'OrderMaster'                                                               
  INNER JOIN [dbo].[BillingAddress] AS BA ON BA.[AutoId] = OM.[BillAddrAutoId]                                                                                    
  INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = OM.[ShipAddrAutoId]                                 
  INNER JOIN [dbo].[State] AS S ON S.AutoId = BA.[State]                                                                                     
  LEFT JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]                                                                         
  INNER JOIN [dbo].[CustomerMaster] AS CM ON CM.[AutoId] = OM.[CustomerAutoId]                                                                                   
  LEFT JOIN [dbo].[CustomerTerms] AS CT ON CT.[TermsId] = CM.[Terms]                                                                  
  LEFT JOIN [dbo].[ShippingType] AS ST ON ST.[AutoId] = OM.[ShippingType]                                                                                     
  left JOIN [dbo].[EmployeeMaster] AS EM ON EM.[AutoId] = OM.[SalesPersonAutoId]       
  WHERE OM.AutoId = @OrderAutoId     
  
declare @CompanyName varchar(250),@Logo varchar(500),@Website varchar(120),@Phone varchar(20),@Address varchar(20),  
@EmailAddress varchar(120),@FaxNo varchar(120),@TermsCondition varchar(max)  
select @CompanyName=CompanyName,@Logo=Logo,@Website=Website,@Phone=[MobileNo],@Address=[Address],@EmailAddress=EmailAddress,  
@FaxNo=FaxNo,@TermsCondition=TermsCondition from CompanyDetails  
declare @orderhtml nvarchar(max)=null  
set @orderhtml='<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml">'  
set @orderhtml+='<head> <style>table{border-collapse:collapse;}td {padding: 3px 8px;}.text-center{text-align:center}'  
set @orderhtml+='table {width: 100%;}body {font-size: 14px;font-family: monospace;}'  
set @orderhtml+='.center{text-align: center;}.right {text-align: right;}thead > tr {background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a;}'  
set @orderhtml+='thead > tr > td {text-align: center !important;}.InvContent {width: 100%;}.tdLabel {font-weight: 700;}'  
set @orderhtml+='@media print {#btnPrint {display: none;}}legend {border-bottom: 1px solid #5f7c8a;}'  
set @orderhtml+='.tableCSS > tbody > tr > td,.tableCSS > thead > tr > td,.tableCSS > tfoot > tr > td {border: 1px solid #5f7c8a;}pre{border:none !important;}'  
set @orderhtml+='</style>'  
set @orderhtml+='</head>'  
set @orderhtml+='<body style="font-family: Arial; font-size: 12px">'  
set @orderhtml+='<div class="InvContent">'  
set @orderhtml+='<fieldset>'  
set @orderhtml+='<table class="table tableCSS">'  
set @orderhtml+='<tbody>'  
set @orderhtml+='<tr>'  
    set @orderhtml+='<td colspan="6" style="text-align: center">'  
    set @orderhtml+='<span style="font-size: 20px;" id="PageHeading">'+@CustomerName+'-'+@OrderNo+'</span>'  
    set @orderhtml+='<span style="font-size: 20px;float:right;font-weight:600"><i>SALES ORDER</i></span>'  
    set @orderhtml+='</td>'  
set @orderhtml+='</tr>'  
set @orderhtml+='<tr>'  
    set @orderhtml+='<td rowspan="2" style="width: 80px !important;">'  
    set @orderhtml+='<img src="/Img/logo/'+@Logo+'" id="logo" class="img-responsive" style="width: 100%;"/></td>'  
                set @orderhtml+='<td><span id="CompanyName">'+@CompanyName+'</span></td>'  
    set @orderhtml+='<td class="tdLabel" style="white-space: nowrap">Customer Id</td>'  
    set @orderhtml+='<td id="acNo" style="white-space: nowrap">'+@CustomerId+'</td>'  
    set @orderhtml+='<td class="tdLabel" style="white-space: nowrap">Sales Rep</td>'  
                set @orderhtml+='<td id="salesRep" style="white-space: nowrap">'+@SalesPerson+'</td>'  
set @orderhtml+='</tr>'  
set @orderhtml+='<tr>'  
                set @orderhtml+='<td><span id="Address">'+@Address+'</span></td>'  
                set @orderhtml+='<td class="tdLabel" style="white-space: nowrap">Customer Contact</td>'  
                set @orderhtml+='<td id="storeName" style="white-space: nowrap">'+@ContactPersonName+'</td>'  
                set @orderhtml+='<td class="tdLabel">Order No </td>'  
                set @orderhtml+='<td id="so">'+@OrderNo+'</td>'  
set @orderhtml+='</tr>'  
set @orderhtml+='<tr>'  
    set @orderhtml+='<td><span id="Website">'+@Website+'</span></td>'  
                set @orderhtml+='<td>Phone:<span id="Phone">'+@Phone+'</span></td>'  
                set @orderhtml+='<td class="tdLabel">Contact No</td>'  
                set @orderhtml+='<td id="ContPerson">'+@Contact+'</td>'  
                set @orderhtml+='<td class="tdLabel" style="white-space: nowrap">Order Date</td>'  
                set @orderhtml+='<td id="soDate" style="white-space: nowrap">'+@OrderDate+'</td>'  
set @orderhtml+='</tr>'  
set @orderhtml+='<tr>'  
                set @orderhtml+='<td><span id="EmailAddress">'+@EmailAddress+'</span></td>'  
                set @orderhtml+='<td>Fax:<span id="FaxNo">'+@FaxNo+'</span></td>'  
                set @orderhtml+='<td class="tdLabel">Terms</td>'  
                set @orderhtml+='<td style="font-size: 13px"><b><span id="terms">'+@TermsDesc+'</span></b> </td>'  
                set @orderhtml+='<td class="tdLabel">Ship Via</td>'  
                set @orderhtml+='<td id="shipVia">'+@shipVia+'</td>'  
set @orderhtml+='</tr>'  
if(@BusinessName!='')  
BEGIN  
set @orderhtml+='<tr>'  
                set @orderhtml+='<td><span class="tdLabel">Business Name:</span></td>'  
                set @orderhtml+='<td colspan="5"><span id="BusinessName">'+@BusinessName+'</span></td>'  
set @orderhtml+='</tr>'  
END  
set @orderhtml+='<tr>'  
    set @orderhtml+='<td><span class="tdLabel">Shipping Address:</span></td>'  
    set @orderhtml+='<td colspan="5"><span id="shipAddr" style="font-size: 13px">'+@shipAddr+'</span></td>'  
    set @orderhtml+='</tr>'  
set @orderhtml+='<tr>'  
                set @orderhtml+='<td><span class="tdLabel">Billing Address:</span></td>'  
                set @orderhtml+='<td colspan="5"><span id="billAddr" style="font-size: 13px">'+@BillAddr+'</span></td>'  
set @orderhtml+='</tr>'  
set @orderhtml+='</tbody>'  
set @orderhtml+='</table>'  
set @orderhtml+='</fieldset><fieldset>'  
set @orderhtml+='<table class="table tableCSS" id="tblProduct">'  
set @orderhtml+='<thead>'  
  
 SELECT ROW_NUMBER()  over(order by CM.[CategoryName]) as RowNo,CM.[CategoryName],PM.[ProductId],PM.[ProductName],                                                                                    
 UM.[UnitType],OIM.[UnitPrice],OIM.[TotalPieces],OIM.[SRP],OIM.[GP],OIM.[Tax],                                                                                    
 OIM.[NetPrice],OIM.[Barcode],OIM.[QtyShip],OIM.[RequiredQty] ,   
 OIM.IsExchange,ISNULL(isFreeItem,0) AS isFreeItem  
 into #OrderItem FROM [dbo].[OrderItemMaster] AS OIM                                                             
 INNER JOIN [dbo].[ProductMaster] AS PM ON PM.AutoId = OIM.[ProductAutoId]                                          
 INNER JOIN [dbo].[UnitMaster] AS UM ON UM.AutoId = OIM.UnitTypeAutoId                                                                                     
 INNER JOIN [dbo].[CategoryMaster] As CM ON CM.[AutoId] = PM.[CategoryAutoId]  
 WHERE [OrderAutoId] = @OrderAutoId                                                                          
 ORDER BY CM.[CategoryName] ASC               
 declare @i int=1  
  set @orderhtml+='<tr>'  
  SET @orderhtml+=(SELECT '<td class="text-center">Qty</td>'+''+'<td style="width: 10px;white-space:nowrap" class="text-center">Unit Type</td>'+'<td style="white-space: nowrap">Product ID</td>'+  
  '<td style="white-space: nowrap">Item</td>'+  
  '<td class="SRP" style="text-align: right; width: 10px">SRP</td>'+  
  '<td class="GP" style="text-align: right; width: 10px">GP(%)</td>'+''+'<td class="UnitPrice" style="text-align: right; width: 10px">Price</td>'+  
  '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td>')  
  set @orderhtml+='</tr>'  
  set @orderhtml+='</thead>'  
   set @orderhtml+='<tbody>'  
 WHILE @i<=(select MAX(RowNo) from #OrderItem)  
 BEGIN  
  set @orderhtml+='<tr>'  
  SET @orderhtml+=(SELECT '<td class="text-center">'+CONVERT(VARCHAR(10),RequiredQty)+'</td>'+''+'<td style="width: 10px">'+[UnitType]+'</td>'+  
  '<td style="white-space: nowrap" class="text-center">'+cONVERT(VARCHAR(20),ProductId)+'</td>'+  
  '<td style="white-space: nowrap">'+cONVERT(VARCHAR(20),ProductName)+'</td>'+  
  --'<td class="Barcode" style="width: 10px">Barcode</td>'+''+  
  '<td class="SRP" style="text-align: right; width: 10px">'+cONVERT(VARCHAR(20),SRP)+'</td>'+  
  '<td class="GP" style="text-align: right; width: 10px">'+cONVERT(VARCHAR(20),GP)+'</td>'+  
  '<td class="UnitPrice" style="text-align: right; width: 10px">'+cONVERT(VARCHAR(20),UnitPrice)+'</td>'+  
  '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">'+cONVERT(VARCHAR(20),NetPrice)+'</td>' FROM #OrderItem WHERE RowNo=@i)  
  set @orderhtml+='</tr>'  
  SET @i=@i+1   
 END   
 DROP TABLE #OrderItem  
set @orderhtml+='</tbody>'  
set @orderhtml+=' <tbody></tbody>'  
set @orderhtml+='</table></fieldset>'  
set @orderhtml+='<fieldset>'  
set @orderhtml+='<table style="width: 100%;">'  
set @orderhtml+='<tbody>'  
set @orderhtml+='<tr>'  
                set @orderhtml+='<td style="width: 66%" colspan="2"><center><div id="TC">'+REPLACE((REPLACE(@TermsCondition,'<pre','<div')),'</pre>','</div>')+'</div></center></td>'  
                set @orderhtml+='<td style="width: 33%;">'  
                    set @orderhtml+='<table class="table tableCSS" style="width:80%;float:right">'  
                    set @orderhtml+='<tbody>'  
                        set @orderhtml+='<tr>'  
                                        set @orderhtml+='<td class="tdLabel">Total Qty</td>'  
                                        set @orderhtml+='<td id="totalQty" style="text-align: right;">'+convert(varchar(5),(select COUNT(1) from OrderItemMaster where OrderAutoId=@OrderAutoId))+'</td>'  
                        set @orderhtml+='</tr>'  
                        set @orderhtml+='<tr>'  
                                        set @orderhtml+='<td class="tdLabel">Sub Total</td>'  
                                        set @orderhtml+='<td style="text-align: right;"><span id="totalwoTax">'+@TotalAmount+'</span></td>'  
                        set @orderhtml+='</tr>'  
      set @orderhtml+='<tr>'  
                                        set @orderhtml+='<td class="tdLabel">Discount</td>'  
                                        set @orderhtml+='<td style="text-align: right;"><span id="disc">0.00</span></td>'  
      set @orderhtml+='</tr>'  
                        set @orderhtml+='<tr>'  
                                        set @orderhtml+='<td class="tdLabel">Tax</td>'  
                                        set @orderhtml+='<td style="text-align: right;"><span id="tax">0.00</span></td>'  
                        set @orderhtml+='</tr>'  
                        set @orderhtml+='<tr>'  
                                        set @orderhtml+='<td class="tdLabel">Shipping</td>'  
                                        set @orderhtml+='<td style="text-align: right;"><span id="shipCharges">0.00</span></td>'  
                       set @orderhtml+='</tr>'  
                       set @orderhtml+='<tr>'  
                                        set @orderhtml+='<td class="tdLabel">NJ ECig/Nicotine Tax</td>'  
                                        set @orderhtml+='<td style="text-align: right;"><span id="MLTax">0.00</span></td>'  
                       set @orderhtml+='</tr>'  
                       set @orderhtml+='<tr>'  
                                        set @orderhtml+='<td class="tdLabel">Weight Tax</td>'  
                                        set @orderhtml+='<td style="text-align: right;"><span id="WeightTax">0.00</span></td>'  
                       set @orderhtml+='</tr>'  
                       set @orderhtml+='<tr>'  
                                        set @orderhtml+='<td class="tdLabel">Adjustment</td>'  
                                        set @orderhtml+='<td style="text-align: right;"><span id="spAdjustment">0.00</span></td>'  
                      set @orderhtml+='</tr>'  
                      set @orderhtml+='<tr>'  
                                        set @orderhtml+='<td class="tdLabel">Grand Total</td>'  
                                        set @orderhtml+='<td style="text-align: right;"><span id="grandTotal">0.00</span></td>'  
                      set @orderhtml+='</tr>'  
                      set @orderhtml+='<tr>'  
                                        set @orderhtml+='<td class="tdLabel">Credit memo</td>'  
                                        set @orderhtml+='<td style="text-align: right;"><span id="spnCreditmemo">0.00</span></td>'  
                      set @orderhtml+='</tr>'  
                      set @orderhtml+='<tr>'  
                                         set @orderhtml+='<td class="tdLabel">Store Credit</td>'  
                                         set @orderhtml+='<td style="text-align: right;"><span id="spnStoreCredit">0.00</span></td>'  
                      set @orderhtml+='</tr>'  
                      set @orderhtml+='<tr>'  
                                         set @orderhtml+='<td class="tdLabel">Payable Amount</td>'  
                                         set @orderhtml+='<td style="text-align: right;"><span id="spnPayableAmount">0.00</span></td>'  
                       set @orderhtml+='</tr>'  
                  set @orderhtml+='</tbody>'  
               set @orderhtml+='</table>'  
           set @orderhtml+='</td>'  
          set @orderhtml+='</tr>'  
     set @orderhtml+='</tbody>'  
    set @orderhtml+='</table>'  
  set @orderhtml+='</fieldset>'  
 set @orderhtml+='</div>'  

 set @orderhtml+='</body>'  
 set @orderhtml+='</html>'  
 select @orderhtml  
END  
  
GO
