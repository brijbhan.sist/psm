  
ALTER Proc [dbo].[ProcApiTrackingReport]  
@Opcode int=null,   
@fromdate datetime=null,  
@todate datetime=null,
@isException bit out,    
@PageIndex INT=1,      
@PageSize INT=10,      
@exceptionMessage varchar(max) out  
as   
begin  
BEGIN TRY      
  Set @isException=0      
  Set @exceptionMessage='Success'      
  IF @Opcode=41      
  BEGIN
		SELECT ROW_NUMBER() OVER(ORDER BY AutoId  DESC                                                                                                                                           
		) AS RowNumber, * INTO #Results from(
		select AutoId,accessToken,appversion,UserName,deviceId,CONVERT(VARCHAR(20),
		[starttimestamp],22) AS starttimestamp,CONVERT(VARCHAR(20),[endtimestamp],22) 
		AS endtimestamp,CONVERT(VARCHAR(20),[creationdate],22) AS creationdate,
		MINUTE as DiffInMinute,SECOND as DiffInSecond
		from TimerequestTracker
		where (@fromdate is null or @fromdate = '' or @todate is null or @todate = '' or                                                                                        
    (CONVERT(date,creationdate)  between CONVERT(date,@fromdate) and CONVERT(date,@todate)))
		) as t
		order by t.AutoId desc


		SELECT COUNT(RowNumber) AS RecordCount, case when @PageSize=0 then COUNT(RowNumber) else @PageSize 
		end AS PageSize, @PageIndex AS PageIndex FROM #Results 

		  SELECT * FROM #Results                                             
  WHERE @PageSize=0 or  RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1 

 END  
 
 IF @OpCode=42      
	BEGIN      
		select (
		SELECT AutoId,ProductId, Convert(varchar,ProductId) +' '+ ProductName as PName  FROM ProductMaster      
		WHERE ProductStatus=1 ORDER BY  PName,REPLACE(ProductName,' ','') for json path
		) as PList, 
		(
		Select EM.AutoId,EM.[FirstName] + ' ' + Em.[LastName] AS SalesPersonName from EmployeeMaster EM  where EM.Status=1
		for json path 
		) as SalesPerson
		for json path 
	END   
 END TRY      
 BEGIN CATCH      
		Set @isException=1      
		Set @exceptionMessage=ERROR_MESSAGE()      
 END CATCH       
end  
GO
