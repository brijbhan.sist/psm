ALTER PROCEDURE [dbo].[ProcEmailServerMaster]                            
 @SendTo varchar(25)=null,  
 @EmailID varchar(50)=null,              
 @Server varchar(50)=null,                           
 @Port int=null,              
 @SSL bit=null,                
 --@RefID varchar(25)=null,              
 --@RefName varchar(25)=null, 
  @Password varchar(250)=null, 
  @ToEmailId varchar(250)=null,  
  @CCEmailId varchar(100)=null,
  @BCCEmailId varchar(100)=null,
  @Subject varchar(max)=null,
  @EmailBody varchar(max)=null,

 --@Status bit=null,              
 @opCode int=Null,      
 @CreatedBy int =null,
 @isException bit out,              
 @exceptionMessage varchar(100) out
AS              
BEGIN              
 BEGIN TRY          
        SET @isException=0            
  BEGIN TRANSACTION               
  IF @opCode=11              
  BEGIN               
   DECLARE @cnt int              
   SET @cnt=(SELECT COUNT(SendTo) from EmailServerMaster where SendTo=@SendTo)              
  IF @cnt=0              
  BEGIN                 
      insert into EmailServerMaster (SendTo,EmailId,[server],Pass,port,[ssl],CreatedBy,CreateDate) values(@SendTo,@EmailID,@Server,EncryptByPassPhrase('ESS',@Password),@Port,@SSL,@CreatedBy,getdate())              
      DECLARE @act varchar(200)              
      SET @act='New server setting has been added in email authentication setting for '+@SendTo              
   END              
  else              
  BEGIN              
  SET @isException=1              
  SET @exceptionMessage='Email server already configured for ('+@SendTo+')'               
   END              
 END              
  IF @opCode=21              
    BEGIN              
  update EmailServerMaster SET EmailId=@EmailID, [server]=@Server,Pass=EncryptByPassPhrase('ESS',@Password),UpdatedBy=@CreatedBy,UpdateDate=getDate(), port=@Port,[ssl]=@SSL where SendTo=@SendTo               
  SET @act='Server setting has been updated in email authentication master for '+@SendTo          
 END              
  IF @opCode=41              
    BEGIN              
        SELECT SendTo,Emailid as email,port,server,convert(varchar(50), DecryptByPassPhrase('ESS',Pass)) as Pass,ssl from EmailServerMaster              
   END              
                
  IF @opCode=42              
    BEGIN              
         SELECT SendTo,EmailId,port,server,DecryptByPassPhrase('ESS',Pass) as Pass,ssl from EmailServerMaster       
   END              
  IF @opCode=43              
    BEGIN              
         SELECT SendTo,EmailId,port,server,convert(varchar(50), DecryptByPassPhrase('ESS',Pass)) as Pass,ssl from EmailServerMaster    where SendTo='Developer'             
   END              
  IF @opCode=61              
    BEGIN              
         SELECT SendTo,EmailId,port,server,convert(varchar(50), DecryptByPassPhrase('ESS',Pass)) as Pass,ssl from EmailServerMaster where  ((@SendTo is Null or @SendTo ='') or ( SendTo=@SendTo ))                 
   END 
     IF @opCode=62       
    BEGIN      
	 DECLARE @FromEmailId varchar(100)=null
	 DECLARE @FromName varchar(100)=null
	 DECLARE @smtp_userName varchar(100)=null
	 DECLARE @SMTPServer varchar(50)=null


	SELECT @FromEmailId=FromEmail,@FromName=DisplayName,@smtp_userName=SMTP_USERNAME,@Password=SMTP_PASSWORD,
	@SMTPServer=HOST,@Port=SMTPPORT,@SSL=EnableSsl from EmailServerMaster_AWS

	 EXEC [Emailing_DB].[dbo].[ProcSubmitEmail] 
	 @Opcode=11,
	 @isException=0,
	 @exceptionMessage='',
	 @FromEmailId=@FromEmailId,
	 @smtp_userName=@smtp_userName,
	 @Port=@Port,
	 @SMTPServer=@SMTPServer,
	 @Password=@Password,
	 @SSL=@SSL,
	 @EmailBody=@EmailBody, 
	 @ToEmailId=@ToEmailId,
	 @CCEmailId=@CCEmailId,
	 @BCCEmailId=@BCCEmailId,
	 @Subject=@Subject,
	 @SourceApp = 'PSM',
	 @SubUrl = ''
   END 
 
   COMMIT TRAN                 
END TRY              
  BEGIN CATCH              
      ROLLBACK TRAN              
   SET @isException=1              
   SET @exceptionMessage= 'Oops! Something went wrong.Please try later.'             
  END CATCH;              
END; 
