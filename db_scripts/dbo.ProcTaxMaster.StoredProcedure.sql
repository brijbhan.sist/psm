ALTER PROCEDURE [dbo].[ProcTaxMaster]    
@OpCode int=Null,    
@TaxAutoId  int=Null,    
@TaxId VARCHAR(12)=NULL,    
@TaxName VARCHAR(50)=NULL,    
@Value varchar(250)=NULL,    
@Status int=null,    
@State int=null,    
@PrintLabel varchar(100)=NULL,    
@isException bit out,    
@exceptionMessage varchar(max) out    
AS    
BEGIN    
 BEGIN TRY    
   SET @isException=0    
   SET @exceptionMessage='success'    
    
  IF @OpCode=11    
  BEGIN   
  IF exists(SELECT TaxableType FROM TaxTypeMaster WHERE State = @State)    
     BEGIN    
		set @isException=1    
		set @exceptionMessage='Tax already added for this state.'    
     END 
	 ELSE     IF exists(SELECT TaxableType FROM TaxTypeMaster WHERE TaxableType = @TaxName)    
     BEGIN    
		set @isException=1    
		set @exceptionMessage='Tax already exists.'    
     END  
    ELSE    
     BEGIN TRY    
      BEGIN TRAN           
     INSERT INTO TaxTypeMaster(State,[TaxableType],Value,Status,[PrintLabel])    
     VALUES (@State,@TaxName,@Value,@Status,@PrintLabel)           
      COMMIT TRANSACTION    
     END TRY     
     BEGIN CATCH    
      ROLLBACK TRAN    
    SET @isException=1    
    SET @exceptionMessage='Oops! Something went wrong.Please try later'    
     END CATCH    
  END    
  ELSE IF @Opcode=21    
   BEGIN    
    IF exists(SELECT TaxableType FROM TaxTypeMaster WHERE TaxableType = @TaxName and AutoId != @TaxId)    
    BEGIN    
   SET @isException=1    
   SET @exceptionMessage='Tax already exists.'    
    END    
    ELSE    
    IF exists(SELECT TaxableType FROM TaxTypeMaster WHERE State = @State and AutoId != @TaxId)    
    BEGIN    
   SET @isException=1    
   SET @exceptionMessage='State already exists.'    
    END    
    ELSE       
    BEGIN    
   UPDATE TaxTypeMaster SET TaxableType = @TaxName, Value = @Value, Status = @Status,    
    State = @State,[PrintLabel]=@PrintLabel WHERE AutoId = @TaxId    
    END    
   END    
  ELSE IF @Opcode=31    
   BEGIN    
       DELETE FROM TaxTypeMaster WHERE AutoId = @TaxId    
   END    
  ELSE IF @OpCode=41    
   BEGIN    
  SELECT CM.AutoId,st.StateName, CM.TaxableType, CM.Value, SM.StatusType AS Status,PrintLabel FROM  TaxTypeMaster AS CM     
  inner join StatusMaster AS SM ON SM.AutoId=CM.Status and SM.Category is NULL     
  left join State AS st ON st.AutoId=CM.State     WHERE (@TaxId is null or @TaxId='' or cm.AutoId= @TaxId)    
  and (@TaxName is null or @TaxName='' or TaxableType like '%'+ @TaxName +'%')    
  and (@Status=2 or cm.Status=@Status)      
   END     
  ELSE IF @OpCode=42    
    BEGIN    
        SELECT AutoId, TaxableType, Value,Status,State,PrintLabel FROM TaxTypeMaster WHERE AutoId=@TaxId    
   END    
  ELSE IF @OpCode=43    
     BEGIN    
         SELECT AutoId, StateName FROM State     
     END    
 END TRY    
 BEGIN CATCH        
    SET @isException=1    
    SET @exceptionMessage='Oops! Something went wrong.Please try later.'    
 END CATCH    
END    
    
    
    
GO
