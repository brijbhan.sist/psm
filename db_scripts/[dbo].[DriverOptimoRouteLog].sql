USE [psmnj.a1whm.com]
GO

ALTER TABLE [dbo].[DriverOptimoRouteLog] DROP CONSTRAINT [DF__DriverOpt__Creat__636563D7]
GO

/****** Object:  Table [dbo].[DriverOptimoRouteLog]    Script Date: 07-04-2020 04:15:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DriverOptimoRouteLog]') AND type in (N'U'))
DROP TABLE [dbo].[DriverOptimoRouteLog]
GO

/****** Object:  Table [dbo].[DriverOptimoRouteLog]    Script Date: 07-04-2020 04:15:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DriverOptimoRouteLog](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[PlanningId] [int] NULL,
	[DriverNo] [int] NULL,
	[DriverAutoId] [int] NULL,
	[DriverPlanningAutoId] [int] NULL,
	[DriverStartTime] [varchar](10) NULL,
	[DraverEndTime] [varchar](10) NULL,
	[NoOfStop] [varchar](50) NULL,
	[CarAutoId] [int] NULL,
	[RouteDate] [datetime] NULL,
	[ManagerAutoid] [int] NULL,
	[CreateDate] [datetime] NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[DriverOptimoRouteLog] ADD  DEFAULT (getdate()) FOR [CreateDate]
GO


