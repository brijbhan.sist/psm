--alter table DriverLog_OrdDetails alter column CustomerName varchar(250)
ALTER procedure [dbo].[Proc_OptimoRoute]  
 @Opcode INT=NULL,    
 @OptimoOrdersDt OptimoOrdersDt readonly,  
 @SaveRuteDt SaveRuteDt readonly,
 @ShippingDt ShipAutoId readonly,
 @DriverLog DriverLog readonly,
 @CheckSecurity varchar(15)=NULL,
 @OrderAutoId INT=NULL, 
 @DrvLogAutoId int=null,
 @EmpAutoId  INT=NULL,
 @isException bit out,                                        
 @exceptionMessage varchar(max) out  
AS  
BEGIN  
     SET @isException=0                                        
     SET @exceptionMessage=''  
	IF @Opcode=40  
	BEGIN --SELECT FORMAT(SYSDATETIME(), N'hh:mm tt');
	BEGIN TRY  
		select OM.AutoId as OrderAutoId,OM.ShippingType as ShippingTypeAutoId, 
		(case when OrdShippingAddress!='' 
		then OrdShippingAddress else ([dbo].[ConvertFirstLetterinCapital](SA.[Address]+', '+SA.[City])+', '+Upper(S1.StateCode)+' - '+SA.[Zipcode]) end) As shipAdd,
    	(case when OrdShippingAddress!='' then OM.SA_Lat else SA.SA_Lat end) as SA_Lat,OM.CustomerAutoId,OM.DriverRemarks,
		Convert(varchar(10),left(CM.OptimotwFrom,5)) as OptimotwFrom
		, Convert(varchar(10),left(CM.OptimotwTo,5)) as OptimotwTo,
		(case when OrdShippingAddress!='' then OM.SA_Long else SA.SA_Long end) as SA_Long,OM.OrderNo,CM.CustomerName,
		EM.FirstName+' '+ISNULL(EM.LastName,'') as SalesPerson,CONVERT(VARCHAR(20),OM.[OrderDate],101) As OrderDate,
		OM.PayableAmount as GrandTotal from OrderMaster as OM  
		INNER JOIN [dbo].[ShippingAddress] As SA ON SA.[AutoId] = OM.[ShipAddrAutoId]       
		INNER JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]   
		INNER JOIN CustomerMaster AS CM ON OM.CustomerAutoId=CM.AutoId
		INNER JOIN EmployeeMaster AS EM ON CM.SalesPersonAutoId=EM.AutoId
		WHERE OM.AutoId IN (Select OrderAutoId FROM @OptimoOrdersDt)  AND SA.City!='' 
		
	    SELECT em.AutoId , FirstName  + ' ' +  LastName  as Name,(Select top 1 optimoStart_Lat FROM CompanyDetails) as StartLocation,
		convert(varchar(10),left(em.OptimotwFrom,5)) as StartTime,(Select top 1 optimoStart_Long FROM CompanyDetails) as EndLocation,
		convert(varchar(10),left(em.OptimotwTo,5)) as EndTime,ISNULL(ZM.CarId,0) as CarNo FROM  EmployeeMaster  as em
		left JOIN ZoneMaster as ZM on 
		em.AutoId=ZM.DriverId
		where em.Status=1 AND EmpType=5 

        select convert(varchar(10),left(TimeFrom,5)) as TimeFrom,Convert(varchar(10),left(TimeTo,5)) as TimeTo,
		FORMAT(CAST(TimeFrom AS datetime), N'hh:mm tt') as FromTime,FORMAT(CAST(TimeTo AS datetime), N'hh:mm tt') as ToTime
		from OptimoTimeWindow

		SELECT  CarAutoId,CarName  FROM  CarDetailsMaster order by CarName

		Select top 1 optimoStart_Lat,optimoStart_Long FROM CompanyDetails

	END TRY  
	BEGIN CATCH  
		SET @isException=1                                        
		SET @exceptionMessage=ERROR_MESSAGE()  
		END CATCH  
	END  
	IF @Opcode=41
	BEGIN
	   
	     IF EXISTS(SELECT * FROM [DriverLog_OrdDetails] WHERE convert(date,CreatedLogDate)=convert(date,getdate()) and OrderNo IN (SELECT dt.OrderNo FROM @SaveRuteDt as dt))
		 BEGIN
		     SET @isException=1                                        
		     SET @exceptionMessage='Duplicate driver' 
		 END
		 ELSE
		 BEGIN

		 BEGIN TRY
		  BEGIN tran t1
			 UPDATE OrderMaster SET ManagerAutoId=@EmpAutoId,Driver = dt.Driver,AssignDate=dt.ScheduleDate,Stoppage=dt.Stop,
			 [SA_Lat]=dt.Lat,[SA_Long]=dt.Long,[DeliveryFromTime]=convert(time(7),dt.DeliveryFrom),[DeliveryToTime]=convert(time(7),dt.DeliveryTo),
			 [ScheduleAt]=convert(time(7),dt.ScheduleAt),[DeliveredAddress]=dt.DeliveredAddress,
			 DriverRemarks=dt.Remark,DriverCarId=dt.CarId,status=4,CustDefaultFromTime=(Select OptimotwFrom FROM CustomerMaster c where c.AutoId=OM.CustomerAutoId),
			 CustDefaultToTime=(Select OptimotwTo FROM CustomerMaster as c where c.AutoId=OM.CustomerAutoId),
			 RootName=(Select Make+ ' '+model+' '+TAGNumber from CarDetailsMaster where CarAutoId=dt.CarId) FROM @SaveRuteDt as dt
			 INNER JOIN OrderMaster as OM
			 ON OM.OrderNo = dt.OrderNo 

			 INSERT INTO [dbo].[tbl_OrderLog] ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId])                                     
			 Select 5,@EmpAutoId,getdate(),(REPLACE((SELECT [ActionDesc] FROM [dbo].[tbl_ActionMaster] where [AutoId]=5), '[DriverName]',
			 (SELECT [FirstName] + ' ' + [LastName] AS DrvName FROM [dbo].[EmployeeMaster] WHERE [AutoId] = dt.Driver))),
			 (Select AutoId from OrderMaster where OrderNo=dt.OrderNo) FROM @SaveRuteDt as dt  

			 INSERT INTO DriverOptimoRouteLog (PlanningId,DriverNo,DriverAutoId,DriverPlanningAutoId,
             DriverStartTime,DraverEndTime,NoOfStop,CarAutoId,RouteDate,ManagerAutoid,CreateDate)
			 SELECT distinct DL.PlanningId,DL.DriverNo,DL.DriverAutoId,DL.DriverPlanningAutoId,
             DL.DriverStartTime,DL.DraverEndTime,DL.NoOfStop,DL.CarAutoId,DL.RouteDate,@EmpAutoId,GETDATE() FROM @DriverLog as DL

			 INSERT INTO DriverOptimoRouteMasterLog (PanningId,OrderAutoId) 
		     SELECT (SELECT top 1 DL.PlanningId FROM @DriverLog as DL),(Select AutoId from OrderMaster where OrderNo=dt.OrderNo) FROM @SaveRuteDt as dt


			select distinct * into #temp411 from @DriverLog
			select ROW_NUMBER() OVER (ORDER BY DriverNo asc) AS rno,* into #tmp41 from #temp411
			 
			declare @OptionCount int=(select COUNT(1) from #tmp41)
			declare @i int=1
			while (@i<=@OptionCount)
		begin
			 insert into [dbo].[Driver_Log]([DriverName],
			 DriverAutoId,[DriverId],OptimoFromTime,OptimoToTime,[ScheduleFromTime],
			 ScheduleToTime,[ScheduleDate],[CarId],[CarName],CreatedDate,[DriverPlanningName],PlanningId) 
			 select 
			 (em.[FirstName] + ' ' + em.[LastName]),
			 DL.DriverAutoId,em.EmpId,em.OptimotwFrom,
			 em.OptimotwTo,DL.DriverStartTime,DL.DraverEndTime,DL.RouteDate,
			 cdm.CarId,cdm.CarName,GETDATE(),(emp.[FirstName] + ' ' + emp.[LastName]),PlanningId
			  FROM #tmp41 as DL
			  inner join EmployeeMaster as em on em.AutoId=DL.DriverAutoId
			  inner join  [dbo].[CarDetailsMaster] as cdm on cdm.CarAutoId=DL.CarAutoId
			  left join EmployeeMaster as emp on emp.AutoId=DL.DriverPlanningAutoId
			  where rno=@i
			  set @DrvLogAutoId=SCOPE_IDENTITY()
			  
			  declare @PlanningId int=(select  PlanningId from Driver_Log where AutoId=@DrvLogAutoId)

			  insert into [dbo].[DriverLog_OrdDetails]([DrvLogAutoId],[OrderNo],[OrderDate],[CustomerId],[CustomerName],[CustOpenTime],[CustCloseTime],[OrderAmount],
			  [PayableAmount],[SetDeliveryFromTime],[SetDeliveryToTime],[ScheduleAt],[StopNo],[Address],[Lat],[Long],[DriverAutoId],PlanningId,SalesPerson) 
			  select @DrvLogAutoId,
			  dt.OrderNo,om.OrderDate,
			  cm.CustomerId,
			  cm.CustomerName,
			  cm.OptimotwFrom,
			  cm.OptimotwTo,
			  om.GrandTotal,om.PayableAmount
			  ,dt.DeliveryFrom,dt.DeliveryTo,
			  dt.ScheduleAt,dt.Stop,
			  (
				Select case when OrdShippingAddress!='' then OrdShippingAddress
				else SA.[Address]+', '+SA.[City]+',<br/> '+S1.StateName+', '+SA.[Zipcode] end
				FROM  [dbo].[ShippingAddress] As SA        
				INNER JOIN [dbo].[State] AS S1 ON S1.AutoId = SA.[State]  
				where SA.[AutoId] = OM.[ShipAddrAutoId]
			  )
			,dt.Lat,dt.Long,
			  dt.Driver,@PlanningId,
			  (select FirstName +' ' +ISNULL(LastName,'') from EmployeeMaster as emp where emp.AutoId=om.SalesPersonAutoId)
			  FROM @SaveRuteDt as dt 
			  inner join OrderMaster as om on om.OrderNo=dt.OrderNo
			  inner join CustomerMaster as cm on cm.AutoId=om.CustomerAutoId
			  where dt.Driver = (select DriverAutoId from Driver_Log as ff where ff.AutoId=@DrvLogAutoId)


		      set @i=@i+1;
		
		end
		     COMMIT tran t1
		 
		   END TRY
		   BEGIN CATCH
			  ROLLBACK tran t1
			  SET @isException=1                                        
			  SET @exceptionMessage=ERROR_MESSAGE() 
		   END CATCH
	   END
	END
	IF @Opcode=42
	BEGIN
	     SELECT [SecurityValue] as OptimoKey FROM [dbo].[TBL_MST_SecurityMaster] WHERE [SecurityValue]=@CheckSecurity and SecurityType=9 and typeEvent=1 
	END
	IF @Opcode=43
	BEGIN
        Select AutoId,ShippingType from ShippingType where AutoId IN (Select ShippingAutoId FROM @ShippingDt)
		Select AutoId,Template,TemplateUrl from TemplateMaster
	END
	IF @Opcode=44
	BEGIN
	   SELECT DateDifference,OptimoDriverLimit FROM CompanyDetails
	END
END