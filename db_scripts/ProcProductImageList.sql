ALTER PROCEDURE [dbo].[ProcProductImageList]                                                                                                                                                                           
@OpCode int=Null,     
@ProductAutoId  int=Null,                                                                                                                                                          
@ImageUrl varchar(250)=NULL,  
@ThumbnailImageUrl varchar(250)=NULL, 
@defaultImg VARCHAR(500)= NULL,
@ImageAutoId INT= NULL,
@FourImageUrl varchar(250)= NULL,
@CategoryAutoId INT= NULL,
@SubcategoryAutoId INT= NULL,
@ImageType INT= NULL,
@ProductId varchar(50)= NULL,
@ProductName varchar(250)= NULL,
@ProductStatus INT= NULL,
@OriginalImageUrl varchar(250)= NULL,

@PageIndex INT = 1,                                                                                                                  
@PageSize INT = 10,                                                                                                                                                     
@RecordCount INT =null, 
@isException bit out,                                                                                                                                                       
@exceptionMessage varchar(max) out                                                                                                                           
AS                                                                                                                                     
BEGIN                                                                                                                                             
 BEGIN TRY                                                                                                                                                       
		  SET @isException=0                                                                                                                                                                       
		  SET @exceptionMessage='Success'                                                                                                                                                                                
 IF @OpCode=41                                                                                                                                                                         
 BEGIN 
	SELECT ROW_NUMBER() over(order by PM.ProductId  desc) as RowNumber,
	PM.AutoId,PM.ProductId,PM.ProductName,ISNULL(PM.ImageUrl,'') as Image,CM.CategoryName AS Category,SCM.SubcategoryName AS Subcategory,
	ISNULL(PM.ThumbnailImageUrl,'') as ThumbnailImageUrl,ISNULL(ThirdImage,'') as ThirdImage
	into #Results from ProductMaster PM
	INNER JOIN CategoryMaster CM ON CM.AutoId=PM.CategoryAutoId
	INNER JOIN SubCategoryMaster SCM ON SCM.AutoId=PM.SubcategoryAutoId
	WHERE (@CategoryAutoId IS NULL OR @CategoryAutoId='' OR PM.[CategoryAutoId] = @CategoryAutoId)                                                                                                                                                                                                                                      
	AND (@SubcategoryAutoId IS NULL OR @SubcategoryAutoId='' OR PM.[SubcategoryAutoId] = @SubcategoryAutoId)                                                                                                                                    
	AND (@ProductId IS NULL OR @ProductId='' OR PM.[ProductId] LIKE '%'+ @ProductId +'%')                                                                   
	AND (@ProductName IS NULL OR @ProductName='' OR PM.ProductName LIKE '%' + @ProductName + '%') 
	--AND (@ImageType in (1,2,3) OR (@ImageType=0 and PM.ImageUrl like '%default%'))	
	AND (@ProductStatus=2 OR @ProductStatus is null or pm.ProductStatus=@ProductStatus)

	SELECT COUNT(RowNumber) AS RecordCount,case when @PageSize=0 then COUNT(*) else @PageSize end AS PageSize,@PageIndex as PageIndex FROM #Results  

	SELECT * FROM #Results                                    
    WHERE (@PageSize = 0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1))
                               
 END                                                                                                               
 IF @OpCode=21                                                                                                                                                                         
 BEGIN                                                                                                                                                                             
	 update ProductMaster set [ImageUrl]=@ImageUrl,[ThumbnailImageUrl]=@ThumbnailImageUrl,[ThirdImage]=@FourImageUrl,
	 OriginalImageUrl=@OriginalImageUrl
	 where AutoId=@ProductAutoId
	 set @ProductId=(select ProductId from ProductMaster where AutoId=@ProductAutoId)
     Exec [dbo].ProcProcProductImage_ALL @ProductId=@ProductId
 END   
  IF @OpCode=42                                                                                                                                                                     
 BEGIN                                                                                                                                                                             
	select PM.AutoId,PM.ProductId,PM.ProductName,CM.CategoryName AS Category,SCM.SubcategoryName AS Subcategory,
	PM.ImageUrl as ImageUrl from ProductMaster PM
	INNER JOIN CategoryMaster CM ON CM.AutoId=PM.CategoryAutoId
	INNER JOIN SubCategoryMaster SCM ON SCM.AutoId=PM.SubcategoryAutoId
	WHERE PM.AutoId=@ProductAutoId                         
 END   
  IF @OpCode=43                                                                                                                                                                     
 BEGIN                                                                                                                                                                             
	select PM.AutoId,PM.ProductId,PM.ProductName,PM.ImageUrl as ImageUrl,
	PM.ThumbnailImageUrl,ThirdImage,Replace(ImageUrl,'/Attachments/','') as ImgName,
	Replace(ThumbnailImageUrl,'/productThumbnailImage/','') as thumb1,
	Replace(ThirdImage,'/productThumbnailImage/','') as thumb2
	from ProductMaster PM order by AutoId desc
 END 
 END TRY                                                    
 BEGIN CATCH                                                                                                               
    SET @isException=1                                                                                                                                                
    SET @exceptionMessage='Oops! Something went wrong.Please try later.'--ERROR_MESSAGE()                                                                                        
 END CATCH                                                                                                                                                                         
END 
