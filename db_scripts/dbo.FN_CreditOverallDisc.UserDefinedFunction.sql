USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_CreditOverallDisc]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION  [dbo].[FN_CreditOverallDisc]
(
@CreditAutoId int
)
RETURNS decimal(18,2)
AS
BEGIN
	DECLARE @OverallDisc decimal(18,2)
	set @OverallDisc=isnull((select (Isnull(OverallDiscAmt,0)/case when Isnull(GrandTotal,0)=0 then 1 else  Isnull(GrandTotal,0) end)*100 
	from CreditMemoMaster where CreditAutoId=@CreditAutoId) ,0.00)
	RETURN @OverallDisc
END
GO
Alter table CreditMemoMaster drop column OverallDisc
Drop function FN_CreditOverallDisc
Alter table CreditMemoMaster Add OverallDisc AS ([dbo].[FN_CreditOverallDisc]([CreditAutoId]))