USE [psmnj.a1whm.com]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create FUNCTION  [dbo].[FN_DefaultContactPerson] 
(   
  @AutoId int   
)  
RETURNS varchar(100)  
AS  
BEGIN  
	DECLARE @ContactPerson varchar(100)
	set @ContactPerson=(SELECT top 1 ContactPerson FROM CustomerContactPerson WHERE CustomerAutoId=@AutoId AND ISDefault=1)
	RETURN @ContactPerson  
END 
--Alter table CustomerMaster Drop column ContactPersonName
--Alter table CustomerMaster add ContactPersonName As ([dbo].[FN_DefaultContactPerson](AutoId))