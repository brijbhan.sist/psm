ALTER Proc [dbo].[ProcDelete_Customer_Order]  
as   
BEGIN  
   
   BEGIN TRY
    BEGIN TRAN
				select   cmnj.* into #tmpCustomer from [psmnj.a1whm.com].dbo.CustomerMaster as cmnj
				inner join [psmny.a1whm.com].dbo.CustomerMaster as cmny on cmnj.CustomerName=cmny.CustomerName
				and cmnj.Status=0
				AND CMNJ.CustomerId='CST11309'

				select AutoId,OrderNo into #tmpOrder from [psmnj.a1whm.com].[dbo].[OrderMaster] where CustomerAutoId in (select t.AutoId from #tmpCustomer as t)
  
			  ------------------------------------------------Delete Credit Memo Details------------------------------------------------

  				select CreditAutoId into #tmpCreditMemo from [psmnj.a1whm.com].[dbo].[CreditMemoMaster] where CustomerAutoId in (select t.AutoId from #tmpCustomer as t)
				delete from [psmnj.a1whm.com].[dbo].[CreditMemoLog] where CreditAutoId in (select T.CreditAutoId from #tmpCreditMemo AS T)
				delete from [psmnj.a1whm.com].[dbo].[CreditItemMaster] where CreditAutoId in (select T.CreditAutoId from #tmpCreditMemo AS T)
				delete from [psmnj.a1whm.com].[dbo].[CreditMemoMaster] where CreditAutoId in (select T.CreditAutoId from #tmpCreditMemo AS T)

			  -------------------------------------------------Delete Payments Details-----------------------------------------------

  		
				select PaymentAutoId into #tmpCustPayDet from [psmnj.a1whm.com].[dbo].[CustomerPaymentDetails] where CustomerAutoId in (select t.AutoId from #tmpCustomer as t)
	
				delete from [psmnj.a1whm.com].[dbo].[PaymentCurrencyDetails] where PaymentAutoId in (select t.PaymentAutoId from #tmpCustPayDet as t)
				delete from [psmnj.a1whm.com].[dbo].[CustomerPaymentDetailsLog] where PaymentAutoId in (select t.PaymentAutoId from #tmpCustPayDet as t)
				delete from [psmnj.a1whm.com].[dbo].[PaymentOrderDetails] where PaymentAutoId in (select t.PaymentAutoId from #tmpCustPayDet as t)
				delete from [psmnj.a1whm.com].[dbo].[CustomerPaymentDetails] where PaymentAutoId in (select t.PaymentAutoId from #tmpCustPayDet as t)
				delete from [psmnj.a1whm.com].[dbo].SalesPaymentMaster where CustomerAutoId in (select t.AutoId from #tmpCustomer as t)

			  ------------------------------------------------Delete Order Details-------------------------------------------------
				delete from [psmnj.a1whm.com].[dbo].tbl_DayEndLogReport where OrderAutoId in (select T.AutoId from #tmpOrder AS T) 
				delete from [psmnj.a1whm.com].[dbo].[OrderItemMaster] where OrderAutoId in  (select T.AutoId from #tmpOrder AS T)  
				delete from [psmnj.a1whm.com].[dbo].[tbl_OrderLog] where OrderAutoId in  (select T.AutoId from #tmpOrder AS T) 
				delete from [psmnj.a1whm.com].[dbo].[DeliveredOrders] where OrderAutoId in  (select T.AutoId from #tmpOrder AS T) 
				delete from [psmnj.a1whm.com].[dbo].[Delivered_Order_Items] where OrderAutoId in  (select T.AutoId from #tmpOrder AS T)  
				delete from [psmnj.a1whm.com].[dbo].[Delete_Delivered_Order_Items_Backup] where OrderAutoId in  (select T.AutoId from #tmpOrder AS T) 
				delete from [psmnj.a1whm.com].[dbo].[Delete_DeliveredOrders_Backup] where OrderAutoId in  (select T.AutoId from #tmpOrder AS T) 
				delete from [psmnj.a1whm.com].[dbo].[DeleteOrderDetails_Backup] where OrderNo in (select OrderNo from #tmpOrder) 
				delete from [psmnj.a1whm.com].[dbo].[DrvLog] where OrderAutoId in (select AutoId from #tmpOrder) 
				delete from [psmnj.a1whm.com].[dbo].[GenPacking] where OrderAutoId in (select AutoId from #tmpOrder) 
				delete from [psmnj.a1whm.com].[dbo].[OrderItems_Original] where OrderAutoId in (select AutoId from #tmpOrder)
				delete from [psmnj.a1whm.com].[dbo].[Order_Original] where OrderNo in (select OrderNo from #tmpOrder) 
				delete from [psmnj.a1whm.com].[dbo].[AllocatedPackingDetails] where OrderAutId in (select AutoId from #tmpOrder)
				delete from [psmnj.a1whm.com].[dbo].[ManagerItemDelete] where OrderAutoId in (select AutoId from #tmpOrder)
				delete from [psmnj.a1whm.com].[dbo].[PaymentLog] where OrderAutoId in (select AutoId from #tmpOrder)  
				delete from [psmnj.a1whm.com].[dbo].[OrderMaster] where AutoId in (select AutoId from #tmpOrder) 
	 

				 ------------------------------------------App Order-------------------------------------------------------------
				 delete from [psmnj.a1whm.com].[dbo].[App_OrderItemMaster] where OrderAutoId in 
				 (select om.AutoId from [psmnj.a1whm.com].[dbo].[App_OrderMaster] as om where CustomerAutoId in (select t.AutoId from #tmpCustomer as t))
				 delete from [psmnj.a1whm.com].[dbo].[App_OrderMaster]  where CustomerAutoId in (select t.AutoId from #tmpCustomer as t)
	
				------------------------------------------------Customer--------------------------------------------------------
 
				delete from [psmnj.a1whm.com].[dbo].[CustomerBankDetails] where CustomerAutoId in (select t.AutoId from #tmpCustomer as t)
				delete from [psmnj.a1whm.com].[dbo].[CustomerCreditMaster] where CustomerAutoId in (select t.AutoId from #tmpCustomer as t)
				delete from [psmnj.a1whm.com].[dbo].[CustomerDocumentMaster] where CustomerAutoId in (select t.AutoId from #tmpCustomer as t)
				delete from [psmnj.a1whm.com].[dbo].[CustomerContactPerson] where CustomerAutoId in (select t.AutoId from #tmpCustomer as t)
				delete from [psmnj.a1whm.com].[dbo].[tbl_Custumor_StoreCreditLog] where CustomerAutoId in (select t.AutoId from #tmpCustomer as t) 
				delete from [psmnj.a1whm.com].[dbo].[RouteLog] where CustomerAutoId in (select t.AutoId from #tmpCustomer as t)  
	
				SELECT PriceLevelAutoId INTO #CPL FROM  [psmnj.a1whm.com].[dbo].[CustomerPriceLevel] where CustomerAutoId in (select t.AutoId from #tmpCustomer as t)
	
				SELECT TT.PriceLevelAutoId INTO #CPL1 FROM  #CPL AS TT
				INNER JOIN  [psmnj.a1whm.com].[dbo].[CustomerPriceLevel] AS CPL ON CPL.PriceLevelAutoId=TT.PriceLevelAutoId AND 
				CPL.CustomerAutoId NOT IN (select t.AutoId from #tmpCustomer as t)

				delete from [psmnj.a1whm.com].[dbo].[CustomerPriceLevel] where CustomerAutoId in (select t.AutoId from #tmpCustomer as t)

				DELETE FROM [psmnj.a1whm.com].[dbo].[ProductPricingInPriceLevel_log]  where PriceLevelAutoId IN (SELECT T.PriceLevelAutoId FROM #CPL AS T)
				AND PriceLevelAutoId NOT IN (SELECT T.PriceLevelAutoId FROM #CPL1 AS T) 

				delete from [psmnj.a1whm.com].[dbo].[ProductPricingInPriceLevel] where PriceLevelAutoId IN (SELECT T.PriceLevelAutoId FROM #CPL AS T)
				AND PriceLevelAutoId NOT IN (SELECT T.PriceLevelAutoId FROM #CPL1 AS T) 

				DELETE FROM [psmnj.a1whm.com].[dbo].PriceLevelMaster WHERE AutoId IN (SELECT T.PriceLevelAutoId FROM #CPL AS T)
				AND AutoId NOT IN (SELECT T.PriceLevelAutoId FROM #CPL1 AS T)

				select DraftAutoId into #tmpDraft from [psmnj.a1whm.com].[dbo].[DraftOrderMaster] where CustomerAutoId in  (select t.AutoId from #tmpCustomer as t) 
				delete from [psmnj.a1whm.com].[dbo].[DraftItemMaster] where DraftAutoId in (select DraftAutoId from #tmpDraft)
				delete from [psmnj.a1whm.com].[dbo].[DraftOrderMaster] where CustomerAutoId in  (select t.AutoId from #tmpCustomer as t) 
				delete from [psmnj.a1whm.com].[dbo].[Back_tbl_Custumor_StoreCreditLog] where CustomerAutoId in  (select t.AutoId from #tmpCustomer as t) 
	
				delete from [psmnj.a1whm.com].[dbo].[DriverLog_OrdDetails] where OrderNo in (select t.OrderNo from #tmpOrder as t) 
				delete from [psmnj.a1whm.com].[dbo].[DriverOptimoRouteMasterLog] where OrderAutoId in (select t.AutoId from #tmpOrder as t) 

				delete from [psmnj.a1whm.com].[dbo].[CustomerMaster] where AutoId in (select AutoId from #tmpCustomer)
		COMMIT TRAN
		END TRY
		BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE(),ERROR_LINE()
		END CATCH
END  