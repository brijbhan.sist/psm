USE [psmnj.a1whm.com]
GO

/****** Object:  Table [dbo].[ExtraItemLogMaster]    Script Date: 07-27-2021 02:32:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ExtraItemLogMaster](
	[AutoId] [int] IDENTITY(1,1) NOT NULL,
	[Barcode] [varchar](25) NULL,
	[DraftAutoId] [int] NULL,
	[EmpAutoId] [int] NULL,
	[LogDate] [datetime] NULL
) ON [PRIMARY]
GO


