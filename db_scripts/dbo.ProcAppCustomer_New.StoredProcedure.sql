ALTER ProcEDURE [dbo].[ProcAppCustomer_New]              
@Opcode INT=Null,                
@SalesPersonAutoId INT=NULL,         
@RouteAutoId INT=NULL,      
@timeStamp datetime=NULL,           
@isException BIT OUT,              
@exceptionMessage VARCHAR(max) OUT              
AS              
BEGIN              
 BEGIN TRY              
  SET @isException=0              
  SET @exceptionMessage='Success!!'              
  IF @Opcode = 41                 
  BEGIN  
    declare @EmpType int =(select EmpType from [EmployeeMaster] where autoid=@SalesPersonAutoId)
	if @EmpType=2
	begin

		select customerAutoid,isnull((do.AmtDue),0) as DueAmount,om.OrderNo as OrderNo,[dbo].ConvertUTCTimeStamp(om.OrderDate) as OrderDate,
		ShippingType
		into #DueOrderList from DeliveredOrders as do 
		inner join  OrderMaster as om on om.AutoId=do.OrderAutoId  and om.Status=11   
		inner join CustomerMaster as cm on cm.AutoId=om.CustomerAutoId and CM.[SalesPersonAutoId]=@SalesPersonAutoId   
		WHERE do.AmtDue > 0          

		select customerAutoid,SUM(DueAmount) as DueAmount into #TotaDueAmount from #DueOrderList Group by CustomerAutoId 

		SELECT  CM.AutoId as AI,[CustomerId] as CId,[CustomerName]as CN,[ContactPersonName] as CPN,CM.[Email] as E,CM.[AltEmail] as AE,CM.[Contact1] as C1,CM.[Contact2] as C2,MobileNo as MN,FaxNo AS FN,            
		TaxId as TId,[TermsDesc] as TMS,
		Convert(varchar(20),ISNULL((select do.DueAmount from #TotaDueAmount as do WHERE do.CustomerAutoId=cm.autoid),0)) as DBA,    
		Convert(varchar(20),isnull((SELECT ccm.CreditAmount FROM CustomerCreditMaster AS CCM WHERE CCM.CustomerAutoId=CM.AutoId ),0.00)) as SCA,     
		cm.[CustomerType] CT,cty.[CustomerType] as CTN, DefaultBillAdd as DBAds, DefaultShipAdd as  DSAds,CM.[SalesPersonAutoId] as SPI,
		isnull([PriceLevelAutoId],'') as PLI,isnull(PLM.[PriceLevelName],'') as PLN, CM.[Status] as S,SM.[StatusType] As ST,
		(case when (select AutoId from RouteLog where RouteAutoId=@RouteAutoId and CustomerAutoId=CM.AutoId) is not null then 1 else 0 end) as RS,
		ISNULL((
			select OrderNo as OrderNo,OrderDate,DueAmount,customerAutoid,st.ShippingType as ST
			from #DueOrderList as do
			inner join ShippingType as st on st.AutoId=do.ShippingType
			where CustomerAutoId=cm.AutoId for json path,INCLUDE_NULL_VALUES
		),'[]') as DOL,
		isnull((
			select ba.autoid, ba.customerAutoid, ba.address, (SELECT [StateName] FROM [State] as st where st.Autoid=ba.State) as state, 
			ba.city, ba.zipcode from [BillingAddress]  as ba           
			where ba.customerautoid=cm.AutoId for json path,INCLUDE_NULL_VALUES
		),'[]') as BAL,
		isnull((
			select sa.autoid, sa.customerAutoid, sa.address, (SELECT [StateName] FROM [State] as st where st.Autoid=sa.State) as state, 
			sa.city, sa.zipcode from [ShippingAddress] 
			as sa where sa.customerautoid=cm.AutoId for json path,INCLUDE_NULL_VALUES
		),'[]') as SAL,
		isnull((
			select ProductAutoId as PID, ppl.UnitAutoId as UT, ppl.CustomPrice as CP from [ProductPricingInPriceLevel] as ppl          
			inner join [CustomerPriceLevel] as cpl on ppl.pricelevelautoid=cpl.pricelevelautoid          
			where customprice is not null and cpl.CustomerAutoId=cm.AutoId for json path,INCLUDE_NULL_VALUES
		),'[]') as PPL,
		isnull((
			select  CustomerAutoId,ProductAutoId,UnitTypeAutoId as UnitType,RemainQty as RemainingQty from OrderMaster  as om    
			inner join OrderItemMaster as oim on oim.OrderAutoId=om.AutoId 
			where RemainQty>0 and om.AutoId in     
			(select max(omx.AutoId) from OrderMaster as omx where omx.CustomerAutoId=cm.AutoId and omx.Status!=8 ) 
			for json path,INCLUDE_NULL_VALUES
		),'[]') as RQL
	         
		FROM [dbo].[CustomerMaster] AS CM                    
		INNER JOIN [dbo].[EmployeeMaster] AS EM ON EM.AutoId = @SalesPersonAutoId             
		LEFT JOIN [dbo].[CustomerPriceLevel] AS CPL ON CPL.[CustomerAutoId] = CM.[AutoId]              
		LEFT JOIN [dbo].[PriceLevelMaster] AS PLM ON PLM.[AutoId] = CPL.[PriceLevelAutoId]              
		INNER JOIN [dbo].[StatusMaster] AS SM ON SM.[AutoId] = CM.[Status] AND SM.[Category] IS NULL              
		LEFT JOIN [dbo].[CustomerTerms] as CT on [TermsId]=[Terms]            
		INNER JOIN [dbo].[CustomerType] as CTY on CTY.AutoId=CM.[CustomerType]           
		WHERE CM.[SalesPersonAutoId]=@SalesPersonAutoId and Cm.Status=1  ORDER BY  CustomerName ASC
		for json path, INCLUDE_NULL_VALUES
	end
	else if @EmpType=5
	begin
		SELECT distinct CustomerAutoId into #tempCust5 FROM [dbo].[OrderMaster] 
		where (Driver = @SalesPersonAutoId) 
		and ((Status in (4,5) and AssignDate<=GETDATE()) or (Status in (6,7) and convert(date, DelDate)=convert(date,GETDATE())))

		select customerAutoid,isnull((do.AmtDue),0) as DueAmount,om.OrderNo as OrderNo,[dbo].ConvertUTCTimeStamp(om.OrderDate) as OrderDate
		into #DueOrderList5 from DeliveredOrders as do 
		inner join  OrderMaster as om on om.AutoId=do.OrderAutoId and om.Status=11  and om.CustomerAutoId in (SELECT CustomerAutoId from #tempCust5)
		WHERE do.AmtDue > 0    
	
		select customerAutoid,SUM(DueAmount) as DueAmount into #TotaDueAmount5 from #DueOrderList5 Group by CustomerAutoId 

		SELECT  CM.AutoId as AI,[CustomerId] as CId,[CustomerName] AS CN,[ContactPersonName] AS CPN,CM.[Email] as E,CM.[AltEmail] AE,
		CM.[Contact1] as C1,CM.[Contact2]as C2,MobileNo as MN,FaxNo as FN,            
		TaxId as TId,[TermsDesc] as TMS,
		Convert(varchar(20),ISNULL((select do.DueAmount from #TotaDueAmount5 as do WHERE do.CustomerAutoId=cm.autoid),0)) as DBA,    
		Convert(varchar(20),isnull((SELECT ccm.CreditAmount FROM CustomerCreditMaster AS CCM WHERE CCM.CustomerAutoId=CM.AutoId ),0.00)) as SCA,     
		cm.[CustomerType] as CT,cty.[CustomerType] as CTN, DefaultBillAdd as DBAds, DefaultShipAdd as DSAds,CM.[SalesPersonAutoId] as SPI,
		isnull([PriceLevelAutoId],'')as PLI,isnull(PLM.[PriceLevelName],'')As PLN, CM.[Status]as S,SM.[StatusType]as ST,
		0 As RS,--Route Status
		ISNULL((
				select OrderNo as OrderNo,OrderDate,DueAmount,customerAutoid
				from #DueOrderList5 where CustomerAutoId=cm.AutoId for json path,INCLUDE_NULL_VALUES
			),'[]'
		) as DOL,
		'[]' as BAL,
		'[]' as SAL,
		'[]' as PPL,
		'[]' as RQL	         
		FROM [dbo].[CustomerMaster] AS CM                    
		INNER JOIN [dbo].[EmployeeMaster] AS EM ON EM.AutoId = @SalesPersonAutoId and Cm.AutoId in (SELECT CustomerAutoId from #tempCust5) and cm.Status=1
		LEFT JOIN [dbo].[CustomerPriceLevel] AS CPL ON CPL.[CustomerAutoId] = CM.[AutoId]              
		LEFT JOIN [dbo].[PriceLevelMaster] AS PLM ON PLM.[AutoId] = CPL.[PriceLevelAutoId]              
		INNER JOIN [dbo].[StatusMaster] AS SM ON SM.[AutoId] = CM.[Status] AND SM.[Category] IS NULL              
		LEFT JOIN [dbo].[CustomerTerms] as CT on [TermsId]=[Terms]            
		INNER JOIN [dbo].[CustomerType] as CTY on CTY.AutoId=CM.[CustomerType] 
		for json path, INCLUDE_NULL_VALUES
	end
  END              
 END TRY              
 BEGIN CATCH              
  SET @isException=1              
  SET @exceptionMessage=ERROR_MESSAGE()              
 END CATCH              
END 
