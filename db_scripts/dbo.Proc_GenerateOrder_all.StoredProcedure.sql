USE [psmct.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[Proc_GenerateOrder_all]    Script Date: 5/1/2021 4:52:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER Proc [dbo].[Proc_GenerateOrder_all]            
@VenderAutoId int=null,            
@POAutoId int=null,            
@Remark varchar(500)=null,            
@PONo varchar(50)=null            
as             
begin            
 Declare @OrderNo varchar(50) ,@OrderAutoId int,@PriceLevelAutoId int            
 declare @CustomerAutoId int=null, @SalesPersonAutoId int =null,@CurrentLocationId int =null,@Location varchar(50)=null            
 SET @Location=(select Replace(DB_Name(),'.a1whm.com','') )           
 SET @CurrentLocationId=(select AutoId from LocationMaster where Location=@Location )                       
 declare @VenderLocation int = null        
 Select @VenderLocation=LocationAutoId,@CustomerAutoId=CustomerAutoId,@SalesPersonAutoId=SalesPersonAutoId from         
 [dbo].[fn_GetCustomerAndSalesPerson](@VenderAutoId,@CurrentLocationId) 
        
  declare @SQL1 nvarchar(max)=N'INSERT INTO '+case         
  when @VenderLocation=1 then '[psmnj.a1whm.com].[dbo].[OrderMaster]'         
  when @VenderLocation=2 then '[psmct.a1whm.com].[dbo].[OrderMaster]'          
  when @VenderLocation=3 then '[psmnjm.a1whm.com].[dbo].[OrderMaster]'          
  when @VenderLocation=4 then '[psmpa.a1whm.com].[dbo].[OrderMaster]'          
  when @VenderLocation=5 then '[psmnpa.a1whm.com].[dbo].[OrderMaster]'                    
  when @VenderLocation=7 then '[psm.a1whm.com].[dbo].[OrderMaster]' 
  when @VenderLocation=9 then '[psmwpa.a1whm.com].[dbo].[OrderMaster]'
  when @VenderLocation=10 then '[psmny.a1whm.com].[dbo].[OrderMaster]' 
  else '' end +'([OrderNo],OrderType,[OrderDate],[DeliveryDate],[CustomerAutoId]        
  ,[Terms],[BillAddrAutoId],[ShipAddrAutoId],[SalesPersonAutoId],[OverallDiscAmt],[ShippingCharges],[Status],[ShippingType],        
  TaxType,TaxValue,OrderRemarks ,referenceOrderNumber,mlTaxPer,IsTaxApply,RouteAutoId,RouteStatus,DeviceID,appVersion,latLong,AppOrderdate,Weigth_OZTax)'        
          
  SET @SQL1+=N'select (SELECT '+case         
  when @VenderLocation=1 then '[psmnj.a1whm.com].[dbo].[SequenceCodeGenerator](''OrderNumber'')'         
  when @VenderLocation=2 then '[psmct.a1whm.com].[dbo].[SequenceCodeGenerator](''OrderNumber'')'          
  when @VenderLocation=3 then '[psmnjm.a1whm.com].[dbo].[SequenceCodeGenerator](''OrderNumber'')'         
  when @VenderLocation=4 then '[psmpa.a1whm.com].[dbo].[SequenceCodeGenerator](''OrderNumber'')'          
  when @VenderLocation=5 then '[psmnpa.a1whm.com].[dbo].[SequenceCodeGenerator](''OrderNumber'')'                   
  when @VenderLocation=7 then '[psm.a1whm.com].[dbo].[SequenceCodeGenerator](''OrderNumber'')'
  when @VenderLocation=9 then '[psmwpa.a1whm.com].[dbo].[SequenceCodeGenerator](''OrderNumber'')'
  when @VenderLocation=10 then '[psmny.a1whm.com].[dbo].[SequenceCodeGenerator](''OrderNumber'')' else '' end +'),5,GETDATE(),GETDATE()        
  ,CustomerAutoId,Terms,BillAddrAutoId,ShipAddrAutoId,SalesPersonAutoId,0,0,1,1,TaxType,isnull(TaxValue,0.00),@Remark,@PONo,ISNULL(TaxValue,0.00)        
  ,IsTaxApply,0,0,0,0,0,null,Weigth_Tax from [dbo].[fn_GetCustomerAndSalesPerson](@VenderAutoId,@CurrentLocationId)'        
  
  SET @SQL1+=N' set @OrderAutoId = (SELECT SCOPE_IDENTITY())'        
         
  Execute Sp_executesql @SQL1,        
  N'@VenderAutoId int,@CurrentLocationId int,@Remark varchar(max),@PONo varchar(max),@OrderAutoId int OUTPUT',         
  @VenderAutoId,@CurrentLocationId,@Remark,@PONo,@OrderAutoId output          
        
  declare @SQL8 nvarchar(max)=N'INSERT INTO '+case         
  when @VenderLocation=1 then '[psmnj.a1whm.com].[dbo].[OrderItemMaster]'         
  when @VenderLocation=2 then '[psmct.a1whm.com].[dbo].[OrderItemMaster]'          
  when @VenderLocation=3 then '[psmnjm.a1whm.com].[dbo].[OrderItemMaster]'          
  when @VenderLocation=4 then '[psmpa.a1whm.com].[dbo].[OrderItemMaster]'          
  when @VenderLocation=5 then '[psmnpa.a1whm.com].[dbo].[OrderItemMaster]'                    
  when @VenderLocation=7 then '[psm.a1whm.com].[dbo].[OrderItemMaster]'
  when @VenderLocation=9 then '[psmwpa.a1whm.com].[dbo].[OrderItemMaster]' 
  when @VenderLocation=10 then '[psmny.a1whm.com].[dbo].[OrderItemMaster]' else '' end +' ([OrderAutoId], [ProductAutoId],      
  [UnitTypeAutoId], [QtyPerUnit], [UnitPrice], [RequiredQty], [SRP], [GP], [Tax],  [IsExchange],UnitMLQty,isFreeItem,Weight_Oz,
  Original_UnitType,OM_MinPrice,OM_CostPrice,BasePrice)                                  
  select t.OrderAutoId,t.ProductAutoId,t.UnitType,t.Qty,      
  (case when t.CustomPrice >= t.CostPrice then t.CustomPrice else t.CostPrice end),      
  t.ReqQty,t.SRP,t.GP,t.tax,t.IsExchange,t.MlQty,t.IsFree,t.WeightOz,t.OrgUnitType,t.CostPrice,t.CostPrice,t.Price from (SELECT @OrderAutoId as OrderAutoId,              
  nj.AutoId as ProductAutoId, pd.UnitType as UnitType,pd.Qty as Qty, pd.CostPrice as CustomPrice,  
  pd.CostPrice as CostPrice ,                                 
  pp.Qty as ReqQty,nj.[P_SRP] as  SRP,               
  CONVERT(DECIMAL(10,2),((nj.[P_SRP]-(pd.[Price]/(CASE WHEN pd.[Qty]=0 then 1 else pd.[Qty] end )))/        
  (CASE WHEN ISNULL(nj.[P_SRP],0)=0 then 1 else nj.[P_SRP] END)) * 100) AS GP              
  ,0 as tax,0 as IsExchange,(case when isnull(nj.IsApply_ML,0)=0 then 0 else isnull(nj.MLQty,0) end) as MlQty,        
  0 as IsFree,(case when isnull(nj.IsApply_Oz,0)=0 then 0 else isnull(nj.WeightOz,0) end) as WeightOz,pd.UnitType as OrgUnitType,pd.Price      
  from PurchaseProductsMaster pp              
  inner join ProductMaster pm on pp.ProductAutoId=pm.autoid       
  inner join PackingDetails pdx on pdx.ProductAutoId=pm.AutoId and pp.unit=pdx.unittype              
  inner join '+case   
  when @VenderLocation=1 then ' [psmnj.a1whm.com].[dbo].[ProductMaster]'         
  when @VenderLocation=2 then '[psmct.a1whm.com].[dbo].[ProductMaster]'          
  when @VenderLocation=3 then '[psmnjm.a1whm.com].[dbo].[ProductMaster]'          
  when @VenderLocation=4 then '[psmpa.a1whm.com].[dbo].[ProductMaster]'          
  when @VenderLocation=5 then '[psmnpa.a1whm.com].[dbo].[ProductMaster]'              
  when @VenderLocation=7 then '[psm.a1whm.com].[dbo].[ProductMaster]' 
  when @VenderLocation=9 then '[psmwpa.a1whm.com].[dbo].[ProductMaster]'  
  when @VenderLocation=10 then '[psmny.a1whm.com].[dbo].[ProductMaster]' else '' end +'  nj on nj.ProductId=pm.ProductId                
  inner join '+case         
  when @VenderLocation=1 then ' [psmnj.a1whm.com].[dbo].[PackingDetails]'         
  when @VenderLocation=2 then '[psmct.a1whm.com].[dbo].[PackingDetails]'          
  when @VenderLocation=3 then '[psmnjm.a1whm.com].[dbo].[PackingDetails]'          
  when @VenderLocation=4 then '[psmpa.a1whm.com].[dbo].[PackingDetails]'          
  when @VenderLocation=5 then '[psmnpa.a1whm.com].[dbo].[PackingDetails]'                
  when @VenderLocation=7 then '[psm.a1whm.com].[dbo].[PackingDetails]'  
  when @VenderLocation=9 then '[psmwpa.a1whm.com].[dbo].[PackingDetails]'  
  when @VenderLocation=10 then '[psmny.a1whm.com].[dbo].[PackingDetails]' else '' end +'  pd on pd.ProductAutoId=nj.AutoId and pp.unit=pd.unittype              
  where pp.POAutoId=@POAutoId ) as t'    
                
  Execute Sp_executesql @SQL8,        
  N'@VenderAutoId int,@CurrentLocationId int,@Remark varchar(max),@PONo varchar(max),@OrderAutoId int,@POAutoId int',         
  @VenderAutoId,@CurrentLocationId,@Remark,@PONo,@OrderAutoId,@POAutoId 
  
  declare @SQL7 nvarchar(max)=N'exec '+ case         
  when @VenderLocation=1 then '[psmnj.a1whm.com].[dbo].[ProcUpdatePOStatus_All]'         
  when @VenderLocation=2 then '[psmct.a1whm.com].[dbo].[ProcUpdatePOStatus_All]'          
  when @VenderLocation=3 then '[psmnjm.a1whm.com].[dbo].[ProcUpdatePOStatus_All]'         
  when @VenderLocation=4 then '[psmpa.a1whm.com].[dbo].[ProcUpdatePOStatus_All]'         
  when @VenderLocation=5 then '[psmnpa.a1whm.com].[dbo].[ProcUpdatePOStatus_All]'                
  when @VenderLocation=7 then '[psm.a1whm.com].[dbo].[ProcUpdatePOStatus_All]'    
  when @VenderLocation=9 then '[psmwpa.a1whm.com].[dbo].[ProcUpdatePOStatus_All]'      
  when @VenderLocation=10 then '[psmny.a1whm.com].[dbo].[ProcUpdatePOStatus_All]'     
  else '' end+' @CustomerId=@CustomerAutoId,@OrderAutoId=@OrderAutoId,@Status=@Status'         
                 
  Execute Sp_executesql @SQL7,        
  N'@OrderAutoId int,@CustomerAutoId int,@Status int',         
  @OrderAutoId,@CustomerAutoId,1   
          
  declare @SQL2 nvarchar(max)=N'Set @PriceLevelAutoId=(SELECT [PriceLevelAutoId] FROM '+case        
  when @VenderLocation=1 then '[psmnj.a1whm.com].[dbo].[CustomerPriceLevel]'        
  when @VenderLocation=2 then '[psmct.a1whm.com].[dbo].[CustomerPriceLevel]'        
  when @VenderLocation=3 then '[psmnjm.a1whm.com].[dbo].[CustomerPriceLevel]'        
  when @VenderLocation=4 then '[psmpa.a1whm.com].[dbo].[CustomerPriceLevel]'        
  when @VenderLocation=5 then '[psmnpa.a1whm.com].[dbo].[CustomerPriceLevel]'               
  when @VenderLocation=7 then '[psm.a1whm.com].[dbo].[CustomerPriceLevel]'  
  when @VenderLocation=9 then '[psmwpa.a1whm.com].[dbo].[CustomerPriceLevel]'  
  when @VenderLocation=10 then '[psmny.a1whm.com].[dbo].[CustomerPriceLevel]' else '' end+' WHERE [CustomerAutoId] = @CustomerAutoId)'                
        
  Execute Sp_executesql @SQL2,        
  N'@CustomerAutoId int,@PriceLevelAutoId int OUTPUT',         
  @CustomerAutoId,@PriceLevelAutoId out          
          
  declare @SQL3 nvarchar(max)=N'exec '+ case         
  when @VenderLocation=1 then '[psmnj.a1whm.com].[dbo].[UPDTAE_PRICELEVEL]'         
  when @VenderLocation=2 then '[psmct.a1whm.com].[dbo].[UPDTAE_PRICELEVEL]'          
  when @VenderLocation=3 then '[psmnjm.a1whm.com].[dbo].[UPDTAE_PRICELEVEL]'         
  when @VenderLocation=4 then '[psmpa.a1whm.com].[dbo].[UPDTAE_PRICELEVEL]'         
  when @VenderLocation=5 then '[psmnpa.a1whm.com].[dbo].[UPDTAE_PRICELEVEL]'               
  when @VenderLocation=7 then '[psm.a1whm.com].[dbo].[UPDTAE_PRICELEVEL]'     
  when @VenderLocation=9 then '[psmwpa.a1whm.com].[dbo].[UPDTAE_PRICELEVEL]'      
  when @VenderLocation=10 then '[psmny.a1whm.com].[dbo].[UPDTAE_PRICELEVEL]'     
  else '' end+' @OrderAutoId=@OrderAutoId, @PriceLevelAutoId=@PriceLevelAutoId'         
                 
  Execute Sp_executesql @SQL3,        
  N'@OrderAutoId int,@PriceLevelAutoId int',         
  @OrderAutoId,@PriceLevelAutoId        
                             
  declare @SQL4 nvarchar(max)= N'INSERT INTO '+ case         
 when @VenderLocation=1 then '[psmnj.a1whm.com].[dbo].[tbl_OrderLog]'         
  when @VenderLocation=2 then '[psmct.a1whm.com].[dbo].[tbl_OrderLog]'          
  when @VenderLocation=3 then '[psmnjm.a1whm.com].[dbo].[tbl_OrderLog]'          
  when @VenderLocation=4 then '[psmpa.a1whm.com].[dbo].[tbl_OrderLog]'          
  when @VenderLocation=5 then '[psmnpa.a1whm.com].[dbo].[tbl_OrderLog]'                  
  when @VenderLocation=7 then '[psm.a1whm.com].[dbo].[tbl_OrderLog]'  
  when @VenderLocation=9 then '[psmwpa.a1whm.com].[dbo].[tbl_OrderLog]'   
  when @VenderLocation=10 then '[psmny.a1whm.com].[dbo].[tbl_OrderLog]'else '' end+'         
  ([ActionTaken],[EmpAutoId],[ActionDate],[Remarks],[OrderAutoId]) 
  VALUES(1,'+convert(varchar(25),@SalesPersonAutoId)+',getdate(),''Order No.'' +(select OrderNo from 
  '+ case         
  when @VenderLocation=1 then '[psmnj.a1whm.com].[dbo].[OrderMaster]'         
  when @VenderLocation=2 then '[psmct.a1whm.com].[dbo].[OrderMaster]'          
  when @VenderLocation=3 then '[psmnjm.a1whm.com].[dbo].[OrderMaster]'          
  when @VenderLocation=4 then '[psmpa.a1whm.com].[dbo].[OrderMaster]'          
  when @VenderLocation=5 then '[psmnpa.a1whm.com].[dbo].[OrderMaster]'                  
  when @VenderLocation=7 then '[psm.a1whm.com].[dbo].[OrderMaster]'  
  when @VenderLocation=9 then '[psmwpa.a1whm.com].[dbo].[OrderMaster]'   
  when @VenderLocation=10 then '[psmny.a1whm.com].[dbo].[OrderMaster]'else '' end+'

   where AutoId='+convert(varchar(25),@OrderAutoId)+'),'+convert(varchar(25),@OrderAutoId)+')'  
  Execute Sp_executesql @SQL4  
   

  declare @SQL5 nvarchar(max)=N'Update '+case         
  when @VenderLocation=1 then '[psmnj.a1whm.com].[dbo].[CustomerMaster]'         
  when @VenderLocation=2 then '[psmct.a1whm.com].[dbo].[CustomerMaster]'          
  when @VenderLocation=3 then '[psmnjm.a1whm.com].[dbo].[CustomerMaster]'          
  when @VenderLocation=4 then '[psmpa.a1whm.com].[dbo].[CustomerMaster]'          
  when @VenderLocation=5 then '[psmnpa.a1whm.com].[dbo].[CustomerMaster]'                  
  when @VenderLocation=7 then '[psm.a1whm.com].[dbo].[CustomerMaster]'  
  when @VenderLocation=8 then '[psmwpa.a1whm.com].[dbo].[CustomerMaster]'  
  when @VenderLocation=9 then '[psmny.a1whm.com].[dbo].[CustomerMaster]' else '' end+' set LastOrderDate = GETDATE() where AutoId =@CustomerAutoId'             
          
  Execute Sp_executesql @SQL5,        
  N'@CustomerAutoId int',         
  @CustomerAutoId        
         
  declare @SQL6 nvarchar(max)=N'UPDATE '+case when @VenderLocation=1 then '[psmnj.a1whm.com].[dbo].[SequenceCodeGeneratorMaster]'         
  when @VenderLocation=2 then '[psmct.a1whm.com].[dbo].[SequenceCodeGeneratorMaster]'          
  when @VenderLocation=3 then '[psmnjm.a1whm.com].[dbo].[SequenceCodeGeneratorMaster]'          
  when @VenderLocation=4 then '[psmpa.a1whm.com].[dbo].[SequenceCodeGeneratorMaster]'          
  when @VenderLocation=5 then '[psmnpa.a1whm.com].[dbo].[SequenceCodeGeneratorMaster]'             
  when @VenderLocation=7 then '[psm.a1whm.com].[dbo].[SequenceCodeGeneratorMaster]'  
  when @VenderLocation=9 then '[psmwpa.a1whm.com].[dbo].[SequenceCodeGeneratorMaster]'  
  when @VenderLocation=10 then '[psmny.a1whm.com].[dbo].[SequenceCodeGeneratorMaster]' else '' end+' SET currentSequence = currentSequence + 1 WHERE SequenceCode=''OrderNumber'''        
  print @sql6        
  Execute Sp_executesql @SQL6
   
END 