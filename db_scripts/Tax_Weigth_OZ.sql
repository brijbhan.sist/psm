USE [psmnj.a1whm.com]
GO

/****** Object:  Table [dbo].[Tax_Weigth_OZ]    Script Date: 12/19/2019 1:58:34 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Tax_Weigth_OZ](
	[ID] [varchar](50) NULL,
	[Value] [decimal](18, 2) NULL,
	[PrintLabel] [varchar](100) NULL
) ON [PRIMARY]
GO


