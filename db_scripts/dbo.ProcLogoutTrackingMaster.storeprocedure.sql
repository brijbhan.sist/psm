USE [psmnj.a1whm.com]
GO
/****** Object:  StoredProcedure [dbo].[ProcOrderMaster]    Script Date: 07/13/2020 22:56:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[ProcLogoutTrackingMaster]                                                                                                                                                    
 @Opcode INT=NULL, 
 @DateFrom datetime=null,
 @DateTo datetime=null,
  @UserName varchar(50)=null,
 @PageIndex INT = 1,     
 @PageSize INT = 10,                                                                                                                            
 @RecordCount INT =null,                                                                                                                                                          
 @isException bit out,                                           
 @exceptionMessage varchar(max) out                                                                                      
AS                                                                                                                                                    
BEGIN                                                                                                                               
  BEGIN TRY                                                                                                                                            
   Set @isException=0                                                                                                                           
   Set @exceptionMessage='Success'                                                                                                         
   IF @Opcode=41
   BEGIN
    SELECT DISTINCT ROW_NUMBER() OVER(ORDER BY userName desc) AS RowNumber, * INTO #Results from                                                                    
    (   
	select Autoid,userName,accessToken,appversion,deviceId,format(Logouttimestamp,'MM/dd/yyyy hh:mm:s tt') as Logouttimestamp ,CreatedDatetime as tms,format(CreatedDatetime,'MM/dd/yyyy hh:mm:s tt') as CreatedDatetime from Tbl_LogoutTrackingMaster
	where 
	(@DateFrom is null or @DateFrom = '' or @DateTo is null or @DateTo = '' or (Convert(date,CreatedDatetime) between @DateFrom and @DateTo))                                                                                                                                           
	and (@UserName is null or @UserName ='' or userName like'%'+ @UserName +'%')   
	)as t order by userName   
	 SELECT CASE WHEN ISNULL(@PageSize,0)=0 then 0 else COUNT(*)  end as RecordCount,@PageSize as PageSize,@PageIndex as PageIndex FROM #Results                                                                    
    SELECT * FROM #Results                                                                   
    WHERE ISNULL(@PageSize,0)=0 or (RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)                          
	order by tms desc   
	END  
  END TRY                                                                      
  BEGIN CATCH                               
  Set @isException=1                                          
  Set @exceptionMessage='Oops! Something went wrong.Please try later.'                                                                                                                                                    
  END CATCH                                                                                       
END