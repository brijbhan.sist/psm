USE [psmnj.a1whm.com]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Delivered_TotalDelQty]    Script Date: 11/20/2019 5:05:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION  [dbo].[FN_Delivered_TotalDelQty]
(
	 @AutoId int
)
RETURNS int
AS
BEGIN
	DECLARE @TotalDelQty  int
	SET @TotalDelQty=(SELECT (
		TotalPieces-
		(FreshReturnQty*isnull(QtyPerUnit_Fresh,0))-
		(DamageReturnQty*isnull(QtyPerUnit_Damage,0))
	) FROM Delivered_Order_Items WHERE AutoId=@AutoId)	
	RETURN @TotalDelQty
END
 
GO
