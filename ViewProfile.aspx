﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="ViewProfile.aspx.cs" Inherits="ViewProfile" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .imagePreview {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
         <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">View Profile</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Settings</a></li>
                         <li class="breadcrumb-item">View Profile</li>

                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
          <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
            <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
            id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false"> Action</button>
            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" onclick="location.href='/Admin/mydashboard.aspx'" class="dropdown-item">Go to Dashboard</button>                
            <input type="hidden" runat="server" id="hdEmpType" />
            </div>
          </div>
        </div>
    </div>

    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-8">
                                        <div class="row form-group">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Employee Code</label>
                                                <input type="text" readonly id="EmpId" class="form-control border-primary input-sm" />
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Profile Name</label>
                                                <input type="text" readonly id="ProfileName" class="form-control border-primary input-sm" />
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Employee Type</label>
                                               <input type="text" readonly id="EmpType" class="form-control border-primary input-sm" />
                                            </div>

                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">First Name</label>
                                                <input type="text" readonly id="fname" class="form-control border-primary input-sm" />
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Last Name</label>
                                                <input type="text" readonly id="lname" class="form-control border-primary input-sm" />
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Address</label>
                                                <input type="text" readonly id="Adress" class="form-control border-primary input-sm" />
                                            </div>
                                        </div>
                                        <div class="row form-group">

                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">State</label>
                                                <input type="text" readonly id="State" class="form-control border-primary input-sm" />
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">City</label>
                                                <input type="text" readonly id="lcity" class="form-control border-primary input-sm" />
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Zip Code</label>
                                                <input type="text" readonly id="lzipcode" class="form-control border-primary input-sm" />
                                            </div>
                                        </div>
                                        <div class="row form-group">

                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Email</label>
                                                <input type="text" readonly id="lemail" class="form-control border-primary input-sm" />
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Contact</label>
                                                <input type="text" readonly id="lcontact" class="form-control border-primary input-sm" />
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">User Name</label>
                                                <input type="text" readonly id="lusername" class="form-control border-primary input-sm" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-4">
                                        <img src="/images/default_pic.png" style="display:none" class="imagePreview" id="imgPreview" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="JS/ViewProfile.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>

</asp:Content>

