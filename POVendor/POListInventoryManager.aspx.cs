﻿using DllPOInventoryManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class POVendor_POListInventoryManager : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod(EnableSession = true)]
    public static string BindStatus()
    {
        PL_PurchaseOrder pobj = new PL_PurchaseOrder();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                BL_PurchaseOrder.bindStatus(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string BindVendor()
    {
        PL_PurchaseOrder pobj = new PL_PurchaseOrder();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                BL_PurchaseOrder.bindVendor(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string getPOList(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_PurchaseOrder pobj = new PL_PurchaseOrder();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                if (jdv["PoNo"] != null && jdv["PoNo"] != "")
                {
                    pobj.OrderNo = jdv["PoNo"];
                }
                if (jdv["VenderAutoId"] != null && jdv["VenderAutoId"] != "0")
                {
                    pobj.VenderAutoId = Convert.ToInt32(jdv["VenderAutoId"]);
                }
                if (jdv["FromDate"] != null && jdv["FromDate"] != "")
                {
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != null && jdv["ToDate"] != "")
                {
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                }
                pobj.OrderStatus = Convert.ToInt32(jdv["Status"]);
                pobj.PageIndex = jdv["pageIndex"];
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                BL_PurchaseOrder.GetPOList(pobj);
                if (!pobj.isException)
                {
                    
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string BindRecStatus()
    {
        PL_PurchaseOrder pobj = new PL_PurchaseOrder();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                BL_PurchaseOrder.BindRecStatus(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
}