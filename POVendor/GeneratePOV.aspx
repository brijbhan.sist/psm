﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="GeneratePOV.aspx.cs" Inherits="POVendor_GeneratePOV" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Generate PO</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Manage PO</a></li>
                        <li class="breadcrumb-item"><a href="#">By Vendor</a>
                        </li>
                        <li class="breadcrumb-item active">Generate PO
                        </li>
                    </ol>
                </div>
            </div>
        </div>
         <%if (Session["EmpTypeNo"].ToString() == "11") { %>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" onclick=" location.href='/POVendor/PoListReceiver.aspx'" class="dropdown-item" animation="pulse" id="Button1" runat="server">
                        PO List</button>
                    <button type="button" onclick=" location.href='/POVendor/PODraftList.aspx'" class="dropdown-item" animation="pulse" id="Button2" runat="server">
                       PO Draft List</button>
                </div>
            </div>
        </div>
         <% } %>
         <%if (Session["EmpTypeNo"].ToString() == "9") { %>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop2" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop2">
                    <button type="button" onclick=" location.href='/POVendor/POListInventoryManager.aspx'" class="dropdown-item" animation="pulse" id="Button3" runat="server">
                        PO List</button>
                    <button type="button" onclick=" location.href='/POVendor/SubmitDraftList.aspx'" class="dropdown-item" animation="pulse" id="Button4" runat="server">
                        Draft List</button>
                </div>
            </div>
        </div>
         <% } %>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">PO Details</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">PO Draft No. </label>
                                            <input type="text" id="txtPoSequenceNo" class="form-control border-primary input-sm req" placeholder="PO No." readonly="readonly" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Generate PO Date </label>
                                            <input type="text" id="txtGenerateDate" disabled="disabled" class="form-control border-primary input-sm req date" placeholder="Generate Date" />

                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Status</label>
                                            <input id="ddlStatus" disabled="disabled" value="Draft" class="form-control border-primary input-sm" />
                                        </div>
                                    </div>
                                   <%-- <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Vendor<span class="required"> *</span> </label>
                                            <select id="ddlPoVendor" class="form-control border-primary input-sm selectvalidate">
                                                <option value="0">-Select Vendor-</option>
                                            </select>
                                        </div>
                                    </div>--%>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <section id="drag-area1">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-content collapse show">
                        <div class="card-body" id="panelProduct">
                            <div class="row">
                                <label class="col-sm-1 control-label">Barcode</label>
                                <input type="hidden" id="hfDraftAutoId" />
                                <div class="col-sm-3 form-group">
                                    <input type="text" class="form-control input-sm border-primary" autocomplete="off" id="txtBarcode" runat="server" onfocus="this.select()" onkeypress="return isNumberKey(event)" onchange="AddItemByBarcode()" placeholder="Enter Barcode here" />
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <label class="control-label">
                                        Product <span class="required">*</span>
                                    </label>
                                    <div class="form-group">
                                        <select class="form-control input-sm selectvalidate border-primary" onchange="bindProductDetails()" id="ddlProduct" runat="server" style="width: 100% !important">
                                            <option value="0">-Select-</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-2 col-md-3">
                                    <label class="control-label">
                                        Unit Type <span class="required">*</span>
                                    </label>
                                    <div class="form-group">
                                        <select class="form-control input-sm Areq border-primary" onchange="dllCalculateTotalQty()" id="ddlUnitType" runat="server">
                                            <option value="0">-Select-</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <label class="control-label" style="white-space: nowrap">
                                        Quantity <span class="required">*</span>

                                    </label>
                                    <div class="form-group">
                                        <input type="text" class="form-control input-sm Areq border-primary" onchange="calculateTotalQty()" id="txtReqQty" value="1" maxlength="5" runat="server" onkeypress="return isNumberKey(event)" />
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <label class="control-label" style="white-space: nowrap">
                                        Total Pieces

                                    </label>
                                    <div class="form-group">
                                        <input type="text" disabled class="form-control input-sm border-primary" id="txtTotalPieces" runat="server" />
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="pull-left">
                                        <button type="button" onclick="AddProductList()" class="btn btn-md btn-success buttonAnimation pull-right round box-shadow-1 btn-sm" id="btnAdd" style="margin-top: 22px;">&nbsp;&nbsp;&nbsp;Add&nbsp;&nbsp;&nbsp;</button>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="drag-area2">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-content collapse show">

                        <div class="card-body" id="panelPoItemList">
                            <style>
                                .table th, .table td {
                                    padding: 0.75rem 0.5rem !important;
                                }
                            </style>
                            <div class="table-responsive">
                                <table id="tblProductDetail" class="table table-striped table-bordered">
                                    <thead class="bg-blue white">
                                        <tr>
                                            <td class="Action text-center wth2" title="Delete">Action</td>
                                            <td class="ProId text-center wth5">Product ID</td>
                                            <td class="ProName">Product Name</td>
                                            <td class="VendorName wth10">Vendor Name</td>
                                            <td class="UnitType  text-center wth7">Unit</td>
                                            <td class="ReqQty  text-center wth4">Quantity</td>
                                            <td class="TtlPcs  text-center wth4">Total Pcs</td>  
                                            <td class="CostPrice center wth4" style="text-align:right">Unit <br /> Cost Price</td>
                                            <td class="TotalCostPrice center wth4" style="text-align:right">Total <br />Cost Price</td>
                                         
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                         <tr style="font-weight: bold;">
                                                <td colspan="8">Total</td>
                                                <td id="TotalCostPrice" style="text-align:right;">0.00</td>
                                            </tr>
                                    </tfoot>
                                </table>
                                <h5 class="well text-center" id="emptyTable">No Product Selected.</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-content collapse show">

                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Remark </label>
                                        <textarea class="form-control input-sm border-primary" rows="2" id="txtRemark" onblur="UpdateRemrks()"></textarea>
                                    </div>
                                </div>

                                <div class="col-md-6" id="PanelRemarkLog" style="display:none;">
                                    <div class="form-group">
                                        <label class="control-label">Remark Logs</label>
                                        <div class="table-responsive">
                                            <table id="tlbRemark" class="table table-striped table-bordered">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="Rdate text-center" style="width: 10%">Date</td>
                                                        <td class="Rby  text-center" style="width: 10%">Remark By</td>
                                                        <td class="Remark">Remark</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section id="drag-area3">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-content collapse show">
                        <div class="row card-footer">
                            <div class="col-md-12 text-right">
                                <div class="btn-group mr-1">
                                    <button type="button" id="btnAproved" class="btn btn-success buttonAnimation pull-right round box-shadow-1 btn-sm animated undefined" style="display: none;">Approved</button>
                                </div>
                                <div class="btn-group mr-1">
                                    <button type="button" id="btnCancel" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm animated undefined" style="display: none;">Cancel</button>
                                </div>
                                 <%if (Session["EmpTypeNo"].ToString() == "11")
                                            {

                                    %>
                                <div class="btn-group mr-1">
                                    <button type="button" id="btnSaveDraftPO" onclick="SavePODraft()" class="btn btn-success buttonAnimation pull-right round box-shadow-1 btn-sm animated undefined">Forward to Manager</button>
                                </div>
                                 <%
                                            }
                                     %>
                                <%if (Session["EmpTypeNo"].ToString() == "9")
                                            {

                                    %>
                                <div class="btn-group mr-1">
                                        <button type="button" id="btnGeneratePO" onclick="GeneratePO()" class="btn btn-success buttonAnimation pull-right round box-shadow-1 btn-sm animated undefined">Generate PO</button>
                                    </div>
                                 <div class="btn-group mr-1">
                                        <button type="button" id="btnRevertDraftPO" onclick="RevertPODraft()" class="btn btn-primary buttonAnimation pull-right round box-shadow-1 btn-sm animated undefined">Revert PO Draft</button>
                                    </div>
                                 <%
                                            }
                                     %>
                              
                                <div class="btn-group mr-1">
                                    <button type="button" id="btnReset" onclick="Reset()" class="btn btn-google buttonAnimation pull-right round box-shadow-1 btn-sm animated undefined">Reset</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <audio class="audios" id="yes_audio" controls preload="none" style="display: none">
        <source src="/Audio/NOExists.mp3" type="audio/mpeg" />
    </audio>
    <audio class="audios" id="No_audio" controls preload="none" style="display: none">
        <source src="/Audio/WrongProduct.mp3" type="audio/mpeg" />
    </audio>
<%--    <script>
        localStorage.setItem('EmpType', Session["EmpTypeNo"].ToString());
    </script>--%>
    <script>
        var EmpTypeSession = '<%= Session["EmpTypeNo"].ToString() %>';
        if (EmpTypeSession == '11') {
            $("#tblProductDetail tr").find('.CostPrice').hide();
            $("#tblProductDetail tr").find('.TotalCostPrice').hide();
            $("#tblProductDetail tbody").find('.CostPrice').hide();
            $("#tblProductDetail tbody").find('.TotalCostPrice').hide();
            $("#tblProductDetail tfoot").hide();
        }
</script>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="JS/GeneratePOV.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
</asp:Content>


