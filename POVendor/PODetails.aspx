﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="PODetails.aspx.cs" Inherits="POVendor_PODetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">PO Details</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Manage PO</a></li>
                        <li class="breadcrumb-item"><a href="#">By Vendor</a>
                        </li>
                        <li class="breadcrumb-item active"> PO Details
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                     <%if (Session["EmpTypeNo"].ToString() == "11") { %>
                    <button type="button" onclick=" location.href='/POVendor/PoListReceiver.aspx'" class="dropdown-item" animation="pulse" id="Button1" runat="server">
                        PO List</button>
                      <% }%>
                      <%if (Session["EmpTypeNo"].ToString() == "9") { %>
                    <button type="button" onclick=" location.href='/POVendor/POListInventoryManager.aspx'" class="dropdown-item" animation="pulse" id="Button2" runat="server">
                        PO List</button>
                      <% }%>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">PO Details</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">PO No. </label>
                                            <input type="text" id="txtPoSequenceNo" disabled class="form-control border-primary input-sm req" placeholder="PO No." />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Generate PO Date </label>
                                            <input type="text" id="txtGenerateDate" disabled="disabled" class="form-control border-primary input-sm req date" placeholder="Generate Date" />

                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Vendor</label>
                                            <input type="text" id="txtVendor" disabled="disabled" class="form-control border-primary input-sm req date" placeholder="Vendor" />

                                        </div>
                                    </div>
                                     <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Status </label>
                                            <input id="ddlStatus" disabled="disabled" class="form-control border-primary input-sm"/>
                                                
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                     <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Bill No.</label>
                                            <input type="text" id="txtBillNo" disabled="disabled" class="form-control border-primary input-sm req date" placeholder="Bill No." />

                                        </div>
                                    </div>
                                     <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Bill Date</label>
                                            <input type="text" id="txtBillDate" disabled="disabled" class="form-control border-primary input-sm req date" placeholder="Bill Date" />

                                        </div>
                                    </div>
                                      <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Created By</label>
                                            <input type="text" id="txtCreatedBy" disabled="disabled" class="form-control border-primary input-sm req date" placeholder="Created By" />

                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Remark </label>
                                            <textarea disabled="disabled" class="form-control input-sm border-primary" rows="2" id="txtRemark"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <section id="drag-area2">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-content collapse show">
                        <div class="card-body" id="panelPoItemList">
                            <style>
                                .table th, .table td {
                                    padding: 0.75rem 1rem;
                                }
                            </style>
                            <div class="table-responsive">
                                <table id="tblProductDetail" class="table table-striped table-bordered">
                                    <thead class="bg-blue white">
                                        <tr>
                                            <td class="ProductId  text-center" style="width: 10%">Product ID</td>
                                            <td class="ProductName">Product Name</td>
                                            <td class="UnitType  text-center" style="width: 20%">Unit</td>
                                            <td class="ReqQty  text-center" style="width: 4%">Quantity</td>
                                            <td class="TtlPcs  text-center" style="width: 4%">Total Pieces</td>
                                          <%--  <td class="RecivedQty  text-center" style="width: 5%;text-align:center !important">Received Quantity</td>--%>
                                            <td class="RecivedPiece  text-center" style="width: 4%">Received Pieces</td>
                                            <td class="Remain  text-center" style="width: 4%">Remain Pieces</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                                <%--<h5 class="well text-center" id="emptyTable">No Product Selected.</h5>--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="drag-area3">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="col-md-12 text-right">
                                <div class="btn-group mr-1">
                                
                                    <button type="button" id="btnNewReceive" onclick="NewReceive()" class="btn btn-success buttonAnimation pull-right round box-shadow-1 btn-sm animated undefined">New Receive</button>
                                </div>
                                  <%if (Session["EmpTypeNo"].ToString() == "9") { %>
                                <div class="btn-group mr-1">
                                    <button type="button" id="btnRecievePo" onclick="return RecievePOStock();" class="btn btn-facebook buttonAnimation round box-shadow-1 btn-sm pulse">Close PO</button>
                                  </div>
                                 <div class="btn-group mr-1">
                                 <button type="button" id="btnCancel" class="btn btn-danger buttonAnimation round box-shadow-1 btn-sm pulse">Cancel PO</button>
                                  </div>
                                     <% } %>
                              <%--  <div class="btn-group mr-1">
                                    <button type="button" id="btnSaveAsDraft" onclick="ConfirmForwordPO(2)" class="btn btn-success buttonAnimation pull-right round box-shadow-1 btn-sm animated undefined" style="display:none;">Submit</button>
                                </div>--%>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
      <%if (Session["EmpTypeNo"].ToString() == "11") { %>
      <section id="drag-area4">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                     <div class="card-header">
                            <h4 class="card-title">Receive Stock List</h4>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse" id="LoadReceiveList"><i class="ft-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    <div class="card-content collapse">
                        <div class="card-body">
                             <div class="row form-group">
                                    <div class="col-md-12">


                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblPOList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="Action text-center">Action</td>
                                                        <td class="RecNo text-center">Receive No</td>
                                                        <td class="RecDate">Receive Date</td>
                                                        <td class="RecBy text-center">Receive By</td>
                                                        <td class="Status text-center">Status</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
      <% } %>
          <%if (Session["EmpTypeNo"].ToString() == "9") { %>
      <section id="drag-area5">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                     <div class="card-header">
                            <h4 class="card-title">Receive Stock List</h4>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse" id="LoadReceiveListInvM"><i class="ft-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    <div class="card-content collapse">
                        <div class="card-body">
                             <div class="row form-group">
                                    <div class="col-md-12">


                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblPOListInvM">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="Action text-center">Action</td>
                                                        <td class="RecNo text-center">Receive No</td>
                                                        <td class="RecDate">Receive Date</td>
                                                        <td class="RecBy text-center">Receive By</td>
                                                        <td class="Status text-center">Receive Status</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable1" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
      <% } %>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="JS/PODetails.js"></sc' + 'ript>');
    </script>
</asp:Content>

