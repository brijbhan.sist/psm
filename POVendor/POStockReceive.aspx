﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="POStockReceive.aspx.cs" Inherits="Purchase_POStockReceive" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
        #tblProductDetail tr tbody .RecivedQty{text-align:center !important}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">PO Receive Stock</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Manage PO</a></li>
                        <li class="breadcrumb-item"><a href="#">By Vendor</a>
                        </li>
                        <li class="breadcrumb-item active">PO Receive Stock
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                     <%if (Session["EmpTypeNo"].ToString() == "9") { %>
                    <button type="button" onclick=" location.href='/POVendor/POListInventoryManager.aspx'" class="dropdown-item" animation="pulse" id="Button3" runat="server">
                        PO List</button>
                     <button type="button" onclick=" location.href='/POVendor/POStockListInventoryManager.aspx'" class="dropdown-item" animation="pulse" id="Button4" runat="server">
                        Receive Stock List</button>
                    <input type="hidden" id="hdnEmpAutoId" value="Manager"/>
                       <% } %>
                     <%if (Session["EmpTypeNo"].ToString() == "11") { %>
                    <button type="button" onclick=" location.href='/POVendor/ReceiveStockList.aspx'" class="dropdown-item" animation="pulse" id="Button2" runat="server">
                        Receive Stock List</button>
                     <button type="button" onclick=" location.href='/POVendor/PoListReceiver.aspx'" class="dropdown-item" animation="pulse" id="Button1" runat="server">
                        PO List</button>
                    <% }%>

                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-5">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">PO Details</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                         <label class="control-label">PO No. </label>
                                        <div class="input-group">
                                               <input type="hidden" id="hfRecAutoId" />
                                           
                                            <input type="text" id="txtPoSequenceNo" disabled class="form-control border-primary input-sm req" placeholder="PO No." />
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    <i class="la la-eye" id="ViewPOId" onclick="RedirectOnPODetails()"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Generate PO Date </label>
                                            <input type="text" id="txtGenerateDate" disabled="disabled" class="form-control border-primary input-sm req date" placeholder="Generate Date" />

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Vendor</label>
                                            <input type="text" id="txtVendor" disabled="disabled" class="form-control border-primary input-sm req date" placeholder="Vendor" />

                                        </div>
                                    </div>
                                     <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">PO Status </label>
                                            <input id="ddlStatus" disabled="disabled" class="form-control border-primary input-sm"/>
                                                
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                     <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="control-label">PO Remark </label>
                                            <textarea disabled="disabled" class="form-control input-sm border-primary" rows="2" id="txtRemark"></textarea>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
          <%--  </div>
              <div class="row">--%>
                <div class="col-md-7">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Receive Details</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Receive No. </label>
                                             <input type="text" id="ReceiveNo" disabled class="form-control border-primary input-sm" placeholder="PO No." />
                                        </div>
                                    </div>
                                      <div class="col-md-5 col-sm-5">
                                        <div class="form-group">
                                            <label class="control-label">Receive Date </label><span class="required">*</span>
                                            <input type="text" id="txtRecDate"  class="form-control border-primary input-sm req date" placeholder="Generate Date" />

                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Receive Status</label>
                                            <input id="ddlRecStatus" disabled="disabled" class="form-control border-primary input-sm" value="Draft"/>

                                        </div>
                                    </div>
                                   
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Bill No.</label>
                                            <input id="txtBillNo" class="form-control border-primary input-sm"  maxlength="40" />

                                        </div>
                                    </div>
                                     <div class="col-md-5 col-sm-5">
                                        <div class="form-group">
                                            <label class="control-label">Bill Date </label>
                                            <input type="text" id="txtBillDate"  class="form-control border-primary input-sm date" placeholder="Bill Date" />

                                        </div>
                                    </div>
                                     <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Receive By</label>
                                            <input id="txtRecBy" class="form-control border-primary input-sm" disabled="disabled"/>

                                        </div>
                                    </div>
                                </div>
                                 <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Remarks <span class="required"> *</span> </label>
                                            <textarea class="form-control input-sm border-primary" rows="2" id="txtReceiveRemark"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <section id="drag-area2">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-content collapse show">
                        <div class="card-body" id="panelPoItemList">
                            <style>
                                .table th, .table td {
                                    padding: 0.75rem 0.5rem !important;
                                    /*width:100%;*/
                                }
                            </style>
                            <div class="table-responsive"  style="width:100%;">
                                <table id="tblProductDetail" class="table table-striped table-bordered">
                                    <thead class="bg-blue white">
                                        <tr>
                                           <%-- <td class="ProductId text-center wth2"></td>
                                            <td class="ProductName"></td>
                                            <td class="UnitType text-center wth7"></td>
                                            <td class="ReqQty  text-center wth4"></td>--%>
                                            <td class="TtlPcs  text-center wth4" colspan="5"></td>
                                            <td class="RecUnitType text-center wth7" colspan="3">Received</td>
                                          <%--  <td class="RecivedQty text-center wth7" style="text-align:center !important;">Received</td>
                                            <td class="RecivedPiece  text-center wth4">Received</td>--%>
                                            <td class="OtherReceivedPieces text-center wth2" colspan="2">Pieces</td>
                                         <%--   <td class="RemainQty text-center wth2"></td>--%>
                                        </tr>
                                         <tr>
                                            <td class="ProductId text-center wth2">Product ID</td>
                                            <td class="ProductName">Product Name</td>
                                            <td class="UnitType text-center wth7">Unit</td>
                                            <td class="ReqQty  text-center wth4">Quantity</td>
                                            <td class="TtlPcs  text-center wth4">Total Pcs</td>
                                            <td class="RecUnitType text-center wth7"> Unit</td>
                                            <td class="RecivedQty text-center wth7" style="text-align:center !important;"> Qty</td>
                                            <td class="RecivedPiece  text-center wth4"> Pcs</td>
                                            <td class="OtherReceivedPieces text-center wth2">Other Rec</td>
                                            <td class="RemainQty text-center wth2">Remain</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                                <%--<h5 class="well text-center" id="emptyTable">No Product Selected.</h5>--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="drag-area3">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-content collapse show">
                        <div class="row card-body">
                            <div class="col-md-12 text-right">
                                <div class="btn-group mr-1">
                                    <button type="button" id="btnNewRec" onclick="NewRecieve()" class="btn btn-flickr buttonAnimation round box-shadow-1 btn-sm animated undefined" style="display:none">New Receive</button>
                                </div>
                                <div class="btn-group mr-1">
                                    <button type="button" id="btnGeneratePO" onclick="ReceiveByStatus(1)" class="btn btn-facebook buttonAnimation pull-right round box-shadow-1 btn-sm animated undefined" style="display:none;">Save as Draft</button>
                                </div>
                                <div class="btn-group mr-1">
                                    <button type="button" id="btnSaveAsDraft" onclick="ConfirmForwordPO(2)" class="btn btn-success buttonAnimation pull-right round box-shadow-1 btn-sm animated undefined" style="display:none;">Submit</button>
                                </div>
                                <%--<div class="btn-group mr-1">
                                    <button type="button" id="btnUpdateDraft" onclick="ReceiveByStatus(3)" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm animated undefined">Cancel PO</button>
                                </div>--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="JS/POStockReceive.js?v=<%=date%>"></sc' + 'ript>');
    </script>
</asp:Content>

