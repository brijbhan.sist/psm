﻿using DLLPOVendorMaster;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class POVendor_GeneratePOV : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static string BindDropDown()
    {
        try
        {
            PL_POVendorMaster pobj = new PL_POVendorMaster();
            BL_POVendorMaster.BindDropDown(pobj);
            return "true";
        }
        catch (Exception ex)
        {
            return "false";
        }

    }
}