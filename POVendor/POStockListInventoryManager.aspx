﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="POStockListInventoryManager.aspx.cs" Inherits="POVendor_POStockListInventoryManager" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Receive Stock List</h3>
              <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage PO</a></li>
                        <li class="breadcrumb-item active">By Vendor</li>
                        <li class="breadcrumb-item active">Receive Stock List</li>
                    </ol>
                </div>
            </div>
        </div> 
         <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">                  
                    <button type="button" onclick=" location.href='/POVendor/GeneratePOV.aspx'" class="dropdown-item" animation="pulse" id="Button1" runat="server">
                            Generate PO</button>
                </div>
            </div>
        </div>           
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 form-group">
                                        <input type="text" class="form-control input-sm border-primary" placeholder="PO No." id="txtPoNo" onfocus="this.select()" />
                                    </div>
                                     <div class="col-md-3 form-group">
                                        <input type="text" class="form-control input-sm border-primary" placeholder="Receive No." id="txtRecPoNo" onfocus="this.select()" />
                                    </div>
                                   <div class="col-md-3 form-group">
                                        <select class="form-control input-sm border-primary" id="ddlPoVendor" runat="server" style="width: 100%">
                                            <option value="0">All Vendors</option>
                                        </select>
                                    </div>
                                     <div class="col-md-3 form-group">
                                        <select class="form-control input-sm border-primary" id="ddlStatus" runat="server" style="width: 100%">
                                            <option value="0">All Status</option>
                                        </select>
                                    </div>
                                </div>
                                 <div class="row">
                                    <div class="col-md-3 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control input-sm border-primary date" placeholder="Generate From Date" id="txtPoFromDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control input-sm border-primary date" placeholder="Generate To Date" id="txtPoToDate" onfocus="this.select()" />
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <button type="button" class="btn btn-info buttonAnimation pull-left round box-shadow-1  btn-sm" id="btnSearch">Search</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <style>
                                    tbody .total {
                                        text-align: right;
                                    }
                                </style>
                                <div class="row form-group">
                                    <div class="col-md-12">


                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblPOList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="Action text-center">Action</td>
                                                       <td class="PONo text-center">PO No.</td>
                                                        <td class="PODate text-center">PO Date</td>
                                                        <td class="POStatus text-center">PO Status</td>
                                                        <td class="RecNo text-center">Rec No</td>
                                                        <td class="RecDate">Rec Date</td>
                                                        <td class="BillNo text-center">Bill No.</td>
                                                        <td class="BillDate text-center">Bill Date</td>
                                                        <td class="RecBy text-center">Rec By</td>
                                                        <td class="Status text-center">Rec Status</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row container-fluid">
                                    <div class="">
                                        <select class="form-control input-sm border-primary" id="ddlPaging" runat="server" style="width: 100%" onchange="POLIst(1);">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
    </div>
    </div>
        </section>
    </div>
       <div id="ShowPODraftLog" class="modal fade" role="dialog">
        <div class="modal-dialog modal-xl">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="modal-title">PO Log</h4>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="tblPODraftLog">
                            <thead class="bg-blue white">
                                <tr>
                                    <td class="SrNo text-center">SN</td>
                                    <td class="ActionBy">Action By</td>
                                    <td class="Date text-center">Date</td>
                                    <td class="Action">Action</td>
                                    <td class="Remark" style="white-space: pre-wrap;">Remark</td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                          <h5 class="well text-center" id="EmptyTable33" style="display: none">No data available.</h5>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="/POVendor/JS/POStockListInventoryManager.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
</asp:Content>

