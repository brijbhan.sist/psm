﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="RecievePoByVendor.aspx.cs" Inherits="RecievePoByVendor" ClientIDMode="static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        #tblProductDetail tbody .ProName {
            text-align: left;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input id="DraftAutoId" type="hidden" />
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Receive PO Stock</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage PO</a></li>
                        <li class="breadcrumb-item"><a href="#">By Vendor</a></li>
                        <li class="breadcrumb-item">PO List</li>
                        <li class="breadcrumb-item">Receive PO Stock</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a class="dropdown-item" href="~/POVendor/POStockListInventoryManager.aspx" runat="server">Receive Stock List</a>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-5">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">PO Details</h4>
                        </div>
                        <input type="hidden" id="hiddenEmpTypeVal" runat="server" />
                        <input type="hidden" id="hiddenStatusCode" runat="server" />
                        <div class="card-content collapse show">
                            <div class="card-body" id="panelBill">
                                <div class="row">
                                    <div class="col-md-6  col-sm-6">
                                        <label class="control-label">
                                            Vendor Name
                                        </label>
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm border-primary" id="vendorname" readonly="readonly" />
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-sm-6 form-group">
                                        <label class="control-label">PO No.</label>
                                        <input type="hidden" id="txtPOAutoId" class="form-control input-sm" />
                                        <div class="input-group">
                                            <input type="text" id="txtPoNo" class="form-control input-sm border-primary" readonly="readonly" />
                                             <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                                            <i class="la la-eye" id="ViewPOId" onclick="RedirectOnPODetails()"></i>
                                                        </span>
                                                    </div>
                                        </div>
                                    </div>
                                </div>
                               
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <label class="control-label">Generate PO Date</label>
                                        <div class="form-group">
                                            <input type="text" id="txtPoDate" class="form-control input-sm border-primary" readonly="readonly" />
                                        </div>
                                    </div>
                                     <div class="col-md-6 col-sm-6">
                                        <label class="control-label">Status</label>
                                        <input type="hidden" id="hiddenOrderStatus" />
                                        <div class="form-group">
                                            <input type="text" id="txtStatus" class="form-control input-sm border-primary" readonly="readonly" />
                                        </div>
                                    </div>
                       
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                   <div class="col-md-7">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Receive Details</h4>
                        </div>
                      
                        <div class="card-content collapse show">
                            <div class="card-body" id="paneldBill">
                               
                                <div class="row">
                                     <div class="col-md-4  col-sm-4">
                                        <label class="control-label">
                                            Receive No.</label>
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm border-primary" maxlength="40" id="txtRecNo" disabled/>
                                        </div>
                                    </div>
                                     <div class="col-md-3  col-sm-3">
                                        <label class="control-label">
                                            Receive Status</label>
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm border-primary" maxlength="40" id="ddlRecStatus" disabled/>
                                        </div>
                                    </div>
                                     <div class="col-md-5 col-sm-5">
                                        <div class="form-group">
                                            <label class="control-label">Receive Date </label><span class="required">*</span>
                                            <input type="text" id="txtRecDate"  class="form-control border-primary input-sm req date" placeholder="Receive Date" disabled/>

                                        </div>
                                    </div>
                                    
                                  
                                   
                                </div>
                                <div class="row">
                                      <div class="col-md-4  col-sm-4">
                                        <label class="control-label">
                                            Receive By
                                        </label>
                                        <div class="form-group">
                                           <input type="text" class="form-control input-sm border-primary" id="txtRecBy" disabled/>
                                        </div>
                                    </div>
                                     <div class="col-md-3  col-sm-3">
                                        <label class="control-label">
                                            Bill No.<span style="color:red;">*</span>
                                        </label>
                                        <div class="form-group">
                                           <input type="text" class="form-control input-sm border-primary" maxlength="40" id="txtBillno" />
                                        </div>
                                    </div>
                                     <div class="col-md-5  col-sm-5">
                                        <label class="control-label">
                                            Bill Date<span style="color:red;">*</span>
                                        </label>
                                        <div class="form-group">
                                           <input type="text" class="form-control input-sm border-primary" id="txtBilldate" />
                                        </div>
                                    </div>
                                     
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-sm-4">
                                        <label class="control-label">
                                            Manager remarks
                                        </label>
                                        <div class="form-group">
                                             <textarea id="txtInvManager_Remark" class="form-control input-sm border-primary"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
<%--        <section id="panelProduct">
            <div class="row" id="panelOrderContent">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Add Products</h4>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse" id="LoadProducts"><i class="ft-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse">
                            <div class="card-body" id="panelBill5">
                                <div class="row">
                                    <label class="col-md-1 col-sm-1 control-label">Barcode</label>
                                    <div class="col-md-3  col-sm-3  form-group">
                                        <input type="text" class="form-control input-sm border-primary" id="txtBarcode" runat="server" onfocus="this.select()" onkeypress="return isNumberKey(event)" placeholder="Enter Barcode here" onchange="readBarcode()" />
                                    </div>

                                    <div class="col-md-3  col-sm-3 ">
                                        <div class="alert alert-default alertSmall pull-right " id="alertBarcodeCount" style="display: none; color: #c62828;">
                                            Barcode Count : &nbsp;<span>sd</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="row">
                                    <div class="col-md-5  col-sm-3 ">
                                        <label class="control-label">
                                            Product <span class="required">*</span>

                                        </label>
                                        <div class="form-group">
                                            <select class="form-control input-sm border-primary selectvalidate" id="ddlProduct" runat="server" style="width: 100% !important" onchange="BindUnittype()">
                                                <option value="0">-Select-</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2  col-sm-2 ">
                                        <label class="control-label">
                                            Unit Type <span class="required">*</span>

                                        </label>
                                        <div class="form-group">
                                            <select class="form-control input-sm border-primary ddlreq" id="ddlUnitType" runat="server">
                                                <option value="0">-Select-</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-1  col-sm-2">
                                        <label class="control-label" style="word-wrap: normal">
                                            Qty <span class="required">*</span>

                                        </label>
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm border-primary ddlreq text-center" id="txtReqQty" runat="server" value="1" maxlength="4" onkeypress="return isNumberKey(event)" />
                                        </div>
                                    </div>
                                    <div class="col-md-2  col-sm-2">
                                        <label class="control-label" style="word-wrap: normal">
                                            Total Pieces

                                        </label>
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm border-primary text-center" id="txtTotalPieces" runat="server" readonly="readonly" />
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="pull-left">
                                            <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1 btn-sm pulse" id="btnAdd" style="margin-top: 22px;">&nbsp;&nbsp;&nbsp;Add&nbsp;&nbsp;&nbsp;</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                               
                                <div class="clearfix"></div>
                                <div id="divBarcode" style="display: none;">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-2 col-ms-2">
                                                    <label class="control-label">Quantity</label>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm border-primary" id="txtQty" value="1" maxlength="4" />
                                                    </div>
                                                </div>
                                                <div class="col-md-1  col-sm-1 ">
                                                    <label class="control-label">
                                                        Is Exchange 
                                                    </label>
                                                    <div class="form-group">
                                                        <input type="checkbox" id="IsExchange" onchange="changetype(this,'IsFreeItem')" />
                                                    </div>
                                                </div>
                                                <div class="col-md-1  col-sm-1 ">
                                                    <label class="control-label">
                                                        Is Free Item 
                                                    </label>
                                                    <div class="form-group">
                                                        <input type="checkbox" id="IsFreeItem" onchange="changetype(this,'IsExchange')" />
                                                    </div>
                                                </div>
                                                <script>
                                                    function changetype(e, idhtml) {
                                                        $('#' + idhtml).prop('checked', false);
                                                    }
                                                </script>
                                                <div class="col-md-3  col-ms-3">
                                                    <label class="control-label">Enter Barcode</label>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm border-primary" id="txtScanBarcode" onchange="checkBarcode()" />
                                                    </div>
                                                </div>
                                                <div class="col-md-3  col-ms-3">
                                                    <label class="control-label">
                                                        Product<span class="required">*</span>
                                                        <asp:RequiredFieldValidator ID="rfvitemproduct" runat="server" ErrorMessage="Please Select Product" SetFocusOnError="true"
                                                            ValidationGroup="AddProductQty" ControlToValidate="ddlitemproduct" Display="None" InitialValue="0" ForeColor="Transparent">*</asp:RequiredFieldValidator>
                                                        <asp:ValidatorCalloutExtender runat="server" CssClass="CustomValidatorCalloutStyle"
                                                            ID="ValidatorCalloutExtender1" TargetControlID="rfvitemproduct" PopupPosition="BottomLeft">
                                                        </asp:ValidatorCalloutExtender>
                                                    </label>
                                                    <div class="form-group">
                                                        <select class="form-control input-sm border-primary" id="ddlitemproduct" runat="server" onchange="itemProduct()" style="width: 100% !important">
                                                            <option value="0" style="display: none">-Select-</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2  col-ms-2">
                                                    <div class="pull-left">
                                                        <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1 btn-sm pulse" <%--id="btnAdd" style="margin-top: 22px;">&nbsp;&nbsp;&nbsp;Add&nbsp;&nbsp;&nbsp;</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>--%>
        <section>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-content collapse show">
                            <div class="card-body" id="panelBill6">
                                  <style>
                                .table th, .table td {
                                    padding: 0.70rem 0.5rem !important;
                                }
                            </style>
                                <div class="table-responsive">
                                    <table id="tblProductDetail" class="table table-striped table-bordered">
                                        <thead class="bg-blue white">
                                            <tr>
                                                <td class="Action text-center wth2">Action</td>
                                                <td class="ProId text-center wth4">ID</td>
                                                <td class="ProName">Product Name</td>
                                                <td class="UnitType text-center">Unit Type</td>
                                                <td class="ReqQty text-center wth2">Quantity</td>
                                                <td class="TtlPcs text-center wth5">Total<br /> Pieces</td>
                                                <td class="RecUnit text-center wth3">Received<br /> Unit Type</td>
                                                <td class="RecievedQty text-center">Received <br />Quantity</td>
                                                <td class="RecievedPieces text-center wth5">Received <br /> Pieces</td>
                                                <td class="CCostPrice price wth5">Current<br />Cost Price</td>
                                                <td class="CostPrice text-center wth5">New Cost<br /> Price</td>
                                                <td class="NetPrice text-right" style="width:5%;">Net Price</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                        <tfoot>
                                            <tr style="font-weight:bold">
                                                <td class="hdnt" colspan="1"></td>
                                                <td colspan="10">Total</td>
                                                <td><span id="TotalPrice" style="float: right;"></span></td>
                                            </tr>
                                            <tr>
                                                <td colspan="12"><span id="TotalItem" style="float: right;"></span></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    <h5 class="well text-center" id="emptyTable" style="display: none">No Product Selected.</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>

        <section>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <input type="hidden" id="hidden1" runat="server" />
                        <input type="hidden" id="hidden2" runat="server" />
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6  col-sm-6">
                                        <label class="control-label">
                                            Generate Remark
                                        </label>
                                        <div class="form-group">
                                            <textarea id="txtGen_Remark" class="form-control input-sm border-primary" readonly="readonly"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6  col-sm-6">
                                        <label class="control-label">
                                            Receiver Remark
                                        </label>
                                        <div class="form-group">
                                            <textarea id="txtReciever_Remark" class="form-control input-sm border-primary" readonly="readonly"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="card" style="padding: 10px;">
                    <section id="Div2">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <button type="button" id="btnPartialPo" class="btn btn-success buttonAnimation round box-shadow-1 btn-sm pulse" onclick="PartialPO()">Receive PO Stock</button>
                                      <%--  <button type="button" id="btnRecievePo" onclick="return RecievePOStock();" class="btn btn-facebook buttonAnimation round box-shadow-1 btn-sm pulse">Receive PO</button>--%>
                                        <button type="button" id="btnRevertPo" class="btn btn-primary buttonAnimation round box-shadow-1 btn-sm pulse" onclick="RevertPOBYIM()">Revert PO</button>
                                       <%-- <button type="button" id="btnCancel" class="btn btn-pink buttonAnimation round box-shadow-1 btn-sm pulse">Cancel PO</button>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

    <div id="managePrice" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-xl" style="width: 90%">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 style="float: left" class="modal-title">Manage Price</h4>&nbsp&nbsp&nbsp&nbsp
                    <h3 class="modal-title" id="ProductText"></h3>
                    <input type="hidden" id="txtHProductAutoId" />
                    <button type="button" class="close" data-dismiss="modal">&nbsp x &nbsp</button>
                </div>

                <div class="modal-body">
                   <%-- <div class="row">
                        <div class="col-md-5"></div>
                        <div class="col-md-6  form-group">
                            <input type="radio" name="Qty" onclick="fun_getQty(this)" class="QtyPop" />&nbsp;&nbsp;<b class="QtyPop">Qty</b>&nbsp;&nbsp;
                            <input type="radio" name="Qty" onclick="fun_getPrice(this)" />&nbsp;&nbsp;<b>Price</b>
                        </div>
                    </div>--%>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered" id="Table1">
                                    <thead class="bg-blue white">
                                        <tr>
                                            <td class="UnitType" style="width: 15%;">Unit Type</td>
                                            <td class="Qty text-center">Qty</td>
                                            <td class="CostPrice" style="width: 15%;text-align:right">Cost Price</td>
                                            <td class="NewCosePrice" style="width: 20%;text-align:right">New Cost Price</td>
                                            <td class="BasePrice" style="width: 15%;text-align:right">Base Price</td>
                                            <td class="NewBasePrice" style="width: 20%;text-align:right">New Base Price</td>
                                            <td class="RetailPrice" style="width: 20%;text-align:right">Min Retail Price</td>
                                            <td class="NewRetailPrice" style="width: 20%;text-align:right">New M. Retail Price</td>
                                            <td class="WHminPrice" style="width: 20%;text-align:right">WH. Min Price</td>
                                            <td class="NWHPrice" style="width: 20%;text-align:right">New WH. Min Price</td>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="alert alert-danger alert-dismissable fade in" id="Div1" style="width: 33%; text-align: left;">
                      <span><b style="color:red">Note - </b>Please check the Enable section check box to manage price.</span>
                    </div>
                    <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1  btn-sm" onclick="updatebulk()">&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;</button>
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1  btn-sm" data-dismiss="modal">&nbsp;&nbsp;&nbsp;Close&nbsp;&nbsp;&nbsp;</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="BarCodenotExists" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="display: block">
                    <h4>Message</h4>
                </div>
                <div class="modal-body">
                    <div style="border-color: #ac2925; border: 1px solid; color: #ac2925; min-height: 45px; line-height: 20px; padding: 9px;">
                        <strong id="barcodemsg"></strong>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse" onclick="focusonBarcode()">OK</button>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="ItemAutoId" />
    <audio class="audios" id="yes_audio" controls preload="none" style="display: none">
        <source src="/Audio/NOExists.mp3" type="audio/mpeg" />
    </audio>
    <audio class="audios" id="No_audio" controls preload="none" style="display: none">
        <source src="/Audio/WrongProduct.mp3" type="audio/mpeg" />
    </audio>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="Js/PurchaseOrder_IM.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
</asp:Content>
