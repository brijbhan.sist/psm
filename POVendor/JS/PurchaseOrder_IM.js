﻿var getid = "", WeightTax = 0.00; RecAutoId = 0;
var PackerSecurityEnable = false;
$(document).ready(function () {
    $('#txtBilldate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
   
    var getQueryString = function (field, url) {
    var href = url ? url : window.location.href;
    var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
    var string = reg.exec(href);
    return string ? string[1] : null;
    };
    getid = getQueryString('Id');
    RecAutoId = getQueryString('RecAutoId');
    if (getid != null) {
        Binddata(getid, RecAutoId);
    }
    else {
            $("#emptyTable").show();
        }
});
$("#LoadProducts").click(function () {
    if ($("#LoadProducts").find("i").attr('class') == "ft-plus") {
        bindDropdown();
    }
});
function bindDropdown() {
    $.ajax({
        type: "POST",
        url: "/Purchase/WebAPI/PurchaseOrder.asmx/bindDropdown",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var product = $(xmldoc).find("Table1");
                
                $("#ddlProduct option:not(:first)").remove();
                $.each(product, function () {
                    $("#ddlProduct").append("<option WeightOz='" + $(this).find("WeightOz").text() + "' MLQty='" + $(this).find("MLQty").text() + "' value='" + $(this).find("AutoId").text() + "'>" + $(this).find("ProductName").text() + "</option>");
                });
                $("#ddlProduct").select2();
            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function BindUnittype() {
    $("#alertStockQty").hide();
    var productAutoId = $("#ddlProduct option:selected").val();
    $.ajax({
        type: "POST",
        url: "/Purchase/WebAPI/PurchaseOrder.asmx/bindUnitType",
        data: "{'productAutoId':" + productAutoId + "}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var unitType = $(xmldoc).find("Table");
                var unitDefault = $(xmldoc).find("Table1");
                var count = 0;

                $("#ddlUnitType option:not(:first)").remove();
                $.each(unitType, function () {
                    $("#ddlUnitType").append("<option value='" + $(this).find("AutoId").text() + "' EligibleforFree='" + $(this).find("EligibleforFree").text() + "' QtyPerUnit='" + $(this).find("Qty").text() + "' >" + $(this).find("UnitType").text() + " (" + $(this).find("Qty").text() + " pcs" + ")</option>");
                });
                if (unitDefault.length > 0) {
                    if (BUnitAutoId == 0) {
                        $("#ddlUnitType").val($(unitDefault).find('AutoId').text()).change();
                    } else {
                        $("#ddlUnitType").val(BUnitAutoId).change();
                        BUnitAutoId = 0;
                    }
                } else {
                    $("#ddlUnitType").val(0);
                }
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
};

function Binddata(Id, RecAutoId) {
    $.ajax({
        type: "POST",
        url: "RecievePoByVendor.aspx/BindData",
        data: "{'AutoId':'" + Id + "','RecAutoId':'" + RecAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var OrderDetails = $(xmldoc).find("Table");
                var Products = $(xmldoc).find("Table1");
                var ReceiveDetails = $(xmldoc).find("Table3");
                if ($(ReceiveDetails).length > 0) {
                    $("#ddlRecStatus").val($(ReceiveDetails).find("StatusType").text());
                    $("#txtRecNo").val($(ReceiveDetails).find("PORecieveId").text());
                    $("#txtRecBy").val($(ReceiveDetails).find("ReceiveBy").text());
                    $("#txtRecDate").val($(ReceiveDetails).find("ReceiveDate").text());
                }
                if ($(OrderDetails).length > 0) {
                    $("#txtBillno").val($(OrderDetails).find("BillNo").text());
                    $("#txtBilldate").val($(OrderDetails).find("BillDate").text());
                    $("#txtPoNo").val($(OrderDetails).find("PONo").text());
                    $("#vendorname").val($(OrderDetails).find("VendorName").text()).change();
                    $("#txtPoDate").val($(OrderDetails).find("PODate").text());
                    $("#txtStatus").val($(OrderDetails).find("Status").text());
                    if ($(OrderDetails).find("StatusAutoId").text() == 3 || $(OrderDetails).find("StatusAutoId").text() == 4) {
                        $("#panelProduct").hide();
                        $("#txtInvManager_Remark").attr('disabled', true);
                        $("#txtBillno").attr('disabled', true);
                        $("#txtBilldate").attr('disabled', true);
                    }
                    $("#txtGen_Remark").val($(OrderDetails).find("PORemarks").text());
                    $("#txtReciever_Remark").val($(OrderDetails).find("RecieveStockRemark").text());
                    $("#txtPOAutoId").val($(OrderDetails).find("AutoId").text());
                    $("#tblProductDetail tbody tr").remove();
                    var row = $("#tblProductDetail thead tr").clone();
                    $.each(Products, function (index) {
                        $(".ProId", row).text($(this).find('ProductId').text());
                        $(".ProName", row).html("<span ProductAutoId='" + $(this).find('ProductAutoId').text() + "'>" + $(this).find('ProductName').text() + "</span>");
                        $(".UnitType", row).html("<span UnitAutoId='" + $(this).find("Unit").text() + "' QtyPerUnit='" + $(this).find("QtyPerUnit").text() + "'>" + $(this).find("UnitType").text() + " (" + $(this).find("QtyPerUnit").text() + " pcs" + ")</span>");
                        $(".ReqQty", row).text($(this).find('Qty').text());
                        $(".TtlPcs", row).text($(this).find('ReqTotalPieces').text());
                        $(".CCostPrice", row).text(parseFloat($(this).find('oldPrice').text()).toFixed(2));
                        $(".RecievedQty", row).text($(this).find('RecivedQty').text());
                        $(".RecievedPieces", row).text($(this).find("RecTotalPieces").text());
                        $(".RecUnit", row).text($(this).find("RecUnitType").text());
                        debugger;
                        if ($(ReceiveDetails).find('Status').text() == '2') {
                            debugger;
                            if ($(this).find("RecTotalPieces").text() != "0") {
                                //$(".CostPrice", row).html("<input class='text-right' onkeyup='rowCal(this)' type='text' onkeypress='return isNumberDecimalKey(event)' style='width:80px'/>");
                                $(".CostPrice", row).html("<input class='text-right' onkeyup='rowCal(this)' type='text' onChange='PopupManagePrice(this)' value='" + $(this).find("CostPrice").text() + "' onkeypress='return isNumberDecimalKey(event)' style='width:80px'/>")
                            }
                            else {
                                $(".CostPrice", row).html("<input class='text-right'  type='text' disabled style='width:80px' />");
                            }
                        } else if ($(ReceiveDetails).find('Status').text() == '3'){
                            debugger;
                            if ($(this).find("RecTotalPieces").text() != "0") {
                                $(".CostPrice", row).html("<input value='" + $(this).find("CostPrice").text() + "' class='text-right' disabled onkeyup='rowCal(this)' type='text' onChange='PopupManagePrice(this)' value='" + $(this).find("CostPrice").text() + "' onkeypress='return isNumberDecimalKey(event)' style='width:80px'/>")
                            }
                            else {
                                $(".CostPrice", row).html("<input class='text-right'  type='text' disabled style='width:80px'/>")
                            }
                            $("#panelProduct").hide();
                            $("#txtInvManager_Remark").attr('disabled', true);
                            $("#txtBillno").attr('disabled', true);
                            $("#txtBilldate").attr('disabled', true);
                        } else if ($(ReceiveDetails).find('Status').text() == '1' || $(ReceiveDetails).find('Status').text() == '4') {
                            debugger;
                            if ($(this).find("RecTotalPieces").text() != "0") {
                                $(".CostPrice", row).html("<input class='text-right' disabled onkeyup='rowCal(this)' type='text'  onkeypress='return isNumberDecimalKey(event)' style='width:80px'/>")
                            }
                            else {
                                $(".CostPrice", row).html("<input class='text-right'  type='text' disabled style='width:80px'/>")
                            }
                        }
                        
                        if ($(this).find("CostPrice").text() != "") {
                            $(".NetPrice", row).html(parseFloat(parseFloat($(this).find('RecivedQty').text()) * parseFloat($(this).find('CostPrice').text())).toFixed(2));
                        }
                        else {
                            $(".NetPrice", row).html("0.0")
                        }
                        if ($(ReceiveDetails).find('Status').text() == '3' || $(ReceiveDetails).find('Status').text() == '4') {
                            $("#tblProductDetail thead tr").find('.Action').hide();
                            $(".Action", row).hide();
                            $("#tblProductDetail tfoot tr").find('.hdnt').hide();
                        } else {
                            $(".Action", row).html("<a title='Edit' href='#' onclick='EditProductPrice(this)'><span class='ft-edit' ></span></a>&nbsp;");
                        }
                        if ($(ReceiveDetails).find('Status').text() != '2') {
                            $("#btnPartialPo").hide();
                            $("#btnRevertPo").hide();
                          
                        } else {
                            $("#btnPartialPo").show();
                            $("#btnRevertPo").show();
                        }
                        
                        $('#tblProductDetail tbody').append(row);
                        row = $("#tblProductDetail thead tr:last").clone(true);
                    });
                    countRow();
                    CalculateFooter();
                }
            }
        }
    });
}
function CalculateFooter() {
    var total1 = 0;
    $("#tblProductDetail tbody tr").each(function () {
        total1 += Number($(this).find('.NetPrice').text());
    })
    $("#TotalPrice").text(total1.toFixed(2));

}
$("#btnAdd").click(function () {
    var flag1 = false;
    var validatecheck = dynamicInputTypeSelect2('selectvalidate');
    if (!validatecheck) {
        checkRequiredField();
    }
    else {
        validatecheck = checkRequiredField();
    }
    if (validatecheck) {
        var productAutoId = $("#ddlProduct option:selected").val();
        var unitAutoId = $("#ddlUnitType option:selected").val();
        if ($("#txtReqQty").val() == 0) {
            toastr.error('Required quantity can not be Zero.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            return;
        }
        $("#emptyTable").hide();
        $("#alertStockQty").hide();

        if (productAutoId != "" && productAutoId != 0) {
            var Draftdata = {
                DraftAutoId: $("#DraftAutoId").val(),
                VenderAutoId: $("#ddlVender").val(),
                ProductAutoId: $("#ddlProduct").val(),
                unitAutoId: $("#ddlUnitType").val(),
                ReqQty: $("#txtReqQty").val(),
                QtyPerUnit: $("#ddlUnitType option:selected").attr("QtyPerUnit"),
                PORemark: $("#txtOrderRemarks").val()
            }

            $.ajax({
                type: "POST",
                url: "/Purchase/WebAPI/PurchaseOrder.asmx/DraftData",
                data: "{'data':'" + JSON.stringify(Draftdata) + "'}",
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                async: false,
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.d != "Session Expired") {
                        if (response.d != "false") {
                            $("#DraftAutoId").val(response.d);
                        }
                    } else {
                        location.href = '/';
                    }

                },
                error: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                },
                failure: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                }
            });

            var product = $("#ddlProduct option:selected").text().split("--");
            var row = $("#tblProductDetail thead tr").clone(true);
            $("#tblProductDetail tbody tr").each(function () {
                if ($(this).find(".ProName span").attr("productautoid") == productAutoId && $(this).find(".UnitType > select").val() == unitAutoId) {
                    var reqQty = Number($(this).find(".ReqQty input").val()) + Number($("#txtReqQty").val());
                    $(this).find(".ReqQty input").val(reqQty);
                    flag1 = true;
                    rowCal($(this).find(".ReqQty > input"));
                    $('#tblProductDetail tbody tr:first').before($(this));
                    countRow();
                    toastr.success('Product added successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    $("#txtBarcode").val('');
                    $("#txtBarcode").focus();
                    $("#ddlProduct").select2('val', '0');
                    $("#ddlUnitType").val(0);
                    $("#txtReqQty").val("1");
                }
            });
            if (!flag1) {
                $(".ProId", row).html(product[0]);
                $(".ProName", row).html("<span productautoid=" + productAutoId + ">" + product[1] + "</span>");
                $(".UnitType", row).html('<select class="form-control input-sm border-primary ddlreq" onchange="rowCal(this)"></select>');
                $(".ReqQty", row).html("<input type='text' onkeypress='return isNumberKey(event)' class='form-control input-sm border-primary text-center' style='width:100px;' onkeyup='rowCal(this)' value='" + $("#txtReqQty").val() + "'/>");
                $(".Action", row).html("<a href='javascript:;' id='deleterow' onclick='deleteItemrecord(this)'><span class='ft-x'></span></a>");
                $('#tblProductDetail tbody').append(row);
                
                fillUnitTlb(row);
                rowCal(row.find(".ReqQty > input"));
                countRow();
                toastr.success('Product added successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            }
        }
        else {
            toastr.error('All * fields are mandatory', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        }
    }
});

function fillUnitTlb(e) {
    var row = $(e);
    var ProductId = row.find(".ProName > span").attr('productautoid');
    $.ajax({
        type: "POST",
        url: "/Purchase/WebAPI/PurchaseOrder.asmx/bindUnitType",
        data: "{'productAutoId':" + ProductId + "}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: true,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var unitType = $(xmldoc).find("Table");
                var unitDefault = $(xmldoc).find("Table1");
                var count = 0;
                ;
                row.find(".UnitType >select option:not(:first)").remove();
                //$("#ddlUnitType option:not(:first)").remove();
                $.each(unitType, function () {
                    row.find(".UnitType > select").append("<option value='" + $(this).find("AutoId").text() + "' EligibleforFree='" + $(this).find("EligibleforFree").text() + "' QtyPerUnit='" + $(this).find("Qty").text() + "' >" + $(this).find("UnitType").text() + " (" + $(this).find("Qty").text() + " pcs" + ")</option>");
                });
                if (unitDefault.length > 0) {
                    if (BUnitAutoId == 0) {
                        row.find(".UnitType > select").val($("#ddlUnitType option:selected").val()).change();
                    } else {
                        row.find(".UnitType > select").val(BUnitAutoId).change();
                        BUnitAutoId = 0;
                    }
                }
                else {
                    row.find(".UnitType > select").val(0);
                }
                $(".TtlPcs", row).text(parseInt($("#txtReqQty").val()) * parseInt($("#ddlUnitType option:selected").attr('QtyPerUnit')));
                $("#txtBarcode").val('');
                $("#txtBarcode").focus();
                $("#ddlProduct").select2('val', '0');
                $("#ddlUnitType").val(0);
                $("#txtReqQty").val("1");
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });


}
function countRow() {
    var count = 0;
    $("#tblProductDetail tbody tr").each(function () {
        count++;
    });
    $('#TotalItem').html("Total Items : " + count);
}
function rowCal(e) {
    var row = $(e).closest("tr");
    row.find('.CostPrice input').removeClass('border-warning');
    if (parseFloat(row.find(".CostPrice > input").val()) == parseFloat(row.find(".CCostPrice").text())) {
        var NetPrice = parseFloat(parseFloat(row.find(".CostPrice > input").val()) * parseFloat(row.find(".RecievedQty").text())).toFixed(2);
        row.find(".NetPrice").html(NetPrice);
    } else {
        row.find(".NetPrice").html('0.0');
    }
    CalculateFooter();
}

function deleteItemrecord(e) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this product.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, Cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: false,
            },
            confirm: {
                text: "Yes, Delete it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            var tr = $(e).closest('tr');

            
            var data = {
                DraftAutoId: $("#DraftAutoId").val(),
                ProductAutoId: $(tr).find('.ProName span').attr('ProductAutoId'),
                UnitAutoId: $(tr).find('.UnitType > select').val()
            }
            if ($("#DraftAutoId").val() != '') {
                $.ajax({
                    type: "POST",
                    url: "/Purchase/WebAPI/PurchaseOrder.asmx/DeleteDraftItem",
                    data: "{'DataValue':'" + JSON.stringify(data) + "'}",
                    contentType: "application/json; charset=utf-8",
                    datatype: "json",
                    async: false,
                    beforeSend: function () {
                        $.blockUI({
                            message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                            overlayCSS: {
                                backgroundColor: '#FFF',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (response) {
                        if (response.d != "Session Expired") {
                            swal("", "Item deleted successfully.", "success");
                        } else {
                            location.href = '/';
                        }
                        $(e).closest('tr').remove();

                    },
                    error: function (result) {
                        console.log(JSON.parse(result.responseText).d);
                    },
                    failure: function (result) {
                        console.log(JSON.parse(result.responseText).d);
                    }
                });
            } else {
                $(e).closest('tr').remove();
                swal("", "Item deleted successfully.", "success");
            }

            if ($("#tblProductDetail tbody tr").length == 0) {
                $('#tblProductDetail tfoot tr').empty();
                $("#emptyTable").show();
            } else {
                countRow();
                $("#emptyTable").hide();
            }
            swal("", "Item deleted successfully.", "success");

        } else {
            swal("", "Your product is safe.", "error");
        }
    })
}
var BUnitAutoId = 0;
function readBarcode() {
    var Barcode = $("#txtBarcode").val();
    if (Barcode != "") {

        var data = {
            Barcode: Barcode,
            DraftAutoId: $("#DraftAutoId").val(),
            VenderAutoId: $("#ddlVender").val(),
            PORemark: $("#txtOrderRemarks").val(),
            ReqQty: "1",
        }
        $.ajax({
            type: "POST",
            url: "/Purchase/WebAPI/PurchaseOrder.asmx/GetBarDetails",
            data: "{'dataValues':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                
                var xmldoc = $.parseXML(response.d);
                var product = $(xmldoc).find("Table");
                var unitType = $(xmldoc).find("Table1");
                var DID = $(xmldoc).find("Table2");
                if (product.length > 0) {
                    $("#DraftAutoId").val($(DID).find('DraftAutoId').text());
                    var productAutoId = $(product).find('ProductAutoId').text();
                    var unitAutoId = $(product).find('UnitType').text();
                    var flag1 = false;
                    $("#tblProductDetail tbody tr").each(function () {
                        if ($(this).find(".ProName > span").attr("productautoid") == productAutoId && $(this).find(".UnitType > select").val() == unitAutoId) {
                            var Qtyreq = 1;
                            var reqQty = Number($(this).find(".ReqQty input").val()) + Number(Qtyreq);
                            $(this).find(".ReqQty input").val(reqQty);
                            flag1 = true;
                            rowCal($(this).find(".ReqQty > input"));
                            $('#tblProductDetail tbody tr:first').before($(this));
                            countRow();
                        }
                    });
                    var row = $("#tblProductDetail thead tr").clone(true);
                    if (!flag1) {
                        $(".ProId", row).html($(product).find('ProductId').text());
                        $(".ProName", row).html("<span ProductAutoId='" + productAutoId + "'>" + $(product).find('ProductName').text() + "</span>");
                        var ddu = '<select class="form-control input-sm border-primary ddlreq" onchange="rowCal(this)">';
                        $.each(unitType, function () {
                            ddu += "<option value='" + $(this).find("AutoId").text() + "' EligibleforFree='" + $(this).find("EligibleforFree").text() + "' QtyPerUnit='" + $(this).find("Qty").text() + "' >" + $(this).find("UnitType").text() + " (" + $(this).find("Qty").text() + " pcs" + ")</option>"
                        })
                        ddu += '</select>';
                        $(".UnitType", row).html(ddu);
                        row.find(".UnitType > select").val(unitAutoId).change();
                        $(".ReqQty", row).html("<input type='text' onkeypress='return isNumberKey(event)' class='form-control input-sm border-primary text-center' runat='server' style='width:100px;' onkeyup='rowCal(this)' value='" + $("#txtReqQty").val() + "'/>");
                        $(".TtlPcs", row).text(parseInt($("#txtReqQty").val()) * $(product).find('UnitQty').text());
                        $(".Action", row).html("<a href='javascript:;' id='deleterow' onclick='deleteItemrecord(this)'><span class='ft-x'></span></a>");
                        if ($('#tblProductDetail tbody tr').length > 0) {
                            $('#tblProductDetail tbody tr:first').before(row);
                        }
                        else {
                            $('#tblProductDetail tbody').append(row);
                        }
                        rowCal(row.find(".ReqQty > input"));
                        countRow();
                        $("#txtQuantity, #txtTotalPieces").val("0");
                        if ($('#tblProductDetail tbody tr').length > 0) {
                            $('#emptyTable').hide();
                        }
                        toastr.success('Product added successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    }

                    else {
                        toastr.success('Product added successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    }
                    $("#txtBarcode").val('');
                    $("#txtBarcode").focus();

                } else {
                    swal({
                        title: "Error!",
                        text: "Barcode does not exists.",
                        icon: "error",
                        closeOnClickOutside: false
                    }).then(function (isConfirm) {

                        if (isConfirm) {
                            $("#txtBarcode").val('');
                            $("#txtBarcode").focus();
                        }
                    });
                    $("#yes_audio")[0].play();
                    $("#panelProduct select").val(0);
                    $("#txtQuantity, #txtTotalPieces").val("0");
                    $("#alertBarcodeCount").hide();
                    $("#txtBarcode").val('');
                    $("#txtReqQty").focus();
                    $("#txtBarcode").blur();
                }


            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    } else {
        $("#ddlProduct").val(0).change();
        $("#alertBarcodeCount").hide();
    }
}
var PackedBoxes = 0;

$("#btnReset").on('click', function () {
    window.location = "/Purchase/PurchaseOrder.aspx"
})
function EditPoOrder(Id) {
    $.ajax({
        type: "POST",
        url: "/Purchase/WebAPI/PurchaseOrder.asmx/editOrder",
        data: "{'AutoId':'" + Id + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var OrderDetails = $(xmldoc).find("Table");
                var Products = $(xmldoc).find("Table1");
                var unitType = $(xmldoc).find("Table2");
                if ($(OrderDetails).length > 0) {
                    
                    $("#txtOrderId").val($(OrderDetails).find("PONo").text());
                    $("#ddlVender").val($(OrderDetails).find("VenderAutoId").text()).change();
                    $("#txtOrderDate").val($(OrderDetails).find("PODate").text());
                    $("#txtDeliveryDate").val($(OrderDetails).find("DeliveryDate").text())
                    $("#txtOrderStatus").val($(OrderDetails).find("Status").text());
                    $("#txtOrderRemarks").val($(OrderDetails).find("PORemarks").text());
                    $("#txtPOAutoId").val($(OrderDetails).find("AutoId").text());
                    var row = $("#tblProductDetail thead tr").clone();
                    $.each(Products, function (index) {
                        var pid = $(this).find('ProductAutoId').text()
                        $(".ProId", row).html($(this).find('ProductId').text());
                        $(".ProName", row).html("<span ProductAutoId='" + $(this).find('ProductAutoId').text() + "'>" + $(this).find('ProductName').text() + "</span>");

                        var ddu = '<select class="form-control input-sm border-primary ddlreq" onchange="rowCal(this)">';
                        $.each(unitType, function () {
                            var pid1 = $(this).find('ProductAutoId').text()
                            console.log(pid + "-" + pid1);
                            if (pid1 == pid) {
                                ddu += "<option value='" + $(this).find("AutoId").text() + "' EligibleforFree='" + $(this).find("EligibleforFree").text() + "' QtyPerUnit='" + $(this).find("Qty").text() + "' >" + $(this).find("UnitType").text() + " (" + $(this).find("Qty").text() + " pcs" + ")</option>"
                            }
                        })
                        ddu += '</select>';
                        $(".UnitType", row).html(ddu);
                        row.find(".UnitType > select").val($(this).find('Unit').text()).change();
                        $(".ReqQty", row).html("<input type='text' onkeypress='return isNumberKey(event)' class='form-control input-sm border-primary text-center' runat='server' style='width:100px;' onkeyup='rowCal(this)' value='" + $(this).find('Qty').text() + "'/>");
                        $(".TtlPcs", row).html($(this).find('TotalPieces').text());
                        $(".PackdQty", row).html($(this).find('PackedQty').text());
                        if ($(this).find('L_UnitType').text() != "") {
                            $(".PackdUnit", row).html($(this).find('L_UnitType').text() + " (" + $(this).find('L_QtyPerUnit').text() + ")");
                        }
                        $(".PackdPieces", row).html($(this).find('TotalPackedPieces').text());
                        $(".Action", row).html("<a href='javascript:;' id='deleterow' onclick='deleteItemrecord(this)'><span class='ft-x'></span></a>");
                        $('#tblProductDetail tbody').append(row);
                        row = $("#tblProductDetail thead tr:last").clone(true);
                    });
                    countRow();

                }
            }
        }
    });
}

function EditProductPrice(e) {
    var row = $(e).closest("tr");
    var CPrice = parseFloat(row.find(".CCostPrice").text())
    var Price = parseFloat(row.find(".CostPrice > input").val())
    var ProductId = parseInt(row.find(".ProName > span").attr("ProductAutoId"))
    var UnitType = parseInt(row.find(".UnitType > span").attr("UnitAutoId"))
    managePrice(ProductId, UnitType);
}

function PopupManagePrice(e) {
  debugger
    var row = $(e).closest("tr");
   
    var CPrice = parseFloat(row.find(".CCostPrice").text())
    var Price = parseFloat(row.find(".CostPrice > input").val())
    var ProductId = parseInt(row.find(".ProName > span").attr("ProductAutoId"))
    var UnitType = parseInt(row.find(".UnitType > span").attr("UnitAutoId"))
    if (CPrice == Price) {
        UpdatePriceProductWise(ProductId, UnitType, Price);
    } else {
        toastr.error("New cost price and current cost price should be equal. ", 'error', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        row.find(".CostPrice > input").val('');
        $(this).find('.CostPrice input').addClass('border-warning');
    }
    //if (CPrice != Price && row.find(".CostPrice > input").val() != '') {
    //    managePrice(ProductId, UnitType,Price)
    //}
    //else if (CPrice == Price && row.find(".CostPrice > input").val() != '') {
        //UpdatePriceProductWise(ProductId, UnitType, Price)
    //}
}
function UpdatePriceProductWise(ProductId, UnitType, CostPrice) {
    var data = {
        ProductAutoId: ProductId,
        UnitAutoId: UnitType,
        CostPrice: CostPrice,
        POAutoId: $("#txtPOAutoId").val()
    }
    $.ajax({
        type: "POST",
        url: "RecievePoByVendor.aspx/UpdateCostPriceOnPO",
        data: "{'DataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d == "true") {
                    //toastr.success("Price has been saved as draft.", 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                }
            }
            else {
                location.href = '/';
            }

        },
        error: function (result) {
        },
        failure: function (result) {
            swal("Error!", 'Oops,some thing went wrong.Please try later.', "error");
        }
    });
}

function managePrice(ProductId, UnitType) {
    console.log('hello');
    $.ajax({
        type: "POST",
        url: "RecievePoByVendor.aspx/getManagePrice",
        data: "{'ProductAutoId':'" + ProductId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var packingDetails = $(xmldoc).find("Table");
                var product = $(xmldoc).find("Table1");
                //ProductText
                $("#Table1 tbody tr").remove();
                var row = $("#Table1 thead tr").clone();
                $(packingDetails).each(function (index) {
                    $(".UnitType", row).html($(this).find('UnitName').text() + "<span UnitType=" + $(this).find('UnitType').text() + "></span>");
                    $(".Qty", row).html($(this).find('Qty').text());
                    if ($(this).find('UnitType').text() == '3') {
                        $(".NewCosePrice", row).html("<input type='text' style='width:80px;text-align:right' onchange='funchangeprice(this)' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm UnitNewCost'  value='" + $(this).find('CostPrice').text() + "'/>");
                    } else {
                        $(".NewCosePrice", row).html("<input type='text' style='width:80px;text-align:right' onchange='funchangeprice(this)' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm'  value='" + $(this).find('CostPrice').text() + "'/>");
                    }
                    $(".CostPrice", row).html($(this).find('CostPrice').text());
                    $(".MinPrice", row).html($(this).find('MinPrice').text());
                    $(".BasePrice", row).html($(this).find('Price').text());
                    if ($(this).find('UnitType').text() == '3') {
                        $(".NewBasePrice", row).html("<input type='text' style='width:80px;text-align:right' onchange='funchangeprice(this)' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm UnitBasePrice'  value='" + $(this).find('Price').text() + "'/>");
                    } else {
                        $(".NewBasePrice", row).html("<input type='text' style='width:80px;text-align:right' onchange='funchangeprice(this)' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm'  value='" + $(this).find('Price').text() + "'/>");
                    }
                    $(".RetailPrice", row).html($(this).find('SRP').text());
                    if ($(this).find('UnitType').text() == '3') {
                        $(".NewRetailPrice", row).html("<input type='text' style='width:80px;text-align:right' onchange='funchangeprice(this)' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm UnitRetailPrice'  value='" + $(this).find('SRP').text() + "'/>");
                    } else {
                        $(".NewRetailPrice", row).html("<input type='text' style='width:80px;text-align:right' onchange='funchangeprice(this)' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm'  value='" + $(this).find('SRP').text() + "'/>");
                    }
                    $(".WHminPrice", row).html($(this).find('WHminPrice').text());
                    if ($(this).find('UnitType').text() == '3') {
                        $(".NWHPrice", row).html("<input type='text' style='width:80px;text-align:right'  onchange='funchangeprice(this)'  onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm UnitWHPrice'  value='" + $(this).find('WHminPrice').text() + "'/>");
                    } else {
                        $(".NWHPrice", row).html("<input type='text' style='width:80px;text-align:right'  onchange='funchangeprice(this)'  onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm'  value='" + $(this).find('WHminPrice').text() + "'/>");
                    }
                    if (parseInt($(this).find('UnitType').text()) != UnitType) {
                        row.find('input').attr("disabled", true);
                        row.find('td').css("background-color", 'white');
                    }
                    else {
                        row.find('input').attr("disabled", false);
                        row.find('td').css("background-color", '#79f3c2');
                    }
                    $("#Table1 tbody").append(row);
                    row = $("#Table1 tbody tr:last-child").clone();
                    $("#ProductText").html($(product).find('ProductName').text());
                    $("#txtHProductAutoId").val(ProductId);

                });
            } else {
                location.href = '/';
            }
        },
        error: function (result) {
        },
        failure: function (result) {
        }
    });
    $('#managePrice').modal('show');
}
function funchangeprice(e) {
    var className = $(e).closest('td').attr('class');
    var tr = $(e).closest('tr');
    var UnitAutoId = tr.find('.UnitType span').attr('UnitType');
    var Price = 0.00;
    if ($(e).val() != '') {
        Price = $(e).val() || 0.00;
    }
    var Qty = parseInt(tr.find('.Qty').text());
    var UnitPrice = 0.00;
    UnitPrice = parseFloat((parseFloat(Price) / Qty)).toFixed(3) || 0.00;
    $("#Table1 tbody tr").each(function () {
            var amount = (parseInt($(this).find('.Qty').text()) * UnitPrice);
            if (UnitAutoId != $(this).find('.UnitType span').attr('UnitType')) {
                if (className == 'NewCosePrice') {
                    $(this).find('.NewCosePrice input').val(parseFloat(amount).toFixed(3));
                } else if (className == 'NewBasePrice') {
                    $(this).find('.NewBasePrice input').val(parseFloat(amount).toFixed(3));
                } else if (className == 'NewRetailPrice') {
                    $(this).find('.NewRetailPrice input').val(parseFloat(amount).toFixed(3));
                } else if (className == 'NWHPrice') {
                    $(this).find('.NWHPrice input').val(parseFloat(amount).toFixed(3));
                }
            }
        
    });
}

function updatebulk() {
    var prc = false;
    $("#Table1 tbody tr").each(function () {
            var Price = Number($(this).find('input').val());
            if (Price == 0 || Price == null) {
                prc = true;
                $(this).find('.NewCosePrice input').addClass('border-warning');
                $(this).find('.NewBasePrice input').addClass('border-warning');
                $(this).find('.NewRetailPrice input').addClass('border-warning');
                $(this).find('.NWHPrice input').addClass('border-warning');
            }
    })
    if (prc) {
        toastr.error('Please manage price before update.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    var ProductAutoId = $("#txtHProductAutoId").val();
    var dataTable = [];
    var chk1 = false; i = 0;
    $('#Table1 tbody tr').each(function (index) {
            dataTable[i] = new Object();
            dataTable[i].ProductAutoId = ProductAutoId,
            dataTable[i].UnitAutoId = $(this).find('.UnitType span').attr('UnitType');
            dataTable[i].CostPrice = $(this).find('.NewCosePrice input').val();
            dataTable[i].BasePrice = $(this).find('.NewBasePrice input').val();
            dataTable[i].RetailPrice = $(this).find('.NewRetailPrice input').val();
            dataTable[i].WHPrice = $(this).find('.NWHPrice input').val();
            if (parseFloat($(this).find('.NewCosePrice input').val()) > parseFloat($(this).find('.NWHPrice input').val())) {
                chk1 = true;
                $(this).find('.NewCosePrice input').addClass('border-warning');
            } else {
                $(this).find('.NewCosePrice input').removeClass('border-warning');
            }
            if (parseFloat($(this).find('.NWHPrice input').val()) > parseFloat($(this).find('.NewRetailPrice input').val())) {
                chk1 = true;
                $(this).find('.NWHPrice input').addClass('border-warning');
            } else {
                $(this).find('.NWHPrice input').removeClass('border-warning');
            }
            if (parseFloat($(this).find('.NewRetailPrice input').val()) > parseFloat($(this).find('.NewBasePrice input').val())) {
                chk1 = true;
                $(this).find('.NewRetailPrice input').addClass('border-warning');
            } else {
                $(this).find('.NewRetailPrice input').removeClass('border-warning');
            }

            i = Number(i) + 1;
    });
    if (chk1) {
        swal("", "Please check highlighted point.", "error");
        return;
    }
    else {
    $.ajax({
        type: "POST",
        url: "RecievePoByVendor.aspx/UpdateManagePrice",
        data: "{'dataTable':'" + JSON.stringify(dataTable) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d == "true") {
                    $('#managePrice').modal('hide');
                    swal("", "Packing details has been updated successfully.", "success");
                    Binddata(getid, RecAutoId);
                } else {
                    swal("", "Packing details not updated.", "success");
                }
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
        },
        failure: function (result) {
            swal("Error!", 'Oops,some thing went wrong.Please try later.', "error");
        }
    });
    }

}




function RevertPOBYIM() {
    swal({
        title: "Are you sure?",
        text: "You want to revert this PO Order",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, Revert it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            RevertPO();
        }
    })
}
function RevertPO() {
    debugger;
    $.ajax({
        type: "POST",
        url: "/POVendor/WebAPI/WPurchaseOrderVendor.asmx/RevertPOS",
        data: "{'POAutoId':'" + getid + "','Remark':'" + $("#txtInvManager_Remark").val() + "','RecAutoId':'" + RecAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: true,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == "Session Expired") {
                location.href = "/";
            }
            else if (response.d == "true") {
                swal("", "PO Revert successfully.", "success").then(function () {
                    window.location.href = "/POVendor/POStockListInventoryManager.aspx";
                });
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function PartialPO() {
    swal({
        title: "Are you sure?",
        text: "You want to receive this PO stock",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, Receive it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            ReceivePOStockByManager();
        }
    })
}
function ReceivePOStockByManager() {
   
     var prc = false;
    $("#tblProductDetail tbody tr").each(function () {
        debugger;
        var Price = Number($(this).find('input').val());
        if ($(this).find('.RecievedPieces').text()!=0) {
            debugger;
            if (Price == 0 || Price == null) {
                prc = true;
                $(this).find('.CostPrice input').addClass('border-warning');

            } else {
                $(this).find('.CostPrice input').removeClass('border-warning');
            }
        }
    })
    if ($("#txtBillno").val() == "") {
        toastr.error('Bill No is mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $("#txtBillno").addClass('border-warning')
        $("#txtBillno").focus();
    }
    else if ($("#txtBilldate").val() == "") {
        toastr.error('Bill-Date is mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $("#txtBilldate").addClass('border-warning');
        $("#txtBilldate").focus();
        $("#txtBillno").removeClass('border-warning');
    }
    else if (prc) {
        $("#txtBillno").removeClass('border-warning');
        $("#txtBilldate").removeClass('border-warning');
        toastr.error('Please fill all items price than revieve stock.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }

    else {
        $("#txtBillno").removeClass('border-warning')
        $("#txtBilldate").removeClass('border-warning')
        var data = {
            ManagerRemark: $("#txtInvManager_Remark").val(),
            POAutoId: $("#txtPOAutoId").val(),
            RecAutoId: RecAutoId,
            BillNo: $('#txtBillno').val(),
            BillDate: $('#txtBilldate').val(),
        }
        $.ajax({
            type: "POST",
            url: "RecievePoByVendor.aspx/PartialRecievePO",
            data: "{'DataValues':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d != "Session Expired") {
                    if (response.d == "true") {
                        swal("", "Stock has been received succesfully.", "success");
                        Binddata(getid, RecAutoId);
                    } else {
                        swal("Error!", response.d, "error");
                    }
                }
                else {
                    location.href = '/';
                }

            },
            error: function (result) {
            },
            failure: function (result) {
                swal("Error!", 'Oops,some thing went wrong.Please try later.', "error");
            }
        });
    }
}
function RedirectOnPODetails() {
    location.href = '/POVendor/PODetails.aspx?PageId=' + getid + '';
}


