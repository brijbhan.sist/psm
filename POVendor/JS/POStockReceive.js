﻿var PageId = 0; var RecAutoId = 0; var getlocalVal = 0;
$(document).ready(function () {
    $('.date').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    PageId = getQueryString('PageId');
    RecAutoId = getQueryString('RecAutoId');
    getlocalVal = localStorage.getItem('PORecieve');
    if (PageId != null && getlocalVal == '1') {
        localStorage.removeItem('RecieveAutoId');
        $('#hfRecAutoId').val(RecAutoId);
        ViewPoDetails(PageId);
    }
    if (PageId != null && (localStorage.getItem('RecieveAutoId')) != '' && localStorage.getItem('RecieveAutoId') != null) {
        $('#hfRecAutoId').val(localStorage.getItem('RecieveAutoId'));
        ViewPoDetails(PageId);
    }
});
function ViewPoDetails(PageId) {
    var data = {
        AutoId: PageId,
        RecAutoId: $('#hfRecAutoId').val(),
    };
    $.ajax({
        type: "POST",
        url: "/POVendor/WebAPI/WPurchaseOrderVendor.asmx/ReceivePo",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var Order = $(xmldoc).find("Table");
            var Product = $(xmldoc).find("Table1");
            var unitType = $(xmldoc).find("Table2");
            var RecDetails = $(xmldoc).find("Table3");
            $("#btnGeneratePO").show();
            $("#btnSaveAsDraft").show();
            $("#btnUpdateDraft").show();
            $("#btnReset").show();

            if (RecDetails.length > 0) {
                $('#ReceiveNo').val($(RecDetails).find('PORecieveId').text());
                $('#ddlRecStatus').val($(RecDetails).find('RecStatus').text());
                $('#txtRecDate').val($(RecDetails).find('ReceiveDate').text());
                $("#txtReceiveRemark").val($(RecDetails).find("Remarks").text());
                $("#txtRecBy").val($(RecDetails).find("ReceivedBy").text());
                if ($(RecDetails).find("Status").text() == "2" || $(RecDetails).find("Status").text() == "3" || $(Order).find("SAutoId").text() == '3' || $(Order).find("SAutoId").text() == '4') {
                    $("#btnGeneratePO").hide();
                    $("#btnSaveAsDraft").hide();
                    $("#btnUpdateDraft").hide();
                    $("#btnReset").hide();
                    $("#btnNewRec").show();
                    $("#txtReceiveRemark").attr('disabled', true);
                    $("#txtBillNo").attr('disabled', true);
                    $("#txtBillDate").attr('disabled', true);
                    $("#txtRecDate").attr('disabled', true);
                }
            }
            if (Order.length > 0) {
                $("#txtPoSequenceNo").val($(Order).find("PONo").text());
                $("#txtGenerateDate").val($(Order).find("PODate").text());
                $("#txtVendor").val($(Order).find("VendorName").text());
                $("#txtRemark").val($(Order).find("PORemarks").text());
                $("#ddlStatus").val($(Order).find("Status").text());
                $("#txtBillNo").val($(Order).find("BillNo").text());
                $("#txtBillDate").val($(Order).find("BillDate").text());

            }
            $("#tblProductDetail tbody tr").remove();
            var row = $("#tblProductDetail thead tr:last-child").clone(true);
            if (Product.length > 0) {
                $.each(Product, function (index) {
                    var pid = $(this).find('ProductAutoId').text();
                    var ddu = '<select class="form-control input-sm border-primary ddlreq" onchange="rowCal(this)" style="width: 130px;">';
                    $.each(unitType, function () {
                        var pid1 = $(this).find('ProductAutoId').text()
                        if (pid1 == pid) {
                            ddu += "<option value='" + $(this).find("AutoId").text() + "' EligibleforFree='" + $(this).find("EligibleforFree").text() + "' QtyPerUnit='" + $(this).find("Qty").text() + "' CostPrice='" + $(this).find("CostPrice").text() + "'>" + $(this).find("UnitType").text() + " (" + $(this).find("Qty").text() + " pcs" + ")</option>"
                        }
                    })
                    ddu += '</select>';
                    $(".RecUnitType", row).html(ddu);




                    $(".ProductId", row).html('<span ProductAutoId=' + $(this).find("ProductAutoId").text() + '>' + $(this).find("ProductId").text() + '</span>');
                    $(".ProductName", row).html($(this).find("ProductName").text());
                    $(".UnitType", row).html('<span Qty=' + $(this).find("QtyPerUnit").text() + ' value=' + $(this).find("Unit").text() + '>' + $(this).find("UnitType").text() + '</span>');
                    $(".ReqQty", row).html($(this).find("Qty").text());
                    $(".RemainQty", row).html($(this).find("RemainQty").text());
                    $(".OtherReceivedPieces", row).html($(this).find("OtherRecivedPieces").text());
                    $(".TtlPcs", row).html($(this).find("TotalPieces").text());
                    if ($('#hfRecAutoId').val() == '' || $(this).find('RecUnitAutoId').text() == 0) {
                        $(".RecivedQty", row).html('<input style="text-align:center" onchange="rowCal(this)" class="form-control border-primary input-sm" maxlength="5" type="text" value=' + 0 + '>');
                        $(".RecivedPiece", row).html('0');
                        row.find(".RecUnitType > select").val($(this).find('Unit').text());
                    } else {
                        $(".RecivedQty", row).html('<input style="text-align:center" onchange="rowCal(this)" class="form-control border-primary input-sm" maxlength="5" type="text" value=' + $(this).find('RecivedQty').text() + '  recqty=' + $(this).find('RecivedQty').text() + '>');
                        $(".RecivedPiece", row).html($(this).find("ReceivedPiece").text());
                        row.find(".RecUnitType > select").val($(this).find('RecUnitAutoId').text());
                    }
                    //if ($(this).find('RecUnitAutoId').text() == 0) {
                    //    row.find(".RecUnitType > select").val($(this).find('Unit').text());
                    //}
                    if ($(RecDetails).find("Status").text() == "2" || $(RecDetails).find("Status").text() == "3") {
                        $(".RecivedQty", row).html('<input style="text-align:center" onchange="rowCal(this)" class="form-control border-primary input-sm" disabled type="text" value=' + $(this).find('RecivedQty').text() + '  recqty=' + $(this).find('RecivedQty').text() + '>');
                        row.find(".RecUnitType > select").attr('disabled', true);
                    }

                    $("#tblProductDetail tbody").append(row);
                    row = $("#tblProductDetail tbody tr:last").clone(true);
                });
            }
            if ($("#ddlRecStatus").val() == 'Submit') {
                var Check = 0;
                $('#tblProductDetail tbody tr').each(function (index) {
                    if ($(this).find('.RemainQty').text() != 0) {
                        Check = 1;
                    }
                });
                if (Check == 0) {
                    $("#drag-area3").hide();
                } else {
                    $("#drag-area3").show();
                }
            }
            
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function AutoUpdatePO(e) {
    var row = $(e).closest("tr");
    var ReveivedQty = row.find(".RecivedQty > input").val();
    var ProductAutoId = row.find(".ProductId span").attr("ProductAutoId");
    var UnitAutoId = row.find(".UnitType span").attr("value");
    var RecUnitType = row.find(".RecUnitType option:selected").attr("value");
    var QtyPerUnit = row.find(".RecUnitType option:selected").attr("qtyperunit");
    var data = {
        POVNumber: $("#txtPoSequenceNo").val(),
        ProductAutoId: ProductAutoId,
        UnitAutoId: UnitAutoId,
        ReveivedQty: ReveivedQty,
        Remark: $("#txtReceiveRemark").val(),
        ReceiveAutoId: $("#hfRecAutoId").val(),
        RecUnitAutoId: RecUnitType,
        QtyUnit: QtyPerUnit,
        //BillNo: $('#txtBillNo').val(),
        //BillDate: $('#txtBillDate').val(),
        //RecDate: $('#txtRecDate').val(),
    };
    $.ajax({
        type: "POST",
        url: "/POVendor/WebAPI/WPurchaseOrderVendor.asmx/AutoUpdatePo",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var Details = $(xmldoc).find("Table");
            if (Details.length > 0) {
                localStorage.removeItem('PORecieve');
                localStorage.setItem("RecieveAutoId", $(Details).find('RecAutoId').text());
                $('#hfRecAutoId').val(localStorage.getItem('RecieveAutoId'));
                ViewPoDetails(PageId);
            }
        }
    })
}
function rowCal(e) {
    var TotalRecQty = 0;
    debugger;
    var row = $(e).closest("tr");
    row.find(".RecivedQty > input").addClass("border-warning");
    TotalRecQty = parseInt(row.find(".RecivedQty > input").attr("recqty")) || 0;
    var TotalRecPieces = (parseInt(row.find(".RecUnitType option:selected").attr("qtyperunit")) * parseInt(row.find(".RecivedQty > input").val()));
    if (TotalRecPieces > parseInt(row.find(".RemainQty").text()) + parseInt(row.find(".RecivedPiece").text())) {
        toastr.error("Received qty can not greater then Requested qty.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        row.find(".RecivedQty > input").addClass("border-warning");
        row.find(".RecivedQty > input").focus();
        row.find(".RecivedQty > input").val(TotalRecQty);
    }
    else {
        //$(this).find(".RecivedQty input").attr("recqty", parseInt(row.find(".RecivedQty > input").val()));
        row.find(".RecivedQty > input").removeClass("border-warning");
        //if (parseInt(row.find(".RecivedQty > input").val() != '')){
        var totalPcs = (parseInt(row.find(".RecivedQty > input").val() || 0) * parseInt(row.find(".RecUnitType option:selected").attr("qtyperunit")));
        row.find(".RecivedPiece").html(totalPcs);
        AutoUpdatePO(e);
        //}

    }

}
function ConfirmForwordPO(e) {
    var check = 0;
    var deff = 0;
    var Req = 0;
    if (e == "2" || e == "3") {
        if ($("#txtReceiveRemark").val() == "") {
            check = 1;
        }
    }
    $('#tblProductDetail tbody tr').each(function (index) {
        if ($(this).find('.ReqQty').text() != $(this).find('.RecivedQty >input').val()) {
            deff = 1;
        }
        if ($(this).find('.RecivedQty >input').val() != 0) {
            Req = 1;
        }
    });

    if (check == 1) {
        toastr.error("Remark is required.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $("#txtReceiveRemark").addClass("border-warning");
    }
    else if (Req == 0) {
        $("#txtReceiveRemark").removeClass("border-warning");
        toastr.error("Please receive qty at least one product .", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
    else if (deff == 1) {
        $("#txtReceiveRemark").removeClass("border-warning");
        swal({
            title: "Are you sure ?",
            text: "Do you want to submit without receiving all requested quantity?",
            icon: "warning",
            showCancelButton: true,
            buttons: {
                cancel: {
                    text: "No, Cancel!",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: true,
                },
                confirm: {
                    text: "Yes, Submit it!",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: true

                }
            }
        }).then(function (isConfirm) {
            if (isConfirm) {
                ReceiveByStatus(e)
            }
        })
    }
    else {
        swal({
            title: "Are you sure ?",
            text: "Do you want to Submit ?",
            icon: "warning",
            showCancelButton: true,
            buttons: {
                cancel: {
                    text: "No, Cancel!",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: true,
                },
                confirm: {
                    text: "Yes, Submit it!",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: true
                }
            }
        }).then(function (isConfirm) {
            if (isConfirm) {
                ReceiveByStatus(e)
            }
        })
    }

}

function ReceiveByStatus(e) {
    debugger
    var check = 0; recdate = 0;
    if (e == "2") {
        if ($("#txtReceiveRemark").val() == "") {
            check = 1;
        }
        if ($("#txtRecDate").val() == "") {
            recdate = 1;
        }
    }
    if (check == 1) {
        toastr.error("Remark is reqired.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $("#txtReceiveRemark").addClass("border-warning");
    }
    else if (recdate == 1) {
        toastr.error("Receive Date is reqired.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $("#txtRecDate").addClass("border-warning");
    }
    else {
        $("#txtReceiveRemark").removeClass("border-warning");
        $("#txtRecDate").removeClass("border-warning");

        var data = {
            Status: e,
            PONumber: PageId,
            ReceiverRemark: $("#txtReceiveRemark").val(),
            RecAutoId: $('#hfRecAutoId').val(),
            BillNo: $('#txtBillNo').val(),
            BillDate: $('#txtBillDate').val(),
            RecDate: $('#txtRecDate').val(),
        };
        $.ajax({
            type: "POST",
            url: "/POVendor/WebAPI/WPurchaseOrderVendor.asmx/ReceiveByStatus",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d == "true") {
                    $("#txtReceiveRemark").removeClass("border-warning");
                    var msg = "";
                    if (e == 1) {
                        swal("", "PO Save as Draft successfully.", "success").then(function () {
                            if ($("#hdnEmpAutoId").val() == 'Manager') {
                                window.location.href = "/POVendor/POStockListInventoryManager.aspx";
                            } else {
                                window.location.href = "/POVendor/ReceiveStockList.aspx";
                            }
                        });
                    }
                    else {
                        //msg = "PO submitted successfully."
                        //ViewPoDetails(PageId);
                        //toastr.success(msg, 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                        swal("", "PO submitted successfully.", "success").then(function () {
                            window.location.href = "POStockReceive.aspx?PageId=" + PageId;
                        });
                    }

                }
            }
        })
    }

}
function RedirectOnPODetails() {
    location.href = '/POVendor/PODetails.aspx?PageId=' + PageId + '';
}
function NewRecieve() {
    localStorage.setItem("PORecieve", '1');
    location.href = '/POVendor/POStockReceive.aspx?PageId=' + PageId + '';
}
