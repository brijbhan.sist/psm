﻿var VendorDrp = '';
$(document).ready(function () {
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    $("#txtGenerateDate").val(month + '/' + day + '/' + year);
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    bindDropdown();
    BindProductDropDown();
    var PageId = getQueryString('PageId');
    if (PageId != null) {
        EditDraft(PageId);
    }
});
function bindDropdown() {
    $.ajax({
        type: "POST",
        url: "/POVendor/WebAPI/WPurchaseOrderVendor.asmx/bindDropDowns",
        async: false,
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var Vendor = $(xmldoc).find("Table");
            //var Product = $(xmldoc).find("Table1");
            VendorDrp = '<select class="form-control input-sm border-primary ddlreq"  onchange="rowCal(this,0)"  style="width: 155px;">';
            $.each(Vendor, function () {
                VendorDrp += "<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("VendorName").text() + "</option>"
            })
            VendorDrp += '</select>';

            //$("#ddlPoVendor option:not(:first)").remove();
            //$.each(Vendor, function () {
            //    $("#ddlPoVendor").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("VendorName").text() + "</option>");
            //});
            //$("#ddlPoVendor").select2();

          
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function BindProductDropDown() {
    $.ajax({
        type: "POST",
        url: "/POVendor/WebAPI/WPurchaseOrderVendor.asmx/BindProductDropDown",
        async: false,
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var DropDown = $.parseJSON(response.d);
            var ddlProduct = $("#ddlProduct");

            for (var i = 0; i < DropDown.length; i++) {
                var AllDropDownList = DropDown[i];
                $("#ddlProduct option:not(:first)").remove();
                var Product = AllDropDownList.Product;
                for (var j = 0; j < Product.length; j++) {
                    var ProductList = Product[j];
                    var option = $("<option />");
                    option.html(ProductList.ProductName);
                    option.val(ProductList.AutoId);
                    option.attr('ID', ProductList.ProductId);
                    option.attr('Name', ProductList.ProductName);
                    option.attr('vendor', ProductList.VendorAutoId);
                    ddlProduct.append(option);
                }
                ddlProduct.select2();
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function bindProductDetails() {

    var BUnitAutoId = 0;
    var validcheck = 0;
    if ($("#ddlProduct").val() !== '0') {
        $.ajax({
            type: "POST",
            url: "/POVendor/WebAPI/WPurchaseOrderVendor.asmx/bindProductDetails",
            data: "{'productAutoId':" + $("#ddlProduct").val() + "}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            async: false,
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d != "Session Expired") {
                    var xmldoc = $.parseXML(response.d);
                    var unitType = $(xmldoc).find("Table");
                    var unitDefault = $(xmldoc).find("Table1");
                    var count = 0;
                    $("#txtReqQty").val('1');
                    $("#ddlUnitType option:not(:first)").remove();
                    $.each(unitType, function () {
                        $("#ddlUnitType").append("<option EligibleforFree='" + $(this).find("EligibleforFree").text() + "' value='" + $(this).find("AutoId").text() + "' QtyPerUnit='" + $(this).find("Qty").text() + "' CostPrice='" + $(this).find("CostPrice").text() + "'>" + $(this).find("UnitType").text() + " (" + $(this).find("Qty").text() + " pcs" + ")</option>");

                    });
                    if (unitDefault.length > 0) {
                        if (BUnitAutoId == 0) {
                            $('#ddlUnitType option').each(function () {
                                if (this.value == $(unitDefault).find('AutoId').text()) {
                                    $("#ddlUnitType").val($(unitDefault).find('AutoId').text()).change();
                                }
                            });
                        } else {
                            $('#ddlUnitType option').each(function () {
                                if (this.value == BUnitAutoId) {
                                    $("#ddlUnitType").val(BUnitAutoId).change();
                                }
                            });
                            BUnitAutoId = 0;
                        }
                        //$("#txtTotalPieces").val($("#ddlUnitType option:selected").attr("QtyPerUnit"));
                    } else {
                        $("#ddlUnitType").val(0).change();
                    }
                } else {
                    location.href = '/';
                }

            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });

    } else {
        $('#chkFreeItem').removeAttr('disabled');
        $("#ddlUnitType option:not(:first)").remove();
        $("#txtTotalPieces").val('');
        $("#txtReqQty").val('1');
    }
}
function AddProductList() {
    debugger
    var reqQty = parseInt($("#txtReqQty").val()) || 0;
    var chkcount = 0;
    if (reqQty == 0) {
        toastr.error('Please enter a valid quantity.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $("#txtReqQty").addClass('border-warning');
        return;
    }
    else {
        $("#txtReqQty").removeClass('border-warning');
    }
    var flag1 = false;
    var validatecheck = dynamicInputTypeSelect2('selectvalidate');
    if (!validatecheck) {
        dynamicALL('Areq');
    }
    else {
        validatecheck = dynamicALL('Areq');
    }
    if (validatecheck) {
        var productAutoId = $("#ddlProduct option:selected").val();
        var unitAutoId = $("#ddlUnitType option:selected").val();
        var VendorID = $("#ddlProduct option:selected").attr("vendor");
        var Draftdata = {
            POVDate: $('#txtGenerateDate').val(),
            VendorId: $("#ddlProduct option:selected").attr("vendor"),
            Remark: $('#txtRemark').val(),
            ProductAutoId: productAutoId,
            UnitAutoId: unitAutoId,
            RequiredQty: $("#txtReqQty").val(),
            QtyPerUnit: $("#ddlUnitType option:selected").attr("QtyPerUnit"),
            CostPrice: $("#ddlUnitType option:selected").attr("costPrice"),
            DraftAutoId: $("#hfDraftAutoId").val(),
        }
        $.ajax({
            type: "POST",
            url: "/POVendor/WebAPI/WPurchaseOrderVendor.asmx/AddPOVItem",
            data: "{'data':'" + JSON.stringify(Draftdata) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            async: true,
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d != "Session Expired") {
                    if (response.d == "false") {
                        return;
                    }
                    else if (response.d == "ambiguity") {
                        toastr.error("You cannot add multiple unit of a product.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                        return;
                    }
                    var xmldoc = $.parseXML(response.d);
                    var product = $(xmldoc).find("Table");
                    var unitType = $(xmldoc).find("Table1");
                    var ItemDetails = $(xmldoc).find("Table2");
                    if (ItemDetails.length > 0) {
                        $('#hfDraftAutoId').val($(ItemDetails).find('DraftPOVId').text());
                        if ($(product).find('ProductId').text() != "" && $(product).find('ProductId').text() != null) {
                            $("#tblProductDetail tbody tr").each(function () {
                                if ($(this).find(".ProName > span").attr("productautoid") == productAutoId && $(this).find(".UnitType > select").val() == unitAutoId) {
                                    var Qtyreq = $("#txtReqQty").val();
                                    var reqQty = Number($(this).find(".ReqQty input").val()) + Number(Qtyreq);
                                    $(this).find(".ReqQty input").val(reqQty);
                                    $(this).find(".ReqQty input").attr("TotReqQty", reqQty);
                                    flag1 = true;
                                    rowCal($(this).find(".ReqQty > input"), 0);
                                    $('#tblProductDetail tbody tr:first').before($(this));
                                    countRow();
                                    toastr.success('Product added successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                                }
                            });
                            var row = $("#tblProductDetail thead tr").clone(true);
                            var TCost = 0.00;
                            if (!flag1) {
                                $(".ProId", row).html($(product).find('ProductId').text());
                                $(".ProName", row).html("<span ProductAutoId='" + productAutoId + "'>" + $(product).find('ProductName').text() + "</span>");
                                var ddu = '<select class="form-control input-sm border-primary ddlreq" onchange="rowCal(this,0)"  style="width: 130px;">';
                                $.each(unitType, function () {
                                    ddu += "<option value='" + $(this).find("AutoId").text() + "' EligibleforFree='" + $(this).find("EligibleforFree").text() + "' QtyPerUnit='" + $(this).find("Qty").text() + "' CostPrice='" + $(this).find("CostPrice").text() + "'>" + $(this).find("UnitType").text() + " (" + $(this).find("Qty").text() + " pcs" + ")</option>"
                                })
                                ddu += '</select>';
                                $(".UnitType", row).html(ddu);
                                row.find(".UnitType > select").val(unitAutoId).change();
                                $(".VendorName", row).html(VendorDrp);
                                row.find(".VendorName > select").val(VendorID).change();
                               
                                $(".ReqQty", row).html("<input type='text' onkeypress='return isNumberKey(event)' class='form-control input-sm border-primary text-center' runat='server' maxlength='5' style='width:70px;' onkeyup='rowCal(this,1)' value='" + $("#txtReqQty").val() + "'  TotReqQty='" + $("#txtReqQty").val() + "'/>");
                                $(".TtlPcs", row).text(parseInt($("#txtReqQty").val()) * $(product).find('UnitQty').text());
                                $(".Action", row).html("<a href='javascript:;' id='deleterow' onclick='deleteProduct(this)'><span class='ft-x'></span></a>");
                                var CostPrice = $(row).find(".UnitType select option:selected").attr("costprice");
                                $(".CostPrice", row).text(parseInt(CostPrice).toFixed(2));
                                var TotalCostPrice = parseFloat(CostPrice) * (parseInt($(row).find(".TtlPcs").text()));
                                $(".TotalCostPrice", row).text(TotalCostPrice.toFixed(2));
                                CalculateFooter();
                                if ($('#tblProductDetail tbody tr').length > 0) {
                                    $('#tblProductDetail tbody tr:first').before(row);
                                }
                                else {
                                    $('#tblProductDetail tbody').append(row);
                                }
                                rowCal(row.find(".ReqQty > input"), 0);
                                countRow();
                                $("#txtQuantity, #txtTotalPieces").val("0");
                                if ($('#tblProductDetail tbody tr').length > 0) {
                                    $('#emptyTable').hide();
                                }
                                toastr.success('Product added successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                            }
                        }
                        $("#txtBarcode").val('');
                        $("#txtBarcode").focus();
                        $("#ddlProduct").select2('val', '0');
                        $("#ddlUnitType").val(0);
                        $("#txtReqQty").val("1");
                        $("#emptyTable").hide();
                        $("#txtTotalPieces").val('');
                    }
                } else {
                    location.href = '/';
                }
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
};
function rowCal(e, i) {
   
    debugger;
    var row = $(e).closest("tr");
    if (row.find(".VendorName > select option:selected").val() > 0) {
        row.find('.VendorName select').removeClass('border-warning');
    }
    if (parseInt(row.find(".ReqQty > input").val()) > 0) {
        var totalPcs = (parseInt(row.find(".ReqQty > input").val()) * parseInt(row.find(".UnitType > select option:selected").attr("QtyPerUnit")));
        row.find(".TtlPcs").html(totalPcs);
        row.find(".ReqQty input").attr("TotReqQty", (parseInt(row.find(".ReqQty > input").val())));
        var CostPrice = parseFloat(row.find(".UnitType > select option:selected").attr("costprice"));
        var TotalCostPrice = parseFloat(CostPrice) * (parseInt(totalPcs));
        row.find(".CostPrice").html(CostPrice.toFixed(2));
        row.find(".TotalCostPrice").html(TotalCostPrice.toFixed(2));
        CalculateFooter();
     
        var dataValue = {
            DraftAutoId: $('#hfDraftAutoId').val(),
            ProductAutoId: $(row).find('.ProName span').attr('productautoid'),
            UnitAutoId: $(row).find('.UnitType select option:selected').val(),
            QtyPerUnit: $(row).find(".UnitType select option:selected").attr("QtyPerUnit"),
            ReqQty: $(row).find('.ReqQty input').val(),
            Vendor: $(row).find('.VendorName select option:selected').val(),
            Price: $(row).find(".UnitType select option:selected").attr("costprice"),
        }

        $.ajax({
            type: "POST",
            url: "/POVendor/WebAPI/WPurchaseOrderVendor.asmx/AutoUpdatePODraftProduct",
            data: "{'dataValue':'" + JSON.stringify(dataValue) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            async: false,
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
              
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
       
    } else {

        if (i == 1) {
            var OldReqQty = Number(row.find(".ReqQty input").attr("TotReqQty"));
            row.find(".ReqQty > input").val(OldReqQty);
            swal("", "Quantity can not be zero", "error");
        }

    }


}
function dllCalculateTotalQty() {
    $("#txtReqQty").val("1");

    var qtyPerUnit = 0; var reqQty = 0; TotalPcs = 0;

    qtyPerUnit = Number($("#ddlUnitType option:selected").attr("qtyperunit")) || 0;
    reqQty = Number($("#txtReqQty").val());
    var TotalPcs = reqQty * qtyPerUnit;
    $("#txtTotalPieces").val(TotalPcs);
    
}
function calculateTotalQty() {
    var reqQty = 0; var defaultPiece = 0;
    reqQty = Number($("#txtReqQty").val());
    if (reqQty > 0) {
        defaultPiece = Number($("#ddlUnitType option:selected").attr("qtyperunit"));
        $("#txtTotalPieces").val(reqQty * defaultPiece);
    } else {
        $("#txtReqQty").val('1');
        swal("", "Quantity can not be zero", "error");
    }
  

}
function deleteProduct(e) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this product.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, Cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: false,
            },
            confirm: {
                text: "Yes, Delete it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            var tr = $(e).closest('tr');

            var data = {
                DraftAutoId: $("#hfDraftAutoId").val(),
                ProductAutoId: $(tr).find('.ProName span').attr('ProductAutoId'),
                UnitAutoId: $(tr).find(".UnitType > select option:selected").attr("QtyPerUnit")
            }
            if ($("#DraftAutoId").val() != '') {
                $.ajax({
                    type: "POST",
                    url: "/POVendor/WebAPI/WPurchaseOrderVendor.asmx/DeleteProductItem",
                    data: "{'DataValue':'" + JSON.stringify(data) + "'}",
                    contentType: "application/json; charset=utf-8",
                    datatype: "json",
                    async: false,
                    beforeSend: function () {
                        $.blockUI({
                            message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                            overlayCSS: {
                                backgroundColor: '#FFF',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (response) {
                        if (response.d != "Session Expired") {
                            swal("", "Item deleted successfully.", "success");
                        } else {
                            location.href = '/';
                        }
                        $(e).closest('tr').remove();

                    },
                    error: function (result) {
                        console.log(JSON.parse(result.responseText).d);
                    },
                    failure: function (result) {
                        console.log(JSON.parse(result.responseText).d);
                    }
                });
            } else {
                $(e).closest('tr').remove();
                swal("", "Item deleted successfully.", "success");
            }

            if ($("#tblProductDetail tbody tr").length == 0) {
                $('#tblProductDetail tfoot tr').empty();
                $("#emptyTable").show();
            } else {
                countRow();
                CalculateFooter();
                $("#emptyTable").hide();
            }
            swal("", "Item deleted successfully.", "success");

        } else {
            swal("", "Your product is safe.", "error");
        }
    })
}
function countRow() {
    var count = 0;
    $("#tblProductDetail tbody tr").each(function () {
        count++;
    });
    $('#TotalItem').html("Total Items : " + count);
}
function AddItemByBarcode() {

 
    var Barcode = $("#txtBarcode").val();
    if (Barcode != "") {

        var data = {
            Barcode: Barcode,
            DraftAutoId: $("#hfDraftAutoId").val(),
            Remark: $("#txtRemark").val(),
            ReqQty: "1",
        }
        $.ajax({
            type: "POST",
            url: "/POVendor/WebAPI/WPurchaseOrderVendor.asmx/AddPOVItemByBarcode",
            data: "{'data':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {

                if (response.d == "ambiguity") {
                    toastr.error("You cannot add multiple unit of a product.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    $("#txtBarcode").val('');
                    return;
                } else if (response.d == "false") {
                    swal({
                        title: "",
                        text: "Barcode does not exists.",
                        icon: "error",
                        closeOnClickOutside: false
                    }).then(function (isConfirm) {

                        if (isConfirm) {
                            $("#txtBarcode").val('');
                            $("#txtBarcode").focus();
                        }
                    });
                    $("#yes_audio")[0].play();
                    $("#panelProduct select").val(0);
                    $("#txtQuantity, #txtTotalPieces").val("0");
                    $("#alertBarcodeCount").hide();
                    $("#txtBarcode").val('');
                    $("#txtReqQty").focus();
                    $("#txtBarcode").blur();
                }
                else if (response.d != "Session Expired") {
                    var xmldoc = $.parseXML(response.d);
                    var product = $(xmldoc).find("Table");
                    var unitType = $(xmldoc).find("Table1");
                    var DID = $(xmldoc).find("Table2");
                    if (product.length > 0) {
                        $("#hfDraftAutoId").val($(DID).find('DraftPOVId').text());
                        var productAutoId = $(product).find('ProductAutoId').text();
                        var unitAutoId = $(product).find('UnitType').text();
                        var VendorID = $(product).find('VendorAutoId').text();
                        var flag1 = false;
                        if ($(product).find('ProductId').text() != "" && $(product).find('ProductId').text() != null) {
                            $("#tblProductDetail tbody tr").each(function () {
                                if ($(this).find(".ProName > span").attr("productautoid") == productAutoId && $(this).find(".UnitType > select").val() == unitAutoId) {
                                    var Qtyreq = 1;
                                    var reqQty = Number($(this).find(".ReqQty input").val()) + Number(Qtyreq);
                                    $(this).find(".ReqQty input").val(reqQty);
                                    $(this).find(".ReqQty input").attr("TotReqQty", reqQty);
                                    flag1 = true;
                                    rowCal($(this).find(".ReqQty > input"), 0);
                                    $('#tblProductDetail tbody tr:first').before($(this));
                                    countRow();
                                    toastr.success('Product added successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                                }
                            });
                            var row = $("#tblProductDetail thead tr").clone(true);
                            if (!flag1) {
                                $(".ProId", row).html($(product).find('ProductId').text());
                                $(".ProName", row).html("<span ProductAutoId='" + productAutoId + "'>" + $(product).find('ProductName').text() + "</span>");
                                $(".ReqQty", row).html("<input type='text' onkeypress='return isNumberKey(event)' class='form-control input-sm border-primary text-center' maxlength='5' runat='server' style='width:70px;' onkeyup='rowCal(this,1)' value='" + $("#txtReqQty").val() + "' TotReqQty='" + $("#txtReqQty").val() + "'/>");

                                var ddu = '<select class="form-control input-sm border-primary ddlreq" onchange="rowCal(this,0)"  style="width: 150px;">';
                                $.each(unitType, function () {
                                    ddu += "<option value='" + $(this).find("AutoId").text() + "' EligibleforFree='" + $(this).find("EligibleforFree").text() + "' QtyPerUnit='" + $(this).find("Qty").text() + "' CostPrice='" + $(this).find("CostPrice").text() + "'>" + $(this).find("UnitType").text() + " (" + $(this).find("Qty").text() + " pcs" + ")</option>"
                                })
                                ddu += '</select>';
                                $(".UnitType", row).html(ddu);
                                alert(VendorID);
                                row.find(".UnitType > select").val(unitAutoId).change();
                                $(".VendorName", row).html(VendorDrp);
                                row.find(".VendorName > select").val(VendorID).change();
                                $(".TtlPcs", row).text(parseInt($("#txtReqQty").val()) * $(product).find('UnitQty').text());
                                $(".Action", row).html("<a href='javascript:;' id='deleterow' onclick='deleteProduct(this)'><span class='ft-x'></span></a>");

                                var CostPrice = $(row).find(".UnitType select option:selected").attr("costprice");
                                $(".CostPrice", row).text(parseInt(CostPrice).toFixed(2));
                                var TotalCostPrice = parseFloat(CostPrice) * (parseInt($(row).find(".TtlPcs").text()));
                                $(".TotalCostPrice", row).text(TotalCostPrice.toFixed(2));
                                if ($('#tblProductDetail tbody tr').length > 0) {
                                    $('#tblProductDetail tbody tr:first').before(row);
                                }
                                else {
                                    $('#tblProductDetail tbody').append(row);
                                }
                                rowCal(row.find(".ReqQty > input"), 0);
                                countRow();
                                CalculateFooter();
                                $("#txtQuantity, #txtTotalPieces").val("0");
                                if ($('#tblProductDetail tbody tr').length > 0) {
                                    $('#emptyTable').hide();
                                }
                                toastr.success('Product added successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                            }
                        }
                        $("#txtBarcode").val('');
                        $("#txtBarcode").focus();

                    } else {
                        swal({
                            title: "",
                            text: "Barcode does not exists.",
                            icon: "error",
                            closeOnClickOutside: false
                        }).then(function (isConfirm) {

                            if (isConfirm) {
                                $("#txtBarcode").val('');
                                $("#txtBarcode").focus();
                            }
                        });
                        $("#yes_audio")[0].play();
                        $("#panelProduct select").val(0);
                        $("#txtQuantity, #txtTotalPieces").val("0");
                        $("#alertBarcodeCount").hide();
                        $("#txtBarcode").val('');
                        $("#txtReqQty").focus();
                        $("#txtBarcode").blur();
                    }
                }
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    } else {
        $("#ddlProduct").val(0).change();
        $("#alertBarcodeCount").hide();
    }

}
function Reset() {
    window.location.href = "/POVendor/GeneratePOV.aspx";
}
function SaveAsDraft() {
    var DraftItem = "";
    var xml = "";
    var tbllength = $('#tblProductDetail tbody tr').length;
    if ($("#ddlPoVendor").val() == "0") {
        $("#ddlPoVendor").addClass("border-warning");
        toastr.error("Vendor is required.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
    else if (tbllength == 0) {
        toastr.error("Product is required.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
    else {
        $('#tblProductDetail tbody tr').each(function (index) {
            xml += "<DraftItems><ProductAutoId><![CDATA[" + $(this).find('.ProName span').attr("productautoid") + "]]></ProductAutoId><Unit><![CDATA[" + $(this).find('.UnitType option:selected').attr('value') + "]]></Unit><QtyPerUnit><![CDATA[" + $(this).find('.UnitType option:selected').attr('qtyperunit') + "]]></QtyPerUnit><Qty><![CDATA[" + $(this).find('.ReqQty input').val() + "]]></Qty><DraftAutoId><![CDATA[" + $("#hfDraftAutoId").val() + "]]></DraftAutoId></DraftItems>";
        });
        var Draftdata = {
            POVDate: $('#txtGenerateDate').val(),
            VendorId: $('#ddlPoVendor').val(),
            Remark: $('#txtRemark').val(),
            Status: $('#ddlStatus').val(),
            DraftAutoId: $("#hfDraftAutoId").val()
        }
        $.ajax({
            type: "POST",
            url: "/POVendor/WebAPI/WPurchaseOrderVendor.asmx/SaveAsDraft",
            data: "{'data':'" + JSON.stringify(Draftdata) + "','XMLData':'" + JSON.stringify(xml) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            async: true,
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d == "Session Expired") {
                    location.href = "/";
                }
                else if (response.d == "true") {
                    toastr.success("Draft has been created successfully.", 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    Reset();
                }
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
}

function GeneratePO() {
    swal({
        title: "Are you sure ?",
        text: "Do you want to Generate PO ?",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, Cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, Generate it!",
                value: true,
                visible: true,
                className: "",
                closeModal: true
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            GeneratePOOrder();
        }
    })
}


function GeneratePOOrder() {
    debugger;
    var Ven = false;
    $("#tblProductDetail tbody tr").each(function () {
        debugger;
        var Vend = Number($(this).find('.VendorName option:selected').val()) || 0;
        if (Vend == 0 || Vend == null) {
            Ven = true;
            $(this).find('.VendorName select').addClass('border-warning');
        } else {
            $(this).find('.VendorName select').removeClass('border-warning');
        }
    })
    var xml = "";
    var tbllength = $('#tblProductDetail tbody tr').length;
  
    if (Ven) {
        toastr.error('Vendor is required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    else if (tbllength == 0) {
        debugger;
        toastr.error("Please add at least on item in list", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
    else {

        debugger;
        $('#tblProductDetail tbody tr').each(function (index) {
            xml += "<DraftItems><ProductAutoId><![CDATA[" + $(this).find('.ProName span').attr("productautoid") + "]]></ProductAutoId><Vendor><![CDATA[" + $(this).find('.VendorName option:selected').val() + "]]></Vendor><CostPrice><![CDATA[" + $(this).find('.UnitType option:selected').attr('costprice') + "]]></CostPrice><Unit><![CDATA[" + $(this).find('.UnitType option:selected').attr('value') + "]]></Unit><QtyPerUnit><![CDATA[" + $(this).find('.UnitType option:selected').attr('qtyperunit') + "]]></QtyPerUnit><Qty><![CDATA[" + $(this).find('.ReqQty input').val() + "]]></Qty><DraftAutoId><![CDATA[" + $("#hfDraftAutoId").val() + "]]></DraftAutoId></DraftItems>";
        });
        var Draftdata = {
            //VendorId: $('#ddlPoVendor').val(),
            Remark: $('#txtRemark').val(),
            DraftAutoId: $("#hfDraftAutoId").val()
        }
        $.ajax({
            type: "POST",
            url: "/POVendor/WebAPI/WPurchaseOrderVendor.asmx/GeneratePO",
            data: "{'data':'" + JSON.stringify(Draftdata) + "','XMLData':'" + JSON.stringify(xml) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            async: true,
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d == "Session Expired") {
                    location.href = "/";
                }
                else if (response.d == "false") {
                    toastr.error('Vendor is required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                }
                else {
                    var xmldoc = $.parseXML(response.d);
                    var Detail = $(xmldoc).find("Table");
                    swal("", "" + $(Detail).find("TotalPO").text() + " Purchase order generated successfully.", "success").then(function () {
                        window.location.href = "/PoVendor/POListInventoryManager.aspx";
                    });

                    
                    
                }
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
}
function EditDraft(PageId) {
    $("#hfDraftAutoId").val(PageId)
    var data = {
        DraftAutoId: $("#hfDraftAutoId").val()
    };
    $.ajax({
        type: "POST",
        url: "/POVendor/WebAPI/WPurchaseOrderVendor.asmx/EditDraft",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var PO = $(xmldoc).find("Table");
                var Products = $(xmldoc).find("Table1");
                var unitType = $(xmldoc).find("Table2");
                if ($(PO).length > 0) {
                    
                    //$("#ddlPoVendor").val($(PO).find("VenderAutoId").text()).change();
                    $("#txtGenerateDate").val($(PO).find("PODate").text());
                    $("#txtRemark").val($(PO).find("PORemarks").text());
                    $("#ddlStatus").val($(PO).find("Status").text());
                    $("#txtPoSequenceNo").val($(PO).find("PODraftId").text());
                    var row = $("#tblProductDetail thead tr").clone();
                    var TCost = 0.00;
                    $.each(Products, function (index) {
                        var pid = $(this).find('ProductAutoId').text();
                        $(".ProId", row).html($(this).find('ProductId').text());
                        $(".ProName", row).html("<span ProductAutoId='" + $(this).find('ProductAutoId').text() + "'>" + $(this).find('ProductName').text() + "</span>");
                        var ddu = '<select class="form-control input-sm border-primary" onchange="rowCal(this,0)" style="width: 130px;">';
                        $.each(unitType, function () {
                            var pid1 = $(this).find('ProductAutoId').text()
                            if (pid1 == pid) {
                                ddu += "<option value='" + $(this).find("AutoId").text() + "' EligibleforFree='" + $(this).find("EligibleforFree").text() + "' QtyPerUnit='" + $(this).find("Qty").text() + "' CostPrice='" + $(this).find("CostPrice").text() + "'>" + $(this).find("UnitType").text() + " (" + $(this).find("Qty").text() + " pcs" + ")</option>"
                            }
                        })
                        ddu += '</select>';
                        $(".UnitType", row).html(ddu);
                        row.find(".UnitType > select").val($(this).find('Unit').text()).change();
                        $(".VendorName", row).html(VendorDrp);
                        row.find(".VendorName > select").val($(this).find('VendorAutoId').text()).change();
                        $(".ReqQty", row).html("<input type='text' onkeypress='return isNumberKey(event)' class='form-control input-sm border-primary text-center' maxlength='5' runat='server' style='width:70px;' onkeyup='rowCal(this,1)' value='" + $(this).find('Qty').text() + "' TotReqQty='" + $(this).find('Qty').text() + "'/>");
                        $(".TtlPcs", row).html($(this).find('TotalPeice').text());
                        $(".Action", row).html("<a href='javascript:;' id='deleterow' onclick='deleteProduct(this)'><span class='ft-x'></span></a>");

                        var CostPrice = parseFloat($(this).find('Price').text());
                        $(".CostPrice", row).text(parseFloat(CostPrice).toFixed(2));
                        var TotalCostPrice = parseFloat(CostPrice) * (parseInt($(row).find(".TtlPcs").text()));
                        $(".TotalCostPrice", row).text(TotalCostPrice.toFixed(2));

                        //if ($(PO).find("SAutoId").text() == '2') {
                        //    $("#btnSaveDraftPO").hide();
                        //    $("#btnReset").hide();
                        //}
                        $('#tblProductDetail tbody').append(row);

                        row = $("#tblProductDetail thead tr:last").clone(true);
                    });
                    $("#TotalCostPrice").text(TCost.toFixed(2));
                    if ($('#tblProductDetail tbody tr').length > 0) {
                        $('#emptyTable').hide();
                    }
                    countRow();
                    CalculateFooter();
                }
                else {
                    $('#emptyTable').show();
                }

            }
        }
    });
}
function UpdateDraft() {
    var DraftItem = "";
    var xml = "";
    $('#tblProductDetail tbody tr').each(function (index) {
        xml += "<DraftItems><ProductAutoId><![CDATA[" + $(this).find('.ProName span').attr("productautoid") + "]]></ProductAutoId><Unit><![CDATA[" + $(this).find('.UnitType option:selected').attr('value') + "]]></Unit><QtyPerUnit><![CDATA[" + $(this).find('.UnitType option:selected').attr('qtyperunit') + "]]></QtyPerUnit><Qty><![CDATA[" + $(this).find('.ReqQty input').val() + "]]></Qty><DraftAutoId><![CDATA[" + $("#hfDraftAutoId").val() + "]]></DraftAutoId></DraftItems>";
    });
    var Draftdata = {
        POVDate: $('#txtGenerateDate').val(),
        VendorId: $('#ddlPoVendor').val(),
        Remark: $('#txtRemark').val(),
        Status: $('#ddlStatus').val(),
        DraftAutoId: $("#hfDraftAutoId").val()
    }
    $.ajax({
        type: "POST",
        url: "/POVendor/WebAPI/WPurchaseOrderVendor.asmx/SaveAsDraft",
        data: "{'data':'" + JSON.stringify(Draftdata) + "','XMLData':'" + JSON.stringify(xml) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: true,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == "Session Expired") {
                location.href = "/";
            }
            else if (response.d == "true") {
                swal({
                    title: "Success",
                    text: "Draft has been updated successfully.",
                    icon: "success",
                    closeOnClickOutside: false
                })
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
//------------------------------------------------------Save PO Draft-------------------------------------------------------------------
function SavePODraft() {
    swal({
        title: "Are you sure ?",
        text: "Do you want to Forward ?",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, Cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, Forward it!",
                value: true,
                visible: true,
                className: "",
                closeModal: true
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            SaveASPODraft();
        }
    })
}

function SaveASPODraft() {
    $.ajax({
        type: "POST",
        url: "/POVendor/WebAPI/WPurchaseOrderVendor.asmx/SavePODraft",
        data: "{'DraftAutoId':'" + $("#hfDraftAutoId").val() + "','Remark':'" + $("#txtRemark").val() + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: true,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == "Session Expired") {
                location.href = "/";
            }
            else if (response.d == "true") {
                swal("", "PO  Request has been  forwarded to manager successfully.", "success").then(function () {
                    Reset();
                });
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

//------------------------------------------------------Revert PO Draft-------------------------------------------------------------------
function RevertPODraft() {
    swal({
        title: "Are you sure?",
        text: "You want to Revert this PO draft!",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, Revert it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            RevertPODraftByIM();
        }
    })
}
function RevertPODraftByIM() {
    $.ajax({
        type: "POST",
        url: "/POVendor/WebAPI/WPurchaseOrderVendor.asmx/RevertPODraft",
        data: "{'DraftAutoId':'" + $("#hfDraftAutoId").val() + "','Remark':'" + $("#txtRemark").val() + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: true,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == "Session Expired") {
                location.href = "/";
            }
            else if (response.d == "true") {
                swal("", "PO Request has been reverted successfully.", "success").then(function () {
                    window.location.href = "/POVendor/SubmitDraftList.aspx";
                });
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function UpdateRemrks() {
    debugger;
    if ($("#hfDraftAutoId").val() != '') {
        $.ajax({
            type: "POST",
            url: "/POVendor/WebAPI/WPurchaseOrderVendor.asmx/AutoUpdateRemarks",
            data: "{'DraftAutoId':'" + $("#hfDraftAutoId").val() + "','Remark':'" + $("#txtRemark").val() + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            async: false,
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {

            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
}
function CalculateFooter() {
    var total1 = 0;
    $("#tblProductDetail tbody tr").each(function () {
        total1 += Number($(this).find('.TotalCostPrice').text());
    })
    $("#TotalCostPrice").text(total1.toFixed(2));
    
}
