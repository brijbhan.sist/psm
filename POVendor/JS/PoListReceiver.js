﻿$(document).ready(function () {
    $('.date').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    bindStatus();
    bindDropdown();
    POLIst(1);
});
function Pagevalue(e) {
    POLIst(parseInt($(e).attr("page")));
};
$("#btnSearch").click(function ()
{
    POLIst(1);
})
function bindDropdown() {
    $.ajax({
        type: "POST",
        url: "/POVendor/WebAPI/WPurchaseOrderVendor.asmx/bindDropDowns",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var Vendor = $(xmldoc).find("Table");

            $("#ddlPoVendor option:not(:first)").remove();
            $.each(Vendor, function () {
                $("#ddlPoVendor").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("VendorName").text() + "</option>");
            });
            $("#ddlPoVendor").select2();
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function POLIst(PageIndex) {
    
    var data = {
        PONo: $("#txtPoNo").val(),
        POFromDate: $("#txtPoFromDate").val(),
        POToDate: $("#txtPoToDate").val(),
        VendorId: $("#ddlPoVendor").val(),
        Status: $("#ddlStatus").val(),
        PageSize: $("#ddlPaging").val(),
        PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "/POVendor/WebAPI/WPurchaseOrderVendor.asmx/POList",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var POList = $(xmldoc).find("Table1");
            $("#tblPOList tbody tr").remove();
            var row = $("#tblPOList thead tr").clone(true);
            if (POList.length > 0) {
                $("#EmptyTable").hide();
                $.each(POList, function (index) {
                    $(".PONo", row).text($(this).find("PONo").text());
                    $(".Remarks", row).text($(this).find("PORemarks").text());
                    $(".PODate", row).html($(this).find("PODate").text());
                    $(".BillNo", row).html($(this).find("BillNo").text());
                    $(".BillDate", row).html($(this).find("BillDate").text());
                    $(".CreatedBy", row).html($(this).find("CreateBy").text());
                    $(".VendorName", row).text($(this).find("VendorName").text());
                    $(".NoOfItems", row).text($(this).find("NoofItems").text());
                    $(".Status", row).html("<span class='badge badge badge-pill' style='background-color:" + $(this).find("ColorCode").text() + ";'>" + $(this).find("Status").text() + "</span");
                    if ($(this).find("Status").text() == "Pending") {
                        $(".Action", row).html("<a title='Log' href='#' onclick='ShowPODraftLog(" + $(this).find("AutoId").text() + ")'><span class='la la-history' ></span></a>&nbsp;" +"<a title='Edit' href='/POVendor/GeneratePOV.aspx?PageId=" + $(this).find("AutoId").text() + "'><span class='la la-edit'></span></a>"+
                          "<a title = 'View' href = '/POVendor/PODetails.aspx?PageId=" + $(this).find("AutoId").text() +"'><span class='la la-eye'></span></a>");    
                    }
                    else {
                        $(".Action", row).html("<a title='Log' href='#' onclick='ShowPODraftLog(" + $(this).find("AutoId").text() + ")'><span class='la la-history' ></span></a>&nbsp;" +"<a title='View' href='/POVendor/PODetails.aspx?PageId=" + $(this).find("AutoId").text() + "'><span class='la la-eye' ></span></a>");
                    }
                    
                    $("#tblPOList tbody").append(row);
                    row = $("#tblPOList tbody tr:last").clone(true);
                });

            } else {
                $("#EmptyTable").show();
            }
            var pager = $(xmldoc).find("Table");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function bindStatus() {
    $.ajax({
        type: "POST",
        url: "POListInventoryManager.aspx/BindStatus",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var status = $(xmldoc).find("Table");
                $("#ddlStatus option:not(:first)").remove();
                $.each(status, function () {
                    $("#ddlStatus").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("StatusType").text() + "</option>");
                });
               
            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function ShowPODraftLog(POAutoId) {
    debugger;
    $.ajax({
        type: "POST",
        url: "/POVendor/WebAPI/WPurchaseOrderVendor.asmx/POLog",
        data: "{'POAutoId':" + POAutoId + "}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var POList = $(xmldoc).find("Table");
            $("#tblPODraftLog tbody tr").remove();
            var row = $("#tblPODraftLog thead tr").clone(true);
            if (POList.length > 0) {
                $("#EmptyTable").hide();
                $.each(POList, function (index) {
                    $(".SrNo", row).text(index + 1);
                    $(".ActionBy", row).text($(this).find("EmpName").text());
                    $(".Date", row).text($(this).find("PODate").text());
                    $(".Action", row).text($(this).find("RemarkType").text());
                    $(".Remark", row).text($(this).find("Remark").text());
                    $("#tblPODraftLog tbody").append(row);
                    row = $("#tblPODraftLog tbody tr:last").clone(true);
                });
            }
            else {
                $('#EmptyTable33').show();
            }
            $("#ShowPODraftLog").modal('show');
        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}