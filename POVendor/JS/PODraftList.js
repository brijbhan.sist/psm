﻿$(document).ready(function () {
    $('.date').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    bindDropdown();
    DraftPOLIst(1);
});
function bindDropdown() {
    $.ajax({
        type: "POST",
        url: "/POVendor/WebAPI/WPurchaseOrderVendor.asmx/bindDropDowns",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var Vendor = $(xmldoc).find("Table");

            $("#ddlPoVendor option:not(:first)").remove();
            $.each(Vendor, function () {
                $("#ddlPoVendor").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("VendorName").text() + "</option>");
            });
            $("#ddlPoVendor").select2();
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
$("#btnSearch").click(function () {
    DraftPOLIst(1);
})
function DraftPOLIst(PageIndex) {
    var data = {
        POFromDate: $("#txtPoFromDate").val(),
        POToDate: $("#txtPoToDate").val(),
        PODraftId: $("#txtPODraftId").val().trim(),
        VendorId: $("#ddlPoVendor").val(),
        PageSize: $("#ddlPaging").val(),
        PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "/POVendor/WebAPI/WPurchaseOrderVendor.asmx/DraftList",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var POList = $(xmldoc).find("Table1");
            $("#tblPOList tbody tr").remove();
            var row = $("#tblPOList thead tr").clone(true);
            if (POList.length > 0) {
                $("#EmptyTable").hide();
                $.each(POList, function (index) {
                    $(".PODate", row).text($(this).find("PODate").text());
                    $(".VendorName", row).text($(this).find("VendorName").text());
                    $(".PODraftId", row).text($(this).find("PODraftId").text());
                    $(".NoOfItems", row).text($(this).find("NoofItems").text());
                    $(".Remarks", row).text($(this).find("PORemarks").text());
                    $(".Action", row).html("<a title='Log' href='#' onclick='ShowPODraftLog(" + $(this).find("DraftAutoId").text()+")'><span class='la la-history' ></span></a>&nbsp;" + "<a title='Edit' href='/POVendor/GeneratePOV.aspx?PageId=" + $(this).find("DraftAutoId").text() + "'><span class='ft-edit' ></span></a>&nbsp;"+
                        "<a title='Delete' href='javascript:;'><span class='la la-remove' onclick='deleterecord(\"" + $(this).find("DraftAutoId").text() + "\")'></span><b></a>&nbsp;&nbsp;</b>");
                    $(".Status", row).html("<span class='badge badge badge-pill' style='background-color:" + $(this).find("ColorCode").text() + ";'>" + $(this).find("Status").text() + "</span");
                    $("#tblPOList tbody").append(row);
                    row = $("#tblPOList tbody tr:last").clone(true);
                });
            }
            else {
                $('#EmptyTable').show();
            }
            var pager = $(xmldoc).find("Table");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function deleterecord(DraftAutoId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this draft!",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, delete it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            deleteDraftOrder(DraftAutoId);
        } 
    })
}

function deleteDraftOrder(DraftAutoId) {
    $.ajax({
        type: "POST",
        url: "/POVendor/WebAPI/WPurchaseOrderVendor.asmx/deleteDraftOrder",
        data: "{'DraftAutoId':" + DraftAutoId + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            swal("", "Draft has been deleted successfully.", "success")
            DraftPOLIst(1);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function ShowPODraftLog(DraftAutoId) {
    $.ajax({
        type: "POST",
        url: "/POVendor/WebAPI/WPurchaseOrderVendor.asmx/DraftLog",
        data: "{'DraftAutoId':" + DraftAutoId + "}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var POList = $(xmldoc).find("Table");
            $("#tblPODraftLog tbody tr").remove();
            var row = $("#tblPODraftLog thead tr").clone(true);
            if (POList.length > 0) {
                $("#EmptyTable").hide();
                $.each(POList, function (index) {
                    $(".SrNo", row).text(index + 1);
                    $(".ActionBy", row).text($(this).find("EmpName").text());
                    $(".Date", row).text($(this).find("PODate").text());
                    $(".Action", row).text($(this).find("RemarkType").text());
                    $(".Remark", row).text($(this).find("Remark").text());
                    $("#tblPODraftLog tbody").append(row);
                    row = $("#tblPODraftLog tbody tr:last").clone(true);
                });
            }
            else {
                $('#EmptyTable33').show();
            }
            $("#ShowPODraftLog").modal('show');
        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}