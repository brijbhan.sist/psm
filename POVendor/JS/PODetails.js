﻿var PageId = 0;
$(document).ready(function () {
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    PageId = getQueryString('PageId');
    if (PageId != null) {
        ViewPoDetails(PageId);
    }

});
function ViewPoDetails(PageId) {
    var data = {
        AutoId: PageId
    };
    $.ajax({
        type: "POST",
        url: "/POVendor/WebAPI/WPurchaseOrderVendor.asmx/PODetails",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var Order = $(xmldoc).find("Table");
            var Product = $(xmldoc).find("Table1");
            if (Order.length > 0) {
                $("#txtPoSequenceNo").val($(Order).find("PONo").text());
                $("#txtGenerateDate").val($(Order).find("PODate").text());
                $("#txtVendor").val($(Order).find("VendorName").text());
                $("#txtRemark").val($(Order).find("PORemarks").text());
                $("#txtBillNo").val($(Order).find("BillNo").text());
                $("#txtBillDate").val($(Order).find("BillDate").text());
                $("#txtCreatedBy").val($(Order).find("CreateBy").text());
                $("#txtReceiveRemark").val($(Order).find("RecieveStockRemark").text());
                $("#ddlStatus").val($(Order).find("Status").text());
                if ($(Order).find("SAutoId").text() == "3" || $(Order).find("SAutoId").text() == "4") {
                    $("#btnRecievePo").hide();
                    $("#btnCancel").hide();
                    $("#btnNewReceive").hide();
                }
                else {
                    $("#btnRecievePo").show();
                    $("#btnCancel").show();
                    $("#btnNewReceive").show();
                }
                if ($(Order).find("SAutoId").text() == "2") {
                    $("#btnCancel").hide();
                }
            }
            $("#tblProductDetail tbody tr").remove();
            var row = $("#tblProductDetail thead tr").clone(true);
            if (Product.length > 0) {
                $.each(Product, function (index) {
                    $(".ProductId", row).html('<span ProductAutoId=' + $(this).find("ProductAutoId").text() + '>' + $(this).find("ProductId").text() + '</span>');
                    $(".ProductName", row).html($(this).find("ProductName").text());
                    $(".UnitType", row).html('<span Qty=' + $(this).find("QtyPerUnit").text() + ' value=' + $(this).find("Unit").text() + '>' + $(this).find("UnitType").text() + '</span>');
                    $(".ReqQty", row).html($(this).find("Qty").text());
                    $(".TtlPcs", row).html($(this).find("TotalPieces").text());
                    //$(".RecivedQty", row).text($(this).find('RecivedQty').text());
                    $(".Remain", row).text($(this).find('RemainPieces').text());
                    $(".RecivedPiece", row).html($(this).find("ReceivedPiece").text());
                    
                    $("#tblProductDetail tbody").append(row);
                    row = $("#tblProductDetail tbody tr:last").clone(true);
                });
            }
            var Check = 0;
            $('#tblProductDetail tbody tr').each(function (index) {
                if ($(this).find('.TtlPcs').text() != $(this).find('.RecivedPiece').text()) {
                    Check = 1;
                }
            });
            if (Check == 0) {
                $("#btnNewReceive").hide();
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function NewReceive() {
    localStorage.setItem("PORecieve",'1');
    location.href = '/POVendor/POStockReceive.aspx?PageId=' + PageId + '';
}

function RecievePOStock() {
    var Req = 0;
    $('#tblProductDetail tbody tr').each(function (index) {
        if ($(this).find('.TtlPcs').text() != $(this).find('.RecivedPiece').text()) {
            Req = 1;
        }
    });
    if (Req == 1) {
        swal({
            title: "Are you sure ?",
            text: "Do you want to Close this PO because all PO items are not recieved ?",
            icon: "warning",
            showCancelButton: true,
            buttons: {
                cancel: {
                    text: "No, Cancel!",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: true,
                },
                confirm: {
                    text: "Yes, close it!",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: true

                }
            }
        }).then(function (isConfirm) {
            if (isConfirm) {
                ClosePOOrder(); 
            }
        })
    } else {
        swal({
            title: "Are you sure?",
            text: "You want to Close this PO Order",
            icon: "warning",
            showCancelButton: true,
            buttons: {
                cancel: {
                    text: "No, cancel!",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: true,
                },
                confirm: {
                    text: "Yes, Close it!",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
                }
            }
        }).then(function (isConfirm) {
            if (isConfirm) {
                ClosePOOrder();
            }
        })
    }
}
function ClosePOOrder() {
    var data = {
        //ManagerRemark: $("#txtInvManager_Remark").val(),
        POAutoId: PageId,
    }
    $.ajax({
        type: "POST",
        url: "RecievePoByVendor.aspx/RecievePOStock",
        data: "{'DataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d == "true") {
                    swal("", "Stock has been Close successfully.", "success");
                   ViewPoDetails(PageId);
                }
            }
            else {
                location.href = '/';
            }

        },
        error: function (result) {
        },
        failure: function (result) {
            swal("Error!", 'Oops,some thing went wrong.Please try later.', "error");
        }
    });
}

$("#btnCancel").on('click', function () {
    swal({
        title: "Are you sure?",
        text: "You want to Cancel this PO Order",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, Cancel it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            CancelPOBYM();
        }
    })
})
function CancelPOBYM() {
    $.ajax({
        type: "POST",
        url: "/POVendor/WebAPI/WPurchaseOrderVendor.asmx/CancelPO",
        data: "{'POAutoId':'" + PageId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: true,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == "Session Expired") {
                location.href = "/";
            }
            else if (response.d == "true") {
                swal("", "PO has been Cancelled", "success").then(function () {
                    location.href = "/POVendor/POListInventoryManager.aspx";
                });
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function POLIst(PageId) {

    var data = {
        PONo: PageId,
    };
    $.ajax({
        type: "POST",
        url: "/POVendor/WebAPI/WInvReceiverPO.asmx/ReceiveListByPO",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var POList = $(xmldoc).find("Table");
            $("#tblPOList tbody tr").remove();
            var row = $("#tblPOList thead tr").clone(true);
            if (POList.length > 0) {
                $("#EmptyTable").hide();
                $.each(POList, function (index) {
                    $(".RecNo", row).text($(this).find("PORecieveId").text());
                    $(".RecDate", row).text($(this).find("ReceiveDate").text());
                    $(".RecBy", row).text($(this).find("ReceiveBy").text());
                    $(".Status", row).html("<span class='badge badge badge-pill' style='background-color:" + $(this).find("ColorCode").text() + ";'>" + $(this).find("Status").text() + "</span");
                    $(".Action", row).html("<a title='View' href='/POVendor/POStockReceive.aspx?PageId=" + $(this).find("POAutoId").text() + "&RecAutoId=" + $(this).find("AutoId").text() + "'><span class='la la-eye' ></span></a>");

                    $("#tblPOList tbody").append(row);
                    row = $("#tblPOList tbody tr:last").clone(true);
                });

            } else {
                $("#EmptyTable").show();
            }
        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
$("#LoadReceiveList").click(function () {
    if ($("#LoadReceiveList").find("i").attr('class') == "ft-plus") {
        POLIst(PageId);
    }
});
function POLIstINV_M(PageId) {

    var data = {
        PONo: PageId,
    };
    $.ajax({
        type: "POST",
        url: "/POVendor/WebAPI/WInvReceiverPO.asmx/ReceiveListByPO",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var POList = $(xmldoc).find("Table");
            $("#tblPOListInvM tbody tr").remove();
            var row = $("#tblPOListInvM thead tr").clone(true);
            if (POList.length > 0) {
                $("#EmptyTable1").hide();
                $.each(POList, function (index) {
                    $(".RecNo", row).text($(this).find("PORecieveId").text());
                    $(".RecDate", row).text($(this).find("ReceiveDate").text());
                    $(".RecBy", row).text($(this).find("ReceiveBy").text());
                    $(".Status", row).html("<span class='badge badge badge-pill' style='background-color:" + $(this).find("ColorCode").text() + ";'>" + $(this).find("Status").text() + "</span");
                    $(".Action", row).html("<a title='View' href='/POVendor/RecievePoByVendor.aspx?Id=" + $(this).find("POAutoId").text() + "&RecAutoId=" + $(this).find("AutoId").text() + "'><span class='la la-eye' ></span></a>");

                    $("#tblPOListInvM tbody").append(row);
                    row = $("#tblPOListInvM tbody tr:last").clone(true);
                });

            } else {
                $("#EmptyTable1").show();
            }
        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
$("#LoadReceiveListInvM").click(function () {
    if ($("#LoadReceiveListInvM").find("i").attr('class') == "ft-plus") {
        POLIstINV_M(PageId);
    }
});
