﻿$(document).ready(function () {
    bindStatus();
    BindVender();
    $('#txtSFromDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtSToDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    getOrderList(1);
})

function Pagevalue(e) {
    getOrderList(parseInt($(e).attr("page")));
};
function bindStatus() {
    $.ajax({
        type: "POST",
        url: "POListInventoryManager.aspx/BindStatus",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var status = $(xmldoc).find("Table");
                $("#ddlStatus option:not(:first)").remove();
                $.each(status, function () {
                    $("#ddlStatus").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("StatusType").text() + "</option>");
                });
                //$("#ddlStatus").val('3');
            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function BindVender() {

    $.ajax({
        type: "POST",
        url: "POListInventoryManager.aspx/BindVendor",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var vender = $(xmldoc).find("Table");
                $("#ddlVender option:not(:first)").remove();
                $.each(vender, function () {
                    $("#ddlVender").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("VendorName").text() + "</option>");
                });
                $("#ddlVender").select2();
            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function getOrderList(pageIndex) {
    debugger;
    var data = {
        PoNo: $("#txtPONo").val().trim(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        Status: $("#ddlStatus").val(),
        VenderAutoId: $("#ddlVender").val(),
        pageIndex: pageIndex,
        PageSize: $("#ddlPageSize").val(),
    };

    $.ajax({
        type: "POST",
        url: "POListInventoryManager.aspx/getPOList",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var POList = $(xmldoc).find("Table1");
                $("#tlbPoList tbody tr").remove();
                var row = $("#tlbPoList thead tr").clone(true);
                if (POList.length > 0) {
                    $("#EmptyTable").hide();
                    $.each(POList, function () {
                        $(".PONo", row).html("<span POAutoId='" + $(this).find("AutoId").text() + "'>" + $(this).find("PONo").text() + "</span>");
                        $(".PODate", row).text($(this).find("PODate").text());
                        $(".vender", row).text($(this).find("VendorName").text());
                        $(".BillNo", row).html($(this).find("BillNo").text());
                        $(".BillDate", row).html($(this).find("BillDate").text());
                        $(".CreatedBy", row).html($(this).find("CreateBy").text());
                        $(".POStatus", row).html("<span class='badge badge badge-pill' style='background-color:" + $(this).find("ColorCode").text() + ";'>" + $(this).find("StatusType").text() + "</span");
                        $(".itenms", row).text($(this).find("NoofItems").text());
                        $(".action", row).html("<a title='Log' href='#' onclick='ShowPODraftLog(" + $(this).find("AutoId").text() + ")'><span class='la la-history' ></span></a>&nbsp;" +"<a title='View' href='/POVendor/PODetails.aspx?PageId=" + $(this).find("AutoId").text() + "'><span class='la la-eye'></span></a>");
                        $("#tlbPoList tbody").append(row);
                        row = $("#tlbPoList tbody tr:last").clone(true);
                    });
                } else {
                    $("#EmptyTable").show();
                }

                var pager = $(xmldoc).find("Table");
                $(".Pager").ASPSnippets_Pager({
                    ActiveCssClass: "current",
                    PagerCssClass: "pager",
                    PageIndex: parseInt(pager.find("PageIndex").text()),
                    PageSize: parseInt(pager.find("PageSize").text()),
                    RecordCount: parseInt(pager.find("RecordCount").text())
                });
            } else {
                location.href = "/";
            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
$("#btnSearch").click(function () {
    getOrderList(1);
})

$("#ddlPageSize").change(function () {
    getOrderList(1);
})
function ShowPODraftLog(POAutoId) {
    debugger;
    $.ajax({
        type: "POST",
        url: "/POVendor/WebAPI/WPurchaseOrderVendor.asmx/POLog",
        data: "{'POAutoId':" + POAutoId + "}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var POList = $(xmldoc).find("Table");
            $("#tblPODraftLog tbody tr").remove();
            var row = $("#tblPODraftLog thead tr").clone(true);
            if (POList.length > 0) {
                $("#EmptyTable").hide();
                $.each(POList, function (index) {
                    $(".SrNo", row).text(index + 1);
                    $(".ActionBy", row).text($(this).find("EmpName").text());
                    $(".Date", row).text($(this).find("PODate").text());
                    $(".Action", row).text($(this).find("RemarkType").text());
                    $(".Remark", row).text($(this).find("Remark").text());
                    $("#tblPODraftLog tbody").append(row);
                    row = $("#tblPODraftLog tbody tr:last").clone(true);
                });
            }
            else {
                $('#EmptyTable33').show();
            }
            $("#ShowPODraftLog").modal('show');
        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
