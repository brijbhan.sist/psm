﻿using DllPOInventoryManager;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RecievePoByVendor : System.Web.UI.Page
{
    protected void Page_load(object sender, EventArgs e)
    {
        try
        {
                      
            hiddenEmpTypeVal.Value = Session["EmpTypeNo"].ToString();
        }
        catch
        {
            Session.Abandon();
            Response.Redirect("~/Default.aspx", false);
        }
        string[] GtUrl = Request.Url.Host.ToString().Split('.');
    }

    [WebMethod(EnableSession = true)]
    public static string BindData(string AutoId, string RecAutoId)
    {
        PL_PurchaseOrder pobj = new PL_PurchaseOrder();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.POAutoId = Convert.ToInt32(AutoId);
                pobj.RecAutoId = Convert.ToInt32(RecAutoId);
                BL_PurchaseOrder.BindPoData(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
     [WebMethod(EnableSession = true)]
    public static string getManagePrice(string ProductAutoId)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_PurchaseOrder pobj = new PL_PurchaseOrder();
            pobj.ProductAutoId = Convert.ToInt32(ProductAutoId);
            BL_PurchaseOrder.getManagePrice(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Session Expired";
        }
    }

    //UpdateCostPrice
    [WebMethod(EnableSession = true)]
    public static string UpdateManagePrice(string dataTable)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_PurchaseOrder pobj = new PL_PurchaseOrder();
            DataTable dtOrder = new DataTable();
            dtOrder = JsonConvert.DeserializeObject<DataTable>(dataTable);
            pobj.EmpId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"].ToString());
            if (dtOrder.Rows.Count > 0)
                pobj.DtPackingDetails = dtOrder;
            BL_PurchaseOrder.UpdateManagePrice(pobj);
            if (!pobj.isException)
            {
                return "true";
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";

        }
    }

    [WebMethod(EnableSession = true)]
    public static string UpdateCostPrice(string DataValues)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(DataValues);
            PL_PurchaseOrder pobj = new PL_PurchaseOrder();
            
            pobj.EmpId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"].ToString());
            pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
            pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]);
            pobj.CostPrice = Convert.ToDecimal(jdv["CostPrice"]);
            pobj.POAutoId = Convert.ToInt32(jdv["POAutoId"]);
            BL_PurchaseOrder.UpdateCostPrice(pobj);
            if (!pobj.isException)
            {
                return "true";
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";

        }
    }
    [WebMethod(EnableSession = true)]
    public static string UpdateCostPriceOnPO(string DataValues)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(DataValues);
            PL_PurchaseOrder pobj = new PL_PurchaseOrder();

            pobj.EmpId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"].ToString());
            pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
            pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]);
            pobj.CostPrice = Convert.ToDecimal(jdv["CostPrice"]);
            pobj.POAutoId = Convert.ToInt32(jdv["POAutoId"]);
            BL_PurchaseOrder.UpdateCostPriceOnPO(pobj);
            if (!pobj.isException)
            {
                return "true";
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";

        }
    }
    [WebMethod(EnableSession = true)]
    public static string RecievePOStock(string DataValues)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(DataValues);
            PL_PurchaseOrder pobj = new PL_PurchaseOrder();

            pobj.EmpId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"].ToString());
            pobj.POAutoId = Convert.ToInt32(jdv["POAutoId"]);
            //pobj.BillNo = jdv["BillNo"];
            //pobj.BillDate = Convert.ToDateTime(jdv["BillDate"]);
            //if (jdv["ManagerRemark"] != "")
            //{
            //    pobj.ManagerRemark = jdv["ManagerRemark"];
            //}
            
            BL_PurchaseOrder.UpdateCostPrice(pobj);
            if (!pobj.isException)
            {
                return "true";
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";

        }
    }
    [WebMethod(EnableSession = true)]
    public static string PartialRecievePO(string DataValues)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(DataValues);
            PL_PurchaseOrder pobj = new PL_PurchaseOrder();

            pobj.EmpId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"].ToString());
            pobj.POAutoId = Convert.ToInt32(jdv["POAutoId"]);
            pobj.RecAutoId = Convert.ToInt32(jdv["RecAutoId"]);
            pobj.BillNo = jdv["BillNo"];
            pobj.BillDate = Convert.ToDateTime(jdv["BillDate"]);
            if (jdv["ManagerRemark"] != "")
            {
                pobj.ManagerRemark = jdv["ManagerRemark"];
            }

            BL_PurchaseOrder.PartialRecievePO(pobj);
            if (!pobj.isException)
            {
                return "true";
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";

        }
    }
}