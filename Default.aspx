﻿<%@ Page Language="C#" CodeFile="Default.aspx.cs" Inherits="Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="icon" href="/favicon.png" type="image/gif" sizes="16x16" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" />
    <link rel="preconnect" href="https://fonts.gstatic.com" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet" />
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap');

        body {
            font-family: 'Montserrat', sans-serif;
        }

        .login_sec {
            width: 100%;
            height: 100%;
            background: #fff;
            position: relative;
            padding: 61px 0;
            overflow: hidden;
        }

            .login_sec:before {
                position: absolute;
                top: -40px;
                left: -60px;
                content: "";
                background: url(images/top.png) no-repeat;
                width: 710px;
                height: 380px;
            }

            .login_sec:after {
                position: absolute;
                bottom: 0px;
                right: 0px;
                content: "";
                background: url(images/bottom.png) no-repeat;
                width: 382px;
                height: 640px;
            }

        .login_wrap {
            width: 90%;
            background: #F7FAFB;
            position: relative;
            z-index: 1;
            box-shadow: 0px 4px 4px rgba(0,0,0, 15%);
            padding: 80px 150px;
        }

            .login_wrap h4 {
                font-size: 28px;
                color: #24394D;
                margin: 30px 0 20px;
            }

        .frm_main {
            max-width: 350px;
            box-shadow: 0px 18px 45px rgba(0,0,0, 9%);
            border-radius: 4px;
            overflow: hidden;
            width: 100%;
        }

        .form_sec {
            background: #fff;
            padding: 60px 40px 0 40px;
        }

            .form_sec .form-control {
                height: calc(2em + .75rem + 2px);
                padding: .375rem 0;
                font-size: 14px;
                color: #999999 !important;
            }

            .form_sec .btn {
                box-shadow: 0px 6px 10px rgba(0,0,0, 10%);
                background: #20BEAD;
                border: 1px solid #20BEAD;
                font-size: 16px;
                font-weight: 700;
            }

            .form_sec h2, .form_sec h3 {
                text-align: center;
                font-size: 24px;
            }

            .form_sec h2 {
                color: #FC5E03;
                font-weight: 700;
                margin: 30px 0;
            }

            .form_sec .orng {
                color: #FC5E03;
                font-size: 14px;
            }

        .fm_btm {
            padding: 10px;
            background: #EFF5F6;
            font-size: 14px;
            color: #999999
        }

            .fm_btm p {
                margin: 0;
            }

        @media only screen and (max-width:1200px) {
            .login_wrap {
                padding: 80px 70px;
                width: 95%;
            }

            .login_sec {
                padding: 50px 0;
            }
        }

        @media only screen and (max-width:1024px) {
            .login_wrap {
                padding: 70px 60px;
            }

                .login_wrap h4 {
                    font-size: 24px;
                    margin: 25px 0 20px;
                }

            .login_sec {
                padding: 120px 0;
            }

            .pl-lg-5, .px-lg-5 {
                padding-left: 15px !important;
            }

            .login_sec:after {
                display: none;
            }

            .login_sec::before {
                background-size: 100%;
            }
        }

        @media only screen and (max-width:768px) {
            .login_sec {
                padding: 50px 0;
            }

            .login_wrap {
                text-align: center;
                width: 80%;
            }

            .frm_main {
                display: inline-block;
                margin-top: 30px;
            }

            .desk_main {
                padding: 0 30px;
            }

            .login_sec::before {
                background-size: 80%;
            }
        }

        @media only screen and (max-width:600px) {
            .login_wrap {
                text-align: center;
                width: 90%;
                padding: 50px 30px;
            }

            .login_sec {
                padding: 30px 0;
            }

            .desk_main {
                padding: 0 15px;
            }

            .login_sec::before {
                background-size: 70%;
            }
        }

        @media only screen and (max-width:400px) {
            .login_wrap h4 {
                font-size: 20px;
            }

            .login_sec::before {
                background-size: 60%;
            }
        }

        @media only screen and (max-width:360px) {

            .login_sec::before {
                background-size: 60%;
            }
        }
    </style>
</head>
<body>
    <div class="login_sec d-flex justify-content-center">
        <div class="login_wrap  justify-content-center">
            <div class="row">
                <!-- For Demo Purpose -->
                <div class="col-lg-7 col-md-6 pl-lg-5 mb-md-0">
                    <div class="desk_main">
                        <img src="images/logo.png" />
                        <h4>Warehouse Management System</h4>
                        <img src="images/desktop.png" class="img-fluid" />
                    </div>
                </div>
                <!-- Registeration Form -->

                <div class="col-md-6 col-lg-5">
                    <div class="frm_main">
                        <div class="form_sec">
                            <h3>Login</h3>
                            <h2>A1WHM</h2>
                            <form action="#">
                                <div class="row">
                                    <!-- Email Address -->
                                    <div class="input-group col-lg-12 mb-4">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text bg-white px-3 border-md border-right-0">
                                                <img src="images/user.png" />
                                            </span>
                                        </div>
                                        <input id="txtusername" type="email" name="email" placeholder="Enter Username" autocomplete="chrome-off" class="form-control bg-white border-left-0 border-md" />
                                    </div>
                                    <!-- Password -->
                                    <div class="input-group col-lg-12 mb-4">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text bg-white px-3 border-md border-right-0">
                                                <img src="images/lock.png" />
                                            </span>
                                        </div>
                                        <input id="txtpassword" type="password" name="password" placeholder="Enter Password" class="form-control bg-white border-left-0 border-md" />
                                    </div>
                                    <!-- Submit Button -->
                                    <div class="form-group col-lg-12 mx-auto mb-4">
                                        <a href="javascript:void(0)" style="cursor: pointer" class="btn btn-primary btn-block py-2" id="btnlogin" onclick="login()">
                                            <span class="font-weight-bold">LOGIN</span>
                                        </a>
                                    </div>
                                    <!-- Already Registered -->
                                    <div class="text-center w-100">
                                        <p class="mb-4 orng">Authorize Access Only</p>
                                    </div>
                                </div>
                        </div>
                        <div class="fm_btm text-center w-100">
                            <p style="cursor: pointer" onclick="openContact()">Need Support?</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="contactModal" class="modal fade" role="dialog">
            <div class="modal-dialog" id="">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="row" style="width: 100%; text-align: left;">
                            <div class="col-md-10">
                                <h4 class="modal-title">Help</h4>
                            </div>
                            <div class="col-md-2">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="OpenDepositList_Popup()">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="modal-body">
                        <center style="font-size: 21px;">For support email us at 
                                                <a href="mailto:support@priorware.com">support@priorware.com</a></center>
                        <br />
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
    <script src="/app-assets/vendors/js/extensions/sweetalert.min.js" type="text/javascript"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="/js/Default.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
</body>
</html>



