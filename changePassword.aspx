﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="changePassword.aspx.cs" Inherits="changePassword" ClientIDMode="Static"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Change Password</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Settings</a>
                        </li>
                         <li class="breadcrumb-item"><a href="#">Change Password</a>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
          <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
            <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
            id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false"> Action</button>
            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" onclick="location.href='/Admin/mydashboard.aspx'" class="dropdown-item">Go to Dashboard</button>                
            <input type="hidden" runat="server" id="hdEmpType" />
            </div>
          </div>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <label class="control-label">Old Password</label> <span class="required">*</span>
                                        <input id="txtOldPassword" type="password" placeholder="Enter old password" class="form-control border-primary input-sm req" runat="server" />
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <label class="control-label">New Password</label> <span class="required">*</span>
                                        <input id="txtNewPassword" type="password" placeholder="Enter new password" class="form-control border-primary input-sm req" runat="server" />
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <label class="control-label">Confirm Password</label> <span class="required">*</span>
                                        <input id="txtConfirmPassword" type="password" placeholder="Enter confirm password" class="form-control border-primary input-sm req" runat="server" />
                                    </div>
                                </div>
                                <div class="row ">
                                    <div class="col-md-4 col-sm-4 col-xs-12"><br />
                                        <button type="button" id="btnChangePassword" class="btn btn-primary">Change Password</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="JS/changePassword.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
</asp:Content>

