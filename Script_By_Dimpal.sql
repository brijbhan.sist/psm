
---------------------------------------------------------------------------------------------------------------------

create FUNCTION  [dbo].[FN_Order_IsProductTaxPerUnit]  
(  
	@AutoId int,  
	@orderAutoId int  
)  
RETURNS decimal(18,2)  
AS  
BEGIN  
	DECLARE @Total decimal(18,2)=0.00      
	IF (select tax from OrderItemMaster where AutoId=@AutoId)=1
	BEGIN  
		set @Total=(select isnull((oim.UnitPrice*om.TaxValue)/100,0) as Tax from OrderItemMaster as oim 
					inner join OrderMaster as om on om.AutoId=oim.OrderAutoId
					where oim.AutoId=@AutoId) 
		
	END  
	RETURN @Total  
END  
------------------------------------------------------------------------------------------------------------------------


create   FUNCTION [dbo].[FN_Order_OIMItemtotalAmount]
(
	 @AutoId int,
	 @orderAutoId int
)
RETURNS decimal(18,2)
AS
BEGIN
	DECLARE @NetAmount decimal(18,2)	
	--CAL TOTAL AMOUNT FOR NEW ,PROCESSED AND CANCELLED ORDER 
	IF EXISTS(SELECT AutoId FROM OrderMaster WHERE AutoId=@orderAutoId AND Status IN (0,1,2,8))
	BEGIN
		SET @NetAmount=ISNULL((SELECT (UnitPrice*	RequiredQty) FROM OrderItemMaster WHERE AutoId=@AutoId),0)	
	END
	ELSE 
	BEGIN	 
		SET @NetAmount=ISNULL((SELECT (UnitPrice*	QtyShip) FROM OrderItemMaster WHERE AutoId=@AutoId),0)	
	END

	RETURN @NetAmount
END
-------------------------------------------------------------------------------------------------------------------------
ALTER FUNCTION  [dbo].[FN_Order_IsProductTax]  
(  
	@AutoId int,  
	@orderAutoId int  
)  
RETURNS decimal(18,2)  
AS  
BEGIN  
	DECLARE @Total decimal(18,2)=0.00      
	IF (select tax from OrderItemMaster where AutoId=@AutoId)=1
	BEGIN  
		set @Total=ISNULL((select isnull((oim.NetPrice*om.TaxValue)/100,0) as Tax from OrderItemMaster as oim 
					inner join OrderMaster as om on om.AutoId=oim.OrderAutoId
					where oim.AutoId=@AutoId),0) 
		
	END  
	RETURN @Total  
END  
---------------------------------------------------------------------------------------------------------------------

alter table [psmct.easywhm.com].[dbo].[OrderItemMaster] add
[AddedItemKey] [varchar](50),
[AddOnPackedPeice] [int],[Oim_Discount] [decimal](18, 2) ,
[Oim_ItemTotal]  AS ([dbo].[FN_Order_OIMItemtotalAmount]([autoid],[OrderAutoid])),
[IsProductTax]  AS ([dbo].[FN_Order_IsProductTax]([AutoId],[OrderAutoId])),
[IsProductTaxPerUnit]  AS ([dbo].[FN_Order_IsProductTaxPerUnit]([AutoId],[OrderAutoId])),
[Oim_DiscountAmount] [decimal](18, 2)

------------------------------------------------------------------------------------------------------------------------------
alter table [psmct.easywhm.com].[dbo].[tbl_OrderLog] add [ActionIPAddress] varchar(100)

alter table [psmct.easywhm.com].[dbo].[CreditMemoLog] add [ActionIPAddress] varchar(100)

alter table [psmct.easywhm.com].[dbo].[CustomerPaymentDetails] add [MigrationType] int

alter table [psmct.easywhm.com].[dbo].[PaymentCurrencyDetails] add [OldTotalAmount] [decimal](10, 2),[P_CurrencyValue] [decimal](10, 2)

alter table [psmct.easywhm.com].[dbo].[Delivered_Order_Items] add
[Del_discount] [decimal](18, 2),
[Del_ItemTotal]  AS ([dbo].[FN_Delivered_ItemTotal]([AutoId])),
[IsDel_ProductTax]  AS ([dbo].[FN_DelOrder_IsProductTax]([AutoId],[OrderAutoId])),
[IsProductTaxPerUnit]  AS ([dbo].[FN_DelOrder_IsProductTaxPerUnit]([AutoId],[OrderAutoId])),
[StaticDelQty] [int],
[StaticNetPrice] [decimal](18, 2)

----------------------------------------------------------------------------------------------------------------------------------
CREATE or alter FUNCTION  [dbo].[FN_Delivered_ItemTotal]
(
	 @AutoId int 
)
RETURNS decimal(10,2)
AS
BEGIN
	DECLARE @NetAmount decimal(10,2)		 	 
	SET @NetAmount=cast((SELECT ((UnitPrice/QtyPerUnit) * QtyDel) FROM [Delivered_Order_Items] WHERE AutoId=@AutoId) as decimal	(10,2))	 
	RETURN @NetAmount
END

GO
ALTER TABLE [dbo].[Delivered_Order_Items]  drop column Del_ItemTotal

ALTER TABLE [dbo].[Delivered_Order_Items] ADD  Del_ItemTotal  AS ([dbo].[FN_Delivered_ItemTotal]([AutoId]))
------------------------------------------------------------------------------------------------------------------------
create   FUNCTION  [dbo].[FN_DelOrder_IsProductTax]  
(  
	@AutoId int,  
	@orderAutoId int  
)  
RETURNS decimal(18,2)  
AS  
BEGIN  
	DECLARE @Total decimal(18,2)=0.00,    
	 @Tax int=null  
	IF (select tax from Delivered_Order_Items where AutoId=@AutoId)=1
	BEGIN  
		set @Total=ISNULL((select isnull((oim.NetPrice*om.TaxValue)/100,0) as Tax from Delivered_Order_Items as oim 
					inner join OrderMaster as om on om.AutoId=oim.OrderAutoId
					where oim.AutoId=@AutoId),0) 

	END  
	RETURN @Total  
END  
-----------------------------------------------------------------------------------------------------------------------------

alter table [psmct.easywhm.com].[dbo].[CreditMemoMaster] add [CreditMemoType] int,
[ReferenceOrderAutoId] int,
[TotalTax_Old] decimal(18,2)
  
------------------------------------------------------------------------------------------------------------------------------

alter table [psmct.easywhm.com].[dbo].[CreditItemMaster] add 
[OM_MinPrice] decimal(18,2),
[OM_CostPrice] decimal(18,2),
[OM_BasePrice] decimal(18,2)

------------------------------------------------------------------------------------------------------------------------------
