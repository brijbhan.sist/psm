﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DllLogin;

public partial class Default : System.Web.UI.Page
{
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Session.Abandon();
        if (ConfigurationManager.AppSettings["Mode"] == "TestMode")
        {
            Session["Location"] = "psmnj";
        }
        else
        {
            Session["Location"] = (Request.Url.Host.ToString().Split('.')[0]).Replace("1", "");
        }
        
       
        if (Session["PageTitleName"] == null)
        {
            TitleName();
        }
        Page.Title = Session["PageTitleName"].ToString().ToUpper();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }

    public void TitleName()
    {
        PL_Login pobj = new PL_Login();
        BL_Login.Bindlogo(pobj);
        string Title = (pobj.Ds.Tables[0].Rows[0][1].ToString());
        Session.Add("PageTitleName", Title);
    }
}