﻿using DllDriverOrderMaster;
using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;

public partial class Driver_TodaysOrders : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Driver/JS/todaysOrders.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredFields'>" + text + "</script>"
                ));
        }
    }

    [WebMethod(EnableSession = true)]
    public  static string getTodayOrders(string dataValue)
    {
        PL_DriverOrderMaster pobj = new PL_DriverOrderMaster();
        try
        {

            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.DrvAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                pobj.OrderNo = jdv["OrderNo"];
                if (jdv["FromDate"] != "")
                {
                    pobj.Fromdate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != "")
                {
                    pobj.Todate = Convert.ToDateTime(jdv["ToDate"]);
                }
                if (jdv["CustomerAutoId"] != "")
                {
                    pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                }
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                pobj.SalesPersonAutoId = Convert.ToInt32(jdv["SalesPersonAutoId"]);
                pobj.OrderStatus = Convert.ToInt32(jdv["StatusAutoId"]);
                BL_DriverOrderMaster.getDrvAsgnOrder(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "session";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public static string Pick_Order(string OrderAutoId)
    {
        PL_DriverOrderMaster pobj = new PL_DriverOrderMaster();
        try
        {
            pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
            pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
            BL_DriverOrderMaster.Pick_Order(pobj);
            if (!pobj.isException)
            {
                return pobj.exceptionMessage;
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod(EnableSession = true)]
    public static string saveDelStatus(string DelStatus, string xml)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(DelStatus);
            PL_DriverOrderMaster pobj = new PL_DriverOrderMaster();
            try
            {
                string t = xml;
                pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
                pobj.Delivered = jdv["Delivered"];
                pobj.Remarks = jdv["Remarks"];
                pobj.DocumentDetails = t;
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);

                BL_DriverOrderMaster.saveDelStatus(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "SessionExpired";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string viewOrder(string OrderAutoId)
    {
        PL_DriverOrderMaster pobj = new PL_DriverOrderMaster();
        try
        {
            pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
            BL_DriverOrderMaster.viewOrder(pobj);
            if (!pobj.isException)
            {
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod(EnableSession = true)]
    public static string bindDropdown()
    {
        PL_DriverOrderMaster pobj = new PL_DriverOrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.LoginEmpType = Convert.ToInt32(1);
                pobj.SalesPersonAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                BL_DriverOrderMaster.bindDropdown(pobj);
                if (!pobj.isException)
                {
                    string json = "";
                    foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                    {
                        json += dr[0].ToString();
                    }
                    if (json == "")
                    {
                        json = "[]";
                    }
                    return json;
                    //return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string bindCustomerSalesPersonWise(string SalesPersonAutoId)
    {
        PL_DriverOrderMaster pobj = new PL_DriverOrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.SalesPersonAutoId = Convert.ToInt32(SalesPersonAutoId);
                BL_DriverOrderMaster.bindCustomerSalesPersonWise(pobj);
                if (!pobj.isException)
                {
                    string json = "";
                    foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                    {
                        json += dr[0].ToString();
                    }
                    if (json == "")
                    {
                        json = "[]";
                    }
                    return json;
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string loadbarcodelog(string OrderAutoId, string Barcode)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {

            PL_DriverOrderMaster pobj = new PL_DriverOrderMaster();
            try
            {
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                pobj.Barcode = Barcode;
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);

                BL_DriverOrderMaster.loadbarcodelog(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "SessionExpired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string unloadbarcodelog(string OrderAutoId, string Barcode)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
           
            PL_DriverOrderMaster pobj = new PL_DriverOrderMaster();
            try
            {
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                pobj.Barcode = Barcode;
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);

                BL_DriverOrderMaster.unloadbarcodelog(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "SessionExpired";
        }
    }
}

