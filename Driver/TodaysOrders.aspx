﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" ClientIDMode="Static" Inherits="Driver_TodaysOrders" CodeFile="TodaysOrders.aspx.cs" %>

<script runat="server">

</script>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        @media (min-width: 768px) {
            .form-group {
                margin-top: 5px;
            }
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input type="hidden" id="txtHOrderAutoId" />

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Order List</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Order</a></li>
                        <li class="breadcrumb-item">Order List</li>
                    </ol>
                </div>
            </div>
        </div>

    </div>

    <div id="panelOrderList">

        <div class="content-body" style="min-height: 400px">
            <section id="drag-area2">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <div class="row form-group">
                                        <div class="col-sm-12 col-md-4">
                                            <input type="text" class="form-control border-primary input-sm" placeholder="Order No" id="txtSOrderNo" />
                                        </div>
                                        <div class="col-sm-12 col-md-4">
                                            <select class="form-control border-primary input-sm" style="width: 100% !important" onchange=" bindCustomerSalesPersonWise()" id="ddlSalesPerson">
                                                <option value="0">All Sales Person</option>
                                            </select>
                                        </div>

                                        <div class="col-sm-12 col-md-4">
                                            <select id="ddlStatus" style="width: 100% !important" class="form-control border-primary input-sm">
                                                <option value="0">All</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-sm-12 col-md-4">
                                            <select id="ddlCustomer" style="width: 100% !important" class="form-control border-primary input-sm">
                                                <option value="0">All Customer</option>
                                            </select>
                                        </div>

                                        <div class="col-sm-12 col-md-4">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="padding: 0rem 0.5rem;">From Order Date <span class="la la-calendar-o"></span>
                                                    </span>
                                                </div>
                                                <input type="text" class="form-control border-primary input-sm" onchange="setdatevalidation(1)" placeholder="Order From Date" id="txtFromDate" />
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-4">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="padding: 0rem 0.5rem;">To Order Date <span class="la la-calendar-o"></span>
                                                    </span>
                                                </div>
                                                <input type="text" class="form-control border-primary input-sm" onchange="setdatevalidation(2)" placeholder="Order To Date" id="txtToday" />
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-12 mt-1">
                                            <button type="button" class="btn btn-info buttonAnimation round box-shadow-1 pull-right  btn-sm" onclick="getTodayOrders(1)" id="btnSearch">Search</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <div class="row form-group">
                                        <div class="col-sm-12 col-md-12">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered" id="tblTodaysOrders">
                                                    <thead class="bg-blue white">
                                                        <tr>
                                                            <td class="Action text-center">Action</td>
                                                            <td class="OrderNo text-center">Order No</td>
                                                            <td class="OrderDt text-center">Order Date</td>
                                                            <td class="Status text-center">Status</td>
                                                            <td class="SalesPerson">Sales Person</td>
                                                            <td class="Customer">Customer</td>
                                                            <td class="OrderVal price">Payable Amount</td>
                                                            <td class="PackedBoxes text-center">Packed Boxes</td>
                                                            <td class="RootName text-center">Assigned Root</td>
                                                            <td class="Stop text-center">Stop</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                                <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="d-flex">
                                        <div class="form-group">
                                            <select id="ddlPaging" class="form-control input-sm border-primary" onchange="getTodayOrders(1)">
                                                <option value="10">10</option>
                                                <option value="50">50</option>
                                                <option value="100">100</option>
                                                <option value="500">500</option>
                                                <option value="1000">1000</option>
                                                <option value="0">All</option>
                                            </select>
                                        </div>
                                        <div class="ml-auto">
                                            <div class="Pager"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>

    </div>
    <div class="content-body" style="display: none" id="PopBarCodeforPickBox">
        <section id="drag-area4">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-12">
                                        <h4 class="content-header-title">Scan Packed Boxes (Load Order)</h4>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-2 col-sm-3 col-xs-3 form-group">
                                        Order No :
                                    </div>
                                    <div class="col-md-3 col-sm-9 col-xs-9 form-group">
                                        <input type="text" id="lblOrderno" class="form-control input-sm" readonly />
                                    </div>

                                    <div class="col-md-2 col-sm-3 col-xs-3 form-group">
                                        No Of Boxes :
                                    </div>
                                    <div class="col-md-3 col-sm-9 col-xs-9 form-group">
                                        <input type="text" id="lblNoOfbox" class="form-control input-sm" readonly />
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-2 col-sm-3 col-xs-3 form-group">
                                        Barcode :
                                    </div>
                                    <div class="col-sm-3 col-md-3 form-group">
                                        <input type="text" id="txtReadBorCode" onkeypress="return AvoidSpace(event)" class="form-control input-sm" onchange="readBoxBarcode()" />
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: right">
                                        <span id="baxbarmsg"></span>
                                    </div>

                                </div>
                                <div class="row form-group">
                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblBarcode">
                                                <thead class="bg-blue white">

                                                    <tr>
                                                        <td class="SRNO text-center">SN</td>
                                                        <td class="Barcode text-center">Barcode</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">

                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group text-right">
                                        <button type="button" id="btnBack" class="btn btn-danger buttonAnimation round box-shadow-1  btn-sm" onclick="BackOrder()" style="margin-right: 10px"><b>Back</b></button>
                                        <button type="button" id="btnPick1" class="btn btn-primary buttonAnimation round box-shadow-1  btn-sm" style="display: none; margin-right: 10px" onclick="PickedOrder()"><b>Save</b></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="content-body" id="PopDriverDropOrder" style="display: none">
        <section id="drag-area5">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-12">
                                        <h4 class="content-header-title">Scan Order (Unload Order)</h4>
                                        <input type="hidden" id="txtOrderId" />
                                        <input type="hidden" id="OrderAutoId" />
                                        <input type="hidden" id="txtPackedBoxes" />
                                        <input type="hidden" id="CustomerAutoId" />
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-2 col-sm-2 col-xs-2">
                                        Order No :
                                    </div>
                                    <div class="col-md-3 col-sm-4 col-xs-4 form-group">
                                        <input type="text" id="OrderNo" style="width: 265px !important; margin-left: 8px;" class="form-control input-sm" readonly />
                                    </div>

                                    <div class="col-md-2 col-sm-4 col-xs-2">
                                        No Of Boxes :
                                    </div>
                                    <div class="col-md-3 col-sm-2 col-xs-4 form-group">
                                        <input type="text" id="NoOfBoxes" style="width: 265px !important; margin-left: 8px;" class="form-control input-sm" readonly />
                                    </div>
                                </div>
                                <div class="row scanOrder form-group">
                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <b style="text-decoration: underline">Read Packed Boxes Barcode</b>
                                    </div>
                                </div>
                                <div class="row scanOrder form-group">
                                    <div class="col-md-2 col-sm-2 col-xs-3">
                                        Barcode :
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-6 form-group">
                                        <input type="text" id="txtBarCode" style="width: 265px !important; margin-left: 8px;" onkeypress="return AvoidSpace(event)" class="form-control input-sm" onchange="readBoxBarcode1()" />
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: right">
                                        <span id="Span1"></span>
                                    </div>
                                </div>
                                <div class="row scanOrder form-group">
                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="Table1">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="SRNO text-center">SN</td>
                                                        <td class="Barcode  text-center">Barcode</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group" id="delivery">

                                    <div class="col-md-5">
                                        <div class="row">
                                            <div class="control-label col-md-5 col-sm-6 col-xs-6">Sign Invoice</div>
                                            <div class="col-md-7 col-sm-6 col-xs-6 form-group">
                                                <input type="file" class="form-control" id="Imageupload" onchange="readURL(this,1)" />
                                                <%--   <span style="color:red;"><i>[Note : Sign Invoice/ Delivery Proof Other]</i></span><br />--%>
                                                <a href="#" id="hrfviewfile" style="display: none;" target="_blank">View File</a>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="control-label col-md-5 col-sm-6 col-xs-6">Delivery Proof</label>
                                            <div class="col-md-7 col-sm-6 col-xs-6 form-group">
                                                <input type="file" class="form-control" id="fudDeliveryProof" onchange="readURL(this,2)" />
                                                <a href="#" id="hrfDeliveryProof" style="display: none;" target="_blank">View File</a>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <label class="control-label col-md-5 col-sm-6 col-xs-6">Other</label>
                                            <div class="col-md-7 col-sm-6 col-xs-6 form-group">
                                                <input type="file" class="form-control" id="fudOther" onchange="readURL(this,3)" />
                                                <a href="#" id="hrfOther" style="display: none;" target="_blank">View File</a>
                                                <br />
                                                <span style="color: red;"><i>[Allowed File : JPG, JPEG, PNG]</i></span>
                                                <br />
                                                <span style="color: red;"><i>[Max size is : 1MB]</i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="row">
                                            <div class="control-label col-md-5 col-sm-6 col-xs-6">Delivered</div>
                                            <div class="col-md-7 col-sm-6 col-xs-6 form-group">
                                                <div class="form-control input-sm">
                                                    <input type="radio" class="radio-inline" name="rblDeliver" value="yes" />&nbsp;Yes
                                            <input type="radio" class="radio-inline" name="rblDeliver" value="no" />&nbsp;No 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="control-label col-md-5 col-sm-6 col-xs-62">Delivery Remark</label>
                                            <div class="col-md-7 col-sm-6 col-xs-6 form-group">
                                                <textarea class="form-control input-sm" rows="7" placeholder="Enter Delivery Remark" id="txtRemarks" maxlength="500"></textarea>
                                                <span style="color: red;"><i>[Note : Remark upto 500 characters]</i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group text-right">
                                        <button type="button" class="btn btn-primary buttonAnimation round box-shadow-1  btn-sm" style="color: #fff; display: none" id="ReceivePayment" onclick="ReceivePayment2();">Receive Payment</button>
                                        <button type="button" id="btnCancel" class="btn btn-danger buttonAnimation round box-shadow-1  btn-sm" onclick="loadBackOrder()" style="margin-right: 10px"><b>Back</b></button>
                                        <button type="button" id="btnSaveDelStatus" class="btn btn-success buttonAnimation round box-shadow-1  btn-sm" onclick="saveDelStatus()" style="display: none; margin-right: 10px"><b>Save</b></button>
                                        <button type="button" id="btnUpdate" class="btn btn-warning buttonAnimation round box-shadow-1  btn-sm" onclick="saveDelStatus()" style="display: none; margin-right: 15px"><b>Update</b></button>&nbsp;&nbsp;&nbsp;
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</asp:Content>
