﻿$(document).ready(function () {
    $('.date').datepicker({
        changeMonth: true,       //this option for allowing user to select month
        changeYear: true,       //this option for allowing user to select from year range
        dateFormat: 'mm/dd/yy' //US date format
    });
    
    getAssignedOrdersList();
});

function getAssignedOrdersList() {
    var data = {       
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val()
    };

    $.ajax({
        type: "Post",
        url: "/Manager/WebAPI/asgnOrderList.asmx/getAssignedOrdersList",
        data: "{'searchValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var Orderlist = $(xmldoc).find("Table");

            $("#tblAssignedOrders tbody tr").remove();
            var row = $("#tblAssignedOrders thead tr").clone(true);
            if (Orderlist.length > 0) {
                $("#EmptyTable").hide();
                $.each(Orderlist, function () {
                    $(".AsgnDt", row).text($(this).find("AssignDate").text());
                    $(".AsgnOrders", row).text($(this).find("TotalOrders").text());
                    $(".Action", row).html("<a href='javascript:;' onclick='get_Drv_Asgn_Order(this)'>View</a>");
                    $("#tblAssignedOrders tbody").append(row);
                    row = $("#tblAssignedOrders tbody tr:last").clone(true);
                });
            } else {
                $("#EmptyTable").show();
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function get_Drv_Asgn_Order(e) {
    var tr = $(e).closest("tr");

    var data = {        
        AsgnDate: $(tr).find(".AsgnDt").text(),
    };

    $.ajax({
        type: "Post",
        url: "/Manager/WebAPI/asgnOrderList.asmx/getDrvAsgnOrder",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var Orderlist = $(xmldoc).find("Table");
            var root = $(xmldoc).find("Table1");

            $("#modalAsgnOrders").modal("toggle");           
            $("#asgnDt").text($(tr).find(".AsgnDt").text());
            $("#Root").text($(root).find("Root").text());

            $("#tblDrvAsgn tbody tr").remove();
            var row = $("#tblDrvAsgn thead tr").clone(true);
            if (Orderlist.length > 0) {
                $.each(Orderlist, function () {
                    $(".OrderNo", row).html("<span orderautoid=" + $(this).find("AutoId").text() + ">" + $(this).find("OrderNo").text() + "</span>");
                    $(".OrderDt", row).text($(this).find("OrderDate").text());
                    $(".Customer", row).text($(this).find("CustomerName").text());
                    $(".OrderVal", row).text($(this).find("GrandTotal").text());
                    $(".Stop", row).text($(this).find("Stoppage").text());

                    $("#tblDrvAsgn tbody").append(row);
                    row = $("#tblDrvAsgn tbody tr:last").clone(true);
                });
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}