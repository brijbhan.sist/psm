﻿var CurrentDate;
function ReceivePayment2() {
    location.href = "/Sales/PayMaster.aspx?PageId=" + $("#CustomerAutoId").val();
}

$(document).ready(function () {
    $('#txtFromDate').pickadate({
        min: new Date("01/01/2019"),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtToday').pickadate({
        min: new Date("01/01/2019"),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    var d = new Date();
    var month = d.getMonth() + 1;
    if (month.toString().length == 1) {
        month = '0' + month;
    }
    var day = d.getDate();
    if (day.toString().length == 1) {
        day = '0' + day;
    }
    var year = d.getFullYear();
    CurrentDate = month + '-' + day + '-' + year
    $("#txtFromDate").val(month + '/' + day + '/' + year);
    $("#txtToday").val(month + '/' + day + '/' + year);
    bindDropdown();
    getTodayOrders(1);

    $("input[name='rblDeliver']").change(function () {
        if ($(this).val() == 'yes') {
            $("input[name='rblPaymentReceive']").attr("disabled", false);
            $("#DriverComment").show();
        } else {
            $("input[name='rblPaymentReceive']").attr("disabled", true).prop("checked", false);

            $("#DriverComment").hide();
        }
    });

});

function setdatevalidation(val) {
    var fdate = $("#txtFromDate").val();
    var tdate = $("#txtToday").val();
    if (val == 1) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtToday").val(fdate);
        }
    }
    else if (val == 2) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtFromDate").val(tdate);
        }
    }
}
function closePacked() {
    if (confirm('Are you sure you want to close')) {
        $('#PopBarCodeforPickBox').modal('hide');
    }
}
function Pagevalue(e) {
    getTodayOrders(parseInt($(e).attr("page")));
};
function getTodayOrders(PageIndex) {
    var data = {
        OrderNo: $("#txtSOrderNo").val().trim(),
        FromDate: $("#txtFromDate").val(),
        ToDate: $("#txtToday").val(),
        CustomerAutoId: $("#ddlCustomer").val(),
        SalesPersonAutoId: $("#ddlSalesPerson").val(),
        StatusAutoId: $("#ddlStatus").val(),
        PageSize: $("#ddlPaging").val(),
        PageIndex: PageIndex
    }
    $.ajax({
        type: "Post",
        url: "/Driver/TodaysOrders.aspx/getTodayOrders",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",

        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == 'session') {
                location.href = '/';
            } else {
                var xmldoc = $.parseXML(response.d);
                var order = $(xmldoc).find("Table");
                var root = $(xmldoc).find("Table2");

                $("#tblTodaysOrders tbody tr").remove();
                var row = $("#tblTodaysOrders thead tr").clone(true);
                if (order.length > 0) {
                    $("#EmptyTable").hide();
                    $.each(order, function () {

                        $(".OrderNo", row).html("<span CustomerAutoId=" + $(this).find("CustomerAutoId").text() + " orderautoid=" + $(this).find("AutoId").text() + ">" + $(this).find("OrderNo").text() + "</span>");
                        $(".OrderDt", row).text($(this).find("OrderDate").text());
                        $(".Customer", row).text($(this).find("CustomerName").text());
                        $(".PackedBoxes", row).text($(this).find("PackedBoxes").text());
                        $(".OrderVal", row).text($(this).find("GrandTotal").text());
                        $(".SalesPerson", row).text($(this).find("SalesPerson").text());
                        $(".OrderVal", row).css('text-align', 'right')
                        $(".Stop", row).text($(this).find("Stoppage").text());
                        $(".RootName", row).text($(this).find("RootName").text());
                        if (Number($(this).find("Status").text()) == 4) {
                            $(".Status", row).html("<span statusCode='" + $(this).find("Status").text() + "' class='badge badge badge-pill badge-primary mr-2' style='background-color:#669900'>" + $(this).find("StatusType").text() + "</span>");
                        } else if (Number($(this).find("Status").text()) == 5) {
                            $(".Status", row).html("<span statusCode='" + $(this).find("Status").text() + "' class='badge badge badge-pill badge-warning mr-2' style='background-color:#33cc33'>" + $(this).find("StatusType").text() + "</span>");
                        } else if (Number($(this).find("Status").text()) == 6) {
                            $(".Status", row).html("<span statusCode='" + $(this).find("Status").text() + "' class='badge badge badge-pill badge-success mr-2'>" + $(this).find("StatusType").text() + "</span>");
                        } else {
                            $(".Status", row).html("<span statusCode='" + $(this).find("Status").text() + "'>" + $(this).find("StatusType").text() + "</span>");
                        }
                        if ($(this).find("Status").text() == 4) {
                            $(".Action", row).html("&nbsp;&nbsp;<a href='javascript:;' onclick='pickOrder(this)'>Load</a>&nbsp;&nbsp;</span>");
                        } else if ($(this).find("Status").text() == 5) {
                            $(".Action", row).html("<span class='pick'><a href='javascript:;' onclick='DriverDropOrder(this)'>Unload Order</a></span>");
                        } else {
                            $(".Action", row).html("<a href='javascript:;' onclick='viewOrder(this)'><span class='la la-eye'></span></a><span class='pick'></span>");
                        }
                        $("#tblTodaysOrders tbody").append(row);
                        row = $("#tblTodaysOrders tbody tr:last").clone(true);
                    });
                } else {
                    $("#EmptyTable").show();
                }
                var pager = $(xmldoc).find("Table1");
                $(".Pager").ASPSnippets_Pager({
                    ActiveCssClass: "current",
                    PagerCssClass: "pager",
                    PageIndex: parseInt(pager.find("PageIndex").text()),
                    PageSize: parseInt(pager.find("PageSize").text()),
                    RecordCount: parseInt(pager.find("RecordCount").text())
                });
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function viewOrder(e) {

    var tr = $(e).closest('tr');
    $("#panelOrderList").hide();
    $("#PopDriverDropOrder").show();
    $("#optexport").hide();
    $("#Span1").html('');
    $.ajax({
        type: "Post",
        url: "/Driver/TodaysOrders.aspx/viewOrder",
        data: "{'OrderAutoId':'" + tr.find(".OrderNo span").attr("orderautoid") + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var Details = $(xmldoc).find("Table");
            var DocumentDetails = $(xmldoc).find("Table1");
            $('.scanOrder').hide();
            $("#OrderNo").val($(Details).find('OrderNo').text());
            $("#NoOfBoxes").val($(Details).find('PackedBoxes').text());
            $("#txtHOrderAutoId").val($(Details).find('OrderAutoId').text());
            $("#CustomerAutoId").val($(Details).find('CustomerAutoId').text());
            $("#txtRemarks").val($(Details).find('DrvRemarks').text());
            $("#txtComment").val($(Details).find('Comment').text());
            $("#ddlCommentType").val($(Details).find('CommentType').text());
            if ($(Details).find('Status').text() == 6 || $(Details).find('Status').text() == 11) {
                $("input[name='rblDeliver'][value='yes']").prop("checked", true);
                $('#DriverComment').show();

            } else if ($(Details).find('Status').text() == 7) {
                $("input[name='rblDeliver'][value='no']").prop("checked", true);
                $('#DriverComment').hide();
            }
            $("#delivery").show()
            $('#btnUpdate').show();
            $("#ReceivePayment").show();
            if ($(Details).find('Status').text() == 11) {
                $('#btnUpdate').hide();
                $('#txtRemarks').attr('disabled', true);
                $("input[type=radio]").attr('disabled', true);
            } else {
                $('#txtRemarks').attr('disabled', false);
                $("input[type=radio]").attr('disabled', false);
            }
            debugger
            $.each(DocumentDetails, function (index, cx) {
                if ($(this).find('fileType').text() == "Signed Invoice") {
                    $("#hrfviewfile").attr("href", $(this).find('PhysicalPath').text()); //
                    $("#hrfviewfile").show();
                }
                else if ($(this).find('fileType').text() == "Delivery Proof") {
                    $("#hrfDeliveryProof").attr("href", $(this).find('PhysicalPath').text()); //
                    $("#hrfDeliveryProof").show();
                } else if ($(this).find('fileType').text() == "Other") {
                    $("#hrfOther").attr("href", $(this).find('PhysicalPath').text()); //
                    $("#hrfOther").show();
                }               
            });
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function BacktoOrder() {
    var len = $("#tblBarcode tbody tr").length;
    $("#optexport").show();
    if (len == 0) {
        $("#panelOrderList").show();
        $("#PopBarCodeforPickBox").hide();
    } else {
        $("#panelOrderList").show();
        $("#PopBarCodeforPickBox").hide();
    }
}

function BackOrder() {
    swal({
        title: "Are you sure?",
        text: "You want to go back.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, Cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, Back it!",
                value: true,
                visible: true,
                className: "",
                closeModal: true
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            BacktoOrder();

        }
    });
};

var NoofBoxRead = 0;

function pickOrder(e) {
    var tr = $(e).closest("tr");
    $("#txtOrderId").val(tr.find(".OrderNo").text());
    $("#OrderAutoId").val(tr.find(".OrderNo span").attr("orderautoid"));
    $("#txtPackedBoxes").val(tr.find(".PackedBoxes").text());
    $("#lblOrderno").val(tr.find(".OrderNo").text());
    $("#lblNoOfbox").val(tr.find(".PackedBoxes").text());
    $("#txtReadBorCode").val('');
    NoofBoxRead = 0;
    $("#txtReadBorCode").attr('disabled', false);
    $("#baxbarmsg").html('');
    $("#panelOrderList").hide();
    $("#PopBarCodeforPickBox").show();
    $("#btnPick1").hide();
    $("#txtReadBorCode").focus();
    $("#tblBarcode tbody tr").remove();
    $("#optexport").hide();
}

var NoofBoxRead = 0;

function DriverDropOrder(e) {
    NoofBoxRead = 0;
    var tr = $(e).closest('tr');
    $("#PopDriverDropOrder").show();
    $("#panelOrderList").hide();
    $('.scanOrder').show();
    $('#btnUpdate').hide();
    $('#txtBarCode').attr('disabled', false);
    $('#txtRemarks').attr('disabled', true);
    $('#Table1 tbody tr').remove();
    $('#txtRemarks').val('');
    $('#OrderNo').val($(tr).find('.OrderNo').text());
    $('#NoOfBoxes').val($(tr).find('.PackedBoxes').text());
    $('#btnSaveDelStatus').hide();
    $('#Span1').html('');
    $('#txtHOrderAutoId').val($(tr).find('.OrderNo span').attr('OrderAutoId'));
    $('#CustomerAutoId').val($(tr).find('.OrderNo span').attr('CustomerAutoId'));
    $("input[name='rblDeliver']").prop('checked', true);
    $('#delivery').hide();
    $('#txtBarCode').focus();
}

function readBoxBarcode() {

    var checkbarcode = true;
    var count = 0;
    var str = $("#txtReadBorCode").val();
    str = str.replace(/\s/g, '');
    $("#txtReadBorCode").val(str);
    $("#tblBarcode tbody tr").each(function (index, item) {
        count++;
        if (($("#txtReadBorCode").val().trim()) == $(item).find('.Barcode').text().trim()) {
            checkbarcode = false;
        }
    });
    var checkivalid = true;
    if (checkbarcode == true) {
        if (count < $("#txtPackedBoxes").val()) {
            for (var i = 1; i <= $("#txtPackedBoxes").val(); i++) {
                var str = $("#txtReadBorCode").val().trim().split('/');
                var ReadBorCode = str[str.length - 2];
                var value = str[str.length - 1];
                if ((Number(value) == Number(i)) && ReadBorCode.toUpperCase() == $("#lblOrderno").val().toUpperCase()) {
                    checkivalid = false;
                    NoofBoxRead = (Number(NoofBoxRead) + 1);
                    var PackedBoxes = $("#txtPackedBoxes").val();
                    var DuePackedBox = (Number(PackedBoxes) - Number(NoofBoxRead))

                    $("#txtReadBorCode").val('');
                    $("#txtReadBorCode").focus();
                    if (Number(DuePackedBox) == 0) {
                        $("#txtReadBorCode").attr('disabled', true);
                        toastr.success('All boxes picked.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                        $("#btnPick1").show();
                        var row = $("#tblBarcode thead tr").clone(true);

                        $(".SRNO", row).html(NoofBoxRead);
                        $(".Barcode", row).text($("#txtOrderId").val() + '/' + i);
                        $("#tblBarcode tbody").append(row);
                        i = Number($("#txtPackedBoxes").val() + 1);

                    } else {
                        toastr.success(DuePackedBox + ' Out Of ' + $("#txtPackedBoxes").val() + ' Remain', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                        var row = $("#tblBarcode thead tr").clone(true);
                        $(".SRNO", row).html(NoofBoxRead);
                        $(".Barcode", row).text($("#txtOrderId").val() + '/' + i);
                        $("#tblBarcode tbody").append(row);
                        i = Number($("#txtPackedBoxes").val() + 1);

                    }


                }
            }
            if (checkivalid) {
                toastr.error('Invalid Barcode.', 'Error', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            }
            else {
                $.ajax({
                    type: "Post",
                    url: "/Driver/TodaysOrders.aspx/loadbarcodelog",
                    data: "{'OrderAutoId':'" + $("#OrderAutoId").val() + "','Barcode':'" + ReadBorCode + '/' + value + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $.blockUI({
                            message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                            overlayCSS: {
                                backgroundColor: '#FFF',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (response) {
                        if (response.d = "true") {
                            return true
                        }
                    },
                    error: function (result) {
                        console.log(JSON.parse(result.responseText).d);
                    },
                    failure: function (result) {
                        console.log(JSON.parse(result.responseText).d);
                    }
                });
            }

        }
        else {
            $("#txtReadBorCode").attr('disabled', true);
            toastr.success('All boxes picked.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            $("#btnPick1").show();
        }
    } else {
        toastr.error('This boxes already picked.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
    $("#txtReadBorCode").focus();
    $("#txtReadBorCode").val('');
}

function PickedOrder() {
    $("#panelOrderList").show();
    $("#PopBarCodeforPickBox").hide();
    $.ajax({
        type: "Post",
        url: "/Driver/TodaysOrders.aspx/Pick_Order",
        data: "{'OrderAutoId':'" + $("#OrderAutoId").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            getTodayOrders(1);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function bindDropdown() {

    $.ajax({
        type: "POST",
        url: "/Driver/TodaysOrders.aspx/bindDropdown",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var getData = $.parseJSON(response.d);
                $("#ddlStatus option:not(:first)").remove();
                $.each(getData[0].Sts, function (index, status) {
                    $("#ddlStatus").append("<option value='" + status.AutoId + "'>" + status.StatusType + "</option>");
                });
                $("#ddlCustomer option:not(:first)").remove();
                $.each(getData[0].CL, function (index, cx) {
                    $("#ddlCustomer").append("<option value='" + cx.AutoId + "'>" + cx.Customer + "</option>");
                });
                $("#ddlCustomer").select2();
                $("#ddlSalesPerson option:not(:first)").remove();
                $.each(getData[0].sL, function (index, sp) {
                    $("#ddlSalesPerson").append("<option value='" + sp.AutoId + "'>" + sp.EmpName + "</option>");
                    $("#ddlSalesPerson").select2();
                });
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function bindCustomerSalesPersonWise() {

    $.ajax({
        type: "POST",
        url: "/Driver/TodaysOrders.aspx/bindCustomerSalesPersonWise",
        data: "{'SalesPersonAutoId':'" + $("#ddlSalesPerson").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var getData = $.parseJSON(response.d);
                $("#ddlCustomer option:not(:first)").remove();
                $.each(getData[0].CL, function (index, cx) {
                    $("#ddlCustomer").append("<option value='" + cx.AutoId + "'>" + cx.Customer + "</option>");
                });
                $("#ddlCustomer").select2();
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}


function PickedOrder1() {
    $('.scanOrder').show();
}

function readBoxBarcode1() {
   
    $("input[name='rblDeliver']").prop('checked', false);
    var checkbarcode = true;
    var count = 0;
    var str = $("#txtBarCode").val();
    str = str.replace(/\s/g, '');
    $("#txtBarCode").val(str);
    $("#Table1 tbody tr").each(function (index, item) {
        count++;
        if (($("#txtBarCode").val().trim()) == $(item).find('.Barcode').text().trim()) {
            checkbarcode = false;
        }
    });
    if (checkbarcode) {
        if (count < $("#NoOfBoxes").val()) {
            var checkinvalid = true;
            for (var i = 1; i <= $("#NoOfBoxes").val(); i++) {

                var str = $("#txtBarCode").val().trim().split('/');
                var ReadBorCode = str[str.length - 2];
                var value = str[str.length - 1];
                if ((Number(value) == Number(i)) && ReadBorCode == $("#OrderNo").val()) {
                    checkinvalid = false;
                    NoofBoxRead = (Number(NoofBoxRead) + 1);
                    var PackedBoxes = $("#NoOfBoxes").val();
                    var DuePackedBox = (Number(PackedBoxes) - Number(NoofBoxRead))

                    $("#Span1").css('color', 'Green');
                    $("#txtBarCode").val('');
                    $("#txtBarCode").focus();
                    if (Number(DuePackedBox) == 0) {
                        $("#txtBarCode").attr('disabled', true);
                        $("#txtRemarks").attr('disabled', false);
                        toastr.success('All boxes picked.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                        $('#delivery').show();
                        var row = $("#Table1 thead tr").clone(true);
                        $(".SRNO", row).html(NoofBoxRead);
                        $(".Barcode", row).text($("#OrderNo").val() + '/' + i);
                        $("#Table1 tbody").append(row);
                        i = Number($("#NoOfBoxes").val()) + 1;
                        $('#btnPickedOrder').hide();
                        $('#btnSaveDelStatus').show();

                    } else {
                        toastr.success(DuePackedBox + ' Out Of ' + $("#NoOfBoxes").val() + ' Remain', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                        var row = $("#Table1 thead tr").clone(true);
                        $(".SRNO", row).html(NoofBoxRead);
                        $(".Barcode", row).text($("#OrderNo").val() + '/' + i);
                        $("#Table1 tbody").append(row);
                        i = Number($("#NoOfBoxes").val()) + 1;
                        $('#txtBarCode').focus();
                    }


                }

            }
            if (checkinvalid) {
                toastr.error('Invalid Barcode', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                $("#btnPick1").hide();
            }
            else {
                $.ajax({
                    type: "Post",
                    url: "/Driver/TodaysOrders.aspx/unloadbarcodelog",
                    data: "{'OrderAutoId':'" + $("#txtHOrderAutoId").val() + "','Barcode':'" + ReadBorCode + '/' + value + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $.blockUI({
                            message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                            overlayCSS: {
                                backgroundColor: '#FFF',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (response) {
                        if (response.d = "true") {
                            return true
                        }
                    },
                    error: function (result) {
                        console.log(JSON.parse(result.responseText).d);
                    },
                    failure: function (result) {
                        console.log(JSON.parse(result.responseText).d);
                    }
                });
            }
        }
        else {
            $("#txtReadBorCode").attr('disabled', true);
            toastr.success('All boxes picked.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            $("#btnPick1").show();
        }
    } else {
        toastr.error('Barcode already scanned.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $("#btnPick1").hide();
    }
    $("#txtBarCode").val('');
    $("#txtBarCode").focus();
}

function saveDelStatus() {
  
    var PhysicalPath = "", Path = "", FileType = "", FileName = "", FilePath = "",
        PhysicalPath1 = "", Path1 = "", FileType1 = "", FileName1 = "", FilePath1 = "",
        PhysicalPath2 = "", Path2 = "", FileType2 = "", FileName2 = "", FilePath2 = "";
    if ($("#txtRemarks").val() == '') {
        toastr.error('Delivery remark is required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $("#txtRemarks").addClass('border-warning');
        return;
    }
    var imgname = $("#Imageupload").val();
    var imgname1 = $("#fudDeliveryProof").val();
    var imgname2 = $("#fudOther").val();
    if (imgname != '') {
        var min = 100;
        var max = 40;
        var my_random_value = Math.floor(Math.random() * (max - min + 1) + min);
        var file = imgname.substr(imgname.lastIndexOf('\\') + 1);
        var str = file.split(".");
        FileName = str[str.length - 2];
        FileType = str[str.length - 1];
        Path = "Sign_Invoice_" + $("#OrderNo").val() + '_' + CurrentDate + '_' + my_random_value;
        PhysicalPath = "/DriverUploaded/" + Path + '.' + FileType;
        FilePath = "/DriverUploaded/" + Path + '.' + FileType;
    }
    if (imgname1 != '') {
        var min1 = 100;
        var max1 = 40;
        var my_random_value1 = Math.floor(Math.random() * (max1 - min1 + 1) + min1);
        var file1 = imgname1.substr(imgname1.lastIndexOf('\\') + 1);
        var str1 = file1.split(".");
        FileName1 = str1[str1.length - 2];
        FileType1 = str1[str1.length - 1];
        Path1 = "Delivery_Proof_" + $("#OrderNo").val() + '_' + CurrentDate + '_' + my_random_value1;
        PhysicalPath1 = "/DriverUploaded/" + Path1 + '.' + FileType1;
        FilePath1 = "/DriverUploaded/" + Path1 + '.' + FileType1;
    }
    if (imgname2 != '') {
        var min2 = 100;
        var max2 = 40;
        var my_random_value2 = Math.floor(Math.random() * (max2 - min2 + 1) + min2);
        var file2 = imgname2.substr(imgname2.lastIndexOf('\\') + 1);
        var str2 = file2.split(".");
        FileName2 = str2[str2.length - 2];
        FileType2 = str2[str2.length - 1];
        Path2 = "Other_" + $("#OrderNo").val() + '_' + CurrentDate + '_' + my_random_value2;
        PhysicalPath2 = "/DriverUploaded/" + Path2 + '.' + FileType2;
        FilePath2 = "/DriverUploaded/" + Path2 + '.' + FileType2;
    }
    Xml = "";
    if (imgname != '') {
        Xml += '<Xml>';
        Xml += '<FileName><![CDATA[' + FileName + ']]></FileName>';
        Xml += '<FileType><![CDATA[Signed Invoice]]></FileType>';
        Xml += '<FilePath><![CDATA[' + FilePath + ']]></FilePath>';
        Xml += '<PhysicalPath><![CDATA[' + PhysicalPath + ']]></PhysicalPath>';
        Xml += '</Xml>';
    }
    if (imgname1 != '') {
        Xml += '<Xml>';
        Xml += '<FileName><![CDATA[' + FileName1 + ']]></FileName>';
        Xml += '<FileType><![CDATA[Delivery Proof]]></FileType>';
        Xml += '<FilePath><![CDATA[' + FilePath1 + ']]></FilePath>';
        Xml += '<PhysicalPath><![CDATA[' + PhysicalPath1 + ']]></PhysicalPath>';
        Xml += '</Xml>';
    }
    if (imgname2 != '') {
        Xml += '<Xml>';
        Xml += '<FileName><![CDATA[' + FileName2 + ']]></FileName>';
        Xml += '<FileType><![CDATA[Other]]></FileType>';
        Xml += '<FilePath><![CDATA[' + FilePath2 + ']]></FilePath>';
        Xml += '<PhysicalPath><![CDATA[' + PhysicalPath2 + ']]></PhysicalPath>';
        Xml += '</Xml>';
    }
    if ($("input[name='rblDeliver']").is(":checked")) {
        uploadImage(Path);
        if (Path1 != '') {
            uploadImage1(Path1);
        }
        if (Path2 != '') {
            uploadImage2(Path2);
        }
        var DelStatus = {
            OrderAutoId: $("#txtHOrderAutoId").val(),
            Delivered: $("input[name='rblDeliver']:checked").val(),
            Remarks: $("#txtRemarks").val()
        };
        $.ajax({
            type: "POST",
            url: "/Driver/TodaysOrders.aspx/saveDelStatus",
            data: JSON.stringify({ DelStatus: JSON.stringify(DelStatus), xml: JSON.stringify(Xml)  }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {

                if (response.d == 'true') {
                    if ($("input[name='rblDeliver']:checked").val() == "yes") {
                        swal("", "Order delivered successfully.", "success");
                        if (PhysicalPath != "") {
                            $("#hrfviewfile").attr("href", PhysicalPath);
                            $("#hrfviewfile").show();                            
                        }
                        if (PhysicalPath1 != "") {
                            $("#hrfDeliveryProof").attr("href", PhysicalPath1);
                            $("#hrfDeliveryProof").show();

                        }
                        if (PhysicalPath2 != "") {
                            $("#hrfOther").attr("href", PhysicalPath2);
                            $("#hrfOther").show();
                        }
                        $("#Imageupload").val('');                       
                        $("#fudDeliveryProof").val('');                       
                        $("#fudOther").val('');
                    }
                    else {
                        swal("", "Order has been undelivered.", "success");
                        if (PhysicalPath != "") {
                            $("#hrfviewfile").attr("href", PhysicalPath);
                            $("#hrfviewfile").show();
                        }
                        if (PhysicalPath1 != "") {
                            $("#hrfDeliveryProof").attr("href", PhysicalPath1);
                            $("#hrfDeliveryProof").show();

                        }
                        if (PhysicalPath2 != "") {
                            $("#hrfOther").attr("href", PhysicalPath2);
                            $("#hrfOther").show();
                        }
                        $("#Imageupload").val('');
                        $("#fudDeliveryProof").val('');
                        $("#fudOther").val('');
                    }

                    $("#txtRemarks").removeClass('border-warning');
                    $("#btnSaveDelStatus").hide();
                    $("#btnUpdate").show();
                    $("#ReceivePayment").show();
                    getTodayOrders(1);
                } else {
                    swal("Error!", response.d, "error");
                }

            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    } else {
        swal("", "Delivery Option cannot be left blank.", "error");
    }
}

function closePacked1() {
    if (confirm('Are you sure you want to close')) {
        $('#PopDriverDropOrder').modal('hide');
    }
}

function back() {
    var len = $("#Table1 tbody tr").length;
    $("#optexport").show();
    if (len == 0) {
        $("#panelOrderList").show();
        $("#PopDriverDropOrder").hide();
    } else {
        $("#panelOrderList").show();
        $("#PopDriverDropOrder").hide();
    }
}

function loadBackOrder() {
    swal({
        title: "Are you sure?",
        text: "You want to go back.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No,Cancel.",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes,Back it.",
                value: true,
                visible: true,
                className: "",
                closeModal: true
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            back();

        }
    });
};
function uploadImage(Path) {
    //var Path = ""; 
    //Path = "Sign_Invoice_" + $("#lblOrderno").text() + Date;
    var fileUpload = $("#Imageupload").get(0);
    var files = fileUpload.files;

    var test = new FormData();
    for (var i = 0; i < files.length; i++) {
        test.append(files[i].name, files[i]);
    }
    $.ajax({
        url: "/DriverUploaded.ashx?timestamp=" + Path,
        type: "POST",
        async: false,
        contentType: false,
        processData: false,
        data: test,
        success: function (result) {
            return;
        }
    })
}
function uploadImage1(Path) {
    //var Path = ""; 
    //Path = "Sign_Invoice_" + $("#lblOrderno").text() + Date;
    var fileUpload = $("#fudDeliveryProof").get(0);
    var files = fileUpload.files;

    var test = new FormData();
    for (var i = 0; i < files.length; i++) {
        test.append(files[i].name, files[i]);
    }
    $.ajax({
        url: "/DriverUploaded.ashx?timestamp=" + Path,
        type: "POST",
        async: false,
        contentType: false,
        processData: false,
        data: test,
        success: function (result) {
            return;
        }
    })
}
function uploadImage2(Path) {
    //var Path = ""; 
    //Path = "Sign_Invoice_" + $("#lblOrderno").text() + Date;
    var fileUpload = $("#fudOther").get(0);
    var files = fileUpload.files;

    var test = new FormData();
    for (var i = 0; i < files.length; i++) {
        test.append(files[i].name, files[i]);
    }
    $.ajax({
        url: "/DriverUploaded.ashx?timestamp=" + Path,
        type: "POST",
        async: false,
        contentType: false,
        processData: false,
        data: test,
        success: function (result) {
            return;
        }
    })
}
function readURL(input, n) {
     
    file = input.files[0];
    if (Number(file.size / (1024 * 1024)) > 1) {
        toastr.error('You are reaching maximum limit. Max limit is ' + 1 + ' MB', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        if (n == 1) {
            $("#hrfviewfile").val('');
            $("#Imageupload").val('');
        }
        else if (n == 2) {
            $("#hrfDeliveryProof").val('');
            $("#fudDeliveryProof").val('');
        }
        else {
            $("#hrfOther").val('');
            $("#fudOther").val('');
        }
        return;
    }
    else {
        if (input.files && input.files[0] && input.files[0].name.match(/\.(jpg|JPG|JPEG|jpeg|png|PNG)$/)) {
            var reader = new FileReader();
            reader.onload = function (e) {
                if (n == 1) {
                    document.getElementById('Imageupload').src = e.target.result;
                    $('#Imageupload').attr('mainsrc', e.target.result);
                }
                else if (n == 2) {
                    document.getElementById('fudDeliveryProof').src = e.target.result;
                    $('#fudDeliveryProof').attr('mainsrc', e.target.result);
                }
                else {
                    document.getElementById('fudOther').src = e.target.result;
                    $('#fudOther').attr('mainsrc', e.target.result);
                }
            }
            reader.readAsDataURL(input.files[0]);
        }
        else {
            toastr.error('Invalid file. Allowed file type are png,jpg,jpeg.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            if (n == 1) {
                $("#Imageupload").val('');
            }
            else if (n == 2) {
                $("#fudDeliveryProof").val('');
            }
            else {
                $("#fudOther").val('');
            }
        }
    }
}
function AvoidSpace(event) {
    var k = event ? event.which : window.event.keyCode;
    if (k == 32) return false;
}