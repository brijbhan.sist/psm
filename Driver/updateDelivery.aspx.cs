﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Driver_updateDelivery : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            hiddenEmpTypeVal.Value = Session["EmpTypeNo"].ToString();

            string[] GtUrl = Request.Url.Host.ToString().Split('.');
            HDDomain.Value = GtUrl[0].ToLower();
        }
        catch
        {
            Session.Abandon();
            Response.Redirect("~/Default.aspx", false);
        }
    }
}