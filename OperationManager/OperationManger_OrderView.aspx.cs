﻿using DLL_OM_OrderList;
using DllUtility;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web.Script.Serialization;
using System.Web.Services;
using Newtonsoft.Json.Linq;

public partial class OperationManager_OperationManger_OrderView : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod(EnableSession = true)]
    public static string getOrderData(string OrderAutoId)
    {
        PL_OM_OrderList pobj = new PL_OM_OrderList();
        try
        {
            pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
            BL_OM_OrderList.getOrderData(pobj);
            if (!pobj.isException)
            {
              
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

}