﻿var rowupdate = "";
var CustomerId; var MLQtyRate = 0.00;
$(document).ready(function () {
    var getid = getQueryString('OrderAutoId');
    if (getid != null) {
        getOrderData(getid);
        $("#txtHOrderAutoId").val(getid);
    }
});
var getQueryString = function (field, url) {
    var href = url ? url : window.location.href;
    var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
    var string = reg.exec(href);
    return string ? string[1] : null;
};
function getOrderData(OrderAutoId) {
    $.ajax({
        type: "POST",
        async: false,
        url: "OperationManger_OrderView.aspx/getOrderData",
        data: "{'OrderAutoId':'" + OrderAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var order = $(xmldoc).find("Table");
            var product = $(xmldoc).find("Table1");
            var duePayment = $(xmldoc).find("Table2");
            var CreditMemoDetails = $(xmldoc).find("Table3");
            var RemarksDetails = $(xmldoc).find("Table4");
            var pkg = $(xmldoc).find("Table5");
            var CreditDetails = $(xmldoc).find("Table6");
            if (Number(CreditDetails.length) > 0 && $(order).find("StatusCode").text() <= 4) {
                $('#MODALpoPFORCREDIT').modal('show');
                $("#tblCreditMemo tbody tr").remove();
                var rowc = $("#tblCreditMemo thead tr:last").clone(true);
                $.each(CreditDetails, function (index) {
                    $(".SRNO", rowc).text(Number(index) + 1);
                    $(".Action", rowc).html("<input type='radio' name='credit' value=" + $(this).find("CreditAutoId").text() + "> ");
                    $(".CreditNo", rowc).html("<a target='_blank' href='/Manager/ManagerCreditMemo.aspx?PageId=" + $(this).find("CreditAutoId").text() + "'>" + $(this).find("CreditNo").text() + "</a>");
                    $(".CreditDate", rowc).text($(this).find("CreditDate").text());
                    $(".NoofItem", rowc).text($(this).find("NoofItem").text());
                    $(".NetAmount", rowc).text($(this).find("NetAmount").text());
                    $("#tblCreditMemo tbody").append(rowc);
                    rowc = $("#tblCreditMemo tbody tr:last").clone(true);
                });
            }
            $("#tblPacked tbody tr").remove();
            var rowP = $("#tblPacked thead tr:last").clone(true);
            if (pkg.length > 0) {
                $("#tblPackedDetails").show();
                $.each(pkg, function (index) {
                    $(".SRNO", rowP).text(Number(index) + 1);
                    $(".PackedId", rowP).text($(this).find("PackingId").text());
                    $(".PackedDate", rowP).text($(this).find("PkgDate").text());
                    $(".PackedBy", rowP).text($(this).find("Packer").text());
                    $('#tblPacked').find("tbody").append(rowP);
                    rowP = $("#tblPacked tbody tr:last").clone(true);
                });
            } else {
                $("#tblPackedDetails").hide();
            }

            $("#Table2 tbody tr").remove();
            if ($(RemarksDetails).length > 0) {
                $("#OrderRemarksDetails").show();
                var rowtest = $("#Table2 thead tr").clone();
                $.each(RemarksDetails, function (index) {
                    $(".SRNO", rowtest).html((Number(index) + 1));
                    $(".EmployeeName", rowtest).html($(this).find("EmpName").text());
                    $(".EmployeeType", rowtest).html($(this).find("EmpType").text());
                    $(".Remarks", rowtest).html($(this).find("Remarks").text());
                    $("#Table2 tbody").append(rowtest);
                    rowtest = $("#Table2 tbody tr:last").clone(true);
                });
            }
            var totalCredit = 0.00;
            if (Number(CreditMemoDetails.length) > 0) {
                $("#CusCreditMemo").show();
                $("#tblCreditMemoListDetails").show();
                $("#tblCreditMemoList tbody tr").remove();
                var rowc = $("#tblCreditMemoList thead tr:last").clone(true);
                $.each(CreditMemoDetails, function (index) {

                    $(".SRNO", rowc).text(Number(index) + 1);
                    $(".Action", rowc).html("<input type='radio' name='credit' value=" + $(this).find("CreditAutoId").text() + "> ");
                    $(".CreditNo", rowc).text($(this).find("CreditNo").text());
                    $(".CreditDate", rowc).text($(this).find("CreditDate").text());
                    $(".ReturnValue", rowc).text($(this).find("ReturnValue").text());
                    totalCredit += parseFloat($(this).find("ReturnValue").text())
                    $("#tblCreditMemoList tbody").append(rowc)
                    rowc = $("#tblCreditMemoList tbody tr:last").clone(true);
                });
            } else {
                $("#CusCreditMemo").hide();
            }
            $("#TotalDue").text(totalCredit.toFixed(2));

            CustomerId = $(order).find("CustomerId").text();
            $('#btneditOrder').hide();
            $('#btnRemoveMlTax').hide();
            $('#btnGenBar').hide();
            $('#btnAsgnDrv').hide();
            $('#btnGenOrderCC').hide();
            $('#btnSetAsProcess').hide();
            $('#btnCancelOrder').hide();
            if (Number($(order).find("StatusCode").text()) == 1 || Number($(order).find("StatusCode").text()) == 2) {
                $('#btnCancelOrder').show();
            }
            else if (Number($(order).find("StatusCode").text()) == 3 || Number($(order).find("StatusCode").text()) == 9 || Number($(order).find("StatusCode").text()) == 10 || Number($(order).find("StatusCode").text()) == 4 || Number($(order).find("StatusCode").text()) == 7) {
                $('#btnCancelOrder').show();
                $('#btnGenBar').show();
                $('#btnAsgnDrv').show();
                if (Number($(order).find("StatusCode").text()) != 7) {
                    $('#btnSetAsProcess').show();
                    $('#btneditOrder').show();
                    $('#btnRemoveMlTax').show();
                    $('#btnGenOrderCC').show();
                }
            }
            else if (Number($(order).find("StatusCode").text()) == 5 || Number($(order).find("StatusCode").text()) == 6 ||
                Number($(order).find("StatusCode").text()) == 11) {
                $("#btnGenBar").show();
                $("#btnGenOrderCC").show();
            }
            if ($(order).find("Driver").text() != null && $(order).find("Driver").text() != "") {
                $("#btnAsgnDrv button").html("<b>Change Driver</b>");

                if ($(order).find("StatusCode").text() == 7) {
                    $("#btnAsgnDrv button").html("<b>Reassign Driver</b>");
                }
                $("#divPkg").append("<span>Driver : <b>" + $(order).find("DrvName").text() + "</b><br />Assigned on : <b>" + $(order).find("AssignDate").text() + "</b></span>");
                $("#divPkg").show();
            }
            if (Number($(order).find("StatusCode").text()) == 6 || Number($(order).find("StatusCode").text()) == 11) {
                $("#txtDelivered").val("Yes");
            } else {
                $("#txtDelivered").val('No');
            }


            if (Number($(order).find("StatusCode").text()) == 8) {
                $("#btnRemoveMlTax").hide();
            } else {
                $("#btnRemoveMlTax").show();
            }

            $("#txtRemarks").val($(order).find("DrvRemarks").text());
            if ($(order).find("PaymentRecev").text() == null || $(order).find("PaymentRecev").text() == "") {
                $("#AccountDeliveryInfo").hide();
            } else {
                $("#AccountDeliveryInfo").show();
            }
            $('#txtRecievedPayment').val($(order).find("PaymentRecev").text());
            $("#txtAmtPaid").text($(order).find("AmtPaid").text());
            $("#txtAmtDue").val($(order).find("AmtDue").text());
            $("#txtAcctRemarks").val($(order).find("AcctRemarks").text());
            $("#txtTaxType").val($(order).find("TaxType").text());
            $("#txtHOrderAutoId").val($(order).find("AutoId").text());
            $("#txtOrderId").val($(order).find("OrderNo").text());
            $("#lblOrderno").text($(order).find("OrderNo").text());
            $("#txtOrderType").val($(order).find("OrderType").text());
            if (Number($(order).find("StatusCode").text()) > 5) {
                $('#DrvDeliveryInfo').show();
            }
            $("#txtOrderDate").val($(order).find("OrderDate").text());
            $('#txtAssignDate').pickadate({
                format: 'mm/dd/yyyy',
                formatSubmit: 'mm/dd/yyyy',
                selectYears: true,
                selectMonths: true,
                min: -7,
                firstDay: 0
            });
            if ($(order).find("CommentType").text() != '')
                $("#ddlCommentType").val($(order).find("CommentType").text());
            $("#txtComment").val($(order).find("Comment").text());
            $("#txtDeliveryDate").val($(order).find("DeliveryDate").text());
            $("#txtOrderStatus").val($(order).find("Status").text());
            $("#txtCustomer").val($(order).find("CustomerName").text());
            $("#hiddenCustAutoId").val($(order).find("CustAutoId").text());
            $("#txtTerms").val($(order).find("Terms").text());
            $("#txtTerms").val($(order).find("TermsDesc").text());
            $("#OrderRemarks").val($(order).find("OrderRemarks").text());
            $("#txtAdjustment").val($(order).find("AdjustmentAmt").text());
            $("#txtMLQty").val($(order).find("MLQty").text());
            $("#txtWeightQty").val($(order).find("WeightOzQty").text());

            $("#txtMLTax").val($(order).find("MLTax").text());
            $("#txtWeightTax").val($(order).find("WeightOzTotalTax").text());

            if ($(order).find("CreditAmount").text() != "") {
                $("#txtStoreCreditAmount").val($(order).find("CreditAmount").text());
            }
            if ($(order).find('PayableAmount').text() != '') {
                $("#txtPaybleAmount").val($(order).find("PayableAmount").text());
            } else {
                $("#txtPaybleAmount").val($(order).find("GrandTotal").text());
            }
            if ($(order).find("DeductionAmount").text() != "")
                $("#txtDeductionAmount").val($(order).find("DeductionAmount").text());
            $("#CreditMemoAmount").text($(order).find("CustomerCredit").text());
            $("#txtBillAddress").val($(order).find("BillAddr").text());
            $("#txtBillState").val($(order).find("State1").text());
            $("#txtBillCity").val($(order).find("City1").text());
            $("#txtBillZip").val($(order).find("Zipcode1").text());

            if ($(order).find('isMLManualyApply').text() == '1') {
                $('#btnRemoveMlTax button').html('Remove ML Tax');
            } else {
                $('#btnRemoveMlTax button').html('Add ML Tax')
            }

            $("#hfMLTaxStatus").val($(order).find('isMLManualyApply').text());
            //if ($(order).find('MLTaxStatus').text() == '1') {
            //    $('#btnRemoveMlTax').show();
            //}
            //else {
            //    $('#btnRemoveMlTax').hide();
            //}

            $("#txtShipAddress").val($(order).find("ShipAddr").text());
            $("#txtShipState").val($(order).find("State2").text());
            $("#txtShipCity").val($(order).find("City2").text());
            $("#txtShipZip").val($(order).find("Zipcode2").text());

            $("#txtTotalAmount").val($(order).find("TotalAmount").text());
            $("#txtOverallDisc").val($(order).find("OverallDisc").text());
            $("#txtDiscAmt").val($(order).find("OverallDiscAmt").text());
            $("#txtShipping").val($(order).find("ShippingCharges").text());
            $("#txtTotalTax").val($(order).find("TotalTax").text());
            $("#txtGrandTotal").val($(order).find("GrandTotal").text());
            $("#hiddenGrandTotal").val($(order).find("GrandTotal").text());
            $("#txtPackedBoxes").val($(order).find("PackedBoxes").text());
            $("#lblNoOfbox").text($(order).find("PackedBoxes").text());
            showDuePayments(duePayment);
            $("#tblProductDetail tbody tr").remove();
            var row = $("#tblProductDetail thead tr").clone(true);
            $.each(product, function () {
               
                $(".ProId", row).html($(this).find("ProductId").text());
                if (Number($(this).find("isFreeItem").text()) == 1) {
                    $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span><product class='badge badge badge-pill badge-success'>Free</product>");

                } else if (Number($(this).find("IsExchange").text()) == 1) {
                    $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span><product class='badge badge badge-pill badge-primary'>Exchange</product>");
                }
                else if (Number($(this).find("Tax").text()) == 1) {
                    $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span><product class='badge badge badge-pill badge-danger'>Taxable</product>");
                } else {
                    $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span>");
                }
                $(".UnitType", row).html($(this).find("UnitType").text() + " (" + $(this).find("QtyPerUnit").text() + " pcs" + ")");
                $(".TtlPcs", row).text($(this).find("TotalPieces").text());
                $(".CostPrice", row).text($(this).find("CostPrice").text());
                $(".NetPrice", row).text($(this).find("NetPrice").text());
                var unitPrice = $(this).find("UnitPrice").text();
                var PriceHtml = "<b>Min Price - " + $(this).find("MinPrice").text() + "</b>";
                PriceHtml += "</br><b>Base Price - " + $(this).find("BasePrice").text() + "</b>";


                if (($(order).find("StatusCode").text() == 3 || $(order).find("StatusCode").text() == 4 || $(order).find("StatusCode").text() == 5) && $(this).find("IsExchange").text() == 0 && $(this).find("isFreeItem").text() == 0 && ($(this).find("UnitPrice").text() < $(this).find("MinPrice").text())) {
                    $(".UnitPrice", row).html($(this).find("UnitPrice").text() + "<span class='la la-question-circle' data-toggle='tooltip' title='" + PriceHtml + "'></span>");
                    $(".UnitPrice", row).addClass("redCell");
                } else {
                    $(".UnitPrice", row).html($(this).find("UnitPrice").text());
                    $(".UnitPrice", row).addClass("removeredCell");
                }

                $(".RequiredQty", row).text($(this).find("RequiredQty").text());
                $(".SRP", row).text($(this).find("SRP").text());
                $(".GP", row).text($(this).find("GP").text());
                if (Number($(this).find("Tax").text()) == 1) {
                    $(".TaxRate", row).html('<span class="la la-check-circle success"></span>');
                } else {
                    $(".TaxRate", row).html('');
                }
                if (Number($(this).find("IsExchange").text()) == 1) {
                    $(".IsExchange", row).html('<span class="la la-check-circle success"></span>');
                } else {
                    $(".IsExchange", row).html('');
                }
                $(".Barcode", row).text($(this).find("Barcode").text());

                $(".QtyShip", row).text($(this).find("QtyShip").text());

                if (Number($(this).find("QtyShip").text()) === 0) {

                    $(row).css('background-color', '#efcccc');
                }
                else if (Number($(this).find("RequiredQty").text()) != Number($(this).find("QtyShip").text())) {
                    $(row).css('background-color', '#87dabc');  //11/01/2019 21:41 PM
                }
                else {
                    $(row).css('background-color', '#fff');
                }
                $('#tblProductDetail tbody').append(row);
                row = $("#tblProductDetail tbody tr:last").clone(true);
            });
            if (Number($(order).find("StatusCode").text()) < 3) {
                $('#PackingSlip').closest('.row').hide();
                $('#WithoutCategorybreakdown').closest('.row').hide();
                $("#btnRemoveMlTax").hide();
            } else {
                $('#PackingSlip').closest('.row').show();
                $('#WithoutCategorybreakdown').closest('.row').show();
                $("#btnRemoveMlTax").show();
            }
            if (Number($(order).find("StatusCode").text()) == 8) {
                $("#btnRemoveMlTax").hide();
            }
            //uuu
            if (Number($(order).find("StatusCode").text()) == 9) {
                $("#btnAsgnDrv").hide();
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip({
        html: true
    });
});

function showDuePayments(duePayment) {
    $("#tblCustDues tbody tr").remove();
    var row = $("#tblCustDues thead tr").clone(true);

    var sumOrderValue = 0,
        sumPaid = 0,
        sumDue = 0;

    if (duePayment.length > 0) {
        $("#noDues").hide();
        $("#CustomerPaymentsHistory").show();
        $.each(duePayment, function () {
            $(".orderNo", row).html("<span orderautoid='" + $(this).find("AutoId").text() + "'><a href='#'>" + $(this).find("OrderNo").text() + "</a></span>");
            $(".orderDate", row).text($(this).find("OrderDate").text());
            $(".value", row).text($(this).find("GrandTotal").text());
            $(".OrderType", row).text($(this).find("OrderType").text());
            $(".amtPaid", row).text($(this).find("AmtPaid").text()).css("color", "#8bc548");
            $(".amtDue", row).text($(this).find("AmtDue").text()).css("color", "red");
            $(".pay", row).html("<input type='text' class='form-control input-sm' style='width:100px;float:right;text-align:right;' value='0.00' placeholder='0.00' onkeyup='sumPay()' />");
            $(".remarks", row).html("<input type='text' class='form-control input-sm' placeholder='Enter payment details here' />");

            sumOrderValue += Number($(this).find("GrandTotal").text());
            sumPaid += Number($(this).find("AmtPaid").text());
            sumDue += Number($(this).find("AmtDue").text());

            $('#tblCustDues').find("tbody").append(row).end()
                .find("tfoot").show().find("#sumOrderValue").text(sumOrderValue.toFixed(2)).end()
                .find("#sumPaid").text(sumPaid.toFixed(2)).css("color", "#8bc548").end()
                .find("#sumDue").text(sumDue.toFixed(2)).css("color", "red").end()
                .find("#sumPay").text("0.00").end();

            row = $("#tblCustDues tbody tr:last").clone(true);
        });
        $("#CustDues").show();
        if ($("#hiddenEmpTypeVal").val() != "5") {
            $("#CustDues").find("#btnPay_Dues").show().end()
                .find("#tblCustDues").find(".pay").show().end()
                .find(".remarks").show().end()
                .find("tfoot > tr > td").show();
        }
    } else {
        $("#tblCustDues").find("tbody tr").remove().end().find("tfoot").hide();
        $("#noDues").show();
        $("#CustDues").show();
        $("#btnPay_Dues, #btnClose_Dues").hide();
        $("CustomerPaymentsHistory").hide();
    }
}
