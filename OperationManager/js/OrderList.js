﻿
$(document).ready(function () {
    bindStatus();
  
    $('#txtSFromDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: false,
        selectMonths: false,
    });
    $('#txtSToDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: false,
        selectMonths: false,
    });
    $('#txtAssignDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: false,
        selectMonths: false,
    });
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    $("#txtSFromDate").val(month + '/' + day + '/' + year);
    $("#txtSToDate").val(month + '/' + day + '/' + year);
    getOrderList(1);
})
function Pagevalue(e) {
    getOrderList(parseInt($(e).attr("page")));
};
function bindStatus() { 
    $.ajax({
        type: "POST",
        url: "OM_OrderList.aspx/bindStatus",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },

        async: false,
        success: function (response) {
          


            if (response.d != "Session Expired") {
                var DropDown = $.parseJSON(response.d);
                for (var i = 0; i < DropDown.length; i++) {
                    var AllDropDownList = DropDown[i];
                    var Status = AllDropDownList.Status;
                    var ddlSStatus = $("#ddlSStatus");
                    $("#ddlSStatus option:not(:first)").remove();
                    for (var j = 0; j < Status.length; j++) {
                        var StatusDropList = Status[j];
                        var option = $("<option />");
                        option.html(StatusDropList.StatusType);
                        option.val(StatusDropList.AutoId);
                        ddlSStatus.append(option);
                    } 
                    var ddlCustomer = $("#ddlCustomer");
                    $("#ddlCustomer option:not(:first)").remove();
                    var Customer = AllDropDownList.Customer;
                    for (var k = 0; k < Customer.length; k++) {
                        var CustomerDropList = Customer[k];
                        var option = $("<option />");
                        option.html(CustomerDropList.Customer);
                        option.val(CustomerDropList.AutoId);
                        ddlCustomer.append(option);
                    }
                    ddlCustomer.select2();
                    var ddlSalesPerson = $("#ddlSalesPerson");
                    $("#ddlSalesPerson option:not(:first)").remove();
                    var SalePerson = AllDropDownList.SalePerson;
                    for (var l = 0; l < SalePerson.length; l++) {
                        var SalePersonDropList = SalePerson[l];
                        var option = $("<option />");
                        option.html(SalePersonDropList.EmpName);
                        option.val(SalePersonDropList.AutoId);
                        ddlSalesPerson.append(option);
                    }
                    ddlSalesPerson.select2();

                }

            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
$("#ddlPageSize").change(function () {
    getOrderList(1);
});


function getOrderList(pageIndex) {

    var data = {
        OrderNo: $("#txtSOrderNo").val().trim(),
        CustomerAutoid: $("#ddlCustomer").val(),
        SalesPersonAutoId: $("#ddlSalesPerson").val(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        Status: $("#ddlSStatus").val(),
        pageIndex: pageIndex,
        PageSize: $("#ddlPageSize").val(),

    };
    $.ajax({
        type: "POST",
        url: "OM_OrderList.aspx/getOrderList",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
            var DropDown = $.parseJSON(response.d);
            var row = $("#tblOrderList thead tr").clone(true);
            for (var i = 0; i < DropDown.length; i++) {
                $("#tblOrderList tbody tr").remove();

                var AllDropDownList = DropDown[i];
                var OrderList = AllDropDownList.OrderList;
                if (OrderList.length > 0) {
                    $("#EmptyTable").hide();

                    for (var j = 0; j < OrderList.length; j++) {
                        var Order = OrderList[j];
                        if (Order.ShipId == '2' || Order.ShipId == '7' || Order.ShipId == '4') {
                            $(row).css('background-color', '#ffe6e6');
                        }
                        else if (Order.ShipId == '8') {
                            $(row).css('background-color', '#eb99ff');
                        }
                        else if (Order.ShipId == '9') {
                            $(row).css('background-color', '#e6fff2');
                        }
                        else if (Order.ShipId == '10') {
                            $(row).css('background-color', '#ccff99');
                        }
                        else {
                            $(row).css('background-color', 'white');
                        }

                        $(".Shipping", row).text(Order.ShippingType);
                        $(".orderNo", row).html("<span OrderAutoId='" + Order.AutoId + "'>" + Order.OrderNo + "</span>");
                        $(".orderDt", row).text(Order.OrderDate);
                        $(".cust", row).text(Order.CustomerName);
                        $(".value", row).text(Order.GrandTotal);
                        $(".product", row).text(Order.NoOfItems);
                        $(".payableAmt", row).text(parseFloat(Order.PayableAmount).toFixed(2));
                        $(".SalesPerson", row).text(Order.SalesPerson);
                        $(".CreditMemo", row).text(Order.CreditMemo);
                        $(".OrderAutoId", row).text(Order.AutoId);
                        $(".Stoppage", row).text(Order.StopNo);
                        $(".CostPrice", row).text(Order.CostPrice);
                        var logView = '';

                        if (Number(Order.StatusCode) == 1) {
                            $(".status", row).html("<span class='badge mr-2 badge badge-pill Status_New'>" + Order.Status + "</span>");
                        } else if (Number(Order.StatusCode) == 2) {
                            $(".status", row).html("<span class='badge mr-2 badge badge-pill Status_Processed'>" + Order.Status + "</span>");
                        } else if (Number(Order.StatusCode) == 3) {
                            logView = '&nbsp;&nbsp;<a href="javascript:void(0)" onclick="BindDriver(this)" class="la la-truck" title="Asign to driver"></a>';
                            $(".status", row).html("<span class='badge badge-pill Status_Packed' style='background-color:#66ff99'>" + Order.Status + "</span>");
                        } else if (Number(Order.StatusCode) == 4) {
                            logView = '&nbsp;&nbsp;<a href="javascript:void(0)" onclick="BindDriver(this)" class="la la-truck" title="Change driver"></a>';
                            $(".status", row).html("<span class='badge mr-2 badge badge-pill Status_Ready_to_Ship' style='background-color:#AB47BC'>" + Order.Status + "</span>");
                        } else if (Number(Order.StatusCode) == 5) {
                            $(".status", row).html("<span class='badge mr-2 badge badge-pill Status_Shipped'>" + Order.Status + "</span>");
                        } else if (Number(Order.StatusCode) == 6) {
                            $(".status", row).html("<span class='badge mr-2 badge badge-pill Status_Delivered'>" + Order.Status + "</span>");
                        } else if (Number(Order.StatusCode) == 7) {
                            logView = '&nbsp;&nbsp;<a href="javascript:void(0)" onclick="BindDriver(this)" class="la la-truck" title="Asign to driver"></a>';
                            $(".status", row).html("<span class='badge mr-2 badge badge-pill Status_Undelivered'>" + Order.Status + "</span>");
                        } else if (Number(Order.StatusCode) == 8) {
                            $(".status", row).html("<span class='badge mr-2 badge badge-pill Status_cancelled' >" + Order.Status + "</span>");
                        }
                        else if (Number(Order.StatusCode) == 9) {
                            logView = '&nbsp;&nbsp;<a href="javascript:void(0)" onclick="BindDriver(this)" class="la la-truck" title="Asign to driver"></a>';
                            $(".status", row).html("<span class='badge mr-2 badge badge-pill Status_Add_On' style='background-color:green'>" + Order.Status + "</span>");
                        }
                        else if (Number(Order.StatusCode) == 10) {
                            logView = '&nbsp;&nbsp;<a href="javascript:void(0)" onclick="BindDriver(this)" class="la la-truck" title="Asign to driver"></a>';
                            $(".status", row).html("<span class='badge mr-2 badge badge-pill Status_Add_On_Packed' style='background-color:#61b960'>" + Order.Status + "</span>");
                        }
                        else if (Number(Order.StatusCode) == 11) {
                            $(".status", row).html("<span class='badge mr-2 badge badge-pill Status_Close'>" + Order.Status + "</span>");
                        }
                        $(".action", row).html("<a href='/OperationManager/OperationManger_OrderView.aspx?OrderAutoId=" + Order.AutoId + "'><span class='la la-eye' title='View detail'></span></a>");
                        //$(".action", row).html("<a href='/OperationManager/OperationManger_OrderView.aspx?OrderAutoId=" + Order.AutoId + "'><span class='la la-eye' title='View detail'></span></a>&nbsp;<a title='Delete' href='javascript:;'><span class='la la-remove' onclick='deleterecord(\"" + Order.AutoId + "\",\"" + Order.OrderNo + "\")'></span></a>");

                        $("#tblOrderList tbody").append(row);
                        row = $("#tblOrderList tbody tr:last").clone(true);


                    }
                } else {
                    $("#EmptyTable").show();
                    $("#tblOrderList tbody tr").remove();
                }

                var PagingList = AllDropDownList.RecordCount;
                for (var k = 0; k < PagingList.length; k++) {
                    var pager = PagingList[k];
                    $(".Pager").ASPSnippets_Pager({
                    ActiveCssClass: "current",
                    PagerCssClass: "pager",
                    PageIndex: parseInt(pager.PageIndex),
                    PageSize: parseInt(pager.PageSize),
                    RecordCount: parseInt(pager.RecordCount)
                });

                }
                
                }
            } else {
                location.href = "/";
            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

$("#btnSearch").click(function () {
    var Driver = $("#ddlDriver").val();
    var OrderStatus = $("#ddlSStatus").val();
    localStorage.setItem('Driver', Driver);
    localStorage.setItem('Orderstatus', OrderStatus);
    getOrderList(1);
});



function deleterecord(OrderAutoId, OrderNo) {
    $("#txtRemarks").val('');
    $("#txtTicketNo").val('');
    $("#modalDelete_Order").modal('show');
    $("#hdnOrderAutoId").val(OrderAutoId);
    $("#lblOrderNo").text(OrderNo);
}
function DeleteOrder() {
    if (checkRequiredField()) {
        swal({
            title: "Are you sure?",
            text: "You want to delete this Order!",
            icon: "warning",
            showCancelButton: true,
            buttons: {
                cancel: {
                    text: "No, cancel!",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: true,
                },
                confirm: {
                    text: "Yes, delete it!",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
                }
            }
        }).then(function (isConfirm) {
            if (isConfirm) {

                deleteOrderDetails();
            }
        })
    } else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}

function deleteOrderDetails() {
    debugger;
    var data = {
        OrderAutoId: $("#hdnOrderAutoId").val(),
        Remarks: $("#txtRemarks").val(),
        TicketNo: $("#txtTicketNo").val(),
    };
    $.ajax({
        type: "POST",
        url: "OM_OrderList.aspx/deleteOrder",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            debugger;
            if (response.d == 'deleted') {
                swal("", "Order has been deleted successfully.", "success")
                getOrderList(1);
                $("#modalDelete_Order").modal('hide');
                $("#txtRemarks").val('');
                $("#txtTicketNo").val('');
            }
            
            else {
                //swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                swal("Error!", response.d, "error");
            }
           
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}



