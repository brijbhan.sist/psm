﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="OM_OrderList.aspx.cs" Inherits="OperationManager_OM_OrderList" ClientIDMode="static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .anyClass {
            height: 400px;
            overflow-y: scroll;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Order List</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage Order</a></li>
                        <li class="breadcrumb-item">Order List</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">

                        <input id="HDDomain" runat="server" type="hidden" />
                        <input type="hidden" id="txtHOrderAutoId" class="form-control input-sm" />
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlCustomer" runat="server">
                                            <option value="0">All Customer</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 form-group" id="SalesPerson">
                                        <select class="form-control border-primary input-sm" id="ddlSalesPerson" runat="server">
                                            <option value="0">All Sales Person</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Order No" id="txtSOrderNo" onfocus="this.select()" />
                                    </div>

                                    <div class="col-md-4 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 0.5rem;">From Order Date<span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" placeholder="Order From Date" id="txtSFromDate" onfocus="this.select()" />

                                        </div>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text " style="padding: 0rem 0.5rem;">To Order Date<span class="la la-calendar-o"></span>

                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" placeholder="Order To Date" id="txtSToDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlSStatus">
                                            <option value="0">All Status</option>
                                        </select>
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnSearch">Search</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <input type="hidden" id="hiddenPackerAutoId" runat="server" />
                                <input type="hidden" id="hiddenEmpType" runat="server" />
                                <div class="row form-group">
                                    <div class="col-md-12 text-center">
                                        <button type="button" class="btn btn-success  buttonAnimation pull-right round box-shadow-1  btn-sm" style="display: none" data-animation="pulse" id="btnAssignRoot">Assign Route</button>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblOrderList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="action text-center">Action</td>
                                                        <td class="status text-center">Status</td>
                                                        <td class="orderNo text-center">Order No</td>
                                                        <td class="orderDt text-center">Order Date</td>
                                                        <td class="SalesPerson">Sales Person</td>
                                                        <td class="cust">Customer</td>
                                                        <%--<td class="value price">Order Amount</td>--%>
                                                        <td class="payableAmt text-right">Payable Amount</td>
                                                        <td class="product text-center">Products</td>
                                                        <td class="CreditMemo text-center">Credit Memo</td>
                                                        <td class="Shipping text-center">Shipping Type</td>
                                                        <td class="Stoppage text-center">Stop No</td>
                                                        <td class="CostPrice text-right">Cost Price</td>
                                                        <td class="OrderAutoId text-center" style="display: none;">OrderAutoId</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex">
                                        <div class="form-group">
                                            <select id="ddlPageSize" class="form-control border-primary input-sm">
                                                <option selected="selected" value="10">10</option>
                                                <option value="50">50</option>
                                                <option value="100">100</option>
                                                <option value="500">500</option>
                                                <option value="1000">1000</option>
                                                <option value="0">All</option>
                                            </select>
                                        </div>
                                    <div class="ml-auto">
                                        <div class="Pager"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="modal fade" id="modalDelete_Order" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row">
                        <div class="col-md-12">
                            <span class="modal-title"><b>Delete Order</b></span>
                            <input type="hidden" id="hdnOrderAutoId" />
                            <span style="padding-left: 150px"><b>Order No :</b>&nbsp;</span><span id="lblOrderNo"></span>
                        </div>
                        <%--<div class="col-md-5" style="display:none">
                          
                            Order No:&nbsp;<span id="lblOrderSNo">5555</span>
                        </div>--%>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row form-group">
                                        <div class="col-sm-3">
                                            <label class="control-label">
                                                Ticket No.
                                            </label>
                                        </div>
                                        <div class="col-sm-9 form-group">
                                            <div class="input-group">
                                                <input type="text" id="txtTicketNo" class="form-control border-primary input-sm" onfocus="this.select()" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row form-group">
                                        <div class="col-sm-3">Remarks<span class="required">&nbsp;*</span></div>
                                        <div class="col-sm-9 form-group">
                                            <div class="input-group">
                                                <textarea id="txtRemarks" class="form-control border-primary input-sm req" onfocus="this.select()"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--  </div>--%>
                </div>
                <div class="modal-footer">
                    <div class="btn-group mr-1 pull-right">
                        <button type="button" class="btn btn-success  buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" onclick="DeleteOrder()">Delete</button>
                    </div>
                    <div class="btn-group mr-1 pull-right">
                        <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <%--    <script src="../js/summernote.js"></script>
    <link href="../css/summernote.css" rel="stylesheet" />--%>
    <%-- <script>
        $(document).ready(function () {
            $('#txtEmailBody').summernote();

        });
    </script>--%>
    <script type="text/javascript">

        document.write('<scr' + 'ipt type="text/javascript" src="/OperationManager/js/OrderList.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
</asp:Content>

