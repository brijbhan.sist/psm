﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="OperationManger_OrderView.aspx.cs" Inherits="OperationManager_OperationManger_OrderView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
      <style>
        .anyClass {
            height: 450px;
            overflow-y: scroll;
        }
        .redCell{
            background:red !important;
            color:#fff;
        }
        .removeredCell{
            background:#fff !important;
            color:#000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Order Detail</h3>
            <div class="row breadcrumbs-top">            
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage Order</a></li>
                        <li class="breadcrumb-item"><a href="#">Order List</a></li>
                        <li class="breadcrumb-item">Order Detail</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
             
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a class="dropdown-item" href="/OperationManager/OM_OrderList.aspx"  runat="server">Go To Order List</a>
                    <input type="hidden" />
                </div>
              
               
             
            </div>
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
            </div>
        </div>
    </div>


    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Order Information</h4>
                            <input type="hidden" id="hiddenEmpTypeVal" runat="server" />
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                     
                                    <div class="col-md-3 col-sm-4 form-group">
                                        <label class="control-label">Order No</label>
                                        <input type="hidden" id="txtHOrderAutoId" class="form-control input-sm" />

                                        <input type="text" id="txtOrderId" class="form-control border-primary input-sm" readonly="readonly" />
                                    </div>
                                    <div class="col-md-3 col-sm-4 form-group">
                                        <label class="control-label">Order Date</label>

                                        <input type="text" id="txtOrderDate" class="form-control border-primary input-sm" readonly="readonly" />

                                    </div>
                                    <div class="col-md-3 col-sm-4 form-group">
                                        <label class="control-label">Delivery Date</label>
                                        <input type="text" id="txtDeliveryDate" class="form-control border-primary input-sm" readonly="readonly" />
                                    </div>

                                    <div class="col-md-3 col-sm-4 form-group">
                                        <label class="control-label">Order Type</label>
                                        <input type="text" id="txtOrderType" class="form-control border-primary input-sm" readonly="readonly" />
                                    </div>


                                    <div class="col-md-3 col-sm-4 form-group">
                                        <label class="control-label">Order Status</label>
                                        <input type="text" id="txtOrderStatus" class="form-control border-primary input-sm" readonly="readonly" />
                                    </div>
                                    <div class="col-md-6 col-sm-7 form-group">
                                        <label class="control-label">Customer</label>
                                        <input type="text" id="txtCustomer" class="form-control border-primary input-sm" readonly="readonly" />
                                        <input type="hidden" id="hiddenCustAutoId" />
                                    </div>
                                    <div class="col-md-3 col-sm-4 form-group">
                                        <label class="control-label">Terms</label>
                                        <input type="text" class="form-control border-primary input-sm" id="txtTerms" readonly="readonly" />
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Billing Details</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-plus"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 form-group">
                                                <label class="control-label">Billing Address</label>
                                                <input type="text" class="form-control border-primary input-sm" id="txtBillAddress" readonly="readonly" />
                                            </div>
                                            <div class="col-md-6 col-sm-6 form-group">
                                                <label class="control-label">State</label>
                                                <input type="text" class="form-control border-primary input-sm" id="txtBillState" readonly="readonly" />
                                            </div>
                                            <div class="col-md-6 col-sm-6 form-group">
                                                <label class="control-label">City</label>
                                                <input type="text" class="form-control border-primary input-sm" id="txtBillCity" readonly="readonly" />
                                            </div>
                                            <div class="col-md-6 col-sm-6 form-group">
                                                <label class="control-label">Zip Code</label>
                                                <input type="text" class="form-control border-primary input-sm" id="txtBillZip" readonly="readonly" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Shipping Details</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-plus"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 form-group">
                                                <label class="control-label">Shipping Address</label>
                                                <input type="text" class="form-control border-primary input-sm" id="txtShipAddress" readonly="readonly" />
                                            </div>
                                            <div class="col-md-6 col-sm-6 form-group">
                                                <label class="control-label">State</label>
                                                <input type="text" class="form-control border-primary input-sm" id="txtShipState" readonly="readonly" />
                                            </div>
                                            <div class="col-md-6 col-sm-6 form-group">
                                                <label class="control-label">City</label>
                                                <input type="text" class="form-control border-primary input-sm" id="txtShipCity" readonly="readonly" />
                                            </div>
                                            <div class="col-md-6 col-sm-6 form-group">
                                                <label class="control-label">Zip Code</label>
                                                <input type="text" class="form-control border-primary input-sm" id="txtShipZip" readonly="readonly" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>



            <div class="row" id="CustomerPaymentsHistory" style="display: none;">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Payments History</h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="tblCustDues" class="table table-striped table-bordered">
                                        <thead class="bg-blue white">
                                            <tr>
                                                <td class="orderNo text-center">Order No</td>
                                                <td class="orderDate  text-center">Date</td>
                                                <td class="OrderType text-center">Order Type</td>
                                                <td class="value price">Order Amount</td>
                                                <td class="amtPaid price">Paid Amount</td>
                                                <td class="amtDue price">Due Amount</td>
                                                <td class="paya price" style="display: none;">Pay</td>
                                                <td class="remarksa" style="display: none;">Remark</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="3" style="text-align: right;"><b>Total</b></td>
                                                <td id="sumOrderValue" style="text-align: right;"></td>
                                                <td id="sumPaid" style="text-align: right;"></td>
                                                <td id="sumDue" style="text-align: right;"></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    <h5 class="well text-center" id="noDues" style="display: none">No Dues.</h5>
                                </div>
                                <div class="btn-group mr-1 pull-right">
                                    <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnPay_Dues1" style="display: none; padding: 5px 15px;" onclick="payDueAmount()"><b>Pay</b></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Order Content</h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>

                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="tblProductDetail" class="table table-striped table-bordered">
                                        <thead class="bg-blue white">
                                            <tr>
                                              
                                                <td class="ProId text-center">ID</td>
                                                <td class="ProName">Product Name</td>
                                                <td class="UnitType text-center" style="width: 120px;">Unit</td>
                                                <td class="RequiredQty text-center">Ordered<br />
                                                    Qty</td>
                                                <td class="Barcode text-center">Barcode</td>
                                                <td class="QtyShip text-center">
                                                    
                                                    Packed<br/> Qty</td>
                                                <td class="TtlPcs text-center">Total
                                                    <br />
                                                    Pieces</td>
                                                <td class="UnitPrice price">Unit
                                                    <br />
                                                    Price</td>
                                                <td class="SRP price">SRP</td>
                                                <td class="GP price">GP (%)</td>
                                                <td class="TaxRate text-center" style="display: none">Tax</td>
                                                <td class="IsExchange text-center" style="display: none">Exchange</td>
                                                <td class="NetPrice price">Net Price</td>
                                                <td class="CostPrice price">Cost Price</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-7  col-sm-7">
                    <div class="row" id="tblPackedDetails" style="display: none;">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Packing Details</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-plus"></i></a></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="card-content collapse">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table id="tblPacked" class="table table-striped table-bordered">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="SRNO  text-center">SN</td>
                                                        <td class="PackedId  text-center">Packed Id</td>
                                                        <td class="PackedDate  text-center">Date</td>
                                                        <td class="PackedBy">Packed By</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row" id="tblCreditMemoListDetails" style="display: none;">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Credit Memo</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-plus"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table id="tblCreditMemoList" class="table table-striped table-bordered">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="SRNO  text-center">SRNO</td>
                                                        <td class="Action rowspan  text-center" style="display: none">Action</td>
                                                        <td class="CreditNo  text-center">Credit No</td>
                                                        <td class="CreditDate  text-center">Date</td>
                                                        <td class="ReturnValue price">Total Value</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot style="font-weight: 700; background: oldlace;">
                                                    <tr style="text-align: right;">
                                                        <td style="display: none" class="rowspan"><b></b></td>
                                                        <td colspan="3"><b>TOTAL</b></td>
                                                        <td id="TotalDue"></td>

                                                    </tr>
                                                </tfoot>
                                            </table>
                                            <h5 class="well text-center" id="H1" style="display: none">No Dues.</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row" id="OrderRemarksDetails" style="display: none;">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Order Remark Details</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-plus"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse">
                                    <div class="card-body">
                                        <div class="table-responsive" id="Div2">
                                            <table id="Table2" class="table table-striped table-bordered">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="SRNO text-center">SN</td>
                                                        <td class="EmployeeName">Employee Name</td>
                                                        <td class="EmployeeType  text-center">Employee Type</td>
                                                        <td class="Remarks">Remark</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row" id="DrvDeliveryInfo" style="display:none">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Driver Details</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-plus"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label class="control-label">Delivered</label>
                                                <div class="form-group">
                                                    <input type="text" class="form-control border-primary input-sm" id="txtDelivered" value="No" readonly="readonly" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label">Delivery Remark</label>
                                                <div class="form-group">
                                                    <textarea class="form-control border-primary input-sm" rows="2" placeholder="" id="txtRemarks" disabled="disabled"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row" id="AccountDeliveryInfo">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Account Details</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-plus"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse">
                                    <div class="card-body">
                                        <div class="row">
                                            <!------------------------------- class NotTo  to hide fields for driver --------------------------------------->
                                            <div class="col-md-6">
                                                <label class="control-label">Payment Received?</label>
                                                <div class="form-group ">

                                                    <input type="text" class="form-control border-primary input-sm" style="text-transform: capitalize" id="txtRecievedPayment" value="No" readonly="readonly" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label">Account Remark</label>
                                                <div class="form-group">
                                                    <textarea class="form-control border-primary input-sm" rows="3" placeholder="" id="txtAcctRemarks" disabled="disabled"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>

                <div class="col-md-5  col-sm-5">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <div class="row">
                                            <label class="col-md-4 col-sm-4 form-group text-right">Total Amount</label>
                                            <div class="col-md-8 col-sm-8 form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                    </div>
                                                    <input type="text" class="form-control border-primary input-sm" style="text-align: right" id="txtTotalAmount" value="0.00" readonly="readonly" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-md-4 col-sm-4 form-group text-right">Overall Discount</label>
                                            <div class="col-md-8 col-sm-8">
                                                <div style="display: flex">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control border-primary input-sm text-right" id="txtOverallDisc" value="0.00" readonly="readonly" />
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text border-radius" style="padding: 0rem 1rem;"><span>%</span></span>
                                                        </div>
                                                    </div>
                                                    &nbsp;                                                   
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                            </div>
                                                            <input type="text" class="form-control border-primary input-sm" style="text-align: right;" id="txtDiscAmt" value="0.00" readonly="readonly" />
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-4 col-sm-4 form-group text-right">Shipping Charges</label>
                                            <div class="col-md-8 col-sm-8 form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                    </div>
                                                    <input type="text" class="form-control border-primary input-sm" style="text-align: right" id="txtShipping" value="0.00" readonly="readonly" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="row">
                                            <label class="col-md-4 col-sm-4 form-group text-right">Tax Type</label>
                                            <div class="col-md-8 col-sm-8 form-group">
                                                <input type="text" class="form-control border-primary input-sm" id="txtTaxType" value="0.00" readonly="readonly" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-4 col-sm-4 form-group text-right">Total Tax</label>
                                            <div class="col-md-8  col-sm-8 form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                    </div>
                                                    <input type="text" class="form-control border-primary input-sm" style="text-align: right" id="txtTotalTax" value="0.00" readonly="readonly" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-sm-4 form-group text-right">ML Quantity</label>
                                            <div class="col-sm-8 form-group">
                                                <input type="text" class="form-control border-primary input-sm" style="text-align: right" id="txtMLQty" value="0.00" readonly="readonly" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-sm-4 form-group text-right">ML Tax</label>
                                            <div class="col-sm-8 form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                    </div>
                                                    <input type="text" class="form-control border-primary input-sm" style="text-align: right" id="txtMLTax" value="0.00" readonly="readonly" />
                                                </div>
                                            </div>
                                        </div>
                                         <div class="row">
                                            <label class="col-sm-4 form-group text-right">Weight Quantity</label>
                                            <div class="col-sm-8 form-group">
                                                <input type="text" class="form-control border-primary input-sm" style="text-align: right" id="txtWeightQty" value="0.00" readonly="readonly" />
                                            </div>
                                        </div>
                                         <div class="row">
                                            <label class="col-sm-4 form-group text-right">Weight Tax</label>
                                            <div class="col-sm-8 form-group">
                                                <input type="text" class="form-control border-primary input-sm" style="text-align: right" id="txtWeightTax" value="0.00" readonly="readonly" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-sm-4 form-group text-right">Adjustment</label>
                                            <div class="col-sm-8 form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                    </div>
                                                    <input type="text" class="form-control border-primary input-sm" style="text-align: right" id="txtAdjustment" value="0.00" readonly="readonly" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-4 col-sm-4 form-group text-right"><b>Grand Total</b></label>
                                            <input type="hidden" id="hiddenGrandTotal" />
                                            <div class="col-md-8 col-sm-8 form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                    </div>
                                                    <input type="text" class="form-control border-primary input-sm inputBold text-right" style="font-weight: bold" readonly="readonly" id="txtGrandTotal" value="0.00" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row SMANAGER">
                                            <label class="col-md-4 col-sm-4 form-group text-right" style="white-space: nowrap">Credit Memo Amount</label>
                                            <div class="col-md-8 col-sm-8 form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                    </div>
                                                    <input type="text" class="form-control border-primary input-sm" style="text-align: right" id="txtDeductionAmount" value="0.00" readonly="readonly" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="row SMANAGER">
                                            <label class="col-md-4 col-sm-4 form-group text-right" style="white-space: nowrap">Store Credit Amount<span style="display: none"> (max $<span id="CreditMemoAmount">0.00</span> Available)</span></label>
                                            <div class="col-md-8 col-sm-8 form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                    </div>
                                                    <input type="text" class="form-control border-primary input-sm" style="text-align: right" readonly="true" id="txtStoreCreditAmount" onfocus="this.select()" value="0.00" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="row SMANAGER">
                                            <label class="col-md-4 col-sm-4 form-group text-right">Payable Amount</label>
                                            <div class="col-md-8 col-sm-8 form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                    </div>
                                                    <input type="text" class="form-control border-primary input-sm" style="text-align: right" readonly="true" id="txtPaybleAmount" value="0.00" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="display: none">
                                            <label class="col-md-4 col-sm-4 form-group text-right"><b>Total Amount Paid</b></label>
                                            <div class="col-md-8 col-sm-8 form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                    </div>
                                                    <input type="text" class="form-control border-primary input-sm inputBold" readonly="readonly" id="txtAmtPaid" value="0.00" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" id="rowAmountDue" style="display: none">
                                            <label class="col-md-4 col-sm-4 form-group text-right"><b>Amount Due</b></label>
                                            <div class="col-md-8 col-sm-8 form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                    </div>
                                                    <input type="text" class="form-control border-primary input-sm inputBold" readonly="readonly" id="txtAmtDue" value="0.00" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="row">
                                            <label class="col-md-4 col-sm- form-group text-right" style="white-space: nowrap">No. of Packed Boxes</label>
                                            <div class="col-md-8 col-sm-8 form-group">
                                                <div class="input-group">

                                                    <input type="text" class="form-control border-primary input-sm" id="txtPackedBoxes" runat="server" readonly="readonly" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>
    <input type="hidden" runat="server" id="HDDomain" />
    <div id="PopPrintTemplate" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Select Invoice Template</h4>
                    <button type="button" class="close pull-right" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-10">
                                        <input type="radio" name="Template" id="chkdefault" />
                                        <label for="chkdefault">PSM Default  - Invoice Only</label>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="row">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-10">
                                        <input type="radio" name="Template" id="chkdueamount" />
                                        <label for="chkdueamount">PSM Default</label>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="row">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-10">
                                        <input type="radio" name="Template" id="PackingSlip" />
                                        <label for="PackingSlip">Packing Slip</label>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="row">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-10">
                                        <input type="radio" name="Template" id="WithoutCategorybreakdown" />
                                        <label for="PackingSlip">Packing Slip - Without Category Breakdown</label>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="row" id="divPSMPADefault" style="display:none">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-10">
                                        <input type="radio" name="Template" id="PSMPADefault" />
                                        <label for="PSMPADefault">PSM PA Default</label>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="row" id="divPSMNPADefault" style="display:none">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-10">
                                        <input type="radio" name="Template" id="rPSMNPADefault" />
                                        <label for="PSMPADefault">PSM NPA Default</label>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="row">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-10">
                                        <input type="radio" name="Template" id="Template1" />
                                        <label for="Template1">7 Eleven (Type 1)</label>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="row">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-10">
                                        <input type="radio" name="Template" id="Template2" />
                                        <label for="Template2">7 Eleven (Type 2)</label>
                                    </div>
                                </div>
                                  <div class="clearfix"></div>
                                 <div class="row form-group">
                                         <div class="col-md-2"></div>
                                        <div class="col-md-10">
                                            <input type="radio" name="Template" id="Templ1" /> Template 1
                                        </div>
                                    </div>
                                 <div class="clearfix"></div>
                                <div class="row form-group">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-10">
                                        <input type="radio" name="Template" id="Templ2" />
                                        Template 2
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" onclick="PrintOrder()">Print</button>
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1  btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <div class="modal fade" id="modalMisc" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Drivers</h4>
                    <button type="button" class="close pull-right" data-dismiss="modal">&times;</button>

                </div>

                <div class="modal-body">
                    <div class="row anyClass">
                        <div class="col-md-12">
                            <div id="insideText">
                            </div>
                            <div id="DriverText" style="display: none">
                                <div class="table-responsive" id="tblAssignDrv" style="display: none;">
                                    <div class="row form-group">
                                        <div class="col-sm-2">Assign Date :</div>
                                        <div class="col-sm-4 form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text " style="padding: 0rem 1rem;">
                                                        <span class="la la-calendar-o"></span>

                                                    </span>
                                                </div>
                                                <input type="text" id="txtAssignDate" readonly class="form-control border-primary input-sm reqdriv" />
                                            </div>
                                        </div>
                                    </div>
                                  
                                    <table class="table table-bordered table-striped" id="tbldriver">
                                        <thead class="bg-blue white">
                                            <tr>
                                                <td class="DrvName">Driver Name</td>
                                                <td class="AsgnOrders text-center">Assigned Orders</td>
                                                <td class="Select  text-center">Select</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1 btn-sm" style="display: none;" id="btnAsgn" onclick="assignDriver()">Assign</button>
                    <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1 btn-sm" style="display: none;" id="btnOk" onclick="packingConfirmed()" data-dismiss="modal">OK</button>
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm" data-dismiss="modal">Cancel</button>
                </div>

            </div>

        </div>
    </div>
    <div class="modal fade" id="OrderBarcode" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    Order Cancellation Form
                </div>
                <div class="modal-body">
                    <div id="form-group">
                        <label class="form-group">Cancellation Remark</label>
                    </div>
                    <div id="Div8">
                        <textarea id="txtCancellationRemarks" placeholder="Please Enter Cancellation Remark" class="form-control border-primary req" row="4"></textarea>
                    </div>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1  btn-sm" id="Button3" onclick="cancelOrder()">Cancel Order</button>
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1  btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Popup For Order Log -->
    <div id="modalOrderLog" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-xl">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row w-100">
                        <div class="col-md-8">
                             <h4 class="modal-title">Order Log</h4>
                        </div>
                         <div class="col-md-4 text-right">
                    <label><b>Order No</b>&nbsp;:&nbsp;<span id="Span1"></span>&nbsp;&nbsp;<b>Order Date</b>&nbsp;:&nbsp;<span id="lblOrderDate"></span></label>

                         </div>
                    </div>
                   
                  
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="tblOrderLog">
                            <thead class="bg-blue white">
                                <tr>
                                    <td class="SrNo text-center">SN</td>
                                    <td class="ActionBy">Action By</td>
                                    <td class="Date text-center">Date</td>
                                    <td class="Action">Action</td>
                                    <td class="Remark">Remark</td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1  btn-sm" data-dismiss="modal">Close</button>

                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="/OperationManager/js/OM_OrderView.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
</asp:Content>

