﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/MasterPage.master" %>

<script runat="server">

</script>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="row" id="dashboard" style="display: none">
        <div class="col-md-11" style="padding-bottom:5px">
            <button type="button" onclick="LoadDashboard()" class="pull-right btn btn-sm btn-success" id="linktoOrderList"><b>Load Dashboard</b></button>

        </div>
        <div class="clearfix"></div>
        
        <div class="col-md-1"></div>
        <div class="col-md-4">
            <table class="table table-striped table-bordered">
                <thead class="bg-blue white">
                    <tr>
                        <td colspan="2" style="text-align: center">Today's Overview</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Total Orders</td>
                        <td id="TotalOrders" style="text-align: right"><span>$</span>&nbsp;0</td>
                    </tr>
                    <tr>
                        <td>Total Sales</td>
                        <td id="TotalSales" style="text-align: right"><span>$</span>&nbsp;0.0</td>
                    </tr>
                </tbody>
            </table>
            <table class="table table-striped table-bordered" id="tableOrder">
                <thead class="bg-blue white">
                    <tr>
                        <td colspan="2" style="text-align: center">Order Status </td>
                    </tr>
                    <tr style="display: none">
                        <td class="StatusType"></td>
                        <td class="TotalOrder"></td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <table class="table table-striped table-bordered">
                <thead class="bg-blue white">
                    <tr>
                        <td colspan="2" style="text-align: center">Inventory Summary </td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Item Below Threshold Value</td>
                        <td id="ThresholdValue"></td>
                    </tr>
                    <tr>
                        <td>Product Out Of Stock</td>
                        <td id="OutOfStock"></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-6">
            <div class="table-responsive">
                <table class="table table-striped table-bordered" id="tableSalesRevenue">
                    <thead class="bg-blue white">
                        <tr>
                            <td colspan="4" style="background-color: #fce5cd; color: black"><b style="vertical-align: -5px">Sales Revenue By Sales Person</b>
                                <select id="ddlSalesRevenue" class="form-control input-sm" style="width: 100px; float: right" onchange="getSalesRevenue()">
                                    <option value="0">Today</option>
                                    <option value="1">Yesterday</option>
                                    <option value="2">This Month</option>
                                    <option value="3">Last Month</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="SalesPerson">Sales Person Name	</td>
                            <td class="TotalOrder" style="text-align: right">Total Order</td>
                            <td class="TotalSales" style="text-align: right">Total Sales</td>
                            <td class="AOV" style="text-align: right">AOV</td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td>Total</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered" id="tableCollectionDetails">
                    <thead class="bg-blue white">
                        <tr>
                            <td colspan="3" style="background-color: #fce5cd; color: black"><b style="vertical-align: -5px">Collection Details</b>
                                <select id="ddlCollection" class="form-control input-sm" style="width: 100px; float: right" onchange="CollectionDetails()">
                                    <option value="0">Today</option>
                                    <option value="1">Yesterday</option>
                                    <option value="5">Overall</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="StatusType">Status	</td>
                            <td class="totalCount">Count</td>
                            <td class="ReceivedAmount">Amount</td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="table-responsive" style="display: none">
                <table class="table table-striped table-bordered" id="table1">
                    <thead class="bg-blue white">
                        <tr>
                            <td colspan="3" style="background-color: #fce5cd; color: black"><b>Total Return Orders</b></td>
                            <td>
                                <select id="ddlReturnOrder" class="form-control input-sm">
                                    <option value="0">Today</option>
                                    <option value="1">Yesterday</option>
                                    <option value="2">Overall</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="StatusType">Total Order	</td>
                            <td class="totalCount">Count</td>
                            <td class="ReceivedAmount">Amount</td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered" id="tableCreditMaster">
                    <thead class="bg-blue white">
                        <tr>
                            <td colspan="3" style="background-color: #fce5cd; color: #000"><b style="vertical-align: -5px">Total Credit Memo</b>
                                <select id="ddlCreditMemo" style="width: 100px; float: right" class="form-control input-sm" onchange="CreditMemoDetails()">
                                    <option value="0">Today</option>
                                    <option value="1">Yesterday</option>
                                    <option value="4">Overall</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="StatusType">Status	</td>
                            <td class="totalCount">Count</td>
                            <td class="ReceivedAmount">Amount</td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script src="/Warehouse/JS/DashBoardMaster.js"></script>
</asp:Content>

