﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="NeworderList.aspx.cs" Inherits="pos_orderList" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Order List</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Orders</a>
                        </li>
                        <li class="breadcrumb-item">Order List
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a href="/Warehouse/OrderNew.aspx" class="dropdown-item" id="linktoOrderList">Create New Order</a>
                    <%--<button type="button" class="dropdown-item">Export</button>--%>
                    <input type="hidden" id="hiddenPackerAutoId" runat="server" />
                    <input type="hidden" id="hiddenEmpType" runat="server" />
                </div>
            </div>
        </div>
    </div>

    <div class="content-body" style="min-height:400px">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-4">
                                        <select class="form-control border-primary input-sm" id="ddlCustomer" runat="server">
                                            <option value="0">All Customer</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Order No" id="txtSOrderNo" onfocus="this.select()" />
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <select class="form-control border-primary input-sm" id="ddlStatus" runat="server">
                                            <option value="0">All Status</option>
                                             <option value="8">Cancelled</option>
                                            <option value="11">Close</option>
                                            <option value="6">Delivered</option>
                                        </select>
                                    </div>

                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    From Order Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" onchange="setdatevalidation(1)" placeholder="Order From Date" id="txtSFromDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    To Order Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" onchange="setdatevalidation(2)" placeholder="Order To Date" id="txtSToDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <select class="form-control input-sm border-primary" id="ddlShippingType" runat="server">
                                            <option value="0">All Shipping Type</option>
                                        </select>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-md-12">
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm pull-right" id="btnSearch">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
       
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblOrderList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="action text-center width3per">Action</td>
                                                        <td class="orderNo text-center width3per">Order No</td>
                                                        <td class="orderDt text-center width4per">Order Date</td>
                                                        <td class="cust">Customer</td>
                                                        <td class="value price width3per">Order Amount ($)</td>
                                                        <td class="product text-center width3per">Order Items</td>
                                                        <td class="status text-center width3per">Status</td>
                                                        <td class="ShippingType text-center">Shipping Type</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <select class="form-control input-sm border-primary pagesize" id="ddlPageSize" onchange="getOrderList(1)"> 
                                            <option selected="selected" value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager text-right"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <!-- Modal -->
    <div id="modalOrderLog" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="modal-title">Order Log</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label style="margin: 4px 0 0 0 !important"><b>Order No </b>&nbsp;:&nbsp;<span id="lblOrderNo"></span>&nbsp;&nbsp;<b> Order Date</b>&nbsp;:&nbsp;<span id="lblOrderDate"></span></label>
                        </div>
                    </div>
                </div>
                <div class="modal-body" style="padding:6px">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="tblOrderLog" style="margin-bottom:0 !important">
                            <thead class="bg-blue white">
                                <tr>
                                    <td class="SrNo text-center width2per">SN</td>
                                    <td class="ActionBy">Action By</td>
                                    <td class="Date text-center width3per">Date</td>
                                    <td class="Action width3per">Action</td>
                                    <td class="Remark">Remark</td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation round box-shadow-1  btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
     
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="JS/OrderList.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
</asp:Content>

