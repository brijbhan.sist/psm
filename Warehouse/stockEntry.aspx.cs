﻿using DllStockReceiverMaster;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Warehouse_stockEntry : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod(EnableSession = true)]
    public static string bindVendor()
    {
        PL_StockReceiverMaster pobj = new PL_StockReceiverMaster();
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                BL_StockReceiverMaster.GetVendornStatus(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                return json;
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string bindProduct()
    {
        PL_StockReceiverMaster pobj = new PL_StockReceiverMaster();
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                BL_StockReceiverMaster.bindProduct(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                return json;
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string bindUnitType(int productAutoId)
    {
        PL_StockReceiverMaster pobj = new PL_StockReceiverMaster();
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {

            try
            {
                pobj.ProductAutoId = productAutoId;
                BL_StockReceiverMaster.bindUnitType(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string countBarcode(string dataValues)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValues);
            PL_StockReceiverMaster pobj = new PL_StockReceiverMaster();
            try
            {
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]);

                BL_StockReceiverMaster.countBarcode(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string Saveasdraft(string Draftdata)
    {

        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(Draftdata);
        PL_StockReceiverMaster pobj = new PL_StockReceiverMaster();
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                if (jdv["DraftAutoId"] != "")
                    pobj.DraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
                pobj.VendorAutoId = Convert.ToInt32(jdv["VendorAutoId"]);
                pobj.BillNo = jdv["billNo"];
                if (jdv["billDate"] != null && jdv["billDate"] != "")
                {
                    pobj.BillDate = Convert.ToDateTime(jdv["billDate"]);
                }
                pobj.Remarks = (jdv["remarks"]);
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]);
                pobj.Quantity = Convert.ToInt32(jdv["Quantity"]);
                pobj.QtyPerUnit = Convert.ToInt32(jdv["QtyPerUnit"]);
                pobj.TotalPieces = Convert.ToInt32(jdv["TotalPieces"]);
                BL_StockReceiverMaster.Saveasdraft(pobj);
                if (!pobj.isException)
                {

                    return pobj.DraftAutoId.ToString();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string UpdateDraftReq(string Draftdata)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(Draftdata);
        PL_StockReceiverMaster pobj = new PL_StockReceiverMaster();
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                if (jdv["DraftAutoId"] != "")
                    pobj.DraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]);
                pobj.Quantity = Convert.ToInt32(jdv["Quantity"]);
                pobj.TotalPieces = Convert.ToInt32(jdv["TotalPieces"]);
                BL_StockReceiverMaster.UpdateDraftReq(pobj);
                if (!pobj.isException)
                {

                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {

                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string getProductThruBarcode(string Draftdata)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(Draftdata);
            PL_StockReceiverMaster pobj = new PL_StockReceiverMaster();
            try
            {
                if (jdv["DraftAutoId"] != "")
                    pobj.DraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
                if (jdv["billDate"] != null && jdv["billDate"] != "")
                {
                    pobj.BillDate = Convert.ToDateTime(jdv["billDate"]);
                }
                pobj.VendorAutoId = Convert.ToInt32(jdv["VendorId"]);
                pobj.Barcode = jdv["Barcode"];
                pobj.BillNo = jdv["BillNo"];
                pobj.Remarks = jdv["Remark"];
                pobj.UserAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                BL_StockReceiverMaster.getProductThruBarcode(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {

                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string updateDraftOrder(string Draftdata)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(Draftdata);
        PL_StockReceiverMaster pobj = new PL_StockReceiverMaster();
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                if (jdv["DraftAutoId"] != "")
                    pobj.DraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
                pobj.BillNo = jdv["billNo"];
                if (jdv["billDate"] != null && jdv["billDate"] != "")
                {
                    pobj.BillDate = Convert.ToDateTime(jdv["billDate"]);
                }
                pobj.Remarks = (jdv["remarks"]);
                BL_StockReceiverMaster.updateDraftOrder(pobj);
                if (!pobj.isException)
                {

                    return pobj.DraftAutoId.ToString();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {

                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string deleteDraftitem(string Draftdata)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(Draftdata);
        PL_StockReceiverMaster pobj = new PL_StockReceiverMaster();
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                if (jdv["DraftAutoId"] != "")
                    pobj.DraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]);

                BL_StockReceiverMaster.deleteDraftitem(pobj);
                if (!pobj.isException)
                {

                    return pobj.DraftAutoId.ToString();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {

                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}