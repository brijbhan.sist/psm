﻿$(document).ready(function () {
    $('#txtSFromBillDate').pickadate({
        min: new Date("01/01/2019"),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtSToBillDate').pickadate({
        min: new Date("01/01/2019"),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    var d = new Date();
    var month = d.getMonth() + 1;
    if (month.toString().length == 1) {
        month = '0' + month;
    }
    var day = d.getDate();
    if (day.toString().length == 1) {
        day = '0' + day;
    }
    var year = d.getFullYear();
    $("#txtSFromBillDate").val(month + '/' + day + '/' + year);
    $("#txtSToBillDate").val(month + '/' + day + '/' + year);
    bindVendor();
    getStockEntryList(1);
})

function bindVendor() {
    $.ajax({
        type: "POST",
        url: "/Admin/DropDownMaster.aspx/BindVendor",
        data: "{}",
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var getData = $.parseJSON(response.d);
            $("#ddlSVendor option:not(:first)").remove();
            $.each(getData[0].VendorList, function (index, item) {
                $("#ddlSVendor").append("<option value='" + item.AutoId + "'>" + item.VendorName + "</option>");
            });
            $("#ddlSVendor").select2();
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function setdatevalidation(val) {
    var fdate = $("#txtSFromBillDate").val();
    var tdate = $("#txtSToBillDate").val();
    if (val == 1) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtSToBillDate").val(fdate);
        }
    }
    else if (val == 2) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtSFromBillDate").val(tdate);
        }
    }
}

function getStockEntryList(index) {
    var data = {
        BillNo: $("#txtSBillNo").val().trim(),
        VendorAutoId: $("#ddlSVendor").val(),
        FromBillDate: $("#txtSFromBillDate").val(),
        ToBillDate: $("#txtSToBillDate").val(),
        PageSize: $("#ddlPaging").val(),
        Index: index
    };

    $.ajax({
        type: "POST",
        url: "/Warehouse/WebAPI/WStockReceiverMaster.asmx/getStockEntryList",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var entryList = $(xmldoc).find("Table");

                    $("#tblStockEntryList tbody tr").remove();
                    var row = $("#tblStockEntryList thead tr").clone(true);

                    if (entryList.length > 0) {
                        $("#EmptyTable").hide();
                        var gTotal = 0.00;
                        $.each(entryList, function () {
                            $(".BillNo", row).html("<span BillAutoId='" + $(this).find("AutoId").text() + "'>" + $(this).find("BillNo").text() + "</span>");
                            $(".BillDate", row).text($(this).find("BillDate").text());
                            $(".Vendor", row).text($(this).find("VendorName").text());
                            $(".Products", row).text($(this).find("NoOfItems").text());
                            $(".Remarks", row).text($(this).find("Remarks").text());
                            if ($(this).find("StatusType").text() == 'Draft') {
                                $(".Status", row).html("<span class='badge badge badge-pill badge-primary'>" + $(this).find("StatusType").text() + "</span>");
                            }
                            else if ($(this).find("StatusType").text() == 'Pending') {
                                $(".Status", row).html("<span class='badge badge badge-pill badge-info'>" + $(this).find("StatusType").text() + "</span>");
                            }
                            else if ($(this).find("StatusType").text() == 'Complete') {
                                $(".Status", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("StatusType").text() + "</span>");
                            } else if ($(this).find("StatusType").text() == 'Revert') {
                                $(".Status", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("StatusType").text() + "</span>");
                            }
                            if ($(this).find("StatusType").text() == 'Complete') {
                                $(".Action", row).html("<a href='/warehouse/stockEntry.aspx?BillAutoId=" + $(this).find("AutoId").text() + "'><span class='ft-edit' ></span></a>&nbsp;&nbsp; <a href='javascript:;' onclick='print_NewOrder(" + $(this).find("AutoId").text() + ")'><span class='icon-printer' title='Print'></span></a>");
                            }
                            else {
                                $(".Action", row).html("<a href='/warehouse/stockEntry.aspx?BillAutoId=" + $(this).find("AutoId").text() + "'><span class='ft-edit' ></span></a>&nbsp;&nbsp;<a title='Delete' href='#'><span class='ft-x' onclick='deleteStocks(\"" + $(this).find("AutoId").text() + "\")'/></a>&nbsp;&nbsp; <a href='javascript:;' onclick='print_NewOrder(" + $(this).find("AutoId").text() + ")'><span class='icon-printer' title='Print'></span></a>");
                            }
                           
                            $("#tblStockEntryList tbody").append(row);
                            row = $("#tblStockEntryList tbody tr:last").clone(true);
                            gTotal = Number(gTotal) + Number($(this).find("NoOfItems").text());
                        });
                        $("#gTotal").text(Number(gTotal).toFixed(0));
                    } else {
                        $("#gTotal").html('0');
                        $("#EmptyTable").show();
                    }
                    
                    var pager = $(xmldoc).find("Table1");
                    $(".Pager").ASPSnippets_Pager({
                        ActiveCssClass: "current",
                        PagerCssClass: "pager",
                        PageIndex: parseInt(pager.find("PageIndex").text()),
                        PageSize: parseInt(pager.find("PageSize").text()),
                        RecordCount: parseInt(pager.find("RecordCount").text())
                    });
                }
            }
            else {
                location.href = "/";
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function print_NewOrder(num) {
    window.open("/Purchase/PurchaseOrderPrint.html?Id=" + num + "&type=" + 2, "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
}
$("#btnSearch").click(function () {
    getStockEntryList(1);
})
function Pagevalue(e) {
    getStockEntryList(parseInt($(e).attr("page")));
};
$("#ddlPaging").change(function () {
    getStockEntryList(1);
})
/*--------------------------------------------------------Delete stock entry ----------------------------------------------------------*/
function deletestock(AutoId) {
    $.ajax({
        type: "POST",
        url: "stockEntryList.aspx/deletestock",
        data: "{'AutoId':'" + AutoId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (result) {
            if (result.d != "Session Expired") {
                if (result.d == 'Success') {
                    swal("", "P.O. deleted successfully.", "success");
                    getStockEntryList(1);
                } else {
                    swal("Error!", result.d, "error");
                }
            }
            else {
                location.href = '/';
            }

        },
        error: function (result) {

        },
        failure: function (result) {

        }
    })
}

function deleteStocks(AutoId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this Stock.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, cancel.",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, delete it.",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            deletestock(AutoId);
        }
    })
}