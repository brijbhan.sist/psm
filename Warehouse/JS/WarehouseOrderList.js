﻿$(document).ready(function () {
    if ($("#hiddenEmpType").val() == 2 || $("#hiddenEmpType").val() == 8) {
        $("#linktoOrderList").show();
    }
    if ($("#hiddenEmpType").val() == 2) {
        $("#col4").show();
    }
    else {
        $("#col4").hide();
    }
    if ($("#hiddenEmpType").val() == 7) {
        $("#btnExport").show();
        $("#Action").hide();
    }
    else {
        $("#Action").show();
    }

    bindStatus();
    BindCustomer();
    $('#txtSFromDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtSToDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    var d = new Date();
    var month = d.getMonth() + 1;
    if (month.toString().length == 1) {
        month = '0' + month;
    }
    var day = d.getDate();
    if (day.toString().length == 1) {
        day = '0' + day;
    }
    var year = d.getFullYear();
    $("#txtSFromDate").val(month + '/' + day + '/' + year);
    $("#txtSToDate").val(month + '/' + day + '/' + year);
 
    getOrderList(1)
})

function Pagevalue(e) {
    getOrderList(parseInt($(e).attr("page")));
};

function bindStatus() {
    $.ajax({
        type: "POST",
        async:false,
        url: "/Warehouse/WebAPI/WarehouseOrderList.asmx/bindStatus",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var status = $(xmldoc).find("Table"); 
                var SalesPerson = $(xmldoc).find("Table1");
                var ShippingType = $(xmldoc).find("Table2");

                $("#ddlSStatus option:not(:first)").remove();
                $.each(status, function () {
                    $("#ddlSStatus").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("StatusType").text() + "</option>");
                });

                $("#ddlSStatus").val('1');
                $("#ddlSalesPerson option:not(:first)").remove();
                $.each(SalesPerson, function () {
                    $("#ddlSalesPerson").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("EmpName").text() + "</option>");
                });
                $("#ddlSalesPerson").select2();

                $("#ddlShippingType option:not(:first)").remove();
                $.each(ShippingType, function () {
                    $("#ddlShippingType").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("ShippingType").text() + "</option>");
                }); $("#ddlShippingType").attr('multiple', 'multiple');
                $("#ddlShippingType").select2({
                    placeholder: 'All Shipping Type',
                    allowClear: true
                });


            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function getOrderList(pageIndex) {

    var ShippingType = '0';
    $("#ddlShippingType option:selected").each(function (i) {
        if (i == 0) {
            ShippingType = $(this).val() + ',';
        } else {
            ShippingType += $(this).val() + ',';
        }
    });

    var data = {
        OrderNo: $("#txtSOrderNo").val().trim(),
        CustomerAutoid: $("#ddlCustomer").val(),
        SalesPersonAutoId: $("#ddlSalesPerson").val(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        Status: $("#ddlSStatus").val(),
        pageIndex: pageIndex,
        PageSize: $("#ddlPageSize").val(),
        ShippingType: ShippingType.toString()
    };

    $.ajax({
        type: "POST",
        url: "/Warehouse/WebAPI/WarehouseOrderList.asmx/getOrderList",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var orderList = $(xmldoc).find("Table1");
                $("#tblOrderList tbody tr").remove();
                var row = $("#tblOrderList thead tr").clone(true);
                if (orderList.length > 0) {
                    $("#EmptyTable").hide();
                    $.each(orderList, function () {
                        if ($(this).find("ShipId").text() == '2' || $(this).find("ShipId").text() == '7' || $(this).find("ShipId").text() == '4') {
                            $(row).css('background-color', '#ffe6e6');
                        }
                        else if ($(this).find("ShipId").text() == '8') {
                            $(row).css('background-color', '#eb99ff');
                        }
                        else if ($(this).find("ShipId").text() == '9') {
                            $(row).css('background-color', '#e6fff2');
                        }
                        else if ($(this).find("ShipId").text() == '10') {
                            $(row).css('background-color', '#ccff99');
                        }
                        else if (Number($(this).find("ShipId").text()) == 11 || Number($(this).find("ShipId").text()) == 12 || Number($(this).find("ShipId").text()) == 13|| Number($(this).find("ShipId").text()) == 15 || Number($(this).find("ShipId").text()) == 16) {
                            $(row).css('background-color', '#ff5200ab');
                        }
                        else {
                            $(row).css('background-color', '#fff');
                        }
                        $(".orderNo", row).html("<span OrderAutoId='" + $(this).find("AutoId").text() + "'>" + $(this).find("OrderNo").text() + "</span>");
                        $(".orderDt", row).text($(this).find("OrderDate").text());
                        $(".Shipping", row).text($(this).find("ShippingType").text());
                        $(".cust", row).text($(this).find("CustomerName").text());
                        $(".value", row).text($(this).find("GrandTotal").text());
                        $(".product", row).text($(this).find("NoOfItems").text());
                        $(".SalesPerson", row).text($(this).find("SalesPerson").text());
                        if (Number($(this).find("StatusCode").text()) == 1) {
                            $(".status", row).html("<span class='badge badge badge-pill Status_New'>" + $(this).find("Status").text() + "</span>");
                        } else if (Number($(this).find("StatusCode").text()) == 2) {
                            $(".status", row).html("<span class='badge badge badge-pill Status_Processed'>" + $(this).find("Status").text() + "</span>");
                        } 
                        else if (Number($(this).find("StatusCode").text()) == 9) {
                            $(".status", row).html("<span class='badge badge badge-pill Status_Add_On'>" + $(this).find("Status").text() + "</span>");
                        }
                        
                        if ($(this).find("StatusCode").text() == '2' || $(this).find("StatusCode").text() == '1' || $(this).find("StatusCode").text() == '9') {
                            $(".action", row).html("<input type='checkbox' name='table_records' id='chkProduct' onclick='chkProduct_OnClick(this);' style='background:blue;'>&nbsp;&nbsp;<b><a title='View' href='/Warehouse/ManageOrder.aspx?OrderNo=" + $(this).find("OrderNo").text() + "'><span class='la la-eye'></span></a>&nbsp;<a href='javascript:;' title='History' onclick='viewOrderLog(" + $(this).find("AutoId").text() + ")'><span class='la la-history'></span></a>");
                        } else {                           
                            $(".action", row).html("<b><a title='View' href='/Warehouse/ManageOrder.aspx?OrderNo=" + $(this).find("OrderNo").text() + "'><span class='la la-eye'></span></a>&nbsp;<a href='javascript:;' title='History' onclick='viewOrderLog(" + $(this).find("AutoId").text() + ")'><span class='la la-history'></span></a></b>");
                        }
                        $("#tblOrderList tbody").append(row);
                        row = $("#tblOrderList tbody tr:last").clone(true);
                    });
                } else {
                    $("#EmptyTable").show();
                }
                var pager = $(xmldoc).find("Table");
                $(".Pager").ASPSnippets_Pager({
                    ActiveCssClass: "current",
                    PagerCssClass: "pager",
                    PageIndex: parseInt(pager.find("PageIndex").text()),
                    PageSize: parseInt(pager.find("PageSize").text()),
                    RecordCount: parseInt(pager.find("RecordCount").text())
                });
            } else {
                location.href = "/";
            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}



function print_NewOrder(e) {
    window.open("/Packer/OrderPrint.html?OrderAutoId=" + $(e).closest("tr").find(".orderNo span").attr("OrderAutoId"), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    setTimeout(function () {
        getOrderList(1);
    }, 2000);
}

$("#ddlPageSize").change(function () {
    getOrderList(1);
})

function viewOrderLog(OrderAutoId) {
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/WOrderLog.asmx/viewOrderLog",
        data: "{'OrderAutoId':" + OrderAutoId + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var orderlog = $(xmldoc).find("Table");
            var order = $(xmldoc).find("Table1");

            $("#lblOrderNo").text(order.find("OrderNo").text());
            $("#lblOrderDate").text(order.find("OrderDate").text());

            $("#tblOrderLog tbody tr").remove();
            var row = $("#tblOrderLog thead tr").clone(true);
            if (orderlog.length > 0) {
                $("#EmptyTable").hide();
                $.each(orderlog, function (index) {
                    $(".SrNo", row).text(index + 1);
                    $(".ActionBy", row).text($(this).find("EmpName").text());
                    $(".Date", row).text($(this).find("ActionDate").text());
                    $(".Action", row).text($(this).find("Action").text());
                    $(".Remark", row).text($(this).find("Remarks").text());

                    $("#tblOrderLog tbody").append(row);
                    row = $("#tblOrderLog tbody tr:last").clone(true);
                });
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
    $("#modalOrderLog").modal('show');
}
$("#btnExport").click(function () {
    var ShippingType = '0';
    $("#ddlShippingType option:selected").each(function (i) {
        if (i == 0) {
            ShippingType = $(this).val() + ',';
        } else {
            ShippingType += $(this).val() + ',';
        }
    });
    var data = {
        OrderNo: $("#txtSOrderNo").val(),
        CustomerAutoid: $("#ddlCustomer").val(),
        SalesPersonAutoId: $("#ddlSalesPerson").val(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        Status: $("#ddlSStatus").val(),
        pageIndex: 1,
        ShippingType: ShippingType.toString(),
        PageSize: 0,
    };

    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/orderList.asmx/getOrderList",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('#fade').show();
        },
        complete: function () {
            $('#fade').hide();
        },
        success: function (response) {

            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var orderList = $(xmldoc).find("Table1");
                $("#tblExport tbody tr").remove();
                var row = $("#tblExport thead tr").clone(true);
                if (orderList.length > 0) {
                    $.each(orderList, function () {
                        $(".orderNo", row).html("<span OrderAutoId='" + $(this).find("AutoId").text() + "'>" + $(this).find("OrderNo").text() + "</span>");
                        $(".orderDt", row).text($(this).find("OrderDate").text());
                        $(".cust", row).text($(this).find("CustomerName").text());
                        $(".value", row).text($(this).find("GrandTotal").text());
                        $(".product", row).text($(this).find("NoOfItems").text());
                        $(".SalesPerson", row).text($(this).find("SalesPerson").text());
                        $(".status", row).html($(this).find("Status").text());
                        $("#tblExport tbody").append(row);
                        row = $("#tblExport tbody tr:last").clone(true);
                    });
                }
                $("#tblExport").table2excel({

                    exclude: ".noExl",
                    name: "Excel Document Name",
                    filename: "Order List",
                    fileext: ".xls",
                    exclude_img: true,
                    exclude_links: true,
                    exclude_inputs: true
                });

            } else {
                location.href = "/";
            }

        },
        failure: function (result) {
        },
        error: function (result) {
        }
    });
});


//-------------------------------------------------Bind Packers-------------------------------------------------
function AssignedPacker() {
    if ($('input:checkbox:checked', '#tblOrderList tbody').length > 0) {
        getPackerList();
        $('#packerDiv').modal('show');
        $("#txtTimes").attr('disabled', false);
        $("#txtTimes").val(30);
        $("#txtWarehouseRemark").attr('disabled', false);
        $("#txtWarehouseRemark").val($('#txtHWarehouseRemark').val());
    }
    else {
        toastr.error('Error : No Order selected.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
function getPackerList() {

    $.ajax({
        type: "Post",
        url: "/Warehouse/WebAPI/WarehouseOrderList.asmx/getDriverList",
        data: "{'LoginEmpType':'3'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var Drivers = $(xmldoc).find("Table");
                var selectDrv = $(xmldoc).find("Table1");
                $("#packerList tbody tr").remove();
                var row = $("#packerList thead tr").clone(true);
                $.each(Drivers, function () {
                    $(".DrvName", row).html("<span pkdautoid =" + $(this).find("AutoId").text() + ">" + $(this).find("Name").text() + "</span>");
                    if ($(this).find("AutoId").text() == $(selectDrv).find("PackerAutoId").text()) {
                        $(".Select", row).html("<input type='radio' name='packer' class='radio' onchange = 'changePacker(this)' checked='checked'>");
                        $("#btnAsgn").attr("disabled", true);
                    } else {
                        $(".Select", row).html("<input type='radio' name='packer' class='radio' onchange = 'changePacker(this)' >");
                    }
                    $("#packerList tbody").append(row);

                    row = $("#packerList tbody tr:last").clone(true);
                });
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

//-----------------------------------------------------Assign Packer-------------------------------------------
function assignPacker() {
    var PackerAutoId, flag = true;
    $("#packerList tbody tr").each(function () {
        if ($(this).find("input[name='packer']").is(":checked")) {
            PackerAutoId = $(this).find(".DrvName span").attr("pkdautoid");
            flag = false;
        }
    });
    var flag1 = false;
    data = new Array();
    $('input:checkbox:checked', '#tblOrderList tbody').each(function (index, item) {
        var row = $(item).closest('tr');
        data[index] = new Object();
        data[index].OrderAutoId = row.find('.orderNo span').attr("orderautoid");
        flag1 = true;
    });
    var dataValues = {
        PackerAutoId: PackerAutoId,
        Times: $("#txtTimes").val(),
        Remarks: $("#txtWarehouseRemark").val()
    };

    if (!flag) {
        $.ajax({
            type: "Post",
            url: "/Warehouse/WebAPI/WarehouseOrderList.asmx/assignPacker",
            data: "{'TableValues':'" + JSON.stringify(data) + "','dataValues':'" + JSON.stringify(dataValues) + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d != "Session Expired") {

                    if (response.d == 'true') {
                        swal("", "Packer assigned successfully.", "success").then(function () {
                            $('#packerDiv').modal('hide');
                            getOrderList(1);
                        });
                    } else {
                        swal("Error!", response.d, "error")
                    }

                } else {
                    location.href = '/';
                }

            },
            error: function (result) {
                swal("Error!", "Oops! Something went wrong.Please try later.", "error")
            },
            failure: function (result) {
                swal("Error!", "Oops! Something went wrong.Please try later.", "error")
            }
        });
    } else {
        toastr.error('Error : No Packer selected.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}

function BindCustomer() {
    var SalesPerson = $("#ddlSalesPerson").val();
    $.ajax({
        type: "POST",
        url: "/Warehouse/WebAPI/WarehouseOrderList.asmx/BindCustomer",
        data: "{'SalesPersonAutoId':'" + SalesPerson + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        asyn: false,
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {

                    var xmldoc = $.parseXML(response.d);
                    var CustomerDetails = $(xmldoc).find("Table");

                    $("#ddlCustomer option:not(:first)").remove();
                    $.each(CustomerDetails, function () {
                        $("#ddlCustomer").append("<option value='" + $(this).find("CustomerAutoId").text() + "'>" + $(this).find("CustomerName").text() + "</option>");
                    });
                    $("#ddlCustomer").select2();
                    var table = $('#tblOrderList').DataTable();
                    table.destroy();
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}


function Print() {
    var selected = new Array();
    var check = 0;
    $("#tblOrderList tbody tr ").each(function () {
        if ($(this).find('.action input').prop("checked") == true) {
            selected.push($(this).find(".orderNo span").attr('orderautoid'));
            localStorage.setItem("2", selected);
            check = 1;
        }
    })
    if (check == 1) {
        window.open("/Warehouse/PrintOrderMultiple.html", "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
      
    } else {
        toastr.error('Please select atleast one order.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}