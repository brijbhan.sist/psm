﻿$(document).ready(function () {
    if ($("#hiddenEmpType").val() == 1 || $("#hiddenEmpType").val() == 2 || $("#hiddenEmpType").val() == 7 || $("#hiddenEmpType").val() == 8) {
        $("#linktoOrderList").show();
    }
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    $("#txtSFromDate").val(month + '/' + day + '/' + year);
    $("#txtSToDate").val(month + '/' + day + '/' + year);
    bindDropdown();
    getOrderList(1);
    $('#txtSFromDate').pickadate({
        min: new Date("01/01/2019"),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtSToDate').pickadate({
        min: new Date("01/01/2019"),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    var d = new Date();
    var month = d.getMonth() + 1;
    if (month.toString().length == 1) {
        month = '0' + month;
    }
    var day = d.getDate();
    if (day.toString().length == 1) {
        day = '0' + day;
    }
    var year = d.getFullYear();
    $("#txtSFromDate").val(month + '/' + day + '/' + year);
    $("#txtSToDate").val(month + '/' + day + '/' + year);
    
})
function bindDropdown() {
    $.ajax({
        type: "POST",
        url: "OrderNew.aspx/bindDropdown",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            if (response.d != "Session Expired") {
                var getData = $.parseJSON(response.d);
                $("#ddlCustomer option:not(:first)").remove();
                $.each(getData[0].CustomerList, function (index, item) {
                    $("#ddlCustomer").append("<option value='" + item.A + "'>" + item.C + "</option>");
                });
                $("#ddlCustomer").select2();
             
                $("#ddlShippingType option:not(:first)").remove();
                $.each(getData[0].ShippingType, function (index, item) {
                    $("#ddlShippingType").append("<option value='" + item.AutoId + "'>" + item.ST + "</option>");
                }); 

              
            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(result.d);
        },
        error: function (result) {
            console.log(result.d);
        }
    });
}
function Pagevalue(e) {
    getOrderList(parseInt($(e).attr("page")));
};

function setdatevalidation(val) {
    var fdate = $("#txtSFromDate").val();
    var tdate = $("#txtSToDate").val();
    if (val == 1) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtSToDate").val(fdate);
        }
    }
    else if (val == 2) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtSFromDate").val(tdate);
        }
    }
}


function getOrderList(pageIndex) {
    var data = {
        OrderNo: $("#txtSOrderNo").val().trim(),
        CustomerAutoid: $("#ddlCustomer").val(),
        OrderStatus: $("#ddlStatus").val(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        ShippingType:$("#ddlShippingType").val(),
        pageIndex: pageIndex,
        PageSize: $("#ddlPageSize").val(),
    };

    $.ajax({
        type: "POST",
        url: "/Warehouse/WebAPI/WPOSOrderList.asmx/getPOSOrderList",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var orderList = $(xmldoc).find("Table1");

                $("#tblOrderList tbody tr").remove();
                var row = $("#tblOrderList thead tr").clone(true);

                if (orderList.length > 0) {
                    $("#EmptyTable").hide();
                    $.each(orderList, function () {
                        $(".orderNo", row).html("<span OrderAutoId='" + $(this).find("AutoId").text() + "'>" + $(this).find("OrderNo").text() + "</span>");
                        $(".orderDt", row).text($(this).find("OrderDate").text());
                        $(".cust", row).text($(this).find("CustomerName").text());
                        $(".value", row).text($(this).find("GrandTotal").text());
                        $(".product", row).text($(this).find("NoOfItems").text());    
                        $(".ShippingType", row).text($(this).find("ShippingType").text());
                       if (Number($(this).find("StatusCode").text()) == 10) {
                           $(".status", row).html("<span class='badge badge badge-pill badge-default'>" + $(this).find("Status").text() + "</span>");
                        }
                        else if (Number($(this).find("StatusCode").text()) == 11) {
                            $(".status", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("Status").text() + "</span>");
                        }
                        else if (Number($(this).find("StatusCode").text()) == 6) {
                            $(".status", row).html("<span class='badge badge badge-pill badge-info'>" + $(this).find("Status").text() + "</span>");
                        }
                        else {
                            $(".status", row).html("<span class='badge badge badge-pill Status_cancelled'>" + $(this).find("Status").text() + "</span>");
                        }
                        $(".action", row).html("<b><a href='/Warehouse/OrderNew.aspx?OrderNo=" + $(this).find("AutoId").text() + "'><span class='la la-edit' title='Edit'></span></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href='javascript:;' onclick='viewOrderLog(" + $(this).find("AutoId").text() + ")'><span class='la la-history' title='Log'></span></a><a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href='#' onclick='SendEmailPopup(" + $(this).find("AutoId").text() + ")'><span class='ft-mail' title='Send Mail'></span></a></b>");

                        $("#tblOrderList tbody").append(row);
                        row = $("#tblOrderList tbody tr:last").clone(true);
                    });
                } else {
                    $("#EmptyTable").show();
                }

                var pager = $(xmldoc).find("Table");
                $(".Pager").ASPSnippets_Pager({
                    ActiveCssClass: "current",
                    PagerCssClass: "pager",
                    PageIndex: parseInt(pager.find("PageIndex").text()),
                    PageSize: parseInt(pager.find("PageSize").text()),
                    RecordCount: parseInt(pager.find("RecordCount").text())
                });
            } else {
                location.href = "/";
            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function SendEmailPopup(OrderAutoId) {
    window.open("/EmailSender/SendEmail.aspx?OrderAutoId=" + OrderAutoId + "", "popUpWindow", "height=700,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");

}

function print_NewOrder(e) {
    window.open("/Packer/OrderPrint.html?OrderAutoId=" + $(e).closest("tr").find(".orderNo span").attr("OrderAutoId"), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    setTimeout(function () {
        getOrderList(1);
    }, 2000);
}

$("#btnSearch").click(function () {
    getOrderList(1);
})
function deleteOrder(orderAutoId) {
    if (confirm('Are you sure you want to delete this entry from list?')) {
        $.ajax({
            type: "POST",
            url: "/Sales/WebAPI/orderList.asmx/deleteOrder",
            data: "{'OrderAutoId':" + orderAutoId + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                $("#alertSuccessDelete").show();
                $("#alertSuccessDelete span").text("Order deleted Successfully!!");
                $(".close").click(function () {
                    $("#alertSuccessDelete").hide();
                })
                getOrderList(1);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
}

function viewOrderLog(OrderAutoId) {
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/WOrderLog.asmx/viewOrderLog",
        data: "{'OrderAutoId':" + OrderAutoId + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var orderlog = $(xmldoc).find("Table");
            var order = $(xmldoc).find("Table1");

            $("#lblOrderNo").text(order.find("OrderNo").text());
            $("#lblOrderDate").text(order.find("OrderDate").text());

            $("#tblOrderLog tbody tr").remove();
            var row = $("#tblOrderLog thead tr").clone(true);
            if (orderlog.length > 0) {
                $("#EmptyTable").hide();
                $.each(orderlog, function (index) {
                    $(".SrNo", row).text(index + 1);
                    $(".ActionBy", row).text($(this).find("EmpName").text());
                    $(".Date", row).text($(this).find("ActionDate").text());
                    $(".Action", row).text($(this).find("Action").text());
                    $(".Remark", row).text($(this).find("Remarks").text());

                    $("#tblOrderLog tbody").append(row);
                    row = $("#tblOrderLog tbody tr:last").clone(true);
                });
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
    $("#modalOrderLog").modal('show');
}
