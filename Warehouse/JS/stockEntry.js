﻿$(document).ready(function () {
    bindVendor();
    bindProduct();
    $('#txtBillDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    var d = new Date();
    var month = d.getMonth() + 1;
    if (month.toString().length == 1) {
        month = '0' + month;
    }
    var day = d.getDate();
    if (day.toString().length == 1) {
        day = '0' + day;
    }
    var year = d.getFullYear();
    $("#txtBillDate").val(month + '/' + day + '/' + year);
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    var getid = getQueryString('BillAutoId');
    if (getid != null) {
        editStockEntry(getid);
    }
    else {
        $("#emptyTable").show();
    }
    $("#txtBarcode").attr("disabled", "disabled");
})

function bindVendor() {
    
    $.ajax({
        type: "POST",
        url: "/Admin/DropDownMaster.aspx/BindVendor",
        data: "{}",
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var getData = $.parseJSON(response.d);
            $("#ddlVendor option:not(:first)").remove();
            $.each(getData[0].VendorList, function (index, item) {
                $("#ddlVendor").append("<option value='" + item.AutoId + "'>" + item.VendorName + "</option>");
            });
            $("#ddlVendor").select2();
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function onchangevendor() {
    $("#txtBarcode").removeAttr("disabled");
}

function bindProduct() {
    
    $.ajax({
        type: "POST",
        url: "/Admin/DropDownMaster.aspx/BindProduct",
        data: "{}",
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var getData = $.parseJSON(response.d);
            $("#ddlProduct option:not(:first)").remove();
            $.each(getData[0].ProductList, function (index, item) {
                $("#ddlProduct").append("<option value='" + item.AutoId + "'>" + item.ProductName + "</option>");
            });
            $("#ddlProduct").select2();
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}



function BindProductOnChenge() {
    
    $("#btnAdd").attr("disabled", false);
    $("#alertBarcodeCount").hide();
    $("#alertBarcode").hide();
    var productAutoId = $("#ddlProduct option:selected").val();
    if (productAutoId != '' || productAutoId != '0') {
        $.ajax({
            type: "POST",
            url: "/Warehouse/WebAPI/WStockReceiverMaster.asmx/bindUnitType",
            data: "{'productAutoId':" + productAutoId + "}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                var xmldoc = $.parseXML(response.d);
                var unitType = $(xmldoc).find("Table");
                var PackingType = $(xmldoc).find("Table1");
                if (unitType.length > 0) {
                    $("#ddlUnitType option:not(:first)").remove();
                    $.each(unitType, function () {
                        $("#ddlUnitType").append($("<option value='" + $(this).find("AutoId").text() + "' QtyPerUnit='" + $(this).find("Qty").text() + "'>" + $(this).find("UnitType").text() + " ( " + $(this).find("Qty").text() + "pcs" + " )</option>"));
                    })
                }

                $("#txtQuantity").val("1");
                $("#txtTotalPieces").val("0");
                if (PackingType.length > 0) {
                    $("#ddlUnitType").removeAttr('disabled');
                    $("#ddlUnitType").val($(PackingType).find("PackingAutoId").text());
                    $("#txtTotalPieces").val($("#ddlUnitType option:selected").attr('QtyPerUnit'));
                }
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
}

$("#ddlUnitType").change(function () {
    $("#txtQuantity").val("1");
    // $("#txtTotalPieces").val("0");
    calTotal();
    /*----------------------------------To Check Barcode Count---------------------------------*/

    var data = {
        ProductAutoId: $("#ddlProduct").val(),
        UnitAutoId: $("#ddlUnitType").val()
    };

    $.ajax({
        type: "Post",
        url: "/Warehouse/WebAPI/WStockReceiverMaster.asmx/countBarcode",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var barcode = $(xmldoc).find("Table");
                var ProductDetails = $(xmldoc).find("Table1");
                var BarcodeCount = $(barcode).find("BarcodeCount").text();
                Price = $(ProductDetails).find('Price').text();
                toastr.warning('Barcode Count : ' + BarcodeCount, 'Message', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

                //if ((BarcodeCount < 3 && (data.UnitAutoId == '1' || data.UnitAutoId == '2')) || (BarcodeCount < 5 && data.UnitAutoId == '3')) {
                $("#btnAdd").attr("disabled", false);
                //} else {
                //    $("#btnAdd").attr("disabled", true);
                //}
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
});

function calTotal() {

    var QtyperUnit = $("#ddlUnitType option:selected").attr("QtyPerUnit") || 0;
    var Qty = $("#txtQuantity").val() || 0;
    $("#txtTotalPieces").val(Number(Qty) * QtyperUnit);

    var unitPrice = $("#txtUnitprice").val() || 0;
    $("#txtTotalAmount").val((Number(Qty) * Number(unitPrice)).toFixed(2));
}

$("#txtQuantity").keyup(function () {
    calTotal();
});

$("#txtUnitprice").keyup(function () {
    calTotal();
});
var CHK = 0;
/*----------------------------------Add Row to table---------------------------------*/
$("#btnAdd").click(function () {
    debugger
    if (Number($("#txtQuantity").val()) == 0) {
        toastr.error("Quantity can't be  left empty or zero.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $("#txtQuantity").focus();
        $("#txtQuantity").addClass('border-warning');
        return;
    }
    if (checkRequiredField2()) {
        var ProductAutoId = $("#ddlProduct option:selected").val();
        var UnitType = $("#ddlUnitType option:selected").val();
        var TotalPieces = $("#txtTotalPieces").val();
        var Quantity = $("#txtQuantity").val();
        var QtyPerUnit = $("#ddlUnitType option:selected").attr("QtyPerUnit");
        var check = false, msg = 0;
        $("#tblProductDetail tbody tr").each(function () {
            if (Number(ProductAutoId) == Number($(this).find('.ProName span').attr('ProductAutoId')) && Number(UnitType) == Number($(this).find('.Unit span').attr('UnitAutoId'))) {
                $(this).find('.Qty input').val(($(this).find('.Qty input').val() == '' ? '0' : (Number($(this).find('.Qty input').val()) + Number($("#txtQuantity").val()))));
                var TotalPeices = 0;
                TotalPeices = parseInt($(this).find('.Qty input').val() * $(this).find('.Unit span').attr('qtyperunit'));
                $(this).find('.Pcs ').html(TotalPeices);
                check = true;
            }
            else if (Number(ProductAutoId) == Number($(this).find('.ProName span').attr('ProductAutoId')) && Number(UnitType) != Number($(this).find('.Unit span').attr('UnitAutoId'))) {
                check = true;
                msg = 1;
            }
        });
        if (msg == 1) {
            swal("", "You can't add different unit of added product.", "error");
        }
        else {
            $("#emptyTable").hide();
            if (check == false) {
                var product = $("#ddlProduct option:selected").text().split("--");
                var row = $("#tblProductDetail thead tr:last").clone(true);
                var unitPrice = $("#txtUnitprice").val() || 0.00;
                $(".ProId", row).text(product[0]);
                $(".ProName", row).html("<span ProductAutoId='" + $("#ddlProduct option:selected").val() + "'>" + product[1] + "</span>");
                $(".Unit", row).html("<span UnitAutoId='" + $("#ddlUnitType option:selected").val() + "' QtyPerUnit='" + $("#ddlUnitType option:selected").attr("QtyPerUnit") + "'>" + $("#ddlUnitType option:selected").text() + "</span>");
                $(".Qty", row).html("<input type='text' class='form-control input-sm text-center' maxlength='6' id='Qty'onfocus='removeProp(this)' onchange='calPcs(this)' value='" +
                    $("#txtQuantity").val() + "' onfocus='this.select()' onkeypress='return isNumberKey(event)'>");
                $(".Pcs", row).text($("#txtTotalPieces").val());

                $(".Action", row).html("<a href='#' id='deleterow' onclick='deleterecord(this)'><span class='ft-x'></span></a>");

                if ($('#tblProductDetail tbody tr').length > 0) {
                    $('#tblProductDetail tbody tr:first').before(row);
                }
                else {
                    $('#tblProductDetail tbody').append(row);
                }

            } caltotal();
            $('#ddlProduct').val('0').change();
            $('#ddlUnitType').val('0');
            $("#txtQuantity").val('');
            $("#txtTotalPieces").val('');
            $("#alertBarcodeCount").text('');
            $("#txtBarcode").focus();
            toastr.success('Item added successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            var Draftdata = {
                VendorAutoId: $("#ddlVendor option:selected").val(),
                billNo: $("#txtBillNo").val().trim(),
                billDate: $("#txtBillDate").val(),
                remarks: $("#txtRemarks").val(),
                ProductAutoId: ProductAutoId,
                UnitAutoId: UnitType,
                TotalPieces: TotalPieces,
                Quantity: Quantity,
                QtyPerUnit: QtyPerUnit,
                DraftAutoId: $("#txtHBillAutoId").val(),
                Status: 1
            };
            $.ajax({
                type: "POST",
                /*   url: "/Warehouse/WebAPI/WStockReceiverMaster.asmx/Saveasdraft",*/
                url: "/Warehouse/stockEntry.aspx/Saveasdraft",
                data: "{'Draftdata':'" + JSON.stringify(Draftdata) + "'}",
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                async: false,
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.d != "Session Expired") {
                        if (response.d != "false") {
                            $("#txtHBillAutoId").val(response.d);
                        }
                    } else {
                        location.href = '/';
                    }

                },
                error: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                },
                failure: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                }
            });
        }
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
});
function deleterecord(e) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this record.",
        icon: "warning",
        showCancelButton: true,
        allowOutsideClick: false,
        closeOnClickOutside: false,

        buttons: {
            cancel: {
                text: "No, Cancel.",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, Delete It.",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(isConfirm => {
        if (isConfirm) {
            deleterow(e);
        }
    })
}
function deleterow(e) {
    var row = $(e).closest("tr");
    $(e).closest('tr').remove();
    var Draftdata =
    {
        ProductAutoId: row.find('.ProName span').attr('ProductAutoId'),
        UnitAutoId: row.find(".Unit span").attr("UnitAutoId"),
        DraftAutoId: $("#txtHBillAutoId").val()
    }
    $.ajax({
        type: "POST",
        url: "/Warehouse/stockEntry.aspx/deleteDraftitem",
        data: "{'Draftdata':'" + JSON.stringify(Draftdata) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    swal("", "Item deleted successfully.", "success");
                    if ($("#tblProductDetail > tbody").children().length == 0) {
                        $("#emptyTable").show();
                    } else {
                        $("#emptyTable").hide();
                    }
                    caltotal();
                }
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

};

function checkRequiredField2() {
    var boolcheck = true;

    $('.ddladdreq').each(function () {
        if ($(this).val() == '0') {
            $(this).closest('div').find('.select2-selection').attr('style', 'border: 1px solid #e87e66 !important;');
        } else {
            $(this).closest('div').find('.select2-selection').removeAttr('style');
        }
    })
    $('.ddlreq').each(function () {
        if ($(this).val() == '0') {
            $(this).closest('div').find('.select2-selection').attr('style', 'border: 1px solid #e87e66 !important;');
        } else {
            $(this).closest('div').find('.select2-selection').removeAttr('style');
        }
    })
    $('.req2').each(function () {
        if ($(this).val() == '' || $(this).val() == '0.00' || $(this).val() == '0') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });
    $('.ddlreq2').each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == '-Select-' || $(this).val() == null) {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });
    return boolcheck;
}

function checkTbl() //update by satish 24-08-2019
{
    boolcheck = true;
    $('table input[type=text]').each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == '0.00') {
            $(this).addClass('border-warning');
            boolcheck = false;
        }
        else {
            $(this).removeClass('border-warning');
        }
    });
    return boolcheck;
}
/*----------------------------------Add Stock----------------------------------------*/
$("#btnAddStock").click(function () {

    saveStock(1)
});
function saveStock(status) {
    if (checkRequiredField() && checkTbl()) {
        var check = true;
        if ($('#tblProductDetail tbody tr').length > 0) {
            var Product = [];
            $("#tblProductDetail tbody tr").each(function () {
                if ($(this).find('.Qty input').val().trim() == '' || Number($(this).find('.Qty input').val().trim()) == '0') {
                    check = false;
                    $(this).find('.Qty input').css('border-color', 'red')
                }

                Product.push({
                    ProductAutoId: $(this).find('.ProName span').attr('ProductAutoId'),
                    UnitAutoId: $(this).find('.Unit span').attr('UnitAutoId'),
                    QtyPerUnit: $(this).find('.Unit span').attr('QtyPerUnit'),
                    Quantity: $(this).find('.Qty input').val(),
                    TotalPieces: $(this).find('.Pcs').text(),
                    Price: 0.00
                });
            });
            if (!check) {
                toastr.error("Quantity can't be  left empty or zero.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

                return;
            }
            var billData = {
                VendorAutoId: $("#ddlVendor option:selected").val(),
                billNo: $("#txtBillNo").val().trim(),
                billDate: $("#txtBillDate").val(),
                remarks: $("#txtRemarks").val(),
                Status: status
            };

            $.ajax({
                type: "Post",
                url: "/Warehouse/WebAPI/WStockReceiverMaster.asmx/insertStockEntry",
                //data: "{'TableValues':'" + JSON.stringify(Product) + "','billData':'" + JSON.stringify(billData) + "'}",
                data: JSON.stringify({ TableValues: JSON.stringify(Product), billData: JSON.stringify(billData) }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (data) {
                    if (data.d != "Session Expired") {
                        if (data.d == "true") {
                            if (status == 1) {
                                swal({
                                    title: "",
                                    text: "Draft P.O. saved successfully.",
                                    icon: "success",
                                    button: "Ok",
                                }).then(function () {
                                    //resetAfterAddStock();
                                    window.location.href = "/Warehouse/stockEntry.aspx";
                                })
                            }
                            else if (status == 2) {
                                swal({
                                    title: "",
                                    text: "P.O. submitted successfully.",
                                    icon: "success",
                                    button: "Ok",
                                }).then(function () {
                                    //resetAfterAddStock();
                                    window.location.href = "/Warehouse/stockEntry.aspx";
                                })
                            }
                        }
                        else {
                            swal("", data.d, "error");
                        }
                    }
                    else {
                        location.href = "/";
                    }
                },
                error: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                },
                failure: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                }
            });
        }
        else {
            toastr.error('No product added in table.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        }
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}



function editStockEntry(BillAutoId) {
    $.ajax({
        type: "POST",
        url: "/Warehouse/WebAPI/WStockReceiverMaster.asmx/editStockEntry",
        data: "{'BillAutoId':" + BillAutoId + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var stockEntry = $(xmldoc).find("Table");
                    var billitems = $(xmldoc).find("Table1");
                    //var product = $(xmldoc).find("Table2");
                    //$("#ddlProduct option:not(:first)").remove();
                    //$.each(product, function () {
                    //    $("#ddlProduct").append($("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("ProductName").text().replace(/&quot;/g, "\'") + "</option>"));
                    //});
                    $("#btnUpdate").show();
                    $("#btnGeneratePO").show();
                    $("#btnAddStock").hide();
                    $("#btnReset").hide();
                    //$("#pnlInputProduct").hide();
                    $("#ddlProduct").select2();
                    $("#txtHBillAutoId").val($(stockEntry).find("AutoId").text())
                    $("#txtStatus").val($(stockEntry).find("Status").text())
                    $("#txtBillNo").val($(stockEntry).find("BillNo").text());
                    $("#txtBillDate").val($(stockEntry).find("BillDate").text());
                    $("#txtRemarks").val($(stockEntry).find("Remarks").text());
                    $("#ddlVendor").val($(stockEntry).find("VendorAutoId").text()).change();
                    // updated by satish 24-08-2019
                    $("#ddlVendor").attr("disabled", true);
                    if ($(stockEntry).find("Status").text() == 'Pending') {
                        $("#act").attr('colspan', '3');
                    }
                    // end
                    $("#tblProductDetail tbody tr").remove();
                    var row = $("#tblProductDetail thead tr").clone(true);

                    if (billitems.length > 0) {
                        $("#emptyTable").hide();
                        $.each(billitems, function () {
                            $(".ProId", row).html("<span rowautoid='" + $(this).find("AutoId").text() + "'>" + $(this).find("ProductId").text() + "</span>");
                            $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "'>" + $(this).find("ProductName").text() + "</span>");
                            $(".Unit", row).html("<span UnitAutoId='" + $(this).find("UnitAutoId").text() + "' QtyPerUnit='" + $(this).find("QtyPerUnit").text() + "'>" + $(this).find("UnitType").text() + " (" + $(this).find("QtyPerUnit").text() + " pcs" + ")</span>");
                            $(".Qty", row).html("<input type='text' maxlength='6' class='form-control input-sm text-center' id='Qty' onchange='calPcs(this)' value='" + $(this).find("Quantity").text() + "'>");
                            $(".Pcs", row).text($(this).find("TotalPieces").text());
                            $(".Action", row).html("<a href='#' id='deleterow' onclick='deleterecord(this)'><span class='ft-x'></span></a>");
                            $("#tblProductDetail tbody").append(row);
                            row = $("#tblProductDetail tbody tr:last").clone(true);
                        });
                        //$("#tblProductDetail tr").find(".Action").hide();
                        caltotal();
                    }
                    else {
                        $("#emptyTable").show();
                    }

                    if ($(stockEntry).find("StatusCode").text() == 2 || $(stockEntry).find("StatusCode").text() == 4) {
                        $("#btnUpdate").hide();
                        $("#btnGeneratePO").hide();
                        $("#pnlInputProduct").hide();
                        $(".Action").hide();
                        $('input').attr('disabled', true);
                        $('select').attr('disabled', true);
                        $('textarea').attr('disabled', true);
                    }
                    if ($(stockEntry).find("StatusCode").text() == 4) {
                        $("#act").attr('colspan', '3');
                    }

                }
            }
            else {
                location.href = "/";
            }
        },
        failure: function (result) {
            swal("", result.d, "error");
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function removeProp(e) {
    $(e).attr('style', 'width: 100px;')
}

function calPcs(e) {
    var row = $(e).closest("tr");
    var Qty = Number(row.find('.Qty input').val()) || 0;
    var pcs = Number(Qty) * Number(row.find(".Unit span").attr("QtyPerUnit"));
    row.find(".Pcs").text(pcs);
    if (Qty > 0) {
        row.find('.Qty input').removeClass('border-warning');
        var Draftdata = {
            ProductAutoId: row.find('.ProName span').attr('ProductAutoId'),
            UnitAutoId: row.find(".Unit span").attr("UnitAutoId"),
            TotalPieces: pcs,
            Quantity: Qty,
            DraftAutoId: $("#txtHBillAutoId").val(),
        };
        $.ajax({
            type: "POST",
           // url: "/Warehouse/WebAPI/WStockReceiverMaster.asmx/UpdateDraftReq",
            url: "/Warehouse/stockEntry.aspx/UpdateDraftReq",
            data: "{'Draftdata':'" + JSON.stringify(Draftdata) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            async: false,
            beforeSend: function () {

            },
            complete: function () {

            },
            success: function (response) {
                if (response.d != "Session Expired") {
                    if (response.d != "false") {
                        caltotal();
                    }
                } else {
                    location.href = '/';
                }

            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
    else {
        row.find('.Qty input').addClass('border-warning');
    }
}
function caltotal() {
    var total = 0, PeiceTotal = 0;
    $("#tblProductDetail tbody tr").each(function () {
        if ($(this).find('.Qty input').val() != '')
            total += parseFloat($(this).find('.Qty input').val());
        PeiceTotal += parseFloat($(this).find('.Pcs').text());
    })
    $('#TotalQty').text(total);
    $('#TotalQtyNo').text(PeiceTotal);
}
/*----------------------------------Update ----------------------------------------*/
$("#btnUpdate").click(function () {
    var check = 1;
    $("#tblProductDetail tbody tr").each(function () {
        if (Number($(this).find('.Qty input').val()) == '' && Number($(this).find('.Qty input').val()) == '0') {
            check = Number(check) + 1;
            $(this).find('.Qty input').addClass('border-warning');
        }
    });
    if (check > 1) {
        toastr.error("Required quantity can't be  left empty or zero.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    if (checkRequiredField() && checkTbl()) {
        UpdateOrder(1);
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
})
function UpdateOrder(Status) {
    debugger;
    if ($("#tblProductDetail tbody tr").length > 0) {
        var Product = [];
        $("#tblProductDetail tbody tr").each(function () {
            Product.push({
                ProductAutoId: $(this).find('.ProName span').attr('ProductAutoId'),
                UnitAutoId: $(this).find('.Unit span').attr('UnitAutoId'),
                QtyPerUnit: $(this).find('.Unit span').attr('QtyPerUnit'),
                Quantity: $(this).find('.Qty input').val(),
                TotalPieces: $(this).find('.Pcs').text(),
                Price: 0.00
            });
        });
        var billData = {
            VendorAutoId: $("#ddlVendor option:selected").val(),
            billAutoId: $("#txtHBillAutoId").val(),
            billNo: $("#txtBillNo").val().trim(),
            billDate: $("#txtBillDate").val(),
            remarks: $("#txtRemarks").val(),
            Status: Status
        };

        $.ajax({
            type: "Post",
            url: "/Warehouse/WebAPI/WStockReceiverMaster.asmx/updateStockEntry",
            //data: "{'TableValues':'" + JSON.stringify(Product) + "','billData':'" + JSON.stringify(billData) + "'}",
            data: JSON.stringify({ TableValues: JSON.stringify(Product), billData: JSON.stringify(billData) }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (data) {
                if (data.d != "Session Expired") {
                    if (data.d == 'true') {
                        if (Status == 1) {
                            swal({
                                title: "",
                                text: "Draft P.O. saved successfully.",
                                icon: "success",
                                button: "OK",
                            }).then(function () {
                                editStockEntry($("#txtHBillAutoId").val())

                            })
                        }
                        else if (Status == 2) {
                            swal({
                                title: "",
                                text: "P.O. submitted successfully.",
                                icon: "success",
                                button: "OK",
                            }).then(function () {
                                editStockEntry($("#txtHBillAutoId").val())
                            })
                        }
                    }
                    else {
                        swal("", data.d, "error");
                    }
                } else {
                    location.href = "/";
                }
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
    else {
        toastr.error('No product added in table.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}

$("#btnGeneratePO").click(function () {
    debugger
    var check = 1;
    $("#tblProductDetail tbody tr").each(function () {
        if (Number($(this).find('.Qty input').val()) == '' && Number($(this).find('.Qty input').val()) == '0') {
            check = Number(check) + 1;
            $(this).find('.Qty input').addClass('border-warning');
        }
    });
    if (check > 1) {
        toastr.error("Quantity can't be  left empty or zero.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    if (checkRequiredField() && checkTbl()) {
        if ($("#txtHBillAutoId").val() != '') {
            UpdateOrder(2);
        } else {
            saveStock(2);
        }
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
})
$("#btnReset").click(function () {
    location.href = "/warehouse/stockEntry.aspx";

})
function resetProductPanel() {
    $("#panelProduct select").val(0);
    $("#txtQuantity, #txtTotalPieces").val("0");
    $("#txtUnitprice, #txtTotalAmount").val("0.00");
    $("#txtBarcode").val("");
    $("#alertBarcodeCount").hide();
    $("#alertBarcode").hide();
    $("#ddlProduct").attr('disabled', false);
    $("#ddlUnitType").attr('disabled', false);
}
function resetAfterAddStock() {
    $("#panelBill input[type='text']").val('');
    $("#panelBill textarea").val('');
    bindProduct();
    $("#tblProductDetail tbody tr").remove();
    $("#ddlProduct").val(0);
    $("#ddlVendor select").val(0);
    $("#txtBillNo").val("");
    $("#txtRemarks").val("");
    resetProductPanel();
    $("#emptyTable").show();
    $("#totalAmount").text("0.00")
    $("#btnUpdate").attr("disabled", true);
    $('#TotalQty').text('0.00');
    $('#TotalQtyNo').text('0.00');
    $('select').val('0');


    bindVendor();

}
function InvalidBarcode() {
    $("#txtBarcode").val("");
    $("#txtBarcode").focus();
}
/*---------------------------------------------------------------------------------------------------------------------------------------*/
function updatedraftOrder() {
    
    if (Number($("#TotalQtyNo").html()) > 0) {
        if ($("#DraftAutoId").val() != '') {
            var Draftdata = {
                VendorAutoId: $("#ddlVendor option:selected").val(),
                billNo: $("#txtBillNo").val().trim(),
                billDate: $("#txtBillDate").val(),
                remarks: $("#txtRemarks").val(),
            }
            $.ajax({
                type: "POST",
                url: "/Warehouse/WebAPI/WStockReceiverMaster.asmx/updateDraftOrder",
                data: JSON.stringify({ Draftdata: JSON.stringify(Draftdata) }),
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                beforeSend: function () {
                },
                complete: function () {
                },
                success: function (response) {

                    if (response.d == "Session Expired") {
                        location.href = '/';
                    }
                    else if (response.d == "false") {
                        swal("", "Oops, Something went wrong.", "error");
                    }
                },
                error: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                },
                failure: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                }
            });
        }
    }
}
//                                                        Barcode Reading
/*---------------------------------------------------------------------------------------------------------------------------------------*/
var Price = 0.00;
function readBarcode() {
    $("#alertBarcode").hide();
    $("#alertBarcode span").html("");
    var barcode = $("#txtBarcode").val();
    var DraftAutoId = $("#txtHBillAutoId").val();
    var vendorid = $("#ddlVendor").val();
    var billDate = $("#txtBillDate").val();
    var Remarks = $("#txtRemarks").val();
    if (barcode != "") {
        $.ajax({
            type: "POST",
            url: "/Warehouse/WebAPI/WStockReceiverMaster.asmx/getProductThruBarcode",
            data: "{'barcode':'" + barcode + "','DraftAutoId':'" + DraftAutoId + "','vendorid':'" + vendorid + "','billDate':'" + billDate + "','Remarks':'" + Remarks + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d == "inactive product") {
                    $("#txtBarcode").val('');
                    $("#txtBarcode").removeAttr('onchange', 'readBarcode()');
                    $("#txtBarcode").attr('onchange', 'InvalidBarcode()');
                    swal({
                        title: "",
                        text: "Inactive Product.",
                        icon: "error",
                        closeOnClickOutside: false
                    }).then(function (isConfirm) {

                        if (isConfirm) {
                            $("#txtBarcode").removeAttr('onchange', 'InvalidBarcode()');
                            $("#txtBarcode").val('');
                            $("#txtBarcode").focus();
                            $("#txtBarcode").attr('onchange', 'readBarcode()');
                        }
                    });

                    $("#txtBarcode").focus();
                }
               else if (response.d == "Invalid Barcode") {
                    $("#txtBarcode").removeAttr('onchange', 'readBarcode()');
                    $("#txtBarcode").attr('onchange', 'InvalidBarcode()');
                    swal({
                        title: "",
                        text: "Barcode does not exists.",
                        icon: "error",
                        closeOnClickOutside: false
                    }).then(function (isConfirm) {

                        if (isConfirm) {
                            $("#txtBarcode").removeAttr('onchange', 'InvalidBarcode()');
                            $("#txtBarcode").val('');
                            $("#txtBarcode").focus();
                            $("#txtBarcode").attr('onchange', 'readBarcode()');
                        }
                    });

                    $("#txtBarcode").focus();
                }
                else if (response.d != "Session Expired") {
                    var xmldoc = $.parseXML(response.d);
                    var product = $(xmldoc).find("Table");
                    var DraftAutoId = $(xmldoc).find("Table1");
                    $("#txtHBillAutoId").val($(DraftAutoId).find('DraftAutoId').text());
                    if (product.length > 0) {
                        var check = true, msgBar = 0;
                        $("#tblProductDetail tbody tr").each(function () {

                            if ($(this).find('.ProName span').attr('ProductAutoId') == $(product).find('ProductAutoId').text() &&
                                $(this).find('.Unit span').attr('UnitAutoId') == $(product).find('UnitAutoId').text()) {
                                check = false;
                                var qty = parseInt($(this).find('.Qty input').val()) || 0;
                                $(this).find('.Qty input').val((qty + 1));
                                $(this).find('.Pcs').html(((qty + 1) * parseInt(($(this).find('.Unit span').attr('qtyperunit')))));
                                $('#tblProductDetail tbody tr:first').before($(this));
                            }
                            else if ($(this).find('.ProName span').attr('ProductAutoId') == $(product).find('ProductAutoId').text() &&
                                $(this).find('.Unit span').attr('UnitAutoId') != $(product).find('UnitAutoId').text()) {
                                check = false;
                                msgBar = 1;
                            }
                        });
                        if (msgBar == 1) {
                            $("#txtBarcode").removeAttr('onchange', 'readBarcode()');
                            $("#txtBarcode").attr('onchange', 'InvalidBarcode()');
                            swal({
                                title: "Error!",
                                text: "You can't add different unit of added product.",
                                icon: "error",
                                closeOnClickOutside: false
                            }).then(function (isConfirm) {

                                if (isConfirm) {
                                    $("#txtBarcode").removeAttr('onchange', 'InvalidBarcode()');
                                    $("#txtBarcode").val('');
                                    $("#txtBarcode").focus();
                                    $("#txtBarcode").attr('onchange', 'readBarcode()');
                                }
                            });

                            $("#txtBarcode").focus();
                            return;
                        }
                        else {
                            if (check) {
                                var row = $("#tblProductDetail thead tr:last").clone(true);
                                $(".ProId", row).text($(product).find('ProductId').text());
                                $(".ProName", row).html("<span ProductAutoId='" + $(product).find('ProductAutoId').text() + "'>" + $(product).find('ProductName').text() + "</span>");
                                $(".Unit", row).html("<span UnitAutoId='" + $(product).find('UnitAutoId').text() + "' QtyPerUnit='" + $(product).find('Qty').text() + "'>" + $(product).find('UnitType').text() + ' (' + $(product).find('Qty').text() + " pcs)</span>");
                                $(".Qty", row).html("<input type='text' class='form-control input-sm text-center' maxlength='6' id='Qty' onchange='calPcs(this)' value='" +
                                    (($("#txtQuantity").val() != "" && $("#txtQuantity").val() != "0") ? $("#txtQuantity").val() : '1') + "' onfocus='this.select()' onkeypress='return isNumberKey(event)'>");
                                $(".Pcs", row).text((parseFloat((($("#txtQuantity").val() != "" && $("#txtQuantity").val() != "0") ? $("#txtQuantity").val() : '1')) * parseFloat($(product).find('Qty').text())).toFixed());
                                $(".Action", row).html("<a href='#' id='deleterow' onclick='deleterow(this)'><span class='ft-x'></span></a>");
                                if ($('#tblProductDetail tbody tr').length > 0) {
                                    $('#tblProductDetail tbody tr:first').before(row);
                                }
                                else {
                                    $('#tblProductDetail tbody').append(row);
                                }
                            }
                        }
                        var msg = false;
                    }
                    else {
                        msg = true
                        swal("", "Wrong Product.", "error");
                        $("#txtBarcode").val('');
                        $("#txtBarcode").focus();
                    }
                    $('#txtBarcode').val('');
                    var TotalQtyNo = 0; var TotalQty = 0;
                    $('#tblProductDetail tbody tr').each(function () {
                        if ($(this).find('.Qty input').val() != '') {
                            TotalQty += Number($(this).find('.Qty input').val());
                        }
                        TotalQtyNo += Number($(this).find('.Pcs').text());

                    });
                    if (msg == false) {
                        toastr.success('Item added successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                        $("#txtQuantity").val('1');
                    }
                    $('#TotalQty').text(TotalQty);
                    $('#TotalQtyNo').text(TotalQtyNo);
                    if ($('#tblProductDetail tbody tr').length > 0) {
                        $("#emptyTable").hide();
                    } else {
                        $("#emptyTable").show();
                    }
                }

                else {
                    location.href = "/";
                }
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    } else {
        $("select").val(0);
        $("#alertBarcodeCount").hide();
        $("#ddlProduct").attr('disabled', false);
        $("#ddlUnitType").attr('disabled', false);
    }
    $("#txtBarcode").focus();
}
var ItemAutoId;
function GetProductDetails() {
    $("#Massage").hide();
    $("#ProductDetails").show();
    $('#Button1').hide();
    $('#Button3').hide();
    $('#Button2').show();
    $('#Button4').show();
    $("#txtBasePrice").attr('readonly', true);
    $("#txtWholesaleMinPrice").attr('readonly', true);
    $("#txtRetailMIN").attr('readonly', true);
    $("#txtCOSTPRICE").attr('readonly', true);
    $("#txtSRP").attr('readonly', true);

    var ProductDetails = {
        ProductAutoId: $("#ddlProduct").val(),
        UnitAutoId: $("#ddlUnitType").val()
    }
    $.ajax({
        type: "Post",
        url: "/Warehouse/WebAPI/WStockReceiverMaster.asmx/GetProductDetails",
        data: JSON.stringify({ dataValues: JSON.stringify(ProductDetails) }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var product = $(xmldoc).find("Table");
            ItemAutoId = $(product).find('ItemAutoId').text();
            $("#txtproductId").val($(product).find('ProductId').text());
            $("#txtProductName").val($(product).find('ProductName').text());
            $("#txtUnitType").val($(product).find('UnitType').text());
            $("#txtBasePrice").val($(product).find('Price').text());
            $("#txtWholesaleMinPrice").val($(product).find('WHminPrice').text());
            $("#txtRetailMIN").val($(product).find('MinPrice').text());
            $("#txtCOSTPRICE").val($(product).find('CostPrice').text());
            $("#txtSRP").val($(product).find('SRP').text());
            $("#txtLocation").val($(product).find('Location').text());
            $("#txtCommissionCode").val($(product).find('CommCode').text());
            $("#txtPreDefinedBarcode").val($(product).find('CommCode').text());
            $("#txtPQty").val($(product).find('Qty').text());
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function updateBasePrice() {
    var ProductDetails = {
        ItemAutoId: ItemAutoId,
        RetailMIN: $("#txtRetailMIN").val(),
        CostPrice: $("#txtCOSTPRICE").val(),
        SRP: $("#txtSRP").val(),
        BasePrice: $("#txtBasePrice").val(),
        WholesaleMinPrice: $("#txtWholesaleMinPrice").val(),
    }
    $.ajax({
        type: "Post",
        url: "/Warehouse/WebAPI/WStockReceiverMaster.asmx/updateBasePrice",
        //data: "{'dataValues':'" + JSON.stringify(ProductDetails) + "'}",
        data: JSON.stringify({ dataValues: JSON.stringify(ProductDetails) }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == "true") {
                Price = $("#txtBasePrice").val();
                $('#UpdateMinPrice').modal('hide');
                swal("", "Record updated successfully.", "success");
            } else {
                swal("", "Oops! Something went wrong. Please try later.", "error");
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function NOPrice() {
    CHK = 1;
    $("#UpdateMinPrice").modal('hide');
}
function EditProductDetails() {
    $("#txtBasePrice").attr('readonly', false);
    $("#txtWholesaleMinPrice").attr('readonly', false);
    $("#txtRetailMIN").attr('readonly', false);
    $("#txtCOSTPRICE").attr('readonly', false);
    $("#txtSRP").attr('readonly', false);
    $('#Button1').show();
    $('#Button3').show();
    $('#Button2').hide();
    $('#Button4').hide();
}
function CancelProduct() {
    GetProductDetails();
}

function alphanumeric(e) {
    var letterNumber = /^[0-9a-zA-Z]+$/;
    if (!($(e).val().match(letterNumber))) {
        $(e).val('');
        toastr.error('Please enter a valid Bill No.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
}