﻿$(document).ready(function () { 
    bindVendor();
    bindStatus();
    $('#txtSFromBillDate').pickadate({
        min: new Date("01/01/2019"),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtSToBillDate').pickadate({
        min: new Date("01/01/2019"),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    var d = new Date();
    var month = d.getMonth() + 1;
    if (month.toString().length == 1) {
        month = '0' + month;
    }
    var day = d.getDate();
    if (day.toString().length == 1) {
        day = '0' + day;
    }
    var year = d.getFullYear();
    $("#txtSFromBillDate").val(month + '/' + day + '/' + year);
    $("#txtSToBillDate").val(month + '/' + day + '/' + year);
    getStockEntryList(1);

})

function setdatevalidation(val) {
    var fdate = $("#txtSFromBillDate").val();
    var tdate = $("#txtSToBillDate").val();
    if (val == 1) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtSToBillDate").val(fdate);
        }
    }
    else if (val == 2) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtSFromBillDate").val(tdate);
        }
    }
}
function Pagevalue(e) {
    getStockEntryList(parseInt($(e).attr("page")));
};

function bindVendor() {
    $.ajax({
        type: "POST",
        url: "/Admin/DropDownMaster.aspx/BindVendor",
        data: "{}",
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var getData = $.parseJSON(response.d);
            $("#ddlSVendor option:not(:first)").remove();
            $.each(getData[0].VendorList, function (index, item) {
                $("#ddlSVendor").append("<option value='" + item.AutoId + "'>" + item.VendorName + "</option>");
            });
            $("#ddlSVendor").select2();
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function bindStatus() {
    $.ajax({
        type: "POST",
        url: "/Admin/DropDownMaster.aspx/BindStatus",
        data: "{'StatusCategory':'inv'}",
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var getData = $.parseJSON(response.d);
            $("#ddlStatus option:not(:first)").remove();
            $.each(getData[0].StatustList, function (index, item) {
                $("#ddlStatus").append("<option value='" + item.AutoId + "'>" + item.StatusType + "</option>");
            });
            $("#ddlStatus").val('2');
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function getStockEntryList(PageIndex) {
    var data = {
        BillNo: $("#txtSBillNo").val().trim(),
        VendorAutoId: $("#ddlSVendor").val(),
        FromBillDate: $("#txtSFromBillDate").val(),
        ToBillDate: $("#txtSToBillDate").val(),
        PageIndex: PageIndex,
        PageSize: $("#ddlPageSize").val(),
        Status: $("#ddlStatus").val()
    };

    $.ajax({
        type: "POST",
        url: "/Warehouse/WebAPI/WStockReceiverMaster.asmx/InvgetStockEntryList",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async:false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == "Unauthorized") {
                location.href = "/";
            }
            else {
                var xmldoc = $.parseXML(response.d);
                var entryList = $(xmldoc).find("Table");

                $("#tblStockEntryList tbody tr").remove();
                var row = $("#tblStockEntryList thead tr").clone(true);
                var gTotal = 0.00;
                if (entryList.length > 0) {
                    $.each(entryList, function () {
                        $("#EmptyTable").hide();
                        $(".BillNo", row).html("<span BillAutoId='" + $(this).find("AutoId").text() + "'>" + $(this).find("BillNo").text() + "</span>");
                        $(".BillDate", row).text($(this).find("BillDate").text());
                        $(".Vendor", row).text($(this).find("VendorName").text());
                        $(".Products", row).text($(this).find("NoOfItems").text());
                        $(".Total", row).text($(this).find("TotalAmount").text());
                        $(".Remarks", row).text($(this).find("Remarks").text());
                        if ($(this).find("StatusType").text() == 'Complete') {
                            $(".Status", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("StatusType").text() + "</span>");
                        }
                        else if ($(this).find("StatusType").text() == 'Pending') {
                            $(".Status", row).html("<span class='badge badge badge-pill badge-info'>" + $(this).find("StatusType").text() + "</span>");
                        }
                        else {
                            $(".Status", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("StatusType").text() + "</span>");
                        }
                        if ($(this).find("StatusType").text() == 'Complete') {
                            $(".Action", row).html("<a href='/warehouse/InvstockEntry.aspx?BillAutoId=" + $(this).find("AutoId").text() + "'><span class='la la-edit' ></span></a>&nbsp;&nbsp; <a href='javascript:;' onclick='print_NewOrder(" + $(this).find("AutoId").text() + ")'><span class='icon-printer' title='Print'></span></a>");
                        }
                        else {
                            $(".Action", row).html("<a href='/warehouse/InvstockEntry.aspx?BillAutoId=" + $(this).find("AutoId").text() + "'><span class='la la-edit' ></span></a>&nbsp;&nbsp;<a title='Delete' href='#'><span class='ft-x' onclick='deleteStocks(\"" + $(this).find("AutoId").text() + "\")'/></a>&nbsp;&nbsp; <a href='javascript:;' onclick='print_NewOrder(" + $(this).find("AutoId").text() + ")'><span class='icon-printer' title='Print'></span></a>");
                        }
                        $("#tblStockEntryList tbody").append(row);
                        row = $("#tblStockEntryList tbody tr:last").clone(true);
                        gTotal = Number(gTotal) + Number($(this).find("TotalAmount").text());
                    });
                }
                else
                {
                    $("#EmptyTable").show();
                }
                $("#gTotal").text(Number(gTotal).toFixed(2));
            }
            var pager = $(xmldoc).find("Table1");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function print_NewOrder(num) {
    window.open("/Purchase/PurchaseOrderPrint.html?Id=" + num + "&type=" + 2, "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
}
$("#btnSearch").click(function () {
    getStockEntryList(1);
})

$("#ddlPageSize").change(function () {
    getStockEntryList(1);
})

function CreateTable(us) {
      row1 = "";
    var data = {
        BillNo: $("#txtSBillNo").val().trim(),
        VendorAutoId: $("#ddlSVendor").val(),
        FromBillDate: $("#txtSFromBillDate").val(),
        ToBillDate: $("#txtSToBillDate").val(),
        PageIndex: 1,
        PageSize: 0,
        Status: $("#ddlStatus").val()
    };
    $.ajax({
        type: "POST",
        url: "/Warehouse/WebAPI/WStockReceiverMaster.asmx/InvgetStockEntryList",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var totalAmountPrint = 0.00;
                    var xmldoc = $.parseXML(response.d);
                    var InvStockList = $(xmldoc).find("Table");
                    $("#PrintTable tbody tr").remove();
                    var row = $("#PrintTable thead tr").clone(true);
                    if (InvStockList.length > 0) {
                        $.each(InvStockList, function () {
                            $(".PBillNo", row).html("<span BillAutoId='" + $(this).find("AutoId").text() + "'>" + $(this).find("BillNo").text() + "</span>");
                            $(".PBillDate", row).text($(this).find("BillDate").text());
                            $(".PVendor", row).text($(this).find("VendorName").text());
                            $(".PProducts", row).text($(this).find("NoOfItems").text());
                            $(".PTotal", row).text($(this).find("TotalAmount").text());
                            $(".PRemarks", row).text($(this).find("Remarks").text());
                            if (us == 2) {
                                $(".PStatus", row).text($(this).find("StatusType").text());
                            }
                            else {
                                if ($(this).find("StatusType").text() == 'Complete') {
                                    $(".PStatus", row).html("<span style='color:white !Important' class='badge badge badge-pill badge-success white'>" + $(this).find("StatusType").text() + "</span>");
                                }
                                else if ($(this).find("StatusType").text() == 'Pending') {
                                    $(".PStatus", row).html("<span style='color:white !Important' class='badge badge badge-pill badge-info'>" + $(this).find("StatusType").text() + "</span>");
                                }
                                else {
                                    $(".PStatus", row).html("<span style='color:white !Important' class='badge badge badge-pill badge-danger'>" + $(this).find("StatusType").text() + "</span>");
                                }
                            }
                            $("#PrintTable tbody").append(row);
                            row = $("#PrintTable tbody tr:last").clone(true);
                            totalAmountPrint = totalAmountPrint + parseFloat($(this).find("TotalAmount").text());
                        });
                        $('#pTotal').html(parseFloat(totalAmountPrint).toFixed(2));
                    }
                    if (us == 1) {
                        var image = $("#imgName").val();
                        var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                        mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                        $("#PrintDate").text("Print Date :" + (new Date()).format("MM/dd/yyyy hh:mm tt"));
                        $("#PrintLogo").attr("src", "/Img/logo/" + image);
                        if ($("#txtSFromBillDate").val() != "") {
                            $("#DateRange").text("Date Range: " + $("#txtSFromBillDate").val() + " To " + $("#txtSToBillDate").val());
                        }
                        mywindow.document.write($(PrintTable1).clone().html());
                        mywindow.document.write('</body></html>');
                        setTimeout(function () {
                            mywindow.print();
                        }, 2000);
                    }
                    if (us == 2) {
                        $("#PrintTable").table2excel({
                            exclude: ".noExl",
                            name: "Excel Document Name",
                            filename: "Draft PO List By Vendor" + (new Date()).format("MM/dd/yyyy hh:mm tt"),
                            fileext: ".xls",
                            exclude_img: true,
                            exclude_links: true,
                            exclude_inputs: true
                        });
                    }
                }
            }
            else {
                location.href = "/";
            }
        }
    });
}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblStockEntryList tbody tr').length > 0) {
        CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
$("#btnExport").click(function () {
    if ($('#tblStockEntryList tbody tr').length > 0) {
        CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
});
