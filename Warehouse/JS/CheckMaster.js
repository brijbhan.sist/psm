﻿var check = false;                  //Global variable
$(document).ready(function () {
    $('.date').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    var dt = new Date().format("MM/dd/yyyy")
    $("#txtCheckDate").val(dt);
    $("#btnUpdate").hide();
    $("#btnCancel").hide();
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    $("#txtFromDate").val(month + '/' + day + '/' + year);
    $("#txtToDate").val(month + '/' + day + '/' + year);

})
function changet() {
    bindType();
    $("#ddlVendor").attr('disabled', false);
}
function SearchtypeBind() {
    bindTypeSearch();
}
function bindTypeSearch() {
    if ($('#ddlSearchType').val() == 1) {
        bindVendorSearch(1);
    } else if ($('#ddlSearchType').val() == 2) {
        bindVendorSearch(2);
    }
    else if ($('#ddlSearchType').val() == 0) {
        $("#ddlSVendor option").remove();
        $("#ddlSVendor").append('<option value="0" selected="selected">ALL</option>')
    }
}
function bindType() {
    if ($('#ddlType').val() == 1) {
        $('#typeName').text('Vendor Name');
        $('#lblAddress').text('Vendor Address');
        bindVendor(1);
    } else if ($('#ddlType').val() == 2) {
        $('#typeName').text('Employee Name');
        $('#lblAddress').text('Employee Address');
        bindVendor(2);
    } else if ($('#ddlType').val() == 0) {
        $('#typeName').text('Name');
        $('#lblAddress').text('Address');
        $("#ddlVendor option").remove();
        $("#ddlVendor").append('<option value="0" selected="selected">-Select Name-</option>')
        $("#txtVendorAddress").val('');
    }

}
function bindVendorSearch(type) {
    $.ajax({
        type: "POST",
        url: "/Warehouse/WebAPI/WCheckMaster.asmx/bindVendor",
        data: "{'Type':'" + type + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == "session") {
                location.href = "/";
            }
            else {
                var xmldoc = $.parseXML(response.d);
                var Vendor = $(xmldoc).find("Table");
                var typeCheck = $(xmldoc).find("Table1");
                $("#ddlSVendor option").remove();
                if ($(typeCheck).find("type").text() == "1") {
                    $("#ddlSVendor").append('<option value="0" selected="selected">All Vendor</option>')
                }
                else if ($(typeCheck).find("type").text() == "2")
                    $("#ddlSVendor").append('<option value="0" selected="selected">All Employee</option>')
                $.each(Vendor, function () {
                    $("#ddlSVendor").append($("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("VendorName").text().replace(/&quot;/g, "\'") + "</option>"));
                });
                $("#ddlSVendor").select2();
            }
        },
        failure: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        },
        error: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        }
    });
}
function bindVendor(type) {
    $.ajax({
        type: "POST",
        url: "/Warehouse/WebAPI/WCheckMaster.asmx/bindVendor",
        data: "{'Type':'" + type + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == "session") {
                location.href = "/";
            }
            else {
                var Vendor = "";
                $("#ddlVendor").empty();
                var xmldoc = $.parseXML(response.d);
                Vendor = $(xmldoc).find("Table");
                var Type = $("#ddlType").val();
                $("#txtVendorAddress").val('');
                $("#ddlVendor option").remove();

                if (Type == "1") {
                    $("#ddlVendor").append('<option value="0" selected="selected">-Select Vendor-</option>')
                }
                else {
                    $("#ddlVendor").append('<option value="0" selected="selected">-Select Employee-</option>')
                }
                $.each(Vendor, function () {
                    $("#ddlVendor").append($("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("VendorName").text().replace(/&quot;/g, "\'") + "</option>"));
                });
                $("#ddlVendor").select2();
            }
        },
        failure: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        },
        error: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        }
    });
}
function bindVendorAddress() {
    var VendorAutoId = $("#ddlVendor").val();
    var Type = $("#ddlType").val();
    $.ajax({
        type: "POST",
        url: "/Warehouse/WebAPI/WCheckMaster.asmx/bindVendorAddress",
        data: "{'VendorAutoId':'" + VendorAutoId + "','Type':'" + Type + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            debugger;
            if (response.d == "session") {
                location.href = "/";
            }
            else {
                var xmldoc = $.parseXML(response.d);
                var Vendor = $(xmldoc).find("Table");
                if (G_CheckAutoId == '') {
                    $('#txtVendorAddress').val($(Vendor).find('Address').text() + "\n" + $(Vendor).find('City').text()
                        + " " + $(Vendor).find('StateName').text() + " " + $(Vendor).find('Zipcode').text());
                }
                else {
                    G_CheckAutoId = '';
                }
            }
        },
        failure: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        },
        error: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        }
    });
}
$(".close").click(function () {
    $(".alert").hide();
});

/*------------------------------------------------------Insert Categroy------------------------------------------------------------*/
$("#btnSave").click(function () {
    var validatecheck = dynamicInputTypeSelect2('ddlreq');
    if (!validatecheck) {
        dynamicALL('req')
    } else {
        validatecheck = dynamicALL('req');
    }
    if (validatecheck) {
        if ($("#txtCheckAmount").val() > 0) {
            var data = {
                Type: $("#ddlType").val(),
                CheckId: $("#txtCheckId").val(),
                VendorAutoId: $("#ddlVendor").val(),
                VendorAddress: $("#txtVendorAddress").val(),
                CheckAmount: $("#txtCheckAmount").val(),
                CheckDate: $("#txtCheckDate").val(),
                Memo: $("#txtDescription").val()
            }
            $.ajax({
                type: "POST",
                url: "/Warehouse/WebAPI/WCheckMaster.asmx/insertCheck",
                data: "{'dataValue':'" + JSON.stringify(data) + "'}",
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (result) {

                    if (result.d == 'session') {
                        location.href = '/';
                    } else if (result.d == 'true') {
                        swal("", "Check details saved successfully.", "success");
                        if (check) {
                            getCheckDetail(1);
                        }
                        reset();
                    } else {
                        swal("Error!", result.d, "error");
                    }
                },
                error: function (result) {
                    swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                },
                failure: function (result) {
                    swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                }
            });
        }
        else {
            toastr.error('Please fill check amount.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        }
    } else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
});

$("#btnReset").click(function () {
    reset();
});
function Pagevalue(e) {
    getCheckDetail(parseInt($(e).attr("page")));
};
/*----------------------------------------------------List Categroy Details--------------------------------------------------------*/
function getCheckDetail(index) {
    var data = {
        CheckNo: $('#txtSCheckId').val().trim(),
        VendorAutoId: $('#ddlSVendor').val(),
        Type: $('#ddlSearchType').val(),
        FromDate: $('#txtFromDate').val(),
        ToDate: $('#txtToDate').val(),
        pageindex: index,
        pagesize: $("#ddlPageSize").val()
    };
    $.ajax({
        type: "POST",
        url: "/Warehouse/WebAPI/WCheckMaster.asmx/getCheckDetail",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: onSuccessOfgetCheckDetail,
        error: function (response) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        }
    });
}
function onSuccessOfgetCheckDetail(response) {
    if (response.d == 'session') {
        location.href = '/';
    } else {
        var xmldoc = $.parseXML(response.d);
        var category = $(xmldoc).find('Table1');

        if (category.length > 0) {
            $('#EmptyTable').hide();
            $('#tblCheckList tbody tr').remove();
            var row = $('#tblCheckList thead tr').clone(true);
            $.each(category, function () {
                $(".CheckNO", row).text($(this).find("CheckNO").text());
                $(".CheckDate", row).text($(this).find("CheckDate").text());
                $(".VendorName", row).text($(this).find("Name").text());
                $(".Type", row).text($(this).find("Type").text());
                $(".VendorAddress", row).text($(this).find("VendorAddress").text());
                $(".Memo", row).text($(this).find("Memo").text());
                $(".CheckAmount", row).text($(this).find("CheckAmount").text());
                $(".action", row).html("<a href='javascript:void(0)' onclick='PrintCheck(\"" + $(this).find("CheckAutoId").text() + "\")'><span class='ft-printer'></a>&nbsp;&nbsp;&nbsp<a href='#'><span class='ft-edit' onclick='editCheck(\"" + $(this).find("CheckAutoId").text() + "\")' /></a>&nbsp;&nbsp<a href='#'><span class='ft-x' onclick= 'deleterecord(\"" + $(this).find("CheckAutoId").text() + "\")' /></a>");
                $("#tblCheckList tbody").append(row);
                row = $("#tblCheckList tbody tr:last-child").clone(true);
            });
        }
        else {
            $('#tblCheckList tbody tr').remove();
            $('#EmptyTable').show();
        }
        var pagingdetails = $(xmldoc).find('Table');
    
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt($(pagingdetails).find("PageIndex").text()),
                PageSize: parseInt($(pagingdetails).find("PageSize").text()),
                RecordCount: parseInt($(pagingdetails).find("RecordCount").text()),
            });
        
    }
}

function deleterecord(CheckAutoId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete check details.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, Cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, Delete it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            deleteCheck(CheckAutoId)
        } else {
            //swal("", "Your check detail is safe.", "error");
        }
    })
}
/*----------------------------------------------------S33earch Engine----------------------------------------------------------*/
$("#btnSearch").click(function () {
    if (check == false) {
        SecurityPoP();
    } else {
        getCheckDetail(1);
    }
});

function SecurityPoP() {
    $("#SecurityEnabledVoid").modal('show');
    $("#txtSecurityVoid").val('');
    $("#txtSecurityVoid").focus();
}

function clickonSecurityVoid() {
    var data = {
        Security: $("#txtSecurityVoid").val()
    }
    $.ajax({
        type: "POST",
        url: "WebAPI/WCheckMaster.asmx/CheckSecurity",
        data: "{'datavalue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == 'SessionExpired') {
                location.href = '/';
            } else {
                if (check == false) {
                    var xmldoc = $.parseXML(response.d);
                    var orderList = $(xmldoc).find("Table");
                    if (orderList.length > 0) {
                        $.each(orderList, function () {
                            if ($("#txtSecurityVoid").val() == $(orderList).find("SecurityValue").text()) {
                                check = true
                                $("#SecurityEnabledVoid").modal('hide');
                                getCheckDetail(1);
                            }
                        });
                    }
                    else {
                        swal("Warning!", "Access denied.", "warning", {
                            button: "OK",
                        }).then(function () {
                            $("#txtSecurityVoid").val('');
                            $("#txtSecurityVoid").focus();
                        })
                    }
                }
                else {
                    $("#SecurityEnabledVoid").modal('hide');
                    getCheckDetail(1);
                }
            }
        },
        failure: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        },
        error: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        }
    });
}
/*----------------------------------------------------Edit Category Detail---------------------------------------------------*/
var G_CheckAutoId = '';
function editCheck(CheckAutoId) {
    G_CheckAutoId = CheckAutoId;
    $.ajax({
        type: "POST",
        url: "/Warehouse/WebAPI/WCheckMaster.asmx/editCheck",
        data: "{'CheckAutoId':'" + CheckAutoId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: onSuccessOfEdit,
        error: function (response) {
            swal("Error!", response.d, "error");
        }
    })
}
function onSuccessOfEdit(response) {
    var xmldoc = $.parseXML(response.d);
    var CheckDetails = $(xmldoc).find('Table');
    var Vendor = $(xmldoc).find('Table1');
    var typeCheck = $(xmldoc).find("Table2");
    $("#txtCheckId").val($(CheckDetails).find("CheckNO").text());
    $("#txtCheckAutoid").val($(CheckDetails).find("CheckAutoId").text());
    $("#ddlType").val($(CheckDetails).find("Type").text()).change();

    $("#ddlVendor").attr('disabled', false);
    $("#ddlVendor option").remove();
    $.each(Vendor, function () {
        $("#ddlVendor").append($("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("VendorName").text().replace(/&quot;/g, "\'") + "</option>"));
    });

    $("#ddlVendor").val($(CheckDetails).find("VendorAutoId").text()).change();
    $("#ddlVendor").select2();
    //$("#txtNumofWords").text($(CheckDetails).find("NumofWords").text());
    $("#txtVendorAddress").val($(CheckDetails).find("VendorAddress").text());
    $("#txtCheckAmount").val($(CheckDetails).find("CheckAmount").text());
    $("#txtCheckDate").val($(CheckDetails).find("CheckDate").text());
    $("#txtDescription").val($(CheckDetails).find("Memo").text());
    $("#btnSave").hide();
    $("#btnReset").hide();
    $("#btnUpdate").show();
    $("#btnCancel").show();


    var str1 = "", str = "***************************************************************************************************************************************************************************************************";
    str1 = $(CheckDetails).find("NumofWords").text() + str;
    if (str1.length > 195) {
        str1 = str1.toString().substr(0, 126);
    }
    $("#txtNumofWords").text(str1);


}
$("#btnCancel").click(function () {
    reset();
    $("#btnUpdate").hide();
    $("#btnCancel").hide();
    $("#btnSave").show();
    $("#btnReset").show();
});

/*----------------------------------------------------Update Category Detail--------------------------------------------------------*/

$("#btnUpdate").click(function () {
    //if()
    $('#ddlVendor').closest('div').find('.select2-selection--single').attr('style', 'border:1px solid #FF9149  !important');
    if (checkRequiredField()) {
        if ($("#txtCheckAmount").val() > 0) {
            var data = {
                Type: $("#ddlType").val(),
                CheckAutoId: $("#txtCheckAutoid").val(),
                CheckId: $("#txtCheckId").val(),
                VendorAutoId: $("#ddlVendor").val(),
                VendorAddress: $("#txtVendorAddress").val(),
                CheckAmount: $("#txtCheckAmount").val(),
                CheckDate: $("#txtCheckDate").val(),
                Memo: $("#txtDescription").val()
            }
            $.ajax({
                type: "POST",
                url: "/Warehouse/WebAPI/WCheckMaster.asmx/updateCheck",
                data: "{'dataValue':'" + JSON.stringify(data) + "'}",
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (result) {
                    if (result.d == 'session') {
                        location.href = '/';
                    } else if (result.d == 'true') {
                        swal("", "Check details updated successfully.", "success");
                        getCheckDetail(1);
                        reset();
                    } else {
                        swal("Error!", (result.d), "error");
                    }
                },
                error: function (result) {
                    swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                },
                failure: function (result) {
                    swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                }
            });
        }
        else {
            toastr.error('Please fill check amount.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        }
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
});

/*--------------------------------------------------------Delete Category ----------------------------------------------------------*/
function deleteCheck(CheckAutoId) {
    $.ajax({
        type: "POST",
        url: "/Warehouse/WebAPI/WCheckMaster.asmx/deleteCheck",
        data: "{'CheckAutoId':'" + CheckAutoId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $("#fade").hide();
        },
        success: function (result) {
            if (result.d == 'session') {
                location.href = '/';
            } else if (result.d == 'Success') {
                swal("", "Check detail deleted successfully.", "success");
                getCheckDetail(1);
                reset();
            } else {
                swal("Error!", result.d, "warning");
            }
        },
        error: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "warning");
            console.log(result);
        },
        failure: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        }
    })
}
/*-----------------------------------------------------------Reset------------------------------------------------------------------*/
function reset() {
    $("#btnUpdate").hide();
    $("#btnCancel").hide();
    $("#btnSave").show();
    $("#btnReset").show();
    $('input[type="text"]').val('');
    $('#ddlType').val(0).change();
    $("#ddlVendor option").remove()
    $('#typeName').text('Name');
    $('#txtNumofWords').text('');
    $('textarea').val('');
    $('input[type="text"]').removeClass('border-warning');
    $('textarea').removeClass('border-warning');
    $('#ddlVendor').closest('div').find('.select2-selection--single').removeAttr('style');
    $("#ddlVendor").append('<option value="0" selected="selected">-Select Name-</option>');
    var dt = new Date().format("MM/dd/yyyy");
    $("#txtCheckDate").val(dt);
}

function PrintCheck(CheckAutoId) {

    window.open("/Warehouse/PrintCheck.html?CheckAutoId=" + CheckAutoId, "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    $('#Popmassege').modal('hide');
}
function CheckPrint() {

    PrintCheck($('#lblCheckAutoId').val());
}
function CheckNo() {
    $('#Popmassege').modal('hide');
}
function getCheckAmount() {
    var CheckAmount = $("#txtCheckAmount").val();
    if (isNaN(CheckAmount)) {
        $("#txtCheckAmount").val('0.00');
        toastr.error('Only Numbers Allow.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
   
    if (CheckAmount == "") {
        $("#txtCheckAmount").val('0.00');
    }
    else {
        if (CheckAmount % 1 != 0) {
            CheckAmount = CheckAmount.toString();
            CheckAmount = CheckAmount.slice(0, (CheckAmount.indexOf(".")) + 3);
        }

        $.ajax({
            type: "POST",
            url: "/Warehouse/WebAPI/WCheckMaster.asmx/getCheckAmount",
            data: "{'CheckAmount':'" + CheckAmount + "'}",
            contentType: "application/json;charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: onSuccessOfgetCheckAmounts,
            error: function (response) {
                swal("Error!", "Oops! Something went wrong.Please try later.", "error");
            }
        });
    }
}
function onSuccessOfgetCheckAmounts(response) {
    if (response.d == 'session') {
        location.href = '/';
    } else {
        var str1 = "", str = "***************************************************************************************************************************************************************************************************";
        var xmldoc = $.parseXML(response.d);
        var CheckAmountinwords = $(xmldoc).find('Table');

        if (CheckAmountinwords.length > 0) {
            str1 = $(CheckAmountinwords).find("NumofWords").text() + str;
            if (str1.length > 195) {
                str1 = str1.toString().substr(0, 126);
            }
            $("#txtNumofWords").text(str1);
        }

    }
}

//$('input#txtCheckAmount').blur(function () {
//    var num = parseFloat($(this).val());
//    if (!isNaN(num)) {
//        var cleanNum = num.toFixed(2);
//        if (num / cleanNum < 1) {
//            swal("", "This field accepts a maximum of two (2) decimal places.", "warning");
//        }
//        if (num % 1 != 0) {
//            num = num.toString();
//            num = num.slice(0, (num.indexOf(".")) + 3);
//        }
//        $(this).val(num);
//    } else {
//        $(this).val('0.00');
//    }
//});

$('input#txtCheckAmount').on('keyup', function (e) {
    if (e.which === 46) return false;
}).on('input', function () {
    var self = this;
    setTimeout(function () {
        if (self.value.indexOf('.') != -1) self.value = parseInt(self.value, 10);
    }, 0);
});