﻿$(document).ready(function () {
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    var CheckAutoId = getQueryString('CheckAutoId');
    getCheckDetail(CheckAutoId);
})

function getCheckDetail(CheckAutoId) {

    $.ajax({
        type: "POST",
        url: "/Warehouse/WebAPI/WCheckMaster.asmx/getPrintDetails",
        data: "{'CheckAutoId':'" + CheckAutoId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        async: false,
        beforeSend: function () {
            $('#fade').show();
        },
        complete: function () {
            $('#fade').hide();
        },
        success: onSuccessOfgetCheckDetail,
        error: function (response) {
            console.log(JSON.parse(response.responseText).d);
        }
    });
}
function onSuccessOfgetCheckDetail(response) {
    var xmldoc = $.parseXML(response.d);
    var checkDetails = $(xmldoc).find('Table');
    var companydetails = $(xmldoc).find('Table1');
    $('#CheckNo').text($(checkDetails).find("CheckNO").text());
    $('#Span5').text($(checkDetails).find("NumofWords").text() + '.');
    $('#CheckDate').text($(checkDetails).find("CheckDate").text());
    $('#Span4').html("*"+$(checkDetails).find("CheckAmount").text());
    $('#Memo').html(" &nbsp;&nbsp;" + $(checkDetails).find("Memo").text());
    $('#VendorName').html($(checkDetails).find("VendorName").text());
    $('#VendorAddress').html($(checkDetails).find("VendorName").text()+"<br/>"+$(checkDetails).find("Address").text() + "<br>" + $(checkDetails).find("City").text() + ' '+$(checkDetails).find("Zipcode").text() +"<br>"+ $(checkDetails).find("State").text() );
    $('#companydetails').html($(companydetails).find("CompanyName").text());
    $('#Address').html($(companydetails).find("Address").text());
    $('#Phone').html($(companydetails).find("MobileNo").text());
    $('#FaxNo').html($(companydetails).find("FaxNo").text());
    $('#logo').attr("src", "../Img/logo/" + $(companydetails).find("Logo").text());
    window.print();
}