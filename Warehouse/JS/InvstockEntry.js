﻿var rowAutoId = 0,popCheck=0,prdt=0,unt=0;
$(document).ready(function () {
    bindVendor();
    bindProduct();
    $('#txtBillDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $("#ddlUnitType").attr('disabled', true);
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    var getid = getQueryString('BillAutoId');
    if (getid != null) {
        editSEntry(getid);
    }
    else {
        location.href = "/Warehouse/invstockEntryList.aspx";
    }
    popCheck = 1;
})
function editSEntry(BillAutoId) {
     
    $.ajax({
        type: "POST",
        async: false,
        url: "/Warehouse/WebAPI/WStockReceiverMaster.asmx/editStockEntry",
        data: "{'BillAutoId':" + BillAutoId + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            var xmldoc = $.parseXML(response.d);
            var stockEntry = $(xmldoc).find("Table");
            var billitems = $(xmldoc).find("Table1");
            var product = $(xmldoc).find("Table2");
            $("#ddlProduct option:not(:first)").remove();
            //$.each(product, function () {
            //    $("#ddlProduct").append($("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("ProductName").text().replace(/&quot;/g, "\'") + "</option>"));
            //});
            //$("#ddlProduct").select2();
            $("#btnUpdate").show();
            $("#btnGeneratePO").show();
            $("#btnAddStock").hide();
            $("#btnReset").hide();
            //$("#pnlInputProduct").hide();          
            $("#txtHBillAutoId").val($(stockEntry).find("AutoId").text())
            $("#txtStatus").val($(stockEntry).find("Status").text())
            $("#txtBillNo").val($(stockEntry).find("BillNo").text());
            $("#txtBillDate").val($(stockEntry).find("BillDate").text());
            $("#txtRemarks").val($(stockEntry).find("Remarks").text());
            $("#ddlVendor").val($(stockEntry).find("VendorAutoId").text()).change();
            $("#tblProductDetail tbody tr").remove();
            var row = $("#tblProductDetail thead tr").clone(true);

            if (billitems.length > 0) {
                $("#emptyTable").hide();
                $.each(billitems, function () {
                    $(".ProId", row).html("<span rowautoid='" + $(this).find("AutoId").text() + "'>" + $(this).find("ProductId").text() + "</span>");
                    $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "'>" + $(this).find("ProductName").text() + "</span>");
                    $(".Unit", row).html("<span UnitAutoId='" + $(this).find("UnitAutoId").text() + "' QtyPerUnit='" + $(this).find("QtyPerUnit").text() + "'>" + $(this).find("UnitType").text() + " ( " + $(this).find("QtyPerUnit").text() + " pcs" + " )</span>");
                    $(".Qty", row).html("<input type='text' maxlength='6' class='form-control input-sm border-primary text-center' id='Qty' onkeyup='calPcs(this)' value='" + $(this).find("Quantity").text() + "'>");
                    $(".Pcs", row).text($(this).find("TotalPieces").text());
                    $(".Price", row).html("<input type='text' class='form-control input-sm border-primary' style='text-align:right;' id='Price' onchange='geteditPrice(this)' onkeyup='calPcs(this)' BasePrice='" +
                        $(this).find("BasePrice").text() + "' Mainvalue='" + $(this).find("Price").text() + "' value='" +
                        $(this).find("Price").text() + "' onfocus='this.select()' onkeypress='return isNumberDecimalKey(event, this)' style='text - align: right'>");
                    $(".TotalPrice", row).text($(this).find("TotalPrice").text());
                    $(".Action", row).html("<a href='#' id='deleterow' onclick='deleterow(this)'><span class='ft-x'></span></a>");
                    $("#tblProductDetail tbody").append(row);
                    row = $("#tblProductDetail tbody tr:last").clone(true);
                });
                calGTotal();
                bindProduct();
                //if ($(stockEntry).find("Status").text() == "Complete" || $(stockEntry).find("Status").text() == "Revert") {
                //    $("#act").hide();
                //}
            }
            else {
                $("#emptyTable").show();
            }
            if ($(stockEntry).find("StatusCode").text() == 4 || $(stockEntry).find("StatusCode").text() == 3) {
                $("#btnUpdate").hide();
                $("#btnGeneratePO").hide();
                $("#pnlInputProduct").hide();
                $(".Action").hide();
                $('input').attr('disabled', true);
                $('select').attr('disabled', true);
                $('textarea').attr('disabled', true);

            }
            
            if ($(stockEntry).find("StatusCode").text()== "3") {
                $("#clspan").attr('colspan', "3");
            }
           else if ($(stockEntry).find("StatusCode").text() != "4") {
                $("#clspan").attr('colspan', "4");
            }
            $("#ddlVendor").attr('disabled', true);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
            //$("#alertDanger").show();
            //$("#alertDanger span").text(JSON.parse(result.responseText).d);
            //$(".close").click(function () {
            //    $("#alertDanger").hide();
            //});
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function bindVendor() {
    $.ajax({
        type: "POST",
        url: "/Admin/DropDownMaster.aspx/BindVendor",
        data: "{}",
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var getData = $.parseJSON(response.d);
            $("#ddlVendor option:not(:first)").remove();
            $.each(getData[0].VendorList, function (index, item) {
                $("#ddlVendor").append("<option value='" + item.AutoId + "'>" + item.VendorName + "</option>");
            });
            $("#ddlVendor").select2();
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function bindProduct() {
    $.ajax({
        type: "POST",
        url: "/Admin/DropDownMaster.aspx/BindProduct",
        data: "{}",
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            debugger;
            var getData = $.parseJSON(response.d);
            $("#ddlProduct option:not(:first)").remove();
            $.each(getData[0].ProductList, function (index, item) {
                $("#ddlProduct").append("<option value='" + item.AutoId + "'>" + item.ProductName + "</option>");
            });
            $("#ddlProduct").select2();
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function BindUnit() {

    $("#btnAdd").attr("disabled", false);
    var productAutoId = $("#ddlProduct option:selected").val();
    $.ajax({
        type: "POST",
        url: "/Warehouse/WebAPI/WStockReceiverMaster.asmx/bindUnitType",
        data: "{'productAutoId':" + productAutoId + "}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var unitType = $(xmldoc).find("Table");
            var PackingType = $(xmldoc).find("Table1");
            $("#ddlUnitType option:not(:first)").remove();
            $("#txtQuantity").val('1');
            $("#txtTotalPieces").val("0");
            $("#txtUnitprice").val('0.00');
            $("#txtTotalAmount").val('0.00');
            if (unitType.length > 0) {
                $.each(unitType, function () {
                    $("#ddlUnitType").append($("<option value='" + $(this).find("AutoId").text() + "' QtyPerUnit='" + $(this).find("Qty").text() + "'>" + $(this).find("UnitType").text() + " ( " + $(this).find("Qty").text() + " pcs" + " )</option>"));
                })
                $("#ddlUnitType").removeAttr('disabled');
                $("#ddlUnitType").val($(PackingType).find("PackingAutoId").text());
                $("#txtTotalPieces").val($("#ddlUnitType option:selected").attr('QtyPerUnit'));
                getBarcodeCount();
            } else {
                $("#ddlUnitType").val('0');
            }
           
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function getBarcodeCount() {
     
    $("#txtQuantity").val("1");
    $("#txtTotalPieces").val($("#ddlUnitType option:selected").attr('qtyperunit'));

    /*----------------------------------To Check Barcode Count---------------------------------*/

    var data = {
        ProductAutoId: $("#ddlProduct").val(),
        UnitAutoId: $("#ddlUnitType").val()
    };
    $.ajax({
        type: "Post",
        url: "/Warehouse/WebAPI/WStockReceiverMaster.asmx/countBarcode",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == "Unauthorized") {
                location.href = "/";
            }
            else {
                var xmldoc = $.parseXML(response.d);
                var barcode = $(xmldoc).find("Table");
                var ProductDetails = $(xmldoc).find("Table1");
                var BarcodeCount = $(barcode).find("BarcodeCount").text();
                Price = $(ProductDetails).find('Price').text();
                toastr.success('Barcode Count : ' + BarcodeCount, 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                $("#btnAdd").attr("disabled", false);
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function calTotal() {

    var QtyperUnit = $("#ddlUnitType option:selected").attr("QtyPerUnit");
    var Qty = $("#txtQuantity").val() || 0;

    $("#txtTotalPieces").val(Number(Qty) * Number(QtyperUnit));

    var unitPrice = $("#txtUnitprice").val() || 0;
    $("#txtTotalAmount").val((Number(Qty) * Number(unitPrice)).toFixed(2));
}

$("#txtQuantity").keyup(function () {
    if ($("#txtQuantity").val() == '') {
        $("#txtQuantity").val(0);
        $("#txtTotalPieces").val(0);
    }
    else {
        calTotal();
    }
});

$("#txtUnitprice").keyup(function () {
    calTotal();
});

function checkRequired() {
    var boolcheck = true;
    if ($("#txtUnitprice").val() == '' || $("#txtUnitprice").val() == '0') {
        boolcheck = false;
        $("#txtUnitprice").addClass('border-warning');
    } else {
        $("#txtUnitprice").removeClass('border-warning');
    }
    if ($("#txtQuantity").val() == '' || $("#txtQuantity").val() == '0.00' || $("#txtQuantity").val() == '0') {
        boolcheck = false;
        $("#txtQuantity").addClass('border-warning');
    } else {
        $("#txtQuantity").removeClass('border-warning');
    }
    if ($("#ddlUnitType").val() == '0' || $("#ddlUnitType").val() == '-Select-') {
        boolcheck = false;
        $("#ddlUnitType").addClass('border-warning');
    } else {
        $("#ddlUnitType").removeClass('border-warning');
    }
    if ($("#ddlProduct").val() == '0' || $("#ddlProduct").val() == '-Select-') {
        boolcheck = false;
        $("#ddlProduct").closest('div').find('.select2-selection--single').attr('style', 'border:1px solid #FF9149  !important');
    } else {
        $("#ddlProduct").closest('div').find('.select2-selection--single').removeAttr('style');
    }
    return boolcheck;
}
var CHK = 0;
/*----------------------------------Add Row to table---------------------------------*/
$("#btnAdd").click(function () {  

    if (Number($("#txtQuantity").val()) == 0) {
        toastr.error('Please enter a valid quantity.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $("#txtQuantity").addClass('border-warning');
        return;
    }
    else {
        $("#txtQuantity").removeClass('border-warning');
    }
    var Priceunit = $("#txtUnitprice").val();
    if (parseFloat(Priceunit) != parseFloat(Price) && CHK == 0) {
        $('#Massage').show();
        $('#ProductDetails').hide();
        GetProductDetails();
        $("#UpdateMinPrice").modal('show');
    } else {
        CHK = 0;
        if (checkRequired()) {
            var ProductAutoId = $("#ddlProduct option:selected").val();
            var UnitType = $("#ddlUnitType option:selected").val();
            var check = false, msg = 0;
            $("#tblProductDetail tbody tr").each(function () {
                if (Number(ProductAutoId) == Number($(this).find('.ProName span').attr('ProductAutoId')) && Number(UnitType) == Number($(this).find('.Unit span').attr('UnitAutoId'))) {
                    $(this).find('.Qty input').val(($(this).find('.Qty input').val() == '' ? '0' : (Number($(this).find('.Qty input').val()) + Number($("#txtQuantity").val()))));
                    $(this).find('.Price input').val(($(this).find('.Price input').val() == '' ? '0.00' : (parseFloat($("#txtUnitprice").val()).toFixed(2))));
                    $(this).find('.Price input').attr('Mainvalue', ($(this).find('.Price input').val() == '' ? '0.00' : (parseFloat($("#txtUnitprice").val()).toFixed(2))));
                    calGTotal();
                    check = true;
                }
                else if (Number(ProductAutoId) == Number($(this).find('.ProName span').attr('ProductAutoId')) && Number(UnitType) != Number($(this).find('.Unit span').attr('UnitAutoId'))) {
                    check = true;
                    msg = 1;
                }
            });
            if (msg == 1) {
                swal("", "You can't add different unit of added product.", "error");
            }
            else {
                $("#emptyTable").hide();
                if (check == false) {
                    var product = $("#ddlProduct option:selected").text().split("--");

                    var row = $("#tblProductDetail thead tr:last").clone(true);
                    var unitPrice = $("#txtUnitprice").val() || 0.00;

                    $(".ProId", row).text(product[0]);
                    $(".ProName", row).html("<span ProductAutoId='" + $("#ddlProduct option:selected").val() + "'>" + product[1] + "</span>");
                    $(".Unit", row).html("<span UnitAutoId='" + $("#ddlUnitType option:selected").val() + "' QtyPerUnit='" + $("#ddlUnitType option:selected").attr("QtyPerUnit") + "'>" + $("#ddlUnitType option:selected").text() + "</span>");
                    $(".Qty", row).html("<input type='text' maxlength='6' class='form-control input-sm border-primary text-center' id='Qty' onkeyup='calPcs(this)' value='" +
                        $("#txtQuantity").val() + "' onfocus='this.select()' onkeypress='return isNumberKey(event)'>");
                    $(".Pcs", row).text($("#txtTotalPieces").val());
                    $(".Price", row).html("<input type='text' class='form-control input-sm border-primary' Mainvalue='" + (Number(unitPrice).toFixed(2)) + "' BasePrice=" + Price + " style='text-align:right;' id='Price' onkeyup='calPcs(this)' value='" +
                        (Number(unitPrice).toFixed(2)) + "' onfocus='this.select()' onkeypress='return isNumberDecimalKey(event, this)' style='text - align: right'>");
                    $(".TotalPrice", row).text($("#txtTotalAmount").val());
                    $(".Action", row).html("<a href='#' id='deleterow' onclick='deleterow(this)'><span class='ft-x'></span></a>");
                    if ($('#tblProductDetail tbody tr').length > 0) {
                        $('#tblProductDetail tbody tr:first').before(row);
                    }
                    else {
                        $('#tblProductDetail tbody').append(row);
                    }
                    calGTotal();
                }
                $('#ddlProduct').val('0').change();
                $('#ddlUnitType').val('0');
                $("#txtQuantity").val('');
                $("#txtTotalPieces").val('');
                $("#txtTotalAmount").val('');
                $("#txtUnitprice").val('');
                $("#txtBarcode").focus();
            }
        }
        else {
            toastr.error('All * fields are mandatory', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        }
    }
});

function deleterow(e) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this.",
        allowOutsideClick: false,
        closeOnClickOutside: false,
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, Cancel.",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, Delete it.",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (willDelete) {
        if (willDelete) {
            $(e).closest('tr').remove();
            swal("","Product deleted successfully.", {
                icon: "success",
            });
            calGTotal();
        }
    })


    if ($("#tblProductDetail > tbody").children().length == 0) {
        $("#emptyTable").show();
    } else {
        $("#emptyTable").hide();
    }
};
/*----------------------------------Validation Generate PO----------------------------------------*/
function ValidationGeneratePO() {
    var boolcheck = true;
    if ($("#txtBillNo").val() == '') {
        boolcheck = false;
        $("#txtBillNo").addClass('border-warning');
    } else {
        $("#txtBillNo").removeClass('border-warning');

        if ($("#txtBillDate").val() == '') {
            boolcheck = false;
            $("#txtBillDate").addClass('border-warning');
        } else {
            $("#txtBillDate").removeClass('border-warning');
        }
        if ($("#ddlVendor").val() == '0' || $("#ddlVendor").val() == '-Select-') {
            boolcheck = false;
            $("#ddlVendor").closest('div').find('.select2-selection--single').attr('style', 'border:1px solid #FF9149  !important');
        } else {
            $("#ddlVendor").closest('div').find('.select2-selection--single').removeAttr('style');
        }
        return boolcheck;
    }
}
function calGTotal() {
    var TotalQtyNo = 0; var TotalQty = 0; var total = 0;
    $('#tblProductDetail tbody tr').each(function () {
        if ($(this).find('.Qty input').val() != '') {
            TotalQty += Number($(this).find('.Qty input').val());
        }
        TotalQtyNo += Number($(this).find('.Pcs').text());
        total += Number($(this).find('.TotalPrice').text());
    });
    $('#TotalQty').text(TotalQty);
    $('#TotalQtyNo').text(TotalQtyNo);
    $('#total').text(parseFloat(total).toFixed(2));
}
/*----------------------------------Add Stock----------------------------------------*/
$("#btnUpdate").click(function () {
    var checkVal = 0;    
    $("#tblProductDetail tbody tr").each(function (i) {
        if (Number($(this).find(".Qty input").val()) == 0) {
            checkVal = Number(checkVal) + 1;
            $(this).find(".Qty input").addClass('border-warning');
        }
    });
    if (checkVal>0) {
        toastr.error("Please fill quantity.Its can't be zero", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    if (popCheck == 1) {
        swal({
            title: "Are you sure?",
            text: "You want to generate this P.O.",
            icon: "warning",
            showCancelButton: true,
            allowOutsideClick: false,
            closeOnClickOutside: false,
            buttons: {
                cancel: {
                    text: "No, Cancel.",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: true,
                },
                confirm: {
                    text: "Yes, Generate it.",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
                }
            }
        }).then(function (GeneratePO) {
            if (GeneratePO) {
                if (ValidationGeneratePO()) {
                    UpdateOrder(4)
                }
                else {
                    toastr.error('All * fields are mandatory', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                }
            }
        })
    }
    else {
        getProductPrice(prdt, unt);
        $("#UpdateMinPrice").modal('show');
    }
});
function calPcs(e) {
    var row = $(e).closest("tr"), pcs;
    var Qty = Number(row.find('.Qty input').val()) || 0;
    var Price = row.find('.Price input').val() || 0;
    var pcs = Number(Qty) * Number(row.find(".Unit span").attr("QtyPerUnit"));
    row.find(".Pcs").text(pcs);
    var TotalPrice = (Number(Price) * Number(Qty)).toFixed(2);
    row.find(".TotalPrice").text(TotalPrice);
    calGTotal();
}
function geteditPrice(e) {
    var tr = $(e).closest('tr');
    var BaseAmount = $(e).attr('BasePrice');
    var mainvalue = $(e).attr('mainvalue');
    var UnitPrice = $(e).val() || 0.00;
    var ProductAutoId = $(tr).find('.ProName span').attr('ProductAutoId');
    var UnitAutoId = $(tr).find('.Unit span').attr('UnitAutoId');
    rowAutoId = $(tr).find('.ProId span').attr('rowautoid');
    prdt = ProductAutoId;
    unt = UnitAutoId;
    if (parseFloat(UnitPrice) != parseFloat(mainvalue)) {// && CHK == 0) {
        $("#Massage").hide();
        $("#ProductDetails").show();
        $('#Button1').hide();
        $('#Button3').hide();
        $('#Button2').show();

        $('#Button4').show();
        $("#txtBasePrice").attr('readonly', true);
        $("#txtWholesaleMinPrice").attr('readonly', true);
        $("#txtRetailMIN").attr('readonly', true);
        $("#txtCOSTPRICE").attr('readonly', true);
        getProductPrice(ProductAutoId, UnitAutoId);
        $("#UpdateMinPrice").modal('show');
        popCheck = 1;
    }
}
/*----------------------------------Update Stock Entry----------------------------------------*/
$("#btnGeneratePO").click(function () { 
    var checkVal = 0;
    $("#tblProductDetail tbody tr").each(function (i) {
        if (Number($(this).find(".Qty input").val()) == 0) {
            checkVal = Number(checkVal) + 1;
            $(this).find(".Qty input").addClass('border-warning');
        }
    });
    if (checkVal>0) {
        toastr.error("Please fill quantity.Its can't be zero", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    swal({
        title: "Are you sure.",
        text: "You want to revert P.O.",      
        icon: "warning",
        showCancelButton: true,
        allowOutsideClick: false,
        closeOnClickOutside: false,
        buttons: {
            cancel: {
                text: "No, Cancel.",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, Revert it.",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (OK) {  
        if (OK) {          
            UpdateOrder(3);
        }
    })
})
function UpdateOrder(Status) {
    var Product = [];
    $("#tblProductDetail tbody tr").each(function () {
        Product.push({
            ProductAutoId: $(this).find('.ProName span').attr('ProductAutoId'),
            UnitAutoId: $(this).find('.Unit span').attr('UnitAutoId'),
            QtyPerUnit: $(this).find('.Unit span').attr('QtyPerUnit'),
            Quantity: $(this).find('.Qty input').val(),
            TotalPieces: $(this).find('.Pcs').text(),
            Price: $(this).find('.Price input').val(),
        });
    });
    var billData = {
        VendorAutoId: $("#ddlVendor option:selected").val(),
        billAutoId: $("#txtHBillAutoId").val(),
        billNo: $("#txtBillNo").val(),
        billDate: $("#txtBillDate").val(),
        remarks: $("#txtRemarks").val(),
        Status: Status
    };

    $.ajax({
        type: "Post",
        url: "/Warehouse/WebAPI/WStockReceiverMaster.asmx/updateStockEntry",
        data: "{'TableValues':'" + JSON.stringify(Product) + "','billData':'" + JSON.stringify(billData) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (data) {
            if (data.d == "true") {
                if (Status == 3) {
                    swal({
                        title: "",
                        text: "PO has been revert successfully.",
                        icon: "success",
                        button: "OK",
                    }).then(function () {
                        location.reload(true);
                    })
                }
                else {
                    swal({
                        title: "",
                        text: "Po has been generated successfully.",
                        icon: "success",
                        button: "OK",
                    }).then(function () {
                        location.reload(true);
                    })
                }
            }
            else if (data.d == "Unauthorized")
            {
                location.href = "/";
            }
            else {
                swal("", data.d, "error");
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
$("#btnReset").click(function () {
    resetAfterAddStock();
})
function resetProductPanel() {
    $("#panelProduct select").val(0);
    $("#txtQuantity, #txtTotalPieces").val("0");
    $("#txtUnitprice, #txtTotalAmount").val("0.00");
    $("#txtBarcode").val("");
    $("#alertBarcodeCount").hide();
    $("#alertBarcode").hide();
    $("#ddlProduct").attr('disabled', false);
    $("#ddlUnitType").attr('disabled', false);
}
function resetAfterAddStock() {
    $("#panelBill input[type='text']").val('');
    $("#panelBill textarea").val('');
    bindProduct();
    $("#tblProductDetail tbody").remove();
    $("#ddlProduct").val(0);
    resetProductPanel();
    $("#emptyTable").show();
    $("#totalAmount").text("0.00")
    $("#btnUpdate").attr("disabled", true);
    $('#TotalQty').text('0.00');
    $('#TotalQtyNo').text('0.00');
}
/*---------------------------------------------------------------------------------------------------------------------------------------*/
//                                                        Barcode Reading
/*---------------------------------------------------------------------------------------------------------------------------------------*/
var Price = 0.00;
function readBarcode() { 
    $("#alertBarcode").hide();
    $("#alertBarcode span").html("");
    var barcode = $("#txtBarcode").val();
    if (barcode != "") {
         var datavalues = {
            Barcode: barcode,
            DraftAutoId: $("#txtHBillAutoId").val()
        };
        $.ajax({
            type: "POST",
            url: "/Warehouse/INVstockEntry.aspx/getProductThruBarcode",
            data: JSON.stringify({ pobj: datavalues }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                var xmldoc = $.parseXML(response.d);
                var product = $(xmldoc).find("Table");
                if (product.length > 0) {
                    var check = true,msgbar=0;
                    $("#tblProductDetail tbody tr").each(function () {

                        if ($(this).find('.ProName span').attr('ProductAutoId') == $(product).find('ProductAutoId').text() && $(this).find('.Unit span').attr('UnitAutoId') == $(product).find('UnitAutoId').text()) {
                            check = false;
                            var qty = parseInt($(this).find('.Qty input').val()) || 0;
                            $(this).find('.Qty input').val((qty + 1));
                            $(this).find('.Pcs').html(((qty + 1) * parseInt(($(this).find('.Unit span').attr('qtyperunit')))));
                            $(this).find(".TotalPrice", row).html(((qty + 1) * parseFloat($(product).find('CostPrice').text())).toFixed(2));

                            $('#tblProductDetail tbody tr:first').before($(this));
                        }
                        else if ($(this).find('.ProName span').attr('ProductAutoId')== $(product).find('ProductAutoId').text() && $(this).find('.Unit span').attr('UnitAutoId') != $(product).find('UnitAutoId').text()) {
                            check = false;
                            msgbar=1
                        }
                    });
                    if (msgbar == 1) {
                        swal("", "You can't add different unit of added product.", "error");
                        return;
                    }
                    else {
                        if (check) {
                            var row = $("#tblProductDetail thead tr:last").clone(true);
                            $(".ProId", row).text($(product).find('ProductId').text());
                            $(".ProName", row).html("<span ProductAutoId='" + $(product).find('ProductAutoId').text() + "'>" + $(product).find('ProductName').text() + "</span>");
                            $(".Unit", row).html("<span UnitAutoId='" + $(product).find('UnitAutoId').text() + "' QtyPerUnit='" + $(product).find('Qty').text() + "'>" + $(product).find('UnitType').text() + ' ( ' + $(product).find('Qty').text() + " pcs )</span>");
                            $(".Qty", row).html("<input type='text' maxlength='6' class='form-control input-sm border-primary text-center'  id='Qty' onkeyup='calPcs(this)' value='" +
                                (($("#txtQuantity").val() != "" && $("#txtQuantity").val() != "0") ? $("#txtQuantity").val() : '1') + "' onfocus='this.select()' onkeypress='return isNumberKey(event)'>");
                            $(".Pcs", row).text((parseFloat((($("#txtQuantity").val() != "" && $("#txtQuantity").val() != "0") ? $("#txtQuantity").val() : '1')) * parseFloat($(product).find('Qty').text())).toFixed());
                            $(".Price", row).html("<input type='text' class='form-control input-sm border-primary'   style='text-align:right;' id='Price'  value='" + $(product).find('CostPrice').text() + "' onkeyup='calPcs(this)' onfocus='this.select()' onkeypress='return isNumberDecimalKey(event, this)' style='text - align: right'>");
                            $(".TotalPrice", row).html((parseFloat((($("#txtQuantity").val() != "" && $("#txtQuantity").val() != "0") ? $("#txtQuantity").val() : '1')) * parseFloat($(product).find('CostPrice').text())).toFixed(2));
                            $(".Action", row).html("<a href='#' id='deleterow' onclick='deleterow(this)'><span class='ft-x'></span></a>");
                            if ($('#tblProductDetail tbody tr').length > 0) {
                                $('#tblProductDetail tbody tr:first').before(row);
                            }
                            else {
                                $('#tblProductDetail tbody').append(row);
                            }
                        }
                    }
                } else {
                    swal("", "Invalid product.", "error");
                }
                $('#txtBarcode').val('');
                var TotalQtyNo = 0; var TotalQty = 0;
                $('#tblProductDetail tbody tr').each(function () {
                    if ($(this).find('.Qty input').val() != '') {
                        TotalQty += Number($(this).find('.Qty input').val());
                    }
                    TotalQtyNo += Number($(this).find('.Pcs').text());
                });
                $('#TotalQty').text(TotalQty);
                $('#TotalQtyNo').text(TotalQtyNo);
                if ($('#tblProductDetail tbody tr').length > 0) {
                    $("#emptyTable").hide();
                } else {
                    $("#emptyTable").show();
                }
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    } else {
        $("select").val(0);
        $("#alertBarcodeCount").hide();
        $("#ddlProduct").attr('disabled', false);
        $("#ddlUnitType").attr('disabled', false);
    }
    $("#txtBarcode").focus();
}
var ItemAutoId;
function GetProductDetails() {
    $("#Massage").hide();
    $("#ProductDetails").show();
    $('#Button1').hide();
    $('#Button3').hide();
    $('#Button2').show();
    $('#Button4').show();
    $("#txtBasePrice").attr('readonly', true);
    $("#txtWholesaleMinPrice").attr('readonly', true);
    $("#txtRetailMIN").attr('readonly', true);
    $("#txtCOSTPRICE").attr('readonly', true);
    $("#txtSRP").attr('readonly', true);
    var ProductAutoId = $("#ddlProduct").val();
    var UnitAutoId = $("#ddlUnitType").val();
    getProductPrice(ProductAutoId, UnitAutoId)
}
function getProductPrice(ProductAutoId, UnitAutoId) {
    var ProductDetails = {
        ProductAutoId: ProductAutoId,
        UnitAutoId: UnitAutoId
    }
    $.ajax({
        type: "Post",
        url: "/Warehouse/WebAPI/WStockReceiverMaster.asmx/GetProductDetails",
        data: "{'dataValues':'" + JSON.stringify(ProductDetails) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var product = $(xmldoc).find("Table");
            ItemAutoId = $(product).find('ItemAutoId').text();
            $("#txtproductId").val($(product).find('ProductId').text());
            $("#txtProductName").val($(product).find('ProductName').text());
            $("#txtUnitType").val($(product).find('UnitType').text());
            $("#txtBasePrice").val($(product).find('Price').text());
            $("#txtWholesaleMinPrice").val($(product).find('WHminPrice').text());
            $("#txtRetailMIN").val($(product).find('MinPrice').text());
            $("#txtCOSTPRICE").val($(product).find('CostPrice').text());
            $("#txtSRP").val($(product).find('SRP').text());
            $("#txtLocation").val($(product).find('Location').text());
            $("#txtCommissionCode").val($(product).find('CommCode').text());
            $("#txtPreDefinedBarcode").val($(product).find('CommCode').text());
            $("#txtPQty").val($(product).find('Qty').text());
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function updateBasePrice() {

    if (Number($("#txtCOSTPRICE").val()) > Number($('#txtWholesaleMinPrice').val())) {
        swal("Warning", "Cost price should be less or equal to Wholesale min price.", "warning")
        return;
    }
    if (Number($("#txtWholesaleMinPrice").val()) > Number($('#txtRetailMIN').val())) {
        swal("", "Whole sale min price should be less or equal to retail min price.", "warning")
        return;
    }
    if (Number($('#txtRetailMIN').val()) > Number($("#txtBasePrice").val())) {
        swal("Warning", "Base price should be greater  or equal to retail min price.", "warning")
        return;
    }
    if ($("#txtSRP").val() == 0 || $("#txtSRP").val() == "") {
        swal("Warning", "SRP should be greater than zero.", "warning")
        return;
    }
    var peicebaseprice = parseFloat(Number($("#txtBasePrice").val()) / Number($("#txtPQty").val())).toFixed(2);
    if (Number(peicebaseprice) > Number($('#txtSRP').val())) {
        swal("Warning", "SRP should be greater  or equal to Base price.", "warning")
        return;
    }
    var ProductDetails = {
        ItemAutoId: ItemAutoId,
        RetailMIN: $("#txtRetailMIN").val(),
        CostPrice: $("#txtCOSTPRICE").val(),
        SRP: $("#txtSRP").val(),
        BasePrice: $("#txtBasePrice").val(),
        WholesaleMinPrice: $("#txtWholesaleMinPrice").val(),
    }
    $.ajax({
        type: "Post",
        url: "/Warehouse/WebAPI/WStockReceiverMaster.asmx/updateBasePrice",
        data: "{'dataValues':'" + JSON.stringify(ProductDetails) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == 'Session Expired') { location.href = "/"; }
            if (response.d == "true") {
                Price = $("#txtBasePrice").val();
                $('#UpdateMinPrice').modal('hide');
                swal("", "Item updated successfully.", "success")
            } else if (response.d == 'false') {
                swal("", "Oops! Something went wrong.Please try later.", "error")
            }
            else if (response.d == "Unauthorized") {
                location.href = "/";
            }
            else {
                var xmldoc = $.parseXML(response.d);
                var PricelevelList = $(xmldoc).find("Table");
                var html = "<div class='table-responsive'><table class='table table-striped table-bordered'><thead><tr><td>Product Id</td><td>Product Name</td><td>Unit</td><td>Price Level Name</td>";
                html += "<td>Custom Price</td></tr></thead><tbody>";
                $(PricelevelList).each(function () {
                    html += "<tr><td>" + $(this).find('ProductId').text() + "</td><td>" + $(this).find('ProductName').text() + "</td>";
                    html += "<td>" + $(this).find('UnitType').text() + "</td><td>" + $(this).find('PriceLevelName').text() + "</td>";
                    html += "<td>" + $(this).find('CustomPrice').text() + "</td>";
                    html += "</tr>";
                });
                html += "</tbody><tfoot><tr><td colspan='5' style='text-align: left;'><b>You can not update cost price becuase custom price should not be less than cost price so please update price level .</b></td></tr></tfoot></table>";
                $("#Sbarcodemsg").html(html);
                $("#msgPop").modal('show');
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function NOPrice() {
    $("#tblProductDetail tbody tr").each(function (o, itm) {
        if (rowAutoId == $(this).find('.ProId span').attr('rowautoid')) {
            $(this).find('.Price input').removeAttr('onchange');
        }
    })    
    CHK = 1;
    $("#UpdateMinPrice").modal('hide');
}
function EditProductDetails() {
    $("#txtBasePrice").attr('readonly', false);
    $("#txtWholesaleMinPrice").attr('readonly', false);
    $("#txtRetailMIN").attr('readonly', false);
    $("#txtCOSTPRICE").attr('readonly', false);
    $("#txtSRP").attr('readonly', false);
    $('#Button1').show();
    $('#Button3').show();
    $('#Button2').hide();
    $('#Button4').hide();

}
function CancelProduct() {
    GetProductDetails();
    $('#UpdateMinPrice').modal('hide');
}

