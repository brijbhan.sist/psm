﻿$(document).ready(function () {

    bindDropdown();
    $('#FromDate').pickadate({
        min: new Date("01/01/2019"),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#ToDate').pickadate({
        min: new Date("01/01/2019"),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#SettledFromDate').pickadate({
        min: new Date("01/01/2019"),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#SettledToDate').pickadate({
        min: new Date("01/01/2019"),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
     
    $("#ddlPageSize").val("10");
})
function setdatevalidation(val) {
    var fdate = $("#FromDate").val();
    var tdate = $("#ToDate").val();
    var cfdate = $("#SettledFromDate").val();
    var ctdate = $("#SettledToDate").val();
    if (val == 1) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#ToDate").val(fdate);
        }
    }
    else if (val == 2) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#FromDate").val(tdate);
        }
    }
    else if (val == 3) {
        if (cfdate == '' || new Date(cfdate) > new Date(ctdate)) {
            $("#SettledToDate").val(cfdate);
        }
    }
    else if (val == 4) {
        if (cfdate == '' || new Date(cfdate) > new Date(ctdate)) {
            $("#SettledFromDate").val(ctdate);
        }
    }
}
function bindDropdown() {
    $.ajax({
        type: "POST",
        url: "PaymentLog.aspx/bindDropdown",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            if (response.d != "Session Expired") {
                var getData = $.parseJSON(response.d);
                $("#ddlCustomer option:not(:first)").remove();
                $.each(getData[0].AllCust, function (index, item) {
                    $("#ddlCustomer").append("<option value='" + item.CUID + "'>" + item.CUN + "</option>");
                });
                $("#ddlCustomer").select2();
                
                $("#ddlCollectionBy option:not(:first)").remove();
                $.each(getData[0].AllEmp, function (index, item) {
                    $("#ddlCollectionBy").append("<option value='" + item.EId + "'>" + item.EMN + "</option>");
                });
                $("#ddlCollectionBy").select2();

                
                $("#ddlStatus option:not(:first)").remove();
                $.each(getData[0].AllSt, function (index, item) {
                    $("#ddlStatus").append("<option value='" + item.AutoId + "'>" + item.StatusType + "</option>");
                });
                $("#ddlStatus").select2();

               
                $("#ddlPaymentMode option:not(:first)").remove();
                $.each(getData[0].PMode, function (index, item) {
                    $("#ddlPaymentMode").append("<option value='" + item.AutoID + "'>" + item.PaymentMode + "</option>");
                });
                $("#ddlPaymentMode").select2();
            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(result.d);
        },
        error: function (result) {
            console.log(result.d);
        }
    });
}
function Pagevalue(e) {
    getPaymentList(parseInt($(e).attr("page")));
};


function getPaymentList(pageindex) {

    var data = {
        CustomerAutoId: $("#ddlCustomer").val(),
        CollectionBy: $("#ddlCollectionBy").val(),
        FromDate: $("#FromDate").val(),
        ToDate: $("#ToDate").val(),
        SettledFromDate: $("#SettledFromDate").val(),
        SettledToDate: $("#SettledToDate").val(),
        PayId: $("#txtPaymentid").val().trim(),
        Status: $("#ddlStatus").val(),
        PageIndex: pageindex,
        PageSize: $('#ddlPageSize').val(),
        Range: $('#ddlRange').val(),
        PaymentAmount: $('#txtPaymentAmt').val(),
        PaymentMode: $('#ddlPaymentMode').val(),
    }
    $.ajax({
        type: "POST",
        url: "PaymentLog.aspx/getPaymentReport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var customerOrderList = $(xmldoc).find('Table1');
                var OrderTotal = $(xmldoc).find("Table2");
                var ReceiveAmount = 0;
                $("#tblCollectionDetails tbody tr").remove();
                var row = $("#tblCollectionDetails thead tr").clone(true);
                var TotalAmount = 0.00;
                $("#EmptyTable").hide();
                if (customerOrderList.length > 0) {
                    $.each(customerOrderList, function (index) {
                        $(".PayId", row).html($(this).find("PayId").text());
                        $(".ReceiveDate", row).html($(this).find("ReceivedDate").text() + "<span PaymentAutoId=" + $(this).find('PaymentAutoId').text() + "></span>");
                        $(".PaymentType", row).html($(this).find("PayType").text() + "<span PayCode=" + $(this).find('PayCode').text() + "></span>");
                        $(".ReceivedBy", row).html($(this).find("ReceivedBy").text() + "<span ProcessStatus=" + $(this).find('ProcessStatus').text() + "></span>");
                        $(".PaymentMode", row).html($(this).find("PaymentMode").text() + "<span PaymentMode=" + $(this).find('Pmode').text() + "></span>");
                        $(".ReceivedAmount", row).text($(this).find("ReceivedAmount").text());
                        $(".SettledDate", row).text($(this).find("SettledDate").text());
                        TotalAmount += parseFloat($(this).find("ReceivedAmount").text());
                        $(".CustomerName", row).text($(this).find("CustomerName").text());
                        $(".ReferenceId", row).html($(this).find("ReferenceId").text() + "<span StatusCode=" + $(this).find('StatusCode').text() + "></span>");
                        $(".Remarks", row).html($(this).find("Remarks").text());
                        $(".ChequeNo", row).html($(this).find("ChequeNo").text());
                        $(".ChequeDate", row).html($(this).find("ChequeDate").text());
                        $(".CancelRemarks", row).html($(this).find("CancelRemarks").text());
                        if (Number($(this).find("StatusCode").text()) == 1) {
                            $(".Status", row).html("<span class='badge badge badge-pill badge-info'>" + $(this).find("Status").text() + "</span>");
                        } else if (Number($(this).find("StatusCode").text()) == 2) {
                            $(".Status", row).html("<span class='badge badge badge-pill badge-primary'>" + $(this).find("Status").text() + "</span>");
                        } else if (Number($(this).find("StatusCode").text()) == 3) {
                            $(".Status", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("Status").text() + "</span>");
                        } else if (Number($(this).find("StatusCode").text()) == 4) {
                            $(".Status", row).html("<span class='badge badge badge-pill badge-warning'>" + $(this).find("Status").text() + "</span>");
                        } else if (Number($(this).find("StatusCode").text()) == 5) {
                            $(".Status", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("Status").text() + "</span>");
                        }
                        if ($(this).find('Pmode').text() == 2) {
                            if ($(this).find('StatusCode').text() == 2) {
                                $('.cancelled', row).html("<span PaymentAutoId=" + $(this).find("PaymentAutoId").text() + " ChequeNo=" + $(this).find('ChequeNo').text() + " ChequeDate=" + $(this).find('ChequeDate').text() + "></span><a href='javascript:void(0)' onclick='CancelPayment(this)'>Cancel Payment</a>");
                            } else {
                                $('.cancelled', row).html('');
                            }
                        } else {
                            $('.cancelled', row).html('');
                        }
                        var action = '';
                        if (Number($(this).find("StatusCode").text()) == 1)
                        {
                            action = "<a href='javascript:;'><span  onclick='editCustomer(\"" + $(this).find("customerAutoId").text() + "\",\"" + $(this).find("PayId").text() + "\")' class='ft-edit'></span></a>";
                        } else {
                            action = "<a href='javascript:;'><span  onclick='editCustomer(\"" + $(this).find("customerAutoId").text() + "\",\"" + $(this).find("PayId").text() + "\")' class='la la-eye'></span></a>"; 
                        }
                        $(".Action", row).html(action);

                        $("#tblCollectionDetails tbody").append(row);
                        row = $("#tblCollectionDetails tbody tr:last").clone(true);

                        ReceiveAmount = ReceiveAmount + parseFloat($(this).find("ReceivedAmount").text());
                    });
                    $('#TotalAmount').html(ReceiveAmount.toFixed(2));
                }
                else
                {
                    //$("#EmptyTable").show();
                    $('#TotalAmount').html('0.00');
                    $('#TotalAmounts').html('0.00');
                }

                var pager = $(xmldoc).find("Table");
                $(".Pager").ASPSnippets_Pager({
                    ActiveCssClass: "current",
                    PagerCssClass: "pager",
                    PageIndex: parseInt(pager.find("PageIndex").text()),
                    PageSize: parseInt(pager.find("PageSize").text()),
                    RecordCount: parseInt(pager.find("RecordCount").text())
                });

                if (Number($("#ddlPageSize").val()) == '0') {
                    $("#trTotal").hide();
                }
                else if (Number(pager.find("RecordCount").text()) < Number(pager.find("PageSize").text())) {
                    $("#trTotal").hide();
                }
                else {
                    $("#trTotal").show();
                }

                $(pager).each(function () {
                    $('#TotalAmounts').html(parseFloat($(this).find('ReceivedAmount').text()).toFixed(2));
                });
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function editCustomer(CustomerId,PaymentId) {
    window.location.href = "/Sales/ViewCustomerDetails.aspx?PageId=" + CustomerId + "&PaymentId=" + PaymentId;
}

function CreateTable(us) { 
    var image = $("#imgName").val();
    var data = {
        CustomerAutoId: $("#ddlCustomer").val(),
        CollectionBy: $("#ddlCollectionBy").val(),
        FromDate: $("#FromDate").val(),
        ToDate: $("#ToDate").val(),
        SettledFromDate: $("#SettledFromDate").val(),
        SettledToDate: $("#SettledToDate").val(),
        PayId: $("#txtPaymentid").val().trim(),
        Status: $("#ddlStatus").val(),
        PageIndex: 1,
        PageSize: 0,
        Range: $('#ddlRange').val(),
        PaymentAmount: $('#txtPaymentAmt').val(),
        PaymentMode: $('#ddlPaymentMode').val(),
    }
    $.ajax({
        type: "POST",
        url: "PaymentLog.aspx/getPaymentReport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var customerOrderList = $(xmldoc).find('Table1');
                    var PrintDate = $(xmldoc).find("Table");
                    var ReceiveAmount = 0;
                    $("#PrintTable tbody tr").remove();
                    var row = $("#PrintTable thead tr").clone(true);
                    var TotalAmount = 0.00;
                    if (customerOrderList.length > 0) {
                        $.each(customerOrderList, function () {
                            $(".P_PayId", row).html($(this).find("PayId").text());
                            $(".P_ReceiveDate", row).html($(this).find("ReceivedDate").text() + "<span PaymentAutoId=" + $(this).find('PaymentAutoId').text() + "></span>");
                            $(".P_PaymentType", row).html($(this).find("PayType").text() + "<span PayCode=" + $(this).find('PayCode').text() + "></span>");
                            $(".P_ReceivedBy", row).html($(this).find("ReceivedBy").text() + "<span ProcessStatus=" + $(this).find('ProcessStatus').text() + "></span>");
                            $(".P_PaymentMode", row).html($(this).find("PaymentMode").text() + "<span PaymentMode=" + $(this).find('Pmode').text() + "></span>");
                            $(".P_ReceivedAmount", row).text($(this).find("ReceivedAmount").text());
                            $(".P_SettledDate", row).text($(this).find("SettledDate").text());
                            TotalAmount += parseFloat($(this).find("ReceivedAmount").text());
                            $(".P_CustomerName", row).text($(this).find("CustomerName").text());
                            $(".P_ReferenceId", row).html($(this).find("ReferenceId").text() + "<span StatusCode=" + $(this).find('StatusCode').text() + "></span>");
                            $(".P_Remarks", row).html($(this).find("Remarks").text());
                            $(".P_ChequeNo", row).html($(this).find("ChequeNo").text());
                            $(".P_ChequeDate", row).html($(this).find("ChequeDate").text());                          
                            $(".P_Status", row).html($(this).find("Status").text());
                            $(".P_CancelRemarks", row).html($(this).find("CancelRemarks").text());
                            
                            $("#PrintTable tbody").append(row);
                            row = $("#PrintTable tbody tr:last").clone(true);
                            ReceiveAmount = ReceiveAmount + parseFloat($(this).find("ReceivedAmount").text());
                        });
                        $('#TotalAmounta').html(ReceiveAmount.toFixed(2));
                    }
                    
                    if (us == 1) {
                        var image = $("#imgName").val();
                        var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                        mywindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                        $("#PrintDate").text("Print Date :" + (PrintDate.find('PrintDate').text()));
                        $("#PrintLogo").attr("src", "/Img/logo/" + image);
                        if ($("#SettledFromDate").val() != "") {
                            $("#DateRange").text("Date Range: " + $("#SettledFromDate").val() + " To " + $("#SettledToDate").val());
                        }
                        if ($("#FromDate").val() != "") {
                            $("#DateRange").text("Date Range: " + $("#FromDate").val() + " To " + $("#ToDate").val());
                        }
                        mywindow.document.write($(PrintTable1).clone().html());
                        mywindow.document.write('</body></html>');
                        setTimeout(function () {
                            mywindow.print();
                        }, 2000);

                    }
                    if (us == 2) {
                        $("#PrintTable").table2excel({
                            exclude: ".noExl",
                            name: "Excel Document Name",
                            filename: "By Payment History",
                            fileext: ".xls",
                            exclude_img: true,
                            exclude_links: true,
                            exclude_inputs: true
                        });
                    }
                   
                }
            }
            else {
                location.href = "/";
            }
        }

    });
}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblCollectionDetails tbody tr').length > 0) {
    CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
function ExportReport() {
    if ($('#tblCollectionDetails tbody tr').length > 0) {
    CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}