﻿$(document).ready(function () {  
    if ($("#txtEmpTypeNo").val() == 8) {
        $("#dashboard").show();
    }
})
function LoadDashboard() {
    $('#fade').show();
   
    getOrderdetails();
    getSalesRevenue();
    CollectionDetails();
    CreditMemoDetails(); 

}
function getOrderdetails() {


    $.ajax({
        type: "POST",
        url: "/admin/WebAPI/WDashBoardMaster.asmx/getDashboardList",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "false") {
                var xmldoc = $.parseXML(response.d);

                var TodaysOverview = $(xmldoc).find("Table");
                $("#TotalOrders").html(parseInt($(TodaysOverview).find('TotalOrder').text()));
                $("#ThresholdValue").html(parseInt($(TodaysOverview).find('ThresholdValue').text()));
                $("#OutOfStock").html(parseInt($(TodaysOverview).find('OutOfStock').text()));
                $("#TotalSales").html('<span>$</span> ' + parseFloat($(TodaysOverview).find('TotalSales').text()).toFixed(2));
                var orderList = $(xmldoc).find("Table1");
                var salesPersonOrderDetails = $(xmldoc).find("Table2");


                $("#tableOrder tbody tr").remove();
                var row = $("#tableOrder thead tr:last-child").clone(true);

                if (orderList.length > 0) {
                    $.each(orderList, function () {
                        $(".StatusType", row).text($(this).find("StatusType").text());
                        $(".TotalOrder", row).text($(this).find("TotalOrder").text());

                        $("#tableOrder tbody").append(row);
                        row = $("#tableOrder tbody tr:last").clone(true);
                    });
                    $("#tableOrder tbody tr").show();
                }
                 
               
            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function getSalesRevenue() {
    $.ajax({
        type: "POST",
        url: "/admin/WebAPI/WDashBoardMaster.asmx/getSalesRevenue",
        data: "{'Type':'" + $("#ddlSalesRevenue").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "false") {
                var xmldoc = $.parseXML(response.d);
                var salesPersonOrderDetails = $(xmldoc).find("Table");
                $("#tableSalesRevenue tbody tr").remove();
                var row = $("#tableSalesRevenue thead tr:last-child").clone(true);
                var TotalOrder = 0, TotalSales = 0, AOV = 0;
                if (salesPersonOrderDetails.length > 0) {
                    $.each(salesPersonOrderDetails, function () {
                        $(".SalesPerson", row).text($(this).find("SalesPerson").text());
                        $(".TotalOrder", row).text($(this).find("TotalOrder").text());
                        TotalOrder += parseInt($(this).find("TotalOrder").text());
                        $(".TotalSales", row).text($(this).find("TotalSales").text());
                        TotalSales += parseFloat($(this).find("TotalSales").text());
                        $(".AOV", row).text($(this).find("AOV").text());
                        AOV += parseFloat($(this).find("AOV").text());
                        $("#tableSalesRevenue tbody").append(row);
                        row = $("#tableSalesRevenue tbody tr:last").clone(true);
                    });
                   
                    $("#tableSalesRevenue tbody tr").show();
                }
                $("#tableSalesRevenue tfoot tr").find('td:eq(1)').html(TotalOrder);
                $("#tableSalesRevenue tfoot tr").find('td:eq(2)').html(TotalSales.toFixed(2));
                $("#tableSalesRevenue tfoot tr").find('td:eq(3)').html(AOV.toFixed(2));
            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function CollectionDetails() {
    $.ajax({
        type: "POST",
        url: "/admin/WebAPI/WDashBoardMaster.asmx/CollectionDetails",
        data: "{'Type':'" + $("#ddlCollection").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "false") {
                var xmldoc = $.parseXML(response.d);
                var CollectionDetails = $(xmldoc).find("Table");
                $("#tableCollectionDetails tbody tr").remove();
                var row = $("#tableCollectionDetails thead tr:last-child").clone(true);
                if (CollectionDetails.length > 0) {
                    $.each(CollectionDetails, function () {
                        $(".StatusType", row).text($(this).find("StatusType").text());
                        $(".totalCount", row).text($(this).find("totalCount").text());
                        $(".ReceivedAmount", row).text($(this).find("ReceivedAmount").text());
                        $(".ReceivedAmount", row).css('text-align', 'right');
                        $("#tableCollectionDetails tbody").append(row);
                        row = $("#tableCollectionDetails tbody tr:last").clone(true);
                    });
                    $("#tableCollectionDetails tbody tr").show();
                }
            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function CreditMemoDetails() {
    $.ajax({
        type: "POST",
        url: "/admin/WebAPI/WDashBoardMaster.asmx/CreditMemoDetails",
        data: "{'Type':'" + $("#ddlCreditMemo").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "false") {
                var xmldoc = $.parseXML(response.d);
                var CreditMaster = $(xmldoc).find("Table");
                $("#tableCreditMaster tbody tr").remove();
                var row = $("#tableCreditMaster thead tr:last-child").clone(true);
                if (CreditMaster.length > 0) {
                    $.each(CreditMaster, function () {
                        $(".StatusType", row).text($(this).find("StatusType").text());
                        $(".totalCount", row).text($(this).find("totalCount").text());
                        $(".ReceivedAmount", row).text($(this).find("ReceivedAmount").text());
                        $(".ReceivedAmount", row).css('text-align', 'right');
                        $("#tableCreditMaster tbody").append(row);
                        row = $("#tableCreditMaster tbody tr:last").clone(true);
                    });
                    $("#tableCreditMaster tbody tr").show();
                }
            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}