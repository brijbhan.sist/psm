﻿$(document).ready(function () {
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    getid = getQueryString('PId');
    getPaymentList(getid);
})
function getPaymentList(PaymentAutoId) {
    $.ajax({
        type: "POST",
        async:false,
        url: "/Warehouse/WebAPI/WPaymentLogReport.asmx/getReceivedPrint",
        data: "{'PaymentAutoId':'" + PaymentAutoId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {           
        },
        complete: function () {
          
        },
        success: function (response) {
             
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var xml = $(xmldoc);
                var Receipt = xml.find("Table1");
                var Company = xml.find("Table");
               // var ReceiptDetails = xml.find("Table2");
                var OrderMaster = xml.find("Table2");

               // $("#lblBeing").html($(ReceiptDetails).find("receiptbeing").text());
                $("#lblDate").html($(Receipt).find("ReceivedDate").text());
                $("#lblInvoiceNo").html($(Receipt).find("PayId").text());
                var receivedAmount = $(Receipt).find("ReceivedAmount").text();
                var amount = receivedAmount.split('.');
                $("#DhsAmount").html(amount[0]);
                $("#filsAmount").html(amount[1]);
                $("#lblCustomerName").html($(Receipt).find("CustomerName").text());
                $("#lblTotalRepuee").html($(Receipt).find("amountinword").text());

                $("#lblCash").text($(Receipt).find("PaymentMode").text()+  ':');
                $("#lblcashcheaq").html($(Receipt).find("PaymentReference").text());

                //if ($(Receipt).find("PaymentMode").text() == 1) {
                //    $("#lblCash").text('By Cash');
                //    $("#lblBank").hide();
                //}
                //else if ($(Receipt).find("PaymentMode").text() == 3) {
                //    $("#lblCash").text('By Credit No :');
                //    $("#lblBank").hide();
                //    $("#lblcashcheaq").html($(Receipt).find("TransactionNo").text())
                //}
                //else if ($(Receipt).find("PaymentMode").text() == 2) {
                //    $("#lblCash").html(' <label>By Cheque No : </label>  ' + $(Receipt).find("ChequeNo").text());
                //} else {
                //    $("#lblCash").text('By MONEY ORDER :');
                //    $("#lblcashcheaq").html($(Receipt).find("ReferenceId").text());
                //}
                $("#lblMobileNo").html($(Company).find("MobileNo").text());
                $("#ReceivedBy").html($(Company).find("Empname").text());
                $("#OfficeMobile").html($(Company).find("FaxNo").text());
                $("#OfficeAddress").html($(Company).find("Address").text());
                $("#OfficeEmail").html($(Company).find("EmailAddress").text());
                $("#lblCompanyname").html($(Company).find("CompanyName").text());
                $("#img").attr('src', '/Img/logo/' + $(Company).find("Logo").text());

                $("#tblduePayment tbody tr").remove();
                var row = $("#tblduePayment thead tr").clone(true);
                if (OrderMaster.length > 0) {
                    var Td1 = 0.00;
                    $.each(OrderMaster, function (index) {
                        $(".SrNo", row).text(Number(index) + 1);
                        $(".OrderDate", row).text($(this).find("OrderDate").text());
                        $(".OrderType", row).text($(this).find("OrderType").text());
                        $(".OrderNo", row).text($(this).find("OrderNo").text());
                        $(".Amount", row).html($(this).find("ReceivedAmount").text());
                        Td1 += parseFloat($(this).find("ReceivedAmount").text());
                        $("#tblduePayment tbody").append(row);
                        row = $("#tblduePayment tbody tr:last").clone(true);
                    });
                   // $("#Td1").text(Td1.toFixed(2));
                }

                 window.print();

            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

