﻿$(document).ready(function () {
    $('#txtAddTime').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    }); 
    $('#txtfromDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtToDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtAddTime1').pickatime({
        interval: 5
    });
    SalesPrsonBind();   
})

function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}


$("#btnAddBalance").click(function () { 
    $("#txtNewAvlBlc").val("0.00");
    $("#txtAddBlc").val("0.00");
    $("#txtAddRemark").val('');
    $("#txtAddTime").val(new Date().toLocaleDateString());
    $("#txtAddTime1").val(formatAMPM(new Date));
    $("#txtAddTime").attr('disabled', false);
    $("#txtAddTime1").attr('disabled', false);
    $("#btnAddUpdate").hide();
    $("#btnAdd").show();
    $("#alertDanger").hide();
    $("#modalAddBlc").modal('show');
    $("#ddlSalePerson").val('0').change();
})

function SalesPrsonBind() {
    $.ajax({
        type: "POST",
        url: "WebAPI/WendOfday.asmx/BindSalesPerson",        
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },

        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var GetCategory = $(xmldoc).find("Table");

            $("#ddlSalesPerson option:not(:first)").remove();
            $.each(GetCategory, function () {
                $("#ddlSalesPerson").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("SalesPerson").text() + "</option>");
            });
            $("#ddlSalesPerson").select2()

            $("#ddlSalePerson option:not(:first)").remove();
            $.each(GetCategory, function () {
                $("#ddlSalePerson").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("SalesPerson").text() + "</option>");
            });
            $("#ddlSalePerson").select2()
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
};

$("#btnSearch").click(function () {
    getRecord(1);
});
$("#ddlPageSize").change(function () {
    getRecord(1);
})
function Pagevalue(e) {
    getRecord(parseInt($(e).attr("page")));
};

function getRecord (index) {
    var data = {
        SalesPersonAutoId: $("#ddlSalesPerson").val(),
        FromDate: $("#txtfromDate").val(),
        ToDate: $("#txtToDate").val(),
        PageSize: $("#ddlPageSize").val(),
        Index: index
    }
    $.ajax({
        type: "POST",
        url: "WebAPI/WendOfday.asmx/GetList",
        data: "{'datavalue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == 'true') {
                location.href = '/';
            } else {

                var xmldoc = $.parseXML(response.d);
                var PattyCashList = $(xmldoc).find("Table");
               
                $('#tblEndOfDayReport tbody tr').remove();
                var row = $('#tblEndOfDayReport thead tr').clone(true);
                if (PattyCashList.length > 0) {
                    $('#EmptyTable').hide();
                    $.each(PattyCashList, function () {                     
                            $(".SalesPerson", row).html($(this).find("SalesPerson").text());
                            $(".Date", row).html($(this).find("EndDate").text());
                            $(".EndTime", row).html($(this).find("EndTime").text());
                         if ($(this).find("TotalOrder").text() != '0') {
                            $(".Status", row).html("<a href='#' class='ft-printer' onclick='printOrder(" + $(this).find("AutoID").text() + ")'> " + $(this).find("TotalOrder").text() + " Sales Orders</a> ");
                         }        
                        
                            $("#tblEndOfDayReport tbody").append(row);
                            row = $("#tblEndOfDayReport tbody tr:last-child").clone(true);                                       
                    });
                }
                else {

                    $("#EmptyTable").show();
                }
                    var pager = $(xmldoc).find("Table1");
                    $(".Pager").ASPSnippets_Pager({
                        ActiveCssClass: "current",
                        PagerCssClass: "pager",
                        PageIndex: parseInt(pager.find("PageIndex").text()),
                        PageSize: parseInt(pager.find("PageSize").text()),
                        RecordCount: parseInt(pager.find("RecordCount").text())
                    });
               
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function printOrder(Autoid)
{
    window.open("/Warehouse/PrintAllOrder.html?PageId=" + Autoid, "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
}

function checkRequiredFieldEndOfDay ()
{
    var boolcheck = true;
    $('.req').each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == '0.00') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });
   
    $('.ddlreq').each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == 'undefined' || $(this).val() == null || $(this).val() == 'Select') {
            boolcheck = false;
            $(this).closest('div').find('.select2-selection--single').attr('style', 'border:1px solid #FF9149  !important');
        } else {
            $(this).removeClass('border-warning');
            $(this).closest('div').find('.select2-selection--single').removeAttr('style');
        }
    });
    return boolcheck;
}

$("#btnAdd").click(function () {
    if (checkRequiredFieldEndOfDay()) {
        var data = {
            SalesPersonAutoId: $("#ddlSalePerson").val(),
            EndDate: $("#txtAddTime").val(),
            DateTime: $("#txtAddTime1").val()
        }
        $.ajax({
            type: "POST",
            url: "WebAPI/WendOfday.asmx/InsertRecord",
            data: "{'datavalue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d == 'true') {
                    location.href = '/';
                } else {
                    swal("", "Data Saved Successfully !!", "success")
                    $("#ddlSalesPerson").val('0').change();
                    $("#modalAddBlc").modal('hide');
                    getRecord(1);

                }
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
    else
    {
        toastr.error('All * field are mandatory', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

    }
})