﻿var getQueryString = function (field, url) {
    var href = url ? url : window.location.href;
    var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
    var string = reg.exec(href);
    return string ? string[1] : null;
};
var CustomerAutoId = 0, CollectionBy = 0, FromDate = '', ToDate = '', Status = '', PayId = '';
$(document).ready(function () {
    if (getQueryString('CustomerAutoId') != null) {
        CustomerAutoId = getQueryString('CustomerAutoId');
    }
    if (getQueryString('CollectionBy') != null) {
        CollectionBy = getQueryString('CollectionBy');
    }
    if (getQueryString('FromDate') != null) {
        FromDate = getQueryString('FromDate');
    }
    if (getQueryString('ToDate') != null) {
        ToDate = getQueryString('ToDate');
    }
    if (getQueryString('Status') != null) {
        Status = getQueryString('Status');
    }
    if (getQueryString('PayId') != null) {
        PayId = getQueryString('PayId');
    }
    PrintReport();
})

function PrintReport() {

    var html = '<table class="table table-striped table-bordered" id="tblCollectionDetails">';
    html += '<thead>';
    html += '<tr>';
    html += '<td class="PayId text-center">Pay Id</td>';
    html += '<td class="CustomerName text-center">Customer Name</td>';
    html += '<td class="ReceiveDate text-center">Receive Date</td>';
    html += '<td class="Status text-center">Status</td>';
    html += '<td class="PaymentType text-center">Payment Type</td>';
    html += '<td class="ReceivedBy text-center">Collect By</td>';
    html += '<td class="ReceivedAmount text-center">Receive Amount</td>';
    html += '<td class="PaymentMode text-center">Payment Mode</td>';
    html += '<td class="ReferenceId text-center">Reference Id</td>';
    html += '<td class="ChequeNo text-center">Cheque No</td>';
    html += '<td class="ChequeDate text-center">Cheque Date</td>';
    html += '<td class="Remarks text-center">Remarks</td>';
    html += '</tr></thead> <tbody>';
    var data = {
        CustomerAutoId: CustomerAutoId,
        CollectionBy: CollectionBy,
        FromDate: FromDate,
        ToDate: ToDate,
        SettledFromDate: '',
        SettledToDate: '',
        Status: Status,
        PayId: PayId,
        PageIndex: 1,
        PageSize: 0
    }
    $.ajax({
        type: "POST",
        url: "/Warehouse/WebAPI/WPaymentLogReport.asmx/getPaymentReport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $('#fade').show();
        },
        complete: function () {
            $('#fade').hide();
        },
        success: function (response) {
            debugger;            
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var customerOrderList = $(xmldoc).find('Table1');
                    var TotalAmount = 0.00;
                    if (customerOrderList.length > 0) {
                        $.each(customerOrderList, function (index) {
                            html += "<tr>";
                            html += "<td class='text-center'>" + $(this).find("PayId").text() + "</td>";
                            html += "<td>" + $(this).find("CustomerName").text() + "</td>";
                            html += "<td class='text-center'>" + $(this).find("ReceivedDate").text() + "</td>";
                            if (Number($(this).find("StatusCode").text()) == 1) {
                                html += "<td class='text-center'><span class='label label-info'>" + $(this).find("Status").text() + "</span></td>";
                            } else if (Number($(this).find("StatusCode").text()) == 2) {
                                html += "<td class='text-center'><span class='label label-primary'>" + $(this).find("Status").text() + "</span></td>";
                            } else if (Number($(this).find("StatusCode").text()) == 3) {
                                html += "<td class='text-center'><span class='StatusPacked'>" + $(this).find("Status").text() + "</span></td>";
                            } else if (Number($(this).find("StatusCode").text()) == 4) {
                                html += "<td class='text-center'><span class='label label-warning'>" + $(this).find("Status").text() + "</span></td>";
                            } else if (Number($(this).find("StatusCode").text()) == 5) {
                                html += "<td class='text-center'><span class='label label-danger'>" + $(this).find("Status").text() + "</span></td>";
                            }
                            html += "<td class='text-center'>" + $(this).find("PayType").text() + "</td>";
                            html += "<td>" + $(this).find("ReceivedBy").text() + "</td>";
                            html += "<td class='text-right'>" + $(this).find("ReceivedAmount").text() + "</td>";
                            html += "<td class='text-center'>" + $(this).find("PaymentMode").text() + "</td>";
                            TotalAmount += parseFloat($(this).find("ReceivedAmount").text());
                            html += "<td>" + $(this).find("ReferenceId").text() + "</td>";
                            html += "<td class='text-center'>" + $(this).find("ChequeNo").text() + "</td>";
                            html += "<td class='text-center'>" + $(this).find("ChequeDate").text() + "</td>";
                            html += "<td>" + $(this).find("Remarks").text() + "</td>";
                        });
                    }

                    html += '</tbody>';
                    html += '<tfoot>';
                    html += '<tr><td colspan="6">Total</td>';
                    html += '<td id="TotalAmount">' + parseFloat(TotalAmount).toFixed(2) + '</td>';
                    html += '<td colspan="5"></td></tr>';
                    html += '</tfoot>';
                    html += '</table>';
                    var CompanyDetails = $(xmldoc).find('Table2');
                    var html1 = "<table><tbody><tr><td colspan='2'><center><img src='/Img/logo/" + $(CompanyDetails).find('Logo').text() + "' class=\"img-responsive\"/ style=\"width:20%;\"><br />";
                    html1 += '<span style="font-size: 12px;text-align:center;">';
                    html1 += '<span id="Address">' + $(CompanyDetails).find('Address').text() + '</span>&nbsp;&nbsp;|&nbsp;&nbsp;Phone:<span id="Phone">' + $(CompanyDetails).find('MobileNo').text() + '</span>,Fax:<span id="FaxNo">' + $(CompanyDetails).find('FaxNo').text() + '</span>&nbsp;&nbsp;|&nbsp;&nbsp;<span id="Website">' + $(CompanyDetails).find('Website').text() + '</span>';
                    $("#logo").attr("src", "../Img/logo/" + $(CompanyDetails).find("Logo").text())
                    $("#Address").text($(CompanyDetails).find("Address").text());
                    $("#Phone").text($(CompanyDetails).find("MobileNo").text());
                    $("#FaxNo").text($(CompanyDetails).find("FaxNo").text());
                    $("#Website").text($(CompanyDetails).find("Website").text());
                    $("#TC").html($(CompanyDetails).find("TermsCondition").text());
                    $('.InvContent').append(html);
                    window.print();
                }
                } else {
                    location.href = '/';
                }
            
        },
        error: function (result) {

        },
        failure: function (result) {

        }
    });


}