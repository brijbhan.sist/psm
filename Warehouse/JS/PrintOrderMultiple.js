﻿$(document).ready(function () {
    var OrderID = localStorage.getItem("2");
    var OrderArr =  OrderID.split(',');
    var html = "";
    $.each(OrderArr, function (index) {     
        html += PrintAllOrder(OrderArr[index]);
    })
    localStorage.removeItem("2");
    $("#AllOrder").append(html);
    window.print();
    var html = "";
});


function PrintAllOrder(OrderAutoId) {
    var html1 = "";
    $.ajax({
        type: "POST",
        url: "/Warehouse/WebAPI/PrintOrderMultiple.asmx/GetOrderDetails",
        data: "{'OrderAutoId':'" + OrderAutoId + "'}",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        async: false,
        beforeSend: function () {
            $('#fade').show();
        },
        complete: function () {
            $('#fade').hide();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var xml = $(xmldoc);
            var headerDetail = xml.find("Table");
            var ProductDetail = xml.find("Table1");

            var orderId = $(ProductDetail).find("OrderNo").text();
            $("#barcode").html(DrawCode39Barcode($(headerDetail).find("OrderNo").text()));

            html1 += '<table style="width:100%"><tbody>';

            html1 += '<tr>'; //Open
            html1 += '<td colspan="2"><div style="border-bottom:1px solid #5f7c8a">' + $("#barcode").html() + '</div> </td>';
            html1 += '</tr>';//ENd

            html1 += '<tr>'; //Open
            html1 += '<td colspan="2"> <legend style="text-align: center; font-weight: 700;">PRICE SMART SALES ORDER #&nbsp;<span>' + $(headerDetail).find("OrderNo").text() + '</span></legend> </td>';
            html1 += '</tr>';//ENd

            html1 += '<tr>'; //Open
            html1 += '<td>';//open
            html1 += '<table  class="table tableCSS" style="border: 1px solid #5f7c8a;" id="tblCust">';
            html1 += '<tbody>';
            html1 += '<tr>';
            html1 += '<td class="tdLabel">CUSTOMER NUMBER</td> <td>' + $(headerDetail).find("CustomerId").text() + '</td>';
            html1 += '</tr>';

            html1 += '<tr>';
            html1 += '<td class="tdLabel">CUSTOMER NAME</td><td>' + $(headerDetail).find("CustomerName").text() + '</td>';
            html1 += '</tr>';

            html1 += '<tr>';
            html1 += '<td class="tdLabel">CHECK BY</td><td>' + $(headerDetail).find("WarehouseName").text() + '</td>';
            html1 += '</tr>';

            html1 += '</tbody>';
            html1 += '</table>';
            html1 += '</td>';//end

            html1 += '<td style="vertical-align: top;">'
            html1 += '<table  class="table tableCSS" id="tblSO">';
            html1 += '<tbody>';

            html1 += '<tr>';
            html1 += '<td class="tdLabel">SALES PERSON</td><td>' + $(headerDetail).find("SalesPerson").text() + '</td>';
            html1 += '</tr>';

            html1 += '<tr>';
            html1 += '<td class="tdLabel">ORDER DATE</td><td>' + $(headerDetail).find("OrderDate").text() + '</td>';
            html1 += '</tr>';

            html1 += '<tr>';
            html1 += ' <td class="tdLabel">PACK BY</td><td>' + $(headerDetail).find("PackerName").text() + '</td>';
            html1 += '</tr>';

            html1 += '</tbody>';
            html1 += '</table>';
            html1 += '</td>'

            html1 += '</tr>';//End
            html1 += '</tbody></table>';

            html1 += '<fieldset>';
            html1 += '<table style="width: 100%;" class="tableCSS">';
            html1 += '<tbody>';
            html1 += '<tr>';
            html1 += '<td colspan="2" style="width: 6%;"><b>Sales Remark</b></td><td style="width: 33%;font-size: 15px;font-weight: 600;">' + $(headerDetail).find("OrderRemarks").text() + '</td>';
            html1 += '</tr>';
            html1 += '<tr>';
            html1 += ' <td colspan="2" style="width: 6%;"><b>Warehouse Remarks</b></td><td style="width: 33%;font-size: 15px;font-weight: 600;">' + $(headerDetail).find("WarehouseRemarks").text() + '</td>';
            html1 += '</tr>';
            html1 += '</tbody>';
            html1 += '</table>';
            html1 += '</fieldset>';

            html1 += '<fieldset>';
            html1 += '<table class="table tableCSS" id="tblProduct">';
            html1 += '<thead>';
            html1 += ' <tr>';
            html1 += '<td class="Qty" style="width:5%;text-align:center">Qty</td>';
            html1 += '<td class="UnitType" style="width:5%;text-align:center">Unit Type</td>';
            html1 += '<td class="TotalPieces" style="width:5%;font-size: 15px;text-align:center">U/M</td>';
            html1 += '<td class="ProId" style="width:5%;text-align:center">Product ID</td>';
            html1 += '<td class="ProName" style="text-align:center">Product Desc.</td>';
            html1 += '<td class="Location" style="width:5%;text-align:center">Location</td>';
            html1 += '</tr>';
            html1 += '</thead>';
            html1 += '<tbody>';

            $.each(ProductDetail, function (index) {
                html1 += ' <tr>';
                html1 += '<td class="Qty" style="width:5%;text-align:center">' + $(this).find("RequiredQty").text() + '</td>';
                html1 += '<td class="UnitType" style="width:5%;text-align:center">' + $(this).find("UnitType").text() + '</td>';
                html1 += '<td class="TotalPieces" style="width:5%;font-size: 15px;text-align:center">' + $(this).find("TotalPieces").text() + '</td>';
                html1 += '<td class="ProId" style="width:5%;text-align:center">' + $(this).find("ProductId").text() + '</td>';
                html1 += '<td class="ProName">' + $(this).find("ProductName").text() + '</td>';
                html1 += '<td class="Location" style="width:5%">' + $(this).find("ProductLocation").text() + '</td>';
                html1 += '</tr>';
            })


            html1 += '</tbody>';
            html1 += '</table>';
            html1 += '</fieldset>';
        }
    });
    html1 += '<p  style="page-break-before: always"></p><br>';

    return html1;
}