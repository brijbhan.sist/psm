﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="ManageOrder.aspx.cs" Inherits="Warehouse_ManageOrder" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        #tblProductDetail tbody .ProName {
            text-align: left;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input id="DraftAutoId" type="hidden" />
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Manage Order</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage Order</a></li>
                        <li class="breadcrumb-item">New Order</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a class="dropdown-item" onclick="location.href='/Warehouse/WarehouseorderList.aspx'" runat="server">Order List</a>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <input type="hidden" id="hiddenEmpTypeVal" runat="server" />
                        <input type="hidden" id="hiddenStatusCode" runat="server" />
                        <input type="hidden" runat="server" id="HDDomain" />
                        <div class="card-content collapse show">
                            <div class="card-body" id="panelBill">
                                <div class="row">
                                    <div class="col-md-3 col-sm-3">
                                        <label class="control-label">Order No</label>
                                        <input type="hidden" id="txtHOrderAutoId" class="form-control input-sm" />
                                        <div class="form-group">
                                            <input id="lblOrderId" class="form-control input-sm border-primary" readonly="readonly" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label class="control-label">Order Date</label>
                                        <div class="form-group input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input id="lblsOrderDate" class="form-control input-sm border-primary" readonly="readonly" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label class="control-label">Order Status</label>
                                        <input type="hidden" id="hiddenOrderStatus" />
                                        <div class="form-group">
                                            <input id="lblOrderStatus" class="form-control input-sm border-primary" readonly="readonly" />
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-3">
                                        <label class="control-label">Order Type</label>
                                        <div class="form-group">
                                            <input id="lblOrdertype" class="form-control input-sm border-primary" readonly="readonly" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-sm-3">
                                        <label class="control-label">Delivery Date</label>
                                        <div class="form-group input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input id="lblDeliveryDate" class="form-control input-sm border-primary" readonly="readonly" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-4 form-group">
                                        <label class="control-label">Sales Person</label>
                                        <div class="input-group">
                                            <input type="text" id="lblSalesperson" class="form-control border-primary input-sm" readonly="readonly" />
                                        </div>
                                    </div>
                                    <div class="col-md-3  col-sm-3">
                                        <label class="control-label">
                                            Customer
                                        </label>
                                        <div class="form-group">
                                            <input id="lblCustomer" class="form-control input-sm border-primary" readonly="readonly" />
                                            <input type="hidden" id="hiddenCustomerId" />
                                        </div>
                                    </div>
                                    <div class="col-md-3  col-sm-3">
                                        <label class="control-label">Customer Type</label>
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm border-primary" id="lblCustomerType" runat="server" readonly="readonly"/>
                                        </div>
                                    </div>


                                </div>
                                <div class="row">
                                    <div class="col-md-3  col-sm-3">
                                        <label class="control-label">Terms</label>
                                        <div class="form-group">
                                            <input id="lblTerms" class="form-control input-sm border-primary" readonly="readonly" />
                                        </div>
                                    </div>
                                    <div class="col-md-3  col-sm-3">
                                        <label class="control-label">Shipping Type</label>
                                        <div class="form-group">
                                            <input id="lblShippingType" class="form-control input-sm border-primary" readonly="readonly" />
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-4 form-group">
                                        <label class="control-label">Billing Address</label>
                                        <textarea class="form-control border-primary input-sm" id="lblBillAddress" readonly="readonly"> </textarea>
                                    </div>
                                    <div class="col-md-3 col-sm-4 form-group">
                                        <label class="control-label">Shipping Address</label>
                                        <textarea class="form-control border-primary input-sm" id="lblShipAddress" readonly="readonly"> </textarea>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section id="panelProduct" style="display: none;">
            <div class="row" id="panelOrderContent">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Add Products</h4>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse" onclick="LoadProducts()" id="LoadProducts"><i class="ft-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse">
                            <div class="card-body" id="panelBill5">
                                <div class="row">
                                    <label class="col-md-1 col-sm-1 control-label">Barcode</label>
                                    <div class="col-md-3  col-sm-3  form-group">
                                        <input type="text" class="form-control input-sm border-primary" id="txtBarcode" runat="server" onfocus="this.select()" onkeypress="return isNumberKey(event)" placeholder="Enter Barcode here" onchange="readBarcode()" />
                                    </div>

                                    <div class="col-md-3  col-sm-3 ">
                                        <div class="alert alert-default alertSmall pull-right " id="alertBarcodeCount" style="display: none; color: #c62828;">
                                            Barcode Count : &nbsp;<span>sd</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="row">
                                    <div class="col-md-5  col-sm-3 ">
                                        <label class="control-label">
                                            Product <span class="required">*</span>

                                        </label>
                                        <div class="form-group">
                                            <select class="form-control input-sm border-primary selectvalidate" id="ddlProduct" runat="server" style="width: 100% !important" onchange="BindUnittype()">
                                                <option value="0">-Select-</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2  col-sm-2 ">
                                        <label class="control-label">
                                            Unit Type <span class="required">*</span>

                                        </label>
                                        <div class="form-group">
                                            <select class="form-control input-sm border-primary ddlreq" id="ddlUnitType" onchange="BindunitDetails()" runat="server">
                                                <option value="0">-Select-</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-1  col-sm-2">
                                        <label class="control-label" style="word-wrap: normal">
                                            Qty <span class="required">*</span>

                                        </label>
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm border-primary ddlreq text-center" id="txtReqQty" runat="server" value="1" maxlength="4" onkeypress="return isNumberKey(event)" />
                                        </div>
                                    </div>
                                    <div class="col-md-1  col-sm-1 ">
                                        <label class="control-label" style="white-space: nowrap">
                                            Is Exchange 
                                        </label>
                                        <div class="form-group">
                                            <input type="checkbox" id="chkExchange" onchange="changetype(this,'chkFreeItem')" />
                                        </div>
                                    </div>
                                    <div class="col-md-1  col-sm-1 " id="freeItem">
                                        <label class="control-label" style="white-space: nowrap">
                                            Is Free Item 
                                        </label>
                                        <div class="form-group">
                                            <input type="checkbox" id="chkFreeItem" onchange="changetype(this,'chkExchange')" />
                                        </div>
                                    </div>
                                    <script>
                                        function changetype(e, idhtml) {
                                            $('#' + idhtml).prop('checked', false);
                                        }
                                    </script>
                                    <div class="col-md-1">
                                        <div class="pull-left">
                                            <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1 btn-sm pulse" id="btnAdd" onclick="AddItems()" style="margin-top: 22px;">&nbsp;&nbsp;&nbsp;Add&nbsp;&nbsp;&nbsp;</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 form-group">
                                        <div class="alert alert-danger alertSmall" id="alertStockQty" style="display: none; text-align: center; color: white !important">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>

        <section>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body" id="panelBill6">
                                <div class="table-responsive">
                                    <table id="tblProductDetail" class="table table-striped table-bordered">
                                        <thead class="bg-blue white">
                                            <tr>
                                                <td class="Action text-center">Action</td>
                                                <td class="ProId text-center">ID</td>
                                                <td class="ProName">Product Name</td>
                                                <td class="UnitType text-center">Unit</td>
                                                <td class="ReqQty text-center" style="word-wrap: break-word; width: 5%">Qty</td>
                                                <td class="TtlPcs text-center">Total Pieces</td>
                                                <td class="IsExchange text-center" style="display: none">Exchange</td>
                                                <td class="IsStatus text-center" style="display: none">Status</td>
                                                <td class="OldUnitType text-center" style="display: none">OldUnitType</td>
                                                <td class="OM_MinPrice text-center" style="display: none">Min Price</td>
                                                <td class="OM_CostPrice text-center" style="display: none">Cost Price</td>
                                                <td class="OM_BasePrice text-center" style="display: none">Base Price</td>
                                                <td class="OM_Discount text-center" style="display: none">Discount</td>
                                                <td class="ItemType text-center" style="display: none">Item Type</td>
                                                <td class="AutoId text-center" style="display: none">Auto Id</td>

                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                    <h5 class="well text-center" id="emptyTable" style="display: none">No Product Selected.</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="row">
            <div class="col-md-7 col-sm-7">
                <section id="Div2">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Order Remark Details</h4>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body" id="panelBill10">
                                        <div class="row">
                                            <div class="col-md-12 form-group">
                                                <div class="table-responsive">
                                                    <table id="Table2" class="table table-striped table-bordered">
                                                        <thead class="bg-blue white">
                                                            <tr>
                                                                <td class="SRNO text-center">SN</td>
                                                                <td class="EmployeeName">Employee Name</td>
                                                                <td class="EmployeeType">Employee Type</td>
                                                                <td class="Remarks" style="white-space: normal;">Remark</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <section>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body" id="panelBill9">
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <button type="button" id="btnEditOrder" class="btn btn-warning buttonAnimation round box-shadow-1 btn-sm pulse" onclick="ClickEditBtn()">Edit Order </button>
                                        <button type="button" id="btnUpdateOrder" class="btn btn-primary buttonAnimation round box-shadow-1 btn-sm pulse" onclick="UpdateOrder()" style="display: none;">Update Order</button>
                                        <button type="button" id="btnBackOrder" class="btn btn-info buttonAnimation round box-shadow-1 btn-sm pulse" onclick="viewOrderLog()">View Log </button>
                                        <button type="button" id="btnback" class="btn btn-google buttonAnimation round box-shadow-1 btn-sm pulse" style="display: none;" onclick="Back()">Back</button>
                                        <button type="button" id="btnAssignPacker" class="btn btn-success buttonAnimation round box-shadow-1 btn-sm pulse" onclick="AssignedPacker()">Assign Packer </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <audio class="audios" id="yes_audio" controls preload="none" style="display: none">
        <source src="/Audio/NOExists.mp3" type="audio/mpeg" />
    </audio>
    <audio class="audios" id="No_audio" controls preload="none" style="display: none">
        <source src="/Audio/WrongProduct.mp3" type="audio/mpeg" />
    </audio>
    <div id="modalOrderLog" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-xl">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="display: block">
                    <h4 class="modal-title">Order Log  
                        <label style="margin-left: 50px;"><b>Order No</b>&nbsp;:&nbsp;<span id="Span1"></span>&nbsp;&nbsp;<b>Order Date</b>&nbsp;:&nbsp;<span id="lblOrderDate"></span></label></h4>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="tblOrderLog">
                            <thead class="bg-blue white">
                                <tr>
                                    <td class="SrNo text-center">SN</td>
                                    <td class="ActionBy">Action By</td>
                                    <td class="Date text-center">Date</td>
                                    <td class="Action text-center">Action</td>
                                    <td class="Remark" style="white-space: normal;">Remark</td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="packerDiv" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="display: block">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Packer List</h4>
                </div>
                <div class="modal-body">
                    <div id="Div4">
                    </div>
                    <div id="Div5">
                        <div class="alert alert-success" id="Div6" style="display: none; text-align: center;">
                        </div>
                        <div class="alert alert-danger" id="alertDangMiscPacker" style="display: none; text-align: center;">
                        </div>
                        <input type="hidden" id="txtHTimes" class="form-control input-sm" />
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped" id="packerList">
                                <thead class="bg-blue white">
                                    <tr>
                                        <td class="DrvName">Packer Name</td>
                                        <td class="Select text-center">Select</td>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                Times (Mins) :
                            </div>
                            <div class="col-md-6">
                                <input type="text" id="txtTimes" maxlength="4" onkeypress="return isNumberKey(event)" class="form-control input-sm form-group border-primary" />
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-md-3">
                                Remark
                            </div>
                            <div class="col-md-9">
                                <textarea id="txtWarehouseRemark" class="form-control input-sm border-primary" rows="3" maxlength="200"></textarea>
                                <span>Note : <i>Max 200 character allowed</i></span>
                            </div>


                        </div>
                    </div>
                </div>
                <input type="hidden" id="txtHWarehouseRemark" />
                <div class="modal-footer">

                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1 btn-sm pulse" id="Button1" onclick="assignPacker()">Assign</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

