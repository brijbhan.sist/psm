﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="DraftList.aspx.cs" Inherits="Sales_DraftList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script>
        $(document).ready(function () {
            $('#txtSFromDate').pickadate({
                min: new Date("01/01/2019"),
                format: 'mm/dd/yyyy',
                formatSubmit: 'mm/dd/yyyy',
                selectYears: true,
                selectMonths: true
            });
            $('#txtSToDate').pickadate({
                min: new Date("01/01/2019"),
                format: 'mm/dd/yyyy',
                formatSubmit: 'mm/dd/yyyy',
                selectYears: true,
                selectMonths: true
            });
            var d = new Date();
            var month = d.getMonth() + 1;
            if (month.toString().length == 1) {
                month = '0' + month;
            }
            var day = d.getDate();
            if (day.toString().length == 1) {
                day = '0' + day;
            }
            var year = d.getFullYear();
            $("#txtSFromDate").val(month + '/' + day + '/' + year);
            $("#txtSToDate").val(month + '/' + day + '/' + year);

            bindDropdown();
        })
        function setdatevalidation(val) {
            var fdate = $("#txtSFromDate").val();
            var tdate = $("#txtSToDate").val();
            if (val == 1) {
                if (fdate == '' || new Date(fdate) > new Date(tdate)) {
                    $("#txtSToDate").val(fdate);
                }
            }
            else if (val == 2) {
                if (fdate == '' || new Date(fdate) > new Date(tdate)) {
                    $("#txtSFromDate").val(tdate);
                }
            }
        }
        function bindDropdown() {
            $.ajax({
                type: "POST",
                url: "OrderNew.aspx/bindDropdown",
                data: "{}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                async: false,
                success: function (response) {
                    if (response.d != "Session Expired") {
                        var getData = $.parseJSON(response.d);
                        $("#ddlCustomer option:not(:first)").remove();
                        $.each(getData[0].CustomerList, function (index, item) {
                            $("#ddlCustomer").append("<option value='" + item.A + "'>" + item.C + "</option>");
                        });
                        $("#ddlCustomer").select2();

                    } else {
                        location.href = '/';
                    }
                },
                failure: function (result) {
                    console.log(result.d);
                },
                error: function (result) {
                    console.log(result.d);
                }
            });
        }
        function Pagevalue(e) {
            getOrderList(parseInt($(e).attr("page")));
        };
        function getOrderList(pageIndex) {
            var data = {
                CustomerAutoId: $('#ddlCustomer').val(),
                FromDate: $("#txtSFromDate").val(),
                ToDate: $("#txtSToDate").val(),
                pageIndex: pageIndex,
                PageSize: $("#ddlPageSize").val()
            };

            $.ajax({
                type: "POST",
                url: "DraftList.aspx/getDraftOrderList",
                data: "{'dataValues':'" + JSON.stringify(data) + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },  
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.d != "Session Expired") {
                        var xmldoc = $.parseXML(response.d);
                        var orderList = $(xmldoc).find("Table1");
                        $("#tblOrderList tbody tr").remove();
                        var row = $("#tblOrderList thead tr").clone(true);
                        if (orderList.length > 0) {
                            $("#EmptyTable").hide();
                            $.each(orderList, function () {
                                $(".orderDt", row).text($(this).find("OrderDate").text());
                                $(".cust", row).text($(this).find("CustomerName").text());
                                $(".DeliveryDate", row).text($(this).find("DeliveryDate").text());
                                $(".product", row).text($(this).find("NoOfItems").text());
                                $(".EmpName", row).text($(this).find("EmpName").text());
                                $(".ShippingType", row).text($(this).find("ShippingType").text());
                                $(".status", row).html("<span class='badge badge badge-pill badge-warning'>" + $(this).find("Status").text() + "</span>");
                                $(".action", row).html("<a href='/Warehouse/OrderNew.aspx?DraftAutoId=" + $(this).find("DraftAutoId").text() + "'><span class='ft-edit' title='Edit'></span></a> | <a href='javascript:;'><span class='ft-x' title='Delete' onclick='deleterecord(\"" + $(this).find("DraftAutoId").text() + "\")'></span><b></a>&nbsp;&nbsp;</b>");
                                $("#tblOrderList tbody").append(row);
                                row = $("#tblOrderList tbody tr:last").clone(true);
                            });
                        } else {
                            $("#EmptyTable").show();
                        }
                        var EmpType = $(xmldoc).find("Table2");
                        if ($(EmpType).find('EmpType').text() != 1) {
                            $(".EmpName").hide();
                        }

                        var pager = $(xmldoc).find("Table");
                        $(".Pager").ASPSnippets_Pager({
                            ActiveCssClass: "current",
                            PagerCssClass: "pager",
                            PageIndex: parseInt(pager.find("PageIndex").text()),
                            PageSize: parseInt(pager.find("PageSize").text()),
                            RecordCount: parseInt(pager.find("RecordCount").text())
                        });

                        if ($("#ddlPageSize").val() == '0') {
                            $(".Pager").hide();
                        }
                        else {
                            $(".Pager").show();
                        }
                    } else {
                        location.href = "/";
                    }

                },
                failure: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                },
                error: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                }
            });
        }

        function deleterecord(DraftAutoId) {
            swal({
                title: "Are you sure?",
                text: "You want to delete order !",
                icon: "warning",
                showCancelButton: true,
                buttons: {
                    cancel: {
                        text: "No, cancel!",
                        value: null,
                        visible: true,
                        className: "btn-warning",
                        closeModal: false,
                    },
                    confirm: {
                        text: "Yes, delete it!",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: false
                    }
                }
            }).then(function (isConfirm) {
                if (isConfirm) {
                    deleteDraftOrder(DraftAutoId);

                } else {
                    swal("", "Your data is safe.", "error");
                }
            })
        }

        function deleteDraftOrder(DraftAutoId) {
            $.ajax({
                type: "POST",
                url: "DraftList.aspx/deleteDraftOrder",
                data: "{'DraftAutoId':" + DraftAutoId + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.d == 'Session Expired') {
                        location.href = '/';
                    } else if (response.d == 'true') {
                        swal("", "Draft order deleted successfully.", "success")
                        getOrderList(1);
                    } else {
                        swal('Error !', "Oops! Something went wrong.Please try later.", "error");

                    }
                },
                failure: function (result) {
                    swal('Error !', "Oops! Something went wrong.Please try later.", "error");

                },
                error: function (result) {
                    swal('Error !', "Oops! Something went wrong.Please try later.", "error");
                }
            });
        }
    </script>

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Draft Order List</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Manage Order</a>
                        </li>
                        <li class="breadcrumb-item active">Draft Order List
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" id="Button5" class="dropdown-item" onclick="location.href='/Warehouse/OrderNew.aspx'">Create new Order</button>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body" style="min-height:400px">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-3">
                                        <select id="ddlCustomer" class="form-control input-sm border-primary">
                                            <option value="0">All Customer</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    From Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control input-sm border-primary" onchange="setdatevalidation(1)" placeholder="From Date" id="txtSFromDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    To Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control input-sm border-primary" onchange="setdatevalidation(2)" placeholder="To Date" id="txtSToDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-1">
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" onclick="getOrderList(1)" id="btnSearch">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblOrderList">
                                                <thead class="bg-blue white">

                                                    <tr>
                                                        <td class="action  text-center width3per">Action</td>
                                                        <td class="orderDt text-center width4per">Order Date</td>
                                                        <td class="cust">Customer</td>
                                                      <%--  <td class="DeliveryDate  text-center">Delivery Date</td>--%>
                                                        <td class="product  text-center width3per">Products</td>
                                                        <td class="status  text-center width3per">Status</td>
                                                        <td class="EmpName" style="display: none">Emp Name</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>

                                </div>
                                <div class="row container-fluid">
                                    <div>
                                        <select class="form-control input-sm border-primary" id="ddlPageSize" onchange="getOrderList(1)">
                                            <option selected="selected" value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</asp:Content>

