﻿using DllOrderMasterWH;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Warehouse_ManageOrder : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Warehouse/JS/ManageOrder.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredField'>" + text + "</script>"
                ));
        }
        try
        {
            if (Request.QueryString["OrderNo"] == null && (Session["EmpTypeNo"].ToString() != "2"))
            {
                Session.Abandon();
                Response.Redirect("~/Default.aspx", false);
            }
            hiddenEmpTypeVal.Value = Session["EmpTypeNo"].ToString();
        }
        catch
        {
            Session.Abandon();
            Response.Redirect("~/Default.aspx", false);
        }
        string[] GtUrl = Request.Url.Host.ToString().Split('.');
        HDDomain.Value = GtUrl[0].ToLower();
    }

    [WebMethod(EnableSession = true)]
    public static string editOrder(string orderNo, string loginEmpType)
    {
        PL_OrderMasterWH pobj = new PL_OrderMasterWH();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.OrderNo = orderNo;
                pobj.LoginEmpType = Convert.ToInt32(loginEmpType);
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                BL_OrderMasterWH.GetOrderdetails(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string bindDropdown()
    {
        PL_OrderMasterWH pobj = new PL_OrderMasterWH();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                BL_OrderMasterWH.bindDropdown(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0].ToString();
                }
                return json;
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string bindUnitType(int productAutoId)
    {
        PL_OrderMasterWH pobj = new PL_OrderMasterWH();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.ProductAutoId = productAutoId;
                BL_OrderMasterWH.bindUnitType(pobj);
                return pobj.Ds.GetXml();
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string GetBarDetails(string dataValues)
    {
        PL_OrderMasterWH pobj = new PL_OrderMasterWH();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValues);
                pobj.ReqQty = 1;
                pobj.Barcode = jdv["Barcode"];
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                BL_OrderMasterWH.GetBarDetails(pobj);
                return pobj.Ds.GetXml();
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string updateOrder(string TableValues, string orderData)
    {
        DataTable dtOrder = JsonConvert.DeserializeObject<DataTable>(TableValues);
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(orderData);
        PL_OrderMasterWH pobj = new PL_OrderMasterWH();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                if (dtOrder.Rows.Count > 0)
                {
                    pobj.TableValue = dtOrder;
                }
                pobj.LoginEmpType = Convert.ToInt32(HttpContext.Current.Session["EmpTypeNo"]);
                pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                BL_OrderMasterWH.updateOrder(pobj);
                if (!pobj.isException)
                {
                    return pobj.exceptionMessage;
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops ! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string assignPacker(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_OrderMasterWH pobj = new PL_OrderMasterWH();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
                pobj.PackerAutoId = Convert.ToInt32(jdv["PackerAutoId"]);
                if (jdv["Times"] != "")
                {
                    pobj.Times = Convert.ToInt32(jdv["Times"]);
                }
                else
                {
                    pobj.Times = 0;
                }
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                pobj.Remarks = jdv["Remarks"];
                BL_OrderMasterWH.assignPacker(pobj);
                if (!pobj.isException)
                {

                    return "true";
                }
                else
                {

                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

}