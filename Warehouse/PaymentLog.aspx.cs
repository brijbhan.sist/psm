﻿using DLLPaymentLogReport;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Warehouse_PaymentLog : System.Web.UI.Page
{
  
    protected void Page_Load(object sender, EventArgs e)
    {
        string text = File.ReadAllText(Server.MapPath("/Warehouse/JS/PaymentLogReport.js"));
        Page.Header.Controls.Add(
             new LiteralControl(
                "<script id='checksdrivRequiredField'>" + text + "</script>"
            ));
    }

    [WebMethod(EnableSession = true)]
    public static string getPaymentReport(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_PaymentLogReport pobj = new PL_PaymentLogReport();
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.EmpAutoId = Convert.ToInt32(jdv["CollectionBy"]);
                pobj.PayId = jdv["PayId"];
                if (jdv["FromDate"] != "")
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                if (jdv["ToDate"] != "")
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);

                if (jdv["SettledFromDate"] != "")
                    pobj.SettledFromDate = Convert.ToDateTime(jdv["SettledFromDate"]);
                if (jdv["SettledToDate"] != "")
                    pobj.SettledToDate = Convert.ToDateTime(jdv["SettledToDate"]);

                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.PaymentMode = Convert.ToInt32(jdv["PaymentMode"]);
                pobj.Range = Convert.ToInt32(jdv["Range"]);
                if (jdv["PaymentAmount"] != "")
                {
                    pobj.PaymentAmount = Convert.ToDecimal(jdv["PaymentAmount"]);
                }
                else
                {
                    pobj.PaymentAmount = Convert.ToDecimal("0.00");
                }

                BL_PaymentLogReport.BindPaymentLogReport(pobj);
                return pobj.Ds.GetXml();
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }


    [WebMethod(EnableSession = true)]
    public static string BindDropDown()
    {
        try
        {
            PL_PaymentLogReport pobj = new PL_PaymentLogReport();
            BL_PaymentLogReport.BindDropDown(pobj);
            string json = "";
            foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
            {
                json += dr[0];
            }
            return json;
        }
        catch(Exception)
        {
            return "false";
        }
    }


    [WebMethod(EnableSession = true)]
    public static string getReceivedPrint(string PaymentAutoId)
    {
        PL_PaymentLogReport pobj = new PL_PaymentLogReport();
        pobj.PaymentAutoId = Convert.ToInt32(PaymentAutoId);
        BL_PaymentLogReport.getReceivedPrint(pobj);
        return pobj.Ds.GetXml();
    }
}