﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/MasterPage.master" ClientIDMode="Static" CodeFile="INVstockEntryList.aspx.cs" Inherits="Warehouse_stockEntryList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Draft PO List</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Manage PO</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">By Vendor</a>
                        </li>
                        <li class="breadcrumb-item">Draft PO List</li>

                    </ol>
                </div>
            </div>
        </div>

    </div>

    <div class="content-body" style="min-height: 400px">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-4">
                                        <select class="form-control border-primary input-sm" id="ddlSVendor" runat="server" style="width: 100%">
                                            <option value="0">All Vendors</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Bill No" id="txtSBillNo" onfocus="this.select()" />
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <select id="ddlStatus" class="form-control border-primary input-sm">
                                            <option value="0">All</option>
                                        </select>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-md-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text input-group-text-custom" style="padding: 3px !important">From Date <span class="la la-calendar"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" onchange="setdatevalidation(1)" placeholder="From Date" id="txtSFromBillDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text input-group-text-custom" style="padding: 3px !important">To Date <span class="la la-calendar"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" onchange="setdatevalidation(2)" placeholder="To Date" id="txtSToBillDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-1 form-group">
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" id="btnSearch">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblStockEntryList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="Action text-center">Action</td>
                                                        <td class="Status text-center">Status</td>
                                                        <td class="BillNo text-center">Bill No</td>
                                                        <td class="BillDate text-center">Bill Date</td>
                                                        <td class="Vendor">Vendor</td>
                                                        <td class="Products text-center ">Products</td>
                                                        <td class="Total price">Total Amount</td>
                                                        <td class="Remarks">Remark</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                    <tr style="font-weight: bold">
                                                        <td colspan="6" style="text-align: right">Total</td>
                                                        <td id="gTotal" style="text-align: right"></td>
                                                        <td></td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex">
                                    <div class="form-group">
                                        <select id="ddlPageSize" class="form-control border-primary input-sm">
                                            <option selected="selected" value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="ml-auto">
                                        <div class="Pager"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="table-responsive" style="display: none;" id="PrintTable1">
            <div class="row" style="margin-bottom: 5px;">
                <div class="col-md-4 col-lg-4 col-sm-4 text-left">
                    <img src="" id="PrintLogo" height="40" />
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-center" style="margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;">
                    Draft PO List By Vendor
                    <br />
                    <span class="text-center DateRangeCSS" style="font-size: 9px; color: black;" id="DateRange"></span>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-right" id="PrintDate" style="line-height: 3; font-size: 12px; color: black; font-weight: bold;">Print Date</div>
            </div>
            <table class="MyTableHeader PrintMyTableHeader" id="PrintTable">
                <thead class="bg-blue white">
                    <tr>
                        <td class="PStatus" style="text-align: center">Status</td>
                        <td class="PBillNo" style="text-align: center">Bill No</td>
                        <td class="PBillDate" style="text-align: center">Bill Date</td>
                        <td class="PVendor">Vendor</td>
                        <td class="PProducts" style="text-align: center">Products</td>
                        <td class="PTotal price" style="text-align: right">Total Amount</td>
                        <td class="PRemarks">Remark</td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                    <tr style="font-weight: bold">
                        <td colspan="5" style="text-align: right">Total</td>
                        <td id="pTotal" style="text-align: right"></td>
                        <td></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="/Warehouse/JS/InvstockEntryList.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>

