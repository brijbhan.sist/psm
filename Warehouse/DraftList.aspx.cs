﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DllSalesManMaster;
using System.Web.Script.Serialization;
public partial class Sales_DraftList : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["EmpType"] == null)
        {
            Session.Abandon();
            Response.Redirect("~/Default.aspx", false);
        }

    }
    [WebMethod(EnableSession = true)]
    public static string getDraftOrderList(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_SalesManMaster pobj = new PL_SalesManMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.Opcode = 424;
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                if (jdv["FromDate"] != null && jdv["FromDate"] != "")
                {
                    pobj.Fromdate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != null && jdv["ToDate"] != "")
                {
                    pobj.Todate = Convert.ToDateTime(jdv["ToDate"]);
                }
                pobj.PageIndex = jdv["pageIndex"];
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                BL_SalesManMaster.getDraftOrderList(pobj);
                if (!pobj.isException)
                {

                    return pobj.Ds.GetXml();
                }
                else
                {

                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public static string deleteDraftOrder(int DraftAutoId)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_SalesManMaster pobj = new PL_SalesManMaster();
            try
            {
                pobj.DraftAutoId = Convert.ToInt32(DraftAutoId);
                BL_SalesManMaster.deleteDraft(pobj);
                if (!pobj.isException)
                {

                    return "true";
                }
                else
                {

                    return "Oops! Something went wrong.Please try later.";
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}