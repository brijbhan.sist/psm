﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="WarehouseOrderList.aspx.cs" Inherits="Sales_CopyOrderList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .StatusRTS {
            BACKGROUND-COLOR: #1166b1;
            COLOR: WHITE;
            PADDING: 3PX;
            BORDER-RADIUS: 5PX;
        }

        .addon {
            background-color: green;
            color: white;
            padding: 5px;
            border-radius: 5px;
        }

        .input-group-text {
            padding: 0 1rem
        }
        .border-radius{
            border-top-right-radius: 4px;
    border-bottom-right-radius: 4px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Order List</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Manage Order</a>
                        </li>
                        <li class="breadcrumb-item">Order List
                        </li>
                    </ol>
                </div>
            </div>
        </div>

    </div>

    <div class="content-body" style="min-height: 400px">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-3">
                                        <select class="form-control border-primary input-sm" id="ddlSalesPerson" runat="server" onchange="BindCustomer();">
                                            <option value="0">All Sales Person</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <select class="form-control border-primary input-sm" id="ddlCustomer" runat="server">
                                            <option value="0">All Customer</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-3 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlSStatus" style="padding: 0 2px !important;">
                                            <option value="0">All Status</option>
                                        </select>
                                    </div>

                                    <div class="col-sm-12 col-md-3">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Order No" id="txtSOrderNo" onfocus="this.select()" />
                                    </div>
                                    <div class="col-sm-12 col-md-3 form-group" id="col4" style="display: none"></div>
                                    <div class="col-md-3 col-sm-12">
                                        <select class="form-control border-primary input-sm" id="ddlShippingType">
                                            <option value="0">All Shipping Type</option>
                                        </select>
                                    </div>

                                    <div class="col-sm-12 col-md-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0 7px !important">From Order Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm border-radius" placeholder="From Date" id="txtSFromDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0 7px !important">To Order Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm border-radius" placeholder="To Date" id="txtSToDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" id="btnSearch" onclick="getOrderList(1);">Search</button>&nbsp;&nbsp;&nbsp;
                                        <button type="button" class="btn btn-success buttonAnimation round box-shadow-1  btn-sm" id="btnAssign" onclick="AssignedPacker()">Assign Packer</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblOrderList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="action text-center width3per">Action</td>
                                                        <td class="status  text-center width3per">Status</td>
                                                        <td class="orderNo  text-center width3per">Order No</td>
                                                        <td class="orderDt  text-center width3per">Order Date</td>
                                                        <td class="SalesPerson">Sales Person</td>
                                                        <td class="cust">Customer</td>
                                                        <%-- <td class="value price width3per">Order Amount</td>           --%>
                                                        <td class="Shipping">Shipping Type</td>
                                                        <td class="product  text-center">Products</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <select id="ddlPageSize" class="form-control border-primary input-sm pagesize" onclick="getOrderList(1);">
                                            <option selected="selected" value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager text-right"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <table class="table table-striped table-bordered" id="tblExport" style="display: none">
        <thead>
            <tr>
                <td class="orderNo">Order No</td>
                <td class="orderDt">Order Date</td>
                <td class="SalesPerson">Sales Person</td>
                <td class="cust">Customer</td>
                <td class="value">Order Amount</td>
                <td class="product">Products(Nos.)</td>
                <td class="status">Status</td>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <!-- Modal -->
    <div id="modalOrderLog" class="modal fade" role="dialog">
        <div class="modal-dialog modal-xl">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row" style="width: 100%">
                        <div class="col-md-3">
                            <h4 class="modal-title" style="margin: 0.5rem;">Order Log</h4>
                        </div>
                        <div class="col-md-9 text-right">
                            <h4 style="margin: 0.5rem;"><b>Order No </b>&nbsp;:&nbsp;<span id="lblOrderNo"></span>&nbsp;&nbsp;<b>Order Date</b>&nbsp;:&nbsp;<span id="lblOrderDate"></span></h4>
                        </div>
                    </div>
                </div>
                <div class="modal-body" style="padding-bottom: 0">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="tblOrderLog">
                            <thead class="bg-blue white">
                                <tr>
                                    <td class="SrNo text-center width2per">SN</td>
                                    <td class="ActionBy">Action By</td>
                                    <td class="Date text-center width3per">Date</td>
                                    <td class="Action">Action</td>
                                    <td class="Remark" style="white-space: normal;">Remark</td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="packerDiv" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="display: block">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Packer List</h4>
                </div>
                <div class="modal-body">
                    <div id="Div4">
                    </div>
                    <div id="Div5">
                        <div class="alert alert-success" id="Div6" style="display: none; text-align: center;">
                        </div>
                        <div class="alert alert-danger" id="alertDangMiscPacker" style="display: none; text-align: center;">
                        </div>
                        <input type="hidden" id="txtHTimes" class="form-control input-sm" />
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped" id="packerList">
                                <thead class="bg-blue white">
                                    <tr>
                                        <td class="DrvName">Packer Name</td>
                                        <td class="Select text-center">Select</td>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                Times (Mins) :
                            </div>
                            <div class="col-md-6">
                                <input type="text" id="txtTimes" maxlength="5" onkeypress="return isNumberKey(event)" class="form-control input-sm form-group border-primary" />
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-md-3">
                                Remark
                            </div>
                            <div class="col-md-9">
                                <textarea id="txtWarehouseRemark" class="form-control input-sm border-primary" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" id="txtHWarehouseRemark" />
                <div class="modal-footer">

                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1 btn-sm pulse" id="Button1" onclick="assignPacker()">Assign</button>
                </div>
            </div>
        </div>
    </div>
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>

