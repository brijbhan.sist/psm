﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/MasterPage.master" ClientIDMode="Static" CodeFile="INVstockEntry.aspx.cs" Inherits="Warehouse_stockEntry" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Stock Entry</h3>
            <input type="hidden" id="hiddenPackerAutoId" runat="server" />
            <input type="hidden" id="hiddenEmpType" runat="server" />
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage Inventory</a></li>
                        <li class="breadcrumb-item active">Stock Entry</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
          <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
            <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
            id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false"> Action</button>
            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" onclick=" location.href='/Warehouse/invstockEntryList.aspx'" class="dropdown-item">Go To Draft PO List</button>
            </div>
          </div>
        </div>
    </div>



        <section id="drag-area">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="row">
                                <label class="col-md-2 col-sm-2 control-label">
                                    Vendor
                                            <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-4 form-group">
                                    <select class="form-control input-sm border-primary ddlreq" id="ddlVendor" runat="server">
                                        <option value="0">-Select-</option>
                                    </select>
                                </div>
                                <label class="col-md-2 col-sm-2 control-label">
                                    Bill No<span class="required">*</span>
                                </label>
                                <input type="hidden" id="txtHBillAutoId" />
                                <div class="col-md-4 col-sm-4 form-group">
                                    <input type="text" id="txtBillNo" class="form-control input-sm border-primary req" runat="server" />
                                </div>
                                <label class="col-md-2 col-sm-2 control-label">
                                    Bill Date
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-4  col-sm-4 form-group">
                                    <input type="text" id="txtBillDate" class="form-control input-sm border-primary req" runat="server" />
                                </div>
                                
                                <div class="clearfix"></div>
                                <label class="col-md-2  col-sm-2 control-label">Status</label>
                                <div class="col-md-4  col-sm-4 form-group">
                                    <input class="form-control input-sm border-primary" rows="1" id="txtStatus" disabled value="Draft" />
                                </div>
                                 <label class="col-md-2  col-sm-2 control-label">Remark</label>
                                <div class="col-md-4  col-sm-4 form-group">
                                    <textarea class="form-control input-sm border-primary" rows="1" id="txtRemarks" runat="server"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <section id="pnlInputProduct">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <label class="col-md-2 col-sm-3 control-label">
                                        Barcode                                
                                    </label>
                                    <div class="col-md-4 col-sm-3  form-group">
                                        <input type="text" class="form-control input-sm border-primary" id="txtBarcode" runat="server" onfocus="this.select()" onkeypress="return isNumberKey(event)" placeholder="Enter Barcode here" onchange="readBarcode()" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="row">
                                    <label class="col-md-2  col-sm-3  control-label">
                                        Product
                                                   <span class="required">*</span>
                                    </label>
                                    <div class="col-md-4  col-sm-3  form-group">
                                        <select class="form-control input-sm border-primary" id="ddlProduct" runat="server" onchange="BindUnit();">
                                            <option value="0">-Select-</option>
                                        </select>
                                    </div>
                                    <label class="col-md-2 col-sm-3  control-label">
                                        Unit Type
                                                    <span class="required">*</span>
                                    </label>
                                    <div class="col-md-4 col-sm-3  form-group">
                                        <select class="form-control input-sm border-primary" onchange="getBarcodeCount()" id="ddlUnitType" runat="server">
                                            <option value="0">-Select-</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-2 col-sm-3  control-label">
                                        Quantity
                                                       <span class="required">*</span>
                                    </label>
                                    <div class="col-md-4 col-sm-3  form-group">
                                        <input type="text" class="form-control input-sm border-primary" id="txtQuantity" maxlength="6" runat="server" value="0" onfocus="this.select()" onkeypress='return isNumberKey(event)' />
                                    </div>
                                    <label class="col-md-2 col-sm-3  control-label">Total Pieces</label>
                                    <div class="col-md-4 col-sm-3  form-group">
                                       
                                            <input type="text" class="form-control input-sm border-primary" id="txtTotalPieces" runat="server" disabled="disabled" value="0" />
                                       
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-2 col-sm-3  control-label">
                                        Unit Price
                                <span class="required">*</span>
                                    </label>
                                    <div class="col-md-4 col-sm-3  form-group">
                                         <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    <span>$</span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control input-sm border-primary" id="txtUnitprice" runat="server" value="0.00"
                                                onfocus="this.select()" onkeypress='return isNumberDecimalKey(event,this)' style="text-align: right" />
                                        </div>
                                    </div>
                                    <label class="col-md-2 col-sm-3  control-label">Total Price</label>
                                    <div class="col-md-4 col-sm-3  form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    <span>$</span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control input-sm border-primary" id="txtTotalAmount" runat="server"
                                                disabled="disabled" value="0.00" style="text-align: right" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <div class="pull-right">
                                            <button type="button" id="btnAdd" class="btn btn-purple buttonAnimation pull-right round box-shadow-1 btn-sm animated undefined" runat="server">Add</button>
                                        </div>
                                    </div>
                                </div>
                            </div>                           
                        </div>
                    </div>
                </div>
            </section>
            <section id="drag-area1">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table id="tblProductDetail" class="table table-striped table-bordered">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="Action text-center">Action</td>
                                                        <td class="ProId text-center"  style="width:5%">ID</td>
                                                        <td class="ProName">Product Name</td>
                                                        <td class="Unit text-center">Unit Type</td>
                                                        <td class="Qty text-center" style="width:5%">Qty</td>
                                                        <td class="Pcs text-center" style="width:5%">Pieces</td>
                                                        <td class="Price" style="width:5%">Unit Price</td>
                                                        <td class="TotalPrice" style="text-align: right;width:5%">Total Price</td>
                                                    </tr>
                                                </thead>
                                                <tbody></tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td id="clspan" colspan="3" style="text-align: right">Total</td>
                                                        <td id="TotalQty" style="text-align: center">0</td>
                                                        <td id="TotalQtyNo" style="text-align: center">0</td>
                                                        <td id="Td1" style="text-align: left"></td>
                                                        <td id="total" style="text-align: right">0.00</td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            <h5 class="well text-center" id="emptyTable" style="display: none" runat="server">No Product Selected.</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <div class="card-footer text-right">
                                 <button type="button" id="btnUpdate" class="btn btn-success buttonAnimation round box-shadow-1 btn-sm" style="display: none" runat="server">Generate P.O.</button>
                                  <button type="button" id="btnGeneratePO" class="btn btn-cyan buttonAnimation round box-shadow-1 btn-sm animated" style="display: none" runat="server">Revert P.O.</button>
                                          
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div id="UpdateMinPrice" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-xl">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <span class="modal-title">
                                <b>Do you want to change Cost Price because Unit Price is not equal to Cost Price.</b>
                            </span>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <div class="">
                                <div class="row">
                                    <div class="col-md-2">
                                        Product ID
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <input type="text" id="txtproductId" class="form-control input-sm border-primary" disabled />
                                    </div>
                                    <div class="col-md-2">
                                        Product Name
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <input type="text" id="txtProductName" class="form-control input-sm border-primary" disabled />
                                    </div>
                                    <div class="col-md-2">
                                        Unit Type
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <input type="text" id="txtUnitType" class="form-control input-sm border-primary" disabled />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="row form-group">
                                    <div class="col-md-2">
                                        Quantity
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <input type="text" id="txtPQty" class="form-control input-sm border-primary" readonly />
                                    </div>
                                    <div class="col-md-2">
                                        Retail Minimum Price
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    <span>$</span>
                                                </span>
                                            </div>
                                            <input type="text" id="txtRetailMIN" class="form-control input-sm border-primary" readonly onfocus="this.select()" onkeypress='return isNumberDecimalKey(event,this)' style="text-align: right" />
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        Cost Price
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    <span>$</span>
                                                </span>
                                            </div>
                                            <input type="text" id="txtCOSTPRICE" class="form-control input-sm border-primary" readonly onfocus="this.select()" onkeypress='return isNumberDecimalKey(event,this)' style="text-align: right" />
                                        </div>
                                    </div>

                                </div>
                                <div class="clearfix"></div>
                                <div class="row form-group">
                                    <div class="col-md-2">
                                        Base Price
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    <span>$</span>
                                                </span>
                                            </div>
                                            <input type="text" id="txtBasePrice" class="form-control input-sm border-primary" readonly onfocus="this.select()" onkeypress='return isNumberDecimalKey(event,this)' style="text-align: right" />
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        Suggested Retail Price
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    <span>$</span>
                                                </span>
                                            </div>
                                            <input type="text" id="txtSRP" class="form-control input-sm border-primary" readonly onfocus="this.select()" onkeypress='return isNumberDecimalKey(event,this)' style="text-align: right" />
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        Wholesale Minimum Price
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    <span>$</span>
                                                </span>
                                            </div>
                                            <input type="text" id="txtWholesaleMinPrice" class="form-control input-sm border-primary" readonly onfocus="this.select()" onkeypress='return isNumberDecimalKey(event,this)' style="text-align: right" />
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="row form-group">
                                    <div class="col-md-2">
                                        Location
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <input type="text" id="txtLocation" class="form-control input-sm border-primary" readonly onfocus="this.select()" />
                                    </div>
                                   <%-- <div class="col-md-2">
                                        Predefined Barcode
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <input type="text" id="txtPreDefinedBarcode" class="form-control input-sm border-primary" readonly onfocus="this.select()" />
                                    </div>--%>
                                    <%--<div class="col-md-2">
                                        Commission Code
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    <span>$</span>
                                                </span>
                                            </div>
                                            <input type="text" id="txtCommissionCode" class="form-control input-sm border-primary" readonly onfocus="this.select()" onkeypress='return isNumberDecimalKey(event,this)' style="text-align: right" />

                                        </div>
                                    </div>--%>
                                </div>
                            </div>
                            <%--<div class="clearfix"></div>--%>
                            <%-- <div class="row form-group">
                                    <div class="col-md-9">
                                        <div class="alert alert-danger alert-dismissable fade in" id="Div1" style="display: none;">
                                            <a href="javascript:;" aria-label="close" class="close">Close</a>
                                            <span></span>
                                        </div>
                                    </div>
                                    <div class="col-md-5 form-group">
                                    </div>
                                </div>--%>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1 btn-sm" id="Button4" onclick="EditProductDetails()">Edit</button>
                            <button type="button" id="Button1" class="btn btn-success buttonAnimation pull-right round box-shadow-1 btn-sm" onclick="updateBasePrice()" style="display: none">Update</button>
                            <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm" id="Button2" onclick="NOPrice()">No</button>
                            <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm" id="Button3" onclick="CancelProduct()">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="msgPop" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-xl">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4>Message</h4>
                        </div>
                        <div class="modal-body">
                            <div style="border-color: #ac2925; border: 1px solid; color: #ac2925; min-height: 45px; line-height: 20px; padding: 9px;">
                                <strong id="Sbarcodemsg"></strong>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="/Warehouse/JS/InvstockEntry.js?v=' + new Date() + '"></scr' + 'ipt>');

    </script>
</asp:Content>

