﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DllStockReceiverMaster;
public partial class Warehouse_stockEntry : System.Web.UI.Page
{
   
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod(EnableSession = true)]
    public static string getProductThruBarcode(PL_StockReceiverMaster pobj)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        { 
            try
            { 
                pobj.UserAutoId = Convert.ToInt32((HttpContext.Current.Session["EmpAutoId"].ToString()));
                pobj.Opcode = 51;
                DL_StockReceiverMaster.ReturnTable(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {

                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

}