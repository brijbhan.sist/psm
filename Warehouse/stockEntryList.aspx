﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" ClientIDMode="Static" CodeFile="stockEntryList.aspx.cs" Inherits="Warehouse_stockEntryList" %>

<script runat="server">

</script>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Manage PO</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage PO</a></li>
                        <li class="breadcrumb-item active">Purchased Orders List</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
            <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <button type="button" onclick=" location.href='/Warehouse/stockEntry.aspx'" class="btn btn-info round  dropdown-menu-right box-shadow-2 px-2  " animation="pulse" id="Button1" runat="server">
                            Generate PO</button>
                    </li>
                </ol>
            </div>
        </div>
    </div>



    <div class="content-body" style="min-height: 400px">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">

                                    <div class="form-group col-md-3">
                                        <input type="text" class="form-control input-sm border-primary" placeholder="Bill No" id="txtSBillNo" onfocus="this.select()" />
                                    </div>
                                    <div class="form-group col-md-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text input-group-text-custom" style="padding: 3px !important">From Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control input-sm border-primary" onchange="setdatevalidation(1)" placeholder="From Date" id="txtSFromBillDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text input-group-text-custom" style="padding: 3px !important">To Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control input-sm border-primary" onchange="setdatevalidation(2)" placeholder="To Date" id="txtSToBillDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <select class="form-control input-sm border-primary" id="ddlSVendor" runat="server" style="width: 100%">
                                            <option value="0">All Vendors</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1 btn-sm" id="btnSearch">Search</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblStockEntryList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="Action text-center">Action</td>
                                                        <td class="BillNo text-center">Bill No</td>
                                                        <td class="BillDate text-center">Date</td>
                                                        <td class="Vendor">Vendor</td>
                                                        <td class="Products text-center">Product</td>
                                                        <%--<td class="total">Total Amount(USD)</td>--%>
                                                        <td class="Status text-center">Status</td>
                                                        <td class="Remarks" style="white-space: normal">Remark</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td colspan="4" style="text-align: right; font-weight: bold;">Total</td>
                                                        <td id="gTotal" class="text-center" style="text-align: center; font-weight: bold;"></td>
                                                        <td></td>
                                                        <td></td>

                                                    </tr>
                                                </tfoot>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex">
                                    <div class="form-group">
                                        <select id="ddlPaging" class="form-control input-sm border-primary">
                                            <option selected="selected" value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="ml-auto">
                                        <div class="Pager"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </div>

    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="/Warehouse/JS/stockEntryList.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
</asp:Content>

