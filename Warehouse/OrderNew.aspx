﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" ClientIDMode="Static" CodeFile="OrderNew.aspx.cs" Inherits="Warehouse_OrderNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        #tblduePayment {
            margin-bottom: 0 !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input id="OrderAutoId" type="hidden" />
    <input type="hidden" id="hiddenEmpTypeVal" runat="server" />
    <input type="hidden" id="hiddenStatusCode" runat="server" />
    <input type="hidden" id="hfDiscount" runat="server" />
    <script type="text/javascript">
        var getid;
        var Stock, AvailableAllocatedQty = 0, RequiredQty = 0, PrdtId = 0;
        $(document).ready(function () {
            $('#panelProduct input').attr('disabled', true);
            $('#panelProduct select').attr('disabled', true);
            $('#panelProduct button').attr('disabled', true);

            var d = new Date();
            var month = d.getMonth() + 1;
            if (month.toString().length == 1) {
                month = '0' + month;
            }
            var day = d.getDate();
            if (day.toString().length == 1) {
                day = '0' + day;
            }
            var year = d.getFullYear();
            $("#txtOrderDate").val(month + '/' + day + '/' + year);
            var getQueryString = function (field, url) {
                var href = url ? url : window.location.href;
                var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
                var string = reg.exec(href);
                return string ? string[1] : null;
            };
            var getid = getQueryString('OrderNo');
            var DraftAutoId = getQueryString('DraftAutoId');
            drtAutoId = DraftAutoId;
            bindDropdown();
            if (getid != null) {
                $('#OrderAutoId').val(getid);
                $("#txtHOrderAutoId").val(getid);
                bindOrderDetails(getid);
                $("#headertitle").text("View order")
            } else if (DraftAutoId != null) {
                $('#DraftAutoId').val(DraftAutoId);
                DraftOrder(DraftAutoId);
            }
            $('#txtScanBarcode').focus();
            $('#txtShipping').bind("cut copy paste", function (e) {
                e.preventDefault();
            });
        });

        var MLQtyRate = 0.00, MLTaxType = 0, WeightOzTex = 0;
        function bindDropdown() {
            $.ajax({
                type: "POST",
                url: "OrderNew.aspx/bindDropdown",
                data: "{}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                async: false,
                success: function (response) {
                    if (response.d != "Session Expired") {
                        var getData = $.parseJSON(response.d);
                        $("#ddlCustomer option:not(:first)").remove();
                        $.each(getData[0].CustomerList, function (index, item) {
                            $("#ddlCustomer").append("<option value='" + item.A + "'>" + item.C + "</option>");
                        });
                        $("#ddlCustomer").select2();
                        $("#ddlProduct option:not(:first)").remove();
                        $.each(getData[0].ProductList, function (index, item) {
                            $("#ddlProduct").append("<option MLQty='" + item.ML + "' WeightOz='" + item.Oz + "' value='" + item.AutoId + "'>" + item.PN + "</option>");
                        });
                        $("#ddlProduct").select2();
                        $("#ddlShippingType option:not(:first)").remove();

                        $.each(getData[0].ShippingType, function (index, item) {
                            $("#ddlShippingType").append("<option taxEnabled='" + item.EnabledTax + "' value='" + item.AutoId + "'>" + item.ST + "</option>");

                        });
                        $("#ddlShippingType").val('8');

                        $("#ddlPaymentMenthod option:not(:first)").remove();
                        $.each(getData[0].PaymentList, function (index, item) {
                            $("#ddlPaymentMenthod").append("<option value='" + item.AutoId + "'>" + item.PM + "</option>");
                        });
                    } else {
                        location.href = '/';
                    }
                },
                failure: function (result) {
                    console.log(result.d);
                },
                error: function (result) {
                    console.log(result.d);
                }
            });
        }
        function DraftOrder(DraftAutoId) {

            $.ajax({
                type: "POST",
                url: "OrderNew.aspx/EditDraftOrder",
                data: "{'DraftAutoId':'" + DraftAutoId + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.d != "Session Expired") {
                        var xmldoc = $.parseXML(response.d);
                        var Order = $(xmldoc).find("Table");
                        $("#txtOrderDate").val($(Order).find('OrderDate').text());
                        $("#txtDeliveryDate").val($(Order).find('DeliveryDate').text());
                        $("#txtOrderRemarks").val($(Order).find('Remarks').text());
                        if ($(Order).find('ShippingType').text() != '' && $(Order).find('ShippingType').text() != '0') {
                            $("#ddlShippingType").val($(Order).find('ShippingType').text());
                        }
                        else {
                            $("#ddlShippingType").val('9');
                        }
                        $("#txtOrderStatus").val($(Order).find('Status').text());
                        if ($(Order).find('CustomerAutoId').text().length > 0) {
                            $("#ddlCustomer").val($(Order).find('CustomerAutoId').text()).change();
                            $("#ddlCustomer").attr('disabled', true);
                        }
                        var items = $(xmldoc).find("Table1");
                        $("#tblProductDetail body tr").remove();
                        var row = $("#tblProductDetail thead tr").clone(true);
                        if (items.length > 0) {
                            $('#emptyTable').hide();
                        }

                        $.each(items, function () {
                            $("#tblProductDetail tbody").append(row);
                            MLQty = parseFloat($(this).find("UnitMLQty").text()).toFixed(2) || 0.00;
                            WeightOz = parseFloat($(this).find("WeightOz").text()).toFixed(2) || 0.00;
                            $(".ProId", row).html($(this).find("ProductId").text() + "<span isfreeitem=" + $(this).find("isFreeItem").text() + "></span>");
                            if (Number($(this).find("isFreeItem").text()) == 1) {
                                $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span>" + " <product class='badge badge badge-pill badge-success'>Free</product>");
                                MLQty = 0.00;
                                WeightOz = 0.00;
                            } else if (Number($(this).find("IsExchange").text()) == 1) {
                                $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span>" + " <product class='badge badge badge-pill badge-primary'>Exchange</product>");
                                MLQty = 0.00;
                                WeightOz = 0.00;
                            }
                            else if (Number($(this).find("IsTaxable").text()) == 1) {
                                $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span>" + " <product class='badge badge badge-pill badge-danger'>Taxable</product>");
                            } else {
                                $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span>");
                            }


                            $(".UnitType", row).html("<span UnitAutoId='" + $(this).find("UnitAutoId").text() + "' QtyPerUnit='" + $(this).find("QtyPerUnit").text() + "'>" + $(this).find("UnitType").text() + " (" + $(this).find("QtyPerUnit").text() + " pcs" + ")</span>");
                            $(".ReqQty", row).html("<input type='text' maxlength='6' onchange='CheckAllocatedQty(this)' class='form-control input-sm border-primary text-center' onkeypress='return isNumberKey(event)' onkeyup='rowCal(this,1)' style='width:70px;' findoldqty='" + $(this).find("ReqQty").text() + "' value='" + $(this).find("ReqQty").text() + "'/>");
                            $(".Barcode", row).text($(this).find("Barcode").text());
                            $(".QtyShip", row).html("<input type='text' class='form-control input-sm' style='width:70px;' value='0' onkeyup='rowCal(this,1);shipVsReq(this)' />");
                            $(".TtlPcs", row).text(Number($(this).find('QtyPerUnit').text()) * Number($(this).find('ReqQty').text()));
                            $(".SRP", row).text($(this).find("SRP").text());
                            $(".GP", row).text($(this).find("GP").text());
                            if (Number($(this).find("TaxRate").text()) == 1) {
                                $(".TaxRate", row).html('<span typetax="1" MLQty="' + MLQty + '" weightoz="' + WeightOz + '"></span>' + '<TaxRate style="font-weight: bold" class="la la-check-circle success center"></TaxRate>');
                            } else {
                                $(".TaxRate", row).html('<span typetax="0" MLQty="' + MLQty + '" weightoz="' + WeightOz + '"></span>');
                            }

                            if (Number($(this).find("IsExchange").text()) == 1) {
                                $(".UnitPrice", row).html("<input type='text' class='form-control input-sm border-primary' maxlength='6' onchange='changePrice(this)' onkeypress='return isNumberDecimalKey(event,this)'  style='width:70px;text-align:right;' value='0.00' minprice='" + $(this).find("minprice").text() + "' BasePrice='" + $(this).find("UnitPrice").text() + "' disabled />");
                                $(".IsExchange", row).html('<span isexchange="1"></span>' + '<IsExchange style="font-weight: bold" class="la la-check-circle success center"></IsExchange>');
                            } else if (Number($(this).find("isFreeItem").text()) == 1) {
                                $(".UnitPrice", row).html("<input type='text' class='form-control input-sm border-primary' maxlength='6' onchange='changePrice(this)' onkeypress='return isNumberDecimalKey(event,this)'  style='width:70px;text-align:right;' value='0.00' minprice='" + $(this).find("minprice").text() + "' BasePrice='" + $(this).find("UnitPrice").text() + "' disabled />");
                                $(".IsExchange", row).html('<span isexchange="0"></span>');
                            }
                            else {
                                $(".UnitPrice", row).html("<input type='text' class='form-control input-sm border-primary' maxlength='6' onchange='changePrice(this)' onkeypress='return isNumberDecimalKey(event,this)'  style='width:70px;text-align:right;' value='" + $(this).find("UnitPrice").text() + "' minprice='" + $(this).find("minprice").text() + "' BasePrice='" + $(this).find("UnitPrice").text() + "' />");
                                $(".IsExchange", row).html('<span isexchange="0"></span>');
                            }

                            $(".OM_MinPrice", row).text($(this).find("minprice").text());
                            $(".OM_CostPrice", row).text($(this).find("CostPrice").text());
                            $(".OM_BasePrice", row).text($(this).find("BasePrice").text());
                            $(".Oim_Discount", row).html("<input type='text' onfocus='this.select()' class='form-control input-sm border-primary' style='width:70px;text-align:right;' onchange='rowCal(this,1)' onkeypress='return isNumberDecimalKey(event,this)'  maxlength='6'  onkeyup='rowCal(this,1);'  value='" + parseFloat($(this).find("Oim_Discount").text()).toFixed(2) + "'  />");
                            $(".Del_ItemTotal", row).html($(this).find("Del_ItemTotal").text());
                            $(".NetPrice", row).text($(this).find("NetPrice").text());
                            $(".Action", row).html("<a href='javascript:;' id='deleterow' onclick='deleterow(this)'><span class='ft-x'></span></a>");
                            $('#tblProductDetail tbody').append(row);
                            row = $("#tblProductDetail tbody tr:last").clone(true);

                        });

                        calTotalAmount();
                        calGrandTotal();
                        calOverallDisc();
                        calTotalTax();
                        $("#txtShipping").val($(Order).find('ShippingCharges').text());
                        $("#txtDiscAmt").val($(Order).find('OverallDiscAmt').text());
                        $("#txtOverallDisc").val($(Order).find('OverallDisc').text());

                    } else {
                        location.href = '/';
                    }

                },
                failure: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                },
                error: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                }
            });
        }


        function bindOrderDetails(OrderAutoId) {

            $.ajax({
                type: "POST",
                url: "OrderNew.aspx/bindOrderDetails",
                data: "{'OrderAutoId':'" + OrderAutoId + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                async: false,
                success: function (response) {

                    if (response.d != "Session Expired") {
                        var xmldoc = $.parseXML(response.d);
                        var BillingAddress = $(xmldoc).find("Table");
                        var ShippingAddress = $(xmldoc).find("Table1");
                        var OrderDetails = $(xmldoc).find("Table2");
                        var OrderItemDetails = $(xmldoc).find("Table3");
                        var CreditDetails = $(xmldoc).find("Table4");
                        var MLTax = $(xmldoc).find("Table5");
                        var WeightOzTex = $(xmldoc).find("Table6");
                        if ($(WeightOzTex).find('Value').text() != '')
                            WeightOzTex = $(WeightOzTex).find('Value').text();
                        if ($(MLTax).find('TaxRate').text() != '')
                            MLQtyRate = $(MLTax).find('TaxRate').text();

                        $("#ddlShippingType").val($(OrderDetails).find("ShippingType").text());

                        $("#txtOrderId").val($(OrderDetails).find("OrderNo").text());
                        $("#ddlPaymentMenthod").val($(OrderDetails).find("paymentMode").text());
                        $("#txtReferenceNo").val($(OrderDetails).find("referNo").text());
                        $("#salesPerson").val($(OrderDetails).find("SALESNAME").text());
                        $("#txtOrderDate").val($(OrderDetails).find("OrderDate").text());
                        $("#txtTotalAmount").val($(OrderDetails).find("TotalAmount").text());
                        $("#txtOverallDisc").val($(OrderDetails).find("OverallDisc").text());
                        $("#txtShipping").val($(OrderDetails).find("ShippingCharges").text());
                        $("#txtDiscAmt").val($(OrderDetails).find("OverallDiscAmt").text());
                        if ($("#ddlShippingType option:selected").attr('taxenabled') == 1) {
                            $("#txtMLTax").val($(OrderDetails).find("MLTax").text());
                            $("#txtMLQty").val($(OrderDetails).find("MLQty").text());
                            $("#txtWeightQty").val($(OrderDetails).find("Weigth_OZQty").text());
                            $("#txtWeightTex").val($(OrderDetails).find("Weigth_OZTaxAmount").text());
                        }
                        else {
                            $("#txtMLTax").val('0.00');
                            $("#txtMLQty").val('0.00');
                            $("#txtWeightQty").val('0.00');
                            $("#txtWeightTex").val('0.00');
                        }

                        $("#txtTotalTax").val($(OrderDetails).find("TotalTax").text());
                        $("#txtGrandTotal").val($(OrderDetails).find("GrandTotal").text());
                        $("#txtPaidAmount").val($(OrderDetails).find("paidAmount").text());
                        $("#hdnPaidAmount").val($(OrderDetails).find("paidAmount").text());
                        $("#txtDeductionAmount").val($(OrderDetails).find("DeductionAmount").text());
                        $("#txtPastDue").val($(OrderDetails).find("PastDue").text());
                        $("#txtAdjustment").val($(OrderDetails).find("AdjustmentAmt").text());

                        var balancAmts = parseFloat(Number($(OrderDetails).find("PastDue").text()) + Number($(OrderDetails).find("PayableAmount").text())).toFixed(2);
                        var balancAmt = parseFloat(balancAmts - $(OrderDetails).find("paidAmount").text()).toFixed(2);
                        $("#txtBalanceAmount").val(balancAmt);


                        if (parseFloat($(OrderDetails).find("GrandTotal").text()) > parseFloat($(OrderDetails).find("DeductionAmount").text())) {
                            if (parseFloat($("#CreditMemoAmount").html()) > 0)//txtStoreCreditAmount
                            {
                                $("#txtStoreCreditAmount").removeAttr("disabled");
                            }
                        }
                        else {
                            $("#txtStoreCreditAmount").attr("disabled", true);
                        }
                        $("#txtStoreCreditAmount").val($(OrderDetails).find("CreditAmount").text());
                        $("#hdnStoreCreditAmount").val($(OrderDetails).find("CreditAmount").text());
                        $("#txtOrderRemarks").val($(OrderDetails).find("OrderRemarks").text());
                        $("#txtPaybleAmount").val($(OrderDetails).find("PayableAmount").text());
                        if ($(OrderDetails).find('CustomerType').text() == 3) {
                            $('.UnitPrice').hide();
                            $('.SRP').hide();
                            $('.GP').hide();
                            $('.NetPrice').hide();
                            $('#orderSummary').hide();
                        } else {
                            $('.UnitPrice').show();
                            $('.SRP').show();
                            $('.GP').show();
                            $('.NetPrice').show();
                            $('#orderSummary').show();
                        }
                        if (Number($(OrderDetails).find('StatusCode').text()) == 6) {
                            $('#btnEditOdrer').show();
                        } else {
                            $('#btnEditOdrer').hide();
                        }
                        if (parseFloat($(OrderDetails).find('StoreCredit').text()) > 0) {
                            $('#StoreCredit').text(parseFloat($(OrderDetails).find('StoreCredit').text()).toFixed(2));
                        }
                        $("#ddlCustomer").val($(OrderDetails).find('CustomerAutoId').text()).change();
                        $("#ddlTaxType").val($(OrderDetails).find("TaxType").text());

                        $("#ddlBillingAddress option:not(:first)").remove();
                        $.each(BillingAddress, function () {
                            $("#ddlBillingAddress").append("<option value='" + $(this).find("BillAddrAutoId").text() + "'>" + $(this).find("BillingAddress").text() + "</option>");
                        });
                        $("#ddlBillingAddress").val($(OrderDetails).find('BillAddrAutoId').text());
                        $("#ddlShippingAddress option:not(:first)").remove();
                        $.each(ShippingAddress, function () {
                            $("#ddlShippingAddress").append("<option value='" + $(this).find("ShipAddrAutoId").text() + "'>" + $(this).find("ShippingAddress").text() + "</option>");
                        });
                        $("#ddlShippingAddress").val($(OrderDetails).find('ShipAddrAutoId').text());
                        $("#tblProductDetail tbody tr").remove();
                        var row = $("#tblProductDetail thead tr").clone(true);
                        $.each(OrderItemDetails, function () {

                            $(".ProId", row).html($(this).find("ProductId").text() + "<span isFreeItem='" + $(this).find("isFreeItem").text() + "'></span>");

                            if (Number($(this).find("isFreeItem").text()) == 1) {
                                $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span>" + " <product class='badge badge badge-pill badge-success'>Free</product>");

                            } else if (Number($(this).find("IsExchange").text()) == 1) {
                                $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span>" + " <product class='badge badge badge-pill badge-primary'>Exchange</product>");
                            }
                            else if (Number($(this).find("Tax").text()) == 1) {
                                $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span>" + " <product class='badge badge badge-pill badge-danger'>Taxable</product>");
                            } else {
                                $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span>");
                            }
                            $(".UnitType", row).html("<span UnitAutoId='" + $(this).find("UnitAutoId").text() + "' QtyPerUnit='" + $(this).find("QtyPerUnit").text() + "'>" + $(this).find("UnitType").text() + " (" + $(this).find("QtyPerUnit").text() + " pcs" + ")</span>");
                            $(".ReqQty", row).html("<input type='text' maxlength='6' onchange='CheckAllocatedQty(this)' class='form-control input-sm border-primary text-center' onkeypress='return isNumberKey(event)' onkeyup='rowCal(this,1)' style='width:70px;' value='" + $(this).find("RequiredQty").text() + "' findoldqty='" + $(this).find("RequiredQty").text() + "'/>");
                            $(".PerPrice", row).text($(this).find("PerpiecePrice").text());
                            $(".TtlPcs", row).text($(this).find("TotalPieces").text());
                            $(".NetPrice", row).text($(this).find("NetPrice").text());
                            if (Number($(this).find("IsExchange").text()) == 1 && Number($(this).find("isFreeItem").text()) == 1) {
                                $(".UnitPrice", row).html("<input type='text' maxlength='6' class='form-control input-sm border-primary' onkeypress='return isNumberDecimalKey(event,this)' style='text-align:right;width:70px;' value='" + $(this).find("UnitPrice").text() + "' onchange='changePrice(this)' minprice=" + $(this).find("MinPrice").text() + " baseprice=" + $(this).find("Price").text() + "/>");
                            } else {
                                $(".UnitPrice", row).html("<input type='text' maxlength='6' class='form-control input-sm border-primary' onkeypress='return isNumberDecimalKey(event,this)' style='text-align:right;width:70px;' value='" + $(this).find("UnitPrice").text() + "' onchange='changePrice(this)' minprice=" + $(this).find("MinPrice").text() + " baseprice=" + $(this).find("Price").text() + " disabled/>");
                            }

                            $(".SRP", row).text($(this).find("SRP").text());
                            $(".GP", row).text($(this).find("GP").text());
                            $(".OM_MinPrice", row).text($(this).find("OM_MinPrice").text());
                            $(".OM_CostPrice", row).text($(this).find("OM_CostPrice").text());
                            $(".OM_BasePrice", row).text($(this).find("BasePrice").text());
                            $(".Del_ItemTotal", row).text($(this).find("Del_ItemTotal").text());
                            $(".Oim_Discount", row).html("<input type='text' onfocus='this.select()' class='form-control input-sm border-primary text-center' onkeypress='return isNumberDecimalKey(event,this)'  maxlength='6'  onchange='rowCal(this,1)'  onkeyup='rowCal(this,1);' style='width:70px;' value='" + parseFloat($(this).find("Del_discount").text()).toFixed(2) + "' />");

                            if (Number($(this).find("Tax").text()) == 1) {
                                $(".TaxRate", row).html('<span typetax="1" MLQty="' + $(this).find("UnitMLQty").text() + '" WeightOz="' + $(this).find("WeightOz").text() + '"></span>' + '<TaxRate style="font-weight: bold" class="la la-check-circle success center"></TaxRate>');
                            } else {
                                $(".TaxRate", row).html('<span typetax="0" MLQty="' + $(this).find("UnitMLQty").text() + '" WeightOz="' + $(this).find("WeightOz").text() + '"></span>');
                            }

                            if (Number($(this).find("IsExchange").text()) == 1) {
                                $(".IsExchange", row).html('<span IsExchange="1"></span>' + '<IsExchange style="font-weight: bold" class="la la-check-circle success center"></IsExchange>');
                            } else {
                                $(".IsExchange", row).html('<span IsExchange="0"></span>');
                            }



                            $(".Barcode", row).text($(this).find("Barcode").text());

                            $(".Action", row).html("<a href='javascript:;' id='deleterow' onclick='deleterow(this)'><span class='ft-x'></span></a>");
                            $('#tblProductDetail tbody').append(row);
                            row = $("#tblProductDetail tbody tr:last").clone(true);

                        });
                        $('#emptyTable').hide();
                        $('input').attr('disabled', true);
                        $('select').attr('disabled', true);
                        if ($(OrderDetails).find('CustomerType').text() == 3) {
                            $('#btnGenOrderCCNew').attr('onclick', 'PrintPackingSlip(1)');
                        }
                        $('#btnGenOrderCCNew').show();

                        $('#btnAdd').closest('.card-body').hide();
                        $('#btnSave').hide();
                        $('#btnReset').hide();
                        $('.Action').hide();
                        $('textarea').attr('disabled', true);
                        $('#btnBackOdrer').hide();
                        $('#btnUpdateOrder').hide();
                        $("#tblCreditMemoList tbody tr").remove();
                        if (CreditDetails.length > 0) {
                            var TotalDue = 0.00;
                            var rowc1 = $("#tblCreditMemoList thead tr").clone(true);
                            $.each(CreditDetails, function (index) {
                                $(".SRNO", rowc1).text(Number(index) + 1);
                                if ($(this).find('OrderAutoId').text() != '') {
                                    $(".Action", rowc1).html("<input type='checkbox' OrderAutoId='" + $(this).find('OrderAutoId').text() + "' disabled checked name='creditMemo' onclick='funcheckprop(this)' value=" + $(this).find("CreditAutoId").text() + "> ");
                                } else {
                                    $(".Action", rowc1).html("<input type='checkbox' OrderAutoId='" + $(this).find('OrderAutoId').text() + "' name='creditMemo' onclick='funcheckprop(this)' value=" + $(this).find("CreditAutoId").text() + "> ");
                                }
                                $(".CreditNo", rowc1).html("<a target='_blank' href='/Sales/CreditMemo.aspx?PageId=" + $(this).find("CreditAutoId").text() + "'>" + $(this).find("CreditNo").text() + "</a>");
                                $(".CreditDate", rowc1).text($(this).find("CreditDate").text());
                                $(".ReturnValue", rowc1).text($(this).find("ReturnValue").text());
                                $(".CreditType", rowc1).text($(this).find("CreditType").text());
                                TotalDue = parseFloat(TotalDue) + parseFloat($(this).find("ReturnValue").text());

                                $("#tblCreditMemoList tbody").append(rowc1)
                                rowc1 = $("#tblCreditMemoList tbody tr:last").clone(true);
                            })
                            $("#TotalDue").text(TotalDue.toFixed(2));
                            $(".Action").show();
                            $('#CusCreditMemo').show();
                            $('#dCuCreditMemo').show();
                        } else {
                            $('#CusCreditMemo').hide();
                            $('#dCuCreditMemo').hide();
                        }
                    } else {
                        location.href = '/';
                    }
                },
                failure: function (result) {
                    console.log(result.d);
                },
                error: function (result) {
                    console.log(result.d);
                }
            });
        }
        var BUnitAutoId = 0;
        var validcheck = 0;

        function invalidBarCode() {
            $("#txtBarcode").val('');
            $("#txtBarcode").focus();
        }
        var BarcodeData = [];
        function readBarcode() {
            var Barcodeitem = {
                "IsExchange": $("#chkExchange").prop('checked') == true ? 1 : 0,
                "IsFreeItem": $("#chkFreeItem").prop('checked') == true ? 1 : 0,
                "IsTaxable": $("#chkIsTaxable").prop('checked') == true ? 1 : 0,
                "ReqQty": $('#txtReqQty').val() == "" ? 1 : $('#txtReqQty').val(),
                "BarCode": $("#txtBarcode").val(),
                "Status": 0,
            };
            BarcodeData.push(Barcodeitem);
            $("#txtBarcode").val('');
            $("#txtBarcode").focus()
            for (var i = 0; i < BarcodeData.length; i++) {
                var item = BarcodeData[i];
                if (item.Status == 0) {
                    scanBarcode(item.IsExchange, item.IsFreeItem, item.IsTaxable, item.ReqQty, item.BarCode);
                    item.Status = 1;
                }
            }
        }
        function scanBarcode(IsExchange, IsFreeItem, IsTaxable, ReqQty, BarCode) {
            //var Barcode = $("#txtBarcode").val().trim();
            if (BarCode != "") {
                //var chkIsTaxable = $('#chkIsTaxable').prop('checked');
                //var chkExchange = $('#chkExchange').prop('checked');
                //var chkFreeItem = $('#chkFreeItem').prop('checked');
                //var IsExchange = 0, IsTaxable = 0, IsFreeItem = 0, chkcount = 0;
                var chkcount = 0;
                if (IsTaxable == 1) {
                    chkcount = 1;
                }
                if (IsExchange == 1) {
                    chkcount = chkcount + 1;
                }
                if (IsFreeItem == 1) {
                    chkcount = chkcount + 1;
                }
                if (chkcount > 1) {
                    toastr.error('Please check only one checkbox.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    $("#txtBarcode").focus();
                    BarcodeData = [];
                    return;
                }
                //$("#txtBarcode").blur();
                var data = {
                    CustomerAutoId: $("#ddlCustomer").val(),
                    Barcode: BarCode,
                    DraftAutoId: $("#DraftAutoId").val(),
                    ShippingType: $('#ddlShippingType  option:selected').val(),
                    DeliveryDate: "",
                    ReqQty: ReqQty,
                    IsTaxable: IsTaxable,
                    IsExchange: IsExchange,
                    IsFreeItem: IsFreeItem,
                    Remarks: $('#txtOrderRemarks').val(),
                    Oim_Discount: 0
                }
                $.ajax({
                    type: "POST",
                    url: "OrderNew.aspx/GetBarCodeDetails",
                    data: JSON.stringify({ dataValues: JSON.stringify(data) }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {

                    },
                    complete: function () {

                    },
                    success: function (response) {
                        if (response.d != "Session Expired") {
                            if (response.d == 'DifferentUnit') {
                                $('#txtBarcode').removeAttr('onchange', 'readBarcode()');
                                $('#txtBarcode').attr('onchange', 'invalidBarCode()');
                                swal({
                                    title: "",
                                    text: "You can't add different unit of added product.",
                                    icon: "error",
                                    closeOnClickOutside: false
                                }).then(function () {
                                    $("#txtBarcode").focus();
                                    $("#txtBarcode").attr('onchange', 'readBarcode()');
                                });;
                                $("#txtBarcode").focus();
                                invalidBarCode();
                                BarcodeData = [];
                            }
                            else if (response.d == 'Barcode') {
                                $("#yes_audio")[0].play();
                                $('#txtBarcode').removeAttr('onchange');
                                $('#txtBarcode').attr('onchange', 'invalidBarCode()');
                                swal({
                                    title: "",
                                    text: "Barcode (" + BarCode+") does not exists.",
                                    icon: "error",
                                    closeOnClickOutside: false
                                }).then(function () {
                                    $("#txtBarcode").focus();
                                    $("#txtBarcode").attr('onchange', 'readBarcode()');
                                });
                                $("#panelProduct select").val(0);
                                $("#txtQuantity, #txtTotalPieces").val("0");
                                $("#alertBarcodeCount").hide();
                                $("#txtBarcode").focus();
                                invalidBarCode();
                                BarcodeData = [];
                            }
                            else if (response.d == 'Product Inactive') {
                                $('#txtBarcode').removeAttr('onchange');
                                $('#txtBarcode').attr('onchange', 'invalidBarCode()');
                                swal({
                                    title: "",
                                    text: "Inactive Product can't be sold.",
                                    icon: "error",
                                    closeOnClickOutside: false
                                }).then(function () {
                                    $("#txtBarcode").focus();
                                    $("#txtBarcode").attr('onchange', 'readBarcode()');
                                });
                                $("#txtBarcode").focus();
                                invalidBarCode();
                                BarcodeData = [];
                            }
                            else if (response.d == 'Stock is not Available') {
                                $('#txtBarcode').removeAttr('onchange');
                                $('#txtBarcode').attr('onchange', 'invalidBarCode()');
                                $("#Stock")[0].play();
                                swal({
                                    title: "",
                                    text: "Stock not available.",
                                    icon: "error",
                                    closeOnClickOutside: false
                                }).then(function () {
                                    $("#txtBarcode").focus();
                                    $("#txtBarcode").attr('onchange', 'readBarcode()');
                                });
                                $("#txtBarcode").focus();
                                invalidBarCode();
                                BarcodeData = [];
                            }
                            else {
                                var xmldoc = $.parseXML(response.d);
                                var product = $(xmldoc).find("Table");
                                if (product.length > 0) {
                                    var productAutoId = $(product).find('ProductAutoId').text();
                                    var unitAutoId = $(product).find('UnitType').text();
                                    var UnitQty = $(product).find('UnitQty').text();
                                    $("#DraftAutoId").val($(product).find('DraftAutoId').text());

                                    if ($(product).find('Message').text() == "Less") {
                                        $("#mdAllowQtyConfirmation").modal('show');
                                        AvailableAllocatedQty = $(product).find('AvailableQty').text();
                                        RequiredQty = $(product).find('RequiredQty').text();
                                        PrdtId = $(product).find('ProductAutoId').text();
                                        $("#txtBarcode").attr('disabled',true);
                                        $("#txtBarcode").removeAttr('onchange');
                                    }
                                    else {
                                        var curentStock = $(product).find('ProductStock').text();
                                        var TQty = 0;
                                        $("#tblProductDetail tbody tr").each(function () {
                                            if ($(this).find(".ProName > span").attr("productautoid") == productAutoId) {
                                                var reqQty = Number($(this).find(".ReqQty input").val()) || 1;
                                                TQty += reqQty;
                                            }
                                        });
                                        TQty = (TQty + Number($("#txtReqQty").val())) * Number(UnitQty);
                                        if (curentStock < TQty) {
                                            $('#txtBarcode').removeAttr('onchange');
                                            $('#txtBarcode').attr('onchange', 'invalidBarCode()');
                                            $("#Stock")[0].play();
                                            swal({
                                                title: "",
                                                text: "Stock not available.",
                                                icon: "error",
                                                closeOnClickOutside: false
                                            }).then(function () {
                                                $("#txtBarcode").focus();
                                                $("#txtBarcode").attr('onchange', 'readBarcode()');
                                            });
                                            $("#txtBarcode").focus();
                                            invalidBarCode();
                                            BarcodeData = [];
                                        }
                                        else {
                                            var flag1 = false; var showmsg = 0,netprice=0.00;
                                            $("#tblProductDetail tbody tr").each(function () {
                                                if ($(this).find(".ProName > span").attr("productautoid") == productAutoId && $(this).find(".UnitType > span").attr("unitautoid") == unitAutoId
                                                    && $(this).find(".IsExchange > span").attr("IsExchange") == IsExchange && $(this).find(".ProId > span").attr("IsFreeItem") == IsFreeItem) {
                                                    var Qtyreq = $("#txtReqQty").val() || 0;
                                                    var reqQty = Number($(this).find(".ReqQty input").val()) + Number(Qtyreq);
                                                    $(this).find(".ReqQty input").val(reqQty);
                                                    $(this).find(".ReqQty input").attr("FindOldQty", reqQty);
                                                    flag1 = true;

                                                    netprice = parseFloat($(this).find(".UnitPrice input").val());
                                                    netprice = parseFloat(reqQty * netprice).toFixed(2);
                                                    $(this).find(".NetPrice").text(netprice);

                                                    //rowCal($(this).find(".ReqQty > input"), 0);
                                                    $('#tblProductDetail tbody tr:first').before($(this));
                                                }

                                                if ($(this).find(".ProName span").attr('ProductAutoId') == productAutoId && $(this).find(".UnitType span").attr('UnitAutoId') != unitAutoId
                                                    && $(this).find(".ProId > span").attr("isfreeitem") == IsFreeItem && $(this).find(".ProId > span").attr("IsExchange") == '0' && IsExchange == 0) {

                                                    showmsg = 4;
                                                }
                                                if ($(this).find(".ProName span").attr('ProductAutoId') == productAutoId && $(this).find(".UnitType span").attr('UnitAutoId') != unitAutoId
                                                    && $(this).find(".IsExchange > span").attr("IsExchange") == IsExchange && $(this).find(".ProId > span").attr("isfreeitem") == '0' && IsFreeItem == 0) {
                                                    showmsg = 4;
                                                }
                                            });

                                            if (showmsg == 4) {
                                                $('#txtBarcode').removeAttr('onchange');
                                                $('#txtBarcode').attr('onchange', 'invalidBarCode()');
                                                swal({
                                                    title: "",
                                                    text: "You can't add different unit of added product.",
                                                    icon: "error",
                                                    closeOnClickOutside: false
                                                }).then(function () {
                                                    $("#txtBarcode").focus();
                                                    $("#txtBarcode").attr('onchange', 'readBarcode()');
                                                });
                                                $("#txtBarcode").focus();
                                                invalidBarCode();
                                                BarcodeData = [];
                                                return;
                                            }
                                            if (!flag1) {

                                                var row = $("#tblProductDetail thead tr").clone(true);
                                                $(".ProId", row).html($(product).find('ProductId').text() + "<span IsFreeItem='" + IsFreeItem + "'></span>");
                                                if (IsFreeItem == 1) {
                                                    $(".ProName", row).html("<span ProductAutoId='" + productAutoId + "'>" + $(product).find('ProductName').text() + "</span>" + " <product class='badge badge badge-pill badge-success'>Free</product>");

                                                } else if (IsExchange == 1) {
                                                    $(".ProName", row).html("<span ProductAutoId='" + productAutoId + "'>" + $(product).find('ProductName').text() + "</span>" + " <product class='badge badge badge-pill badge-primary'>Exchange</product>");

                                                } else if (IsTaxable == 1) {
                                                    $(".ProName", row).html("<span ProductAutoId='" + productAutoId + "'>" + $(product).find('ProductName').text() + "</span>" + " <product class='badge badge badge-pill badge-danger'>Taxable</product>");

                                                } else {
                                                    $(".ProName", row).html("<span ProductAutoId='" + productAutoId + "'>" + $(product).find('ProductName').text() + "</span>");
                                                }
                                                $(".UnitType", row).html("<span UnitAutoId='" + unitAutoId + "' QtyPerUnit='" + $(product).find('UnitQty').text() + "'>" + $(product).find('UnitName').text() + "</span>");
                                                $(".ReqQty", row).html("<input type='text' onchange='CheckAllocatedQty(this)' maxlength='6' onkeypress='return isNumberKey(event)' class='form-control input-sm border-primary text-center' onkeyup='rowCal(this,1)' style='width:70px;' value='" + $("#txtReqQty").val() + "' FindOldQty='" + $("#txtReqQty").val() + "'/>");
                                                $(".Barcode", row).text(BarCode);
                                                $(".QtyShip", row).html("<input type='text' class='form-control input-sm border-primary' onkeypress='return isNumberKey(event)' style='width:70px;' disabled value='0' onkeyup='rowCal(this,1);shipVsReq(this)' />");
                                                $(".TtlPcs", row).text(parseInt($("#txtReqQty").val()) * $(product).find('UnitQty').text());
                                                if ($('#chkExchange').prop('checked') == false && $('#chkFreeItem').prop('checked') == false) {
                                                    if ($(product).find('CustomPrice').text() == "") {
                                                        $(".UnitPrice", row).html("<input type='text' maxlength='6' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm border-primary' onchange='changePrice(this)' style='width:70px;text-align:right;' value='" + $(product).find('Price').text() + "' minprice='" + $(product).find('MinPrice').text() + "' BasePrice='" + $(product).find('Price').text() + "' />");
                                                    } else {
                                                        $(".UnitPrice", row).html("<input type='text' maxlength='6' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm border-primary' onchange='changePrice(this)' style='width:70px;text-align:right;' value='" + (parseFloat($(product).find('CustomPrice').text()) >= parseFloat($(product).find('MinPrice').text()) ? $(product).find('CustomPrice').text() : $(product).find('Price').text()) + "' minprice='" + $(product).find('MinPrice').text() + "' BasePrice='" + $(product).find('Price').text() + "' />");
                                                    }
                                                } else {
                                                    $(".UnitPrice", row).html("<input type='text' maxlength='6' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm border-primary' onchange='changePrice(this)' style='width:70px;text-align:right;' value='0.00' minprice='0.00' BasePrice='0.00' disabled/>");
                                                }
                                                $(".SRP", row).text($(product).find('SRP').text());
                                                $(".GP", row).text($(product).find('GP').text());
                                                $(".Oim_Discount", row).html("<input type='text' onfocus='this.select()' class='form-control input-sm border-primary ' style='width:70px;text-align:right;'  onchange='rowCal(this,1)'  onkeyup='rowCal(this,1);'  value='" + $(product).find("Oim_Discount").text() + "'  />");
                                                $(".Del_ItemTotal", row).html($(product).find("Del_ItemTotal").text());
                                                $(".OM_MinPrice", row).text($(product).find('MinPrice').text());
                                                $(".OM_CostPrice", row).text($(product).find('CostPrice').text());
                                                $(".OM_BasePrice", row).text($(product).find('BasePrice').text());
                                                var taxType = '', TypeTax = 0;
                                                var MLQty = $(product).find('MLQty').text();
                                                if ($('#chkExchange').prop('checked') == true || $('#chkFreeItem').prop('checked') == true) {
                                                    MLQty = 0.00;
                                                }
                                                if ($('#chkIsTaxable').prop('checked') == true) {
                                                    taxType = 'Taxable';
                                                    TypeTax = 1;
                                                }
                                                var IsExchange1 = '', oz = 0;
                                                if ($('#chkExchange').prop('checked') == true) {
                                                    IsExchange1 = 'Exchange';
                                                }
                                                if ($(product).find('ML').text() == '0') {
                                                    MLQty = 0;
                                                }
                                                if ($(product).find('OZ').text() == '0') {
                                                    oz = 0;
                                                }
                                                else {
                                                    oz = $(product).find('WeightOz').text();
                                                }

                                                if (TypeTax == 1) {
                                                    $(".TaxRate", row).html("<span TypeTax='" + TypeTax + "' MLQty='" + MLQty + "' WeightOz='" + oz + "'> </span>" + '<TaxRate style="font-weight: bold" class="la la-check-circle success center"></TaxRate>');
                                                } else {
                                                    $(".TaxRate", row).html("<span TypeTax='" + TypeTax + "' MLQty='" + MLQty + "' WeightOz='" + oz + "'> </span>");
                                                }
                                                if (IsExchange == 1) {
                                                    $(".IsExchange", row).html("<span IsExchange='" + IsExchange + "'> </span>" + '<IsExchange style="font-weight: bold" class="la la-check-circle success center"></IsExchange>');
                                                } else {
                                                    $(".IsExchange", row).html("<span IsExchange='" + IsExchange + "'> </span>");
                                                }
                                                netprice = parseFloat($(row).find(".UnitPrice input").val());
                                                netprice = parseFloat(Number($("#txtReqQty").val()) * netprice).toFixed(2);

                                                $(".NetPrice", row).text(netprice);
                                                $(".Action", row).html("<a href='javascript:;' id='deleterow' onclick='deleterow(this)'><span class='ft-x'></span></a>");
                                                if ($('#tblProductDetail tbody tr').length > 0) {
                                                    $('#tblProductDetail tbody tr:first').before(row);
                                                }
                                                else {
                                                    $('#tblProductDetail tbody').append(row);
                                                }
                                                //rowCal(row.find(".ReqQty > input"), 0);
                                                $("#txtQuantity, #txtTotalPieces").val("0");
                                                if ($('#tblProductDetail tbody tr').length > 0) {
                                                    $('#emptyTable').hide();
                                                    $("#ddlCustomer").attr('disabled', true);
                                                }
                                                toastr.success('Product added successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                                                $('#chkFreeItem').removeAttr('disabled');
                                                $("#txtBarcode").focus();

                                            } else {
                                                $("#txtBarcode").focus();
                                                toastr.success('Product added successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                                            }
                                            $("#txtBarcode").focus();
                                        }
                                    }
                                }
                                //$("#chkExchange").prop('checked', false);
                                //$("#chkFreeItem").prop('checked', false);
                                //$("#chkIsTaxable").prop('checked', false);
                                $("#txtReqQty").val("1");
                                calTotalAmount();
                            }
                        }
                    },
                    failure: function (result) {
                        console.log(JSON.parse(result.responseText).d);
                    },
                    error: function (result) {
                        console.log(JSON.parse(result.responseText).d);
                    }
                });
            } else {
                $("#ddlProduct").val(0).change();
                $("#alertBarcodeCount").hide();
                $("#ddlProduct").attr('disabled', false);
                $("#ddlUnitType").attr('disabled', false);
            }
        }
        var price, taxRate, SRP, minPrice, CustomPrice, GP; // ---------- Global Variables ---------------

        function ChangeUnit() {


            $('#chkExchange').prop('checked', false);
            $('#chkIsTaxable').prop('checked', false);
            $('#chkFreeItem').prop('checked', false);
            if ($("#ddlUnitType option:selected").attr('eligibleforfree') == 1) {
                $('#chkFreeItem').attr('disabled', false);
            } else {
                $('#chkFreeItem').attr('disabled', true);
            }
            $("#alertStockQty").hide();
            var data = {
                ProductAutoId: $("#ddlProduct").val(),
                UnitAutoId: $("#ddlUnitType").val(),
                CustAutoId: $("#ddlCustomer").val()
            };

            if (data.UnitAutoId != 0) {
                $.ajax({
                    type: "POST",
                    url: "OrderNew.aspx/selectQtyPrice",
                    data: JSON.stringify({ dataValues: JSON.stringify(data) }),
                    contentType: "application/json; charset=utf-8",
                    datatype: "json",
                    beforeSend: function () {
                        $.blockUI({
                            message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                            overlayCSS: {
                                backgroundColor: '#FFF',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (response) {
                        if (response.d != "Session Expired") {
                            var xmldoc = $.parseXML(response.d);
                            var stockAndPrice = $(xmldoc).find("Table");
                            price = stockAndPrice.find("Price").text();
                            minPrice = stockAndPrice.find("MinPrice").text();
                            CostPrice = stockAndPrice.find("CostPrice").text();
                            CustomPrice = stockAndPrice.find("CustomPrice").text();
                            taxRate = stockAndPrice.find("TaxRate").text();
                            SRP = stockAndPrice.find("SRP").text();
                            GP = stockAndPrice.find("GP").text();
                            Stock = stockAndPrice.find("Stock").text();
                            if ($("#ddlUnitType").val() != 0) {
                                $("#alertStockQty").text('');
                                if (Number($("#ddlUnitType").val()) == 1) {
                                    $("#alertStockQty").append(" [ Stock : " + $(stockAndPrice).find("Stock").text() + " case ] ," +
                                        " [ Base Price : $" + price + " ]");
                                } else if (Number($("#ddlUnitType").val()) == 2) {
                                    $("#alertStockQty").append(" [ Stock : " + $(stockAndPrice).find("Stock").text() + " box ] ," +
                                        " [ Base Price : $" + price + " ]");
                                } else {
                                    $("#alertStockQty").append(" [ Stock : " + $(stockAndPrice).find("Stock").text() + " pcs ] ," +
                                        " [ Base Price : $" + price + " ]");
                                }
                                $("#alertStockQty").show();
                                validcheck = 0;

                            } else {
                                $("#alertStockQty").hide();
                            }
                        } else {
                            location.href = '/';
                        }

                    },
                    error: function (result) {
                        console.log(JSON.parse(result.responseText).d);
                    },
                    failure: function (result) {
                        console.log(JSON.parse(result.responseText).d);
                    }
                });
            }
        };                                                 //Check Stock 

        /*-------------------------------------------------------------------------------------------------------------------------------*/

        //                                                          Add Row to table
        /*-------------------------------------------------------------------------------------------------------------------------------*/
        function AddItemList() {
            if (checkRequiredField1()) {
                var chkIsTaxable = $('#chkIsTaxable').prop('checked');
                var chkExchange = $('#chkExchange').prop('checked');
                var chkFreeItem = $('#chkFreeItem').prop('checked');
                var MLQty = $("#ddlProduct option:selected").attr('MLQty');
                var weightoz = $("#ddlProduct option:selected").attr('weightoz');
                var reqQty = parseInt($("#txtReqQty").val()) || 0;
                var IsExchange = 0, IsTaxable = 0, IsFreeItem = 0;
                var chkcount = 0;

                if (reqQty == 0) {
                    toastr.error('Please enter a valid quantity.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    $("#txtReqQty").addClass('border-warning');
                    return;
                }
                else {
                    $("#txtReqQty").removeClass('border-warning');
                }
                if (chkIsTaxable == true) {
                    IsTaxable = 1;
                    chkcount = 1;
                }
                if (chkExchange == true) {
                    IsExchange = 1;
                    chkcount = chkcount + 1;
                    MLQty = 0.00;
                    weightoz = 0.00;
                }
                if (chkFreeItem == true) {
                    IsFreeItem = 1;
                    chkcount = chkcount + 1;
                    MLQty = 0.00;
                    weightoz = 0.00;
                }
                if (MLTaxType == "0") {
                    MLQty = 0.00;
                }

                if (chkcount > 1) {
                    toastr.error('Please check only one checkbox.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    return;
                }

                var flag1 = false, validatecheck = true;

                if ($("#ddlProduct option:selected").val() == '0' && $("#ddlUnitType option:selected").val() == '0') {
                    validatecheck = false;
                }


                var productAutoId = $("#ddlProduct option:selected").val();
                var unitAutoId = $("#ddlUnitType option:selected").val();

                var Draftdata = {
                    DraftAutoId: $('#DraftAutoId').val(),
                    CustomerAutoId: $('#ddlCustomer').val(),
                    ShippingType: $('#ddlShippingType  option:selected').val(),
                    productAutoId: productAutoId,
                    unitAutoId: unitAutoId,
                    ReqQty: $("#txtReqQty").val(),
                    IsTaxable: chkIsTaxable,
                    IsExchange: IsExchange,
                    IsFreeItem: IsFreeItem,
                    MLQty: MLQty,
                    Remarks: $('#txtOrderRemarks').val(),
                    Oim_Discount: 0
                }
                $.ajax({
                    type: "POST",
                    url: "orderNew.aspx/SaveDraftOrder",
                    data: JSON.stringify({ dataValues: JSON.stringify(Draftdata) }),
                    contentType: "application/json; charset=utf-8",
                    datatype: "json",
                    async: true,
                    beforeSend: function () {
                        $.blockUI({
                            message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                            overlayCSS: {
                                backgroundColor: '#FFF',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (response) {

                        if (response.d != "Session Expired") {
                            if (response.d == 'DifferentUnit') {
                                $('#txtBarcode').removeAttr('onchange', 'readBarcode()');
                                $('#txtBarcode').attr('onchange', 'invalidBarCode()');
                                swal({
                                    title: "",
                                    text: "You can't add different unit of added product.",
                                    icon: "error",
                                    closeOnClickOutside: false
                                }).then(function () {
                                    $("#txtBarcode").val("");
                                    $("#txtBarcode").focus();
                                    $("#txtBarcode").attr('onchange', 'readBarcode()');
                                });;
                                $("#txtBarcode").focus();
                                invalidBarCode();
                            }
                            else if (response.d == "Stock") {
                                $('#txtBarcode').removeAttr('onchange');
                                $('#txtBarcode').attr('onchange', 'invalidBarCode()');
                                $("#Stock")[0].play();
                                swal({
                                    title: "",
                                    text: "Stock not available.",
                                    icon: "error",
                                    closeOnClickOutside: false
                                }).then(function () {
                                    $("#txtBarcode").val("");
                                    $("#txtBarcode").focus();
                                    $("#txtBarcode").attr('onchange', 'readBarcode()');
                                });
                                $("#txtBarcode").focus();
                                invalidBarCode();
                            }
                            else if (response.d == "false") {
                                return;
                            }
                            else {
                                var xmldoc = $.parseXML(response.d);
                                var ItemDetails = $(xmldoc).find("Table");
                                if (ItemDetails.length > 0) {
                                    if ($(ItemDetails).find('Message').text() == "Less") {
                                        $("#tblProductDetail tbody tr").each(function () {
                                            if ($(this).find(".ProName > span").attr("productautoid") == $("#ddlProduct option:selected").val()) {
                                                $(this).find(".ReqQty > input").val($(this).find(".ReqQty > input").attr("findoldqty"));
                                                $(this).find(".ReqQty > input").attr("findoldqty", $(this).find(".ReqQty > input").attr("findoldqty"));
                                                $("#ddlProduct").select2('val', '0');
                                                $("#ddlUnitType").val(0);
                                                $("#txtReqQty").val("1");
                                            }
                                        });
                                        $("#mdAllowQtyConfirmation").modal('show');
                                        AvailableAllocatedQty = $(ItemDetails).find('AvailableQty').text();
                                        RequiredQty = $(ItemDetails).find('RequiredQty').text();
                                        PrdtId = $(ItemDetails).find('ProductAutoId').text();
                                    }
                                    else {

                                        var curentStock = $(ItemDetails).find('stock').text();
                                        var TQty = 0;
                                        $("#tblProductDetail tbody tr").each(function () {
                                            if ($(this).find(".ProName > span").attr("productautoid") == productAutoId) {
                                                var reqQty = Number($(this).find(".ReqQty input").val()) || 0;
                                                TQty += parseInt($(this).find(".UnitType  span").attr("qtyperunit")) * reqQty;
                                            }
                                        });
                                        TQty = TQty + (Number($("#txtReqQty").val()) * parseInt($("#ddlUnitType option:selected").attr("qtyperunit")))
                                        if (curentStock < TQty) {
                                            $('#txtBarcode').removeAttr('onchange');
                                            $('#txtBarcode').attr('onchange', 'invalidBarCode()');
                                            $("#Stock")[0].play();
                                            swal({
                                                title: "",
                                                text: "Stock not available.",
                                                icon: "error",
                                                closeOnClickOutside: false
                                            }).then(function () {
                                                $("#txtBarcode").val("");
                                                $("#txtBarcode").focus();
                                                $("#txtBarcode").attr('onchange', 'readBarcode()');
                                            });
                                            $("#txtBarcode").focus();
                                            invalidBarCode();
                                        }
                                        else {
                                            var shwmsg = 0, oldqty = 0, freenotifi = 0;
                                            $("#tblProductDetail tbody tr").each(function () {
                                                if ($(this).find(".ProName > span").attr("productautoid") == productAutoId && $(this).find(".UnitType > span").attr("unitautoid") == unitAutoId && $(this).find(".ProId > span").attr("isfreeitem") == IsFreeItem
                                                    && $(this).find(".IsExchange > span").attr("isexchange") == IsExchange) {
                                                    var reqQty = Number($(this).find(".ReqQty input").val()) + Number($("#txtReqQty").val());
                                                    $(this).find(".ReqQty input").val(reqQty);
                                                    $(this).find(".ReqQty input").attr("FindOldQty", reqQty);
                                                    flag1 = true;
                                                    $('#tblProductDetail tbody tr:first').before($(this));
                                                }
                                                if ($(this).find(".ProName span").attr('ProductAutoId') == productAutoId && $(this).find(".UnitType span").attr('UnitAutoId') != unitAutoId
                                                    && $(this).find(".ProId > span").attr("isfreeitem") == IsFreeItem && $(this).find(".IsExchange > span").attr("isexchange") == '0' && IsExchange == 0) {
                                                    shwmsg = 2;

                                                }
                                                if ($(this).find(".ProName span").attr('ProductAutoId') == productAutoId && $(this).find(".UnitType span").attr('UnitAutoId') != unitAutoId
                                                    && $(this).find(".IsExchange > span").attr("isexchange") == IsExchange && $(this).find(".ProId > span").attr("isfreeitem") == '0' && IsFreeItem == 0) {
                                                    shwmsg = 2;
                                                }
                                            });
                                            if (shwmsg == 2) {
                                                $('#txtBarcode').removeAttr('onchange', 'readBarcode()');
                                                $('#txtBarcode').attr('onchange', 'invalidBarCode()');
                                                swal({
                                                    title: "",
                                                    text: "You can't add different unit of added product.",
                                                    icon: "error",
                                                    closeOnClickOutside: false
                                                }).then(function () {
                                                    $("#txtBarcode").val("");
                                                    $("#txtBarcode").focus();
                                                    $("#txtBarcode").attr('onchange', 'readBarcode()');
                                                });;
                                                $("#txtBarcode").focus();
                                                invalidBarCode();
                                                return;
                                            }
                                            $('#DraftAutoId').val($(ItemDetails).find('DraftAutoId').text());
                                            if (!flag1) {
                                                var product = $("#ddlProduct option:selected").text().split("--");
                                                var row = $("#tblProductDetail thead tr").clone(true);
                                                $(".ProId", row).html(product[0] + "<span IsFreeItem='" + IsFreeItem + "'></span>");
                                                if (IsFreeItem == 1) {
                                                    $(".ProName", row).html("<span ProductAutoId='" + $("#ddlProduct option:selected").val() + "'>" + product[1] + "</span>" + " <product class='badge badge badge-pill badge-success'>Free</product>");

                                                } else if (IsExchange == 1) {
                                                    $(".ProName", row).html("<span ProductAutoId='" + $("#ddlProduct option:selected").val() + "'>" + product[1] + "</span>" + " <product class='badge badge badge-pill badge-primary'>Exchange</product>");
                                                }
                                                else if (IsTaxable == 1) {
                                                    $(".ProName", row).html("<span ProductAutoId='" + $("#ddlProduct option:selected").val() + "'>" + product[1] + "</span>" + " <product class='badge badge badge-pill badge-danger'>Taxable</product>");
                                                } else {
                                                    $(".ProName", row).html("<span ProductAutoId='" + $("#ddlProduct option:selected").val() + "'>" + product[1] + "</span>");
                                                }


                                                $(".UnitType", row).html("<span UnitAutoId='" + $("#ddlUnitType option:selected").val() + "' QtyPerUnit='" + $("#ddlUnitType option:selected").attr("QtyPerUnit") + "'>" + $("#ddlUnitType option:selected").text() + "</span>");
                                                if ($("#hiddenOrderStatus").val() == "3") {
                                                    $(".ReqQty", row).html('<input type="text" maxlength="6"  onchange="CheckAllocatedQty(this)" onkeypress="return isNumberKey(event)" class="form-control input-sm border-primary text-center" value="' + $("#txtReqQty").val() + '"  FindOldQty="' + $("#txtReqQty").val() + '"/>');
                                                }
                                                else {
                                                    $(".ReqQty", row).html('<input type="text" maxlength="6"  onchange="CheckAllocatedQty(this)" onkeypress="return isNumberKey(event)" class="form-control input-sm border-primary text-center"  onkeyup="rowCal(this,1)" style="width:70px;" value="' + $("#txtReqQty").val() + '"   FindOldQty="' + $("#txtReqQty").val() + '"/>');
                                                }
                                                $(".TtlPcs", row).text(parseInt($("#txtReqQty").val()) * parseInt($("#ddlUnitType option:selected").attr("QtyPerUnit")));
                                                if ($('#chkExchange').prop('checked') == false && $('#chkFreeItem').prop('checked') == false) {
                                                    $(".UnitPrice", row).html("<input type='text' maxlength='6' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm border-primary' onchange='changePrice(this)' style='width:70px;text-align:right;' value=" + $(ItemDetails).find('UnitPrice').text() + " minprice=" + $(ItemDetails).find('minPrice').text() + " BasePrice=" + $(ItemDetails).find('BasePrice').text() + " />");
                                                } else {
                                                    $(".UnitPrice", row).html("<input type='text' maxlength='6' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm border-primary' onchange='changePrice(this)' style='width:70px;text-align:right;' value='0.00' minprice='0.00' BasePrice='0.00' disabled/>");
                                                }
                                                $(".SRP", row).text(SRP);
                                                $(".GP", row).text(GP);
                                                $(".Oim_Discount", row).html("<input type='text' onfocus='this.select()' class='form-control input-sm border-primary '  style='width:70px;text-align:right;' onkeypress='return isNumberDecimalKey(event,this)'  onkeypress='return isNumberDecimalKey(event,this)'  maxlength='6' onchange='rowCal(this,1)'  onkeyup='rowCal(this,1);'  value='" + $(ItemDetails).find('Oim_Discount').text() + "'  />");
                                                $(".Del_ItemTotal", row).html($(ItemDetails).find("Del_ItemTotal").text());

                                                $(".OM_MinPrice", row).text($(ItemDetails).find('minPrice').text());
                                                $(".OM_CostPrice", row).text(CostPrice);
                                                $(".OM_BasePrice", row).text($(ItemDetails).find('ProductBasePrice').text());
                                                $(".Barcode", row).text($(ItemDetails).find('Barcode').text());

                                                var taxType = '', TypeTax = 0;
                                                if ($('#chkIsTaxable').prop('checked') == true) {
                                                    taxType = 'Taxable';
                                                    TypeTax = 1;
                                                }
                                                var IsExchange1 = '';
                                                if ($('#chkExchange').prop('checked') == true) {
                                                    IsExchange1 = 'Exchange'
                                                }

                                                if ($(ItemDetails).find('ML').text() == '0') {
                                                    MLQty = 0;
                                                }
                                                else {
                                                    MLQty = parseFloat($(ItemDetails).find('ML').text());
                                                }
                                                if ($(ItemDetails).find('Oz').text() == '0') {
                                                    weightoz = 0;
                                                }
                                                else {
                                                    weightoz = parseFloat($(ItemDetails).find('Oz').text());
                                                }

                                                if (TypeTax == 1) {
                                                    $(".TaxRate", row).html("<span TypeTax='" + TypeTax + "'  MLQty='" + MLQty + "' weightoz='" + weightoz + "'></span>" + '<TaxRate style="font-weight: bold" class="la la-check-circle success center"></TaxRate>');
                                                } else {
                                                    $(".TaxRate", row).html("<span TypeTax='" + TypeTax + "'  MLQty='" + MLQty + "' weightoz='" + weightoz + "'></span>");
                                                }
                                                if (IsExchange == 1) {
                                                    $(".IsExchange", row).html("<span IsExchange='" + IsExchange + "'> </span>" + '<IsExchange style="font-weight: bold" class="la la-check-circle success center"></IsExchange>');
                                                } else {
                                                    $(".IsExchange", row).html("<span IsExchange='" + IsExchange + "'> </span>");
                                                }
                                                $(".NetPrice", row).text("0.00");
                                                $(".Action", row).html("<a href='javascript:;' id='deleterow' onclick='deleterow(this)'><span class='ft-x'></span></a>");
                                                if ($('#tblProductDetail tbody tr').length > 0) {
                                                    $('#tblProductDetail tbody tr:first').before(row);
                                                }
                                                else {
                                                    $('#tblProductDetail tbody').append(row);
                                                }
                                                rowCal(row.find(".ReqQty > input"), 0);
                                            }
                                            if ($('#tblProductDetail tbody tr').length > 0) {
                                                $("#ddlCustomer").attr('disabled', true);
                                            }
                                            $('#chkIsTaxable').prop('checked', false);
                                            $('#chkExchange').prop('checked', false);
                                            $('#chkFreeItem').prop('checked', false);
                                            $("#txtBarcode").val('');
                                            $("#txtBarcode").focus();
                                            $("#ddlProduct").select2('val', '0');
                                            $("#ddlUnitType").val(0);
                                            $("#txtReqQty").val("1");
                                            $("#emptyTable").hide();
                                            toastr.success('Product added successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

                                            $("#tblProductDetail tbody tr").each(function () {
                                                if ($(this).find(".ProName > span").attr("productautoid") == productAutoId && $(this).find(".UnitType > span").attr("unitautoid") == unitAutoId && $(this).find(".ProId > span").attr("isfreeitem") == IsFreeItem && $(this).find(".IsExchange > span").attr("isexchange") == IsExchange) {
                                                    rowCal($(this).find(".ReqQty > input"), 0);
                                                }
                                            });

                                            calTotalAmount();
                                        }
                                    }
                                }
                            }
                        } else {
                            location.href = '/';
                        }

                    },
                    error: function (result) {
                        console.log(JSON.parse(result.responseText).d);
                    },
                    failure: function (result) {
                        console.log(JSON.parse(result.responseText).d);
                    }
                });


            }
            else {
                toastr.error('All * fields are mandatory', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            }
        }
        /*----------------------------------------------*/
        /*-------------------------------------------------------------------------------------------------------------------------------*/

        //                                                          Calculations
        /*-------------------------------------------------------------------------------------------------------------------------------*/
        function rowCal(e, i) {
            debugger
            var row = $(e).closest("tr");
            var disc = 0;
            $('#txtStoreCreditAmount').val('0.00');
            if (Number($(row).find(".ReqQty input").val()) == 0) {
                $(row).find(".ReqQty input").val($(row).find(".ReqQty input").attr('value'));
                toastr.error("Quantity can't be left empty or zero.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                return;
            }
            if (parseFloat($(row).find('.Oim_Discount input').val()) > 100) {
                toastr.error("Discount % can't be greater than 100.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                disc = 0;
                $(row).find('.Oim_Discount input').val('0.00');
                return;
            }
            else if (parseFloat($(row).find('.Oim_Discount input').val()) == '' || isNaN(parseFloat($(row).find('.Oim_Discount input').val())) == true) {
                disc = 0;
                $(row).find('.Oim_Discount input').val('0.00');
            } else if (parseFloat($(row).find('.Oim_Discount input').val()) < 0) {
                toastr.error("Discount can't be negative.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                disc = 0;
                $(row).find('.Oim_Discount input').val('0.00');
                return;
            }
            else {
                var minprice = $(row).find('.UnitPrice input').attr('minprice');
                disc = parseFloat($(row).find('.Oim_Discount input').val()).toFixed(2);
                var unitprice = parseFloat($(row).find('.UnitPrice input').val()) || 0;
                var discountamount = parseFloat(unitprice - unitprice * disc / 100);
                if (discountamount < minprice) {
                    toastr.error("unit Price  can't be less than Min price.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    $(row).find('.Oim_Discount input').val('0.00');
                    return;
                }
            }
            var dataValue = {
                DraftAutoId: $('#DraftAutoId').val(),
                ProductAutoId: $(row).find('.ProName span').attr('productautoid'),
                UnitAutoId: $(row).find('.UnitType span').attr('unitautoid'),
                ReqQty: $(row).find('.ReqQty input').val() || 0,
                UnitPrice: $(row).find('.UnitPrice input').val() || 0,
                IsExchange: $(row).find('.IsExchange span').attr('IsExchange'),
                IsFreeItem: $(row).find('.ProId span').attr('IsFreeItem'),
                TotalReqQnty: ($(row).find('.UnitType span').attr('qtyperunit') * $(row).find('.ReqQty input').val()),
                Remarks: $("#txtOrderRemarks").val(),
                Oim_Discount: disc || 0,
            }
            $.ajax({
                type: "POST",
                url: "orderNew.aspx/EditQty",
                data: JSON.stringify({ dataValues: JSON.stringify(dataValue) }),
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                asynch: true,
                beforeSend: function () {
                    //$.blockUI({
                    //    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    //    overlayCSS: {
                    //        backgroundColor: '#FFF',
                    //        opacity: 0.8,
                    //        cursor: 'wait'
                    //    },
                    //    css: {
                    //        border: 0,
                    //        padding: 0,
                    //        backgroundColor: 'transparent'
                    //    }
                    //});
                },
                complete: function () {
                    //$.unblockUI();
                },
                success: function (response) {
                    if (response.d == 'false') {
                        var Oim_Discount = 0, netPrice = 0, disc = 0;
                        var OldReqQty = Number(row.find(".ReqQty input").attr("findoldqty"));
                        if (i == 1) {
                            row.find(".ReqQty input").val(OldReqQty);
                            row.find(".TtlPcs").text((Number(OldReqQty) * Number(row.find(".UnitType span").attr("QtyPerUnit"))));
                            price = Number(row.find(".UnitPrice input").val());
                            var netPrice = (price * Number(row.find(".ReqQty input").val()));
                            row.find(".NetPrice").text(netPrice.toFixed(2));
                            Oim_Discount = parseFloat($(row).find('.Oim_Discount input').val());
                            netPrice = parseFloat((row).find('.NetPrice').text());
                            disc = parseFloat((netPrice * Oim_Discount) / 100);
                            if (Number(disc) != 0) {
                                row.find(".NetPrice").text(parseFloat(netPrice - disc).toFixed(2));
                                row.find(".Del_ItemTotal").text(parseFloat(parseFloat($(row).find('.ReqQty input').val()) * parseFloat($(row).find('.UnitPrice input').val())).toFixed(2));
                            }
                            else {
                                row.find(".NetPrice").text(parseFloat(netPrice).toFixed(2));
                                row.find(".Del_ItemTotal").text(parseFloat(netPrice).toFixed(2));
                            }
                            $('#txtBarcode').removeAttr('onchange');
                            $('#txtBarcode').attr('onchange', 'invalidBarCode()');
                            swal({
                                title: "",
                                text: "Stock not available.",
                                icon: "error",
                                closeOnClickOutside: false
                            }).then(function () {
                                $("#txtBarcode").val("");
                                $("#txtBarcode").focus();
                                $("#txtBarcode").attr('onchange', 'readBarcode()');
                            });
                            $("#txtBarcode").focus();
                            invalidBarCode();
                            calTotalAmount();
                        }
                    } else {
                        var xmldoc = $.parseXML(response.d);
                        var ItemDetails = $(xmldoc).find("Table");
                        var curentStock = $(ItemDetails).find('Stock').text();
                        var productAutoId = $(ItemDetails).find('AutoId').text();
                        var TQty = 0;
                        $("#tblProductDetail tbody tr").each(function () {
                            if ($(this).find(".ProName > span").attr("productautoid") == productAutoId) {
                                var reqQty = Number($(this).find(".ReqQty input").val()) || 0;
                                TQty += reqQty * Number($(this).find(".UnitType span").attr('qtyperunit'));
                            }
                        });

                        if (curentStock < TQty) {

                            $('#txtBarcode').removeAttr('onchange');
                            $('#txtBarcode').attr('onchange', 'invalidBarCode()');
                            $("#Stock")[0].play();
                            swal({
                                title: "",
                                text: "Stock not available.",
                                icon: "error",
                                closeOnClickOutside: false
                            }).then(function () {
                                $("#txtBarcode").val("");
                                $("#txtBarcode").focus();
                                $("#txtBarcode").attr('onchange', 'readBarcode()');
                            });
                            $("#txtBarcode").focus();
                            var OldReqQty = Number(row.find(".ReqQty input").attr("findoldqty"));
                            row.find(".ReqQty input").val(OldReqQty);
                            row.find(".TtlPcs").text((Number(OldReqQty) * Number(row.find(".UnitType span").attr("QtyPerUnit"))));
                            price = Number(row.find(".UnitPrice input").val());
                            var netPrice = (price * Number(row.find(".ReqQty input").val()));
                            row.find(".NetPrice").text(netPrice.toFixed(2));
                            Oim_Discount = parseFloat($(row).find('.Oim_Discount input').val());
                            netPrice = parseFloat((row).find('.NetPrice').text());
                            disc = parseFloat((netPrice * Oim_Discount) / 100).toFixed(2);
                            if (Number(disc) != 0) {
                                row.find(".NetPrice").text(parseFloat(netPrice - disc).toFixed(2));
                                row.find(".Del_ItemTotal").text(parseFloat(parseFloat($(row).find('.ReqQty input').val()) * parseFloat($(row).find('.UnitPrice input').val())).toFixed(2));
                            }
                            else {
                                row.find(".NetPrice").text(parseFloat(netPrice).toFixed(2));
                                row.find(".Del_ItemTotal").text(parseFloat(netPrice).toFixed(2));
                            }
                            invalidBarCode();
                            calTotalAmount();
                        }
                        else {
                            var totalPcs = (Number($(row).find('.ReqQty input').val()) * Number(row.find(".UnitType span").attr("QtyPerUnit")));
                            row.find(".TtlPcs").text(totalPcs);
                            price = Number(row.find(".UnitPrice input").val());
                            var netPrice = (price * Number(row.find(".ReqQty input").val()));
                            row.find(".NetPrice").text(netPrice.toFixed(2));
                            row.find(".ReqQty input").removeClass('border-warning');
                            Oim_Discount = parseFloat($(row).find('.Oim_Discount input').val());
                            netPrice = parseFloat((row).find('.NetPrice').text());
                            disc = parseFloat((netPrice * Oim_Discount) / 100).toFixed(2);
                            if (Number(disc) != 0) {
                                row.find(".NetPrice").text(parseFloat(netPrice - disc).toFixed(2));
                                row.find(".Del_ItemTotal").text(parseFloat(parseFloat($(row).find('.ReqQty input').val()) * parseFloat($(row).find('.UnitPrice input').val())).toFixed(2));
                            }
                            else {
                                row.find(".NetPrice").text(parseFloat(netPrice).toFixed(2));
                                row.find(".Del_ItemTotal").text(parseFloat(netPrice).toFixed(2));
                            }
                            calTotalAmount();
                        }

                    }

                },
                error: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                },
                failure: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                }
            });

        }
        function changePrice(e) {
            var disc = 0;
            if (parseFloat($(row).find('.Oim_Discount input').val()) > 100) {
                toastr.error("Discount % can't be greater than 100.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                disc = 0;
                $(row).find('.Oim_Discount input').val('0.00');
            }
            else if (parseFloat($(row).find('.Oim_Discount input').val()) == '' || isNaN(parseFloat($(row).find('.Oim_Discount input').val())) == true) {
                disc = 0;
                $(row).find('.Oim_Discount input').val('0.00');
            } else if (parseFloat($(row).find('.Oim_Discount input').val()) < 0) {
                toastr.error("Discount can't be negative.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                disc = 0;
                $(row).find('.Oim_Discount input').val('0.00');
            }
            else if (parseFloat($(row).find('.UnitPrice input').val()) == '0' || parseFloat($(row).find('.UnitPrice input').val()) == '0.00' || parseFloat($(row).find('.UnitPrice input').val()) == '') {
                $(row).find('.UnitPrice input').val($(row).find('.UnitPrice input').attr('value'));
                toastr.error("Unit Price can't be left empty or zero .", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            }
            else {
                disc = parseFloat($(row).find('.Oim_Discount input').val()).toFixed(2);
            }
            if ($(row).find('.ReqQty input').val() != "") {
                var row = $(e).closest("tr");
                var dataValue = {
                    DraftAutoId: $('#DraftAutoId').val(),
                    ProductAutoId: $(row).find('.ProName span').attr('productautoid'),
                    UnitAutoId: $(row).find('.UnitType span').attr('unitautoid'),
                    ReqQty: $(row).find('.ReqQty input').val() || 0,
                    UnitPrice: $(row).find('.UnitPrice input').val() || 0,
                    IsExchange: $(row).find('.IsExchange span').attr('IsExchange'),
                    TotalReqQnty: $(row).find('.TtlPcs ').text(),
                    Remarks: $("#txtOrderRemarks").val(),
                    Oim_Discount: disc || 0
                }

                $.ajax({
                    type: "POST",
                    url: "orderNew.aspx/EditQty",
                    data: JSON.stringify({ dataValues: JSON.stringify(dataValue) }),
                    contentType: "application/json; charset=utf-8",
                    datatype: "json",
                    beforeSend: function () {
                        //$.blockUI({
                        //    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        //    overlayCSS: {
                        //        backgroundColor: '#FFF',
                        //        opacity: 0.8,
                        //        cursor: 'wait'
                        //    },
                        //    css: {
                        //        border: 0,
                        //        padding: 0,
                        //        backgroundColor: 'transparent'
                        //    }
                        //});
                    },
                    complete: function () {
                        //$.unblockUI();
                    },
                    success: function (response) {
                    },
                    error: function (result) {
                        console.log(JSON.parse(result.responseText).d);
                    },
                    failure: function (result) {
                        console.log(JSON.parse(result.responseText).d);
                    }
                });

                rowCal(row.find(".ReqQty > input"), 0);
                if (Number($(e).val()) < Number($(e).attr("minprice"))) {
                    $(e).css("background-color", "#FF9149").addClass("PriceNotOk");
                } else {
                    $(e).css("background-color", "#FFF").removeClass("PriceNotOk");
                }
                var price = Number(row.find(".UnitPrice input").val());
                var netPrice = (price * Number(row.find(".ReqQty input").val()));
                var Oim_Discount = parseFloat($(row).find('.Oim_Discount input').val());
                var netPrice = parseFloat((row).find('.NetPrice').text());
                var disc = parseFloat((netPrice * Oim_Discount) / 100).toFixed(2);
                if (Number(disc) != 0) {
                    row.find(".NetPrice").text(parseFloat(netPrice - disc).toFixed(2));
                    row.find(".Del_ItemTotal").text(parseFloat(parseFloat($(row).find('.ReqQty input').val()) * parseFloat($(row).find('.UnitPrice input').val())).toFixed(2));
                }
                else {
                    row.find(".NetPrice").text(parseFloat(netPrice).toFixed(2));
                    row.find(".Del_ItemTotal").text(parseFloat(netPrice).toFixed(2));
                }
            }
        }
        function calTotalAmount() {
            var total = 0.00;
            $("#tblProductDetail tbody tr").each(function () {
                total += Number($(this).find(".NetPrice").text());
            });
            $("#txtTotalAmount").val(total.toFixed(2));
            discountataddItem();
        }
        function discountataddItem() {

            var factor = 0.00, discountamount = 0.00, totalamt = 0.00;
            if ($("#txtDiscAmt").val() == "") {
                $("#txtDiscAmt").val('0.00');
                $("#txtOverallDisc").val('0.00');
            }
            else if (parseFloat($("#txtDiscAmt").val()) > (parseFloat($("#txtTotalAmount").val()))) {
                $("#txtDiscAmt").val('0.00');
                $("#txtOverallDisc").val('0.00');
            }
            discountamount = Number($("#txtDiscAmt").val()) || 0.00;
            totalamt = Number($("#txtTotalAmount").val()) || 0.00;
            if (totalamt > 0) {
                factor = parseFloat((parseFloat(discountamount) / parseFloat(totalamt)) * 100).toFixed(2) || 0.00;
            }
            $("#txtOverallDisc").val(parseFloat(factor).toFixed(2));
            calTotalTax();
        }
        function calTotalTax() {

            var totalTax = 0.00, TaxValue = 0, qty;
            var MLQty = 0;
            var WeightOz = 0;
            $("#tblProductDetail tbody tr").each(function () {
                qty = Number($(this).find(".ReqQty input").val());
                if ($(this).find('.TaxRate span').attr('typetax') == 1) {
                    var totalPrice = Number($(this).find(".UnitPrice input").val()) * qty;
                    var Disc = totalPrice * Number($("#txtOverallDisc").val()) * 0.01;
                    var priceAfterDisc = totalPrice - Disc;
                    if ($('#ddlTaxType').val() != null && $('#ddlTaxType').val() != "") {
                        TaxValue = $('#ddlTaxType option:selected').attr("TaxValue");
                    }

                    totalTax += priceAfterDisc * Number(TaxValue) * 0.01;
                }
                MLQty += parseFloat($(this).find('.TtlPcs').text()) * parseFloat($(this).find('.TaxRate span').attr('MLQty'));
                WeightOz += parseFloat($(this).find('.TtlPcs').text()) * parseFloat($(this).find('.TaxRate span').attr('weightoz'));
            });

            var taxenabledval = $("#ddlShippingType option:selected").attr('taxenabled');
            if (taxenabledval == 1) {
                $("#txtMLQty").val(MLQty.toFixed(2));
                $("#txtMLTax").val((parseFloat(MLQty) * parseFloat(MLQtyRate)).toFixed(2));
                $("#txtWeightQty").val(WeightOz.toFixed(2));
                $("#txtWeightTex").val((parseFloat(WeightOz) * parseFloat(WeightOzTex)).toFixed(2));
            } else {
                $("#txtMLQty").val("0");
                $("#txtMLTax").val("0.00");
                $("#txtWeightQty").val('0.00');
                $("#txtWeightTex").val('0.00');
            }

            $("#txtTotalTax").val(totalTax.toFixed(2));
            calGrandTotal();

        }
        function calOverallDisc1() {
            var DiscAmt = parseFloat($("#txtDiscAmt").val()) || 0.00;
            var TotalAmount = (Number($("#txtTotalAmount").val())) || 0.00;
            if (DiscAmt > TotalAmount) {
                $("#txtOverallDisc").val('0.00');
                $("#txtDiscAmt").val('0.00');
                toastr.error('Discount Amount should less or equal to order amount.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            } else {
                var per = (DiscAmt / TotalAmount) * 100;
                $("#txtOverallDisc").val(per.toFixed(2));
            }
            calTotalTax();
        }
        function calOverallDisc() {

            var factor = 0.00;
            if ($("#txtOverallDisc").val() == "") {
                $("#txtOverallDisc").val('0.00');
            }
            else if (parseFloat($("#txtOverallDisc").val()) > 100) {
                $("#txtOverallDisc").val('0.00');
                toastr.error("Discount % can't be greater than 100.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            }
            factor = Number($("#txtOverallDisc").val()) * 0.01 || 0.00;

            $("#txtDiscAmt").val((Number($("#txtTotalAmount").val()) * factor).toFixed(2));
            calTotalTax();
        }
        function calGrandTotal() {

            var DiscAmt = 0.00;
            if ($("#txtDiscAmt").val() != "") {
                DiscAmt = parseFloat($("#txtDiscAmt").val())
            }
            var grandTotal = Number($("#txtTotalAmount").val()) - Number(DiscAmt) + Number($("#txtShipping").val()) + Number($("#txtTotalTax").val()) + Number($("#txtWeightTex").val()) + Number($("#txtMLTax").val());
            var round = Math.round(grandTotal);
            $("#txtAdjustment").val((round - grandTotal).toFixed(2));
            $("#txtGrandTotal").val(round.toFixed(2));
            paidAmount();
        }

        function PaidAmountUsed() {
            var paidamount = 0.00;
            if ($("#hdnPaidAmount").val() != '') {
                paidamount = $("#hdnPaidAmount").val();
            }
            var Balanceamount = 0.00;
            if ($("#txtOpenBalance").val() != '') {
                Balanceamount = $("#txtOpenBalance").val();
            }
            if (parseFloat($("#txtPaybleAmount").val()) < parseFloat($("#txtPaidAmount").val())) {
                toastr.error('Paid amount always less than or equal to payable amount.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                $("#txtPaidAmount").val(paidamount);
                $("#txtBalanceAmount").val(Balanceamount);
            }
            else {
                paidAmount();
            }
        }

        function paidAmount() {

            if ($("#txtPaidAmount").val() != "") {
                if (parseFloat($("#txtPaidAmount").val()) > 0) {
                    $('#ddlPaymentMenthod').attr('disabled', false);
                    $('#txtReferenceNo').attr('disabled', false);
                } else {
                    $('#ddlPaymentMenthod').val('0');
                    $('#txtReferenceNo').val('');
                    $('#ddlPaymentMenthod').attr('disabled', true);
                    $('#txtReferenceNo').attr('disabled', true);
                }
            } else {
                $('#ddlPaymentMenthod').val('0');
                $('#txtReferenceNo').val('');
                $('#ddlPaymentMenthod').attr('disabled', true);
                $('#txtReferenceNo').attr('disabled', true);
            }
            var grandTotal = parseFloat($("#txtGrandTotal").val());
            var DeductionAmount = 0.00;
            if ($("#txtDeductionAmount").val() != "") {
                DeductionAmount = parseFloat($("#txtDeductionAmount").val());
            }
            var StoreCreditAmount = 0.00;
            if ($("#txtStoreCreditAmount").val() != "") {
                if (parseFloat($("#txtStoreCreditAmount").val()) > 0) {
                    StoreCreditAmount = parseFloat($("#txtStoreCreditAmount").val());
                }
            }
            var balanceAmount = 0.00;

            if (parseFloat(DeductionAmount) > parseFloat(grandTotal)) {
                payable = 0.00;
                $("#txtStoreCreditAmount").attr('disabled', true);
                $("#txtStoreCreditAmount").val(parseFloat(grandTotal).toFixed(2) - parseFloat(DeductionAmount).toFixed(2));
                $("#txtPaidAmount").val('0.00');
                $("#hdnPaidAmount").val('0.00');
            } else {
                payable = parseFloat(grandTotal) - parseFloat(DeductionAmount);
                if (parseFloat(StoreCreditAmount) > 0) {
                    payable = payable - parseFloat(StoreCreditAmount).toFixed(2);
                }
                if (parseFloat($("#CreditMemoAmount").html()).toFixed(2) > 0) {
                    $("#txtStoreCreditAmount").attr('disabled', false);
                }
                else {
                    $("#txtStoreCreditAmount").attr('disabled', true);
                }
                //$("#txtStoreCreditAmount").val("0.00");
            }
            $("#txtPaybleAmount").val(payable.toFixed(2));
            var DueAmount = 0.00;
            if ($('#txtPastDue').val() != "") {
                PastDue = $('#txtPastDue').val();
            }
            var OpenBalance = 0.00;
            OpenBalance = parseFloat(parseFloat(PastDue) + parseFloat(payable)).toFixed(2);
            $('#txtOpenBalance').val(parseFloat(OpenBalance).toFixed(2));
            var PaidAmount = 0.00;
            if ($("#txtPaidAmount").val() != "") {
                PaidAmount = parseFloat($("#txtPaidAmount").val()).toFixed(2);
            }
            $('#txtBalanceAmount').val((parseFloat(OpenBalance) - parseFloat(PaidAmount)).toFixed(2));
        }
        function changecredit() {
            var StoreCredit = $('#CreditMemoAmount').text();
            var StoreCreditAmount = 0.00;
            if ($('#txtStoreCreditAmount').val() != "") {
                StoreCreditAmount = $('#txtStoreCreditAmount').val()
            }
            if ($("#hdnStoreCreditAmount").val() != "") {
                StoreCreditAmountUsed = $("#hdnStoreCreditAmount").val();
            }
            var GrandTotal = $('#txtGrandTotal').val();
            var DeductionAmount = $('#txtDeductionAmount').val();

            if (parseFloat(DeductionAmount) + parseFloat(StoreCreditAmount) >= GrandTotal) {
                $("#txtPaidAmount").val('0');
                $("#hdnPaidAmount").val('0.00');
            }

            if (parseFloat(GrandTotal) < parseFloat(StoreCreditAmount)) {
                toastr.error('You can use maximum order amount.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                $('#txtStoreCreditAmount').val('0.00');
                $("#txtPaidAmount").val('0');
                $("#hdnPaidAmount").val('0.00');
                paidAmount();
            }
            else if (parseFloat(GrandTotal) >= parseFloat(DeductionAmount)) {
                if (parseFloat(StoreCreditAmount) > parseFloat(StoreCredit)) {
                    toastr.error('You can use maximum "' + parseFloat(StoreCredit).toFixed(2) + '" Store Credit .', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    $('#txtStoreCreditAmount').val(parseFloat(StoreCredit).toFixed(2));
                    $("#txtPaidAmount").val('0');
                    $("#hdnPaidAmount").val('0.00');
                }
                var checkamount = parseFloat(GrandTotal) - parseFloat(DeductionAmount);
                if (checkamount < StoreCreditAmount) {
                    $('#txtStoreCreditAmount').val('0.00');
                    $("#txtPaidAmount").val('0');
                    $("#hdnPaidAmount").val('0.00');
                    toastr.error('You can use maximum payable Amount.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                }
                paidAmount();

            }

        }

        function CheckAllocatedQty(e) {
            debugger;
            var row = $(e).closest('tr');
            if (Number($(row).find(".ReqQty input").val()) == 0) {
                $(row).find(".ReqQty input").val($(row).find(".ReqQty input").attr('value'));
                toastr.error("Quantity can't be left empty or zero.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                return;
            }
            var Draftdata = {
                CustomerAutoId: $('#ddlCustomer').val(),
                productAutoId: $(row).find(".ProName span").attr('productautoid'),
                unitAutoId: $(row).find(".UnitType span").attr('unitautoid'),
                ReqQty: $(e).val() || 0
            }
            $.ajax({
                type: "POST",
                url: "orderNew.aspx/checkAllocationQty",
                data: JSON.stringify({ dataValue: JSON.stringify(Draftdata) }),
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {

                    if (response.d != "Session Expired") {
                        if (response.d == "false") {
                            swal("", "Oops,Something went wrong.Please try later.", "error");
                        }
                        else if (response.d == "Available") {
                            toastr.success('Product added successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                        }
                        else {
                            var xmldoc = $.parseXML(response.d);
                            var product = $(xmldoc).find("Table");
                            if (product.length > 0) {
                                if ($(product).find('Message').text() == "Less") {
                                    $(e).val($(row).find(".ReqQty input").attr('findoldqty'));
                                    rowCal($(row).find(".ReqQty > input"), $(row).find(".ReqQty input").attr('findoldqty'));
                                    $("#mdAllowQtyConfirmation").modal('show');
                                    AvailableAllocatedQty = $(product).find('AvailableQty').text();
                                    RequiredQty = $(product).find('RequiredQty').text();
                                    PrdtId = $(product).find('ProductAutoId').text();
                                }
                            }
                        }
                    } else {
                        location.href = '/';
                    }
                },
                error: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                },
                failure: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                }
            });
        }
        function bindUnitType() {
            $("#alertStockQty").hide();
            if ($("#ddlProduct option:selected").attr('eligibleforfree') == 1) {
                $('#chkFreeItem').attr('disabled', false);
            } else {
                $('#chkFreeItem').attr('disabled', true);
            }
            if ($("#ddlProduct").val() !== '0') {
                $.ajax({
                    type: "POST",
                    url: "OrderNew.aspx/bindUnitType",
                    data: "{'productAutoId':" + $("#ddlProduct").val() + "}",
                    contentType: "application/json; charset=utf-8",
                    datatype: "json",
                    beforeSend: function () {
                        $.blockUI({
                            message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                            overlayCSS: {
                                backgroundColor: '#FFF',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (response) {
                        if (response.d != "Session Expired") {
                            var xmldoc = $.parseXML(response.d);
                            var unitType = $(xmldoc).find("Table");
                            var unitDefault = $(xmldoc).find("Table1");
                            var count = 0;
                            $("#ddlUnitType option:not(:first)").remove();
                            $.each(unitType, function () {
                                $("#ddlUnitType").append("<option EligibleforFree='" + $(this).find("EligibleforFree").text() + "' value='" + $(this).find("AutoId").text() + "' QtyPerUnit='" + $(this).find("Qty").text() + "' prostock='" + $(this).find("PStock").text() + "'>" + $(this).find("UnitType").text() + " (" + $(this).find("Qty").text() + " pcs" + ")</option>");
                            });
                            if (unitDefault.length > 0) {
                                if (BUnitAutoId == 0) {
                                    $('#ddlUnitType option').each(function () {
                                        if (this.value == $(unitDefault).find('AutoId').text()) {
                                            $("#ddlUnitType").val($(unitDefault).find('AutoId').text()).change();
                                        }
                                    });
                                } else {
                                    $('#ddlUnitType option').each(function () {
                                        if (this.value == BUnitAutoId) {
                                            $("#ddlUnitType").val(BUnitAutoId).change();
                                        }
                                    });
                                    BUnitAutoId = 0;
                                }
                            } else {
                                $("#ddlUnitType").val(0).change();
                            }
                        } else {
                            location.href = '/';
                        }

                    },
                    error: function (result) {
                        console.log(JSON.parse(result.responseText).d);
                    },
                    failure: function (result) {
                        console.log(JSON.parse(result.responseText).d);
                    }
                });

            } else {
                $('#chkFreeItem').removeAttr('disabled');
                $("#ddlUnitType option:not(:first)").remove();
                $("#txtReqQty").val(1);
            }
        }

        function deleteItemrecord(e) {
            var tr = $(e).closest('tr');
            var data = {
                DraftAutoId: $('#DraftAutoId').val(),
                ProductAutoId: $(tr).find('.ProName span').attr('ProductAutoId'),
                UnitAutoId: $(tr).find('.UnitType span').attr('UnitAutoId'),
                isfreeitem: $(tr).find('.ProId span').attr('isfreeitem'),
                IsExchange: $(tr).find('.IsExchange span').attr('IsExchange')
            }
            if ($('#DraftAutoId').val() != '') {
                $.ajax({
                    type: "POST",
                    url: "orderNew.aspx/DeleteItem",
                    data: "{'DataValue':'" + JSON.stringify(data) + "'}",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    beforeSend: function () {
                        $.blockUI({
                            message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                            overlayCSS: {
                                backgroundColor: '#FFF',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (result) {
                        if (result.d == "true") {
                            swal("", "Product deleted successfully.", "success");
                            $(e).closest('tr').remove();
                            calTotalAmount();
                            if ($("#tblProductDetail tbody tr").length == 0) {
                                $("#ddlCustomer").attr('disabled', false);
                                $("#emptyTable").show();
                            } else {
                                $("#ddlCustomer").attr('disabled', true);
                                $("#emptyTable").hide();
                            }
                        }
                        else if (result.d == "false") {
                            swal("", "Oops! Something went wrong.Please try later.", "success");
                        }
                    },
                    error: function (result) {
                        console.log(JSON.parse(result.responseText).d);
                    },
                    failure: function (result) {
                        console.log(JSON.parse(result.responseText).d);
                    }
                })
            } else {
                $(e).closest('tr').remove();
                swal("", "Item deleted successfully.", "success");
            }



        }

        function deleterow(e) {
            swal({
                title: "Are you sure?",
                text: "You want to delete Product.",
                icon: "warning",
                showCancelButton: true,
                closeOnClickOutside: false,
                buttons: {
                    cancel: {
                        text: "No, cancel",
                        value: null,
                        visible: true,
                        className: "btn-warning",
                        closeModal: true,
                    },
                    confirm: {
                        text: "Yes, delete it",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: false
                    }
                }
            }).then(function (isConfirm) {
                if (isConfirm) {
                    deleteItemrecord(e);
                }
            })
        }

        function BindAddressCustomer() {
            if ($("#ddlCustomer").val() == 0) {
                $('#panelProduct input').attr('disabled', true);
                $('#panelProduct select').attr('disabled', true);
                $('#panelProduct button').attr('disabled', true);
            } else {
                $('#panelProduct input').attr('disabled', false);
                $('#panelProduct select').attr('disabled', false);
                $('#panelProduct button').attr('disabled', false);
            }
            $.ajax({
                type: "POST",
                url: "OrderNew.aspx/BindAddressCustomer",
                data: "{'CustomerAutoId':" + $("#ddlCustomer").val() + ",'OrderAutoId':" + 0 + "}",
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                async: false,
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {

                    if (response.d == 'false') {
                        swal("", "Oops, Something went wrong. Please try later.", "error");
                    }
                    else if (response.d != "Session Expired") {

                        var CustomerDetail = $.parseJSON(response.d);
                        for (var i = 0; i < CustomerDetail.length; i++) {
                            var CustomerDetails = CustomerDetail[i];

                            if (CustomerDetails.MLTaxRate != null) {
                                MLQtyRate = CustomerDetails.MLTaxRate[0].TaxRate;
                                MLTaxType = CustomerDetails.MLTaxRate[0].MLTaxType;
                            }
                            if (CustomerDetails.WeightTaxRate != null) {
                                WeightOzTex = CustomerDetails.WeightTaxRate[0].Value;
                            }
                            if (CustomerDetails.EmployeeName != null) {
                                $('#CustomerType').val(CustomerDetails.EmployeeName[0].CustomerType);
                            }
                            if (CustomerDetails.EmployeeName != null) {
                                if (CustomerDetails.EmployeeName[0].CustomerType == 3) {
                                    $('.UnitPrice').hide();
                                    $('.SRP').hide();
                                    $('.GP').hide();
                                    $('.NetPrice').hide();
                                    $('#orderSummary').hide();
                                    $("#drag-area11").hide();
                                    $('#divTaxable').hide();
                                    $('.Oim_Discount').hide();
                                    $('.Del_ItemTotal').hide();
                                }
                                else {
                                    $('.UnitPrice').show();
                                    $('.SRP').show();
                                    $('.GP').show();
                                    $('.NetPrice').show();
                                    $('#orderSummary').show();
                                    $("#drag-area11").show();
                                    $('#divTaxable').show();
                                    $('.Oim_Discount').show();
                                    $('.Del_ItemTotal').show();
                                }
                            }
                            if (CustomerDetails.TaxTypeMaster != null) {
                                var StateTax = CustomerDetails.TaxTypeMaster;
                                var TaxType = $("#ddlTaxType");
                                $("#ddlTaxType option").remove();
                                for (var k = 0; k < StateTax.length; k++) {
                                    var State = StateTax[k];
                                    var option = $("<option />");
                                    option.html(State.TaxableType + '(' + State.Value + ')');
                                    option.val(State.AutoId);
                                    option.attr('taxvalue', State.Value);
                                    option.attr('value', State.AutoId);
                                    TaxType.append(option);
                                }
                            }
                            if (CustomerDetails.CustomerName != null) {
                                $('#lblCustomerName').text(CustomerDetails.CustomerName[0].CustomerName);
                            }
                            var DuePayment = 0, DueAmount = 0;
                            if (CustomerDetails.OrderDetails != null) {
                                DuePayment = CustomerDetails.OrderDetails;
                                $("#tblduePayment tbody tr").remove();
                                var rowdue = $("#tblduePayment thead tr").clone(true);
                                var check = 0;
                                var DueAmount = 0;
                                for (var k = 0; k < DuePayment.length; k++) {
                                    var due = DuePayment[k];
                                    if (parseFloat(due.AmtDue) > 0) {
                                        $(".SrNo", rowdue).text(Number(check) + 1);
                                        $(".OrderNo", rowdue).text(due.OrderNo);
                                        $(".OrderDate", rowdue).text(due.OrderDate);
                                        $(".AmtDue", rowdue).html(due.AmtDue);
                                        DueAmount += parseFloat(due.AmtDue);
                                        $('#tblduePayment tbody').append(rowdue);
                                        rowdue = $("#tblduePayment tbody tr:last").clone(true);
                                        check = check + 1;
                                    }
                                }
                                $('#CustomerDueAmount').modal('show');
                            }
                            $("#TotalDueAmount").html(parseFloat(DueAmount).toFixed(2));
                            $('#StoreCredit').text(parseFloat(DueAmount).toFixed(2));
                            $('#txtPastDue').val(parseFloat(DueAmount).toFixed(2));
                            $('#txtOpenBalance').val(parseFloat(DueAmount).toFixed(2));

                            if (CustomerDetails.BillingAddress != null) {
                                var Billing = CustomerDetails.BillingAddress;
                                var bType = $("#ddlBillingAddress"); var defVal = 0;
                                $("#ddlBillingAddress option:not(:first)").remove();
                                for (var k = 0; k < Billing.length; k++) {
                                    var bill = Billing[k];
                                    if (bill.IsDefault == 1) {
                                        defVal = bill.BillAddrAutoId;
                                    }
                                    var option = $("<option />");
                                    option.html(bill.BillingAddress);
                                    option.val(bill.BillAddrAutoId);
                                    bType.append(option);
                                }
                                $("#ddlBillingAddress").val(defVal).change();
                            }

                            if (CustomerDetails.ShippingAddress != null) {
                                var shipping = CustomerDetails.ShippingAddress;
                                var sType = $("#ddlShippingAddress"); var defship = 0;
                                $("#ddlShippingAddress option:not(:first)").remove();
                                for (var k = 0; k < shipping.length; k++) {
                                    var ship = shipping[k];
                                    if (ship.IsDefault == 1) {
                                        defship = ship.ShipAddrAutoId;
                                    }
                                    var option = $("<option />");
                                    option.html(ship.ShippingAddress);
                                    option.val(ship.ShipAddrAutoId);
                                    sType.append(option);
                                }
                                $("#ddlShippingAddress").val(defship).change();
                            }
                            if (CustomerDetails.CreditAmount != null) {
                                if (parseFloat(CustomerDetails.CreditAmount[0].CreditAmount) > 0) {
                                    $('#CreditMemoAmount').text(CustomerDetails.CreditAmount[0].CreditAmount);
                                    $('#txtStoreCreditAmount').attr('readonly', false);
                                } else {
                                    $('#txtStoreCreditAmount').attr('readonly', true);
                                }
                            } else {
                                $('#txtStoreCreditAmount').attr('readonly', true);
                            }

                            if (CustomerDetails.EmployeeName != null) {
                                $('#salesPerson').val(CustomerDetails.EmployeeName[0].EMPNAME);
                            } else {
                                $('#salesPerson').val('');
                            }
                            var CreditDetails = 0;
                            if (CustomerDetails.CreditDetails != null) {
                                CreditDetails = CustomerDetails.CreditDetails;
                                $("#tblCreditMemoList tbody tr").remove();
                                var rowc1 = $("#tblCreditMemoList thead tr").clone(true);
                                var TotalDue = 0.00;
                                for (var k = 0; k < CreditDetails.length; k++) {
                                    var cd = CreditDetails[k];
                                    $(".SRNO", rowc1).text(Number(k) + 1);
                                    if (cd.OrderAutoId != null) {
                                        $(".Action", rowc1).html("<input type='checkbox' OrderAutoId='" + cd.OrderAutoId + "' checked name='creditMemo' onclick='funcheckprop(this)' value=" + cd.CreditAutoId + "> ");
                                    } else {
                                        $(".Action", rowc1).html("<input type='checkbox' OrderAutoId='" + cd.OrderAutoId + "' name='creditMemo' onclick='funcheckprop(this)' value=" + cd.CreditAutoId + "> ");
                                    }
                                    $(".CreditNo", rowc1).html("<a target='_blank' href='/Sales/CreditMemo.aspx?PageId=" + cd.CreditAutoId + "'>" + cd.CreditNo + "</a>");
                                    $(".CreditDate", rowc1).text(cd.CreditDate);
                                    $(".ReturnValue", rowc1).text(cd.ReturnValue);
                                    $(".CreditType", rowc1).text(cd.CreditType);
                                    TotalDue = parseFloat(TotalDue) + parseFloat(cd.ReturnValue);
                                    $("#tblCreditMemoList tbody").append(rowc1)
                                    rowc1 = $("#tblCreditMemoList tbody tr:last").clone(true);
                                }
                                $("#TotalDue").text(TotalDue.toFixed(2));
                                $(".Action").show();
                                $('#CusCreditMemo').show();
                                $('#dCuCreditMemo').show();
                            }
                            else {
                                $('#CusCreditMemo').hide();
                                $('#dCuCreditMemo').hide();
                            }
                            $('#txtBarcode').focus();
                        }
                    } else {
                        location.href = '/';
                    }

                },
                error: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                },
                failure: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                }
            });
        }
        /**************************************Attached Credit Memo***********************************/
        function funcheckprop(e) {
            if ($("#txtStoreCreditAmount").val() == '') {
                $("#txtStoreCreditAmount").val('0.00');
            }
            var deductionAmount = 0.00;
            var GrandTotal = $("#txtGrandTotal").val();
            var payable = 0.00;

            $("#tblCreditMemoList tbody tr").each(function () {
                if ($(this).find('.Action input').prop('checked') == true) {
                    deductionAmount += parseFloat($(this).find('.ReturnValue').text());
                }
            });

            if (deductionAmount > GrandTotal) {
                $("#txtPaidAmount").val('0');
                $("#hdnPaidAmount").val('0.00');
            }
            if (parseFloat(deductionAmount + parseFloat($("#txtStoreCreditAmount").val())) > GrandTotal) {
                $("#txtPaidAmount").val('0');
                $("#hdnPaidAmount").val('0.00');
            }
            if (parseFloat(deductionAmount + parseFloat($("#txtStoreCreditAmount").val()) + parseFloat($("#txtPaidAmount").val())) > GrandTotal) {
                $("#txtPaidAmount").val('0');
                $("#hdnPaidAmount").val('0.00');
            }

            $("#txtStoreCreditAmount").val('0.00');
            $("#txtDeductionAmount").val(parseFloat(deductionAmount).toFixed(2));

            if (parseFloat(deductionAmount) >= parseFloat(GrandTotal)) {
                payable = 0.00;
                $("#txtStoreCreditAmount").attr('disabled', true);
                $("#txtStoreCreditAmount").val(parseFloat(GrandTotal).toFixed(2) - parseFloat(deductionAmount).toFixed(2));
            } else {
                payable = parseFloat(GrandTotal) - parseFloat(deductionAmount);
                if (parseFloat($("#CreditMemoAmount").html()).toFixed(2) > 0) {
                    $("#txtStoreCreditAmount").attr('disabled', false);
                }
                else {
                    $("#txtStoreCreditAmount").attr('disabled', true);
                }
            }

            $("#txtPaybleAmount").val(payable.toFixed(2));
            calTotalAmount();
        }
        /**************************************End Attached Cedit Memo********************************/


        /*-------------------------------------------------------------------------------------------------------------------------------*/
        //                                                          Generate Order
        /*-------------------------------------------------------------------------------------------------------------------------------*/
        function PrintInvoice(i) {
            $("#SavedMassege").modal('hide');
            printOrder_CustCopyNew(i);
        }
        function printOrder_CustCopy() {
            window.open("/Manager/OrderPrintCC.html?OrderAutoId=" + $("#txtHOrderAutoId").val() + "&Print=1", "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");

        }
        function PrintPackingSlip(i) {
            window.open("/Manager/OrderPrintCCF5.html?OrderAutoId=" + $("#txtHOrderAutoId").val() + "&Print=1", "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
            if (i == 0)
                location.href = "/Warehouse/OrderNew.aspx";
        }
        function saveOrder() {
            if (checkRequiredField()) {
                if ($("#ddlShippingAddress").val() != '0' && $("#ddlBillingAddress").val() != '0') {
                    $('#txtOverallDisc').removeClass('border-warning');
                    $('#ddlCustomer').closest('div').find('.select2-selection--single').removeAttr('style');
                    var flag1 = false, flag2 = false, flag3 = false, chkUP = 0;
                    var grandTotal = parseFloat($("#txtGrandTotal").val());
                    var DeductionAmount = 0.00;
                    if ($("#txtDeductionAmount").val() != "") {
                        DeductionAmount = parseFloat($("#txtDeductionAmount").val());
                    }

                    var StoreCredit = $('#CreditMemoAmount').text();
                    var StoreCreditAmount = 0.00;
                    if ($('#txtStoreCreditAmount').val() != "") {
                        StoreCreditAmount = $('#txtStoreCreditAmount').val()
                    }
                    if (parseFloat(StoreCreditAmount) > parseFloat(StoreCredit)) {
                        toastr.error('You can use maximum "' + StoreCredit + '" Store Credit !!', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    }
                    else if (Number($('#ddlCustomer').val()) != 0) {
                        if ($('#tblProductDetail tbody tr').length > 0) {
                            $("#tblProductDetail tbody tr").each(function () {
                                if ($(this).find(".ReqQty input").val() == "" || $(this).find(".ReqQty input").val() == null || parseInt($(this).find(".ReqQty input").val()) == 0) {
                                    $(this).find(".ReqQty input").focus().addClass('border-warning');
                                    flag1 = true;
                                } else {
                                    $(this).find(".ReqQty input").blur().removeClass('border-warning');
                                }
                                if ($(this).find(".ProId span").attr('isfreeitem') == '0' && $(this).find(".IsExchange span").attr('isexchange') == '0') {
                                    if (Number($(this).find(".UnitPrice input").val()) == 0) {
                                        $(this).find(".UnitPrice input").focus().addClass('border-warning');
                                        chkUP = Number(chkUP) + 1;
                                    }
                                    else if (Number($(this).find(".UnitPrice input").val()) < Number($(this).find(".UnitPrice input").attr('minprice'))) {
                                        flag2 = true;
                                    }
                                    else {
                                        $(this).find(".UnitPrice input").blur().removeClass('border-warning');
                                    }
                                }
                            });
                            if (chkUP > 0) {
                                toastr.error("Unit Price can't be blank or zero", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                                return;
                            }
                            if (!flag1 && !flag2 && !flag3) {
                                $("#alertDOrder").hide();

                                if (parseFloat($("#txtPaidAmount").val()) > 0) {
                                    if ($("#ddlPaymentMenthod").val() == 0) {
                                        toastr.error('Payment method is required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

                                        return;

                                    }
                                    else {
                                        OSave()
                                    }
                                } else {
                                    OSave();
                                }

                            } else {
                                if (flag1) {
                                    toastr.error("Required quantity can't be  left empty or zero .", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                                }
                                else {
                                    toastr.error("Unit price can't be less than min price.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                                }
                            }

                        }
                        else {
                            toastr.error('No product added into the table.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

                        }
                    } else {
                        $('#ddlCustomer').closest('div').find('.select2-selection--single').attr('style', 'border:1px solid #FF9149  !important');
                        toastr.error('Please select customer !!', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

                    }
                }
            }
            else {
                toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            }
        }

        function OSave() {
            if (parseFloat($('#txtOverallDisc').val()) > 100) {
                $('#txtOverallDisc').addClass('border-warning');
                toastr.error('Discount Amount should be less than 100%.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                return;
            }
            if ($("#txtDeductionAmount").val() != "") {
                DeductionAmount = parseFloat($("#txtDeductionAmount").val());
            }
            var CreditNo = '';
            $("#tblCreditMemoList tbody tr").each(function () {
                if ($(this).find('.Action input').prop('checked') == true) {
                    CreditNo += $(this).find('.Action input').val() + ',';

                }
            })
            if ($('#txtStoreCreditAmount').val() != "") {
                StoreCreditAmount = $('#txtStoreCreditAmount').val()
            }
            var orderData = {
                DraftAutoId: $('#DraftAutoId').val(),
                CustomerAutoId: $('#ddlCustomer').val(),
                ShippingType: $('#ddlShippingType').val(),
                TotalAmount: $("#txtTotalAmount").val(),
                OverallDisc: $("#txtOverallDisc").val(),
                OverallDiscAmt: $("#txtDiscAmt").val(),
                ShippingCharges: $("#txtShipping").val(),
                TotalTax: $("#txtTotalTax").val(),
                GrandTotal: $("#txtGrandTotal").val(),
                TaxType: $("#ddlTaxType").val(),
                PaidAmount: $("#txtPaidAmount").val(),
                CreditMemo: $("#txtDeductionAmount").val(),
                StoreCredit: $("#txtStoreCreditAmount").val(),
                PayableAmount: $("#txtPaybleAmount").val(),
                BalanceAmount: $("#txtBalanceAmount").val(),
                PaymentMenthod: $("#ddlPaymentMenthod").val(),
                ShippingAutoId: $("#ddlShippingAddress").val(),
                BillingAutoId: $("#ddlBillingAddress").val(),
                ReferenceNo: $("#txtReferenceNo").val(),
                Remarks: $("#txtOrderRemarks").val(),
                PastDue: $("#txtPastDue").val(),
                Adjustment: $("#txtAdjustment").val(),
                CreditNo: CreditNo
            };
            var Product = [];
            $("#tblProductDetail tbody tr").each(function () {

                Product.push({
                    ProductAutoId: $(this).find('.ProName').find('span').attr('ProductAutoId'),
                    UnitAutoId: $(this).find('.UnitType').find('span').attr('UnitAutoId'),
                    QtyPerUnit: $(this).find('.UnitType').find('span').attr('QtyPerUnit'),
                    QtyReq: $(this).find('.ReqQty input').val(),
                    TotalPieces: $(this).find('.TtlPcs').text(),
                    UnitPrice: $(this).find('.UnitPrice input').val() || 0,
                    SRP: $(this).find('.SRP').text() || 0,
                    GP: $(this).find('.GP').text() || 0,
                    Tax: $(this).find('.TaxRate span').attr('typetax'),
                    IsExchange: $(this).find('.IsExchange').find('span').attr('IsExchange'),
                    isFreeItem: $(this).find('.ProId').find('span').attr('isFreeItem'),
                    NetPrice: $(this).find('.NetPrice').text() || 0,
                    OM_MinPrice: $(this).find('.OM_MinPrice').text() || 0,
                    OM_CostPrice: $(this).find('.OM_CostPrice').text() || 0,
                    OM_BasePrice: $(this).find('.OM_BasePrice').text() || 0,
                    Barcode: $(this).find('.Barcode').text(),
                    Oim_Discount: $(this).find('.Oim_Discount input').val() || 0
                });
            });



            $.ajax({
                type: "Post",
                url: "OrderNew.aspx/insertOrderData",
                data: JSON.stringify({ orderData: JSON.stringify(orderData), ProductDetails: JSON.stringify(Product) }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (data) {
                    if (data.d == "InvalidPOS") {
                        swal("", "Unauthorized Access.", "error")
                    }
                    else if (data.d != "Session Expired") {
                        if (data.d != "False") {
                            if ($('#CustomerType').val() == '3') {
                                $('#NoPrint').attr('onclick', 'PrintPackingSlip(0)');
                            } else {
                                $('#NoPrint').attr('onclick', 'PrintInvoice(1)');
                            }
                            var xmldoc = $.parseXML(data.d);
                            var tbl = $(xmldoc).find("Table");
                            var memoList = $(xmldoc).find("Table1");
                            if ($(tbl).find("Type").text() == "Available") {
                                resetOrder();
                                $("#txtHOrderAutoId").val($(tbl).find('OrderAutoId').text());
                                $("#CreditMemoAmount").html('');
                                $("#SavedMassege").modal('show');
                            }
                            else if ($(tbl).find("Message").text() == "InvalidCRMemo") {
                                $("#tblCRMStatusList tbody tr").remove();
                                var row = $("#tblCRMStatusList thead tr").clone(true);
                                $.each(memoList, function () {
                                    $(".CreditNo", row).text($(this).find("CreditNo").text());
                                    $(".SalesAmount", row).text($(this).find("SalesAmount").text());
                                    $(".Status", row).html('<span class="badge badge-sm badge-danger">' + $(this).find("Status").text() + '</span>');
                                    $('#tblCRMStatusList tbody').append(row);
                                    row = $("#tblCRMStatusList tbody tr:last").clone(true);
                                });
                                $("#CheckCreditMemo").modal('show');
                            }
                            else {
                                $("#tblProductList tbody tr").remove();
                                var row = $("#tblProductList thead tr").clone(true);
                                $.each(tbl, function () {
                                    $(".Pop_ProId", row).text($(this).find("ProductId").text());
                                    $(".Pop_ProName", row).text($(this).find("ProductName").text());
                                    $(".Pop_RequiredQty", row).text($(this).find("TotalPieces").text());
                                    $(".Pop_AvilStock", row).text($(this).find("Stock").text());
                                    $('#tblProductList tbody').append(row);
                                    row = $("#tblProductList tbody tr:last").clone(true);
                                });
                                $("#CheckStock").modal('show');
                            }

                        } else {
                            swal("Error !", "Oops! Something went wrong.Please try later.", "error")
                        }
                    } else {
                        location.href = '/';
                    }
                },
                error: function (result) {
                    swal("Error !", "Oops! Something went wrong.Please try later.", "error")
                },
                failure: function (result) {
                    swal("Error !", "Oops! Something went wrong.Please try later.", "error")
                }
            });
        }
        function resetOrder() {
            $("input[type='text']").val('');
            var d = new Date();
            CurrentDate = (d.getMonth() + 1) + "/" + d.getDate() + "/" + d.getFullYear();
            $('#txtOrderDate').val(CurrentDate);
            $("#DraftAutoId").val(0);
            $("#btnaddCustomer").show();
            $("#btnChangeCustomer").hide();
            $("#ddlCustomer").removeAttr('disabled');
            $("#popBillAddr button").attr('disabled', true);
            $("#Small1 button").attr('disabled', true);
            $("#tblProductDetail tbody tr").remove();
            $("#tblCustDues tbody tr").remove();
            $("#tblTop25Products tbody tr").remove();
            $("#sumOrderValue").text('0.00');
            $("#sumPaid").text('0.00');
            $("#sumDue").text('0.00');
            $('#orderSummary input').val('0.00');
            $("#txtReferenceNo").val('');
            $("#ddlCustomer").select2("val", "0");
            $("#ddlShippingAddress").val('0');
            $("#ddlBillingAddress").val('0');
            $("#ddlShippingType").val('0');
            $("#txtOrderRemarks").val('');
            $("#ddlProduct").val('0').change();
            $("#txtOrderDate").val('');
            $("#CreditMemoAmount").html('0.00');
            $("#ddlTaxType option").remove();
            var a = '<option value="0" taxvalue="0">-Select Tax Type-</option>'
            $("#ddlTaxType").append(a);
        }
        function nocreateorder() {
            location.href = '/Warehouse/NeworderList.aspx';
        }
        function createorder() {
            location.href = '/Warehouse/OrderNew.aspx';
        }
        function updatedraftOrder() {
            if ($("#DraftAutoId").val() != '') {
                var Draftdata = {
                    CustomerAutoId: $("#ddlCustomer").val(),
                    DraftAutoId: $("#DraftAutoId").val(),
                    ShippingType: $("#ddlShippingType").val(),
                    Remark: $("#txtOrderRemarks").val(),
                    ShippingCharge: $("#txtShipping").val() || 0,
                    OverallDisc: $("#txtOverallDisc").val() || 0,
                    DiscAmt: $("#txtDiscAmt").val() || 0
                }
                $.ajax({
                    type: "POST",
                    url: "OrderNew.aspx/updateDraftOrder",
                    data: JSON.stringify({ dataValues: JSON.stringify(Draftdata) }),
                    contentType: "application/json; charset=utf-8",
                    datatype: "json",
                    //beforeSend: function () {
                    //    $.blockUI({
                    //        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    //        overlayCSS: {
                    //            backgroundColor: '#FFF',
                    //            opacity: 0.8,
                    //            cursor: 'wait'
                    //        },
                    //        css: {
                    //            border: 0,
                    //            padding: 0,
                    //            backgroundColor: 'transparent'
                    //        }
                    //    });
                    //},
                    //complete: function () {
                    //    $.unblockUI();
                    //},
                    success: function (response) {
                        $("#btnSave").removeAttr("disabled");
                    },
                    error: function (result) {
                        console.log(JSON.parse(result.responseText).d);
                        $("#btnSave").removeAttr("disabled");
                    },
                    failure: function (result) {
                        console.log(JSON.parse(result.responseText).d);
                        $("#btnSave").removeAttr("disabled");
                    }
                });
            }
        }
    </script>
    <div id="CheckStock" class="modal" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content ">
                <div class="modal-header">
                    <h4 class="modal-title"><b>Stock Details</b></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table id="tblProductList" class="table table-striped table-bordered">
                                    <thead class="bg-blue white">
                                        <tr>
                                            <td class="Pop_ProId text-center">Product ID</td>
                                            <td class="Pop_ProName">Product Name</td>
                                            <td class="Pop_AvilStock text-center" style="width: 10px">Available Qty</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="ok" class="btn btn-success buttonAnimation round box-shadow-1  btn-sm" data-dismiss="modal"><b>OK</b></button>
                </div>
            </div>
        </div>
    </div>

    <div id="CheckCreditMemo" class="modal" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content ">
                <div class="modal-header">
                    <h4 class="modal-title"><b>Attached Credit Memo Details</b></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table id="tblCRMStatusList" class="table table-striped table-bordered">
                                    <thead class="bg-blue white">
                                        <tr>
                                            <td class="CreditNo text-center">Credit No</td>
                                            <td class="SalesAmount price right">Sales Amount</td>
                                            <td class="Status text-center" style="width: 10px">Status</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row" style="width: 100%;">
                        <div class="col-md-10">
                            <span style="color: red; font-size: 16px;">Above Credit Memo has been cancelled.</span>
                        </div>
                        <div class="col-md-2 text-right">
                            <button type="button" id="OK" class="btn btn-success buttonAnimation round box-shadow-1  btn-sm" data-dismiss="modal"><b>OK</b></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title" id="headertitle">New Order</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">

                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Orders</a></li>
                        <li class="breadcrumb-item">New Order</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" id="Button5" class="dropdown-item" onclick="location.href='/Warehouse/NeworderList.aspx'">Go To Order List</button>

                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <input type="hidden" id="CustomerType" />
                        <div class="card-content collapse show">
                            <div class="card-body" id="panelBill">
                                <div class="row">
                                    <div class="col-sm-2  col-sm-3">
                                        <label class="control-label">Order No</label>
                                        <input type="hidden" id="txtHOrderAutoId" class="form-control input-sm" />
                                        <input type="hidden" id="DraftAutoId" value="0" class="form-control input-sm" />
                                        <div class="form-group">
                                            <input type="text" id="txtOrderId" class="form-control input-sm border-primary" readonly="readonly" />
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-sm-3">
                                        <label class="control-label">Order Date</label>
                                        <div class="form-group">
                                            <input type="text" id="txtOrderDate" class="form-control input-sm border-primary" readonly="readonly" />
                                        </div>
                                    </div>

                                    <div class="col-sm-3 col-sm-4">
                                        <label class="control-label">
                                            Select Customer <span class="required">*</span>

                                        </label>
                                        <div class="form-group">
                                            <select class="form-control input-sm selectvalidate border-primary ddlSreq" style="padding: 3px !important" id="ddlCustomer" runat="server" onchange="BindAddressCustomer()">
                                                <option value="0">- Select Customer -</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-sm-2">
                                        <label class="control-label">
                                            Sales Person 
                                        </label>
                                        <div class="form-group">
                                            <input type="text" id="salesPerson" disabled="disabled" class="form-control input-sm    border-primary" />
                                        </div>
                                    </div>

                                    <div class="col-sm-2 col-sm-6">
                                        <label class="control-label">
                                            Shipping Address <span class="required">*</span>

                                        </label>
                                        <div class="form-group">
                                            <select class="form-control input-sm selectvalidate border-primary ddlreq" style="padding: 3px !important" id="ddlShippingAddress" runat="server">
                                                <option value="0">- Select -</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-2 col-sm-6">
                                        <label class="control-label">
                                            Billing Address <span class="required">*</span>

                                        </label>
                                        <div class="form-group">
                                            <select class="form-control input-sm selectvalidate border-primary ddlreq" style="padding: 3px !important" id="ddlBillingAddress" runat="server">
                                                <option value="0">- Select -</option>
                                            </select>
                                        </div>
                                    </div>
                                    <input type="hidden" id="taxEnabled" />
                                    <div class="clearfix"></div>
                                    <div class="col-sm-2 col-sm-6">
                                        <label class="control-label">
                                            Shipping Type <span class="required">*</span>

                                        </label>
                                        <div class="form-group">
                                            <select class="form-control input-sm selectvalidate border-primary ddlreq" style="padding: 3px !important" onfocus="disableSubmitButton()" onchange="updatedraftOrder();calTotalAmount();" id="ddlShippingType" runat="server">
                                                <option value="0">- Select -</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section id="drag-area1">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body" id="panelProduct">

                                <div class="row">
                                    <label class="col-sm-1 control-label">Barcode</label>
                                    <div class="col-sm-3 form-group">
                                        <input type="text" class="form-control input-sm border-primary" aria-autocomplete="none" autocomplete="new-password" id="txtBarcode" runat="server" onfocus="this.select()" placeholder="Enter Barcode here" onchange="readBarcode()" />
                                    </div>

                                </div>
                                <div class="clearfix"></div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label class="control-label">
                                            Product <span class="required">*</span>

                                        </label>
                                        <div class="form-group">
                                            <select class="form-control input-sm selectvalidate border-primary ddlSreq1" id="ddlProduct" runat="server" style="width: 100% !important" onchange="bindUnitType()">
                                                <option value="0">-Select-</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-md-2">
                                        <label class="control-label">
                                            Unit Type <span class="required">*</span>
                                        </label>
                                        <div class="form-group">
                                            <select class="form-control input-sm border-primary ddlreq1" id="ddlUnitType" runat="server" onchange="ChangeUnit()">
                                                <option value="0">-Select-</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-1">
                                        <label class="control-label" style="white-space: nowrap">
                                            Quantity <span class="required">*</span>

                                        </label>
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm req border-primary " id="txtReqQty" value="1" maxlength="6" runat="server" onkeypress="return isNumberKey(event)" />
                                        </div>
                                    </div>

                                    <div class="col-sm-1" id="divExchange" runat="server">
                                        <label class="control-label" style="white-space: nowrap">
                                            Is Exchange 
                                        </label>
                                        <div class="form-group">
                                            <input type="checkbox" id="chkExchange" onchange="changetype(this,'chkIsTaxable','chkFreeItem')" />
                                        </div>
                                    </div>
                                    <div class="col-sm-1" id="divTaxable">
                                        <label class="control-label" style="white-space: nowrap">
                                            Is Taxable 
                                        </label>
                                        <div class="form-group">
                                            <input type="checkbox" id="chkIsTaxable" onchange="changetype(this,'chkExchange','chkFreeItem')" />
                                        </div>
                                    </div>
                                    <div class="col-md-1 col-sm-1" id="divFree" runat="server">
                                        <label class="control-label" style="white-space: nowrap">Free Item</label>
                                        <div class="form-group">
                                            <input type="checkbox" id="chkFreeItem" onchange="changetype(this,'chkExchange','chkIsTaxable')" />
                                        </div>
                                    </div>
                                    <script>
                                        function changetype(e, idhtml1, idhtml2) {

                                            $('#' + idhtml1).prop('checked', false);
                                            $('#' + idhtml2).prop('checked', false);
                                        }
                                    </script>
                                    <div class="col-sm-2">
                                        <div class="pull-left">
                                            <button type="button" class="btn btn-md btn-success buttonAnimation pull-right round box-shadow-1 btn-sm" onclick="AddItemList()" id="btnAdd" style="margin-top: 22px;">&nbsp;&nbsp;&nbsp;Add&nbsp;&nbsp;&nbsp;</button>
                                        </div>
                                    </div>

                                </div>
                                <div class="clearfix"></div>
                                <div class="row">
                                    <div class="col-sm-6 form-group">
                                        <div class="alert alert-danger alertSmall" id="alertStockQty" style="text-align: center; display: none; color: white !important;"></div>
                                    </div>
                                    <div class="col-sm-6 form-group">
                                        <div class="alert alert-danger alertSmall" id="Div2" style="text-align: center; display: none"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="drag-area4">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body" id="panelBill4">

                                <style>
                                    .table th, .table td {
                                        padding: 0.75rem 1rem;
                                    }
                                </style>
                                <div class="table-responsive">
                                    <table id="tblProductDetail" class="table table-striped table-bordered">
                                        <thead class="bg-blue white">
                                            <tr>
                                                <td class="Action text-center" title="Delete" style="width: 4%">Action</td>
                                                <td class="ProId  text-center" style="width: 4%">ID</td>
                                                <td class="ProName">Product Name</td>
                                                <td class="UnitType  text-center" style="width: 4%">Unit</td>
                                                <td class="OM_MinPrice  text-center" style="display: none;">Min Price</td>
                                                <td class="OM_CostPrice  text-center" style="display: none;">Cost Price</td>
                                                <td class="OM_BasePrice  text-center" style="display: none;">Base Price</td>
                                                <td class="Barcode  text-center" style="display: none;">Barcode</td>
                                                <td class="ReqQty  text-center" style="width: 4%">Qty</td>
                                                <td class="TtlPcs  text-center" style="width: 4%">Total Pieces</td>
                                                <td class="UnitPrice  price" style="width: 4%">Unit Price </td>
                                                <td class="SRP  price" style="width: 4%">SRP</td>
                                                <td class="GP price" style="width: 4%">GP (%)</td>
                                                <td class="TaxRate text-center" style="width: 4%; display: none">Tax</td>
                                                <td class="IsExchange text-center" style="width: 4%; display: none">Exchange</td>
                                                <td class="Del_ItemTotal price" style="width: 4%">Item Total</td>
                                                <td class="Oim_Discount  price" style="width: 4%">Discount (%)</td>
                                                <td class="NetPrice price" style="width: 4%">Net Price</td>

                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                    <h5 class="well text-center" id="emptyTable">No Product Selected.</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="row">
            <div class="col-md-5 col-sm-5">
                <section id="drag-area3">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-content collapse show">
                                    <div class="card-body" id="panelBill3">

                                        <div id="Div1" class="panel panel-default">

                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-sm-12 form-group">
                                                        <label>Remark</label><textarea id="txtOrderRemarks" onmouseout="enableSubmitButton()" onfocus="disableSubmitButton()" onblur="updatedraftOrder()" class="form-control input-sm border-primary" maxlength="500"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12" id="CusCreditMemo" style="display: none">
                                                <div class="panel panel-default">
                                                    <%--<h4 class="form-section"></h4>--%>
                                                    <i class="la la-eye" style="position: relative; top: 2px;"></i>
                                                    <label>Customer Credit Memo</label>
                                                    <div class="panel-body" id="dCuCreditMemo" style="display: none">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="table-responsive">
                                                                    <table id="tblCreditMemoList" class="table table-striped table-bordered ">
                                                                        <thead class="bg-blue white">
                                                                            <tr>
                                                                                <td class="SRNO">Sr No.</td>
                                                                                <td class="Action rowspan" style="display: none">Action</td>
                                                                                <td class="CreditNo">Credit No.</td>
                                                                                <td class="CreditType">Credit Type</td>
                                                                                <td class="CreditDate">Credit Date</td>
                                                                                <td class="ReturnValue" style="text-align: right;">Total Value ($)</td>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        </tbody>
                                                                        <tfoot style="font-weight: 700; background: oldlace;">
                                                                            <tr style="text-align: right;">
                                                                                <td style="display: none" class="rowspan"><b></b></td>
                                                                                <td colspan="5"><b>Total</b></td>
                                                                                <td id="TotalDue"></td>
                                                                            </tr>
                                                                        </tfoot>
                                                                    </table>

                                                                </div>


                                                            </div>


                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="table-responsive">
                                                    <table id="tblMemoList" class="table table-striped table-bordered " style="display: none">
                                                        <thead class="bg-blue white">
                                                            <tr>
                                                                <td class="Action" style="width: 10%">Action</td>
                                                                <td class="CreditNo" style="width: 10%">Credit No.</td>
                                                                <td class="CreditType" style="width: 10%">Credit Type</td>
                                                                <td class="empName" style="width: 10%">deduction By</td>
                                                                <td class="PayDate" style="width: 20%">Deduction Date</td>
                                                                <td class="DeductionAmount" style="text-align: right; width: 20%">Deducted Amount&nbsp;(&nbsp;$&nbsp;)</td>
                                                                <td class="dueRemarks">Remark</td>

                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                        <tfoot style="font-weight: 700; background: oldlace;">
                                                            <tr style="text-align: right;">
                                                                <td colspan="5"><b>TOTAL</b></td>
                                                                <td id="Td4"></td>
                                                                <td id="Td1"></td>

                                                            </tr>
                                                        </tfoot>
                                                    </table>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>

            <div class="col-md-7 col-sm-7">
                <section id="drag-area11">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-content collapse show">
                                    <div class="card-body" id="panelBil13">

                                        <div style="text-align: right" id="orderSummary">
                                            <div class="row">
                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">Sub Total</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtTotalAmount" value="0.00" readonly="readonly" />
                                                    </div>
                                                </div>


                                                <div class="col-sm-6 form-group">
                                                    <b>
                                                        <label style="display: flex;">Order Total</label></b>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right; font-size: 14px; font-weight: 700;" readonly="readonly" id="txtGrandTotal" value="0.00" />
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="row">
                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">Overall Discount</label>
                                                    <div style="display: flex;">
                                                        <div class="input-group" style="width: 50%">

                                                            <input type="text" class="form-control input-sm border-primary" disabled="disabled" style="text-align: right;" onchange="updatedraftOrder()" id="txtOverallDisc" maxlength="6" value="0.00" onkeyup="calOverallDisc()" onkeypress="return isNumberDecimalKey(event,this)" />
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" style="padding: 0rem 1rem;"><span>%</span></span>
                                                            </div>
                                                        </div>
                                                        &nbsp;&nbsp;&nbsp;
                                                    <div class="input-group" style="width: 50%">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" disabled="disabled" style="text-align: right;" onfocus="this.select()" id="txtDiscAmt" value="0.00" onchange="updatedraftOrder()" onkeyup="calOverallDisc1()"
                                                            onkeypress="return isNumberDecimalKey(event,this)" />
                                                    </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">Credit Memo Amount</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtDeductionAmount" value="0.00" readonly="readonly" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>
                                            <div class="row">
                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">Shipping Charges</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">

                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" onchange="updatedraftOrder()" id="txtShipping" autocomplete="off" value="0.00" onkeyup="calGrandTotal()" onkeypress="return isNumberDecimalKey(event,this)" onfocus="this.select()" />

                                                    </div>
                                                </div>

                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex; white-space: nowrap">Store Credit Amount&nbsp;<span id="creditShow">(max <span id="CreditMemoAmount" class="text-bold-600">0.00</span> Available)</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtStoreCreditAmount" onkeypress="return isNumberDecimalKey(event,this)" onchange="changecredit()" onfocus="this.select()" value="0.00" readonly="readonly" />
                                                        <input type="hidden" id="hdnStoreCreditAmount" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>
                                            <div class="row">
                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">
                                                        Tax Type<span class="required"></span>

                                                    </label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>%</span></span>
                                                        </div>
                                                        <select class="form-control input-sm border-primary" id="ddlTaxType" onchange="calTotalTax()" runat="server">
                                                            <option value="0" taxvalue="0.0">-Select Tax Type-</option>
                                                        </select>
                                                    </div>

                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <b>
                                                        <label style="display: flex;">Payable Amount</label></b>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtPaybleAmount" readonly value="0.00" />
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="row">
                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">Total Tax</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtTotalTax" value="0.00" readonly="readonly" />
                                                    </div>
                                                </div>
                                                <div class="col-sm-6  form-group">
                                                    <label style="display: flex;">Past Due Amount</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right; font-size: 14px; font-weight: 700;" readonly id="txtPastDue" onkeypress="return isNumberDecimalKey(event,this)" value="0.00" onkeyup="paidAmount()" onchange="paidAmount()" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>
                                            <div class="row">
                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">ML Quantity</label>

                                                    <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtMLQty" value="0.00" readonly="readonly" />

                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <b>
                                                        <label style="display: flex;">Open Balance</label></b>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right; font-size: 14px; font-weight: 700;" readonly id="txtOpenBalance" onkeypress="return isNumberDecimalKey(event,this)" value="0.00" onkeyup="paidAmount()" onchange="paidAmount()" />
                                                    </div>
                                                </div>

                                            </div>


                                            <div class="clearfix"></div>
                                            <div class="row">
                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">ML Tax</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtMLTax" value="0.00" readonly="readonly" />
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">Paid Amount</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">

                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right; font-size: 14px; font-weight: 700;" id="txtPaidAmount" onblur="PaidAmountUsed()" onkeypress="return isNumberDecimalKey(event,this)" value="0.00" onkeyup="PaidAmountUsed()" onchange="PaidAmountUsed()" />
                                                        <input type="hidden" id="hdnPaidAmount" />
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">Weight Quantity</label>
                                                    <%-- <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>--%>
                                                    <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtWeightQty" value="0" readonly="readonly" />
                                                    <%--</div>--%>
                                                </div>

                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">Balance Amount</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtBalanceAmount" value="0.00" readonly="true" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">Weight Tax</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtWeightTex" value="0.00" readonly="readonly" />
                                                    </div>
                                                </div>

                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">Payment Method</label>
                                                    <select class="form-control input-sm border-primary" id="ddlPaymentMenthod" runat="server" disabled>
                                                        <option value="0">-Select Payment Method-</option>
                                                    </select>
                                                </div>

                                            </div>

                                            <div class="row">


                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">Adjustment</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;"><span>$</span></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtAdjustment" value="0.00" readonly="readonly" />
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <label style="display: flex;">Reference No </label>
                                                    <input type="text" maxlength="15" class="form-control input-sm border-primary" id="txtReferenceNo" runat="server" disabled />
                                                </div>

                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>
            </div>
        </div>

        <div class="card-footer">
            <div class="row">

                <div class="col-sm-12">
                    <div class="btn-group mr-1 pull-right">
                        <button type="button" id="btnSave" class="btn btn-success buttonAnimation pull-right round box-shadow-1 btn-sm animated undefined" onclick="saveOrder()"><b>Submit</b></button>
                    </div>
                    <div class="btn-group mr-1 pull-right">
                        <button type="button" id="btnReset" class=" btn btn-secondary buttonAnimation pull-right round box-shadow-1 btn-sm pulse" onclick="resetOrder()"><b>Reset</b></button>
                    </div>
                    <div class="btn-group mr-1 pull-right">
                        <button type="button" id="btnEditOdrer" class="btn btn-warning buttonAnimation pull-right round box-shadow-1 btn-sm pulse" style="display: none;" onclick="EditOrder()"><b>Edit</b></button>
                    </div>
                    <div class="btn-group mr-1 pull-right">
                        <button type="button" id="btnUpdateOrder" class="btn btn-primary buttonAnimation pull-right round box-shadow-1  btn-sm" style="display: none;" onclick="UpdateOrder()"><b>Update Order</b></button>
                    </div>

                    <div class="btn-group mr-1 pull-right">
                        <button type="button" id="btnGenOrderCCNew" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" style="display: none;" onclick="printOrder_CustCopyNew(0)"><b>Generate Customer Copy of Order</b></button>
                    </div>
                    <div class="btn-group mr-1 pull-right">
                        <button type="button" id="btnBackOdrer" class="btn btn-danger buttonAnimation pull-right round box-shadow-1  btn-sm" style="display: none;" onclick="BackOrder()"><b>Back</b></button>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <audio class="audios" id="yes_audio" controls preload="none" style="display: none">
        <source src="/Audio/NOExists.mp3" type="audio/mpeg" />
    </audio>
    <audio class="audios" id="No_audio" controls preload="none" style="display: none">
        <source src="/Audio/WrongProduct.mp3" type="audio/mpeg" />
    </audio>
    <audio class="audios" id="Stock" controls preload="none" style="display: none">
        <source src="/Audio/Stock.mp3" type="audio/mpeg" />
    </audio>


    <%--   </div>

                </div>
            </div>
        </section>
    </div>--%>
    <script type="text/javascript">
        function EditOrder() {

            $('#btnEditOdrer').hide();
            $('#btnBackOdrer').show();
            $('#btnUpdateOrder').show();
            $('#ddlShippingType').attr('disabled', false);
            $('#ddlShippingAddress').attr('disabled', false);
            $('#ddlBillingAddress').attr('disabled', false);
            //$('#txtDiscAmt').attr('disabled', false);
            $('#btnAdd').closest('.card-body').show();
            $('#ddlProduct').attr('disabled', false);
            $('#txtBarcode').attr('disabled', false);
            $('#ddlUnitType').attr('disabled', false);
            $('#txtReqQty').attr('disabled', false);
            $('#chkExchange').attr('disabled', false);
            $('#chkIsTaxable').attr('disabled', false);
            $('.Action').show();
            $("#tblCreditMemoList tbody tr").each(function () {
                $(this).find('.Action input').attr('disabled', false);
            });
            $('#tblProductDetail tbody tr').each(function () {
                $(this).find('.ReqQty input').attr('disabled', false);
                $(this).find('.Oim_Discount input').attr('disabled', false);
                if ($(this).find('.IsExchange span').attr('IsExchange') == '0' && $(this).find('.ProId span').attr('isfreeitem') == '0') {
                    $(this).find('.UnitPrice input').attr('disabled', false);
                }
            });
            //$('#txtOverallDisc').attr('disabled', false);
            $('#txtShipping').attr('disabled', false);
            $('#ddlTaxType').attr('disabled', false);
            $('#txtOrderRemarks').attr('disabled', false);
            $('#txtPaidAmount').attr('disabled', false);
            $('#ddlPaymentMenthod').attr('disabled', false);
            $('#txtReferenceNo').attr('disabled', false);
            $.ajax({
                type: "POST",
                url: "OrderNew.aspx/BindAddressCustomer",
                data: "{'CustomerAutoId':" + $("#ddlCustomer").val() + ",'OrderAutoId':" + $("#OrderAutoId").val() + "}",
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.d != "Session Expired") {
                        var CustomerDetail = $.parseJSON(response.d);
                        for (var i = 0; i < CustomerDetail.length; i++) {
                            var CustomerDetails = CustomerDetail[i];

                            var DuePayment = 0, DueAmount = 0;
                            if (CustomerDetails.OrderDetails != null) {
                                DuePayment = CustomerDetails.OrderDetails;
                                for (var k = 0; k < DuePayment.length; k++) {
                                    var due = DuePayment[k];
                                    if (parseFloat(due.AmtDue) > 0) {
                                        DueAmount += parseFloat(due.AmtDue);
                                    }
                                }
                                $('#StoreCredit').text(parseFloat(DueAmount).toFixed(2));
                                $('#txtPastDue').val(parseFloat(DueAmount).toFixed(2));
                                $('#txtOpenBalance').val(parseFloat(DueAmount).toFixed(2));
                            }
                            var CreditDetails = 0;
                            if (CustomerDetails.CreditDetails != null) {
                                CreditDetails = CustomerDetails.CreditDetails;
                                $("#tblCreditMemoList tbody tr").remove();
                                var rowc1 = $("#tblCreditMemoList thead tr").clone(true);
                                var TotalDue = 0.00;
                                for (var k = 0; k < CreditDetails.length; k++) {
                                    var cd = CreditDetails[k];
                                    $(".SRNO", rowc1).text(Number(k) + 1);
                                    if (cd.OrderAutoId != null) {
                                        $(".Action", rowc1).html("<input type='checkbox' OrderAutoId='" + cd.OrderAutoId + "' checked name='creditMemo' onclick='funcheckprop(this)' value=" + cd.CreditAutoId + "> ");
                                    } else {
                                        $(".Action", rowc1).html("<input type='checkbox' OrderAutoId='" + cd.OrderAutoId + "' name='creditMemo' onclick='funcheckprop(this)' value=" + cd.CreditAutoId + "> ");
                                    }
                                    $(".CreditNo", rowc1).html("<a target='_blank' href='/Sales/CreditMemo.aspx?PageId=" + cd.CreditAutoId + "'>" + cd.CreditNo + "</a>");
                                    $(".CreditDate", rowc1).text(cd.CreditDate);
                                    $(".ReturnValue", rowc1).text(cd.ReturnValue);
                                    $(".CreditType", rowc1).text(cd.CreditType);
                                    TotalDue = parseFloat(TotalDue) + parseFloat(cd.ReturnValue);
                                    $("#tblCreditMemoList tbody").append(rowc1)
                                    rowc1 = $("#tblCreditMemoList tbody tr:last").clone(true);
                                }
                                $("#TotalDue").text(TotalDue.toFixed(2));
                                $(".Action").show();
                                $('#CusCreditMemo').show();
                                $('#dCuCreditMemo').show();
                            }
                            calTotalAmount();
                            if (CustomerDetails.CreditAmount != null) {
                                if (parseFloat($("#txtGrandTotal").val()) > parseFloat($("#txtDeductionAmount").val())) {
                                    if (parseFloat(CustomerDetails.CreditAmount[0].CreditAmount) > 0) {
                                        $('#CreditMemoAmount').text(CustomerDetails.CreditAmount[0].CreditAmount);
                                        $("#txtStoreCreditAmount").removeAttr("disabled");
                                    }
                                    else {
                                        $("#txtStoreCreditAmount").attr("disabled", true);
                                    }
                                }
                                else if (parseFloat($("#txtGrandTotal").val()) == parseFloat($("#txtDeductionAmount").val())) {
                                    $("#txtStoreCreditAmount").attr("disabled", true);
                                }
                            }
                            $('#btnAdd').attr('disabled', false);
                        }
                    } else {
                        location.href = '/';
                    }
                },
                error: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                },
                failure: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                }
            });
        }
        function BackOrder() {

            $('#btnEditOdrer').show();
            $('#btnBackOdrer').hide();
            $('#btnUpdateOrder').hide();
            $('#ddlShippingAddress').attr('disabled', true);
            $('#ddlBillingAddress').attr('disabled', true);
            $('#btnAdd').closest('.card-body').hide();
            $('#ddlProduct').attr('disabled', true);
            $('#txtBarcode').attr('disabled', true);
            $('#ddlUnitType').attr('disabled', true);
            $('#txtReqQty').attr('disabled', true);
            $('#chkExchange').attr('disabled', true);
            $('#chkIsTaxable').attr('disabled', true);
            $('.Action').hide();
            $('#tblProductDetail input').attr('disabled', true);

            $('#txtOverallDisc').attr('disabled', true);
            $('#txtShipping').attr('disabled', true);
            $('#ddlTaxType').attr('disabled', true);
            $('#txtOrderRemarks').attr('disabled', true);
            $('#txtPaidAmount').attr('disabled', true);
            $('#ddlPaymentMenthod').attr('disabled', true);
            $('#txtReferenceNo').attr('disabled', true);
            bindOrderDetails($('#OrderAutoId').val());
        }
        function UpdateOrder() {
            if (parseFloat($("#txtPaidAmount").val()) > 0 && $("#ddlPaymentMenthod").val() == '0') {
                toastr.error('Payment method is required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                $("#ddlPaymentMenthod").addClass('border-warning');
                return;
            }
            $("#ddlPaymentMenthod").removeClass('border-warning');
            $('#txtOverallDisc').removeClass('border-warning');
            var flag1 = false, flag2 = false, flag3 = false, chkUnp = 0;
            var grandTotal = parseFloat($("#txtGrandTotal").val());
            var DeductionAmount = 0.00;
            if ($("#txtDeductionAmount").val() != "") {
                DeductionAmount = parseFloat($("#txtDeductionAmount").val());
            }

            var StoreCredit = $('#CreditMemoAmount').text();
            var StoreCreditAmount = 0.00;
            if ($('#txtStoreCreditAmount').val() != "") {
                StoreCreditAmount = $('#txtStoreCreditAmount').val()
            }
            var StoreCreditAmountUsed = 0.00;
            if ($("#hdnStoreCreditAmount").val() != "") {
                StoreCreditAmountUsed = $("#hdnStoreCreditAmount").val();
            }
            if (parseFloat(StoreCreditAmount) > parseFloat(parseFloat(StoreCredit) + parseFloat(StoreCreditAmountUsed))) {
                toastr.error('You can use maximum "' + StoreCredit + '" Store Credit !!', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });


            }
            else if (Number($('#ddlCustomer').val()) != 0) {
                if ($('#tblProductDetail tbody tr').length > 0) {

                    $("#tblProductDetail tbody tr").each(function () {
                        if ($(this).find(".ReqQty input").val() == "" || $(this).find(".ReqQty input").val() == null || $(this).find(".ReqQty input").val() == "0") {
                            $(this).find(".ReqQty input").focus().addClass("border-warning");
                            flag1 = true;
                        } else {
                            $(this).find(".ReqQty input").blur().removeClass("border-warning");
                        }

                        if ($(this).find(".ProId span").attr('isfreeitem') == 0 && $(this).find(".IsExchange span").attr('IsExchange') == 0 && Number($(this).find(".UnitPrice input").val()) < Number($(this).find(".UnitPrice input").attr('minprice'))) {
                            flag2 = true;
                            $(this).find(".UnitPrice input").focus().addClass("border-warning");
                        }
                        else if ($(this).find(".ProId span").attr('isfreeitem') == 0 && $(this).find(".IsExchange span").attr('IsExchange') == 0 && Number($(this).find(".UnitPrice input").val()) == 0) {
                            chkUnp = Number(chkUnp) + 1;
                            $(this).find(".UnitPrice input").focus().addClass("border-warning");
                        }
                        else {
                            $(this).find(".UnitPrice input").focus().removeClass("border-warning");
                        }
                    });
                    var CreditNo = '';
                    $("#tblCreditMemoList tbody tr").each(function () {
                        if ($(this).find('.Action input').prop('checked') == true) {
                            CreditNo += $(this).find('.Action input').val() + ',';

                        }
                    })
                    if (chkUnp > 0) {
                        toastr.error("Unit Price can't be blank or zero.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                        return;
                    }

                    if (!flag1 && !flag2 && !flag3) {
                        //$("#alertDOrder").hide();
                        var orderData = {
                            OrderAutoId: $('#OrderAutoId').val(),
                            CustomerAutoId: $('#ddlCustomer').val(),
                            ShippingType: $('#ddlShippingType').val(),
                            TotalAmount: $("#txtTotalAmount").val(),
                            OverallDisc: $("#txtOverallDisc").val(),
                            OverallDiscAmt: $("#txtDiscAmt").val(),
                            ShippingCharges: $("#txtShipping").val(),
                            TotalTax: $("#txtTotalTax").val(),
                            GrandTotal: $("#txtGrandTotal").val(),
                            TaxType: $("#ddlTaxType").val(),
                            PaidAmount: $("#txtPaidAmount").val(),
                            CreditMemo: $("#txtDeductionAmount").val(),
                            StoreCredit: $("#txtStoreCreditAmount").val(),
                            PayableAmount: $("#txtPaybleAmount").val(),
                            BalanceAmount: $("#txtBalanceAmount").val(),
                            PaymentMenthod: $("#ddlPaymentMenthod").val(),
                            ShippingAutoId: $("#ddlShippingAddress").val(),
                            BillingAutoId: $("#ddlBillingAddress").val(),
                            ReferenceNo: $("#txtReferenceNo").val(),
                            Remarks: $("#txtOrderRemarks").val(),
                            PastDue: $("#txtPastDue").val(),
                            CreditNo: CreditNo
                        };
                        if (parseFloat($('#txtOverallDisc').val()) > 100) {
                            $('#txtOverallDisc').addClass('border-warning');
                            toastr.error('Discount amount should be less than 100%.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                            return;
                        }

                        var Product = [];
                        $("#tblProductDetail tbody tr").each(function () {

                            Product.push({
                                ProductAutoId: $(this).find('.ProName').find('span').attr('ProductAutoId'),
                                UnitAutoId: $(this).find('.UnitType').find('span').attr('UnitAutoId'),
                                QtyPerUnit: $(this).find('.UnitType').find('span').attr('QtyPerUnit'),
                                QtyReq: $(this).find('.ReqQty input').val() || 0,
                                TotalPieces: $(this).find('.TtlPcs').text() || 0,
                                UnitPrice: $(this).find('.UnitPrice input').val() || 0,
                                SRP: $(this).find('.SRP').text() || 0,
                                GP: $(this).find('.GP').text() || 0,
                                Tax: $(this).find('.TaxRate span').attr('typetax'),
                                IsExchange: $(this).find('.IsExchange').find('span').attr('IsExchange'),
                                isFreeItem: $(this).find('.ProId').find('span').attr('isfreeitem'),
                                NetPrice: $(this).find('.NetPrice').text() || 0,
                                OM_MinPrice: $(this).find('.OM_MinPrice').text() || 0,
                                OM_CostPrice: $(this).find('.OM_CostPrice').text() || 0,
                                OM_BasePrice: $(this).find('.OM_BasePrice').text() || 0,
                                Barcode: $(this).find('.Barcode').text(),
                                Oim_Discount: parseFloat($(this).find('.Oim_Discount input').val()) || 0,
                            });
                        });

                        $.ajax({
                            type: "Post",
                            url: "OrderNew.aspx/updateOrderData",
                            data: JSON.stringify({ orderData: JSON.stringify(orderData), ProductDetails: JSON.stringify(Product) }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            beforeSend: function () {
                                $.blockUI({
                                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                                    overlayCSS: {
                                        backgroundColor: '#FFF',
                                        opacity: 0.8,
                                        cursor: 'wait'
                                    },
                                    css: {
                                        border: 0,
                                        padding: 0,
                                        backgroundColor: 'transparent'
                                    }
                                });
                            },
                            complete: function () {
                                $.unblockUI();
                            },
                            success: function (data) {
                                if (data.d == "OrderClosed") {
                                    toastr.error("Order has been closed.You can't update anything.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                                }
                                else if (data.d == "InvalidPOS") {
                                    swal("", "Unauthorized Access.", "error");
                                }
                                else if (data.d == "false") {
                                    swal("", "Oops, Something went wrong.", "error");
                                }
                                else if (data.d != "Session Expired") {
                                    var xmldoc = $.parseXML(data.d);
                                    var tbl = $(xmldoc).find("Table");
                                    var memoList = $(xmldoc).find("Table1");
                                    if ($(tbl).find("Type").text() == '') {
                                        toastr.error('Oops some thing wrong please check input', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                                    } else if ($(tbl).find("Type").text() == "Available") {
                                        //swal("", "Order updated Successfully.", "success").then(function () {
                                        //    bindOrderDetails($('#OrderAutoId').val());
                                        //});
                                        swal({
                                            text: "Order updated Successfully.",
                                            allowOutsideClick: false,
                                            closeOnClickOutside: false,
                                            icon: "success",
                                            buttons: {
                                                confirm: {
                                                    text: "Ok",
                                                    value: true,
                                                    visible: true,
                                                    className: "",
                                                    closeModal: true
                                                }
                                            }
                                        }).then(function (ok) {
                                            if (ok) {
                                                bindOrderDetails($('#OrderAutoId').val());
                                            }
                                        })
                                    }
                                    else if ($(tbl).find("Message").text() == "InvalidCRMemo") {
                                        $("#tblCRMStatusList tbody tr").remove();
                                        var row = $("#tblCRMStatusList thead tr").clone(true);
                                        $.each(memoList, function () {
                                            $(".CreditNo", row).text($(this).find("CreditNo").text());
                                            $(".SalesAmount", row).text($(this).find("SalesAmount").text());
                                            $(".Status", row).html('<span class="badge badge-sm badge-danger">' + $(this).find("Status").text() + '</span>');
                                            $('#tblCRMStatusList tbody').append(row);
                                            row = $("#tblCRMStatusList tbody tr:last").clone(true);
                                        });
                                        $("#CheckCreditMemo").modal('show');
                                    }
                                    else {
                                        $("#tblProductList tbody tr").remove();
                                        var row = $("#tblProductList thead tr").clone(true);
                                        $.each(tbl, function () {
                                            $(".Pop_ProId", row).text($(this).find("ProductId").text());
                                            $(".Pop_ProName", row).text($(this).find("ProductName").text());
                                            $(".Pop_AvilStock", row).text($(this).find("Stock").text());
                                            $('#tblProductList tbody').append(row);
                                            row = $("#tblProductList tbody tr:last").clone(true);
                                        });
                                        $("#CheckStock").modal('show');
                                    }
                                } else {
                                    location.href = '/';
                                }
                            },
                            error: function (result) {
                                $("#alertSOrder").show();
                                $("#alertSOrder span").text(result.d);
                            },
                            failure: function (result) {
                                console.log(JSON.parse(result.responseText).d);
                            }
                        });

                    } else {
                        if (flag1) {
                            toastr.error("Required quantity can't be zero or empty.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

                        } else {
                            toastr.error("Unit price can't be less than min price.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

                        }
                    }
                }
                else {
                    toastr.error('No product added into the table', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

                }
            } else {
                toastr.error('Please select customer', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

            }
        }

        $('#SavedMassege').modal('show');
        function OpenSecurity() {
            $("#mdSecurityCheck").modal('show');
            $("#mdAllowQtyConfirmation").modal('hide');
        }
        function CloseAllowConfirm() {
            $("#mdAllowQtyConfirmation").modal('hide');
            enableReadBarcode();
        }
        function enableReadBarcode() {
            $("#txtBarcode").removeAttr('disabled');
            $("#txtBarcode").focus();
            $("#txtBarcode").attr('onchange', 'readBarcode()');
        }
        function CheckManagerSecurity() {
            checkmanagersecuritykey()
        }
        function closeMnagerSecurity() {
            enableReadBarcode();
        }
        function checkmanagersecuritykey() {
            $.ajax({
                type: "Post",
                url: "OrderNew.aspx/CheckManagerSecurity",
                data: "{'SecurityKey':" + $("#txtManagerSecurityCheck").val() + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.d != "Session Expired") {
                        if (response.d == 'true') {
                            $("#mdSecurityCheck").modal('hide');
                            $("#txtManagerSecurityCheck").val('');
                            $("#modalOverrideQty").modal('show');
                            $("#txtAvailQty").val(AvailableAllocatedQty);
                            $("#txtRequiredQty").val(RequiredQty);
                        } else {
                            toastr.error('Access Denied.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                        }
                    } else {
                        location.href = '/';
                    }
                },
                error: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                },
                failure: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                }
            });
        }
        function OverrideQty() {
            enableReadBarcode();
            var ProductAutoId = 0
            if ($("#ddlProduct option:selected").val() == '0') {
                ProductAutoId = PrdtId;
            }
            else {
                ProductAutoId = $("#ddlProduct option:selected").val();
            }
            var Data = {
                CustomerAutoId: $('#ddlCustomer').val(),
                ProductAutoId: ProductAutoId,
                RequiredQuantity: $("#txtRequiredQty").val()
            }
            $.ajax({
                type: "Post",
                url: "OrderNew.aspx/AddAllocationQty",
                data: "{'dataValue':'" + JSON.stringify(Data) + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.d != "Session Expired") {
                        if (response.d == 'true') {
                            toastr.success('Qauntity allocated successfully', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                            $("#modalOverrideQty").modal('hide')
                        } else {
                            toastr.error('Oops,Something went wrong.Please try later', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                        }
                    } else {
                        location.href = '/';
                    }
                },
                error: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                },
                failure: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                }
            });
        }
        function disableSubmitButton() {
            $('#btnSave').attr('disabled', true);
        }
        function enableSubmitButton() {
            $('#btnSave').removeAttr('disabled');
        }

    </script>
    <div id="SavedMassege" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-md">

            <!-- Modal content-->
            <div class="modal-content">
                <br />
                <div class="modal-header">
                    <center style="width: 100%;"><h4>Order Generated.</h4><br /> <p>Do you want to Create Another Order ?</p></center>
                </div>

                <div class="modal-body">
                    <center>
                       <button type="button" class="btn btn-primary buttonAnimation round box-shadow-1 btn-sm" onclick="createorder()">Yes</button>
                       <button type="button" class="btn btn-danger buttonAnimation round box-shadow-1 btn-sm" onclick="nocreateorder()" >No</button>          
                    </center>
                </div>
                <div class="modal-body">
                    <center>
                       <p> Do you want to Print Invoice ?</p>                      
                       <button type="button" id="NoPrint" class="btn btn-success buttonAnimation  round box-shadow-1 btn-sm" onclick="PrintInvoice(1)" >Print</button>          
                    </center>
                </div>
            </div>
        </div>
    </div>

    <div id="CustomerDueAmount" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="display: block">
                    <button type="button" class="close" data-dismiss="modal" style="margin-right: 0;">&times;</button>
                    <h4 class="modal-title"><span id="lblCustomerName"></span>[Due Amount]</h4>
                    <h4 style="font-size: 15px; font-weight: bold"></h4>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered " id="tblduePayment">
                            <thead class="bg-blue white">
                                <tr>
                                    <td class="SrNo text-center">SN</td>
                                    <td class="OrderNo  text-center">Order No</td>
                                    <td class="OrderDate  text-center">Order Date</td>
                                    <td class="AmtDue price">Due Amount ($)</td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="3" class="text-right text-bold-600">Total Due Amount</td>
                                    <td id="TotalDueAmount" class="text-right text-bold-600"></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation  round box-shadow-1 btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div id="msgMassage" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">
            <!-- Modal content-->

            <div class="modal-content">
                <div class="modal-header">
                    <h4>Message</h4>
                </div>
                <div class="modal-body">
                    <div>
                        <strong style="border-color: #ac2925; border: 1px solid; padding: 8px 29px; color: #ac2925;" id="barcodemsg">Barcode does not exists</strong>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" onclick="focusonBarcode()">OK</button>
                </div>
            </div>
        </div>
    </div>
    <div id="modalOverrideQty" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">
            <!-- Modal content-->

            <div class="modal-content">
                <div class="modal-header">
                    <h4>Add Allocation Quantity</h4>
                </div>
                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col-md-6">
                            <label for="as">Available quantity</label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" disabled id="txtAvailQty" class="form-control input-sm border-primary" />
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6">
                            <label for="as">Required quantity</label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" maxlength="6" onkeypress="return isNumberKey(event)" id="txtRequiredQty" class="form-control input-sm border-primary" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success buttonAnimation  round box-shadow-1 btn-sm" onclick="OverrideQty()">Add</button>
                    <button type="button" class="btn btn-danger  buttonAnimation  round box-shadow-1 btn-sm" onclick="enableReadBarcode()" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        function focusonBarcode() {
            $("#msgMassage").modal('hide');
            $("#txtBarcode").focus();

        }
    </script>

    <style>
        ::-webkit-scrollbar {
            width: 5px;
            height: 5px;
            background: none;
        }
    </style>
    <div id="PopPrintTemplate" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="display: block">
                    <h4 class="modal-title">Select Invoice Template</h4>

                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <input type="radio" checked name="Template" id="chkdefault" /><label>&nbsp; PSM Default  - Invoice Only</label>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <input type="radio" name="Template" id="chkdueamount" /><label>&nbsp; PSM Default</label>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <input type="radio" name="Template" id="PackingSlip" /><label>&nbsp; Packing Slip</label>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <input type="radio" name="Template" id="WithoutCategorybreakdown" /><label>&nbsp; Packing Slip - Without Category Breakdown</label>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row" id="divPSMPADefault" style="display: none">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <input type="radio" name="Template" id="PSMPADefault" /><label>&nbsp; PSM PA Default</label>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row" id="divPSMWPADefault" style="display: none">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <input type="radio" name="Template" id="PSMWPADefault" /><label>&nbsp; PSM WPA-PA</label>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row" id="divPSMNPADefault" style="display: none">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <input type="radio" name="Template" id="rPSMNPADefault" />
                                    PSM NPA Default
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <input type="radio" name="Template" id="Template1" /><label>&nbsp; 7 Eleven (Type 1)</label>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <input type="radio" name="Template" id="Template2" /><label>&nbsp; 7 Eleven (Type 2)</label>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row form-group">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <input type="radio" name="Template" id="Templ1" />
                                    Template 1
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row form-group">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <input type="radio" name="Template" id="Templ2" />
                                    Template 2
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row" id="divPSMWPANDefault">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <input type="radio" name="Template" id="PSMWPANDefault" />
                                    <label for="PSMWPADefault">PSM PA - N</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1 btn-sm" onclick="PrintOrder()">Print</button>
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm" onclick="location.reload()" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mdAllowQtyConfirmation" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-md">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Message</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div style="border-color: #ac2925; border: 1px solid; color: #ac2925; min-height: 45px; line-height: 20px; padding: 9px;">
                                <strong>Product stock is not available for this sales person.<br />
                                    Do you want to override existing quantity with required quantity.</strong>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" onclick="OpenSecurity()">Yes</button>
                    <button type="button" class="btn btn-danger" onclick="CloseAllowConfirm()">No</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mdSecurityCheck" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Manager security check </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">Security </div>
                        <div class="col-md-9">
                            <input type="password" id="txtManagerSecurityCheck" class="form-control input-sm border-primary" />
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-12">
                            <span id="errormsgVoid"></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1 btn-sm" id="btnMulipleCheck" onclick="CheckManagerSecurity()">OK</button>
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm" data-dismiss="modal" onclick="closeMnagerSecurity()">Close</button>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="HDDomain" runat="server" />
    <script>
        /*-------------------------------REA------------------------------------------------------------------------------------------------*/
        //                                                         Print Order Customer Copy
        /*-------------------------------------------------------------------------------------------------------------------------------*/
        var j = 0;
        function printOrder_CustCopyNew(i) {
            $("#divPSMWPADefault").hide();
            if ($('#HDDomain').val() == 'psmpa') {
                $("#divPSMPADefault").show();
                $("#PSMPADefault").prop('checked', true);
            }
            else if ($('#HDDomain').val() == 'psmwpa') {
                $("#divPSMWPADefault").show();
            }
            else if ($('#HDDomain').val() == 'psmnpa') {
                $("#divPSMNPADefault").show();
                $("#divPSMPADefault").hide();
                $("#rPSMNPADefault").prop('checked', true);
            }
            else {
                $("#chkdueamount").prop('checked', true);
            }
            $('#PopPrintTemplate input').attr('disabled', false);
            $('#PopPrintTemplate').modal('show');
            j = i;
        }
        function PrintOrder() {

            $('#PopPrintTemplate').modal('hide');
            if ($("#chkdueamount").prop('checked') == true) {
                window.open("/Manager/OrderPrintCCF1.html?OrderAutoId=" + $("#txtHOrderAutoId").val() + "", "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
            } else if ($("#chkdefault").prop('checked') == true) {
                window.open("/Manager/OrderPrintCC.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
            }
            else if ($("#Template1").prop('checked') == true) {
                window.open("/Manager/OrderPrintCCF2.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
            }
            else if ($("#Template2").prop('checked') == true) {
                window.open("/Manager/OrderPrintCCF3.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
            } else if ($("#PSMPADefault").prop('checked') == true) {
                window.open("/Manager/OrderPrintCCF4.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
            }
            else if ($("#PSMWPADefault").prop('checked') == true) {
                window.open("/Manager/OrderPrintCCF6.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
            }
            else if ($("#PackingSlip").prop('checked') == true) {
                window.open("/Manager/OrderPrintCCF5.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
            }
            else if ($("#WithoutCategorybreakdown").prop('checked') == true) {
                window.open("/Manager/WithoutCategorybreakdown.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
            }
            else if ($("#rPSMNPADefault").prop('checked') == true) {
                window.open("/Manager/OrderPrintCCF4.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
            }
            else if ($("#Templ1").prop('checked') == true) {
                window.open("/Manager/PrintOrderItem.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
            }
            else if ($("#Templ2").prop('checked') == true) {
                window.open("/Manager/PrintOrderItemTemplate2.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
            }
            else if ($("#PSMWPANDefault").prop('checked') == true) {
                window.open("/Manager/OrderPrintCCFPN.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
            }
            if (j != 0)
                location.href = "/Warehouse/OrderNew.aspx";

        }

        function checkRequiredField1() {
            var boolcheck = true;
            $('.ddlreq1').each(function () {
                if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == '-Select-' || $(this).val() == null) {
                    boolcheck = false;
                    $(this).addClass('border-warning');
                } else {
                    $(this).removeClass('border-warning');
                }
            });
            $('.ddlSreq1').each(function () {
                if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == 'undefined' || $(this).val() == null || $(this).val() == 'Select') {
                    boolcheck = false;
                    $(this).closest('div').find('.select2-selection--single').attr('style', 'border:1px solid #FF9149  !important');
                } else {
                    $(this).removeClass('border-warning');
                    $(this).closest('div').find('.select2-selection--single').removeAttr('style');
                }
            });
            return boolcheck;
        }
    </script>
</asp:Content>

