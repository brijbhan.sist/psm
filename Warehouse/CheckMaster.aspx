﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="CheckMaster.aspx.cs" Inherits="Warehouse_CheckMaster" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .picker__holder {
            z-index: 9999999999999999999;
        }

        #checkbody1 {
            background-image: url("/images/check.png");
            background-color: #cccccc;
            background-size: cover;
            background-repeat: no-repeat;
            border: 1px double #c9cbcc;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Manage Check</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item active">Manage Check</li>
                         <li class="breadcrumb-item active" title="Help"><a href="#" onclick="GetPageInformation(10059)"><i class="la la-question-circle"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="txtCheckAutoid" />
    <div class="content-body" id="demo">
        <section id="drag-area">

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Check Details</h4>
                        </div>
                        <div class="card-content collapse show">

                            <div class="card-body" id="checkbody1">
                                <div class="row">
                                    <div class="col-md-6"></div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label">Check Number    <span class="required">*</span></label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" maxlength="10" class="form-control input-sm border-primary req" id="txtCheckId" onpaste="return false;" oncopy="return false;" onkeypress="return isNumberDecimalKey(event,this)" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">

                                            <div class="col-md-4">
                                                <label class="control-label">Type</label>
                                                <span class="required">*</span>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <select class="form-control input-sm border-primary req select2" id="ddlType" onchange="changet()" runat="server">
                                                    <option value="0">-Select Type-</option>
                                                    <option value="2">Employee</option>
                                                    <option value="1">Vendor</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label">Date</label>
                                                <span class="required">*</span>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                                            <span class="la la-calendar-o"></span>
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control input-sm border-primary date req" id="txtCheckDate" runat="server" />
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="well" id="CheckAmount"></div>
                                </div>

                                <div class="row">

                                    <div class="col-md-2">
                                        <label class="control-label" id="typeName">Name</label>
                                        <span class="required">*</span>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <select class="form-control input-sm border-primary ddlreq select2" id="ddlVendor" style="width: 100% !important" runat="server" onchange="bindVendorAddress()">
                                            <option value="0" id="selectName">Choose an option</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <label class="control-label">Check Amount</label>
                                        <span class="required">*</span>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text input-group-text-custom">
                                                    <span>$</span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control input-sm border-primary req" onchange="getCheckAmount();" id="txtCheckAmount" maxlength="10" placeholder="0" style="text-align: right" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-2">
                                    </div>
                                    <div class="col-md-10 form-group">
                                        <label class="control-label" id="txtNumofWords" style="border-bottom: 1px dashed #3068e4; background: transparent; width: 100%"></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <label class="control-label">Address</label>
                                        <span class="required">*</span>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <textarea class="form-control input-sm border-primary req" rows="3" id="txtVendorAddress" runat="server"></textarea>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <label class="control-label">Memo</label>
                                    </div>
                                    <div class="col-md-10 form-group">
                                        <textarea class="form-control input-sm border-primary" rows="1" id="txtDescription"></textarea>
                                    </div>
                                </div>


                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="btn-group mr-1 pull-right">
                                            <button type="button" runat="server" class="btn btn-success buttonAnimation round box-shadow-1  btn-sm" data-animation="pulse" id="btnSave">Save</button>
                                        </div>
                                        <div class="btn-group mr-1 pull-right">
                                            <button type="button" runat="server" class="btn btn-secondary buttonAnimation round box-shadow-1  btn-sm" data-animation="pulse" id="btnReset">Reset</button>
                                        </div>
                                        <div class="btn-group mr-1 pull-right">
                                            <button type="button" runat="server" class="btn btn-warning buttonAnimation round box-shadow-1 btn-sm" id="btnCancel" data-animation="pulse" style="display: none">Cancel</button>
                                        </div>
                                        <div class="btn-group mr-1 pull-right">
                                            <button type="button" runat="server" class="btn btn-primary buttonAnimation round box-shadow-1  btn-sm" data-animation="pulse" id="btnUpdate" style="display: none">Update</button>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Check List</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-3 form-group">
                                        <input type="text" class="form-control input-sm border-primary" placeholder="Check Number" id="txtSCheckId" />
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control input-sm border-primary date" placeholder="From Date" id="txtFromDate" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control input-sm border-primary date" placeholder="To Date" id="txtToDate" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <select class="form-control input-sm border-primary" id="ddlSearchType" runat="server" onchange="SearchtypeBind()">
                                            <option value="0">All Type</option>
                                            <option value="2">Employee</option>
                                            <option value="1">Vendor</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <select class="form-control input-sm border-primary" id="ddlSVendor">
                                            <option value="0">All Name</option>
                                        </select>
                                    </div>
                                    <div class="col-md-1 form-group">
                                        <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1 btn-sm" id="btnSearch">Search</button>
                                    </div>
                                </div>
                                <div class="row row form-group">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblCheckList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="action text-center">Action</td>
                                                        <td class="CheckNO text-center">Check Number</td>
                                                        <td class="CheckDate">Check Date</td>
                                                        <td class="Type">Type</td>
                                                        <td class="VendorName">Name</td>
                                                        <td class="CheckAmount price">Amount</td>
                                                        <td class="Memo">Memo</td>
                                                        <td class="VendorAddress">Address</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row container-fluid">
                                    <div>
                                        <select class="form-control input-sm border-primary" id="ddlPageSize" onchange="getCheckDetail(1)">
                                            <option selected="selected" value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="Pager"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <%------------------------------Security Check------------------------------------------------%>
            <div class="modal fade" id="SecurityEnabledVoid" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-sm">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4>Security Check </h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-3">Security </div>
                                <div class="col-md-9">
                                    <input type="password" id="txtSecurityVoid" class="form-control input-sm border-primary" />
                                </div>
                            </div>

                            <br />
                            <div class="row">
                                <div class="col-md-12">
                                    <span id="errormsgVoid"></span>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1 btn-sm" onclick="clickonSecurityVoid()">OK</button>
                            <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="SecurityEnvalid" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-xl">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4>Message</h4>
                        </div>
                        <div class="modal-body">
                            <div id="Envalid" style="border-color: #ac2925; border: 1px solid; color: #ac2925; min-height: 45px; line-height: 20px; padding: 9px;">
                                <strong id="Sbarcodemsg"></strong>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="JS/CheckMaster.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
</asp:Content>

