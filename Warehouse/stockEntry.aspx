﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" ClientIDMode="Static" CodeFile="stockEntry.aspx.cs" Inherits="Warehouse_stockEntry" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Stock Receive</h3>
            <input type="hidden" id="hiddenPackerAutoId" runat="server" />
            <input type="hidden" id="hiddenEmpType" runat="server" />
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage PO</a></li>             
                        <li class="breadcrumb-item active">Stock Receive</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
            <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <button id="Button5" type="button" onclick=" location.href='/Warehouse/stockEntryList.aspx'" class="btn btn-info round  dropdown-menu-right box-shadow-2 px-2  " animation="pulse">
                            Purchase Order List</button>
                    </li>
                </ol>
            </div>
        </div>
    </div>

    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <label class="col-md-1 col-sm-2 control-label">
                                        Vendor
                                    <span class="required">*</span>
                                    </label>
                                    <div class="col-md-3 col-sm-4 form-group">
                                        <select class="form-control input-sm border-primary ddlreq" id="ddlVendor" runat="server" onchange="onchangevendor();">
                                            <option value="0">-Select-</option>
                                        </select>
                                    </div>
                                    <label class="col-md-1 col-sm-2 control-label">
                                        Bill No
                                    <span class="required">*</span></label>

                                    <input type="hidden" id="txtHBillAutoId" />
                                    <div class="col-md-3 col-sm-4 form-group">
                                        <input type="text" id="txtBillNo" class="form-control input-sm border-primary req" maxlength="20" runat="server"/>
                                    </div>
                                    <label class="col-md-1 col-sm-2 control-label" style="white-space:nowrap">Bill Date
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-3 col-sm-4 form-group">
                                         <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text input-group-text-custom" style="padding:0rem 0.5rem !important">
                                                <span class="la la-calendar-o pl-0"></span>
                                            </span>
                                            </div>
                                        <input type="text" id="txtBillDate" class="form-control input-sm border-primary req" runat="server" onchange="updatedraftOrder()" />
                                             </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-md-1  col-sm-2 control-label">Remark</label>
                                    <div class="col-md-3  col-sm-4 form-group">
                                        <textarea class="form-control input-sm border-primary" maxlength="200" rows="1" id="txtRemarks" runat="server" onchange="updatedraftOrder()"></textarea>
                                          <span style="color:red;"><i>[Note :Remark upto 200 characters]</i></span>
                                    </div>
                                    <div class="clearfix"></div>
                                    <label class="col-md-1  col-sm-2 control-label">Status</label>
                                    <div class="col-md-3  col-sm-4 form-group">
                                        <input class="form-control input-sm border-primary" rows="1" id="txtStatus" disabled value="Draft" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">                      
                        <div class="panel panel-default" id="panelProduct">
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    
                                    <div id="pnlInputProduct">
                                        <div class="panel-body">
                                            <div class="row">
                                                <label class="col-md-1 col-sm-3 control-label">
                                                    Barcode                                
                                                </label>
                                                <div class="col-md-3 col-sm-3  form-group">
                                                    <input type="text" class="form-control input-sm border-primary" id="txtBarcode" runat="server" onfocus="this.select()" onkeypress="return isNumberKey(event)" placeholder="Enter Barcode here" onchange="readBarcode()" />
                                                </div>
                                                <label class="col-md-1  col-sm-3  control-label">Product
                                                    <span class="required">*</span></label>
                                                
                                                <div class="col-md-3  col-sm-3  form-group">
                                                    <select class="form-control input-sm border-primary ddladdreq" id="ddlProduct" runat="server" onchange="BindProductOnChenge()">
                                                        <option value="0">-Select-</option>
                                                    </select>
                                                </div>
                                                <label class="col-md-1 col-sm-3 control-label" style="white-space:nowrap">Unit Type 
                                                <span class="required">*</span></label>
                                                
                                                <div class="col-md-3 col-sm-3  form-group">
                                                    <select class="form-control input-sm border-primary ddlreq2" id="ddlUnitType" runat="server">
                                                        <option value="0">-Select-</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <label class="col-md-1 col-sm-3  control-label" style="white-space:nowrap">Quantity
                                                    <span class="required">*</span></label>
                                               
                                                <div class="col-md-3 col-sm-3  form-group">
                                                    <input type="text" class="form-control input-sm border-primary req2" maxlength="6" id="txtQuantity" runat="server" value="1" onfocus="this.select()" onkeypress='return isNumberKey(event)' />
                                                </div>
                                                <label class="col-md-1 col-sm-3  control-label" style="white-space: nowrap">Total Pieces</label>
                                                <div class="col-md-3 col-sm-3  form-group">
                                                    <div class="input-group">                                                       
                                                        <input type="text" class="form-control input-sm border-primary" id="txtTotalPieces" runat="server" disabled="disabled" value="0" />                                                                                                            
                                                    </div>
                                                </div>
                                                <div class="col-md-3"></div>
                                                <div class="col-md-1">
                                                    <div class="pull-right">
                                                        <button type="button" id="btnAdd" class="btn btn-purple buttonAnimation pull-right round box-shadow-1 btn-sm" runat="server">Add</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="table-responsive">
                                        <table id="tblProductDetail" class="table table-striped table-bordered">
                                              <thead class="bg-blue white">
                                                <tr>
                                                    <td class="Action text-center">Action</td>
                                                    <td class="ProId text-center">ID</td>
                                                    <td class="ProName">Product Name</td>
                                                    <td class="Unit text-center">Unit Type</td>
                                                    <td class="Qty text-center" style="width:5%;">Quantity</td>
                                                    <td class="Pcs text-center">Total Pieces</td>                                                    
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                            <tfoot>
                                                <tr>
                                                    <td id="act" colspan="4" class="text-right"  style="font-weight:bold;">Total</td>
                                                    <td id="TotalQty" style="text-align: center;font-weight:bold;">0</td>
                                                    <td id="TotalQtyNo"  style="text-align: center;font-weight:bold;">0</td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                        <h5 class="well text-center" id="emptyTable" style="display: none" runat="server">No Product Selected.</h5>
                                    </div>
                                    <div class="row">                                      
                                        <div class="col-md-12 form-group pull-right">
                                             
                                            <div class="pull-right mr-1">
                                                <button type="button" id="btnAddStock" class="btn btn-cyan buttonAnimation round box-shadow-1 btn-sm" runat="server">Save As Draft</button>
                                            </div>
                                            <div class="pull-right mr-1">
                                                <button type="button" id="btnUpdate" class="btn btn-primary round box-shadow-1 btn-sm" style="display: none" runat="server">Save As Draft</button>
                                            </div>
                                            <div class="pull-right mr-1">
                                                <button type="button" id="btnGeneratePO" class="btn btn-success buttonAnimation round box-shadow-1 btn-sm" runat="server">Submit P.O.</button>
                                            </div>
                                           <div class="pull-right mr-1">
                                                <button type="button" id="btnReset" class="btn btn-secondary buttonAnimation round box-shadow-1 btn-sm pulse animated" runat="server">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="/Warehouse/JS/stockEntry.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>

</asp:Content>

