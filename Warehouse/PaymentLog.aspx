﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/MasterPage.master" AutoEventWireup="true" CodeFile="PaymentLog.aspx.cs" Inherits="Warehouse_PaymentLog" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .tblwth {
            width: 8%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">By Payment History</h3>
            <input type="hidden" id="hiddenPackerAutoId" runat="server" />
            <input type="hidden" id="hiddenEmpType" runat="server" />
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Reports</a></li>
                        <li class="breadcrumb-item"><a href="#">Account Report  </a></li>
                        <li class="breadcrumb-item">By Payment History</li>
                        <li class="breadcrumb-item active" title="Help"><a href="#" onclick="GetPageInformation(10012)"><i class="la la-question-circle"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem();">Print</button>
                    <button type="button" class="dropdown-item" id="btnExport" onclick="ExportReport();">Export</button>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4 form-group">
                                        <select id="ddlCustomer" class="form-control border-primary input-sm" style="width: 100%;">
                                            <option value="0">ALL Customer</option>
                                        </select>
                                    </div>

                                    <div class="col-md-4 form-group">
                                        <select id="ddlCollectionBy" class="form-control border-primary input-sm" style="width: 100%;">
                                            <option value="0">ALL Receiver</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <select class="form-control input-sm" id="ddlStatus" style="width: 100%;">
                                            <option value="0">All</option>
                                            <option value="1">New</option>
                                            <option value="2">Settled</option>
                                            <option value="3">Cancelled</option>
                                        </select>
                                    </div>

                                    <div class="col-md-4 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 0.5rem;">From Received Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" onchange="setdatevalidation(1)" placeholder="From Received Date" id="FromDate" />
                                        </div>
                                    </div>

                                    <div class="col-md-4 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 0.5rem;">To Received Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" onchange="setdatevalidation(2)" placeholder="To Received Date" id="ToDate" />
                                        </div>
                                    </div>
                                    <div class="col-md-4 form-group">

                                        <input type="text" class="form-control border-primary input-sm" placeholder="Payment Id" id="txtPaymentid" />

                                    </div>

                                    <div class="col-md-4 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 0.5rem;">From Settled Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" onchange="setdatevalidation(3)" placeholder="From Settled Date" id="SettledFromDate" />
                                        </div>
                                    </div>

                                    <div class="col-md-4 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 0.5rem;">To Settled Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" onchange="setdatevalidation(4)" placeholder="To Settled Date" id="SettledToDate" />
                                        </div>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <select class="form-control input-sm" id="ddlRange" style="width: 100%;">
                                            <option value="0">All</option>
                                            <option value="1">=</option>
                                            <option value="2">></option>
                                            <option value="3"><</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <div class="input-group">
                                            <input type="text" class="form-control border-primary input-sm" value="0.00" placeholder="Payment Amount" id="txtPaymentAmt" style="text-align: right;" />
                                        </div>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <div class="input-group">
                                            <select class="form-control input-sm" id="ddlPaymentMode" style="width: 100%;">
                                                <option value="0">All Payment Mode</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <button type="button" class="btn btn-info buttonAnimation pull-left round box-shadow-1  btn-sm" data-animation="pulse" id="Button4" onclick="getPaymentList(1)">Search</button>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="table-responsive">
                                            <table class="MyTableHeader" id="tblCollectionDetails">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="Action text-center wth4">Action</td>
                                                        <td class="PayId  wth7">Pay ID</td>
                                                        <td class="CustomerName left">Customer Name</td>
                                                        <td class="ReceiveDate text-center tblwth">Receive Date</td>
                                                        <td class="Status text-center wth5">Status</td>
                                                        <td class="SettledDate text-center wth7">Settled Date</td>
                                                        <td class="PaymentType left wth8">Payment Type</td>
                                                        <td class="ReceivedBy left">Collect By</td>
                                                        <td class="ReceivedAmount price right wth6">Receive Amount</td>
                                                        <td class="PaymentMode text-center wth6">Payment Mode</td>
                                                        <td class="ReferenceId wth6">Reference ID</td>
                                                        <td class="ChequeNo text-center wth6">Check No</td>
                                                        <td class="ChequeDate text-center wth7">Check Deposit Date</td>
                                                        <td class="Remarks left wth8">Remark</td>
                                                        <td class="CancelRemarks left wth7">Cancel Remark</td>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                    <tr style="font-weight: bold;">
                                                        <td colspan="8">Total</td>
                                                        <td id="TotalAmount" class="right">0.00</td>
                                                        <td colspan="6"></td>
                                                    </tr>
                                                    <tr style="font-weight: bold;" id="trTotal">
                                                        <td colspan="8">Overall Total</td>
                                                        <td id="TotalAmounts" class="right">0.00</td>
                                                        <td colspan="6"></td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex">
                                    <div class="form-group">
                                        <select class="form-control input-sm border-primary" id="ddlPageSize" onchange="getPaymentList(1)">
                                            <option selected="selected" value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="ml-auto">
                                        <div class="Pager text-right"></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>
    <div class="table-responsive" style="display: none;" id="PrintTable1">
        <div class="row" style="margin-bottom: 5px;">
            <div class="col-md-4 col-lg-4 col-sm-4 text-left">
                <img src="" id="PrintLogo" height="40" />

            </div>
            <div class="col-md-4 col-lg-4 col-sm-4 text-center" style="margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;">
                By Payment History
                    <br />
                <span class="text-center DateRangeCSS" style="font-size: 9px; color: black;" id="DateRange"></span>

            </div>
            <div class="col-md-4 col-lg-4 col-sm-4 text-right" id="PrintDate" style="line-height: 3; font-size: 12px; color: black; font-weight: bold;">Print Date</div>
        </div>
        <table class="MyTableHeader PrintMyTableHeader" id="PrintTable">
            <thead class="bg-blue white">
                <tr>
                    <td class="P_PayId text-center wth7">Pay ID</td>
                    <td class="P_CustomerName left">Customer Name</td>
                    <td class="P_ReceiveDate text-center tblwth">Receive Date</td>
                    <td class="P_Status text-center wth5">Status</td>
                    <td class="P_SettledDate text-center wth7">Settled Date</td>
                    <td class="P_PaymentType text-center wth8">Payment Type</td>
                    <td class="P_ReceivedBy left">Collect By</td>
                    <td class="P_ReceivedAmount right wth6">Receive Amount</td>
                    <td class="P_PaymentMode text-center wth6">Payment Mode</td>
                    <td class="P_ReferenceId text-center wth6">Reference ID</td>
                    <td class="P_ChequeNo text-center wth6">Check No</td>
                    <td class="P_ChequeDate text-center wth7">Check Deposit Date</td>
                    <td class="P_Remarks left wth8">Remark</td>
                    <td class="P_CancelRemarks left wth8">Cancel Remark</td>
                </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="7" style="font-weight: bold !important;">Overall Total</td>
                    <td style="font-weight: bold !important;" id="TotalAmounta" class="right">0.00</td>
                    <td colspan="6"></td>
                </tr>
            </tfoot>
        </table>
    </div>


    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>

