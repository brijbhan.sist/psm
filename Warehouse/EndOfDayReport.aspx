﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="EndOfDayReport.aspx.cs" Inherits="Reports_EndOfDayReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
    <style type="text/css">
        /*.bootstrap-datetimepicker-wedget .dropdown-menu .buttom {
        z-index:999999999 !important;
           position:absolute
        }*/
        .bootstrap-datetimepicker-widget.dropdown-menu {
            z-index: 9999999999 !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Day End Report</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <%--<li class="breadcrumb-item"><a href="#">Reports</a></li>
                        <li class="breadcrumb-item"><a href="#">............</a></li>--%>
                        <li class="breadcrumb-item active">Day End Report</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <input type="button" id="btnAddBalance" value="Add End of Day" class="dropdown-item" />
                    <%--<button type="button" class="dropdown-item" id="btnExport">Export</button>--%>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="form-group col-md-3 form-group">
                                        <select id="ddlSalesPerson" class="form-control input-sm border-primary">
                                            <option selected="selected" value="0">-Select-</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3 form-group">
                                        <div class="input-group">
                                            <div class="input-group-append">
                                                <span class="input-group-text input-group-text-custom">
                                                    <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control input-sm border-primary" placeholder="From Date" id="txtfromDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3 form-group">
                                        <div class="input-group">
                                            <div class="input-group-append">
                                                <span class="input-group-text input-group-text-custom">
                                                    <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control input-sm border-primary" placeholder="To Date" id="txtToDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="form-group col-md-1 form-group">
                                        <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1  btn-sm" id="btnSearch">Search</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row container-fluid">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered" id="tblEndOfDayReport">
                                            <thead class="bg-blue white">
                                                <tr>
                                                    <td class="SalesPerson">Sales Person</td>
                                                    <td class="Date text-center">End Day Date</td>
                                                    <td class="EndTime text-center">End Day Time</td>
                                                    <td class="Status text-center"></td>
                                                </tr>

                                            </thead>
                                            <tbody>
                                            </tbody>
                                            <tfoot>
                                            </tfoot>
                                        </table>
                                        <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                    </div>
                                </div>
                                <div class="row container-fluid">
                                    <div>
                                        <select class="form-control input-sm border-primary" id="ddlPageSize">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">ALL</option>
                                        </select>
                                    </div>
                                    <div class="col-md-8">
                                        <div id="page" class="Pager"></div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </section>
    </div>
    <div id="modalAddBlc" class="modal fade" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">End of Day Form</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body" style="min-height: 250px">

                    <div class="row">
                        <div class="col-md-4">
                            <label class="control-label">Sales Person <span class="required">*</span></label>
                        </div>
                        <div class="col-md-8 form-group">
                            <select id="ddlSalePerson" class="form-control input-sm ddlreq" style="width: 100%" runat="server">
                                <option selected="selected" value="0">-Select-</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-4">
                            <label class="control-label">Date <span class="required">*</span></label>
                        </div>
                        <div class="col-md-8 form-group">
                            <div class="input-group">
                                <div class="input-group-append">
                                    <span class="input-group-text input-group-text-custom">
                                        <span class="la la-calendar"></span>
                                    </span>
                                </div>
                                <input type="text" class="form-control input-sm border-primary req" id="txtAddTime" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="control-label">Time <span class="required">*</span></label>
                        </div>
                        <div class="col-md-8 form-group">
                            <div class="input-group">
                                <div class="input-group-append">
                                    <span class="input-group-text input-group-text-custom">
                                        <span class="ft-clock"></span>
                                    </span>
                                </div>
                                <input type="text" class="form-control input-sm border-primary req" id="txtAddTime1" runat="server" />
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <div class="col-md-8 form-group" style="text-align: left">
                        <div class="alert alert-success alert-dismissable fade in" id="alertSuccess" style="display: none">
                            <a style="cursor: pointer;" aria-label="close" class="close">&times;</a>
                            <span></span>
                        </div>
                        <div class="alert alert-danger alert-dismissable fade in" id="alertDanger" style="display: none">
                            <a style="cursor: pointer;" aria-label="close" class="close">&times;</a>
                            <span></span>
                        </div>
                    </div>
                    <input type="button" value="Add" id="btnAdd" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" />
                    <input type="button" value="Update" id="btnAddUpdate" class="btn btn-primary buttonAnimation pull-right round box-shadow-1  btn-sm" style="display: none" />
                    <button type="button" class="btn btn-warning buttonAnimation pull-right round box-shadow-1  btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="SecurityEnvalid" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Message</h4>
                </div>
                <div class="modal-body">
                    <div id="Envalid" style="border-color: #ac2925; border: 1px solid; color: #ac2925; min-height: 45px; line-height: 20px; padding: 9px;">
                        <strong id="Sbarcodemsg"></strong>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="JS/EndOfDay.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
</asp:Content>

