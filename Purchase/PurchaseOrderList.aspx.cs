﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PurchaseOrderList : System.Web.UI.Page
{
   

    protected void Page_load(object sender, EventArgs e)
    {
        try
        {
            if (Session["EmpTypeNo"].ToString() == "3")
            {
                hiddenPackerAutoId.Value = Session["EmpAutoId"].ToString();                
            }
            else
            {
                hiddenPackerAutoId.Value = "";                
            }
            hiddenEmpType.Value = Session["EmpTypeNo"].ToString();
        }
        catch
        {
            Session.Abandon();
            Response.Redirect("~/Default.aspx", false);
        }
    }
}