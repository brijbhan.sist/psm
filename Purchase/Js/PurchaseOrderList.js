﻿$(document).ready(function () {
    bindStatus();
    BindVender();
    $('#txtSFromDate').pickadate({
        min: new Date("01/01/2019"),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtSToDate').pickadate({
        min: new Date("01/01/2019"),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    var d = new Date();
    var month = d.getMonth() + 1;
    if (month.toString().length == 1) {
        month = '0' + month;
    }
    var day = d.getDate();
    if (day.toString().length == 1) {
        day = '0' + day;
    }
    var year = d.getFullYear();
    $("#txtSFromDate").val(month + '/' + day + '/' + year);
    $("#txtSToDate").val(month + '/' + day + '/' + year);
    getOrderList(1);
})
function setdatevalidation(val) {
    var fdate = $("#txtSFromDate").val();
    var tdate = $("#txtSToDate").val();
    if (val == 1) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtSToDate").val(fdate);
        }
    }
    else if (val == 2) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtSFromDate").val(tdate);
        }
    }
}

function Pagevalue(e) {
    getOrderList(parseInt($(e).attr("page")));
};

function bindStatus() {
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/orderList.asmx/bindStatus",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var status = $(xmldoc).find("Table");
                $("#ddlStatus option:not(:first)").remove();
                $.each(status, function () {
                    $("#ddlStatus").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("StatusType").text() + "</option>");
                });

                //if (Number($("#hiddenEmpType").val()) == 7) {
                //    $("#ddlStatus").val('1');
                //}
            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function BindVender() {

    $.ajax({
        type: "POST",
        url: "/Purchase/WebAPI/PurchaseOrder.asmx/bindDropdown",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var vender = $(xmldoc).find("Table");
                $("#ddlVender option:not(:first)").remove();
                $.each(vender, function () {
                    $("#ddlVender").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("VendorName").text() + "</option>");
                });
                $("#ddlVender").select2();
            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function getOrderList(pageIndex) {
    var data = {
        PoNo: $("#txtPONo").val().trim(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        Status: $("#ddlStatus").val(), 
        PoStatus: $("#ddlPoStatus").val(),
        VenderAutoId: $("#ddlVender").val(),
        pageIndex: pageIndex,
        PageSize: $("#ddlPageSize").val(),
    };

    $.ajax({
        type: "POST",
        url: "/Purchase/WebAPI/PurchaseOrderList.asmx/getOrderList",
        data: JSON.stringify({ dataValues: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var POList = $(xmldoc).find("Table1");
                $("#tblOrderList tbody tr").remove();
                var row = $("#tblOrderList thead tr").clone(true);
                if (POList.length > 0) {
                    $("#EmptyTable").hide();
                    $.each(POList, function () {
                        $(".PONo", row).html("<span POAutoId='" + $(this).find("AutoId").text() + "'>" + $(this).find("PONo").text() + "</span>");
                        $(".PODate", row).text($(this).find("PODate").text());
                        $(".vender", row).text($(this).find("VendorName").text());
                        $(".VenderStatus", row).html("<span class='badge badge badge-pill' style='background-color:" + $(this).find("ColorCode").text() + ";'>" + $(this).find("StatusType").text() + "</span");
                        if ($(this).find("POStatus").text() == 'Pending') {
                            $(".POStatus", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("POStatus").text() + "</span>");
                        } else if ($(this).find("POStatus").text() == 'Received') {
                            $(".POStatus", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("POStatus").text() + "</span>");
                        }
                        $(".OrderTotal", row).text($(this).find("TotalOrderAmount").text());
                        $(".ReffNo", row).text($(this).find("OrderNo").text());
                        $(".itenms", row).text($(this).find("TotalNoOfItem").text());
                        $(".DDate", row).text($(this).find("DeliveryDate").text());
                        $(".PORemark", row).html('<p style="width:260px !Important;white-space: normal;text-align:justify">' + $(this).find("PORemarks").text() + '</p>');
                        $(".action", row).html("<a title='View' href='/Purchase/PurchaseOrderDetails.aspx?Id=" + $(this).find("AutoId").text() + "'><span class='la la-eye'></span></a> | <a href='javascript:;' onclick='print_NewOrder(" + $(this).find("AutoId").text() + ")'><span class='icon-printer' title='Print'></span></a>");
                        $("#tblOrderList tbody").append(row);
                        row = $("#tblOrderList tbody tr:last").clone(true);
                    });
                } else {
                    $("#EmptyTable").show();
                }

                var pager = $(xmldoc).find("Table");
                $(".Pager").ASPSnippets_Pager({
                    ActiveCssClass: "current",
                    PagerCssClass: "pager",
                    PageIndex: parseInt(pager.find("PageIndex").text()),
                    PageSize: parseInt(pager.find("PageSize").text()),
                    RecordCount: parseInt(pager.find("RecordCount").text())
                });
            } else {
                location.href = "/";
            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function print_NewOrder(num) {
    window.open("/Purchase/PurchaseOrderPrint.html?Id=" + num + "&type=" + 1, "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
}

$("#btnSearch").click(function () {
    getOrderList(1);
})

$("#ddlPageSize").change(function () {
    getOrderList(1);
})

function viewOrderLog(OrderAutoId) {
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/WOrderLog.asmx/viewOrderLog",
        data: "{'OrderAutoId':" + OrderAutoId + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var orderlog = $(xmldoc).find("Table");
            var order = $(xmldoc).find("Table1");

            $("#lblOrderNo").text(order.find("OrderNo").text());
            $("#lblOrderDate").text(order.find("OrderDate").text());

            $("#tblOrderLog tbody tr").remove();
            var row = $("#tblOrderLog thead tr").clone(true);
            if (orderlog.length > 0) {
                $("#EmptyTable").hide();
                $.each(orderlog, function (index) {
                    $(".SrNo", row).text(index + 1);
                    $(".ActionBy", row).text($(this).find("EmpName").text());
                    $(".Date", row).text($(this).find("ActionDate").text());
                    $(".Action", row).text($(this).find("Action").text());
                    $(".Remark", row).text($(this).find("Remarks").text());

                    $("#tblOrderLog tbody").append(row);
                    row = $("#tblOrderLog tbody tr:last").clone(true);
                });
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
    $("#modalOrderLog").modal('show');
}
$("#btnExport").click(function () {
    var ShippingType = '0';
    $("#ddlShippingType option:selected").each(function (i) {
        if (i == 0) {
            ShippingType = $(this).val() + ',';
        } else {
            ShippingType += $(this).val() + ',';
        }
    });
    var data = {
        OrderNo: $("#txtSOrderNo").val(),
        CustomerAutoid: $("#ddlCustomer").val(),
        SalesPersonAutoId: $("#ddlSalesPerson").val(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        Status: $("#ddlSStatus").val(),
        pageIndex: 1,
        ShippingType: ShippingType.toString(),
        PageSize: 0,
    };

    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/orderList.asmx/getOrderList",
        data: JSON.stringify({ dataValues: JSON.stringify(data) }), 
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('#fade').show();
        },
        complete: function () {
            $('#fade').hide();
        },
        success: function (response) {

            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var orderList = $(xmldoc).find("Table1");
                $("#tblExport tbody tr").remove();
                var row = $("#tblExport thead tr").clone(true);
                if (orderList.length > 0) {
                    $.each(orderList, function () {
                        $(".orderNo", row).html("<span OrderAutoId='" + $(this).find("AutoId").text() + "'>" + $(this).find("OrderNo").text() + "</span>");
                        $(".orderDt", row).text($(this).find("OrderDate").text());
                        $(".cust", row).text($(this).find("CustomerName").text());
                        $(".value", row).text($(this).find("GrandTotal").text());
                        $(".product", row).text($(this).find("NoOfItems").text());
                        $(".SalesPerson", row).text($(this).find("SalesPerson").text());
                        $(".status", row).html($(this).find("Status").text());
                        $("#tblExport tbody").append(row);
                        row = $("#tblExport tbody tr:last").clone(true);
                    });
                }
                $("#tblExport").table2excel({

                    exclude: ".noExl",
                    name: "Excel Document Name",
                    filename: "Order List",
                    fileext: ".xls",
                    exclude_img: true,
                    exclude_links: true,
                    exclude_inputs: true
                });

            } else {
                location.href = "/";
            }

        },
        failure: function (result) {
        },
        error: function (result) {
        }
    });
});