﻿var getid = "", WeightTax = 0.00,stts=0;
var PackerSecurityEnable = false;
$(document).ready(function () {
    debugger;
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    getid = getQueryString('Id');
    stts = getQueryString('status')||0;
    EditPoOrder(getid, stts);
});
function countRow() {
    //var count = 0;
    //$("#tblProductDetail tbody tr").each(function () {
    //    count++;
    //});
    //$('#TotalItem').html("Total Items : " + count);
}
$("#btnBack").on('click', function () {

    window.location = "/Purchase/PurchaseOrderlist.aspx"

})
//$("#btnDelete").on('click', function () {

//    deleterecord(getid);

//})
//$("#btnEdit").on('click', function () {
//    debugger
//    window.location.href = "/Purchase/PurchaseOrder.aspx?id=" + getid+"&status="+1;
//})
function backPO() {
    window.location = "/Purchase/PurchaseOrderlist.aspx";
}
function editPO() {
    window.location.href = "/Purchase/PurchaseOrder.aspx?id=" + getid + "&status=" + 1;
}
function deletePO() {
    deleterecord(getid);
}
function deleterecord(orderAutoId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this Order.",
        icon: "warning",
        showCancelButton: true,
        allowOutsideClick: false,
        closeOnClickOutside: false,
        buttons: {
            cancel: {
                text: "No, Cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, Delete it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            deleteOrder(orderAutoId);
        }
    })
}
function deleteOrder(orderAutoId) {
    $.ajax({
        type: "POST",
        url: "/Purchase/WebAPI/PurchaseOrderList.asmx/deleteOrder",
        data: "{'OrderAutoId':" + orderAutoId + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == "Session Expired") {
                location.href = "/";
            }
            else if (response.d == "Deleted") {

                swal({
                    title: "",
                    text: "Order details deleted successfully.",
                    icon: "success",
                    showCancelButton: true,
                    allowOutsideClick: false,
                    closeOnClickOutside: false,
                    buttons: {
                        confirm: {
                            text: "Ok",
                            value: true,
                            visible: true,
                            className: "",
                            closeModal: true
                        }
                    }
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        window.location = "/Purchase/PurchaseOrderlist.aspx"
                    }
                })
            }
            else if (response.d == "Not Deleted") {
                swal("", "Order can not deleted because order has been processed.", "error");
            }
            else {
                swal("", response.d, "error");
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function EditPoOrder(Id, count) {
    $.ajax({
        type: "POST",
        url: "/Purchase/WebAPI/PurchaseOrder.asmx/editOrder",
        data: "{'AutoId':'" + Id + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var OrderDetails = $(xmldoc).find("Table");
                var Products = $(xmldoc).find("Table1");
                if ($(OrderDetails).length > 0) {
                    $("#txtOrderId").val($(OrderDetails).find("PONo").text());
                    $("#ddlVender").val($(OrderDetails).find("VenderAutoId").text()).change();
                    $("#txtOrderDate").val($(OrderDetails).find("PODate").text());
                    $("#txtDeliveryDate").val($(OrderDetails).find("DeliveryDate").text())
                    $("#txtOrderStatus").val($(OrderDetails).find("Status").text());
                    $("#txtOrderRemarks").val($(OrderDetails).find("PORemarks").text());
                    $("#txtPOAutoId").val($(OrderDetails).find("AutoId").text());
                    $("#txtVender").val($(OrderDetails).find("VendorName").text());
                    $("#ddlVender").val($(OrderDetails).find("VenderAutoId").text());
                    $("#txtRefOrderNo").val($(OrderDetails).find("OrderNo").text());
                    $("#txtPOStatus").val($(OrderDetails).find("StockStatus").text());
                    $("#txtReciveOrderRemarks").val($(OrderDetails).find("RecieveStockRemark").text());

                    var poTotalPrice = 0,tr="";

                    debugger;
                    $.each(Products, function (index) {
                        if ($(OrderDetails).find('StatusAutoId').text() != "1") {
                            if ($(this).find("isFreeItem").text() == '0' && $(this).find("IsExchange").text() == '0') {
                                if ($(this).find('UnitType').text() == "") {
                                    tr += '<tr style="background-color:#f0f18f">';
                                }
                                else if ($(this).find('UnitType').text() != $(this).find('L_UnitType').text() && $(this).find('UnitType').text() != "") {
                                    tr += '<tr style="background-color:#f0f18f">';
                                }
                                else {
                                    tr += '<tr style="background-color:#fff">';
                                }
                            }
                            else {
                                tr += '<tr style="background-color:#fff">';
                            }
                        }
                        else {
                            tr += '<tr style="background-color:#fff">';
                        }
                        if ($(OrderDetails).find('StatusAutoId').text() == "11" && $("#txtPOStatus").val() == "Pending") {
                            if ($(this).find("isFreeItem").text() == '0' && $(this).find("IsExchange").text() == '0') {
                                if ($(this).find("PackedQty").text() != '0') {
                                    if (parseFloat($(this).find('CurrentCostPrice').text()) != parseFloat($(this).find('NewCostPrice').text())) {
                                        tr += '<tr style="background-color:#ee4c4c85">';
                                    }
                                    else {
                                        tr += '<tr style="background-color:#fff">';
                                    }
                                }
                                else {
                                    tr += '<tr style="background-color:#fff">';
                                }
                            }
                            else {
                                tr += '<tr style="background-color:#fff">';
                            }
                        }
                        else {
                            tr += '<tr style="background-color:#fff">';
                        }
                        if ($("#txtOrderStatus").val() == "Close" && $("#txtPOStatus").val() == "Pending") {
                            tr += '<td class="action" style="text-align:center"><a title="Edit" onclick="EditProductPrice(this)"><span class="la la-edit"></span></a></td>';
                        }
                        tr += '<td class="ProId text-center tblwth"><span Index=' + index + ' Free=' + $(this).find("isFreeItem").text() + ' Exchange=' + $(this).find("IsExchange").text() + '>' + $(this).find('ProductId').text() + '</span></td>';
                        
                        if ($(this).find("isFreeItem").text() == 1) {
                            tr += '<td class="ProName"><span ProductAutoId=' + $(this).find("L_ProductAutoId").text() + '></span><productName>' + $(this).find("ProductName").text() + '</productName> <item class="badge badge badge-pill badge-success">Free</item></td>';
                        }
                        else if ($(this).find("IsExchange").text() == 1) {
                            tr += "<td class='ProName'><span ProductAutoId='" + $(this).find("L_ProductAutoId").text() + "'></span><productName>" + $(this).find("ProductName").text() + "</productName> <item class='badge badge badge-pill badge-primary'>Exchange</item></td>";
                        }
                        else {
                            tr += "<td class='ProName'><span ProductAutoId='" + $(this).find('L_ProductAutoId').text() + "'>" + $(this).find('ProductName').text() + "</span></td>";
                        }
                        if ($(this).find('UnitType').text() != "") {
                            tr += "<td class='UnitType' style='text-align:center'><span UnitAutoId=" + $(this).find('Unit').text() + ">" + $(this).find('UnitType').text() + " (" + $(this).find('QtyPerUnit').text() + ")" + "</span></td>";
                        }
                        else {
                            tr += "<td class='UnitType' style='text-align:center'>--</td>";
                        }

                        debugger;
                        if ($(this).find('L_Unit').text() == "") {
                            tr += "<td class='ReqQty'  style='text-align:center'>" + $(this).find('Qty').text() + "</td>";

                        }
                        else {
                            tr += "<td class='ReqQty'  style='text-align:center'>" + $(this).find('Qty').text() + "<span UnitAutoId=" + $(this).find('L_Unit').text() + "></span></td>";

                        }
                        tr += "<td class='TtlPcs' style='text-align:center'>" + $(this).find('TotalPieces').text() + "</td>";

                        if ($(this).find('L_UnitType').text() != "") {
                            tr += "<td class='PackdUnit' style='text-align:center'>" + $(this).find('L_UnitType').text() + " (" + $(this).find('L_QtyPerUnit').text() + ")</td>";

                        } else {
                            tr += "<td class='PackdUnit' style='text-align:center'>--</td>";
                        }
                        tr += "<td class='PackdQty' style='text-align:center'>" + $(this).find('PackedQty').text()||0 + "</td>";
                        tr += "<td class='PackdPieces' style='text-align:center'>" + $(this).find('TotalPackedPieces').text()+ "</td>";

                        if ($("#txtOrderStatus").val() == "Close" && $("#txtPOStatus").val() == "Pending") {
                            if (parseInt($(this).find('PackedQty').text()) != 0) {
                                tr += "<td style='text-align:right' class='CCostPrice'>" + parseFloat($(this).find('CurrentCostPrice').text()).toFixed(2) +"</td>";
                                tr += "<td style='text-align:right' class='NewCostPrice'>" + $(this).find('NewCostPrice').text() + "</td>";
                            }
                            else {
                                tr += "<td style='text-align:right' class='CCostPrice'>0.00</td>";
                                tr += "<td style='text-align:right' class='NewCostPrice'>0.00</td>";
                            }
                        }
                        if ($(OrderDetails).find('StatusAutoId').text() == "11" && $("#txtPOStatus").val() == "Pending") {
                            tr += "<td style='text-align:right' class='NetPrice'>" + parseFloat(parseFloat($(this).find('PackedQty').text()) * parseFloat($(this).find('NewCostPrice').text())).toFixed(2) + "</td>";
                            poTotalPrice = poTotalPrice + parseFloat($(this).find('PackedQty').text()) * parseFloat($(this).find('NewCostPrice').text())
                        }                       
                    });
                    $('#tblProductDetail tbody').append(tr);
                    countRow();


                    if (($(OrderDetails).find("StatusAutoId").text()) == "11" && ($(OrderDetails).find("StockStatus").text()) != "Received") {
                        $(".RevieveRemark").show();
                        $("#UpdateStock").show();
                        $("#tblProductDetail tbody tr").find(".NewCostPrice").show();
                        $("#tblProductDetail thead tr").find(".NewCostPrice").show();
                        $("#tblProductDetail tbody tr").find(".CCostPrice").show();
                        $("#tblProductDetail thead tr").find(".CCostPrice").show();
                        $("#tblProductDetail thead tr").find(".Head").show();
                        $("#tblProductDetail thead tr").find(".HeadR").show();//added
                        $("#tblProductDetail thead tr").find(".action").show();
                        $("#tblProductDetail tbody tr").find(".action").show();
                        $("#tblProductDetail tbody tr").find(".NetPrice").show();
                        $("#spTotalPrice").html($(OrderDetails).find("TotalOrderAmount").text());
                    }
                    else if (($(OrderDetails).find("StatusAutoId").text()) == "11" && ($(OrderDetails).find("StockStatus").text()) == "Received") {
                        $(".RevieveRemark").show();
                        $("#tblProductDetail thead tr").find(".HeadR").show();//added
                        $("#txtReciveOrderRemarks").attr('disabled', 'disabled');
                        $("#tdItem").attr('colspan', 11);
                        $("#tdPriceTD").attr('colspan', 11);
                        $("#tdPrice").hide();
                    }
                    else if (($(OrderDetails).find("StatusAutoId").text()) == "1") {
                        $("#btnEdit").show();
                        $("#btnDelete").show();
                    }
                    else {
                        $("#btnCancel").show();
                        $("#UpdateStock").hide();
                    }
                    if (($(OrderDetails).find("StatusAutoId").text()) != "1") {
                        $("#tblProductDetail thead tr").find(".PackdQty").show();
                        $("#tblProductDetail tbody tr").find(".PackdQty").show();
                        $("#tblProductDetail thead tr").find(".PackdUnit").show();
                        $("#tblProductDetail tbody tr").find(".PackdUnit").show();
                        $("#tblProductDetail thead tr").find(".PackdPieces").show();
                        $("#tblProductDetail tbody tr").find(".PackdPieces").show();
                        $("#tblProductDetail thead tr").find(".HeadR").show();
                        if ($(OrderDetails).find("StatusAutoId").text() == '11' && ($(OrderDetails).find("StockStatus").text()) != "Received") {
                            $("#tdItem").attr('colspan', 12);
                            $("#tdPrice").show();
                        }
                        else {
                            $("#tdItem").attr('colspan', 8);
                            $("#tdPrice").hide();
                        }
                    }
                    else {
                        $("#tblProductDetail thead tr").find(".PackdQty").hide();
                        $("#tblProductDetail tbody tr").find(".PackdQty").hide();
                        $("#tblProductDetail thead tr").find(".PackdUnit").hide();
                        $("#tblProductDetail tbody tr").find(".PackdUnit").hide();
                        $("#tblProductDetail thead tr").find(".PackdPieces").hide();
                        $("#tblProductDetail tbody tr").find(".PackdPieces").hide();
                        $("#tblProductDetail tbody tr").find(".NewCostPrice").hide();
                        $("#tblProductDetail thead tr").find(".NewCostPrice").hide();
                        $("#tblProductDetail tbody tr").find(".CCostPrice").hide();
                        $("#tblProductDetail thead tr").find(".CCostPrice").hide();
                        $("#tblProductDetail thead tr").find(".NetPrice").hide();
                        $("#tdItem").attr('colspan', 11);
                        $("#tdPrice").hide();
                    }
                }
                $("#PageDiv").show();
                //if (($(OrderDetails).find("StatusAutoId").text()) != "11" && ($(OrderDetails).find("StockStatus").text()) != "Received") {
                if (($(OrderDetails).find("StatusAutoId").text()) == "1") {
                    var inactiveProduct = $(xmldoc).find("Table3");
                }
                else {
                    var inactiveProduct = $(xmldoc).find("Table2");
                }
                if (inactiveProduct.length > 0) {
                    $("#mdlInactiveProductList").modal('show');
                    $("#tblInactiveProductList tbody tr").remove();
                    var row = $("#tblInactiveProductList thead tr:last-child").clone();
                    $.each(inactiveProduct, function (index) {
                        $(".ProductId", row).html($(this).find('ProductId').text());
                        $(".ProductName", row).html($(this).find('ProductName').text());
                        $(".Status", row).html('<span class="badge badge-pill badge-warning">' + $(this).find('ProductStatus').text() + '</span>');
                        $('#tblInactiveProductList tbody').append(row);
                        row = $("#tblInactiveProductList tbody tr:last-child").clone(true);
                    });
                }
                //}
            }
        }
    });
}
//$("#UpdateStock").on('click', function () {
function updateStock() {
    var prc = false;
    $("#tblProductDetail tbody tr").each(function () {
        if ($(this).find('.ProId span').attr('Exchange') == '0' && $(this).find('.ProId span').attr('Free') == '0') {
            if ($(this).find(".PackedQty").text() != '0') {
                var OldP = parseFloat($(this).find('.CCostPrice').text());
                var NewP = parseFloat($(this).find('.NewCostPrice').text());
                if (OldP != NewP) {
                    prc = true;
                }
            }
        }
    })
    if (prc) {
        toastr.error('Please manage price before update.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    else {
        swal({
            title: "Are you sure?",
            text: "You want to receive stock.",
            icon: "warning",
            showCancelButton: true,
            allowOutsideClick: false,
            closeOnClickOutside: false,
            buttons: {
                cancel: {
                    text: "No, Cancel!",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: true,
                },
                confirm: {
                    text: "Yes,Receive It",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
                }
            }
        }).then(function (isConfirm) {
            if (isConfirm) {
                ReceiveStock();
            }
        })
    }
}
//});
function ReceiveStock() {
    var data = {
        VenderAutoId: $("#ddlVender").val(),
        PONo: $("#txtOrderId").val(),
        POAutoId: $("#txtPOAutoId").val(),
        Remark: $("#txtReciveOrderRemarks").val()
    }

    $.ajax({
        type: "POST",
        url: "/Purchase/WebAPI/PurchaseOrderList.asmx/UpdateStock",
        data: JSON.stringify({ data: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d == "Success") {

                swal({
                    title: "",
                    text: "Stock has been received.",
                    icon: "success",
                    showCancelButton: true,
                    allowOutsideClick: false,
                    closeOnClickOutside: false,
                    buttons: {
                        confirm: {
                            text: "Ok",
                            value: true,
                            visible: true,
                            className: "",
                            closeModal: true
                        }
                    }
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        window.location.href = "/Purchase/PurchaseOrderDetails.aspx?id=" + getid;
                    }
                })
            }
            else if (response.d == "AlreadyReceived")
            {
                swal({
                    title: "",
                    text: "Stock has been already received.",
                    icon: "warning",
                    showCancelButton: true,
                    allowOutsideClick: false,
                    closeOnClickOutside: false,
                    buttons: {
                        confirm: {
                            text: "Ok",
                            value: true,
                            visible: true,
                            className: "",
                            closeModal: true
                        }
                    }
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        window.location.href = "/Purchase/PurchaseOrderDetails.aspx?id=" + getid;
                    }
                })
            }
            else {
                swal({
                    title: "",
                    text: "Oops, Something Went wrong.Please try later",
                    icon: "error",
                    showCancelButton: true,
                    allowOutsideClick: false,
                    closeOnClickOutside: false,
                    buttons: {
                        confirm: {
                            text: "Ok",
                            value: true,
                            visible: true,
                            className: "",
                            closeModal: true
                        }
                    }
                });
            }

        }
    });


}
function EditProductPrice(e) {
    var row = $(e).closest("tr");
    var ProductId = parseInt(row.find(".ProId  > span").html());
    var UnitType = parseInt(row.find(".ReqQty > span").attr("UnitAutoId"));
    $("#Index").val(parseInt(row.find(".ProId > span").attr("index")));
    managePrice(ProductId, UnitType);
}
function PopupManagePrice(e) {
    var row = $(e).closest("tr");

    var CPrice = parseFloat(row.find(".CCostPrice").text())
    var Price = parseFloat(row.find(".CostPrice > input").val()).toFixed(3);
    var ProductId = parseInt(row.find(".ProName > span").attr("ProductAutoId"))
    var UnitType = parseInt(row.find(".UnitType > span").attr("UnitAutoId"))
    UpdatePriceProductWise(ProductId, UnitType, Price);
}
function UpdatePriceProductWise(ProductId, UnitType, CostPrice) {
    var data = {
        ProductAutoId: ProductId,
        UnitAutoId: UnitType,
        CostPrice: CostPrice,
        POAutoId: $("#txtPOAutoId").val()
    }
    $.ajax({
        type: "POST",
        url: "RecievePoByVendor.aspx/UpdateCostPriceOnPO",
        data: "{'DataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d == "true") {
                    swal("", "Price has been saved as draft.", "success");
                }
            }
            else {
                location.href = '/';
            }

        },
        error: function (result) {
        },
        failure: function (result) {
            swal("Error!", 'Oops,some thing went wrong.Please try later.', "error");
        }
    });
}
function managePrice(ProductId, UnitType) {
    $.ajax({
        type: "POST",
        url: "PurchaseOrderDetails.aspx/getManagePrice",
        data: "{'ProductId':'" + ProductId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var packingDetails = $(xmldoc).find("Table");
                var product = $(xmldoc).find("Table1");
                //ProductText
                $("#Table1 tbody tr").remove();
                var row = $("#Table1 thead tr:last-child").clone();

                $(packingDetails).each(function (index) {
                    $(".UnitType", row).html($(this).find('UnitName').text() + "<span UnitType=" + $(this).find('UnitType').text() + "></span>");
                    $(".Qty", row).html($(this).find('Qty').text());
                    if ($(this).find('UnitType').text() == '3') {
                        $(".NewCosePrice", row).html("<input type='text' style='width:80px;text-align:right' onchange='funchangeprice(this)' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm UnitNewCost'  value='" + $(this).find('CostPrice').text() + "'/>");
                    } else {
                        $(".NewCosePrice", row).html("<input type='text' style='width:80px;text-align:right' onchange='funchangeprice(this)' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm'  value='" + $(this).find('CostPrice').text() + "'/>");
                    }
                    $(".CostPrice", row).html($(this).find('CostPrice').text());
                    $(".MinPrice", row).html($(this).find('MinPrice').text());
                    $(".BasePrice", row).html($(this).find('Price').text());
                    if ($(this).find('UnitType').text() == '3') {
                        $(".NewBasePrice", row).html("<input type='text' style='width:80px;text-align:right' onchange='funchangeprice(this)' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm UnitBasePrice'  value='" + $(this).find('Price').text() + "'/>");
                    } else {
                        $(".NewBasePrice", row).html("<input type='text' style='width:80px;text-align:right' onchange='funchangeprice(this)' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm'  value='" + $(this).find('Price').text() + "'/>");
                    }
                    $(".RetailPrice", row).html($(this).find('SRP').text());
                    if ($(this).find('UnitType').text() == '3') {
                        $(".NewRetailPrice", row).html("<input type='text' style='width:80px;text-align:right' onchange='funchangeprice(this)' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm UnitRetailPrice'  value='" + $(this).find('SRP').text() + "'/>");
                    } else {
                        $(".NewRetailPrice", row).html("<input type='text' style='width:80px;text-align:right' onchange='funchangeprice(this)' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm'  value='" + $(this).find('SRP').text() + "'/>");
                    }
                    $(".WHminPrice", row).html($(this).find('WHminPrice').text());
                    if ($(this).find('UnitType').text() == '3') {
                        $(".NWHPrice", row).html("<input type='text' style='width:80px;text-align:right'  onchange='funchangeprice(this)'  onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm UnitWHPrice'  value='" + $(this).find('WHminPrice').text() + "'/>");
                    } else {
                        $(".NWHPrice", row).html("<input type='text' style='width:80px;text-align:right'  onchange='funchangeprice(this)'  onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm'  value='" + $(this).find('WHminPrice').text() + "'/>");
                    }
                    if (parseInt($(this).find('UnitType').text()) != UnitType) {
                        row.find('input').attr("disabled", true);
                        row.find('td').css("background-color", 'white');
                        row.find('.NewCosePrice input').attr("defaultunit", '0');
                    }
                    else {
                        row.find('input').attr("disabled", false);
                        row.find('.NewCosePrice input').attr("defaultunit", UnitType);
                        row.find('td').css("background-color", '#79f3c2');
                    }
                    $("#Table1 tbody").append(row);
                    row = $("#Table1 tbody tr:last-child").clone();
                    $("#ProductText").html($(product).find('ProductName').text());
                    $("#txtHProductAutoId").val(ProductId);

                });
            } else {
                location.href = '/';
            }
        },
        error: function (result) {
        },
        failure: function (result) {
        }
    });
    $('#managePrice').modal('show');
}
function funchangeprice(e) {
    var className = $(e).closest('td').attr('class');
    var tr = $(e).closest('tr');
    var UnitAutoId = tr.find('.UnitType span').attr('UnitType');
    var Price = 0.00;
    if ($(e).val() != '') {
        Price = $(e).val() || 0.00;
    }
    var Qty = parseInt(tr.find('.Qty').text());
    var UnitPrice = 0.00;
    UnitPrice = parseFloat((parseFloat(Price) / Qty)).toFixed(3) || 0.00;
    $("#Table1 tbody tr").each(function () {
        var amount = (parseInt($(this).find('.Qty').text()) * UnitPrice).toFixed(3);
        if (UnitAutoId != $(this).find('.UnitType span').attr('UnitType')) {
            if (className == 'NewCosePrice') {
                $(this).find('.NewCosePrice input').val(parseFloat(amount).toFixed(3));
            } else if (className == 'NewBasePrice') {
                $(this).find('.NewBasePrice input').val(parseFloat(amount).toFixed(3));
            } else if (className == 'NewRetailPrice') {
                $(this).find('.NewRetailPrice input').val(parseFloat(amount).toFixed(3));
            } else if (className == 'NWHPrice') {
                $(this).find('.NWHPrice input').val(parseFloat(amount).toFixed(3));
            }
        }

    });
}
function updatebulk() {
    var prc = false;
    $("#Table1 tbody tr").each(function () {
        var Price = Number($(this).find('input').val());
        if (Price == 0 || Price == null) {
            prc = true;
            $(this).find('.NewCosePrice input').addClass('border-warning');
            $(this).find('.NewBasePrice input').addClass('border-warning');
            $(this).find('.NewRetailPrice input').addClass('border-warning');
            $(this).find('.NWHPrice input').addClass('border-warning');
        }
    })
    if (prc) {
        toastr.error('Please manage price before update.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    var ProductAutoId = $("#txtHProductAutoId").val();
    var dataTable = [];
    var chk1 = false; i = 0;
    $('#Table1 tbody tr').each(function (index) {
        dataTable[i] = new Object();
        dataTable[i].ProductAutoId = ProductAutoId,
            dataTable[i].UnitAutoId = $(this).find('.UnitType span').attr('UnitType');
        dataTable[i].CostPrice = $(this).find('.NewCosePrice input').val();
        dataTable[i].BasePrice = $(this).find('.NewBasePrice input').val();
        dataTable[i].RetailPrice = $(this).find('.NewRetailPrice input').val();
        dataTable[i].WHPrice = $(this).find('.NWHPrice input').val();
        if (parseFloat($(this).find('.NewCosePrice input').val()) > parseFloat($(this).find('.NWHPrice input').val())) {
            chk1 = true;
            $(this).find('.NewCosePrice input').addClass('border-warning');
        } else {
            $(this).find('.NewCosePrice input').removeClass('border-warning');
        }
        if (parseFloat($(this).find('.NWHPrice input').val()) > parseFloat($(this).find('.NewRetailPrice input').val())) {
            chk1 = true;
            $(this).find('.NWHPrice input').addClass('border-warning');
        } else {
            $(this).find('.NWHPrice input').removeClass('border-warning');
        }
        if (parseFloat($(this).find('.NewRetailPrice input').val()) > parseFloat($(this).find('.NewBasePrice input').val())) {
            chk1 = true;
            $(this).find('.NewRetailPrice input').addClass('border-warning');
        } else {
            $(this).find('.NewRetailPrice input').removeClass('border-warning');
        }

        i = Number(i) + 1;
    });
    if (chk1) {
        swal("", "Please check highlighted point.", "error");
        return;
    }
    else {
        $.ajax({
            type: "POST",
            url: "PurchaseOrderDetails.aspx/UpdateManagePrice",
            data: "{'dataTable':'" + JSON.stringify(dataTable) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d != "Session Expired") {
                    if (response.d == "true") {
                        $('#managePrice').modal('hide');
                        swal("", "Packing details has been updated successfully.", "success").then(function () {
                            var newcostPrice = 0.00;
                            $("#Table1 tbody tr").each(function () {
                                if ($(this).find(".NewCosePrice input").attr('defaultunit') != '0') {
                                    newcostPrice = $(this).find(".NewCosePrice input").val();
                                }
                            })
                            $("#tblProductDetail tbody tr").each(function () {
                                if ($("#Index").val() == $(this).find(".ProId span").attr('index')) {
                                    $(this).find(".CCostPrice").html(parseFloat(newcostPrice).toFixed(2));
                                    if ($(this).find(".CCostPrice").html() == $(this).find(".NewCostPrice ").html()) {
                                        $(this).css('background-color', '#fff');
                                    } else {
                                        $(this).css('background-color', '#ee4c4c85');
                                    }
                                }
                            });
                        });

                    } else {
                        swal("", "Packing details not updated.", "success");
                    }
                } else {
                    location.href = '/';
                }

            },
            error: function (result) {
            },
            failure: function (result) {
                swal("Error!", 'Oops,some thing went wrong.Please try later.', "error");
            }
        });
    }

}
