﻿
var getid = "", WeightTax = 0.00, DefUnit = "", checkIP = 0;
var PackerSecurityEnable = false;
$(document).ready(function () {
     
    bindProduct();
  
    $("#txtOrderDate").val((new Date()).format("MM/dd/yyyy"));
    $("#txtOrderStatus").val("New");
    $("#txtBarcode").focus();
    $("#btnSaveAndUpdate").show();
    $("#btnReset").show();
    $("#btnCancel").hide();
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    getid = getQueryString('Id');
    if (getid != null) {
        //bindDropdown();
        $("#ddlVender").attr('disabled', 'disabled');
        $("#btnSaveAndUpdate").text('Update Order');
        $("#btnSaveAndUpdate").show();
        $("#btnCancel").show();
        $("#btnReset").hide();
        EditPoOrder(getid);
    }
    else {
        var DraftAutoId = getQueryString('DraftAutoId');
        $("#DraftAutoId").val(DraftAutoId)
        if (DraftAutoId != null) {
            DraftOrder(DraftAutoId);
            $("#btnCancel").show();
            $("#btnReset").hide();
        }
        else {
            //bindDropdown();
            $('#txtOrderDate').val((new Date()).format("MM/dd/yyyy"));
            $("#emptyTable").show();
        }
    }
    $("#ddlPOType").val('1').change();

});

function DraftOrder(DraftAutoId) {
    bindDropdown();
    $.ajax({
        type: "POST",
        url: "/Purchase/WebAPI/PurchaseOrderBulk.asmx/EditDraftOrder",
        data: "{'DraftAutoId':'" + DraftAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var OrderDetails = $(xmldoc).find("Table");
                var Products = $(xmldoc).find("Table1");
                var unitType = $(xmldoc).find("Table2");
                if ($(OrderDetails).length > 0) {
                    $("#txtBarcode").removeAttr('disabled');
                    $("#ddlProduct").removeAttr('disabled');
                    $("#ddlUnitType").removeAttr('disabled');
                    $("#txtReqQty").removeAttr('disabled');
                    $("#txtOrderId").val('');
                    $("#ddlVender").val($(OrderDetails).find("VenderAutoId").text()).change();
                    $("#txtOrderDate").val($(OrderDetails).find("PODate").text());
                    $("#txtDeliveryDate").val('')
                    $("#txtOrderStatus").val('Draft');
                    $("#txtOrderRemarks").val($(OrderDetails).find("PORemarks").text());
                    $("#txtPOAutoId").val(); $("#ddlVender").attr('disabled', true);
                    var row = $("#tblProductDetail thead tr").clone();
                    $.each(Products, function (index) {
                        var pid = $(this).find('ProductAutoId').text()
                        $(".ProId", row).html($(this).find('ProductId').text());
                        $(".CurrentStock", row).html($(this).find('DefUnit').text() + " - " + $(this).find('CurrentStock').text());
                        $(".ProName", row).html("<span ProductAutoId='" + $(this).find('ProductAutoId').text() + "'>" + $(this).find('ProductName').text() + "</span>");

                        var ddu = '<select class="form-control input-sm border-primary ddlreq" onchange="rowCal(this)">';
                        $.each(unitType, function () {
                            var pid1 = $(this).find('ProductAutoId').text()
                            console.log(pid + "-" + pid1);
                            if (pid1 == pid) {
                                ddu += "<option value='" + $(this).find("AutoId").text() + "' EligibleforFree='" + $(this).find("EligibleforFree").text() + "' QtyPerUnit='" + $(this).find("Qty").text() + "' >" + $(this).find("UnitType").text() + " (" + $(this).find("Qty").text() + " pcs" + ")</option>"
                            }
                        })
                        ddu += '</select>';
                        $(".UnitType", row).html(ddu);
                        row.find(".UnitType > select").val($(this).find('Unit').text()).change();
                        $(".ReqQty", row).html("<input type='text' onkeypress='return isNumberKey(event)' onfocus='this.select()' class='form-control input-sm border-primary text-center' runat='server' style='width:100px;' onkeyup='rowCal(this)' value='" + $(this).find('Qty').text() + "'/>");
                        $(".TtlPcs", row).html($(this).find('TotalPieces').text());
                        $(".Action", row).html("<a href='javascript:;' id='deleterow' onclick='deleteItemrecord(this)'><span class='ft-x'></span></a>");
                        $('#tblProductDetail tbody').append(row);
                        row = $("#tblProductDetail thead tr:last").clone(true);
                    });
                    countRow();
                }
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
var vendorHtml = ""; var VenList = "";
function bindDropdown() {
    $.ajax({
        type: "POST",
        url: "/Purchase/WebAPI/PurchaseOrderBulk.asmx/bindDropdown",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var vender = $(xmldoc).find("Table");
                $("#ddlVender option:not(:first)").remove();
                $.each(vender, function () {
                    $("#ddlVender").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("VendorName").text() + "</option>");
                });
                $("#ddlVender").select2();

                vendorHtml = '<select class="input-sm border-primary" style="border-radius:3px;height:28px">';
                vendorHtml += "<option value='0'>--Select--</option>";
                $.each(vender, function () {
                    vendorHtml += "<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("VendorName").text() + "</option>";
                });
                vendorHtml += '</select>';
            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}


function changeRefilltype() {
    $('#tblProductDetail tbody tr').remove();
    $("#Total_Item").text("");
    $("#TotalItem").text("");
    if ($("#ddlPOType").val() == 1 || $("#ddlPOType").val() == 0) {
        $("#RE_Fill").hide();
    } else {
        $("#RE_Fill").show();
    }
}

function Refill() {
    $("#ddlRefillType").removeClass('border-warning');
    $("#ddlPOType").removeClass('border-warning');
    $('#tblProductDetail tbody tr').remove();
    $("#Total_Item").text("");
    $("#TotalItem").text("");
    if ($("#ddlPOType").val() == 2 && $("#ddlRefillType").val() == 0) {
        $("#ddlRefillType").addClass('border-warning');
        toastr.error("Refill Type is required.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    } else if ($("#ddlPOType").val() == 0) {
        $("#ddlPOType").addClass('border-warning');
        toastr.error("PO Type is required.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
    else {
        $("#ddlRefillType").removeClass('border-warning');
        $("#ddlPOType").removeClass('border-warning');
        addProductInBulk();
    }

}

function bindProduct() {
   
        $.ajax({
            type: "POST",
            url: "/Purchase/PurchaseOrderBulk.aspx/BindProduct",
            data: "{}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            async: true,
            success: function (response) {
                if (response.d != "Session Expired") {
                    var getData = $.parseJSON(response.d), option = ""; 
                    $("#ddlProduct option:not(:first)").remove();
                    $.each(getData[0].ProductList, function (index, item) {
                        option += "<option WeightOz='" + item.W + "' CurrentStock='" + item.C + "' MLQty='" + item.M + "' value='" + item.AutoId + "'>" + item.P + "</option>";
                    });
                    $("#ddlProduct").append(option);
                    $("#ddlProduct").select2();

                    VenList = '<select class="form-control input-sm border-primary ddlreq">';
                    VenList += "<option value='0'>--Select--</option>";
                    $.each(getData[0].VendorList, function (index, item) { 
                        VenList += "<option value='" + item.AutoId + "'>" + item.VendorName + "</option>";
                    });
                    VenList += '</select>'; 
                } else {
                    location.href = '/';
                }
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
}
function BindUnittype() {
    $("#alertStockQty").hide();
    var productAutoId = $("#ddlProduct option:selected").val();
    $.ajax({
        type: "POST",
        url: "/Purchase/WebAPI/PurchaseOrderBulk.asmx/bindUnitType",
        data: "{'productAutoId':" + productAutoId + "}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var unitType = $(xmldoc).find("Table");
                var unitDefault = $(xmldoc).find("Table1");
                var count = 0;
                $("#txtReqQty").val(1);
                $("#ddlUnitType option:not(:first)").remove();
                $.each(unitType, function () {
                    $("#ddlUnitType").append("<option value='" + $(this).find("AutoId").text() + "' EligibleforFree='" + $(this).find("EligibleforFree").text() + "' QtyPerUnit='" + $(this).find("Qty").text() + "' >" + $(this).find("UnitType").text() + " (" + $(this).find("Qty").text() + " pcs" + ")</option>");
                });
                if (unitDefault.length > 0) {
                    if (BUnitAutoId == 0) {
                        $("#ddlUnitType").val($(unitDefault).find('AutoId').text()).change();
                    } else {
                        $("#ddlUnitType").val(BUnitAutoId).change();
                        BUnitAutoId = 0;
                    }
                    DefUnit = $(unitDefault).find('DefPacking').text();
                } else {
                    $("#ddlUnitType").val(0);
                }
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
};
var unitType,vendorList="";
function addProduct() {
    var flag1 = false;
    var validatecheck = dynamicInputTypeSelect2('selectvalidate');
    if (!validatecheck) {
        checkRequiredField();
        toastr.error('All * fields are mandatory', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
    else {
        validatecheck = checkRequiredField();
    }
    if (validatecheck) {
        var productAutoId = $("#ddlProduct option:selected").val();
        var unitAutoId = $("#ddlUnitType option:selected").val();
        if ($("#txtReqQty").val() == 0) {
            toastr.error('Required quantity can not be Zero.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            return;
        }
        $("#emptyTable").hide();
        $("#alertStockQty").hide();
        if (productAutoId != "" && productAutoId != 0) {
            var Draftdata = {
                //DraftAutoId: $("#DraftAutoId").val(),
                //VenderAutoId: $("#ddlVender").val(),
                ProductAutoId: $("#ddlProduct").val(),
                unitAutoId: $("#ddlUnitType").val(),
                ReqQty: $("#txtReqQty").val(),
                QtyPerUnit: $("#ddlUnitType option:selected").attr("QtyPerUnit"),
                PORemark: $("#txtOrderRemarks").val()
            }

            $.ajax({
                type: "POST",
                url: "/Purchase/WebAPI/PurchaseOrderBulk.asmx/DraftData",
                data: JSON.stringify({ data: JSON.stringify(Draftdata) }),
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                async: false,
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.d != "Session Expired") {
                        if (response.d != "false") {
                            var xmldoc = $.parseXML(response.d);
                            unitType = $(xmldoc).find("Table");
                            vendorList = $(xmldoc).find("Table1");
                            //DraftAutoId = $(xmldoc).find("Table1");
                            //if (DraftAutoId.length > 0) {
                            //    $("#DraftAutoId").val($(DraftAutoId).find('DraftAutoId').text());
                            //}
                        }
                    } else {
                        location.href = '/';
                    }

                },
                error: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                },
                failure: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                }
            });

            var product = $("#ddlProduct option:selected").text().split("--");
            var row = $("#tblProductDetail thead tr").clone(true);
            $("#tblProductDetail tbody tr").each(function () {
                if ($(this).find(".ProName span").attr("productautoid") == productAutoId && $(this).find(".UnitType > select").val() == unitAutoId) {
                    var reqQty = Number($(this).find(".ReqQty input").val()) + Number($("#txtReqQty").val());
                    $(this).find(".ReqQty input").val(reqQty);
                    $(this).find(".ReqQty input").attr('poqty', reqQty);
                    flag1 = true;
                    var totalPcs = parseInt($(this).find(".ReqQty > input").val()) * parseInt($(this).find(".UnitType > select option:selected").attr("QtyPerUnit"));
                    $(this).find(".TtlPcs").html(totalPcs);
                    $('#tblProductDetail tbody tr:first').before($(this));
                    countRow();
                    //$("#ddlProduct").select2('val', '0');
                    $("#ddlProduct").val('0');
                    $("#ddlUnitType").val(0);

                    $("#txtReqQty").val("1");
                    toastr.success('Product qty has been updated.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                }
                else if ($(this).find(".ProName span").attr("productautoid") == productAutoId) {
                    flag1 = true;
                    toastr.error('Product already added in list.', 'Worning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    //$("#ddlProduct").select2('val', '0');
                    $("#ddlProduct").val('0');
                    $("#ddlUnitType").val(0);
                    $("#txtReqQty").val("1");
                }
            });
            if (!flag1) {
                $(".ProId", row).html(product[0]);
                $(".CurrentStock", row).html(parseFloat($("#ddlProduct option:selected").attr("currentstock")).toFixed(2));
                $(".ProName", row).html("<span productautoid=" + productAutoId + ">" + product[1] + "</span>");

                var venID = ""; var QtyPerUnit = "";
                var ddu = '<select class="form-control input-sm border-primary ddlreq" onchange="rowCal2(this)">';
                $.each(unitType, function () {
                    venID = $(this).find("VendorAutoId").text();
                    if ($(this).find("AutoId").text() == unitAutoId) {
                        ddu += "<option selected value='" + $(this).find("AutoId").text() + "' EligibleforFree='" + $(this).find("EligibleforFree").text() + "' QtyPerUnit='" + $(this).find("Qty").text() + "' >" + $(this).find("UnitType").text() + " (" + $(this).find("Qty").text() + " pcs" + ")</option>";
                        QtyPerUnit = $(this).find("Qty").text();
                    } else {
                        ddu += "<option value='" + $(this).find("AutoId").text() + "' EligibleforFree='" + $(this).find("EligibleforFree").text() + "' QtyPerUnit='" + $(this).find("Qty").text() + "' >" + $(this).find("UnitType").text() + " (" + $(this).find("Qty").text() + " pcs" + ")</option>";
                    }
                })
                ddu += '</select>';
                $(".UnitType", row).html(ddu);

                var vdu = '<select class="form-control input-sm border-primary">';
              
                $.each(vendorList, function (i) {
                    if ($(this).find("AutoId").text() == venID) {
                        vdu += "<option selected value='" + $(this).find("AutoId").text() + "' >" + $(this).find("VendorName").text() + " </option>";
                    } else {
                        if (i == 0) {
                            vdu += "<option value='0' >- Select Vendor -</option>";
                        }
                        vdu += "<option value='" + $(this).find("AutoId").text() + "' >" + $(this).find("VendorName").text() + " </option>";
                    }
                })
                vdu += '</select>';
                $(".Vendor", row).html(vdu);


                //$(".UnitType", row).html('<select class="form-control input-sm border-primary ddlreq" onchange="rowCal(this)"></select>');
                $(".ReqQty", row).html("<input type='text'  qtyPerUnit='" + QtyPerUnit + "' UnitAutoId='" + unitAutoId + "' PoQty='1' onkeypress='return isNumberKey(event)' maxlength='4' class='form-control input-sm border-primary text-center' style='width:100px;'  onmouseout='rowCal3(this)' onblur='rowCal3(this)' value='" + $("#txtReqQty").val() + "'/>");
                $(".TtlPcs", row).text(parseInt($("#txtReqQty").val()) * parseInt($("#ddlUnitType option:selected").attr("QtyPerUnit")));
                $(".Action", row).html("<a href='javascript:;' id='deleterow' onclick='deleteItemrecord(this)'><span class='ft-x'></span></a>");
                if ($('#tblProductDetail tbody tr').length > 0) {
                    $('#tblProductDetail tbody tr:first').before(row);
                }
                else {
                    $('#tblProductDetail tbody').append(row);
                }
                //fillUnitTlb(row);
                //rowCal(row.find(".ReqQty > input"));
                countRow();
                toastr.success('Product added successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            }
            if ($('#tblProductDetail tbody tr').length > 0) {
                $("#ddlVender").attr('disabled', true);
            }
        }
        else {
            toastr.error('All * fields are mandatory', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        }
        $("#txtBarcode").val('');
        $("#txtBarcode").focus();
        //$("#ddlProduct").select2('val', '0');
        $("#ddlProduct").val('0');
        $("#ddlUnitType").val(0);
        $("#txtReqQty").val("1");
    }
}
function fillUnitTlb(e) {
    var row = $(e);
    var ProductId = row.find(".ProName > span").attr('productautoid');
    $.ajax({
        type: "POST",
        url: "/Purchase/WebAPI/PurchaseOrderBulk.asmx/bindUnitType",
        data: "{'productAutoId':" + ProductId + "}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: true,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var unitType = $(xmldoc).find("Table");
                var unitDefault = $(xmldoc).find("Table1");
                var count = 0;
                row.find(".UnitType >select option:not(:first)").remove();
                //$("#ddlUnitType option:not(:first)").remove();
                $.each(unitType, function () {
                    row.find(".UnitType > select").append("<option value='" + $(this).find("AutoId").text() + "' EligibleforFree='" + $(this).find("EligibleforFree").text() + "' QtyPerUnit='" + $(this).find("Qty").text() + "' >" + $(this).find("UnitType").text() + " (" + $(this).find("Qty").text() + " pcs" + ")</option>");
                });
                if (unitDefault.length > 0) {
                    if (BUnitAutoId == 0) {
                        row.find(".UnitType > select").val($("#ddlUnitType option:selected").val()).change();
                    } else {
                        row.find(".UnitType > select").val(BUnitAutoId).change();
                        BUnitAutoId = 0;
                    }
                }
                else {
                    row.find(".UnitType > select").val(0);
                }
                $(".TtlPcs", row).text(parseInt($("#txtReqQty").val()) * parseInt($("#ddlUnitType option:selected").attr('QtyPerUnit')));
                $("#txtBarcode").val('');
                $("#txtBarcode").focus();
                //$("#ddlProduct").select2('val', '0');
                $("#ddlProduct").val('0');
                $("#ddlUnitType").val(0);
                $("#txtReqQty").val("1");
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });


}
function countRow() {
    var count = 0;
    $("#tblProductDetail tbody tr").each(function () {
        count++;
    });
    $('#TotalItem').html("Total Items : " + count);
    $('#Total_Item').html("Total Items : " + count);
}
function rowCal2(e) {
     
    var row = $(e).closest("tr");
    var ReqQty = row.find(".ReqQty > input").val();
    var QtyPerUnit = row.find(".UnitType > select option:selected").attr("QtyPerUnit");
    var Unit = row.find(".UnitType > select option:selected").attr("value");
    var TotalPiece = row.find(".TtlPcs").text();
    if (parseFloat(TotalPiece) > parseFloat(QtyPerUnit)) {
        if (Unit == '3') {
                TotalPiece = parseFloat(row.find(".ReqQty > input").attr('qtyperunit')) * parseFloat(row.find(".ReqQty > input").attr('poqty'));
                row.find(".ReqQty > input").val(parseFloat(TotalPiece));
                row.find(".TtlPcs").text(parseFloat(TotalPiece));
        }
        else {
            if (parseFloat(QtyPerUnit) == parseFloat(row.find(".ReqQty > input").attr('qtyperunit'))) {
                TotalPiece = parseFloat(row.find(".ReqQty > input").attr('qtyperunit')) * parseFloat(row.find(".ReqQty > input").attr('poqty'));
                row.find(".ReqQty > input").val(parseFloat(row.find(".ReqQty > input").attr('poqty')));
                row.find(".TtlPcs").text(parseFloat(TotalPiece));
            }
            if (parseFloat(QtyPerUnit) > parseFloat(row.find(".ReqQty > input").attr('qtyperunit'))) {
                row.find(".ReqQty > input").val('1');
                row.find(".TtlPcs").text(parseFloat(QtyPerUnit));
            }
            //TotalPiece = parseFloat(TotalPiece) % parseFloat(QtyPerUnit);
            //if (TotalPiece != 0) {
            //    row.find(".ReqQty > input").val(parseFloat(TotalPiece));
            //}
            //else {
            //    ReqQty = parseFloat(row.find(".TtlPcs").text()) / parseFloat(QtyPerUnit);
            //    row.find(".ReqQty > input").val(parseFloat(ReqQty));
            //    row.find(".TtlPcs").text(parseFloat(row.find(".TtlPcs").text()));
            //}
        }
    }
    else {
        if (Unit == '3') {
            row.find(".ReqQty > input").val(parseFloat(QtyPerUnit));
            row.find(".TtlPcs").text(parseFloat(QtyPerUnit));
        }
        else {
            row.find(".ReqQty > input").val(parseFloat(1));
            row.find(".TtlPcs").text(parseFloat(QtyPerUnit));
        }
    }
}

function rowCal3(e) {
     
    var row = $(e).closest("tr");
    var ReqQty = parseFloat(row.find(".ReqQty > input").val()) || 0;
    if (ReqQty == 0 || ReqQty == '') {
        toastr.error("Quantity can't be empty or blank.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
    var QtyPerUnit = row.find(".UnitType > select option:selected").attr("QtyPerUnit");
    row.find(".TtlPcs").text(parseFloat(ReqQty) * parseFloat(QtyPerUnit));
    row.find(".ReqQty > input").attr('poqty', ReqQty);
    row.find(".ReqQty > input").attr('qtyperunit', QtyPerUnit);
}

function rowCal(e) {
    var row = $(e).closest("tr");
    if ($('#DraftAutoId').val() == "") {
        $('#DraftAutoId').val(getid);
    }
    if ($('#DraftAutoId').val() != "" && typeof row.find('.ReqQty > input').val() != "undefined") {
        var Draftdata = {
            DraftAutoId: $("#DraftAutoId").val(),
            ProductAutoId: $(row).find('.ProName span').attr('ProductAutoId'),
            unitAutoId: row.find(".UnitType > select option:selected").val(),
            ReqQty: row.find(".ReqQty > input").val(),
            QtyPerUnit: row.find(".UnitType > select option:selected").attr("QtyPerUnit")
        }
        $.ajax({
            type: "POST",
            url: "/Purchase/WebAPI/PurchaseOrderBulk.asmx/UpdateDraftReq",
            data: JSON.stringify({ dataValues: JSON.stringify(Draftdata) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d != "Session Expired") {
                    if (response.d != "false") {
                        var totalPcs = (parseInt(row.find(".ReqQty > input").val()) * parseInt(row.find(".UnitType > select option:selected").attr("QtyPerUnit")));
                        row.find(".TtlPcs").html(totalPcs);
                    }
                } else {
                    location.href = '/';
                }

            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });

    }
}
function deleteItemrecord(e) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this product.",
        icon: "warning",
        showCancelButton: true,
        allowOutsideClick: false,
        closeOnClickOutside: false,

        buttons: {
            cancel: {
                text: "No, Cancel.",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, Delete it.",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            var tr = $(e).closest('tr');
            var data = {
                DraftAutoId: $("#DraftAutoId").val(),
                ProductAutoId: $(tr).find('.ProName span').attr('ProductAutoId'),
                UnitAutoId: $(tr).find('.UnitType > select').val()
            }
            if ($("#DraftAutoId").val() != '') {
                $.ajax({
                    type: "POST",
                    url: "/Purchase/WebAPI/PurchaseOrderBulk.asmx/DeleteDraftItem",
                    data: JSON.stringify({ DataValue: JSON.stringify(data) }),
                    contentType: "application/json; charset=utf-8",
                    datatype: "json",
                    async: false,
                    beforeSend: function () {
                        $.blockUI({
                            message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                            overlayCSS: {
                                backgroundColor: '#FFF',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (response) {
                        if (response.d != "Session Expired") {
                            swal("", "Item deleted successfully.", "success");
                        } else {
                            location.href = '/';
                        }
                        $(e).closest('tr').remove();

                    },
                    error: function (result) {
                        console.log(JSON.parse(result.responseText).d);
                    },
                    failure: function (result) {
                        console.log(JSON.parse(result.responseText).d);
                    }
                });
            } else {
                $(e).closest('tr').remove();
                swal("", "Item deleted successfully.", "success");
            }

            if ($("#tblProductDetail tbody tr").length == 0) {
                $('#tblProductDetail tfoot tr').empty();
                $("#emptyTable").show();
                $("#ddlVender").attr('disabled', false);
            } else {
                countRow();
                $("#emptyTable").hide();
            }
            swal("", "Item deleted successfully.", "success");

        }
    })
}
var BUnitAutoId = 0;
function readBarcode() {
    $("#ddlProduct").val('0');
    //$("#ddlProduct").select2('val', '0');
    $("#ddlUnitType").val(0);
    $("#txtReqQty").val("1");
    var Barcode = $("#txtBarcode").val();
    if (Barcode != "") {
        var data = {
            Barcode: Barcode,
            //DraftAutoId: $("#DraftAutoId").val(),
            //VenderAutoId: $("#ddlVender").val(),
            PORemark: $("#txtOrderRemarks").val(),
            ReqQty: "1",
        }
        $.ajax({
            type: "POST",
            url: "/Purchase/WebAPI/PurchaseOrderBulk.asmx/GetBarDetails",
            data: JSON.stringify({ dataValues: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d == "ProductisInactive") {
                    $('#txtBarcode').removeAttr('onchange');
                    $('#txtBarcode').attr('onchange', 'invalidbarcodechnage()');
                    swal({
                        title: "",
                        text: "Inactive product can't be sold.",
                        icon: "error",
                        allowOutsideClick: false,
                        closeOnClickOutside: false,
                        allowEscapeKey: false
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $("#txtBarcode").val('');
                            $("#txtBarcode").focus();
                            $("#txtBarcode").attr('onchange', 'readBarcode()');
                        }
                    });
                    invalidbarcodechnage();
                }
                else if (response.d == "BarcodeDoesNotExist") {
                    swal({
                        title: "",
                        text: "Barcode does not exists.",
                        icon: "error",
                        closeOnClickOutside: false
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $("#txtBarcode").val('');
                            $("#txtBarcode").focus();
                        }
                    });
                    $("#yes_audio")[0].play();
                    $("#panelProduct select").val(0);
                    $("#txtQuantity, #txtTotalPieces").val("0");
                    $("#alertBarcodeCount").hide();
                    $("#txtBarcode").val('');
                    $("#txtBarcode").focus();
                    $("#txtBarcode").blur();
                }
                else if (response.d == "Session Expired") {
                    location.href = '/';
                }
                else {
                    debugger
                    var xmldoc = $.parseXML(response.d);
                    var product = $(xmldoc).find("Table");
                    var unitType = $(xmldoc).find("Table1");
                    var VendorList = $(xmldoc).find("Table2");
                    if (product.length > 0) {
                        //$("#DraftAutoId").val($(DID).find('DraftAutoId').text());
                        var productAutoId = $(product).find('ProductAutoId').text();
                        var unitAutoId = $(product).find('UnitType').text();
                        var VendorAutoId = $(product).find('VendorAutoId').text();
                        var flag1 = false;
                        $("#tblProductDetail tbody tr").each(function () {
                            if ($(this).find(".ProName > span").attr("productautoid") == productAutoId && $(this).find(".UnitType > select").val() == unitAutoId) {
                                var Qtyreq = 1;
                                var reqQty = Number($(this).find(".ReqQty input").val()) + Number(Qtyreq);
                                $(this).find(".ReqQty input").val(reqQty);
                                $(this).find(".ReqQty input").attr('poqty',reqQty);
                                var totalPcs = parseInt($(this).find(".ReqQty > input").val()) * parseInt($(this).find(".UnitType > select option:selected").attr("QtyPerUnit"));
                                $(this).find(".TtlPcs").html(totalPcs);
                                flag1 = true;
                                $('#tblProductDetail tbody tr:first').before($(this));
                                countRow();
                                toastr.success('Product qty has been updated.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                            }
                            else if ($(this).find(".ProName span").attr("productautoid") == productAutoId) {
                                flag1 = true;
                                toastr.error('Product already added in list.', 'Worning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                                $("#txtBarcode").val('');
                                $("#txtBarcode").focus();
                            }
                        });
                        var row = $("#tblProductDetail thead tr").clone(true);
                        if (!flag1) {
                            $(".ProId", row).html($(product).find('ProductId').text());
                            $(".CurrentStock", row).html(parseFloat($(product).find('CurrentStock').text()).toFixed(2));
                            $(".ProName", row).html("<span ProductAutoId='" + productAutoId + "'>" + $(product).find('ProductName').text() + "</span>");
                            var QtyPerUnit = 0;
                            var ddu = '<select class="form-control input-sm border-primary ddlreq" onchange="rowCal2(this)">';
                            $.each(unitType, function () {
                                if ($(this).find("AutoId").text() == unitAutoId) {
                                    ddu += "<option selected value='" + $(this).find("AutoId").text() + "' EligibleforFree='" + $(this).find("EligibleforFree").text() + "' QtyPerUnit='" + $(this).find("Qty").text() + "' >" + $(this).find("UnitType").text() + " (" + $(this).find("Qty").text() + " pcs" + ")</option>";
                                    QtyPerUnit = $(this).find("Qty").text();
                                } else {
                                    ddu += "<option value='" + $(this).find("AutoId").text() + "' EligibleforFree='" + $(this).find("EligibleforFree").text() + "' QtyPerUnit='" + $(this).find("Qty").text() + "' >" + $(this).find("UnitType").text() + " (" + $(this).find("Qty").text() + " pcs" + ")</option>";
                                }
                            })
                            ddu += '</select>';
                            $(".UnitType", row).html(ddu);
                            ddu += '</select>';

                            var vdu = '<select class="form-control input-sm border-primary">';
                            $.each(VendorList, function (i) {
                                if ($(this).find("AutoId").text() == VendorAutoId) {
                                    vdu += "<option selected value='" + $(this).find("AutoId").text() + "' >" + $(this).find("VendorName").text() + " </option>";
                                } else {
                                    if (i == 0) {
                                        vdu += "<option value='0' >- Select Vendor -</option>";
                                    }
                                    vdu += "<option value='" + $(this).find("AutoId").text() + "' >" + $(this).find("VendorName").text() + "</option>";
                                }
                            })
                            vdu += '</select>';
                            $(".Vendor", row).html(vdu);
                          
                            $(".ReqQty", row).html("<input   qtyPerUnit='" + QtyPerUnit + "' UnitAutoId='" + unitAutoId + "' PoQty='1' type='text' onkeypress='return isNumberKey(event)'  maxlength='4' class='form-control input-sm border-primary text-center' runat='server' style='width:100px;'  onmouseout='rowCal3(this)' onkeyup='rowCal3(this)' value='1'/>");
                            $(".TtlPcs", row).text(parseInt($("#txtReqQty").val()) * $(product).find('UnitQty').text());
                            $(".Action", row).html("<a href='javascript:;' id='deleterow' onclick='deleteItemrecord(this)'><span class='ft-x'></span></a>");
                            if ($('#tblProductDetail tbody tr').length > 0) {
                                $('#tblProductDetail tbody tr:first').before(row);
                            }
                            else {
                                $('#tblProductDetail tbody').append(row);
                            }
                            countRow();
                            $("#txtQuantity, #txtTotalPieces").val("0");
                            if ($('#tblProductDetail tbody tr').length > 0) {
                                $('#emptyTable').hide();
                            }
                            toastr.success('Product added successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                        }
                        $("#txtBarcode").val('');
                        $("#txtBarcode").focus();
                        $("#ddlVender").attr('disabled', true);
                    }
                }
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    } else {
        $("#ddlProduct").val(0).change();
        $("#alertBarcodeCount").hide();
    }
}
var PackedBoxes = 0;
$("#btnSaveAndUpdate").click(function () {
    if ($("#ddlVender").val() == 0) {
        toastr.error('Please Select Vendor First.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
    else if ($('#tblProductDetail tbody tr').length > 0) {
        var check = 0;
        $("#tblProductDetail tbody tr").each(function () {
            if (parseInt(($(this).find(".ReqQty input").val())) == 0 || $(this).find(".ReqQty input").val() == "") {
                $(this).find(".ReqQty input").addClass('border-warning');
                check = 1;
            } else {
                $(this).find(".ReqQty input").removeClass('border-warning');
            }
        });
        if (check == '0') {
            Xml = "";
            $("#tblProductDetail tbody tr").each(function () {
                var row = $(this);
                Xml += '<Xml>';
                Xml += '<ProductAutoId><![CDATA[' + row.find(".ProName span").attr("ProductAutoId") + ']]></ProductAutoId>';
                Xml += '<Unit><![CDATA[' + row.find(".UnitType > select").val() + ']]></Unit>';
                Xml += '<QtyPerUnit><![CDATA[' + row.find(".UnitType > select option:selected").attr("QtyPerUnit") + ']]></QtyPerUnit>';
                Xml += '<Qty><![CDATA[' + row.find(".ReqQty").find("input").val() + ']]></Qty>';
                Xml += '<TotalPieces><![CDATA[' + row.find(".TtlPcs").html() + ']]></TotalPieces>';
                Xml += '</Xml>';
            });
            var data = {
                VenderAutoId: $("#ddlVender").val(),
                PORemark: $("#txtOrderRemarks").val(),
                NoofItems: $('#tblProductDetail tbody tr').length,
                POAutoId: $("#txtPOAutoId").val(),
                DraftAutoId: $("#DraftAutoId").val()
            }

            $.ajax({
                type: "Post",
                url: "/Purchase/WebAPI/PurchaseOrderBulk.asmx/InsUpdData",
                data: JSON.stringify({ data: JSON.stringify(data), xml: JSON.stringify(Xml) }),
                //data: "{'data':'" + JSON.stringify(data) + "','xml':'" + JSON.stringify(Xml) + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (data) {
                    if (data.d != "Session Expired") {

                        if (data.d == "Updated") {

                            swal("", "Purchase order updated successfully.", "success").then(function () {
                                window.location.href = "/Purchase/PurchaseOrderDetails.aspx?id=" + getid
                            });


                        }
                        else if (data.d == "Success") {

                            swal("", "Purchase order generated successfully.", "success").then(function () {

                                window.location.href = "/Purchase/PurchaseOrderBulk.aspx";
                            })
                        }

                        else if (data.d == "NotExist") {

                            swal("", "Customer does not register on vender's location.", "warning");

                        }

                        else if (data.d == "ChangeStatus") {

                            swal("", "Purchase Order has been procced, so can not update this purchase order.", "warning");

                        }

                    } else {
                        location.href = '/';
                    }
                },
                error: function (result) {
                    $("#alertSOrder").show();
                    $("#alertSOrder span").text(result.d);
                },
                failure: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                }
            });
        }
        else {
            toastr.error("Quantity can't be zero or blank.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        }
    }
    else {
        toastr.error(' No Product Added in the List.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
});
$("#btnCancel").on('click', function () {
    window.location = "/Purchase/PurchaseOrderList.aspx"
})
function resetAllField() {
    $("#ddlProduct").val('0');
    //$("#ddlProduct").select2('val', '0');
    //$("#ddlVender").select2('val', '0');
    //$("#ddlVender").removeAttr('disabled');
    $("#ddlUnitType").val(0);
    $("#txtReqQty").val("1");
    $("#Total_Item").text("");
    $("#TotalItem").text("");
    $('#tblProductDetail tbody tr').remove();
    $("#TotalItem").html('');
    $('#txtOrderRemarks').val('');
    //$("#txtBarcode").attr('disabled', 'disabled');
    //$("#ddlProduct").attr('disabled', 'disabled');
    //$("#ddlUnitType").attr('disabled', 'disabled');
    //$("#txtReqQty").attr('disabled', 'disabled');
    $("#ddlRefillType").val('0');
    $("#ddlPOType").val('1').change();
}
function EditPoOrder(Id) {
    $.ajax({
        type: "POST",
        url: "/Purchase/WebAPI/PurchaseOrderBulk.asmx/editOrder",
        data: "{'AutoId':'" + Id + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var OrderDetails = $(xmldoc).find("Table");
                var Products = $(xmldoc).find("Table1");
                var unitType = $(xmldoc).find("Table2");
                if ($(OrderDetails).length > 0) {
                    $("#txtOrderId").val($(OrderDetails).find("PONo").text());
                    $("#ddlVender").val($(OrderDetails).find("VenderAutoId").text()).change();
                    $("#txtOrderDate").val($(OrderDetails).find("PODate").text());
                    $("#txtDeliveryDate").val($(OrderDetails).find("DeliveryDate").text())
                    $("#txtOrderStatus").val($(OrderDetails).find("Status").text());
                    $("#txtOrderRemarks").val($(OrderDetails).find("PORemarks").text());
                    $("#txtPOAutoId").val($(OrderDetails).find("AutoId").text()); $("#ddlVender").attr('disabled', true);
                    var row = $("#tblProductDetail thead tr").clone();
                    $.each(Products, function (index) {
                        var pid = $(this).find('ProductAutoId').text()
                        $(".ProId", row).html($(this).find('ProductId').text());
                        $(".CurrentStock", row).html($(this).find('DefUnit').text() + " - " + $(this).find('CurrentStock').text());
                        $(".ProName", row).html("<span ProductAutoId='" + $(this).find('ProductAutoId').text() + "'>" + $(this).find('ProductName').text() + "</span>");

                        var ddu = '<select class="form-control input-sm border-primary ddlreq" onchange="rowCal(this)">';
                        $.each(unitType, function () {
                            var pid1 = $(this).find('ProductAutoId').text()
                            console.log(pid + "-" + pid1);
                            if (pid1 == pid) {
                                ddu += "<option value='" + $(this).find("AutoId").text() + "' EligibleforFree='" + $(this).find("EligibleforFree").text() + "' QtyPerUnit='" + $(this).find("Qty").text() + "' >" + $(this).find("UnitType").text() + " (" + $(this).find("Qty").text() + " pcs" + ")</option>"
                            }
                        })
                        ddu += '</select>';
                        $(".UnitType", row).html(ddu);
                        row.find(".UnitType > select").val($(this).find('Unit').text()).change();
                        $(".ReqQty", row).html("<input type='text' onkeypress='return isNumberKey(event)' onfocus='this.select()' maxlength='4' class='form-control input-sm border-primary text-center' runat='server' style='width:100px;' onkeyup='rowCal(this)' value='" + $(this).find('Qty').text() + "'/>");
                        $(".TtlPcs", row).html($(this).find('TotalPieces').text());
                        $(".PackdQty", row).html($(this).find('PackedQty').text());
                        if ($(this).find('L_UnitType').text() != "") {
                            $(".PackdUnit", row).html($(this).find('L_UnitType').text() + " (" + $(this).find('L_QtyPerUnit').text() + ")");
                        }
                        $(".PackdPieces", row).html($(this).find('TotalPackedPieces').text());
                        $(".Action", row).html("<a href='javascript:;' id='deleterow' onclick='deleteItemrecord(this)'><span class='ft-x'></span></a>");
                        $('#tblProductDetail tbody').append(row);
                        row = $("#tblProductDetail thead tr:last").clone(true);
                    });
                    countRow();
                }
            }
        }
    });
}
function updateDraftPO() {
    if ($("#DraftAutoId").val() != '') {
        var Draftdata = {
            DraftAutoId: $("#DraftAutoId").val(),
            PORemark: $("#txtOrderRemarks").val()
        }
        $.ajax({
            type: "POST",
            url: "PurchaseOrderBulk.aspx/updateDraftPO",
            data: JSON.stringify({ dataValues: JSON.stringify(Draftdata) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            //async: false,
            //beforeSend: function () {
            //    $.blockUI({
            //        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
            //        overlayCSS: {
            //            backgroundColor: '#FFF',
            //            opacity: 0.8,
            //            cursor: 'wait'
            //        },
            //        css: {
            //            border: 0,
            //            padding: 0,
            //            backgroundColor: 'transparent'
            //        }
            //    });
            //},
            //complete: function () {
            //    $.unblockUI();
            //},
            success: function (response) {
                if (response.d != "Session Expired") {

                } else {
                    location.href = '/';
                }
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
}
function invalidbarcodechnage() {
    $("#txtBarcode").val('');
    $("#txtBarcode").focus();
}

function addProductInBulk() {
    if ($("#ddlVender").val() == '0') {
        $(this).closest('#ddlVender').find('.select2-selection--single').attr('style', 'border:1px solid #FF9149  !important');
        toastr.error('Vendor is required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
    else {
        $(this).closest('#ddlVender').find('.select2-selection--single').removeAttr('style');
        var Draftdata = {
            DraftAutoId: $("#DraftAutoId").val(),
            POType: $("#ddlPOType").val(),
            RefillType: $("#ddlRefillType").val(),
            VenderAutoId: $("#ddlVender").val(),
            Remark: $("#txtOrderRemarks").val()
        }
        $.ajax({
            type: "POST",
            url: "PurchaseOrderBulk.aspx/RefillProduct",
            data: JSON.stringify({ dataValues: JSON.stringify(Draftdata) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) { 
                if (response.d != "Session Expired") {
                    if (response.d != "false") {
                        debugger
                        var responseList = $.parseJSON(response.d);
                        for (var i = 0; i < responseList.length; i++) {
                            var response = responseList[i];
                            if (response.ProductList != null) {
                                var ProductList = response.ProductList;
                                var UnitList = response.UnitList;
                             /*   var VendorList = response.VendorList;*/
                                //var DraftAutoId = response.DraftAutoId[0].DraftAutoId;

                                if (ProductList.length > 0) {
                                    $("#ddlVender").attr('disabled', true);
                                    $("#emptyTable").hide();
                                    //$("#DraftAutoId").val(DraftAutoId);
                                    var row = $("#tblProductDetail thead tr").clone(true);
                                    for (var k = 0; k < ProductList.length; k++) {
                                        var pl = ProductList[k];
                                        var check = 1;
                                        $("#tblProductDetail tbody tr").each(function (j, item) {
                                            if (pl.ProductAutoId == $(item).find(".ProName span").attr('productautoid') && pl.PackingAutoId == $(item).find(".UnitType option:selected").attr('value')) {
                                                check = 0;
                                            }
                                        })
                                        if (check == 1 && Number(pl.Qty) > 0) {
                                           
                                            $(".Action", row).html("<a href='javascript:;' id='deleterow' onclick='deleteItemrecord(this)'><span class='ft-x'></span></a>");
                                            $(".ProId", row).html(pl.ProductId);
                                            $(".ProName", row).html("<span productautoid=" + pl.ProductAutoId + ">" + pl.ProductName + "</span>");
                                            //$(".CurrentStock", row).html(pl.DefaultUnit + " - " + pl.CurrentStock);
                                            $(".CurrentStock", row).html(parseFloat(pl.CurrentStock).toFixed(2));
                                            var unitAutoId = pl.PackingAutoId; var qtyPerUnit = 0;
                                            var ddu = '<select class="form-control input-sm border-primary ddlreq" onchange="rowCal2(this)">';
                                            for (var l = 0; l < UnitList.length; l++) {
                                                var ul = UnitList[l];
                                                if (pl.ProductAutoId == ul.ProductAutoId) {
                                                    if (ul.AutoId == unitAutoId) {
                                                        ddu += "<option selected value='" + ul.AutoId + "' EligibleforFree='" + ul.EligibleforFree + "' QtyPerUnit='" + ul.qty + "' >" + ul.UnitType + " (" + ul.qty + " pcs" + ")</option>";
                                                        qtyPerUnit = ul.qty;
                                                    } else {
                                                        ddu += "<option value='" + ul.AutoId + "' EligibleforFree='" + ul.EligibleforFree + "' QtyPerUnit='" + ul.qty + "' >" + ul.UnitType + " (" + ul.qty + " pcs" + ")</option>";
                                                    }
                                                }
                                            }
                                            ddu += '</select>';

                                            $(".Vendor", row).html(VenList);
                                            $(".Vendor", row).find("select option[value=" + pl.AutoId + "]").attr('selected', true);
                                            $(".UnitType", row).html(ddu);
                                            $(".ReqQty", row).html("<input type='text' qtyPerUnit='" + qtyPerUnit + "' UnitAutoId='" + unitAutoId + "' PoQty='" + pl.Qty + "' onkeypress='return isNumberKey(event)' maxlength='4' class='form-control input-sm border-primary text-center' style='width:100px;' onmouseout='rowCal3(this)' onblur='rowCal3(this)' value='" + pl.Qty + "'/>");
                                            $(".TtlPcs", row).text(parseInt(pl.QtyPerUnit) * parseInt(pl.Qty));
                                            $("#tblProductDetail tbody").append(row);
                                            row = $("#tblProductDetail tbody tr:last").clone(true);

                                        }
                                    }
                                    //$("#txtBarcode").removeAttr('disabled');
                                    //$("#ddlProduct").removeAttr('disabled');
                                    //$("#ddlUnitType").removeAttr('disabled');
                                    //$("#txtReqQty").removeAttr('disabled');
                                    countRow();
                                
                                    
                                }
                            }
                        }
                    }
                    else if (response.d == "false") {
                        toastr.error('Oops, Something went wrong.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
                    } else {
                        location.href = '/';
                    }
                }

            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
}
function generatePO() {
    debugger
    if ($("#txtOrderRemarks").val() == '') {
        toastr.error('Remark is mandatory', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $("#txtOrderRemarks").addClass('border-warning');
    }
    else if ($('#tblProductDetail tbody tr').length > 0) {
        $("#txtOrderRemarks").removeClass('border-warning');
        var check = 0,check1=0;
        $("#tblProductDetail tbody tr").each(function () {
            if (parseInt(($(this).find(".ReqQty input").val())) == 0 || $(this).find(".ReqQty input").val() == "") {
                $(this).find(".ReqQty input").addClass('border-warning');
                check = 1;
            } else {
                $(this).find(".ReqQty input").removeClass('border-warning');
            }
            if ($(this).find(".Vendor option:selected").val() == '0') {
                $(this).find(".Vendor select").addClass('border-warning');
                check = 1;
                check1 = 1;
            }
            else {
                $(this).find(".Vendor select").removeClass('border-warning');
            }
        });
        if (check == '0') {
            Xml = "";
            $("#tblProductDetail tbody tr").each(function () {
                var row = $(this);
                Xml += '<Xml>';
                Xml += '<VendorAutoId><![CDATA[' + row.find(".Vendor > select option:selected").val() + ']]></VendorAutoId>';
                Xml += '<ProductAutoId><![CDATA[' + row.find(".ProName span").attr("ProductAutoId") + ']]></ProductAutoId>';
                Xml += '<Unit><![CDATA[' + row.find(".UnitType > select").val() + ']]></Unit>';
                Xml += '<QtyPerUnit><![CDATA[' + row.find(".UnitType > select option:selected").attr("QtyPerUnit") + ']]></QtyPerUnit>';
                Xml += '<Qty><![CDATA[' + row.find(".ReqQty").find("input").val() + ']]></Qty>';
                Xml += '<TotalPieces><![CDATA[' + row.find(".TtlPcs").html() + ']]></TotalPieces>';
                Xml += '</Xml>';
            });
            var data = {
                //VenderAutoId: $("#ddlVender").val(),
                PORemark: $("#txtOrderRemarks").val().trim(),
                NoofItems: $('#tblProductDetail tbody tr').length,
                //POAutoId: $("#txtPOAutoId").val(),
                //DraftAutoId: $("#DraftAutoId").val()
            }

            $.ajax({
                type: "Post",
                url: "/Purchase/WebAPI/PurchaseOrderBulk.asmx/InsUpdData",
                data: JSON.stringify({ data: JSON.stringify(data), xml: JSON.stringify(Xml) }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (data) {
                    if (data.d != "Session Expired") {
                        var xmldoc = $.parseXML(data.d); var pid;
                        InactiveProductList = $(xmldoc).find("Table");
                        if (InactiveProductList.length > 0) {
                            $.each(InactiveProductList, function (index) {
                                pid = $(this).find('ProductId').text();
                                $("#tblProductDetail tbody tr").each(function (j, item) {
                                    if ($(item).find(".ProId").text() == pid) {
                                        $(item).css('background-color', '#ffe6e6');
                                    }
                                });
                            });
                            if (pid != '') {
                                swal("", "Highlighted products are inactive on vendor location.", "warning")
                            }
                            else {
                                swal("", "Purchase order generated successfully.", "success").then(function () {

                                    window.location.href = "/Purchase/PurchaseOrderBulk.aspx";
                                })
                            }
                        }
                        else {
                            swal("", "Purchase order generated successfully.", "success").then(function () {

                                window.location.href = "/Purchase/PurchaseOrderBulk.aspx";
                            })
                        }
                    } else {
                        location.href = '/';
                    }
                },
                error: function (result) {
                    $("#alertSOrder").show();
                    $("#alertSOrder span").text(result.d);
                },
                failure: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                }
            });
        }
        else {
            if (check1 == '1') {
                toastr.error("Vendor is required.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            }
            else {
                toastr.error("Quantity can't be zero or blank.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            }
        }
    }
    else {
        toastr.error(' No Product Added in the List.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
