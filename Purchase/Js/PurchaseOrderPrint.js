﻿
var Print = 1, PrintLabel = '';
$(document).ready(function () {
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    var getid = getQueryString('Id');
    var type  = getQueryString('type');
    if (getid != null) {
        getOrderData(getid,type);
    }
});
/*-------------------------------------------------------------------------------------------------------------------------------*/
function getOrderData(POAutoId,type) {
    $.ajax({
        type: "POST",
        async: false,
        url: "PurchaseOrder.aspx/PrintPO",
        data: "{'POAutoId':'" + POAutoId + "','type':'" + type + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('#fade').show();
        },
        complete: function () {
            $('#fade').hide();
        },
        success: function (response) {
            if (response.d == 'Session Expired') {
                location.href = '/';
                return;
            }
            var xmldoc = $.parseXML(response.d);
            var order = $(xmldoc).find("Table");
            var items = $(xmldoc).find("Table1");

            //$("#logo").attr("src", "../Img/logo/" + $(Company).find("Logo").text())
            $("#VendorId").text($(order).find("VendorName").text());
            $("#VendorContact").text($(order).find("ContactPerson").text());
            $("#PageHeading").text($(order).find("PONo").text());
            $("#PODate").text($(order).find("PODate").text());
            $("#VendorAddress").html($(order).find("CompleteAddress").text());
            $("#Contact").text($(order).find("Cell").text());

            var html = '<table class="table tableCSS tblProduct" id="tblProduct"><thead><tr>';
            html += '<td class="ProId" style="width: 10px; white-space: nowrap;text-align:center">Product ID</td>';
            html += '<td class="ProName" style="white-space: nowrap">Product Name</td>';
            html += '<td class="UnitType" style="width: 10px; white-space: nowrap;text-align:center">Unit Type</td>';
            html += '<td class="Qty" style="width: 10px;text-align:center">U/M</td>';
            html += '<td class="Qty" style="width: 10px;text-align:center">Qty</td>';
            html += '</tr></thead><tbody>';


            var row = $("#tblProduct thead tr").clone(true);
            var qty = 0, overallPrice = 0;;
            $.each(items, function (index) {
                qty = Number(qty) + 1;
                orderQty = $(this).find("Qty").text();
                html += "<tr><td style='text-align:center'>" + $(this).find("ProductId").text() + "</td>";
                productname = $(this).find("ProductName").text();
                html += "<td>" + productname.replace(/&quot;/g, "\'") + "</td>";
                html += "<td style='text-align:center'>" + $(this).find("DefUnit").text() + "</td>";
                html += "<td style='text-align:center'>" + $(this).find("TotalPieces").text() + "</td>";
                html += "<td style='text-align:center'>" + orderQty + "</td></tr>";
            });
            html += '</tbody></table>';
            $("#totalQty").html(qty);
            $("#totalwoTax").html(parseFloat(overallPrice).toFixed(2));
            $("#grandTotal").html(parseFloat(overallPrice).toFixed(2));
            $('#tableDynamic').html(html);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
