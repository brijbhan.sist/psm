﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="PurchaseOrderBulk.aspx.cs" Inherits="PurchaseOrder" ClientIDMode="static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        #tblProductDetail tbody .ProName {
            text-align: left;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input id="DraftAutoId" type="hidden" />
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Generate PO</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item">Manage PO
                        </li>
                        <li class="breadcrumb-item">By Internal
                        </li>
                        <li class="breadcrumb-item">Generate PO - Refill
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a class="dropdown-item" href="~/Purchase/PurchaseOrderList.aspx" runat="server">Purchase Order List</a>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <input type="hidden" id="hiddenEmpTypeVal" runat="server" />
                        <input type="hidden" id="hiddenStatusCode" runat="server" />
                        <div class="card-content collapse show">
                            <div class="card-body" id="panelBill">
                                <div class="row">
                                    <div class="col-md-3 col-sm-3">
                                        <label class="control-label">PO No</label>
                                        <input type="hidden" id="txtPOAutoId" class="form-control input-sm" />
                                        <div class="form-group">
                                            <input type="text" id="txtOrderId" class="form-control input-sm border-primary" readonly="readonly" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label class="control-label">PO Date</label>
                                             <div class="form-group input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 0.5rem;">
                                                    <span class="la la-calendar-o pl-0"></span>
                                                </span>
                                            </div>
                                            <input type="text" id="txtOrderDate" class="form-control input-sm border-primary" readonly="readonly" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label class="control-label">Delivery Date</label>
                                        <div class="form-group input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 0.5rem;">
                                                    <span class="la la-calendar-o pl-0"></span>
                                                </span>
                                            </div>
                                            <input type="text" id="txtDeliveryDate" class="form-control input-sm border-primary" readonly="readonly" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label class="control-label">Status</label>
                                        <input type="hidden" id="hiddenOrderStatus" />
                                        <div class="form-group">
                                            <input type="text" id="txtOrderStatus" class="form-control input-sm border-primary" readonly="readonly" />
                                        </div>
                                    </div>

                                   
            </div>
                                <div class="row">
                                     <div class="col-md-3  col-sm-3">
                                        <label class="control-label">
                                            PO Type 
                                        </label>
                                        <div class="form-group">
                                            <select class="form-control input-sm border-primary" onchange="changeRefilltype()" id="ddlPOType" runat="server" style="width: 100% !important">
                                                <option value="0">-Select PO Type-</option>
                                                <option value="1">By Re Order Mark</option>
                                                <option value="2">By sales</option>
                                            </select>
                                        </div>
                                    </div>
                                     <div class="col-md-3  col-sm-3" id="RE_Fill">
                                        <label class="control-label">
                                            Refill Type 
                                        </label>
                                        <div class="form-group">
                                            <select class="form-control input-sm border-primary" onchange="" id="ddlRefillType" runat="server" style="width: 100% !important">
                                                <option value="0">-Select Refill Type-</option>
                                                <option value="1">Last 2 Week</option>
                                                <option value="2">Last 1 Month</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3  col-sm-3">
                                     <button type="button" onclick="Refill()" class="btn btn-primary buttonAnimation  round box-shadow-1 btn-sm pulse" id="btnRefill" style="margin-top: 22px;">&nbsp;&nbsp;&nbsp;Refill&nbsp;&nbsp;&nbsp;</button>
                                </div>
                                </div>
                                
            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="panelProduct">
            <div class="row" id="panelOrderContent">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body" id="panelBill5">
                                <div class="row">
                                    <label class="col-md-1 col-sm-1 control-label">Barcode</label>
                                    <div class="col-md-3  col-sm-3  form-group">
                                        <input type="text" class="form-control input-sm border-primary" id="txtBarcode" runat="server" onfocus="this.select()" onkeypress="return isNumberKey(event)" placeholder="Enter Barcode here" onchange="readBarcode()" />
                                    </div>

                                    <div class="col-md-3  col-sm-3 ">
                                        <div class="alert alert-default alertSmall pull-right " id="alertBarcodeCount" style="display: none; color: #c62828;">
                                            Barcode Count : &nbsp;<span>sd</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="row">
                                    <div class="col-md-5  col-sm-3 ">
                                        <label class="control-label">
                                            Product <span class="required">*</span>

                                        </label>
                                        <div class="form-group">
                                            <select class="form-control input-sm border-primary selectvalidate" id="ddlProduct" runat="server" style="width: 100% !important" onchange="BindUnittype()">
                                                <option value="0">-Select-</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2  col-sm-2 ">
                                        <label class="control-label">
                                            Unit Type <span class="required">*</span>

                                        </label>
                                        <div class="form-group">
                                            <select class="form-control input-sm border-primary ddlreq" id="ddlUnitType" runat="server">
                                                <option value="0">-Select-</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-1  col-sm-2">
                                        <label class="control-label" style="word-wrap: normal">
                                            Qty <span class="required">*</span>

                                        </label>
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm border-primary ddlreq text-center" id="txtReqQty" runat="server" value="1" maxlength="4" onkeypress="return isNumberKey(event)" />
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="pull-left">
                                            <button type="button" onclick="addProduct()" class="btn btn-primary buttonAnimation pull-right round box-shadow-1 btn-sm pulse" id="btnAdd" style="margin-top: 22px;">&nbsp;&nbsp;&nbsp;Add&nbsp;&nbsp;&nbsp;</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <%--<div class="row">
                                    <div class="col-md-6 col-sm-6 form-group">
                                        <div class="alert alert-danger alertSmall" id="alertStockQty" style="display: none; text-align: center; color: white !important">
                                        </div>
                                    </div>

                                </div>--%>
                                <div class="clearfix"></div>
                                <div id="divBarcode" style="display: none;">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-2 col-ms-2">
                                                    <label class="control-label">Quantity</label>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm border-primary" id="txtQty" value="1" maxlength="4" />
                                                    </div>
                                                </div>
                                                <div class="col-md-1  col-sm-1 ">
                                                    <label class="control-label">
                                                        Is Exchange 
                                                    </label>
                                                    <div class="form-group">
                                                        <input type="checkbox" id="IsExchange" onchange="changetype(this,'IsFreeItem')" />
                                                    </div>
                                                </div>
                                                <div class="col-md-1  col-sm-1 ">
                                                    <label class="control-label">
                                                        Is Free Item 
                                                    </label>
                                                    <div class="form-group">
                                                        <input type="checkbox" id="IsFreeItem" onchange="changetype(this,'IsExchange')" />
                                                    </div>
                                                </div>
                                                <script>
                                                    function changetype(e, idhtml) {
                                                        $('#' + idhtml).prop('checked', false);
                                                    }
                                                </script>
                                                <div class="col-md-3  col-ms-3">
                                                    <label class="control-label">Enter Barcode</label>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm border-primary" id="txtScanBarcode" onchange="checkBarcode()" />
                                                    </div>
                                                </div>
                                                <div class="col-md-3  col-ms-3">
                                                    <label class="control-label">
                                                        Product<span class="required">*</span>
                                                    </label>
                                                    <div class="form-group">
                                                        <select class="form-control input-sm border-primary" id="ddlitemproduct" runat="server" onchange="itemProduct()" style="width: 100% !important">
                                                            <option value="0" style="display: none">-Select-</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2  col-ms-2">
                                                    <div class="pull-left">
                                                        <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1 btn-sm pulse" <%--id="btnAdd"--%> <%--onclick="AddProductQty()"--%> style="margin-top: 22px;">&nbsp;&nbsp;&nbsp;Add&nbsp;&nbsp;&nbsp;</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        <section>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-content collapse show">
                            <div class="card-body" id="panelBill6">
                                <div class="row" style="float: right;">
                                    <div class="col-sm-12">
                                     <table>
                                <tr>
                                    <td style="padding-right: 10px;"></td>
                                    <td style="padding-right: 10px;"> <span id="Total_Item" style="margin-left: 30px;"></span></td>
                                   
                                </tr>
                            </table>
                                    </div>
                                   <%-- <div class="col-sm-2">
                                        <span id="Total_Item" style="margin-left: 30px;"></span>
                                    </div>--%>
                                </div>
                                <div class="table-responsive">
                                    <table id="tblProductDetail" class="table table-striped table-bordered">
                                     
                                        <thead class="bg-blue white">
                                            <tr>
                                                <td class="Action text-center wth5">Action</td>
                                                <td class="Vendor text-center">Vendor</td>
                                                <td class="ProId text-center">ID</td>
                                                <td class="ProName">Product Name</td>
                                                <td class="UnitType text-center width-15-per">PO Unit</td>
                                                <td class="CurrentStock text-center wth2">Current Stock</td>
                                                <td class="ReqQty text-center wth2">PO Qty</td>
                                                <td class="TtlPcs text-center">PO Pieces</td>
                                                <td class="PackdUnit text-center" style="display: none;">Received Unit</td>
                                                <td class="PackdQty text-center" style="display: none;">Received Qty</td>
                                                <td class="PackdPieces text-center" style="display: none;">Received Pieces</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="8"><span id="TotalItem" style="float: right;"></span></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    <h5 class="well text-center" id="emptyTable" style="display: none">No Product Selected.</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>

        <div class="row">
            <div class="col-md-7 col-sm-7">

                <section id="Div1">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">PO Remark for All Vendor <span class="required">*</span></h4>
                                </div>

                                <div class="card-content collapse show">
                                    <div class="card-body" id="panelBill7">
                                        <div class="row">
                                            <div class="col-md-12 form-group">
                                                <textarea id="txtOrderRemarks" maxlength="500" onchange="updateDraftPO()" class="form-control border-primary"></textarea>
                                                <span style="color: red;"><i>[Note :PO remark for All Vendor upto 500 characters]</i></span>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </section>

            </div>

            <div class="col-md-5 col-sm-5">
                <div class="card" style="padding: 10px;">
                    <section id="Div2">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <button type="button" id="btnReset" onclick="resetAllField()" class="btn btn-secondary buttonAnimation round box-shadow-1 btn-sm pulse" style="display: none">Reset</button>
                                        <button type="button" id="btnCancel" class="btn btn-warning buttonAnimation round box-shadow-1 btn-sm pulse" style="display: none">Cancel</button>
                                        <button type="button" id="btnGeneratePO" onclick="generatePO()" class="btn btn-success buttonAnimation round box-shadow-1 btn-sm pulse">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="BarCodenotExists" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="display: block">
                    <h4>Message</h4>
                </div>
                <div class="modal-body">
                    <div style="border-color: #ac2925; border: 1px solid; color: #ac2925; min-height: 45px; line-height: 20px; padding: 9px;">
                        <strong id="barcodemsg"></strong>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse" onclick="focusonBarcode()">OK</button>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="ItemAutoId" />
    <audio class="audios" id="yes_audio" controls preload="none" style="display: none">
        <source src="/Audio/NOExists.mp3" type="audio/mpeg" />
    </audio>
    <audio class="audios" id="No_audio" controls preload="none" style="display: none">
        <source src="/Audio/WrongProduct.mp3" type="audio/mpeg" />
    </audio>
    <%-- <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="Js/PurchaseOrderBulk.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>--%>
</asp:Content>
