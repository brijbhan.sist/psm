﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/MasterPage.master" AutoEventWireup="true" CodeFile="PurchaseOrderList.aspx.cs" Inherits="PurchaseOrderList" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .StatusRTS {
            BACKGROUND-COLOR: #1166b1;
            COLOR: WHITE;
            PADDING: 3PX;
            BORDER-RADIUS: 5PX;
        }

        .addon {
            background-color: green;
            color: white;
            padding: 5px;
            border-radius: 5px;
        }

        .input-group-text {
            padding: 0 1rem;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">PO List</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item">Manage PO
                        </li>
                        <li class="breadcrumb-item">By Internal
                        </li>
                        <li class="breadcrumb-item">PO List
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12" id="Action">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>

                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a class="dropdown-item" href="~/Purchase/PurchaseOrder.aspx" runat="server">Add New Purchase Order</a>
                </div>


                <input type="hidden" id="hiddenPackerAutoId" runat="server" />
                <input type="hidden" id="hiddenEmpType" runat="server" />
            </div>
        </div>
    </div>
    <div class="content-body" style="min-height: 400px">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-4">
                                        <select class="form-control border-primary input-sm" id="ddlVender" runat="server">
                                            <option value="0">All Vendor</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-4 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlStatus">
                                            <option value="0">All Vendor Status</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-4 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlPoStatus">
                                            <option value="">All PO Status</option>
                                            <option value="Pending">Pending</option>
                                            <option value="Received">Received</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="PO No" id="txtPONo" onfocus="this.select()" />
                                    </div>
                                    <div class="col-sm-12 col-md-3 form-group" id="col4" style="display: none"></div>

                                    <div class="col-sm-12 col-md-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 0.5rem">PO From Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" onchange="setdatevalidation(1)" placeholder="PO From Date" id="txtSFromDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 0.5rem">PO To Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" onchange="setdatevalidation(2)" placeholder="PO To Date" id="txtSToDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-12">
                                        <button type="button" class="btn btn-info pull-right buttonAnimation round box-shadow-1  btn-sm" id="btnSearch">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblOrderList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="action text-center">Action</td>
                                                        <td class="PONo  text-center">PO No</td>
                                                        <td class="PODate  text-center">PO Date</td>
                                                        <td class="vender">Vendor Name</td>
                                                        <td class="POStatus  text-center">PO Status</td>
                                                        <td class="VenderStatus  text-center">Vendor Status</td>
                                                        <td class="itenms text-center">No of Items</td>
                                                        <td class="OrderTotal right price">Order Total Amount</td>
                                                        <td class="ReffNo  text-center">Reference No</td>
                                                        <td class="DDate text-center">Delivery Date</td>
                                                        <td class="PORemark  text-center">PO Remarks</td>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex">
                                    <div class="form-group">
                                        <select id="ddlPageSize" class="form-control border-primary input-sm">
                                            <option selected="selected" value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="ml-auto">
                                        <div class="Pager"></div>
                                    </div>
                                </div>

                            </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="Js/PurchaseOrderList.js?v=' + new Date() + '"></sc' + 'ript>');
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>

