﻿using DllPurchaseOrder;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PurchaseOrder : System.Web.UI.Page
{


    protected void Page_load(object sender, EventArgs e)
    {
        try
        {
            string text = File.ReadAllText(Server.MapPath("/Purchase/JS/PurchaseOrderDetails.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredField'>" + text + "</script>"
                ));
            hiddenEmpTypeVal.Value = Session["EmpTypeNo"].ToString();
        }
        catch
        {
            Session.Abandon();
            Response.Redirect("~/Default.aspx", false);
        }


        string[] GtUrl = Request.Url.Host.ToString().Split('.');
    }

    [WebMethod(EnableSession = true)]
    public static string getManagePrice(string ProductId)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_PurchaseOrder pobj = new PL_PurchaseOrder();
            pobj.ProductId = Convert.ToInt32(ProductId);
            BL_PurchaseOrder.getManagePrice(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string UpdateManagePrice(string dataTable)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_PurchaseOrder pobj = new PL_PurchaseOrder();
            DataTable dtOrder = new DataTable();
            dtOrder = JsonConvert.DeserializeObject<DataTable>(dataTable);
            pobj.EmpId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"].ToString());
            if (dtOrder.Rows.Count > 0)
                pobj.DtPackingDetails = dtOrder;
            BL_PurchaseOrder.UpdateManagePrice(pobj);
            if (!pobj.isException)
            {
                return "true";
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";

        }
    }
}