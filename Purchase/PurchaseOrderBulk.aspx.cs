﻿
using DllPurchaseOrderBulk;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PurchaseOrder : System.Web.UI.Page
{


    protected void Page_load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Purchase/Js/PurchaseOrderBulk.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredFields'>" + text + "</script>"
                ));
        }
        try
        {
            hiddenEmpTypeVal.Value = Session["EmpTypeNo"].ToString();
        }
        catch
        {
            Session.Abandon();
            Response.Redirect("~/Default.aspx", false);
        }
        string[] GtUrl = Request.Url.Host.ToString().Split('.');
    }
    [WebMethod(EnableSession = true)]
    public static string BindProduct()
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                PL_PurchaseOrder pobj = new PL_PurchaseOrder();
                //pobj.VenderAutoId = Convert.ToInt32(Vendor);
                BL_PurchaseOrder.bindProduct(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0].ToString();
                }
                if (json == "")
                {
                    json = "[]";
                }
                return json;

            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string updateDraftPO(string dataValues)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValues);
                PL_PurchaseOrder pobj = new PL_PurchaseOrder();
                pobj.DraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
                pobj.PORemarks = jdv["PORemark"];
                BL_PurchaseOrder.updateDraftPO(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string RefillProduct(string dataValues)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValues);
                PL_PurchaseOrder pobj = new PL_PurchaseOrder();
                pobj.RefillType = Convert.ToInt32(jdv["RefillType"]);
                pobj.POType = Convert.ToInt32(jdv["POType"]);
                //pobj.VenderAutoId = Convert.ToInt32(jdv["VenderAutoId"]);
                pobj.EmpId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                if (jdv["DraftAutoId"] != "")
                {
                    pobj.DraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
                }
                if (jdv["Remark"] != "")
                {
                    pobj.PORemarks = jdv["Remark"];
                }
                BL_PurchaseOrder.RefillProduct(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }

                return json;
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}