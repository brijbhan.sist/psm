﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="PurchaseOrderDetails1.aspx.cs" Inherits="PurchaseOrder" ClientIDMode="static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        #tblProductDetail tbody .ProName {
            text-align: left;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="col-md-12" id="PageDiv" style="display:none;">
    <input id="DraftAutoId" type="hidden" />
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">PO Details</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item">Manage PO
                        </li>
                        <li class="breadcrumb-item">By Internal
                        </li>
                        <li class="breadcrumb-item">Generate PO 
                        </li>
                        <li class="breadcrumb-item">PO Details 
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a class="dropdown-item" href="~/Purchase/PurchaseOrderList.aspx" runat="server">Purchase Order List</a>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <input type="hidden" id="hiddenEmpTypeVal" runat="server" />
                        <input type="hidden" id="hiddenStatusCode" runat="server" />
                        <div class="card-content collapse show">
                            <div class="card-body" id="panelBill">
                                <div class="row">
                                    <div class="col-md-3 col-sm-3">
                                        <label class="control-label">PO No</label>
                                        <input type="hidden" id="txtPOAutoId" class="form-control input-sm" />
                                        <div class="form-group">
                                            <input type="text" id="txtOrderId" class="form-control input-sm border-primary" readonly="readonly" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label class="control-label">PO Date</label>
                                        <div class="form-group">
                                            <input type="text" id="txtOrderDate" class="form-control input-sm border-primary" readonly="readonly" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label class="control-label">Delivery Date</label>
                                        <div class="form-group input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" id="txtDeliveryDate" class="form-control input-sm border-primary" readonly="readonly" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label class="control-label">PO Status</label>
                                        <div class="form-group">
                                            <input type="text" id="txtPOStatus" class="form-control input-sm border-primary" readonly="readonly" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6  col-sm-6">
                                        <label class="control-label">
                                            Vendor

                                        </label>
                                        <div class="form-group">
                                            <input type="hidden" id="ddlVender" />
                                            <input type="text" id="txtVender" class="form-control input-sm border-primary" readonly="readonly" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label class="control-label">Reference Order No</label>
                                        <div class="form-group">
                                            <input type="text" id="txtRefOrderNo" class="form-control input-sm border-primary" readonly="readonly" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label class="control-label">Vendor Status</label>
                                        <input type="hidden" id="hiddenOrderStatus" />
                                        <div class="form-group">
                                            <input type="text" id="txtOrderStatus" class="form-control input-sm border-primary" readonly="readonly" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body" id="panelBill6">
                                <div class="table-responsive">
                                    <table id="tblProductDetail" class="table table-striped table-bordered">
                                        <thead class="bg-blue white">
                                            <tr>
                                                <td class="Head" style="display: none;"></td>
                                                <td colspan="2">Product</td>
                                                <td colspan="3">PO</td>
                                                <td class="HeadR" colspan="3" style="display: none;">Received</td>
                                                <td class="Head" colspan="2" style="display: none;">Unit Price</td>                                                
                                                <td class="Head" rowspan="2" style="display: none;">Net Price</td>
                                            </tr>
                                            <tr>
                                                <td class="action text-center" style="display: none;">Action</td>
                                                <td class="ProId text-center">ID</td>
                                                <td class="ProName">Name</td>
                                                <td class="UnitType text-center">Unit</td>
                                                <td class="ReqQty text-center wth2">Qty</td>
                                                <td class="TtlPcs text-center">Pieces</td>
                                                <td class="PackdUnit text-center" style="display: none;">Unit</td>
                                                <td class="PackdQty text-center" style="display: none;">Qty</td>
                                                <td class="PackdPieces text-center" style="display: none;">Pieces</td>
                                                <td class="CCostPrice text-center" style="display: none;">Old</td>
                                                <td class="NewCostPrice text-center" style="display: none;">New</td>
                                                <td class="NetPrice text-center" style="display: none;">Net Price</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                        <tfoot style="font-weight:bold">
                                            <tr>
                                                <td id="tdItem" colspan="12"><span id="TotalItem" style="float: right;"></span></td>
                                            </tr>
                                            <tr id="tdPrice">
                                              <td colspan="11"  id="tdPriceTD" style="text-align:right">Total Price</td>  <td><span id="spTotalPrice" style="float: right;">0.00</span></td>
                                            </tr>

                                        </tfoot>
                                    </table>
                                    <h5 class="well text-center" id="emptyTable" style="display: none">No Product Selected.</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>

        <div class="row">
            <div class="col-md-12 col-sm-12">
                <section id="Div1">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Purchase Order Remark</h4>
                                </div>

                                <div class="card-content collapse show">
                                    <div class="card-body" id="panelBill7">
                                        <div class="row">
                                            <div class="col-md-12 form-group">
                                                <textarea id="txtOrderRemarks" class="form-control border-primary" readonly="readonly"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 RevieveRemark" style="display:none;">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Received Order Remark</h4>
                                </div>

                                <div class="card-content collapse show">
                                    <div class="card-body" id="">
                                        <div class="row">
                                            <div class="col-md-12 form-group">
                                            
                                                <textarea id="txtReciveOrderRemarks" maxlength="500" class="form-control border-primary"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 RevieveRemark" style="display:none;"></div>
                        <div class="col-md-6 col-sm-6">
                            <section id="div">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card" style="padding: 10px;">
                                        <div class="row">
                                            <div class="col-md-12 text-right">
                                                <button type="button" id="btnEdit" class="btn btn-info buttonAnimation round box-shadow-1 btn-sm pulse" style="display: none">Edit</button>
                                                <button type="button" id="btnDelete" class="btn btn-danger buttonAnimation round box-shadow-1 btn-sm pulse" style="display: none">Delete</button>
                                                <button type="button" id="btnBack" class="btn btn-cyan buttonAnimation round box-shadow-1 btn-sm pulse">Back</button>
                                                <button type="button" id="UpdateStock" class="btn btn-info buttonAnimation round box-shadow-1 btn-sm pulse" style="display: none">Receive Stock</button>
                                            </div>
                                        </div>
                                            </div>
                                    </div>
                                </div>
                            </section>
                        </div>

                    </div>

                </section>

            </div>
            <center>
              <div id="managePrice" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-xl" style="width: 70%">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 style="float: left" class="modal-title">Manage Price</h4>&nbsp&nbsp&nbsp&nbsp
                    <h3 class="modal-title" id="ProductText"></h3>
                    <input type="hidden" id="txtHProductAutoId" />
                    <%--<button type="button" style="margin:-1rem 0 -1rem auto !important" class="close" data-dismiss="modal">&nbsp x &nbsp</button>--%>
                </div>

                <div class="modal-body">
                  <div class="row">
                        <div class="col-md-12">
                            <style>
                                .table th, .table td {
                                    padding: 0.75rem 1rem !important;
                                }
                            </style>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered" id="Table1">
                                    <thead class="bg-blue white">
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td colspan="2">Cost Price</td> 
                                            <td colspan="2">WH. Min Price</td>
                                            <td colspan="2">Min Retail Price</td>
                                          <td colspan="2">Base Price</td>
                                        </tr>
                                        
                                        <tr>
                                            <td class="UnitType" style="width:10%;">Unit Type</td>
                                            <td class="Qty text-center" style="width:10%;">Qty</td>
                                            <td class="CostPrice" style="width:10%;text-align:right">Old</td>
                                            <td class="NewCosePrice" style="width:10%;text-align:right">New</td>
                                             <td class="WHminPrice" style="width:10%;text-align:right">Old</td>
                                            <td class="NWHPrice" style="width:10%;text-align:right">New</td>
                                            <td class="RetailPrice" style="width:10%;text-align:right">Old</td>
                                            <td class="NewRetailPrice" style="width:10%;text-align:right">New</td>
                                             <td class="BasePrice" style="width:10%;text-align:right">Old</td>
                                            <td class="NewBasePrice" style="width:10%;text-align:right">New</td>
                                           
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                   <%-- <div class="alert alert-danger alert-dismissable fade in" id="Div1" style="width: 33%; text-align: left;">
                      <span><b style="color:red">Note - </b>Please check the Enable section check box to manage price.</span>
                    </div>--%>
                    <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1  btn-sm" onclick="updatebulk()">&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;</button>
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1  btn-sm" data-dismiss="modal">&nbsp;&nbsp;&nbsp;Close&nbsp;&nbsp;&nbsp;</button>
                </div>
            </div>
        </div>
    </div>
</center>
            <input type="hidden" id="Index" />
        </div>
    </div>

        </div>
    <div class="modal fade" id="mdlInactiveProductList" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content" style=" width: 700px !important;">
                <div class="modal-header">
                    <h4>Inactive Product List</h4>
                </div>
                <div class="modal-body">
                    <table class="table table-striped table-bordered" id="tblInactiveProductList">
                        <thead class="bg-blue white">
                            <tr>
                                <td class="ProductId" style="text-align: center">Product Id</td>
                                <td class="ProductName" style="text-align: left">Product Name</td>
                                <td class="Status" style="text-align: center">Status</td>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="Js/PurchaseOrderDetails.js?v=8390450"></sc' + 'ript>');
    </script>
</asp:Content>
