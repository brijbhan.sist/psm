﻿
using DllPurchaseOrder;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DllPrintPO;
public partial class PurchaseOrder : System.Web.UI.Page
{


    protected void Page_load(object sender, EventArgs e)
    {
        try
        {
                      
            hiddenEmpTypeVal.Value = Session["EmpTypeNo"].ToString();
        }
        catch
        {
            Session.Abandon();
            Response.Redirect("~/Default.aspx", false);
        }


        string[] GtUrl = Request.Url.Host.ToString().Split('.');
    }
    [WebMethod(EnableSession = true)]
    public static string BindProduct(string Vendor)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                PL_PurchaseOrder pobj = new PL_PurchaseOrder();
                pobj.VenderAutoId = Convert.ToInt32(Vendor);
                BL_PurchaseOrder.bindProduct(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0].ToString();
                }
                if (json == "")
                {
                    json = "[]";
                }
                return json;

            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string updateDraftPO(string dataValues)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValues);
                PL_PurchaseOrder pobj = new PL_PurchaseOrder();
                pobj.DraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
                pobj.PORemarks = jdv["PORemark"];
                BL_PurchaseOrder.updateDraftPO(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string PrintPO(string POAutoId,string type)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                PL_PrintPO pobj = new PL_PrintPO();
                pobj.PoAutoId = Convert.ToInt32(POAutoId);
                pobj.type = Convert.ToInt32(type);
                BL_PrintPO.PrintPurchaseOrder(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}