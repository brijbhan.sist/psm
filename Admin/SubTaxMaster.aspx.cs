﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DllSubTaxMaster;
public partial class SubTaxMaster : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Admin/JS/SubTaxMaster.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredField'>" + text + "</script>"
                ));
        }
    }
    [WebMethod(EnableSession = true)]
    public static string insertTax(string dataValue)
    {
        if (HttpContext.Current.Session["EmpTypeNo"] != null)
        {

            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_SubTaxMaster pobj = new PL_SubTaxMaster();
            try
            {
                pobj.TaxName = jdv["TaxName"];
                pobj.UserAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                if (jdv["Value"] != "")
                {
                    pobj.Value = Convert.ToDecimal(jdv["Value"]);
                }
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.Description =jdv["Description"];
                BL_SubTaxMaster.insert(pobj);
                if (!pobj.isException)
                {
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string getTaxDetail(string dataValue)
    {
        if (HttpContext.Current.Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_SubTaxMaster pobj = new PL_SubTaxMaster();
            try
            {
                pobj.TaxName = jdv["TaxName"];
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                BL_SubTaxMaster.select(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string editTax(string TaxId)
    {
        if (HttpContext.Current.Session["EmpTypeNo"] != null)
        {
            PL_SubTaxMaster pobj = new PL_SubTaxMaster();
            pobj.AutoId = Convert.ToInt32(TaxId);
            BL_SubTaxMaster.editTax(pobj);
            if (!pobj.isException)
            {
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string updateTax(string dataValue)
    {
        if (HttpContext.Current.Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_SubTaxMaster pobj = new PL_SubTaxMaster();
            try
            {
                pobj.AutoId =Convert.ToInt32(jdv["TaxId"]);
                pobj.UserAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                pobj.TaxName = jdv["TaxName"];
                if (jdv["Value"] != "")
                {
                    pobj.Value = Convert.ToDecimal(jdv["Value"]);
                }
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.Description = jdv["Description"];
                BL_SubTaxMaster.update(pobj);
                if (!pobj.isException)
                {                
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string deleteTax(string TaxId)
    {
        if (HttpContext.Current.Session["EmpTypeNo"] != null)
        {
            PL_SubTaxMaster pobj = new PL_SubTaxMaster();
            try
            {
                pobj.AutoId = Convert.ToInt32(TaxId);
                BL_SubTaxMaster.delete(pobj);
                if (!pobj.isException)
                {
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    
}