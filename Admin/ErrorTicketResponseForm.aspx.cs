﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Data;
public partial class Admin_ErrorTicketResponseForm : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["EmpTypeNo"].ToString() == "1")
        {
            developerStatus.Visible = true;
        }
        else
        {
            descrip.Attributes["class"] = "col-md-9";
        }
    }
}