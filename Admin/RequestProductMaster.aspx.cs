﻿using DLLRequestProduct;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_RequestProductMaster : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Admin/JS/RequestProductMaster.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredFields'>" + text + "</script>"
                ));
        }
    }
    [WebMethod(EnableSession = true)]
    public static string insertProduct(string dataValue, string dataTable, string Locationstatus)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_Product pobj = new PL_Product();
        DataTable dtOrder = new DataTable();
        dtOrder = JsonConvert.DeserializeObject<DataTable>(dataTable);
        DataTable dtLocationstatus = new DataTable();
        dtLocationstatus = JsonConvert.DeserializeObject<DataTable>(Locationstatus);
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.ProductName = jdv["ProductName"];
                pobj.BrandAutoId = Convert.ToInt32(jdv["BrandAutoId"]);
                pobj.IsAppyMLQty = Convert.ToInt32(jdv["IsApplyMlQty"]);
                pobj.IsAppyWeightQty = Convert.ToInt32(jdv["IsApplyWeightQty"]);
                pobj.ProductId = jdv["ProductId"];
                pobj.ImageUrl = "/DraftProduct/" + (jdv["ImageUrl"]);
                pobj.ThumbnailImage100 = "/DraftProduct/100_100_Thumbnail_" + (jdv["ImageUrl"]);
                pobj.ThumbnailImage400 = "/DraftProduct/400_400_Thumbnail_" + (jdv["ImageUrl"]);
                pobj.CategoryAutoId = Convert.ToInt32(jdv["CategoryAutoId"]);
                pobj.SubcategoryAutoId = Convert.ToInt32(jdv["SubcategoryAutoId"]);
                if (jdv["Weight"] != "")
                    pobj.WeightOz = Convert.ToDecimal(jdv["Weight"]);
                if (jdv["ReOrderMark"] != "")
                    pobj.ReOrderMark = Convert.ToInt32(jdv["ReOrderMark"]);
                if (jdv["MLQty"] != "")
                    pobj.MLQty = Convert.ToDecimal(jdv["MLQty"]);
                pobj.EmpAutoId = HttpContext.Current.Session["EmpAutoId"].ToString();
                pobj.dtbulkUnit = dtOrder;
                pobj.LocationStatus=dtLocationstatus;
                pobj.Status = 1;
                if (jdv["CommCode"] != "")
                {
                    pobj.CommCode = Convert.ToDecimal(jdv["CommCode"]);
                }
                if (jdv["SRP"] != "")
                {
                    pobj.SRP = Convert.ToDecimal(jdv["SRP"]);
                }
                string[] GtUrl = HttpContext.Current.Request.Url.Host.ToString().Split('.');
                string dirFullPath = "";
                dirFullPath = HttpContext.Current.Server.MapPath("~/DraftProduct/" + jdv["ImageUrl"]);

                if (File.Exists(dirFullPath))
                {
                    BL_Product.insert(pobj);
                    if (!pobj.isException)
                    {
                       // getMailBody1(pobj, "1");
                        return "Success";
                    }
                    else
                    {
                        return pobj.exceptionMessage;
                    }
                }
                else
                {
                    return "file does not exist.";
                }

            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return "false";
        }
    }


    [WebMethod(EnableSession = true)]
    public static string updateProduct(string dataValue, string dataTable)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_Product pobj = new PL_Product();
        DataTable dtOrder = new DataTable();
        dtOrder = JsonConvert.DeserializeObject<DataTable>(dataTable);
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.AutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.IsAppyMLQty = Convert.ToInt32(jdv["IsApplyMlQty"]);
                pobj.IsAppyWeightQty = Convert.ToInt32(jdv["IsApplyWeightQty"]);
                pobj.BrandAutoId = Convert.ToInt32(jdv["BrandAutoId"]);
                pobj.ProductId = jdv["ProductId"];
                pobj.ProductName = jdv["ProductName"];
                pobj.ImageUrl = "/DraftProduct/" + (jdv["ImageUrl"]);
                pobj.ThumbnailImage100 = "/DraftProduct/100_100_Thumbnail_" + (jdv["ImageUrl"]);
                pobj.ThumbnailImage400 = "/DraftProduct/400_400_Thumbnail_" + (jdv["ImageUrl"]);
                pobj.CategoryAutoId = Convert.ToInt32(jdv["CategoryAutoId"]);
                pobj.SubcategoryAutoId = Convert.ToInt32(jdv["SubcategoryAutoId"]);
                pobj.dtbulkUnit = dtOrder;
                if (jdv["ReOrderMark"] != "")
                    pobj.ReOrderMark = Convert.ToInt32(jdv["ReOrderMark"]);
                if (jdv["MLQty"] != "")
                    pobj.MLQty = Convert.ToDecimal(jdv["MLQty"]);
                if (jdv["WeightOz"] != "")
                    pobj.WeightOz = Convert.ToDecimal(jdv["WeightOz"]);
                pobj.EmpAutoId = HttpContext.Current.Session["EmpAutoId"].ToString();
                if (jdv["CommCode"] != "")
                {
                    pobj.CommCode = Convert.ToDecimal(jdv["CommCode"]);
                }
                if (jdv["SRP"] != "")
                {
                    pobj.SRP = Convert.ToDecimal(jdv["SRP"]);
                }
                
                string[] GtUrl = HttpContext.Current.Request.Url.Host.ToString().Split('.');
                string dirFullPath = "";
                dirFullPath = HttpContext.Current.Server.MapPath("~/DraftProduct/" + jdv["ImageUrl"]);

                if (File.Exists(dirFullPath))
                {
                    BL_Product.updateProduct(pobj);
                    if (!pobj.isException)
                    {
                       //getMailBody1(pobj, "2");
                        return "Success";
                    }
                    else
                    {
                        return pobj.exceptionMessage;
                    }
                }
                else
                {
                    return "file does not exist.";
                }
                
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return "Oops, Something went wrong .please try later.";
        }
    }

    private static string getMailBody1(PL_Product pobj, string Type)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            string Body = "", location = "";
            Body = "<!DOCTYPE html><html xmlns='http://www.w3.org/1999/xhtml'>";
            Body += "<head> <style>table{border-collapse:collapse;}td {padding: 3px 8px;}.text-center{text-align:center}";
            Body += "table {width: 100%;}body {font-size: 14px;font-family: monospace;}";
            Body += ".center{text-align: center;}.right {text-align: right;}thead > tr {background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a;}";
            Body += "thead > tr > td {text-align: center !important;}.InvContent {width: 100%;}.tdLabel {font-weight: 700;}";
            Body += "@media print {#btnPrint {display: none;}}legend {border-bottom: 1px solid #5f7c8a;}";
            Body += ".tableCSS > tbody > tr > td,.tableCSS > thead > tr > td,.tableCSS > tfoot > tr > td {border: 1px solid #5f7c8a;}pre{border:none !important;}";
            Body += "</style>";
            Body += "</head>";
            Body += "<body style='font-family: Arial; font-size: 12px'>";
            try
            {
                DataSet Ds = pobj.Ds;
                var productAutoId = Ds.Tables[0].Rows[0]["ProductId"];

                if (Ds.Tables[0].Rows.Count > 0)
                {
                    Body += "<table class='table tableCSS'>";
                    Body += "<tbody>";
                    Body += "<tr><td><b>Product Id</b></td><td>" + Ds.Tables[0].Rows[0]["Prdtid"].ToString() + "</td></tr>";
                    Body += "<tr><td><b>Product Name</b></td><td>" + Ds.Tables[0].Rows[0]["ProductName"].ToString() + "</td></tr>";
                    Body += "<tr><td><b>Product Image</b></td><td><img src=" + Ds.Tables[0].Rows[0]["ImageUrl"].ToString() + " alt='Product Image' style='width: 50px;height: 50px;'></td></tr>";
                    Body += "<tr><td><b>Category</b></td><td>" + Ds.Tables[0].Rows[0]["CategoryName"].ToString() + "</td></tr>";
                    Body += "<tr><td><b>Subcategory</b></td><td>" + Ds.Tables[0].Rows[0]["SubcategoryName"].ToString() + "</td></tr>";
                    Body += "<tr><td><b>Brand</b></td><td>" + Ds.Tables[0].Rows[0]["BrandName"].ToString() + "</td></tr>";
                    Body += "<tr><td><b>Re Order Mark (Pieces)</b></td><td>" + Ds.Tables[0].Rows[0]["ReOrderMark"].ToString() + "</td></tr>";
                    Body += "<tr><td><b>Is Apply ML Quantity</b></td><td>" + Ds.Tables[0].Rows[0]["IsApply_ML"].ToString() + "</td></tr>";
                    Body += "<tr><td><b>ML Quantity</b></td><td>" + Ds.Tables[0].Rows[0]["MLQty"].ToString() + "</td></tr>";
                    Body += "<tr><td><b>Is Apply Weight (OZ)</b></td><td>" + Ds.Tables[0].Rows[0]["IsApply_Oz"].ToString() + "</td></tr>";
                    Body += "<tr><td><b>Weight (Oz)</b></td><td>" + Ds.Tables[0].Rows[0]["WeightOz"].ToString() + "</td></tr>";
                   // Body += "<tr><td><b>Status</b></td><td>" + Ds.Tables[0].Rows[0]["ProductStatus"].ToString() + "</td></tr>";
                    Body += "<tr><td><b>Commission Code</b></td><td>" + Ds.Tables[0].Rows[0]["P_CommCode"].ToString() + "</td></tr>";
                    Body += "<tr><td><b>SRP</b></td><td>" + Ds.Tables[0].Rows[0]["SRP"].ToString() + "</td></tr>";


                    if (Type == "1")
                    {
                        Body += "<tr><td><b>Create Date & Time</b></td><td>" + Ds.Tables[0].Rows[0]["CreateDate"].ToString() + "</td></tr>";
                        Body += "<tr><td><b>Created By</b></td><td>" + Ds.Tables[0].Rows[0]["EmpName"].ToString() + "</td></tr>";
                    }
                    else if (Type == "12")
                    {
                        Body += "<tr><td><b>Create Date & Time</b></td><td>" + Ds.Tables[0].Rows[0]["CreateDate"].ToString() + "</td></tr>";
                        Body += "<tr><td><b>Created By</b></td><td>" + Ds.Tables[0].Rows[0]["EmpName"].ToString() + "</td></tr>";
                    }
                    else
                    {
                        Body += "<tr><td><b>Update Date & Time</b></td><td>" + Ds.Tables[0].Rows[0]["UpdateDate"].ToString() + "</td></tr>";
                        Body += "<tr><td><b>Updated By</b></td><td>" + Ds.Tables[0].Rows[0]["EmpName"].ToString() + "</td></tr>";
                    }

                    location = Ds.Tables[0].Rows[0]["Location"].ToString();
                }
                Body += "</tbody>";
                Body += "</table>";

                //Location wise status start
                if (pobj != null && pobj.Ds.Tables[3].Rows.Count > 0)
                {
                    Body += "<table class='table tableCSS' style='margin-top: 10px;'>";
                    Body += "<tbody>";
                    Body += "<tr>";
                    foreach (DataRow dr in pobj.Ds.Tables[3].Rows)
                    {
                        if (dr["Status"].ToString() == "0")
                        {
                            Body += "<td style='text-align:center'>" + dr["Location"].ToString() + " : <b>Inactive</b></td>";
                            //Body += "<label class='container'>" + dr["Location"].ToString() + "<input type='checkbox' disabled><span class='checkmark'></span></label>";

                        }
                        else
                        {
                            Body += "<td style='text-align:center'>" + dr["Location"].ToString() + " : <b>Active</b></td>";
                            //Body += "<label class='container'>" + dr["Location"].ToString() + "<input type='checkbox' disabled checked='checked'><span class='checkmark'></span></label>";

                        }

                    }
                    Body += "</tr>";
                    Body += "</tbody>";
                    Body += "</table>";
                }
                    //Location wise status end



                    if (pobj != null && pobj.Ds.Tables[1].Rows.Count > 0)
                {
                    #region
                    Body += "<table class='table tableCSS' style='margin-top: 10px;'>";
                    if (pobj.Ds.Tables[1].Rows.Count > 0)
                    {
                        Body += "<thead>";
                        Body += "<tr>";
                        Body += "<td><b>Unit Type</b></td><td><b>Quantity</b></td><td><b>Cost Price</b></td><td><b>Wholesale Minimum Price</b></td><td><b>Retail Minimum Price</b></td><td><b>Base Price</b></td><td><b>Free</b></td><td><b>Location</b></td><td><b>Barcode</b></td>";
                        Body += "</tr>";
                        Body += "</thead>";
                        Body += "<tbody>";
                        Body += "<tr>";

                        foreach (DataRow dr in pobj.Ds.Tables[1].Rows)
                        {
                            var a = dr["DefPacking"].ToString();
                            if (a != "")
                            {
                                Body += "<td style='text-align:center'>" + dr["UnitType"].ToString() + "(Default)</td>";
                            }
                            else
                            {
                                Body += "<td style='text-align:center'>" + dr["UnitType"].ToString() + "</td>";
                            }
                            Body += "<td style='text-align:center'>" + dr["Qty"].ToString() + "</td>";
                            Body += "<td style='text-align:right'>" + dr["CostPrice"].ToString() + "</td>";
                            Body += "<td style='text-align:right'>" + dr["WHminPrice"].ToString() + "</td>";
                            Body += "<td style='text-align:right'>" + dr["RetailMinPrice"].ToString() + "</td>";
                            Body += "<td style='text-align:right'>" + dr["Price"].ToString() + "</td>";
                            Body += "<td style='text-align:left'>" + dr["EligibleforFree"].ToString() + "</td>";
                            Body += "<td style='text-align:left'>" + dr["Location"].ToString() + "</td>";
                            Body += "<td style='text-align:left'>" + dr["Barcode"].ToString() + "</td><tr/>";

                        }

                    }
                    Body += "</tbody>";
                    Body += "</table>";

                    Body += "<br>";
                    Body += "<div class='row'>";
                    Body += "<a href='http://psmnj.a1whm.com/Admin/RequestProductMaster.aspx?ProductId=" + productAutoId + "' style='background: green!important;padding: 9px 8px 23px 8px!important;vertical-align: middle!important;color: #fff!important;line-height: 33px!important;text-decoration: none!important;'>Create Product</a>";
                    Body += "</div>";
                    #endregion

                    Body += "</body>";
                    Body += "</html>";
                    string OperationType = "", ReceiverEmailId = "";
                    if (Type == "1")
                    {
                        OperationType = location + "- Draft Product Created";

                    }
                    else if (Type == "12")
                    {
                        OperationType = location + "- Product created succussfully";

                    }
                    else
                    {
                        OperationType = location + "- Draft Product updated";
                    }

                    foreach (DataRow dr in pobj.Ds.Tables[2].Rows)
                    {
                        ReceiverEmailId += dr["Email"].ToString() + ",";
                    }


                    if (ReceiverEmailId != null && ReceiverEmailId != string.Empty && ReceiverEmailId.Length > 0)
                    {
                        string emailmsg = MailSending.SendMailResponse(ReceiverEmailId, "rizwan.sist@gmail.com", "brijbhan.sist@gmail.com,saharab.sist@gmail.com,farheen.sist@gmail.com", OperationType, Body.ToString(), "Developer");
                        if (emailmsg == "Successful")
                        {
                            return "true";
                        }
                        else
                        {
                            return "EmailNotSend";
                        }
                    }
                    else
                    {
                        return "receiverMail";
                    }
                }
                else
                {
                    return "EmailNotSend";
                }
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }

    }


    // CREATE PRODUCT FROM DRAFT PRODUCT
    [WebMethod(EnableSession = true)]
    public static string createProduct(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_Product pobj = new PL_Product();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.ProductId = jdv["ProductId"];
                pobj.EmpAutoId = HttpContext.Current.Session["EmpAutoId"].ToString();
                BL_Product.create(pobj);
                if (!pobj.isException)
                {
                   // getMailBody1(pobj, "3");
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception)
        {
            return "Oops,some thing went wrong.Please try again.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string ProductCreate(string dataValue, string dataTable, string Locationstatus)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_Product pobj = new PL_Product();
        DataTable dtOrder = new DataTable();
        dtOrder = JsonConvert.DeserializeObject<DataTable>(dataTable);
        DataTable dtLocationstatus = new DataTable();
        dtLocationstatus = JsonConvert.DeserializeObject<DataTable>(Locationstatus);
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.AutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.IsAppyMLQty = Convert.ToInt32(jdv["IsApplyMlQty"]);
                pobj.IsAppyWeightQty = Convert.ToInt32(jdv["IsApplyWeightQty"]);
                pobj.BrandAutoId = Convert.ToInt32(jdv["BrandAutoId"]);
                pobj.ProductId = jdv["ProductId"];
                pobj.ProductName = jdv["ProductName"];
                pobj.ImageUrl = "/DraftProduct/" + (jdv["ImageUrl"]);
                pobj.ThumbnailImage100= "/DraftProduct/100_100_Thumbnail_" + (jdv["ImageUrl"]);
                pobj.ThumbnailImage400 = "/DraftProduct/400_400_Thumbnail_" + (jdv["ImageUrl"]);
                pobj.CategoryAutoId = Convert.ToInt32(jdv["CategoryAutoId"]);
                pobj.SubcategoryAutoId = Convert.ToInt32(jdv["SubcategoryAutoId"]);
                pobj.dtbulkUnit = dtOrder;
                pobj.LocationStatus = dtLocationstatus;
                if (jdv["ReOrderMark"] != "")
                    pobj.ReOrderMark = Convert.ToInt32(jdv["ReOrderMark"]);
                if (jdv["MLQty"] != "")
                    pobj.MLQty = Convert.ToDecimal(jdv["MLQty"]);
                if (jdv["WeightOz"] != "")
                    pobj.WeightOz = Convert.ToDecimal(jdv["WeightOz"]);
                pobj.EmpAutoId = HttpContext.Current.Session["EmpAutoId"].ToString();
                if (jdv["CommCode"] != "")
                {
                    pobj.CommCode = Convert.ToDecimal(jdv["CommCode"]);
                }
                if (jdv["SRP"] != "")
                {
                    pobj.SRP = Convert.ToDecimal(jdv["SRP"]);
                }
                pobj.Status = jdv["status"];

                string[] GtUrl = HttpContext.Current.Request.Url.Host.ToString().Split('.');
                string dirFullPath = "";
                    dirFullPath = HttpContext.Current.Server.MapPath("~/DraftProduct/" + jdv["ImageUrl"]);
                if (File.Exists(dirFullPath))
                {
                    BL_Product.updateProduct(pobj);

                    if (!pobj.isException)
                    {
                        
                        return "Success";
                    }
                    else
                    {
                        return pobj.exceptionMessage;
                    }
                }
                else
                {
                    return "file does not exist.";
                }                
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return "Oops, Something went wrong .please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string bindLocation(string AutoId)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_Product pobj = new PL_Product();
            try
            {
                if(AutoId!="")
                {
                    pobj.AutoId = Convert.ToInt32(AutoId);
                }
                BL_Product.bindLocation(pobj);
                
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                return json;
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}