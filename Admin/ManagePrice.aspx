﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="ManagePrice.aspx.cs" Inherits="Admin_ManagePrice" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .table tbody td {
            padding: 3px;
            vertical-align: middle !important;
        }

        .table th, .table td {
            padding: 3px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Manage Price</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Product</a></li>
                        <li class="breadcrumb-item active">Manage Price</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlSCategory" onchange="BindSubCategory();">
                                            <option value="0">All Category</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlSSubcategory">
                                            <option value="0">All Subcategory</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlBrand">
                                            <option value="0">All Brand</option>
                                        </select>
                                    </div>

                                    <div class="col-md-4 form-group">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Product ID" id="txtProductId" />
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Product Name" id="txtProductName" />
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlCostPriceCompare">
                                            <option value="0">All</option>
                                            <option value="1">Cost Price = Wh. Min. Price</option>
                                            <option value="2">Cost Price = Wh. Min. Price = Retail Min. Price</option>
                                            <option value="3">Cost Price = Wh. Min. Price = Retail Min. Price = Base Price</option>
                                        </select>
                                    </div>


                                    <div class="col-md-12 form-group">
                                        <div class="btn-group pull-right" id="displaybtnChangeReorder">
                                            <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" onclick="showChangeReorderModelPop();" id="btnChangeReorder">Change Re Order Mark</button>
                                        </div> 
                                        <div class="btn-group mr-1 pull-right" id="displaybtnChangePrice">
                                            <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" onclick="showChangePriceModelPop();" id="btnChangePrice">Change Price</button>
                                        </div>
                                        <div class="btn-group mr-1 pull-right">
                                            <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" onclick="getProductDetails(1);" id="btnSearch">Search</button>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <section>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row form-group">
                            <div class="col-md-12">
                                <div class="card-content collapse show">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered" id="tblPriceDetails">
                                            <thead class="bg-blue white">
                                                <tr>
                                                    <td class="Action text-center width3per">
                                                        <input type="checkbox" onclick="checkAllProduct()" name="table_records" id="chkAllPrice" /></td>
                                                    <td class="ProductId text-center width3per">Product Id</td>
                                                    <td class="ProductName">Product Name</td>
                                                    <td class="UnitType text-center width2per">Unit</td>
                                                    <td class="Qty text-center text-center width3per">Qty</td>
                                                    <td class="SRP text-center text-center width2per">SRP</td>
                                                    <td class="Stock text-center text-center width2per">Current Stock</td>
                                                    <td class="ReOrderMark text-center text-center width4per" style="text-align: center">Re Order Mark<br />
                                                        (In Default Unit)</td>
                                                    <td class="CostPrice text-center text-center width4per" style="text-align: center">Cost Price</td>
                                                    <td class="WMinPrice text-center text-center width4per">Wh. Min. Price</td>
                                                    <td class="MinPrice text-center text-center width4per">Retail Min Price</td>
                                                    <td class="Price price text-center text-center width4per">Base Price</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                        <h5 class="well text-center" id="emptyTable" style="display: none">No data available.</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex">
                            <div class="form-group">
                                <select class="form-control input-sm border-primary" onchange="getProductDetails(1)" id="ddlPageSize">
                                    <option value="10">10</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                    <option value="500">500</option>
                                    <option value="1000">1000</option>
                                    <option value="0">All</option>
                                </select>
                            </div>
                            <div class="ml-auto">
                                <div class="Pager"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="ChangePricePopup" class="modal fade" role="dialog">
        <div class="modal-dialog modal-xs">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="modal-title">Change Price in Bulk
                                <br />
                                <span style="color: red; text-align: right; font-size: 16px;" id="totalProductSelected"></span></h4>
                        </div>
                    </div>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-6 col-sm-12  form-group">
                            <label class="control-label">Cost Price</label>
                        </div>
                        <div class="col-md-6 col-sm-12  form-group">
                            <input type="text" id="txtCostPrice" style="text-align: right;" maxlength="10" onkeypress="return isNumberDecimalKey(event,this)" maxlength="9" value="0.00" class="form-control border-primary input-sm" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12  form-group">
                            <label class="control-label" style="white-space: nowrap">Whole Sale Min Price</label>
                        </div>
                        <div class="col-md-6 col-sm-12  form-group">
                            <input type="text" id="txtWhMinPrice" style="text-align: right;" maxlength="10" onkeypress="return isNumberDecimalKey(event,this)" value="0.00" class="form-control border-primary input-sm" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12  form-group">
                            <label class="control-label">Retail Min Price</label>
                        </div>
                        <div class="col-md-6 col-sm-12  form-group">
                            <input type="text" id="txtRetailMinPrice" style="text-align: right;" maxlength="10" onkeypress="return isNumberDecimalKey(event,this)" value="0.00" class="form-control border-primary input-sm" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12  form-group">
                            <label class="control-label">Base Price</label>
                        </div>
                        <div class="col-md-6 col-sm-12  form-group">
                            <input type="text" id="txtBasePrice" style="text-align: right;" maxlength="10" onkeypress="return isNumberDecimalKey(event,this)" value="0.00" class="form-control border-primary input-sm" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12  form-group">
                            <label class="control-label">SRP (In Piece)</label>
                        </div>
                        <div class="col-md-6 col-sm-12  form-group">
                            <input type="text" id="txtSRP" style="text-align: right;" maxlength="10" onkeypress="return isNumberDecimalKey(event,this)" value="0.00" class="form-control border-primary input-sm" />
                        </div>
                    </div>
                   <%-- <div class="row">
                        <div class="col-md-6 col-sm-12  form-group">
                            <label class="control-label">
                                Re Order Mark<br />
                                (In Piece)</label>
                        </div>
                        <div class="col-md-6 col-sm-12  form-group">
                            <input type="hidden" id="hdProductAutoId" />
                            <input type="text" id="txtReOrderMask" style="text-align: right;" maxlength="6" onkeypress="return isNumberKey(event,this)" value="0" class="form-control border-primary input-sm" />
                        </div>
                    </div>--%>


                    <div class="modal-footer" style="margin-top: 15px">
                        <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" onclick="UpdateChangePrice()">Update</button>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" id="hfLocation" runat="server" />
    </div>

     <div id="ChangeReorderPopup" class="modal fade" role="dialog">
        <div class="modal-dialog modal-xs">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="modal-title">Change Re Order Mark
                                <br />
                                <span style="color: red; text-align: right; font-size: 16px;" id="totalProductSelection"></span></h4>
                        </div>
                    </div>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-6 col-sm-12  form-group">
                            <label class="control-label">
                                Re Order Mark<%--<br />--%>
                                <%--(In Default Unit)--%></label>
                        </div>
                        <div class="col-md-6 col-sm-12  form-group">
                            <input type="hidden" id="hdProductAutoId" />
                            <input type="text" id="txtReOrderMask" style="text-align: right;" maxlength="6" onkeypress="return isNumberKey(event,this)" value="0" class="form-control border-primary input-sm" />
                        </div>
                    </div>


                    <div class="modal-footer" style="margin-top: 15px">
                        <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" onclick="UpdateChangeReorderMark()">Update</button>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" id="Hidden1" runat="server" />
    </div>
</asp:Content>

