﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true"
    CodeFile="ManagePage.aspx.cs" Inherits="Admin_ManagePage" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        #tblBrand_wrapper {
            padding: 0;
        }

        .desc {
            margin-bottom: 7px;
        }

        .addActionBtn {
            cursor: pointer;
        }

        .hideAutoId {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Manage Page</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage Application</a></li>
                        <li class="breadcrumb-item">Manage Page</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a href="/Admin/ManagePageAccess.aspx">
                        <button type="button" class="dropdown-item">Assign Module</button></a>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Page Details</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row" id="panelPageMaster">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label">Page Id</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" class="form-control input-sm border-primary" id="PageId" runat="server" readonly="true" />
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label">Page Url</label>
                                                <span class="required">*</span>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" class="form-control input-sm border-primary req" id="txtPageUrl" runat="server" />
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label">Type</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <select class="form-control input-sm border-primary" id="pageType" runat="server" onchange="getUserType(this.value);">
                                                    <option value="1">Generic</option>
                                                    <option value="0">Role Based</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row userType" style="display: none;">
                                            <div class="col-md-4">
                                                <label class="control-label">Role</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <select class="form-control input-sm border-primary" id="UserType" runat="server">
                                                    <option value="0">Select Role</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label">Description</label>
                                            </div>
                                            <div class="col-md-8">
                                                <textarea runat="server" class="form-control input-sm border-primary" id="txtDescription"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label">Page Name&nbsp;<span class="required">*</span></label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" class="form-control input-sm border-primary req" id="txtPageName" runat="server" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label">Module</label>
                                                <span class="required">*</span>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <select class="form-control input-sm border-primary req" id="module" runat="server">
                                                    <option value="0">Select Module</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label">Status</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <select class="form-control input-sm border-primary" id="status" runat="server">
                                                    <option value="1">Active</option>
                                                    <option value="0">Inactive</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <br />

                            </div>
                            <input type="hidden" id="hiddenAutoId" value="" />
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="btn-group mr-1 pull-right">
                                            <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="savePageBtn">Save</button>
                                        </div>
                                        <div class="btn-group mr-1 pull-right">
                                            <button type="button" class="btn btn-secondary  buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" onclick="clearField()" id="btnReset">Reset</button>
                                        </div>
                                        <div class="btn-group mr-1 pull-right">
                                            <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnUpdate" style="display: none">Update</button>
                                        </div>
                                        <div class="btn-group mr-1 pull-right">
                                            <a href="/admin/ManagePage.aspx">
                                                <button type="button" class="btn btn-warning buttonAnimation pull-right round box-shadow-1 btn-sm" id="btnCancel" data-animation="pulse" style="display: none">Cancel</button></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="row actionDiv" style="display: none;">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Page Action</h4>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="control-label">Action</label>
                                        <span class="required">*</span>
                                    </div>
                                    <div class="col-md-8 form-group">
                                        <input type="text" class="form-control input-sm border-primary actReq" id="ActionName" runat="server" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="control-label">Type</label>
                                    </div>
                                    <input type="hidden" id="hiddenActionAutoId" />
                                    <div class="col-md-8 form-group">
                                        <select class="form-control input-sm border-primary" id="ActionType" runat="server" onchange="getUserRole(this.value);">
                                            <option value="1">Generic</option>
                                            <option value="0">Role Based</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row userRole act" style="display: none;">
                                    <div class="col-md-4">
                                        <label class="control-label">Role</label>
                                    </div>
                                    <div class="col-md-8 form-group">
                                        <select class="form-control input-sm border-primary" id="UserRole" runat="server">
                                            <option value="0">Select Role</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="btn-group mr-1 pull-right">
                                            <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="saveActionBtn">Save</button>
                                        </div>
                                        <div class="btn-group mr-1 pull-right">
                                            <button type="button" class="btn btn-secondary  buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" onclick="clearActionField()" id="btnActReset">Reset</button>
                                        </div>
                                        <div class="btn-group mr-1 pull-right">
                                            <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnActUpdate" style="display: none">Update</button>
                                        </div>
                                        <div class="btn-group mr-1 pull-right">
                                            <button type="button" class="btn btn-warning buttonAnimation pull-right round box-shadow-1 btn-sm" id="btnActCancel" data-animation="pulse" onclick="clearActionField()" style="display: none">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered" id="actionList">
                                        <thead class="bg-blue white">
                                            <tr>
                                                <td class="action text-center">Action</td>
                                                <td class="AutoId" style="display: none">AutoId</td>
                                                <td class="HiddenType" style="display: none">Type</td>
                                                <td class="HiddenAssignRole" style="display: none">Role</td>
                                                <td class="ActionName">Action Name</td>
                                                <td class="AssignRole">Assigned Role</td>
                                                <td class="Type">Action Type</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                    <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="display: none;">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Page List</h4>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-3 form-group">
                                                    <input type="text" class="form-control border-primary input-sm" placeholder="Page ID" id="txtSearchPageId" />
                                                </div>
                                                <div class="col-md-3 form-group">
                                                    <input type="text" class="form-control border-primary input-sm" placeholder="Page Name" id="txtSearchPageName" />
                                                </div>
                                                <div class="col-md-3 form-group">
                                                    <select class="form-control input-sm" id="searchStatus">
                                                        <option value="2">All</option>
                                                        <option value="1">Active</option>
                                                        <option value="0">Inactive</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3 form-group">
                                                    <button type="button" class="btn btn-info buttonAnimation pull-left round box-shadow-1 btn-sm" onclick="getPageList();">Search</button>
                                                </div>
                                            </div>

                                            <br />
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered" id="pageList">
                                                    <thead class="bg-blue white">
                                                        <tr>
                                                            <td class="action text-center">Action</td>
                                                            <td class="AutoId" style="display: none">AutoId</td>
                                                            <td class="Role" style="display: none">Role</td>
                                                            <td class="HiddenType" style="display: none">Type</td>
                                                            <td class="HiddenStatus" style="display: none">Status</td>
                                                            <td class="PageId text-center">Page ID</td>
                                                            <td class="PageName">Page Name</td>
                                                            <td class="PageUrl">Page Url</td>
                                                            <td class="Type">Type</td>
                                                            <td class="UserType">Role</td>
                                                            <td class="Description">Description</td>
                                                            <td class="ActionName">Action Name</td>
                                                            <td class="Status text-center">Status</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                                <h5 class="well text-center" id="EmptyTable1" style="display: none">No data available.</h5>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="/Admin/JS/ManagePage.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
</asp:Content>
