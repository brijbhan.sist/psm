﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true"
    CodeFile="ManagePageAccess.aspx.cs" Inherits="Admin_ManagePageAccess" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        #tblBrand_wrapper {
            padding: 0;
        }

        .desc {
            margin-bottom: 7px;
        }

        .addActionBtn {
            cursor: pointer;
        }

        .hideAutoId {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Manage Page Access</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage Page</a></li>
                        <li class="breadcrumb-item"><a href="#">Assign Module</a></li>
                        <li class="breadcrumb-item">Manage Page Access</li>
                    </ol>
                </div>
            </div>
        </div>
        <%--<div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a href="/Admin/ManagePageAccess.aspx"><button type="button" class="dropdown-item">Manage Page Access</button></a>
                </div>
            </div>
        </div>--%>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Page Access Details</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row" id="panelPageMaster">
                                    <div class="col-md-6">

                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label">Module</label>
                                                <span class="required">*</span>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <select class="form-control input-sm border-primary req" id="Module" runat="server" onchange="getUserType()">
                                                    <option value="0">Select</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label">Role</label>
                                                <span class="required">*</span>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <select class="form-control input-sm border-primary req" id="UserRole" runat="server" onchange="getPage()">
                                                    <option value="0">Select</option>
                                                </select>
                                            </div>
                                        </div>


                                    </div>
                                    <input type="hidden" value="" id="HiddenAutoId" />
                                    <div class="col-md-6 pgliost">

                                        <div class="row">
                                            <div class="col-md-12 form-group pageListData">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <div class="card-footer">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="btn-group mr-1 pull-right">
                                                <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="saveBtn">Assign</button>
                                            </div>
                                            <div class="btn-group mr-1 pull-right">
                                                <button type="button" class="btn btn-secondary  buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" onclick="clearField()" id="btnReset">Reset</button>
                                            </div>
                                            <div class="btn-group mr-1 pull-right">
                                                <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnUpdate" style="display: none">Update</button>
                                            </div>
                                            <div class="btn-group mr-1 pull-right">
                                                <button type="button" class="btn btn-warning buttonAnimation pull-right round box-shadow-1 btn-sm" id="btnCancel" data-animation="pulse" style="display: none">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Page Access List</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4 form-group">
                                        <select class="form-control input-sm" id="searchRole">
                                            <option value="0">All Role</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <select class="form-control input-sm" id="searchModule">
                                            <option value="0">All Module</option>
                                        </select>
                                    </div>
                                   
                                    <div class="col-md-3 form-group">
                                        <button type="button" class="btn btn-info buttonAnimation pull-left round box-shadow-1 btn-sm" onclick="getSavedAccessPageList(1);">Search</button>
                                    </div>
                                </div>

                                <br />
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered" id="AccessPageList">
                                        <thead class="bg-blue white">
                                            <tr>
                                                <td class="action text-center">Action</td>
                                                <td class="AutoId" style="display: none">AutoId</td>
                                                <td class="HiddenStatus" style="display: none">Status</td>
                                                <td class="HiddenModule" style="display: none">Module</td>
                                                <td class="HiddenPage" style="display: none">Page</td>
                                                <td class="HiddenRole" style="display: none">Role</td>
                                                <td class="HiddenAction" style="display: none">Action</td>
                                                <td class="Module text-center">Module</td>
                                                <td class="PageName">Page Name</td>
                                                <td class="UserRole">User Type</td>
                                                <td class="Status text-center">Status</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                    <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                </div>
                                 <br />
                                <div class="row container-fluid">
                                    <div>
                                        <select id="ddlPageSize" class="form-control input-sm border-primary" onchange="getSavedAccessPageList(1);">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="/Admin/JS/ManagePageAccess.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
</asp:Content>
