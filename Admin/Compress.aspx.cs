﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using Newtonsoft.Json;

using System.Drawing;
using System.Drawing.Drawing2D;
using System.Web.Script.Serialization;
using DllProduct;
using DllUtility;
using System.Data.SqlClient;

public partial class Admin_Compress : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        PL_Product pobj = new PL_Product();
        try
        {

            DataSet Ds = getThumbData(pobj);
            if (Ds.Tables[0].Rows.Count > 0)
            {
                int i = 0;
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    var AutoId = Convert.ToInt32(dr["AutoId"]);
                    var ImageUrl = dr["ImageUrl"].ToString();
                    string dirFullPath = Server.MapPath("/Attachments/");

                    string fnamefull = dirFullPath + ImageUrl;

                    if (File.Exists(fnamefull))
                    {
                        string returnurThumnailUrl100 = "", returnurThumnailUrl400 = "";
                        if (!File.Exists(Server.MapPath(dr["ThumbnailImageUrl"].ToString())))
                        {
                            returnurThumnailUrl100 = CreateThumbnail(100, 100, fnamefull, "productThumbnailImage");
                        }
                        if (!File.Exists(Server.MapPath(dr["ThirdImage"].ToString())))
                        {
                            returnurThumnailUrl400 = CreateThumbnail(400, 400, fnamefull, "productThumbnailImage");
                        }
                        if (returnurThumnailUrl100 != "" && returnurThumnailUrl400 != "")
                            i += updateDataBaseTable(AutoId, returnurThumnailUrl100, returnurThumnailUrl400);
                    }
                    lblmsg.Text = AutoId.ToString() + i.ToString();
                }

            }
            else
            {
                return;
            }
        }
        catch (Exception ex)
        {
            lblmsg.Text = ex.Message;
        }
    }
    public string CreateThumbnail(int maxWidth, int maxHeight, string path, string folderName)
    {

        try
        {
            var image = System.Drawing.Image.FromFile(path);
            var ratioX = (double)maxWidth / image.Width;
            var ratioY = (double)maxHeight / image.Height;
            var ratio = Math.Min(ratioX, ratioY);
            var newWidth = (int)(image.Width * ratio);
            var newHeight = (int)(image.Height * ratio);
            var newImage = new Bitmap(newWidth, newHeight);
            Graphics thumbGraph = Graphics.FromImage(newImage);
            thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
            thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
            thumbGraph.DrawImage(image, 0, 0, newWidth, newHeight);
            image.Dispose();
            string fileRelativePath = "/" + folderName + "/" + maxWidth + "_" + maxWidth + "_Thumbnail_" + Path.GetFileName(path);
            newImage.Save(Server.MapPath(fileRelativePath), newImage.RawFormat);
            return fileRelativePath;
        }
        catch (Exception)
        {

            return "";
        }
    }


    public int updateDataBaseTable(int AutoId, string returnurThumnailUrl100, string returnurThumnailUrl400)
    {
        PL_Product pobj = new PL_Product();
        pobj.AutoId = Convert.ToInt32(AutoId);
        pobj.ThumbnailImageUrl = returnurThumnailUrl100;
        pobj.FourImageUrl = returnurThumnailUrl400;
        Config connect = new Config();
        SqlCommand sqlCmd = new SqlCommand("ProcProductMaster", connect.con);
        sqlCmd.CommandType = CommandType.StoredProcedure;
        sqlCmd.Parameters.AddWithValue("@Opcode", 901);
        sqlCmd.Parameters.AddWithValue("@ThumbnailImageUrl", pobj.ThumbnailImageUrl);
        sqlCmd.Parameters.AddWithValue("@FourImageUrl", pobj.FourImageUrl);
        sqlCmd.Parameters.AddWithValue("@AutoId", pobj.AutoId);
        sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
        sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
        sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
        sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
        SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
        pobj.Ds = new DataSet();
        sqlAdp.Fill(pobj.Ds);
        pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
        pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
        if (!pobj.isException)
        {
            return pobj.AutoId;
        }
        else
        {
            return 0;
        }
    }
    public DataSet getThumbData(PL_Product pobj)
    {
        Config connect = new Config();
        SqlCommand sqlCmd = new SqlCommand("ProcProductMaster", connect.con);
        sqlCmd.CommandType = CommandType.StoredProcedure;
        sqlCmd.Parameters.AddWithValue("@Opcode", 900);
        sqlCmd.Parameters.AddWithValue("@ThumbnailImageUrl", pobj.ThumbnailImageUrl);
        sqlCmd.Parameters.AddWithValue("@FourImageUrl", pobj.FourImageUrl);
        sqlCmd.Parameters.AddWithValue("@AutoId", pobj.AutoId);
        sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
        sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
        sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
        sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
        SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
        pobj.Ds = new DataSet();
        sqlAdp.Fill(pobj.Ds);
        return pobj.Ds;
    }


}