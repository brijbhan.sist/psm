﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="UpdateStock.aspx.cs" Inherits="Admin_UpdateStock" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Manage Stock</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage Inventory</a></li>
                        <li class="breadcrumb-item">Update Stock</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area">

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Stock Details </h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Barcode</label>

                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                                                <input type="text" id="txtBarCode" class="form-control border-primary input-sm" onchange="readBarcode()" onkeypress='return isNumberKey(event)' />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Product <span class="required">&nbsp;*</span></label>

                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                                                <select id="ddlStock" runat="server" class="form-control border-primary input-sm ddlreq" style="width: 100%;" onchange="Stockchange();">
                                                    <option selected="selected" value="0">-Select Product-</option>
                                                </select>

                                            </div>
                                        </div>
                                    </div>
                                   
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Available in Stock</label>
                                            </div>

                                            <div class="col-md-8 col-sm-8 col-xs-12 form-group">

                                                <input type="text" id="txtProductQty" class="form-control border-primary input-sm" runat="server" readonly="true" />

                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row form-group" style="overflow: hidden">
                                    <div class="col-md-9">
                                        <div id="divUnitType">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <img id="imgProductImage" src="#" style="width: 200px; height: 200px; display: none" />
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            
                                        </div>
                                    </div>
                                    <div class="col-md-6">

                                        <div class="btn-group mr-1 pull-right">
                                            <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnUpdate2">Update</button>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>

            <div class="row" id="UpStocklists">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Stock Update Log List</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row" id="ProcutStockUpdate" display="none">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblProcutStockUpdateLog">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="Date text-center">Date / Time</td>
                                                        <td class="ProductId text-center">Product ID</td>
                                                        <td class="ProductName">Product Name</td>
                                                        <td class="OldStock text-center">Old Stock</td>
                                                        <td class="NewStock text-center">New Stock</td>
                                                        <td class="UserAutoId">Updated By</td>
                                                        <td class="Remark">Remark</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex mt-1">
                                    <div class="form-group">
                                        <select class="form-control input-sm border-primary" id="PageSize" style="display: none">
                                            <option selected="selected" value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="ml-auto">
                                        <div class="Pager"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>



    <div class="modal fade" id="BarCodenotExists" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Message</h4>
                </div>
                <div class="modal-body">
                    <div style="border-color: #ac2925; border: 1px solid; color: #ac2925; min-height: 45px; line-height: 20px; padding: 9px;">
                        <strong id="barcodemsg"></strong>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="UpdateStockPopup" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Message</h4>
                </div>

                <div class="modal-body">
                    <div style="border-color: #ac2925; border: 1px solid; color: #ac2925; min-height: 45px; line-height: 20px; padding: 9px;">
                        <strong id="UpdateStockMsg"></strong>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="JS/UpdateStock.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
</asp:Content>

