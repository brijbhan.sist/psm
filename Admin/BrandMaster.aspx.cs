﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_BrandMaster : System.Web.UI.Page
{
 
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
            string text = File.ReadAllText(Server.MapPath("/Admin/JS/BrandMaster.js"));
            Page.Header.Controls.Add(
                new LiteralControl(
                    "<script id='checksdrivRequiredFields'>" + text + "</script>"
                    ));
        }

    }
}