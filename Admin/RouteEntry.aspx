﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="RouteEntry.aspx.cs" Inherits="RouteEntry" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <style>
    .table th, .table td {
     padding:3px !important
}
    .table tbody td {
    padding: 3px !important;
    vertical-align: middle !important;
    text-align: center !important;
}
      </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Manage Route </h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Application</a></li>
                         <li class="breadcrumb-item"><a href="#">Manage Route</a></li>
                         <li class="breadcrumb-item" id="liPageTitle">New Route</li>

                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
          <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
            <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
            id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false"> Action</button>
            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" onclick="location.href='/Admin/RouteEntry.aspx'" class="dropdown-item"> Add New Route</button>
                    <button type="button" onclick="location.href='/Admin/RouteList.aspx'" class="dropdown-item"> Go to Route List</button>
                 <input type="hidden" runat="server" id="hdEmpType" />
                <input type="hidden" id="lblrouteid" class="hide"/>
            </div>
          </div>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="card">
                        <div class="card-header" style="padding: 16px !important;">
                            <h4 class="card-title">Route Details</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <label class="control-label">Route ID</label>
                                    </div>
                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                        <input type="text" class="form-control border-primary input-sm" id="txtRouteId" readonly="readonly" />
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <label class="control-label">
                                            Route Name <span class="required">*</span>
                                        </label>
                                    </div>
                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                        <input type="text" class="form-control border-primary input-sm req" id="txtRouteName" runat="server" />
                                    </div>
                                </div>
                                <div class="row form-group" runat="server" visible="true" id="SalesPerson">
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <label class="control-label">
                                            Sales Person <span class="required">*</span>
                                        </label>
                                    </div>
                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                        <select class="form-control border-primary input-sm ddlreq" onchange="SalesPersonChange()" id="ddlSalePerson" runat="server">
                                            <option value="0">-Select Sales Person-</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <label class="control-label">Status</label>
                                    </div>
                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                        <select class="form-control border-primary input-sm" id="ddlStatus">
                                            <option value="1" selected="selected">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="btn-group mr-1 pull-right">
                                            <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnSave">Save</button>
                                        </div>
                                        <div class="btn-group mr-1 pull-right">
                                            <button type="button" class="btn btn-secondary  buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnReset">Reset</button>
                                        </div>
                                        <div class="btn-group mr-1 pull-right">
                                            <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnUpdate" style="display: none">Update</button>
                                        </div>
                                        <div class="btn-group mr-1 pull-right">
                                            <button type="button" class="btn btn-warning buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnCancel" style="display: none
">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="card">
                        <div class="card-header" style="padding: 16px !important;">
                            <h4 class="card-title">Manage Route Customer</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <select class="form-control border-primary input-sm" id="ddlSalesPersonSearch" runat="server">
                                            <option value="0">All Customer</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1  btn-sm" id="btnSearch" data-animation="pulse">Search</button>
                                    </div>
                                </div>
                                <br /><%--style="overflow: scroll; height: 580px; overflow-x: hidden; "--%>
                                <div class="table-responsive" >
                                    <table class="table table-striped table-bordered" id="tblRouteCustomer">
                                        <thead class="bg-blue white">
                                            <tr>
                                                <td class="SelCheckbox">Action
                                                </td>
                                                <td class="Id"  style="display: none;width:50px !important;text-align:center !important">ID</td>
                                                <td class="Customer">Customer Name</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                    <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="JS/RouteMaster.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
</asp:Content>

