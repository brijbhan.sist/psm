﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DLLIPAddressMaster;
public partial class Admin_IPMaster : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
            string text = File.ReadAllText(Server.MapPath("/Admin/JS/IPMaster.js"));
            Page.Header.Controls.Add(
                new LiteralControl(
                    "<script id='checksdrivRequiredFields'>" + text + "</script>"
                    ));
        }
    }

    [WebMethod(EnableSession = true)]
    public static string Insert(string dataValue)
    {
        PL_IPAddressMaster pobj = new PL_IPAddressMaster();
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                pobj.userIPAddress = jdv["IPAddress"];
                pobj.NameofLocation = jdv["NameofLocation"];
                pobj.Description = jdv["Description"];
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"].ToString());
                BL_IPAddressMaster.insert(pobj);
                if (!pobj.isException)
                {
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    


    [WebMethod(EnableSession = true)]
    public static string bindList(string dataValue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_IPAddressMaster pobj = new PL_IPAddressMaster();
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                BL_IPAddressMaster.bindlist(pobj);

                return pobj.Ds.GetXml();
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string edit(string AutoId)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_IPAddressMaster pobj = new PL_IPAddressMaster();
            pobj.AutoId = Convert.ToInt32(AutoId);
            BL_IPAddressMaster.select(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string update(string dataValue)
    {
        PL_IPAddressMaster pobj = new PL_IPAddressMaster();
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                pobj.AutoId = Convert.ToInt32(jdv["AutoId"]);
                pobj.userIPAddress = jdv["IPAddress"];
                pobj.NameofLocation = jdv["NameofLocation"];
                pobj.Description = jdv["Description"];
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"].ToString());
                BL_IPAddressMaster.update(pobj);
                if (!pobj.isException)
                {
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string delete(string AutoId)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {

                PL_IPAddressMaster pobj = new PL_IPAddressMaster();
                pobj.AutoId = Convert.ToInt32(AutoId);
                BL_IPAddressMaster.delete(pobj);
                if (!pobj.isException)
                {
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}