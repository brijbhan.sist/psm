﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="productMaster.aspx.cs" Inherits="Admin_productMaster" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .disp {
            display: none;
        }

        #Table1 tr th {
            vertical-align: middle;
            text-align: center
        }

        #Table1 th, .table td {
            padding: 5px 5px !important;
        }

        #Table1 tbody td {
            padding: 5px 5px !important;
            vertical-align: middle !important;
        }

        .tiles {
            counter-reset: cool -1 good -1;
            padding: 12px 0;
            position: sticky;
        }

            .tiles > div {
                margin: 3px;
                padding: 2px;
            }

            .tiles > .col-xs-2 {
                width: 19%;
                box-shadow: 0px 1px 4px 0px;
                height: auto;
            }

            .tiles > .col-xs-4 {
                width: 40%;
            }

            .tiles > div:nth-of-type(3) > div {
                margin-bottom: -100%;
            }

            .tiles > div:nth-of-type(7) {
                margin-left: 40%;
            }

            .tiles > div:nth-of-type(8),
            .tiles > div:nth-of-type(22) {
                clear: both;
            }

            .tiles > div:nth-of-type(19) {
                clear: left;
            }

            .tiles > div > div {
                position: relative;
            }

                .tiles > div > div:before {
                    color: #fff;
                    font-size: 28px;
                    font-weight: bold;
                    left: 8px;
                    position: absolute;
                    top: 2px;
                }

            /*.tiles > div.col-xs-2 > div:before {
                content: counter(good);
                counter-increment: good;
            }

            .tiles > div.col-xs-4 > div:before {
                content: counter(cool);
                counter-increment: cool;
                font-size: 56px;
            }*/

            .tiles img {
                display: block;
                /*max-height: 116px;*/
                width: 100%;
                cursor: pointer;
            }



        .imageThumb {
            max-height: 75px;
            border: 0.5px solid blue;
            padding: 17px;
            cursor: pointer;
            width: 154px;
        }

        .pip {
            display: inline-block;
            margin: 10px 10px 0 0;
        }

        .remove {
            display: block;
            background: #444;
            border: 1px solid black;
            color: white;
            text-align: center;
            cursor: pointer;
        }

            .remove:hover {
                background: white;
                color: black;
            }

        .defaultImage {
            border: 1px solid red;
        }

        i.la.la-times {
            position: absolute;
            top: 17px;
            cursor: pointer;
            background: #333;
            color: #fff;
            font-size: 16px;
        }
        select.form-control:not([size]):not([multiple]).input-sm{
            padding: 2px !important;
        }
        input.form-control.input-sm {
            padding: 5px !important;
        }
        /*.form-control {
           padding: 3px !important;
        }*/
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Manage Product</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage Inventory</a></li>
                        <li class="breadcrumb-item active">Manage Product</li>
                        <li class="breadcrumb-item active"><a href="#" onclick="GetPageInformation(10003)"><i class="la la-question-circle"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a class="dropdown-item" href="/Admin/productMaster.aspx" id="addProduct" runat="server">Add New Product</a>
                    <a class="dropdown-item" href="productList.aspx" id="A3">Go to Product List</a>
                    <input type="hidden" id="HDDomain" runat="server" />
                    <%--<a class="dropdown-item" href="/Admin/productMaster.aspx" id="NextProduct" runat="server">Next Product</a>--%>

                    <%--<a class="dropdown-item" href="#" id="PreviousProduct" runat="server">Previous Product</a>--%>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Product Details
                            <span id="next" style="float: right; display: none "><a href="#" id="PreviousProduct" class="btn btn-info buttonAnimation round box-shadow-1 btn-sm">Previous</a> &nbsp; &nbsp; 
                            <a href="#" id="NextProduct" class="btn btn-info buttonAnimation pull-right round box-shadow-1 btn-sm">Next</a></span></h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <input type="hidden" id="CheckimpType" runat="server" />
                                <input type="hidden" id="hiddenText" runat="server" />
                                <div class="row">
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <div class="col-md-5 col-sm-5 col-xs-12">
                                                        <label class="control-label">Category</label>
                                                        <span class="required">*</span>
                                                    </div>
                                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                                        <select class="form-control border-primary input-sm ddlreq" id="ddlCategory" onchange="bindSubcategory()" runat="server">
                                                        </select>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                                        <label class="control-label">Subcategory</label>
                                                        <span class="required">*</span>
                                                    </div>
                                                    <div class="col-md-7 col-sm-7 col-xs-12 form-group">
                                                        <select class="form-control border-primary input-sm ddlreq" id="ddlSubcategory" runat="server" disabled="disabled">
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <div class="col-md-5 col-sm-5 col-xs-12">
                                                        <label class="control-label">Product ID</label>
                                                        <span class="required">*</span>
                                                    </div>
                                                    <input type="hidden" id="txtHProductAutoId" />
                                                    <div class="col-md-7 col-sm-7 col-xs-12 form-group">
                                                        <input type="text" class="form-control border-primary input-sm req" maxlength="8" id="txtProductId" runat="server" onkeypress='return isNumberKey(event)' />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                                        <label class="control-label">Product Name</label>
                                                        <span class="required">*</span>
                                                    </div>
                                                    <div class="col-md-7 col-sm-7 col-xs-12 form-group">
                                                        <input type="text" class="form-control border-primary input-sm req" maxlength="50" id="txtProductName" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <div class="col-md-5 col-sm-5 col-xs-12">
                                                        <label class="control-label">Prefered Vendor</label>
                                                    </div>
                                                    <div class="col-md-7 col-sm-7 col-xs-12 form-group">
                                                        <select class="form-control border-primary input-sm" id="ddlVendor" runat="server">
                                                            <option value="0">Select Vendor</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                                        <label class="control-label">Product Image</label>
                                                        <span class="required">*</span>
                                                    </div>
                                                    <div class="col-md-7 col-sm-7 col-xs-12 form-group">
                                                        <input type="file" name="img" style="overflow:hidden;padding: 3px !important;" accept="img/*" id="fileImageUrl" class="form-control border-primary" onchange='readURL(this)' runat="server" />
                                                        <i>Recommended image ratio is 1:1</i>
                                                        <%--onerror="this.onerror=null;this.src='/images/default_pic.png';"
                                                            OnError='this.src =\"https://image.mypsm.net/Attachments/default_pic.png\"'--%>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <div class="col-md-5 col-sm-5 col-xs-12">
                                                        <label class="control-label">Brand</label>
                                                        <span class="required">*</span>
                                                    </div>
                                                    <div class="col-md-7 col-sm-7 col-xs-12 form-group">
                                                        <select class="form-control border-primary input-sm req" id="ddlBrand">
                                                            <option value="0">Choose an Brand</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                                        <label class="control-label" id="reorder_text">Re Order Mark (Pieces)</label>
                                                        <label class="control-label" id="reQty" style="display:none"></label>
                                                    </div>
                                                    <div class="col-md-7 col-sm-7 col-xs-12 form-group">
                                                        <input type="text" class="form-control border-primary input-sm" maxlength="6" id="txtReOrderMark" runat="server" onkeypress="return isNumberKey(event)" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <div class="col-md-5 col-sm-5 col-xs-12">
                                                        <label class="control-label">Is Apply ML Quantity</label>
                                                    </div>
                                                    <div class="col-md-7 col-sm-7 col-xs-12 form-group">
                                                        <input type="checkbox" id="cbMlQuantity" runat="server" /> <i>If checked then charge ML Tax based on Billing Address</i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                                        <label class="control-label">ML Quantity</label>
                                                    </div>
                                                    <div class="col-md-7 col-sm-7 col-xs-12 form-group">
                                                        <input type="text" class="form-control border-primary input-sm" maxlength="5" id="txtMLQty" value="0.00" runat="server" onkeypress="return isNumberDecimalKey(event,this)" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <div class="col-md-5 col-sm-5 col-xs-12">
                                                        <label class="control-label">Is Apply Weight (OZ)</label>
                                                    </div>
                                                    <div class="col-md-7 col-sm-7 col-xs-12 form-group">
                                                        <input type="checkbox" id="CbWeightQuantity" runat="server" /> <i>If checked then charge Weight (Oz) Tax based on Billing Address</i>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                                        <label class="control-label">Weight (Oz)</label>
                                                    </div>
                                                    <div class="col-md-7 col-sm-7 col-xs-12 form-group">
                                                        <input type="text" class="form-control border-primary input-sm" maxlength="5" id="txtWeightOz" value="0.00" runat="server" onkeypress="return isNumberDecimalKey(event,this)" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="row">

                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <div class="col-md-5 col-sm-5 col-xs-12">
                                                        <label class="control-label">
                                                            Commission Code                                                            
                                                        </label>
                                                        <span class="required">*</span>
                                                    </div>
                                                    <div class="col-md-7 col-sm-7 col-xs-12 form-group">
                                                        <select id="ddlCommCode" onchange="IsNumeric()" class="form-control border-primary input-sm"></select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                                        <label class="control-label">Stock(Pieces)</label>
                                                    </div>
                                                    <div class="col-md-7 col-sm-7 col-xs-12 form-group">
                                                        <input type="text" class="form-control border-primary input-sm" id="txtStock" runat="server" readonly="readonly" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" id="divSRP">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <div class="col-md-5 col-sm-5 col-xs-12">
                                                        <label class="control-label">
                                                            SRP<sub>(Suggested Retail Price)</sub><span class="required">&nbsp*</span>
                                                        </label>

                                                    </div>
                                                    <div class="col-md-7 col-sm-7 col-xs-12 form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                                    <span>$</span>
                                                                </span>
                                                            </div>
                                                            <input type="text" class="form-control border-primary input-sm req" maxlength="5" id="txtSRP" style="text-align: right; white-space: nowrap" runat="server" onkeypress='return isNumberDecimalKey(event,this)' placeholder="0.00" onfocus="this.select()" value="0.00" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                             <div class="col-md-6" style="display:none" id="divdefaultstock">
                                                <div class="form-group row">
                                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                                        <label class="control-label">Default Stock in <br />(<label id="lbldefaultunit"></label>)</label>
                                                    </div>
                                                    <div class="col-md-7 col-sm-7 col-xs-12 form-group">
                                                        <input type="text" class="form-control border-primary input-sm" id="TxtdefaultunitStock" runat="server" readonly="readonly" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-2">
                                        <img id="imgProductImage" src="/images/default_pic.png"  onerror="this.src='/images/default_pic.png'" class="imagePreview form-control border-primary input-sm" width="170" />
                                        <br /> 
                                       <i> Allowed file: <b style="color:red">[png,jpg,jpeg]</b></i>   <br /> 
                                       <i> Max size is: <b style="color:red"><%=System.Configuration.ConfigurationManager.AppSettings["ImageValidation"]%>MB</b></i>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <div class="col-md-2 col-sm-2 col-xs-12">
                                                <label class="control-label">Location Wise Status</label>
                                                <span class="required">*</span>
                                            </div>
                                            <div class="col-md-10 col-sm-10 col-xs-12" id="bindcheckbox">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnSave" onclick="Saveproduct()">Save</button>
                                    </div>
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" class="btn btn-secondary  buttonAnimation pull-right round box-shadow-1  btn-sm" onclick="resetButton()" data-animation="pulse" id="btnReset1">Reset</button>
                                    </div>

                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnUpdate1" onclick="UpdateProduct()" style="margin-top: 10px; display: none">Update</button>
                                    </div>
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" class="btn btn-warning buttonAnimation pull-right round box-shadow-1 btn-sm" id="btnCancel" data-animation="pulse" style="display: none">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body" id="PackingDetails" style="display: none;">
                <section>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-3 col-sm-6">
                                            <h4 class="card-title">Packing Details</h4>
                                        </div>
                                        <div class="col-lg-9 col-md-9 col-sm-6 text-right">
                                            <button type="button" class="btn btn-secondary buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnManagePrince" style="margin-left: 10px;" onclick="managePrice()">Manage Price</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="card-content collapse show">

                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered" id="tblAddedPacking">
                                                        <thead class="bg-blue white">
                                                            <tr>
                                                                <td class="UnitType">Unit Type</td>
                                                                <td class="Qty text-center">Quantity</td>
                                                                <td class="CostPrice price">Cost Price</td>
                                                                <td class="WMinPrice price">Wh. Min. Price</td>
                                                                <td class="MinPrice price">Retail Min Price</td>
                                                                <td class="Price price">Base Price</td>
                                                                <td class="Location text-center">Location</td>
                                                                <td class="BarCode text-center">Barcode</td>
                                                                <td class="Free text-center">Free</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                    <h5 class="well text-center" id="emptyTable" style="display: none">No data available.</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <input type="hidden" value="" id="boxId">
            <div class="row" id="webImagesitedtls" style="display: none;">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Website Details</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a><i class="ft-plus" id="ftclass" onclick="showSecurityModal(1)"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content" id="websiteContent" style="display: none;">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-12">

                                                <div class="card-content show">
                                                    <div class="card-body">
                                                        <div class="row">
                                                            <div class="col-md-10">
                                                                <div class="form-group row">
                                                                    <div class="col-md-2 col-sm-2 col-xs-2">
                                                                        <label class="control-label">Show on website</label>
                                                                    </div>
                                                                    <div class="col-md-8 col-sm-8 col-xs-8 form-group">
                                                                        <input type="checkbox" id="IsShowOnWebsite1" runat="server" onclick="showConfirmBox()" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-10">
                                                                <div class="form-group row">
                                                                    <div class="col-md-2 col-sm-2 col-xs-2">
                                                                        <label class="control-label">Out Of Stock</label>
                                                                    </div>
                                                                    <div class="col-md-8 col-sm-8 col-xs-8 form-group">
                                                                        <input type="checkbox" id="CheckboxOOS" runat="server" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-10">
                                                                <div class="row">
                                                                    <label class="col-lg-2 col-sm-2 form-group">
                                                                        Description
                                                                    </label>
                                                                    <div class="col-lg-10  col-sm-10 form-group">
                                                                        <textarea id="txtDescription" class="form-control border-primary input-sm"></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <div class="row" id="btnWebsiteDetails">
                                            <div class="col-md-12">
                                                <div class="btn-group mr-1 pull-right">
                                                    <button type="button" id="webUpdate" class="btn btn-primary buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" onclick="updateProductDetail()">Update</button>
                                                </div>
                                                <div class="btn-group mr-1 pull-right">
                                                    <button type="button" class="btn btn-warning buttonAnimation pull-right round box-shadow-1 btn-sm" id="btnWebCancel" data-animation="pulse" style="display: none">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" id="webImageSection">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Image Details</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a><i class="ft-plus" id="ftclass1" onclick="showSecurityModal(2)"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content" id="imageContent" style="display: none;">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-12">

                                                <div class="card-content collapse show">
                                                    <div class="card-body">
                                                        <div class="row">
                                                            <div class="col-md-10">
                                                                <div class="form-group row" id="Imagesec">
                                                                    <div class="col-md-2 col-sm-2 col-xs-12">
                                                                        <label class="control-label">Image <span class="required">*</span></label>
                                                                    </div>
                                                                    <div class="col-md-10 col-sm-10 col-xs-12 form-group">
                                                                        <input type="file" id="file1"  style="overflow:hidden;" name="files[]" accept="img/*" class="form-control border-primary" runat="server" multiple onchange='readWebURL(this)' />
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <div class="col-md-2 col-sm-2 col-xs-12">
                                                                        <input type="hidden" value="" id="defaultImageName" />
                                                                        
                                                                    </div>
                                                                    <div class="col-md-10 col-sm-10 col-xs-12 form-group imgbox">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <div class="col-md-2 col-sm-2 col-xs-12">
                                                                    </div>
                                                                    <div class="col-md-10 col-sm-10 col-xs-12 form-group">
                                                                        <b>Note <span class="required">*</span></b> : <i>Highlighted image is default image</i><br />
                                                                         <i style="margin-left:50px">: Allowed file type :<b style="color:red">[png,jpg,jpeg]</b></i><br />
                                                                         <i style="margin-left:50px">: Recommended image ratio is 1:1</i>  <br /> 
                                                                         <i style="margin-left:50px">: Max size is :<b style="color:red"><%=System.Configuration.ConfigurationManager.AppSettings["ImageValidation"]%> MB</b></i>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <div class="row" id="btnImageDetails">
                                            <div class="col-md-12">

                                                <div class="btn-group mr-1 pull-right">
                                                    <button type="button" id="imgUpdate" class="btn btn-primary buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" onclick="updateProductImage()">Update</button>
                                                </div>
                                                <div class="btn-group mr-1 pull-right">
                                                    <button type="button" class="btn btn-warning buttonAnimation pull-right round box-shadow-1 btn-sm" id="btnImageCancel" data-animation="pulse" style="display: none">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>






    <div id="managePrice" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-md" style="min-width: 1250px">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 style="float: left" class="modal-title">Manage Price <span id="ProductIDNName" style="font-size: 14px; font-weight: bold"></span></h4> 
                </div>

                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered" id="Table1">
                                    <thead class="bg-blue white">
                                        <tr>
                                            <th rowspan="2" class="Action text-center" style="width: 10%; display: none">Create</th>
                                            <th rowspan="2" class="UnitType" style="width: 10%; text-align: center">Unit<br />
                                                Type</th>
                                         <%--   <th rowspan="2" class="Status text-center" style="width: 10%;">Status<br />
                                                Active?</th>--%>
                                            <th rowspan="2" class="Default text-center" style="width: 10%; display: none">Default</th>
                                            <th rowspan="2" class="Free text-center" style="width: 10%; display: none">Free ?</th>
                                            <th id="fqty" style="width: 10%" colspan="2">Qty</th>
                                            <th style="width: 10%" colspan="2">Cost Price</th>
                                            <th style="width: 10%" colspan="2">WH. Min Price	</th>
                                            <th style="width: 10%" colspan="2">Min Retail Price</th>
                                            <th style="width: 10%" colspan="2">Base Price</th>
                                            <th style="width: 30%" rowspan="2">Location<br />[Rack-Section-Row-Box No]<br />[A- 01 - 01 - 101]</th>
                                        </tr>
                                        <tr>
                                            <th id="sqty" class="Qty text-center" style="width: 5%;">Old </th>
                                            <th id="snqty" class="NewQty price" style="width: 5%;">New </th>
                                            <th class="CostPrice price" style="width: 5%;">Old </th>
                                            <th class="NewCosePrice" style="width: 5%;">New </th>
                                            <th class="WHminPrice price" style="width: 5%;">Old </th>
                                            <th class="NWHPrice" style="width: 5%;">New </th>
                                            <th class="RetailPrice price" style="width: 5%;">Old </th>
                                            <th class="NewRetailPrice" style="width: 5%;">New </th>
                                            <th class="BasePrice price" style="width: 5%;">Old </th>
                                            <th class="NewBasePrice" style="width: 5%;">New </th>

                                        </tr>
                                        <tr style="display: none">
                                            <td class="Action text-center" style="width: 5%; display: none">Enable</td>
                                            <td class="UnitType" style="width: 5%; text-align: center">Unit Type</td>
                                            <%--<td class="Status text-center" style="width: 5%;">Status</td>--%>
                                            <td class="Default text-center" style="width: 5%; display: none">Default</td>
                                            <td class="Free text-center" style="width: 5%; display: none">Free ?</td>
                                            <td class="Qty text-center">Qty</td>
                                            <td class="NewQty price">New Qty</td>
                                            <td class="CostPrice price" style="width: 5%;">Cost Price</td>
                                            <td class="NewCosePrice" style="width: 5%;">New Cost Price</td>
                                            <td class="WHminPrice price" style="width: 5%;">WH. Min Price</td>
                                            <td class="NWHPrice" style="width: 5%;">New WH. Min Price</td>
                                            <td class="RetailPrice price" style="width: 5%;">Min Retail Price</td>
                                            <td class="NewRetailPrice" style="width: 5%;">New M. Retail Price</td>
                                            <td class="BasePrice price" style="width: 5%;">Base Price</td>
                                            <td class="NewBasePrice" style="width: 5%;">New Base Price</td>
                                            <td class="Location" style="width: 30%">Location</td>
                                        </tr>

                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="alert alert-danger alert-dismissable fade in" id="Div1" style="width: 33%; text-align: left;">

                        <span><b style="color: red">Note - </b>Please check the Enable section check box to manage price.</span>
                    </div>
                    <%-- <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" onclick="TestSendEmail()">&nbsp;&nbsp;&nbsp;TestSendingEmail&nbsp;&nbsp;&nbsp;</button>--%>
                    <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1  btn-sm" onclick="checkDefault()">&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;</button>
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1  btn-sm" data-dismiss="modal">&nbsp;&nbsp;&nbsp;Close&nbsp;&nbsp;&nbsp;</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Barcode Modal -->
    <div id="barcodeModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content" style="width:950px">
                <div class="modal-header">
                    <h4 class="modal-title float-left"><span id="productName"></span> - <span id="productId"></span> (<span id="UnitName"></span>)</h4>


                    <input type="hidden" id="hiddenPackingAutoId" />
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="tblBarcode">
                            <thead class="bg-blue white">
                                <tr>
                                    <td style="width: 20%; text-align: center">Barcode Number</td>
                                    <td style="text-align: center">Barcode</td>
                                    <td style="text-align: center">No. of used</td>
                                    <td style="text-align: center">Last used date</td>
                                    <td style="width: 22%; text-align: center">Action</td>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm" data-dismiss="modal">&nbsp;&nbsp;&nbsp;Close&nbsp;&nbsp;&nbsp;</button>
                </div>
            </div>
        </div>
    </div>
    <div id="barcode" style="width: 250px; display: none"></div>
    <div class="modal fade" id="msgPop" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Message</h4>
                </div>
                <div class="modal-body">
                    <div style="border-color: #ac2925; border: 1px solid; color: #ac2925; min-height: 45px; line-height: 20px; padding: 9px;">
                        <strong id="Sbarcodemsg"></strong>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="ProductMasterPopup" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">



                    <h4>Message</h4>
                </div>

                <div class="modal-body">
                    <div style="border-color: #ac2925; border: 1px solid; color: #ac2925; min-height: 45px; line-height: 20px; padding: 9px;">
                        <strong id="ProductMasterMsg"></strong>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="SecurityEnabled" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="display: block">
                    <h4>Check Security </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">Security </div>
                        <div class="col-md-9">
                            <input type="password" id="txtSecurity" class="form-control input-sm border-primary" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <span id="errormsg"></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning buttonAnimation pull-right round box-shadow-1 btn-sm pulse" onclick="clickonSecurity()">OK</button>
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="SecurityEnvalid" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Message</h4>
                </div>
                <div class="modal-body">
                    <div style="border-color: #ac2925; border: 1px solid; color: #ac2925; min-height: 45px; line-height: 20px; padding: 9px;">
                        <strong id="Sbarcodemsgg"></strong>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1 btn-sm pulse" onclick="ClosePop()">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script src="../js/summernote.js"></script>
    <link href="../css/summernote.css" rel="stylesheet" />
    <script>
        $(document).ready(function () {
            $('#txtDescription').summernote();

        });
    </script>
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script src="BarCode/jquery-barcode.js"></script>

</asp:Content>

