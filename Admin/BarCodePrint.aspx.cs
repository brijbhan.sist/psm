﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DLLPrintBarcode;
public partial class Admin_BarCodePrint : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
            string text = File.ReadAllText(Server.MapPath("/Admin/JS/BarCodePrint.js"));
            Page.Header.Controls.Add(
                new LiteralControl(
                    "<script id='checksdrivRequiredFields'>" + text + "</script>"
                    ));
        }
    }
    #region Bind  All Category
    [WebMethod(EnableSession = true)]
    public static string BindCategory()
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                PL_PrintBarcode pobj = new PL_PrintBarcode();
                BL_PrintBarcode.BindCategory(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                if (json == "")
                {
                    json = "[]";
                }
                return json;
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    #endregion
    #region Bind  All SubCategory
    [WebMethod(EnableSession = true)]
    public static string BindSubCategory( string categoryAutoId)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                PL_PrintBarcode pobj = new PL_PrintBarcode();
                pobj.CategoryAutoId = Convert.ToInt32(categoryAutoId);
                BL_PrintBarcode.BindSubCategory(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                if (json == "")
                {
                    json = "[]";
                }
                return json;
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    #endregion
    #region Bind  All Product
    [WebMethod(EnableSession = true)]
    public static string BindAllProduct(string SubCategoryAutoId,string PrintOption)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                PL_PrintBarcode pobj = new PL_PrintBarcode();
                pobj.SubCategoryAutoId = Convert.ToInt32(SubCategoryAutoId);
                pobj.PrintOption= Convert.ToInt32(PrintOption);
                BL_PrintBarcode.BindProduct(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                if (json == "")
                {
                    json = "[]";
                }
                return json;
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    #endregion
    #region Bind  All Unit type
    [WebMethod(EnableSession = true)]
    public static string BindAllUnit(string ProductAutoId, string PrintOption)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                PL_PrintBarcode pobj = new PL_PrintBarcode();
                pobj.ProductAutoId = Convert.ToInt32(ProductAutoId);
                pobj.PrintOption = Convert.ToInt32(PrintOption);
                BL_PrintBarcode.BindUnitType(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                if (json == "")
                {
                    json = "[]";
                }
                return json;
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    #endregion
    #region Bind barcodeDetails
    [WebMethod(EnableSession = true)]
    public static string getbarcodeDetails(string dataValues)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValues);
            try
            {
                PL_PrintBarcode pobj = new PL_PrintBarcode();
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.PrintOption = Convert.ToInt32(jdv["PrintOption"]);
                pobj.UnitType = Convert.ToInt32(jdv["UnitTypeAutoId"]);
                BL_PrintBarcode.getbarcodeDetails(pobj);
                return pobj.Ds.GetXml();
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    #endregion
    #region Bind barcodeDetails
    [WebMethod(EnableSession = true)]
    public static string AddinList(string Barcode)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
           
            try
            {
                PL_PrintBarcode pobj = new PL_PrintBarcode();
                pobj.Barcode = Barcode;
                BL_PrintBarcode.AddinList(pobj);
                return pobj.Ds.GetXml();
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    #endregion
}