﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DLLAllowQtyInPieces;
using System.IO;

public partial class Packer_AllowQtyToSalesPerson : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
            string text = File.ReadAllText(Server.MapPath("/admin/JS/AllowQtyToSalesPerson.js"));
            Page.Header.Controls.Add(
                new LiteralControl(
                    "<script id='checksdrivRequiredFields'>" + text + "</script>"
                    ));
        }

    }
    [WebMethod(EnableSession = true)]
    public static string BindSalesPerson()
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                PL_AllowQtyInPieces pobj = new PL_AllowQtyInPieces();
                pobj.Opcode = 42;
                BL_AllowQtyInPieces.BindSalesPerson(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0].ToString();
                }
                if (json == "")
                {
                    json = "[]";
                }
                return json;
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string BindProduct()
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                PL_AllowQtyInPieces pobj = new PL_AllowQtyInPieces();
                BL_AllowQtyInPieces.BindProduct(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0].ToString();
                }
                if (json == "")
                {
                    json = "[]";
                }
                return json;
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string AllocatedProductQtyDetails(String AutoId)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                PL_AllowQtyInPieces pobj = new PL_AllowQtyInPieces();
                pobj.AutoId = Convert.ToInt32(AutoId);
                BL_AllowQtyInPieces.AllocatedProductQtyDetails(pobj);
                return pobj.Ds.GetXml();
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}