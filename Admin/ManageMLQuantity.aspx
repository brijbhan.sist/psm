﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="ManageMLQuantity.aspx.cs" Inherits="Admin_ManageMLQuantity" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Manage ML Quantity</h3>
            <input type="hidden" id="HPriceLevelAutoId" />
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Product</a></li>
                        <li class="breadcrumb-item">Manage ML Quantity</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-3">
                                        <select class="form-control border-primary input-sm" id="ddlSCategory" onchange="BindSubCategory()">
                                            <option value="0">All Category</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <select class="form-control border-primary input-sm" id="ddlSSubcategory">
                                            <option value="0">All Subcategory</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">

                                        <input type="text" onkeypress='return isNumberKey(event)' class="form-control border-primary input-sm" placeholder="Product ID" id="txtSProductId" />
                                    </div>
                                    <div class="col-md-3">

                                        <input type="text" class="form-control border-primary input-sm" placeholder="Product Name" id="txtSProductName" />
                                    </div>
                                    <div class="col-md-2">
                                    </div>
                                    <div class="col-md-12 mt-1 text-right">
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" id="btnSearch">Search</button>

                                        <%--<button type="button" class="btn btn-success buttonAnimation  round box-shadow-1  btn-sm" id="MLQtyUpdateBtn1" onclick="return UpdateMlQuantity();">Update</button>--%>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped" id="ManageProductList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="ProductId text-center">Product ID</td>
                                                        <td class="ProductName">Product Name</td>
                                                        <td class="IsApplyML  text-center">Is Apply ML Quantity</td>
                                                        <td class="MLQty price">ML Quantity</td>
                                                        <td class="IsApplyWT  text-center">Is Apply Weight (Oz)</td>
                                                        <td class="WeightQty price">Weight (Oz)</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="button" style="margin: 10px; float: right;" class="btn btn-success buttonAnimation pull-left round box-shadow-1  btn-sm" id="MLQtyUpdateBtn" onclick="return UpdateMlQuantity();">Update</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="/Admin/JS/ManageMLQuantity.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>


</asp:Content>

