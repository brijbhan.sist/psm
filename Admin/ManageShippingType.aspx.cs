﻿using DLLManageShippingType;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_ManageShippingType : System.Web.UI.Page
{
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string text = File.ReadAllText(Server.MapPath("/Admin/JS/ManageShippingType.js"));
                Page.Header.Controls.Add(
                     new LiteralControl(
                        "<script id='checksdrivRequiredField'>" + text + "</script>"
                    ));
            }
        }
    [WebMethod(EnableSession = true)]
    public static string insertShippingType(string dataValue)
    {
        try
        {
            Pl_ManageShippingType pobj = new Pl_ManageShippingType();
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);

            pobj.ShippingType = jdv["ShippingType"];
            pobj.EnableTax = Convert.ToInt32(jdv["EnableTax"]);
            pobj.Status = Convert.ToInt32(jdv["Status"]);
            BL_ManageShippingType.insertShippingType(pobj);

            if (!pobj.isException)
            {
                return "Success";
            }
            else
            {
                return pobj.exceptionMessage;
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod(EnableSession = true)]
    public static string GetShippingType()
    {
        try
        {
            Pl_ManageShippingType pobj = new Pl_ManageShippingType();
            BL_ManageShippingType.GetShippingType(pobj);

            if (!pobj.isException)
            {
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }

           

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod(EnableSession = true)]
    public static string editShippingType(string dataValue)
    {
        try
        {
            Pl_ManageShippingType pobj = new Pl_ManageShippingType();
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);

            pobj.Autoid = Convert.ToInt32(jdv["AutoId"]);
            BL_ManageShippingType.EditShippingType(pobj);

            if (!pobj.isException)
            {
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod(EnableSession = true)]
    public static string updateShippingType(string dataValue)
    {
        try
        {
            Pl_ManageShippingType pobj = new Pl_ManageShippingType();
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);

            pobj.ShippingType = jdv["ShippingType"];
            pobj.Autoid = Convert.ToInt32(jdv["AutoId"]);
            pobj.Status = Convert.ToInt32(jdv["Status"]);

            BL_ManageShippingType.updateShippingType(pobj);

            if (!pobj.isException)
            {
                return "Success";
            }
            else
            {
                return pobj.exceptionMessage;
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    //[WebMethod(EnableSession = true)]
    //public static string deleteShippingType(string dataValue)
    //{
    //    try
    //    {
    //        Pl_ManageShippingType pobj = new Pl_ManageShippingType();
    //        var jss = new JavaScriptSerializer();
    //        var jdv = jss.Deserialize<dynamic>(dataValue);

    //        pobj.Autoid = Convert.ToInt32(jdv["AutoId"]);
    //        BL_ManageShippingType.deleteShippingType(pobj);

    //        if (!pobj.isException)
    //        {
    //            return "Success";
    //        }
    //        else
    //        {
    //            return pobj.exceptionMessage;
    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //        return ex.Message;
    //    }
    //}
}