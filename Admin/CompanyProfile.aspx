﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .note-editable {
            height: 200px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content-header row" id="test">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Company Profile</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage Application</a></li>
                        <li class="breadcrumb-item">Manage Company Profile</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Company Details</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <label class="col-lg-4 col-sm-4 form-group">
                                                Company ID                                 
                                            </label>
                                            <div class="col-lg-8  col-sm-8 form-group">
                                                <input type="text" id="txtCompanyId" disabled="disabled" runat="server" class="form-control border-primary input-sm" />
                                            </div>
                                        </div>

                                        <div class="row">
                                            <label class="col-lg-4 col-sm-4 form-group">
                                                Company Name <span class="required">*</span>
                                            </label>
                                            <div class="col-lg-8  col-sm-8 col-xs-12 form-group">
                                                <input type="text" id="txtCompanyName" runat="server" class="form-control border-primary input-sm req" />
                                            </div>
                                        </div>

                                        <div class="row">
                                            <label class="col-lg-4 col-sm-4 form-group">
                                                Address <span class="required">*</span>
                                            </label>
                                            <div class="col-lg-8  col-sm-8 form-group">
                                                <input type="text" id="txtAddressLine1" runat="server" class="form-control border-primary input-sm req" />
                                            </div>
                                        </div>
                                         <div class="row">
                                            <label class="col-lg-4 col-sm-4 form-group">
                                                Optimo Start Latitude <span class="required">*</span>
                                            </label>
                                            <div class="col-lg-8  col-sm-8 form-group">
                                                <input type="text" id="txtStartLatitude" runat="server" class="form-control border-primary input-sm req" disabled />
                                            </div>
                                        </div>
                                        

                                        <div class="row">
                                            <label class="col-lg-4 col-sm-4 form-group">
                                                Email Address <span class="required">*</span>
                                            </label>
                                            <div class="col-lg-8  col-sm-8 form-group">
                                                <div class="form-group">
                                                    <div class="input-group">

                                                        <input type="text" id="txtEmailId" runat="server" class="form-control border-primary input-sm req" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-lg-4 col-sm-4 form-group">
                                                Website <span class="required">*</span>
                                            </label>
                                            <div class="col-lg-8  col-sm-8 form-group">
                                                <input type="text" id="txtWebsite" runat="server" class="form-control border-primary input-sm req" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-lg-4 col-sm-4 col-xs-12 form-group">
                                                Phone Number <span class="required">*</span>
                                            </label>
                                            <div class="col-lg-8  col-sm-8 col-xs-12 form-group">
                                                <input type="text" id="txtMobileNo" runat="server" class="form-control border-primary input-sm req" />
                                            </div>
                                        </div>

                                        <div class="row">
                                            <label class="col-lg-4 col-sm-4 form-group">
                                                Fax Number <span class="required">*</span>
                                            </label>
                                            <div class="col-lg-8  col-sm-8 form-group">
                                                <input type="text" id="txtFaxNo" runat="server" class="form-control border-primary input-sm req" />
                                            </div>
                                        </div>

                                        <div class="row">
                                            <label class="col-lg-4 col-sm-4 form-group">
                                                App Logout Time <span class="required">*</span>
                                            </label>
                                            <div class="col-lg-8  col-sm-8 form-group">
                                                <input type="time" id="appLogoutTime" runat="server" class="form-control border-primary input-sm req" />
                                            </div>
                                        </div>
                                          <div class="row">
                                            <label class="col-lg-4 col-sm-4 form-group" style="white-space:nowrap">
                                                Optimo Route Driver Limit <span class="required">*</span>
                                            </label>
                                            <div class="col-lg-8  col-sm-8 form-group">
                                                 <input type="text" id="txtDriverLimit" placeholder="Optimo Route Driver Limit" runat="server" class="form-control border-primary input-sm req" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <label class="col-lg-4 col-sm-4 form-group">
                                                Start Date <span class="required">*</span>
                                            </label>
                                            <div class="col-lg-8  col-sm-8 form-group">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;">
                                                                <span class="la la-calendar-o"></span>
                                                            </span>
                                                        </div>
                                                        <input type="text" id="StartDate" runat="server" class="form-control border-primary input-sm date req" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-lg-4 col-sm-4 form-group">
                                                Subscription Alert Date <span class="required">*</span>
                                            </label>
                                            <div class="col-lg-8  col-sm-8 form-group">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;">
                                                                <span class="la la-calendar-o"></span>
                                                            </span>
                                                        </div>
                                                        <input type="text" id="txtSubscriptionAlertDate" runat="server" class="form-control border-primary input-sm date req" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="row">
                                              <label class="col-lg-4 col-sm-4 form-group">
                                                Subscription Expiry Date<span class="required">*</span>
                                            </label>
                                            <div class="col-lg-8  col-sm-8 form-group">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;">
                                                                <span class="la la-calendar-o"></span>
                                                            </span>
                                                        </div>
                                                        <input type="text" id="txtSubscriptionExpiry" runat="server" class="form-control border-primary input-sm date req" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                              <div class="row">
                                            <label class="col-lg-4 col-sm-4 form-group">
                                                Optimo Start Longitude <span class="required">*</span>
                                            </label>
                                            <div class="col-lg-8  col-sm-8 form-group">
                                                <input type="text" id="txtStartLongitude" runat="server" class="form-control border-primary input-sm req" disabled />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-lg-4 col-sm-4 form-group">
                                                Date Difference<span class="required">*</span>
                                            </label>
                                            <div class="col-lg-8  col-sm-8 form-group">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="number" id="TextDateDifference" runat="server" min="0" class="form-control border-primary input-sm date req" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4  col-sm-4 form-group"></div>
                                            <div class="col-md-8  col-sm-8 form-group">
                                                <center>
                                       <img src="/Img/logo/logo.png" id="imgPreview" class="img-responsive" style="height:100px;margin-bottom:5px" />
                                </center>
                                                <input type="file" class=" form-control" id="txtfileUpload" />
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <div class="row">
                                    <label class="col-lg-2 col-sm-2 form-group">
                                        Current Version
                                    </label>
                                    <div class="col-lg-10  col-sm-10 form-group">
                                        <textarea id="currentVersion" class="form-control border-primary input-sm" rows="5"></textarea>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-lg-2 col-sm-2 form-group">
                                        Invoice Footer
                                    </label>
                                    <div class="col-lg-10  col-sm-10 form-group">
                                        <textarea id="txtTermsCondition" class="form-control border-primary input-sm"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnUpdate" style="display: none">Update</button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </div>

    <!---------Script------------->
    <!-- include summernote css/js-->

    <script src="../js/summernote.js"></script>
    <link href="../css/summernote.css" rel="stylesheet" />
    <script>
        $(document).ready(function () {
            $('#txtTermsCondition').summernote();

        });
    </script>
    <script type="text/javascript"> 
        document.write('<scr' + 'ipt type="text/javascript" src="JS/CompanyProfile.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
    <script>
        google.maps.event.addDomListener(window, 'load', function () {

            var places = new google.maps.places.Autocomplete(document.getElementById('txtAddressLine1'));
            google.maps.event.addListener(places, 'place_changed', function () {

                var place = places.getPlace();
                var address = place.formatted_address;
                $("#txtAddressLine1").val(address);
                $("#txtStartLongitude").val(place.geometry.viewport.Ya.i);
                $("#txtStartLatitude").val(place.geometry.viewport.Ua.i);
            });
        });
        </script>

</asp:Content>

