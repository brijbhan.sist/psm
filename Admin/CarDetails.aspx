﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" ClientIDMode="Static" CodeFile="CarDetails.aspx.cs" Inherits="Admin_CarMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title"> Manage Car</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                         <li class="breadcrumb-item"><a href="#">Manage Application</a>
                        </li>
                         <li class="breadcrumb-item">Manage Car</li>
                    </ol>
                </div>
            </div>
        </div>
     
         <div class="content-header-right col-md-6 col-12">
          <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
            <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
            id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false"> Action</button>
              <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                  <button type="button" class="dropdown-item" onclick=" location.href='/Admin/CarDetailsList.aspx'"id="btnAdd">Go to Car List</button>
                  <input type="hidden" id="CarAutoId" />
              </div>
          </div>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title"> Car Details</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label class="control-label">Car ID</label>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <input type="text" class="form-control input-sm border-primary" id="txtCarId" readonly="readonly" />
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <label class="control-label">
                                            Car Nick Name <span class="required">*</span>
                                        </label>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <input type="text" class="form-control input-sm border-primary req" id="txtCarName" runat="server"  />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <label class="control-label">
                                            Year <span class="required">*</span>                                          
                                        </label>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <input type="text" class="form-control input-sm border-primary req" id="txtYear" runat="server" />
                                        </div>
                                    </div>                               
                                    <div class="col-md-2">
                                        <label class="control-label">
                                            Make <span class="required">*</span>
                                        </label>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <input type="text" class="form-control input-sm border-primary req" id="txtMake" runat="server" />
                                        </div>
                                    </div>
                                 </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <label class="control-label">
                                            Model <span class="required">*</span>
                                        </label>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <input type="text" class="form-control input-sm border-primary req" id="txtModel" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <label class="control-label">
                                            VIN Number <span class="required">*</span>
                                        </label>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <input type="text" class="form-control input-sm border-primary req" id="txtVINNumber" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <label class="control-label">
                                            License Plate <span class="required">*</span>
                                        </label>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <input type="text" class="form-control input-sm border-primary req" id="txtTAGNumber" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <label class="control-label">
                                            Inspect. Exp Date <span class="required">*</span>
                                        </label>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <input type="text" class="form-control input-sm border-primary req" id="txtInspectionExpDate" runat="server" />
                                        </div>
                                    </div>
                                     </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <label class="control-label">
                                            Start Mileage <span class="required">*</span>
                                        </label>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <input type="text" class="form-control input-sm border-primary req" id="txtStartMilage" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <label class="control-label">Status</label>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <select class="form-control input-sm border-primary" id="ddlStatus">
                                                <option value="1" selected="selected">Active</option>
                                                <option value="0">Inactive</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <label class="control-label">Description</label>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <textarea class="form-control input-sm border-primary" rows="2" id="txtDescription" ></textarea>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="btn-group mr-1 pull-right">
                                            <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnSave" onclick="Save();">Save</button>
                                        </div>
                                        <div class="btn-group mr-1 pull-right">
                                            <button type="button" class="btn btn-secondary  buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnReset" onclick=" resetCar();">Reset</button>
                                        </div>
                                        <div class="btn-group mr-1 pull-right">
                                            <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1 btn-sm" data-animation="pulse" id="btnUpdate" style="display: none" onclick="Update();">Update</button>
                                        </div>
                                        <div class="btn-group mr-1 pull-right">
                                            <button type="button" class="btn btn-warning buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnCancel" style="display: none" onclick="Cancel();">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="alert alert-success alert-dismissable fade in" id="alertSuccess" style="display: none">
                                <a style="cursor: pointer;" aria-label="close" class="close">&times;</a>
                                <span></span>
                            </div>
                            <div class="alert alert-danger alert-dismissable fade in" id="alertDanger" style="display: none">
                                <a style="cursor: pointer;" aria-label="close" class="close">&times;</a>
                                <span></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="content-body" style="display:none" id="InsuranceDetails">
        <section id="drag-area1">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Insurance Details</h4>
                        </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <label style="white-space: nowrap"> Company ID</label>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="form-group row">
                                                <input type="text" class="form-control input-sm border-primary" placeholder="Company Id" id="txtInsuranceCompanyid" readonly="readonly" />
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <label style="white-space: nowrap"> Company Name</label>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="form-group row">
                                                <input type="text" class="form-control input-sm border-primary" placeholder="Company Name" id="txtInsuranceCompanyName" />
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <label style="white-space: nowrap">Start Date</label>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="form-group row">
                                                <input type="text" class="form-control input-sm border-primary" placeholder="Start Date" id="txtStartDate" />
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <label style="white-space: nowrap">End Date</label>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="form-group row">
                                                <input type="text" class="form-control input-sm border-primary" placeholder="End Date" id="txtEndDate" />
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <label style="white-space: nowrap">Primium</label>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="form-group row">
                                                <input type="text" class="form-control input-sm border-primary" placeholder="Primium" id="txtPrimium" />
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-lg-8">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered" id="tblCar">
                                                    <thead class="bg-blue white">
                                                        <tr>
                                                            <td class="InsuranceCompanyId">Company Name</td>
                                                            <td class="StartDate">Start Date</td>
                                                            <td class="EndDate">End Date</td>
                                                            <td class="Primium">Primium</td>
                                                            <td class="PaidBy">Paid By</td>
                                                            <td class="action">Action</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer"></div>
                    </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="content-body" style="display:none" id="CarService">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Car Service</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4  col-sm-4">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <label style="white-space: nowrap">Milage ID</label>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="form-group row">
                                                <input type="text" class="form-control input-sm border-primary" placeholder="Milage Id" id="txtMilageId" readonly="readonly" />
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <label style="white-space: nowrap">Milage</label>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="form-group row">
                                                <input type="text" class="form-control input-sm border-primary" placeholder="Milage" id="txtMilage" />
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <label style="white-space: nowrap">Oil Change</label>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="form-group row">
                                                <input type="text" class="form-control input-sm border-primary" placeholder="Oil Change" id="txtOilChange" />
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <label style="white-space: nowrap">By Whome</label>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="form-group row">
                                                <input type="text" class="form-control input-sm border-primary" placeholder="By Whome" id="txtByWhome" />
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <label style="white-space: nowrap">Detail</label>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="form-group row">
                                                <input type="text" class="form-control input-sm border-primary" placeholder="Detail" id="txtDetail" />
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-8  col-sm-8">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered" id="Table1">
                                            <thead class="bg-blue white">
                                                <tr>
                                                    <td class="CarId">Milage ID</td>
                                                    <td class="CarName">Milage</td>
                                                    <td class="description">By Whome</td>
                                                    <td class="status">Detail</td>
                                                    <td class="action">Action</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                        <h5 class="well text-center" id="H1" style="display: none">No data available.</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer"></div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="content-body" style="display:none" id="Usage">
        <section id="drag-area3">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Usage</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered" id="Table2">
                                        <thead class="bg-blue white">
                                            <tr>
                                                <td class="Date">Date</td>
                                                <td class="DriverName">Driver Name</td>
                                                <td class="StartMilage">Start Milage</td>
                                                <td class="EndMilage">End Milage</td>
                                                <td class="TotalMilageDrive">Total Milage Drive</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                    <h5 class="well text-center" id="H2" style="display: none">No data available.</h5>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer"></div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="modal fade" id="CarDetailPopup" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Message</h4>
                </div>
                <div class="modal-body">
                    <div style="border-color: #ac2925; border: 1px solid; color: #ac2925; min-height: 45px; line-height: 20px; padding: 9px;">
                        <strong id="CarDetailMsg"></strong>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-sm"  data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
