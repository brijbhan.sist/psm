﻿<%@ Page Title="Create Message" Language="C#" MasterPageFile="~/Admin/MasterPage.master" CodeFile="CreateMessage.aspx.cs" Inherits="CreateMessage" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Manage Message</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Manage Application</a>
                        </li>
                        <li class="breadcrumb-item">Manage Message
                        </li>
                    </ol>
                </div>
            </div>
            <input type="hidden" id="MessageAutoId" />
        </div>
    </div>

    <div class="content-body">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-header">
                                <h4 class="card-title">Message Details</h4>
                            </div>
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-2">
                                        <label class="control-label">
                                            Select Group&nbsp;<span class="required">*</span>

                                        </label>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <div class="form-group">
                                            <select class="form-control border-primary input-sm ddlreq" id="ddlGroup" runat="server" onchange="BindRecipient()">
                                                <option value="0">- Select -</option>
                                                <option value="2">Individual</option>
                                                <option value="1">Team</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-2">
                                        <label class="control-label">
                                            Select Recipients&nbsp;<span class="required">*</span>

                                        </label>
                                    </div>
                                    <div class="col-sm-12 col-md-4">

                                        <select class="form-control border-primary input-sm ddlreq" id="ddlRecipient" style="height: 28px !important; width: 100% !important" runat="server" multiple="true">
                                            <option value="0">- Select -</option>
                                        </select>
                                    </div>
                                </div>
                                 <div class="row form-group">
                                    <div class="col-sm-12 col-md-2">
                                        <label class="control-label">Start Date</label>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="padding: 0rem 1rem;">
                                                        <span class="la la-calendar-o"></span>
                                                    </span>
                                                </div>
                                                <input type="text" class="form-control border-primary input-sm date" runat="server" id="txtStartDate" placeholder="Start Date" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-2">
                                        <label class="control-label">Start Time</label>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="padding: 0rem 1rem;">
                                                        <span class="ft-clock"></span>
                                                    </span>
                                                </div>
                                                <input type="text" class="form-control border-primary input-sm pickatime-format" runat="server" id="txtStartTime" placeholder="Start Time" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-2">
                                        <label class="control-label">Expiry Date</label>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="padding: 0rem 1rem;">
                                                        <span class="la la-calendar-o"></span>
                                                    </span>
                                                </div>
                                                <input type="text" class="form-control border-primary input-sm date" runat="server" id="txtExpiryDate" placeholder="Expiry Date" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-2">
                                        <label class="control-label">Expiry Time</label>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="padding: 0rem 1rem;">
                                                        <span class="ft-clock"></span>
                                                    </span>
                                                </div>
                                                <input type="text" class="form-control border-primary input-sm pickatime-format" onchange="checkMessage()" runat="server" id="txtExpiryTime" placeholder="Expiry Time" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-2">
                                        <label class="control-label">Title</label>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" class="form-control border-primary input-sm" runat="server" id="Texttitle" placeholder="Title" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-2">
                                        <label class="control-label">Sequence No</label>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="number" class="form-control border-primary input-sm" min="1" runat="server" id="TextSequence" placeholder="Sequence No" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 <div class="row form-group">
                                     <div class="col-sm-12 col-md-2">
                                        Repeat&nbsp;
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <div class="form-group">
                                          <input type="checkbox" id="cbRepeat" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-2">
                                        Image&nbsp;
                                    </div>
                                    <div class="col-sm-12 col-md-4">

                                        <div class="form-group">
                                          <input type="file" id="file1" name="files[]" accept="img/*" class="form-control border-primary" runat="server" multiple onchange="SaveWebsiteDetails(this)" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-2">
                                        Message
                                    </div>
                                    <div class="col-sm-12 col-md-6">

                                        <div class="form-group">
                                            <textarea id="txtMessage" maxlength="500" class="form-control border-primary input-sm" rows="15" placeholder="Enter your message here" runat="server"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <div class="form-group">
                                            <img src="../images/NoImageYet.png" onerror="this.src='/images/NoImageYet.png'" id="imgAlertMessage" class="img-responsive" style="margin-bottom: 10px;width: 100%;border: 2px solid grey;height: 269px;" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <button type="button" class="btn btn-success buttonAnimation  pull-right round box-shadow-1  btn-sm" id="btnSaveMessage" onclick="SaveMessage()">Save</button>
                                        <button type="button" class="btn btn-warning buttonAnimation  pull-right round box-shadow-1  btn-sm" id="btnupdateMessage" style="display: none" onclick="UpdateMessage()">Update</button>&nbsp;&nbsp;
                                        <button type="button" class="btn btn-secondary buttonAnimation pull-right  round box-shadow-1  btn-sm" id="btnResetMessage" style="margin: 0 5px" onclick="resetData()">Reset</button>
                                        <button type="button" class="btn btn-danger buttonAnimation  pull-right round box-shadow-1  btn-sm" id="btnCancelMessage" style="display: none; margin: 0 5px" onclick="resetData();">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </div>
    <div class="content-body">
        <section id="drag-area1">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-header">
                                <h4 class="card-title">Message List </h4>
                            </div>
                            <div class="card-body">

                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-4">
                                        <input type="text" class="form-control border-primary input-sm date" placeholder="Expiry Date" id="txtSExpiryDate" />
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" id="btnSearch" onclick="BindMessage(1)">Search</button>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblMessageList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="Action text-center">Action</td>
                                                        <td class="Status  text-center">Status</td>
                                                        <td class="MsgDate">Message Date</td>
                                                        <td class="Msg">Message</td>
                                                        <td class="Group">Group</td>
                                                        <td class="AssignTo">Assign To</td>
                                                        <td class="StartDateTime">Start Date & Time</td>
                                                        <td class="ExpDateTime">Expiry Date & Time</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-12">
                                    <div class="Pager"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</asp:Content>

