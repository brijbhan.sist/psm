﻿using DllDropDownMaster;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DropDownMaster : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    #region Bind Active Vendor
    [WebMethod(EnableSession = true)]
    public static string BindVendor()
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                PL_DropDownMaster pobj = new PL_DropDownMaster();
                BL_DropDownMaster.BindVendor(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                if (json == "")
                {
                    json = "[]";
                }
                return json;
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    #endregion
    #region Bind Active Product
    [WebMethod(EnableSession = true)]
    public static string BindProduct()
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                PL_DropDownMaster pobj = new PL_DropDownMaster();
                BL_DropDownMaster.BindProduct(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                if (json == "")
                {
                    json = "[]";
                }
                return json;
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    #endregion
    #region Bind Active Status
    [WebMethod(EnableSession = true)]
    public static string BindStatus(string StatusCategory)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                PL_DropDownMaster pobj = new PL_DropDownMaster();
                pobj.StatusCategory = StatusCategory;
                BL_DropDownMaster.BindStatus(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                if (json == "")
                {
                    json = "[]";
                }
                return json;
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    #endregion
    #region Bind  All Order Type
    [WebMethod(EnableSession = true)]
    public static string BindAllOrderType()
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                PL_DropDownMaster pobj = new PL_DropDownMaster();
                BL_DropDownMaster.BindAllOrderType(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                if (json == "")
                {
                    json = "[]";
                }
                return json;
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    #endregion

    #region Bind  All Brand
    [WebMethod(EnableSession = true)]
    public static string BindBrand()
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                PL_DropDownMaster pobj = new PL_DropDownMaster();
                BL_DropDownMaster.BindBrand(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                if (json == "")
                {
                    json = "[]";
                }
                return json;
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    #endregion


}