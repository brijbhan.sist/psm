﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="ErrorTicketForm.aspx.cs" Inherits="Admin_ErrorTicketForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Generate Support Ticket</h3>
            <input type="hidden" id="CreditAutoId" />
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item">Generate Support Ticket</li>                       
                    </ol>
                </div>
            </div>
        </div>
         <div class="content-header-right col-md-6 col-12">

            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a class="dropdown-item" href="/Admin/ErrorTicketLog.aspx"  runat="server">Go Ticket List</a>
                </div>
            </div>
        </div>
    </div>

    
      <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Generate Ticket</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">

                                    <div class="col-md-4 form-group">
                                        <label class="label-control">Date</label>
                                        <input type="text" id="txtdate" readonly="readonly" class="form-control border-primary input-sm date" style="text-transform:uppercase" runat="server" />
                                    </div>


                                    <div class="col-md-4 form-group">
                                        <label class="label-control">
                                            Type<span class="required">&nbsp*</span>

                                        </label>
                                        <select id="ddltype" runat="server" style="padding:1px !important;" class="form-control border-primary input-sm ddlreq">
                                            <option value="0">-Select-</option>
                                            <option>Bug</option>
                                            <option>Modification</option>
                                            <option>New development</option>
                                        </select>

                                    </div>

                                    <div class="col-md-4 form-group">
                                        <label class="label-control">
                                            Priority<span class="required">&nbsp*</span>

                                        </label>
                                        <select id="ddlproir" runat="server" style="padding:1px !important;" class="form-control border-primary input-sm ddlreq">
                                            <option value="0">-Select-</option>
                                            <option>Urgent</option>
                                            <option>Normal</option>
                                        </select>

                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-md-12 form-group">
                                        <label class="label-control">
                                            Subject<span class="required">&nbsp*</span>

                                        </label>
                                        <input type="text" id="txtsubject" class="form-control border-primary input-sm req" runat="server" />
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col-md-12 form-group">
                                        <label id="Label5" runat="server">Reported url</label>
                                        <input type="text" id="txturl" class="form-control border-primary input-sm" runat="server" />
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col-md-12 form-group">
                                        <label>
                                            Description<span class="required">&nbsp*</span>

                                        </label>
                                        <textarea id="txtcomment" style="padding:5px !important;" class="form-form-control border-primary form-control input-sm req" runat="server" 
                                       ></textarea>
                                                
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col-md-12 form-group">
                                        <label class="label-control">Attachment</label><br />
                                        <input type="file" style="padding:5px !important;" class=" form-control border-primary" id="txtfileUpload" runat="server" />

                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="btn-group mr-1 pull-right">
                                        <button id="ButtonTkSave" type="button" runat="server" class="btn btn-primary btn-sm buttonAnimation pull-right round box-shadow-1">Generate Ticket</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="alert alert-success alert-dismissable fade in" id="alertSuccess" style="display: none">
                        <a style="cursor: pointer;" aria-label="close" class="close">&times;</a>
                        <span></span>
                    </div>
                    <div class="alert alert-danger alert-dismissable fade in" id="alertDanger" style="display: none">
                        <a style="cursor: pointer;" aria-label="close" class="close">&times;</a>
                        <span></span>
                    </div>
                </div>

            </div>
        </section>
    </div>

     <script type="text/javascript">
         document.write('<scr' + 'ipt type="text/javascript" src="/Admin/JS/ErrorTicketMaster.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
   

</asp:Content>

