﻿using DLLRouteMaster;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_RouteList : System.Web.UI.Page
{
     
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["EmpTypeNo"]!= null)
        {
            if (Session["EmpTypeNo"].ToString() == "2")
            {
                sales.Visible = false;
            }
        }
        else
        {
            Response.Redirect("/");
        }
        
    }
    [WebMethod(EnableSession = true)]
    public static string DeleteRoute(string AutoId)
    {
        PL_RouteMaster pobj = new PL_RouteMaster();
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                pobj.AutoId = Convert.ToInt16(AutoId);
                BL_RouteMaster.DeleteRoute(pobj);
                if(!pobj.isException)
                {
                    return "true";
                }
                else
                {
                   return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }

    }
}