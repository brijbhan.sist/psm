﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Web.Configuration;
using DllLogin;
using System.Data;
using System.Web.Services;
using System.Web.Script.Serialization;
using DllPageTrackerMaster;
using DllPageTitleMaster;
using System.IO;

public partial class MasterPage : System.Web.UI.MasterPage
{

    protected void Page_Load(object sender, EventArgs e)
    {
        string text = File.ReadAllText(Server.MapPath("/JS/SessionFile.js"));
        string text1 = File.ReadAllText(Server.MapPath("/JS/Commonfumction.js"));
        Page.Header.Controls.Add(
             new LiteralControl("<script id='checksdrivRequiredField'>" + text + "</script>")
             ); Page.Header.Controls.Add(
             new LiteralControl("<script id='checksdrivRequiredField'>" + text1 + "</script>")
             );

        if (Session["redirectStatus"]!=null)
        {
            AuthStatus.Value = Session["redirectStatus"].ToString();
            Session["redirectStatus"] = null;
        }
        VerifyUserAccess();
        if (Session["Location"] == null)
        {
            Response.Redirect("/");
        }
        ModuleAccess();
        if (Session["PageTitleName"] == null)
        {
            TitleName();
        }
        Page.Title = Session["PageTitleName"].ToString().ToUpper();
        if (Session["companyidentifier"] != null)
        {
            companyidentifier.InnerHtml = Session["companyidentifier"].ToString();
        }
        else
        {
            TitleName();
        }
        if (!IsPostBack)
        {
            try
            {
                if (Session["ImageURL"] != null)
                {
                    imgPreviewProfile.Src = "/Img/logo/" + Session["ImageURL"].ToString();
                    imgName.Value = Session["ImageURL"].ToString();
                    PageTracker();
                }
                if (Session["EmpType"].ToString() == null)
                {
                    Session.Abandon();
                    Response.Redirect("~/Default.aspx", false);
                }
                else
                {
                    hEmpTypeNo.Value = Session["EmpTypeNo"].ToString();
                    lblUserName.InnerHtml = (Session["ProfileName"].ToString() == "" ? Session["EmpFirstName"].ToString() : Session["ProfileName"].ToString()) + " <br>" + Session["EmpType"].ToString() + ""; ;
                }
            }
            catch
            {
                Session.Abandon();
                Response.Redirect("~/Default.aspx", false);
            }
        }
        try
        {

            if (Session["DBLocation"] != null)
            {

                Hd_Domain.Value = Session["DBLocation"].ToString();
            }
            else
            {
                Response.Redirect("/");
            }
        }
        catch (Exception)
        {

        }
    }
    protected void lnkLogout_ServerClick(object sender, EventArgs e)
    {
        Session.Abandon();
        Response.Redirect("~/Default.aspx", false);
    }
    public void TitleName()
    {
        PL_Login pobj = new PL_Login();
        BL_Login.Bindlogo(pobj);
        string Title = (pobj.Ds.Tables[0].Rows[0][1].ToString());
        companyidentifier.InnerHtml = (pobj.Ds.Tables[0].Rows[0][2].ToString());
        Session["companyidentifier"] = (pobj.Ds.Tables[0].Rows[0][2].ToString());
        Session.Add("PageTitleName", Title);
    }
    public void ModuleAccess()
    {
        PL_Login pobj = new PL_Login();
        pobj.EmpType = Convert.ToInt32(Session["EmpTypeNo"]);
        BL_Login.getModuleList(pobj);
        string html = "";

        if (pobj.Ds != null && pobj.Ds.Tables.Count != 0)
        {
            foreach (DataRow mod in pobj.Ds.Tables[0].Rows)
            {
                if (mod["totalChild"].ToString() == "0" && mod["HasChild"].ToString() == "0")
                {
                    html += "<li class='nav-item hus-sub'><a href = '" + mod["PageUrl"].ToString() + "' ><i class='" + mod["IconClass"].ToString() + "'></i><span class='menu-title' data-i18n='nav.dash.main'>" + mod["ModuleName"] + "</span></a></li>";

                }

                else if (mod["totalChild"].ToString() != "0" && mod["HasChild"].ToString() == "0")
                {
                    html += "<li class='nav-item'><a href = '#' ><i class='" + mod["IconClass"].ToString() + "'></i><span class='menu-title' data-i18n='nav.am.main'>" + mod["ModuleName"] + "</span></a>";


                    html += "<ul class='menu-content'>";
                    foreach (DataRow subMod in pobj.Ds.Tables[0].Rows)
                    {
                        if (subMod["ParentId"].ToString() == mod["AutoId"].ToString())
                        {
                            if (subMod["totalChild"].ToString() == "0")
                            {
                                html += "<li><a class='menu - item' href='" + subMod["PageUrl"].ToString() + "' data-i18n='nav.am.erm'>" + subMod["ModuleName"] + "</a></li>";
                            }
                            else
                            {
                                html += "<li class='nav-item'><a class='menu - item' href='" + subMod["PageUrl"].ToString() + "' data-i18n='nav.am.erm'>" + subMod["ModuleName"] + "</a>";

                                html += "<ul class='menu-content'>";
                                foreach (DataRow innersubMod in pobj.Ds.Tables[0].Rows)
                                {

                                    if (innersubMod["ParentId"].ToString() == subMod["AutoId"].ToString())
                                    {
                                        html += "<li><a class='menu-item' href='" + innersubMod["PageUrl"].ToString() + "' data-i18n='nav.im.prl'>" + innersubMod["ModuleName"] + "</a></li>";
                                    }

                                }
                                html += "</li></ul>";
                            }


                        }
                    }
                    html += "</ul></ li >";
                }


            }
        }
        mainMenu.InnerHtml = html.ToString();
        Session.Add("ModuleName", mainMenu);

    }
    public void PageTracker()
    {
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                PL_PageTrackerMaster pobj = new PL_PageTrackerMaster();
                pobj.Opcode = 101;
                pobj.UserAutoId= Convert.ToInt32(Session["EmpAutoId"]);
                pobj.PageUrl = HttpContext.Current.Request.Url.AbsolutePath;
                pobj.IpAddress = HttpContext.Current.Request.UserHostAddress;
                BL_PageTrackerMaster.Add(pobj);
            }
        }
        catch (Exception ex)
        {
        }
    }
    public void VerifyUserAccess()
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                PL_PageTitleMaster pobj = new PL_PageTitleMaster();
                pobj.PageUrl = HttpContext.Current.Request.Url.PathAndQuery;
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpTypeNo"]);
                BL_PageTitleMaster.VerifyUserAccess(pobj);
                if (pobj.exceptionMessage== "InvalidAccess")
                {
                    Session["redirectStatus"] = "1";
                    if (pobj.EmpAutoId==2)
                    {
                        Response.Redirect("/Sales/mydashboard_sales.aspx");
                    }
                    else
                    {
                        Response.Redirect("/Admin/mydashboard.aspx", false); 
                    }
                   
                }
            }
            catch (Exception ex )
            {

            }
        }
        else
        {
            Response.Redirect("/");
        }
    }
}
 