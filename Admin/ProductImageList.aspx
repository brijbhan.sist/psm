﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="ProductImageList.aspx.cs" Inherits="Admin_ProductImageList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .table th, .table td {
            padding: 0.75rem !important;
        }
        .Pager{
            text-align:right
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Product Image List</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Manage Product</a></li>
                        <li class="breadcrumb-item active">Product Image List</li>
                    </ol>
                </div>
            </div>
        </div>

    </div>
    <style>
        td.Category.text-center.sorting {
            width: 113px !important;
        }

        td.Subcategory.text-center.sorting {
            width: 101px !important;
        }

        td.ProductName.sorting {
            width: 224px !important;
        }
    </style>

    <div class="content-body">
        <section id="drag-area3">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-2 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlSCategory" onchange="bindSubcategory()">
                                            <option value="0">Select Category</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlSSubcategory">
                                            <option value="0">All Subcategory</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Product ID" id="txtSProductId" />
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Product Name" id="txtSProductName" />
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlStatus">
                                            <option value="2">All</option>
                                            <option value="1" selected>Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group" style="display: none">
                                        <select class="form-control border-primary input-sm" id="ddlImageType">
                                            <option value="2">All Image Type</option>
                                            <option value="1" selected>Broken</option>
                                        </select>
                                    </div>

                                    <div class="col-md-1 form-group">
                                        <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" onclick="getProductList(1);" id="btnSearch">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblProductList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="Action text-center">Action</td>
                                                        <td class="Sr text-center">Sr</td>
                                                        <td class="ProductId text-center">Product Id</td>
                                                        <td class="ProductName">Product Name</td>
                                                        <td class="Category text-center">Category</td>
                                                        <td class="Subcategory text-center">Subcategory</td>
                                                        <td class="Image text-center">Image</td>
                                                        <td class="Thumbail100 text-center">Thumbnail 100*100</td>
                                                        <td class="Thumbail400 text-center">Thumbnail 400*400</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                 <div class="row">
                                    <div class="col-md-2" style="padding-right:110px">
                                        <select class="form-control border-primary input-sm" id="ddlPageSize" onchange="getProductList(1)">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                     <div class="col-md-10">
                                    <div class="Pager"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div id="PopCustomerDocuments" class="modal fade" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="card-title">Upload Image</h4>
                    <input type="hidden" id="hdnuploadAutoId" />
                </div>
                <div class="modal-body">
                    <div class="row form-group">

                        <div class="col-md-4">
                            <label class="control-label">
                                Product Name : 
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" id="txtProductName" class="form-control border-primary" runat="server" disabled />
                            </div>
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">
                                Product Id :
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" id="txtProductsId" class="form-control border-primary" runat="server" disabled />
                            </div>
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">
                                Category :
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" id="txtCategory" class="form-control border-primary" runat="server" disabled />
                            </div>
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">
                                Subcategory : 
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" id="txtsubcategory" class="form-control border-primary" runat="server" disabled />
                            </div>
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">
                                Image : <span class="required">*</span>
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="hidden" id="txtProductId" />
                                <input type="file" name="img" accept="img/*" style="overflow: hidden;" id="fileImageUrl" class="form-control border-primary" onchange='checkFileDetails(this)' runat="server" />
                                <i>Recommended image ratio is 1:1</i>

                            </div>
                        </div>

                    </div>
                    <div class="row form-group">
                        <div class="col-md-4"></div>
                        <div class="col-md-8">
                            <img id="imgProductImage" src="../images/default_pic.png" class="border-primary" style="width: 100px; height: 80px" />
                            <br />
                            <i>Allowed file :<b style="color: red">[png,jpg,jpeg]</b></i>
                            <br />
                            <i>Max size is :<b style="color: red"><%=System.Configuration.ConfigurationManager.AppSettings["ImageValidation"]%> MB</b></i>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-success pull-right  buttonAnimation round box-shadow-1  btn-sm" id="btnUploadImage" onclick="UploadImage()" style="margin: 0 5px 0 5px">Save</button>
                            <button type="button" class="btn btn-danger pull-right  buttonAnimation round box-shadow-1  btn-sm" onclick="clearBorder()" data-dismiss="modal" style="margin: 0 5px 0 5px">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="JS/ProductImageList.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
    <%--    <script>
        $(document).ready(function () {
            $('#tblProductList').DataTable({
                paging: true,
                //pageLength: '10',
            });
        });
    </script>--%>
</asp:Content>

