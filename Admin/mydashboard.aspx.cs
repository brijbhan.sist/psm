﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DLLDashboard;

public partial class Admin_Dashboard : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["EmpTypeNo"] != null)
        {
            if (Session["EmpTypeNo"].ToString() == "2")
            {
                Response.Redirect("/Sales/mydashboard_sales.aspx");
            }
        }
    }
    [WebMethod(EnableSession = true)]
    public static string bindCustomer()
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {

                PL_Dashboard pobj = new PL_Dashboard();
                BL_Dashboard.bindCustomer(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string InvalidPacking()
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {

                PL_Dashboard pobj = new PL_Dashboard();
                BL_Dashboard.InvalidPacking(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}