﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="ErrorTicketLog.aspx.cs" Inherits="Admin_ErrorTicketLog" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Ticket List</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <%--<li class="breadcrumb-item"><a href="#">Reports</a></li>
                        <li class="breadcrumb-item"><a href="#">Sales Report</a></li>--%>
                        <li class="breadcrumb-item"><a href="#">Developer Help</a></li>
                        <li class="breadcrumb-item active">Ticket List</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" id="btnExportToExcel">Export To Excel</button>
                     <input type="hidden" id="hiddenEmpType" runat="server" />
                </div>
            </div>
        </div>
    </div>

    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-3" id="divCustomerType" runat="server">
                                        <asp:DropDownList ID="ddlCustomerType" runat="server" CssClass="form-control input-sm border-primary">
                                            <asp:ListItem Value="0">All User Type</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-md-3">
                                        <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control input-sm border-primary">
                                            <asp:ListItem Value="0">ALL Client Status</asp:ListItem>
                                            <asp:ListItem>Close</asp:ListItem>
                                            <asp:ListItem>Open</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-md-3">
                                        <asp:DropDownList ID="ddlDeveloperStatus" runat="server" CssClass="form-control input-sm border-primary">
                                            <asp:ListItem Value="0">All Developer Status</asp:ListItem>
                                            <asp:ListItem>Close</asp:ListItem>
                                            <asp:ListItem>Open</asp:ListItem>
                                            <asp:ListItem>Under Process</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <asp:DropDownList ID="ddltype" runat="server" CssClass="form-control input-sm border-primary">
                                            <asp:ListItem Value="0">All</asp:ListItem>
                                            <asp:ListItem>Bug</asp:ListItem>
                                            <asp:ListItem>Modification</asp:ListItem>
                                            <asp:ListItem>New development</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="col-md-3">
                                        <asp:TextBox ID="txtTicketID" runat="server" placeholder="Ticket ID" CssClass="form-control input-sm border-primary"></asp:TextBox>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <asp:TextBox ID="txtSearchbysubject" runat="server" placeholder="Subject" CssClass="form-control input-sm border-primary"></asp:TextBox>
                                    </div>
                                    <div class="col-md-3 form-group" id="hide" runat="server">
                                        <asp:DropDownList ID="ddlempid" runat="server" CssClass="form-control input-sm border-primary">
                                            <asp:ListItem Value="0">All Employee</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <asp:TextBox ID="txtFromDate" runat="server" onchange="setdatevalidation(1)" CssClass="form-control input-sm border-primary" placeholder="From Date"
                                                MaxLength="10"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <asp:TextBox ID="txtToDate" runat="server" onchange="setdatevalidation(2)" CssClass="form-control input-sm border-primary" placeholder="To Date"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-1 form-group">
                                        <button id="btnSearch" type="button" runat="server" class="btn btn-info buttonAnimation pull-right round box-shadow-1 btn-sm" onclick="getDetail(1);">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body" style="min-height:300px !important">
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblErrorTicketLog">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="TicketId text-center">Ticket ID </td>
                                                        <td class="TicketDate text-center">Date/Time </td>
                                                        <td class="Fullname">By Employee</td>
                                                        <td class="Priority text-center">Priority </td>
                                                        <td class="Type">Type </td>
                                                        <td class="ClentStatus text-center">Client Status </td>
                                                        <td class="DeveloperStatus text-center">Dev.Status</td>
                                                        <td class="TicketCloseDate text-center">Last Response Date </td>
                                                        <td class="Subject">Subject </td>
                                                        <td class="Description">Description </td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row container-fluid">
                                    <div class="">
                                        <select id="ddlPaging" class="form-control input-sm border-primary">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div>
        <table class="table table-striped table-bordered" id="tblErrorTicketLogExport" style="display: none">
            <thead class="bg-blue white">
                <tr>
                    <td class="TicketId">Ticket ID </td>
                    <td class="TicketDate">Date </td>
                    <td class="Fullname">By Employee</td>
                    <td class="Priority">Priority </td>
                    <td class="Type">Type </td>
                    <td class="ClentStatus">Client Status </td>
                    <td class="DeveloperStatus">Dev.Status</td>
                    <td class="TicketCloseDate">Last Response Date </td>
                    <td class="Subject">Subject </td>
                    <td class="Description">Description </td>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="/Admin/JS/ErrorTicketLog.js?v=' + new Date() + '"></scr' + 'ipt>');
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
</asp:Content>

