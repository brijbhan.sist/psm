﻿using ManageIP;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_ManageIP : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Admin/JS/ManageIP.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredFields'>" + text + "</script>"
                ));
        }
    }
    [WebMethod(EnableSession = true)]
    public static string saveIpAddress(string dataValue)
    {
        PL_ManageIP pobj = new PL_ManageIP();
        try
        {

            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                pobj.isDefault = Convert.ToInt32(jdv["isDefault"]);
                pobj.userIpAddress = jdv["userIpAddress"];
                pobj.description = jdv["description"];
                BL_ManageIP.saveIpAddress(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "session";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod(EnableSession = true)]
    public static string GetIPList(string dataValue)
    {

        if(HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_ManageIP pobj = new PL_ManageIP();
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.userIpAddress = jdv["userIpAddress"];
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                BL_ManageIP.select(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "false";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string GetIPDetail(string dataValue)
    {
        PL_ManageIP pobj = new PL_ManageIP();
        try
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.AutoId = Convert.ToInt32(jdv["AutoId"]);
                BL_ManageIP.GetIPDetail(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "session";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod(EnableSession = true)]
    public static string UpdateIpAddress(string dataValue)
    {
        PL_ManageIP pobj = new PL_ManageIP();
        try
        {

            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                pobj.isDefault = Convert.ToInt32(jdv["isDefault"]);
                pobj.userIpAddress = jdv["userIpAddress"];
                pobj.description = jdv["description"];
                pobj.AutoId = Convert.ToInt32(jdv["AutoId"]);

                BL_ManageIP.UpdateIpAddress(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "session";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod(EnableSession = true)]
    public static string DeleteIp(string dataValue)
    {
        PL_ManageIP pobj = new PL_ManageIP();
        try
        {

            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.AutoId = Convert.ToInt32(jdv["AutoId"]);
                BL_ManageIP.DeleteIp(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "session";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
}