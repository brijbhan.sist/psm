﻿using DLLExpenseMaster;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_ExpeneMaster : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Admin/JS/ExpenseMaster.js"));           
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredFields'>" + text + "</script>"
                ));
        }
    }
    [WebMethod(EnableSession = true)]
    public static string Binddropdown()
    {
        PL_ExpenseMaster pobj = new PL_ExpenseMaster();
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {

                BL_ExpenseMaster.Binddropdown(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                return json;


            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }

    }

}