﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" ClientIDMode="Static" CodeFile="stockEntry.aspx.cs" Inherits="Admin_stockEntry" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .tblpading {
            padding: 10px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Receive Stock</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage PO</a></li>
                        <li class="breadcrumb-item"><a href="#">By Vendor</a></li>
                        <li class="breadcrumb-item active">New Stock Entry</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">                  
                    <button type="button" onclick=" location.href='/Admin/stockEntryList.aspx'" class="dropdown-item" animation="pulse" id="Button5" runat="server">
                         Received Stock List</button>
                </div>
            </div>
        </div>          
    </div>


    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                       <%-- <div class="card-header">
                            <h4 class="card-title">Receive Stock Details</h4>
                        </div>--%>

                        <div class="card-content collapse show">
                            <div class="card-body" id="panelBill">

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-4 col-sm-4 col-xs-12">
                                                Vendor <span class="required">*</span>
                                            </label>
                                            <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                                                <select class="form-control input-sm border-primary sddlreq" id="ddlVendor" runat="server">
                                                    <option value="0">-Select-</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-4 col-sm-4 col-xs-12">
                                                Bill No <span class="required">*</span>
                                            </label>
                                            <input type="hidden" id="txtHBillAutoId" />
                                            <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                                                <input type="text" id="txtBillNo" class="form-control input-sm border-primary sreq" maxlength="20" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-4 col-sm-4 col-xs-12">
                                                Bill Date <span class="required">*</span>
                                            </label>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                                            <span class="la la-calendar-o"></span>
                                                        </span>
                                                    </div>
                                                    <input type="text" id="txtBillDate" class="form-control input-sm border-primary sreq" runat="server" readonly="readonly" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-4 col-sm-4 col-xs-12">Remark</label>
                                            <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                                                <textarea class="form-control input-sm border-primary" rows="1" id="txtRemarks" runat="server"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                           
                        </div>

                    </div>

                </div>
            </div>
        </section>
    </div>

    <div class="content-body">
        <section id="drag-area3">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-4 col-sm-4 col-xs-12 control-label">Barcode
                                            </label>
                                            <div class="col-md-8 col-sm-8 col-xs-12  form-group">
                                                <input type="text" class="form-control input-sm border-primary" id="txtBarcode" runat="server" onfocus="this.select()" onkeypress="return isNumberKey(event)" placeholder="Enter Barcode here" onchange="readBarcode()" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-4 col-sm-4 col-xs-12 control-label">
                                                Product <span class="required">*</span>
                                            </label>
                                            <div class="col-md-8 col-sm-8 col-xs-12  form-group">
                                                <select class="form-control input-sm border-primary" id="ddlProduct" runat="server" onchange="bindUnitType()">
                                                    <option value="0">-Select-</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">
                                                    Unit Type <span class="required">*</span>
                                                </label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12  form-group">
                                                <select class="form-control input-sm border-primary ddlreq" id="ddlUnitType" runat="server">
                                                    <%--<option value="0">-Select-</option>--%>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="clearfix"></div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">
                                                    Quantity
                                                                       <span class="required">*</span>
                                                </label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12  form-group">
                                                <input type="text" class="form-control input-sm border-primary req" id="txtQuantity" runat="server" value="0" onfocus="this.select()" onkeypress='return isNumberKey(event)' />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Total Pieces</label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                                               <%-- <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text input-group-text-custom">
                                                            <span class="control-symble">Nos.</span>
                                                        </span>
                                                    </div>--%>
                                                    <input type="text" class="form-control input-sm border-primary" id="txtTotalPieces" runat="server" disabled="disabled" value="0" />
                                               <%-- </div>--%>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">
                                                    Unit Price
                                                                     <span class="required">*</span>
                                                </label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12  form-group">

                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text input-group-text-custom">
                                                            <span>$</span>
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control input-sm border-primary req" id="txtUnitprice" runat="server" value="0.00"
                                                        onfocus="this.select()" onkeypress='return isNumberDecimalKey(event,this)' style="text-align: right" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Total Price</label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text input-group-text-custom">
                                                            <span>$</span>
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control input-sm border-primary" id="txtTotalAmount" runat="server"
                                                        disabled="disabled" value="0.00" style="text-align: right" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                <div class="pull-right">
                                                    <button type="button" id="btnAdd" class="btn btn-purple buttonAnimation pull-right round box-shadow-1 btn-sm" runat="server">Add</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                           
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="content-body">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                       <%-- <div class="card-header">
                            <h4 class="card-title">Item List</h4>
                        </div>--%>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                                <table id="tblProductDetail" class="table table-striped table-bordered">
                                                    <thead class="bg-blue white">
                                                        <tr>
                                                            <td class="Action text-center">Action</td>
                                                            <td class="ProId text-center">ID</td>
                                                            <td class="ProName tblpading">Product Name</td>
                                                            <td class="Unit text-center">Unit Type</td>
                                                            <td class="Qty text-center">Quantity</td>
                                                            <td class="Pcs text-center">Total Pieces</td>
                                                            <td class="Price tblpading" style="text-align: right">Price</td>
                                                            <td class="TotalPrice tblpading" style="text-align: right">Total Price</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td colspan="7" style="text-align: right;font-weight:600;" class="tblpading">Total</td>
                                                            <td id="totalAmount" style="text-align: right;font-weight:600;" class="tblpading">0.00</td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                                <h5 class="well text-center" id="emptyTable" style="display: none" runat="server">No Product Selected.</h5>
                                            </div>
                                    </div>
                                </div>
                            </div>
                             <div class="card-footer">
                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="btn-group mr-1 pull-right">
                                            <button type="button" id="btnAddStock" class="btn btn-success buttonAnimation pull-right round box-shadow-1 btn-sm" runat="server">Save</button>
                                        </div>
                                        <div class="btn-group mr-1 pull-right">
                                            <button type="button" id="btnUpdate" class="btn btn-info buttonAnimation pull-right round box-shadow-1  btn-sm" style="display: none" runat="server">Update</button>
                                        </div>
                                        <div class="btn-group mr-1 pull-right">
                                            <button type="button" id="btnReset" class="btn btn-secondary buttonAnimation pull-right round box-shadow-1 btn-sm animated pulse" runat="server">Reset</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div id="UpdateMinPrice" class="modal fade" role="dialog">
        <div class="modal-dialog modal-xl">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <span class="modal-title">
                        <b>Do you want to change Cost Price bacause Unit Price is not equal to Cost Price.</b>
                    </span>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive col-md-12">
                        <div class="row">
                            <div class="col-md-2">
                                Product ID
                            </div>
                            <div class="col-md-2 form-group">
                                <input type="text" id="txtproductId" class="form-control border-primary input-sm" disabled />
                            </div>
                            <div class="col-md-2">
                                Product Name
                            </div>
                            <div class="col-md-2 form-group">
                                <input type="text" id="txtProductName" class="form-control border-primary  input-sm" disabled />
                            </div>
                            <div class="col-md-2">
                                Unit Type
                            </div>
                            <div class="col-md-2 form-group">
                                <input type="text" id="txtUnitType" class="form-control border-primary  input-sm" disabled />
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="row form-group">
                            <div class="col-md-2">
                                Quantity
                            </div>
                            <div class="col-md-2 form-group">
                                <input type="text" id="txtPQty" class="form-control border-primary  input-sm" readonly />
                            </div>
                            <div class="col-md-2">
                                Retail Minimum Price
                            </div>
                            <div class="col-md-2 form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                            <span>$</span>
                                        </span>
                                    </div>
                                    <input type="text" id="txtRetailMIN" class="form-control border-primary  input-sm" readonly onfocus="this.select()" onkeypress='return isNumberDecimalKey(event,this)' style="text-align: right" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                Cost Price
                            </div>
                            <div class="col-md-2 form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                            <span>$</span>
                                        </span>
                                    </div>
                                    <input type="text" id="txtCOSTPRICE" class="form-control border-primary  input-sm" readonly onfocus="this.select()" onkeypress='return isNumberDecimalKey(event,this)' style="text-align: right" />
                                </div>
                            </div>

                        </div>
                        <div class="clearfix"></div>
                        <div class="row form-group">
                            <div class="col-md-2">
                                Base Price
                            </div>
                            <div class="col-md-2 form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                            <span>$</span>
                                        </span>
                                    </div>
                                    <input type="text" id="txtBasePrice" class="form-control border-primary  input-sm" readonly onfocus="this.select()" onkeypress='return isNumberDecimalKey(event,this)' style="text-align: right" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                SRP
                            </div>
                            <div class="col-md-2 form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                            <span>$</span>
                                        </span>
                                    </div>
                                    <input type="text" id="txtSRP" class="form-control border-primary  input-sm" readonly onfocus="this.select()" onkeypress='return isNumberDecimalKey(event,this)' style="text-align: right" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                Wholesale Minimum Price
                            </div>
                            <div class="col-md-2 form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                            <span>$</span>
                                        </span>
                                    </div>
                                    <input type="text" id="txtWholesaleMinPrice" class="form-control border-primary  input-sm" readonly onfocus="this.select()" onkeypress='return isNumberDecimalKey(event,this)' style="text-align: right" />
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="row form-group">
                            <div class="col-md-2">
                                Location
                            </div>
                            <div class="col-md-2 form-group">
                                <input type="text" id="txtLocation" class="form-control border-primary  input-sm" readonly onfocus="this.select()" />
                            </div>
                            <%-- <div class="col-md-2">
                                Pre-Defined Barcode
                            </div>--%>
                            <%-- <div class="col-md-2 form-group">
                                <input type="text" id="txtPreDefinedBarcode" class="form-control input-sm" readonly onfocus="this.select()" />
                            </div>--%>
                            <div class="col-md-2">
                                Commission Code
                            </div>
                            <div class="col-md-2 form-group">
                                <div class="input-group">
                                    <input type="text" id="txtCommissionCode" class="form-control border-primary  input-sm" readonly onfocus="this.select()" onkeypress='return isNumberDecimalKey(event,this)' style="text-align: right" />
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                            <span>%</span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="row form-group">
                            <div class="col-md-8">
                                <div class="alert alert-danger alert-dismissable fade in" id="Div1" style="display: none;">
                                    <a href="javascript:;" aria-label="close" class="close">Close</a>
                                    <span></span>
                                </div>
                            </div>
                            <div class="col-md-4 form-group">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-adn buttonAnimation pull-right round box-shadow-1 btn-sm" id="Button4" onclick="EditProductDetails()">Edit</button>
                    <button type="button" id="Button1" class="btn btn-bitbucket buttonAnimation pull-right round box-shadow-1 btn-sm" onclick="updateBasePrice()" style="display: none">Update</button>
                    <button type="button" class="btn btn-amber buttonAnimation pull-right round box-shadow-1 btn-sm" id="Button2" onclick="NOPrice()">No</button>
                    <button type="button" class="btn btn-warning buttonAnimation pull-right round box-shadow-1 btn-sm" id="Button3" onclick="CancelProduct()">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="msgPop" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-md">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Message</h4>
                </div>
                <div class="modal-body">
                    <div style="border-color: #ac2925; border: 1px solid; color: #ac2925; min-height: 45px; line-height: 20px; padding: 9px;">
                        <strong id="Sbarcodemsg"></strong>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="StockEntryPopup" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-xl">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Message</h4>
                </div>
                <div class="modal-body">
                    <div style="min-height: auto; line-height: 20px; padding: 9px;">
                        <div id="StockEntryMsg"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <p style='text-align: justify; padding: 20px'><b>You can not update cost price becuase custom price should not be less than cost price so please update price level .</b></p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1  btn-sm" id="btnclose" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="/Admin/JS/stockEntry.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>

</asp:Content>

