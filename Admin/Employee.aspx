﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="Employee.aspx.cs" Inherits="Admin_Employee" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Manage Employee</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Application</a></li>
                        <li class="breadcrumb-item active">Manage Employee</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>



    <input type="hidden" id="emptype" runat="server" />
    <div class="content-body">
        <section id="drag-area">
            <%
                if (Session["EmpTypeNo"] != null)
                {
                    if (Session["EmpTypeNo"].ToString() == "1")
                    {
            %>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Employee Details</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row" id="panelEmployeeMaster">
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label id="AutoId" style="display: none"></label>
                                                <label class="control-label">User Type</label>
                                                <span class="required">*</span>

                                            </div>
                                            <div class="col-md-4">
                                                <select class="form-control input-sm border-primary ddlreq" id="ddlEmpType" runat="server" onchange="validateDeliveryTime(this.value);">
                                                    <option value="0">-Select-</option>
                                                </select>
                                            </div>

                                            <div class="col-md-2">
                                                <label class="control-label">Employee Code&nbsp;<span class="required">*</span></label>

                                            </div>
                                            <div class="col-md-4 form-group">
                                                <input type="text" class="form-control input-sm border-primary req" id="txtEmployeeId" runat="server" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label class="control-label">Profile Name</label>
                                                <span class="required">*</span>
                                            </div>
                                            <div class="col-md-4 form-group">
                                                <input type="text" class="form-control input-sm border-primary req" id="txtProfileName" runat="server" />
                                            </div>

                                            <div class="col-md-2">
                                                <label class="control-label">First Name</label>
                                                <span class="required">*</span>
                                            </div>
                                            <div class="col-md-4 form-group">
                                                <input type="text" class="form-control input-sm border-primary req" id="txtFirstName" runat="server" onfocus="this.select()" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label class="control-label">Last Name</label>
                                                <span class="required">*</span>
                                            </div>
                                            <div class="col-md-4 form-group">
                                                <input type="text" class="form-control input-sm border-primary req" id="txtLastName" runat="server" onfocus="this.select()" />
                                            </div>

                                            <div class="col-md-2">
                                                <label class="control-label">Address</label>
                                            </div>
                                            <div class="col-md-4 form-group">
                                                <input type="text" class="form-control input-sm border-primary" id="txtAddress" runat="server" onfocus="this.select()" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label class="control-label">State</label>
                                            </div>
                                            <div class="col-md-4 form-group">
                                                <input type="text" class="form-control input-sm border-primary" id="txtState" runat="server" onfocus="this.select()" />
                                            </div>

                                            <div class="col-md-2">
                                                <label class="control-label">City</label>
                                            </div>
                                            <div class="col-md-4 form-group">
                                                <input type="text" class="form-control input-sm border-primary" id="txtCity" runat="server" onfocus="this.select()" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label class="control-label">
                                                    Zip Code                              
                                                </label>
                                            </div>
                                            <div class="col-md-4 form-group">
                                                <input type="text" class="form-control input-sm border-primary" id="txtZipcode" runat="server" maxlength="5" onfocus="this.select()" onblur="validateZipCode(this)" onkeypress='return isNumberKey(event)' />
                                            </div>
                                            <div class="col-md-2">
                                                <label class="control-label">
                                                    Email                                                        
                                                </label>
                                            </div>
                                            <div class="col-md-4 form-group">
                                                <input type="text" class="form-control input-sm border-primary" id="txtEmail" runat="server" onfocus="this.select()" onchange="validateEmail(this)" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label class="control-label">
                                                    Contact No                                
                                                </label>
                                            </div>
                                            <div class="col-md-4 form-group">
                                                <input type="text" class="form-control input-sm border-primary" maxlength="10" id="txtContact" runat="server" onfocus="this.select()" onkeypress='return isNumberKey(event)' />
                                            </div>
                                            <div class="col-md-2">
                                                <label class="control-label">Status</label>
                                            </div>
                                            <div class="col-md-4 form-group">
                                                <select class="form-control input-sm border-primary" id="ddlStatus" runat="server">
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row" id="deliverytimeDiv" style="display: none;">
                                            <div class="col-md-2 form-group">
                                                <label class="control-label">Delivery Start Time&nbsp;<span class="required">*</span></label>
                                            </div>
                                            <div class="col-md-4 form-group">
                                                <select class="form-control input-sm border-primary" id="ddlDeliveryStartTime" runat="server">
                                                </select>
                                            </div>
                                            <div class="col-md-2 form-group">
                                                <label class="control-label">
                                                    Delivery Close Time&nbsp;<span class="required">*</span></label>
                                            </div>
                                            <div class="col-md-4">
                                                <select class="form-control input-sm border-primary" id="ddlDeliveryCloseTime" runat="server">
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label class="control-label">User Name</label>
                                                <span class="required">*</span>
                                            </div>
                                            <div class="col-md-4 form-group">
                                                <div class="input-group">
                                                    <input type="text" class="form-control input-sm border-primary req" id="txtUserName" runat="server" onfocus="this.select()" />
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                                            <span id="CompanyId"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="control-label">Password</label>
                                                <span class="required">*</span>
                                            </div>
                                            <div class="col-md-4 form-group">
                                                <div class="input-group">
                                                    <input type="password" class="form-control input-sm border-primary req" id="txtPassword" runat="server" onfocus="this.select()" />
                                                    <%
                                                        if (Session["EmpTypeNo"] != null)
                                                        {
                                                            if (Session["EmpTypeNo"].ToString() == "1")
                                                            {
                                                    %>

                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                                            <i class="la la-eye" id="ViewPsd" onclick="myFunction()"></i>
                                                        </span>
                                                    </div>
                                                    <%
                                                            }
                                                        }
                                                    %>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label class="control-label">Confirm Password&nbsp;<span class="required">*</span></label>

                                            </div>
                                            <div class="col-md-4 form-group">
                                                <input type="password" class="form-control input-sm border-primary req" id="txtConfirmPassword" runat="server" onfocus="this.select()" />
                                            </div>
                                            <div class="col-md-2">
                                                <label class="control-label">Is Apply IP</label>
                                            </div>
                                            <div class="col-md-4">
                                                <input type="checkbox" class="checkbox border-primary" runat="server" id="chkloginip" />
                                            </div>
                                        </div>
                                        <div runat="server" id="IpDiv" visible="false">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label class="control-label">Is App Login</label>
                                                </div>
                                                <div class="col-md-4">
                                                    <input type="checkbox" class="checkbox border-primary" runat="server" id="chkAppLogin" />
                                                </div>

                                                <div class="col-md-2">
                                                    <label class="control-label">IP Address</label>
                                                </div>
                                                <div class="col-md-4">
                                                    <select class="form-control input-sm border-primary" id="dllIpAddress" runat="server">
                                                    </select>
                                                    <%--<textarea runat="server" class="form-control input-sm border-primary" id="txtIpAddress"></textarea>--%>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-md-2">
                                                <label class="control-label">Assign Device</label>
                                            </div>
                                            <div class="col-md-4">
                                                <select class="form-control input-sm border-primary" id="ddlAssignDevice" runat="server">
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <img src="/images/Emp_Default.png" class="imagePreview border-primary" id="imgPreview" width="100%" style="margin-bottom: 10px" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 form-group">
                                                <input type="file" class="form-control border-primary" id="filePhoto" runat="server" onchange="readURL(this)" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" runat="server" class="btn btn-success buttonAnimation round box-shadow-1  btn-sm" data-animation="pulse" id="btnSave">Save</button>
                                    </div>
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" runat="server" class="btn btn-secondary  buttonAnimation round box-shadow-1  btn-sm" data-animation="pulse" id="btnReset">Reset</button>
                                    </div>
                                    <%
                                        if (Session["EmpTypeNo"] != null)
                                        {
                                            if (Session["EmpTypeNo"].ToString() == "1")
                                            {
                                    %>
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" runat="server" class="btn btn-primary buttonAnimation round box-shadow-1  btn-sm" data-animation="pulse" id="btnUpdate" style="display: none">Update</button>
                                    </div>
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" runat="server" class="btn btn-warning buttonAnimation round box-shadow-1 btn-sm" id="btnCancel" data-animation="pulse" style="display: none">Cancel</button>
                                    </div>
                                    <%
                                            }
                                        }
                                    %>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <%
                    }
                }
            %>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Employee List</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 form-group">
                                        <select class="form-control input-sm border-primary" id="ddlSEmpType">
                                            <option value="0">All Employee</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <input type="text" class="form-control input-sm border-primary" placeholder="Employee Code" id="txtSEmployeeId" onfocus="this.select()" />
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <input type="text" class="form-control input-sm border-primary" placeholder="Employee Name" id="txtSEmployeeName" onfocus="this.select()" />
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-3 form-group">
                                        <input type="text" class="form-control input-sm border-primary" placeholder="Email" id="txtSEmail" onfocus="this.select()" />
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <select class="form-control input-sm border-primary" id="ddlSStatus">
                                            <option value="2">All Status</option>
                                            <option value="1" selected>Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                    </div>
                                    <div class="col-md-1 form-group">
                                        <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1 btn-sm" id="btnSearch">Search</button>
                                    </div>
                                </div>
                                <div class="row form-group ">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblEmployeeList">
                                                <thead class="bg-blue white">
                                                    <tr>

                                                        <td class="Action text-center" style="width: 70px;">Action</td>

                                                        <td class="Status text-center">Status</td>
                                                        <td class="AutoId" style="display: none">AutoId</td>
                                                        <td class="EmployeeCode text-center">Employee Code</td>
                                                        <td class="Name">Employee Name</td>
                                                        <td class="Profile">Profile Name</td>
                                                        <td class="UserName">User Name</td>
                                                        <td class="Type">User Type</td>
                                                        <td class="AssignDevice">Assign Device</td>
                                                        <td class="Address">Address</td>
                                                        <td class="Email">Email</td>
                                                        <td class="Contact text-center">Contact No</td>
                                                        <td class="LoginDate text-center">Last Login Date</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <select id="ddlPageSize" class="form-control input-sm border-primary pagesize" onchange="getEmpRecord(1);">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager text-right"></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div id="ModalChangePassord" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-md" style="min-width: 250px">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-content">
                    <div class="modal-header">
                        <input type="hidden" id="EmpAutoId" />
                        <h4 class="modal-title"><b>User Info : </b>
                            <label id="lblemployeeName"></label>
                        </h4>
                        <button type="button" class="close pull-right" data-dismiss="modal" style="margin-right: 0;">×</button>
                    </div>
                    <div class="modal-body">
                         <div class="form-group row">
                            <div class="col-md-4">User Name</div>
                            <div class="col-md-8">
                                <div class="input-group">
                                    <input type="text" id="txtUserNameP" disabled class="form-control border-primary passreq input-sm" />
                                    
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4">Current Password</div>
                            <div class="col-md-8">
                                <div class="input-group">
                                    <input type="password" id="txtoldpassword" disabled class="form-control border-primary passreq input-sm" />
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                            <i class="la la-eye" id="ViewPsd3" onclick="myFunction2()"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-4">New Password <span class="required">*</span></div>
                            <div class="col-md-8">
                                <div class="input-group">
                                    <input type="password" id="txtNewpassword" maxlength="20" class="form-control border-primary passreq input-sm" />
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                            <i class="la la-eye" id="ViewPsd2" onclick="myFunction1()"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4">Confirm Password <span class="required">*</span></div>
                            <div class="col-md-8">
                                <input type="password" id="txtConfNewpassword" maxlength="20" class="form-control border-primary passreq input-sm" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4">Is Apply IP</div>
                            <div class="col-md-8">
                                <input type="checkbox" id="chkisApplyIp" class="border-primary passreq input-sm" />
                            </div>
                        </div>
                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1  btn-sm" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" id="btnchanagepass" onclick="changepass()">Update</button>
                </div>
            </div>
        </div>
    </div>
    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="/Admin/JS/employeeMaster.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
</asp:Content>
