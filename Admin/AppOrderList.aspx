﻿<%@ Page Language="C#" MasterPageFile="~/Admin/MasterPage.master"  AutoEventWireup="true" CodeFile="AppOrderList.aspx.cs" Inherits="Admin_AppOrderList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
       .anyClass {
            height: 400px;

            overflow-y: scroll;
        }
        .incdec {
            width: 43px !important;
            height: 28px !important;
            border-radius: 2px;
            border: 1px blue inset;
        }
        .pad{padding:0 !important}
        #tblRouteDetails th, #tblRouteDetails td {
            padding: 5px 5px !important;
        }
        #tblUnscheduledOrder th, #tblUnscheduledOrder td {
            padding: 5px 5px !important;
        }
        #tblDrvList th, #tblDrvList td {
            padding: 5px 5px !important;
        }
        .addScroll {
            overflow-y: auto;
            height: 450px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Order List</h3>       
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage Order</a></li>
                        <li class="breadcrumb-item">App Order List</li>
                    </ol>
                </div>
            </div>
        </div>
               <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                <%--    <button type="button" class="dropdown-item" onclick="checkSecurity()" data-animation="pulse" id="btnAssignRoot">Assign Route</button>
                    <button type="button" class="dropdown-item" onclick="popupPrintOption()">Print</button>--%>
                </div>
            </div>
        </div>
        
    </div>

    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <input id="HDDomain" runat="server" type="hidden" />
                            <input type="hidden" id="txtHOrderAutoId" class="form-control input-sm" />

                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row container-fluid">
                                    <div class="col-md-4 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlCustomer" runat="server">
                                            <option value="0">All Customer</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 form-group" id="SalesPerson">
                                        <select class="form-control border-primary input-sm" id="ddlSalesPerson" runat="server">
                                            <option value="0">All Sales Person</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Order No" id="txtSOrderNo" onfocus="this.select()" />
                                    </div>

                                    <div class="col-md-4 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    From Order Date<span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" placeholder="Order From Date" id="txtSFromDate" onfocus="this.select()" />

                                        </div>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text " style="padding: 0rem 1rem; ">
                                                    To Order Date<span class="la la-calendar-o"></span>

                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" placeholder="Order To Date" id="txtSToDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-md-1 form-group">
                                        <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnSearch" onclick="Search()">Search</button>
                                    </div>
                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <input type="hidden" id="hiddenPackerAutoId" runat="server" />
                                <input type="hidden" id="hiddenEmpType" runat="server" />
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblOrderList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="action text-left"></td>
                                                        <td class="orderNo text-center">Order No</td>
                                                        <td class="orderDt text-center">Order Date</td>
                                                        <td class="SalesPerson">Sales Person</td>
                                                        <td class="cust">Customer</td>
                                                        <td class="product text-center">Products</td>
                                                        <td class="CreditMemo text-center">Credit Memo</td>
                                                        <td class="Shipping text-center">Shipping Type</td>
                                                        <td class="OrderAutoId text-center" style="display:none;">OrderAutoId</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row container-fluid">
                                    <div class="">
                                        <div class="form-group">
                                            <select id="ddlPageSize" class="form-control border-primary input-sm" onchange="PageSize()">
                                                <option selected="selected" value="10">10</option>
                                                <option value="50">50</option>
                                                <option value="100">100</option>
                                                <option value="500">500</option>
                                                <option value="1000">1000</option>
                                                <option value="0">All</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-10 col-sm-12">
                                        <div class="Pager"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</asp:Content>

