﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_ErrorTicketLog : System.Web.UI.Page
{
     
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["EmpType"] == null)
        {
            Response.Redirect("/");
        }
        if (Session["EmpTypeNo"].ToString() != null)
        {
            hiddenEmpType.Value = Session["EmpTypeNo"].ToString();
        }
        if (Session["EmpTypeNo"].ToString() == "1" || Session["EmpTypeNo"].ToString() == "8")
        {
            hide.Visible = true;
            divCustomerType.Visible = true;
        }
        else
        {
            hide.Visible = false;
            divCustomerType.Visible = false;
        }        
    }
   
}