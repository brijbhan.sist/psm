﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true"
    CodeFile="AsignPage.aspx.cs" Inherits="Admin_AsignPage" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        #tblBrand_wrapper {
            padding: 0;
        }

        .desc {
            margin-bottom: 7px;
        }

        .addActionBtn {
            cursor: pointer;
        }

        .hideAutoId {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Assign Module</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage Page</a></li>
                        <li class="breadcrumb-item">Assign Module</li>
                    </ol>
                </div>
            </div>
        </div>
        <%--<div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a href="/Admin/ManagePageAccess.aspx"><button type="button" class="dropdown-item">Manage Page Access</button></a>
                </div>
            </div>
        </div>--%>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Page Details</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row" id="panelPageMaster">
                                    <div class="col-md-6">


                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label">Module</label>
                                                <span class="required">*</span>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <select class="form-control input-sm border-primary req" id="txtModule" runat="server" onchange="bindSubModule(this.value)">
                                                    <option value="">Select</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label">Page</label>
                                                <span class="required">*</span>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <select class="form-control input-sm border-primary req" id="textPageName" runat="server">
                                                    <option value="0">Select</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label">Location</label>
                                                <span class="required">*</span>
                                            </div>
                                            <div class="col-md-8 form-group" id="locationHtml">
                                                
                                            </div>
                                        </div>
                                        <input type="hidden" id="hiddenEditAutoId" />
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label">Sub Module</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <select class="form-control input-sm border-primary" id="txtSubModule" runat="server" onchange="bindPageName(this.value)">
                                                    <option value="0">Select</option>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="btn-group mr-1 pull-right">
                                                <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="saveBtn" onclick="save()">Assign</button>
                                            </div>
                                            <div class="btn-group mr-1 pull-right">
                                                <button type="button" class="btn btn-secondary  buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" onclick="clearField()" id="btnReset">Reset</button>
                                            </div>
                                            <div class="btn-group mr-1 pull-right">
                                                <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnUpdate" style="display: none" onclick="Update();">Update</button>
                                            </div>
                                            <div class="btn-group mr-1 pull-right">
                                                <button type="button" class="btn btn-warning buttonAnimation pull-right round box-shadow-1 btn-sm" id="btnCancel" data-animation="pulse" style="display: none">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Module List</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 form-group">
                                        <select class="form-control input-sm" id="searchLocation">
                                            <option value="">All Location</option>
                                        </select>
                                    </div>
                                    <%--<div class="col-md-3 form-group">
                                        <select class="form-control input-sm" id="searchPage">
                                            <option value="">All Page</option>
                                        </select>
                                    </div>--%>
                                    <div class="col-md-3 form-group">
                                        <select class="form-control input-sm" id="searchModule" onchange="bindSubModule(this.value)">
                                            <option value="">All Module</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <select class="form-control input-sm" id="searchSubModule" >
                                            <option value="">All Sub Module</option>
                                        </select>
                                    </div>
                                    
                                    <div class="col-md-3 form-group">
                                        <button type="button" class="btn btn-info buttonAnimation pull-left round box-shadow-1 btn-sm" onclick="getAssignedModuleList();">Search</button>
                                    </div>
                                </div>

                                <br />
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered" id="moduleList">
                                        <thead class="bg-blue white">
                                            <tr>
                                                <td class="action text-center">Action</td>
                                                <td class="AutoId" style="display: none">AutoId</td>
                                                <td class="HiddenStatus" style="display: none">Status</td>
                                                <td class="HiddenModule" style="display: none">Module</td>
                                                <td class="HiddenSubModule" style="display: none">SubModule</td>
                                                <td class="HiddenPage" style="display: none">Page</td>
                                                <td class="HiddenLocation" style="display: none">Location</td>
                                                <td class="Module text-center">Module</td>
                                                <td class="SubModule">Sub Module</td>
                                                <td class="PageName">Page Name</td>
                                                <%--<td class="PageUrl">PageUrl</td>--%>
                                                <td class="Location">Location</td>
                                                <td class="Status text-center">Status</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                    <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</asp:Content>
