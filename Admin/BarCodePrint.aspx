﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="BarCodePrint.aspx.cs" Inherits="Admin_BarCodePrint" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Print Barcode</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Products</a></li>
                        <li class="breadcrumb-item active">Print Barcode</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">

                                    <div class="col-md-3 form-group" >
                                        <select class="form-control border-primary input-sm " id="ddlLabelOption" onchange="bindEnableField()">
                                            <option value="0">Select Barcode Template</option>
                                            <option value="4"> 7 Eleven</option>
                                            <option value="1"> 1" x 0.5" [UPC A : 11 or 12 Digit]</option>
                                            <option value="2"> 1.5" x 1" [UPC A : 11 or 12 Digit]</option>
                                            <option value="3"> 1.5" x 1" [Code 128 : 14 Digit]</option>
                                        </select>
                                    </div>
                                     <div class="col-md-5 form-group" >
                                         <h4 id="tempMessage" style="margin:0"></h4>
                                         </div>
                                </div>
                                <div class="row">

                                    <div class="col-md-3 form-group" style="display: none;">
                                        <select class="form-control border-primary input-sm " id="ddlSCategory" onchange="bindSubcategory()">
                                            <option value="0">Select Category</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group" style="display: none;">
                                        <select class="form-control border-primary input-sm" id="ddlSSubcategory" onchange="bindProduct()">
                                            <option value="0">Select Subcategory</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <select class="form-control border-primary input-sm ddlreqs " disabled id="ddlProduct" onchange="bindUnit()">
                                            <option value="0">Select Product</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <select class="form-control border-primary input-sm ddlreqs" disabled id="ddlUnitType">
                                            <option value="0">Select Unit</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <input type="text" id="txtQty" maxlength="3" class="form-control border-primary input-sm req" disabled placeholder="Qty To Print" />

                                    </div>
                                    <div class="col-md-3 form-group">
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" id="Add" onclick="getbarcodeDetails()">Add</button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-12 ">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="action text-center width3per">Action</td>
                                                        <td class="ProductId text-center width3per">Product ID</td>
                                                        <td class="ProductName text-center width3per">Product Name </td>
                                                        <td class="Unit text-center width3per">Unit Type</td>
                                                        <td class="Barcode text-center width3per">Barcode</td>
                                                        <td class="Qty text-center width3per">Qty To  Print</td>
                                                        <td class="SRP text-center width3per">SRP</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <div class="btn-group mr-1">
                                        <button type="button" class="btn btn-success buttonAnimation round box-shadow-1  btn-sm" data-animation="pulse" style="display: none;margin:0 3px 0 3px" id="btnprint" onclick="printtemplate();">Print</button>
                                        <button type="button" class="btn btn-default buttonAnimation round box-shadow-1  btn-sm" id="Reset" onclick="resetField()">Reset</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div id="showBarcodeModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content" style="width: 600px">
                <div class="modal-header">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="modal-title">Select Barcode</h4>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-4 form-group">
                                    <label>Product ID</label>
                                </div>
                                <div class="col-md-8 form-group">
                                    <label id="ProductId"></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 form-group">
                                    <label>Product Name</label>
                                </div>
                                <div class="col-md-8 form-group">
                                    <label id="ProductName"></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 form-group">
                                    <label>Unit Type</label>
                                </div>
                                <div class="col-md-8 form-group">
                                    <label id="Unit"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" id="tblBarcode">
                                <thead class="bg-blue white">
                                    <tr>
                                        <td class="Barcode text-center">Barcode</td>
                                        <td class="LastUsesDate text-center">Last Used Date</td>
                                        <td class="Count text-center">Count</td>
                                        <td class="Action text-center">Action</td>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse" onclick="closModal()" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="PopPrintTemplate" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="display: block">
                    <h4 class="modal-title">Select Barcode Print Template</h4>

                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <div class="container">

                            <div class="row form-group">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <input type="radio" name="Template" id="Templ1" />
                                    7 Eleven
                                </div>
                            </div>
                           
                             <div class="row form-group">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <input type="radio" name="Template" id="Templ2" />
                                     1" x 0.5"
                                </div>
                            </div> 
                            <div class="row form-group">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <input type="radio" name="Template" id="Templ3" />
                                    1.5" x 1"
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1 btn-sm" onclick="Print()">Print</button>
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

