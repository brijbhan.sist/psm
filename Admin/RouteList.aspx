﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="RouteList.aspx.cs" Inherits="Admin_RouteList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .table th, .table td {
            padding: 3px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Route List</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Application</a>
                        </li>  
                        <li class="breadcrumb-item"><a href="#">Manage Route</a>
                        </li> 
                         <li class="breadcrumb-item">Route List
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
          <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
            <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
            id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false"> Action</button>
            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                <button id="Button1" type="button" onclick="location.href='/Admin/RouteEntry.aspx'" class="dropdown-item" animation="pulse">
                          Add New Route</button>                
            <input type="hidden" id="hiddenPackerAutoId" runat="server" />
            <input type="hidden" id="hiddenEmpType" runat="server" />
            </div>
          </div>
        </div>       
    </div>

    <div class="content-body">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                 <div class="row">
                                    <div class="col-md-3" >
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Route Name" id="txtRoutName" onfocus="this.select()" />
                                    </div>
                                    <div class="col-md-3" id="sales" runat="server">
                                        <select class="form-control border-primary input-sm" id="ddlSalePerson" runat="server">
                                            <option value="0">All Sales Person</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <select class="form-control border-primary input-sm" id="ddlSStatus">
                                            <option value="2">All Status</option>
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" id="btnSearch" data-animation="pulse">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body" style="padding: 1.5rem !important;">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblRouteList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="action text-center width3per">Action</td>
                                                        <td class="AutoId text-center width3per" style="display: none">Auto ID</td>
                                                        <td class="RouteId text-center width3per">Route ID</td>
                                                        <td class="RouteName">Route Name</td>
                                                        <td class="SalesPerson">Sales Person</td>
                                                        <td class="NoZip text-center width3per">Customer</td>
                                                        <td class="status text-center width3per">Status</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row container-fluid">
                                    <div class="">
                                        <select id="ddlPageSize" onchange="GetRouteList(1)" class="form-control border-primary input-sm">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager" id="pager"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="/Admin/JS/RouteList.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
</asp:Content>

