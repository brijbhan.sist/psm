﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="EmailServerMaster.aspx.cs" Inherits="Admin_EmailServerMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Email Server Setting</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Manage Application</a>
                        </li>
                        <li class="breadcrumb-item">Email Server Setting
                        </li>
                    </ol>
                </div>
            </div>
        </div> 
    </div>

    <div class="content-body">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-md-10">                                        
                                        <h4 class="card-title">Email Server Details</h4>
                                        <input type="hidden" id="hfhidden" />
                                    </div>
                                    <div class="col-md-2">
                                          <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1  btn-sm" id="btnCheckmail">Test Mail</button>

                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-3"> 
                                         <label class="control-label">Send To <span class="required">*</span></label>                                     
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <select id="ddlUName" class="form-control border-primary input-sm server">
                                            <option value="0">-Select-</option>
                                            <option value="Client">Client</option>
                                            <option value="Developer">Developer</option>
                                            <option value="Internal">Internal</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                         <label class="control-label">Email ID <span class="required">*</span></label>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                         <input type="text" id="txtEmailid" maxlength="80" class="form-control border-primary input-sm server" onchange="validateEmail(this);"/>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-3">   
                                         <label class="control-label">Password <span class="required">*</span></label>                                   
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <input type="password" id="txtPassword"  class="form-control border-primary input-sm server" />
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                         <label class="control-label">SMTP Server <span class="required">*</span></label>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                          <input type="text" id="txtServer" class="form-control border-primary input-sm server" />
                                    </div>
                                </div>
                                 <div class="row form-group">
                                    <div class="col-sm-12 col-md-3">  
                                        <label class="control-label">SMTP Port <span class="required">*</span></label>                                    
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <input type="text" id="txtport" maxlength="10" onkeypress="return isNumberKey(event)" class="form-control border-primary input-sm server"/>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                         <label class="control-label"> SSL</label>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <input type="checkbox" id="chkSsl" class="border-primary" />
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-12 text-right">
                                        <button type="button" class="btn btn-secondary buttonAnimation round box-shadow-1  btn-sm" onclick="resetData()" id="btnClear">Reset</button>
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" style="display: none" id="btnUpdate">Update</button>
                                        <button type="button" class="btn btn-success buttonAnimation round box-shadow-1  btn-sm" id="btnSave">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="content-body">
        <section id="drag-area3">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-header">
                                <h4 class="card-title">Email Server List</h4>
                            </div>
                            <div class="card-body">
                                 <div class="row form-group ">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblEmailServerList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="Action text-center">Action</td> 
                                                        <td class="SendTo">Send To</td>
                                                        <td class="Email">Email ID</td>
                                                        <td class="PORT text-center">SMTP Port</td>
                                                        <td class="Server">SMTP Server</td>
                                                        <td class="SSL text-center">SSL</td>                                                      
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="Pager"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div> 

    <div id="TestEmailPop" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content" style="margin-top: 200px;">
                <div class="modal-header">
                    <h4 class="modal-title">Test Email</h4>
                </div>
                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col-sm-12 col-md-2"></div>
                        <div class="col-sm-12 col-md-3">
                            <label class="control-label">Send To <span class="required">*</span></label>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <select id="ddlUType" class="form-control border-primary input-sm mail">
                                <option value="0">-Select-</option>
                                <option value="Client">Client</option>
                                <option value="Developer">Developer</option>
                                <option value="Internal">Internal</option>
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                         <div class="col-sm-12 col-md-2"></div>
                         <div class="col-sm-12 col-md-3">
                             <label class="control-label">Email ID <span class="required">*</span></label>
                         </div>
                        <div class="col-sm-12 col-md-4">
                            <input type="text" id="txtTestEmail" class="form-control border-primary input-sm mail" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer text-right">
                    <button type="button" class="btn btn-primary buttonAnimation round box-shadow-1  btn-sm" id="btnSendmail">Send</button>
                     <button type="button" class="btn btn-danger buttonAnimation round box-shadow-1  btn-sm" onclick="resetData()"  id="btnClose">Close</button>
                </div>
            </div>
        </div>
    </div>
        <script type="text/javascript">
            document.write('<scr' + 'ipt type="text/javascript" src="/Admin/JS/EmailServer.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script> 
</asp:Content>

