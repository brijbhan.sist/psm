﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="ZoneList.aspx.cs" Inherits="Admin_ZoneList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <input type="hidden" id="hiddenPackerAutoId" runat="server" />
            <input type="hidden" id="hiddenEmpType" runat="server" />
            <h3 class="content-header-title">Zone List</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage Application</a></li>
                        <li class="breadcrumb-item">Manage Zone</li>
                    </ol>
                </div>
            </div>
        </div>

        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a class="dropdown-item" href="/Admin/ZoneEntry.aspx" id="linkAddNewProduct" runat="server">Add Zone</a>
                </div>
            </div>
        </div>

    </div>

    <div class="content-body">
        <section id="drag-area">

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 form-group">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Zone Name" id="txtSZoneName" onfocus="this.select()" />
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlSDriver" runat="server" style="width:100%;">
                                            <option value="0">All Driver</option>
                                        </select>
                                    </div>
                                
                                    <div class="col-md-3 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlSCar" runat="server" style="width:100%;">
                                            <option value="0">All Car</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlSStatus">
                                            <option value="2">All Status</option>
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnSearch">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblZoneList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="action text-center width3per">Action</td>
                                                        <td class="status text-center width3per">Status</td>
                                                        <td class="ZoneId" style="display: none">Zone ID</td>
                                                        <td class="Zone">Zone Name</td>
                                                        <td class="Driver">Driver</td>
                                                        <td class="Car">Car</td>
                                                        <td class="NoZip text-center width3per">No. Of Zip</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="Pager"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="/Admin/JS/ZoneList.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
</asp:Content>

