﻿$(document).ready(function () {
    BindPriceLevel(1);
    BindStatus();
});

function Pagevalue(e) {    
    BindPriceLevel(parseInt($(e).attr("page")));
};

function BindPriceLevel(pageIndex) {
   
    var data = {
        PriceLevelName: ($("#txtSPriceLevelName").val()).trim(),
        Status: parseInt($("#ddlSPLStatus").val()),
        CustomerType: parseInt($("#ddlCustomerType").val()),
        PageSize: $("#ddlPageSize").val(),
        PageIndex: pageIndex       
    };
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WPriceLevelList.asmx/BindPriceLevel",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),       
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
       
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var priceLevelList = $(xmldoc).find("Table");

            $("#tblPriceLevelList tbody tr").remove();
            var row = $("#tblPriceLevelList thead tr").clone(true);
            if (priceLevelList.length > 0) {
                $("#EmptyTable").hide();
                $.each(priceLevelList, function () {
                    $(".PriceLevelId", row).text($(this).find("PriceLevelId").text());
                    $(".PriceLevelName", row).text($(this).find("PriceLevelName").text());
                    $(".Customer", row).html($(this).find("Customer").text());
                    $(".CustomerCount", row).html($(this).find("TotalCustomer").text());
                    $(".CreatedBy", row).text($(this).find("CreatedBy").text());
                    $(".CreatedOn", row).text($(this).find("CreatedDate").text()); 
                    if ($(this).find("StatusType").text() == 'Active') {
                        $(".Status", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("StatusType").text() + "</span>");
                    } else {
                        $(".Status", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("StatusType").text() + "</span>");

                    }
                   
                    $(".Action", row).html("<a title='Edit' href='/Admin/PriceTemplate.aspx?PLAutoId=" + $(this).find("PriceLevelAutoId").text()
                        + "'><span class='ft-edit'></span></a><a title='Print' href='javascript:void(0)' onclick='PrintOrderPop(" + $(this).find("PriceLevelAutoId").text()
                        + ")'>&nbsp;&nbsp;&nbsp;&nbsp;<span class='icon-printer'></span></a>");
                    $("#tblPriceLevelList tbody").append(row);
                    row = $("#tblPriceLevelList tbody tr:last").clone(true);
                });
            } else {
                $("#EmptyTable").show();
            }

            var pager = $(xmldoc).find("Table1");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
        },
        error: function (result) { 
        },
        failure: function (result) { 
        }
    });
}
//function PrintLevel(PriceLevelAutoId) {
//    window.open("/Admin/PriceLevelPrint.html?PLAutoId=" + PriceLevelAutoId, "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");

//}
//function PrintLevelNew(PriceLevelAutoId) {
//    window.open("/Admin/PriceLevelPrint2.html?PLAutoId=" + PriceLevelAutoId, "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
//}

function PrintOrderPop(PriceLevelAutoId) {
    $('#txtPrintOrderPop').val(PriceLevelAutoId);
    $("#chkdefault").prop('checked', true);
    $('#PopPrintTemplate').modal('show');
}

function PrintOrder() {
    $('#PopPrintTemplate').modal('hide');
    var PriceLevelAutoId = $('#txtPrintOrderPop').val();
    if ($("#chkdefault").prop('checked') == true) {
        window.open("/Admin/PriceLevelPrint.html?PLAutoId=" + PriceLevelAutoId, "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    } else if ($("#checktemplate1").prop('checked') == true) {
        window.open("/Admin/PriceLevelPrint2.html?PLAutoId=" + PriceLevelAutoId, "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
}







function BindStatus() {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WPriceLevelList.asmx/BindStatus",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
       
        async: false,
        success: function (response) {
            if (response.d != 'Session Expired') {
                var xmldoc = $.parseXML(response.d);
                var statusList = $(xmldoc).find("Table");                
                var CustomerType = $(xmldoc).find("Table1");                

                $("#ddlSPLStatus option:not(:first)").remove();
                $.each(statusList, function () {
                    $("#ddlSPLStatus").append("<option value=" + $(this).find("AutoId").text() + ">" + $(this).find("StatusType").text() + "</option>");
                });

                $("#ddlCustomerType option:not(:first)").remove();
                $.each(CustomerType, function () {
                    $("#ddlCustomerType").append("<option value=" + $(this).find("AutoId").text() + ">" + $(this).find("CustomerType").text() + "</option>");
                });
                $("#ddlCustomerType").select2();

            } else {
                location.href = '/';
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}