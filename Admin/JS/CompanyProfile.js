﻿var imagelogo = "";
$(document).ready(function () {
    $('#txtSubscriptionAlertDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtSubscriptionExpiry').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });

    $('#StartDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
      EditCompany();
});



/*-----------------------------------------------------Show Company Details---------------------------------------------------------------------------*/
function EditCompany() {
    $.ajax({
        type: "POST",
        url: "WebAPI/CompanyProfile.asmx/EditCompany",
        data: "{}",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        beforeSend: function () { 
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>', 
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: OnSuccessofEdit,
        error: function (response) {
        }
    });
};
function OnSuccessofEdit(response) {
    var xmldoc = $.parseXML(response.d);
    var xml = $(xmldoc);
    var Company = xml.find("Table");
    var appLogout = xml.find("Table1");
   
    console.log(response.d);
    $("#txtCompanyId").val($(Company).find("CompanyId").text());
    $("#txtCompanyName").val($(Company).find("CompanyName").text());
    $("#txtAddressLine1").val($(Company).find("Address").text());
    $("#txtWebsite").val($(Company).find("Website").text());
    $("#txtMobileNo").val($(Company).find("MobileNo").text());
    $("#txtEmailId").val($(Company).find("EmailAddress").text());
    $("#txtFaxNo").val($(Company).find("FaxNo").text());
    $("#TextDateDifference").val($(Company).find("DateDifference").text());
    $("#currentVersion").val($(Company).find("currentVersion").text());
    $("#txtStartLatitude").val($(Company).find("optimoStart_Lat").text());
    $("#txtStartLongitude").val($(Company).find("optimoStart_Long").text());
    $("#txtSubscriptionAlertDate").val($(Company).find("SubscriptionAlertDate").text());
    $("#txtDriverLimit").val($(Company).find("OptimoDriverLimit").text());
    $("#StartDate").val($(Company).find("StartDate").text());
    $("#txtSubscriptionExpiry").val($(Company).find("SubscriptionExpiryDate").text());
    $('#txtTermsCondition').summernote('code', $(Company).find("TermsCondition").text());
    $("#imgPreview").attr("src", "../Img/logo/" + $(Company).find("Logo").text())
    imagelogo = $(Company).find("Logo").text();
    $("#appLogoutTime").val($(appLogout).find("LogoutTime").text());

    $("#btnUpdate").show();
    
};

/*-----------------------------------------------------Update Category Detail---------------------------------------------------------------------------*/
$("#btnUpdate").click(function () {
    if (checkRequiredField()) {
        var timeStamp = Date.parse(new Date());
        var logo = "";
        if ($("#txtfileUpload").val() != "") {
            logo = timeStamp + '_' + $("#txtfileUpload").val().split('\\').pop()
        }
        else {
            logo = imagelogo;
        }
        var data = {
            CompanyId: $("#txtCompanyId").val(),
            CompanyName: $('#txtCompanyName').val(),
            Address: $("#txtAddressLine1").val(),
            Website: $("#txtWebsite").val(),
            MobileNo: $("#txtMobileNo").val(),
            FaxNo: $('#txtFaxNo').val(),
            SubscriptionAlertDate: $("#txtSubscriptionAlertDate").val(),
            SubscriptionExpryDate: $("#txtSubscriptionExpiry").val(),
            TermsCondition: $('#txtTermsCondition').summernote('code'),
            Logo: logo,
            EmailAddress: $("#txtEmailId").val(),
            StartDate: $("#StartDate").val(),
            currentVersion: $("#currentVersion").val(),
            appLogoutTime: $("#appLogoutTime").val(),
            DateDifference: $("#TextDateDifference").val(),
            StartLatitude: $("#txtStartLatitude").val(),
            StartLongitude: $("#txtStartLongitude").val(),
            OptimoDriverLimit: $("#txtDriverLimit").val()
        }
        $.ajax({
            type: "POST",
            url: "WebAPI/CompanyProfile.asmx/UpdateCompanyDetails",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (data) {
                if (data.d == 'true') {
                    if ($("#txtfileUpload").val() != "") {
                        var fileUpload = $("#txtfileUpload").get(0);
                        var files = fileUpload.files;
                        var test = new FormData();
                        for (var i = 0; i < files.length; i++) {
                            test.append(files[i].name, files[i]);
                        }
                        $.ajax({
                            url: "imageUploadHandler.ashx?timestamp=" + timeStamp,
                            type: "POST",
                            contentType: false,
                            processData: false,
                            data: test,
                            success: function (result) {
                                Console.log(result)
                            },
                            error: function (err) {

                            }
                        });
                        EditCompany();
                    }
                    swal("", "Company details updated successfully.", "success");
                }
                else {
                    swal("Error!", data.d, "error");
                }
            },
            error: function (result) {
                swal("Error!", "Oops! Something went wrong.Please try later.", "error");
            },
            failure: function (result) {
                swal("Error!", "Oops! Something went wrong.Please try later.", "error");
            }
        });
    } else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
});
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#imgPreview').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$("#txtfileUpload").change(function () {

    readURL(this);
});

function checkMessage(n) {
    var st, et;
    if (n == 1) {
        st = minFromMidnight($("#txtStartTime").val());
        et = minFromMidnight($("#txtExpiryTime").val());
        if (st > et) {
            toastr.error('End time must be greater than start time.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            $("#txtExpiryTime").val('');
        }
    }
    else {
        st = minFromMidnight($("#txtStartTime").val());
        et = minFromMidnight($("#txtExpiryTime").val());
        if (st > et) {
            toastr.error('End time must be greater than start time.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            $("#txtExpiryTime").val('');
        }
    }
    function minFromMidnight(tm) {
        var ampm = tm.substr(-2)
        var clk = tm.substr(0, 5);
        var m = parseInt(clk.match(/\d+$/)[0], 10);
        var h = parseInt(clk.match(/^\d+/)[0], 10);
        h += (ampm.match(/pm/i)) ? 12 : 0;
        return h * 60 + m;
    }
}