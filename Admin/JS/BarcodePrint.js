﻿$(document).ready(function () {
    if ($("#hiddenForPacker").val() == "") {
        $("#btnBulkUpload").show();
    }
    //BindCategory()
    //bindSubcategory();
    bindProduct();

});
/*---------------------------------Bind  Category-----------------------------------*/
function BindCategory() {
    $.ajax({
        type: "POST",
        url: "BarCodePrint.aspx/BindCategory",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: false,
        cache: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },

        success: function (response) {
            if (response.d != "Session Expired") {
                var getData = $.parseJSON(response.d);
                $("#ddlSCategory option:not(:first)").remove();
                $.each(getData, function (index, item) {
                    $("#ddlSCategory").append("<option value='" + getData[index].AutoId + "'>" + getData[index].CategoryName + "</option>");
                });
                $("#ddlSCategory").select2();
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}
/*-------------------------------------------------------Bind Subcategory-----------------------------------------------------------*/
function bindSubcategory() {
    var categoryAutoId = $("#ddlSCategory").val();
    $.ajax({
        type: "POST",
        url: "BarCodePrint.aspx/BindSubCategory",
        data: "{'categoryAutoId':'" + categoryAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var getData = $.parseJSON(response.d);
            $("#ddlSSubcategory option:not(:first)").remove();
            $.each(getData, function (index, item) {
                $("#ddlSSubcategory").append("<option value='" + getData[index].AutoId + "'>" + getData[index].SubcategoryName + "</option>");
            });
            $("#ddlSSubcategory").select2();
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
/*-------------------------------------------------------Bind Product-----------------------------------------------------------*/
function bindProduct() {
    var SubCategoryAutoId = $("#ddlSSubcategory").val();
    $.ajax({
        type: "POST",
        url: "BarCodePrint.aspx/BindAllProduct",
        data: "{'SubCategoryAutoId':'" + SubCategoryAutoId + "','PrintOption':'" + $("#ddlLabelOption").val() + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var getData = $.parseJSON(response.d);
            $("#ddlProduct option:not(:first)").remove();
            $.each(getData[0].Product, function (index, item) {
                $("#ddlProduct").append("<option value='" + item.ID + "'>" + item.PRM + "</option>");
            });
            $("#ddlProduct").select2();
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
/*-------------------------------------------------------Bind Unit-----------------------------------------------------------*/
function bindUnit() {
    var ProductAutoId = $("#ddlProduct").val();
    $.ajax({
        type: "POST",
        url: "BarCodePrint.aspx/BindAllUnit",
        data: "{'ProductAutoId':'" + ProductAutoId + "','PrintOption':'" + $("#ddlLabelOption").val() + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var getData = $.parseJSON(response.d);
            var Defaltunit = getData[0].Defaltunit
            $("#ddlUnitType option:not(:first)").remove();
            $.each(getData[0].Allunit, function (index, item) {
                $("#ddlUnitType").append("<option value='" + item.AutoId + "'>" + item.UnitType + "</option>");
            });
            $("#ddlUnitType").select2();
            
            try {
                $("#ddlUnitType").val(Defaltunit[0].PackingAutoId).change();
            } catch (e) {

            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function resetField() {
    $("#txtQty").val('');
    $("#tempMessage").html('');
    $("#ddlProduct").val('0').change();
    $("#ddlLabelOption").val('0');
    $("#ddlUnitType").val('0');
    $("#ddlLabelOption").removeAttr('disabled');
    $("#tblList tbody tr").remove();
}
function getbarcodeDetails() {
    if (Number($("#txtQty").val()) == 0) {
        toastr.error('Please enter a valid quantity.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        $("#txtQty").addClass('border-warning');
    }
    else {
        $("#txtQty").removeClass('border-warning');
        if (checkRequiredField1()) {
            var qty = $("#txtQty").val();
            var data = {
                ProductAutoId: $("#ddlProduct").val(),
                PrintOption: $("#ddlLabelOption").val(),
                UnitTypeAutoId: $("#ddlUnitType").val()
            };
            $.ajax({
                type: "POST",
                url: "BarCodePrint.aspx/getbarcodeDetails",
                data: "{'dataValues':'" + JSON.stringify(data) + "'}",
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.d != "Session Expired") {
                        var xmldoc = $.parseXML(response.d);
                        var Count = $(xmldoc).find("Table");

                        if (Count.find("TotalBarcodeCOUNT").text() > 1) {
                            var Barcode = $(xmldoc).find("Table1");
                            $("#tblBarcode tbody tr").remove();
                            var row = $("#tblBarcode thead tr").clone(true);
                            if (Barcode.length > 0) {
                                $("#EmptyTable").hide();
                                $.each(Barcode, function (index) {
                                    $(".Barcode", row).text($(this).find("Barcode").text());
                                    if ($(this).find("LastUsedDate").text() != "") {
                                        $(".LastUsesDate", row).text($(this).find("LastUsedDate").text());
                                    }
                                    else {
                                        $(".LastUsesDate", row).text("Never Used");
                                    }
                                    $(".Count", row).text($(this).find("count").text());
                                    $(".Action", row).html('<input type="radio" name="radio" group="radio" onclick="AddinList(\'' + $(this).find("Barcode").text() + '\');"/>');
                                    $("#tblBarcode tbody").append(row);
                                    row = $("#tblBarcode tbody tr:last").clone(true);
                                });
                                var productdetails = $(xmldoc).find("Table2");
                                $("#ProductId").text(productdetails.find("ProductId").text());
                                $("#ProductName").text(productdetails.find("ProductName").text());
                                $("#Unit").text(productdetails.find("UnitType").text());
                                $("#showBarcodeModal").modal("show");
                                $("#ddlLabelOption").attr('disabled', true);
                            }
                            else {
                                $("#EmptyTable").show();
                            }
                        }
                        else {

                            var items = $(xmldoc).find("Table1");
                            $("#emptyTable").hide();

                            if (items.length > 0) {

                                var row = $("#tblList thead tr").clone(true);
                                $(".action", row).html("<a title='Delete' href='#'><span class='ft-x' onclick= 'deleterecordB(this)' /></a>");
                                $(".ProductId", row).text(items.find("ProductId").text());
                                $(".ProductName", row).text(items.find("ProductName").text());
                                $(".Unit", row).text(items.find("UnitType").text());
                                $(".Barcode", row).text(items.find("Barcode").text());
                                $(".Qty", row).text(qty);
                                $(".SRP", row).text(items.find("SRP").text());
                                if ($('#tblList tbody tr').length > 0) {
                                    $('#tblList tbody tr:first').before(row);
                                }
                                else {
                                    $('#tblList tbody').append(row);
                                }
                                toastr.success('Product added successfully .', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                                $("#btnprint").show();
                                $("#ddlProduct").val(0).change();
                                $("#ddlUnitType").val(0).change();
                                $("#txtQty").val('');
                                $("#showBarcodeModal").modal("hide");
                                $("#ddlLabelOption").attr('disabled', true);
                            }
                            else {
                                if ($("#ddlLabelOption").val() == "1" || $("#ddlLabelOption").val() == "2" || $("#ddlLabelOption").val() == "3") {
                                    $("#ddlProduct").val(0).change();
                                    $("#ddlUnitType").val(0).change();
                                    $("#txtQty").val('');
                                    toastr.warning("Invalid product for selected template.", 'Message', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                                }
                            }
                        }
                    }
                    else {
                        location.href = '/';
                    }
                },
                error: function (result) {
                    alert(JSON.parse(result.responseText).d);
                },
                failure: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                }
            });
        }
        else {
            toastr.error('All fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        }
    }
}
function deleterecordB(e) {
    $(e).closest('tr').remove();
}
function AddinList(Barcode) {

    var qty = $("#txtQty").val();
    $.ajax({
        type: "POST",
        url: "BarCodePrint.aspx/AddinList",
        data: "{'Barcode':'" + Barcode + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },

        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var items = $(xmldoc).find("Table");
                $("#emptyTable").hide();
                if (items.length > 0) {

                    var row = $("#tblList thead tr").clone(true);
                    $(".action", row).html("<a title='Delete' href='#'><span class='ft-x' onclick= 'deleterecordB(this)' /></a>");
                    $(".ProductId", row).text(items.find("ProductId").text());
                    $(".ProductName", row).text(items.find("ProductName").text());
                    $(".Unit", row).text(items.find("UnitType").text());
                    $(".Barcode", row).text(items.find("Barcode").text());
                    $(".Qty", row).text(qty);
                    $(".SRP", row).text(items.find("SRP").text());
                    if ($('#tblList tbody tr').length > 0) {
                        $('#tblList tbody tr:first').before(row);
                    }
                    else {
                        $('#tblList tbody').append(row);
                    }
                    $("#showBarcodeModal").modal("hide");
                    toastr.success('Product added successfully .', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    $("#btnprint").show();
                    $("#ddlProduct").val(0).change();
                    $("#ddlUnitType").val(0).change();
                    $("#txtQty").val('');
                }
            }
            else {
                location.href = '/';
            }
        },
        failure: function (result) {
            swal("Error!", result.d, "error");
        },
        error: function (result) {
            swal("Error!", result.d, "error");
        }
    });
}
function printtemplate() {

    //$('#PopPrintTemplate').modal('show');
    Print();
}


function Print() {

    if ($('#tblList tbody tr').length > 0) {
        var BarcodeDetails = [];
        $("#tblList tbody tr").each(function (i) {
            BarcodeDetails.push({

                'ProductId': $(this).find('.ProductId').text(),
                'ProductName': $(this).find('.ProductName').text(),
                'Barcode': $(this).find('.Barcode').text(),
                'Unittype': $(this).find('.Unit').text(),
                'Qty': $(this).find('.Qty').text(),
                'SRP': $(this).find('.SRP').text()

            });

        });
        localStorage.setItem('BarcodeDetails', JSON.stringify(BarcodeDetails));
        if ($("#ddlLabelOption").val() == '4') {
            $('#PopPrintTemplate').modal('hide');
            window.open("/Admin/PrintBarcode.html", "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
        }
        else if ($("#ddlLabelOption").val() == '1') {
            $('#PopPrintTemplate').modal('hide');
            window.open("/Admin/PrintBarcode2.html", "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
        }
        else if ($("#ddlLabelOption").val() == '2') {
            $('#PopPrintTemplate').modal('hide');
            window.open("/Admin/PrintBarcode3.html", "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
       }
        else if ($("#ddlLabelOption").val() == '3') {
            $('#PopPrintTemplate').modal('hide');
            window.open("/Admin/PrintBarcode4.html", "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
        }
        //else {
        //    toastr.error('Select template first', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        //}
        //if ($("#Templ1").prop('checked') == true) {
        //    $('#PopPrintTemplate').modal('hide');
        //    window.open("/Admin/PrintBarcode.html", "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
        //}        
        //else if ($("#Templ2").prop('checked') == true) {
        //    $('#PopPrintTemplate').modal('hide');
        //    window.open("/Admin/PrintBarcode2.html", "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
        //}
        //else if ($("#Templ3").prop('checked') == true) {
        //    $('#PopPrintTemplate').modal('hide');
        //    window.open("/Admin/PrintBarcode3.html", "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
        //}
        //else {
        //    toastr.error('Select template first', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        //}

    }
    else {
        toastr.error('No items Added into the Table.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
}


function checkRequiredField1() {
    var boolcheck = true;
    $('.req').each(function () {
        if ($(this).val().trim() == '' || $(this).val().trim() == '0' || $(this).val().trim() == '0.00') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });
    $('.ddlreq').each(function () {
        if ($(this).val().trim() == '' || $(this).val().trim() == '0' || $(this).val().trim() == '-Select-' || $(this).val().trim() == null) {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });
    $('.ddlreqs').each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == 'undefined' || $(this).val() == null || $(this).val() == 'Select') {
            boolcheck = false;
            $(this).closest('div').find('.select2-selection--single').attr('style', 'border:1px solid #FF9149  !important');
        } else {
            $(this).removeClass('border-warning');
            $(this).closest('div').find('.select2-selection--single').removeAttr('style');
        }
    });
    return boolcheck;
}

function bindEnableField() {
    $("#ddlProduct").removeAttr('disabled');
    $("#ddlUnitType").removeAttr('disabled');
    $("#txtQty").removeAttr('disabled');
    $("#ddlLabelOption").removeClass('border-warning');
    if ($("#ddlLabelOption").val() == '1' || $("#ddlLabelOption").val() == '2') {
        $("#tempMessage").html('Only 11 or 12 digit barcode will be added.');
    }
    else if ($("#ddlLabelOption").val() == '3') {
        $("#tempMessage").html('Only 14 digit barcode will be added.');
    }
    else {
        $("#tempMessage").html('');
    }
    bindProduct();
}