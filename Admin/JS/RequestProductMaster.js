﻿var AutoId = 0;
var imgURL = '';
$(document).ready(function () {
    $('#btnManagePrince').attr('disabled', false);
    var hostName = $(location).attr('hostname');;
    var protocol = $(location).attr('protocol');
    imgURL = protocol + '//' + hostName;
    var getId = getProductId("ProductId");
    AutoId = getId;
    if (getId != '') {
        Bindlocation(AutoId);
        editProduct(getId);
        $("#panelPacking *").attr('disabled', false);
        $('#btnManagePrince').attr('disabled', false);
    }
    else {
        managePrice()
        bindCategory();
        Bindlocation(AutoId);
        $("#panelPacking *").attr('disabled', true);
        $('#btnManagePrince').attr('disabled', true);
    }
    $("#emptyTable").show();
});
function readDraftURL(input) {
    if ($(input).val() == '') {
        $("#imgProductImage").attr("src", "/images/default_pic.png");
    }

    var file = input.files[0];
    if (Math.round(file.size / (1024 * 1024)) > Number($("#getSize").html())) {
        toastr.error('You are reaching maximum limit. Max limit is ' + $("#getSize").html() + ' MB', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $("#fileImageUrl").val('');
        return true;
    }
    else {
        if (input.files && input.files[0] && input.files[0].name.match(/\.(jpg|JPG|JPEG|jpeg|png|PNG)$/)) {
            var reader = new FileReader();
            reader.onload = function (e) {
                document.getElementById('imgProductImage').src = e.target.result;
                $('#imgProductImage').attr('mainsrc', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
        else {
            toastr.error('Invalid file. Allowed file type is :[png,jpg,jpeg]', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            $("#fileImageUrl").val('');
        }
    }
}


/*--------------------------------------------Get Product Id through Query String----------------------------------------------*/
function getProductId(args) {
    var url = window.location.href.slice(window.location.href.indexOf("?") + 1).split();
    var productId = "";
    $(url).each(function () {
        var arr = this.split("=");
        if (arr[0] == args) {
            productId = arr[1];
        }
    });
    return productId;
}
/*---------------------------------------------------Bind Category-----------------------------------------------------------*/
function bindCategory() {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/RequestProductMaster.asmx/bindCategory",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successBindCategory,
        error: function (result) {
            swal("", "Oops! Something went wrong.Please try later.", "error");
        },
        failure: function (result) {
            swal("", "Oops! Something went wrong.Please try later.", "error");
        }
    });
}

function successBindCategory(response) {
    if (response.d != "Session Expired") {
        var xmldoc = $.parseXML(response.d);
        var category = $(xmldoc).find("Table");

        $("#ddlCategory option").remove();
        $("#ddlCategory").append("<option value='0'>Choose a Category</option>");
        $.each(category, function () {
            $("#ddlCategory").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("CategoryName").text() + "</option>");
        });
        var VendorList = $(xmldoc).find("Table1");

        $("#ddlVendor option").remove();
        $("#ddlVendor").append("<option value='0'>Choose an Vendor</option>");
        $.each(VendorList, function () {
            $("#ddlVendor").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("VendorName").text() + "</option>");
        });
        var BrandList = $(xmldoc).find("Table2");

        $("#ddlBrand option").remove();
        $("#ddlBrand").append("<option value='0'>Choose an Brand</option>");
        $.each(BrandList, function () {
            $("#ddlBrand").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("BrandName").text() + "</option>");
        });

        var CommissionCode = $(xmldoc).find("Table3");

        $("#ddlCommCode option").remove();
        $("#ddlCommCode").append("<option value='0'>Choose a  Commission Code</option>");
        $.each(CommissionCode, function () {
            $("#ddlCommCode").append("<option value='" + $(this).find("CommissionCode").text() + "'>" + $(this).find("C_DisplayName").text() + "</option>");
        });
    } else if (response.d == "false") {
        swal("", "Oops! Something went wrong.Please try later.", "error")
    }
    else {
        location.href = '/';
    }

}
/*-------------------------------------------------------Bind Subcategory-----------------------------------------------------------*/
function bindSubcategory() {
    var categoryAutoId = $("#ddlCategory").val();
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/RequestProductMaster.asmx/bindSubcategory",
        data: "{'categoryAutoId':'" + categoryAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successBindSubcategory,
        error: function (result) {
            swal("", "Oops! Something went wrong.Please try later.", "error");
        },
        failure: function (result) {
            swal("", "Oops! Something went wrong.Please try later.", "error");
        }
    });
}

function successBindSubcategory(response) {
    if (response.d != "Session Expired") {
        var xmldoc = $.parseXML(response.d);
        var subcategory = $(xmldoc).find("Table");

        if (subcategory.length > 0) {
            $('#ddlSubcategory').attr('disabled', false);
            $("#ddlSubcategory option").remove();
            $("#ddlSubcategory").append("<option value='0'>-Select-</option>");
            $.each(subcategory, function () {
                $("#ddlSubcategory").append("<option value=' " + $(this).find("AutoId").text() + "'>" + $(this).find("SubcategoryName").text() + "</option>");
            });
        } else {
            $('#ddlSubcategory').attr('disabled', 'disabled');
            $('#ddlSubcategory option:not(:first)').remove();
        }
    } else if (response.d == "false") {
        swal("", "Oops! Something went wrong.Please try later.", "error")
    }
    else {
        location.href = '/';
    }

}
/*-------------------------------------------------------Bind UnitType-----------------------------------------------------------*/
function bindUnitType() {

    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/RequestProductMaster.asmx/bindUnitType",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (result) {
            if (result.d != "Session Expired") {
                var xmldoc = $.parseXML(result.d);
                var unitType = $(xmldoc).find("Table");
                $("#ddlUnitType option").remove();
                $("#ddlUnitType").append("<option value='0'>-Select-</option>");
                $.each(unitType, function () {
                    $("#ddlUnitType").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("UnitType").text() + "</option>");
                });
            } else {
                location.href = '/';
            }
        },
        error: function (result) {
            swal("", "Oops! Something went wrong.Please try later.", "error");
        },
        failure: function (result) {
            swal("", "Oops! Something went wrong.Please try later.", "error");
        }
    });
}

$("#ddlUnitType").change(function () {
    if ($("#ddlUnitType option:selected").text() == "Piece") {
        $("#txtQty").val("1");
        $("#txtQty").attr("readonly", true);
    } else {
        $("#txtQty").val("");
        $("#txtQty").attr("readonly", false);
    }
})

function FillTax() {
    if ($("#ddlProductType").val() == 0) {
        $("#txtTaxRate").val('0.00').attr('disabled', true);
    } else {
        $("#txtTaxRate").val('').attr('disabled', false);
    }
}
/*------------------------------------------------------Insert Product------------------------------------------------------------*/

function uploadProductImage(productId) {
    var fileUpload = $("#fileImageUrl").get(0);
    var files = fileUpload.files;
    var test = new FormData();
    for (var i = 0; i < files.length; i++) {
        test.append(files[i].name, files[i]);
        imagefilename = files[i].name;
    }

    $.ajax({
        url: "/UploadFileOnPSMNJ.ashx?timestamp=" + productId,
        type: "POST",
        contentType: false,
        processData: false,
        async: false,
        data: test,
        success: function (result) {
            return;
        },
        error: function (err) {
        }
    });
}
function Bindlocation() {
    $.ajax({
        type: "POST",
        url: "RequestProductMaster.aspx/bindLocation",
        data: "{'AutoId':'" + AutoId + "'}",
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            var getData = $.parseJSON(response.d);
            var html = '';
            var count = 0;
            var html = '<div class="form-group row">';
            $.each(getData, function (index, item) {
                count = count + 1;
                
                if (getData[index].Status == 1) {
                    html += '<input type="checkbox" checked="true" name="Location" style="margin: 4px;"  value= "' + getData[index].AutoId + '">';
                }
                else {
                    html += '<input type="checkbox" name="Location" style="margin: 4px;"  value= "' + getData[index].AutoId + '">';
                }
                html += '<label name="' + getData[index].Location + '" class="LocationId" style="font-size: 18px;font-weight: 600;margin-right:25px;display: block;width: 97px">' + getData[index].Location + '</label>';
                if (count == 3) {
                    html += '</div><div class="form-grop row">';
                    count = 0;
                }

            });
            html += '</div>';
            $("#bindcheckbox").append(html);

        }
    });
}

function Save() { 
    var check = 0;
    $('#bindcheckbox input[type=checkbox]').each(function (index) {
        if ($(this).prop('checked') == true) {
            check = 1;
        }
    });
    if (check == 0) {
        toastr.error('Please select atleast one status.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    var IsApplyMLQty = 0, IsApplyWeightQuantity = 0;

    if ($('#cbMlQuantity').prop('checked') === true) {
        IsApplyMLQty = 1
    }
    if ($('#CbWeightQuantity').prop('checked') === true) {
        IsApplyWeightQuantity = 1
    }
    var chk = false, prc = false;
    var checkAuto = 0, autId = 7, autId1 = 8, autId2 = 9, Setdefault = 0, chkloc = 0;
    $("#Table1 tbody tr").each(function () {
        if ($(this).find('.Action input').prop('checked') == true) {
            var basePrice = Number($(this).find('.NewCosePrice input').val());
            if (basePrice == 0 || basePrice == null) {
                prc = true;
                $(this).find('.NewCosePrice input').addClass('border-warning');
                $(this).find('.NewBasePrice input').addClass('border-warning');
                $(this).find('.NewRetailPrice input').addClass('border-warning');
                $(this).find('.NWHPrice input').addClass('border-warning');
            }
            checkAuto = Number($(this).find('.Action input').attr("autoid"));
            if (checkAuto == 0) {
                autId = Number($(this).find('.NewQty input').val());;
            }
            else if (checkAuto == 1) {
                autId1 = Number($(this).find('.NewQty input').val());;
            }
            else if (checkAuto == 2) {
                autId2 = Number($(this).find('.NewQty input').val());;
            }
        }
        var totalQty = Number($(this).find('.NewQty input').val());
        if (totalQty == 0 || totalQty == null) {
            chk = true;
            $(this).find('.NewQty input').addClass('border-warning');
        }
        if ($(this).find('.Default input').prop("checked") == true) {
            Setdefault += Setdefault + 1;
        }
        var loc = 0;
        if ($(this).find('.Location input.first').val() != '') {
            loc = 1;
        }
        if ($(this).find('.Location input.second').val() != '') {
            loc = Number(loc) + 1;
        }
        if ($(this).find('.Location input.third').val() != '') {
            loc = Number(loc) + 1;
        }
        if ($(this).find('.Location input.fourth').val() != '') {
            loc = Number(loc) + 1;
        }
        if (Number(loc) != 0) {
            if (Number(loc) < 4) {
                $(this).find('.Location input.first').addClass('border-warning');
                $(this).find('.Location input.second').addClass('border-warning');
                $(this).find('.Location input.third').addClass('border-warning');
                $(this).find('.Location input.fourth').addClass('border-warning');
                chkloc = Number(chkloc) + 1;
            }
            else {
                $(this).find('.Location input.first').removeClass('border-warning');
                $(this).find('.Location input.second').removeClass('border-warning');
                $(this).find('.Location input.third').removeClass('border-warning');
                $(this).find('.Location input.fourth').removeClass('border-warning');
            }
        }
    })
    if (prc) {
        toastr.error('Please manage price before update.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    if (chk) {
        toastr.error('Please enter new quantity.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }

    if (autId == autId1 == autId2) {
        toastr.error('Please manage quantity.Its cannot be same.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    else if (autId == autId1) {
        toastr.error('Please manage quantity.Its cannot be same.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    else if (autId == autId2) {
        toastr.error('Please manage quantity.Its cannot be same.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    else if (autId1 == autId2) {
        toastr.error('Please manage New Quantity.Its cannot be same.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        swal.close();
        return;
    }
    else if (Setdefault == 0) {
        toastr.error('Please set default before update.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    } else if (chkloc != 0) {
        toastr.error('Please manage location.All fields required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    var check = 0;
    $("#Table1 tbody tr").each(function () {
        if ($(this).find('.Action input').prop('checked') == true) {
            {
                check += check + 1;
                chk = true;
            }
        }
    });
    if (check == 0) {
        toastr.error('Please check the checkbox to manage price.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    var ProductAutoId = $("#txtHProductAutoId").val();
    var dataTable = [];
    var chk1 = false; i = 0;
    $('#Table1 tbody tr').each(function (index) {
        if ($(this).find('.Action input').prop('checked') == true) {//By Rizwan Ahmad 11/27/2019

            dataTable[i] = new Object();
            dataTable[i].ProductAutoId = ProductAutoId,
                dataTable[i].UnitAutoId = $(this).find('.UnitType span').attr('UnitType');
            dataTable[i].Qty = $(this).find('.NewQty input').val();
            dataTable[i].CostPrice = $(this).find('.NewCosePrice input').val();
            dataTable[i].BasePrice = $(this).find('.NewBasePrice input').val();
            dataTable[i].RetailPrice = $(this).find('.NewRetailPrice input').val();
            dataTable[i].WHPrice = $(this).find('.NWHPrice input').val();
            if ($(this).find('.Default input').prop("checked") == true) {
                dataTable[i].SETDefault = 1;//Set Default
            }
            else {
                dataTable[i].SETDefault = 0;
            }
            if ($(this).find('.Free input').prop("checked") == true) {
                dataTable[i].SETFree = 1;//Set Free
            }
            else {
                dataTable[i].SETFree = 0;
            }
            dataTable[i].BarCode = $(this).find('.BarCode input').val();
            dataTable[i].UnitType = $(this).find('.UnitType').text();

            if ($(this).find('.Free input').prop("checked") == true) {
                dataTable[i].isFree = 'Yes';//Set Free
            }
            else {
                dataTable[i].isFree = 'No';//Set Free

            }
            var sec, rw, bn;
            if ($(this).find('.Location input.second').val().length == 1) {
                sec = '0' + $(this).find('.Location input.second').val();
            }
            else {
                sec = $(this).find('.Location input.second').val();
            }
            if ($(this).find('.Location input.third').val().length == 1) {
                rw = '0' + $(this).find('.Location input.third').val();
            }
            else {
                rw = $(this).find('.Location input.third').val();
            }
            if ($(this).find('.Location input.fourth').val().length == 1) {
                bn = '0' + $(this).find('.Location input.fourth').val();
            }
            else {
                bn = $(this).find('.Location input.fourth').val();
            }
            dataTable[i].Rack = $(this).find('.Location input.first').val();
            dataTable[i].Section = sec;
            dataTable[i].Row = rw;
            dataTable[i].BoxNo = bn;
            if (parseFloat($(this).find('.NewCosePrice input').val()) > parseFloat($(this).find('.NWHPrice input').val())) {
                chk1 = true;
                $(this).find('.NewCosePrice input').addClass('border-warning');
            } else {
                $(this).find('.NewCosePrice input').removeClass('border-warning');
            }
            if (parseFloat($(this).find('.NWHPrice input').val()) > parseFloat($(this).find('.NewRetailPrice input').val())) {
                chk1 = true;
                $(this).find('.NWHPrice input').addClass('border-warning');
            } else {
                $(this).find('.NWHPrice input').removeClass('border-warning');
            }
            if (parseFloat($(this).find('.NewRetailPrice input').val()) > parseFloat($(this).find('.NewBasePrice input').val())) {
                chk1 = true;
                $(this).find('.NewRetailPrice input').addClass('border-warning');
            } else {
                $(this).find('.NewRetailPrice input').removeClass('border-warning');
            }

            i = Number(i) + 1;
        }
    });
    if (chk1) {
        swal("", "Please check highlighted point.", "error");
        return;
    }
    var unitbaseprice = parseFloat($('.UnitBasePrice').val());
    var srp = parseFloat($('#txtSRP').val());
    if (srp < unitbaseprice) {
        swal("", "SRP can't be less than base price of piece.", "error");
        return;
    }

    uploadProductImage($("#txtProductId").val());
    var imgname = $("#fileImageUrl").val();
    var extension = imgname.substr((imgname.lastIndexOf('.') + 1));
    if ($("#fileImageUrl").val() != '') {
        imgname = $("#txtProductId").val() + '.' + extension;
    }
    else {
        toastr.error('Please upload image', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    if (checkrequiredProduct()) {
        Lstatus = new Array();
        $("input:checkbox[name=Location]").each(function (index, item) {
            Lstatus[index] = new Object();
            if ($(this).is(':checked')) {
                Lstatus[index].status = 1;
            }
            else {
                Lstatus[index].status = 0;
            }
            Lstatus[index].LocationId = $(item).attr('value');
        });
        var data = {
            IsApplyMlQty: IsApplyMLQty,
            IsApplyWeightQty: IsApplyWeightQuantity,
            CategoryAutoId: $("#ddlCategory").val(),
            SubcategoryAutoId: $("#ddlSubcategory").val(),
            ProductId: $("#txtProductId").val(),
            ProductName: $("#txtProductName").val(),
            ReOrderMark: $("#txtReOrderMark").val(),
            MLQty: $("#txtMLQty").val(),
            Weight: $("#txtWeightOz").val(),
            ImageUrl: imgname,
            CommCode: $("#ddlCommCode").val(),
            SRP: $("#txtSRP").val() || '0',
            BrandAutoId: $("#ddlBrand").val()
        }

        $.ajax({
            type: "POST",
            url: "RequestProductMaster.aspx/insertProduct",
            data: JSON.stringify({ dataValue: JSON.stringify(data), dataTable: JSON.stringify(dataTable), Locationstatus: JSON.stringify(Lstatus) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            async: true,
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d == 'Product ID already exists.') {
                    swal("", "Product ID already exist", "error");
                    return;

                }
                else if (response.d == 'Product name already exists.') {
                    swal("", "Product name already exist.", "error");
                    return;
                }
                else if (response.d == 'Product ID already exists in draft request.') {
                    swal("", "Product ID already exists in draft request", "error");
                    return;
                }
                else if (response.d == 'Product name already exists in draft request.') {
                    swal("", "Product name already exists in draft request", "error");
                    return;
                }
                else if (response.d == "false") {
                    swal("", "Oops! Something went wrong.Please try later.", "error");
                    return;
                }
                else if (response.d == "file does not exist.") {
                    swal("", "Oops! Image not uploaded. Please try again.", "error");
                    return;
                }
                else if (response.d == "file does not exist.") {
                    swal("", "Oops! Image not uploaded. Please try again.", "error");
                    return;
                }
                else if (response.d == "file does not exist.") {
                    swal("", "Oops! Image not uploaded. Please try again.", "error");
                    return;
                }

                else {
                    if (response.d == 'Success') {
                        swal("", "Product details saved successfully.", "success").then(function () {
                            window.location.href = "/Admin/RequestProductMaster.aspx";
                        });
                    }
                    else {
                        swal("", response.d, "error");
                    }
                }
            },
            error: function (result) {
                swal("", "Product details not saved", "error");
            },
            failure: function (result) {
                swal("", 'Oops,some thing went wrong.Please try again.', "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
};
function reset() {
    window.location.href = "/Admin/RequestProductMaster.aspx";
}

function checkrequiredProduct() {
    var boolcheck = true;
    $('.req').each(function () {
        if ($(this).val() == '') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else if (parseFloat($(this).val()) <= 0) {
            boolcheck = false;
            $(this).addClass('border-warning');
        }
        else {
            $(this).removeClass('border-warning');
        }
    });
    $('.ddlreq').each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == 'undefined' || $(this).val() == null) {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });
    return boolcheck;
}

function checkrequiredUnit() {
    var boolcheck = true;
    $('.ureq').each(function () {
        if ($(this).val() == '') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else if (parseInt($(this).val()) == 0) {
            boolcheck = false;
            $(this).addClass('border-warning');
        }
        else {
            $(this).removeClass('border-warning');
        }
    });
    $('.uddlreq').each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == 'undefined' || $(this).val() == null) {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });
    return boolcheck;
}


$("#btnReset1").click(function () {
    resetProduct();
    $("#panelPacking *").attr('disabled', 'disabled');
    $(".alert").hide();
});
/*------------------------------------------------------Insert Packing Details------------------------------------------------------------*/
$("#btnAdd").click(function () {
    var EligibleforFree = 0;
    if ($('#YesEligible').prop('checked')) {
        EligibleforFree = $('#YesEligible').val();
    }
    if (checkrequiredUnit()) {
        if (parseFloat($("#txtCostPrice").val()) > parseFloat($('#txtMinPrice').val())) {
            swal("Error!", "Cost Price should be less or equal to Retail Min Price.", "error");
            return;
        }
        if (parseFloat($("#txtWholesalePrice").val()) > parseFloat($('#txtMinPrice').val())) {
            swal("Error!", "Whole Sale Min Price should be less or equal to  Retail Min Price.", "error");
            return;
        }
        if ((parseFloat($("#txtWholesalePrice").val()) > parseFloat($("#txtPrice").val())) || (parseFloat($('#txtMinPrice').val()) > parseFloat($("#txtPrice").val()))) {
            swal("Error!", "Base Price should be greater  or equal to Retail Min Price and Whole Sale Min Price.", "error");
            return;
        }
        var data = {
            UnitType: $("#ddlUnitType").val(),
            UnitQty: $("#txtQty").val(),
            MinPrice: $("#txtMinPrice").val(),
            CostPrice: $("#txtCostPrice").val(),
            Price: $("#txtPrice").val(),
            ProductLocation: $("#txtLocation").val(),
            WHminPrice: $("#txtWholesalePrice").val(),
            PreDefinedBarcode: $("#txtPreDefinedBarcode").val(),
            // CommCode: $("#txtCommCode").val(),
            ProductAutoId: $("#txtHProductAutoId").val(),
            EligibleforFree: EligibleforFree
        };
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/RequestProductMaster.asmx/insertPackingDetails",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d != "Session Expired") {
                    if (result.d == "true") {
                        swal("", "Packing details added.", "success");
                        getPackingDetails();
                        resetPackingDetails();
                    } else {
                        swal("", result.d, "error");
                    }
                } else {
                    location.href = '/';
                }
            },
            error: function (result) {

                swal("", 'Oops,some thing went wrong.Please try again.', "error")
            },
            failure: function (result) {
                swal("", 'Oops,some thing went wrong.Please try again.', "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
});


function cancelUpdate(e) {
    td = $(e).closest("tr").find("td:eq(0)");
    td.html("<span BarcodeAutoId='" + td.find("span").attr("BarcodeAutoId") + "'>" + barcode + "</span>");
    $(e).closest("td").html("<a href='javascript:;'><span class='glyphicon glyphicon-edit' onclick='editBarcode(this)'></span></a><a href='javascript:;'><span class='glyphicon glyphicon-remove' onclick='deleteBarcode(this)'></span></a><a href='javascript:;'><span class='glyphicon glyphicon-print'></span></a>");
    $("#tblBarcode tbody tr").find("a").show();
}


function cancelSave(e) {
    $(e).closest("tr").find("td:eq(0)").html("");
    $(e).closest("td").html("<a href='javascript:;' onclick='addMoreBarcode(this)'>Add More</a>");
}

/*-------------------------------------------------Edit Product--------------------------------------------------------*/
function editProduct(ProductId) {
    $('#next').show();
    $("#txtHProductAutoId").val(ProductId);
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/RequestProductMaster.asmx/editProduct",
        data: "{'ProductId':'" + ProductId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successEditProduct,

        error: function (result) {
            swal("", 'Oops,some thing went wrong.Please try later.', "error");
        },
        failure: function (result) {
            swal("", 'Oops,some thing went wrong.Please try later.', "error");
        }
    });
}

function successEditProduct(response) {
    if (response.d != "Session Expired") {
        var xmldoc = $.parseXML(response.d);
        var subcategory = $(xmldoc).find("Table");
        var category = $(xmldoc).find("Table1");
        var unitType = $(xmldoc).find("Table2");
        var product = $(xmldoc).find("Table3");
        var packing = $(xmldoc).find("Table4");
        var VendorDetails = $(xmldoc).find("Table5");
        var BrandMaster = $(xmldoc).find("Table6");
        var CommissionCode = $(xmldoc).find("Table7");
        $("#linkAddNew").show();
        $("#btnSave").hide();
        $("#btnReset1").hide();
        $("#divStock").show();
        $('#ddlSubcategory').attr('disabled', false);
        if ($("#hiddenText").val() != "") {
            $("select").attr("disabled", true);
            $("input[type='text']").attr("disabled", true);
            $("#fileImageUrl").attr("disabled", true);
            $("#linkAddNew").hide();
            $("#btnUpdate1").hide();
            // $("#panelPacking").hide();
            $("#tblAddedPacking tr").find("td:eq(8)").hide();
            $("#tblBarcode").find("td:eq(2)").hide();
        }
        $("#ddlCategory option").remove();
        $("#ddlCategory").append("<option value='0'>-Select-</option>");
        $.each(category, function () {
            $("#ddlCategory").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("CategoryName").text() + "</option>");
        });

        $("#ddlSubcategory option").remove();
        $("#ddlSubcategory").append('<option value="0">-Select-</option>');
        $.each(subcategory, function () {
            $("#ddlSubcategory").append($("<option></option>").val($(this).find("AutoId").text()).html($(this).find("SubcategoryName").text()));
        });
        $("#ddlVendor option").remove();
        $("#ddlVendor").append('<option value="0">-Select-</option>');
        $.each(VendorDetails, function () {
            $("#ddlVendor").append($("<option></option>").val($(this).find("AutoId").text()).html($(this).find("VendorName").text()));
        });

        $("#ddlUnitType option").remove();
        $("#ddlUnitType").append('<option value="0">-Select-</option>');
        $.each(unitType, function () {
            $("#ddlUnitType").append($("<option></option>").val($(this).find("AutoId").text()).html($(this).find("UnitType").text()));
        });
        //if ($(product).find("VendorAutoId").text() != "") {
        //    $("#ddlVendor").val($(product).find("VendorAutoId").text());
        //}
        $("#ddlBrand option").remove();
        $("#ddlBrand").append('<option value="0">-Select-</option>');
        $.each(BrandMaster, function () {
            $("#ddlBrand").append($("<option></option>").val($(this).find("AutoId").text()).html($(this).find("BrandName").text()));
        });

        $("#ddlCommCode option").remove();
        $("#ddlCommCode").append('<option value="0">Choose a  Commission Code</option>');
        $.each(CommissionCode, function () {
            $("#ddlCommCode").append($("<option></option>").val($(this).find("CommissionCode").text()).html($(this).find("C_DisplayName").text()));
        });

        if ($(product).find("BrandAutoId").text() != "") {
            $("#ddlBrand").val($(product).find("BrandAutoId").text());
        }

        if ($(product).find("Status").text() == "1" || $(product).find("Status").text() == 1) {

            $("#btnUpdate1").hide();
            $("#btnCreateProduct").hide();
        } else {
            $("#btnUpdate1").show();
            $("#btnCreateProduct").show();
        }
        if ($(product).find("ProductStatus").text() == "1" || $(product).find("ProductStatus").text() == 1) {
            $("#btnUpdate1").show();
            $("#btnCreateProduct").show();
        } else {
            $("#btnUpdate1").hide();
            $("#btnCreateProduct").hide();
        }


        $("#ddlCategory").val($(product).find("CategoryAutoId").text());
        $("#ddlStatus").val($(product).find("Status1").text());
        $("#ddlSubcategory").val($(product).find("SubcategoryAutoId").text());
        $("#txtHProductAutoId").val($(product).find("AutoId").text());
        $("#txtProductId").val($(product).find("ProductId").text());
        $("#txtProductName").val($(product).find("ProductName").text());
        $("#txtMLQty").val($(product).find("MLQty").text());
        $("#txtLocation").val($(product).find("ProductLocation").text());
        if ($(product).find('ImageUrl').text() != '') {
            $("#imgProductImage").attr("src", imgURL + $(product).find('ImageUrl').text());
            $("#imgProductImage").attr("mainsrc", $(product).find('ImageUrl').text());
        }
        else {
            $("#imgProductImage").attr("src", "/images/default_pic.png");
            $("#imgProductImage").attr("mainsrc", "/images/default_pic.png");
        }
        $("#ddlProductType").val($(product).find('ProductType').text());
        $("#txtStock").val($(product).find('Stock').text());
        $("#txtTaxRate").val($(product).find('TaxRate').text());
        $("#txtReOrderMark").val($(product).find('ReOrderMark').text());

        if ($(product).find("IsApply_ML").text() == "true") {
            $('#cbMlQuantity').prop('checked', true);
        } else {
            $('#cbMlQuantity').prop('checked', false);
        }
        $("#txtMLQty").val($(product).find('MLQty').text());
        if ($(product).find("IsApply_Oz").text() == "true") {
            $('#CbWeightQuantity').prop('checked', true);
        } else {
            $('#CbWeightQuantity').prop('checked', false);
        }
        $("#txtWeightOz").val($(product).find('WeightOz').text());

        $("#ddlCommCode").val($(product).find("CommCode").text());
        $("#txtSRP").val($(product).find("SRP").text());
        var row = $("#tblAddedPacking thead tr").clone(true);
        $("#tblAddedPacking tbody tr").remove();
        var arr = [];
        $("#Table1 tbody tr").remove();
        var row = $("#Table1 thead tr:last-child").clone();
        $(packing).each(function (index) {
            if ($(this).find('UnitType').text() == '3') {
                $(".Action", row).html('<input type="checkbox" checked class="clsAction" disabled onclick="funchangeQty(this)" AutoId="' + index + '" id="tblPriceAction" name="tblAction">');
            } else {
                if ($(this).find('PackingId').text() == '0') {
                    $(".Action", row).html('<input type="checkbox" class="clsAction" onclick="funchangeQty(this)" AutoId="' + index + '" id="tblPriceAction" name="tblAction">');
                }
                else {
                    $(".Action", row).html('<input type="checkbox" checked class="clsAction" onclick="funchangeQty(this)" AutoId="' + index + '" id="tblPriceAction" name="tblAction">');
                }
            }
            if ($(this).find('DefaultPack').text() == '0') {
                $(".Default", row).html('<input type="radio"  class="clsDefault" name="SetDefault">');
            }
            else {
                $(".Default", row).html('<input type="radio" checked class="clsDefault" name="SetDefault">');
                localStorage.setItem('Default', index);
            }
            if ($(this).find('Free').text() == '0') {
                $(".Free", row).html('<input type="checkbox" class="clsFree" name="SetFree">');
            }
            else {
                $(".Free", row).html('<input type="checkbox" checked class="clsFree" name="SetFree">');
            }
            $(".UnitType", row).html($(this).find('UnitName').text() + "<span UnitType=" + $(this).find('UnitType').text() + "></span>");
            if ($(this).find('UnitType').text() == '3') {
                $(".NewQty", row).html("<input type='text' style='width:60px;text-align:center' class='form-control input-sm' onchange='funchangeprice(this)' onkeypress='return isNumberDecimalKey(event,this)' disabled  value='" + $(this).find('Qty').text() + "'/>");
            } else {
                $(".NewQty", row).html("<input type='text' style='width:60px;text-align:center' class='form-control input-sm' onchange='funchangeQty(this)' onkeypress='return isNumberDecimalKey(event,this)'  value='" + $(this).find('Qty').text() + "'/>");
            }

            if ($(this).find('UnitType').text() == '3') {
                $(".NewCosePrice", row).html("<input type='text' style='width:80px;text-align:right' onchange='funchangeprice(this)' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm UnitNewCost'  value='" + $(this).find('CostPrice').text() + "'/>");
            } else {
                $(".NewCosePrice", row).html("<input type='text' style='width:80px;text-align:right' onchange='funchangeprice(this)' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm'  value='" + $(this).find('CostPrice').text() + "'/>");
            }
            if ($(this).find('UnitType').text() == '3') {
                $(".NewBasePrice", row).html("<input type='text' style='width:80px;text-align:right' onchange='funchangeprice(this)' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm UnitBasePrice'  value='" + $(this).find('Price').text() + "'/>");
            } else {
                $(".NewBasePrice", row).html("<input type='text' style='width:80px;text-align:right' onchange='funchangeprice(this)' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm'  value='" + $(this).find('Price').text() + "'/>");
            }
            if ($(this).find('UnitType').text() == '3') {
                $(".NewRetailPrice", row).html("<input type='text' style='width:80px;text-align:right' onchange='funchangeprice(this)' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm UnitRetailPrice'  value='" + $(this).find('MinPrice').text() + "'/>");
            } else {
                $(".NewRetailPrice", row).html("<input type='text' style='width:80px;text-align:right' onchange='funchangeprice(this)' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm'  value='" + $(this).find('MinPrice').text() + "'/>");
            }
            if ($(this).find('UnitType').text() == '3') {
                $(".NWHPrice", row).html("<input type='text' style='width:80px;text-align:right'  onchange='funchangeprice(this)'  onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm UnitWHPrice'  value='" + $(this).find('WHminPrice').text() + "'/>");
            } else {
                $(".NWHPrice", row).html("<input type='text' style='width:80px;text-align:right'  onchange='funchangeprice(this)'  onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm'  value='" + $(this).find('WHminPrice').text() + "'/>");
            }
            $(".BarCode", row).html("<input type='text' class='Barcode form-control input-sm' maxlength='16' name='Barcode' value='" + $(this).find('LatestBarcode').text() + "' onblur='checkUniqueBarcode(this)' >");
            $(".Location", row).html("<input type='text' class='first' maxlength='1' placeholder='A' onchange='alphaOnly(this)' style='width:50px;text-align:center;text-transform:uppercase'  value='" + $(this).find('Rack').text() + "'/>-" +
                "<input type = 'text' class='second' maxlength = '2' placeholder='01' onkeypress='return isNumberKey(event)' style='width:50px;text-align:center' value = '" + $(this).find('Section').text() + "' />-" +
                "<input type = 'text' class='third' maxlength = '3' placeholder='01' onkeypress='return isNumberKey(event)' style='width:50px;text-align:center' value = '" + $(this).find('Row').text() + "' />-" +
                "<input type='text' class='fourth' maxlength='3' onkeypress='return isNumberKey(event)' style='width:50px;text-align:center' placeholder='01'   value='" + $(this).find('BoxNo').text() + "'/>");

            $("#Table1 tbody").append(row);
            row = $("#Table1 tbody tr:last-child").clone();
        });
        $("#Table1 tbody tr").show();
        $("#Table1 tbody tr").each(function () {
            if ($(this).find('.Action input').prop('checked') == false) {
                $(this).find('.NewQty input').attr('disabled', true);
                $(this).find('.NewCosePrice input').attr('disabled', true);
                $(this).find('.NewBasePrice input').attr('disabled', true);
                $(this).find('.NewRetailPrice input').attr('disabled', true);
                $(this).find('.NWHPrice input').attr('disabled', true);
                $(this).find(".Default input").attr("disabled", true);
                $(this).find(".Free input").attr("disabled", true);
                $(this).find(".BarCode  input").attr("disabled", true);
                $(this).find(".Location input").attr("disabled", true);
            }
        })
        if ($('#Hd_Domain').val() == 'psmnj' || $('#Hd_Domain').val() == 'psm') {
            $('#btnCreateProduct').show();
        } else {
            $('#btnCreateProduct').hide();
        }

    } else {
        location.href = '/';
    }
}

$("#btnAddNew").click(function () {
    window.location.href = "/Admin/productMaster.aspx";
})




$("#btnCancel2").click(function () {
    resetPackingDetails();
    $('#ddlUnitType').attr('disabled', false);
    $('#txtQty').attr('readonly', false);
})


function resetProduct() {
    $('#cbMlQuantity').prop('checked', false);
    $('#CbWeightQuantity').prop('checked', false);
    $("#ddlCategory").val(0);
    $("#ddlVendor").val(0).change();
    $("#ddlBrand").val(0).change();
    $("#ddlStatus").val(1).change();
    $("#ddlCommCode").val(0).change();
    $("#txtReOrderMark").val('0.00');
    $("#txtMLQty").val('0');
    $("#txtWeightOz").val('0.00');
    $("#txtSRP").val('0.00');
    $("#ddlSubcategory").val(0).attr('disabled', true);
    $("#panelProductInformation input[type='text']").val('');
    $("#btnUpdate1").hide();
    $("#btnAddNew").hide();
    $("#btnSave").show();
    $("#btnReset1").show();
    $("#panelProductInformation input[type='text']").removeClass('border-warning');

}
function resetPackingDetails() {
    $("#ddlUnitType").val(0);
    $("#ddlUnitType").attr('disabled', false);
    $("#panelPacking input[type='text']").val('');
    $("#btnAdd").show();
    $("#btnUpdate2").hide();
    $("#btnCancel2").hide();
    if ($('#Hd_Domain').val().toLowerCase() != 'psmnj' && $('#Hd_Domain').val().toLowerCase() != "psmnjm" && $('#Hd_Domain').val().toLowerCase() != "localhost" && $('#Hd_Domain').val().toLowerCase() != "demo" && $('#Hd_Domain').val().toLowerCase() != "psm") {
        $("#btnAdd").hide();
    }
    $("#YesEligible").prop('checked', false);
    $("#NoEligible").prop('checked', true);
    // Start added on 11/27/2019 By Rizwan Ahmad
    $("#ddlUnitType").attr('disabled', false);
    $("#txtQty").attr('disabled', false);
    $("#txtMinPrice").attr('disabled', false);
    $("#txtCostPrice").attr('disabled', false);
    $("#txtPrice").attr('disabled', false);
    $("#txtWholesalePrice").attr('disabled', false);
    $("#txtLocation").attr('disabled', false);
    $("#txtPreDefinedBarcode").attr('disabled', false);// End 
}

/*Set Default*/
function fndefault(e) {
    $("#alertDPacking").hide();
    $("#alertSPacking").hide();
    var tr = $(e).closest('tr');

    if (tr.find("td:eq(1) input").prop('checked') == false) {
        tr.find("td:eq(1) input").prop('checked', true);
        swal("Error!", 'Atleast set one packing as default.', "error");
        return;
    }
    var PackingAutoId = tr.find("td:eq(3) span").attr('PackingAutoId')

    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/RequestProductMaster.asmx/DefaultPacking",
        data: "{'PackingAutoId':'" + PackingAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                swal("", "Packing details updated successfully.", "success");
                getPackingDetails();
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            swal("", 'Oops,some thing went wrong.Please try later.', "error");
        },
        failure: function (result) {
            swal("", 'Oops,some thing went wrong.Please try later.', "error");
        }
    });
}

function managePrice() {
    $('#Div1').hide();
    $("input:radio").attr("checked", false);
    ProductAutoId = $("#txtHProductAutoId").val();
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/RequestProductMaster.asmx/getManagePrice",
        data: "{'ProductAutoId':'" + ProductAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var packingDetails = $(xmldoc).find("Table");
                $("#Table1 tbody tr").remove();
                var row = $("#Table1 thead tr:last-child").clone();
                console.log(packingDetails);
                $(packingDetails).each(function (index) {

                    $(".Default", row).html('<input type="radio"  class="clsDefault" name="SetDefault">');
                    $(".Free", row).html('<input type="checkbox" class="clsFree" name="SetFree">');

                    if ($(this).find('UnitType').text() == '3') {
                        $(".Action", row).html('<input type="checkbox" checked disabled class="clsAction" onclick="funchangeQty(this)" AutoId="' + index + '" id="tblPriceAction" name="tblAction">');
                    } else {
                        $(".Action", row).html('<input type="checkbox" class="clsAction" onclick="funchangeQty(this)" AutoId="' + index + '" id="tblPriceAction" name="tblAction">');
                    }


                    $(".UnitType", row).html($(this).find('UnitName').text() + "<span UnitType=" + $(this).find('UnitType').text() + "></span>");
                    if ($(this).find('UnitType').text() == '3') {
                        $(".NewQty", row).html("<input type='text' style='width:60px;text-align:center' class='form-control input-sm' onchange='funchangeprice(this)' onkeypress='return isNumberDecimalKey(event,this)' disabled  value='" + $(this).find('Qty').text() + "'/>");
                    } else {
                        $(".NewQty", row).html("<input type='text' style='width:60px;text-align:center' class='form-control input-sm' onchange='funchangeQty(this)' onkeypress='return isNumberDecimalKey(event,this)'  value='" + $(this).find('Qty').text() + "'/>");
                    }

                    if ($(this).find('UnitType').text() == '3') {
                        $(".NewCosePrice", row).html("<input type='text' style='width:80px;text-align:right' onchange='funchangeprice(this)' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm UnitNewCost'  value='" + $(this).find('CostPrice').text() + "'/>");
                    } else {
                        $(".NewCosePrice", row).html("<input type='text' style='width:80px;text-align:right' onchange='funchangeprice(this)' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm'  value='" + $(this).find('CostPrice').text() + "'/>");
                    }
                    if ($(this).find('UnitType').text() == '3') {
                        $(".NewBasePrice", row).html("<input type='text' style='width:80px;text-align:right' onchange='funchangeprice(this)' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm UnitBasePrice'  value='" + $(this).find('Price').text() + "'/>");
                    } else {
                        $(".NewBasePrice", row).html("<input type='text' style='width:80px;text-align:right' onchange='funchangeprice(this)' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm'  value='" + $(this).find('Price').text() + "'/>");
                    }
                    if ($(this).find('UnitType').text() == '3') {
                        $(".NewRetailPrice", row).html("<input type='text' style='width:80px;text-align:right' onchange='funchangeprice(this)' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm UnitRetailPrice'  value='" + $(this).find('SRP').text() + "'/>");
                    } else {
                        $(".NewRetailPrice", row).html("<input type='text' style='width:80px;text-align:right' onchange='funchangeprice(this)' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm'  value='" + $(this).find('SRP').text() + "'/>");
                    }
                    if ($(this).find('UnitType').text() == '3') {
                        $(".NWHPrice", row).html("<input type='text' style='width:80px;text-align:right'  onchange='funchangeprice(this)'  onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm UnitWHPrice'  value='" + $(this).find('WHminPrice').text() + "'/>");
                    } else {
                        $(".NWHPrice", row).html("<input type='text' style='width:80px;text-align:right'  onchange='funchangeprice(this)'  onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm'  value='" + $(this).find('WHminPrice').text() + "'/>");
                    }
                    $(".BarCode", row).html("<input type='text' class='Barcode form-control input-sm' name='Barcode' maxlength='16' value='' onblur='checkUniqueBarcode(this)'>");
                    $(".Location", row).html("<input type='text' class='first' maxlength='1' placeholder='A' onchange='alphaOnly(this)' style='width:50px;text-align:center;text-transform:uppercase'  value='" + $(this).find('Rack').text() + "'/>-" +
                        "<input type = 'text' class='second' maxlength = '2' placeholder='01' onkeypress='return isNumberKey(event)' style='width:50px;text-align:center' value = '" + $(this).find('Section').text() + "' />-" +
                        "<input type = 'text' class='third' maxlength = '3' placeholder='01' onkeypress='return isNumberKey(event)' style='width:50px;text-align:center' value = '" + $(this).find('Row').text() + "' />-" +
                        "<input type='text' class='fourth' maxlength='3' onkeypress='return isNumberKey(event)' style='width:50px;text-align:center' placeholder='01'   value='" + $(this).find('BoxNo').text() + "'/>");

                    $("#Table1 tbody").append(row);
                    row = $("#Table1 tbody tr:last-child").clone();
                });
                $("#Table1 tbody tr").show();
                $("#Table1 tbody tr").each(function () {
                    if ($(this).find('.Action input').prop('checked') == false) {
                        $(this).find('.NewQty input').attr('disabled', true);
                        $(this).find('.NewCosePrice input').attr('disabled', true);
                        $(this).find('.NewBasePrice input').attr('disabled', true);
                        $(this).find('.NewRetailPrice input').attr('disabled', true);
                        $(this).find('.NWHPrice input').attr('disabled', true);
                        $(this).find(".Default input").attr("disabled", true);
                        $(this).find(".Free input").attr("disabled", true);
                        $(this).find(".BarCode input").attr("disabled", true);
                        $(this).find(".Location input").attr("disabled", true);
                    }
                });
                if ($('#HDDomain').val() == 'psmnj' || $('#HDDomain').val() == 'psm' || $('#HDDomain').val() == 'localhost' || $('#HDDomain').val() == 'demo') {
                    $(".Action").removeAttr("Style");
                    $(".Default").removeAttr("Style");
                    $(".Free").removeAttr("Style");
                    $(".NewQty").removeAttr("Style");
                }

            } else {
                location.href = '/';
            }
        },
        error: function (result) {
        },
        failure: function (result) {
        }
    });
    if ($('#Hd_Domain').val().toLowerCase() != 'psmnj' && $('#Hd_Domain').val().toLowerCase() != "psmnjm" && $('#Hd_Domain').val().toLowerCase() != "localhost" && $('#Hd_Domain').val().toLowerCase() != "demo" && $('#Hd_Domain').val().toLowerCase() != "psm") {
        $('.QtyPop').hide();
    }
    $('#managePrice').modal('show');
}


function checkDefault() {
    var checkd = 0, currentDefault = 0, loc = 0, chkloc = 0;
    var Default = localStorage.getItem("Default");
    $("#Table1 tbody tr").each(function () {
        if ($(this).find('.Action input').prop('checked') == true) {
            if ($(this).find('.Default input').prop('checked') == true) {
                currentDefault = Number($(this).find('.Action input').attr("autoid"));
                //var length=$(this).find("#Table1 tbody tr").length;
                if (Default != currentDefault) {
                    checkd = 1;
                }
            }
        }

        if ($(this).find('.Location input.first').val() != '') {
            loc = 1;
        }
        if ($(this).find('.Location input.second').val() != '') {
            loc = Number(loc) + 1;
        }
        if ($(this).find('.Location input.third').val() != '') {
            loc = Number(loc) + 1;
        }
        if ($(this).find('.Location input.fourth').val() != '') {
            loc = Number(loc) + 1;
        }
        if (Number(loc) != 0) {
            if (Number(loc) < 4) {
                $(this).find('.Location input.first').addClass('border-warning');
                $(this).find('.Location input.second').addClass('border-warning');
                $(this).find('.Location input.third').addClass('border-warning');
                $(this).find('.Location input.fourth').addClass('border-warning');
                chkloc = Number(chkloc) + 1;
            }
            else {
                $(this).find('.Location input.first').removeClass('border-warning');
                $(this).find('.Location input.second').removeClass('border-warning');
                $(this).find('.Location input.third').removeClass('border-warning');
                $(this).find('.Location input.fourth').removeClass('border-warning');
            }
        }
    })
    if (chkloc != 0) {
        toastr.error('Please manage location. All fields required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    if (checkd == 1) {
        swal({
            title: "Are you sure?",
            text: "You want to change default packing.",
            icon: "warning",
            showCancelButton: true,
            allowOutsideClick: false,
            closeOnClickOutside: false,
            buttons: {
                cancel: {
                    text: "No, Cancel.",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: true,
                },
                confirm: {
                    text: "Yes, Change it.",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
                }
            }
        }).then(function (isConfirm) {
            if (isConfirm) {
                updatebulk();
            } else {
                closeModal: true
                $("#managePrice").modal('hide');
            }
        })
    }
    else {
        updatebulk();
    }
}

function funchangeprice(e) {
    var className = $(e).closest('td').attr('class');
    var tr = $(e).closest('tr');
    var UnitAutoId = tr.find('.UnitType span').attr('UnitType');
    var Price = 0.00;
    if ($(e).val() != '') {
        Price = $(e).val() || 0.00;
    }
    var Qty = 1;
    if (tr.find('.NewQty input').val() != '') {
        Qty = tr.find('.NewQty input').val() || 0.00;
    }
    var UnitPrice = 0.00, NewQty = 1;
    UnitPrice = parseFloat((parseFloat(Price) / parseInt(Qty))).toFixed(3) || 0.00;
    $("#Table1 tbody tr").each(function () {
        if ($(this).find('.Action input').prop('checked') == true) {
            if ($(this).find('.NewQty input').val() != '') {
                NewQty = $(this).find('.NewQty input').val();
            }
            var amount = (NewQty * UnitPrice);
            if (UnitAutoId != $(this).find('.UnitType span').attr('UnitType')) {
                if (className == 'NewCosePrice') {
                    $(this).find('.NewCosePrice input').val(parseFloat(amount).toFixed(3));
                } else if (className == 'NewBasePrice') {
                    $(this).find('.NewBasePrice input').val(parseFloat(amount).toFixed(3));
                } else if (className == 'NewRetailPrice') {
                    $(this).find('.NewRetailPrice input').val(parseFloat(amount).toFixed(3));
                } else if (className == 'NWHPrice') {
                    $(this).find('.NWHPrice input').val(parseFloat(amount).toFixed(3));
                }
            }
        }
    });

}
function funchangeQty(e) {
    var tr = $(e).closest('tr');
    var Qty = 1;
    if ($(e).val() != '') {
        if ($(e).val() == "on") {
            Qty = 1
        }
        else {
            Qty = $(e).val();
        }
    }
    var UnitType = $(tr).find('.UnitType > span').attr("unittype");
    if ($(tr).find(".Action input").prop("checked") == true) {
        if (UnitType != 3) {
            $(tr).find('.NewQty input').removeAttr('disabled');
        }
        $(tr).find('.NewCosePrice input').removeAttr('disabled');
        $(tr).find('.NewBasePrice input').removeAttr('disabled');
        $(tr).find('.NewRetailPrice input').removeAttr('disabled');
        $(tr).find('.NWHPrice input').removeAttr('disabled');
        $(tr).find(".Default input").removeAttr('disabled');
        $(tr).find(".Free input").removeAttr('disabled');
        $(tr).find(".BarCode input").removeAttr('disabled');
        $(tr).find(".Location input").removeAttr('disabled');

        var UnitNewCost = $('.UnitNewCost').val() || 0.000;
        var UnitBasePrice = $('.UnitBasePrice').val() || 0.000;
        $(tr).find('.NewCosePrice input').val(parseFloat(parseFloat(UnitNewCost) * parseFloat(Qty)).toFixed(3));
        $(tr).find('.CostPrice').text(parseFloat(parseFloat(UnitNewCost) * parseFloat(Qty)).toFixed(3));
        $(tr).find('.NewBasePrice input').val(parseFloat(parseFloat(UnitBasePrice) * parseFloat(Qty)).toFixed(3));
        $(tr).find('.BasePrice').text(parseFloat(parseFloat(UnitBasePrice) * parseFloat(Qty)).toFixed(3));
        UnitBasePrice = $('.UnitRetailPrice').val() || 0.000;
        $(tr).find('.NewRetailPrice input').val(parseFloat(parseFloat(UnitBasePrice) * parseFloat(Qty)).toFixed(3));
        $(tr).find('.RetailPrice').text(parseFloat(parseFloat(UnitBasePrice) * parseFloat(Qty)).toFixed(3));
        UnitBasePrice = $('.UnitWHPrice').val() || 0.000;
        $(tr).find('.NWHPrice input').val(parseFloat(parseFloat(UnitBasePrice) * parseFloat(Qty)).toFixed(3));
        $(tr).find('.WHminPrice').text(parseFloat(parseFloat(UnitBasePrice) * parseFloat(Qty)).toFixed(3));
    }
    else {
        $(tr).find('.NewCosePrice input').val("0.000");
        $(tr).find('.NewBasePrice input').val("0.000");
        $(tr).find('.NewRetailPrice input').val("0.000");
        $(tr).find('.NWHPrice input').val("0.000");
        $(tr).find('.NewQty input').val("1");
        $(tr).find('.Qty').html("1");
        $(tr).find('.CostPrice').html("0.000");
        $(tr).find('.BasePrice').html("0.000");
        $(tr).find('.RetailPrice').html("0.000");
        $(tr).find('.WHminPrice').html("0.000");
        $(tr).find('.NewQty input').attr('disabled', true);
        $(tr).find('.NewCosePrice input').attr('disabled', true);
        $(tr).find('.NewBasePrice input').attr('disabled', true);
        $(tr).find('.NewRetailPrice input').attr('disabled', true);
        $(tr).find('.NWHPrice input').attr('disabled', true);
        $(tr).find('.Default input').prop('checked', false);
        $(tr).find('.Default input').attr("disabled", true);
        $(tr).find('.Free input').prop('checked', false);
        $(tr).find(".Free input").attr("disabled", true);
        $(tr).find(".BarCode input").attr("disabled", true);
        $(tr).find(".Location input").attr("disabled", true);
    }
}

function IsNumeric() {
    var data = $("#ddlCommCode").val()
    return parseFloat(data) == data;
}


function CreateNewProduct(status) {
    debugger;
    var check = 0;
    $('#bindcheckbox input[type=checkbox]').each(function (index) {
        if ($(this).prop('checked') == true) {
            check = 1;
        }
    });
    if (check == 0) {
        toastr.error('Please select atleast one status.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    var IsApplyMLQty = 0, IsApplyWeightQuantity = 0;

    if ($('#cbMlQuantity').prop('checked') === true) {
        IsApplyMLQty = 1
    }
    if ($('#CbWeightQuantity').prop('checked') === true) {
        IsApplyWeightQuantity = 1
    }


    var chk = false, prc = false;
    var checkAuto = 0, autId = 7, autId1 = 8, autId2 = 9, Setdefault = 0, chkloc = 0;
    $("#Table1 tbody tr").each(function () {
        if ($(this).find('.Action input').prop('checked') == true) {
            var basePrice = Number($(this).find('.NewCosePrice input').val());
            if (basePrice == 0 || basePrice == null) {
                prc = true;
                $(this).find('.NewCosePrice input').addClass('border-warning');
                $(this).find('.NewBasePrice input').addClass('border-warning');
                $(this).find('.NewRetailPrice input').addClass('border-warning');
                $(this).find('.NWHPrice input').addClass('border-warning');
            }
            checkAuto = Number($(this).find('.Action input').attr("autoid"));
            if (checkAuto == 0) {
                autId = Number($(this).find('.NewQty input').val());;
            }
            else if (checkAuto == 1) {
                autId1 = Number($(this).find('.NewQty input').val());;
            }
            else if (checkAuto == 2) {
                autId2 = Number($(this).find('.NewQty input').val());;
            }
        }
        var totalQty = Number($(this).find('.NewQty input').val());
        if (totalQty == 0 || totalQty == null) {
            chk = true;
            $(this).find('.NewQty input').addClass('border-warning');
        }
        if ($(this).find('.Default input').prop("checked") == true) {
            Setdefault += Setdefault + 1;
        }
        var loc = 0;
        if ($(this).find('.Location input.first').val() != '') {
            loc = 1;
        }
        if ($(this).find('.Location input.second').val() != '') {
            loc = Number(loc) + 1;
        }
        if ($(this).find('.Location input.third').val() != '') {
            loc = Number(loc) + 1;
        }
        if ($(this).find('.Location input.fourth').val() != '') {
            loc = Number(loc) + 1;
        }
        if (Number(loc) != 0) {
            if (Number(loc) < 4) {
                $(this).find('.Location input.first').addClass('border-warning');
                $(this).find('.Location input.second').addClass('border-warning');
                $(this).find('.Location input.third').addClass('border-warning');
                $(this).find('.Location input.fourth').addClass('border-warning');
                chkloc = Number(chkloc) + 1;
            }
            else {
                $(this).find('.Location input.first').removeClass('border-warning');
                $(this).find('.Location input.second').removeClass('border-warning');
                $(this).find('.Location input.third').removeClass('border-warning');
                $(this).find('.Location input.fourth').removeClass('border-warning');
            }
        }
    })
    if (chkloc != 0) {
        toastr.error('Please manage location.All fields required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    if (prc) {
        toastr.error('Please manage price before update.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    if (chk) {
        toastr.error('Please enter new quantity.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }

    if (autId == autId1 == autId2) {
        toastr.error('Please manage quantity.Its cannot be same.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    else if (autId == autId1) {
        toastr.error('Please manage quantity.Its cannot be same.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    else if (autId == autId2) {
        toastr.error('Please manage quantity.Its cannot be same.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    else if (autId1 == autId2) {
        toastr.error('Please manage New Quantity.Its cannot be same.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        swal.close();
        return;
    }
    else if (Setdefault == 0) {
        toastr.error('Please set default before update.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    var check = 0;
    $("#Table1 tbody tr").each(function () {
        if ($(this).find('.Action input').prop('checked') == true) {
            {
                check += check + 1;
                chk = true;
            }
        }
    });
    if (check == 0) {
        toastr.error('Please check the checkbox to manage price.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    var ProductAutoId = $("#txtHProductAutoId").val();
    var dataTable = [];
    var chk1 = false; i = 0;
    $('#Table1 tbody tr').each(function (index) {
        if ($(this).find('.Action input').prop('checked') == true) {//By Rizwan Ahmad 11/27/2019

            dataTable[i] = new Object();
            dataTable[i].ProductAutoId = ProductAutoId,
                dataTable[i].UnitAutoId = $(this).find('.UnitType span').attr('UnitType');
            dataTable[i].Qty = $(this).find('.NewQty input').val();
            dataTable[i].CostPrice = $(this).find('.NewCosePrice input').val();
            dataTable[i].BasePrice = $(this).find('.NewBasePrice input').val();
            dataTable[i].RetailPrice = $(this).find('.NewRetailPrice input').val();
            dataTable[i].WHPrice = $(this).find('.NWHPrice input').val();
            if ($(this).find('.Default input').prop("checked") == true) {
                dataTable[i].SETDefault = 1;//Set Default
            }
            else {
                dataTable[i].SETDefault = 0;
            }
            if ($(this).find('.Free input').prop("checked") == true) {
                dataTable[i].SETFree = 1;//Set Free
            }
            else {
                dataTable[i].SETFree = 0;

            }
            dataTable[i].BarCode = $(this).find('.BarCode input').val();
            dataTable[i].UnitType = $(this).find('.UnitType').text();

            if ($(this).find('.Free input').prop("checked") == true) {
                dataTable[i].isFree = 'Yes';//Set Free
            }
            else {
                dataTable[i].isFree = 'No';//Set Free

            }
            var sec, rw, bn;
            if ($(this).find('.Location input.second').val().length == 1) {
                sec = '0' + $(this).find('.Location input.second').val();
            }
            else {
                sec = $(this).find('.Location input.second').val();
            }
            if ($(this).find('.Location input.third').val().length == 1) {
                rw = '0' + $(this).find('.Location input.third').val();
            }
            else {
                rw = $(this).find('.Location input.third').val();
            }
            if ($(this).find('.Location input.fourth').val().length == 1) {
                bn = '0' + $(this).find('.Location input.fourth').val();
            }
            else {
                bn = $(this).find('.Location input.fourth').val();
            }
            dataTable[i].Rack = $(this).find('.Location input.first').val();
            dataTable[i].Section = sec;
            dataTable[i].Row = rw;
            dataTable[i].BoxNo = bn;
            if (parseFloat($(this).find('.NewCosePrice input').val()) > parseFloat($(this).find('.NWHPrice input').val())) {
                chk1 = true;
                $(this).find('.NewCosePrice input').addClass('border-warning');
            } else {
                $(this).find('.NewCosePrice input').removeClass('border-warning');
            }
            if (parseFloat($(this).find('.NWHPrice input').val()) > parseFloat($(this).find('.NewRetailPrice input').val())) {
                chk1 = true;
                $(this).find('.NWHPrice input').addClass('border-warning');
            } else {
                $(this).find('.NWHPrice input').removeClass('border-warning');
            }
            if (parseFloat($(this).find('.NewRetailPrice input').val()) > parseFloat($(this).find('.NewBasePrice input').val())) {
                chk1 = true;
                $(this).find('.NewRetailPrice input').addClass('border-warning');
            } else {
                $(this).find('.NewRetailPrice input').removeClass('border-warning');
            }


            i = Number(i) + 1;
        }
    });
    if (chk1) {
        swal("", "Please check highlighted point.", "error");
        return;
    }
    var imgname = $("#fileImageUrl").val();
    var extension = imgname.substr((imgname.lastIndexOf('.') + 1));

    if ($("#fileImageUrl").val() != '') {
        imgname = $("#txtProductId").val() + '.' + extension;
            uploadProductImage($("#txtProductId").val());
    }
    else {
        if ($("#imgProductImage").attr("mainsrc") != undefined) {
            imgname = $("#imgProductImage").attr("mainsrc").split("/")[2] || "";
        }
        else {
            imgname = "default_pic.png";
        }
    }

    var unitbaseprice = parseFloat($('.UnitBasePrice').val());
    var srp = parseFloat($('#txtSRP').val());
    if (srp < unitbaseprice) {
        swal("", "SRP can't be less than base price of piece.", "error");
        return;
    }
    if (checkrequiredProduct()) {

        Lstatus = new Array();
        $("input:checkbox[name=Location]").each(function (index, item) {
            Lstatus[index] = new Object();
            if ($(this).is(':checked')) {
                Lstatus[index].status = 1;
            }
            else {
                Lstatus[index].status = 0;
            }
            Lstatus[index].LocationId = $(item).attr('value');
        });
        var data = {
            IsApplyMlQty: IsApplyMLQty,
            IsApplyWeightQty: IsApplyWeightQuantity,
            ProductAutoId: $("#txtHProductAutoId").val(),
            CategoryAutoId: $("#ddlCategory").val(),
            SubcategoryAutoId: $("#ddlSubcategory").val(),
            ProductId: $("#txtProductId").val(),
            ProductName: $("#txtProductName").val(),
            ReOrderMark: $("#txtReOrderMark").val(),
            MLQty: $("#txtMLQty").val(),
            WeightOz: $("#txtWeightOz").val(),
            ImageUrl: imgname,
            CommCode: $("#ddlCommCode").val(),
            SRP: $("#txtSRP").val() || '0',
            BrandAutoId: $("#ddlBrand").val(),
            status: status
        };
        $.ajax({
            type: "POST",
            url: "RequestProductMaster.aspx/ProductCreate",
            data: JSON.stringify({ dataValue: JSON.stringify(data), dataTable: JSON.stringify(dataTable), Locationstatus: JSON.stringify(Lstatus) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d != "Session Expired") {
                    if (response.d == "Success") {
                        //var fileUpload = $("#fileImageUrl").get(0);
                        //var files = fileUpload.files;
                        //var test = new FormData();
                        //for (var i = 0; i < files.length; i++) {
                        //    test.append(files[i].name, files[i]);
                        //}
                        //$.ajax({
                        //    url: "/UploadFileOnPSMNJ.ashx?timestamp=" + data.ProductId,
                        //    type: "POST",
                        //    contentType: false,
                        //    processData: false,
                        //    data: test,
                        //    success: function (result) {

                        //    },
                        //    error: function (err) {
                        //    }
                        //});
                        if (status == 0) {
                            swal("", "Product details updated successfully.", "success");
                        } else {
                            swal("", "Product created successfully.", "success").then(function () {

                                window.location.href = "/Admin/RequestProductList.aspx";

                            });
                        }

                    }
                    else if (response.d == "file does not exist.") {
                        swal("", "Oops! Image not uploaded. Please try again.", "error");
                        return;
                    }
                    else {
                        swal("", response.d, "error");
                        return;
                    }

                } else {
                    location.href = '/';
                }

            },
            error: function (result) {
                swal("", "Product details not updated", "error");
            },
            failure: function (result) {
                swal("", 'Oops,some thing went wrong.Please try again.', "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }

}
function checkUniqueBarcode(input) {
    barcode = input.value
    var i = 0;
    $('#Table1 > tbody  > tr').each(function (index, tr) {
        if ($(this).find('.BarCode  input').val() != '') {
            if ($(this).find('.BarCode  input').val().trim() == barcode) {
                i++;
            }
        }
    });
    if (i > 1) {
        $(input).val('');
        swal("Error!", "Barcode already exist.", "error");
    }
}


