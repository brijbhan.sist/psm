﻿$(document).ready(function () {
    GetRouteList(1);
    bindSalePerson();
});
$("#btnSearch").click(function () {
    GetRouteList(1);
})
function Pagevalue(e) {

    GetRouteList(parseInt($(e).attr("page")));
}
function bindSalePerson() {
    $.ajax({
        type: "POST",
        url: "WebAPI/RouteMaster.asmx/GetdSalesPerson",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var GetSalePerson = $(xmldoc).find("Table");

            $("#ddlSalePerson option:not(:first)").remove();
            $.each(GetSalePerson, function () {
                $("#ddlSalePerson").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("SalesPerson").text() + "</option>");
            });

            $("#ddlSalePerson").select2()
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function GetRouteList(PageIndex) {
    var data = {
        SalesPersonName: $("#ddlSalePerson").val() || '0',
        RouteName: $("#txtRoutName").val().trim(),
        Status: $("#ddlSStatus").val(),
        PageSize: $("#ddlPageSize").val(),
        PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "WebAPI/RouteMaster.asmx/GetRouteList",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successGetRouteList,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function successGetRouteList(response) {
    var xmldoc = $.parseXML(response.d);
    var RouteList = $(xmldoc).find("Table1");

    $("#tblRouteList tbody tr").remove();

    var row = $("#tblRouteList thead tr").clone(true);
    if (RouteList.length > 0) {
        $("#EmptyTable").hide();
        $.each(RouteList, function (index) {
            $(".AutoId", row).text($(this).find("AutoId").text());
            $(".RouteId", row).text($(this).find("RouteId").text());
            $(".RouteName", row).text($(this).find("RouteName").text());
            $(".SalesPerson", row).text($(this).find("SalesPerson").text());
            $(".NoZip", row).text($(this).find("NoOfCustomer").text());
            if ($(this).find("Status").text() == 'Active') {
                $(".status", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("Status").text() + "</span>");
            } else {
                $(".status", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("Status").text() + "</span>");

            }
            $(".action", row).html("<a title='Edit' href='/Admin/RouteEntry.aspx?PageId=" + $(this).find("AutoId").text() + "'><span class='ft-edit' ></span></a>&nbsp;&nbsp<a title='Edit' href='#' onclick='deleteRouteConfirmation(\"" + $(this).find("AutoId").text() + "\")'><span class='ft-x' ></span></a>");
            $("#tblRouteList tbody").append(row);
            row = $("#tblRouteList tbody tr:last").clone(true);
        });

    } else {
        $("#EmptyTable").show();
    }

    var pager = $(xmldoc).find("Table");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
}
function deleteRouteConfirmation(routeId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this Route",
        icon: "warning",
        showCancelButton: true,
        allowOutsideClick: false,
        closeOnClickOutside: false,

        buttons: {
            cancel: {
                text: "No, Cancel",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, Delete it",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            deleteRoute(routeId);
        }
    })
}
function deleteRoute(routeId) {
    $.ajax({
        type: "POST",
        url: "RouteList.aspx/DeleteRoute",
        data: "{'AutoId':'" + routeId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (result) {
            if (result.d == "true") {
                swal("", "Route deleted successfully.", "success");
                GetRouteList(1);
            }
            else if (result.d == "Session Expired") {
                location.href = "/";
            }
            else {
                swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                GetRouteList(1);
            }
        },
        error: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        },
        failure: function (result) {
            swal("Error!", result.d, "error");
        }
    });

}