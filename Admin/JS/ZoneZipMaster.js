﻿$(document).ready(function () {
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    var PageId = getQueryString('PageId');
    if (PageId != null) {
        editZone(PageId);
        $("#txtZoneId").val(PageId);
        $("#btnUpdate").show();
        $("#btnCancel").show();
        $("#btnSave").hide();
        $("#btnReset").hide();
        $("#divaddzipcode").show();
    } else {
        
        $("#btnUpdate").hide();
        $("#btnCancel").hide();
        $("#divaddzipcode").hide();
        $("#btnSave").show();
        $("#btnReset").show();
        bindDriverCar();
    }
  
})

function checkZonentryRequiredField() {
    var boolcheck = true;
    $('.req').each(function () {
        if ($(this).val() == '') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });   
    return boolcheck;
}
function bindDriverCar() {
    $.ajax({
        type: "POST",
        url: "WebAPI/ZoneZipMaster.asmx/SelectDriverCar",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {},
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var xmldoc1 = $.parseXML(response.d);
            var CarDetail = $(xmldoc).find("Table");
            var Driverdetail = $(xmldoc1).find("Table1");

            $("#ddlDriver option:not(:first)").remove();
            $.each(Driverdetail, function () {
                $("#ddlDriver").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("EmpName").text() + "</option>");
            });
            $("#ddlDriver").select2()

            $("#ddlCar option:not(:first)").remove();
            $.each(CarDetail, function () {
                $("#ddlCar").append("<option value='" + $(this).find("CarAutoId").text() + "'>" + $(this).find("CarName").text() + "</option>");
            });
            $("#ddlCar").select2()

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function editZone(AutoId) {
    $.ajax({
        type: "POST",
        async:false,
        url: "WebAPI/ZoneZipMaster.asmx/EditZoneMaster",
        data: "{'AutoId':'" + AutoId + "','SZip':'" + $("#txtSZipCode").val().trim() + "','Scity':'" + $("#txtCityname").val().trim() + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
            if ($.fn.DataTable.isDataTable('#tblZips')) {
                $('#tblZips').DataTable().destroy();
            }
        },
        complete: function () {
          
            $('#tblZips').DataTable(
               {
                   "paging": true,
                   "ordering": false,
                   "info": true,
                   "searching": false,
                   "aoColumns": [
                       { "bSortable": false },
                       { "bSortable": true },
                       { "bSortable": false },
                       { "bSortable": false }
                     
                   ]
               });
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var CarDetail = $(xmldoc).find("Table");
            var Driverdetail = $(xmldoc).find("Table1");
            var ZoneDetail = $(xmldoc).find("Table2");
            var ZipDetail = $(xmldoc).find("Table3");

            $("#ddlDriver option:not(:first)").remove();
            $.each(Driverdetail, function () {
                $("#ddlDriver").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("EmpName").text() + "</option>");
            });
            $("#ddlDriver").select2();

            $("#ddlCar option:not(:first)").remove();
            $.each(CarDetail, function () {
                $("#ddlCar").append("<option value='" + $(this).find("CarAutoId").text() + "'>" + $(this).find("CarName").text() + "</option>");
            });
            $("#ddlCar").select2();

            $("#txtZoneName").val($(ZoneDetail).find("ZoneName").text());
            $("#ddlDriver").val($(ZoneDetail).find("DriverId").text()).change();
            $("#ddlCar").val($(ZoneDetail).find("CarId").text()).change();
            $("#ddlStatus").val($(ZoneDetail).find("Status").text()).change();
            $("#txtZoneId").val($(ZoneDetail).find("AutoId").text());



            $("#tblZips tbody tr").remove();
            var row = $("#tblZips thead tr").clone(true);
            if (ZipDetail.length > 0) {
                $("#EmptyTable").hide();
                $.each(ZipDetail, function (index) {
                    $(".Id", row).text($(this).find("AutoId").text());
                    $(".ZipCode", row).text($(this).find("Zipcode").text());
                    $(".City", row).text($(this).find("CityName").text());
                    if ($(this).find("SelectStatus").text() == 0) {
                        $(".SelCheckbox", row).html('<input type="checkbox" class="zipcheckbox" onclick="UpdateZoneZip(this);"/>');
                    }
                    else {
                        $(".SelCheckbox", row).html('<input type="checkbox" class="zipcheckbox" checked="true" onclick="UpdateZoneZip(this);" />');
                    }
                    $("#tblZips tbody").append(row);
                    row = $("#tblZips tbody tr:last").clone(true);
                });

            } else {
                $("#EmptyTable").show();
            }


        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

$("#btnSave").click(function () {
    if (checkZonentryRequiredField()) {
        var data = {
            Name: $("#txtZoneName").val(),
            DriverID: $("#ddlDriver").val(),
            CarId: $("#ddlCar").val(),
            Status: $("#ddlStatus").val()
        }
        $.ajax({
            type: "POST",
            async:false,
            url: "WebAPI/ZoneZipMaster.asmx/insert",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    timeout: 2000, //unblock after 2 seconds
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function (result) {
                if (result.d == "false") {
                    swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                }
                else if (result.d == "Exists") {
                    swal("Error!", "Zone already exists.", "error");  
                }
                else if(result.d=="Unauthorized access.")
                {
                    location.href = "/";
                }
                else {
                    var xmldoc = $.parseXML(result.d);
                    var Id = $(xmldoc).find("Table");
                    swal("", "Zone details saved successfully.", "success").then(function () {
                        window.location.href = "/Admin/ZoneEntry.aspx?PageId=" + $(Id).find("AutoId").text();
                    });
                    //swal({
                    //    title: "",
                    //    text: "Data has been saved successfully.",
                    //    type: "success"
                    //})
                    
                } 
            },
            error: function (result) {
                swal("Error!", result.d, "error");
            },
            failure: function (result) {
                swal("Error!", result.d, "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
});

$("#btnUpdate").click(function () {
    if (checkZonentryRequiredField()) {
        var data = {
            Name: $("#txtZoneName").val(),
            DriverID: $("#ddlDriver").val(),
            CarId: $("#ddlCar").val(),
            Status: $("#ddlStatus").val(),
            AutoId: $("#txtZoneId").val()
        }
        $.ajax({
            type: "POST",
            url: "WebAPI/ZoneZipMaster.asmx/update",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    timeout: 2000, //unblock after 2 seconds
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },           
            success: function (result) {
                if (result.d == 'true') {
                    swal("", "Zone details updated successfully.", "success");
                } else if (result.d == "Exists") {
                    swal("Error!", "Zone already exists.", "error");
                }
                else if (result.d == "Unauthorized access.")
                {
                    location.href = "/";
                }
                else if (result.d == "false") {
                    swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                }
            },
            error: function (result) {
                swal("Error!", result.d, "error");
            },
            failure: function (result) {
                swal("Error!", result.d, "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
});
function resttext() {
    $("#txtZoneName").val('')
    $("#ddlDriver").val('0').change();
    $("#ddlCar").val('0').change();
    $("#ddlStatus").val('1')
    $("#txtZoneId").val('')
    $("#txtZoneName").removeClass('border-warning');
}
$("#btnReset").click(function () {
    resttext();
});

$("#btnCancel").click(function () {
    window.location.replace("ZoneList.aspx");
}); 
function UpdateZoneZip(e)
{
    var check = 0;
    var tr = $(e).parents('tr');
    if (tr.find('.zipcheckbox').prop("checked") == true) {
        check = 1;
    }
    var zipcode = tr.find('.ZipCode').text();
    var data = {
        AutoId: $("#txtZoneId").val(),
        ZipCode: zipcode
    }

    $.ajax({
        type: "POST",
        url: "WebAPI/ZoneZipMaster.asmx/updateZoneZipCode",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },       
        success: function (result) {
            if (result.d == "Unauthorized access.") {
                location.href = "/";
                            }
            else {
                editZone($("#txtZoneId").val());
                swal("", "Zip code assigned to this zone successfully.", "success");
            }
        },
        error: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        },
        failure: function (result) {
            swal("Error!", result.d, "error");
        }
    });
}
$("#btnSearch").click(function () {
    editZone($("#txtZoneId").val())
})

