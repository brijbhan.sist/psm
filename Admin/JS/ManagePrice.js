﻿var ProductAutoId = 0;

$(document).ready(function () {
    bindCategory();
    getProductDetails(1);
    //if ($('#hfLocation').val() == 'psmnj' || $('#hfLocation').val() == 'psmnj1' || $('#hfLocation').val() == 'localhost' || $('#hfLocation').val() == 'psm' || $('#hfLocation').val() == 'demo') {
    //    $("#divSRP").show();
    //}
    //else {
    //    $("#divSRP").hide();
    //}
})
function Pagevalue(e) {
    getProductDetails(parseInt($(e).attr("page")));
}

/*---------------------------------------------------Bind Category-----------------------------------------------------------*/
function bindCategory() {

    $.ajax({
        type: "POST",
        url: "ManagePrice.aspx/bindCategory",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",

        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var getData = $.parseJSON(response.d);

            $("#ddlSCategory option:not(:first)").remove();
            $.each(getData[0].Category, function (index, item) {

                $("#ddlSCategory").append("<option value='" + item.CAID + "'>" + item.CAN + "</option>");
            });
            $("#ddlSCategory").select2();
            $("#ddlBrand option:not(:first)").remove();
            $.each(getData[0].Brand, function (index, item) {
                $("#ddlBrand").append("<option value='" + item.BID + "'>" + item.BN + "</option>");
            });
            $("#ddlBrand").select2();
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

/*-------------------------------------------------------Bind Subcategory-----------------------------------------------------------*/
function BindSubCategory() {
    $("#ddlSSubcategory").val(0).change();
    var categoryAutoId = $("#ddlSCategory").val();
    $.ajax({
        type: "POST",
        url: "ManagePrice.aspx/bindSubcategory",
        data: "{'categoryAutoId':'" + categoryAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "") {
                var getData = $.parseJSON(response.d);
                $("#ddlSSubcategory option:not(:first)").remove();
                $.each(getData, function (index, item) {
                    $("#ddlSSubcategory").append("<option value=' " + getData[index].SCAID + "'>" + getData[index].SCAN + "</option>");
                });
                $("#ddlSSubcategory").select2();
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function getProductDetails(PageIndex) {
    var data = {
        ProductId: $("#txtProductId").val().trim(),
        ProductName: $("#txtProductName").val().trim(),
        CategoryAutoId: $("#ddlSCategory").val(),
        SubcategoryAutoId: $("#ddlSSubcategory").val(),
        BrandName: $("#ddlBrand").val(),
        CostComparePrice: $("#ddlCostPriceCompare").val(),
        PageSize: $("#ddlPageSize").val(),
        PageIndex: PageIndex
    }
    $.ajax({
        type: "POST",
        url: "ManagePrice.aspx/GetPriceDetails",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (result) {
            var xmldoc = $.parseXML(result.d);
            var packing = $(xmldoc).find("Table1");
            if (packing.length > 0) {
                $("#emptyTable").hide();
                var thml = "";
                $("#tblPriceDetails tbody tr").remove();
                var check = false;
                var row = $("#tblPriceDetails thead tr").clone(true);
                $.each(packing, function () {
                    $(".ProductId", row).html("<span ProductAutoId='" + $(this).find("AutoId").text() + "'>" + $(this).find("ProductId").text() + "</span>");
                    $(".ProductName", row).html($(this).find("ProductName").text());
                    $(".UnitType", row).html("<span PSRP=" + $(this).find('SRP').text() + " UnitType=" + $(this).find('UnitType').text() + " ProductAutoId='" + $(this).find("AutoId").text() + "'>" + $(this).find("UnitName").text() + thml + "</span>");
                    $(".Qty", row).html($(this).find("Qty").text());
                    //if ($('#hfLocation').val() == 'psmnj' || $('#hfLocation').val() == 'demo') {
                    $(".SRP", row).html("<span srp=" + $(this).find("SRP").text() + "><input type='text' style='width:80px;text-align:right' maxlength='10' onkeypress='return isNumberDecimalKey(event,this)' onchange='confirmUpdate(" + $(this).find("AutoId").text() + ")' class='form-control input-sm UnitBasePrice'  value='" + $(this).find('SRP').text() + "'/></span>");
                    //}
                    //else {
                    //    $(".SRP", row).html("<span srp="+$(this).find("SRP").text()+">"+$(this).find("SRP").text()+"</span>");
                    //}

                    $(".Stock", row).html($(this).find("Stock").text());
                    $(".MinPrice", row).html("<input type='text' style='width:80px;text-align:right' maxlength='10' onkeypress='return isNumberDecimalKey(event,this)' onchange='confirmUpdate(" + $(this).find("AutoId").text() + ")' class='form-control input-sm UnitBasePrice'  value='" + $(this).find('MinPrice').text() + "'/>");
                    $(".CostPrice", row).html("<input type='text' style='width:80px;text-align:right' maxlength='10' onkeypress='return isNumberDecimalKey(event,this)' onchange='confirmUpdate(" + $(this).find("AutoId").text() + ")' class='form-control input-sm UnitBasePrice'   value='" + $(this).find('CostPrice').text() + "'/>");
                    $(".ReOrderMark", row).html("<input type='text' style='width:80px;text-align:center' maxlength='10' onkeypress='return isNumberDecimalKey(event,this)' onchange='confirmUpdateReOrderMark(" + $(this).find("AutoId").text() + ")' class='form-control input-sm UnitBasePrice'   value='" + $(this).find('ReOrderMark').text() + "'/>");
                    $(".WMinPrice", row).html("<input type='text' style='width:80px;text-align:right' maxlength='10' onkeypress='return isNumberDecimalKey(event,this)' onchange='confirmUpdate(" + $(this).find("AutoId").text() + ")' class='form-control input-sm UnitBasePrice'  value='" + $(this).find('WHminPrice').text() + "'/>");
                    $(".Price", row).html("<input type='text' style='width:80px;text-align:right' maxlength='10' onkeypress='return isNumberDecimalKey(event,this)' onchange='confirmUpdate(" + $(this).find("AutoId").text() + ")' class='form-control input-sm UnitBasePrice'  value='" + $(this).find('Price').text() + "'/>");
                    $(".Action", row).html('<input type="checkbox" name="table_records"  id="chkPrice">');
                    $("#tblPriceDetails tbody").append(row);
                    row = $("#tblPriceDetails tbody tr:last").clone(true);
                    check = true;
                });

                $("#tblPriceDetails tbody tr").each(function (i) {
                    if ($(this).find('.Action input').prop('checked') == true) {
                        $(this).find('.Action input').prop('checked', false);
                    }
                });
                $("#check-all").prop('checked', false);
            } else {
                $("#tblPriceDetails tbody tr").remove();
            }
            var pager = $(xmldoc).find("Table");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
        },
        error: function (result) {
            swal("Error!", 'Oops,some thing went wrong.Please try later.', "error");
        },
        failure: function (result) {
            swal("Error!", 'Oops,some thing went wrong.Please try later.', "error");
        }
    });
}
function confirmUpdate(ProductAutoId) {
    debugger;
    var Qty = 0, CostPrice = 0, Price = 0, MinPrice = 0, WMinPrice = 0,SRP=0, chk1 = 0, msg = 0;
    $('#tblPriceDetails tbody tr').each(function (index) {
        if (ProductAutoId == $(this).find('.UnitType span').attr('ProductAutoId')) {
            if (parseFloat($(this).find('.CostPrice input').val()) > parseFloat($(this).find('.WMinPrice input').val())) {
                
                chk1 = 1;
                $(this).find('.CostPrice input').addClass('border-warning');
                $(this).find('.CostPrice input').val($(this).find('.CostPrice input').attr('value'));
            } else {
                $(this).find('.CostPrice input').removeClass('border-warning');
            }
            if (parseFloat($(this).find('.WMinPrice input').val()) > parseFloat($(this).find('.MinPrice input').val())) {
                
                chk1 = 2;
                $(this).find('.WMinPrice input').addClass('border-warning');
                $(this).find('.WMinPrice input').val($(this).find('.WMinPrice input').attr('value'))
            } else {
                $(this).find('.WMinPrice input').removeClass('border-warning');
            }
            if (parseFloat($(this).find('.MinPrice input').val()) > parseFloat($(this).find('.Price input').val())) {
                
                chk1 = 3;
                $(this).find('.MinPrice input').addClass('border-warning');
                $(this).find('.MinPrice input').val($(this).find('.MinPrice input').attr('value'))
            } else {
                $(this).find('.MinPrice input').removeClass('border-warning');
            }
            if (parseFloat($(this).find('.UnitType span').attr('PSRP')) > parseFloat($(this).find('.Price input').val())) {
                
                chk = 4;
            } 
            if (checkLength($(this).find('.SRP span').attr('srp')) > 6) {
                
                chk1 = 9;
                $(this).find('.SRP input').addClass('border-warning');
                $(this).find('.SRP input').val($(this).find('.MinPrice input').attr('value'))
            }
            else {
                
                $(this).find('.SRP input').removeClass('border-warning');
            }
            Qty = $(this).find('.Qty').text();
            CostPrice = $(this).find('.CostPrice input').val();
            Price = $(this).find('.Price input').val();
            MinPrice = $(this).find('.MinPrice input').val();
            WMinPrice = $(this).find('.WMinPrice input').val();

            //if ($('#hfLocation').val() == 'psmnj' || $('#hfLocation').val() == 'demo') {
                SRP = $(this).find('.SRP span input').val();
            //}
            //else {
            //    SRP = $(this).find('.SRP span').attr('srp');
            //}      
            if (SRP < Price) {
                $(this).find('.SRP span input').val($(this).find('.SRP span input').attr('value'));
            }
            if (CostPrice == '' || Number(CostPrice) == 0) {
                chk1 = 5;
                //$(this).find('.CostPrice input').addClass('border-warning');
                $(this).find('.CostPrice input').val($(this).find('.CostPrice input').attr('value'));
            }
            else if (Price == '' || Number(Price) == 0) {
                chk1 = 6;
                //$(this).find('.Price input').addClass('border-warning');
                $(this).find('.Price input').val($(this).find('.Price input').attr('value'));
            }
            else if (MinPrice == '' || Number(MinPrice) == 0) {
                chk1 = 7;
                //$(this).find('.MinPrice input').addClass('border-warning');
                $(this).find('.MinPrice input').val($(this).find('.MinPrice input').attr('value'));
            }
            else if (WMinPrice == '' || Number(WMinPrice) == 0) {
                chk1 = 8;
                //$(this).find('.WMinPrice input').addClass('border-warning');
                $(this).find('.WMinPrice input').val($(this).find('.WMinPrice input').attr('value'));
            }
            else if (SRP == '' || Number(SRP) == 0) {
                chk1 = 10;
                //$(this).find('.WMinPrice input').addClass('border-warning');
                $(this).find('.SRP input').val($(this).find('.SRP input').attr('value'));
            }
            else {
                $(this).find('.CostPrice input').removeClass('border-warning');
                $(this).find('.Price input').removeClass('border-warning');
                $(this).find('.MinPrice input').removeClass('border-warning');
                $(this).find('.WMinPrice input').removeClass('border-warning');
                $(this).find('.SRP input').removeClass('border-warning');
            }
        }
    });
    var data = {
        ProductAutoId: ProductAutoId,
        Qty: Qty,
        CostPrice: CostPrice,
        Price: Price,
        MinPrice: MinPrice,
        WMinPrice: WMinPrice,
        SRP:SRP
    }
    if (chk1 == 1) {
        swal("", "Cost price should be less or equal to whole sale min price.", "error");
        return;
    }
    else if (chk1 == 2) {
        swal("", "Whole sale min price should be less or equal to retail min price.", "error");
        return;
    }
    else if (chk1 == 3) {
        swal("", "Retail min price should be less or equal to base price.", "error");
        return;
    }
    else if (chk1 == 4) {
        swal("", "SRP can't be greater than product base price.", "error");
        return;
    }
    else if (chk1 == 5) {
        swal("", "Cost Price can't be zero or blank.", "error");
        return;
    }
    else if (chk1 == 6) {
        swal("", "Base Price can't be zero or blank.", "error");
        return;
    } else if (chk1 == 7) {
        swal("", "Retail Min Price can't be zero or blank.", "error");
        return;
    } else if (chk1 == 8) {
        swal("", "Whole Sale Min Price can't be zero or blank.", "error");
        return;
    } else if (chk1 == 9) {
        swal("", "You are reaching max limit.Maximum 6 digit allowed.", "error");
        return;
    }
    else if (chk1 == 10) {
        swal("", "SRP can't be zero or blank.", "error");
        return;
    }
   
    updatePrice(data);
    
}
function confirmUpdateReOrderMark(ProductAutoId) {
    var ReOrderMark = 0, chk2 = 0;
    $('#tblPriceDetails tbody tr').each(function (index) {
        if (ProductAutoId == $(this).find('.UnitType span').attr('ProductAutoId')) {
            Qty = $(this).find('.Qty').text();
            ReOrderMark = $(this).find('.ReOrderMark input').val() * Number(Qty);
            if (ReOrderMark == '') {
                chk2 = 1;
                $(this).find('.ReOrderMark input').addClass('border-warning');
            }
            else {
                $(this).find('.ReOrderMark input').removeClass('border-warning');
            }
        }
    });
    var data = {

        ProductAutoId: ProductAutoId,
        Qty: Qty,
        ReOrderMark: ReOrderMark
    }
    if (chk2 == 2) {
        swal("", "Re Order Mark can't be empty.", "error");
        return;
    }
    updateReOrderMark(data);

}
function updatePrice(data) {
    $.ajax({
        type: "POST",
        url: "ManagePrice.aspx/UpdatePrice",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d == "true") {
                    swal("", "Price updated successfully.", "success");
                    getProductDetails(1);
                } else {
                    swal("", response.d, "error");
                }
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            swal("Error!", 'Oops,some thing went wrong.Please try later.', "error");
        },
        failure: function (result) {
            swal("Error!", 'Oops,some thing went wrong.Please try later.', "error");
        }
    });

}
function updateReOrderMark(data) {
    $.ajax({
        type: "POST",
        url: "ManagePrice.aspx/updateReOrderMark",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d == "true") {
                    swal("", "Re Order Mark updated successfully.", "success");
                    getProductDetails(1);
                } else {
                    swal("", response.d, "error");
                }
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            swal("Error!", 'Oops,some thing went wrong.Please try later.', "error");
        },
        failure: function (result) {
            swal("Error!", 'Oops,some thing went wrong.Please try later.', "error");
        }
    });

}

function showChangePriceModelPop(ProductAutoId) {

    var CountTotalSelectedProduct = $('input:checkbox:checked', '#tblPriceDetails tbody').length;
    //if ($('#hfLocation').val() == 'psmnj' || $('#hfLocation').val() == 'psmnj1' || $('#hfLocation').val() == 'localhost' || $('#hfLocation').val() == 'psm' || $('#hfLocation').val() == 'demo') {
        $("#divSRP").show();
    //}
    //else {
    //    $("#divSRP").hide();
    //}
    if ($('input:checkbox:checked', '#tblPriceDetails tbody').length > 0) {

        $("#totalProductSelected").html('(' + CountTotalSelectedProduct + ' Product selected)');
        $("#hdProductAutoId").val(ProductAutoId);
        $("#ChangePricePopup").modal('show');
        /*$("#txtReOrderMask").val('0');*/
        $("#txtCostPrice").val('0.00');
        $("#txtWhMinPrice").val('0.00');
        $("#txtRetailMinPrice").val('0.00');
        $("#txtBasePrice").val('0.00');
    }
    else {
        toastr.error('Error : No Product selected.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }

    //getProductDetails(1);
}

function UpdateChangePrice() {

    var ReOrderMarkChange = 0, chkchange1 = 0, CostChangePrice = 0, MinChangePrice = 0, BaseChangePrice = 0, WMinChangePrice = 0, SRP = 0;

    if (checkLength($("#txtCostPrice").val()) > 6) {
        toastr.error('You are reaching max limit.Maximum 6 digit allowed.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $("#txtCostPrice").addClass('border-warning');
        return;
    }
    else {
        $("#txtCostPrice").removeClass('border-warning');
    }
    if (checkLength($("#txtWhMinPrice").val()) > 6) {
        toastr.error('You are reaching max limit.Maximum 6 digit allowed.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $("#txtWhMinPrice").addClass('border-warning');
        return;
    }
    else {
        $("#txtWhMinPrice").removeClass('border-warning');
    }
    if (checkLength($("#txtRetailMinPrice").val()) > 6) {
        toastr.error('You are reaching max limit.Maximum 6 digit allowed.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $("#txtRetailMinPrice").addClass('border-warning');
        return;
    }
    else {
        $("#txtRetailMinPrice").removeClass('border-warning');
    }
    if (checkLength($("#txtBasePrice").val()) > 6) {
        toastr.error('You are reaching max limit.Maximum 6 digit allowed.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $("#txtBasePrice").addClass('border-warning');
        return;
    }
    else {
        $("#txtBasePrice").removeClass('border-warning');
    }
    if (checkLength($("#txtSRP").val()) > 6) {
        toastr.error('You are reaching max limit.Maximum 6 digit allowed.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $("#txtSRP").addClass('border-warning');
        return;
    }
    else {
        $("#txtSRP").removeClass('border-warning');
    }
    if (parseFloat($("#txtCostPrice").val()) > parseFloat($("#txtWhMinPrice").val())) {
        chkchange1 = 1;
        $("#txtCostPrice").addClass('border-warning');
    } else {
        $("#txtCostPrice").removeClass('border-warning');
    }
    if (parseFloat($("#txtWhMinPrice").val()) > parseFloat($("#txtRetailMinPrice").val())) {
        chkchange1 = 2;
        $("#txtWhMinPrice").addClass('border-warning');
    } else {
        $("#txtWhMinPrice").removeClass('border-warning');
    }
    if (parseFloat($("#txtRetailMinPrice").val()) > parseFloat($("#txtBasePrice").val())) {
        chkchange1 = 3;
        $("#txtRetailMinPrice").addClass('border-warning');
    } else {
        $("#txtRetailMinPrice").removeClass('border-warning');
    }
    //ReOrderMarkChange = $("#txtReOrderMask").val().trim();
    CostChangePrice = $("#txtCostPrice").val().trim();
    BaseChangePrice = $("#txtBasePrice").val().trim();
    MinChangePrice = $("#txtRetailMinPrice").val().trim();
    WMinChangePrice = $("#txtWhMinPrice").val().trim();
    SRP = $("#txtSRP").val().trim();
    if (CostChangePrice == '' || Number(CostChangePrice) == 0) {
        chkchange1 = 4;
        $("#txtCostPrice").addClass('border-warning');
    }

    else if (WMinChangePrice == '' || Number(WMinChangePrice) == 0) {
        chkchange1 = 5;
        $("#txtWhMinPrice").addClass('border-warning');
    }
    else if (MinChangePrice == '' || Number(MinChangePrice) == 0) {
        chkchange1 = 6;
        $("#txtRetailMinPrice").addClass('border-warning');
    } 
    else if (BaseChangePrice == '' || Number(BaseChangePrice) == 0) {
        chkchange1 = 7;
        $("#txtBasePrice").addClass('border-warning');
    } else {
        $("#txtCostPrice").removeClass('border-warning');
        $("#txtWhMinPrice").removeClass('border-warning');
        $("#txtRetailMinPrice").removeClass('border-warning');
        $("#txtBasePrice").removeClass('border-warning');
    }

  
    ProductAutoIdData = new Array();
    $('input:checkbox:checked', '#tblPriceDetails tbody').each(function (index, item) {
        var row = $(item).closest('tr');
        ProductAutoIdData[index] = new Object();
        ProductAutoIdData[index].ProductAutoId = row.find('.ProductId span').attr("ProductAutoId");
    });
    var data = {
        //ReOrderMarkChange: ReOrderMarkChange,
        CostChangePrice: CostChangePrice,
        BaseChangePrice: BaseChangePrice,
        MinChangePrice: MinChangePrice,
        WMinChangePrice: WMinChangePrice,
        SRP: SRP
    }
    if (chkchange1 == 1) {
        swal("", "Cost price should be less or equal to whole sale min price.", "error");
        return;
    }
    else if (chkchange1 == 2) {
        swal("", "Whole sale min price should be less or equal to retail min price.", "error");
        return;
    }
    else if (chkchange1 == 3) {
        swal("", "Retail min price should be less or equal to base price.", "error");
        return;
    }
    else if (chkchange1 == 4) {
        swal("", "Cost Price can't be zero.", "error");
        return;
    }
    else if (chkchange1 == 5) {
        swal("", "Whole Sale Min Price can't be zero.", "error");
        return;
    }
    else if (chkchange1 == 6) {
        swal("", "Retail Min Price can't be zero.", "error");
        return;
    } else if (chkchange1 == 7) {
        swal("", " Base Price can't be zero.", "error");
        return;
    }
    updateChangePrice(data, ProductAutoIdData);
}

function updateChangePrice(data, ProductAutoIdData) {
    $.ajax({
        type: "POST",
        url: "ManagePrice.aspx/UpdateChangedPrice",
        data: "{'dataValue':'" + JSON.stringify(data) + "','ProductAutoIdValue':'" + JSON.stringify(ProductAutoIdData) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d == "true") {
                    swal("", "Price Changed successfully.", "success");
                    getProductDetails(1);
                    $("#ChangePricePopup").modal('hide');
                    $('#chkAllPrice').prop('checked', false);
                    $("#txtSRP").val('0.00');
                }
                else if (response.d=='SRP')
                {
                    swal("", "Base Price can't be greater than product SRP.", "error");
                }
                else {
                    swal("", response.d, "error");
                }
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            swal("Error!", 'Oops,some thing went wrong.Please try later.', "error");
        },
        failure: function (result) {
            swal("Error!", 'Oops,some thing went wrong.Please try later.', "error");
        }
    });

}

function checkAllProduct() {
    var checked = 0;
    if ($("#chkAllPrice").prop('checked') == true) {
        checked = 1;
    }
    $('#tblPriceDetails tbody tr').each(function (index) {
        if (checked == 1) {
            $(this).find('.Action input').prop('checked', true);
        }
        else {
            $(this).find('.Action input').prop('checked',false);
        }
    })
}

function checkLength(num) {
    
    var nString = String(num);
    var nArray = nString.split('.');
    if (nArray[0].length > 6) {
        return nArray[0].length;
    }
}

function showChangeReorderModelPop(ProductAutoId) {

    var CountTotalSelectedProduct = $('input:checkbox:checked', '#tblPriceDetails tbody').length;
    //if ($('#hfLocation').val() == 'psmnj' || $('#hfLocation').val() == 'psmnj1' || $('#hfLocation').val() == 'localhost' || $('#hfLocation').val() == 'psm' || $('#hfLocation').val() == 'demo') {
    $("#divSRP").show();
    //}
    //else {
    //    $("#divSRP").hide();
    //}
    if ($('input:checkbox:checked', '#tblPriceDetails tbody').length > 0) {

        $("#totalProductSelection").html('(' + CountTotalSelectedProduct + ' Product selected)');
        $("#hdProductAutoId").val(ProductAutoId);
        $("#ChangeReorderPopup").modal('show');
        $("#txtReOrderMask").val('0');
        $("#txtReOrderMask").removeClass('border-warning');
    }
    else {
        toastr.error('Error : No Product selected.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }

    //getProductDetails(1);
}

function UpdateChangeReorderMark() {
    debugger;
    var ReOrderMarkChange = 0; chkchange1 = 0;
    ReOrderMarkChange = $("#txtReOrderMask").val().trim();
   
    if (ReOrderMarkChange == '' || Number(ReOrderMarkChange) == 0) {
        chkchange1 = 4;
        $("#txtReOrderMask").addClass('border-warning');
        toastr.error('Error : Re Order Mark field is mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }else {
        $("#txtReOrderMask").removeClass('border-warning');
    }

    if (chkchange1 != 4) {
        ProductAutoIdData = new Array();
        $('input:checkbox:checked', '#tblPriceDetails tbody').each(function (index, item) {
            var row = $(item).closest('tr');
            ProductAutoIdData[index] = new Object();
            ProductAutoIdData[index].ProductAutoId = row.find('.ProductId span').attr("ProductAutoId");
        });
        var data = {
            ReOrderMarkChange: ReOrderMarkChange
        }
        updateChangeReorderMark(data, ProductAutoIdData);
    }
}

function updateChangeReorderMark(data, ProductAutoIdData) {
    debugger;
    $.ajax({
        type: "POST",
        url: "ManagePrice.aspx/UpdateChangedReorderMark",
        data: "{'dataValue':'" + JSON.stringify(data) + "','ProductAutoIdValue':'" + JSON.stringify(ProductAutoIdData) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d == "true") {
                    swal("", "Re order Mark Changed successfully.", "success");
                    getProductDetails(1);
                    $("#ChangeReorderPopup").modal('hide');
                    $("#txtReOrderMask").removeClass('border-warning');
                    //$('#chkAllPrice').prop('checked', false);
                    //$("#txtSRP").val('0.00');
                }
                else {
                    swal("", response.d, "error");
                }
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            swal("Error!", 'Oops,some thing went wrong.Please try later.', "error");
        },
        failure: function (result) {
            swal("Error!", 'Oops,some thing went wrong.Please try later.', "error");
        }
    });

}


