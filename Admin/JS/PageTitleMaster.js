﻿

$(document).ready(function () {
    $('#txtSFromDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtSToDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    $("#txtSFromDate").val(month + '/' + day + '/' + year);
    $("#txtSToDate").val(month + '/' + day + '/' + year);

    getPageTitleDetailList(1);
});
function Pagevalue(e) {
    getPageTitleDetailList(parseInt($(e).attr("page")));
};
/*------------------------------------------------------Insert Page Title Master Detail------------------------------------------------------------*/
function savePageTitleDetail() {
    if (checkRequiredField()) {
        var data = {
            PageId: $("#txtPageId").val().trim(),
            PageUrl: $("#txtPageUrl").val().trim(),
            PageTitle: $("#txtPageTitle").val().trim(),
            Status: $("#ddlPageTitleStatus").val(),
            Location: $("#ddlLocation").val(),
            UserDescription: $("#txtUserDescription").val(),
            AdminDescription: $("#txtAdminDescription").val(),
        }
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/PageTitleMaster.asmx/savePageTitleMasterDetail",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d == "false") {
                    swal("Error!", "" + response.d, "error");
                    return;
                }
                if (response.d != "Session Expired") {
                    if (response.d == 'PageId already exist !') {
                        swal("", "" + response.d, "error");
                    }
                    else {
                        resetButton();
                        swal("", "Page Title details saved successfully.", "success").then(function () {
                          
                            location.reload(true); 

                            getPageTitleDetailList(1);

                        });
                    }
                } else {
                    location.href = '/';
                }
            },
            error: function (result) {
                swal("Error!", "Page Title details not saved", "error");
            },
            failure: function (result) {
                swal("Error!", 'Oops,something went wrong.Please try again.', "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------------------------------------------------------Reset Detail------------------------------------------------------------------- */
function resetButton() {
    $("#txtPageId").val(''),
        $("#txtPageUrl").val(''),
        $("#txtPageTitle").val(''),
        $("#ddlPageTitleStatus").val("1"),
        $("#ddlLocation").val("0"),
        $("#AllLocation").show();
        $("#IndiviLocation").hide();
        $("#txtUserDescription").val(""),
        $("#txtAdminDescription").val(""),
        $(".alert").hide();
    $(".cbLocation").prop('checkd', false);
}
/*------------------------------------------------------bind Page Title Detail List ------------------------------------------------------------*/
function getPageTitleDetailList(PageIndex) {
    var data = {
        SPageId: $("#sPageID").val().trim(),
        SPageUrl: $("#sPageUrl").val().trim(),
        SPageTitle: $("#sPageTitle").val().trim(),
        SStatus: $("#ddlStatus").val(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        PageIndex: PageIndex,
        PageSize: $('#ddlPageSize').val(),
    };
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/PageTitleMaster.asmx/getPageTitleMasterDetail",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successgetReport,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function successgetReport(response) {

    if (response.d != "Session Expired") {
        if (response.d != "false") {
            var xmldoc = $.parseXML(response.d);
            var ReportData = $(xmldoc).find("Table1");
            $("#tblpageTitleList tbody tr").remove();
            var row = $("#tblpageTitleList thead tr").clone(true);
            if (ReportData.length > 0) {
                $("#EmptyTable").hide();
                var i = 0;
                $.each(ReportData, function (index) {
                    i++;
                    $(".Action", row).html("<a title='Edit' onclick='sendupdateAutoId(" + $(this).find("AutoId").text() + ")'><span class='la la-edit'></span></a>");
                    $(".PageId", row).text($(this).find("PageId").text());
                    $(".PageUrl", row).text($(this).find("PageUrl").text());
                    $(".PageTitle", row).text($(this).find("PageTitle").text());
                    $(".Status", row).text($(this).find("Status").text());
                    $(".AdminDescription", row).html($(this).find("AdminDescription").text());
                    $(".UserDescription", row).html($(this).find("UserDescription").text());
                    $("#tblpageTitleList tbody").append(row);
                    row = $("#tblpageTitleList tbody tr:last").clone(true);
                });
            }
            else {
                $("#EmptyTable").show();
            }


            var pager = $(xmldoc).find("Table");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
        }
    }
    else {
        location.href = "/";
    }
}
/*------------------------------------------------------------------update---------------------------------------------*/
function sendupdateAutoId(AutoId) {
    var data = {
        AutoId: AutoId,
    };
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/PageTitleMaster.asmx/getData",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var pageTitleMasterDetail = $(xmldoc).find("Table");

            $("#btnSave").hide();
            $("#btnReset1").hide();
            $("#btnUpdate").show();
            $("#AllLocation").hide();
            $("#txtPageId").prop("disabled", true);
            $("#IndiviLocation").show();

            $("#txtPageId").val($(pageTitleMasterDetail).find('PageId').text());
            $("#txtPageUrl").val($(pageTitleMasterDetail).find('PageUrl').text());
            $("#txtPageTitle").val($(pageTitleMasterDetail).find('PageTitle').text());
            $("#ddlPageTitleStatus").val($(pageTitleMasterDetail).find('Status').text());
            $("#txtUserDescription").summernote("code", $(pageTitleMasterDetail).find('UserDescription').text());
            $("#txtAdminDescription").summernote("code", $(pageTitleMasterDetail).find('AdminDescription').text());
        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function updatePageTitleDetail() {
    if (checkRequiredField()) {
        var data = {
            PageId: $("#txtPageId").val().trim(),
            PageUrl: $("#txtPageUrl").val().trim(),
            PageTitle: $("#txtPageTitle").val().trim(),
            Status: $("#ddlPageTitleStatus").val(),
            Location: $("#ddlIndividualLocation").val(),
            UserDescription: $("#txtUserDescription").val(),
            AdminDescription: $("#txtAdminDescription").val(),
        }
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/PageTitleMaster.asmx/updatePageTitleMasterDetail",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                debugger;
                if (response.d == "false") {
                    swal("Error!", "PageId does not exist !", "error");
                    return;
                }
                if (response.d != "Session Expired") {
                    debugger;
                    if (response.d == 'PageId does not exist !') {
                        swal("", "" + response.d, "error");
                    }
                    else {
                        resetButton();
                        swal("", "Page Title details updated successfully.", "success").then(function () {
                            getPageTitleDetailList(1);
                            resetButton();
                            $("#btnSave").show();
                            $("#btnReset1").show();
                            $("#btnUpdate").hide();
                            location.reload(true);

                        });
                    }
                } else {
                    location.href = '/';
                }
            },
            error: function (result) {
                swal("Error!", "Page Title details not updated", "error");
            },
            failure: function (result) {
                swal("Error!", 'Oops,something went wrong.Please try again.', "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}

function IsNumeric() {
    var data = $("#ddlCommCode").val()
    return parseFloat(data) == data;
}
function showConfirmBox() {
    if ($("#IsShowOnWebsite1").is(":checked")) {
        var msg = "You want to show this product on website.";
        var status = 1;
    } else {
        var msg = "You want to hide this product from website.";
        var status = 0;
    }
    swal({
        title: "Are you sure?",
        text: msg,
        icon: "warning",
        showCancelButton: true,
        allowOutsideClick: false,
        closeOnClickOutside: false,
        buttons: {
            cancel: {
                text: "No, Cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes !",
                value: true,
                visible: true,
                className: "",
                closeModal: true
            }
        }
    }).then(isConfirm => {
        if (isConfirm) {
            if (status == 0) {
                $('#IsShowOnWebsite1').prop('checked', false);
            } else {
                $('#IsShowOnWebsite1').prop('checked', true);
            }
        } else {
            if (status == 0) {
                $('#IsShowOnWebsite1').prop('checked', true);
            } else {
                $('#IsShowOnWebsite1').prop('checked', false);
            }
        }
    })
}
