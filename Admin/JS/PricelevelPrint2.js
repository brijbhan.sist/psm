﻿var getid = 0;
$(document).ready(function () {
    getid = getQueryString('PLAutoId');
    if (getid != null) {
        PrintPriceLevel2(getid);
    }
});

var getQueryString = function (field, url) {
    var href = url ? url : window.location.href;
    var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
    var string = reg.exec(href);
    return string ? string[1] : null;
};


function PrintPriceLevel2(PriceLevelAutoId) {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/PriceTemplate.asmx/PrintPriceLevel2",
        data: "{'PriceLevelAutoId':'" + PriceLevelAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('#fade').show();
        },
        complete: function () {
            $('#fade').hide();
        },
        success: function (response) {

            if (response.d != 'Session Expired') {
                var getData = $.parseJSON(response.d);
                $("#PriceLevelName").html(getData[0].pl[0].PriceLevelName);
                $("#CurrentDate").html(getData[0].pl[0].CurrentDate);
                $("#Customer").html(getData[0].pl[0].Customer);

                var row = $("#tblProduct thead tr").clone();
                $.each(getData[0].pd, function (index, product) {
                    $(".ProductId", row).html(product.ProductId);
                    $(".ProductName", row).html(product.ProductName);
                    $(".UnitType", row).html(product.UnitType);
                    $(".CustomPrice", row).html(parseFloat(product.CustomPrice).toFixed(2));
                    $(".SRP", row).html(parseFloat(product.SRP).toFixed(2));
                    $("#tblProduct tbody").append(row);
                    row = $("#tblProduct tbody tr:last-child").clone();
                });

                $("#logo").attr("src", "../Img/logo/" + getData[0].cd[0].Logo);
                $("#Address").text(getData[0].cd[0].Address);
                $("#Phone").text(getData[0].cd[0].MobileNo);
                $("#FaxNo").text(getData[0].cd[0].FaxNo);
                $("#Website").text(getData[0].cd[0].Website);
            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
