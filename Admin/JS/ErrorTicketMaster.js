﻿var imageattach = "";
$(document).ready(function () {
    setInterval(function () {
        formatAMPM();
    },1000);
    
    
});

function formatAMPM() {
    date = new Date();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var today = (date.getMonth() + 1) + '/' + date.getDate() + '/' +  date.getFullYear();
    var strTime = today + " " + hours + ':' + minutes + ' ' + ampm;
    $("#txtdate").val(strTime);
}

/*-----------------------------------------------------Generate Ticket---------------------------------------------------------------------------*/
$("#ButtonTkSave").click(function () {
    if (ErrorticketcheckRequiredField())
    {     
        var timeStamp = Date.parse(new Date());
        var attach = "";
        if ($("#txtfileUpload").val() != "") {
            attach = timeStamp + '_' + $("#txtfileUpload").val().split('\\').pop()
        }
        else {
            attach = imageattach;
        }
        var data = {                    
            Type: $("#ddltype").val(),
            Priority: $("#ddlproir").val(),
            Subject: $('#txtsubject').val(),
            PageUrl: $("#txturl").val(),                    
            Description: $('#txtcomment').val(),
            Attach: attach
        }
        $.ajax({
            type: "POST",
            url: "WebAPI/WErrorTicketMaster.asmx/GenerateTicket",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },                          
            success: function (data) {
                if (data.d == 'true') {
                    $("#ButtonTkSave").attr('disabled', true);
                    if ($("#txtfileUpload").val() != "") {                       
                        var fileUpload = $("#txtfileUpload").get(0);
                        var files = fileUpload.files;
                        var test = new FormData();
                        for (var i = 0; i < files.length; i++) {
                            test.append(files[i].name, files[i]);
                        }
                        $.ajax({
                            url: "/TicketFilehandler.ashx?timestamp=" + timeStamp,
                            type: "POST",
                            contentType: false,
                            processData: false,
                            data: test,
                            success: function (result) {
                                Console.log(result)
                            },
                            error: function (err) {
                                console.log(err);
                            }
                        });                      
                    }
                    swal("", "Ticket generated successfully.", "success");                   
                   reset();
                }
                else {
                    swal("Error!", "Oops something went wrong.Please try later.", "error");
                }               
                $("#ButtonTkSave").attr('disabled', false);
            },
            error: function (result) {
                swal("warning!", result.d, "warning");
            },
            failure: function (result) {
                swal("warning!", result.d, "warning");
            }

        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
});

function reset()
{  
    $("#ddltype").val('0');
    $("#ddlproir").val('0');
    $('#txtsubject').val('');
    $("#txturl").val('');
    $('#txtcomment').val('');
    $("#txtfileUpload").val('');
}

function ErrorticketcheckRequiredField() {
    var boolcheck = true;
    $('.req').each(function () {
        if ($(this).val() == '') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });
   
    $('.ddlreq').each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == 'undefined' || $(this).val() == 'Select') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });
    return boolcheck;
}
