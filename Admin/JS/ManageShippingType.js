﻿$(document).ready(function () {
    GetShippingType();
});

function saveShippingType() {
    if (checkRequiredField()) {
        if ($("#txtEnableTax").prop('checked') == true) {
            var taxEnable = 1;
        } else {
            var taxEnable = 0;
        }
        var data = {
            ShippingType: $("#txtShippingType").val().trim(),
            Status: $("#ddlStatus").val(),
            EnableTax: taxEnable
        }
        $.ajax({
            type: "POST",
            url: "ManageShippingType.aspx/insertShippingType",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == "Session Expired") {
                    window.location = "/";
                }
                else if (result.d == 'Success') {
                    swal("", "Shipping type saved successfully.", "success");
                    $("#txtShippingType").val('');
                    $("#ddlStatus").val('1');
                    $("#txtEnableTax").prop('checked', false);
                    GetShippingType();

                }
                else if (result.d == 'Shipping Type Already Exists') {
                    swal("", "Shipping Type Already Exists.", "error");

                }
                else {
                    swal("Error!", result.d, "error");
                }
            },
            error: function (result) {

            },
            failure: function (result) {

            }
        });
    }
    else {
        toastr.error('All * fields are mandatory .', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
}


function GetShippingType() {

    $.ajax({
        type: "POST",
        url: "ManageShippingType.aspx/GetShippingType",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
            if ($.fn.DataTable.isDataTable('#tblShippingType')) {
                $('#tblShippingType').DataTable().destroy();
            }
        },
        complete: function () {
            $.unblockUI();
        },
        success: onSuccessOfShipping,
        error: function (response) {
            console.log(JSON.parse(response.responseText).d);
        }
    });
}
function onSuccessOfShipping(response) {
    var xmldoc = $.parseXML(response.d);
    var category = $(xmldoc).find('Table');

    if (category.length > 0) {

        $('#tblShippingType tbody tr').remove();
        var row = $('#tblShippingType thead tr').clone(true);
        $.each(category, function () {
            $(".AutoId", row).html($(this).find("AutoId").text());
            $(".ShippingType", row).html($(this).find("ShippingType").text());
            if ($(this).find("Shippingstatus").text() == '1') {
                $(".Status", row).html("<span class='badge badge badge-pill badge-success'>Active</span>");
            } else {
                $(".Status", row).html("<span class='badge badge badge-pill badge-danger'>Inactive</span>");

            }
            if ($(this).find("EnabledTax").text() == '1') {
                $(".TaxEnable", row).html("<span class='badge badge badge-pill badge-success'>Yes</span>");
            } else {
                $(".TaxEnable", row).html("<span class='badge badge badge-pill badge-danger'>No</span>");

            }
            $(".action", row).html("<a title='Edit' href='#'><span class='ft-edit' onclick='editShippingType(\"" + $(this).find("AutoId").text() + "\")' /></a>");
            $("#tblShippingType tbody").append(row);
            row = $("#tblShippingType tbody tr:last-child").clone(true);
        });
    }
    else {
        $('#tblShippingType tbody tr').remove();

    }

}

function editShippingType(AutoId) {
    var data = {
        AutoId: AutoId
    }
    $.ajax({
        type: "POST",
        url: "ManageShippingType.aspx/editShippingType",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: onSuccessOfEdit,
        error: function (response) {
            console.log(response.d)
        }
    })
}
function onSuccessOfEdit(response) {
    if (response.d == 'Session Expired') {
        location.href = '/';
    } else {
        var xmldoc = $.parseXML(response.d);
        var Shipping = $(xmldoc).find('Table');
        $("#hdnShippingAutoId").text($(Shipping).find("AutoId").text());
        $("#txtShippingType").val($(Shipping).find("ShippingType").text());
        $("#ddlStatus").val($(Shipping).find("Shippingstatus").text());
        if ($(Shipping).find("EnabledTax").text() == 1) {
            $("#txtEnableTax").attr('checked', true);
        } else {
            $("#txtEnableTax").prop('checked', false);
        }
        $("#txtEnableTax").attr("disabled", true);
        $("#btnSave").hide();
        $("#btnReset").hide();
        $("#btnUpdate").show();
        $("#btnCancel").show();
    }
}

function updateShippingType() {
    if (checkRequiredField()) {
        var data = {
            AutoId: $("#hdnShippingAutoId").text(),
            ShippingType: $("#txtShippingType").val().trim(),
            Status: $("#ddlStatus").val()

        };

        $.ajax({
            type: "POST",
            url: "ManageShippingType.aspx/updateShippingType",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json;charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },

            success: function (result) {
                if (result.d == "Session Expired") {
                    window.location = "/";
                }
                else if (result.d == 'Success') {
                    swal("", "Shipping type updated successfully.", "success");
                    GetShippingType();
                    $("#txtShippingType").val('');
                    $("#ddlStatus").val('1');

                } else {
                    swal("Error!", result.d, "error");
                }
            },
            error: function (result) {
                swal("Error!", "Oops! Something went wrong.Please try later", "error");
            },
            failure: function (result) {
                swal("Error!", "Oops! Something went wrong.Please try later", "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
}

function cancelBtn() {
    $("#txtShippingType").val('');
    $("#btnUpdate").hide();
    $("#btnCancel").hide();
    $("#btnSave").show();
    $("#btnReset").show();
}

function resetBtn() {
    $("#txtShippingType").val('');
    $("#btnUpdate").hide();
    $("#btnCancel").hide();
    $("#btnSave").show();
    $("#btnReset").show();
}

function deleterecord(AutoId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this shipping type!",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: false,
            },
            confirm: {
                text: "Yes, delete it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(isConfirm => {
        if (isConfirm) {
            deleteShippingType(AutoId);

        } else {
            swal("", "Your data is safe.", "error");
        }
    })
}

//function deleteShippingType(AutoId) {
//    var data = {
//        AutoId: AutoId
//    };
//    $.ajax({
//        type: "POST",
//        url: "ManageShippingType.aspx/deleteShippingType",
//        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
//        contentType: "application/json;charset=utf-8",
//        datatype: "json",
//        beforeSend: function () {
//            $.blockUI({
//                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
//                overlayCSS: {
//                    backgroundColor: '#FFF',
//                    opacity: 0.8,
//                    cursor: 'wait'
//                },
//                css: {
//                    border: 0,
//                    padding: 0,
//                    backgroundColor: 'transparent'
//                }
//            });
//        },
//        complete: function () {
//            $.unblockUI();
//        },
//        success: function (result) {
//            if (result.d == "Session Expired") {
//                window.location = "/";
//            }
//            else if (result.d == 'Success') {
//                swal("", "Brand details deleted successfully.", "success");
//                GetShippingType();
//            } else {
//                swal("Error!", result.d, "error");
//            }
//        },
//        error: function (result) {
//            swal("Error!", "Oops! Something went wrong.Please try later", "error");

//        },
//        failure: function (result) {
//            swal("Error!", "Oops! Something went wrong.Please try later", "error");
//        }
//    })
//}
