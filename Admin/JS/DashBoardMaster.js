﻿var EmpType = '';
$(document).ready(function () {
    if ($("#hEmpTypeNo").val() == '9') {
        InvalidPacking();
    } 
    getTicketList();
    getWebOrderList();
})
function closewebsiteorderlist() {
    bindCustomer();
    $("#ShowWebOrderList").modal('hide');
}
function getSalesRevenue_Graph() {
    $.ajax({
        type: "POST",
        url: "/admin/WebAPI/WDashBoardMaster.asmx/getSalesRevenue",
        data: "{'Type':'" + $("#ddlSalesRevenue").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "false") {
                var xmldoc = $.parseXML(response.d);
                var salesPersonOrderDetails = $(xmldoc).find("Table");
                var newarray = [];
                newarray.push(['Sales Person', 'Total Sold']);
                if (salesPersonOrderDetails.length > 0) {
                    $.each(salesPersonOrderDetails, function () {
                        newarray.push([$(this).find("SalesPerson").text(), parseFloat($(this).find("TotalSales").text())]);
                    });
                }
                var data = new google.visualization.arrayToDataTable(newarray);
                var options_bar = {
                    height: 400,
                    width: 600,
                    fontSize: 12,
                    colors: ['#2494be', '#F6B75A'],
                    chartArea: {
                        left: '20%',
                        width: '77%',
                        height: 350
                    },
                    hAxis: {
                        gridlines: {
                            color: '#e9e9e9',
                        },
                    },
                    vAxis: {
                        gridlines: {
                            count: 10
                        },
                        minValue: 0
                    },
                    legend: {
                        position: 'top',
                        alignment: 'center',
                        textStyle: {
                            fontSize: 12
                        }
                    }
                };
                var bar = new google.visualization.BarChart(document.getElementById('bar-chart'));
                bar.draw(data, options_bar);
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function LoadDashboard() {
    // getSalesRevenue_Graph();
    getOrderdetails();
    $("#dashboard").show();
    getSalesRevenue();
    CollectionDetails();
    CreditMemoDetails();
    $('#imgPreview').hide();
}
function getOrderdetails() {
    $.ajax({
        type: "POST",
        url: "/admin/WebAPI/WDashBoardMaster.asmx/getDashboardList",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "false") {
                var xmldoc = $.parseXML(response.d);
                var TodaysOverview = $(xmldoc).find("Table");
                var CompanyDetails = $(xmldoc).find("Table2");
                $("#imgPreviewProfile").attr("src", "/Img/logo/" + $(CompanyDetails).find("Logo").text());
                $("#imgLogo").val($(CompanyDetails).find("Logo").text());
                $("#TotalOrders").html(parseInt($(TodaysOverview).find('TotalOrder').text()));
                $("#ThresholdValue").html(parseInt($(TodaysOverview).find('ThresholdValue').text()));
                if ($(TodaysOverview).find('OutOfStock').text() != '') {
                    $("#OutOfStock").html(parseInt($(TodaysOverview).find('OutOfStock').text()));
                } else {
                    $("#OutOfStock").html('0');
                }
                $("#TotalSales").html(parseFloat($(TodaysOverview).find('TotalSales').text()).toFixed(2));
                var orderList = $(xmldoc).find("Table1");
                var salesPersonOrderDetails = $(xmldoc).find("Table2");


                $("#tableOrder tbody tr").remove();
                var row = $("#tableOrder thead tr:last-child").clone(true);

                if (orderList.length > 0) {
                    $.each(orderList, function () {
                        $(".StatusType", row).text($(this).find("StatusType").text());
                        $(".TotalOrder", row).text($(this).find("TotalOrder").text());

                        $("#tableOrder tbody").append(row);
                        row = $("#tableOrder tbody tr:last").clone(true);
                    });
                    $("#tableOrder tbody tr").show();
                }


            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function getSalesRevenue() {
    $.ajax({
        type: "POST",
        url: "/admin/WebAPI/WDashBoardMaster.asmx/getSalesRevenue",
        data: "{'Type':'" + $("#ddlSalesRevenue").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "false") {
                var xmldoc = $.parseXML(response.d);
                var salesPersonOrderDetails = $(xmldoc).find("Table");
                $("#tableSalesRevenue tbody tr").remove();
                var row = $("#tableSalesRevenue thead tr:last-child").clone(true);
                var TotalOrder = 0, TotalSales = 0, AOV = 0;
                if (salesPersonOrderDetails.length > 0) {
                    $.each(salesPersonOrderDetails, function () {
                        $(".SalesPerson", row).text($(this).find("SalesPerson").text());
                        $(".TotalOrder", row).text($(this).find("TotalOrder").text());
                        TotalOrder += parseInt($(this).find("TotalOrder").text());
                        $(".TotalSales", row).text($(this).find("TotalSales").text());
                        TotalSales += parseFloat($(this).find("TotalSales").text());
                        $(".AOV", row).text($(this).find("AOV").text());
                        AOV += parseFloat($(this).find("AOV").text());
                        $("#tableSalesRevenue tbody").append(row);
                        row = $("#tableSalesRevenue tbody tr:last").clone(true);
                    });

                    $("#tableSalesRevenue tbody tr").show();
                }
                $("#tableSalesRevenue tfoot tr").find('td:eq(1)').html(TotalOrder);
                $("#tableSalesRevenue tfoot tr").find('td:eq(2)').html(TotalSales.toFixed(2));
                var totalAOV = parseFloat(TotalSales / TotalOrder) || 0;
                $("#tableSalesRevenue tfoot tr").find('td:eq(3)').html(totalAOV.toFixed(2));
            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function CollectionDetails() {
    $.ajax({
        type: "POST",
        url: "/admin/WebAPI/WDashBoardMaster.asmx/CollectionDetails",
        data: "{'Type':'" + $("#ddlCollection").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "false") {
                var xmldoc = $.parseXML(response.d);
                var CollectionDetails = $(xmldoc).find("Table");
                $("#tableCollectionDetails tbody tr").remove();
                var row = $("#tableCollectionDetails thead tr:last-child").clone(true);
                if (CollectionDetails.length > 0) {
                    $.each(CollectionDetails, function () {
                        $(".StatusType", row).text($(this).find("StatusType").text());
                        $(".totalCount", row).text($(this).find("totalCount").text());
                        $(".ReceivedAmount", row).text($(this).find("ReceivedAmount").text());
                        $(".ReceivedAmount", row).css('text-align', 'right');
                        $("#tableCollectionDetails tbody").append(row);
                        row = $("#tableCollectionDetails tbody tr:last").clone(true);
                    });
                    $("#tableCollectionDetails tbody tr").show();
                }
            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function CreditMemoDetails() {
    $.ajax({
        type: "POST",
        url: "/admin/WebAPI/WDashBoardMaster.asmx/CreditMemoDetails",
        data: "{'Type':'" + $("#ddlCreditMemo").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "false") {
                var xmldoc = $.parseXML(response.d);
                var CreditMaster = $(xmldoc).find("Table");
                $("#tableCreditMaster tbody tr").remove();
                var row = $("#tableCreditMaster thead tr:last-child").clone(true);
                if (CreditMaster.length > 0) {
                    $.each(CreditMaster, function () {
                        $(".StatusType", row).text($(this).find("StatusType").text());
                        $(".totalCount", row).text($(this).find("totalCount").text());
                        $(".ReceivedAmount", row).text($(this).find("ReceivedAmount").text());
                        $(".ReceivedAmount", row).css('text-align', 'right');
                        $("#tableCreditMaster tbody").append(row);
                        row = $("#tableCreditMaster tbody tr:last").clone(true);
                    });
                    $("#tableCreditMaster tbody tr").show();
                }
            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function getHoldPaymentList() {
    $.ajax({
        type: "POST",
        url: "/admin/WebAPI/WDashBoardMaster.asmx/getHoldPaymentList",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "false") {
                var xmldoc = $.parseXML(response.d);
                var HoldPaymentList = $(xmldoc).find("Table");
                var Emp = $(xmldoc).find("Table1");

                EmpType = $(Emp).find("EmpType").text();

                $("#tblPaymentLog tbody tr").remove();
                var row = $("#tblPaymentLog thead tr:last-child").clone(true);

                if (HoldPaymentList.length > 0) {
                    $("#ShowPODraftLog").modal('show');
                    $("#EmptyTable33").hide();
                    $.each(HoldPaymentList, function () {
                        $(".PaymentId", row).text($(this).find("PaymentId").text());
                        $(".CustomerName", row).text($(this).find("CustomerName").text());
                        $(".ReceivedDate", row).text($(this).find("ReceiveDate").text());
                        $(".ReceivedBy", row).text($(this).find("ReceivedBy").text());
                        $(".HoldBy", row).text($(this).find("HoldBy").text());
                        $(".DepositeDate", row).text($(this).find("DepositeDate").text());
                        $(".CheckNo", row).text($(this).find("ChequeNo").text());
                        $(".Amount", row).text($(this).find("ReceivedAmount").text());

                        $("#tblPaymentLog tbody").append(row);
                        row = $("#tblPaymentLog tbody tr:last").clone(true);
                    });
                    $("#tblPaymentLog tbody tr").show();
                } else {
                    $("#EmptyTable33").show();
                }


            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function OpenDepositList_Popup() {
    getHoldPaymentList();
}
function getTicketList() {
    $.ajax({
        type: "POST",
        url: "/admin/WebAPI/WDashBoardMaster.asmx/getTicketList",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "false") {
                var xmldoc = $.parseXML(response.d);
                var HoldPaymentList = $(xmldoc).find("Table");

                $("#tblTicketList tbody tr").remove();
                var row = $("#tblTicketList thead tr:last-child").clone(true);

                if (HoldPaymentList.length > 0) {
                    $("#ShowTicketList").modal('show');
                    $("#EmptyTable2").hide();
                    $.each(HoldPaymentList, function () {
                        $(".TicketId", row).html("<a href='/admin/ErrorTicketResponseForm.aspx?TicketAutoId=" + $(this).find("TicketID").text() + "'>" + $(this).find("TicketID").text() + "</a>");
                        $(".TicketDate", row).html($(this).find("TicketDate").text());
                        $(".Fullname", row).html($(this).find("Fullname").text());
                        $(".Priority", row).html($(this).find("Priority").text());
                        $(".Type", row).html($(this).find("Type").text());
                        if ($(this).find("Status").text() == "Open") {
                            $(".ClentStatus", row).html("<span class='badge badge badge-pill badge-success mr-2'>" + $(this).find("Status").text() + "</span>")
                        }
                        else {
                            $(".ClentStatus", row).html("<span class='badge badge badge-pill badge-danger mr-2'>" + $(this).find("Status").text() + "</span>")
                        }
                        if ($(this).find("DeveloperStatus").text() == "Open") {
                            $(".DeveloperStatus", row).html("<span class='badge badge badge-pill badge-success mr-2'>" + $(this).find("DeveloperStatus").text() + "</span>");
                        }
                        else if ($(this).find("DeveloperStatus").text() == "Under Process") {
                            $(".DeveloperStatus", row).html("<span class='badge badge badge-pill badge-primary mr-2'>" + $(this).find("DeveloperStatus").text() + "</span>");
                        }
                        else {
                            $(".DeveloperStatus", row).html("<span class='badge badge badge-pill badge-danger mr-2'>" + $(this).find("DeveloperStatus").text() + "</span>");
                        }

                        $(".TicketCloseDate", row).html($(this).find("TicketCloseDate").text());
                        $(".Subject", row).html($(this).find("Subject").text());
                        $(".Description", row).html($(this).find("Description").text());
                        $("#tblTicketList tbody").append(row);
                        row = $("#tblTicketList tbody tr:last").clone(true);
                    });
                    $("#tblTicketList tbody tr").show();
                } else {
                    getHoldPaymentList();
                }


            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function getWebOrderList() {
    $.ajax({
        type: "POST",
        url: "/admin/WebAPI/WDashBoardMaster.asmx/getWebOrderList",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var orderList = $(xmldoc).find("Table");

                $("#tblWebOrderList tbody tr").remove();
                var row = $("#tblWebOrderList thead tr").clone(true);

                if (orderList.length > 0) {
                    $("#ShowWebOrderList").modal('show');
                    $.each(orderList, function () {
                        if ($(this).find("ShipId").text() == '2' || $(this).find("ShipId").text() == '7' || $(this).find("ShipId").text() == '4') {
                            $(row).css('background-color', '#ffe6e6');
                        }
                        else if ($(this).find("ShipId").text() == '8') {
                            $(row).css('background-color', '#eb99ff');
                        }
                        else if ($(this).find("ShipId").text() == '9') {
                            $(row).css('background-color', '#e6fff2');
                        }
                        else if ($(this).find("ShipId").text() == '10') {
                            $(row).css('background-color', '#ccff99');
                        }
                        else {
                            $(row).css('background-color', 'white');
                        }
                        $(".orderNo", row).html("<span OrderAutoId='" + $(this).find("AutoId").text() + "'>" + $(this).find("OrderNo").text() + "</span>");
                        $(".orderDt", row).text($(this).find("OrderDate").text());
                        $(".cust", row).text($(this).find("CustomerName").text());
                        $(".value", row).text($(this).find("GrandTotal").text());
                        $(".SalesPerson", row).text($(this).find("SalesPerson").text());
                        if (Number($(this).find("StatusCode").text()) == 0) {
                            $(".status", row).html("<span class='badge badge badge-pill Status_Close'>" + $(this).find("Status").text() + "</span>");
                        }
                        $("#tblWebOrderList tbody").append(row);
                        row = $("#tblWebOrderList tbody tr:last").clone(true);
                    });
                }
                else {
                    bindCustomer();
                }
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
} 

function bindCustomer() {
    $.ajax({
        type: "POST",
        url: "/admin/mydashboard.aspx/bindCustomer",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var Customer = $(xmldoc).find("Table");
                $("#tblCustomerlist tbody tr").remove();
                var row = $("#tblCustomerlist thead tr").clone(true);
                if (Customer.length > 0) {

                    $("#CustomerlistModal").modal("show");
                    $.each(Customer, function () {

                        $(".CustomerName", row).text($(this).find("CustomerName").text());
                        $(".CustomerType", row).text($(this).find("CustomerType").text());
                        $(".Date", row).html($(this).find("CreatedDate").text());
                        $("#tblCustomerlist tbody").append(row);
                        row = $("#tblCustomerlist tbody tr:last").clone(true);
                    });

                }

            } else {
                location.href = '/';
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function InvalidPacking() {

    $.ajax({
        type: "POST",
        url: "/admin/mydashboard.aspx/InvalidPacking",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var PackingList = $(xmldoc).find("Table");
                $("#tblInvalidPackingList tbody tr").remove();
                var row = $("#tblInvalidPackingList thead tr").clone(true);
                if (PackingList.length > 0) {
                    $("#mdlInvalidPackingDetails").modal("show");
                    $.each(PackingList, function () {
                        $(".ProductId", row).text($(this).find("ProductId").text());
                        $(".ProductName", row).text($(this).find("ProductName").text());
                        $(".ImageUrl", row).html("<img src='" + $(this).find('ImageUrl').text() + "' OnError='this.src =\"http://psmnj.a1whm.com/Attachments/default_pic.png\"' class='zoom img img-circle img-responsive' style='width: 50px;height: 50px;'  >");
                        $(".CreateBy", row).text($(this).find("CreateBy").text());
                        $(".CreateDate", row).text($(this).find("CreateDate").text());
                        $(".LastUpdateDate", row).html($(this).find("LastUpdateDate").text());
                        $("#tblInvalidPackingList tbody").append(row);
                        row = $("#tblInvalidPackingList tbody tr:last").clone(true);
                    });
                }
            } else {
                location.href = '/';
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}


