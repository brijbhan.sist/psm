﻿var getid = 0;
$(document).ready(function () {
    $("#MLQtyUpdateBtn").hide();
    $("#MLQtyUpdateBtn1").hide();
    BindDropdowns();
    $("#btnSearch").click(function () {
        if ($("#ddlSCategory").val() == "0" && $("#ddlSSubcategory").val() == "0" && $("#txtSProductId").val().trim() == "" && $("#txtSProductName").val().trim() == "") {
            toastr.error('Please select category first .', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        }
        else {
            BindProducts();
        }


    });
});
function BindDropdowns() {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/PriceTemplate.asmx/BindDropdowns",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },

        async: false,
        success: function (response) {
            if (response.d != 'Session Expired') {
                var xmldoc = $.parseXML(response.d);
                var category = $(xmldoc).find("Table1");

                $("#ddlSCategory option:not(:first)").remove();
                $.each(category, function () {
                    $("#ddlSCategory").append("<option value=" + $(this).find("AutoId").text() + ">" + $(this).find("CategoryName").text() + "</option>");
                });

                $("#ddlSCategory").select2();

            } else {
                location.href = '/';
            }
        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function BindSubCategory() {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/PriceTemplate.asmx/BindSubcategory",
        data: "{'CategoryAutoId':'" + parseInt($("#ddlSCategory").val()) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            if (response.d != 'Session Expired') {
                var xmldoc = $.parseXML(response.d);
                var subCat = $(xmldoc).find("Table");

                $("#ddlSSubcategory option:not(:first)").remove();
                $.each(subCat, function () {
                    $("#ddlSSubcategory").append("<option value=" + $(this).find("AutoId").text() + ">" + $(this).find("SubcategoryName").text() + "</option>");
                });
                $("#ddlSSubcategory").select2();
            } else {
                location.href = '/';
            }
        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function BindProducts() {
    var data = {
        CategoryAutoId: parseInt($("#ddlSCategory").val()),
        SubcategoryAutoId: parseInt($("#ddlSSubcategory").val()),
        ProductName: $("#txtSProductName").val().trim(),
        Productid: $("#txtSProductId").val().trim()
    };

    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/ManageMLQuantity.asmx/getProductList",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            if (response.d != 'Session Expired') {
                var xmldoc = $.parseXML(response.d);
                var productList = $(xmldoc).find("Table");
                $("#ManageProductList tbody tr").remove();
                if (productList.length > 0) {
                    $("#EmptyTable").hide();
                    var html = "";
                    var i = 0;
                    $.each(productList, function (index) {
                        html += "<tr>"
                        html += "<td class='text-center' name='" + $(this).find("AutoId").text() + "'>" + $(this).find("ProductId").text() + "</td>"
                        html += "<td class='text-left'>" + $(this).find("ProductName").text() + "</td>"
                        ;
                        var abs = $(this).find("IsApply_ML").text();
                        if (abs == 'false') {
                            html += "<td class='text-center'><input type='Checkbox' class='form-control' id='RowIsApplyMl_" + i + "' value=" + abs + " /></td>"
                        }
                        else {
                            html += "<td class='text-center'><input type='Checkbox' class='form-control' id='RowIsApplyML_" + i + "' value=" + abs + " checked/></td>"
                        }
                        html += "<td class='text-center'><input type='text' class='form-control input-sm border-primary text-center' maxlength='8' onchange='ImproveDecimal(this)' id='TxtML_" + i + "'  value=" + $(this).find("MLQty").text() + " placeholder='Enter ML Quantity' onkeypress='return isNumberDecimalKey(event,this)'/></td>"
                        var wts = $(this).find("IsApply_Oz").text();
                        if (wts == 'false') {
                            html += "<td class='text-center'><input type='Checkbox' class='form-control' id='RowIsApplyOz_" + i + "' value=" + wts + " /></td>"
                        }
                        else {
                            html += "<td class='text-center'><input type='Checkbox' class='form-control' id='RowIsApplyOz_" + i + "' value=" + wts + " checked/></td>"
                        }
                        html += "<td class='text-center'><input type='text' class='form-control input-sm border-primary text-center' maxlength='8'  onchange='ImproveDecimal(this)' id='TxtOz_" + i + "' value=" + $(this).find("WeightOz").text() + " placeholder='Enter Weight (Oz)' onkeypress='return isNumberDecimalKey(event,this)'/></td>"

                        html += "</tr>"
                        i++;
                    });
                    $("#ManageProductList tbody").append(html);
                    $("#MLQtyUpdateBtn").show();
                    $("#MLQtyUpdateBtn1").show();

                } else {
                    $("#EmptyTable").show();
                }
            } else {
                location.href = '/';
            }
        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}



function UpdateMlQuantity() {
    Xml = "";
    $("#ManageProductList tbody tr").each(function () {
        var row = $(this);
        Xml += '<Xml>';
        Xml += '<AutoId><![CDATA[' + row.find("td").eq(0).attr('name') + ']]></AutoId>';
        Xml += '<IsApply_ML><![CDATA[' + (row.find("td").eq(2).find("input").prop('checked') == true ? 1 : 0) + ']]></IsApply_ML>';
        Xml += '<MLQty><![CDATA[' + (row.find("td").eq(3).find("input").val() == '' ? 0 : row.find("td").eq(3).find("input").val()) + ']]></MLQty>';
        Xml += '<WeightOz><![CDATA[' + (row.find("td").eq(5).find("input").val() == '' ? 0 : row.find("td").eq(5).find("input").val()) + ']]></WeightOz>';
        Xml += '<IsApply_Oz><![CDATA[' + (row.find("td").eq(4).find("input").prop('checked') == true ? 1 : 0) + ']]></IsApply_Oz>';
        Xml += '</Xml>';

    });
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/ManageMLQuantity.asmx/UpdateML_And_Weight",
        data: JSON.stringify({ dataValue: JSON.stringify(Xml) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) { 
           if (response.d == 'Success') {

               swal("", "Data updated successfully.", "success")
            }
            else if (response.d == 'Session Expired') {
                window.location.href = "/";
            }
            else {
                swal("Error!", "Oops! Something went wrong.Please try later", "error");
            }
        },
    });
}

