﻿$(document).ready(function () {
    getTermsDetail();
    resetTerms();
    $("#btnUpdate").hide();
    $("#btnCancel").hide();
})
/*------------------------------------------------------Insert Categroy------------------------------------------------------------*/
$("#btnSave").click(function () {
    if (checkRequiredField()) {
        var data = {
            TermsName: $("#txtTermsName").val(),
            Description: $("#txtDescription").val(),
            Status: $("#ddlStatus").val()
        }
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/WTermsMaster.asmx/insertTerms",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) { 
                if(result.d=="Success")
                {
                    swal("", "Terms details saved successfully.", "success");
                    var table = $('#tblTerms').DataTable();
                    table.destroy();
                    getTermsDetail();                
                    resetTerms();
                }
                else if(result.d=="Unauthorized access.")
                {
                    location.href="/";
                }
                else
                {
                    swal("Error!", result.d, "error");
                }
            },
            error: function (result) {
                swal("Error!", "Terms already exists.", "error");
               
            },
            failure: function (result) {
                swal("Error!", result.d, "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
});

$("#btnReset").click(function () {
    resetTerms();
});
/*----------------------------------------------------List Categroy Details--------------------------------------------------------*/
function getTermsDetail()
{
    var data = {
        TermsId: $('#txtSTermsId').val(),
        TermsName: $('#txtSTermsName').val(),
        Status: $('#ddlSStatus').val()
    };
    $.ajax({
        type: "POST",
        async:false,
        url: "/Admin/WebAPI/WTermsMaster.asmx/getTermsDetail",
        data: "{'dataValue':'"+ JSON.stringify(data) +"'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
            if ($.fn.DataTable.isDataTable('#tblTerms')) {
                $('#tblTerms').DataTable().destroy();
            }
        },
        complete: function () {
            $.unblockUI();
            $('#tblTerms').DataTable(
              {
                  "paging": fa,
                  "ordering": false,
                  "info": false,
                  "sorting":false,
                  "aoColumns": [
                      { "bSortable": false },
                      { "bSortable": false },
                      { "bSortable": false },
                      { "bSortable": false },
                      { "bSortable": false } 
                  ]
              });
            $("#tblTerms thead td:first").removeClass('sorting_asc ');
        },
        success: onSuccessOfTerms,
        error: function (response) {
            console.log(JSON.parse(response.responseText).d);
        }
    });
}
function onSuccessOfTerms(response) {
    var xmldoc = $.parseXML(response.d);   
    var Terms = $(xmldoc).find('Table'); 
    $('#EmptyTable').hide();
    if (Terms.length > 0)
    {   
        $('#tblTerms tbody tr').remove();
        var row = $('#tblTerms thead tr').clone(true);
        $.each(Terms, function () {
            $(".TermsId", row).text($(this).find("TermsId").text());
            $(".TermsName", row).text($(this).find("TermsDesc").text());
            $(".description", row).text($(this).find("Description").text());
          
            if ($(this).find("Status").text() == 'Active') {
                $(".status", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("Status").text() + "</span>");
            } else {
                $(".status", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("Status").text() + "</span>");

            }
            $(".action", row).html("<a title='Edit' href='#'><span class='ft-edit' onclick='editTerms(\"" + $(this).find("TermsId").text() + "\")' /></a>&nbsp;&nbsp;&nbsp;&nbsp;<a title='Delete' href='#'><span class='ft-x' onclick= 'deleteTerm(\"" + $(this).find("TermsId").text() + "\")' /></a>");
            $("#tblTerms tbody").append(row);
            row = $("#tblTerms tbody tr:last-child").clone(true);
        });        
    }
    else {
        $('#EmptyTable').show();
        $('#tblTerms tbody tr').remove();
          
    }
}
/*----------------------------------------------------Search Engine----------------------------------------------------------*/
$("#btnSearch").click(function () {    
    getTermsDetail();
});
/*----------------------------------------------------Edit Terms Detail---------------------------------------------------*/
function editTerms(TermsId) {   
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WTermsMaster.asmx/editTerms",
        data: "{'TermsId':'" + TermsId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: onSuccessOfEdit,
        error: function (response) {
            console.log(response.d)
        }
    })
}
function onSuccessOfEdit(response) {
    var xmldoc = $.parseXML(response.d);
    var Terms = $(xmldoc).find('Table');
    $("#txtTermsId").val($(Terms).find("TermsId").text());
    $("#txtTermsName").val($(Terms).find("TermsDesc").text());
    $("#txtDescription").val($(Terms).find("Description").text());
    $("#ddlStatus").val($(Terms).find("Status").text());
    $("#btnSave").hide();
    $("#btnReset").hide();
    $("#btnUpdate").show();
    $("#btnCancel").show();
}
$("#btnCancel").click(function () {
    resetTerms();
    $("#btnUpdate").hide();
    $("#btnCancel").hide();
    $("#btnSave").show();
    $("#btnReset").show();

});

/*----------------------------------------------------Update Terms Detail--------------------------------------------------------*/

$("#btnUpdate").click(function () {
    if (checkRequiredField()) {
        var data = {
            TermsId: $("#txtTermsId").val(),
            TermsName: $("#txtTermsName").val(),
            Description: $("#txtDescription").val(),
            Status: $("#ddlStatus").val()
        };
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/WTermsMaster.asmx/updateTerms",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json;charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if(result.d=="Success")
                {
                    swal("", "Terms details updated successfully.", "success");
                    var table = $('#tblTerms').DataTable();
                    table.destroy();
                    getTermsDetail();
                    resetTerms();
                }
                else if(result.d=="Unauthorized access.")
                {
                    location.href="/";
                }
                else
                {
                    swal("Error!", result.d, "error");        
                }
            },
            error: function (result) {
                swal("Error!", "Terms already exists.", "error");               
            },
            failure: function (result) {
                swal("Error!", result.d, "error");                
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
});

/*--------------------------------------------------------Delete Terms ----------------------------------------------------------*/
function deleteTerms(TermsId)
{
    $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/WTermsMaster.asmx/deleteTerms",
            data: "{'TermsId':'" + TermsId + "'}",
            contentType: "application/json;charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
                if ($.fn.DataTable.isDataTable('#tblTerms')) {
                    $('#tblTerms').DataTable().destroy();
                }
            },
            complete: function () {
                $.unblockUI();
            },                       
            success: function (result) {  
                if(result.d=="Success")
                {
                    swal("", "Terms deleted successfully.", "success");
                    var table = $('#tblTerms').DataTable();
                    table.destroy();
                    getTermsDetail();
                    resetTerms();
                }
                else if(result.d=="Unauthorized access.")
                {
                    location.href="/";
                }
                else
                {
                    swal("error", result.d, "error");     
                }
            },
            error: function (result) {                              
                swal("error", "Alert ! Terms already assigned.", "error");             
            },
            failure: function (result) {
                swal("error", "Alert ! Terms already assigned.", "error");
            }
        })
    }

/*-----------------------------------------------------------Reset------------------------------------------------------------------*/
function resetTerms() {
    $("#btnUpdate").hide();
    $("#btnCancel").hide();
    $("#btnSave").show();
    $("#btnReset").show();
    $('input[type="text"]').val('');
    $('textarea').val('');
    $('#ddlStatus').val('1');
    $("#txtTermsName").removeClass('border-warning');
}

function deleteTerm(TermsId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this terms.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, cancel.",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: false,
            },
            confirm: {
                text: "Yes, delete it.",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(isConfirm => {
        if (isConfirm) {
             deleteTerms(TermsId);
            
} else {
             swal("", "Your Terms is safe.", "error");
}
})
}