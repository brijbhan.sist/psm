﻿var OrdAutoId = 0;


$(document).ready(function () {
    bindDropdown();
    getOrderList(1);
    $('#txtSFromDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: false,
        selectMonths: false,
    });
    $('#txtSToDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: false,
        selectMonths: false,
    });
})
function Pagevalue(e) {
    getOrderList(parseInt($(e).attr("page")));
};

function bindDropdown() {
    $.ajax({
        type: "POST",
        url: "AppOrderList.aspx/bindDropdown",
        data: "{}",
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {

            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var customer = $(xmldoc).find("Table");
                var SalesPerson = $(xmldoc).find("Table2");
                var ShippingType = $(xmldoc).find("Table4");

                $("#ddlCustomer option:not(:first)").remove();
                $.each(customer, function () {
                    $("#ddlCustomer").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("Customer").text() + "</option>");
                });
                $("#ddlCustomer").select2();
                $("#ddlSalesPerson option:not(:first)").remove();
                $.each(SalesPerson, function () {
                    $("#ddlSalesPerson").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("EmpName").text() + "</option>");
                });
                $("#ddlSalesPerson").attr('multiple', 'multiple')
                $("#ddlSalesPerson").select2({
                    placeholder: 'All Shipping Type',
                    allowClear: true
                });

                $("#ddlShippingType option:not(:first)").remove();
                $.each(ShippingType, function () {
                    $("#ddlShippingType").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("ShippingType").text() + "</option>");
                });
                $("#ddlShippingType").attr('multiple', 'multiple')
                $("#ddlShippingType").select2({
                    placeholder: 'All Shipping Type',
                    allowClear: true
                });

            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function PageSize() {
    $("#divSlectedOrder").hide();
    $("#bCountSelectedOrder").html('');
    getOrderList(1);
}


function getOrderList(pageIndex) {
    var ShippingType = '0', SalesPerson = '0';

    $("#ddlShippingType option:selected").each(function (i) {
        if (i == 0) {
            ShippingType = $(this).val() + ',';
        } else {
            ShippingType += $(this).val() + ',';
        }
    });
    $("#ddlSalesPerson option:selected").each(function (i) {
        if (i == 0) {
            SalesPerson = $(this).val() + ',';
        } else {
            SalesPerson += $(this).val() + ',';
        }
    });
    var data = {
        OrderNo: $("#txtSOrderNo").val().trim(),
        CustomerAutoid: $("#ddlCustomer").val(),
        SalesPerson: SalesPerson.toString(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        pageIndex: pageIndex,
        PageSize: $("#ddlPageSize").val(),
        ShippingType: ShippingType.toString()
    };
    $.ajax({
        type: "POST",
        url: "AppOrderList.aspx/getOrderList",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            console.log("order list success ");
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var orderList = $(xmldoc).find("Table1");
                if (orderList.length > 0) {
                    $("#EmptyTable").hide();
                    $("#tblOrderList tbody tr").remove();
                    var row = $("#tblOrderList thead tr").clone(true);
                    $.each(orderList, function () {
                        if ($(this).find("ShipId").text() == '2' || $(this).find("ShipId").text() == '7' || $(this).find("ShipId").text() == '4') {
                            $(row).css('background-color', '#ffe6e6');
                        }
                        else if ($(this).find("ShipId").text() == '8') {
                            $(row).css('background-color', '#eb99ff');
                        }
                        else if ($(this).find("ShipId").text() == '9') {
                            $(row).css('background-color', '#e6fff2');
                        }
                        else if ($(this).find("ShipId").text() == '10') {
                            $(row).css('background-color', '#ccff99');
                        }
                        else {
                            $(row).css('background-color', 'white');
                        }
                        $(".Shipping", row).text($(this).find("ShippingType").text());
                        $(".orderNo", row).html("<span OrderAutoId='" + $(this).find("AutoId").text() + "'>" + $(this).find("OrderNo").text() + "</span>");
                        $(".orderDt", row).text($(this).find("OrderDate").text());
                        $(".cust", row).html('<span customerAutoId=' + $(this).find("CustomerAutoId").text() + '>' + $(this).find("CustomerName").text() + '</span>');
                        $(".value", row).text($(this).find("GrandTotal").text());
                        $(".product", row).text($(this).find("NoOfItems").text());
                        $(".SalesPerson", row).text($(this).find("SalesPerson").text());
                        $(".CreditMemo", row).text($(this).find("CreditMemo").text());
                        $(".OrderAutoId", row).text($(this).find("AutoId").text());
                        
                            $(".action", row).html("<a href='/Manager/Manager_viewOrder.aspx?OrderAutoId=" + $(this).find("AutoId").text() + "'><span class='la la-eye' title='View detail'></span></a>");
                       

                        $("#tblOrderList tbody").append(row);
                        row = $("#tblOrderList tbody tr:last").clone(true);
                    });
                } else {
                    $("#EmptyTable").show();
                    $("#tblOrderList tbody tr").remove();
                }

                var pager = $(xmldoc).find("Table");
                $(".Pager").ASPSnippets_Pager({
                    ActiveCssClass: "current",
                    PagerCssClass: "pager",
                    PageIndex: parseInt(pager.find("PageIndex").text()),
                    PageSize: parseInt(pager.find("PageSize").text()),
                    RecordCount: parseInt(pager.find("RecordCount").text())
                });
            } else {
                location.href = "/";
            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function Search() {
    $("#divSlectedOrder").hide();
    $("#bCountSelectedOrder").html('');//bCountSelectedCustomers
    $("#bCountSelectedCustomers").html('');
    getOrderList(1);
}