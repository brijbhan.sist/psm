﻿$(document).ready(function () {
    resetDevice();
    GetDeviceDetail(1);
    $("#btnUpdate").hide();
    $("#btnCancel").hide();

})
/*------------------------------------------------------Insert Currency------------------------------------------------------------*/
function Save() {
  
    if ($("#isLocationRequired").is(':checked') == true) {
        var isLocationRequired = 1;
    } else {
        var isLocationRequired = 0;
    }
    if (checkRequiredField()) {
        var data = {
            DeviceId: $("#txtDeviceId").val(),
            DeviceName: $("#txtDeviceName").val(),
            Status: $("#ddlStatus").val(),
            IsLocationRequired: isLocationRequired
        }
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/WDeviceMaster.asmx/insertDevice",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == "Success") {
                    swal("", "Device details saved successfully.", "success");
                    var table = $('#tblDevice').DataTable();
                    table.destroy();
                    resetDevice();
                    GetDeviceDetail(1);
                }
                else if (result.d == "Unauthorized access.") {
                    location.href = "/";
                }
                else {
                    swal("Error!", result.d, "error");
                }
            },
            error: function (result) {
                swal("Error!", result.d, "error");
            },
            failure: function (result) {
                swal("Error!", result.d, "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
};

function Pagevalue(e) {
    GetDeviceDetail(parseInt($(e).attr("page")));
};
/*----------------------------------------------------List Categroy Details--------------------------------------------------------*/
function GetDeviceDetail(PageIndex) {
    debugger;
    var data = {
        DeviceId: $("#txtSDeviceId").val().trim(),
        DeviceName: $("#txtSDeviceName").val().trim(),
        Status: $("#ddlSStatus").val(),
        PageSize: $("#ddlPageSize").val(), 
        PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WDeviceMaster.asmx/getDeviceDetail",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: onSuccessOfCurrency,
        error: function (response) {
            console.log(JSON.parse(response.responseText).d);
        }
    });
}
function onSuccessOfCurrency(response) {
    var xmldoc = $.parseXML(response.d);
    var DeviceDetails = $(xmldoc).find('Table1');
    if (DeviceDetails.length > 0) {
        $('#EmptyTable').hide();
        $('#tblDevice tbody tr').remove();
        var row = $('#tblDevice thead tr').clone(true);
        $.each(DeviceDetails, function () {
            $(".DeviceId", row).text($(this).find("DeviceId").text());
            $(".DeviceName", row).text($(this).find("DeviceName").text());
            $(".AttachedEmp", row).text($(this).find("AttachedEmp").text());
            if ($(this).find("Status").text() == 'Active') {
                $(".status", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("Status").text() + "</span>");
            } else {
                $(".status", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("Status").text() + "</span>");
            }
            if ($(this).find("IsLocationRequired").text() == 1) {
                $(".IsLocationRequire", row).html("<span class='badge badge badge-pill badge-success'>Yes</span>");
            } else {
                $(".IsLocationRequire", row).html("<span class='badge badge badge-pill badge-danger'>No</span>");
            }
            $(".action", row).html("<a title='Edit' href='#'><span class='ft-edit' onclick='editDevice(\"" + $(this).find("AutoId").text() + "\")' />&nbsp;&nbsp;</a><a title='Delete' href='#'><span class='ft-x' onclick= 'deleterecord(\"" + $(this).find("AutoId").text() + "\")' /></a>");
            $("#tblDevice tbody").append(row);
            row = $("#tblDevice tbody tr:last-child").clone(true);
        });
    }
    else {
        $('#tblDevice tbody tr').remove();
        $('#EmptyTable').show();
    }
    var pager = $(xmldoc).find("Table");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
}

/*----------------------------------------------------Edit CurrencyDetails Detail---------------------------------------------------*/
function editDevice(DeviceAutoId) {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WDeviceMaster.asmx/editDevice",
        data: "{'DeviceAutoId':'" + DeviceAutoId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: onSuccessOfEdit,
        error: function (response) {
            console.log(response.d)
        }
    })
}
function onSuccessOfEdit(response) {
    var xmldoc = $.parseXML(response.d);
    var DeviceDetails = $(xmldoc).find('Table');
    $("#txtDeviceId").val($(DeviceDetails).find("DeviceId").text());
    $("#txtDeviceName").val($(DeviceDetails).find("DeviceName").text());
    $("#ddlStatus").val($(DeviceDetails).find("Status").text());
    $("#hdnDeviceAutoId").val($(DeviceDetails).find("AutoId").text());

    if ($(DeviceDetails).find("IsLocationRequired").text() == 1) {
        $("#isLocationRequired").prop('checked', true);
    } else {
        $("#isLocationRequired").prop('checked', false);

    }
    $("#btnSave").hide();
    $("#btnReset").hide();
    $("#btnUpdate").show();
    $("#btnCancel").show();
}
/*----------------------------------------------------Update Currency Detail--------------------------------------------------------*/
function Update() {
    if ($("#isLocationRequired").is(':checked') == true) {
        var isLocationRequired = 1;
    } else {
        var isLocationRequired = 0;
    }
    if (checkRequiredField()) {
        var data = {
            DeviceId: $("#txtDeviceId").val(),
            DeviceName: $("#txtDeviceName").val(),
            Status: $("#ddlStatus").val(),
            DeviceAutoId: $("#hdnDeviceAutoId").val(),
            IsLocationRequired: isLocationRequired
        }
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/WDeviceMaster.asmx/updateDevice",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == "Success") {
                    swal("", "Device details updated successfully.", "success");
                    var table = $('#tblDevice').DataTable();
                    table.destroy();
                    resetDevice();
                    GetDeviceDetail(1);
                }
                else if (result.d == "Unauthorized access.") {
                    location.href = "/";
                }
                else {
                    swal("Error!", result.d, "error");
                }
            },
            error: function (result) {
                swal("Error!", result.d, "error");
            },
            failure: function (result) {
                swal("Error!", result.d, "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
};
function resetDevice() {
    $("#btnUpdate").hide();
    $("#btnCancel").hide();
    $("#btnSave").show();
    $("#btnReset").show();
    $("#txtDeviceName").removeClass('border-warning');
    $('input[type="text"]').val('');
    $('textarea').val('');
    $('option:selected').each(function () {
        $(this).attr('selected', false);
    });
    $('select option:first-child').each(function () {
        $(this).attr('selected', 'selected');
    });
    $("#ddlStatus").val('1');
    $("#isLocationRequired").prop('checked', false);

}
function Cancel() {
    resetDevice();
    $("#btnUpdate").hide();
    $("#btnCancel").hide();
    $("#btnSave").show();
    $("#btnReset").show();
};
/*--------------------------------------------------------Delete Currency Details ----------------------------------------------------------*/
function deleteDevice(DeviceAutoId) {

    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WDeviceMaster.asmx/deleteDevice",
        data: "{'DeviceAutoId':'" + DeviceAutoId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (result) {
            if (result.d == "Success") {
                swal("", "Device details deleted successfully.", "success");
                var table = $('#tblDevice').DataTable();
                table.destroy();
                GetDeviceDetail(1);
                resetDevice();
            }
            else if (result.d == "Unauthorized access.") {
                location.href = "/";
            }
            else {
                swal("error", result.d, "error");
            }
        },
        error: function (result) {
            swal("error", result.d, "error");

        },
        failure: function (result) {
            swal("error", result.d, "error");
        }
    })
}
function deleterecord(DeviceAutoId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this Device Details.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: false,
            },
            confirm: {
                text: "Yes, delete it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(isConfirm => {
        if (isConfirm) {
            deleteDevice(DeviceAutoId);

        } else {
            swal("", "Your Device Details is safe.", "error");
        }
    })
}