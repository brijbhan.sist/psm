﻿$(document).ready(function () {
    getStatusList(1);
});
function Pagevalue(e) {
    getStatusList(parseInt($(e).attr("page")));
};

/*-------------------------------------------------------Get Status List----------------------------------------------------------*/
function getStatusList(PageIndex) {
    var data = {
        PageSize: 10,
        PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WStatusMaster.asmx/getStatusList",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successGetStatusList,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function successGetStatusList(response) {
    var xmldoc = $.parseXML(response.d);
    var CarList = $(xmldoc).find("Table");

    $("#tblStatusList tbody tr").remove();

    var row = $("#tblStatusList thead tr").clone(true);
    if (CarList.length > 0) {
        $("#EmptyTable").hide();
        $.each(CarList, function (index) {
            $(".StatusType", row).text($(this).find("StatusType").text());
            $(".StatusCategory", row).text($(this).find("Category").text());
            $(".Action", row).html("<a title='Edit' href='#'><span class='ft-edit' ></span></a>");
            $("#tblStatusList tbody").append(row);
            row = $("#tblStatusList tbody tr:last").clone(true);
        });

    } else {
        $("#EmptyTable").show();
    }

    var pager = $(xmldoc).find("Table");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
}

$("#btnSearch").click(function () {
    getStatusList(1);
})
