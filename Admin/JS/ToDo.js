﻿$(document).ready(function () {
    $('#txtFromDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: false,
        selectMonths: false,
    });
    $('#txtToDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: false,
        selectMonths: false,
    });
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    var lastWeek = new Date(d.getFullYear(), d.getMonth(), d.getDate() - 7);
    var oldDate = lastWeek.getMonth() + 1 + "/" + lastWeek.getDate() + "/" + lastWeek.getFullYear();
    $("#txtFromDate").val(oldDate);
    $("#txtToDate").val(month + '/' + day + '/' + year);
    bindStatus();
    getList(1);
});
function Pagevalue(e) {
    getList(parseInt($(e).attr("page")));
};
function bindStatus() {
    $.ajax({
        type: "POST",
        url: "ToDo.aspx/SelectStatus",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != 'Session Expired') {
                var xmldoc = $.parseXML(response.d);
                var Status = $(xmldoc).find("Table");

                $("#ddlStatus option").remove();
                $("#ddlStatus").append("<option value='0'>-Select-</option>");
                $.each(Status, function () {
                    $("#ddlStatus").append($('<option value="' + $(this).find("AutoId").text() + '">' + $(this).find("StatusType").text() + '</option>'));
                });

                $("#ddlSIsCompleted option").remove();
                $("#ddlSIsCompleted").append("<option value='0'>All Status</option>");
                $.each(Status, function () {
                    $("#ddlSIsCompleted").append($('<option value="' + $(this).find("AutoId").text() + '">' + $(this).find("StatusType").text() + '</option>'));
                });

            } else {
                location.href = '/';
            }
        },
        error: function (response) {
            console.log(response);
        },
        failure: function (response) {
            console.log(response);
        }
    });
}
/*------------------------------------------------------------------Insert------------------------------------------------------------ */
function Save() {
    if (checkRequiredField()) {
        $("#btnSave").attr('disabled', true);
        var Starred = 0;
        var Important = 0;
        if ($("#ChkStarred").prop("checked") == true && $("#ChkImportant").prop("checked") == false) {
            Starred = 1;
            Important = 0;
        }
        else if ($("#ChkStarred").prop("checked") == false && $("#ChkImportant").prop("checked") == true) {
            Starred = 0;
            Important = 1;
        }
        else if ($("#ChkStarred").prop("checked") == true && $("#ChkImportant").prop("checked") == true) {
            Starred = 1;
            Important = 1;
        }
        else if ($("#ChkStarred").prop("checked") == false && $("#ChkImportant").prop("checked") == false) {
            Starred = 0;
            Important = 0;
        }
        var data = {
            Title: $("#TxtTitle").val().trim(),
            Description: $("#txtDescription").val().trim(),
            IsCompleted: $("#ddlStatus").val(),
            Starred: Starred,
            Important: Important
        }
        $.ajax({
            type: "POST",
            url: "ToDo.aspx/insert",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                if (response.d != "Session Expired") {
                    if (response.d == 'Success') {
                        swal("", "To Do saved successfully.", "success");
                        getList(1);
                        $("#btnSave").attr('disabled', false);
                        reset();
                    }
                    else {
                        swal("Error!", response.d, "error");
                        $("#btnSave").attr('disabled', false);
                    }
                }
                else {
                    location.href = "/";
                    $("#btnSave").attr('disabled', false);
                }
            },
            error: function (response) {
                $("#btnSave").attr('disabled', false);
            },
            failure: function (response) {
                $("#btnSave").attr('disabled', false);
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $("#btnSave").attr('disabled', false);
    }
}

/*------------------------------------------------------------------Bind List------------------------------------------------------------ */
function getList(pageIndex) {
    var Starred = 0;
    var Important = 0;
    if ($("#ddlSToDoType").val() == 0) {
        Starred = 1;
        Important = 0;
    } else if ($("#ddlSToDoType").val() == 1) {
        Important = 1;
        Starred = 0;
    } else if ($("#ddlSToDoType").val() == 2) {
        Important = 1;
        Starred = 1;
    }
    else {

    }
    var data = {
        FromDate: $("#txtFromDate").val(),
        ToDate: $("#txtToDate").val(),
        Starred: Starred,
        Important: Important,
        PageIndex: pageIndex,
        PageSize: $("#ddlPageSize").val(),
        IsSCompleted: $("#ddlSIsCompleted").val()

    };
    $.ajax({
        type: "POST",
        url: "ToDo.aspx/Select",
        data: JSON.stringify({ 'dataValue': JSON.stringify(data) }),
        contentType: "application/json;charset=utf-8",
        datatype: "json",

        success: onSuccessOfEmpType,
        error: function (response) {
            console.log(JSON.parse(response.responseText).d);
        }
    });
}
function onSuccessOfEmpType(response) {
    if (response.d != "Session Expired") {
        var xmldoc = $.parseXML(response.d);
        var Emptye = $(xmldoc).find('Table1');
        if (Emptye.length > 0) {
            $('#TableToDo tbody tr').remove();
            $('#EmptyTable').hide();
            var row = $('#TableToDo thead tr').clone(true);
            $.each(Emptye, function () {
                $(".Id", row).html($(this).find("AutoId").text());
                $(".Title", row).html($(this).find("Title").text());
                $(".StatusType", row).html($(this).find("StatusType").text());
                $(".Description", row).html($(this).find("Description").text());
                $(".CreateDate", row).html($(this).find("CreatedDate").text());
                var Starred = "";
                var Important = "";
                var Starredtitle = "";
                var Importanttitle = "";
                if ((Number($(this).find("Starred").text()) == 1)) {
                    Starred = 'style="color:#e09a19"';
                    Starredtitle = "Starred";
                }
                if ((Number($(this).find("Important").text()) == 1)) {
                    Important = 'style="color:#e09a19"';
                    Importanttitle = "Important";
                }
                $(".ToDoType", row).html('<i class="la la-star" ' + Starred + ' title="' + Starredtitle + '"></i>&nbsp;<i  title="' + Importanttitle + '" class="la la-info-circle" ' + Important + '></i>');

                if ($(this).find("IsCompleted").text() == '3') {
                    $(".action", row).html("<input type='checkbox' checked name='complete' id='chkComplete' onclick='chkInComplete_OnClick(" + $(this).find("AutoId").text() + ");' >&nbsp;&nbsp;<b><a title='Edit' href='#'><span class='ft-edit' onclick='edit(\"" + $(this).find("AutoId").text() + "\")'/></a>&nbsp;&nbsp;<a title='Delete' href='#'><span class='ft-x' onclick='deletes(\"" + $(this).find("AutoId").text() + "\")'/></a>");
                } else {
                    $(".action", row).html("<input type='checkbox'  name='complete' id='chkComplete' onclick='chkComplete_OnClick(" + $(this).find("AutoId").text() + ");' >&nbsp;&nbsp;<b><a title='Edit' href='#'><span class='ft-edit' onclick='edit(\"" + $(this).find("AutoId").text() + "\")'/></a>&nbsp;&nbsp;<a title='Delete' href='#'><span class='ft-x' onclick='deletes(\"" + $(this).find("AutoId").text() + "\")'/></a>");
                }

                $("#TableToDo tbody").append(row);
                row = $("#TableToDo tbody tr:last-child").clone(true);
            });
        }
        else {
            $('#TableToDo tbody tr').remove();
            $('#EmptyTable').show();

        }
        var pager = $(xmldoc).find("Table");
        $(".Pager").ASPSnippets_Pager({
            ActiveCssClass: "current",
            PagerCssClass: "pager",
            PageIndex: parseInt(pager.find("PageIndex").text()),
            PageSize: parseInt(pager.find("PageSize").text()),
            RecordCount: parseInt(pager.find("RecordCount").text())
        });
    }
    else {
        location.href = "/";
    }
}

/*------------------------------------------------------------------Edit------------------------------------------------------------ */
function edit(AutoId) {
    $.ajax({
        type: "POST",
        url: "ToDo.aspx/edit",
        data: "{'AutoId':'" + AutoId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",

        success: onSuccessOfEdit,
        error: function (response) {
            console.log(response.d)
        }
    })
}
function onSuccessOfEdit(response) {
    if (response.d != "Session Expired") {
        var xmldoc = $.parseXML(response.d);
        var todo = $(xmldoc).find('Table');
        $("#addnote").modal('show');
        $("#lblAutoId").text($(todo).find("AutoId").text());
        $("#ddlStatus").val($(todo).find("IsCompleted").text());
        $("#TxtTitle").val($(todo).find("Title").text());
        $("#txtDescription").val($(todo).find("Description").text());
        if (Number($(todo).find("Starred").text()) == 1) {
            $("#ChkStarred").prop("checked", true)
        } else {
            $("#ChkStarred").prop("checked", false)
        }
        if (Number($(todo).find("Important").text()) == 1) {
            $("#ChkImportant").prop("checked", true)
        } else {
            $("#ChkImportant").prop("checked", false)
        }
        $("#btnSave").hide();
        $("#btnReset").hide();
        $("#btnUpdate").show();
        $("#btnCancel").show();
    }
    else {
        location.href = "/";
    }
}

/*------------------------------------------------------------------Delete------------------------------------------------------------ */
function deletes(AutoId) {
    swal({
        title: "Are you sure?",
        text: "You want to move this To Do into Trash.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, cancel.",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            deletetodo(AutoId);
        }
    })
}
function deletetodo(AutoId) {
    $.ajax({
        type: "POST",
        url: "ToDo.aspx/Delete",
        data: "{'AutoId':'" + AutoId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d == 'Success') {
                    swal("", "To Do  moved into Trash successfully.", "success");
                    getList(1);
                    reset();
                } else {
                    swal("Error!", response.d, "error");
                }
            }
            else {
                location.href = "/";
            }
        }
    })
}

/*------------------------------------------------------------------Update------------------------------------------------------------ */
function Update() {
    if (checkRequiredField()) {
        $("#btnUpdate").attr('disabled', true);
        var Starred = 0;
        if ($("#ChkStarred").prop("checked") == true) {
            Starred = 1
        }
        else {
            Starred = 0;
        }
        var Important = 0;
        if ($("#ChkImportant").prop("checked") == true) {
            Important = 1
        }
        else {
            Important = 0;
        }
        var data = {
            AutoId: $("#lblAutoId").text(),
            Title: $("#TxtTitle").val().trim(),
            IsCompleted: $("#ddlStatus").val(),
            Description: $("#txtDescription").val().trim(),
            Starred: Starred,
            Important: Important
        }
        $.ajax({
            type: "POST",
            url: "ToDo.aspx/update",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d != "Session Expired") {
                    if (response.d == 'Success') {
                        swal("", "To Do  updated successfully.", "success").then(function () {
                            $("#btnUpdate").attr('disabled', false);
                            window.location.href = "/Admin/ToDo.aspx";
                        });
                        reset();
                    } else {
                        swal("Error!", response.d, "error");
                        $("#btnUpdate").attr('disabled', false);
                    }
                }
                else {
                    location.href = "/";
                    $("#btnUpdate").attr('disabled', false);
                }
            },
            error: function (response) {
                $("#btnUpdate").attr('disabled', false);
            },
            failure: function (response) {
                $("#btnUpdate").attr('disabled', false);
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $("#btnUpdate").attr('disabled', false);
    }
}

/*------------------------------------------------------------------Mark Completed------------------------------------------------------------ */
function chkComplete_OnClick(AutoId) {

    var IsCompleted = 3;
    var data = {
        AutoId: AutoId,
        IsCompleted: IsCompleted
    };
    $.ajax({
        type: "POST",
        url: "ToDo.aspx/CompletedTodo",
        data: JSON.stringify({ 'dataValue': JSON.stringify(data) }),
        contentType: "application/json;charset=utf-8",
        datatype: "json",

        success: onSuccessOfIsCompleted,
        error: function (response) {
            console.log(response.d)
        }
    })
}

function onSuccessOfIsCompleted(response) {
    if (response.d != "Session Expired") {
        if (response.d == 'Success') {
            getList(1);
            swal("", "To Do marked as closed.", "success");
            reset();
        } else {
            swal("Error!", response.d, "error");
        }
    }
    else {
        location.href = "/";
    }
}

/*------------------------------------------------------------------Mark InCompleted------------------------------------------------------------ */
function chkInComplete_OnClick(AutoId) {

    var IsCompleted = 1;

    var data = {
        AutoId: AutoId,
        IsCompleted: IsCompleted
    };
    $.ajax({
        type: "POST",
        url: "ToDo.aspx/CompletedTodo",
        data: JSON.stringify({ 'dataValue': JSON.stringify(data) }),
        contentType: "application/json;charset=utf-8",
        datatype: "json",

        success: onSuccessOfIsInCompleted,
        error: function (response) {
            console.log(response.d)
        }
    })
}

function onSuccessOfIsInCompleted(response) {
    if (response.d != "Session Expired") {
        if (response.d == 'Success') {
            getList(1);
            swal("", "To Do marked as open.", "success");

            reset();
        } else {
            swal("Error!", response.d, "error");
        }
    }
    else {
        location.href = "/";
    }
}

/*------------------------------------------------------------------Reset and cancel------------------------------------------------------------ */
function reset() {
    $("#btnUpdate").hide();
    $("#btnCancel").hide();
    $("#btnSave").show();
    $("#btnReset").show();
    $('input[type="text"]').val('');
    $("#lblAutoId").val('');
    $("#lblAutoId").val('');
    $("#txtDescription").val('');
    $("#ddlStatus").val('0');
    $("#ChkImportant").prop("checked", false);
    $("#ChkStarred").prop("checked", false);
    $("#addnote").modal('hide');
}

function Cancel() {
    $('input[type="text"]').val('');
    $("#btnUpdate").hide();
    $("#btnCancel").hide();
    $("#btnSave").show();
    $("#btnReset").show();
    $("#lblAutoId").val('');
    $("#lblAutoId").val('');
    $("#txtDescription").val('');
    $("#ddlStatus").val('0');
    $("#ChkImportant").prop("checked", false);
    $("#ChkStarred").prop("checked", false);
    $("#addnote").modal('hide');
}


function openAddNewDo() {
    $("#addnote").modal('show');
}