﻿$(document).ready(function () {
    if ($("#hiddenForPacker").val() == "") {
        $("#btnBulkUpload").show();
    }
    getCarList(1);
});
function Pagevalue(e) {
    getCarList(parseInt($(e).attr("page")));
};
/*---------------------------------------------------Bind Category-----------------------------------------------------------*/
function bindCategory() {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/CarList.asmx/bindCategory",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var category = $(xmldoc).find("Table");

            $("#ddlSCategory option:not(:first)").remove();
            $.each(category, function () {
                $("#ddlSCategory").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("CategoryName").text() + "</option>");
            });
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

/*-------------------------------------------------------Bind Subcategory-----------------------------------------------------------*/
function SubCategory() {
    var categoryAutoId = $("#ddlSCategory").val();
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/CarList.asmx/bindSubcategory",
        data: "{'categoryAutoId':'" + categoryAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var subcategory = $(xmldoc).find("Table");
            $("#ddlSSubcategory option:not(:first)").remove();
            $.each(subcategory, function () {
                $("#ddlSSubcategory").append("<option value=' " + $(this).find("AutoId").text() + "'>" + $(this).find("SubcategoryName").text() + "</option>");
            });
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
/*-------------------------------------------------------Get Car List----------------------------------------------------------*/
function getCarList(PageIndex) {

    var data = {
        CarName: $("#txtScarName").val().trim(),
        PageSize: 10,
        PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WCarDetails.asmx/getCarList",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successGetCarList,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function successGetCarList(response) {
    var xmldoc = $.parseXML(response.d);
    var CarList = $(xmldoc).find("Table1");

    $("#tblCarList tbody tr").remove();

    var row = $("#tblCarList thead tr").clone(true);
    if (CarList.length > 0) {
        $("#EmptyTable").hide();
        $.each(CarList, function (index) {
            $(".CarId", row).text($(this).find("CarId").text());
            $(".CarName", row).text($(this).find("CarName").text());
            $(".YearName", row).text($(this).find("YearName").text());
            $(".Make", row).text($(this).find("Make").text());
            $(".Model", row).text($(this).find("Model").text());
            $(".VINNo", row).text($(this).find("VINNumber").text());
            $(".TagNo", row).text($(this).find("TAGNumber").text());
            $(".InspectionExpDate", row).text($(this).find("InspectionExpDate").text());
            $(".StartMilage", row).text($(this).find("StartMilage").text());
            $(".STATUS", row).text($(this).find("STATUS").text());
            $(".Action", row).html("<a title='Edit' href='/Admin/CarDetails.aspx?PageId=" + $(this).find("CarAutoId").text() + "'><span class='ft-edit' ></span></a>&nbsp;<a title='Delete' href='#'><span class='ft-x' onclick='deleterecord(\"" + $(this).find("CarAutoId").text() + "\") '></span></a>");
            $("#tblCarList tbody").append(row);
            row = $("#tblCarList tbody tr:last").clone(true);
        });

    } else {
        $("#EmptyTable").show();
    }

    if ($("#hiddenForPacker").val() != "") {
        $("#linkAddNewCar").hide();
        $(".glyphicon-remove").hide();
        $(".action").hide();
    }

    var pager = $(xmldoc).find("Table");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
}

function deleteCar(CarId) {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WCarDetails.asmx/deleteCar",
        data: "{'CarAutoId':'" + CarId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (result) {
            if (result.d == "true") {
                swal("", "Car details deleted successfully.", "success");
                getCarList(1);
            }
            else if (result.d == "Exists") {
                swal("Error!", "This car has been used in application.", "error");

            }
            else if (result.d == "Session Timeout") {
                location.href = "/";
            }
            else {
                swal("Error!", "Oops! Something went wrong.Please try later.", "error");

                getCarList(1);
            }

        },
        error: function (result) {
            swal("Error!", "This car has been used in application.", "error");
        },
        failure: function (result) {
            swal("Error!", result.d, "error");
        }
    });

}
function deleterecord(cariid) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this car",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: false,
            },
            confirm: {
                text: "Yes, delete it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            deleteCar(cariid);

        } else {
            swal("", "Your car detail is safe.", "error");
        }
    })
}

function BulkUpload() {
    var timeStamp = Date.parse(new Date());
    if ($("#fileCarBulk").val() != '') {
        var imgname = timeStamp;;// + "_" + $("#fileCarBulk").get(0).files[0].name;
    }
    else {
        var imgname = "default_pic.png";
    }
    if ($("#fileCarBulk").val() != "") {
        $("#errorMsg").hide().text("");
        $("#succMsg").hide().text("");

        var fileUpload = $("#fileCarBulk").get(0);
        var files = fileUpload.files;
        var test = new FormData();
        var fileExtension, flagExt = true;
        for (var i = 0; i < files.length; i++) {
            fileExtension = files[i].name.slice(files[i].name.lastIndexOf('.'));
            if (fileExtension == '.xls') {
                test.append(files[i].name, files[i]);
                flagExt = false;
            } else {
                $("#errorMsg").show().text("Error ! Only .xls files are allowed");
            }
        }

        if (!flagExt) {
            $.ajax({
                type: "POST",
                url: "/FileUploadHandler.ashx?timestamp=" + timeStamp,
                data: test,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (result) {
                    $.ajax({
                        type: "POST",
                        url: "/Admin/WebAPI/CarBulkUpload.asmx/BulkUpload",
                        data: "{'FileName':'" + JSON.parse(result)[0].URL + "'}",
                        contentType: "application/json; charset=utf-8",
                        datatype: "json",
                        beforeSend: function () {
                            $("#fade").show();
                        },
                        complete: function () {

                        },
                        async: false,
                        success: function (response) {
                            if (response.d != 'Session Expired') {
                                //if (response.d == 'Invalid Extension') {
                                //    $("#errorMsg").show().text("Error ! Only .xls files are allowed");                                
                                //} else {
                                $("#errorMsg").hide().text("");
                                var xmldoc = $.parseXML(response.d);
                                var CarList = $(xmldoc).find("Table1");
                                $("#fileCarBulk").hide();
                                $("#btnUploadBulk").hide();
                                $("#btnFinalSave").show();

                                $("#tblTempCarList tbody tr").remove();
                                var row = $("#tblTempCarList thead tr").clone(true);
                                if (CarList.length > 0) {
                                    $.each(CarList, function () {
                                        $(".CarId", row).text($(this).find("CarId").text());
                                        if ($(this).find("chkCategory").text() == 'Ok') {
                                            $(".Category", row).text($(this).find("Category").text()).css("background-color", '#FFF');
                                        } else {
                                            $(".Category", row).text($(this).find("Category").text()).css("background-color", '#FF8A80');
                                        }
                                        if ($(this).find("chkSubcategory").text() == 'Ok') {
                                            $(".SubCategory", row).text($(this).find("Subcategory").text()).css("background-color", '#FFF');
                                        } else {
                                            $(".SubCategory", row).text($(this).find("Subcategory").text()).css("background-color", '#FF8A80');
                                        }

                                        $(".CarName", row).text($(this).find("CarName").text());
                                        $(".PreferVendor", row).text($(this).find("PreferVendor").text());
                                        $(".ReOrderMarkBox", row).text($(this).find("ReOrderMarkBox").text());
                                        $(".Unit1", row).text('Piece');
                                        $(".Qty1", row).text('1');
                                        $(".MinPrice1", row).text($(this).find("P_MinPrice").text());
                                        $(".wholesalePrice1", row).text($(this).find("P_WholeSale").text());
                                        $(".BasePrice1", row).text($(this).find("P_BasePrice").text());
                                        $(".CostPrice1", row).text($(this).find("P_CostPrice").text());
                                        $(".SRP1", row).text($(this).find("P_SRP").text());
                                        $(".Commission1", row).text($(this).find("P_CommCode").text());
                                        $(".Location1", row).text($(this).find("P_Location").text());
                                        $(".Barcode1", row).text($(this).find("P_Barcode").text());



                                        $(".Unit2", row).text('Box');
                                        if ($(this).find("chkBox").text() == 'Ok') {
                                            $(".Pieces2", row).text($(this).find("B_NoOfPieces").text()).css("background-color", '#fff');
                                            $(".BasePrice2", row).text($(this).find("B_BasePrice").text()).css("background-color", '#fff');
                                        } else {
                                            if ($(this).find("B_NoOfPieces").text() == "") {
                                                $(".Pieces2", row).text($(this).find("B_NoOfPieces").text()).css("background-color", '#FF8A80');
                                            } else {
                                                $(".Pieces2", row).text($(this).find("B_NoOfPieces").text());
                                            }
                                            if ($(this).find("B_BasePrice").text() == "") {
                                                $(".BasePrice2", row).text($(this).find("B_BasePrice").text()).css("background-color", '#FF8A80');
                                            } else {
                                                $(".BasePrice2", row).text($(this).find("B_BasePrice").text());
                                            }
                                        }

                                        $(".Qty2", row).text($(this).find("B_Qty").text());
                                        $(".MinPrice2", row).text($(this).find("B_MinPrice").text());
                                        $(".wholesalePrice2", row).text($(this).find("B_WholeSale").text());
                                        $(".BasePrice2", row).text($(this).find("B_BasePrice").text());
                                        $(".CostPrice2", row).text($(this).find("B_CostPrice").text());
                                        $(".SRP2", row).text($(this).find("B_SRP").text());
                                        $(".Commission2", row).text($(this).find("B_CommCode").text());
                                        $(".Location2", row).text($(this).find("B_Location").text());
                                        $(".Barcode2", row).text($(this).find("B_Barcode").text());
                                        $(".Unit3", row).text('Case');
                                        if ($(this).find("chkCase").text() == 'Ok') {
                                            $(".Pieces3", row).text($(this).find("C_NoOfPieces").text());
                                            $(".BasePrice3", row).text($(this).find("C_BasePrice").text());
                                        } else {
                                            if ($(this).find("B_NoOfPieces").text() == "") {
                                                $(".Pieces3", row).text($(this).find("C_NoOfPieces").text()).css("background-color", '#FF8A80');
                                                $(".BasePrice3", row).text($(this).find("C_BasePrice").text());
                                            } else {
                                                $(".Pieces3", row).text($(this).find("C_NoOfPieces").text());
                                                $(".BasePrice3", row).text($(this).find("C_BasePrice").text()).css("background-color", '#FF8A80');
                                            }
                                        }

                                        $(".Qty3", row).text($(this).find("C_Qty").text());
                                        $(".MinPrice3", row).text($(this).find("C_MinPrice").text());
                                        $(".wholesalePrice3", row).text($(this).find("C_WholeSale").text());
                                        $(".BasePrice3", row).text($(this).find("C_BasePrice").text());
                                        $(".CostPrice3", row).text($(this).find("C_CostPrice").text());
                                        $(".SRP3", row).text($(this).find("C_SRP").text());
                                        $(".Commission3", row).text($(this).find("C_CommCode").text());
                                        $(".Location3", row).text($(this).find("C_Location").text());
                                        $(".Barcode3", row).text($(this).find("C_Barcode").text());
                                        $(".D_Selling", row).text($(this).find("D_Selling").text());

                                        $("#tblTempCarList tbody").append(row);
                                        row = $("#tblTempCarList tbody tr:last").clone(true);
                                    });
                                    $("#tblTempCarList").show();
                                    //$("#tblTempCarList tbody td").each(function () {
                                    //    if ($(this).text() == "") {

                                    //    }
                                    //});

                                } else {
                                    $("#tblTempCarList tbody tr").remove();
                                    $("#tblTempCarList").hide();
                                }

                                //var pager = $(xmldoc).find("Table");
                                //$(".PagerTemp").ASPSnippets_Pager({
                                //    ActiveCssClass: "current",
                                //    PagerCssClass: "pager",
                                //    PageIndex: parseInt(pager.find("PageIndex").text()),
                                //    PageSize: parseInt(pager.find("PageSize").text()),
                                //    RecordCount: parseInt(pager.find("RecordCount").text())
                                //});
                                //}
                            } else {
                                location.href = '/';
                            }
                            $("#fade").hide();
                        },
                        error: function (result) {
                            console.log(result);
                            $("#errorMsg").show().text("Error ! Something not right with excel sheet");
                        },
                        failure: function (result) {
                            console.log(result);
                        }
                    });
                },
                error: function (result) {
                    $("#errorMsg").show().text("Something not right.");
                },
                failure: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                }
            });
        }
    } else {
        $("#errorMsg").show().text("No file selected.");
    }
}

function FinalSave() {

    $("#finalError").hide().text('');
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/CarBulkUpload.asmx/FinalUpload",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (result) {
            alert(result.d);
            if (result.d != 'Session Expired') {
                getCarList(1);
                $("#ModalBulUpload").modal('toggle');
            } else {
                location.href = '/';
            }
        },
        error: function (result) {
            console.log(result);
            $("#finalError").show().text('Something went wrong');
        },
        failure: function (result) {
            console.log(result);
        }
    });
}


function resetModalBulUpload() {
    $("#finalError").hide().text('');
    $("#errorMsg").hide().text('');
    $("#fileCarBulk").show().val("");
    $("#btnUploadBulk").show();
    $("#btnFinalSave").hide();
    $("#tblTempCarList").hide().find("tbody tr").remove();
}