﻿$(document).ready(function () {
    bindSalesPerson();
    BindProduct();
    var yesterday = new Date((new Date()).valueOf() - 1000 * 60 * 60 * 24);

    $('#txtSFromDate').pickadate({
        min: new Date("01/01/2019"),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true

    });
    $('#txtSToDate').pickadate({
        min: new Date("01/01/2019"),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true

    });
    $('#Pop_txtAllowDate').pickadate({
        min: new Date("01/01/2019"),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true,
        disable: [
    { from: [0, 0, 0], to: yesterday }
        ]
    });
    var d = new Date();
    var month = d.getMonth() + 1;
    if (month.toString().length == 1) {
        month = '0' + month;
    }
    var day = d.getDate();
    if (day.toString().length == 1) {
        day = '0' + day;
    }
    var year = d.getFullYear();
    $("#txtSFromDate").val(month + '/' + day + '/' + year);
    $("#txtSToDate").val(month + '/' + day + '/' + year);
})
function setdatevalidation(val) {
    var fdate = $("#txtSFromDate").val();
    var tdate = $("#txtSToDate").val();
    if (val == 1) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtSToDate").val(fdate);
        }
    }
    else if (val == 2) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtSFromDate").val(tdate);
        }
    }
}
function bindSalesPerson() {
    $.ajax({
        type: "POST",
        url: "AllowQtyToSalesPerson.aspx/BindSalesPerson",
        data: "{}",
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "Session Expired") {
                var getData = $.parseJSON(response.d);               
                $("#ddlSalesPerson option:not(:first)").remove();
                $("#ddlSalesPePop_ddlSalesPersonrson option:not(:first)").remove();
                $.each(getData, function (index, item) { 
                    $("#ddlSalesPerson").append("<option value='" + item.A + "'>" + item.SP + "</option>");
                    $("#Pop_ddlSalesPerson").append("<option value='" + item.A + "'>" + item.SP + "</option>");
                });
                $("#ddlSalesPerson").select2();
                $("#Pop_ddlSalesPerson").select2();             
            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function OpenPopup() {
    Cancel();
    $("#ModalSaveAllowQty").modal('show');
    $("#alertStockQty").hide();

}

function BindProduct() {

    $.ajax({
        type: "POST",
        url: "AllowQtyToSalesPerson.aspx/BindProduct", 
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var getData = $.parseJSON(response.d);
                    $("#ddlProduct option:not(:first)").remove();
                    $("#Pop_ddlProduct option:not(:first)").remove();
                    $.each(getData, function (index, item) {
                        $("#ddlProduct").append("<option value='" + item.A + "'>" + item.PM + "</option>");
                        $("#Pop_ddlProduct").append("<option value='" + item.A + "'>" + item.PM + "</option>");
                    });
                    $("#ddlProduct").select2();
                    $("#Pop_ddlProduct").select2();  
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function Popsave() {
    if ($("#Pop_save_btn").html() == "Update") {
        checkConfirm();
    } else {
        InsertData();
    }
}

function checkConfirm() {
    swal({
        title: "Are you sure?",
        text: "You want to update.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, Cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, Update it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            if ($("#Pop_TxtAllowPieces").val() == 0) {
                swal("", "Allow Quantity In Pieces can not be zero.", "error").then(function () {
                    $("#Pop_TxtAllowPieces").addClass('border-warning');
                    $("#Pop_TxtAllowPieces").focus();
                });
            }
            else if (parseInt($("#Pop_TxtAllowPieces").val()) >= parseInt($("#Pop_UsedQty").val())) {
                InsertData();
            }
            else {
                swal("", "Allow Qty In Piece is not less than used qty " + parseInt($("#Pop_UsedQty").val()) + ".", "error").then(function () {
                    $("#Pop_TxtAllowPieces").addClass('border-warning');
                    $("#Pop_TxtAllowPieces").focus();

                });;
            }

        }
            //else {
        //    //swal("", "Your data is safe.", "error");
        //}
    })


}
function InsertData() {
    if (checkrequired()) {
        if ($("#Pop_TxtAllowPieces").val() == 0) {
            swal("", "Allow Quantity In Pieces can not be zero.", "error").then(function () {
                $("#Pop_TxtAllowPieces").addClass('border-warning');
                $("#Pop_TxtAllowPieces").focus();
            });;
            //toastr.error('Allow Quantity In Pieces can not be zero.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        }
        else {
            var data = {
                AutoId: $("#Pop_AutoId").val(),
                AllowDate: $("#Pop_txtAllowDate").val(),
                ProductId: $("#Pop_ddlProduct").val(),
                AllowQty: $("#Pop_TxtAllowPieces").val(),
                SalesPersonId: $("#Pop_ddlSalesPerson").val()
            }
            $.ajax({
                type: "POST",
                url: "/Admin/WebAPI/AllowQtyInPiece.asmx/InsertAndUpdate",
                data: JSON.stringify({ dataValue: JSON.stringify(data) }),
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {

                    if (response.d == "Insert") {

                        swal("", "Qty allowed successfully.", "success").then(function () {
                            getAllowQtyList(1);
                            Cancel();
                            $("#ModalSaveAllowQty").modal('hide');

                        });;
                    }
                    if (response.d == "Update") {

                        swal("", "Allow qty updated successfully.", "success").then(function () {
                            getAllowQtyList(1);
                            Cancel();
                            $("#ModalSaveAllowQty").modal('hide');

                        });;
                    }

                },
                error: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                },
                failure: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                }
            });

        }
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
}


function SetProductUnit() {
    var id = $("#Pop_ddlProduct").val();
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/AllowQtyInPiece.asmx/GetProductUnit",
        data: "{'ProductId':'" + id + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var data = $(xmldoc).find("Table");
                if (data.length > 0) {
                    if ($(data).find("PackingAutoId").text() != "3") {
                        $("#alertStockQty").html(" [ " + $(data).find("UnitType").text() + " : " + $(data).find("Qty").text() + " Pieces ]");
                        $("#alertStockQty").show();
                    }
                    else {
                        $("#alertStockQty").html('');
                        $("#alertStockQty").hide();
                    }
                } else {
                    $("#alertStockQty").html('');
                    $("#alertStockQty").hide();
                }

            } else {
                location.href = "/";
            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}

function Cancel() {
    $("#Pop_save_btn").html("Save");
    $("#Pop_heading").html("Add New");
    $("#Pop_AutoId").val('');
    $("#Pop_UsedQty").val('');
    $("#Pop_txtAllowDate").val('');
    $("#Pop_ddlProduct").select2('val', '0');
    $("#Pop_TxtAllowPieces").val('');
    $("#Pop_ddlSalesPerson").select2('val', '0');
    $("#ModalSaveAllowQty").modal('hide');
    $('.req').each(function () {
        $(this).removeClass('border-warning');
    });
    $('.ddlreq').each(function () {
        $(this).closest('div').find('.select2-selection--single').removeAttr('style');
    });
    $("#alertStockQty").hide();
    $("#Pop_txtAllowDate").removeAttr('disabled');
    $("#Pop_ddlProduct").removeAttr('disabled');
    $("#Pop_ddlSalesPerson").removeAttr('disabled');
}

function getAllowQtyList(pageIndex) {

    var data = {
        SalesPersonAutoId: $("#ddlSalesPerson").val(),
        ProductId: $("#ddlProduct").val(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        pageIndex: pageIndex,
        PageSize: $("#ddlPageSize").val(),
    };

    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/AllowQtyInPiece.asmx/getList",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var orderList = $(xmldoc).find("Table1");
                $("#tblOrderList tbody tr").remove();
                var row = $("#tblOrderList thead tr").clone(true);
                if (orderList.length > 0) {
                    $("#EmptyTable").hide();
                    $.each(orderList, function () {
                        if ($(this).find("UsedQty").text() == '0') {
                            $(".action", row).html("<a AutoId=" + $(this).find("AutoId").text() + " title='Edit' href='javascript:;' onclick='EditRecord(\"" + $(this).find("AutoId").text() + "\")'><span class='la la-edit'></span></a>&nbsp;<a href='javascript:;' title='Delete'><span class='la la-remove' onclick='DeleteRecord(\"" + $(this).find("AutoId").text() + "\")'></span><b></a></b>");

                        }
                        else {
                            $(".action", row).html("<a AutoId=" + $(this).find("AutoId").text() + " title='Edit' href='javascript:;' onclick='EditRecord(\"" + $(this).find("AutoId").text() + "\")'><span class='la la-edit'></span></a>&nbsp;<a href='javascript:;' title='Delete'><span class='la la-remove' onclick='DeleteRecord(\"" + $(this).find("AutoId").text() + "\")'></span><b></a>&nbsp;<a href='javascript:;' title='Allocated product details'><span class='la la-history' onclick='AllocatedProductQtyDetails(this)'></span><b></a></b>");

                        }
                        $(".AllowDate", row).text($(this).find("AllowDate").text());
                        $(".SalesPerson", row).text($(this).find("EmpNmae").text());
                        $(".ProductId", row).text($(this).find("ProductId").text());
                        $(".Product", row).text($(this).find("ProductName").text());
                        $(".DefautlUnit", row).text($(this).find("DefautlUnit").text());
                        $(".AllowQty", row).text($(this).find("AllowQtyInPieces").text());
                        $(".UseQty", row).text($(this).find("UsedQty").text());
                        $(".RemainQty", row).text($(this).find("RemainQty").text());
                        $("#tblOrderList tbody").append(row);
                        row = $("#tblOrderList tbody tr:last").clone(true);
                    });
                } else {
                    $("#EmptyTable").show();
                }

                var pager = $(xmldoc).find("Table");
                $(".Pager").ASPSnippets_Pager({
                    ActiveCssClass: "current",
                    PagerCssClass: "pager",
                    PageIndex: parseInt(pager.find("PageIndex").text()),
                    PageSize: parseInt(pager.find("PageSize").text()),
                    RecordCount: parseInt(pager.find("RecordCount").text())
                });
            } else {
                location.href = "/";
            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function Pagevalue(e) {
    getAllowQtyList(parseInt($(e).attr("page")));
};


function DeleteRecord(id) {
    swal({
        title: "Are you sure?",
        text: "You want to delete.",
        icon: "warning",
        showCancelButton: true,
        allowOutsideClick: false,
        closeOnClickOutside: false,

        buttons: {
            cancel: {
                text: "No, Cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, Delete it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "POST",
                url: "/Admin/WebAPI/AllowQtyInPiece.asmx/Delete",
                data: "{'AutoId':" + id + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.d == "Session Expired") {
                        location.href = "/";
                    }
                    else if (response.d == "Success") {

                        swal("", "Deleted successfully.", "success")
                        getAllowQtyList(1);
                    }
                },
                failure: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                },
                error: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                }
            });

        } 
    })
}
function EditRecord(id) {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/AllowQtyInPiece.asmx/GetEditData",
        data: "{'AutoId':" + id + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var List = $(xmldoc).find("Table");
                if (List.length > 0) {
                    $("#Pop_save_btn").html("Update");
                    $("#Pop_heading").html("Update");
                    $("#Pop_AutoId").val($(List).find("AutoId").text());
                    $("#Pop_UsedQty").val($(List).find("UsedQty").text());
                    $("#Pop_txtAllowDate").val($(List).find("AllowDate").text());
                    $('#Pop_ddlProduct').val($(List).find("ProductAutoId").text()).change();
                    $("#Pop_TxtAllowPieces").val($(List).find("AllowQtyInPieces").text());
                    $("#Pop_ddlSalesPerson").val($(List).find("SalesPersonAutoId").text()).change();
                    $("#Pop_txtAllowDate").attr('disabled', 'disabled');
                    $("#Pop_ddlProduct").attr('disabled', 'disabled');
                    $("#Pop_ddlSalesPerson").attr('disabled', 'disabled');
                    $("#ModalSaveAllowQty").modal('show');
                }

            } else {
                location.href = "/";
            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });


}

function checkrequired() {
    var boolcheck = true;
    $('.req').each(function () {
        if ($(this).val() == '') {
            boolcheck = false;
            $(this).addClass('border-warning');
        }
            //} else if (parseFloat($(this).val()) <= 0) {
            //    boolcheck = false;
            //    $(this).addClass('border-warning');
            //}
        else {
            $(this).removeClass('border-warning');
        }
    });
    $('.ddlreq').each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == 'undefined' || $(this).val() == null) {
            boolcheck = false;
            $(this).closest('div').find('.select2-selection--single').attr('style', 'border:1px solid #FF9149 !important');
        } else {
            $(this).removeClass('border-warning');
            $(this).closest('div').find('.select2-selection--single').removeAttr('style');
        }
    });

    return boolcheck;
}

function AllocatedProductQtyDetails(e) {
    debugger;
    var row = $(e).closest('tr');
    var AutoId = $(row).find(".action  a").attr('AutoId')
    if ($(row).find(".UseQty").html() != '0') {
        $.ajax({
            type: "POST",
            url: "AllowQtyToSalesPerson.aspx/AllocatedProductQtyDetails",
            data: "{'AutoId':'" + AutoId + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d != "Session Expired") {
                    var xmldoc = $.parseXML(response.d);
                    var orderList = $(xmldoc).find("Table");

                    $("#mdAllowQtyDetails").modal('show');
                    $("#tblAllocatedQtySummary tbody tr").remove();
                    var row = $("#tblAllocatedQtySummary thead tr").clone(true);
                    if (orderList.length > 0) {
                        var pieces = 0;
                        $("#EmptyTable2").hide();
                        $.each(orderList, function () {
                            $(".OrderNo", row).text($(this).find("OrderNo").text());
                            $(".OrderDate", row).text($(this).find("OrderDate").text());
                            $(".CustomerName", row).text($(this).find("CustomerName").text());
                            $(".PackedQty", row).text($(this).find("PackedQty").text());
                            $(".DefautlUnit", row).text($(this).find("DefautlUnit").text());
                            $(".PackedPieces", row).text($(this).find("PackedPieces").text());
                            pieces += parseFloat($(this).find("PackedPieces").text());
                            $("#tblAllocatedQtySummary tbody").append(row);
                            row = $("#tblAllocatedQtySummary tbody tr:last").clone(true);
                        });
                        $("#tdPackedPieces").html(pieces || 0);
                    } else {
                        $("#EmptyTable2").show();
                    }
                    var headerDetails = $(xmldoc).find("Table1");
                    if (headerDetails.length > 0) {

                        $("#spSalesPerson").html($(headerDetails).find("Name").text());
                        $("#spProductName").html($(headerDetails).find("ProductName").text());
                        $("#spAllowDate").html($(headerDetails).find("AllowDate").text());
                    }
                } else {
                    location.href = "/";
                }
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
}