﻿$(document).ready(function () {
    getProductList(1);
    bindCategory();
});
function Pagevalue(e) {
    getProductList(parseInt($(e).attr("page")));
};
/*---------------------------------------------------Bind Category-----------------------------------------------------------*/
function bindCategory() {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/RequestProductList.asmx/bindCategory",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () { 
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>', 
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var category = $(xmldoc).find("Table");

            $("#ddlSCategory option:not(:first)").remove();            
            $.each(category, function () {
                $("#ddlSCategory").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("CategoryName").text() + "</option>");
            });            
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

/*-------------------------------------------------------Bind Subcategory-----------------------------------------------------------*/
$("#ddlSCategory").change(function () {
    var categoryAutoId = $("#ddlSCategory").val();
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/RequestProductList.asmx/bindSubcategory",
        data: "{'categoryAutoId':'" + categoryAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () { 
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>', 
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var subcategory = $(xmldoc).find("Table");
            $("#ddlSSubcategory option:not(:first)").remove();
            $.each(subcategory, function () {
                $("#ddlSSubcategory").append("<option value=' " + $(this).find("AutoId").text() + "'>" + $(this).find("SubcategoryName").text() + "</option>");
            });
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
})
/*-------------------------------------------------------Get Product List----------------------------------------------------------*/
function getProductList(PageIndex) {
    var data = {
        CategoryAutoId: $("#ddlSCategory").val(),
        SubcategoryAutoId: $("#ddlSSubcategory").val(),
        Status: $("#ddlStatus").val(),
        ProductId: $("#txtSProductId").val().trim(),
        ProductName: $("#txtSProductName").val().trim(),
        Location: $("#Hd_Domain").val(),
        BarCode: $("#txtBarCode").val(),      
        PageSize: $("#ddlPageSize").val(),
        pageIndex: PageIndex
    };
    localStorage.setItem('SearchFilter',JSON.stringify(data));
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/RequestProductList.asmx/getProductList",
        data: JSON.stringify({'dataValue': JSON.stringify(data)}),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () { 
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>', 
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successGetProductList,        
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function successGetProductList(response)
{ 
    var xmldoc = $.parseXML(response.d);
    var productList = $(xmldoc).find("Table1");
    
    $("#tblProductList tbody tr").remove();

    var row = $("#tblProductList thead tr").clone(true);   
    console.log(productList);
    if (productList.length > 0) { 
        $("#EmptyTable").hide();
        $.each(productList, function (index) {           
            $(".ProductId", row).text($(this).find("ProductId").text());
            $(".Category", row).text($(this).find("Category").text());
            $(".Subcategory", row).text($(this).find("Subcategory").text());
            $(".ProductName", row).text($(this).find("ProductName").text().replace(/&quot;/g, "\'"));
            $(".Stock", row).text($(this).find("Stock").text());
            $(".BrandName", row).text($(this).find("BrandName").text());
            $(".ReOrderMark", row).text($(this).find("ReOrderMark").text());
            $(".Location", row).text($(this).find("ProductLocation").text());
            $(".CreatedOn", row).text($(this).find("CreatedOn").text());
            $(".CreatedBy", row).text($(this).find("CreatedBy").text());
            if($(this).find("Status").text()=='Active')
            {
                $(".Status", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("Status").text() + "</span>");
            }
            else
            {               
                $(".Status", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("Status").text() + "</span>");
            }
           
            $(".CommCode", row).html($(this).find("CommCode").text());
            $(".ImageUrl", row).html("<img src='" + $(this).find('ImageUrl').text() + "' class='zoom img img-circle img-responsive' style='width: 50px;height: 50px;'  >");
          
            if ($('#Hd_Domain').val() == 'psmnj' || $('#Hd_Domain').val() == 'psm' || $('#Hd_Domain').val() == 'localhost') {//Delete option show to only PSMNJ
                $(".action", row).html("<a title='Edit' href='/Admin/RequestProductMaster.aspx?ProductId=" + $(this).find("AutoId").text() + "'><span class='ft-edit' ></span></a>&nbsp;&nbsp<a title='Delete' href='javascript:;'><span class='ft-x' onclick='deleterecord(\"" + $(this).find("ProductId").text() + "\") '></span></a>");
            }
            else
            {
                $(".action", row).html("<a title='Edit' href='/Admin/RequestProductMaster.aspx?ProductId=" + $(this).find("AutoId").text() + "'><span class='ft-edit' ></span></a>");
            }
            $("#tblProductList tbody").append(row);
            row = $("#tblProductList tbody tr:last").clone(true);
        }); 
       
    } else {
        $("#EmptyTable").show();
    }
    
    if ($("#hiddenForPacker").val() != "") {
        $("#linkAddNewProduct").hide();
        $(".glyphicon-remove").hide(); 
        $(".action").hide();
    }
   
    var pager = $(xmldoc).find("Table"); 
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
}
$("#btnSearch").click(function () {
    getProductList(1);    
})

function deleterecord(ProductId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this product.",
        icon: "warning",
        showCancelButton: true,
        allowOutsideClick: false,
        closeOnClickOutside: false,
        buttons: {
            cancel: {
                text: "No, Cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, Delete it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(isConfirm => {
        if (isConfirm) {   
             deleteProduct(ProductId);            
} 
})
}
function deleteProduct(ProductId)
{  
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/RequestProductList.asmx/deleteProduct",
        data: "{'ProductId':'" + ProductId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () { 
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>', 
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (result) {
            if (result.d == 'Session Expired') {
                location.href="/";
            }
            else if (result.d == 'true') {
                swal("", "Draft Product deleted successfully.", "success");                 
                getProductList(1);
            } 
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
            $("#alertCannotDelete").show();
            $("#cannotDeleteClose").click(function () {
                $("#alertCannotDelete").hide();
            })
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
