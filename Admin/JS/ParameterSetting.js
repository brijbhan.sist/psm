﻿$(document).ready(function () {
    debugger;
    getStateDetail(1);
    bindActiveState();
    getCityDetail(1);
    bindSearchState_City();
    bindActiveCity_Zip();
    getZipDetail(1);
})
function Pagevalue(e) {
    if ($(e).parent().attr("id") == "PagerState") {
        getStateDetail(parseInt($(e).attr("page")));
    }
    if ($(e).parent().attr("id") == "PagerCity") {
        getCityDetail(parseInt($(e).attr("page")));
    }
    if ($(e).parent().attr("id") == "PagerZip") {
        getZipDetail(parseInt($(e).attr("page")));
    }
};
//here start function to state tab
$(".close").click(function () {
    $(".alert").hide();
});
function bindstate() {

    var data = {
        SSName: $("#txtStateNameState").val(),
        Status: $("#ddlStatus_State").val()
    }
    $.ajax({
        type: "POST",
        url: "WebAPI/ParameterSetting.asmx/insertData",
        data: "{'datavalue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $("#fade").show();
        },
        complete: function () {
            $("#fade").hide();
        },
        success: function (result) {
        },
        error: function (result) {
            swal("Error!", "Oops! something wrong.Please try later.", "error");
        },
        failure: function (result) {
            swal("Error!", "Oops! something wrong.Please try later.", "error");
        }
    });
}
function checkRequiredFieldState() {
    var boolcheck = true;
    $('.req').each(function () {
        if ($(this).val().trim() == '') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });
    return boolcheck;
}
$("#btnSave").click(function () {
    if (checkRequiredFieldState()) {
        var data = {
            SId: $("#txtStateCode").val().trim(),
            SName: $("#txtStateName_State").val().trim(),
            Abbreviation: $("#txtAbbreviation_State").val().trim(),
            Status: $("#ddlStatus_State").val()
        }
        $.ajax({
            type: "POST",
            url: "WebAPI/ParameterSetting.asmx/insertData",
            data: "{'datavalue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == "Success") {
                    swal("", "State details saved successfully.", "success");
                    resetStateAdd();
                    getStateDetail(1);
                    bindActiveState();
                }
                else if (result.d == "Unauthorized access.") {
                    location.href = "/";
                }
                else {
                    swal("", result.d, "error");
                }
            },
            error: function (result) {
                swal("Error!", "Oops! Something wrong.Please try later.", "error");
            },
            failure: function (result) {
                swal("Error!", "Oops! Something wrong.Please tyr later.", "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
});
$("#btnUpdate").click(function () {
    if (checkRequiredFieldState()) {
        var data = {
            SId: $("#txtStateCode").val(),
            SName: $("#txtStateName_State").val(),
            Abbreviation: $("#txtAbbreviation_State").val(),
            Status: $("#ddlStatus_State").val()
        }
        $.ajax({
            type: "POST",
            url: "WebAPI/ParameterSetting.asmx/UpdateStateMaster",
            data: "{'datavalue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == "Success") {
                    swal("", "State details updated successfully.", "success");
                    resetStateAdd()
                    getStateDetail(1);
                }
                else if (result.d == "Unauthorized access.") {
                    location.href = "/";
                }
                else {
                    swal("", result.d, "error");
                }
            },
            error: function (result) {
                swal("Error!", "Oops! Something wrong.Please try later.", "error");
            },
            failure: function (result) {
                swal("Error!", "Oops! Something wrong.Please try later.", "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
});
function getStateDetail(pageIndex) {
    var data = {
        StateSearch: $('#txtSNameSearch_State').val().trim(),
        Status: $('#ddlSStatusSearch_State').val(),
        pageIndex: pageIndex
    }
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/ParameterSetting.asmx/getStateDetail",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: onSuccessOfCategory,
        error: function (response) {
            console.log(JSON.parse(response.responseText).d);
        }
    });
}
function onSuccessOfCategory(response) {
    var xmldoc = $.parseXML(response.d);
    var category = $(xmldoc).find('Table1');

    if (category.length > 0) {
        $('#EmptyTableState').hide();
        $('#tblState tbody tr').remove();
        var row = $('#tblState thead tr').clone(true);
        $.each(category, function () {
            $(".SStateName", row).text($(this).find("StateName").text());
            $(".SAbbreviation", row).text($(this).find("StateCode").text());

            if ($(this).find("Status").text() == 'Active') {
                $(".Sstatus", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("Status").text() + "</span>");
            }
            else {
                $(".Sstatus", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("Status").text() + "</span>");
            }
            $(".Saction", row).html("<a href='#' title='Edit'><span class='ft-edit' onclick='editState(\"" + $(this).find("AutoId").text() + "\")' /></a>&nbsp;&nbsp<a href='#' title='Delete'><span class='ft-x' onclick= 'deleteState(\"" + $(this).find("AutoId").text() + "\")' /></a>");
            $("#tblState tbody").append(row);
            row = $("#tblState tbody tr:last-child").clone(true);
        });
    }
    else {
        $('#tblState tbody tr').remove();
        $('#EmptyTableState').show();
    }
    var pager = $(xmldoc).find("Table");
    $("#PagerState").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
}
function editState(AutoId) {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/ParameterSetting.asmx/EditStateMaster",
        data: "{'AutoId':'" + AutoId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $("#fade").hide();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var StateDetail = $(xmldoc).find("Table");
            $("#txtStateCode").val($(StateDetail).find("AutoId").text());
            $("#txtStateName_State").val($(StateDetail).find("StateName").text()).change();
            $("#txtAbbreviation_State").val($(StateDetail).find("StateCode").text()).change();
            $("#ddlStatus_State").val($(StateDetail).find("Status").text());
            btnShow();
        },
        error: function (result) {
            swal("Error!", "Oops! Something wrong.Please try later.", "error");
        },
        failure: function (result) {
            swal("Error!", "Oops! Something wrong.Please try later.", "error");
        }
    });
}

function deleteState(AutoId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this data",
        icon: "warning",
        showCancelButton: true,
        allowOutsideClick: false,
        closeOnClickOutside: false,
        buttons: {
            cancel: {
                text: "No, cancel.",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, delete it.",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (willdelete) {
        if (willdelete) {
            $.ajax({
                type: "POST",
                url: "/Admin/WebAPI/ParameterSetting.asmx/DeleteStateMaster",
                data: "{'AutoId':'" + AutoId + "'}",
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.d == "Success") {
                        swal({
                            title: "",
                            text: "State details deleted successfully.",
                            icon: "success",
                            button: "OK",
                        }).then(function () {
                            getStateDetail(1);
                        })
                    }
                    else if (response.d == "Unauthorized access.") {
                        location.href = "/";
                    }
                    else {
                        swal("Error!", response.d, "error");
                    }
                },
                error: function (result) {
                    swal("Error!", "Oops! Something wrong.Please try later.", "error");
                },
                failure: function (result) {
                    swal("Error!", "Oops! Something wrong.Please try later.", "error");
                }
            });
        }
    })
}
$("#btnCancel").click(function () {
    resetStateAdd();
});
$("#btnReset").click(function () {
    resetStateAdd();
});
$("#btnSearch_State").click(function () {
    getStateDetail(1)
});
function resetStateAdd() {
    $("#btnUpdate").hide();
    $("#btnCancel").hide();
    $("#btnSave").show();
    $("#btnReset").show();
    $("#txtStateName_State").removeClass('border-warning');
    $("#txtAbbreviation_State").removeClass('border-warning');
    $("#ddlStatus_State").val(1);
    $('input[type="text"]').val('');
    $('textarea').val('');
    $('option:selected').each(function () {
        $(this).attr('selected', false);
    });
    $('select option:first-child').each(function () {
        $(this).attr('selected', 'selected');
    });
}
function btnShow() {
    $("#btnSave").hide();
    $("#btnReset").hide();
    $("#btnUpdate").show();
    $("#btnCancel").show();
}


//here start function to city tab

function bindActiveState() {
    $.ajax({
        type: "POST",
        url: "WebAPI/ParameterSetting.asmx/bindActiveState",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var StateDetail = $(xmldoc).find("Table");

            $("#ddlState_City option:not(:first)").remove();
            $.each(StateDetail, function () {
                $("#ddlState_City").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("StateName").text() + "</option>");
            });

            $("#ddlState_Zip option:not(:first)").remove();
            $.each(StateDetail, function () {
                $("#ddlState_Zip").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("StateName").text() + "</option>");
            });

            $("#ddlStateSearch_Zip option:not(:first)").remove();
            $.each(StateDetail, function () {
                $("#ddlStateSearch_Zip").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("StateName").text() + "</option>");
            });

            $("#ddlStateSearch_Zip").select2()

            $("#ddlState_Zip").select2()

            $("#ddlState_City").select2()

        },
        error: function (result) {
            swal("Error!", "Oops! something wrong.Please try later.", "error");
        },
        failure: function (result) {
            swal("Error!", "Oops! something wrong.Please try later.", "error");
        }
    });
}
function checkRequiredFieldCity() {
    var boolcheck = true;
    $('.reqc').each(function () {
        if ($(this).val().trim() == '') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });
    if (boolcheck) {
        boolcheck = dynamicInputTypeSelect2('ddlreqc')
    } else {
        dynamicInputTypeSelect2('ddlreqc');
    }

    return boolcheck;
}
$("#btnSave_City").click(function () {
    if (checkRequiredFieldCity()) {
        var data = {
            //CId: $("#txtCity").val(),
            CState: $("#ddlState_City").val(),
            CCity: $("#txtCity_City").val(),
            CStatus: $("#ddlStatus_City").val()
        }
        $.ajax({
            type: "POST",
            url: "WebAPI/ParameterSetting.asmx/insertData_City",
            data: "{'datavalue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == "Success") {
                    swal({
                        title: "",
                        text: "City details saved successfully.",
                        icon: "success",
                        button: "OK",
                    }).then(function () {
                        getCityDetail(1);
                        btnReset();
                        bindActiveCity_Zip();
                    })
                }
                else if (result.d == "Unauthorized access.") {
                    location.href = "/";
                }
                else {
                    swal("Warning!", result.d, "error");
                }
            },
            error: function (result) {
                swal("Error!", "Oops! something wrong.Please try later.", "error");
            },
            failure: function (result) {
                swal("Error!", "Oops! something wrong.Please try later.", "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
});
$("#btnReset_City").click(function () {
    btnReset();
});
function btnReset() {
    $("#btnUpdate_City").hide();
    $("#btnCancel_City").hide();
    $("#btnSave_City").show();
    $("#btnReset_City").show();
    $("#ddlStatus_City").val(1);
    $('input[type="text"]').val('');
    $('textarea').val('');
    $("#txtCity_City").val('');
    $("#ddlState_City").closest('div').find('.select2-selection--single').removeAttr('style');
    $("#txtCity_City").removeClass('border-warning');
    $("#ddlState_City").val(0).change();
    $('option:selected').each(function () {
        $(this).attr('selected', false);
    });
    $('select option:first-child').each(function () {
        $(this).attr('selected', 'selected');
    });
}
function getCityDetail(pageIndex) {
    var data = {
        StateSearch: $('#ddlStateSearch_City').val(),
        CitySeach: $('#txtCitySearch_City').val().trim(),
        Status: $('#ddlStatusSearch_City').val(),
        pageIndex: pageIndex
    }
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/ParameterSetting.asmx/getCityDetail",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: onSuccessOfCity,
        error: function (response) {
            swal("Error!", "Oops! something wrong.Please try later.", "error");
        }
    });
}
function onSuccessOfCity(response) {
    var xmldoc = $.parseXML(response.d);
    var category = $(xmldoc).find('Table1');

    if (category.length > 0) {
        $('#EmptyTableCity').hide();
        $('#tblCity tbody tr').remove();
        var row = $('#tblCity thead tr').clone(true);
        $.each(category, function () {

            $(".CId", row).text($(this).find("RowNumber").text());
            $(".CStateName", row).text($(this).find("StateName").text());
            $(".CCityName", row).text($(this).find("CityName").text());
            if ($(this).find("Status").text() == 'Active') {
                $(".Cstatus", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("Status").text() + "</span>");
            }
            else {
                $(".Cstatus", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("Status").text() + "</span>");
            }
            $(".Caction", row).html("<a href='#' title='Edit'><span class='ft-edit' onclick='editCity(\"" + $(this).find("AutoId").text() + "\")' /></a>&nbsp;&nbsp<a href='#' title='Delete'><span class='ft-x' onclick= 'deleteCity(\"" + $(this).find("AutoId").text() + "\")' /></a>");
            $("#tblCity tbody").append(row);
            row = $("#tblCity tbody tr:last-child").clone(true);
        });
    }
    else {
        $('#tblCity tbody tr').remove();
        $('#EmptyTableCity').show();
    }
    var pager = $(xmldoc).find("Table");
    $("#PagerCity").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
}
$("#btnSearch_City").click(function () {
    getCityDetail(1);
    bindSearchState_City();
});
function bindSearchState_City() {
    $.ajax({
        type: "POST",
        url: "WebAPI/ParameterSetting.asmx/bindSearchState_City",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var StateDetail = $(xmldoc).find("Table");

            $("#ddlStateSearch_City option:not(:first)").remove();
            $.each(StateDetail, function () {
                $("#ddlStateSearch_City").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("StateName").text() + "</option>");
            });
            $("#ddlStateSearch_City").select2();
        },
        error: function (result) {
            swal("Error!", "Oops! something wrong.Please try later.", "error");
        },
        failure: function (result) {
            swal("Error!", "Oops! something wrong.Please try later.", "error");
        }
    });
}
function deleteCity(AutoId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this data",
        icon: "warning",
        showCancelButton: true,
        allowOutsideClick: false,
        closeOnClickOutside: false,
        buttons: {
            cancel: {
                text: "No, cancel.",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, delete it.",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (willdelete) {
        if (willdelete) {
            $.ajax({
                type: "POST",
                url: "/Admin/WebAPI/ParameterSetting.asmx/DeleteCityMaster",
                data: "{'AutoId':'" + AutoId + "'}",
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.d == "Success") {
                        swal({
                            title: "",
                            text: "City details deleted successfully.",
                            icon: "success",
                        }).then(function () {
                            getCityDetail(1);
                        })
                    }
                    else if (response.d == "Unauthorized access.") {
                        location.href = "/";
                    }
                    else {
                        swal("Error!", response.d, "error");
                    }
                },
                error: function (result) {
                    swal("Error!", "Oops! something wrong.Please try later.", "error");
                },
                failure: function (result) {
                    swal("Error!", "Oops! something wrong.Please try later.", "error");
                }
            });
        }
    })
}
function editCity(AutoId) {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/ParameterSetting.asmx/EditCityMaster",
        data: "{'AutoId':'" + AutoId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var StateDetail = $(xmldoc).find("Table");
            $("#txtCityCode_City").val($(StateDetail).find("AutoId").text());
            $("#ddlState_City").val($(StateDetail).find("StateId").text()).change();
            $("#txtCity_City").val($(StateDetail).find("CityName").text()).change();
            $("#ddlStatus_City").val($(StateDetail).find("Status").text());
            btnShow_City();
        },
        error: function (result) {
            swal("Error!", "Oops! something wrong.Please try later.", "error");
        },
        failure: function (result) {
            swal("Error!", "Oops! something wrong.Please try later.", "error");
        }
    });
}
function btnShow_City() {
    $("#btnSave_City").hide();
    $("#btnReset_City").hide();
    $("#btnUpdate_City").show();
    $("#btnCancel_City").show();
}
$("#btnUpdate_City").click(function () {
    if (checkRequiredFieldCity()) {
        var data = {
            CCityId: $("#txtCityCode_City").val(),
            CState: $("#ddlState_City").val(),
            CCity: $("#txtCity_City").val(),
            CStatus: $("#ddlStatus_City").val()
        }
        $.ajax({
            type: "POST",
            url: "WebAPI/ParameterSetting.asmx/UpdateCityMaster",
            data: "{'datavalue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == "Success") {
                    swal({
                        title: "",
                        text: "City details updated successfully.",
                        icon: "success",
                    }).then(function () {
                        getCityDetail(1);
                        btnReset();
                    })
                }
                else if (result.d == "Unauthorized access.") {
                    location.href = "/";
                }
                else {
                    swal("Warning!", result.d, "error");
                }
            },
            error: function (result) {
                swal("Error!", "Oops! something wrong.Please try later.", "error");
            },
            failure: function (result) {
                swal("Error!", "Oops! something wrong.Please try later.", "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
});
$("#btnCancel_City").click(function () {
    btnReset();
})

//here start function to Zip tab

function checkRequiredFieldZip() {
    var boolcheck = true;
    $('.reqz').each(function () {
        if ($(this).val() == '') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });
    if (boolcheck) {
        boolcheck = dynamicInputTypeSelect2('ddlreqz')
    } else {
        dynamicInputTypeSelect2('ddlreqz');
    }

    return boolcheck;
}
function bindActiveCity_Zip() {
    $.ajax({
        type: "POST",
        url: "WebAPI/ParameterSetting.asmx/bindActiveCity_Zip",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var StateDetail = $(xmldoc).find("Table");

            $("#ddlCity_Zip option:not(:first)").remove();
            $.each(StateDetail, function () {
                $("#ddlCity_Zip").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("CityName").text() + "</option>");
            });

            $("#ddlCitySearch_Zip option:not(:first)").remove();
            $.each(StateDetail, function () {
                $("#ddlCitySearch_Zip").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("CityName").text() + "</option>");
            });

            $("#ddlCity_Zip").select2()
            $("#ddlCitySearch_Zip").select2()

        },
        error: function (result) {
            swal("Error!", "Oops! something wrong.Please try later.", "error");
        },
        failure: function (result) {
            swal("Error!", "Oops! something wrong.Please try later.", "error");
        }
    });
}
function getZipDetail(pageIndex) {

    var data = {
        StateSearch: $('#ddlStateSearch_Zip').val(),
        CitySearch: $('#ddlCitySearch_Zip').val(),
        ZipSearch: $('#txtZipCodeSearch').val().trim(),
        Status: $('#ddlSStatus').val(),
        pageIndex: pageIndex
    }
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/ParameterSetting.asmx/getZipDetail",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: onSuccessOfZip,
        error: function (response) {
            swal("Error!", "Oops! something wrong.Please try later.", "error");
        }
    });
}
function onSuccessOfZip(response) {

    var xmldoc = $.parseXML(response.d);
    var category = $(xmldoc).find('Table1');

    if (category.length > 0) {
        $('#EmptyTable').hide();
        $('#tblZip tbody tr').remove();
        var row = $('#tblZip thead tr').clone(true);
        $.each(category, function () {

            $(".CId", row).text($(this).find("RowNumber").text());
            $(".CStateName", row).text($(this).find("StateName").text());
            $(".CCityName", row).text($(this).find("CityName").text());
            $(".CZip", row).text($(this).find("Zipcode").text());
            if ($(this).find("Status").text() == 'Active') {
                $(".Cstatus", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("Status").text() + "</span>");
            }
            else {
                $(".Cstatus", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("Status").text() + "</span>");
            }
            $(".Caction", row).html("<a href='#' title='Edit'><span class='ft-edit' onclick='editZip(\"" + $(this).find("AutoId").text() + "\")' /></a>&nbsp;&nbsp<a href='#' title='Delete'><span class='ft-x' onclick= 'deleteZip(\"" + $(this).find("AutoId").text() + "\")' /></a>");
            $("#tblZip tbody").append(row);
            row = $("#tblZip tbody tr:last-child").clone(true);
        });
    }
    else {
        $('#tblZip tbody tr').remove();
        $('#EmptyTable').show();
    }
    var pager = $(xmldoc).find("Table");
    $("#PagerZip").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
}
$("#btnSave_Zip").click(function () {
    if (checkRequiredFieldZip()) {
        var data = {
            ZState: $("#ddlState_Zip").val(),
            ZCity: $("#ddlCity_Zip").val(),
            ZZip: $("#txtZipCode_Zip").val(),
            ZStatus: $("#ddlStatus_Zip").val()
        }
        $.ajax({
            type: "POST",
            url: "WebAPI/ParameterSetting.asmx/insertData_Zip",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == "Success") {
                    swal("","Zip code details saved successfully.","success");
                        getZipDetail(1);
                        btnReset_Zip();
                }
                else if (result.d == "Unauthorized access.") {
                    location.href = "/";
                }
                else {
                    swal("Warning!", result.d, "error");
                }
            },
            error: function (result) {
                swal("Error!", "Oops! something wrong.Please try later.", "error");
            },
            failure: function (result) {
                swal("Error!", "Oops! something wrong.Please try later.", "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
});
$("#btnSearch_Zip").click(function () {
    getZipDetail(1);
})
$("#btnReset_Zip").click(function () {
    btnReset_Zip();
})
function btnReset_Zip() {
    $("#btnUpdate_Zip").hide();
    $("#btnCancel_Zip").hide();
    $("#btnSave_Zip").show();
    $("#btnReset_Zip").show();
    $("#ddlStatus_Zip").val(1);
    $("#txtZipCode_Zip").removeClass('border-warning');
    $("#ddlCity_Zip").closest('div').find('.select2-selection--single').removeAttr('style');
    $("#ddlState_Zip").closest('div').find('.select2-selection--single').removeAttr('style');
    $('input[type="text"]').val('');
    $('textarea').val('');
    $("#txtZipCode_Zip").val('');
    $("#ddlState_Zip").val(0).change();
    $("#ddlCity_Zip").val(0).change();
    $("#ddlStateSearch_Zip").val(0).change();
    $("#ddlCitySearch_Zip").val(0).change();
    $('option:selected').each(function () {
        $(this).attr('selected', false);
    });
    $('select option:first-child').each(function () {
        $(this).attr('selected', 'selected');
    });
}
function deleteZip(AutoId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this Zipcode",
        icon: "warning",
        showCancelButton: true,
        allowOutsideClick: false,
        closeOnClickOutside: false,
        buttons: {
            cancel: {
                text: "No, cancel.",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, delete it.",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (willdelete) {
        if (willdelete) {
            $.ajax({
                type: "POST",
                url: "/Admin/WebAPI/ParameterSetting.asmx/deleteZip",
                data: "{'AutoId':'" + AutoId + "'}",
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.d == "Success") {
                        swal({
                            title: "",
                            text: "Zip code details deleted successfully.",
                            icon: "success",
                        }).then(function () {
                            getZipDetail(1);
                        })
                    }
                    else if (response.d == "Unauthorized access.") {
                        location.href = "/";
                    }
                    else {
                        swal("", response.d, "error");
                    }
                },
                error: function (result) {
                    swal("", "Oops! something wrong.Please try later.", "error");
                },
                failure: function (result) {
                    swal("", "Oops! something wrong.Please try later.", "error");
                }
            });
        }
    })
}
function editZip(AutoId) {
    $.ajax({
        type: "POST",
        async: false,
        url: "/Admin/WebAPI/ParameterSetting.asmx/EditZipMaster",
        data: "{'AutoId':'" + AutoId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            var xmldoc = $.parseXML(response.d);
            var StateDetail = $(xmldoc).find("Table");
            $("#txtZipId").val($(StateDetail).find("ZipId").text());
            $("#ddlState_Zip").val($(StateDetail).find("StateId").text()).change();
            $("#ddlCity_Zip").val($(StateDetail).find("CityId").text()).change();
            $("#txtZipCode_Zip").val($(StateDetail).find("Zipcode").text()).change();
            $("#ddlStatus_Zip").val($(StateDetail).find("Status").text());
            btnShow_Zip();
        },
        error: function (result) {
            swal("Error!", "Oops! something wrong.Please try later.", "error");
        },
        failure: function (result) {
            swal("Error!", "Oops! something wrong.Please try later.", "error");
        }
    });
}
function btnShow_Zip() {
    $("#btnSave_Zip").hide();
    $("#btnReset_Zip").hide();
    $("#btnUpdate_Zip").show();
    $("#btnCancel_Zip").show();
}
$("#btnUpdate_Zip").click(function () {
    if (checkRequiredFieldZip()) {
        var data = {
            ZZipId: $("#txtZipId").val(),
            ZStateId: $("#ddlState_Zip").val(),
            ZCityId: $("#ddlCity_Zip").val(),
            ZZipCode: $("#txtZipCode_Zip").val(),
            ZStatus: $("#ddlStatus_Zip").val()
        }
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/ParameterSetting.asmx/UpdateZipMaster",
            data: "{'datavalue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {

                if (result.d == "Success") {
                    swal({
                        title: " ",
                        text: "Zip code details updated successfully.",
                        icon: "success",
                    }).then(function () {
                        getZipDetail(1)
                        btnReset_Zip();
                    })
                }
                else if (result.d == "Unauthorized access.") {
                    location.href = "/";
                }
                else {
                    swal("Error!", result.d, "error");
                }
            },
            error: function (result) {
                swal("Error!", "Oops! something wrong.Please try later.", "error");
            },
            failure: function (result) {
                swal("Error!", "Oops! something wrong.Please try later.", "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
});
$("#btnCancel_Zip").click(function () {
    btnReset_Zip();
});

function bindSubcategory() {
    //debugger;
    var categoryAutoId = $("#ddlState_Zip").val();
    //console.log(categoryAutoId.d);
    $.ajax({
        type: "POST",
        async: false,
        url: "/Admin/WebAPI/ParameterSetting.asmx/binedState_Zip",
        data: "{'categoryAutoId':'" + categoryAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successBindSubcategory,
        error: function (result) {
            swal("Error!", "Oops! something wrong.Please try later.", "error");
        },
        failure: function (result) {
            swal("Error!", "Oops! something wrong.Please try later.", "error");
        }
    });
}
function successBindSubcategory(response) {

    if (response.d != "Session Expired") {
        var xmldoc = $.parseXML(response.d);
        var subcategory = $(xmldoc).find("Table");

        if (subcategory.length > 0) {
            $('#ddlCity_Zip').attr('disabled', false);
            $("#ddlCity_Zip option").remove();
            $("#ddlCity_Zip").append("<option value='0'>-Select-</option>");
            $.each(subcategory, function () {
                $("#ddlCity_Zip").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("CityName").text() + "</option>");
            });
        } else {
            $('#ddlCity_Zip').attr('disabled', 'disabled');
            $('#ddlCity_Zip option:not(:first)').remove();
        }
    } else {
        location.href = '/';
    }

}
function SbindSubcategory() {
    var categoryAutoId = $("#ddlStateSearch_Zip").val();
    // console.log(categoryAutoId)
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/ParameterSetting.asmx/binedState_Zip",
        data: "{'categoryAutoId':'" + categoryAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successBindSubcategoryS,
        error: function (result) {
            swal("Error!", "Oops! something wrong.Please try later.", "error");
        },
        failure: function (result) {
            swal("Error!", "Oops! something wrong.Please try later.", "error");
        }
    });
}
function successBindSubcategoryS(response) {
    console.log(response);
    if (response.d != "Session Expired") {
        var xmldoc = $.parseXML(response.d);
        var subcategory = $(xmldoc).find("Table");

        if (subcategory.length > 0) {
            $("#ddlCitySearch_Zip option").remove();
            $("#ddlCitySearch_Zip").append("<option value='0'>ALL City</option>");
            $.each(subcategory, function () {
                $("#ddlCitySearch_Zip").append("<option value=' " + $(this).find("AutoId").text() + "'>" + $(this).find("CityName").text() + "</option>");
            });
        } else {
            $('#ddlCitySearch_Zip option:not(:first)').remove();
        }
    } else {
        location.href = '/';
    }
}

//$('#txtCity_City').bind('keypress', function (e) {
//    if ($('#txtCity_City').val().length == 0) {
//        var k = e.which;
//        var ok = k >= 65 && k <= 90 || // A-Z
//            k >= 97 && k <= 122 || // a-z
//            k >= 48 && k <= 57; // 0-9

//        if (!ok) {
//            e.preventDefault();
//        }
//    }
//}); 

