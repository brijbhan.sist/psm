﻿$(document).ready(function () {
    resetCommission();
    GetommissionDetail();
    $("#btnUpdate").hide();
    $("#btnCancel").hide();
    
})
/*------------------------------------------------------Insert Currency------------------------------------------------------------*/
function Save() {
    if (checkRequiredField1()) { 
        var XmlCommission;
        XmlCommission = '<XmlCommission>';
        XmlCommission += '<CommissionCode><![CDATA[' + $("#txtCommissionCode").val() + ']]></CommissionCode>';
        XmlCommission += '<Description><![CDATA[' + $("#txtDescription").val() + ']]></Description>';
        XmlCommission += '<Status><![CDATA[' + $("#ddlStatus").val() + ']]></Status>';
        XmlCommission += '</XmlCommission>';
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/WCommissionMaster.asmx/InsertCommission",
            data: JSON.stringify({ dataValue: JSON.stringify(XmlCommission) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () { 
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>', 
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if(result.d=="Success")
                {
                    swal("", "Commission code saved successfully.", "success");
                    var table = $('#tblCommission').DataTable();
                    table.destroy();
                    resetCommission();
                    GetommissionDetail();
                }
                else if(result.d=="Unauthorized access.")
                {
                    location.href="/";
                }
                else
                {
                    swal("Error!", result.d, "error");
                }
            },
            error: function (result) {
                swal("Error!", "Commission code already exists.", "error");
               
            },
            failure: function (result) {
                swal("Error!", result.d, "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
};

/*----------------------------------------------------List Categroy Details--------------------------------------------------------*/
function GetommissionDetail() {
   
    var data = {
       
    };
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WCommissionMaster.asmx/getCommissionDetail",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        
            if ($.fn.DataTable.isDataTable('#tblCommission')) {
                $('#tblCommission').DataTable().destroy();
            }
            $('#tblCommission').DataTable(
               {
                   "paging": true,
                   "ordering": true,
                   "info": true,
                   "searching": false,
                   "sorting":false,
                   "aoColumns": [
                       { "bSortable": false },
                       { "bSortable": false },
                       { "bSortable": false },
                       { "bSortable": false },
                       { "bSortable": false }
                      
                   ]
               });
            
        },
        success: onSuccessOfCurrency,
        error: function (response) {
            console.log(JSON.parse(response.responseText).d);
        }
    });
}
function onSuccessOfCurrency(response) {
    var xmldoc = $.parseXML(response.d);
    var CurrencyDetails = $(xmldoc).find('Table');

    if (CurrencyDetails.length > 0) {
        $('#EmptyTable').hide();
        $('#tblCommission tbody tr').remove();
        var row = $('#tblCommission thead tr').clone(true);
        $.each(CurrencyDetails, function () {
            $(".CommissionId", row).text($(this).find("AutoId").text());
            $(".CommissionCode", row).text($(this).find("CommissionCode").text());
            $(".Description", row).text($(this).find("Description").text());
            
            if(($(this).find("Status").text())=='Active')
            {
                $(".status", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("Status").text() + "</span>");
            }
            if(($(this).find("Status").text())=='Inactive')
            {
                $(".status", row).html("<span class='badge badge badge-pill  badge-danger'>" + $(this).find("Status").text() + "</span>");
            }
            $(".action", row).html("<a title='Edit' href='#'><span class='ft-edit' onclick='editCommission(\"" + $(this).find("AutoId").text() + "\")' />&nbsp;&nbsp;</a><a title='Delete' href='#'><span class='ft-x' onclick= 'deleterecord(\"" + $(this).find("AutoId").text() + "\")' /></a>");
            $("#tblCommission tbody").append(row);
            row = $("#tblCommission tbody tr:last-child").clone(true);
        });
    }
    else {
        $('#tblCommission tbody tr').remove();
        $('#EmptyTable').show();
    }

}

/*----------------------------------------------------Edit CurrencyDetails Detail---------------------------------------------------*/
function editCommission(CommissionId) {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WCommissionMaster.asmx/editCommission",
        data: "{'CommissionId':'" + CommissionId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: onSuccessOfEdit,
        error: function (response) {
            console.log(response.d)
        }
    })
}
function onSuccessOfEdit(response) {
    var xmldoc = $.parseXML(response.d);
    var CurrencyDetails = $(xmldoc).find('Table');
    $("#txtCommId").val($(CurrencyDetails).find("AutoId").text());
    $("#txtCommissionCode").val($(CurrencyDetails).find("CommissionCode").text());
    $("#txtDescription").val($(CurrencyDetails).find("Description").text());
    $("#ddlStatus").val($(CurrencyDetails).find("Status").text());
    $("#btnSave").hide();
    $("#btnReset").hide();
    $("#btnUpdate").show();
    $("#btnCancel").show();
}
/*----------------------------------------------------Update Currency Detail--------------------------------------------------------*/

function Update() {
    if (checkRequiredField1()) {

        var XmlCommission;
        XmlCommission = '<XmlCommission>';
        XmlCommission += '<Autoid><![CDATA[' + $("#txtCommId").val() + ']]></Autoid>';
        XmlCommission += '<CommissionCode><![CDATA[' + $("#txtCommissionCode").val() + ']]></CommissionCode>';
        XmlCommission += '<Description><![CDATA[' + $("#txtDescription").val() + ']]></Description>';
        XmlCommission += '<Status><![CDATA[' + $("#ddlStatus").val() + ']]></Status>';
        XmlCommission += '</XmlCommission>';
    
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/WCommissionMaster.asmx/UpdateCommission",
            data: JSON.stringify({ dataValue: JSON.stringify(XmlCommission) }),
            contentType: "application/json;charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if(result.d=="Success")
                {
                    swal("", "Commission details updated successfully.", "success");
                    var table = $('#tblCommission').DataTable();
                    table.destroy();
                    resetCommission();
                    GetommissionDetail();
                }
                else  if(result.d=="Unauthorized access.")
                {
                    location.href="/";
                }
                else
                {
                    swal("Error!", result.d, "error");
                }              
            },
            error: function (result) {
                swal("Error!", result.d, "error");
            },
            failure: function (result) {
                swal("Error!", result.d, "error");
            }
        });
    }
    else {
        toastr.error('Focus * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
};


function resetCommission() {
    $("#btnUpdate").hide();
    $("#btnCancel").hide();
    $("#btnSave").show();
    $("#btnReset").show();
    $("#ddlStatus").val('1');
    $("#txtCommissionCode").removeClass('border-warning');
    $('input[type="text"]').val('');
    $('textarea').val('');
    $('option:selected').each(function () {
        $(this).attr('selected', false);
    });
    $('select option:first-child').each(function () {
        $(this).attr('selected', 'selected');
    });
}
function Cancel() {
    resetCommission();
    $("#btnUpdate").hide();
    $("#btnCancel").hide();
    $("#btnSave").show();
    $("#btnReset").show();
};
/*--------------------------------------------------------Delete Currency Details ----------------------------------------------------------*/
function deleteCommission(CommissionId) {
   
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WCommissionMaster.asmx/deleteCommission",
        data: "{'CommissionId':'" + CommissionId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () { 
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>', 
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (result) {
            if(result.d=="Success")
            {
                swal("", "Commission details deleted successfully.", "success");
                var table = $('#tblCommission').DataTable();
                table.destroy();
                GetommissionDetail();
                resetCommission();
            }
            else if(result.d=="Unauthorized access.")
            {
                location.href="/";
            }
            else
            {
                swal("error", result.d, "error");
            }           
        },
        error: function (result) {
            swal("error", result.d, "error");
                
        },
        failure: function (result) {
            swal("error", result.d, "error");
        }
    })   
}
function deleterecord(CommissionId) {
    swal({
        title: "Are you sure?",
        text: "you want to delete this commission details.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: false,
            },
            confirm: {
                text: "Yes, delete it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(isConfirm => {
        if (isConfirm) {
             deleteCommission(CommissionId);
            
} else {
             swal("", "Your commission code is safe.", "error");
}
})
}

function checkRequiredField1() {
    var boolcheck = true;
    $('.req').each(function () {
        if ($(this).val().trim() == '') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    }); 
    return boolcheck;
}