﻿$(document).ready(function () {

    getProductList(1);
});
function Pagevalue(e) {
    getProductList(parseInt($(e).attr("page")));
};

function getProductList(PageIndex) {
    var data = {
        ProductName: '',
        Unit: '',
        PageSize : $('#ddlPageSize').val(),
        PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WrongProductPrice.asmx/getProductList",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (result) {
            console.log(result);
           var xmldoc = $.parseXML(result.d);

            var pages = $(xmldoc).find('Table1');
            $("#tblProductList tbody tr").remove();
            var row = $("#tblProductList thead tr").clone(true);

            if (pages.length > 0) {
                $.each(pages, function () {
                    $(".ProductId", row).html("<a target='_blank' href='/Admin/productMaster.aspx?ProductId=" + $(this).find("ProductAutoId").text()+"'>"+$(this).find("ProductId").text()+"</a>");
                    $(".ProductName", row).text($(this).find("ProductName").text());
                    $(".UnitType", row).text($(this).find("UnitType").text());
                    $(".Qty", row).text($(this).find("Qty").text());
                    $(".MinPrice", row).text($(this).find("MinPrice").text());
                    $(".CostPrice", row).text($(this).find("CostPrice").text());
                    $(".Price", row).text($(this).find("Price").text());
                    $(".SRP", row).text($(this).find("SRP").text());
                    $(".WHminPrice", row).text($(this).find("WHminPrice").text());
                    $(".LastDate", row).text($(this).find("LastDate").text());
                    $("#tblProductList tbody").append(row);
                    row = $("#tblProductList tbody tr:last-child").clone(true);
                });
            }
            var pager = $(xmldoc).find("Table");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
        },
        error: function (result) {

        },

    });

   


}
