﻿$(document).ready(function () {
    $("#TxtRate").attr("disabled", true);
    $("#txtPrintLabel").attr("disabled", true);
    BindTaxDetails();
    $("#btnUpdate").hide();
    $("#btnCancel").hide();
})
/*------------------------------------------------------Insert Categroy------------------------------------------------------------*/
function checkRequiredField() {
    var boolcheck = true;
    $('.req').each(function () {
        if ($(this).val() == '') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });
    if (boolcheck) {
        boolcheck = dynamicInputTypeSelect2('ddlreq')
    } else {
        dynamicInputTypeSelect2('ddlreq');
    }

    return boolcheck;
}
$("#btnEdit").click(function(){
    $("#TxtRate").attr("disabled", false);
    $("#txtPrintLabel").attr("disabled", false);
    $("#btnEdit").hide();
    $("#btnUpdate").show();
})

$("#btnReset").click(function () {
    resetTax();
});
/*----------------------------------------------------List Categroy Details--------------------------------------------------------*/
function getTaxDetail() { 
    var data = {
        TaxId: $('#txtSTaxId').val(),
        TaxState: $('#ddlTaxState').val(),
        Status: $('#ddlSStatus').val()
    };
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WMLTaxMaster.asmx/getTaxDetail",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
            if ($.fn.DataTable.isDataTable('#tblTax')) {
                $('#tblTax').DataTable().destroy();
            }
        },
        complete: function () {            
           
            $('#tblTax').DataTable(
               {
                   "paging": false,
                   "ordering": false,
                   "info": true,
                   "searching": false,
                   "sorting":false,
                   "aoColumns": [
                       { "bSortable": false },
                       { "bSortable": true },
                       { "bSortable": true },
                       { "bSortable": false },
                       { "bSortable": false },
                       { "bSortable": false }
                   ]
               });
        },
        success: onSuccessOfTax,
        error: function (response) {
            console.log(JSON.parse(response.responseText).d);
        }
    });
}
function onSuccessOfTax(response) {
    var xmldoc = $.parseXML(response.d);
    var Tax = $(xmldoc).find('Table');

    if (Tax.length > 0) {
        $('#EmptyTable').hide();
        $('#tblTax tbody tr').remove();
        var row = $('#tblTax thead tr').clone(true);
        $.each(Tax, function () {
            $(".TaxId", row).text($(this).find("AutoId").text());
            $(".StateName", row).text($(this).find("StateName").text());
            $(".Value", row).text($(this).find("TaxRate").text());
            $(".status", row).text($(this).find("Status").text());
            if($(this).find("Status").text()=='Active')
            {
                $(".status", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("Status").text() + "</span>");
            }else
            {     $(".status", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("Status").text() + "</span>");

            }
            $(".action", row).html("<a href='#' title='Edit'><span class='ft-edit' onclick='editTax(\"" + $(this).find("AutoId").text() + "\")' /></a>&nbsp;<a href='#' title='Delete'><span class='ft-x' onclick= 'deleterecord(\"" + $(this).find("AutoId").text() + "\")' /></a>");
            $("#tblTax tbody").append(row);
            row = $("#tblTax tbody tr:last-child").clone(true);
        });
    }
    else {
        $('#tblTax tbody tr').remove();
        $('#EmptyTable').show();
    }
}
/*----------------------------------------------------Search Engine----------------------------------------------------------*/
$("#btnSearch").click(function () {
    debugger;
    getTaxDetail();
});
$("#btnSearch").click(function(){
    getTaxDetail();
})
/*----------------------------------------------------Edit Tax Detail---------------------------------------------------*/
function editTax(TaxId) {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WMLTaxMaster.asmx/editTax",
        data: "{'TaxId':'" + TaxId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
       
        success: onSuccessOfEdit,
        error: function (response) {
            console.log(response.d)
        }
    })
}
function onSuccessOfEdit(response) {
    var xmldoc = $.parseXML(response.d);
    var Tax = $(xmldoc).find('Table');
    $("#txtTaxId").val($(Tax).find("AutoId").text());
    $("#TxtRate").val($(Tax).find("TaxRate").text());
    $("#ddlStatus").val($(Tax).find("Status").text());
    $("#txtPrintLabel").val($(Tax).find("PrintLabel").text());
    $("#ddlState").val($(Tax).find("TaxState").text()).change();
    $("#btnSave").hide();
    $("#btnReset").hide();
    $("#btnUpdate").show();
    $("#btnCancel").show();
}
$("#btnCancel").click(function () {
    resetTax();
    $("#btnUpdate").hide();
    $("#btnCancel").hide();
    $("#btnSave").show();
    $("#btnReset").show();
});

/*----------------------------------------------------Update Tax Detail--------------------------------------------------------*/

$("#btnUpdate").click(function () {
    if (checkRequiredField()) {
        var data = {
            TaxId: $("#txtTaxId").val(),
            Value: $("#TxtRate").val(),
            PrintLabel: $("#txtPrintLabel").val(),
        };
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/WMLTaxMaster.asmx/updateWeightTax",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json;charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    timeout: 2000, //unblock after 2 seconds
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
           
            success: function (result) {
                if(result.d=="Unauthorized access.")
                {
                    location.href="/";
                }
                else if(result.d=="Success")
                {
                    swal("", "Tax details updated successfully.", "success");
                    var table = $('#tblTax').DataTable();
                    table.destroy();
                    BindTaxDetails();
                    resetTax();
                }
                else
                {
                    swal("Error!", result.d, "error");
                }
            },
            error: function (result) {
                swal("Error!", result.d, "error");
            },
            failure: function (result) {
                swal("Error!", result.d, "error");
            }
        });
    }else{
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
});


/*-----------------------------------------------------------Reset------------------------------------------------------------------*/
function resetTax() {
    $("#btnUpdate").hide();
    $("#btnCancel").hide();
    $("#btnEdit").show();
    $("#btnReset").show();
  
    $('.req').removeClass('border-warning');
    $('.ddlreq').removeClass('border-warning');
    $('.select2-selection').removeAttr('style');
    $("#TxtRate").attr("disabled", true);
    $("#txtPrintLabel").attr("disabled", true);
}

function BindTaxDetails() {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WMLTaxMaster.asmx/BindTaxDetails",
        data: "{}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
      
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var TaxDetails = $(xmldoc).find("Table");
            $("#txtTaxId").val($(TaxDetails).find("ID").text());
            $("#TxtRate").val($(TaxDetails).find("Value").text());
            $("#txtPrintLabel").val($(TaxDetails).find("PrintLabel").text());
        },
        error: function (response) {
            console.log(JSON.parse(response.responseText).d);
        }
    });
}


function deleterecord(TaxId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this Tax.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, cancel.",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: false,
            },
            confirm: {
                text: "Yes, delete it.",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(isConfirm => {
        if (isConfirm) {
             deleteTax(TaxId);
            
} else {
             swal("", "Your Tax is safe.", "error");
}
})
}


function dynamicInputTypeSelect2(className) {
    var boolcheck = true;
    $('.' + className).each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == 'undefined' || $(this).val() == null ||  $(this).val() == 'Select') {
            boolcheck = false;
            $(this).closest('div').find('.select2-selection--single').attr('style', 'border:1px solid #FF9149  !important');
        } else {
            $(this).removeClass('border-warning');
            $(this).closest('div').find('.select2-selection--single').removeAttr('style');
        }
    });
    return boolcheck;
}