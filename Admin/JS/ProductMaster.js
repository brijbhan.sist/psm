﻿var Imgresponse = [];

$(document).ready(function () {


    if ($('#Hd_Domain').val() == 'psmnjm') {
        $('#txtProductId').removeAttr('onkeypress');
    }
    domain = $(location).attr('hostname');
    $('#btnManagePrince').attr('disabled', false);
    var getId = getProductId("ProductId");
    if (getId != '') {
        editProduct(getId);
        Bindlocation(getId)
        $("#panelPacking *").attr('disabled', false);
        $('#btnManagePrince').attr('disabled', false);
    }
    else {
        bindCategory();
        Bindlocation(getId)
        $("#panelPacking *").attr('disabled', true);
        $('#btnManagePrince').attr('disabled', true);
    }
    $("#emptyTable").show();//$('#Hd_Domain').val().toLowerCase() != "psmnj" &&
    if ($('#Hd_Domain').val().toLowerCase() != "psmnj" && $('#Hd_Domain').val().toLowerCase() != "psmnjm" && $('#Hd_Domain').val().toLowerCase() != "psm" && $('#Hd_Domain').val().toLowerCase() != "demo") {
        $('#ddlBrand').attr('disabled', true);
        $('#imgUpdate').hide();
        $("#Imagesec").hide();
    } else {
        $('#ddlBrand').attr('disabled', false);
        $('#imgUpdate').show();
        $("#Imagesec").show();
    }
});
function readURL(input) {
    file = input.files[0];
    if ($(input).val() == '') {
        $("#imgProductImage").attr("src", "/images/default_pic.png");
    }
    if (Number(file.size / (1024 * 1024)) > Number($("#getSize").html())) {
        toastr.error('You are reaching maximum limit. Max limit is ' + $("#getSize").html() + ' MB', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $("#fileImageUrl").val('');
        $("#imgProductImage").attr("src", "/images/default_pic.png");
        return true;
    }
    else {
        if (input.files && input.files[0] && input.files[0].name.match(/\.(jpg|JPG|JPEG|jpeg|png|PNG)$/)) {
            var reader = new FileReader();
            reader.onload = function (e) {
                document.getElementById('imgProductImage').src = e.target.result;
                $('#imgProductImage').attr('mainsrc', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
        else {
            toastr.error('Invalid file. Allowed file type are png,jpg,jpeg.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            $("#fileImageUrl").val('');
        }
    }
}
function getProductId(args) {
    var url = window.location.href.slice(window.location.href.indexOf("?") + 1).split();
    var productId = "";
    $(url).each(function () {
        var arr = this.split("=");
        if (arr[0] == args) {
            productId = arr[1];
        }
    });
    return productId;
}
function Bindlocation(productId) {
    $.ajax({
        type: "POST",
        url: "productMaster.aspx/bindLocation",
        data: "{'AutoId':'" + productId + "'}",
        contentType: "application/json",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            $("#bindcheckbox").html('');
            var getData = $.parseJSON(response.d);
            var html = '', count = 0;
            var html = '<div class="form-grop row" style="margin-left: 2px;">';
            $.each(getData, function (index, item) {
              
                if (getData[index].Status == 1 && getData[index].DbLoction == 1) {
                    html += '<input type="checkbox" class="cbLocation"  checked="true" name="Location" style="margin: 4px;"  value= "' + getData[index].AutoId + '">';
                }
                else if (getData[index].Status == 0 && getData[index].DbLoction == 1) {
                    html += '<input type="checkbox" class="cbLocation"   name="Location" style="margin: 4px;"  value= "' + getData[index].AutoId + '">';
                }
                else if (getData[index].Status == 1 && getData[index].DbLoction == 0) {
                    html += '<input type="checkbox" class="cbLocation"  disabled="disabled"  checked="true"  name="Location" style="margin: 4px;"  value= "' + getData[index].AutoId + '">';
                }
                else if (productId == "") {
                    html += '<input type="checkbox"  class="cbLocation"    name="Location" style="margin: 4px;"  value= "' + getData[index].AutoId + '">';
                }
                else {
                    html += '<input type="checkbox" class="cbLocation"   disabled="disabled"  name="Location" style="margin: 4px;"  value= "' + getData[index].AutoId + '">';
                }
                html += '<label name="' + getData[index].Location + '" class="LocationId" style="font-size: 18px;font-weight: 600;margin-right:13px;display: block;width: 78px">' + getData[index].Location + '</label>';

            });
            html += '</div>'
            $("#bindcheckbox").append(html);
        }
    });
}
/*---------------------------------------------------Bind Category-----------------------------------------------------------*/
function bindCategory() {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/productMaster.asmx/bindCategory",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successBindCategory,
        error: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        },
        failure: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        }
    });
}
function successBindCategory(response) {
    if (response.d != "Session Expired") {
        var xmldoc = $.parseXML(response.d);
        var category = $(xmldoc).find("Table");

        $("#ddlCategory option").remove();
        $("#ddlCategory").append("<option value='0'>Choose a Category</option>");
        $.each(category, function () {
            $("#ddlCategory").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("CategoryName").text() + "</option>");
        });
        var VendorList = $(xmldoc).find("Table1");

        $("#ddlVendor option").remove();
        $("#ddlVendor").append("<option value='0'>Choose an Vendor</option>");
        $.each(VendorList, function () {
            $("#ddlVendor").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("VendorName").text() + "</option>");
        });
        var BrandList = $(xmldoc).find("Table2");

        $("#ddlBrand option").remove();
        $("#ddlBrand").append("<option value='0'>Choose an Brand</option>");
        $.each(BrandList, function () {
            $("#ddlBrand").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("BrandName").text() + "</option>");
        });

        var CommissionCode = $(xmldoc).find("Table3");

        $("#ddlCommCode option").remove();
        $("#ddlCommCode").append("<option value=''>Choose a  Commission Code</option>");
        $.each(CommissionCode, function () {
            $("#ddlCommCode").append("<option value='" + $(this).find("CommissionCode").text() + "'>" + $(this).find("C_DisplayName").text() + "</option>");
        });
    } else if (response.d == "false") {
        swal("", "Oops! Something went wrong.Please try later.", "error")
    }
    else {
        location.href = '/';
    }

}
/*-------------------------------------------------------Bind Subcategory-----------------------------------------------------------*/
function bindSubcategory() {
    var categoryAutoId = $("#ddlCategory").val();
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/productMaster.asmx/bindSubcategory",
        data: "{'categoryAutoId':'" + categoryAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successBindSubcategory,
        error: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        },
        failure: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        }
    });
}
function successBindSubcategory(response) {
    if (response.d != "Session Expired") {
        var xmldoc = $.parseXML(response.d);
        var subcategory = $(xmldoc).find("Table");

        if (subcategory.length > 0) {
            $('#ddlSubcategory').attr('disabled', false);
            $("#ddlSubcategory option").remove();
            $("#ddlSubcategory").append("<option value='0'>-Select-</option>");
            $.each(subcategory, function () {
                $("#ddlSubcategory").append("<option value=' " + $(this).find("AutoId").text() + "'>" + $(this).find("SubcategoryName").text() + "</option>");
            });
        } else {
            $('#ddlSubcategory').attr('disabled', 'disabled');
            $('#ddlSubcategory option:not(:first)').remove();
        }
    } else if (response.d == "false") {
        swal("", "Oops! Something went wrong.Please try later.", "error")
    }
    else {
        location.href = '/';
    }

}
/*-------------------------------------------------------Bind UnitType-----------------------------------------------------------*/
function bindUnitType() {

    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/productMaster.asmx/bindUnitType",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (result) {
            if (result.d != "Session Expired") {
                var xmldoc = $.parseXML(result.d);
                var unitType = $(xmldoc).find("Table");
                $("#ddlUnitType option").remove();
                $("#ddlUnitType").append("<option value='0'>-Select-</option>");
                $.each(unitType, function () {
                    $("#ddlUnitType").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("UnitType").text() + "</option>");
                });
            } else {
                location.href = '/';
            }
        },
        error: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        },
        failure: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        }
    });
}
$("#ddlUnitType").change(function () {
    if ($("#ddlUnitType option:selected").text() == "Piece") {
        $("#txtQty").val("1");
        $("#txtQty").attr("readonly", true);
    } else {
        $("#txtQty").val("");
        $("#txtQty").attr("readonly", false);
    }
})

function FillTax() {
    if ($("#ddlProductType").val() == 0) {
        $("#txtTaxRate").val('0.00').attr('disabled', true);
    } else {
        $("#txtTaxRate").val('').attr('disabled', false);
    }
}
/*------------------------------------------------------Insert Product------------------------------------------------------------*/
function Saveproduct() { 
    var imgname = $("#fileImageUrl").val();

    var extension = imgname.substr((imgname.lastIndexOf('.') + 1));
    if ($("#fileImageUrl").val() != '') {
        imgname = $("#txtProductId").val() + '.' + extension;
    }
    else {

        imgname = "default_pic.png";
    }
    var IsApplyMLQty = 0, IsApplyWeightQuantity = 0, IsShowOnWeb = 0;

    if ($('#cbMlQuantity').prop('checked') === true) {
        IsApplyMLQty = 1
    }
    if ($('#CbWeightQuantity').prop('checked') === true) {
        IsApplyWeightQuantity = 1
    }
    //if ($('#cbShowOnWebsite').prop('checked') === true) {
    //    IsShowOnWeb = 1
    //}

    var commCodeCheck = $("#ddlCommCode").val();
    if (commCodeCheck == '') {
        $("#ddlCommCode").addClass("req");
    } else {
        $("#ddlCommCode").removeClass("req");
        $("#ddlCommCode").removeClass("border-warning");
    }
    if (checkrequiredProduct()) {
        if (imgname != '' || imgname != 'default_pic.png') {
            uploadImage($("#txtProductId").val());
        }
        Lstatus = new Array();
        $("input:checkbox[name=Location]").each(function (index, item) {
            Lstatus[index] = new Object();
            if ($(this).is(':checked')) {
                Lstatus[index].status = 1;
            }
            else {
                Lstatus[index].status = 0;
            }
            Lstatus[index].LocationId = $(item).attr('value');
        });
        var data = {
            IsApplyMlQty: IsApplyMLQty,
            IsApplyWeightQty: IsApplyWeightQuantity,
            CategoryAutoId: $("#ddlCategory").val(),
            SubcategoryAutoId: $("#ddlSubcategory").val(),
            ProductId: $("#txtProductId").val(),
            ProductName: $("#txtProductName").val().trim(),
            ReOrderMark: $("#txtReOrderMark").val(),
            MLQty: $("#txtMLQty").val(),
            Weight: $("#txtWeightOz").val(),
            ImageUrl: imgname,
            ThumbnailImageUrl: "Thumbnail_" + imgname,
            ThumbnailImageUrl1000: "Thumbnail_" + imgname,
            CommCode: $("#ddlCommCode").val(),
            SRP: $("#txtSRP").val() || '0',
            VendorAutoId: $("#ddlVendor").val(),
            BrandAutoId: $("#ddlBrand").val(),
        }
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/productMaster.asmx/insertProduct",
            data: JSON.stringify({ dataValue: JSON.stringify(data), Locationstatus: JSON.stringify(Lstatus) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                
                if (response.d == "false") {
                    swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                    return;
                }
                else if (response.d == 'ImageIssue') {
                    swal("", "Oops! Image not uploaded. Please try again.", "error");
                    return;
                }
                if (response.d != "Session Expired") {
                    if (response.d == 'Product ID already exists.') {
                        swal("", "Product ID already exist", "error");

                    } else if (response.d == 'Product name already exists.') {
                        swal("", "Product name already exist.", "error");
                    }
                    else {
                        var xmldoc = $.parseXML(response.d);
                        var product = $(xmldoc).find("Table");
                        swal("", "Product details saved successfully.", "success").then(function () {
                            window.location.href = "/Admin/productMaster.aspx?ProductId=" + $(product).find("ProductId").text();
                        });
                        $("#panelPacking *").attr('disabled', false);
                        bindUnitType();
                    }
                } else {
                    location.href = '/';
                }
            },
            error: function (result) {
                swal("Error!", "Product details not saved", "error");
            },
            failure: function (result) {
                swal("Error!", 'Oops,something went wrong.Please try again.', "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
function checkrequiredProduct() {
    var boolcheck = true;
    $('.req').each(function () {
        if ($(this).val().trim() == ''|| $(this).val().trim() == '.') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else if (parseFloat($(this).val()) <= 0) {
            boolcheck = false;
            $(this).addClass('border-warning');
        }
        else {
            $(this).removeClass('border-warning');
        }
    });
    $('.ddlreq').each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == 'undefined' || $(this).val() == null) {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });
    return boolcheck;
}

function checkrequiredUnit() {
    var boolcheck = true;
    $('.ureq').each(function () {
        if ($(this).val() == '') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else if (parseInt($(this).val()) == 0) {
            boolcheck = false;
            $(this).addClass('border-warning');
        }
        else {
            $(this).removeClass('border-warning');
        }
    });
    $('.uddlreq').each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == 'undefined' || $(this).val() == null) {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });
    return boolcheck;
}
function UpdateProduct() {
    var imgname = $("#fileImageUrl").val();
    var extension = imgname.substr((imgname.lastIndexOf('.') + 1));
    if ($("#fileImageUrl").val() != '') {
        imgname = $("#txtProductId").val() + '.' + extension;
    }
    else {
        if ($("#imgProductImage").attr("mainsrc") != undefined) {
            imgname = $("#imgProductImage").attr("mainsrc").split("/")[2] || "";
        }
    }
    var IsApplyMLQty = 0, IsApplyWeightQuantity = 0, IsShowOnWeb = 0;

    if ($('#cbMlQuantity').prop('checked') === true) {
        IsApplyMLQty = 1
    }
    if ($('#CbWeightQuantity').prop('checked') === true) {
        IsApplyWeightQuantity = 1
    }
    if (checkrequiredProduct()) {
        if (imgname != '' || imgname != 'default_pic.png') {
            uploadImage($("#txtProductId").val());
        }
        Lstatus = new Array();
        $("input:checkbox[name=Location]").each(function (index, item) {
            Lstatus[index] = new Object();
            if ($(this).is(':checked')) {
                Lstatus[index].status = 1;
            }
            else {
                Lstatus[index].status = 0;
            }
            Lstatus[index].LocationId = $(item).attr('value');
        });
        var data = {
            IsApplyMlQty: IsApplyMLQty,
            IsApplyWeightQty: IsApplyWeightQuantity,
            ProductAutoId: $("#txtHProductAutoId").val(),
            CategoryAutoId: $("#ddlCategory").val(),
            SubcategoryAutoId: $("#ddlSubcategory").val(),
            ProductId: $("#txtProductId").val(),
            ProductName: $("#txtProductName").val().trim(),
            ReOrderMark:$("#txtReOrderMark").val(),
            QtyPerPiece: $("#reQty").val(),
            MLQty: $("#txtMLQty").val(),
            WeightOz: $("#txtWeightOz").val(),
            ImageUrl: imgname,
            ThumbnailImageUrl: "Thumbnail_" + imgname,
            ThumbnailImageUrl1000: "Thumbnail_" + imgname,
            CommCode: $("#ddlCommCode").val(),
            SRP: $("#txtSRP").val() || '0',
            VendorAutoId: $("#ddlVendor").val(),
            BrandAutoId: $("#ddlBrand").val()
        };

        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/productMaster.asmx/updateProduct",
            data: JSON.stringify({ dataValue: JSON.stringify(data), Locationstatus: JSON.stringify(Lstatus) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d != "Session Expired") {
                    if (response.d == 'ImageIssue') {
                        swal("", "Oops! Image not uploaded. Please try again.", "error");
                        return;
                    }
                    else if (response.d != 'Success') {
                        swal("Error!", response.d, "error");
                        return;
                    }

                    else {
                        swal("", "Product details updated successfully.", "success");
                        editProduct($("#txtHProductAutoId").val());
                        setTimeout(function () {

                        }, 3000);
                       
                    }
                } else {
                    location.href = '/';
                }

            },
            error: function (result) {
                swal("", "Product details not updated", "error");
            },
            failure: function (result) {
                swal("", 'Oops,some thing went wrong.Please try again.', "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
function uploadImage(ProductId) {

    var fileUpload = $("#fileImageUrl").get(0);
    var files = fileUpload.files;
    var test = new FormData();
    for (var i = 0; i < files.length; i++) {
        test.append(files[i].name, files[i]);
    }
    $.ajax({
        url: "/FileUploadHandler.ashx?timestamp=" + ProductId,
        type: "POST",
        async: false,
        contentType: false,
        processData: false,
        data: test,
        success: function (result) {
            return;
        }
    })
}
function resetButton() {
    resetProduct();
    $("#panelPacking *").attr('disabled', 'disabled');
    $(".alert").hide();
    $(".cbLocation").prop('checkd', false);
    Bindlocation(0);
}
/*------------------------------------------------------Insert Packing Details------------------------------------------------------------*/
$("#btnAdd").click(function () {
    var EligibleforFree = 0;
    if ($('#YesEligible').prop('checked')) {
        EligibleforFree = $('#YesEligible').val();
    }
    if (checkrequiredUnit()) {
        if (parseFloat($("#txtCostPrice").val()) > parseFloat($('#txtMinPrice').val())) {
            swal("Error!", "Cost Price should be less or equal to Retail Min Price.", "error");
            return;
        }
        if (parseFloat($("#txtWholesalePrice").val()) > parseFloat($('#txtMinPrice').val())) {
            swal("Error!", "Whole Sale Min Price should be less or equal to  Retail Min Price.", "error");
            return;
        }
        if ((parseFloat($("#txtWholesalePrice").val()) > parseFloat($("#txtPrice").val())) || (parseFloat($('#txtMinPrice').val()) > parseFloat($("#txtPrice").val()))) {
            swal("Error!", "Base Price should be greater  or equal to Retail Min Price and Whole Sale Min Price.", "error");
            return;
        }
        var data = {
            UnitType: $("#ddlUnitType").val(),
            UnitQty: $("#txtQty").val(),
            MinPrice: $("#txtMinPrice").val(),
            CostPrice: $("#txtCostPrice").val(),
            Price: $("#txtPrice").val(),
            ProductLocation: $("#txtLocation").val(),
            WHminPrice: $("#txtWholesalePrice").val(),
            PreDefinedBarcode: $("#txtPreDefinedBarcode").val(),
            // CommCode: $("#txtCommCode").val(),
            ProductAutoId: $("#txtHProductAutoId").val(),
            EligibleforFree: EligibleforFree
        };
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/productMaster.asmx/insertPackingDetails",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d != "Session Expired") {
                    if (result.d == "true") {
                        swal("", "Packing details added.", "success");
                        getPackingDetails();
                        resetPackingDetails();
                    } else {
                        swal("Error!", result.d, "error");
                    }
                } else {
                    location.href = '/';
                }
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d)
                swal("Error!", 'Oops,some thing went wrong.Please try again.', "error")
            },
            failure: function (result) {
                swal("Error!", 'Oops,some thing went wrong.Please try again.', "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
});
$("#btnReset2").click(function () {
    resetPackingDetails();
});
/*----------------------------------------------------------Get Packing Details-----------------------------------------------------*/
function getPackingDetails() {
    var ProductAutoId = $("#txtHProductAutoId").val();
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/productMaster.asmx/getPackingDetails",
        data: "{'ProductAutoId':'" + ProductAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successGetPacking,
        error: function (result) {
            swal("Error!", 'Oops,some thing went wrong.Please try again.', "error");
        },
        failure: function (result) {
            swal("Error!", 'Oops,some thing went wrong.Please try again.', "error");
        }
    });
}

function successGetPacking(response) {
    
    if (response.d != "Session Expired") {
        var xmldoc = $.parseXML(response.d);
        var packing = $(xmldoc).find("Table");
        $("#tblAddedPacking tbody tr").remove();
        var row = $("#tblAddedPacking thead tr").clone(true);
        if (packing.length > 0) {
            $("#emptyTable").hide();
            var thml = '';
            $.each(packing, function () {
                if ($(this).find("PackingAutoId").text() != 0) {
                    $(".Edit", row).html("<input type='checkbox'  checked='true' onclick='fndefault(this)'/>");
                } else {
                    $(".Edit", row).html("<input type='checkbox' onclick='fndefault(this)'/>");
                }
                if ($(this).find("PackingAutoId").text() != 0) {
                    thml = '<a href="#" title="Default Packing" style="color:green"> * </a>';
                }
                else {
                    thml = "";
                }
                $(".UnitType", row).html("<span PackingDetailAutoId='" + $(this).find("AutoId").text() + "'>" + $(this).find("UnitType").text() + thml + "</span>");
                $(".Qty", row).html("<span PackingAutoId='" + $(this).find("AutoId").text() + "'>" + $(this).find("Qty").text());
                $(".MinPrice", row).text($(this).find("MinPrice").text());
                $(".CostPrice", row).text($(this).find("CostPrice").text());
                $(".WMinPrice", row).text($(this).find("WHminPrice").text());
                $(".Location", row).text($(this).find("Location").text());
                $(".Price", row).text($(this).find("Price").text());
                $(".Free", row).text($(this).find("EligibleforFree").text());
                $(".BarCode", row).html("<a href='#' data-toggle='modal' data-target='#barcodeModal' onclick='showBarcode(" + $(this).find("AutoId").text() + ")'><i class='la la-eye'></i></a>");
                //$(".Action", row).html("<a href='javascript:;'><span class='la la-eye' onclick='editPacking(" + $(this).find("AutoId").text() + ")'></span></a>");
                $(".Action", row).html("<a href='javascript:;'><span class='ft-edit' onclick='editPacking(" + $(this).find("AutoId").text() + ")'></span></a><a href='javascript:;'>&nbsp;&nbsp;&nbsp;&nbsp;<span class='ft-x' onclick='deletePacking(" + $(this).find("AutoId").text() + ")'></span></a>");
                $("#tblAddedPacking tbody").append(row);
                row = $("#tblAddedPacking tbody tr:last").clone(true);
            });
        } else {
            $("#emptyTable").show();
        }
    } else {
        location.href = '/';
    }

}

/*-------------------------------------------------Show Barcode-------------------------------------------------------*/

function showBarcode(PackingDetailAutoId) {

    $("#hiddenPackingAutoId").val(PackingDetailAutoId);
    $("#alertBarcode").hide();
    var data = {
        ProductAutoId: $("#txtHProductAutoId").val(),
        PackingDetailAutoId: PackingDetailAutoId
    };

    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/productMaster.asmx/getBarcode",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var barcode = $(xmldoc).find("Table1");
                var count = $(xmldoc).find("Table2");

                $("#tblBarcode tbody tr").remove();
                var row = $("#tblBarcode thead tr").clone(true);
                $.each(barcode, function () {
                    if ($(this).find("Barcode").text() != '') {
                        $("td", row).eq(0).html("<span BarcodeAutoId='" + $(this).find("AutoId").text() + "'>" + $(this).find("Barcode").text() + "</span>");
                        $("td", row).eq(1).html(DrawCode39Barcode($(this).find("Barcode").text()));
                        if ($('#CheckimpType').val() == 1) {
                            $("td", row).eq(4).html("<a href='javascript:;' onclick='printBarCode(this)'><span class='icon-printer'></span></a>&nbsp<a href='javascript:;' onclick='deleterecordBarcode(" + $(this).find("AutoId").text() + ")'><span class='ft-x'></span></a>");
                        } else {
                            $("td", row).eq(4).html("<a href='javascript:;' onclick='printBarCode(this)'><span class='icon-printer'></span></a>");
                        }
                        $("td", row).eq(2).html($(this).find("Totalcount").text());
                        $("td", row).eq(3).html($(this).find("LastUsedDate").text());
                        $("#tblBarcode tbody").append(row);
                        row = $("#tblBarcode tbody tr:last").clone(true);
                    }
                    $("#productId").text($(this).find("ProductId").text());
                    $("#productName").text($(this).find("ProductName").text());
                    $("#UnitName").text($(this).find("UnitName").text());
                });
                if ($(count).length > 0)
                    barcodeCount = $(count).find("BarcodeCount").text();
                $("td", row).eq(0).text("");
                $("td", row).eq(1).text("");
                $("td", row).eq(2).text("");
                $("td", row).eq(3).text("");
                $("td", row).eq(4).html("<a href='javascript:;' onclick='addMoreBarcode(this)'>Add More</a>");
                $("#tblBarcode tbody").append(row);
                $("#barcodeModal").modal('show');

            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            swal("Error!", 'Oops,some thing went wrong.Please try again.', "error");
        },
        failure: function (result) {
            swal("Error!", 'Oops,some thing went wrong.Please try again.', "error");
        }
    });
}

function deleterecordBarcode(e) {

    var barcodeAutoId = e;
    // var barcodeAutoId = $(e).closest("tr").find("td:eq(0)").find("span").attr("BarcodeAutoId");
    swal({
        title: "Are you sure?",
        text: "You want to delete this barcode.",
        icon: "warning",
        showCancelButton: true,
        allowOutsideClick: false,
        closeOnClickOutside: false,
        buttons: {
            cancel: {
                text: "No, Cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, Delete it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(isConfirm => {
        if (isConfirm) {
            deleteBarcode(barcodeAutoId);
        }
    })
}

/*-------------------------------------------------Edit Barcode-------------------------------------------------------*/
var barcode;
function editBarcode(e) {
    var td = $(e).closest("tr").find("td:eq(0)");
    barcode = td.find("span").text();
    td.html("<span BarcodeAutoId='" + td.find("span").attr("BarcodeAutoId") + "'><input type='text' class='form-control input-sm' value='" + barcode + "'/></span>");
    $(e).closest("td").html("<button type='button' class='btn btn-success btn-xs' id='btnBarcodeUpdate' onclick='updateBarcode(this)'>Update</button><br /><br /><button type='button' class='btn btn-default btn-xs' id='btnBarcodeCancel' onclick='cancelUpdate(this)'>Cancel</button>");
    $("#tblBarcode tbody tr").find("a").hide();
}

function cancelUpdate(e) {
    td = $(e).closest("tr").find("td:eq(0)");
    td.html("<span BarcodeAutoId='" + td.find("span").attr("BarcodeAutoId") + "'>" + barcode + "</span>");
    $(e).closest("td").html("<a href='javascript:;'><span class='glyphicon glyphicon-edit' onclick='editBarcode(this)'></span></a><a href='javascript:;'><span class='glyphicon glyphicon-remove' onclick='deleteBarcode(this)'></span></a><a href='javascript:;'><span class='glyphicon glyphicon-print'></span></a>");
    $("#tblBarcode tbody tr").find("a").show();
}

/*-------------------------------------------------Update Barcode-------------------------------------------------------*/
function updateBarcode(e) {
    var td = $(e).closest("tr").find("td:eq(0)");
    var data = {
        barcodeAutoId: td.find("span").attr("BarcodeAutoId"),
        barcode: td.find("input").val()
    };
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/productMaster.asmx/updateBarcode",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                showBarcode($("#hiddenPackingAutoId").val());
            } else {
                location.href = '/';
            }
        },
        error: function (result) {
            swal("Error!", "Barcode already exists in eecord.", "error");
        },
        failure: function (result) {
            swal("Error!", 'Oops,some thing went wrong.Please try again.', "error");
        }
    });
}

/*-------------------------------------------------Delete Barcode------------------------------------------------------------------*/
function deleteBarcode(barcodeAutoId) {
    //if (confirm("Are you sure you want to delete this Barcode?")) {
    //var barcodeAutoId = $(e).closest("tr").find("td:eq(0)").find("span").attr("BarcodeAutoId");
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/productMaster.asmx/deleteBarcode",
        data: "{'barcodeAutoId':'" + barcodeAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                showBarcode($("#hiddenPackingAutoId").val());
                swal("", "Barcode deleted successfully.", "success");
            } else {
                location.href = '/';
            }


        },
        error: function (result) {
            swal("Error!", 'Oops,some thing went wrong.Please try again.', "error");
        },
        failure: function (result) {
            swal("Error!", 'Oops,some thing went wrong.Please try again.', "error");
        }
    });
}
//}

/*-------------------------------------------------Add More Option For Barcode-------------------------------------------------------*/
function addMoreBarcode(e) {
    $(e).closest("tr").find("td:eq(0)").html("<input type='text' maxlength='16'  class='form-control input-sm border-primary' id='txtNewBarcode' />");
    $(e).closest("td").html("<button type='button' class='btn btn-success buttonAnimation round box-shadow-1 btn-sm' id='btnBarcodeSave' onclick='saveBarcode()'>Save</button><button type='button' class='btn btn-warning buttonAnimation pull-right round box-shadow-1 btn-sm animated pulse' id='btnEntryCancel' onclick='cancelSave(this)'>Cancel</button>");
}

/*-------------------------------------------------Save Barcode In Popup-------------------------------------------------------------*/
function saveBarcode() {
    var brCode = $("#txtNewBarcode").val();
    if ($("#txtNewBarcode").val() == '') {
        $("#txtNewBarcode").addClass('border-warning');
        return;
    }
    else if (brCode.indexOf(' ') >= 0) {
        $("#txtNewBarcode").removeClass('border-warning');
        swal("", "Invalid Barcode its contains space. Please remove the space.", "warning");
        return
    }
    else {
        $("#txtNewBarcode").removeClass('border-warning');
    }
    var data = {
        PackingAutoId: $("#hiddenPackingAutoId").val(),
        ProductAutoId: $("#txtHProductAutoId").val(),
        NewBarcode: $("#txtNewBarcode").val()
    };

    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/productMaster.asmx/saveBarcode",
        data: JSON.stringify({ dataValues: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "false") {
                debugger
                var xmldoc = $.parseXML(response.d);
                var BarcodeDetails = $(xmldoc).find("Table4"); var html = '';
                //showBarcode($("#hiddenPackingAutoId").val());
                if (BarcodeDetails.length > 0) {
                    html += '<tr><td style="text-align:center"><span BarcodeAutoId=' + $(BarcodeDetails).find("AutoId").text() + '>' + $(BarcodeDetails).find("Barcode").text() + '</span></td>';
                    html += '<td style="text-align:center">' + DrawCode39Barcode($(BarcodeDetails).find("Barcode").text()) + '</td>';
                    html += '<td style="text-align:center">' + $(BarcodeDetails).find("Totalcount").text() + '</td>';
                    html += '<td style="text-align:center">' + $(BarcodeDetails).find("LastUsedDate").text() + '</td>';
                    if ($('#CheckimpType').val() == 1) {
                        html += '<td style="text-align:center"><a href="javascript:;" onclick="printBarCode(this)"><span class="icon-printer"></span></a>&nbsp<a href="javascript:;" onclick="deleterecordBarcode(' + $(BarcodeDetails).find("AutoId").text() + ')"><span class="ft-x"></span></a></td>';
                    }
                    else {
                        html += '<td style="text-align:center"><a href="javascript:;" onclick="printBarCode(this)"><span class="icon-printer"></span></a></td></tr>';
                    }
                    $("#tblBarcode tr:last").before(html);
                    $("#tblBarcode tbody tr:last").remove();
                    html = '';
                    html = '<tr><td></td><td></td><td></td><td></td><td style="text-align:center"><a href="javascript:;" onclick="addMoreBarcode(this)">Add More</a></td>';
                    $("#tblBarcode tbody").append(html);
                }
                swal("", " Barcode saved successfully.", "success");
            }
            else if (response.d == "false") {
                swal("", "Oops, Something went wrong. Please try later.", "error");
            }
            else {
                location.href = '/';
            }
        },
        error: function (result) {
            swal("Error!", " Barcode already exists in record.", "error");
        },
        failure: function (result) {
            swal("Error!", 'Oops,some thing went wrong.Please try later.', "error");
        }
    });
}
/*-------------------------------------------------Cancel Save Option-------------------------------------------------------*/
function cancelSave(e) {
    $(e).closest("tr").find("td:eq(0)").html("");
    $(e).closest("td").html("<a href='javascript:;' onclick='addMoreBarcode(this)'>Add More</a>");
}

/*-------------------------------------------------Edit Product--------------------------------------------------------*/
function editProduct(ProductId) {
    $('#next').show();
    $('.webdtl').show();
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/productMaster.asmx/editProduct",
        data: "{'ProductId':'" + ProductId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successEditProduct,

        error: function (result) {
            swal("Error!", 'Oops,some thing went wrong.Please try later.', "error");
        },
        failure: function (result) {
            swal("Error!", 'Oops,some thing went wrong.Please try later.', "error");
        }
    });
}
function imageExists(image_url) {

    var http = new XMLHttpRequest();

    http.open('HEAD', image_url, false);
    http.send();

    return http.status != 404;

}
function successEditProduct(response) {
    if (response.d != "Session Expired") {
        var xmldoc = $.parseXML(response.d);
        var subcategory = $(xmldoc).find("Table");
        var category = $(xmldoc).find("Table1");
        var unitType = $(xmldoc).find("Table2");
        var product = $(xmldoc).find("Table3");
        var packing = $(xmldoc).find("Table4");
        var VendorDetails = $(xmldoc).find("Table5");
        var BrandMaster = $(xmldoc).find("Table6");
        var CommissionCode = $(xmldoc).find("Table7");
        var websiteDetail = $(xmldoc).find("Table8");
        var domain = $('#HDDomain').val();
      
        $("#btnUpdate1").show();
        $("#PackingDetails").show();
        $("#linkAddNew").show();
        $("#btnSave").hide();
        $("#btnReset1").hide();
        $("#divStock").show();
        $("#webImagesitedtls").show();
        $('#ddlSubcategory').attr('disabled', false);
        if ($("#hiddenText").val() != "") {
            $("select").attr("disabled", true);
            $("input[type='text']").attr("disabled", true);
            $("#fileImageUrl").attr("disabled", true);
            $("#linkAddNew").hide();
            $("#btnUpdate1").hide();
            // $("#panelPacking").hide();
            $("#tblAddedPacking tr").find("td:eq(8)").hide();
            $("#tblBarcode").find("td:eq(2)").hide();

        }

        $("#ddlCategory option").remove();
        $("#ddlCategory").append("<option value='0'>-Select-</option>");
        $.each(category, function () {
            $("#ddlCategory").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("CategoryName").text() + "</option>");
        });

        $("#ddlSubcategory option").remove();
        $("#ddlSubcategory").append('<option value="0">-Select-</option>');
        $.each(subcategory, function () {
            $("#ddlSubcategory").append($("<option></option>").val($(this).find("AutoId").text()).html($(this).find("SubcategoryName").text()));
        });
        $("#ddlVendor option").remove();
        $("#ddlVendor").append('<option value="0">-Select-</option>');
        $.each(VendorDetails, function () {
            $("#ddlVendor").append($("<option></option>").val($(this).find("AutoId").text()).html($(this).find("VendorName").text()));
        });

        $("#ddlUnitType option").remove();
        $("#ddlUnitType").append('<option value="0">-Select-</option>');
        $.each(unitType, function () {
            $("#ddlUnitType").append($("<option></option>").val($(this).find("AutoId").text()).html($(this).find("UnitType").text()));
        });
        if ($(product).find("VendorAutoId").text() != "") {
            $("#ddlVendor").val($(product).find("VendorAutoId").text());
        }
        $("#ddlBrand option").remove();
        $("#ddlBrand").append('<option value="0">-Select-</option>');
        $.each(BrandMaster, function () {
            $("#ddlBrand").append($("<option></option>").val($(this).find("AutoId").text()).html($(this).find("BrandName").text()));
        });

        $("#ddlCommCode option").remove();
        $("#ddlCommCode").append('<option value="">Choose a  Commission Code</option>');
        $.each(CommissionCode, function () {
            $("#ddlCommCode").append($("<option></option>").val($(this).find("CommissionCode").text()).html($(this).find("C_DisplayName").text()));
        });

        if ($(product).find("BrandAutoId").text() != "") {
            $("#ddlBrand").val($(product).find("BrandAutoId").text());
        }

        if ($(product).find("PreviousProduct").text() == 0) {
            $("#PreviousProduct").attr('href', "javascript:void(0)");
        } else {
            $("#PreviousProduct").attr('href', "/Admin/productMaster.aspx?ProductId=" + $(product).find("PreviousProduct").text());
            //$("#PreviousProduct").attr('href', $(product).find("PreviousProduct").text());
        }
        if ($(product).find("NextProduct").text() == 0) {
            $("#NextProduct").attr('href', "javascript:void(0)");
        } else {
            $("#NextProduct").attr('href', "/Admin/productMaster.aspx?ProductId=" + $(product).find("NextProduct").text());
        }
        $('#txtDescription').summernote('code', $(product).find("Description").text());
        if ($(product).find("IsOutOfStock").text() == '1') {
            $("#CheckboxOOS").prop('checked', true);
        } else {
            $("#CheckboxOOS").prop('checked', false);
        }


        $("#ddlCategory").val($(product).find("CategoryAutoId").text());
        $("#ddlSubcategory").val($(product).find("SubcategoryAutoId").text());
        $("#txtHProductAutoId").val($(product).find("AutoId").text());
        $("#txtProductId").val($(product).find("ProductId").text());
        $("#txtProductName").val($(product).find("ProductName").text());
        $("#txtMLQty").val($(product).find("MLQty").text());
        $("#txtLocation").val($(product).find("ProductLocation").text());
        debugger;
       var check= imageExists($(product).find('ImageUrl').text());
        if ($(product).find('ImageUrl').text() != '') {
            $("#imgProductImage").attr("src", $(product).find('ImageUrl').text());
            $("#imgProductImage").attr("mainsrc", $(product).find('ImageUrl').text());
        }
        else {
            $("#imgProductImage").attr("src", "/images/default_pic.png");
        }
        $("#ddlProductType").val($(product).find('ProductType').text());
        $("#txtStock").val($(product).find('Stock').text());
        if ($(product).find('DefaultUnitType').text() != "Piece") {
            $("#lbldefaultunit").text($(product).find('DefaultUnitType').text());
            $("#TxtdefaultunitStock").val($(product).find('CurrentStock').text());
            $("#divdefaultstock").show();
        }
        else {
            $("#divdefaultstock").hide();
        }
        $("#txtTaxRate").val($(product).find('TaxRate').text());
        $("#reQty").val($(product).find('Qty').text());
        if ($(product).find('DefaultUnitType').text() == 'Piece') {
            $("#reorder_text").html('Re Order Mark (Piece)');
            //$("#reorder_text").html('Re Order Mark (Piece) </br> <span style="color:red;font-weight:bold">Piece Qty - ' + $(product).find('PieceReOrderMark').text() + '</span>');
        } else {
            var piece = 0;
            if ($(product).find('ReOrderMark').text() > 0) {
                piece = $(product).find('PieceReOrderMark').text();
            } else {
                piece = 0;
            }
            $("#reorder_text").html('Re Order Mark (' + $(product).find('DefaultUnitType').text() + ') </br> <span style="color:red;font-weight:bold">Piece Qty - ' + piece+'</span>');
        }

        $("#txtReOrderMark").val($(product).find('ReOrderMark').text());
      
        if ($(product).find("IsApply_ML").text() == "true") {
            $('#cbMlQuantity').prop('checked', true);
        } else {
            $('#cbMlQuantity').prop('checked', false);
        }
        if ($(product).find("ShowOnWebsite").text() == "1") {
            $('#IsShowOnWebsite1').prop('checked', true);
        } else {
            $('#IsShowOnWebsite1').prop('checked', false);
        }
        $("#txtMLQty").val($(product).find('MLQty').text());
        if ($(product).find("IsApply_Oz").text() == "true") {
            $('#CbWeightQuantity').prop('checked', true);
        } else {
            $('#CbWeightQuantity').prop('checked', false);
        }
        $("#txtWeightOz").val($(product).find('WeightOz').text());
        $("#ddlCommCode").val($(product).find("CommCode").text());
        $("#txtSRP").val($(product).find("SRP").text());
        var row = $("#tblAddedPacking thead tr").clone(true);
        $("#tblAddedPacking tbody tr").remove();
        var arr = [];
        if (packing.length > 0) {
            $("#emptyTable").hide();
            var thml = "";
            $.each(packing, function () {
                if ($(this).find("PackingAutoId").text() != 0) {
                    $(".Edit", row).html("<input type='checkbox'  checked='true' onclick='fndefault(this)'/>");
                } else {
                    $(".Edit", row).html("<input type='checkbox' onclick='fndefault(this)'/>");
                }
                if ($(this).find("PackingAutoId").text() != 0) {
                    thml = '<a href="#" title="Default Packing" style="color:green"> * </a>';
                }
                else {
                    thml = "";
                }
                $(".UnitType", row).html("<span PackingDetailAutoId='" + $(this).find("AutoId").text() + "'>" + $(this).find("UnitType").text() + thml + "</span>");
                $(".Qty", row).html("<span PackingAutoId='" + $(this).find("AutoId").text() + "'>" + $(this).find("Qty").text());
                $(".MinPrice", row).text($(this).find("MinPrice").text());
                $(".CostPrice", row).text($(this).find("CostPrice").text());
                $(".WMinPrice", row).text($(this).find("WHminPrice").text());
                $(".Location", row).text($(this).find("Location").text());
                $(".Price", row).text($(this).find("Price").text());
                $(".SRP", row).text($(this).find("SRP").text());

                if ($(this).find("EligibleforFree").text() == "Yes") {
                    $(".Free", row).html('<span class="badge badge badge-pill badge-success">Yes</span>');
                }
                else {
                    $(".Free", row).html('<span class="badge badge badge-pill badge-danger">No</span>');
                }
                $(".BarCode", row).html("<a href='#'  onclick='showBarcode(" + $(this).find("AutoId").text() + ")'><i class='la la-eye'></i></a>");
                $(".CommCode", row).text($(this).find("CommCode").text());
                // $(".Action", row).html("<a href='javascript:;'><span class='la la-eye' onclick='editPacking(" + $(this).find("AutoId").text() + ")'></span></a>");

                $(".Action", row).html("<a href='javascript:;'><span class='la la-eye' onclick='editPacking(" + $(this).find("AutoId").text() + ")'></span></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:;'><span class='ft-x' onclick='deletePacking(" + $(this).find("AutoId").text() + ")'></span></a>");
                $("#tblAddedPacking tbody").append(row);
                row = $("#tblAddedPacking tbody tr:last").clone(true);
            });


        }
        else {
            $("#emptyTable").show();
        }
        if ($('#Hd_Domain').val().toLowerCase() != "psmnj" && $('#Hd_Domain').val().toLowerCase() != "psmnjm" && $('#Hd_Domain').val().toLowerCase() != "localhost" && $('#Hd_Domain').val().toLowerCase() != "psm" && $('#Hd_Domain').val().toLowerCase() != "demo") {
            $('#ddlCategory').attr('disabled', 'disabled');
            $('#ddlSubcategory').attr('disabled', 'disabled');

            $('#txtProductName').attr('disabled', 'disabled');
            $('#fileImageUrl').attr('disabled', 'disabled');
            $("#ddlCommCode").attr('disabled', 'disabled');
            $('#txtMLQty').attr('disabled', 'disabled');
            $('#txtWeightOz').attr('disabled', 'disabled');
            $('#btnAdd').hide();
            $('.Delete').hide();
            //$("#txtSRP").attr("disabled", true);
        }
        $('#txtProductId').attr('disabled', 'disabled');
        if (websiteDetail.length > 0) {
            var htmlimg1 = `<div class="row tiles">`;
            var m = 0;
            $.each(websiteDetail, function () {
                if ($(this).find("isDefault").text() != '') {
                    $("#defaultImageName").val($(this).find("ImageUrl").text());
                } else {
                }
                Imgresponse.push({
                    'URL': $(this).find("ImageUrl").text(),
                    'Thumb100': $(this).find("Thumbnail_100").text(),
                    'Thumb400': $(this).find("Thumbnaill_400").text(),
                    'Status': 1
                });

                htmlimg1 += '<div class="col-xs-2">'//$('#Hd_Domain').val().toLowerCase()== "psmnj" && 
                if ($('#Hd_Domain').val().toLowerCase() == "psmnj" || $('#Hd_Domain').val().toLowerCase() == "psmnjm" || $('#Hd_Domain').val().toLowerCase() == "localhost" || $('#Hd_Domain').val().toLowerCase() == "psm" || $('#Hd_Domain').val().toLowerCase() == "demo") {
                    htmlimg1 += '<div><img class="' + $(this).find("isDefault").text() + '" src="/Attachments/' + $(this).find("ImageUrl").text() + '" title="' + $(this).find("ImageUrl").text() + '" onclick="setDefaultDynamic(this)"></div>'
                    htmlimg1 += '<i class="la la-times" onclick="deleteProdutcImage(' + $(this).find("AutoId").text() + ')"></i></div>';
                }
                else {
                    htmlimg1 += '<div><img class="' + $(this).find("isDefault").text() + '" src="/Attachments/' + $(this).find("ImageUrl").text() + '" title="' + $(this).find("ImageUrl").text() + '"></div>'
                    htmlimg1 += '</div>';
                }
                if (m % 4 == 0 && m != 0) {
                    htmlimg1 += '</div><div class="row tiles">';
                }
                m++;
            });
            $(".imgbox").html(htmlimg1);

        }


    } else {
        location.href = '/';
    }
}

$("#btnAddNew").click(function () {
    window.location.href = "/Admin/productMaster.aspx";
})
var par;
function editPacking(PackingDetailAutoId) {
    $("#ddlUnitType").prop('disabled', true);
    par = PackingDetailAutoId;

    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/productMaster.asmx/editPacking",
        data: "{'PackingDetailAutoId':" + PackingDetailAutoId + "}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var packing = $(xmldoc).find("Table");

                if ($(packing).find("UnitType").text() == 3) {
                    $("#txtQty").attr('readonly', true);
                } else {
                    $("#txtQty").attr('readonly', false);
                }
                $("#ddlUnitType").val($(packing).find("UnitType").text());
                $("#txtQty").val($(packing).find("Qty").text());
                $("#txtMinPrice").val($(packing).find("MinPrice").text());
                $("#txtCostPrice").val($(packing).find("CostPrice").text());
                $("#txtPrice").val($(packing).find("Price").text());
                $("#txtWholesalePrice").val($(packing).find("WHminPrice").text());
                $("#txtLocation").val($(packing).find("Location").text());
                if ($(packing).find('EligibleforFree').text() == 1) {
                    $("#YesEligible").prop('checked', true);
                    $("#NoEligible").prop('checked', false);
                } else {
                    $("#YesEligible").prop('checked', false);
                    $("#NoEligible").prop('checked', true);
                }
                $("#btnAdd").hide();
                //  $("#btnUpdate2").show();
                $("#btnCancel2").show();// Start added on 11/27/2019 By Rizwan Ahmad
                $("#ddlUnitType").attr('disabled', true);
                $("#txtQty").attr('disabled', true);
                $("#txtMinPrice").attr('disabled', true);
                $("#txtCostPrice").attr('disabled', true);
                $("#txtPrice").attr('disabled', true);
                $("#txtWholesalePrice").attr('disabled', true);
                $("#txtLocation").attr('disabled', true);
                $("#txtPreDefinedBarcode").attr('disabled', true);// End 

                if ($('#Hd_Domain').val().toLowerCase() != "psmnj" && $('#Hd_Domain').val().toLowerCase() != "psmnjm" && $('#Hd_Domain').val().toLowerCase() != "localhost" && $('#Hd_Domain').val().toLowerCase() != "psm" && $('#Hd_Domain').val().toLowerCase() != "demo") {
                    $("#ddlUnitType").attr('disabled', true);
                    $("#txtPreDefinedBarcode").attr('disabled', true);
                }
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            swal("Error!", 'Oops,some thing went wrong.Please try later.', "error");
        },
        failure: function (result) {
            swal("Error!", 'Oops,some thing went wrong.Please try later.', "error");
        }
    });
}

function deletePacking(PackingDetailAutoId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this packing.",
        icon: "warning",
        showCancelButton: true,
        allowOutsideClick: false,
        closeOnClickOutside: false,
        buttons: {
            cancel: {
                text: "No, Cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, Delete it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(isConfirm => {
        if (isConfirm) {
            deleterecord(PackingDetailAutoId);
        }
    })
}


function deleterecord(PackingDetailAutoId) {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/productMaster.asmx/deletePacking",
        data: "{'PackingDetailAutoId':" + PackingDetailAutoId + "}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d == 'Success!!') {
                    swal("", "Packing details deleted successfully.", "success");
                    getPackingDetails();
                    resetPackingDetails();
                }
                else {
                    swal("Error!", response.d, "error");
                }
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            swal("Error!", 'Oops,some thing went wrong.Please try later.', "error");
        },
        failure: function (result) {
            swal("Error!", 'Oops,some thing went wrong.Please try later.', "error");
        }
    });
}

$("#btnCancel2").click(function () {
    resetPackingDetails();
    $('#ddlUnitType').attr('disabled', false);
    $('#txtQty').attr('readonly', false);
})

$("#btnUpdate2").click(function () {
    if (checkrequiredUnit()) {

        if (parseFloat($("#txtCostPrice").val()) > parseFloat($('#txtMinPrice').val())) {

            $('#ProductMasterPopup').modal('show');
            $('#ProductMasterMsg').html('Cost Price should be less or equal to Retail Min Price.').css('color', 'red');
            return;
        }
        if (parseFloat($("#txtWholesalePrice").val()) > parseFloat($('#txtMinPrice').val())) {
            $('#ProductMasterPopup').modal('show');
            $('#ProductMasterMsg').html('Whole Sale Min Price should be less or equal to  Retail Min Price.').css('color', 'red');
            return;
        }
        if ((parseFloat($("#txtWholesalePrice").val()) > parseFloat($("#txtPrice").val())) || (parseFloat($('#txtMinPrice').val()) > parseFloat($("#txtPrice").val()))) {
            $('#ProductMasterPopup').modal('show');
            $('#ProductMasterMsg').html('Base Price should be greater  or equal to Retail Min Price and Whole Sale Min Price.').css('color', 'red');
            return;
        }

        var txtBarcode;
        if ($("input[name='PreDefinedBarcode']").is(":checked")) {
            txtBarcode = $("#txtPreDefinedBarcode").val();
        } else {
            txtBarcode = "";
        }
        var EligibleforFree = 0;
        if ($('#YesEligible').prop('checked')) {
            EligibleforFree = $('#YesEligible').val();
        }
        var data = {
            UnitType: $("#ddlUnitType").val(),
            UnitQty: $("#txtQty").val(),
            MinPrice: $("#txtMinPrice").val(),
            CostPrice: $("#txtCostPrice").val(),
            Price: $("#txtPrice").val(),
            ProductLocation: $("#txtLocation").val(),
            WHminPrice: $("#txtWholesalePrice").val(),
            ProductAutoId: $("#txtHProductAutoId").val(),
            PackingDetailAutoId: par,
            EligibleforFree: EligibleforFree
        };

        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/productMaster.asmx/updatePacking",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d != "Session Expired") {
                    if (response.d == 'Success') {
                        swal("", "Packing details updated successfully.", "success");
                        getPackingDetails();
                        resetPackingDetails();
                    } else if (response.d == 'false') {
                        swal("Error!", 'Oops,some thing went wrong.Please try later.', "error");
                    } else {
                        var xmldoc = $.parseXML(response.d);
                        var PricelevelList = $(xmldoc).find("Table");
                        var html = "<div class='table-responsive'><table class='table table-striped table-bordered'><thead><tr><td>Unit</td><td>Price Level Name</td>";
                        html += "<td>Custom Price</td></tr></thead><tbody>";
                        $(PricelevelList).each(function () {
                            //html += "<tr><td>" + $(this).find('ProductId').text() + "</td><td>" + $(this).find('ProductName').text() + "</td>";
                            html += "<tr><td>" + $(this).find('UnitType').text() + "</td><td>" + $(this).find('PriceLevelName').text() + "</td>";
                            html += "<td class='text-right'>" + $(this).find('CustomPrice').text() + "</td>";
                            html += "</tr>";
                        });
                        html += "</tbody><tfoot><tr><td colspan='5' style='text-align: left;white-space:pre-wrap'><b>You can not update cost price because price level should not be less than cost price so please update price level.</b></td></tr></tfoot></table></div>";
                        $("#Sbarcodemsg").html(html);
                        $("#msgPop").modal('show');
                        //swal({
                        //    title: '',
                        //    text: '',
                        //    content: "html"
                        //});
                        //swal("Error!", html , "error");
                    }
                } else {
                    location.href = '/';
                }

            },
            error: function (result) {
                swal("Error!", 'Oops,some thing went wrong.Please try later.', "error");
            },
            failure: function (result) {
                swal("Error!", 'Oops,some thing went wrong.Please try later.', "error");

            }
        });
    }
})
/*-----------------------------------------Reset Product-----------------------------------------------------------*/
function resetProduct() {
    $('#cbMlQuantity').prop('checked', false);
    $('#CbWeightQuantity').prop('checked', false);
    $("#ddlCategory").val(0);
    $("#ddlVendor").val(0).change();
    $("#ddlBrand").val(0).change();
    $("#ddlCommCode").val(0).change();
    $("#txtReOrderMark").val('0.00');
    $("#txtMLQty").val('0');
    $("#txtWeightOz").val('0.00');
    $("#txtSRP").val('0.00');

    $("#txtProductId").val('');
    $("#txtProductName").val('');
    $("#fileImageUrl").val('');
    $("#imgProductImage").attr('src', '/images/default_pic.png');

    $("#ddlSubcategory").val(0).attr('disabled', true);
    $("#panelProductInformation input[type='text']").val('');
    $("#btnUpdate1").hide();
    $("#btnAddNew").hide();
    $("#btnSave").show();
    $("#btnReset1").show();
    $("#panelProductInformation input[type='text']").removeClass('border-warning');

}
function resetPackingDetails() {
    $("#ddlUnitType").val(0);
    $("#ddlUnitType").attr('disabled', false);
    $("#panelPacking input[type='text']").val('');
    $("#btnAdd").show();
    $("#btnUpdate2").hide();
    $("#btnCancel2").hide();
    if ($('#Hd_Domain').val().toLowerCase() != 'psmnj' && $('#Hd_Domain').val().toLowerCase() != "psmnjm" && $('#Hd_Domain').val().toLowerCase() != "localhost" && $('#Hd_Domain').val().toLowerCase() != "demo" && $('#Hd_Domain').val().toLowerCase() != "psm") {
        $("#btnAdd").hide();
        //$('#ddlBrand').attr('disabled', true);
    }
    $("#YesEligible").prop('checked', false);
    $("#NoEligible").prop('checked', true);
    // Start added on 11/27/2019 By Rizwan Ahmad
    $("#ddlUnitType").attr('disabled', false);
    $("#txtQty").attr('disabled', false);
    $("#txtMinPrice").attr('disabled', false);
    $("#txtCostPrice").attr('disabled', false);
    $("#txtPrice").attr('disabled', false);
    $("#txtWholesalePrice").attr('disabled', false);
    $("#txtLocation").attr('disabled', false);
    $("#txtPreDefinedBarcode").attr('disabled', false);// End 
}
function fndefault(e) {
    $("#alertDPacking").hide();
    $("#alertSPacking").hide();
    var tr = $(e).closest('tr');

    if (tr.find("td:eq(1) input").prop('checked') == false) {
        tr.find("td:eq(1) input").prop('checked', true);
        swal("Error!", 'Atleast set one packing as default.', "error");
        return;
    }
    var PackingAutoId = tr.find("td:eq(3) span").attr('PackingAutoId')

    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/productMaster.asmx/DefaultPacking",
        data: "{'PackingAutoId':'" + PackingAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                swal("", "Packing details updated successfully.", "success");
                getPackingDetails();
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            swal("Error!", 'Oops,some thing went wrong.Please try later.', "error");
        },
        failure: function (result) {
            swal("Error!", 'Oops,some thing went wrong.Please try later.', "error");
        }
    });
}
function managePrice() {
    $('#Div1').hide();
    $("input:radio").attr("checked", false);
    ProductAutoId = $("#txtHProductAutoId").val();
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/productMaster.asmx/getManagePrice",
        data: "{'ProductAutoId':'" + ProductAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var packingDetails = $(xmldoc).find("Table");
                $("#Table1 tbody tr").remove();
                var row = $("#Table1 thead tr:last-child").clone();
                $(packingDetails).each(function (index) {
                    $("#ProductIDNName").html('[ ' + $("#txtProductId").val() + ' - ' + $("#txtProductName").val() + ' ]');
                    //if ($(this).find('Status').text() == '0') {
                    //    $(".Status", row).html('<input type="checkbox" class="UStatus" name="UnitStatus">');
                    //}
                    //else {
                    //    $(".Status", row).html('<input type="checkbox" checked class="UStatus" name="UnitStatus">');
                    //}

                    if ($(this).find('OrderAttachment').text() == '0') {
                        $(".Action", row).html("<span OrderAttachment=" + $(this).find('OrderAttachment').text() + "></span>" + '<input type="checkbox" class="clsAction" onclick="funchangeQty(this)" AutoId="' + index + '" id="tblPriceAction" name="tblAction">');
                    }
                    else {
                        $(".Action", row).html("<span OrderAttachment=" + $(this).find('OrderAttachment').text() + "></span>" + '<input type="checkbox" disabled class="clsAction" onclick="funchangeQty(this)" AutoId="' + index + '" id="tblPriceAction" name="tblAction">');
                    }
                    if ($(this).find('DefaultPack').text() == '0') {
                        $(".Default", row).html('<input type="radio" class="clsDefault" name="SetDefault">');
                    }
                    else {
                        $(".Default", row).html('<input type="radio" checked class="clsDefault" name="SetDefault">');
                        localStorage.setItem('Default', index);
                    }
                    if ($(this).find('Free').text() == '0') {
                        $(".Free", row).html('<input type="checkbox" class="clsFree" name="SetFree">');
                    }
                    else {
                        $(".Free", row).html('<input type="checkbox" checked class="clsFree" name="SetFree">');
                    }

                    if ($('#HDDomain').val() != 'psmnj') {
                        if ($(this).find('DefaultPack').text() != '0') {
                            $(".UnitType", row).html($(this).find('UnitName').text() + "<span UnitType=" + $(this).find('UnitType').text() + "></span><b title='Default Packing' style='color:green'> * </b>");
                        }
                        else {
                            $(".UnitType", row).html($(this).find('UnitName').text() + "<span UnitType=" + $(this).find('UnitType').text() + "></span>");
                        }
                    }
                    else {
                        $(".UnitType", row).html($(this).find('UnitName').text() + "<span UnitType=" + $(this).find('UnitType').text() + "></span>");
                    }
                    $(".Qty", row).html($(this).find('Qty').text() + '<span DefaultPack="' + $(this).find('DefaultPack').text() + '" PackingId="' + $(this).find('PackingId').text() + '"></span>');

                    if ($(this).find('UnitType').text() == '3') {
                        $(".NewQty", row).html("<input type='text' style='width:60px;text-align:center' class='form-control input-sm' maxlength='4' onchange='funchangeprice(this)' onkeypress='return isNumberDecimalKey(event,this)' disabled  value='" + $(this).find('Qty').text() + "'/>");
                    } else {
                        $(".NewQty", row).html("<input type='text' style='width:60px;text-align:center' class='form-control input-sm' maxlength='4' onchange='funchangeQty(this)' onkeypress='return isNumberDecimalKey(event,this)'  value='" + $(this).find('Qty').text() + "'/>");
                    }

                    if ($(this).find('UnitType').text() == '3') {
                        $(".NewCosePrice", row).html("<input type='text' style='width:80px;text-align:right' onchange='funchangeprice(this)' maxlength='10' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm UnitNewCost'  value='" + $(this).find('CostPrice').text() + "'/>");
                    } else {
                        $(".NewCosePrice", row).html("<input type='text' style='width:80px;text-align:right' onchange='funchangeprice(this)' maxlength='10' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm'  value='" + $(this).find('CostPrice').text() + "'/>");
                    }
                    $(".CostPrice", row).html($(this).find('CostPrice').text());
                    $(".MinPrice", row).html($(this).find('MinPrice').text());
                    $(".BasePrice", row).html($(this).find('Price').text());
                    if ($(this).find('UnitType').text() == '3') {
                        $(".NewBasePrice", row).html("<input type='text' style='width:80px;text-align:right' onchange='funchangeprice(this)' maxlength='10' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm UnitBasePrice'  value='" + $(this).find('Price').text() + "'/>");
                    } else {
                        $(".NewBasePrice", row).html("<input type='text' style='width:80px;text-align:right' onchange='funchangeprice(this)' maxlength='10' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm'  value='" + $(this).find('Price').text() + "'/>");
                    }
                    $(".RetailPrice", row).html($(this).find('MinPrice').text());
                    if ($(this).find('UnitType').text() == '3') {
                        $(".NewRetailPrice", row).html("<input type='text' style='width:80px;text-align:right' onchange='funchangeprice(this)' maxlength='10' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm UnitRetailPrice'  value='" + $(this).find('MinPrice').text() + "'/>");
                    } else {
                        $(".NewRetailPrice", row).html("<input type='text' style='width:80px;text-align:right' onchange='funchangeprice(this)' maxlength='10' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm'  value='" + $(this).find('MinPrice').text() + "'/>");
                    }
                    $(".WHminPrice", row).html($(this).find('WHminPrice').text());
                    if ($(this).find('UnitType').text() == '3') {
                        $(".NWHPrice", row).html("<input type='text' style='width:80px;text-align:right'  onchange='funchangeprice(this)' maxlength='10'  onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm UnitWHPrice'  value='" + $(this).find('WHminPrice').text() + "'/>");
                    } else {
                        $(".NWHPrice", row).html("<input type='text' style='width:80px;text-align:right'  onchange='funchangeprice(this)' maxlength='10'  onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm'  value='" + $(this).find('WHminPrice').text() + "'/>");
                    }
                    $(".Location", row).html("<input type='text' class='first' maxlength='1' onchange='alphaOnly(this)' style='width:50px;text-align:center;text-transform:uppercase'  value='" + $(this).find('Rack').text() + "'/>-" +
                        "<input type = 'text' class='second' maxlength = '2' onkeypress='return isNumberKey(event)' style='width:50px;text-align:center' value = '" + $(this).find('Section').text() + "' />-" +
                        "<input type = 'text' class='third' maxlength = '2' onkeypress='return isNumberKey(event)' style='width:50px;text-align:center' value = '" + $(this).find('Row').text() + "' />-" +
                        "<input type='text' class='fourth' maxlength='3' onkeypress='return isNumberKey(event)' style='width:50px;text-align:center'   value='" + $(this).find('BoxNo').text() + "'/>");
                    $("#Table1 tbody").append(row);
                    row = $("#Table1 tbody tr:last-child").clone();
                });
                $("#Table1 tbody tr").show();
                $("#Table1 tbody tr").each(function () {
                    if (parseFloat($(this).find('.CostPrice').text()) != "0.00" && parseFloat($(this).find('.BasePrice').text()) != "0.00") {
                        if ($(this).find('.UnitType span').attr("UnitType") == "3") {
                            $(this).find(".clsAction").attr("checked", true);
                            $(this).find(".clsAction").attr("disabled", true);
                        }
                        else {
                            $(this).find(".clsAction").attr("checked", true);
                        }
                    }
                    if ($(this).find('.UnitType span').attr("UnitType") == "3") {
                        $(this).find(".clsAction").attr("checked", true);
                        $(this).find(".clsAction").attr("disabled", true);
                    }
                    if ($(this).find('.Action input').prop('checked') == false) {
                        $(this).find('.NewQty input').attr('disabled', true);
                        $(this).find('.NewCosePrice input').attr('disabled', true);
                        $(this).find('.NewBasePrice input').attr('disabled', true);
                        $(this).find('.NewRetailPrice input').attr('disabled', true);
                        $(this).find('.NWHPrice input').attr('disabled', true);
                        $(this).find(".Default input").attr("disabled", true);
                        $(this).find(".Free input").attr("disabled", true);
                        $(this).find(".Location input").attr("disabled", true);
                        $(this).find(".Status input").attr("disabled", true);
                    }
                    else {
                        $(this).find(".Status input").removeAttr("disabled");
                    }
                });

                if ($('#HDDomain').val() == 'psmnj' || $('#HDDomain').val() == 'psm') {
                    $(".Action").removeAttr("Style");
                    $(".Default").removeAttr("Style");
                    $(".Free").removeAttr("Style");
                    $("#managePrice .modal-dialog").attr('style', 'min-width: 1250px !important;');

                }
                else {
                    $(this).find(".clsAction").attr("checked", true);
                    $(this).find(".clsAction").attr("disabled", true);
                    $("#Table1 tbody .Qty").attr("colspan", "2");
                    $("#Table1 tbody .NewQty").hide();
                    $("#Table1 thead #fqty").attr("rowspan", "2");
                    $("#Table1 thead #fqty").html("Quantity");
                    $("#Table1 thead #sqty").hide();
                    $("#Table1 thead #snqty").hide();
                    $("#managePrice .modal-dialog").attr('style', 'min-width: 1044px !important;');
                }

                $(".first").keydown(function (event) {
                    if (event.keyCode == 32) {
                        event.preventDefault();
                    }
                });

            } else {
                location.href = '/';
            }
        },
        error: function (result) {
        },
        failure: function (result) {
        }
    });
    if ($('#Hd_Domain').val().toLowerCase() != 'psmnj' && $('#Hd_Domain').val().toLowerCase() != "psmnjm" && $('#Hd_Domain').val().toLowerCase() != "localhost" && $('#Hd_Domain').val().toLowerCase() != "demo" && $('#Hd_Domain').val().toLowerCase() != "psm") {
        $('.QtyPop').hide();
    }
    $('#managePrice').modal('show');
}
function checkDefault() {
    var chk = false, prc = false;
    var tqty = 0, checkAuto = 0, autId = 0, autId1 = 0, autId2 = 0, OrderAttachement = 0, OrderAttachement1 = 0, OrderAttachement2 = 0, Setdefault = 0, checkd = 0, loc = 0, chkloc = 0;
    $("#Table1 tbody tr").each(function () {

        if ($(this).find('.Action input').prop('checked') == true) {
            var basePrice = Number($(this).find('.NewCosePrice input').val());
            if (basePrice == 0 || basePrice == null) {
                prc = true;
                $(this).find('.NewCosePrice input').addClass('border-warning');
                $(this).find('.NewBasePrice input').addClass('border-warning');
                $(this).find('.NewRetailPrice input').addClass('border-warning');
                $(this).find('.NWHPrice input').addClass('border-warning');
            }
            checkAuto = Number($(this).find('.Action input').attr("autoid"));

            if (checkAuto == 0) {
                autId = Number($(this).find('.NewQty input').val());;
                OrderAttachement = Number($(this).find('.Action span').attr("OrderAttachment"));
            }
            else if (checkAuto == 1) {
                autId1 = Number($(this).find('.NewQty input').val());;
                OrderAttachement1 = Number($(this).find('.Action span').attr("OrderAttachment"));
            }
            else if (checkAuto == 2) {
                autId2 = Number($(this).find('.NewQty input').val());;
                OrderAttachement2 = Number($(this).find('.Action span').attr("OrderAttachment"))
            }

        }

        var totalQty = Number($(this).find('.NewQty input').val());
        if (totalQty == 0 || totalQty == null) {
            chk = true;
            $(this).find('.NewQty input').addClass('border-warning');
        }
        //if ($(this).find('.Status input').prop('checked') == true) {
        if ($(this).find('.Default input').prop("checked") == true) {
            Setdefault += Setdefault + 1;
        }
        //}
        loc = 0;
        if ($(this).find('.Location input.first').val().trim() != '') {
            loc = 1;
        }
        if ($(this).find('.Location input.second').val().trim() != '') {
            loc = Number(loc) + 1;
        }
        if ($(this).find('.Location input.third').val().trim() != '') {
            loc = Number(loc) + 1;
        }
        if ($(this).find('.Location input.fourth').val().trim() != '') {
            loc = Number(loc) + 1;
        }
        if (Number(loc) != 0) {
            if (Number(loc) < 4) {
                $(this).find('.Location input.first').addClass('border-warning');
                $(this).find('.Location input.second').addClass('border-warning');
                $(this).find('.Location input.third').addClass('border-warning');
                $(this).find('.Location input.fourth').addClass('border-warning');
                chkloc = Number(chkloc) + 1;
            }
            else {
                $(this).find('.Location input.first').removeClass('border-warning');
                $(this).find('.Location input.second').removeClass('border-warning');
                $(this).find('.Location input.third').removeClass('border-warning');
                $(this).find('.Location input.fourth').removeClass('border-warning');
            }
        }
    })
    if (prc) {
        toastr.error('Please manage price before update.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    if (chk) {
        toastr.error('Please enter New Quantity.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }

    if (autId === autId1 === autId2) {
        toastr.error('Please manage quantity.Its cannot be same.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    else if (autId == autId1 && (autId > 0 || autId1 > 0)) {
        toastr.error('Please manage quantity.Its cannot be same.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    else if (autId == autId2 && (autId > 0 || autId2 > 0)) {
        toastr.error('Please manage quantity.Its cannot be same.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    else if (autId1 == autId2 && (autId1 > 0 || autId2 > 0)) {
        toastr.error('Please manage New Quantity.Its cannot be same.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

        return;
    }

    if ((autId == autId1 == autId2) && OrderAttachement != 0 && OrderAttachement1 != 0 && OrderAttachement2 != 0) {
        toastr.error('Please manage New Quantity.Its cannot be same.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

        return;
    }
    else if ((autId == autId1) && OrderAttachement != 0 && OrderAttachement1 != 0) {
        toastr.error('Please manage New Quantity.Its cannot be same.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

        return;
    }
    else if ((autId == autId2) && OrderAttachement != 0 && OrderAttachement2 != 0) {
        toastr.error('Please manage New Quantity.Its cannot be same.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

        return;
    }
    else if ((autId1 == autId2) && OrderAttachement1 != 0 && OrderAttachement2 != 0) {
        toastr.error('Please manage New Quantity.Its cannot be same.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

        return;
    }
    else if (Setdefault == 0) {
        toastr.error('Please set default before update.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    else if (chkloc != 0) {
        toastr.error('Please manage location.All fields required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    var check = 0;
    $("#Table1 tbody tr").each(function () {
        if ($(this).find('.Action input').prop('checked') == true) {
            {
                check += check + 1;
                chk = true;
            }
        }
    });
    if (check == 0) {
        toastr.error('Please check the checkbox to manage price.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    //Validation end
    var checkd = 0, currentDefault = 0;
    var Default = localStorage.getItem("Default");
    $("#Table1 tbody tr").each(function () {
        if ($(this).find('.Action input').prop('checked') == true) {
            if ($(this).find('.Default input').prop('checked') == true && $(this).find('.Qty span').attr("defaultpack") == '0'
                && $(this).find('.Qty span').attr("PackingId") != '0') {
                checkd = 1;
            }
        }
    })
    if ($("#Hd_Domain").val().toUpperCase() != 'PSMNJ') {
        checkd = 0;
    }
    if (checkd == 1) {
        swal({
            title: "Are you sure?",
            text: "You want to change default packing.",
            icon: "warning",
            showCancelButton: true,
            allowOutsideClick: false,
            closeOnClickOutside: false,
            buttons: {
                cancel: {
                    text: "No, Cancel.",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: true,
                },
                confirm: {
                    text: "Yes, Change it.",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
                }
            }
        }).then(function (isConfirm) {
            if (isConfirm) {
                updatebulk();
            }
        })
    }
    else {
        updatebulk();
    }
}
function updatebulk() {

    var ProductAutoId = $("#txtHProductAutoId").val();
    var dataTable = [];
    var chk1 = false; i = 0;
    $('#Table1 tbody tr').each(function (index) {
        if ($(this).find('.Action input').prop('checked') == true) {//By Rizwan Ahmad 11/27/2019

            dataTable[i] = new Object();
            dataTable[i].ProductAutoId = ProductAutoId;
            dataTable[i].UnitAutoId = $(this).find('.UnitType span').attr('UnitType');
            dataTable[i].Qty = $(this).find('.NewQty input').val();
            dataTable[i].CostPrice = $(this).find('.NewCosePrice input').val();
            dataTable[i].BasePrice = $(this).find('.NewBasePrice input').val();
            dataTable[i].RetailPrice = $(this).find('.NewRetailPrice input').val();
            dataTable[i].WHPrice = $(this).find('.NWHPrice input').val();

            if ($(this).find('.Default input').prop("checked") == true) {
                dataTable[i].SETDefault = 1;//Set Default
            }
            else {
                dataTable[i].SETDefault = 0;
            }
            if ($(this).find('.Free input').prop("checked") == true) {
                dataTable[i].SETFree = 1;//Set Free
            }
            else {
                dataTable[i].SETFree = 0;
            }
            //if ($(this).find('.Status input').prop("checked") == true) {
            dataTable[i].Status = 1;
            //}
            //else {
            //    dataTable[i].Status = 0;
            //}
            var sec, rw, bn;
            if ($(this).find('.Location input.second').val().length == 1) {
                sec = '0' + $(this).find('.Location input.second').val();
            }
            else {
                sec = $(this).find('.Location input.second').val();
            }
            if ($(this).find('.Location input.third').val().length == 1) {
                rw = '0' + $(this).find('.Location input.third').val();
            }
            else {
                rw = $(this).find('.Location input.third').val();
            }
            if ($(this).find('.Location input.fourth').val().length == 1) {
                bn = '00' + $(this).find('.Location input.fourth').val();
            }
            else if ($(this).find('.Location input.fourth').val().length == 2) {
                bn = '0' + $(this).find('.Location input.fourth').val();
            } else {
                bn = $(this).find('.Location input.fourth').val();
            }
            dataTable[i].Rack = $(this).find('.Location input.first').val();
            dataTable[i].Section = sec;
            dataTable[i].Row = rw;
            dataTable[i].BoxNo = bn;
            if (parseFloat($(this).find('.NewCosePrice input').val()) > parseFloat($(this).find('.NWHPrice input').val())) {
                chk1 = true;
                $(this).find('.NewCosePrice input').addClass('border-warning');
            } else {
                $(this).find('.NewCosePrice input').removeClass('border-warning');
            }
            if (parseFloat($(this).find('.NWHPrice input').val()) > parseFloat($(this).find('.NewRetailPrice input').val())) {
                chk1 = true;
                $(this).find('.NWHPrice input').addClass('border-warning');
            } else {
                $(this).find('.NWHPrice input').removeClass('border-warning');
            }
            if (parseFloat($(this).find('.NewRetailPrice input').val()) > parseFloat($(this).find('.NewBasePrice input').val())) {
                chk1 = true;
                $(this).find('.NewRetailPrice input').addClass('border-warning');
            } else {
                $(this).find('.NewRetailPrice input').removeClass('border-warning');
            }

            i = Number(i) + 1;
        }
    });
    if (chk1) {
        swal("", "Please check highlighted point.", "error");
        return;
    }

    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/productMaster.asmx/updatebulk",
        data: "{'dataTable':'" + JSON.stringify(dataTable) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d == "true") {
                    $('#managePrice').modal('hide');
                    swal("", "Packing details updated successfully.", "success");
                    editProduct(ProductAutoId);
                } else {
                    swal("Error!", response.d, "error");
                }
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            swal("Error!", 'Oops,some thing went wrong.Please try later.', "error");
        },
        failure: function (result) {
            swal("Error!", 'Oops,some thing went wrong.Please try later.', "error");
        }
    });

}
function funchangeprice(e) {
    var className = $(e).closest('td').attr('class');
    var tr = $(e).closest('tr');
    var UnitAutoId = tr.find('.UnitType span').attr('UnitType');
    var Price = 0.00;
    if ($(e).val() != '') {
        Price = $(e).val() || 0.00;
    }
    var Qty = 1;
    if (tr.find('.NewQty input').val() != '') {
        Qty = tr.find('.NewQty input').val() || 0.00;
    }
    var UnitPrice = 0.00, NewQty = 1; check = 0;
    UnitPrice = parseFloat((parseFloat(Price) / parseInt(Qty))).toFixed(3) || 0.00;
    $("#Table1 tbody tr").each(function () {
        if ($(this).find('.Action input').prop('checked') == true) {
            if ($(this).find('.NewQty input').val() != '') {
                NewQty = $(this).find('.NewQty input').val();
            }
            var amount = parseFloat((NewQty * UnitPrice)).toFixed(3);
            if (UnitAutoId != $(this).find('.UnitType span').attr('UnitType')) {
                if (className == 'NewCosePrice') {
                    $(this).find('.NewCosePrice input').val(parseFloat(amount).toFixed(3));
                } else if (className == 'NewBasePrice') {
                    $(this).find('.NewBasePrice input').val(parseFloat(amount).toFixed(3));
                } else if (className == 'NewRetailPrice') {
                    $(this).find('.NewRetailPrice input').val(parseFloat(amount).toFixed(3));
                } else if (className == 'NWHPrice') {
                    $(this).find('.NWHPrice input').val(parseFloat(amount).toFixed(3));
                }

            }
        }
    });
}
function funchangeQty(e) {
    var tr = $(e).closest('tr');
    var Qty = 1;
    if ($(e).val() != '') {
        if ($(e).val() == "on") {
            Qty = 1
        }
        else {
            Qty = $(e).val();
        }
    }
    if ($(e).val() <= 0) {
        toastr.error('Minimum Qty must greater than 0', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $(e).val($(tr).find('.Qty').html());
        Qty = $(tr).find('.Qty').html();
    }
    if ($(tr).find(".Action input").prop("checked") == true) {
        $(tr).find('.NewQty input').removeAttr('disabled');
        $(tr).find('.NewCosePrice input').removeAttr('disabled');
        $(tr).find('.NewBasePrice input').removeAttr('disabled');
        $(tr).find('.NewRetailPrice input').removeAttr('disabled');
        $(tr).find('.NWHPrice input').removeAttr('disabled');
        $(tr).find(".Default input").removeAttr('disabled');
        $(tr).find(".Free input").removeAttr('disabled');
        $(tr).find(".Location input").removeAttr('disabled');

        var UnitNewCost = $('.UnitNewCost').val() || 0.00;
        var UnitBasePrice = $('.UnitBasePrice').val() || 0.00;
        $(tr).find('.NewCosePrice input').val(parseFloat(parseFloat(UnitNewCost) * parseFloat(Qty)).toFixed(3));
        $(tr).find('.NewBasePrice input').val(parseFloat(parseFloat(UnitBasePrice) * parseFloat(Qty)).toFixed(3));
        UnitBasePrice = $('.UnitRetailPrice').val() || 0.00;
        $(tr).find('.NewRetailPrice input').val(parseFloat(parseFloat(UnitBasePrice) * parseFloat(Qty)).toFixed(3));
        UnitBasePrice = $('.UnitWHPrice').val() || 0.00;
        $(tr).find('.NWHPrice input').val(parseFloat(parseFloat(UnitBasePrice) * parseFloat(Qty)).toFixed(3));
        $(tr).find(".Status input").removeAttr("disabled");
    }
    else {
        $(tr).find('.NewCosePrice input').val("0.000");
        $(tr).find('.NewBasePrice input').val("0.000");
        $(tr).find('.NewRetailPrice input').val("0.000");
        $(tr).find('.NWHPrice input').val("0.000");
        $(tr).find('.NewQty input').val("1");
        $(tr).find('.NewQty input').attr('disabled', true);
        $(tr).find('.NewCosePrice input').attr('disabled', true);
        $(tr).find('.NewBasePrice input').attr('disabled', true);
        $(tr).find('.NewRetailPrice input').attr('disabled', true);
        $(tr).find('.NWHPrice input').attr('disabled', true);
        $(tr).find('.Default input').prop('checked', false);
        $(tr).find('.Default input').attr("disabled", true);
        $(tr).find('.Free input').prop('checked', false);
        $(tr).find(".Free input").attr("disabled", true);
        $(tr).find(".Status input").attr("disabled", true);
        $(tr).find(".Location input").attr("disabled", true);

    }
    $(tr).find('.Location input').val('');
}
function printBarCode(e) {
    console.log($(e).closest('tr').find('td:eq(0)').text())
    var mywindow = window.open('', 'mywindow', 'height=400,width=600');
    var renderer = "css";
    var settings = {
        output: renderer,
    };
    value = { code: $(e).closest('tr').find('td:eq(0)').text(), rect: true };
    var btype = "code128";
    $('#barcode').html(DrawCode39Barcode($(e).closest('tr').find('td:eq(0)').text()));
    mywindow.document.write('<html><head><title></title>');
    mywindow.document.write('</head><body style="width:200px">');
    mywindow.document.write($('#barcode').html());
    mywindow.document.write('</body></html>');
    mywindow.print();
}

function IsNumeric() {
    var data = $("#ddlCommCode").val()
    return parseFloat(data) == data;
}



function setDefault(e) {


    swal({
        title: "Are you sure?",
        text: "You want to set as default image.",
        icon: "warning",
        showCancelButton: true,
        allowOutsideClick: false,
        closeOnClickOutside: false,
        buttons: {
            cancel: {
                text: "No, Cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes !",
                value: true,
                visible: true,
                className: "",
                closeModal: true
            }
        }
    }).then(isConfirm => {
        if (isConfirm) {
            $(".imageThumb").removeClass("defaultImage");
            $(e).addClass("defaultImage");
            $("#defaultImageName").val(e.title);
        }
    })

}

function setDefaultDynamic(e) {


    swal({
        title: "Are you sure?",
        text: "You want to set as default image.",
        icon: "warning",
        showCancelButton: true,
        allowOutsideClick: false,
        closeOnClickOutside: false,
        buttons: {
            cancel: {
                text: "No, Cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes !",
                value: true,
                visible: true,
                className: "",
                closeModal: true
            }
        }
    }).then(isConfirm => {
        if (isConfirm) {
            $(".imageThumb").removeClass("defaultImage");
            $(e).addClass("defaultImage");
            $("#defaultImageName").val(e.title);
            updateDefaultImage(e.title);
        }
    })

}

function deleteStaticProdutcImage(index) {
    swal({
        title: "Are you sure?",
        text: "You want to delete image.",
        icon: "warning",
        showCancelButton: true,
        allowOutsideClick: false,
        closeOnClickOutside: false,
        buttons: {
            cancel: {
                text: "No, Cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes !",
                value: true,
                visible: true,
                className: "",
                closeModal: true
            }
        }
    }).then(isConfirm => {
        if (isConfirm) {
            Imgresponse.splice(index, 1);
            showImageHtml();
            $('#file1').val('');
        }
    })

}

function showImageHtml() {
    var m = 0;
    var htmlimg = `<div class="row tiles">`;
    for (var k in Imgresponse) {
        if (m == 0) {
            var defimg = "defaultImage";
            $("#defaultImageName").val(Imgresponse[k].URL);

        } else {
            var defimg = "";
        }
        if (Imgresponse[k].Status == 0) {
            htmlimg += `<div class="col-xs-2">
                            <div><img class="` + defimg + `" src="/Attachments/` + Imgresponse[k].URL + `" title="` + Imgresponse[k].URL + `" onclick="setDefault(this)"></div>
                            <i class="la la-times" onclick="deleteStaticProdutcImage(` + k + `)"></i></div>`;

        } else {
            htmlimg += `<div class="col-xs-2">
                            <div><img class="` + defimg + `" src="/Attachments/` + Imgresponse[k].URL + `" title="` + Imgresponse[k].URL + `" onclick="setDefault(this)"></div>
                            <i class="la la-times" onclick="deleteProdutcImage(` + $(this).find("AutoId").text() + `)"></i></div>`;
        }




        if (m % 4 == 0 && m != 0) {
            htmlimg += `</div><div class="row tiles">`;

        }


        m++;
    }
    $(".imgbox").html(htmlimg);
}

function updateDefaultImage(defaultImg) {
    var ProductId = $("#txtHProductAutoId").val();
    var data = {
        ProductAutoId: ProductId,
        defaultImg: defaultImg
    };
    $.ajax({
        url: "/Admin/WebAPI/productMaster.asmx/updateDefaultImage",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        success: function (response) {
            if (response.d = "Success") {
                swal("", "Default Image updated successfully.", "success").then(function () {
                    window.location.href = "/Admin/productMaster.aspx?ProductId=" + ProductId;
                });
            }
        },
        error: function (result) {
            console.log(result);
            swal("Error!", 'Oops,some thing went wrong.Please try later.', "error");
        },
        failure: function (result) {
            swal("Error!", 'Oops,some thing went wrong.Please try later.', "error");
        }
    });

}

function readWebURL(input) {
    file = input.files[0];
    if (Number(file.size / (1024 * 1024)) > Number($("#getSize").html())) {
        toastr.error('You are reaching maximum limit. Max limit is ' + $("#getSize").html() + ' MB', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $("#file1").val('');
        return true;
    }

    if (input.files && input.files[0] && input.files[0].name.match(/\.(jpg|JPG|JPEG|jpeg|png|PNG)$/)) {
        var reader = new FileReader();
        reader.onload = function (e) {
            SaveWebsiteDetails();
        }
        reader.readAsDataURL(input.files[0]);
    }
    else {
        toastr.error('Invalid file. Allowed file type is :[png,jpg,jpeg]', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $("#file1").val('');
    }
}

function SaveWebsiteDetails() {
    var ProductId = $("#txtProductId").val();
    var fileUpload = $("#file1").get(0);
    var files = fileUpload.files;
    var domain = $('#HDDomain').val();
    var test = new FormData();
    if (files.length == 0) {
        toastr.error('Select atleast one images.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    for (var i = 0; i < files.length; i++) {
        test.append(files[i].name, files[i]);
    }

    $.ajax({
        url: "/admin/WebsiteImageUploadHandler.ashx?timestamp=" + ProductId,
        type: "POST",
        contentType: false,
        processData: false,
        data: test,
        success: function (response) {
            var img1 = JSON.parse(response);
            for (var j in img1) {
                Imgresponse.push({
                    'URL': img1[j].URL,
                    'Thumb100': img1[j].Thumb100,
                    'Thumb400': img1[j].Thumb400,
                    'Status': 0
                });
            }
            var htmlimg = `<div class="row tiles">`;
            var m = 0;
            for (var k in Imgresponse) {
                if (m == 0) {
                    var defimg = "defaultImage";
                    $("#defaultImageName").val(Imgresponse[k].URL);

                } else {
                    var defimg = "";
                }
                if (Imgresponse[k].Status == 0) {
                    htmlimg += '<div class="col-xs-2">'
                    htmlimg += '<div ><img class="' + defimg + '" src="/Attachments/' + Imgresponse[k].URL + '" title="' + Imgresponse[k].URL + '" Thumb100="' + Imgresponse[k].Thumb100 + '" Thumb400="' + Imgresponse[k].Thumb400 + '" onclick="setDefault(this)"></div>'
                    if (domain == "psmnj") {
                        htmlimg += '<i class="la la-times" onclick="deleteStaticProdutcImage(` + k + `)"></i></div >'
                    }
                } else {
                    htmlimg += '<div class="col-xs-2">'
                    htmlimg += '<div><img class="' + defimg + '" src="/Attachments/' + Imgresponse[k].URL + '" title="' + Imgresponse[k].URL + '" Thumb100="' + Imgresponse[k].Thumb100 + '" Thumb400="' + Imgresponse[k].Thumb400 + '" onclick="setDefault(this)"></div>'
                    if (domain == "psmnj") {
                        htmlimg += '<i class="la la-times" onclick="deleteProdutcImage(` + $(this).find(" AutoId").text() + `)"></i></div>';
                    }
                }
                if (m % 4 == 0 && m != 0) {
                    htmlimg += '</div > <div class="row tiles">';
                }
                m++;
            }

            $(".imgbox").html(htmlimg);
            $("#file1").val('');
        },
        error: function (result) {
            swal("Error!", 'Oops,some thing went wrong.Please try later.', "error");
        },
        failure: function (result) {
            swal("Error!", 'Oops,some thing went wrong.Please try later.', "error");
        }
    });
}

function updateProductDetail() {
    var ProductAutoId = $("#txtHProductAutoId").val();
    if ($('#IsShowOnWebsite1').prop('checked') === true) {
        IsShowOnWeb = 1
    } else {
        IsShowOnWeb = 0
    }
    if ($('#CheckboxOOS').prop('checked') === true) {
        CheckboxOOS = 1
    } else {
        CheckboxOOS = 0
    }
    if (checkrequiredProduct()) {
        var data = {
            IsShowOnWebsite: IsShowOnWeb,
            CheckboxOOS: CheckboxOOS,
            txtDescription: $("#txtDescription").val(),
            ProductAutoId: ProductAutoId
        }
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/productMaster.asmx/savewebsiteDetails",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d == "false") {
                    swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                    return;
                }
                if (response.d == "Success") {
                    swal("", "Product details saved successfully.", "success").then(function () {
                        window.location.href = "/Admin/productMaster.aspx?ProductId=" + ProductAutoId;
                    });
                } else {
                    location.href = '/';
                }
            },
            error: function (result) {
                swal("Error!", "Product details not saved", "error");
            },
            failure: function (result) {
                swal("Error!", 'Oops,something went wrong.Please try again.', "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}



function deleteProdutcImage(ImageAutoId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete image.",
        icon: "warning",
        showCancelButton: true,
        allowOutsideClick: false,
        closeOnClickOutside: false,
        buttons: {
            cancel: {
                text: "No, Cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes,delete it !",
                value: true,
                visible: true,
                className: "",
                closeModal: true
            }
        }
    }).then(isConfirm => {
        if (isConfirm) {
            removeProdutcImage(ImageAutoId);
        }
    })

}

function removeProdutcImage(ImageAutoId) {
    var ProductId = $("#txtHProductAutoId").val();
    var data = {
        ProductAutoId: ProductId,
        ImageAutoId: ImageAutoId

    };
    $.ajax({
        url: "/Admin/WebAPI/productMaster.asmx/removeProdutcImage",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        success: function (response) {
            if (response.d = "Success") {
                swal("", "Image deleted successfully.", "success").then(function () {
                    window.location.href = "/Admin/productMaster.aspx?ProductId=" + ProductId;
                });
            }
        },
        error: function (result) {
            console.log(result);
            swal("Error!", 'Oops,some thing went wrong.Please try later.', "error");
        },
        failure: function (result) {
            swal("Error!", 'Oops,some thing went wrong.Please try later.', "error");
        }
    });


}


function updateProductImage() {
    if (Imgresponse.length == 0) {
        toastr.error('Select atleast one images.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    var ProductAutoId = $("#txtHProductAutoId").val();

    if ($("#defaultImageName").val() == '') {
        toastr.error('Select atleast one default images.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    if (checkrequiredProduct()) {
        var data = {
            ImageUrl: JSON.stringify(Imgresponse),
            defaultImg: $("#defaultImageName").val(),
            ProductAutoId: ProductAutoId
        }
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/productMaster.asmx/updateImageDetails",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d == "false") {
                    swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                    return;
                }
                else if (response.d == "Success") {
                    swal("", "Product image saved successfully.", "success").then(function () {
                        window.location.href = "/Admin/productMaster.aspx?ProductId=" + ProductAutoId;
                    });
                } else {
                    swal("Error!", response.d, "error");
                }
            },
            error: function (result) {
                swal("Error!", "Product image not saved", "error");
            },
            failure: function (result) {
                swal("Error!", 'Oops,something went wrong.Please try again.', "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}

function showConfirmBox() {
    if ($("#IsShowOnWebsite1").is(":checked")) {
        var msg = "You want to show this product on website.";
        var status = 1;
    } else {
        var msg = "You want to hide this product from website.";
        var status = 0;
    }
    swal({
        title: "Are you sure?",
        text: msg,
        icon: "warning",
        showCancelButton: true,
        allowOutsideClick: false,
        closeOnClickOutside: false,
        buttons: {
            cancel: {
                text: "No, Cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes !",
                value: true,
                visible: true,
                className: "",
                closeModal: true
            }
        }
    }).then(isConfirm => {
        if (isConfirm) {
            if (status == 0) {
                $('#IsShowOnWebsite1').prop('checked', false);
            } else {
                $('#IsShowOnWebsite1').prop('checked', true);
            }
        } else {
            if (status == 0) {
                $('#IsShowOnWebsite1').prop('checked', true);
            } else {
                $('#IsShowOnWebsite1').prop('checked', false);
            }
        }
    })
}

var securityCount = 0;
function showSecurityModal(val) {
    if (val == 1 && securityCount == 0) {
        $('#boxId').val(1);
        var mainclass = $('#ftclass').attr('class');
        if (mainclass == "ft-plus") {
            $('#SecurityEnabled').modal('show');
            $("#ftclass").removeClass("ft-minus");
        } else {
            $("#ftclass").addClass("ft-plus");
            $("#ftclass").removeClass("ft-minus");
            $("#websiteContent").hide();
        }
    } else if (val == 2 && securityCount == 0) {
        $('#boxId').val(2);
        var mainclass1 = $('#ftclass1').attr('class');
        if (mainclass1 == "ft-plus") {
            $('#SecurityEnabled').modal('show');
            $("#ftclass1").removeClass("ft-minus");
        } else {
            $("#ftclass1").addClass("ft-plus");
            $("#ftclass1").removeClass("ft-minus");
            $("#imageContent").hide();
        }
    }
    else if (val == 1 && securityCount == 1) {
        var mainclass = $('#ftclass').attr('class');
        if (mainclass == "ft-plus") {
            $("#ftclass").removeClass("ft-plus");
            $("#ftclass").addClass("ft-minus");
            $("#websiteContent").show();
        } else {
            $("#ftclass").addClass("ft-plus");
            $("#ftclass").removeClass("ft-minus");
            $("#websiteContent").hide();
        }
    }
    else if (val == 2 && securityCount == 1) {
        var mainclass1 = $('#ftclass1').attr('class');
        if (mainclass1 == "ft-plus") {
            $("#ftclass1").removeClass("ft-plus");
            $("#ftclass1").addClass("ft-minus");
            $("#imageContent").show();
        } else {
            $("#ftclass1").addClass("ft-plus");
            $("#ftclass1").removeClass("ft-minus");
            $("#imageContent").hide();
        }
    }

    $("#txtSecurity").val('');
    $("#txtSecurity").removeAttr('disabled');
    $("#txtSecurity").focus();

}
function clickonSecurity() {
    var vals = $('#boxId').val();
    $.ajax({
        type: "Post",
        url: "/Admin/WebAPI/productMaster.asmx/clickonSecurity",
        data: "{'CheckSecurity':'" + $("#txtSecurity").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d == 'true') {
                    PackerSecurityEnable = true;
                    securityCount = securityCount + 1;
                    $("#SecurityEnabled").modal('hide');

                    if (vals == 1) {
                        $("#ftclass").removeClass("ft-plus");
                        $("#ftclass").addClass("ft-minus");
                        $("#websiteContent").show();
                    } else {
                        $("#ftclass1").removeClass("ft-plus");
                        $("#ftclass1").addClass("ft-minus");
                        $("#imageContent").show();
                    }
                } else {
                    $('#SecurityEnvalid').modal('show');
                    $('#Sbarcodemsgg').html('Invalid Security Key.');
                    $("#txtSecurity").val('');
                    $("#websiteContent").hide();
                    $("#imageContent").hide();

                }
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function ClosePop() {
    $('#SecurityEnvalid').modal('hide');
}
function alphaOnly(event) {
    var key = event.keyCode;
    return ((key >= 65 && key <= 90) || key == 8);
};