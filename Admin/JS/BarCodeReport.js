﻿$(document).ready(function () {
    bindCategory();
});

function Pagevalue(e) {
    getBarcodeReport(parseInt($(e).attr("page")));
};

/*---------------------------------------------------Bind Category-----------------------------------------------------------*/
function bindCategory() {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/productList.asmx/bindCategory",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        asyn: false,
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var category = $(xmldoc).find("Table");

                    $("#ddlSCategory option:not(:first)").remove();
                    $.each(category, function () {
                        $("#ddlSCategory").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("CategoryName").text() + "</option>");
                    });
                    $("#ddlSCategory").select2();
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

/*-------------------------------------------------------Bind Subcategory-----------------------------------------------------------*/
function Category() {

    var categoryAutoId = $("#ddlSCategory").val();
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/productList.asmx/bindSubcategory",
        data: "{'categoryAutoId':'" + categoryAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var subcategory = $(xmldoc).find("Table");
                    $("#ddlSSubcategory option:not(:first)").remove();
                    $.each(subcategory, function () {
                        $("#ddlSSubcategory").append("<option value=' " + $(this).find("AutoId").text() + "'>" + $(this).find("SubcategoryName").text() + "</option>");
                    });
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

/*-------------------------------------------------------Get Bar Code Report----------------------------------------------------------*/
function getBarcodeReport(PageIndex) {
    var data = {
        CategoryAutoId: $("#ddlSCategory").val(),
        SubcategoryAutoId: $("#ddlSSubcategory").val(),
        ProductId: $("#txtSProductId").val().trim(),
        ProductName: $("#txtSProductName").val().trim(),
        PageSize: $("#ddlPageSize").val(),
        pageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/ProductList.asmx/getBarcodeReport",
        //data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successGetProductBarCode,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function successGetProductBarCode(response) {
    if (response.d != "Session Expired") {
        if(response.d != "false"){
            var xmldoc = $.parseXML(response.d);
        var productBarcode = $(xmldoc).find("Table1");

        $("#tblProductList tbody tr").remove();

        var row = $("#tblProductList thead tr").clone(true);
        if (productBarcode.length > 0) {
            $("#EmptyTable").hide();
            $.each(productBarcode, function () {

                $(".ProductId", row).text($(this).find("ProductId").text());
                $(".Category", row).text($(this).find("Category").text());
                $(".SubCategory", row).text($(this).find("Subcategory").text());
                $(".ProductName", row).text($(this).find("ProductName").text().replace(/&quot;/g, "\'"));
                $(".Unit", row).text($(this).find("Unit").text());
                $(".barcode", row).html($(this).find("barcode").text());
                $("#tblProductList tbody").append(row);
                row = $("#tblProductList tbody tr:last").clone(true);
            });
        } else {
            $("#EmptyTable").show();
        }

        var pager = $(xmldoc).find("Table");
        $(".Pager").ASPSnippets_Pager({
            ActiveCssClass: "current",
            PagerCssClass: "pager",
            PageIndex: parseInt(pager.find("PageIndex").text()),
            PageSize: parseInt(pager.find("PageSize").text()),
            RecordCount: parseInt(pager.find("RecordCount").text())
        });
    }
    }
    else {
        location.href = "/";
    }
}

/*---------------Export To Excel---------------------------*/
function Export() {
    if ($('#tblProductList tbody tr').length > 0) {
    var data = {
        CategoryAutoId: $("#ddlSCategory").val(),
        SubcategoryAutoId: $("#ddlSSubcategory").val(),
        ProductId: $("#txtSProductId").val(),
        ProductName: $("#txtSProductName").val(),
    };
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/ProductList.asmx/getBarcodeReportExport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if(response.d != "false"){
                    var xmldoc = $.parseXML(response.d);
                var ExportBarCode = $(xmldoc).find("Table");
                $("#tblProductListExport tbody tr").remove();
                if (ExportBarCode.length > 0) {
                    var row1 = $("#tblProductListExport thead tr:first").clone(true);
                    $.each(ExportBarCode, function () {
                        $("td", row1).eq(0).text($(this).find("Category").text());
                        $("td", row1).eq(1).text($(this).find("Subcategory").text());
                        $("td", row1).eq(2).text($(this).find("ProductId").text());
                        $("td", row1).eq(3).text($(this).find("ProductName").text().replace(/&quot;/g, "\'"));
                        $("td", row1).eq(4).html($(this).find("barcode").text());
                        $("#tblProductListExport").append(row1);
                        row1 = $("#tblProductListExport tbody tr:last-child").clone(true);
                    });
                }
                $("#LblDate").text(new Date().toLocaleDateString());
                //$("#LblCategory").text($("#ddlSCategory option:selected").text());
                //$("#LblSubCategory").text($("#ddlSSubcategory option:selected").text());
                //$("#LblProductId").text($("#txtSProductId").val());
                //$("#LblProductName").text($("#txtSProductName").val());
                //$("#LblFromDate").html($("#txtSFromdate").val());
                //$("#LblToDate").html($("#txtSTodate").val());

                $("#tblProductListExport").table2excel({
                    exclude: ".noExl",
                    name: "Excel Document Name",
                    filename: "Product Bar Code Report ",
                    fileext: ".xls",
                    exclude_img: true,
                    exclude_links: true,
                    exclude_inputs: true
                });
            }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
};

/*---------------Print Bar Code---------------------------*/
function PrintElem() {
    if ($('#tblProductList tbody tr').length > 0) {
    var row1 = "";
    var data = {
        CategoryAutoId: $("#ddlSCategory").val(),
        SubcategoryAutoId: $("#ddlSSubcategory").val(),
        ProductId: $("#txtSProductId").val(),
        ProductName: $("#txtSProductName").val(),
    };
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/ProductList.asmx/getBarcodeReportExport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            debugger
            if(response.d != "Session Expired"){
                if(response.d != "false"){
                var xmldoc = $.parseXML(response.d);
                var ExportBarCode = $(xmldoc).find("Table");
            
                if (ExportBarCode.length > 0) {

                    row1 = "<table style='width:1000px'> <tbody>"
                    $.each(ExportBarCode, function (index) {
                     
                        if (index % 3 == 0)
                        {
                            row1 += "<tr>";
                        }
                        row1 += "<td style='text-align:center'>" + "<div style='font-size:9px;'>" + $(this).find("ProductName").text().replace(/&quot;/g, "\'") + "</div>" + "</br>" + DrawCode39Barcode($(this).find("barcode").text()) + "</td>";
                        if (index % 3 == 2) {
                            row1 += "</tr>";                    
                        }
                        $("#barcodeprint").html('');

                    });
                    row1 += "</table> </tbody>"

                    var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                    mywindow.document.write('<html><head><title></title>');
                    mywindow.document.write('</head><body >'); 
                    mywindow.document.write(row1);
                    mywindow.document.write('</body></html>'); 
                    mywindow.print(); 
                }
            }
        }
    else{
    location.href = "/";
    }
        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}

 