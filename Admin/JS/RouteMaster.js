﻿
var PageId = 0;
$(document).ready(function () {
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    PageId = getQueryString('PageId');
    bindSalePerson();
    if (PageId != null) {
        $("#lblrouteid").text(PageId);
        $("#btnUpdate").show();
        $("#btnCancel").show();
        $("#btnSave").hide();
        $("#btnReset").hide();
        $("#divaddzipcode").show();
        editZone();
    } else {
        $("#btnUpdate").hide();
        $("#btnCancel").hide();
        $("#divaddzipcode").hide();
        $("#btnSave").show();
        $("#btnReset").show();
    }
   
});
$(".close").click(function () {
    $(".alert").hide();
});
$("#btnSearch").click(function () {
    editZone();
})
function editZone() {
    $.ajax({
        type: "POST",
        async:false,
        url: "WebAPI/RouteMaster.asmx/EditRouteMaster",
        data: "{'AutoId':'" + $("#lblrouteid").text().trim() + "','Salesperson':'" + $("#ddlSalePerson").val() + "','CustomerDetail': '" + $("#ddlSalesPersonSearch").val() + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var RouteDetail = $(xmldoc).find("Table1");
            var Customer = $(xmldoc).find("Table");
            var CustomerDetail = $(xmldoc).find("Table2");
            $("#txtRouteId").val($(RouteDetail).find("RouteId").text());
            $("#txtRouteName").val($(RouteDetail).find("RouteName").text());
            $("#ddlStatus").val($(RouteDetail).find("Status").text()).change();
            var EmpType = $(RouteDetail).find("EmpType").text();
            $("#ddlSalePerson").val($(RouteDetail).find("SalesPersonAutoId").text()).change();
            $("#ddlSalesPersonSearch option:not(:first)").remove();
            $.each(Customer, function () {
                $("#ddlSalesPersonSearch").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("Customer").text() + "</option>");
            });
            $("#ddlSalesPersonSearch").select2();
            $("#tblRouteCustomer tbody tr").remove();
            var row = $("#tblRouteCustomer thead tr").clone(true);
            if (CustomerDetail.length > 0) {
                $("#tblRouteCustomer").show();
                $("#EmptyTable").hide();
                var flag = true;
                $.each(CustomerDetail, function (index) {
                    $(".Id", row).text($(this).find("AutoId").text());
                    $(".Customer", row).text($(this).find("Customer").text());
                    if ($(this).find("SelectStatus").text() == 0) {
                        $(".SelCheckbox", row).html("<input type='checkbox' class='zipcheckbox' onclick='updateCustomerRoute(\"" + $(this).find("AutoId").text() + "\",this)'  />");
                    }
                    else {
                        flag = false;
                        $("#ddlSalePerson").prop("disabled", true);
                        $(".SelCheckbox", row).html('<input type="checkbox" class="zipcheckbox" checked="true"  onclick="updateCustomerRoute(' + $(this).find("AutoId").text() + ',this)" />');
                    }
                    $("#tblRouteCustomer tbody").append(row);
                    row = $("#tblRouteCustomer tbody tr:last").clone(true);
                });
                if (flag) {
                    if (EmpType != '2') {
                        $("#ddlSalePerson").prop("disabled", false);
                    }
                }
                var heightOfTable = Number(CustomerDetail.length) * 26;
                if (heightOfTable > 260)
                {
                    heightOfTable = "341px";
                    $(".table-responsive").attr('style', 'overflow:scroll;overflow-x:hidden; ');
                }
                else {
                    heightOfTable = Number(heightOfTable) + 52;
                    heightOfTable = heightOfTable.toString() + "px";
                }
                $(".table-responsive").css('height', heightOfTable);
                $("#liPageTitle").html('Update Route');
            } else {
                $("#EmptyTable").show();
                $("#tblRouteCustomer").hide();
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function bindSalePerson() {
    $.ajax({
        type: "POST",
        url: "WebAPI/RouteMaster.asmx/GetdSalesPerson",
        data: "{}",
        async: false,
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == 'session') {
                location.location = '/';
            } else {
                var xmldoc = $.parseXML(response.d);
                var GetSalePerson = $(xmldoc).find("Table");
                $("#ddlSalePerson option:not(:first)").remove();
                $.each(GetSalePerson, function () {
                    $("#ddlSalePerson").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("SalesPerson").text() + "</option>");
                });
                if ($('#hdEmpType').val() != '0') {
                    $("#ddlSalePerson").val($('#hdEmpType').val());
                    $("#ddlSalePerson").attr('disabled', true);
                }
                $("#ddlSalePerson").select2();
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

$("#btnSave").click(function () {
    var validatecheck = dynamicInputTypeSelect2('ddlreq');
    if (!validatecheck) {
        dynamicALL('req')
    } else {
        validatecheck = dynamicALL('req');
    }
    if (validatecheck) {
        var data = {
            RouteName: $("#txtRouteName").val().trim(),
            SalesPersonID: $("#ddlSalePerson").val() || 0,
            Status: $("#ddlStatus").val()
        }
        $.ajax({
            type: "POST",
            url: "WebAPI/RouteMaster.asmx/insertRoute",
            //data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == "session") {
                    location.href = '/';
                } else if (result.d == "false") {
                    swal("Error!", "Oops! something wrong.Please try later.", "error");
                }
                else if (result.d == "Exists") {
                    swal("", "Route Name already exists.", "error");
                }
                else {
                    var xmldoc = $.parseXML(result.d);
                    var AutoId = $(xmldoc).find("Table");                  
                    $("#txtRouteId").val($(AutoId).find("AutoId").text());
                    $("#lblrouteid").text($(AutoId).find("routeid").text());
                    swal("", "Route details saved successfully", "success").then(function () {                       
                        location.href = "/Admin/RouteEntry.aspx?PageId=" + $(AutoId).find("routeid").text();
                        editZone();
                    });                                    
                }
            },
            error: function (result) {
                swal("Error!", "Route already exists.", "error");
            },
            failure: function (result) {

            }
        });
    }else
    {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
});

$("#btnUpdate").click(function () {
    var validatecheck = dynamicInputTypeSelect2('ddlreq');
    if (!validatecheck) {
        dynamicALL('req')
    } else {
        validatecheck = dynamicALL('req');
    }
    if (validatecheck) {
        var data = {
            AutoId: $("#lblrouteid").text().trim(),
            RouteName: $("#txtRouteName").val().trim(),
            SalesPersonID: $("#ddlSalePerson").val() || 0,
            Status: $("#ddlStatus").val()
        }
        $.ajax({
            type: "POST",
            url: "WebAPI/RouteMaster.asmx/UpateRoute",
            //data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == "session") {
                    location.location = '/';
                } else if (result.d == "false") {
                    swal("Error!", result.d, "error");
                }
                else if (result.d == "Exists") {
                    swal("", "Route Name already exists.", "error");
                }
                else if (result.d == "success") {
                    swal("", "Route details updated successfully.", "success");
                    $("#divaddzipcode").show();
                    editZone();
                }
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
                swal("Error!", "Route name already exists.", "error");
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    } else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
});

function updateCustomerRoute(b,e) 
{
    var data = {
        AutoId: $("#lblrouteid").text().trim(),
        custid: b
    }
    $.ajax({
        type: "POST",
        url: "WebAPI/RouteMaster.asmx/updateCheckCustomer",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (result) {
            if (result.d == "success") {
                if ($(e).prop('checked')) {
                    swal("", "Customer assigned to this route successfully.", "success");
                    $("#ddlSalePerson").prop("disabled", true);
                }
                else {
                    swal("", "Customer removed from this route successfully.", "success");
                    $("#ddlSalePerson").prop("disabled", false);
                    editZone();
                }
            }
            else if (result.d == "session")
            {
                location.href = "/";
            }
            else
            {
                swal("Error!", result.d, "error");
            }
        },
        error: function (result) {
            swal("Error!", result.d, "error");
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
};

$("#btnReset").click(function () {
    reset();
});
function reset() {
    $("#txtRouteName").val('');
    $("#ddlStatus").val(1);
    if ($("#hdEmpType").val() == '2') {
        $("#ddlSalePerson").val(0).change();
        $("#ddlSalePerson").prop("disabled", false);
    }
    $("#txtRouteId").val('');
    $("#alertDanger").hide();
    $("#alertSuccess").hide();
    RequredValidatorDisable();
}
$(".close").click(function () {
    $(".alert").hide();
});
$("#btnCancel").click(function () {
    window.location.replace("RouteList.aspx");
})

function SalesPersonChange()
{
    var PageIds = PageId || 0;
    if (PageIds != null && PageIds != "0") {
        $.ajax({
            type: "POST",
            url: "WebAPI/RouteMaster.asmx/GetCustomerBySalesPerson",
            data: "{'AutoID':'" + $("#ddlSalePerson").val() + "','RouteId':'" + PageIds + "'}",
            async: false,
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d == 'session') {
                    location.location = '/';
                } else {
                    var xmldoc = $.parseXML(response.d);
                    var GetCustomer = $(xmldoc).find("Table");
                    var CustomerList = $(xmldoc).find("Table1");
                    var EmpType = $(xmldoc).find("Table2");
                    $("#ddlSalesPersonSearch option:not(:first)").remove();
                    $.each(GetCustomer, function () {
                        $("#ddlSalesPersonSearch").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("Customer").text() + "</option>");
                    });
                    $("#ddlSalesPersonSearch").select2();


                    $("#tblRouteCustomer tbody tr").remove();
                    var row = $("#tblRouteCustomer thead tr").clone(true);
                    if (CustomerList.length > 0) {
                        $("#tblRouteCustomer").show();
                        $("#EmptyTable").hide();
                        var flag = true;
                        $.each(CustomerList, function (index) {
                            $(".Id", row).text($(this).find("AutoId").text());
                            $(".Customer", row).text($(this).find("Customer").text());
                            if ($(this).find("SelectStatus").text() == 0) {
                                $(".SelCheckbox", row).html("<input type='checkbox' class='zipcheckbox' onclick='updateCustomerRoute(\"" + $(this).find("AutoId").text() + "\")'  />");
                            }
                            else {
                                flag = false;
                               $("#ddlSalePerson").prop("disabled", true);
                                $(".SelCheckbox", row).html('<input type="checkbox" class="zipcheckbox" checked="true"  onclick="updateCustomerRoute(' + $(this).find("AutoId").text() + ')" />');
                            }
                            $("#tblRouteCustomer tbody").append(row);
                            row = $("#tblRouteCustomer tbody tr:last").clone(true);
                        });
                        if (flag) {
                            if ($(EmpType).find('EmpType').text() != '2') {
                                $("#ddlSalePerson").prop("disabled", false);
                            }
                        }
                    } else {
                        $("#EmptyTable").show();
                        $("#tblRouteCustomer").hide();
                    }

                }
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
}




