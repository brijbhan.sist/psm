﻿$(document).ready(function () {
    bindEmpTypeAndStatus();
    getEmpRecord(1);

});
function Pagevalue(e) {
    getEmpRecord(parseInt($(e).attr("page")));
};
function validateDeliveryTime(emptype) {
    if (emptype == 5) {
        $("#deliverytimeDiv").show();
    } else {
        $("#deliverytimeDiv").hide();
    }
    $("#ddlDeliveryStartTime").val('07:00:00').change();
    $("#ddlDeliveryCloseTime").val('19:30:00').change();
}
/*-------------------------------------------------------------------Display Selected Image-----------------------------------------------------*/
function readURL(input) {
    if ($(input).val() == '') {
        $("#imgPreview").attr("src", "/images/Emp_Default.png");
    } else {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                document.getElementById('imgPreview').src = e.target.result;
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
}
/*-------------------------------------------------------------------Bind Employee Type and Status---------------------------------------------*/
function bindEmpTypeAndStatus() {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/employeeMaster.asmx/bindEmpTypeAndStatus",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var EmpType = $(xmldoc).find("Table");
            var Status = $(xmldoc).find("Table1");
            var CompanyId = $(xmldoc).find("Table2");
            var DeviceDetails = $(xmldoc).find("Table3");
            var OptmoTime = $(xmldoc).find("Table4");
            var IpAutoId = $(xmldoc).find("Table5");
            $('#CompanyId').text('@' + $(CompanyId).find('CompanyId').text());
            $("#ddlEmpType option:not(:first)").remove();
            $.each(EmpType, function () {
                $("#ddlEmpType").append($("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("TypeName").text() + "</option>"));
            });

            $("#ddlSEmpType option:not(:first)").remove();
            $.each(EmpType, function () {
                $("#ddlSEmpType").append($("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("TypeName").text() + "</option>"));
            });

            $("#ddlStatus option:not(:first)").remove();
            $.each(Status, function () {
                $("#ddlStatus").append($("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("StatusType").text() + "</option>"));
            });

            $("#ddlDeliveryStartTime option:not(:first)").remove();
            $.each(OptmoTime, function () {
                $("#ddlDeliveryStartTime").append($("<option value='" + $(this).find("TimeFrom").text() + "'>" + $(this).find("TimeFromLabel").text() + "</option>"));
            });


            $("#ddlDeliveryCloseTime option:not(:first)").remove();
            $.each(OptmoTime, function () {
                $("#ddlDeliveryCloseTime").append($("<option value='" + $(this).find("TimeTo").text() + "'>" + $(this).find("TimeToLabel").text() + "</option>"));
            });
            $("#ddlDeliveryStartTime").val('07:00:00');
            $("#ddlDeliveryCloseTime").val('19:30:00');
            $("#ddlDeliveryStartTime").select2();
            $("#ddlDeliveryCloseTime").select2();



            $("#ddlAssignDevice option:not(:first)").remove();
            $.each(DeviceDetails, function () {
                $("#ddlAssignDevice").append($("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("DeviceId").text() + "</option>"));
            });
            //$("#ddlAssignDevice").select2();
            $("#ddlAssignDevice").attr('multiple', 'multiple');
            $("#ddlAssignDevice").select2({
                placeholder: 'Select',
                allowClear: true,
                //data: ["1","3"],
                multiple: true,


            });
            $("#ddlAssignDevice").val([0]);
            $("#ddlAssignDevice").trigger("change");

            $("#dllIpAddress option:not(:first)").remove();
            $.each(IpAutoId, function () {
                $("#dllIpAddress").append($("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("NameofLocation").text() + "</option>"));
            });

            //$("#ddlAssignDevice").select2();
            $("#dllIpAddress").attr('multiple', 'multiple');
            $("#dllIpAddress").select2({
                placeholder: 'Select',
                allowClear: true,
                //data: ["1","3"],
                multiple: true,


            });
            $("#dllIpAddress").val([0]);
            $("#dllIpAddress").trigger("change");
        },
        failure: function (result) {
            swal("", "Oops! Something went wrong. Please try later.", "error");
        },
        error: function (result) {
            swal("", "Oops! Something went wrong. Please try later.", "error");
        }
    });
};
/*-------------------------------------------------------------------Insert Employee Details-----------------------------------------------------*/
$("#btnSave").click(function () {
    if (checkRequiredField()) {
        if ($("#txtPassword").val() != $("#txtConfirmPassword").val()) {//condition added on 11/30/2019 By Rizwan Ahmad
            toastr.error('Password do not match.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        }
        else {
            var timeStamp = Date.parse(new Date());
            var FileName = $("#filePhoto").val();
            var extension = FileName.substr((FileName.lastIndexOf('.') + 1));
            if ($("#filePhoto").val() != '') {
                var imgname = timeStamp + "" + extension;
            }
            else {
                var imgname = "Emp_Default.png";
            }
            var chklogin = 0;
            if ($("#chkloginip").prop("checked") == true) {
                chklogin = 1
            }
            else {
                chklogin = 0;
            }

            var appLogin = 0;
            if ($("#chkAppLogin").prop("checked") == true) {
                appLogin = 1
            }
            else {
                appLogin = 0;
            }
            var data = {
                EmpCode: $("#txtEmployeeId").val(),
                ProfileName: $("#txtProfileName").val(),
                EmpType: $("#ddlEmpType").val(),
                FirstName: $("#txtFirstName").val(),
                LastName: $("#txtLastName").val(),
                Address: $("#txtAddress").val(),
                State: $("#txtState").val(),
                City: $("#txtCity").val(),
                Zipcode: $("#txtZipcode").val(),
                Email: $("#txtEmail").val(),
                Contact: $("#txtContact").val(),
                ImageURL: imgname,
                Status: $("#ddlStatus").val(),
                UserName: $("#txtUserName").val(),
                Password: $("#txtPassword").val(),
                //IpAddress: $("#txtIpAddress").val(),
                IsLoginIp: chklogin,
                IsAppLogin: appLogin,
                AssignDevice: $("#ddlAssignDevice").val(),
                IpAddressAutoId: $("#dllIpAddress").val(),
                DeliveryStartTime: $("#ddlDeliveryStartTime").val(),
                DeliveryCloseTime: $("#ddlDeliveryCloseTime").val()
            }
            $.ajax({
                type: "POST",
                url: "/Admin/WebAPI/employeeMaster.asmx/insertEmpRecord",
                data: "{'dataValue':'" + JSON.stringify(data) + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (result) {
                    if (result.d == 'true') {
                        var fileUpload = $("#filePhoto").get(0);
                        var files = fileUpload.files;
                        var test = new FormData();
                        for (var i = 0; i < files.length; i++) {
                            test.append(files[i].name, files[i]);
                        }
                        $.ajax({
                            url: "/FileUploadHandler.ashx?timestamp=" + timeStamp,
                            type: "POST",
                            contentType: false,
                            processData: false,
                            data: test,
                            success: function (result) {
                                console.log(result);
                            },
                            error: function (err) {
                                console.log(err);
                            }
                        });
                        swal("", "Employee details saved successfully.", "success");
                        getEmpRecord(1);
                        reset();
                    }
                    else if (result.d == "Unauthorized access.") {
                        location.href = "/";
                    }
                    else {
                        swal("", result.d, "error");
                    }
                },
                error: function (result) {
                    swal("", "Oops! Something went wrong.Please try later.", "error");
                },
                failure: function (result) {
                    swal("", "Oops! Something went wrong.Please try later.", "error");
                }
            });
        }
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }

})
/*--------------------------------------------------------Bind Employee Details in Table----------------------------------------------------*/
function getEmpRecord(pageIndex) {
    var data = {
        EmpType: $("#ddlSEmpType").val(),
        EmpId: $("#txtSEmployeeId").val().trim(),
        Name: ($("#txtSEmployeeName").val()).trim(),
        Email: $("#txtSEmail").val().trim(),
        Status: $("#ddlSStatus").val(),
        PageSize: $("#ddlPageSize").val(),
        pageIndex: pageIndex
    }
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/employeeMaster.asmx/getEmpRecord",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var Emp = $(xmldoc).find("Table1");

            if (Emp.length > 0) {
                $("#EmptyTable").hide();
                $("#tblEmployeeList tbody tr").remove();
                var row = $("#tblEmployeeList thead tr").clone(true);
                $.each(Emp, function () {
                    if ($("#emptype").val() == 1 && $(this).find("Status").text() == 'Active') {
                        $(".Action", row).html("<a href='javascript:;'><span class='ft-edit' onclick='editEmp(\"" + $(this).find("AutoId").text() + "\")'></span></a>&nbsp;&nbsp<a href='javascript:;'><span class='ft-x' onclick='deleterecord(\"" + $(this).find("AutoId").text() + "\")'></span></a>&nbsp;&nbsp<a href='javascript:;'><span class='la la-sign-in' onclick='loginwith(\"" + $(this).find("AutoId").text() + "\")'></span></a>");
                    } else if ($("#emptype").val() == 1 && $(this).find("Status").text() != 'Active') {
                        $(".Action", row).html("<a href='javascript:;'><span class='ft-edit' onclick='editEmp(\"" + $(this).find("AutoId").text() + "\")'></span></a>&nbsp;&nbsp<a href='javascript:;'><span class='ft-x' onclick='deleterecord(\"" + $(this).find("AutoId").text() + "\")'></span></a>");
                    }
                    else {
                        $(".Action", row).html("<a href='javascript:;'><span class='ft-edit' onclick='ShowModalChangePassord(\"" + $(this).find("AutoId").text() + "\")'></span></a>");
                    }
                    $(".AutoId", row).html($(this).find("AutoId").text());
                    $(".EmployeeCode", row).html($(this).find("EmployeeCode").text());
                    $(".Name", row).html($(this).find("FirstName").text() + " " + $(this).find("LastName").text());
                    $(".Profile", row).html($(this).find("ProfileName").text());
                    $(".UserName", row).html($(this).find("UserName").text());
                    $(".Type", row).html($(this).find("EmpType").text());
                    $(".Address", row).html($(this).find("Address").text() + " " + $(this).find("State").text() + " " + $(this).find("City").text() + " " + $(this).find("Zipcode").text());
                    $(".Email", row).html($(this).find("Email").text());
                    $(".Contact", row).html($(this).find("Contact").text());
                    $(".LoginDate", row).html($(this).find("LoginDate").text());
                    $(".AssignDevice", row).html($(this).find("DeviceId").text());

                    if ($(this).find("Status").text() == 'Active') {
                        $(".Status", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("Status").text() + "</span>");
                    }
                    else {
                        $(".Status", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("Status").text() + "</span>");
                    }
                    $("#tblEmployeeList tbody").append(row);
                    row = $("#tblEmployeeList tbody tr:last-child").clone(true);
                });
            }
            else {
                $("#tblEmployeeList tbody tr").remove();
                $("#EmptyTable").show();
            }
            var pager = $(xmldoc).find("Table");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
        },
        error: function (result) {
            swal("", "Oops! Something went wrong. Please try later.", "error");
        },
        failure: function (result) {
            swal("", "Oops! Something went wrong. Please try later.", "error");
        }
    });
}

function deleterecord(AutoId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this employee detail.",
        icon: "warning",
        showCancelButton: true,
        allowOutsideClick: false,
        closeOnClickOutside: false,
        buttons: {
            cancel: {
                text: "No, Cancel.",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, Delete it.",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            deleteEmp(AutoId)
        }
    })
}
$("#btnSearch").click(function () {
    getEmpRecord(1);
})
/*-------------------------------------------------------------------Edit Employee Details-----------------------------------------------------*/
function CheckPasswodType() {
    var x = document.getElementById("txtPassword");
    if (x.type === "text") {
        x.type = "password";
    } else {
        x.type = "password";
    }
}

function editEmp(AutoId) {
    CheckPasswodType();
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/employeeMaster.asmx/editEmpRecord",
        data: "{'AutoId':'" + AutoId + "'}",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var Emp = $(xmldoc).find("Table");
            $("#AutoId").text($(Emp).find("AutoId").text());
            $("#ddlEmpType").val($(Emp).find("EmpType").text());
            $("#txtEmployeeId").val($(Emp).find("EmployeeCode").text());
            $("#txtProfileName").val($(Emp).find("ProfileName").text());
            $("#txtFirstName").val($(Emp).find("FirstName").text());
            $("#txtLastName").val($(Emp).find("LastName").text());
            $("#txtAddress").val($(Emp).find("Address").text());
            $("#txtState").val($(Emp).find("State").text());
            $("#txtCity").val($(Emp).find("City").text());
            $("#txtZipcode").val($(Emp).find("Zipcode").text());
            $("#txtEmail").val($(Emp).find("Email").text());
            $("#txtContact").val($(Emp).find("Contact").text());

            if ($(Emp).find("EmpType").text() == 5) {
                $("#deliverytimeDiv").show();
            } else {
                $("#deliverytimeDiv").hide();
            }

            var string = $(Emp).find("AssignDeviceAutoId").text();
            var a = string.split(',');
            $("#ddlAssignDevice").val(a);
            $("#ddlAssignDevice").trigger("change");
            $("#ddlAssignDevice").val($(Emp).find("AssignDeviceAutoId").text());

            var string1 = $(Emp).find("IpAddressAutoId").text();
            var a1 = string1.split(',');
            $("#dllIpAddress").val(a1);
            $("#dllIpAddress").trigger("change");

            $("#dllIpAddress").val($(Emp).find("IpAddressAutoId").text());
            $("#ddlEmpType").prop('disabled', true);
            if ($(Emp).find("ImageURL").text() == '' || $(Emp).find("ImageURL").text() == '/Attachments/default_pic.png') {
                $("#imgPreview").attr("src", "/images/Emp_Default.png");
            } else if ($(Emp).find("ImageURL").text() == '' || $(Emp).find("ImageURL").text() == '/Attachments/Emp_Default.png') {
                $("#imgPreview").attr("src", "/images/Emp_Default.png");
            }
            else {
                $("#imgPreview").attr("src", ".." + $(Emp).find("ImageURL").text());
            }
            $("#ddlStatus").val($(Emp).find("Status").text());
            var TEST = $(Emp).find("UserName").text();
            var UserName = TEST.split('@');
            $("#txtUserName").val(UserName[0]);
            $("#txtPassword").val($(Emp).find("Password").text());
            $("#txtConfirmPassword").val($(Emp).find("Password").text());
            $("#txtIpAddress").val($(Emp).find("IP_Address").text());
            localStorage.setItem('PSW', $(Emp).find("Password").text());
            var a = $(Emp).find("isApplyIP").text();
            if (a == "1") {
                $("#chkloginip").prop("checked", true);
            }
            else {
                $("#chkloginip").prop("checked", false);
            }

            var b = $(Emp).find("IsAppLogin").text();
            if (b == "1") {
                $("#chkAppLogin").prop("checked", true);
            }
            else {
                $("#chkAppLogin").prop("checked", false);
            }

            if ($(Emp).find("OptimotwFrom").text() != '') {
                $("#ddlDeliveryStartTime").val($(Emp).find("OptimotwFrom").text()).change();
                $("#ddlDeliveryCloseTime").val($(Emp).find("OptimotwTo").text()).change();
            }
            $("#btnSave").hide();
            $("#btnReset").hide();
            $("#btnUpdate").show();
            $("#btnCancel").show();
        },
        error: function (result) {
            swal("", "Oops! Something went wrong. Please try later.", "error");
        },
        failure: function (result) {
            swal("", "Oops! Something went wrong. Please try later.", "error");
        }
    });
}

/*-------------------------------------------------------------------Update Employee Details-----------------------------------------------------*/
$("#btnUpdate").click(function () {
    var change = 0;
    if ($("#txtPassword").val() != $("#txtConfirmPassword").val()) {//condition added on 11/30/2019 By Rizwan Ahmad
        toastr.error('Password do not match.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
    else {
        if (localStorage.getItem('PSW') != "" && localStorage.getItem('PSW') != null) {
            var oldPSW = localStorage.getItem('PSW');
            if (oldPSW != $("#txtPassword").val()) {
                change = 1;
            }
        }
        var timeStamp = Date.parse(new Date());
        var FileName = $("#filePhoto").val();
        var extension = FileName.substr((FileName.lastIndexOf('.') + 1));
        if ($("#filePhoto").val() != '') {
            var imgname = timeStamp + "" + extension;
        }
        else {
            var imgname = $("#imgPreview").attr("src").split("/")[2];
        }
        var chklogin = 0;
        //var IpAddress = "";
        if ($("#chkloginip").prop("checked") == true) {
            chklogin = 1
        }
        else {
            chklogin = 0;
        }
        var appLogin = 0;
        if ($("#chkAppLogin").prop("checked") == true) {
            appLogin = 1
        }
        else {
            appLogin = 0;
        }
        if (checkRequiredField()) {
            var data = {
                AutoId: $("#AutoId").text(),
                EmpCode: $("#txtEmployeeId").val(),
                ProfileName: $("#txtProfileName").val(),
                EmpType: $("#ddlEmpType").val(),
                FirstName: $("#txtFirstName").val(),
                LastName: $("#txtLastName").val(),
                Address: $("#txtAddress").val(),
                State: $("#txtState").val(),
                City: $("#txtCity").val(),
                Zipcode: $("#txtZipcode").val(),
                Email: $("#txtEmail").val(),
                Contact: $("#txtContact").val(),
                ImageURL: imgname,
                Status: $("#ddlStatus").val(),
                UserName: $("#txtUserName").val(),
                Password: $("#txtPassword").val(),
                //IpAddress: $("#txtIpAddress").val()||'',
                IsLoginIp: chklogin || 0,
                IsAppLogin: appLogin || 0,
                PasswordChange: change,
                AssignDevice: $("#ddlAssignDevice").val(),
                IpAddressAutoId: $("#dllIpAddress").val(),
                DeliveryStartTime: $("#ddlDeliveryStartTime").val(),
                DeliveryCloseTime: $("#ddlDeliveryCloseTime").val()
            }
            $.ajax({
                type: "POST",
                url: "/Admin/WebAPI/employeeMaster.asmx/updateEmpRecord",
                data: "{'dataValue':'" + JSON.stringify(data) + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (result) {
                    if (result.d == "true") {
                        var fileUpload = $("#filePhoto").get(0);
                        var files = fileUpload.files;
                        var test = new FormData();
                        for (var i = 0; i < files.length; i++) {
                            test.append(files[i].name, files[i]);
                        }
                        $.ajax({
                            url: "/FileUploadHandler.ashx?timestamp=" + timeStamp,
                            type: "POST",
                            contentType: false,
                            processData: false,
                            data: test,
                            success: function (result) {
                                console.log(result);
                            },
                            error: function (err) {
                                console.log(err);
                            }
                        });
                        swal("", "Employee details updated successfully.", "success");
                        getEmpRecord(1);
                        reset();
                    }
                    else if (result.d == "Unauthorized access.") {
                        location.href = "/";
                    }
                    else {
                        swal("", result.d, "warning");
                    }
                },
                error: function (result) {
                    swal("", "Oops! Something went wrong. Please try later.", "error");
                },
                failure: function (result) {
                    swal("", "Oops! Something went wrong. Please try later.", "error");
                }
            });
        }
        else {
            toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        }
    }
})

function deleteEmp(AutoId) {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/employeeMaster.asmx/deleteEmpRecord",
        data: "{'AutoId':'" + AutoId + "'}",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == "true") {
                swal("", "Employee details deleted successfully.", "success");
                getEmpRecord(1);
            }
            else if (response.d == "Unauthorized access.") {
                location.href = "/";
            }
            else {
                swal("", response.d, "error");
            }
        },
        error: function (result) {
            swal("", "Oops! Something went wrong. Please try later.", "error");
        },
        failure: function (result) {
            swal("", "Oops! Something went wrong. Please try later.", "error");
        }
    });
}

$("#btnCancel").click(function () {
    reset();
})
$("#btnReset").click(function () {
    CheckPasswodType();
    reset();
})
function reset() {

    $("#deliverytimeDiv").hide();
    $("#ddlDeliveryStartTime").val('07:00:00').change();
    $("#ddlDeliveryCloseTime").val('19:30:00').change();
    $('#panelEmployeeMaster input[type="text"]').val('');
    $('#panelEmployeeMaster input[type="password"]').val('');
    $("#btnSave").show();
    $("#btnReset").show();
    $("#btnUpdate").hide();
    $("#btnCancel").hide();
    $("#ddlStatus").val(1);
    $("#ddlAssignDevice").val([0]);
    $("#ddlAssignDevice").trigger("change");
    $("#dllIpAddress").val([0]);
    $("#dllIpAddress").trigger("change");
    $("#ddlEmpType").val(0);
    $("#filePhoto").val('');
    $("#imgPreview").attr("src", "/images/Emp_Default.png");
    $("#txtIpAddress").val('');
    $("#ddlAssignDevice").val('0');
    $("#chkloginip").prop("checked", false);
    $("#chkAppLogin").prop("checked", false);
    $("#ddlEmpType").prop("disabled", false);
    $('input[type="text"]').removeClass('border-warning');
    $('select.form-control').removeClass('border-warning');
    $('input[type="password"]').removeClass('border-warning');
}

function validateZipCode(elementValue) {
    var zipCodePattern = /^[0-9]{5}(?:-[0-9]{4})?$/;
    var check = zipCodePattern.test($(elementValue).val());
    if (!check) {
        $(elementValue).val('');
        toastr.error('Invalid Zip Code .', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}

function validateEmail(elementValue) {
    var EmailCodePattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var check = EmailCodePattern.test($(elementValue).val());
    if (!check) {
        toastr.error('Invalid email id.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $(elementValue).val('');
    }
    return re.test(String(elementValue).toLowerCase());
}

function myFunction() {
    var x = document.getElementById("txtPassword");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}
function myFunction1() {
    var x = document.getElementById("txtNewpassword");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}

function myFunction2() {
    var x = document.getElementById("txtoldpassword");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}

$("#txtConfirmPassword").change(function () {
    if ($("#txtPassword").val() != $("#txtConfirmPassword").val()) {
        toastr.error('Password not matched.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        //$("#txtConfirmPassword").val('');//added on 11/30/2019 By Rizwan Ahmad
    }
})

$("#txtPassword").change(function () {//added on 11/30/2019 By Rizwan Ahmad
    if ($("#txtConfirmPassword").val() != $("#txtPassword").val()) {
        toastr.error('Password not matched.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        //$("#txtPassword").val('');
    }
})


function ShowModalChangePassord(AutoId) {

    $.ajax({
        type: "POST",
        url: "/Admin/Employee.aspx/getemployeeinfo",
        data: JSON.stringify({ AutoId: AutoId }),
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var Emp = $(xmldoc).find("Table");
            if (Emp.length > 0) {

                $("#ModalChangePassord").modal('show');
                $("#txtNewpassword").val('');
                $("#txtConfNewpassword").val('');
                $("#EmpAutoId").val($(Emp).find('AutoId').text());
                $("#txtUserNameP").val($(Emp).find('UserId').text());
                $("#lblemployeeName").html($(Emp).find('UserName').text());
                $("#txtoldpassword").val($(Emp).find('Password').text());
                $("#txtoldpassword").attr('type', 'password');
                if ($(Emp).find('isApplyIP').text() == 1) {
                    $("#chkisApplyIp").prop('checked', true);
                } else {
                    $("#chkisApplyIp").prop('checked', false);
                }

            }
        },
        error: function (result) {
            swal("", "Oops! Something went wrong.Please try later.", "error");
        },
        failure: function (result) {
            swal("", "Oops! Something went wrong.Please try later.", "error");
        }
    });

}

function checkRequiredFieldPassreq() {
    var boolcheck = true;
    $('.passreq').each(function () {
        if ($(this).val().trim() == '' || $(this).val().trim() == '0' || $(this).val().trim() == '0.00') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });
    return boolcheck;
}

function changepass() {
    var pass = $("#txtNewpassword").val().toLowerCase();
    var Cpass = $("#txtConfNewpassword").val().toLowerCase();
    var checkvalid = 0;
    if (pass != '' || Cpass != '') {
        if (pass == '' || Cpass == '') {
            checkvalid = 1;
        }
    }

    if (checkvalid) {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    } else {

        if (pass != Cpass) {//condition added on 11/30/2019 By Rizwan Ahmad
            $("#txtNewpassword").addClass('border-warning');
            $("#txtConfNewpassword").addClass('border-warning');
            toastr.error('Password do not match.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            return false
        }
        var IsLoginIp = 0;
        if ($("#chkisApplyIp").prop('checked')) {
            IsLoginIp = 1;
        }

        var data = {
            AutoId: $("#EmpAutoId").val(),
            Password: $("#txtNewpassword").val(),
            PasswordChange: 1,
            IsLoginIp: IsLoginIp
        }
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/employeeMaster.asmx/updatepass",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == "true") {
                    $("#ModalChangePassord").modal('hide');
                    swal("", "User Info updated successfully.", "success");
                    getEmpRecord(1);
                    reset();
                }
                else if (result.d == "Unauthorized access.") {
                    location.href = "/";
                }
                else {
                    swal("", result.d, "warning");
                }
            },
            error: function (result) {
                swal("", "Oops! Something went wrong. Please try later.", "error");
            },
            failure: function (result) {
                swal("", "Oops! Something went wrong. Please try later.", "error");
            }
        });
    }
}
function loginwith(empId) {
    swal({
        title: "Are you sure?",
        text: "You want to login with this employee.",
        icon: "warning",
        showCancelButton: true,
        allowOutsideClick: false,
        closeOnClickOutside: false,
        buttons: {
            cancel: {
                text: "No, Cancel.",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, Login it.",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "POST",
                url: "/Admin/WebAPI/employeeMaster.asmx/loginWith",
                data: "{'EmpAutoId':'" + empId + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                },
                complete: function () {
                },

                success: function (response) {
                    console.log(response);
                    if (response.d == 'Access Denied') {
                        swal("", "Access Denied", "error");
                    }
                    else if (response.d == 'Username And / Or Password Incorrect') {
                        swal("", "Username / Password Incorrect", "error");
                    }
                    else {
                        var xmldoc = $.parseXML(response.d);
                        var loginDetails = $(xmldoc).find("Table");
                        if (loginDetails.length > 0) {
                            if ($(loginDetails).find("Submsg").text() != "0") {
                                swal("Alert", $(loginDetails).find("Submsg").text(), "warning").then(function () {
                                    if ($(loginDetails).find("ExpMsg").text() != "1") {
                                        document.location.href = "/Admin/mydashboard.aspx";
                                    }
                                    else {
                                        location.href = "/"
                                    }
                                })
                            } else {
                                document.location.href = "/Admin/mydashboard.aspx";
                            }
                        }
                        else {
                            swal("", "Username / Password Incorrect.", "error");
                        }
                    }
                },
                error: function (result) {
                    swal("", "Error ! Username / Password Incorrect.", "warning");
                },
                failure: function (result) {
                    swal("", "Error ! Access Denied", "warning");
                }
            });
        }
    })
}

