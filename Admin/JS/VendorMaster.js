﻿var check = 0, getid = 0;
$(document).ready(function () {
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    bindDropDown();
    getid = getQueryString('vendorId');
    if (getid != null || getid == '') {
        editVendor(getid);
    }
});
function Pagevalue(e) {
    getVendorRecord(parseInt($(e).attr("page")));
};
/*------------------------------------------------------------------------------------------------------------------------*/
//                                                Bind State & Status
/*------------------------------------------------------------------------------------------------------------------------*/
function bindDropDown() {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/vendorMaster.asmx/bindDropDown",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var State = $(xmldoc).find("Table");
            var Status = $(xmldoc).find("Table1");
            var country = $(xmldoc).find("Table2");
            var zipcode = $(xmldoc).find("Table3");

            $("#ddlCountry option:not(:first)").remove();
            $.each(country, function () {
                $("#ddlCountry").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("Country").text() + "</option>");
            });
            $("#ddlCountry").val(1).change();
            $("#ddlState option:not(:first)").remove();
            $.each(State, function () {
                $("#ddlState").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("StateName").text() + "</option>");
            });

            $("#ddlStatus option:not(:first)").remove();
            $.each(Status, function () {
                $("#ddlStatus").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("StatusType").text() + "</option>");
            });

            $("#ddlZipcode option:not(:first)").remove();//For Zipcode
            $.each(zipcode, function () {
                $("#ddlZipcode").append("<option value='" + $(this).find("Zipcode").text() + "'>" + $(this).find("Zipcode").text() + "</option>");
            });
            $("#ddlZipcode").select2()
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
};
/*------------------------------------------------------------------------------------------------------------------------*/
//                                                Insert Vendor Details
/*------------------------------------------------------------------------------------------------------------------------*/
$("#btnSave").click(function () {
    var location = 0, CityId = 0, StateId = 0;
    if (localStorage.getItem('StateId') != null) {
        StateId = localStorage.getItem('StateId');
    }
    if ($('#ddlType').val() == 1 && $('#ddlInternal').val() == 0) {
        $('#ddlInternal').addClass('border-warning');
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        return;
    } else {
        $('#ddlInternal').removeClass('border-warning');
        location = $('#ddlInternal').val();
    }
    if (checkSpecialCharacter($("#txtVendorName") == 1)) {
        return;
    }
    if (vencheckRequiredField()) {
        var data = {
            Location: location,
            Type: $("#ddlType").val(),
            VendorName: $("#txtVendorName").val(),
            Address: $("#txtAddress").val(),
            Country: $("#ddlCountry").val(),
            State: StateId,
            City: $("#txtCity").val(),
            Zipcode: $("#ddlZipcode").val(),
            ContactPerson: $("#txtContactPerson").val(),
            Cell: $("#txtCell").val(),
            Office1: $("#txtOffice1").val(),
            Office2: $("#txtOffice2").val(),
            Email: $("#txtEmail").val(),
            Status: $("#ddlStatus").val(),
        }

        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/vendorMaster.asmx/insertVendor",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d == "Unauthorized") {
                    location.href = "/";
                }
                else if (response.d == "success") {
                    swal("", "Vendor details saved successfully.", "success");
                    reset();
                }
                else {
                    swal("Error!", response.d, "error");
                }
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
                swal("Error!", JSON.parse(result.responseText).d, "error");
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
                swal("Error!", JSON.parse(result.responseText).d, "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
})

/*------------------------------------------------------------------------------------------------------------------------*/
//                                                Get Vendor List
/*------------------------------------------------------------------------------------------------------------------------*/

function getVendorRecord(pageIndex) {
    var data = {
        VendorName: $("#txtSVendorName").val().trim(),
        VendorCell: $("#txtSVendorCell").val().trim(),
        pageIndex: pageIndex,
        PageSize: $("#ddlPageSize").val()
    }
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/vendorMaster.asmx/getVendorRecord",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },

        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var vendor = $(xmldoc).find("Table1");
            if (vendor.length > 0) {
                $("#EmptyTable").hide();
                $("#tblVendorList tbody tr").remove();
                var row = $("#tblVendorList thead tr").clone(true);
                $.each(vendor, function () {
                    $(".Action", row).html("<a title='View' href='javascript:;'><span class='la la-eye' onclick='editVendor2(\"" + $(this).find("VendorId").text() + "\")'></span></a>");
                    $(".VendorId", row).html($(this).find("VendorId").text());
                    $(".VendorName", row).html($(this).find("VendorName").text());
                    $(".ContactPerson", row).html($(this).find("ContactPerson").text());
                    $(".Cell", row).html($(this).find("Cell").text());
                    $(".Office", row).html($(this).find("Office").text());
                    $(".Email", row).html($(this).find("Email").text());
                    if ($(this).find("Status").text() == 'Active') {
                        $(".Status", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("Status").text() + "</span>");
                    } else {
                        $(".Status", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("Status").text() + "</span>");

                    }
                    $("#tblVendorList tbody").append(row);
                    row = $("#tblVendorList tbody tr:last-child").clone(true);
                });
            }
            else {
                $("#tblVendorList tbody tr").remove();
                $("#EmptyTable").show();
            }

            var pager = $(xmldoc).find("Table");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

$("#btnSearch").click(function () {
    getVendorRecord(1);
})
/*------------------------------------------------------------------------------------------------------------------------*/
//                                                Edit Vendor Record
/*------------------------------------------------------------------------------------------------------------------------*/
function editVendor2(VendorId) {
    window.location.href = "/Admin/ViewVenderDetails.aspx?vendorId=" + VendorId;
}
function editVendor(VendorId) {
    LocationbindDropDown();
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/vendorMaster.asmx/editVendor",
        data: "{'VendorId':'" + VendorId + "'}",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            check = 1;
            var xmldoc = $.parseXML(response.d);
            var vendor = $(xmldoc).find("Table");
            $("#txtVendorId").val($(vendor).find("VendorId").text());
            $("#txtVendorName").val($(vendor).find("VendorName").text());
            $("#txtAddress").val($(vendor).find("Address").text());
            $("#txtState").val($(vendor).find("StateName").text());
            $("#txtCity").val($(vendor).find("City").text());
            $("#ddlZipcode").val($(vendor).find("Zipcode").text()).change();
            $("#ddlZipcode").val($(vendor).find("Zipcode").text());
            $("#ddlCountry").val($(vendor).find("AutoId").text());
            $("#txtContactPerson").val($(vendor).find("ContactPerson").text());
            $("#txtCell").val($(vendor).find("Cell").text());
            $("#txtOffice1").val($(vendor).find("Office1").text());
            $("#txtOffice2").val($(vendor).find("Office2").text());
            $("#txtEmail").val($(vendor).find("Email").text());
            $("#ddlStatus").val($(vendor).find("Status").text());
            localStorage.setItem('StateId', $(vendor).find("StateId").text());
            if ($(vendor).find("LocationType").text() == "1") {
                $("#ddlType").val(1).change();
                $("#RowEnternal").show();
            } else {
                $("#ddlType").val(0).change();
                $("#RowEnternal").hide();
            }
            if ($(vendor).find("LocationName").text() != "0") {
                $("#ddlInternal").val($(vendor).find("LocationName").text()).change();
            } else {
                $("#ddlInternal").val(0).change();
            }
            $("#btnSave").hide();
            $("#btnReset").hide();
            $("#btnUpdate").show();
            $("#btnCancel").show();
            check = 0;
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
/*------------------------------------------------------------------------------------------------------------------------*/
//                                                Update Vendor Record
/*------------------------------------------------------------------------------------------------------------------------*/

$("#btnUpdate").click(function () {
    var location = 0, CityId = 0, StateId = 0;
    if (localStorage.getItem('StateId') != null) {
        StateId = localStorage.getItem('StateId');
    }
    if ($('#ddlType').val() == 1 && $('#ddlInternal').val() == 0) {
        $('#ddlInternal').addClass('border-warning');
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        return;
    } else {
        $('#ddlInternal').removeClass('border-warning');
        location = $('#ddlInternal').val();
    }
    if (checkSpecialCharacter($("#txtVendorName") == 1)) {
        return;
    }
    if (vencheckRequiredField()) {
        var data = {
            Location: location,
            Type: $("#ddlType").val(),
            VendorId: $("#txtVendorId").val(),
            VendorName: $("#txtVendorName").val(),
            Address: $("#txtAddress").val(),
            Country: $("#ddlCountry").val(),
            State: StateId,
            City: $("#txtCity").val(),
            Zipcode: $("#ddlZipcode").val(),
            ContactPerson: $("#txtContactPerson").val(),
            Cell: $("#txtCell").val(),
            Office1: $("#txtOffice1").val(),
            Office2: $("#txtOffice2").val(),
            Email: $("#txtEmail").val(),
            Status: $("#ddlStatus").val(),
        }

        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/vendorMaster.asmx/updateVendor",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },

            success: function (response) {
                if (response.d == "Unauthorized") {
                    location.href = "/";
                }
                else if (response.d == "true") {
                    swal({
                        icon: "success",
                        title: "",
                        text: "Vendor details updated successfully.",
                        type: "success"
                    }).then(function () {
                        window.location.href = "/Admin/ViewVenderDetails.aspx?vendorId=" + getid;
                    });
                }
                else {
                    swal("", response.d, "error");
                }
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
                swal("Error!", JSON.parse(result.responseText).d, "error");
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
                swal("Error!", JSON.parse(result.responseText).d, "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
})

///*------------------------------------------------------------------------------------------------------------------------*/
////                                                Delete Vendor Record
///*------------------------------------------------------------------------------------------------------------------------*/

//function deleteVendor(VendorId) {

//    $.ajax({
//        type: "POST",
//        url: "/Admin/WebAPI/vendorMaster.asmx/deleteVendor",
//        data: "{'VendorId':'" + VendorId + "'}",
//        contentType: "application/json;charset=utf-8",
//        datatype: "json",
//        beforeSend: function () {
//            $.blockUI({
//                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
//                overlayCSS: {
//                    backgroundColor: '#FFF',
//                    opacity: 0.8,
//                    cursor: 'wait'
//                },
//                css: {
//                    border: 0,
//                    padding: 0,
//                    backgroundColor: 'transparent'
//                }
//            });
//        },
//        complete: function () {
//            $.unblockUI();
//        },

//        success: function (response) {
//            if(response.d=="Unauthorized")
//            {
//                location.href="/";
//            }
//            else  if(response.d=="success")
//            {
//                swal("", "Vendor details deleted successfully.", "success");
//                getVendorRecord(1);
//                reset();
//            }
//            else
//            {
//                swal("", response.d, "error");
//            }
//        },
//        error: function (result) {
//            swal("", "This Vendor cannot be deleted as Orders has been placed to this Vendor in the Past.", "error");
//            console.log(JSON.parse(result.responseText).d);
//        },
//        failure: function (result) {
//            console.log(JSON.parse(result.responseText).d);

//        }
//    })

//}
//function deleterecord(VendorId) {
//    swal({
//        title: "Are you sure?",
//        text: "You want to delete this vendor.",
//        icon: "warning",
//        showCancelButton: true,
//        buttons: {
//            cancel: {
//                text: "No, cancel.",
//                value: null,
//                visible: true,
//                className: "btn-warning",
//                closeModal: false,
//            },
//            confirm: {
//                text: "Yes, delete it.",
//                value: true,
//                visible: true,
//                className: "",
//                closeModal: false
//            }
//        }
//    }).then(isConfirm => {
//        if (isConfirm) {
//            deleteVendor(VendorId);

//} else {
//            swal("", "Your vendor is safe.", "error");
//}
//});
//};
/*------------------------------------------------------------------------------------------------------------------------*/
//                                               Reset Input Fields
/*------------------------------------------------------------------------------------------------------------------------*/

$("#btnCancel,#btnReset").click(function () {
    reset();
})
function reset() {
    $('input[type="text"]').val('');
    $('textarea').val('');
    $("#btnSave").show();
    $("#btnReset").show();
    $("#btnUpdate").hide();
    $("#btnCancel").hide();
    $("#ddlStatus").val(1);
    $("#ddlZipcode").val(0).change();
    $("#ddlCountry").val(0);
    $("#ddlType").val(0).change();
    $("#ddlInternal").val(0).change();
    $("#RowEnternal").hide();
    $('input[type="text"]').removeClass('border-warning');
    $('textarea').removeClass('border-warning');
    $("#ddlStatus").removeClass('border-warning');
    $("#ddlZipcode").closest('div').find('.select2-selection--single').removeAttr('style');
    $("#ddlCountry").removeClass('border-warning');

}

function vencheckRequiredField() {
    var boolcheck = true;
    $('.req').each(function () {
        if ($(this).val() == '') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });

    $('.ddlreq').each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == 'undefined' || $(this).val() == 'Select') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });

    if ($('#ddlZipcode').val() == '0') {
        $('#ddlZipcode').closest('div').find('.select2-selection--single').attr('style', 'border:1px solid #FF9149  !important');
    } else {
        $('#ddlZipcode').closest('div').find('.select2-selection--single').removeAttr('style');
    }
    return boolcheck;
}
function applyStateCity() {
    debugger;
    var vCity = 0;
    $("#tblStateCity tbody tr").each(function () {
        if ($(this).find('.CAction input').prop('checked') == true) {
            vCity = 1;
        }
    });
    if (vCity == 0) {
        toastr.error('Please choose State/City.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
    if (vCity == 1) {
        $("#tblStateCity tbody tr").each(function () {
            if ($(this).find('.CAction input').prop('checked') == true) {
                $("#txtCity").val($(this).find(".CCity").text());
                $("#txtState").val($(this).find(".CState").text());
                localStorage.setItem('StateId', $(this).find(".CAction input").attr("StateId"));
            }
        });
        $('#mdMultipleCityState').modal('hide');
        $('#mdMultipleCityState').modal('');
    }
}
function closeCitySateModal() {
    debugger;
    if (getid == 0 || getid == null) {
        bindDropDown();
    }
}
function statecity() {
    debugger;
    var html = "";
    var Zipcode = $("#ddlZipcode").val();
    if (check == 0) {
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/vendorMaster.asmx/GetStateCity",
            data: "{'Zipcode':'" + Zipcode + "'}",
            contentType: "application/json;charset=utf-8",
            datatype: "json",
            async: false,
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (check == 0) {
                    $("#divStateCity").html('');
                    var xmldoc = $.parseXML(response.d);
                    var data = $(xmldoc).find("Table");
                    if (data.length > 1) {
                        $('#mdMultipleCityState').modal('show');
                        $("#tblStateCity tbody tr").remove();
                        var row = $("#tblStateCity thead tr").clone(true);
                        $.each(data, function () {
                            $(".CAction", row).html('<input type="radio" name="cbState" StateId="' + $(this).find("StateId").text() + '" class="rState" value="">');
                            $(".CZipcode", row).html($(this).find("Zipcode").text());
                            $(".CState", row).html($(this).find("StateName").text());
                            $(".CCity", row).html($(this).find("CityName").text());
                            $("#tblStateCity tbody").append(row);
                            row = $("#tblStateCity tbody tr:last-child").clone(true);
                        });
                    }
                    else {
                        //if(check!=0)
                        //{
                        $('#mdMultipleCityState').modal('hide');
                        $("#txtCity").val($(data).find("CityName").text());
                        $("#txtState").val($(data).find("StateName").text());
                        localStorage.setItem('StateId', $(data).find("StateId").text());
                        //}
                    }
                }
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        })
    }
}
function validateEmail(elementValue) {
    if ($("#txtEmail").val() != "") {
        var EmailCodePattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var check = EmailCodePattern.test($(elementValue).val());
        if (!check) {
            toastr.error('Invalid email id.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            $(elementValue).val('');
        }
        return re.test(String(elementValue).toLowerCase());
    }
}

$('#ddlType').change(function () {
    if ($('#ddlType').val() == 1) {
        $('#RowEnternal').show();
        LocationbindDropDown();
    } else {
        $('#ddlInternal').val(0);
        $('#RowEnternal').hide();
    }
})

function LocationbindDropDown() {
    var Location = $('#Hd_Domain').val();
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/vendorMaster.asmx/LocationBind",
        data: "{'LocationName':'" + Location + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },

        success: function (response) {

            var xmldoc = $.parseXML(response.d);
            var Location = $(xmldoc).find("Table");
            $("#ddlInternal option:not(:first)").remove();
            $.each(Location, function () {
                $("#ddlInternal").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("Location").text() + "</option>");
            });

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
};


function checkSpecialCharacter(event) {
    debugger;
    //if (event.charCode != 0) {
    //    var regex = new RegExp("^[a-zA-Z]+$");
    //    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    //    if (!regex.test(key)) {
    //        event.preventDefault();
    //        return false;
    //    }
    //}
}


$("#txtContactPerson").bind("keypress", function (event) {
    //if (event.charCode != 0) {
    //    var regex = new RegExp("^[a-zA-Z]+$");
    //    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    //    if (!regex.test(key)) {
    //        event.preventDefault();
    //        return false;
    //    }
    //}
});
function MobileLength(e,t) {
    if ($(e).val().length < 10) {
        toastr.error('Please enter a valid 10 digit number.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        if ($(e).val()!= '') {
            $(e).focus();
        }
    }
}
