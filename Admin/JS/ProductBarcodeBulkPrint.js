﻿$(document).ready(function () {
   
    if (localStorage.getItem('SearchFilter') != null) {
        var getid = JSON.parse(localStorage.getItem('SearchFilter'));
        GetProductAllBarcode(getid);
    }
});
/*-------------------------------------------------------------------------------------------------------------------------------*/
function GetProductAllBarcode(data) {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/ProductList.asmx/GetProductAllBarcode",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('#fade').show();
        },
        async: false,
        complete: function () {
            $('#fade').hide();
        },
        success: function (response) {
            debugger;
            var html = ''; html1 = ''; var DefaultUnit = '';
            var getData = $.parseJSON(response.d);
            var ProductID = 0;
            var i = 0;
            html1 += '<div>'
            $.each(getData[0].AllBarcode, function (index, item) {
                var heighttd = 0;
                if (Number(item.TU) == 1) {
                    heighttd = 'style="height: 350px;padding-top:10px;page-break-after: always;"' + item.TU + ' ';
                }
                else if (Number(item.TU) == 2)
                {
                    heighttd = 'style="height: 175px;padding-top:10px;page-break-after: always;" ' + item.TU + ' ';
                }
                else if (Number(item.TU) == 3)
                {
                    heighttd = 'style="height: 116px;padding-top:10px;page-break-after: always;" ' + item.TU + ' ';
                }
                if (item.PID != ProductID) {

                    if (Number(index) > 0) {
                         i = 0;
                        html1 += '</tbody>';
                        html1 += '</table>';
                        html1 += '</td>';
                        html1 += '</tr>';
                        html1 += '</tbody>';
                        html1 += '</table><p  style="page-break-before: always"></p>';
                    }
                    html1 += '<table class="table tableCSS" id="tblProduct">';
                    html1 += '<tbody><tr><td class="ProductId" style="width: 40%;height:350px !important; text-align: center; vertical-align: middle; word-break: break-word;">';
                    html1 += '<h1 style="font-size: 55px;"><b>' + item.PID + '</b></h1>' + '<h2>' + item.PN + '<br/><br/>' + item.LOC + '</h2></td>';
                    html1 += '<td class="Barcode" style="width:60%;text-align:center;padding:0">';
                    html1 += '<table id="tblProduct1" style="width:100%;height:350px;"><tbody>';
                }

                var hr = '';               
                var value = "";
                var btype = "code128";
                var renderer = "css";

                var quietZone = false;

                var settings = {
                    output: renderer,
                };
                if (i > 0) {
                    hr = 'style="border-top: 1px solid;"';
                }
                i++;
                value = { code: item.BC, rect: true };
                $("#barcode").barcode(value, btype, settings);

                DefaultUnit = item.PAI;

                html1 += '<tr ' + hr + '><td class="text-center" ' + heighttd + '><div style="width:100%;display: table-cell;">' + $('#barcode').html() + '</div><div>' + item.BC + '<br/><h2 class="header">' + item.UT + (item.UID == DefaultUnit ? '*' : '') + '</h2></div></td></tr>';



                //html1 += '<tr>'
                //if ($(this).find("ProductId").text() != ProductID) {
                //    html1 += '<td><h1 style="font-size: 55px;"><b>' + $(this).find("ProductId").text() + '</b></h1>' + '<h2>' + $(this).find("ProductName").text() + '</h2></td>';
                //    DefaultUnit = $(this).find("PackingAutoId").text();
                //    html1 += '<td><tr ' + hr + '><td class="text-center" style="padding-top:10px"><div style="width:100%;display: table-cell;">' + $('#barcode').html() + '</div><div>' + $(this).find("Barcode").text() + '<br/><h2 class="header">' + $(this).find("UnitType").text() + ($(this).find("UnitAutoId").text() == DefaultUnit ? '*' : '') + '</h2></div></td></tr></td>';

                //}
                //else {

                //    html1 += '<td><tr ' + hr + '><td class="text-center" style="padding-top:10px"><div style="width:100%;display: table-cell;">' + $('#barcode').html() + '</div><div>' + $(this).find("Barcode").text() + '<br/><h2 class="header">' + $(this).find("UnitType").text() + ($(this).find("UnitAutoId").text() == DefaultUnit ? '*' : '') + '</h2></div></td></tr></td>';
                //    i++;
                ProductID = item.PID;
                //}
                //html1 += '<tr>'
            });
            html1 += '</div>'
            $("#tblProduct").append(html1);
            window.print();
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}