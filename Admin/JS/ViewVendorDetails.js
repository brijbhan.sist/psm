﻿var VendorId;
$(document).ready(function () {
    bindCardType();
    $('.date').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtSFromDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    }); 
    $('#txtSToDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    }); 
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    
    VendorId = getQueryString('vendorId');
    VendorDetails(VendorId);
    CustomerDocument();
    //getOrderList(1); 
    CollectionDetails();
    GetBankDetailsList(); 
    getPayList(1);
    OrderLog(1);
    getReceivedBill(1);
});

$('#EditVendor').click(function () {
    var VendorId = $("#spnVendorId").text();
    window.location.href = "/Admin/vendorMaster.aspx?vendorId=" + VendorId;
})

function VendorDetails(VendorId) {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WvenderDetails.asmx/ViewVenderDetails",
        data: "{'VendorId':'" + VendorId + "'}",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },

        success: function (response) {

            var xmldoc = $.parseXML(response.d);
            var vendor = $(xmldoc).find("Table");

            $("#spnVendorId").text($(vendor).find("Vendorid").text());
            $("#spnVendorName").text($(vendor).find("VendorName").text());
            $("#spanContactPerson").text($(vendor).find("ContactPerson").text());
            $("#spanVendorType").text($(vendor).find("LocationAutoId").text());
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}


//function Pagevalue1(e) {
//    getPayList(parseInt($(e).attr("pagers")));
//};

function getOrderList(pageIndex) {
    var data = {
        VendorId: VendorId,
        pageIndex: pageIndex,
        PageSize: $("#ddlPageSize").val(),
    };
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WvenderDetails.asmx/PoList",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var POList = $(xmldoc).find("Table1");
                $("#tblPOList tbody tr").remove();
                var row = $("#tblPOList thead tr").clone(true);
                var gTotal = 0.00;
                if (POList.length > 0) {
                    $("#EmptyTable").hide();
                    $.each(POList, function () {
                        $(".BillNo", row).html("<a target='_blank' href='/Purchase/PurchaseOrderDetails.aspx?Id=" + $(this).find("AutoId").text() + "'>" + $(this).find("PONo").text() + "</a>");
                        $(".BillDate", row).text($(this).find("PODate").text());
                        $(".VendorName", row).text($(this).find("VendorName").text());
                        $(".NoOfItems", row).text($(this).find("NoofItems").text());
                        $(".Status", row).text($(this).find("POStatus").text());
                        $(".Remarks", row).text($(this).find("PORemarks").text());
                        $("#tblPOList tbody").append(row);
                        row = $("#tblPOList tbody tr:last").clone(true);
                    });
                } else {
                    $("#EmptyTable").show();
                }
                var pager = $(xmldoc).find("Table");
                $(".Pager").ASPSnippets_Pager({
                    ActiveCssClass: "current",
                    PagerCssClass: "pager",
                    PageIndex: parseInt(pager.find("PageIndex").text()),
                    PageSize: parseInt(pager.find("PageSize").text()),
                    RecordCount: parseInt(pager.find("RecordCount").text())
                });
            } else {
                location.href = "/";
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function getReceivedBill(pageIndex) {
    var data = {
        VendorId: VendorId,
        pageIndex: pageIndex,
        PageSize: $("#ddlPageSize").val(),
    };
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WvenderDetails.asmx/getReceivedBill",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var ReceivedBill = $(xmldoc).find("Table");
                $("#tblReceivedBill tbody tr").remove();
                var row = $("#tblReceivedBill thead tr").clone(true);
                var GTotal = 0.00; TotalPaid = 0.00; TotalDue = 0.00;
                if (ReceivedBill.length > 0) {
                    $("#EmptyTable12").hide();
                    $.each(ReceivedBill, function () {
                        if ($("#hEmpTypeNo").val() == '6') {
                            $(".BillNo", row).text($(this).find("BillNo").text());
                        }
                        else {
                            if (parseFloat($(this).find("TotalAmount").text()) == parseFloat($(this).find("PaidAmount").text())) {
                                $(".BillNo", row).text($(this).find("BillNo").text());
                            }
                            else {
                                $(".BillNo", row).html("<a target='_blank' href='/Admin/stockEntry.aspx?BillAutoId=" + $(this).find("AutoId").text() + "'>" + $(this).find("BillNo").text() + "</a>");
                            }
                        }
                        $(".BillDate", row).text($(this).find("BillDate").text());
                        $(".VendorName", row).text($(this).find("VendorName").text());
                        $(".NoofItems", row).text($(this).find("NoOfItems").text());
                        $(".PONo", row).text($(this).find("Reference_Code").text());
                        $(".TotalAmount", row).text($(this).find("TotalAmount").text());
                        $(".PaidAmount", row).text($(this).find("PaidAmount").text());
                        $(".DueAmount", row).text($(this).find("DueAmount").text());
                        $(".ReceivedBy", row).text($(this).find("UpdatedBy").text());
                        $(".ReceivedDate", row).text($(this).find("UpdateDate").text());
                        $("#tblReceivedBill tbody").append(row);
                        row = $("#tblReceivedBill tbody tr:last").clone(true);
                        GTotal = Number(GTotal) + Number($(this).find("TotalAmount").text());
                        TotalPaid = Number(TotalPaid) + Number($(this).find("PaidAmount").text());
                        TotalDue = Number(TotalDue) + Number($(this).find("DueAmount").text());
                    });
                    $("#TotalAmounts").text(Number(GTotal).toFixed(2));
                    $("#PaidAmounts").text(Number(TotalPaid).toFixed(2));
                    $("#DueAmounts").text(Number(TotalDue).toFixed(2));
                } else {
                    $("#EmptyTable12").show();
                    $("#TotalAmounts").text('0.00');
                    $("#PaidAmounts").text('0.00');
                    $("#DueAmounts").text('0.00');
                }
                var pager = $(xmldoc).find("Table1");
                $("#PagingReceived").ASPSnippets_Pager({
                    ActiveCssClass: "current",
                    PagerCssClass: "pager",
                    PageIndex: parseInt(pager.find("PageIndex").text()),
                    PageSize: parseInt(pager.find("PageSize").text()),
                    RecordCount: parseInt(pager.find("RecordCount").text())
                });
            } else {
                location.href = "/";
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}


function CollectionDetails() {
    var data = {
        VendorId: VendorId,
    };

    $.ajax({
        type: "POST",
        async: false,
        url: "/Admin/WebAPI/WvenderDetails.asmx/getPayList",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });           
        },

        complete: function () {
            $.unblockUI();
           
        },
        success: function (response) {

            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var PayDetails = $(xmldoc).find("Table");

                $("#tblCollectionDetails tbody tr").remove();
                var row = $("#tblCollectionDetails thead tr").clone(true);

                if (PayDetails.length > 0) {
                    $("#EmptyTable").hide();
                    $.each(PayDetails, function () {
                        var html = '';
                        if ((Number($(this).find("Status").text()) == 1)) {
                            if (html != '') {
                                $(".action", row).html(html);
                            } else {
                                $(".action", row).html("<input type='radio' name='name' onclick='funcheck(this)'/> ");
                            }
                        }
                        $(".PayID", row).text($(this).find("PayId").text());
                        $(".VendorName", row).text($(this).find("VendorName").text());
                        $(".PaymentDate", row).text($(this).find("PaymentDate").text());
                        $(".PaymentAmount", row).text($(this).find("PaymentAmount").text());
                        $(".PaymentMode", row).text($(this).find("PaymentMode").text());
                        $(".ReferenceId", row).text($(this).find("ReferenceID").text());
                        $(".Status", row).text($(this).find("Status").text());
                        $(".Remark", row).text($(this).find("Remark").text());
                        if ($(this).find("Status").text() == '1') {
                            $(".Status", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("StatusType").text() + "</span>");
                        } else {
                            $(".Status", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("StatusType").text() + "</span>");
                        }
                        $("#tblCollectionDetails tbody").append(row);
                        row = $("#tblCollectionDetails tbody tr:last").clone(true);
                    });
                }

                var pager = $(xmldoc).find("Table1");
                $(".Pager").ASPSnippets_Pager({
                    ActiveCssClass: "current",
                    PagerCssClass: "pager",
                    PageIndex: parseInt(pager.find("PageIndex").text()),
                    PageSize: parseInt(pager.find("PageSize").text()),
                    RecordCount: parseInt(pager.find("RecordCount").text())
                });
            } else {
                location.href = "/";
            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
//-------------------------------------------------------------------Upload Documents----------------------------------------------
function uploaddocument() {
    $('#txtDocumentName').val('');
    $('#uploadedFile').val('');
    $('#Button3').hide();
    $('#Button2').show();
    $("#hrefViewDoc").hide();
    $('#PopCustomerDocuments').modal('show');
}
function DocumentSave() { //using ValidationGroup document
    debugger;
    if (dynamicALL('document1')) {
        //validation
        var ext = $('#uploadedFile').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg', 'doc', 'pdf']) == -1) {
            toastr.error('Invalid file.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            return;
        }
        //ENd       
        var fileUpload = $("#uploadedFile").get(0);
        var files = fileUpload.files;
        var test = new FormData();
        for (var i = 0; i < files.length; i++) {
            test.append(files[i].name, files[i]);
        }
        $.ajax({
            url: "/UploadDocument.ashx?Id=" + VendorId,
            type: "POST",
            contentType: false,
            processData: false,
            data: test,
            success: function (resultupload) {
                if (resultupload != '' || resultupload != null) {
                    var data = {
                        VendorId: VendorId,
                        DocumentName: $('#txtDocumentName').val(),
                        DocumentURL: resultupload
                    }
                    $.ajax({
                        type: "Post",
                        url: "/Admin/WebAPI/WvenderDetails.asmx/DocumentSave",
                        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function () {
                            $.blockUI({
                                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                                overlayCSS: {
                                    backgroundColor: '#FFF',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'transparent'
                                }
                            });
                        },
                        complete: function () {
                            $.unblockUI();
                        },
                        success: function (data) {
                            if (data.d == 'Session Expired') {
                                location.href = '/';
                            } else if (data.d == 'true') {
                                $('#txtDocumentName').val('');
                                $('#uploadedFile').val('');
                                swal("", "Document details saved successfully.", "success");
                                $('#PopCustomerDocuments').modal('hide');
                                CustomerDocument(); clearBorder();
                                
                            } else {
                                swal("Error !", "Oops! Something went wrong.Please try later.", "error");
                            }
                        },
                        error: function (result) {
                        },
                        failure: function (result) {
                        }
                    });
                }
            },
            error: function (err) {
                console.log(err);
            }
        });

    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
function CustomerDocument() {
    var data = {
        VendorId: VendorId
    }
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WvenderDetails.asmx/CustomerDocument",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var customerDocument = $(xmldoc).find('Table');
                $("#tblCustomerDocuments tbody tr").remove();
                var row = $("#tblCustomerDocuments thead tr").clone(true);
                if (customerDocument.length > 0) {
                    $.each(customerDocument, function (index) {
                        $(".SrNo", row).html((Number(index) + 1) + "<span FileAutoId=" + $(this).find("AutoId").text() + "></span>");
                        $(".Document", row).html("<a title='Download' href='javascript:void(0)' onclick='downloadfile(\"" + $(this).find("DocumentURL").text() + "\")'><span class='la la-download'></span></a>&nbsp;&nbsp;<a title='View' target='_default'   href='/UploadDocument/" + $(this).find("DocumentURL").text() + "'><span class='la la-eye'></span></a>");
                        $(".ViewDownload", row).html("<a title='Edit' href='javascript:void(0)' onclick='Editdownloadfile(this)'><span class='la la-edit'><span></a>&nbsp;&nbsp;<a title='Delete' href='javascript:void(0)' onclick='deleterecordDocument(\"" + $(this).find("AutoId").text() + "\")'><span class='la la-remove'></span></a>");
                        $(".DocumentName", row).text($(this).find("DocumentName").text());
                        $("#tblCustomerDocuments tbody").append(row);
                        row = $("#tblCustomerDocuments tbody tr:last").clone(true);
                    });
                }
            } else {
                location.href = '/';
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function downloadfile(string) {
    var a = document.createElement('a');
    var url = '/UploadDocument/' + string;
    a.href = url;
    a.download = string;
    a.click();
    window.URL.revokeObjectURL(url);
}
function Editdownloadfile(e) {
    debugger;
    var tr = $(e).closest('tr');
    $('#hdnuploadAutoId').val(tr.find('.SrNo span').attr('FileAutoId'));
    $('#txtDocumentName').val(tr.find('.DocumentName').text());

    if (tr.find('.Document a:last-child').attr('href') != "") {
        $("#hrefViewDoc").show();
        $("#hrefViewDoc").attr('href', tr.find('.Document a:last-child').attr('href'));
    }
    else {
       $("#hrefViewDoc").hide();
    }
    $('#PopCustomerDocuments').modal('show');
    $('#Button2').hide();
    $('#Button3').show();
}
function UpdateDocument() {
    debugger
    if (dynamicALL('documents')) {
        //validation
        var ext = $('#uploadedFile').val().split('.').pop().toLowerCase();
        if (ext != "") {
            if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg', 'doc', 'pdf', 'jfif']) == -1) {
                toastr.error('Invalid file.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                return;
            }
        }
        //ENd
        var fileUpload = $("#uploadedFile").get(0);
        var files = fileUpload.files;
        var test = new FormData();
        for (var i = 0; i < files.length; i++) {
            test.append(files[i].name, files[i]);
        }
        $.ajax({
            url: "/UploadDocument.ashx?Id=" + VendorId,
            type: "POST",
            contentType: false,
            processData: false,
            data: test,
            success: function (resultupload) {
                if (resultupload != '' || resultupload != null) {
                    var data = {
                        FileAutoId: $('#hdnuploadAutoId').val(),
                        DocumentName: $('#txtDocumentName').val(),
                        DocumentURL: resultupload
                    }
                    $.ajax({
                        type: "Post",
                        url: "/Admin/WebAPI/WvenderDetails.asmx/UpdateDocument",
                        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function () {
                            $.blockUI({
                                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                                overlayCSS: {
                                    backgroundColor: '#FFF',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'transparent'
                                }
                            });
                        },
                        complete: function () {
                            $.unblockUI();
                        },
                        success: function (data) {
                            if (data.d == 'Session Expired') {
                                location.href = '/';
                            } else if (data.d == 'true') {
                                $('#txtDocumentName').val('');
                                $('#uploadedFile').val('');
                                swal("", "Document details updated successfully", "success");
                                $('#PopCustomerDocuments').modal('hide');
                                CustomerDocument(); clearBorder();
                            } else {
                                swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                            }
                        },
                        error: function (result) {
                        },
                        failure: function (result) {
                        }
                    });
                }
            },
            error: function (err) {
                console.log(err);
            }
        });

    } else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
function clearBorder() {
    $("#txtDocumentName").removeClass('border-warning');
    $("#uploadedFile").removeClass('border-warning');
}
function deleterecordDocument(FileAutoId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete document!",
        icon: "warning",
        showCancelButton: true,
        allowOutsideClick: false,
        closeOnClickOutside: false,
        buttons: {
            cancel: {
                text: "No, Cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, Delete It!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            DeleteDocument(FileAutoId);
        }
    })
}
function DeleteDocument(FileAutoId) {
    $.ajax({
        type: "Post",
        url: "/Admin/WebAPI/WvenderDetails.asmx/DeleteDocument",
        data: "{'FileAutoId':'" + FileAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (data) {
            if (data.d == 'Session Expired') {
                location.href = '/';
            } else if (data.d == 'true') {
                swal("", "Document details deleted successfully", "success");
            } else {
                swal("Error", "Oops! Something went wrong.Please try later.", "error");
            }
            CustomerDocument();
        },
        error: function (result) {
        },
        failure: function (result) {
        }
    });
}
function OpenPopup() {
    $('#txtRoutingNo').val('');
    $("#txtBankName").val('');
    $("#txtACName").val('');
    $("#ddlCardType").val('0');
    $("#txtCardNo").val('');
    $("#txtCVV").val('');
    $("#txtExpiryDate").val('');
    $("#txtBankRemarks").val('');
    $('#btnUpdates').hide();
    $('#btnSaves').show();
    $('#PopBankDetails').modal('show');
}
function checkCVVLength() { 
    if ($("#txtCVV").val().length > 2) {
        $("#txtCVV").attr('style', 'border:1px solid #666EE8  !important');
    } else {
        $("#txtCVV").attr('style', 'border:1px solid #FF9149  !important');
        $("#txtCVV").val('');
    }
}
function OpenPopup1() {
    $("#txtBankName").val('');
    $("#txtACName").val('');
    $("#ddlCardType").val('0');
    $("#txtCardNo").val('');
    $("#txtCVV").val('');
    $("#txtZipcode").val('');
    $("#txtExpiryDate").val('');
    $("#txtCardRemarks").val('');
    $('#btnUpdates1').hide();
    $('#btnSaves1').show();
    $('#PopCardDetails').modal('show');
    bindCardType();
}
function bindCardType() {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WvenderDetails.asmx/bindCardType",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },

        success: function (response) {
            if (response.d == 'Session Expired') {
                location.href = '/';
            } else if (response.d != 'false') {
                var xmldoc = $.parseXML(response.d);
                var CardType = $(xmldoc).find('Table');
                $("#ddlCardType option:not(:first)").remove();  //-------------------------------For Search Field
                $.each(CardType, function () {
                    $("#ddlCardType").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("CardType").text() + "</option>");
                });
                $("#ddlCardType").select2();
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function SaveBankDetails() {
    if (checkRequiredField()) {
        var data = {
            BankName: $("#txtBankName").val(),
            ACName: $("#txtACName").val(),
            RoutingNo: $("#txtRoutingNo").val(),
            Remarks: $("#txtBankRemarks").val(),
            VendorId: VendorId,
             Status: '1'
        }
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/WvenderDetails.asmx/SaveBankDetails",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == 'false') {
                    swal("Error!", "This Account No. is already exist.", "error");
                }
                else if (result.d == 'Unauthorized access.') {
                    location.href = "/";
                }
                else {
                    swal("", "Bank details saved successfully.", "success");
                    $('#PopBankDetails').modal('hide');
                    $("#txtBankName").val('');
                    $("#txtACName").val('');
                    $("#ddlCardType").val('0');
                    $("#txtCardNo").val('');
                    $("#txtCVV").val('');
                    $("#txtExpiryDate").val('');
                    GetBankDetailsList();
                }
            },
            error: function (result) {
                swal("Error!", result.d, "error");
            },
            failure: function (result) {
                swal("Error!", result.d, "error");
            }
        });
    } else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}


function SaveBankDetails1() {
    if (dynamicALL('ddlCardreq')) {
        if ($("#txtCardNo").val().length == 16) {
            var data = {
                CardType: $("#ddlCardType").val(),
                CardNo: $("#txtCardNo").val(),
                ExpiryDate: $("#txtExpiryDate").val(),
                CVV: $("#txtCVV").val(),
                Remarks: $("#txtCardRemarks").val(),
                VendorId: VendorId,
                Status: '2',
                Zipcode: $("#txtZipcode").val()
            }
            $.ajax({
                type: "POST",
                url: "/Admin/WebAPI/WvenderDetails.asmx/SaveBankDetails",
                data: "{'dataValue':'" + JSON.stringify(data) + "'}",
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (result) {
                    if (result.d == 'false') {
                        swal("Error!", "This Card No. is already exist.", "error");
                    }
                    else if (result.d == 'Unauthorized access.') {
                        location.href = "/";
                    }
                    else {
                        swal("", "Card details saved successfully.", "success");
                        $('#PopCardDetails').modal('hide');
                        $("#txtBankName").val('');
                        $("#txtACName").val('');
                        $("#ddlCardType").val('0');
                        $("#txtCardNo").val('');
                        $("#txtCVV").val('');
                        $("#txtExpiryDate").val('');
                        $("#txtZipcode").val('');
                        GetBankDetailsList();
                    }
                },
                error: function (result) {
                    swal("Error!", result.d, "error");
                },
                failure: function (result) {
                    swal("Error!", result.d, "error");
                }
            });
    } else {
        $("#txtCardNo").attr('style', 'border:1px solid #FF9149  !important');
        toastr.error('Card No. should be enter 16 digits', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        }
    } else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}

//------------------------------------------------Bind Bank Details List-------------------------------------
function GetBankDetailsList() {
    var data = {
        VendorId: VendorId,
    };
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WvenderDetails.asmx/GetBankDetailsList",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successGetBankDetailsList,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function successGetBankDetailsList(response) {
    var xmldoc = $.parseXML(response.d);
    var BaknDetailsList = $(xmldoc).find("Table");
    var CardDetailsList = $(xmldoc).find("Table1");
    $("#tblBankDetails tbody tr").remove();

    var row = $("#tblBankDetails thead tr").clone(true);
    if (BaknDetailsList.length > 0) {
        $("#EmptyTable").hide();
        $.each(BaknDetailsList, function (index) {
            $(".BankName", row).text($(this).find("BankName").text());
            $(".BankACC", row).text($(this).find("BankACC").text());
            $(".Routing", row).text($(this).find("RoutingNo").text());
            $(".BankRemarks", row).text($(this).find("BankRemark").text());
            $(".Action", row).html("<a title='Edit' href='#'><span class='ft-edit' onclick='EditBankDetails(" + $(this).find("AutoId").text() + ")'></span></a>&nbsp;<a title='Delete' href='#'><span class='ft-x' onclick='DeleteBankDetails(" + $(this).find("AutoId").text() + ")'></span></a>");
            $("#tblBankDetails tbody").append(row);
            row = $("#tblBankDetails tbody tr:last").clone(true);
        });

    } else {
        $("#EmptyTable").show();
    }

    $("#tblCardDetails tbody tr").remove();

    var row = $("#tblCardDetails thead tr").clone(true);
    if (CardDetailsList.length > 0) {
        $("#EmptyTable").hide();
        $.each(CardDetailsList, function (index) {
            $(".CardType", row).text($(this).find("CardType").text());
            $(".CardNo", row).text($(this).find("CardNo").text());
            $(".ExpiryDate", row).text($(this).find("ExpiryDate").text());
            $(".CVV", row).text($(this).find("CVV").text());
            $(".Zipcode", row).text($(this).find("Zipcode").text());
            $(".CardRemarks", row).text($(this).find("CardRemark").text());
            $(".Action", row).html("<a title='Edit' href='#'><span class='ft-edit' onclick='EditBankDetails1(" + $(this).find("AutoId").text() + ")'></span></a>&nbsp;<a title='Delete' href='#'><span class='ft-x' onclick='DeleteCardDetails(" + $(this).find("AutoId").text() + ")'></span></a>");
            $("#tblCardDetails tbody").append(row);
            row = $("#tblCardDetails tbody tr:last").clone(true);
        });

    } else {
        $("#EmptyTable").show();
    }
}
function EditBankDetails(AutoId) {
    $("#hdnAutoId").val(AutoId);
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WvenderDetails.asmx/EditBankDetails",
        data: "{'AutoId':'" + AutoId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: onSuccessOfEditBankDetails,
        error: function (response) {
            console.log(response.d)
        }
    })
}
function onSuccessOfEditBankDetails(response) {
    if (response.d == 'Session Expired') {
        location.href = '/';
    } else {
        $('#PopBankDetails').modal('show');

        var xmldoc = $.parseXML(response.d);
        var Details = $(xmldoc).find('Table');
        $("#txtBankName").val($(Details).find("BankName").text());
        $("#txtACName").val($(Details).find("BankACC").text());
        $("#txtRoutingNo").val($(Details).find("RoutingNo").text());
        $("#txtBankRemarks").val($(Details).find("BankRemark").text());
        $("#btnSaves").hide();
        $("#btnReset").hide();
        $("#btnUpdates").show();
        $("#btnCancel").show();
    }
}

function EditBankDetails1(AutoId) {
    $("#hdnCardAutoId").val(AutoId);
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WvenderDetails.asmx/EditBankDetails",
        data: "{'AutoId':'" + AutoId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: onSuccessOfEditBankDetails1,
        error: function (response) {
            console.log(response.d)
        }
    })
}
function onSuccessOfEditBankDetails1(response) {
    if (response.d == 'Session Expired') {
        location.href = '/';
    } else {
        $('#PopCardDetails').modal('show');

        var xmldoc = $.parseXML(response.d);
        var Details = $(xmldoc).find('Table');
        $("#ddlCardType").val($(Details).find("CardTypeAutoId").text()).change();
        $("#txtExpiryDate").val($(Details).find("ExpiryDate").text());
        $("#txtCVV").val($(Details).find("CVV").text());
        $("#txtCardNo").val($(Details).find("CardNo").text());
        $("#txtZipcode").val($(Details).find("Zipcode").text());
        $("#txtCardRemarks").val($(Details).find("CardRemark").text());
        $("#btnSaves1").hide();
        $("#btnReset").hide();
        $("#btnUpdates1").show();
        $("#btnCancel").show();
    }
}



function UpdateBankDetails() {
    if (checkRequiredField()) {
        var data = {
            BankName: $("#txtBankName").val(),
            ACName: $("#txtACName").val(),
            RoutingNo: $("#txtRoutingNo").val(),
            AutoId: $("#hdnAutoId").val(),
            Remarks: $("#txtBankRemarks").val(),
            Status: '1'
        }
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/WvenderDetails.asmx/UpdateBankDetails",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == 'false') {
                    swal("Error!", "This Account No. is already exist..", "error");
                }
                else if (result.d == 'Unauthorized access.') {
                    location.href = "/";
                }
                else {
                    swal("", "Bank details saved successfully.", "success");
                    $('#PopBankDetails').modal('hide');
                    $("#txtBankName").val('');
                    $("#txtACName").val('');
                    $("#ddlCardType").val('0');
                    $("#txtCardNo").val('');
                    $("#txtCVV").val('');
                    $("#txtExpiryDate").val('');
                    GetBankDetailsList();
                }
            },
            error: function (result) {
                swal("Error!", result.d, "error");
            },
            failure: function (result) {
                swal("Error!", result.d, "error");
            }
        });
    } else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}

function UpdateBankDetails1() {
    if (dynamicALL('ddlCardreq')) {
        var data = {
            //BankName: $("#txtBankName").val(),
            //ACName: $("#txtACName").val(),
            CardType: $("#ddlCardType").val(),
            CardNo: $("#txtCardNo").val(),
            ExpiryDate: $("#txtExpiryDate").val(),
            CVV: $("#txtCVV").val(),
            AutoId: $("#hdnCardAutoId").val(),
            Zipcode: $("#txtZipcode").val(),
            Remarks: $("#txtCardRemarks").val(),
            Status:'2'
        }
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/WvenderDetails.asmx/UpdateBankDetails",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == 'false') {
                    swal("Error!", "This Card No. is already exist.", "error");
                }
                else if (result.d == 'Unauthorized access.') {
                    location.href = "/";
                }
                else {
                    swal("", "Card details saved successfully.", "success");
                    $('#PopCardDetails').modal('hide');
                    $("#txtBankName").val('');
                    $("#txtACName").val('');
                    $("#ddlCardType").val('0');
                    $("#txtCardNo").val('');
                    $("#txtCVV").val('');
                    $("#txtExpiryDate").val('');
                    $("#txtZipcode").val('');
                    GetBankDetailsList();
                }
            },
            error: function (result) {
                swal("Error!", result.d, "error");
            },
            failure: function (result) {
                swal("Error!", result.d, "error");
            }
        });
    } else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
function DeleteBankDetails(AutoId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this Details.",
        icon: "warning",
        showCancelButton: true,
        allowOutsideClick: false,
        closeOnClickOutside: false,
        buttons: {
            cancel: {
                text: "No, Cancel.",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, Delete it.",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(isConfirm => {
        if (isConfirm) {
            DeleteDetail(AutoId);
        } 
    })
}
function DeleteCardDetails(AutoId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this Details.",
        icon: "warning",
        showCancelButton: true,
        allowOutsideClick: false,
        closeOnClickOutside: false,
        buttons: {
            cancel: {
                text: "No, cancel.",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, delete it.",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(isConfirm => {
        if (isConfirm) {
            DeleteCaDetail(AutoId);
        }
    })
}
/*--------------------------------------------------------Delete Bank Details ----------------------------------------------------------*/
function DeleteDetail(AutoId) {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WvenderDetails.asmx/DeleteBankDetails",
        data: "{'AutoId':'" + AutoId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },

        success: function (result) {
            swal("", "Bank details deleted successfully.", "success");
            var table = $('#tblBankDetails').DataTable();
            table.destroy();
            GetBankDetailsList();
        },
        error: function (result) {
            swal("error", "Bank details already assigned to Orders.", "error");
        },
        failure: function (result) {
            swal("error", "Bank details already assigned to Orders.", "error");
        }
    })
}
//-----------------------------------------------------Delete Card------------------------------------------
function DeleteCaDetail(AutoId) {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WvenderDetails.asmx/DeleteBankDetails",
        data: "{'AutoId':'" + AutoId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },

        success: function (result) {
            swal("", "Card details deleted successfully.", "success");
            var table = $('#tblBankDetails').DataTable();
            table.destroy();
            GetBankDetailsList();
        },
        error: function (result) {
            swal("error", "Card details already assigned to Orders.", "error");
        },
        failure: function (result) {
            swal("error", "Card details already assigned to Orders.", "error");
        }
    })
}
//-----------------------------------------Delete Vender-------------------------------------------
function deleteVendor(VendorId) {

    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WvenderDetails.asmx/deleteVendor",
        data: "{'VendorId':'" + VendorId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },

        success: function (response) {
            if (response.d == "Unauthorized") {
                location.href = "/";
            }
            else if (response.d == "success") {
                swal("", "Vendor details deleted successfully.", "success");
                location.href = "/Admin/VendorList.aspx";
            }
           
            else {
                swal("", response.d, "error");
            }
        },
        error: function (result) {
            swal("", "This Vendor cannot be deleted as Orders has been placed to this Vendor in the Past.", "error");
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);

        }
    })

}
function deleterecord() {
    swal({
        title: "Are you sure?",
        text: "You want to delete this vendor.",
        icon: "warning",
        showCancelButton: true,
        allowOutsideClick: false,
        closeOnClickOutside: false,
        buttons: {
            cancel: {
                text: "No, Cancel.",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, Delete it.",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(isConfirm => {
        if (isConfirm) {
            deleteVendor(VendorId);
        }
    });
};
function Pagevalue(e) {
    var divid = $(e).parent().attr('id');
    if (divid == 'PayHis') {
        getPayList(parseInt($(e).attr("page")));
    }
    if (divid == 'OrdLog') {
        OrderLog(parseInt($(e).attr("page")));
    }
    if (divid == 'PagingReceived') {
        getReceivedBill(parseInt($(e).attr("page")));
    }
};
function getPayList(pageIndex) {
    debugger;
    var data = {
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        VendorAutoId: VendorId,
        pageIndex: pageIndex
    };

    $.ajax({
        type: "POST",
        async: false,
        url: "/Admin/WebAPI/WvenderDetails.asmx/getPayLists",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },

        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var PayDetails = $(xmldoc).find("Table1");
                var VendorLog = $(xmldoc).find("Table3");
                $("#tblPayDetails tbody tr").remove();
                var row = $("#tblPayDetails thead tr").clone(true);

                if (PayDetails.length > 0) {
                    $("#EmptyTable").hide();
                    $.each(PayDetails, function () {
                        if ($(this).find("Status").text() == 1) {

                            $(".action", row).html("<a title='Edit' href='#' onclick='fnEdit(" + $(this).find("PaymentAutoId").text() + ")'><span class='ft-edit'></span></a></b>");
                        } else {
                            $(".action", row).html("");
                        } $(".PayID", row).text($(this).find("PayId").text());
                        $(".VendorName", row).text($(this).find("VendorName").text());
                        $(".PaymentDate", row).text($(this).find("PaymentDate").text());
                        $(".PaymentAmount", row).text($(this).find("PaymentAmount").text());
                        $(".PaymentMode", row).text($(this).find("PaymentMode").text());
                        $(".PaymentType", row).text($(this).find("PaymentType").text());
                        $(".ReferenceId", row).text($(this).find("NoOfBill").text());
                        $(".Status", row).text($(this).find("Status").text());
                        $(".Remark", row).text($(this).find("Remark").text());
                        if ($(this).find("Status").text() == '1') {
                            $(".Status", row).html("<span class='badge badge badge-pill badge-info'>" + $(this).find("StatusType").text() + "</span>");
                        }
                        else if ($(this).find("Status").text() == '2') {
                            $(".Status", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("StatusType").text() + "</span>");
                        } else {
                            $(".Status", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("StatusType").text() + "</span>");
                        }
                        $("#tblPayDetails tbody").append(row);
                        row = $("#tblPayDetails tbody tr:last").clone(true);
                    });
                }
               
                var pager = $(xmldoc).find("Table");
                $("#PayHis").ASPSnippets_Pager({
                    ActiveCssClass: "current",
                    PagerCssClass: "pager",
                    PageIndex: parseInt(pager.find("PageIndex").text()),
                    PageSize: parseInt(pager.find("PageSize").text()),
                    RecordCount: parseInt(pager.find("RecordCount").text())
                });
               

            } else {
                location.href = "/";
            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function OrderLog(pageIndex) {
    var data = {
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        VendorAutoId: VendorId,
        pageIndex: pageIndex
    };

    $.ajax({
        type: "POST",
        async: false,
        url: "/Admin/WebAPI/WvenderDetails.asmx/getPayLists",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            }); 
        },

        complete: function () {
            $.unblockUI(); 
        },
        success: function (response) {

            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var PayDetails = $(xmldoc).find("Table1");
                var VendorLog = $(xmldoc).find("Table3");
               
                //-----------------------------------------------------------------------------------------------------------------------------------------------
                $("#tblOrderLog tbody tr").remove();
                var row = $("#tblOrderLog thead tr").clone(true);

                if (VendorLog.length > 0) {
                    $("#EmptyTable").hide();
                    $.each(VendorLog, function (index) { 
                        $(".Action", row).text($(this).find("Action").text());
                        $(".SrNo", row).text($(this).find("RowNumber").text());
                        $(".ActionBy", row).text($(this).find("ActionBy").text());
                        $(".Date", row).text($(this).find("ActionDate").text());
                        $(".Remark", row).text($(this).find("Remarks").text());
                        $("#tblOrderLog tbody").append(row);
                        row = $("#tblOrderLog tbody tr:last").clone(true);
                    });
                }

                var pager = $(xmldoc).find("Table2");
                $("#OrdLog").ASPSnippets_Pager({
                    ActiveCssClass: "current",
                    PagerCssClass: "pager",
                    PageIndex: parseInt(pager.find("PageIndex").text()),
                    PageSize: parseInt(pager.find("PageSize").text()),
                    RecordCount: parseInt(pager.find("RecordCount").text())
                });
            
            } else {
                location.href = "/";
            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function fnEdit(PaymentAutoId) {
    location.href = '/Vendor/VendorPayMaster.aspx?PaymentId=' + PaymentAutoId + '&VendorId=' + VendorId;
}
function dtval(d, e) {
    var pK = e ? e.which : window.event.keyCode;
    if (pK == 8) { d.value = substr(0, d.value.length - 1); return; }
    var dt = d.value;

    var da = dt.split('/');
    for (var a = 0; a < da.length; a++) { if (da[a] != +da[a]) da[a] = da[a].substr(0, da[a].length - 1); }

    if (da[0] > 12) { da[1] = da[0].substr(da[0].length - 1, 1); da[0] = '0' + da[0].substr(0, da[0].length - 1); }

    if (da[1] > 99) da[0] = da[1].substr(0, da[1].length - 1);

    dt = da.join('/');
    if (dt.length == 2) dt += '/';
    d.value = dt;
    //var dtDate = new Date();
    //var month = dtDate.getMonth() + 1;
    //if (Number(d.value) > Number(month)) {
    //    d.value = '';
    //}
}
function dtexp(d) {
    debugger;
    var month = new Date().getMonth() + 1;
    var year = new Date().getFullYear();
    year = year.toString().substr(2, 2);
    var dt = d.value;
    if (d.value.substr(2, 1) != '/') { d.value = ''; toastr.error('Invalid Expiry Date.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' }); }
    var da = dt.split('/');
    if (Number(da[1]) < Number(year)) { d.value = ''; toastr.error('Invalid Expiry Date.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' }); }
    if (Number(da[1]) == Number(year) & Number(da[0]) < Number(month)) {

        d.value = '';
        toastr.error('Invalid Expiry Date.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}

