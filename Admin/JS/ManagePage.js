﻿var actionListArr = [];

$(document).ready(function () {
    getPageList();

    getParentModule();
    var getPage= getPageId("PageId");
    if (getPage != '') {
        editPage(getPage);
        getActionList(getPage);
    }
});
function getPageId(args) {
    var url = window.location.href.slice(window.location.href.indexOf("?") + 1).split();
    var pageId = "";
    $(url).each(function () {
        var arr = this.split("=");
        if (arr[0] == args) {
            pageId = arr[1];
        }
    });
    return pageId;
}

function removeAction(indexValue, ActionName,PageAutoId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this action",
        icon: "warning",
        buttons: {
            cancel: {
                text: "No, cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: false,
            },
            confirm: {
                text: "Yes, delete it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm, e) {
        if (isConfirm) {
            var data = {
                PageAction: ActionName,
                PageAutoId: PageAutoId
            }
            $.ajax({
                type: "POST",
                url: "/Admin/WebAPI/WManagePage.asmx/checkActionBeforeRemove",
                data: "{'dataValue':'" + JSON.stringify(data) + "'}",
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                beforeSend: function () {

                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (result) {
                    console.log(result);
                    if (result.d == 'Success') {
                        actionListArr.splice(indexValue, 1);
                        swal("", "Action deleted successfully.", "success");
                        actionListHtml(PageAutoId);

                    }
                    else if (result.d == 'Error') {
                        swal("", "You can not change/delete the action that action has already assigned to a role .", "error");
                    }
                    else {
                        swal("", "Oops! Something went wrong.Please try later.", "error");
                    }
                },
                error: function (result) {
                    swal("Error!", result.d, "error");
                },
                failure: function (result) {
                    swal("Error!", result.d, "error");
                }
            });
        } else {
            swal("", "Your action is safe.", "error");
        }
    })
}

$("#savePageBtn").click(function () {
    if (checkRequiredField()) {

        var data = {
            PageName: $("#txtPageName").val(),
            PageUrl: $("#txtPageUrl").val(),
            Status: $("#status").val(),
            Description: $("#txtDescription").val(),
            PageType: $("#pageType").val(),
            UserType: $("#UserType").val(),
            ModuleAutoId: $("#module").val()
        }
        
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/WManagePage.asmx/insert",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {

                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                console.log(result);
                
                if (result.d == "Page name already exist") {
                    swal("", "Page name already exist.", "error").then(function () {
                        return flase;
                    });
                }
                else if (result.d == "Page url already exist") {
                    swal("", "Page url already exist.", "error").then(function () {
                        return flase;

                    });
                }
                
                else if (result.d == 'Unauthorized access.') {
                    location.href = "/";
                }
                else {
                    var xmldoc = $.parseXML(result.d);
                    var pagesDetail = $(xmldoc).find('Table');
                    swal("", "Page details saved successfully.", "success").then(function () {
                        location.href = '/admin/ManagePage.aspx?PageId=' + $(pagesDetail).find("AutoId").text();
                    });
                }
            },
            error: function (result) {
                swal("Error!", result.d, "error");
            },
            failure: function (result) {
                swal("Error!", result.d, "error");
            }
        });
    } else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
});

function getPageList() {
    var data = {
        SearchPageId: $('#txtSearchPageId').val(),
        SearchPageName: $('#txtSearchPageName').val(),
        Status: $('#searchStatus').val()
    };
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WManagePage.asmx/getPageList",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (result) {
            var xmldoc = $.parseXML(result.d);

            var pages = $(xmldoc).find('Table');
            $("#pageList tbody tr").remove();
            var row = $("#pageList thead tr").clone(true);

            if (pages.length > 0) {
                $.each(pages, function () {
                  
                    $(".action", row).html("<a href='ManagePage.aspx?PageId=" + $(this).find("AutoId").text() + "' title='Edit'><span class='ft-edit' /></a>&nbsp;&nbsp;&nbsp;<a href='#' title='Remove'><span class='ft-x' onclick='deleterecord(" + $(this).find("AutoId").text() +")' /></a>");
                    $(".AutoId", row).text($(this).find("AutoId").text());
                    $(".Role", row).text($(this).find("Role").text());
                    $(".PageId", row).text($(this).find("PageId").text());
                    $(".PageName", row).text($(this).find("PageName").text());
                    $(".PageUrl", row).text($(this).find("PageUrl").text());
                    $(".Description", row).text($(this).find("Description").text());
                    if ($(this).find("Status").text() == '1') {
                        $(".Status", row).html("<span class='badge badge badge-pill badge-success'>Active</span>");
                        $(".HiddenStatus", row).text("1");
                    }
                    else {
                        $(".Status", row).html("<span class='badge badge badge-pill badge-danger'>Inactive</span>");
                        $(".HiddenStatus", row).text("0");
                    }
                    if ($(this).find("Type").text() == '1') {
                        $(".Type", row).text("Generic");
                        $(".HiddenType", row).text("1");
                    }
                    else {
                        $(".Type", row).text("Role Based");
                        $(".HiddenType", row).text("0");
                    }
                    $(".UserType", row).text($(this).find("TypeName").text());
                    $("#pageList tbody").append(row);
                    row = $("#pageList tbody tr:last-child").clone(true);
                });
            }

        },
        error: function (result) {

        },

    });
}

function clearField() {
    $("#txtPageName").val("");
    $("#txtPageUrl").val("");
    $("#txtDescription").val("");
    $("#PageId").val("");
    $("#txtActionName").val("");
    actionListArr = [];
    $(".actionListData").html('');
    return;
}
  
function editPage(PageId) {
    var data = {
        PageAutoId: PageId
    };
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WManagePage.asmx/editPage",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (result) {
            var xmldoc = $.parseXML(result.d);
            var pages = $(xmldoc).find('Table');

            var $AutoId = $(pages).find("AutoId").text();
            var $PageName = $(pages).find("PageName").text();
            var $PageUrl = $(pages).find("PageUrl").text();
            var $Description = $(pages).find("Description").text();
            var $PageId = $(pages).find("PageId").text();
            var $PageType = $(pages).find("Type").text();
            var $Status = $(pages).find("Status").text();
            var $Role = $(pages).find("Role").text();
            var $ParentModule = $(pages).find("ParentModuleAutoId").text();
            console.log($ParentModule);
            $("#hiddenAutoId").val($AutoId);
            $("#txtPageName").val($PageName);
            $("#txtPageUrl").val($PageUrl);
            $("#txtDescription").val($Description);
            $("#PageId").val($PageId);
            $("#pageType").val($PageType);
            $("#status").val($Status);
            getUserType($PageType);
            $("#UserType").val($Role).change();
            $("#module").val($ParentModule).change();
            $("#savePageBtn").hide();
            $("#btnReset").hide();
            $("#btnUpdate").show();
            $("#btnCancel").show();
            $(".actionDiv").show();

        }
    });

}

$("#btnCancel").click(function () {
    $("#savePageBtn").show();
    $("#btnReset").show();
    $("#btnUpdate").hide();
    $("#btnCancel").hide();
    clearField();
});

$("#btnUpdate").click(function () {
    if (checkRequiredField()) {
        var data = {
            AutoId: $("#hiddenAutoId").val(),
            PageName: $("#txtPageName").val(),
            PageUrl: $("#txtPageUrl").val(),
            Status: $("#status").val(),
            Description: $("#txtDescription").val(),
            PageType: $("#pageType").val(),
            ModuleAutoId: $("#module").val(),
            UserType: $("#UserType").val()

        }
        console.log(data);
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/WManagePage.asmx/update",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                console.log(result);
                if (result.d == 'Success') {
                    swal("", "Page detail updated successfully.", "success");
                }
                else if (result.d == "Page name already exist") {
                    swal("", "Page name already exist.", "error").then(function () {
                        return flase;
                    });
                }
                else if (result.d == "Page url already exist") {
                    swal("", "Page url already exist.", "error").then(function () {
                        return flase;

                    });
                }
                else if (result.d == 'Unauthorized access.') {
                    location.href = "/";
                }
                else {
                    swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                }
            },
            error: function (result) {
                swal("Error!", result.d, "error");
            },
            failure: function (result) {
                swal("Error!", result.d, "error");
            }
        });
    } else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
});

function getUserType(value)
{
    if (value == 0 || value == '0') {
        $(".userType").show();
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/WManagePage.asmx/selectUserType",
            data: "{}",
            async:false,
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (result) {
                var xmldoc = $.parseXML(result.d);
                var role = $(xmldoc).find('Table');
                $.each(role, function () {
                    $("#UserType").append($("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("TypeName").text().replace(/&quot;/g, "\'") + "</option>"));
                });
                
                $("#UserType").select2();


            },
            error: function (result) {

            },

        });
    }
    else {
        $(".userType").hide();
    }
}

function getUserRole(value) {
    if (value == 0 || value == '0') {
        $(".userRole").show();
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/WManagePage.asmx/selectUserType",
            data: "{}",
            async: false,
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (result) {
                console.log(result);
                var xmldoc = $.parseXML(result.d);
                var role = $(xmldoc).find('Table');
                $.each(role, function () {
                    $("#UserRole").append($("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("TypeName").text().replace(/&quot;/g, "\'") + "</option>"));
                });

                $("#UserRole").select2();
            },
            error: function (result) {

            },

        });
    }
    else {
        $(".userType").hide();
    }
}

$("#saveActionBtn").click(function () {
    if (dynamicALL('actReq')) {
        var data = {
            PageAutoId: $("#hiddenAutoId").val(),
            ActionName: $("#ActionName").val(),
            ActionType: $("#ActionType").val(),
            UserType: $("#UserRole").val(),
            ModuleAutoId: $("#ModuleBox").val()
        }
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/WManagePage.asmx/saveAction",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {

                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == 'Page Action already exist') {
                    swal("", "Page Action already exist.", "error");
                    return;
                } else {
                    console.log(result);
                    var xmldoc = $.parseXML(result.d);
                    var pagesDetail = $(xmldoc).find('Table');
                    if (pagesDetail.length > 0) {
                        swal("", "Action saved successfully.", "success");
                        getActionList($("#hiddenAutoId").val())
                        clearActionField();
                    }

                    else if (result.d == 'Unauthorized access.') {
                        location.href = "/";
                    }
                    else {
                        swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                    }
                }
            },
            error: function (result) {
                swal("Error!", result.d, "error");
            },
            failure: function (result) {
                swal("Error!", result.d, "error");
            }
        });
    } else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
});

function getActionList(AutoId) {
    var data = {
        PageAutoId: AutoId
    };
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WManagePage.asmx/getActionList",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (result) {
            console.log(result);
            var xmldoc = $.parseXML(result.d);
            var actionList = $(xmldoc).find('Table');
            $("#actionList tbody tr").remove();
            var row = $("#actionList thead tr").clone(true);

            if (actionList.length > 0) {
                $.each(actionList, function () {

                    $(".action", row).html("<a><span class='ft-edit' onclick='editAction(this)'/></a>");
                    $(".AutoId", row).text($(this).find("AutoId").text());
                    $(".ActionName", row).text($(this).find("ActionName").text());
                    $(".AssignRole", row).text($(this).find("TypeName").text());
                    $(".HiddenAssignRole", row).text($(this).find("UserType").text());
                    $(".HiddenType", row).text($(this).find("ActionType").text());
                    
                    if ($(this).find("ActionType").text() == '1') {
                        $(".Type", row).text("Generic");
                    }
                    else {
                        $(".Type", row).text("Role Based");

                    }
                    $("#actionList tbody").append(row);
                    row = $("#actionList tbody tr:last-child").clone(true);
                });
            }

        },
        error: function (result) {

        },

    });
}

function editAction(e) {
    var $row = $(e).closest("tr");
    var $AutoId = $row.find(".AutoId").text();
    var $ActionName = $row.find(".ActionName").text();
    var $HiddenType = $row.find(".HiddenType").text();
    var $HiddenAssignRole = $row.find(".HiddenAssignRole").text();
    getActionUserRole($HiddenType,$HiddenAssignRole);

    $("#hiddenActionAutoId").val($AutoId);
    $("#ActionName").val($ActionName);
    $("#saveActionBtn").hide();
    $("#btnActReset").hide();
    $("#btnActUpdate").show();
    $("#btnActCancel").show();
}

function getActionUserRole(value, $HiddenAssignRole) {
    if (value == 0 || value == '0') {
        $(".act").show();
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/WManagePage.asmx/selectUserType",
            data: "{}",
            async: false,
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (result) {
                console.log(result);
                var xmldoc = $.parseXML(result.d);
                var role = $(xmldoc).find('Table');
                $.each(role, function () {
                    $("#UserRole").append($("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("TypeName").text().replace(/&quot;/g, "\'") + "</option>"));
                });

                $("#UserRole").select2().val($HiddenAssignRole);
            },
            error: function (result) {

            },

        });
    }
    else {
        $(".act").hide();
    }
}

$("#btnActUpdate").click(function () {
    if (checkRequiredField()) {
        var data = {
            PageAutoId: $("#hiddenAutoId").val(),
            ActionName: $("#ActionName").val(),
            ActionType: $("#ActionType").val(),
            UserType: $("#UserRole").val(),
            ActionAutoId : $("#hiddenActionAutoId").val()

        }
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/WManagePage.asmx/updateAction",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {

                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                console.log(result);
                if (result.d == 'Page Action already exist') {
                    swal("", "Page Action already exist.", "error");

                    return;
                } else {
                   
                    if (result.d == 'Success') {
                        swal("", "Action saved successfully.", "success");
                    }

                    else if (result.d == 'Unauthorized access.') {
                        location.href = "/";
                    }
                    else {
                        swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                    }
                }
            },
            error: function (result) {
                swal("Error!", result.d, "error");
            },
            failure: function (result) {
                swal("Error!", result.d, "error");
            }
        });
    } else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
});

function clearActionField() {
    $("#ActionType").val("1");
    $("#ActionName").val("");
    $(".act").hide();
    $("#UserRole").val("1").change();
    return;
}

function getParentModule() {
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/WManagePage.asmx/getParentModule",
            data: "{}",
            async: false,
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (result) {
              
                var xmldoc = $.parseXML(result.d);
                var module = $(xmldoc).find('Table');
                $.each(module, function () {
                    if ($(this).find("ParentModuleName").text() != '') {
                        $("#module").append($("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("ParentModuleName").text().replace(/&quot;/g, "\'") + " > " + $(this).find("ModuleName").text().replace(/&quot;/g, "\'") + "</option>"));
                    } else {
                        $("#module").append($("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("ModuleName").text().replace(/&quot;/g, "\'") + "</option>"));

                    }
                });

                $("#module").select2();


            },
            error: function (result) {

            },

        });
    }
function deletePage(PageAutoId) {
    var data = {
        AutoId: PageAutoId
    };
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WManagePage.asmx/deletePage",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (result) {
            if (result.d == "Success") {
                swal("", "Page details deleted successfully.", "success");
                getPageList();
            }
            else if (result.d == "Exists") {
                swal("Error!", "This Page details has been used in application.", "error");

            }
            else if (result.d == "Session Timeout") {
                location.href = "/";
            }
            else {
                swal("Error!", "Oops! Something went wrong.Please try later.", "error");

                getPageList();
            }

        },
        error: function (result) {
            swal("Error!", "This Page details has been used in application.", "error");
        },
        failure: function (result) {
            swal("Error!", result.d, "error");
        }
    });

}
function deleterecord(PageAutoId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this Page details",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: false,
            },
            confirm: {
                text: "Yes, delete it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            deletePage(PageAutoId);

        } else {
            swal("", "Your Page details is safe.", "error");
        }
    })
}
