﻿var TicketAutoId;
$(document).ready(function () {

    
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    TicketAutoId = getQueryString('TicketAutoId');
    getErrorTicketResponse(TicketAutoId);
});
function Pagevalue(e) {
    getErrorTicketResponse(parseInt($(e).attr("page")));
};

function getErrorTicketResponse(TicketAutoId) {
    if ($.fn.DataTable.isDataTable('#tblErrorTicketResponse')) {
        $('#tblErrorTicketResponse').DataTable().destroy();
    }
    $.ajax({
        type: "POST",
        async: false,
        url: "/Admin/WebAPI/ErrorTicketResponse.asmx/getErrorTicket",
        data: "{'TicketAutoId':'" + TicketAutoId + "'}",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
            if ($.fn.DataTable.isDataTable('#tblErrorTicketResponse')) {
                $('#tblErrorTicketResponse').DataTable().destroy();
            }
            $('#tblErrorTicketResponse').DataTable(
               {
                   "paging": true,
                   "ordering": false,
                   "info": true,
                   "searching": false,
                   "aoColumns": [
                       { "bSortable": false },
                       { "bSortable": false },
                       { "bSortable": false },
                       { "bSortable": false },
                       { "bSortable": false },
                       { "bSortable": false },
                   ]
               });
        },
        success: function (response) {
            if(response.d != "false"){
                var xmldoc = $.parseXML(response.d);
            var Emp = $(xmldoc).find("Table");
            var Emp1 = $(xmldoc).find("Table1");
            var i = 0;          
            if (Emp.length > 0) {
            
                $("#txtticketid").val($(Emp).find("TicketID").text());
                $("#txtTicketDate").val($(Emp).find("TicketDate").text());
                $("#txtbyname").val($(Emp).find("Fullname").text());
                $("#txtPageUrl").val($(Emp).find("PageUrl").text());
                $("#txttype").val($(Emp).find("Type").text());
                $("#txtcurrentstat").val($(Emp).find("Status").text());
                $("#txtsub").val($(Emp).find("Subject").text());
                $("#ddldevStatus").val($(Emp).find("DeveloperStatus").text());
                if ($("#txtcurrentstat").Text != "Close") {
                    $("#ddlClientStatus").val("Open");
                }
                $("#txtDescription").val($(Emp).find("Description").text());              
                if ($(Emp).find("attach").text() != "") {
                    $("#download").attr("href",/Attachments/ + $(Emp).find("attach").text());                  
                }
                $("#txtPriority").val($(Emp).find("Priority").text());
            }
         
            $("#tblErrorTicketResponse tbody tr").remove();
            var row = $("#tblErrorTicketResponse thead tr").clone(true);
            if (Emp1.length > 0) {
                $("#EmptyTable").hide();
                $.each(Emp1, function (index) {
                   
                    $(".SN", row).text(index + 1);
                    $(".EmpName", row).text($(this).find("EmpName").text());
                    $(".ActionDate", row).text($(this).find("ActionDate").text());
                    if ($(this).find("Status").text() == "Open") {
                        $(".status", row).html("<span class='badge badge badge-pill badge-warning'>" + $(this).find("Status").text() + "</span>");
                    }
                    else if ($(this).find("Status").text() == "Close") {
                        $(".status", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("Status").text() + "</span>");
                    }

                    if ($(this).find("DeveloperStatus").text() == "Open") {
                        $(".developerstatus", row).html("<span class='badge badge badge-pill badge-warning'>" + $(this).find("DeveloperStatus").text() + "</span>");
                    }
                    else if ($(this).find("DeveloperStatus").text() == "Under Process") {
                        $(".developerstatus", row).html("<span class='badge badge badge-pill badge-info'>" + $(this).find("DeveloperStatus").text() + "</span>");
                    }
                    else if ($(this).find("DeveloperStatus").text() == "Close") {
                        $(".developerstatus", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("DeveloperStatus").text() + "</span>");
                    }
                    else if ($(this).find("DeveloperStatus").text() == "") {
                        $(".developerstatus", row).html("<span class=''>" + $(this).find("DeveloperStatus").text() + "</span>");
                    }
                    $(".Action", row).text($(this).find("Action").text());
                    $("#tblErrorTicketResponse tbody").append(row);
                    row = $("#tblErrorTicketResponse tbody tr:last").clone(true);
                });

            }
        }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

$("#btnSaveTicket").click(function () {   
    if (dynamicALL('req')) {
        var data = {
            TicketID: TicketAutoId,
            Action: "Response : " + $("#txtResponse").val(),
            ActionDate: $("#txtTicketDate").val(),
            Status: $("#ddlClientStatus").val(),
            DeveloperStatus: $("#ddldevStatus").val() || '',
            Subject: $("#txtsub").val(),
            Priority: $("#txtPriority").val(),
            Comment: $("#txtPriority").val(),
            Description: $("#txtDescription").val()
        }
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/ErrorTicketResponse.asmx/insertErrorTicketResponse",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == 'true') {                                 
                    swal("", "Response saved successfully.", "success").then(function () {                       
                        getErrorTicketResponse(TicketAutoId);
                    })
                   
                } else {
                    swal("Error!", " There is some issue .please try again.", "error");
                }
                $("#txtcurrentstat").val(0);
                $("#ddldevStatus").val(0);
                $("#txtResponse").val('');
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
})

