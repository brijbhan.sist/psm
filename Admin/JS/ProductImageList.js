﻿$(document).ready(function () {
    bindCategory();
});

function Pagevalue(e) {
    getProductList(parseInt($(e).attr("page")));
};
function getProductList(PageIndex) {
    if ($("#ddlSCategory").val() == '0') {
        toastr.error('Category is required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $("#ddlSCategory").addClass('border-warning');
    } else {
        $("#ddlSCategory").removeClass('border-warning');
        var data = {
            CategoryAutoId: $("#ddlSCategory").val(),
            SubcategoryAutoId: $("#ddlSSubcategory").val(),
            ImageType: $("#ddlImageType").val(),
            ProductId: $("#txtSProductId").val().trim(),
            ProductName: $("#txtSProductName").val().trim(),
            ProductStatus: $("#ddlStatus").val(),
            PageSize: $("#ddlPageSize").val(),
            PageIndex: PageIndex
        };
        $.ajax({
            type: "POST",
            url: "ProductImageList.aspx/getProductList",
            data: JSON.stringify({ dataValues: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                var xmldoc = $.parseXML(response.d);
                var ImageList = $(xmldoc).find("Table1");
                $("#tblProductList tbody tr").remove();
                if (ImageList.length > 0) {
                    $("#EmptyTable").hide();
                    var row = $("#tblProductList thead tr").clone(true);
                    $.each(ImageList, function (j) {
                        $(".Sr", row).text(Number(j) + 1);
                        $(".ProductId", row).text($(this).find("ProductId").text());
                        $(".ProductName", row).text($(this).find("ProductName").text());
                        $(".Category", row).text($(this).find("Category").text());
                        $(".Subcategory", row).text($(this).find("Subcategory").text());
                        $(".Image", row).html("<img src='" + $(this).find("Image").text() + "' class='zoom img img-circle img-responsive' style='width: 50px;height: 50px;'  >");
                        $(".Thumbail100", row).html("<img src='" + $(this).find("ThumbnailImageUrl").text() + "' class='zoom img img-circle img-responsive' style='width: 50px;height: 50px;'  >");
                        $(".Thumbail400", row).html("<img src='" + $(this).find("ThirdImage").text() + "' class='zoom img img-circle img-responsive' style='width: 50px;height: 50px;'  >");
                        $(".Action", row).html("<a title='Upload' href='#' onclick='uploaddocument(" + $(this).find("AutoId").text() + ")'><span class='la la-upload'></span></a>");
                        $("#tblProductList tbody").append(row);
                        row = $("#tblProductList tbody tr:last").clone(true);
                    });
                } else {
                    $("#EmptyTable").show();
                }
                var pager = $(xmldoc).find("Table");
                $(".Pager").ASPSnippets_Pager({
                    ActiveCssClass: "current",
                    PagerCssClass: "pager",
                    PageIndex: parseInt(pager.find("PageIndex").text()),
                    PageSize: parseInt(pager.find("PageSize").text()),
                    RecordCount: parseInt(pager.find("RecordCount").text())
                });
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
}
function uploaddocument(ProductAutoId) {
    debugger;
    var data = {
        ProductAutoId: ProductAutoId
    };
    $.ajax({
        type: "POST",
        url: "ProductImageList.aspx/getProductDetail",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var product = $(xmldoc).find("Table");
            $.each(product, function () {
                $("#txtProductName").val($(this).find("ProductName").text());
                $("#txtProductsId").val($(this).find("ProductId").text());
                $("#txtCategory").val($(this).find("Category").text());
                $("#txtsubcategory").val($(this).find("Subcategory").text());
                $("#imgProductImage").attr("src", $(this).find('ImageUrl').text());
            });

            $('#txtDocumentName').val('');
            $('#uploadedFile').val('');
            $('#PopCustomerDocuments').modal('show');
            $("#txtProductId").val(ProductAutoId);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
//function readURL(input) {
//    if ($(input).val() == '') {
//        $("#imgProductImage").attr("src", "/images/default_pic.png");
//    } else {
//        if (input.files && input.files[0]) {
//            var reader = new FileReader();
//            reader.onload = function (e) {
//                document.getElementById('imgProductImage').src = e.target.result;
//            }
//            reader.readAsDataURL(input.files[0]);
//        }
//    }
//}

function checkFileDetails(input) {
    debugger;
    if ($(input).val() == '') {
        $("#imgProductImage").attr("src", "/images/default_pic.png");
    }
    var file = input.files[0];
    if (Math.round(file.size / (1024 * 1024)) > Number($("#getSize").html())) {
        toastr.error('You are reaching maximum limit. Max limit is ' + $("#getSize").html() + ' MB', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $("#fileImageUrl").val('');
        return true;
    }
    else {
        if (input.files && input.files[0] && input.files[0].name.match(/\.(jpg|jpeg|png|PNG)$/)) {
            var reader = new FileReader();
            reader.onload = function (e) {
                document.getElementById('imgProductImage').src = e.target.result;
                $('#imgProductImage').attr('mainsrc', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
        else {
            toastr.error('Invalid file. Allowed file type is :[png,jpg,jpeg]', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            $("#fileImageUrl").val('');
        }
    }
}


function UploadImage() {
    debugger;
    var imgname = $("#fileImageUrl").val();
    var extension = imgname.substr((imgname.lastIndexOf('.') + 1));
    if ($("#fileImageUrl").val() != '') {
        imgname = $("#txtProductsId").val() + '.' + extension;
    }
    else {

        imgname = "default_pic.png";
    }

    if ($("#fileImageUrl").val() != '') {

        var data = {
            ProductId: $("#txtProductId").val(),
            Product: $("#txtProductsId").val(),
            ImageUrl: imgname,
            ThumbnailImageUrl: "Thumbnail_" + imgname,
            ThumbnailImageUrl1000: "Thumbnail_" + imgname,
        }
        $.ajax({
            type: "POST",
            url: "ProductImageList.aspx/UploadImage",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d == "false") {
                    swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                    return;
                }
                if (response.d != "Session Expired") {
                    debugger;

                    var fileUpload = $("#fileImageUrl").get(0);
                    var files = fileUpload.files;
                    var test = new FormData();
                    for (var i = 0; i < files.length; i++) {
                        test.append(files[i].name, files[i]);
                    }

                    $.ajax({
                        url: "/FileUploadHandler.ashx?timestamp=" + data.Product,
                        type: "POST",
                        contentType: false,
                        processData: false,
                        data: test,
                        success: function (result) {
                            console.log(result.d);
                        },
                        error: function (err) {

                        }
                    });
                    $("#fileImageUrl").val('');
                    swal("", "Image uploaded successfully.", "success").then(function () {
                        $('#PopCustomerDocuments').modal('hide');
                        getProductList(1);
                    });

                    $("#tblProductList tbody tr").each(function (i) {
                        if ($("#txtProductsId").val() == $(this).find('.ProductId').html()) {
                            $(this).closest('tr').remove();
                        }
                    })
                }
                else {
                    location.href = '/';
                }
            },
            error: function (result) {
                swal("Error!", "Image not uplaoded", "error");
            },
            failure: function (result) {
                swal("Error!", 'Oops,something went wrong.Please try again.', "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
function bindCategory() {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/productList.asmx/bindCategory",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var category = $(xmldoc).find("Table");

            $("#ddlSCategory option:not(:first)").remove();
            $.each(category, function () {
                $("#ddlSCategory").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("CategoryName").text() + "</option>");
            });
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function bindSubcategory() {
    var categoryAutoId = $("#ddlSCategory").val();
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/productList.asmx/bindSubcategory",
        data: "{'categoryAutoId':'" + categoryAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var subcategory = $(xmldoc).find("Table");
            $("#ddlSSubcategory option:not(:first)").remove();
            $.each(subcategory, function () {
                $("#ddlSSubcategory").append("<option value=' " + $(this).find("AutoId").text() + "'>" + $(this).find("SubcategoryName").text() + "</option>");
            });
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}