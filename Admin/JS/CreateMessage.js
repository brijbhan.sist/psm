﻿var Imgresponse = [];

$(document).ready(function () {
    $('#txtStartDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: false,
        selectMonths: false,
        defaultDate: new Date()
    });
    $('#txtExpiryDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: false,
        selectMonths: false,
    });
    BindMessage(1);
    $('#txtExpiryTime').pickatime({
        interval: 5
    });
    $('#txtStartTime').pickatime({
        interval: 5
    });

});

function Pagevalue(e) {
    BindMessage(parseInt($(e).attr("page")));
};

function BindRecipient() {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WCreateMessage.asmx/BindRecipient",
        data: "{'TeamId':" + $("#ddlGroup").val() + "}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var recipient = $(xmldoc).find("Table");
                $("#ddlRecipient").attr('multiple', 'multiple')
                $("#ddlRecipient option:not(:first)").remove();
                $.each(recipient, function () {
                    $("#ddlRecipient").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("Recipient").text() + "</option>");
                });
                $("#ddlRecipient").select2({
                    placeholder: 'All Recipient',
                    allowClear: true
                });
            } else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(result.d);
        },
        failure: function (result) {
            console.log(result.d);
        }
    });
}

function SaveMessage() {
    var IsRepeat = 0;
    if ($("#cbRepeat").prop('checked') == true) {
        IsRepeat = 1;
    }
    var checkval = dynamicALL('req');
    if (!checkval) {
        dynamicALL('ddlreq');
    } else {
        checkval = dynamicALL('ddlreq');
    }
    if (!checkval) {
        dynamicInputTypeSelect2Multi('ddlreq');
    } else {
        checkval = dynamicInputTypeSelect2Multi('ddlreq');
    }
    if (checkval) {
        var recipient = "", count = 0;
        $("#ddlRecipient option:selected").each(function () {
            if (count == 0) {
                recipient += $(this).val();
                count++;
            } else {
                recipient += ',' + $(this).val();
            }
        });

        var MessageData = {
            TeamId: parseInt($("#ddlGroup").val()),
            Recipient: recipient,
            StartDate: $("#txtStartDate").val(),
            StartTime: $("#txtStartTime").val(),
            ExpiryDate: $("#txtExpiryDate").val(),
            ExpiryTime: $("#txtExpiryTime").val(),
            IsRepeat: IsRepeat,
            Message: $("#txtMessage").val(),
            Title: $("#Texttitle").val(),
            messageImageUrl: JSON.stringify(Imgresponse),
            Sequence: $("#TextSequence").val()

        };

        $.ajax({
            type: "Post",
            url: "/Admin/WebAPI/WCreateMessage.asmx/SaveMessage",
            data: JSON.stringify({ MessageData: JSON.stringify(MessageData) }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (data) {
                if (data.d == "true") {
                    swal("", "Message details saved successfully.", "success").then(function () {
                        BindMessage(1);
                        resetData();

                    });
                } else if (data.d == "Session Expired") {
                    location.href = '/';
                }
                else {
                    swal("", data.d, "error");
                }
            },
            error: function (result) {
                console.log(result);
                swal("", "Oops! Something went wrong. Please try later.", "error");
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    } else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}

function BindMessage(pageIndex) {
    var data = {
        ExpiryDate: $("#txtSExpiryDate").val(),
        PageIndex: pageIndex
    };
    $.ajax({
        type: "Post",
        url: "/Admin/WebAPI/WCreateMessage.asmx/BindMessage",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var message = $(xmldoc).find("Table1");

                $("#tblMessageList tbody tr").remove();
                var row = $("#tblMessageList thead tr").clone(true);
                if (message.length > 0) {
                    $("#EmptyTable").hide();
                    $.each(message, function () {
                        $(".MsgDate", row).text($(this).find("CreatedDate").text());
                        $(".Msg", row).text($(this).find("Message").text());
                        if ($(this).find("Team").text() == "1") {
                            $(".Group", row).text("Team");
                        }
                        else {
                            $(".Group", row).text("Individual");
                        }
                        if ($(this).find("EmpType").text() == "") {
                            $(".AssignTo", row).text($(this).find("EmpName").text());
                        }

                        if ($(this).find("EmpName").text() == "") {
                            $(".AssignTo", row).text($(this).find("EmpType").text());
                        }
                        $(".ExpDateTime", row).text($(this).find("ExpiryDate").text());
                        $(".StartDateTime", row).text($(this).find("StartDate").text());
                        if ($(this).find("IsExpired").text() == "0") {
                            $(".Status", row).html("<span class='badge badge badge-pill badge-success'>Active</span>");
                        } else {
                            $(".Status", row).html("<span class='badge badge badge-pill badge-danger'>Expired</span>");
                        }
                        $(".Action", row).html("<a title='Edit' href='javascript:void(0)' onclick='EditMessage(" + $(this).find("MessageAutoId").text() + ")'><span class='la la-edit' ></span></a>&nbsp;<a title='Delete' href='javascript:;'><span class='la la-remove' onclick='deleterecord(" + $(this).find("MessageAutoId").text() + ") '></span></a>");
                        $("#tblMessageList tbody").append(row);
                        row = $("#tblMessageList tbody tr:last").clone(true);
                    });
                    var EmpType = $(xmldoc).find("Table2");
                    if ($(EmpType).find('EmpType').text() != 1) {
                        $('.Action').text('');
                        $('.Action').hide();
                    }
                } else {
                    $("#EmptyTable").show();
                }

                var pager = $(xmldoc).find("Table");
                $(".Pager").ASPSnippets_Pager({
                    ActiveCssClass: "current",
                    PagerCssClass: "pager",
                    PageIndex: parseInt(pager.find("PageIndex").text()),
                    PageSize: parseInt(pager.find("PageSize").text()),
                    RecordCount: parseInt(pager.find("RecordCount").text())
                });
            } else {
                location.href = '/';
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function resetData() {
    $("#btnupdateMessage").hide();
    $("#btnCancelMessage").hide();
    $("#btnSaveMessage").show();
    $("#btnResetMessage").show();
    $("#ddlGroup").val(0);
    $("#txtExpiryDate").val('');
    $("#txtExpiryTime").val('');
    $("#txtMessage").val('');
    $("#file1").val('');
    $("#txtSExpiryDate").val('');
    $("#Texttitle").val('');
    $("#TextSequence").val('');
    $("#txtStartDate").val('');
    $("#txtStartTime").val('');
    $("#cbRepeat").prop("checked", false);
    //$("#txtStartTime").select2({
    //    allowClear: true
    //});
    $("#ddlRecipient").empty();
    $("#imgAlertMessage").attr('src','../images/NoImageYet.png');
    $("#ddlGroup").removeClass("border-warning");
    $("#txtExpiryDate").removeClass("border-warning");
    $("#txtExpiryTime").removeClass("border-warning");
    $("#txtMessage").removeClass("border-warning");

    $('.select2-selection,.select2-selection--multiple').attr('style', '');
}

function EditMessage(MessageAutoId) {
    $("#MessageAutoId").val(MessageAutoId);
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WCreateMessage.asmx/EditMessage",
        data: "{'MessageAutoId':" + MessageAutoId + "}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            debugger;
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var message = $(xmldoc).find("Table");
                $("#ddlGroup").val($(message).find('Team').text()).change();
                if ($(message).find('Team').text() == '1') {
                    var ddlRecipient = $(message).find('EmpType').text().split(',');
                    $("#ddlRecipient").val(ddlRecipient).change();
                } else {
                    var ddlRecipient = $(message).find('EmpName').text().split(',');
                    $("#ddlRecipient").val(ddlRecipient).change();
                }
                $("#Texttitle").val($(message).find('Title').text());
                $("#TextSequence").val($(message).find('Sequence').text());

                $("#txtExpiryDate").val($(message).find('ExpiryDate').text());
                $("#txtExpiryTime").val($(message).find('ExpiryTime').text());
                $("#txtStartDate").val($(message).find('StartDate').text());
                $("#txtStartTime").val($(message).find('StartTime').text());

                if ($(message).find('IsRepeat').text() == '1') {
                    $("#cbRepeat").attr('checked', true);
                }
                else {
                    $("#cbRepeat").attr('checked', false);
                }

                if ($(message).find("ImageUrl").text() != "") {
                    $("#imgAlertMessage").attr("src", "../MessageImageAttachments/" + $(message).find("ImageUrl").text())
                } else {
                    $("#imgAlertMessage").attr("src", "../images/default_pic.png")
                }

                $("#txtMessage").val($(message).find('Message').text());
                $("#btnupdateMessage").show();
                $("#btnCancelMessage").show();
                $("#btnSaveMessage").hide();
                $("#btnResetMessage").hide();
            } else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(result.d);
        },
        failure: function (result) {
            console.log(result.d);
        }
    });
}
function UpdateMessage() {
    var IsRepeat = 0;
    if ($("#cbRepeat").prop('checked') == true) {
        IsRepeat = 1;
    }
    var checkval = dynamicALL('req');
    if (!checkval) {
        dynamicALL('ddlreq');
    } else {
        checkval = dynamicALL('ddlreq');
    }
    if (!checkval) {
        dynamicInputTypeSelect2Multi('ddlreq');
    } else {
        checkval = dynamicInputTypeSelect2Multi('ddlreq');
    }
    if (checkval) {
        var recipient = "", count = 0;
        $("#ddlRecipient option:selected").each(function () {
            if (count == 0) {
                recipient += $(this).val();
                count++;
            } else {
                recipient += ',' + $(this).val();
            }
        });

        var MessageData = {
            MessageAutoId: $("#MessageAutoId").val(),
            TeamId: parseInt($("#ddlGroup").val()),
            Recipient: recipient,
            messageImageUrl: JSON.stringify(Imgresponse),
            ExpiryDate: $("#txtExpiryDate").val(),
            ExpiryTime: $("#txtExpiryTime").val(),
            StartDate: $("#txtStartDate").val(),
            StartTime: $("#txtStartTime").val(),
            IsRepeat: IsRepeat,

            Message: $("#txtMessage").val(),
            Title: $("#Texttitle").val(),
            Sequence: $("#TextSequence").val()

        };

        $.ajax({
            type: "Post",
            url: "/Admin/WebAPI/WCreateMessage.asmx/UpdateMessage",
            data: JSON.stringify({ MessageData: JSON.stringify(MessageData) }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (data) {
                if (data.d == "updated") {

                    swal("", "Message details updated successfully.", "success").then(function () {
                        location.reload(true);
                    });

                }
                else if (data.d == "Session Expired") {
                    location.href = '/';
                }
                else {
                    swal("", data.d, "error");
                }
            },
            error: function (result) {
                swal("", "Oops! Something went wrong. Please try later.", "error");
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    } else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
function deleteMessage(MessageAutoId) {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WCreateMessage.asmx/deleteMessage",
        data: "{'MessageAutoId':" + MessageAutoId + "}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            if (response.d == "true") {
                swal("", "Message details deleted successfully.", "success");
                BindMessage(1);
            } else if (response.d == "Session Expired") {
                location.href = '/';
            }
            else {
                swal("", response.d, "warning");
            }
        },
        error: function (result) {
            console.log(result.d);
        },
        failure: function (result) {
            console.log(result.d);
        }
    });
}

function deleterecord(MessageAutoId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this message.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, Cancel.",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, Delete it.",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(isConfirm => {
        if (isConfirm) {
            deleteMessage(MessageAutoId);
        }
        //else {
        //             swal("", "Your message is safe.", "error");
        //}
    })
}

function SaveWebsiteDetails(input) {
    debugger;
    if (input.files && input.files[0] && input.files[0].name.match(/\.(jpg|jpeg|png|PNG)$/)) {
        var reader = new FileReader();
        reader.onload = function (e) {
            document.getElementById('imgAlertMessage').src = e.target.result;
            $('#imgAlertMessage').attr('mainsrc', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
    else {
        toastr.error('Invalid file. Allowed file type is :[png,jpg,jpeg]', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $("#file1").val('');
        return;
    }
    var timeStamp = Math.floor(Date.now() / 1000);
    var fileUpload = $("#file1").get(0);
    var files = fileUpload.files;
    var test = new FormData();

    for (var i = 0; i < files.length; i++) {
        test.append(files[i].name, files[i]);

    }
    $.ajax({
        url: "/FileUploadHandler1.ashx?timestamp=" + timeStamp,
        type: "POST",
        contentType: false,
        processData: false,
        data: test,
        success: function (response) {
            debugger;
            var img1 = JSON.parse(response);
            for (var j in img1) {
                Imgresponse.push({
                    'URL': img1[j].URL,
                });
            }
        },
        error: function (result) {
            swal("Error!", 'Oops, Something went wrong. Please try later.', "error");
        },
        failure: function (result) {
            swal("Error!", 'Oops, Something went wrong. Please try later.', "error");
        }
    });


}
function checkMessage() {
    st = minFromMidnight($("#txtStartTime").val());
    et = minFromMidnight($("#txtExpiryTime").val());
    if (st > et) {
        toastr.error('End time must be greater than start time.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $("#txtExpiryTime").val('');
    }
    function minFromMidnight(tm) {
        var ampm = tm.substr(-2)
        var clk = tm.substr(0, 5);
        var m = parseInt(clk.match(/\d+$/)[0], 10);
        var h = parseInt(clk.match(/^\d+/)[0], 10);
        h += (ampm.match(/pm/i)) ? 12 : 0;
        return h * 60 + m;
    }
}