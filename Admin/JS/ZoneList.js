﻿$(document).ready(function () {
    
    getMasterList();
});


$("#btnSearch").click(function () {
    getZoneList(1);
})
function getMasterList() {
    $.ajax({
        type: "POST",
        url: "WebAPI/ZoneZipMaster.asmx/SelectAllDriverCar",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
       
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var CarDetail = $(xmldoc).find("Table");
            var Driverdetail = $(xmldoc).find("Table1");

            $("#ddlSDriver option:not(:first)").remove();
            $.each(Driverdetail, function () {
                $("#ddlSDriver").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("EmpName").text() + "</option>");
            });

            $("#ddlSCar option:not(:first)").remove();
            $.each(CarDetail, function () {
                $("#ddlSCar").append("<option value='" + $(this).find("CarAutoId").text() + "'>" + $(this).find("CarName").text() + "</option>");
            });
            $("#ddlSCar").select2()
            $("#ddlSDriver").select2()
            getZoneList(1);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function Pagevalue(e) {
    getZoneList(parseInt($(e).attr("page")));
};

function getZoneList(PageIndex) {
    var data = {
        ZoneName: $("#txtSZoneName").val().trim(),
        DriverId: $("#ddlSDriver").val(),
        CarId: $("#ddlSCar").val(),
        Status: $("#ddlSStatus").val(),
        PageSize: 10,
        PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "WebAPI/ZoneZipMaster.asmx/GetZoneList",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        
        success: successGetZoneList,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function successGetZoneList(response) {
    var xmldoc = $.parseXML(response.d);
    var CarList = $(xmldoc).find("Table1");

    $("#tblZoneList tbody tr").remove();

    var row = $("#tblZoneList thead tr").clone(true);
    if (CarList.length > 0) {
        $("#EmptyTable").hide();
        $.each(CarList, function (index) {
            $(".ZoneId", row).text($(this).find("AutoId").text());
            $(".Zone", row).text($(this).find("ZoneName").text());
            $(".Driver", row).text($(this).find("EmpName").text());
            $(".Car", row).text($(this).find("CarName").text());
            $(".NoZip", row).text($(this).find("countzip").text());
            
            if ($(this).find("STATUS").text() == 'Active') {
                $(".status", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("STATUS").text() + "</span>");
            } else {
                $(".status", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("STATUS").text() + "</span>");

            }
            $(".action", row).html("<a title='Edit' href='/Admin/ZoneEntry.aspx?PageId=" + $(this).find("AutoId").text() + "'><span class='ft-edit' ></span></a>");
            $("#tblZoneList tbody").append(row);
            row = $("#tblZoneList tbody tr:last").clone(true);
        });

    } else {
        $("#EmptyTable").show();
    }
    
    var pager = $(xmldoc).find("Table");
    var pager = $(xmldoc).find("Table");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
}