﻿
$(document).ready(function () {
    getCategoryDetail();
    resetCategory();
    $("#btnUpdate").hide();
    $("#btnCancel").hide();
})

$(".close").click(function () {
    $(".alert").hide();
});

/*------------------------------------------------------Insert Categroy------------------------------------------------------------*/
function Save() {
    if (checkRequiredField()) {
        var data = {
            CategoryName: $("#txtCategoryName").val().trim(),
            Description: $("#txtDescription").val().trim(),
            Status: $("#ddlStatus").val(),
        }
        $.ajax({
            type: "POST",
            url: "Expensecategorymaster.aspx/insertCategory",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == 'Unauthorized access.') {
                    location.href = '/';
                } else if (result.d == 'Success') {
                    swal("", "Category saved successfully.", "success");
                    //var table = $('#tblCategory').DataTable();
                    //table.destroy();
                    getCategoryDetail();
                    resetCategory();
                } else {
                    swal("Error!", result.d, "error");
                }
            },
            error: function (result) {
                swal("Error!", "Category already exists.", "error");

            },
            failure: function (result) {
                swal("Error!", result.d, "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
}


/*----------------------------------------------------List Categroy Details--------------------------------------------------------*/
function getCategoryDetail() {
    var data = {
        CategoryId: $('#txtSCategoryId').val().trim(),
        CategoryName: $('#txtSCategoryName').val().trim(),
        Status: $('#ddlSStatus').val()
    };
    $.ajax({
        type: "POST",
        url: "Expensecategorymaster.aspx/getCategoryDetail",
        data: JSON.stringify({ 'dataValue': JSON.stringify(data) }),
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
            if ($.fn.dataTable.isDataTable('#tblCategory')) {
                $('#tblCategory').DataTable().destroy();
            }
        },
        complete: function () {
            $.unblockUI();
        },
        success: onSuccessOfCategory,
        error: function (response) {
            console.log(JSON.parse(response.responseText).d);
        }
    });
}
function onSuccessOfCategory(response) {
    if (response.d == 'Unauthorized access.') {
        location.href = '/';
    } else {
        var xmldoc = $.parseXML(response.d);
        var category = $(xmldoc).find('Table');

        if (category.length > 0) {

            $('#tblCategory tbody tr').remove();
            var row = $('#tblCategory thead tr').clone(true);
            $.each(category, function () {
                $("#EmptyTable").hide();
                $(".action", row).html("<a title='Edit' href='#'><span class='ft-edit' onclick='editCategory(\"" + $(this).find("CategoryId").text() + "\")' /></a>&nbsp;&nbsp;&nbsp;&nbsp;<a title='Delete' href='#'><span class='ft-x' onclick= 'deleteCategorys(\"" + $(this).find("CategoryId").text() + "\")' /></a>");
                $(".categoryId", row).html($(this).find("CategoryId").text());
                $(".categoryName", row).html($(this).find("CategoryName").text());
                $(".description", row).html($(this).find("Description").text());
                if ($(this).find("Status").text() == 'Active') {
                    $(".status", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("Status").text() + "</span>");
                }
                else {
                    $(".status", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("Status").text() + "</span>");
                }
                $("#tblCategory tbody").append(row);
                row = $("#tblCategory tbody tr:last-child").clone(true);
            });
        }
        else {
            $('#tblCategory tbody tr').remove();
            $("#EmptyTable").show();
        }
    }
}
/*----------------------------------------------------Search Engine----------------------------------------------------------*/

/*----------------------------------------------------Edit Category Detail---------------------------------------------------*/
function editCategory(CategoryId) {
    $.ajax({
        type: "POST",
        url: "Expensecategorymaster.aspx/editCategory",
        data: "{'CategoryId':'" + CategoryId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: onSuccessOfEdit,
        error: function (response) {
            console.log(response.d)
        }
    })
}
function onSuccessOfEdit(response) {
    if (response.d == 'Unauthorized access.') {
        location.href = '/';
    } else {
        var xmldoc = $.parseXML(response.d);
        var category = $(xmldoc).find('Table');
        $("#txtCategoryId").val($(category).find("CategoryId").text());
        $("#txtCategoryName").val($(category).find("CategoryName").text());
        $("#txtDescription").val($(category).find("Description").text());
        $("#ddlStatus").val($(category).find("Status").text());
        
        $("#btnSave").hide();
        $("#btnReset").hide();
        $("#btnUpdate").show();
        $("#btnCancel").show();
    }
}
function Cancel() {
    resetCategory();
    $("#btnUpdate").hide();
    $("#btnCancel").hide();
    $("#btnSave").show();
    $("#btnReset").show();
};

/*----------------------------------------------------Update Category Detail--------------------------------------------------------*/

function Update() {
    if (checkRequiredField()) {
        var data = {
            CategoryId: $("#txtCategoryId").val().trim(),
            CategoryName: $("#txtCategoryName").val().trim(),
            Description: $("#txtDescription").val().trim(),
            Status: $("#ddlStatus").val().trim(),
           
        };
        $.ajax({
            type: "POST",
            url: "Expensecategorymaster.aspx/updateCategory",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json;charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == 'Unauthorized access.') {
                    location.href = '/';
                } else if (result.d == 'Success') {
                    swal("", "Category updated successfully.", "success");
                    var table = $('#tblCategory').DataTable();
                    table.destroy();
                    getCategoryDetail();
                    resetCategory();
                } else {
                    swal("", result.d, "error");
                }
            },
            error: function (result) {
                swal("Error!", "Category already exists.", "error");
            },
            failure: function (result) {
                swal("Error!", result.d, "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
};

/*--------------------------------------------------------Delete Category ----------------------------------------------------------*/
function deleteCategory(CategoryId) {
    $.ajax({
        type: "POST",
        url: "Expensecategorymaster.aspx/deleteCategory",
        data: "{'CategoryId':'" + CategoryId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (result) {
            if (result.d == 'Unauthorized access.') {
                location.href = '/';
            } else if (result.d == 'Success') {
                swal("", "Category deleted successfully.", "success");
                var table = $('#tblCategory').DataTable();
                table.destroy();
                getCategoryDetail();
                resetCategory();
            } else {
                swal("Error!", result.d, "error");
            }

        },
        error: function (result) {

        },
        failure: function (result) {

        }
    })
}

/*-----------------------------------------------------------Reset------------------------------------------------------------------*/
function resetCategory() {
    $("#btnUpdate").hide();
    $("#btnCancel").hide();
    $("#btnSave").show();
    $("#btnReset").show();
    $('input[type="text"]').val('');
    $('textarea').val('');
    $('select').val('1');
    $('input[type="text"]').removeClass('border-warning');
}

function deleteCategorys(CategoryId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this category.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, cancel.",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, delete it.",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            deleteCategory(CategoryId);

        }
    })
}