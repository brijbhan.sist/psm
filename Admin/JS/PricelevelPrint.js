﻿var getid = 0;
$(document).ready(function () {
    getid = getQueryString('PLAutoId');
    if (getid != null) {
        PrintPriceLevel(getid);
    }
});

var getQueryString = function (field, url) {
    var href = url ? url : window.location.href;
    var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
    var string = reg.exec(href);
    return string ? string[1] : null;
};


function PrintPriceLevel(PriceLevelAutoId) {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/PriceTemplate.asmx/PrintPriceLevel",
        data: "{'PriceLevelAutoId':'" + PriceLevelAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('#fade').show();
        },
        complete: function () {
            $('#fade').hide();
        },
        success: function (response) {
            if (response.d != 'Session Expired') {
                var xmldoc = $.parseXML(response.d);
                var template = $(xmldoc).find("Table");
                $("#PriceLevelName").html(template.find("PriceLevelName").text());
                $("#CurrentDate").html(template.find("CurrentDate").text());
                $("#Customer").html(template.find("Customer").text());

                var template1 = $(xmldoc).find("Table1");
                var row = $("#tblProduct thead tr").clone();
                $(template1).each(function () {
                    $(".ProductId", row).html($(this).find('ProductId').text());
                    $(".ProductName", row).html($(this).find('ProductName').text());
                    $(".Unit", row).html($(this).find('UnitType').text());
                    $(".MinPrice", row).html($(this).find('MinPrice').text());
                    $(".BasePrice", row).html($(this).find('Price').text());
                    $(".CustomPrice", row).html($(this).find('CustomPrice').text());
                    $("#tblProduct tbody").append(row);
                    row = $("#tblProduct tbody tr:last-child").clone();
                });

                var Company = $(xmldoc).find("Table2"); 
                $("#logo").attr("src", "../Img/logo/" + $(Company).find("Logo").text());
                $("#Address").text($(Company).find("Address").text());
                $("#Phone").text($(Company).find("MobileNo").text());
                $("#FaxNo").text($(Company).find("FaxNo").text());
                $("#Website").text($(Company).find("Website").text());
            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
