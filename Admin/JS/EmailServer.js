﻿$(document).ready(function () {
    getEmailServer(1);
    $("#chkSsl").prop('checked',true);
});
function Pagevalue(e) {
    getEmailServer(parseInt($(e).attr("page")));
};

function getEmailServer()
{
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/EmailServerMaster.asmx/getEmailServer",
        data: "",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var Emp = $(xmldoc).find("Table");
            var i = 0;
            if (Emp.length > 0) {
                var i = 1;
                $("#EmptyTable").hide();
                $("#tblEmailServerList tbody tr").remove();
                var row = $("#tblEmailServerList thead tr").clone(true);

                $.each(Emp, function () {               
                    $(".Action", row).html("<a title='Edit' href='javascript:;'><span class='la la-edit' onclick='EditServer(\"" + $(this).find("SendTo").text() + "\")'></span></a>");
                    $(".SendTo", row).html($(this).find("SendTo").text());
                    $(".Email", row).html($(this).find("EmailId").text());
                    $(".PORT", row).html($(this).find("port").text());
                    $(".Server", row).html($(this).find("server").text());
                    $(".Password", row).html($(this).find("Pass").text());
                    $(".SSL", row).html('<span style="color:blue">'+$(this).find("ssl").text()+'</span>');
                    $(".SN", row).html(i);
                    $("#tblEmailServerList tbody").append(row);
                    row = $("#tblEmailServerList tbody tr:last-child").clone(true);
                    i++;
                });
            }
            else {
                $("#tblEmailServerList tbody tr").remove();
                $("#EmptyTable").show();
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

$("#btnSave").click(function () {
    if (dynamicALL('server')) {
        // updated by satish 08/27/2019
        var sslcheck = 0;
        if($("#chkSsl").prop("checked") == true)
        {
            sslcheck = 1;
        }
       // end
        var data = {
            SendTo: $("#ddlUName").val(),
            Emailid: $("#txtEmailid").val(),
            Password: $("#txtPassword").val(),
            Server: $("#txtServer").val(),
            Port: $("#txtport").val(),
            SSL: sslcheck
        }

        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/EmailServerMaster.asmx/insertEmailServer",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == 'true') {
                    swal("", "Email server details saved successfully.", "success");
                    getEmailServer(1);
                    resetData();
                } else if (result.d == 'Unauthorized Access') {
                    location.href="/";
                }
                else{
                    swal("warning!", result.d, "warning");
                }
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
})

function EditServer(SendTo)
{
    debugger;
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/EmailServerMaster.asmx/editEmailServer",
        data: "{'SendTo':'" + SendTo + "'}",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var Emp = $(xmldoc).find("Table");
            if (Emp.length > 0) {
                $("#hfautoID").val($(Emp).find("SendTo").text());
                $("#ddlUName").val($(Emp).find("SendTo").text());
                $("#txtEmailid").val($(Emp).find("EmailId").text());
                $("#txtport").val($(Emp).find("port").text());
                $("#txtServer").val($(Emp).find("server").text());
                $("#txtPassword").val($(Emp).find("Pass").text());
                if ($(Emp).find("ssl").text() == 'true')
                {
                    $("#chkSsl").prop('checked',true);
                }
                else
                {
                    $("#chkSsl").prop('checked', false);
                }
                $("#btnSave").hide();
                $("#btnUpdate").show();
                $("#btnClear").addClass('btn-warning');
                $("#btnClear").html('Cancel');

               // $("#ddlUName").prop('disable',true);
                $("#ddlUName").attr('disabled',true);
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

$("#btnUpdate").click(function () {
    if (dynamicALL('server')) {
        var sslcheck = 0;
        if ($("#chkSsl").prop("checked") == true) {
            sslcheck = 1;
        }
        var data = {
            SendTo: $("#ddlUName").val(),
            Emailid: $("#txtEmailid").val(),
            Password: $("#txtPassword").val(),
            Server: $("#txtServer").val(),
            Port: $("#txtport").val(),
            SSL: sslcheck
        }

        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/EmailServerMaster.asmx/updateEmailServer",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == 'true') {
                    swal("", "Email server details updated successfully.", "success");
                    getEmailServer(1);
                    resetData();
                    $("#btnSave").show();
                    $("#btnUpdate").hide();
                    $("#ddlUName").removeAttr('disabled');
                }
                else if (result.d == "Unauthorized Access") {
                    location.href = "/";
                }
                else {
                    swal("warning!", result.d, "warning");
                }
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
})

function resetData() {
    $("#btnClear").removeClass('btn-warning');
    $("#btnClear").html('Reset');
    $("#ddlUName").val(0),
    $("#txtEmailid").val('');
    $("#txtPassword").val('');
    $("#txtServer").val('');
    $("#txtport").val('');
    $("#chkSsl").prop('checked', false);

    $("#txtTestEmail").val('');
    $("#ddlUType").val(0),

    $("#ddlUName").removeClass('border-warning');
    $("#txtEmailid").removeClass('border-warning');
    $("#txtPassword").removeClass('border-warning');
    $("#txtServer").removeClass('border-warning');
    $("#txtport").removeClass('border-warning');
    $("#chkSsl").removeClass('border-warning');

    $("#ddlUType").removeClass('border-warning');
    $("#txtTestEmail").removeClass('border-warning');

    $("#btnSave").show();
    $("#btnUpdate").hide();


}

$("#btnCheckmail").click(function ()
{
    $('#TestEmailPop').modal('show');
})

$("#btnClose").click(function () {
    $('#TestEmailPop').modal('hide');
})

$("#btnSendmail").click(function () { 
    if (dynamicALL('mail')) { 
        var data = {
            SendTo: $("#ddlUType").val(),
            Emailid: $("#txtTestEmail").val()
        }
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/EmailServerMaster.asmx/testEmailServer",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == 'true') {
                    swal("", " Email has been configured successfully.", "success");
                    $('#TestEmailPop').modal('hide');
                    $("#ddlUType").val(0);
                    $("#txtTestEmail").val('');
                }
                else if (result.d == "Unauthorized Access") {
                    location.href = "/";
                }
                else {
                    swal("warning!", result.d, "Network problem.");
                }               
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
})

function validateEmail(elementValue) {
     
    var EmailCodePattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var check = EmailCodePattern.test($(elementValue).val());
    if (!check) {
        toastr.error('Invalid email id.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $(elementValue).val('');
    }
    return re.test(String(elementValue).toLowerCase());
}