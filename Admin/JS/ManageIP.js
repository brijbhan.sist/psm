﻿$(document).ready(function () {
    getIPList(1);
});
function Pagevalue(e) {
    getIPList(parseInt($(e).attr("page")));
};

function getIPList(pageIndex) {
    var data = {
        userIpAddress: ($("#txtSIpAddresss").val()).trim(),
        PageSize: $("#ddlPageSize").val(),
        PageIndex: pageIndex
    };
    $.ajax({
        type: "POST",
        url: "/Admin/ManageIP.aspx/GetIPList",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }), 
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) { 
            var xmldoc = $.parseXML(response.d);
            var IPlist = $(xmldoc).find("Table");
            var i = 0;
            if (IPlist.length > 0) {
                $("#EmptyTable").hide();
                $("#tblIpList tbody tr").remove();
                var row = $("#tblIpList thead tr").clone(true);
                $.each(IPlist, function () {

                    $(".Action", row).html("<a title='Edit' href='javascript:;'><span class='la la-edit' onclick='EditIpAddress(\"" + $(this).find("AutoId").text() + "\")'></span></a> &nbsp;<a title='Edit' href='javascript:;'><span class='la la-remove' onclick='deleterecord(\"" + $(this).find("AutoId").text() + "\")'></span></a>");

                    $(".IPAddress", row).html($(this).find("IpAddress").text());
                    if ($(this).find("isDefault").text() == 0) {
                        var Isdefault = 'No';
                    } else {
                        var Isdefault = 'Yes';
                    }
                    $(".DefaultIp", row).html(Isdefault);
                    $(".CreateDate", row).html($(this).find("CreateDate").text());
                    $("#tblIpList tbody").append(row);
                    row = $("#tblIpList tbody tr:last-child").clone(true);
                });
            }
            else {
                $("#tblIpList tbody tr").remove();
                $("#EmptyTable").show();
            }
            var pager = $(xmldoc).find("Table1");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function svaeIpAddress() {
    if (checkRequiredField()) {
        if ($('#isDefault').prop('checked') === true) {
            isDefault = 1
        } else {
            isDefault = 0;
        }
        var data = {
            userIpAddress: $("#txtipaddress").val(),
            description: $("#txtDescription").val(),
            isDefault: isDefault
        }
        $.ajax({
            type: "POST",
            url: "/Admin/ManageIP.aspx/saveIpAddress",
            data: JSON.stringify({dataValue: JSON.stringify(data)}),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == 'true') {
                    swal("", "IP saved successfully.", "success");
                    getIPList(1);
                    resetData();
                }
                else if (result.d == "Unauthorized Access") {
                    location.href = "/";
                }
                else {
                    swal("", result.d, "warning");
                }
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}

function EditIpAddress(autoid) {
    var data = {
        AutoId: autoid
    }
    $.ajax({
        type: "POST",
        url: "/Admin/ManageIP.aspx/GetIPDetail",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            console.log(response);
            var xmldoc = $.parseXML(response.d);
            var ipDetail = $(xmldoc).find("Table");
            $.each(ipDetail, function () {
                if ($(ipDetail).find("isDefault").text() == 1) {
                    $("#isDefault").prop('checked', true);
                } else {
                    $("#isDefault").prop('checked', false);

                }
                $("#txtipaddress").val($(ipDetail).find("IpAddress").text());
                $("#txtDescription").val($(ipDetail).find("description").text());
                $("#hiddenAutoId").val($(ipDetail).find("AutoId").text());

                $("#btnUpdate").show();
                $("#btnSave").hide();
                $("#ddlempid").prop('disabled', 'true');
                $("#btnClear").addClass('btn-warning');
                $("#btnClear").html('Cancel');
            });
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function updateIpAddress(){

    if (checkRequiredField()) {

        if ($('#isDefault').prop('checked') === true) {
            isDefault = 1
        } else {
            isDefault = 0;
        }
        var data = {
            userIpAddress: $("#txtipaddress").val(),
            description: $("#txtDescription").val(),
            AutoId: $("#hiddenAutoId").val(),
            isDefault: isDefault
        }
        $.ajax({
            type: "POST",
            url: "/Admin/ManageIP.aspx/UpdateIpAddress",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == 'true') {
                    swal("", "IP updated successfully.", "success");
                    getIPList(1);
                    resetData();
                    $("#btnUpdate").hide();
                    $("#btnSave").show();
                    $("#ddlempid").removeAttr('disabled');

                } else if (result.d == 'Unauthorized Access') {
                    location.href = "/";
                }
                else {
                    swal("", result.d, "error");
                }
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}

function deleterecord(autoid) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this ip",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: false,
            },
            confirm: {
                text: "Yes, delete it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(isConfirm => {
        if (isConfirm) {
            DeleteIp(autoid);

        } else {
            swal("", "Your data is safe.", "error");
        }
    })
}

function DeleteIp(autoid) {
    var data = {
        AutoId: autoid
    }
    $.ajax({
        type: "POST",
        url: "/Admin/ManageIP.aspx/DeleteIp",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == "Unauthorized Access") {
                location.href = "/";
            } else if (response.d == "Assigned") {
                swal("", "Unable to delete ! IP assiged to employee.", "error");
            }
            else if (response.d !== "false") {
                getIPList(1);
                swal("", "IP deleted successfully.", "success");
            }
            else {
                swal("", response.d, "error");
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function resetData() {

    $("#btnClear").removeClass('btn-warning');
    $("#btnClear").html('Reset');
    $("#hiddenAutoId").val('');
    $("#txtipaddress").val('');
    $("#txtDescription").val('');
    $("#isDefault").prop('checked', false);
    $("#txtipaddress").removeClass('border-warning');

    $("#btnUpdate").hide();
    $("#btnSave").show();
}