﻿$(document).ready(function () {
    getlist(1);
    $("#btnUpdate").hide();
    $("#btnCancel").hide();
})
function Pagevalue(e) {
    getlist(parseInt($(e).attr("page")));
};
/*------------------------------------------------------Insert Categroy------------------------------------------------------------*/

function Save() {

    if (checkRequiredField()) {
        var IPAddress = $("#txtIPAddress").val().trim();

        if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(IPAddress)) {
            var data = {
                IPAddress: $("#txtIPAddress").val().trim(),
                NameofLocation: $("#txtNameofLocation").val().trim(),
                Description: $("#txtDescription").val().trim(),
            }
            $.ajax({
                type: "POST",
                url: "IPMaster.aspx/Insert",
                data: "{'dataValue':'" + JSON.stringify(data) + "'}",
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        timeout: 2000, //unblock after 2 seconds
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },

                success: function (result) {
                    if (result.d == "Success") {
                        swal("", "IP details saved successfully.", "success");
                        getlist(1)
                        reset();
                    }
                    else if (result.d == "Session Expired") {
                        location.href = "/";
                    }
                    else {
                        swal("", result.d, "error");
                    }
                },
                error: function (result) {
                    swal("Error!", result.d, "error");
                },
                failure: function (result) {
                    swal("Error!", result.d, "error");
                }
            });
        }
        else {
            toastr.error('Invalid IP address', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            return (false)
        }
    } else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}


/*----------------------------------------------------List Categroy Details--------------------------------------------------------*/
function getlist(PageIndex) {

    var data = {
        PageSize: 10,
        PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "IPMaster.aspx/bindList",
        data: JSON.stringify({ 'dataValue': JSON.stringify(data) }),
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });

        },
        complete: function () {
            $.unblockUI();
        },
        success: onSuccessOfIPAddress,
        error: function (response) {
            console.log(JSON.parse(response.responseText).d);
        }
    });
}
function onSuccessOfIPAddress(response) {
    var xmldoc = $.parseXML(response.d);
    var IpAddress = $(xmldoc).find('Table1');

    if (IpAddress.length > 0) {
        $('#EmptyTable').hide();
        $('#tbllist tbody tr').remove();
        var row = $('#tbllist thead tr').clone(true);
        $.each(IpAddress, function () {
            $(".AutoId", row).text($(this).find("AutoId").text());
            $(".IpAddress", row).text($(this).find("IPAddress").text());
            $(".NameofLocation", row).text($(this).find("NameofLocation").text());
            $(".description", row).text($(this).find("Description").text());
            $(".action", row).html("<a href='#' title='Edit'><span class='ft-edit' onclick='edit(\"" + $(this).find("AutoId").text() + "\")' /></a>&nbsp;<a href='#' title='Delete'><span class='ft-x' onclick= 'deleterecord(\"" + $(this).find("AutoId").text() + "\")' /></a>");
            $("#tbllist tbody").append(row);
            row = $("#tbllist tbody tr:last-child").clone(true);
        });
        var pager = $(xmldoc).find("Table");
        $(".Pager").ASPSnippets_Pager({
            ActiveCssClass: "current",
            PagerCssClass: "pager",
            PageIndex: parseInt(pager.find("PageIndex").text()),
            PageSize: parseInt(pager.find("PageSize").text()),
            RecordCount: parseInt(pager.find("RecordCount").text())
        });
    }
    else {
        $('#tbllist tbody tr').remove();
        $('#EmptyTable').show();
    }
}

/*----------------------------------------------------Edit Tax Detail---------------------------------------------------*/
function edit(AutoId) {
    $.ajax({
        type: "POST",
        url: "IPMaster.aspx/edit",
        data: "{'AutoId':'" + AutoId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                timeout: 300, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },

        success: onSuccessOfEdit,
        error: function (response) {
            console.log(response.d)
        }
    })
}
function onSuccessOfEdit(response) {
    var xmldoc = $.parseXML(response.d);
    var IpAddress = $(xmldoc).find('Table');
    $("#lblAutoId").text($(IpAddress).find("AutoId").text());
    $("#txtIPAddress").val($(IpAddress).find("IPAddress").text());
    $("#txtNameofLocation").val($(IpAddress).find("NameofLocation").text());
    $("#txtDescription").val($(IpAddress).find("Description").text());

    $("#btnSave").hide();
    $("#btnReset").hide();
    $("#btnUpdate").show();
    $("#btnCancel").show();
}
function Cancel() {
    reset();
    $("#btnUpdate").hide();
    $("#btnCancel").hide();
    $("#btnSave").show();
    $("#btnReset").show();
}


/*--------------------------------------------------------Delete Tax ----------------------------------------------------------*/
function deleteIp(AutoId) {
    $.ajax({
        type: "POST",
        url: "IPMaster.aspx/delete",
        data: "{'AutoId':'" + AutoId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },

        success: function (result) {
            if (result.d == "Session Expired") {
                location.href = "/";
            }
            else if (result.d == "Success") {
                swal("", "IP Address details deleted successfully.", "success");
                getlist(1)
                reset();
            }
            else {
                swal("Error!", result.d, "error");
            }
        },
        error: function (result) {
            console.log(result.d)
        },
        failure: function (result) {
            console.log(result.d)
        }
    })
}
/*-----------------------------------------------------------Reset------------------------------------------------------------------*/
function reset() {
    $("#btnUpdate").hide();
    $("#btnCancel").hide();
    $("#btnSave").show();
    $("#btnReset").show();
    $('input[type="text"]').val('');
    $('textarea').val('');
    $('.req').removeClass('border-warning');
}

function Update() {
    if (checkRequiredField()) {
        var data = {
            AutoId: $("#lblAutoId").text(),
            IPAddress: $("#txtIPAddress").val().trim(),
            NameofLocation: $("#txtNameofLocation").val().trim(),
            Description: $("#txtDescription").val().trim(),
        }
        $.ajax({
            type: "POST",
            url: "IPMaster.aspx/update",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    timeout: 2000, //unblock after 2 seconds
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: {},

            success: function (result) {
                if (result.d == "Success") {
                    swal("", "IP details updated successfully.", "success");
                    getlist(1)
                    reset();
                }
                else if (result.d == "Session Expired") {
                    location.href = "/";
                }
                else {
                    swal("", result.d, "error");
                }
            },
            error: function (result) {
                swal("Error!", result.d, "error");
            },
            failure: function (result) {
                swal("Error!", result.d, "error");
            }
        });
    } else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}


function deleterecord(AutoId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this IP.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, cancel.",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, delete it.",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            deleteIp(AutoId);
        }
    })
}


