﻿$(document).ready(function () {
    $("#tblProcutStockUpdateLog").hide()
    $("#UpStocklists").hide()
    StockList();
})
function StockList() {
    $.ajax({
        type: "POST",
        url: "WebAPI/UpdateStock.asmx/StockList",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == 'Session Expired') {
                location.href = '/';
            } else {
                var xmldoc = $.parseXML(response.d);
                var productList = $(xmldoc).find("Table");
                $("#ddlStock option:not(:first)").remove();
                $.each(productList, function () {
                    $("#ddlStock").append("<option value='" + $(this).find("Productid").text() + "'>" + $(this).find("ProductDetail").text() + "</option>");
                });
                $("#ddlStock").select2();
            }

        },
        error: function (result) {
            console.log(result.d);
        },
        failure: function (result) {
            console.log(result.d);
        }
    });
}
function Pagevalue(e) {
    BindTable(parseInt($(e).attr("page")), $("#ddlStock option:selected").val());
}
function Stockchange() {
    var xmldoc = $.parseXML();
    var ProductDetail = $(xmldoc).find('Table2');
    $("#txtProductQty").val($(ProductDetail).find("ZoneName").text());


    var productAutoId = $("#ddlStock option:selected").val();
    if (productAutoId != 0) {
        BindTable(1, productAutoId, 0);
    }
}

function BindTable(Pageindex, productAutoId, Type) {
    $("#ddlUnitType").val('0').change();
    $.ajax({
        type: "POST",
        url: "WebAPI/UpdateStock.asmx/StockQty",
        data: "{'productAutoId':" + productAutoId + ",'PageIndex':" + Pageindex + "}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var unitType = $(xmldoc).find("Table");
                var ProductDetail = $(xmldoc).find('Table2');
                var UnitDetail = $(xmldoc).find('Table3');
                if (ProductDetail.length > 0) {
                    $('#EmptyTable').hide();
                    $("#tblProcutStockUpdateLog").show();
                    $("#UpStocklists").show();
                    $('#tblProcutStockUpdateLog tbody tr').remove();
                    var row = $('#tblProcutStockUpdateLog thead tr').clone(true);
                    $.each(ProductDetail, function () {
                        $(".UserAutoId", row).text($(this).find("UserName").text());
                        $(".ProductId", row).text($(this).find("ProductId").text());
                        $(".ProductName", row).text($(this).find("ProductName").text());
                        $(".NewStock", row).text($(this).find("NewStock").text());
                        $(".OldStock", row).text($(this).find("OledStock").text());
                        $(".Date", row).text($(this).find("Date").text());
                        $(".Remark", row).text($(this).find("ActionRemark").text());
                        $("#tblProcutStockUpdateLog tbody").append(row);
                        row = $("#tblProcutStockUpdateLog tbody tr:last-child").clone(true);
                    });
                }
                else {
                    $('#tblProcutStockUpdateLog tbody tr').remove();
                    $('#EmptyTable').show();
                    $("#tblProcutStockUpdateLog").hide();
                    $("#UpStocklists").hide();
                }
                $("#txtProductQty").val('');
                if (Type == 0) {

                    $.each(unitType, function () {
                        $("#txtProductQty").val($(this).find("Stock").text());
                        $("#imgProductImage").attr("src", $(this).find("Productimage").text());
                        $("#imgProductImage").show();
                    });
                }
                var pager = $(xmldoc).find("Table1");
                $(".Pager").ASPSnippets_Pager({
                    ActiveCssClass: "current",
                    PagerCssClass: "pager",
                    PageIndex: parseInt(pager.find("PageIndex").text()),
                    PageSize: parseInt(pager.find("PageSize").text()),
                    RecordCount: parseInt(pager.find("RecordCount").text())
                });
                var html = '';
                html += '<table class="table table-striped table-bordered" id="tblUnitDetails">';
                html += '<thead>';
                html += '<tr><th class="UnitType text-center">Unit Type</th><th class="QtyPerUnit text-center">Quantity Per Unit</th><th></th><th class="Action text-center">Quantity</th><th class="Total text-center">Total</th>';
                html += '</tr>';
                html += '</thead>';
                html += '<tbody>';
                 $.each(UnitDetail, function () {
                     html += "<tr><td class='UnitType text-center' qty=" + $(this).find("Qty").text() + ">" + $(this).find("UnitType").text() + "</td>";
                     html += "<td class='QtyPerUnit text-center' > " + $(this).find("Qty").text() + "</td>";
                     html += "<td class='text-center' >*</td>";
                     html += "<td class='Action text-center' > <input type='text' class='form-control border-primary input-sm text-center' value='0' onkeyup='calTotal()' onkeypress='return isNumberKey(event)' /></td > <td class='Total text-center'>0</td></tr > ";
                 });
                html += '</tbody>';
                html += '<tfoot>';
                html += '<tr>';
                html += "<td colspan='4' class='text-right'><b>Total New Stock in Pieces</b></td><td id='tdTotalPieces' class='text-center'><b><input type='text' id='txtTotalPieces' style='text-align:center' value='0' disabled class='form-control border-primary input-sm'/></b></td>";
                html += '</tr>';
                html += '</tfoot>';
                html += '</table>';
                $(" #divUnitType").html(html);
            }
            else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(result.d);
        },
        failure: function (result) {
            console.log(result.d);
        }
    });
}
function calTotal() {
    debugger
    var totalQty = 0;
    $("#tblUnitDetails tbody tr").each(function (i) {
        totalQty += Number($(this).find('.UnitType').attr('qty')) * Number($(this).find('.Action input').val()) || 0;
        $(this).find('.Total').html(Number($(this).find('.UnitType').attr('qty')) * Number($(this).find('.Action input').val()) || 0);
        if (Number($(this).find('.Action input').val()) != 0) {
            $(this).find('.Action input').removeClass('border-warning');
        }
    });
    $("#txtTotalPieces").val(totalQty);
}
$("#btnUpdate2").click(function () {
    if (Number($("#txtTotalPieces").val()) == 0 || Number($("#txtTotalPieces").val()) == '') {
        $("#tblUnitDetails tbody tr").each(function (i) {
            $(this).find('.Action input').addClass('border-warning');
        });
        swal({
            title: "",
            text: "Are you sure ?.You want to make out of stock.",
            icon: "warning",
            showCancelButton: true,
            allowOutsideClick: false,
            closeOnClickOutside: false,
            buttons: {
                cancel: {
                    text: "No, Cancel",
                    value: null,
                    visible: true,
                    icon: "warning",
                    className: "btn-warning",
                    closeModal: true,
                },
                confirm: {
                    text: "Yes",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: true
                }
            }
        }).then(function (isConfirm) {
            if (isConfirm) {
                updateStock();
            }
        });
    }
    else {
        updateStock();
    }
});
function updateStock() {
    var productAutoId = $("#ddlStock option:selected").val();
    var qty = 0, unitQty = 0;
    qty = $("#txtNewProductQty").val();
    unitQty = $("#ddlUnitType option:selected").attr('qty');
    qty = Number(qty) * Number(unitQty);
    if (UScheckRequiredField()) {
        var data = {
            ProductId: productAutoId,
            Qty: $("#txtTotalPieces").val()
        }
        $.ajax({
            type: "POST",
            url: "WebAPI/UpdateStock.asmx/Update",
            data: JSON.stringify({ datavalue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == 'Session Expired') {
                    location.href = '/';
                } else {
                    reset();
                    if (result.d != "fail") {
                        $('#tblProcutStockUpdateLog').hide();
                        $('.Pager').hide();
                        swal("", "Stock updated successfully.", "success");
                        $("#ddlUnitType").val('0').change();
                        $("#ddlStock").val(0).change();
                        $("#divUnitType").html('');
                    }
                    else {
                        swal("Error!", "Oops! Something went wrong.Please try later.", "error");

                    }
                }
            },
            error: function (result) {
                swal("Error!", "Oops! Something went wrong.Please try later.", "error");

            },
            failure: function (result) {
                swal("Error!", "Oops! Something went wrong.Please try later.", "error");

            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
}
function reset() {
    $("#txtNewProductQty").val('');
    $("#txtProductQty").val('');
}
$(".close").click(function () {
    $(".alert").hide();
});


function readBarcode() {

    var Barcode = $("#txtBarCode").val();
    if (Barcode != "") {
        $.ajax({
            type: "POST",
            url: "WebAPI/UpdateStock.asmx/GetBarDetails",
            data: "{'Barcode':'" + Barcode + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d == 'Session Expired') {
                    location.href = '/';
                } else {
                    var xmldoc = $.parseXML(response.d);
                    var product = $(xmldoc).find("Table");
                    if (product.length > 0) {
                        var productAutoId = $(product).find('ProductId').text();
                        $('#ddlStock').val(productAutoId).change()
                    } else {
                        swal("Error!", "Barcode does not exist.", "error").then(function () {
                            $("#txtBarCode").focus();
                        });

                    }
                    $("#txtBarCode").val('');
                }
            },
            failure: function (result) {
                swal("Error!", result.d, "error");
            },
            error: function (result) {
                swal("Error!", result.d, "error");
            }
        });
    } else {
        $("#ddlProduct").val(0).change();
        $("#alertBarcodeCount").hide();
        $("#ddlProduct").attr('disabled', false);
        $("#ddlUnitType").attr('disabled', false);
    }
}

function UScheckRequiredField() {
    var boolcheck = true;
    $('.req').each(function () {
        if ($(this).val() == '') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });

    $('.ddlreq').each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == 'undefined' || $(this).val() == 'Select') {
            boolcheck = false;
            $(this).closest('div').find('.select2-selection--single').attr('style', 'border:1px solid #FF9149 !important');
        } else {
            $(this).removeClass('border-warning');
            $(this).closest('div').find('.select2-selection--single').removeAttr('style');
        }
    });
    return boolcheck;
}
