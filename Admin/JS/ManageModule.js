﻿$(document).ready(function () {
    getModuleList(1);
    getParentModule();
});

function manageModuleBox(value) {
    if (value == 1) {
        $("#parentModule").show();
    } else {
        $("#parentModule").hide();
    }
}
function getParentModule() {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WManageModule.asmx/getParentModule",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (result) {

            var xmldoc = $.parseXML(result.d);
            var role = $(xmldoc).find('Table');
            $("#parentModuleBox option:not(:first)").remove();
            $.each(role, function () {
                $("#parentModuleBox").append($("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("ModuleName").text().replace(/&quot;/g, "\'") + "</option>"));
            });

            $("#searchParentModule option:not(:first)").remove();
            $.each(role, function () {
                $("#searchParentModule").append($("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("ModuleName").text().replace(/&quot;/g, "\'") + "</option>"));
            });
            
            $("#parentModuleBox").select2();
            $("#searchParentModule").select2();

        },
        error: function (result) {

        }

    });
}

function Pagevalue(e) {
    getModuleList(parseInt($(e).attr("page")));
};
function getModuleList(PageIndex) {
    var data = {
        Module: $('#SearchModuleName').val(),
        ParentModule: $('#searchParentModule').val(),
        Status: $('#searchStatus').val(),
        PageSize: $("#ddlPageSize").val(),
        PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WManageModule.asmx/getModuleList",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (result) {
            console.log(result);
            var xmldoc = $.parseXML(result.d);

            var pages = $(xmldoc).find('Table1');
            $("#moduleList tbody tr").remove();
            var row = $("#moduleList thead tr").clone(true);

            if (pages.length > 0) {
                $.each(pages, function () {

                    $(".action", row).html("<a href='#' title='Edit'><span class='ft-edit' onclick='editModule(this)' /></a>&nbsp;&nbsp;&nbsp;<a href='#' title='Remove'><span class='ft-x' onclick='deleterecord(" + $(this).find("AutoId").text()+")' /></a>");
                    $(".AutoId", row).text($(this).find("AutoId").text());
                    $(".HasParent", row).text($(this).find("HasParent").text());
                    $(".HiddenModule", row).text($(this).find("ModuleName").text());
                    $(".HiddenIconClass", row).text($(this).find("iconClass").text());
                    $(".HiddenStatus", row).text($(this).find("Status").text());
                    $(".HiddenParentModule", row).text($(this).find("ParentId").text());
                    $(".ModuleId", row).text($(this).find("ModuleId").text());
                    $(".Module", row).text($(this).find("ModuleName").text());
                    $(".MenuType", row).text($(this).find("MenuType").text());
                    $(".ParentModule", row).text($(this).find("ParentModule").text());
                    if ($(this).find("Status").text() == '1') {
                        $(".Status", row).html("<span class='badge badge badge-pill badge-success'>Active</span>");
                        $(".HiddenStatus", row).text("1");

                    }
                    else {
                        $(".Status", row).html("<span class='badge badge badge-pill badge-danger'>Inactive</span>");
                        $(".HiddenStatus", row).text("0");

                    }
                    $(".SequenceNo", row).text($(this).find("SequenceNo").text());                    
                    $("#moduleList tbody").append(row);
                    row = $("#moduleList tbody tr:last-child").clone(true);
                });
            }
            var pager = $(xmldoc).find("Table");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
        },
        error: function (result) {

        },

    });
   
}

$("#savePageBtn").click(function () {
    if (checkRequiredField()) {
        var data = {
            Module: $("#txtModule").val(),
            ModuleId: $("#txtModuleId").val(),
            HasModule: $("#HasModule").val(),
            Status: $("#status").val(),
            ParentModule: $("#parentModuleBox").val(),
            iconClass: $("#iconClass").val(),
            SequenceNo: $("#txtSequenceNumber").val(),
            MenuType: $("#ddlMenuType").val()
        }
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/WManageModule.asmx/insert",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {

                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == 'Success') {
                    swal("", "Module details saved successfully.", "success");
                    clearField();
                    getParentModule();
                    getModuleList();
                }
                else if (result.d == 'Module already exists.') {
                    swal("", "Module already exists.", "error");
                }
                else if (result.d == 'Module Id already exists.') {
                    swal("", "Module Id already exists.", "error");
                }
                else if (result.d == 'Unauthorized access.') {
                    location.href = "/";
                }
                else {
                    swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                }
            },
            error: function (result) {
                swal("Error!", result.d, "error");
            },
            failure: function (result) {
                swal("Error!", result.d, "error");
            }
        });
    } else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
});


function editModule(e) {
    debugger;
    var $row = $(e).closest("tr");
    var $AutoId = $row.find(".AutoId").text();
    var $ModuleId = $row.find(".ModuleId").text();
    var $ModuleName = $row.find(".Module").text();
    var $HiddenStatus = $row.find(".HiddenStatus").text();
    var $HasParent = $row.find(".HasParent").text();
    var $ParentModule = $row.find(".HiddenParentModule").text();
    var $HiddenIconClass = $row.find(".HiddenIconClass").text();
    var $MenuType = $row.find(".MenuType").text();

    $("#HiddenAutoId").val($AutoId);
    $("#txtModuleId").val($ModuleId);
    $("#txtModule").val($ModuleName);
    $("#HasModule").val($HasParent);
    $("#Status").val($HiddenStatus);
    $("#iconClass").val($HiddenIconClass);
    if ($MenuType == 'External') {
        $("#ddlMenuType").val('0');
    }
    else {
        $("#ddlMenuType").val('1');
    }
    if ($HasParent == 1) {
        $("#parentModule").show();
        $("#parentModuleBox").val($ParentModule).change();

    } else {
        $("#parentModule").hide();
    }

    $("#savePageBtn").hide();
    $("#btnReset").hide();
    $("#btnUpdate").show();
    $("#btnCancel").show();
}

$("#btnUpdate").click(function () {
    if (checkRequiredField()) {
        debugger;
        var data = {
            AutoId: $("#HiddenAutoId").val(),
            Module: $("#txtModule").val(),
            ModuleId: $("#txtModuleId").val(),
            HasModule: $("#HasModule").val(),
            Status: $("#status").val(),
            ParentModule: $("#parentModuleBox").val(),
            iconClass: $("#iconClass").val(),
            SequenceNo: $("#txtSequenceNumber").val(),
            MenuType: $("#ddlMenuType").val()
        }

        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/WManageModule.asmx/update",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == 'Success') {
                    swal("", "Module detail updated successfully.", "success");
                    clearField();
                    $("#savePageBtn").show();
                    $("#btnReset").show();
                    $("#btnUpdate").hide();
                    $("#btnCancel").hide();
                    getModuleList(1);
                }
                else if (result.d == 'ModuleExists') {
                    swal("", "Module already exists.", "error");
                }
                else if (result.d == 'Module Id already exists.') {
                    swal("", "Module Id already exists.", "error");
                }
                else if (result.d == 'Unauthorized access.') {
                    location.href = "/";
                }
                else {
                    swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                }
            },
            error: function (result) {
                swal("Error!", result.d, "error");
            },
            failure: function (result) {
                swal("Error!", result.d, "error");
            }
        });
    } else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
});

function clearField() {
    $("#txtModuleId").val("");
    $("#txtModule").val("");
    $("#iconClass").val("");
    $("#HasModule").val("0");
    $("#status").val("1");
    $("#ddlMenuType").val("1");
    $("#parentModule").hide();
    $("#txtSequenceNumber").val('');
    return;
}

$("#btnCancel").click(function () {
    $("#txtModuleId").val("");
    $("#txtModule").val("");
    $("#iconClass").val("");
    $("#HasModule").val("0");
    $("#status").val("1");
    $("#ddlMenuType").val("1");
    $("#parentModule").hide();
    $("#parentModuleBox").val("0").change();
    $("#savePageBtn").show();
    $("#btnReset").show();
    $("#btnUpdate").hide();
    $("#btnCancel").hide();
    return;
});

function deleteModule(ModuleAutoId) {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WManageModule.asmx/deleteModule",
        data: "{'ModuleAutoId':'" + ModuleAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (result) {
            if (result.d == "true") {
                swal("", "Module details deleted successfully.", "success");
                getModuleList(1);
            }
            else if (result.d == "Exists") {
                swal("Error!", "This Module has been used in application.", "error");

            }
            else if (result.d == "Session Timeout") {
                location.href = "/";
            }
            else {
                swal("Error!", "Oops! Something went wrong.Please try later.", "error");

                getModuleList(1);
            }

        },
        error: function (result) {
            swal("Error!", "This Module has been used in application.", "error");
        },
        failure: function (result) {
            swal("Error!", result.d, "error");
        }
    });

}
function deleterecord(ModuleAutoId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this Module",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: false,
            },
            confirm: {
                text: "Yes, delete it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            deleteModule(ModuleAutoId);

        } else {
            swal("", "Your Module detail is safe.", "error");
        }
    })
}