﻿$(document).ready(function () {
    getSavedAccessPageList(1);
    bindModule();
    bindSearchModule();
    searchUserType();
});

function Pagevalue(e) {
    getSavedAccessPageList(parseInt($(e).attr("page")));
};

function getSavedAccessPageList(PageIndex) {
    var data = {
        UserRole: $('#searchRole').val(),
        ModuleAutoId: $('#searchModule').val(),
        PageSize: $("#ddlPageSize").val(),
        PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WManageAccessPage.asmx/getSavedAccessPageList",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (result) {
            var xmldoc = $.parseXML(result.d);

            var pages = $(xmldoc).find('Table1');
            $("#AccessPageList tbody tr").remove();
            var row = $("#AccessPageList thead tr").clone(true);
            $(".pgliost").hide();
            $(".pageListData").hide();
            if (pages.length > 0) {
                $.each(pages, function () {
                    $(".action", row).html("<a href='#' title='edit'><span class='ft-edit' onclick='EditAccessPage(\"" + $(this).find("AutoId").text() + "\")' /></a>&nbsp;<a href='#' title='delete'><span class='ft-x' onclick= 'removeAccessPage(\"" + $(this).find("AutoId").text() + "\")' /></a>");
                    $(".AutoId", row).text($(this).find("AutoId").text());
                    $(".HiddenStatus", row).text($(this).find("Status").text());
                    $(".HiddenModule", row).text($(this).find("ModuleAutoId").text());
                    $(".HiddenRole", row).text($(this).find("Role").text());
                    $(".HiddenAction", row).text($(this).find("AssignedAction").text());

                    $(".Module", row).text($(this).find("ModuleName").text());
                    $(".PageName", row).text($(this).find("PageUrl").text());
                    $(".UserRole", row).text($(this).find("TypeName").text());
                    if ($(this).find("Status").text() == '1') {
                        $(".Status", row).html("<span class='badge badge badge-pill badge-success'>active</span>");
                    }
                    else {
                        $(".Status", row).html("<span class='badge badge badge-pill badge-danger'>inactive</span>");
                    }
                    $("#AccessPageList tbody").append(row);
                    row = $("#AccessPageList tbody tr:last-child").clone(true);

                });
                var pager = $(xmldoc).find("Table");
                $(".Pager").ASPSnippets_Pager({
                    ActiveCssClass: "current",
                    PagerCssClass: "pager",
                    PageIndex: parseInt(pager.find("PageIndex").text()),
                    PageSize: parseInt(pager.find("PageSize").text()),
                    RecordCount: parseInt(pager.find("RecordCount").text())
                });
            }

        },
        error: function (result) {

        },

    });
   
    
}

function clearField() {
    location.href = "/Admin/ManagePageAccess.aspx";
}

function EditAccessPage(AutoId) {
    var data = {
        AutoId: AutoId
    };
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WManageAccessPage.asmx/edit",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async:false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            console.log(response);
            var xmldoc = $.parseXML(response.d);
            var PageList = $(xmldoc).find("Table");

            var Action = $(xmldoc).find("Table1");
            var ActionStr = $(Action).find("AssignedAction").text();

            if (Action.length > 0) {
                var arr = ActionStr.split(',');
                $("#saveBtn").hide();
                $("#btnReset").hide();
                $("#btnUpdate").show();
                $("#btnCancel").show();
                $("#Module").attr("disabled", true);
                $("#UserRole").attr("disabled", true);
            } else {
                var arr = [];
                $("#saveBtn").show();
                $("#btnReset").show();
                $("#btnUpdate").hide();
                $("#btnCancel").hide();
            }
            var ModuleAutoId = $(Action).find("ModuleAutoId").text();
            var Role = $(Action).find("Role").text();
            $("#Module").val(ModuleAutoId).change();
            $("#UserRole").val(Role).change();

            var pageHtml = '';
            pageHtml += '<div class="table-responsive"><table class="table table-striped table-bordered" id="tblActionList">';
            pageHtml += '<thead class="bg-blue white"><tr><td class="actionName text-center">Module Name</td><td class="action text-center">Action</td></tr></thead><tbody>';
            var i = 1;
            pageHtml += '<tr>';
            pageHtml += '<td class="text-center">' + $('#Module option:selected').text() + '</td><td>';
            $.each(PageList, function () {
                console.log(" Page Action Id :  ", $(this).find("AutoId").text());

                if ($.inArray($(this).find("AutoId").text(), arr) > -1) {
                    pageHtml += '<input type="checkbox" name="pageAction" value="' + $(this).find("AutoId").text() + '" checked> ' + $(this).find("ActionName").text() + ' ';

                } else {
                    pageHtml += '<input type="checkbox" name="pageAction" value="' + $(this).find("AutoId").text() + '"> ' + $(this).find("ActionName").text() + ' ';

                }
            });
            pageHtml += ' </td></tr>';
            pageHtml += '</tbody></table></div>';
            $(".pageListData").html(pageHtml);

           
    
          
        },
        error: function (result) {
            swal("Error!", result.d, "error");
        },
        failure: function (result) {
            swal("Error!", result.d, "error");
        }
    });
    
}

$("#btnCancel").click(function () {
    location.href = "/Admin/ManagePageAccess.aspx";
});

$("#btnUpdate").click(function () {
    if (checkRequiredField()) {
        if ($("input[name=pageAction]:checked").length > 0) {
            var pageAction = $("input[name=pageAction]:checked").map(function () {
                return this.value;
            }).get().join(",");
        } else {
            toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            return false;
        }

        var data = {
            ModuleAutoId: $("#Module").val(),
            AssignedAction: pageAction,
            UserRole: $("#UserRole").val(),
        }

        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/WManageAccessPage.asmx/update",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == 'Success') {
                    swal("", "Module assigned updated successfully.", "success").then(function () {
                        clearField();

                    });
                }
                else if (result.d == 'Unauthorized access.') {
                    location.href = "/";
                }
                else {
                    swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                }
            },
            error: function (result) {
                swal("Error!", result.d, "error");
            },
            failure: function (result) {
                swal("Error!", result.d, "error");
            }
        });
    } else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
});

function getUserType() {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WManageAccessPage.asmx/getUserType",
        data: "{}",
        async: false,
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (result) {
            var xmldoc = $.parseXML(result.d);
            var role = $(xmldoc).find('Table');
            $("#UserRole option:not(:first)").remove();
            $.each(role, function () {
                $("#UserRole").append($("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("TypeName").text().replace(/&quot;/g, "\'") + "</option>"));
            });
            $("#UserRole").select2();
        },
        error: function (result) {

        },

    });
}


function bindModule() {

    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WManageAccessPage.asmx/bindModule",
        data: "{}",
        async: false,
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (result) {
            console.log("dhdkjfhkfhskdjfhsdf");
            console.log(result);
            var xmldoc = $.parseXML(result.d);
            var module = $(xmldoc).find('Table');
            $('#Module option:not(:first)').remove();
            $.each(module, function () {
                if ($(this).find("ParenntModule").text() != '') {
                    $("#Module").append($("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("ParenntModule").text().replace(/&quot;/g, "\'") + " > " + $(this).find("ModuleName").text().replace(/&quot;/g, "\'") + "</option>"));
                } else {
                    $("#Module").append($("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("ModuleName").text().replace(/&quot;/g, "\'") + "</option>"));

                }
            });
            $("#Module").select2();
        },
        error: function (result) {

        },

    });
}

function getPage() {
    var data = {
        UserRole: $("#UserRole").val(),
        ModuleAutoId: $("#Module").val()
    };
    $.ajax({
        type: "POST",
        async: true,
        url: "/Admin/WebAPI/WManageAccessPage.asmx/getPage",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == "SessionExpire") {
                location.href = "/";
            }
            else {
                $(".pgliost").show();
                $(".pageListData").show();

                var xmldoc = $.parseXML(response.d);
                var PageList = $(xmldoc).find("Table");

                var Action = $(xmldoc).find("Table1");
                var ActionStr = $(Action).find("AssignedAction").text();
                if (Action.length > 0) {
                    var arr = ActionStr.split(',');
                    $("#saveBtn").hide();
                    $("#btnReset").hide();
                    $("#btnUpdate").show();
                    $("#btnCancel").show();
                } else {
                    var arr = [];
                    $("#saveBtn").show();
                    $("#btnReset").show();
                    $("#btnUpdate").hide();
                    $("#btnCancel").hide();
                }
                console.log(arr);
                var pageHtml = '';
                pageHtml += '<div class="table-responsive"><table class="table table-striped table-bordered" id="tblActionList">';
                pageHtml += '<thead class="bg-blue white"><tr><td class="actionName text-center">Module Name</td><td class="action text-center">Action</td></tr></thead><tbody>';
                var i = 1;
                pageHtml += '<tr>';
                pageHtml += '<td class="text-center">' + $('#Module option:selected').text() + '</td><td>';
                $.each(PageList, function () {
                    console.log(" Page Action Id :  ", $(this).find("AutoId").text());

                    if ($.inArray($(this).find("AutoId").text(), arr) > -1) {
                        pageHtml += '<input type="checkbox" name="pageAction" value="' + $(this).find("AutoId").text() + '" checked> ' + $(this).find("ActionName").text() + ' ';

                    } else {
                        pageHtml += '<input type="checkbox" name="pageAction" value="' + $(this).find("AutoId").text() + '"> ' + $(this).find("ActionName").text() + ' ';

                    }
                });
                pageHtml += ' </td></tr>';
                pageHtml += '</tbody></table></div>';
                $(".pageListData").html(pageHtml);

            }
        },
        failure: function (result) {
            swal("Error!", 'Oops!,Something went wrong.Please try again.', "error");
        },
        error: function (result) {
            swal("Error!", 'Oops!,Something went wrong.Please try again.', "error");
        }
    });
}

$("#saveBtn").click(function () {
    if (checkRequiredField()) {
        if ($("input[name=pageAction]:checked").length > 0) {
            var pageAction = $("input[name=pageAction]:checked").map(function () {
                return this.value;
            }).get().join(",");
        } else {
            toastr.error('Please check atleast one action', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            return false;
        }
        var data = {
            ModuleAutoId: $("#Module").val(),
            AssignedAction: pageAction,
            UserRole: $("#UserRole").val()
        }

        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/WManageAccessPage.asmx/insert",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == 'Success') {
                    swal("", "Module assigned successfully.", "success").then(function () {
                        clearField();
                    });
                }
                else if (result.d == 'Unauthorized access.') {
                    location.href = "/";
                }
                else if (result.d = 'Module already assigned.') {
                    swal("Error!", "Module already assigned.", "error");
                }
                else {
                    swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                }
            },
            error: function (result) {
                swal("Error!", result.d, "error");
            },
            failure: function (result) {
                swal("Error!", result.d, "error");
            }
        });
    } else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
});

function bindSearchModule() {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WManageAccessPage.asmx/bindModule",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (result) {
            var xmldoc = $.parseXML(result.d);
            var module = $(xmldoc).find('Table');
            $('#searchModule option:not(:first)').remove();
            //$.each(module, function () {
            //    $("#searchModule").append($("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("ModuleName").text().replace(/&quot;/g, "\'") + "</option>"));
            //});
            //$("#searchModule").select2();
            $.each(module, function () {
                if ($(this).find("ParenntModule").text() != '') {
                    $("#searchModule").append($("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("ParenntModule").text().replace(/&quot;/g, "\'") + " > " + $(this).find("ModuleName").text().replace(/&quot;/g, "\'") + "</option>"));
                } else {
                    $("#searchModule").append($("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("ModuleName").text().replace(/&quot;/g, "\'") + "</option>"));

                }
            });
            $("#searchModule").select2();
        },
        error: function (result) {

        },

    });
}

function removeAccessPage(AutoId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this page",
        icon: "warning",
        buttons: {
            cancel: {
                text: "No, cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: false,
            },
            confirm: {
                text: "Yes, delete it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm, e) {
        if (isConfirm) {
            var data = {
                AutoId: AutoId
            }
            $.ajax({
                type: "POST",
                url: "/Admin/WebAPI/WManageAccessPage.asmx/delete",
                data: "{'dataValue':'" + JSON.stringify(data) + "'}",
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (result) {
                    if (result.d == 'Success') {
                        swal("", "Assigned Page deleted successfully.", "success");
                        getSavedAccessPageList(1);
                    } else {
                        swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                    }
                },
                error: function (result) {
                    swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                },

            });

        } else {
            swal("", "Your data is safe.", "error");
        }
    })
}

function searchUserType() {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WManageAccessPage.asmx/getUserType",
        data: "{}",
        async: false,
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (result) {
            var xmldoc = $.parseXML(result.d);
            var role = $(xmldoc).find('Table');
            $.each(role, function () {
                $("#searchRole").append($("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("TypeName").text().replace(/&quot;/g, "\'") + "</option>"));
            });
            $("#searchRole").select2();
        },
        error: function (result) {

        },

    });
}