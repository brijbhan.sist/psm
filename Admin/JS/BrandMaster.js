﻿$(document).ready(function () {
    getBrandDetail(1);
    resetBrand();
    $("#btnUpdate").hide();
    $("#btnCancel").hide();
})

function Save() {
    if (checkRequiredField()) {
        var data = {
            BrandName: $("#txtBrandName").val().trim(),
            Description: $("#txtDescription").val().trim(),
            Status: $("#ddlStatus").val()
        }
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/BrandMaster.asmx/insertBrand",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == "Session Expired") {
                    window.location = "/";
                }
                else if (result.d == 'Success') {
                    swal({
                        title: "",
                        text: "Brand details saved successfully.",
                        icon: "success",
                        allowOutsideClick: false,
                        closeOnClickOutside: false,
                        button: "OK",
                    }).then(function () {
                        getBrandDetail(1);
                    }); 
                    resetBrand();
                } else {
                    swal("Error!", result.d, "error");
                }
            },
            error: function (result) {

            },
            failure: function (result) {

            }
        });
    }
    else {
        toastr.error('All * fields are mandatory .', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
};
function Pagevalue(e) {
    getBrandDetail(parseInt($(e).attr("page")));
};
function getBrandDetail(PageIndex) {
    var data = {
        BrandId: $('#txtSBrandId').val().trim(),
        BrandName: $('#txtSBrandName').val().trim(),
        Status: $('#ddlSStatus').val(),
        PageSize: $("#ddlPageSize").val(),
        PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/BrandMaster.asmx/getBrandDetail",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: onSuccessOfCategory,
        error: function (response) {
            console.log(JSON.parse(response.responseText).d);
        }
    });
}
function onSuccessOfCategory(response) {
    var xmldoc = $.parseXML(response.d);
    var category = $(xmldoc).find('Table1');

    if (category.length > 0) {

        $('#tblBrand tbody tr').remove();
        var row = $('#tblBrand thead tr').clone(true);
        $.each(category, function () {
            $(".AutoId", row).html($(this).find("AutoId").text());
            $(".BrandId", row).html($(this).find("BrandId").text());
            $(".BrandName", row).html($(this).find("BrandName").text());
            $(".description", row).html($(this).find("Description").text());
            if ($(this).find("Status").text() == 'Active') {
                $(".status", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("Status").text() + "</span>");
            } else {
                $(".status", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("Status").text() + "</span>");

            }
            $(".action", row).html("<a title='Edit' href='#'><span class='ft-edit' onclick='editBrand(\"" + $(this).find("AutoId").text() + "\")' /></a>&nbsp;&nbsp;<a title='Delete' href='#'><span class='ft-x' onclick= 'deleterecord(\"" + $(this).find("AutoId").text() + "\")' /></a>");
            $("#tblBrand tbody").append(row);
            row = $("#tblBrand tbody tr:last-child").clone(true);
        });
        var pager = $(xmldoc).find("Table");
        $(".Pager").ASPSnippets_Pager({
            ActiveCssClass: "current",
            PagerCssClass: "pager",
            PageIndex: parseInt(pager.find("PageIndex").text()),
            PageSize: parseInt(pager.find("PageSize").text()),
            RecordCount: parseInt(pager.find("RecordCount").text())
        });
    }
    else {
        $('#tblBrand tbody tr').remove();

    }

}

function editBrand(AutoId) {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/BrandMaster.asmx/editBrand",
        data: "{'AutoId':'" + AutoId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: onSuccessOfEdit,
        error: function (response) {
            console.log(response.d)
        }
    })
}
function onSuccessOfEdit(response) {
    if (response.d == 'Session Expired') {
        location.href = '/';
    } else {
        var xmldoc = $.parseXML(response.d);
        var category = $(xmldoc).find('Table');
        $("#lblAutoId").text($(category).find("AutoId").text());
        $("#txtBrandId").val($(category).find("BrandId").text());
        $("#txtBrandName").val($(category).find("BrandName").text());
        $("#txtDescription").val($(category).find("Description").text());
        $("#ddlStatus").val($(category).find("Status").text());
        $("#btnSave").hide();
        $("#btnReset").hide();
        $("#btnUpdate").show();
        $("#btnCancel").show();
    }
}

function deleteBrand(AutoId) {

    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/BrandMaster.asmx/deleteBrand",
        data: "{'AutoId':'" + AutoId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (result) {
            if (result.d == "Session Expired") {
                window.location = "/";
            }
            else if (result.d == 'Success') {
                swal({
                    title: "",
                    text: "Brand details deleted successfully.",
                    icon: "success",
                    allowOutsideClick: false,
                    closeOnClickOutside: false,
                    button: "OK",
                }).then(function () {
                    getBrandDetail(1);
                }); 
                resetBrand();
            } else {
                swal("Error!", result.d, "error");
            }
        },
        error: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later", "error");

        },
        failure: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later", "error");
        }
    })
}


function Update() {
    if (checkRequiredField()) {
        var data = {
            AutoId: $("#lblAutoId").text(),
            BrandId: $("#txtBrandId").val().trim(),
            BrandName: $("#txtBrandName").val().trim(),
            Description: $("#txtDescription").val().trim(),
            Status: $("#ddlStatus").val()
        };
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/BrandMaster.asmx/updateBrand",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json;charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },

            success: function (result) {
                if (result.d == "Session Expired") {
                    window.location = "/";
                }
                else if (result.d == 'Success') {
                    swal({
                        title: "",
                        text: "Brand details updated successfully.",
                        icon: "success",
                        allowOutsideClick: false,
                        closeOnClickOutside: false,
                        button: "OK",
                    }).then(function () {
                        getBrandDetail(1);
                    });
                  
                    resetBrand();
                } else {
                    swal("Error!", result.d, "error");
                }
            },
            error: function (result) {
                swal("Error!", "Oops! Something went wrong.Please try later", "error");
            },
            failure: function (result) {
                swal("Error!", "Oops! Something went wrong.Please try later", "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
};
function resetBrand() {
    $("#btnUpdate").hide();
    $("#btnCancel").hide();
    $("#btnSave").show();
    $("#btnReset").show();
    $('input[type="text"]').val('');
    $('textarea').val('');
    $('#ddlStatus').val('1');
    $("#txtBrandName").removeClass('border-warning');
}


function Cancel() {
    resetBrand();
    $("#btnUpdate").hide();
    $("#btnCancel").hide();
    $("#btnSave").show();
    $("#btnReset").show();
}
function deleterecord(AutoId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this item.",
        icon: "warning",
        showCancelButton: true,
        allowOutsideClick: false,
        closeOnClickOutside: false,
        buttons: {
            cancel: {
                text: "No, cancel it",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, delete it",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(isConfirm => {
        if (isConfirm) {
            deleteBrand(AutoId);
        }
    })
}