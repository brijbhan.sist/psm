﻿$(document).ready(function () {
    getTaxDetail();
    resetTax();
    $("#btnUpdate").hide();
    $("#btnCancel").hide();
})

function save() {
    if (checkRequiredField()) {
        var data = {
            TaxName: $("#TxtTaxName").val().trim(),
            Description: $("#TxtDescription").val(),
            Value: $("#Txtvalue").val().trim(),
            Status: $("#ddlStatus").val(),
        }
        $.ajax({
            type: "POST",
            url: "/Admin/SubTaxMaster.aspx/insertTax",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    timeout: 2000, //unblock after 2 seconds
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },

            success: function (result) {
                if (result.d == "Success") {
                    swal("", "Subcategory tax details saved successfully.", "success");

                    getTaxDetail();
                    resetTax();
                }
                else if (result.d == "Unauthorized access.") {
                    location.href = "/";
                }
                else {
                    swal("", result.d, "error");
                }
            },
            error: function (result) {
                swal("Error!", result.d, "error");
            },
            failure: function (result) {
                swal("Error!", result.d, "error");
            }
        });
    } else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
};

/*----------------------------------------------------List Tax Details--------------------------------------------------------*/
function getTaxDetail() {

    var data = {
        TaxName: $('#txtSTaxName').val().trim(),
        Status: $('#ddlSStatus').val()
    };
    $.ajax({
        type: "POST",
        url: "/Admin/SubTaxMaster.aspx/getTaxDetail",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });

        },
        complete: function () {

        },
        success: onSuccessOfTax,
        error: function (response) {
            console.log(JSON.parse(response.responseText).d);
        }
    });
}
function onSuccessOfTax(response) {
    var xmldoc = $.parseXML(response.d);
    var Tax = $(xmldoc).find('Table');

    if (Tax.length > 0) {
        $('#EmptyTable').hide();
        $('#tblTax tbody tr').remove();
        var row = $('#tblTax thead tr').clone(true);
        $.each(Tax, function () {
            $(".TaxName", row).text($(this).find("TaxName").text());
            $(".Value", row).text($(this).find("Value").text());
            $(".Description", row).text($(this).find("Description").text());
            if ($(this).find("Status").text() == 'Active') {
                $(".status", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("Status").text() + "</span>");
            } else {
                $(".status", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("Status").text() + "</span>");

            }
            $(".action", row).html("<a href='#' title='Edit'><span class='ft-edit' onclick='editTax(\"" + $(this).find("AutoId").text() + "\")' /></a>&nbsp;<a href='#' title='Delete'><span class='ft-x' onclick= 'deleterecord(\"" + $(this).find("AutoId").text() + "\")' /></a>");
            $("#tblTax tbody").append(row);
            row = $("#tblTax tbody tr:last-child").clone(true);
        });
    }
    else {
        $('#tblTax tbody tr').remove();
        $('#EmptyTable').show();
    }
}
/*----------------------------------------------------Search Engine----------------------------------------------------------*/
$("#btnSearch").click(function () {
    getTaxDetail();
});
/*----------------------------------------------------Edit Tax Detail---------------------------------------------------*/
function editTax(TaxId) {
    $.ajax({
        type: "POST",
        url: "/Admin/SubTaxMaster.aspx/editTax",
        data: "{'TaxId':'" + TaxId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },

        success: onSuccessOfEdit,
        error: function (response) {
            console.log(response.d)
        }
    })
}
function onSuccessOfEdit(response) {
    var xmldoc = $.parseXML(response.d);
    var Tax = $(xmldoc).find('Table');
    $("#hfTaxId").val($(Tax).find("AutoId").text());
    $("#TxtTaxName").val($(Tax).find("TaxName").text());
    $("#Txtvalue").val($(Tax).find("Value").text());
    $("#ddlStatus").val($(Tax).find("Status").text());
    $("#TxtDescription").val($(Tax).find("Description").text());
    $("#btnSave").hide();
    $("#btnReset").hide();
    $("#btnUpdate").show();
    $("#btnCancel").show();
}

/*----------------------------------------------------Update Tax Detail--------------------------------------------------------*/

function Update() {
    if (checkRequiredField()) {
        var data = {
            TaxId: $("#hfTaxId").val().trim(),
            TaxName: $("#TxtTaxName").val().trim(),
            Description: $("#TxtDescription").val(),
            Value: $("#Txtvalue").val().trim(),
            Status: $("#ddlStatus").val(),
        };
        $.ajax({
            type: "POST",
            url: "/Admin/SubTaxMaster.aspx/updateTax",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json;charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    timeout: 2000, //unblock after 2 seconds
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },

            success: function (result) {
                if (result.d == "Unauthorized access.") {
                    location.href = "/";
                }
                else if (result.d == "Success") {
                    swal("", "Subcategory tax details updated successfully.", "success");

                    getTaxDetail();
                    resetTax();
                }
                else {
                    swal("Error!", result.d, "error");
                }
            },
            error: function (result) {
                swal("Error!", result.d, "error");
            },
            failure: function (result) {
                swal("Error!", result.d, "error");
            }
        });
    } else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}

/*--------------------------------------------------------Delete Tax ----------------------------------------------------------*/
function deleteTax(TaxId) {
    $.ajax({
        type: "POST",
        url: "/Admin/SubTaxMaster.aspx/deleteTax",
        data: "{'TaxId':'" + TaxId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },

        success: function (result) {
            if (result.d == "Success") {
                swal("", "Subcategory tax details deleted successfully.", "success");
                getTaxDetail();
                resetTax();
            }
            else {
                swal("", result.d, "error");
            }

        },
        error: function (result) {
            swal("error", "Subcategory tax already assigned to Orders.", "error");
        },
        failure: function (result) {
            swal("error", "Subcategory tax already assigned to Orders.", "error");
        }
    })
}
/*-----------------------------------------------------------Reset------------------------------------------------------------------*/
function resetTax() {
    $("#btnUpdate").hide();
    $("#btnCancel").hide();
    $("#btnSave").show();
    $("#btnReset").show();
    $('textarea').val('');
    $('#Txtvalue').val('0.00');
    $('#TxtTaxName').val('');
    $('#ddlStatus').val('1');
    $('#ddlSStatus').val('2');
    $('.req').removeClass('border-warning');
    $('.ddlreq').removeClass('border-warning');
    $('.select2-selection').removeAttr('style');
}


function deleterecord(TaxId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this subcategory tax.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, cancel.",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, delete it.",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(isConfirm => {
        if (isConfirm) {
            deleteTax(TaxId);

        } 
    })
}