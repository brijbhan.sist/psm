﻿$(document).ready(function () {

    $('#txtSFromBillDate').pickadate({
        min: new Date("01/01/2019"),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    })
    $('#txtSToBillDate').pickadate({
        min: new Date("01/01/2019"),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    })
    var d = new Date();
    var month = d.getMonth() + 1;
    if (month.toString().length == 1) {
        month = '0' + month;
    }
    var day = d.getDate();
    if (day.toString().length == 1) {
        day = '0' + day;
    }
    var year = d.getFullYear();
    $("#txtSFromBillDate").val(month + '/' + day + '/' + year);
    $("#txtSToBillDate").val(month + '/' + day + '/' + year);
    bindVendor();
    getStockEntryList(1);

})

function setdatevalidation(val) {
    var fdate = $("#txtSFromBillDate").val();
    var tdate = $("#txtSToBillDate").val();
    if (val == 1) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtSToBillDate").val(fdate);
        }
    }
    else if (val == 2) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtSFromBillDate").val(tdate);
        }
    }
}

function pagingSize() {
    getStockEntryList(1);
}
//$("#ddlPaging").change(function () {
//    getStockEntryList(1);
//})

function bindVendor() {
    $.ajax({
        type: "POST",
        url: "/Admin/DropDownMaster.aspx/BindVendor",
        data: "{}",
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var getData = $.parseJSON(response.d);
            $("#ddlSVendor option:not(:first)").remove();
            $.each(getData[0].VendorList, function (index, item) {
                $("#ddlSVendor").append("<option value='" + item.AutoId + "'>" + item.VendorName + "</option>");
            });
            $("#ddlSVendor").select2();
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function Pagevalue(e) {
    getStockEntryList(parseInt($(e).attr("page")));
};
function getStockEntryList(pageIndex) {
    var data = {
        BillNo: $("#txtSBillNo").val().trim(),
        VendorAutoId: $("#ddlSVendor").val(),
        FromBillDate: $("#txtSFromBillDate").val(),
        ToBillDate: $("#txtSToBillDate").val(),
        PageSize: $("#ddlPaging").val(),
        pageIndex: pageIndex
    };

    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/stockEntryList.asmx/getStockEntryList",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == "Session") {
                location.href = "/";
            }
            else {
                var xmldoc = $.parseXML(response.d);
                var entryList = $(xmldoc).find("Table");

                $("#tblStockEntryList tbody tr").remove();
                var row = $("#tblStockEntryList thead tr").clone(true);
                debugger
                if (entryList.length > 0) {
                    $("#EmptyTable").hide();
                    var gTotal = 0.00;
                    $.each(entryList, function () {
                        if ($(this).find("Settlement").text() == '1') {
                            $(".Action", row).html("--");
                        }
                        else {
                            $(".Action", row).html("<a title='Edit' href='/Admin/stockEntry.aspx?BillAutoId=" + $(this).find("AutoId").text() + "'><span class='ft-edit' ></span></a>");
                        }
                        $(".BillNo", row).html("<span BillAutoId='" + $(this).find("AutoId").text() + "'>" + $(this).find("BillNo").text() + "</span>");
                        $(".BillDate", row).text($(this).find("BillDate").text());
                        $(".VendorName", row).text($(this).find("VendorName").text());
                        $(".NoOfItems", row).text($(this).find("NoOfItems").text());
                        $(".TotalAmount", row).text($(this).find("TotalAmount").text());
                        $(".Remarks", row).text($(this).find("Remarks").text());
                        $("#tblStockEntryList tbody").append(row);
                        row = $("#tblStockEntryList tbody tr:last").clone(true);
                        gTotal = Number(gTotal) + Number($(this).find("TotalAmount").text());
                    });
                    $("#gTotal").text(Number(gTotal).toFixed(2));
                } else {
                    $("#EmptyTable").show();
                    $("#gTotal").text('0.00');
                }
                var pager = $(xmldoc).find("Table1");
                $(".Pager").ASPSnippets_Pager({
                    ActiveCssClass: "current",
                    PagerCssClass: "pager",
                    PageIndex: parseInt(pager.find("PageIndex").text()),
                    PageSize: parseInt(pager.find("PageSize").text()),
                    RecordCount: parseInt(pager.find("RecordCount").text())
                });
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

$("#btnSearch").click(function () {
    getStockEntryList(1);
}) 