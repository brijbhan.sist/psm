﻿
$(document).ready(function () {
    bindCategory();
    resetSubcategory();
    getSubcategory(1);
})
/*---------------------------------------Bind Category-----------------------------------------------*/
function bindCategory() {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/subcategoryMaster.asmx/bindCategory",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: success,
        error: function (response) {
            console.log(response);
        },
        failure: function (response) {
            console.log(response);
        }
    });
}
function Pagevalue(e) {
    getSubcategory(parseInt($(e).attr("page")));
};
function success(response) {
    if (response.d != 'Session Expired') {
        var xmldoc = $.parseXML(response.d);
        var category = $(xmldoc).find("Table");
        var SubTax = $(xmldoc).find("Table1");

        $("#ddlCategoryName option").remove();
        $("#ddlCategoryName").append("<option value='0'>-Select-</option>");
        $.each(category, function () {
            $("#ddlCategoryName").append($('<option value="' + $(this).find("AutoId").text() + '">' + $(this).find("CategoryName").text() + '</option>'));
        });
        $("#ddlSCategory option").remove();
        $("#ddlSCategory").append("<option value='0'>All Category</option>");
        $.each(category, function () {
            $("#ddlSCategory").append($('<option value="' + $(this).find("AutoId").text() + '">' + $(this).find("CategoryName").text() + '</option>'));
        });
        $("#dllSubcategoryTax option").remove();
        $("#dllSubcategoryTax").append("<option value='0'>Select Subcategory Tax</option>");
        $.each(SubTax, function () {
            $("#dllSubcategoryTax").append($('<option value="' + $(this).find("AutoId").text() + '">' + $(this).find("TaxName").text() + '</option>'));
        });
        $("#dllSSubcategoryTax option").remove();
        $("#dllSSubcategoryTax").append("<option value='0'>All Subcategory Tax</option>");
        $.each(SubTax, function () {
            $("#dllSSubcategoryTax").append($('<option value="' + $(this).find("AutoId").text() + '">' + $(this).find("TaxName").text() + '</option>'));
        });
    } else {
        location.href = '/';
    }
}
/*---------------------------------------Insert Sub Category-----------------------------------------------*/
$("#btnSave").click(function () {
    var IsShowOnWeb = 0;
    if ($('#cbShowOnWebsite').prop('checked') === true) {
        IsShowOnWeb = 1
    }
    $("#alertSuccess").hide();
    $("#alertDanger").hide();
    if (checkRequiredField()) {
        var data = {
            SubcategoryName: $("#txtSubcategoryName").val(),
            Description: $("#txtDescription").val(),
            SubcategoryTax: $("#dllSubcategoryTax").val(),
            Status: $("#ddlStatus").val(),
            CategoryAutoId: $("#ddlCategoryName").val(),
            IsShowOnWeb: IsShowOnWeb,
            SequenceNo: $("#txtSequenceNo").val(),
        }
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/subcategoryMaster.asmx/insertSubcategory",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d != 'Session Expired') {
                    if (result.d == 'Success') {
                        swal("", "Subcategory details saved successfully.", "success");
                        var table = $('#tblSubcategory').DataTable();
                        table.destroy();
                        getSubcategory();
                        resetSubcategory();
                    } else if (result.d == 'Already Exists') {
                        swal("", "Subcategory already exists.", "error");
                    } else {
                        swal("", "Oops! Something went wrong. Please try later.", "error");
                    }
                } else {
                    location.href = '/';
                }
            },
            error: function (result) {
                swal("", "Oops! Something went wrong. Please try later.", "error");
            },
            failure: function (result) {
                swal("", result.d, "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
});
/*---------------------------------------Select Table Subcategory-----------------------------------------------*/
function getSubcategory(PageIndex) {
    
    var data = {
        SubcategoryId: $("#txtSSubcategoryId").val().trim(),
        SubcategoryName: $("#txtSSubcategoryName").val().trim(),
        SubcategoryTax: $("#dllSSubcategoryTax").val().trim(),
        Status: $("#ddlSStatus").val(),
        CategoryAutoId: $("#ddlSCategory").val(),
        PageSize: $("#ddlPageSize").val(),
        pageIndex: PageIndex

    };
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/subcategoryMaster.asmx/getSubcategory",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
            if ($.fn.DataTable.isDataTable('#tblSubcategory')) {
                $('#tblSubcategory').DataTable().destroy();
            }
        },
        complete: function () {
            $.unblockUI();

        },
        success: onSuccessOfSubcategory,
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function onSuccessOfSubcategory(response) {
    
    if (response.d != 'Session Expired') {
        var xmldoc = $.parseXML(response.d);
        var subcategory = $(xmldoc).find("Table1");

        if (subcategory.length > 0) {

            $("#tblSubcategory tbody tr").remove();
            var row = $("#tblSubcategory thead tr").clone(true);
            $.each(subcategory, function () {
                $(".Action", row).html("<a title='Edit' href='#'><span class='ft-edit' onclick = 'editSubcategory(\"" + $(this).find("SubcategoryId").text() + "\")' /></a>&nbsp;&nbsp;&nbsp;&nbsp;<a title='Delete' href='javascript:;'><span class='ft-x' onclick='deleteSubcategorys(\"" + $(this).find("SubcategoryId").text() + "\")'/></a>");
                $(".Category", row).html($(this).find("CategoryName").text());
                $(".TaxName", row).html($(this).find("TaxName").text());
                $(".SubCatId", row).html($(this).find("SubcategoryId").text());
                $(".SubcategoryName", row).html($(this).find("SubcategoryName").text());
                $(".Description", row).html($(this).find("Description").text());
                $(".SeqNo", row).html($(this).find("SeqNo").text());
                if ($(this).find("Status").text() == 'Active') {
                    $(".Status", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("Status").text() + "</span>");
                } else {
                    $(".Status", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("Status").text() + "</span>");
                }
                if ($(this).find("IsShow").text() == '1') {
                    $(".IsShow", row).html("<span class='badge badge badge-pill badge-success'>Yes</span>");
                } else {
                    $(".IsShow", row).html("<span class='badge badge badge-pill badge-danger'>No</span>");
                }
                $("#tblSubcategory tbody").append(row);
                row = $("#tblSubcategory tbody tr:last-child").clone(true);
            });
        }
        else {
            $("#tblSubcategory tbody tr").remove();
        }
        var pager = $(xmldoc).find("Table");
        $("#ProductPager").ASPSnippets_Pager({
            ActiveCssClass: "current",
            PagerCssClass: "pager",
            PageIndex: parseInt(pager.find("PageIndex").text()),
            PageSize: parseInt(pager.find("PageSize").text()),
            RecordCount: parseInt(pager.find("RecordCount").text())
        });
    } else {
        location.href = '/';
    }
}
$("#btnSearch").click(function () {
    getSubcategory(1);
});

/*---------------------------------------Edit Subcategory-----------------------------------------------*/
function editSubcategory(SubcategoryId) {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/subcategoryMaster.asmx/editSubcategory",
        data: "{'SubcategoryId':'" + SubcategoryId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: onSuccessOfEdit,
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function onSuccessOfEdit(response) {
    if (response.d != 'Session Expired') {
        var xmldoc = $.parseXML(response.d);
        var Subcategory = $(xmldoc).find("Table");

        $.each(Subcategory, function () {
            $("#ddlCategoryName").val($(Subcategory).find("CategoryAutoId").text());
            $("#txtSubcategoryId").val($(Subcategory).find("SubcategoryId").text());
            $("#dllSubcategoryTax").val($(Subcategory).find("SubcategoryTax").text());
            $("#txtSubcategoryName").val($(Subcategory).find("SubcategoryName").text());
            $("#txtDescription").val($(Subcategory).find("Description").text());
            $("#ddlStatus").val($(Subcategory).find("Status").text());
            $("#txtSequenceNo").val($(Subcategory).find("SeqNo").text());
            if ($(Subcategory).find("IsShow").text() == "1") {
                $("#cbShowOnWebsite").prop("checked", true);
            }
            else {
                $("#cbShowOnWebsite").prop("checked", false);
            }
        });
        $("#btnUpdate").show();
        $("#btnCancel").show();
        $("#btnSave").hide();
        $("#btnReset").hide();
    } else {
        location.href = '/';
    }
}

$("#btnCancel").click(function () {
    resetSubcategory();
});
$("#btnReset").click(function () {
    resetSubcategory();
});
/*---------------------------------------Update Subcategory-----------------------------------------------*/

$("#btnUpdate").click(function () {
    var IsShowOnWeb = 0;
    if ($('#cbShowOnWebsite').prop('checked') === true) {
        IsShowOnWeb = 1
    }
    $("#alertSuccess").hide();
    $("#alertDanger").hide();
    if (checkRequiredField()) {
        var data = {
            SubcategoryId: $("#txtSubcategoryId").val(),
            SubcategoryName: $("#txtSubcategoryName").val(),
            SubcategoryTax: $("#dllSubcategoryTax").val(),
            Description: $("#txtDescription").val(),
            Status: $("#ddlStatus").val(),
            CategoryAutoId: $("#ddlCategoryName").val(),
            IsShowOnWeb: IsShowOnWeb,
            SequenceNo: $("#txtSequenceNo").val()
        }
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/subcategoryMaster.asmx/updateSubcategory",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d != 'Session Expired') {
                    if (result.d == 'Success') {
                        swal("", "Subcategory details updated successfully.", "success");
                        var table = $('#tblSubcategory').DataTable();
                        table.destroy();
                        getSubcategory(1);
                        resetSubcategory();
                    } else if (result.d == 'Already Exists') {
                        swal("", "Subcategory already exists.", "error");

                    } else {
                        swal("", "Oops, Something went wrong. Please try again.", "error");
                    }
                } else {
                    location.href = '/';
                }
            },
            error: function (result) {
                swal("", "Oops, Something went wrong. Please try again.", "error");
            },
            failure: function (result) {
                swal("", "Oops! Something went wrong. Please try later.", "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
});
/*---------------------------------------Delete Subcategory-----------------------------------------------*/
function deleteSubcategory(SubcategoryId) {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/subcategoryMaster.asmx/deleteSubcategory",
        data: "{'SubcategoryId':'" + SubcategoryId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (result) {
            if (result.d != 'Session Expired') {
                if (result.d == "Success") {
                    swal("", "Subcategory details deleted successfully.", "success");
                    getSubcategory(1);
                } else if (result.d == "can not delete") {
                    swal("", "Subcategory has been used in application.", "error");

                } else {
                    swal("", "Oops! Something went wrong. Please try later.", "error");
                }
            } else {
                location.href = '/';
            }
        },
        error: function (response) {
            swal("", "Oops! Something went wrong. Please try later.", "error");
        },
        failure: function (result) {
            swal("", "Oops! Something went wrong. Please try later.", "error");
        }
    });
}


function resetSubcategory() {
    $("input[type='text']").val('');
    $("textarea").val('');
    $('#ddlStatus').val('1')
    $('#dllSubcategoryTax').val('0');
    $('#ddlCategoryName').val('0');
    $("#btnUpdate").hide();
    $("#btnCancel").hide();
    $("#btnSave").show();
    $("#btnReset").show();
    $("#ddlCategoryName").removeClass('border-warning');
    $("#txtSubcategoryName").removeClass('border-warning');
    $('#cbShowOnWebsite').prop('checked', false);
}
function deleteSubcategorys(SubcategoryId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this subcategory.",
        icon: "warning",
        showCancelButton: true,
        allowOutsideClick: false,
        closeOnClickOutside: false,
        buttons: {
            cancel: {
                text: "No, cancel.",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, delete it.",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(isConfirm => {
        if (isConfirm) {
            deleteSubcategory(SubcategoryId);
        }
    })
}