﻿$(document).ready(function () {
    $('#txtInspectionExpDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    var PageId = getQueryString('PageId');
    if (PageId != null) {
        editCar(PageId);
        $("#CarAutoId").val(PageId);
        $("#btnUpdate").show();
        $("#btnCancel").show();
        $("#btnSave").hide();
        $("#btnReset").hide();
    } else {
        $("#btnUpdate").hide();
        $("#btnCancel").hide();
        $("#btnSave").show();
        $("#btnReset").show();
    }
})

$(".close").click(function () {
    $(".alert").hide();
});

/*------------------------------------------------------Insert Categroy------------------------------------------------------------*/
function Save() {   
    if (checkRequiredField()) {
        var data = {
            CarName: $("#txtCarName").val(),
            Year: $("#txtYear").val(),
            Make: $("#txtMake").val(),
            Model: $("#txtModel").val(),
            VINNumber: $("#txtVINNumber").val(),
            TAGNumber: $("#txtTAGNumber").val(),
            InspectionExpDate: $("#txtInspectionExpDate").val(),
            StartMilage: $("#txtStartMilage").val(),
            Description: $("#txtDescription").val(),
            Status: $("#ddlStatus").val()
        }
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/WCarDetails.asmx/insert",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
               if (result.d== 'false') {
                    swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                }
                else if (result.d== 'Unauthorized access.') 
                {
                    location.href="/";
                }
                else {
                    var xmldoc = $.parseXML(result.d);
                    CarDetails = $(xmldoc).find('Table');
                    $("#btnSave").hide();
                    $("#btnReset").hide();
                    $("#btnUpdate").show();
                    $("#btnCancel").show();
                    $("#CarAutoId").val($(CarDetails).find('CarAutoId').text());
                    $("#txtCarId").val($(CarDetails).find('CarId').text());                   
                    swal("", "Car details saved successfully.", "success"); 
                }
            },
            error: function (result) {
                swal("Error!", result.d, "error");
            },
            failure: function (result) {
                swal("Error!", result.d, "error");
            }
        });
    } else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
};


/*----------------------------------------------------Edit Car Detail---------------------------------------------------*/
function editCar(CarAutoId) {
      $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WCarDetails.asmx/editCar",
        data: "{'CarAutoId':'" + CarAutoId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: onSuccessOfEdit,
        error: function (response) {
            console.log(response.d)
        }
    })
}
function onSuccessOfEdit(response) {
    var xmldoc = $.parseXML(response.d);
    var Car = $(xmldoc).find('Table');
    $("#txtCarId").val($(Car).find("CarId").text());
    $("#txtCarName").val($(Car).find("CarName").text());
    $("#txtDescription").val($(Car).find("Description").text());
    $("#ddlStatus").val($(Car).find("Status").text());
    $("#txtYear").val($(Car).find("YearName").text());
    $("#txtMake").val($(Car).find("Make").text());
    $("#txtModel").val($(Car).find("Model").text());
    $("#txtVINNumber").val($(Car).find("VINNumber").text());
    $("#txtTAGNumber").val($(Car).find("TAGNumber").text());
    $("#txtInspectionExpDate").val($(Car).find("InspectionExpDate").text());
    $("#txtStartMilage").val($(Car).find("StartMilage").text());
    $("#CarAutoId").val($(Car).find("CarAutoId").text());

}
function Cancel() {
    window.location.replace("CarDetailsList.aspx");
};

/*----------------------------------------------------Update Car Detail--------------------------------------------------------*/

function Update() {
    if (checkRequiredField()) {
        var data = {
            CarAutoId: $("#CarAutoId").val(),
            CarName: $("#txtCarName").val(),
            Year: $("#txtYear").val(),
            Make: $("#txtMake").val(),
            Model: $("#txtModel").val(),
            VINNumber: $("#txtVINNumber").val(),
            TAGNumber: $("#txtTAGNumber").val(),
            InspectionExpDate: $("#txtInspectionExpDate").val(),
            StartMilage: $("#txtStartMilage").val(),
            Description: $("#txtDescription").val(),
            Status: $("#ddlStatus").val()
        }
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/WCarDetails.asmx/update",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == "true") {
                    swal("", "Car details updated successfully.", "success");
                }
                else if(result.d=="false")
                {
                    swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                }
                else if (result.d == "Unauthorized access.") {
                    location.href="/"
                }
                else
                {
                    swal("Error!", result.d, "error");
                }
            },
            error: function (result) {
                swal("Error!", result.d, "error");
            },
            failure: function (result) {
                swal("Error!", result.d, "error");
            }
        });
    } else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
};

/*--------------------------------------------------------Delete Car ----------------------------------------------------------*/
function deleteCar(CarId) {
    if (confirm('Are your sure you want to delete this entry?')) {

        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/CarMaster.asmx/deleteCar",
            data: "{'CarId':'" + CarId + "'}",
            contentType: "application/json;charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == "true") {
                    swal("", "Car details deleted successfully.", "success");
                }
                else if (result.d == "Exists") {
                    swal("", "Oops! Something went wrong.Please try later.", "success");
                }
            },
            error: function (result) {
                swal("Error!", "Car already assigned to product.", "error");            },
            failure: function (result) {
                swal("Error!", result.d, "error");
            }
        })
    }
}
/*-----------------------------------------------------------Reset------------------------------------------------------------------*/
function resetCar() {
    $("#btnUpdate").hide();
    $("#btnCancel").hide();
    $("#btnSave").show();
    $("#btnReset").show();
    $('input[type="text"]').val('');
    $('textarea').val('');
    $('option:selected').each(function () {
        $(this).attr('selected', false);
    });
    $('select option:first-child').each(function () {
        $(this).attr('selected', 'selected');
    });
    $('.req').removeClass('border-warning');
}