﻿$(document).ready(function () {
    $('#txtFromDate').pickadate({
        min: new Date("01/01/2019"),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtToDate').pickadate({
        min: new Date("01/01/2019"),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    var d = new Date();
    var month = d.getMonth() + 1;
    if (month.toString().length == 1) {
        month = '0' + month;
    }
    var day = d.getDate();
    if (day.toString().length == 1) {
        day = '0' + day;
    }
    var year = d.getFullYear();
    $("#txtToDate").val(month + '/' + day + '/' + year);
    $("#txtFromDate").val(month + '/' + day + '/' + year);


    bindddlList(); $('#EmptyTable').show();
})

function setdatevalidation(val) {
    var fdate = $("#txtFromDate").val();
    var tdate = $("#txtToDate").val();
    if (val == 1) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtToDate").val(fdate);
        }
    }
    else if (val == 2) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtFromDate").val(tdate);
        }
    }
}
function checkRequiredFieldState() {
    var boolcheck = true;
    $('.req').each(function () {
        if ($(this).val() == '') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });
    return boolcheck;
}
function Pagevalue(e) {
    getDetail(parseInt($(e).attr('page')));
};
$("#ddlPaging").change(function () {
    getDetail(1);
})

function bindddlList() {
    $.ajax({
        type: "POST",
        url: "WebAPI/ErrorTicketLog.asmx/ddl",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $("#fade").hide();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var EmpType = $(xmldoc).find("Table");
            var AllEmp = $(xmldoc).find("Table1");

            $("#ddlCustomerType option:not(:first)").remove();
            $.each(EmpType, function () {
                $("#ddlCustomerType").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("TypeName").text() + "</option>");
            });

            //$("#ddlStatus option:not(:first)").remove();
            //$.each(StateDetail, function () {
            //    $("#ddlStatus").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("StateName").text() + "</option>");
            //});

            $("#ddlempid option:not(:first)").remove();
            $.each(AllEmp, function () {
                $("#ddlempid").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("fullname").text() + "</option>");
            });

            $("#ddlCustomerType").select2()

           // $("#ddlStatus").select2()

            $("#ddlempid").select2()

        },
        error: function (result) {
            swal("Error!", "Oops! Something went wrong !!", "error");
        },
        failure: function (result) {
            swal("Error!", "Oops! Something went wrong !!", "error");
        }
    });
}


function getDetail(pageIndex) {
    var data = {
        UserType: $('#ddlCustomerType').val()||'0',
        ClientStatus: $('#ddlStatus').val(),
        DevoloperStatus: $('#ddlDeveloperStatus').val(),
        AllType: $('#ddltype').val(),
        TicketId: $('#txtTicketID').val().trim(),
        Subject: $('#txtSearchbysubject').val().trim(),
        FromDate: $('#txtFromDate').val(),
        ToDate: $('#txtToDate').val(),
        Employee: $('#ddlempid').val()||'0',
        pageSize:$("#ddlPaging").val(),
        pageIndex: pageIndex
    }
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/ErrorTicketLog.asmx/GetErrorTicketList",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: onSuccessOfCategory,
        error: function (response) {
            swal("Error!", "Oops! Something went wrong !!", "error");
        }
    });
}
function onSuccessOfCategory(response) {
    var xmldoc = $.parseXML(response.d);
    var ErrorTicketlist = $(xmldoc).find('Table');

    if (ErrorTicketlist.length > 0) {
        $('#EmptyTable').hide();
        $('#tblErrorTicketLog tbody tr').remove();
        var row = $('#tblErrorTicketLog thead tr').clone(true);
        $.each(ErrorTicketlist, function () {

            $(".TicketId", row).html("<a href='/admin/ErrorTicketResponseForm.aspx?TicketAutoId=" + $(this).find("TicketID").text() + "'>" + $(this).find("TicketID").text() + "</a>")            
            $(".TicketDate", row).html($(this).find("TicketDate").text());
            $(".Fullname", row).html($(this).find("Fullname").text());            
            $(".Priority", row).html($(this).find("Priority").text());
            $(".Type", row).html($(this).find("Type").text());
            if ($(this).find("Status").text() == "Open") {
                $(".ClentStatus", row).html("<span class='badge badge badge-pill badge-success mr-2'>" + $(this).find("Status").text() + "</span>")               
            }
            else
            {
                $(".ClentStatus", row).html("<span class='badge badge badge-pill badge-danger mr-2'>" + $(this).find("Status").text() + "</span>")
            }
            if ($(this).find("DeveloperStatus").text() == "Open") {
                $(".DeveloperStatus", row).html("<span class='badge badge badge-pill badge-success mr-2'>" + $(this).find("DeveloperStatus").text() + "</span>");               
            }
            else if ($(this).find("DeveloperStatus").text() == "Under Process")
            {
                $(".DeveloperStatus", row).html("<span class='badge badge badge-pill badge-primary mr-2'>" + $(this).find("DeveloperStatus").text() + "</span>");
            }
            else {
                $(".DeveloperStatus", row).html("<span class='badge badge badge-pill badge-danger mr-2'>" + $(this).find("DeveloperStatus").text() + "</span>");
            }
            
            $(".TicketCloseDate", row).html($(this).find("TicketCloseDate").text());
            $(".Subject", row).html($(this).find("Subject").text());
            $(".Description", row).html($(this).find("Description").text());
            $("#tblErrorTicketLog tbody").append(row);
            row = $("#tblErrorTicketLog tbody tr:last-child").clone(true);
        });
    }
    else {
        $('#tblErrorTicketLog tbody tr').remove();
        $('#EmptyTable').show();
    }
    var pager = $(xmldoc).find("Table1");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
}

$("#btnExportToExcel").click(function () {  
    if ($('#tblErrorTicketLog tbody tr').length > 0) {
    var data = {
        UserType: $('#ddlCustomerType').val() || '0',
        ClientStatus: $('#ddlStatus').val(),
        DevoloperStatus: $('#ddlDeveloperStatus').val(),
        AllType: $('#ddltype').val(),
        TicketId: $('#txtTicketID').val().trim(),
        Subject: $('#txtSearchbysubject').val().trim(),
        FromDate: $('#txtFromDate').val(),
        ToDate: $('#txtToDate').val(),
        Employee: $('#ddlempid').val() || '0',       
    };
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/ErrorTicketLog.asmx/GetErrorTicketListExport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $("#fade").hide();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var TicketListExport = $(xmldoc).find("Table");
            $("#tblErrorTicketLogExport tbody tr").remove();
            if (TicketListExport.length > 0) {             
                $('#tblErrorTicketLogExport tbody tr').remove();
                var row1 = $('#tblErrorTicketLogExport thead tr').clone(true);
                $.each(TicketListExport, function () {                  
                    $(".TicketId", row1).html($(this).find("TicketID").text());
                    $(".TicketDate", row1).html($(this).find("TicketDate").text());
                    $(".Fullname", row1).html($(this).find("Fullname").text());
                    $(".Priority", row1).html($(this).find("Priority").text());
                    $(".Type", row1).html($(this).find("Type").text());
                    $(".ClentStatus", row1).html($(this).find("Status").text());
                    $(".DeveloperStatus", row1).html($(this).find("DeveloperStatus").text());
                    $(".TicketCloseDate", row1).html($(this).find("TicketCloseDate").text());
                    $(".Subject", row1).html($(this).find("Subject").text());
                    $(".Description", row1).html($(this).find("Description").text());
                    $("#tblErrorTicketLogExport tbody").append(row1);
                    row1 = $("#tblErrorTicketLogExport tbody tr:last-child").clone(true);
                });
            }           
            $("#tblErrorTicketLogExport").table2excel({
                exclude: ".noExl",
                name: "Excel Document Name",
                filename: "Ticket List Report ",
                fileext: ".xls",
                exclude_img: true,
                exclude_links: true,
                exclude_inputs: true
            });
        },
        error: function (result) {
            swal("Error!", "Oops! Something went wrong !!", "error");
        },
        failure: function (result) {
            swal("Error!", "Oops! Something went wrong !!", "error");
        }
    });
}
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
});