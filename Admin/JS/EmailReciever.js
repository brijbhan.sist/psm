﻿$(document).ready(function () {
    bindAgent();
    getEmailReciever(1);
});
function Pagevalue(e) {
    getEmailReciever(parseInt($(e).attr("page")));
};

function bindAgent() {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/EmailRecieverMaster.asmx/bindAgent",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        asyn: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);

            var agent = $(xmldoc).find("Table");

            $("#ddlempid option:not(:first)").remove();
            $.each(agent, function () {
                $("#ddlempid").append($("<option value='" + $(this).find("EMPId").text() + "'>" + $(this).find("EmployeeName").text() + "</option>"));
            });
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
};

function bindAgentbyId() {
     
    var  autoid=$("#ddlempid").val()
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/EmailRecieverMaster.asmx/bindAgentById",
        data: "{'autoid':'" + autoid + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        asyn: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
             
            var xmldoc = $.parseXML(response.d);
            var agent = $(xmldoc).find("Table");
            $.each(agent, function () {
                $("#txtempname").val($(agent).find("EmployeeName").text());
                $("#txtEmailID").val($(agent).find("Email").text());
            });
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
};

function getEmailReciever() {
     
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/EmailRecieverMaster.asmx/getEmailReciever",
        data: "",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var Emp = $(xmldoc).find("Table");
            var i=0;
            if (Emp.length > 0) {
                var i = 1;
                $("#EmptyTable").hide();
                $("#tblEmailRecieverList tbody tr").remove();
                var row = $("#tblEmailRecieverList thead tr").clone(true);
                $.each(Emp, function () {
                   
                    $(".Action", row).html("<a title='Edit' href='javascript:;'><span class='la la-edit' onclick='EditReciever(\"" + $(this).find("id").text() + "\")'></span></a> &nbsp;<a title='Edit' href='javascript:;'><span class='la la-remove' onclick='deleterecord(\"" + $(this).find("id").text() + "\")'></span></a>");
                  
                    $(".EmpName", row).html($(this).find("EmployeeName").text());
                    $(".EmailID", row).html($(this).find("EmailID").text());
                    $(".Category", row).html($(this).find("Category").text());
                    $(".SN", row).html(i);
                    $("#tblEmailRecieverList tbody").append(row);
                    row = $("#tblEmailRecieverList tbody tr:last-child").clone(true);
                    i++;
                });
            }
            else {
                $("#tblEmailRecieverList tbody tr").remove();
                $("#EmptyTable").show();
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

$("#btnSave").click(function () {
     
    if (checkRequiredField()) {

        var data = {
            EmployeeID: $("#ddlempid").val(),
            EmployeeName: $("#txtempname").val(),
            EmailID: $("#txtEmailID").val(),
            Category: $("#ddlcategory").val()
        }
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/EmailRecieverMaster.asmx/insertEmailReciever",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == 'true') {                   
                    swal("", "Receiver details saved successfully.", "success");
                    getEmailReciever(1);
                    resetData();
                }
                else if(result.d=="Unauthorized Access") {
                    location.href="/";
                }
                else {
                    swal("warning!", result.d, "warning");
                }
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
})

function EditReciever(autoid)
{
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/EmailRecieverMaster.asmx/getEmailRecieverByID",
        data: "{'autoid':'" + autoid + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        asyn: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var agent = $(xmldoc).find("Table");
            $.each(agent, function () {
                bindAgent();
                $("#hfautoID").val($(agent).find("id").text())
                $("#ddlempid").val($(agent).find("EmployeeID").text());
                $("#txtempname").val($(agent).find("EmployeeName").text());
                $("#txtEmailID").val($(agent).find("EmailID").text());
                $("#ddlcategory").val($(agent).find("Category").text());
                $("#btnUpdate").show();
                $("#btnSave").hide();
                $("#ddlempid").prop('disabled', 'true');                
                $("#btnClear").addClass('btn-warning');
                $("#btnClear").html('Cancel');
            });
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

$("#btnUpdate").click(function () {
     
    if (checkRequiredField()) {

        var data = {
            EmployeeID: $("#hfautoID").val(),
            EmployeeName: $("#txtempname").val(),
            EmailID: $("#txtEmailID").val(),
            Category: $("#ddlcategory").val()
        }
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/EmailRecieverMaster.asmx/updateEmailReciever",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == 'true') {
                    swal("", "Receiver details updated successfully.", "success");
                    getEmailReciever(1);
                    resetData();
                    $("#btnUpdate").hide();
                    $("#btnSave").show();
                    $("#ddlempid").removeAttr('disabled');

                } else if(result.d =='Unauthorized Access') {
                    location.href="/";
                }
                else  {
                    swal("", result.d, "error");
                }
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
})

function deleterecord(autoid) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this receiver",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: false,
            },
            confirm: {
                text: "Yes, delete it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(isConfirm => {
        if (isConfirm) {
             DeleteReciever(autoid);
            
} else {
             swal("", "Your data is safe.:)", "error");
}
})
}

function DeleteReciever(autoid)
{
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/EmailRecieverMaster.asmx/DeleteEmailReciever",
        data: "{'autoid':'" + autoid + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if(response.d=="Unauthorized Access")
            {
                location.href="/";
            }
            else if(response.d!=="false")
            {                
                getEmailReciever();
                swal("", "Receiver details deleted successfully.", "success");
            }
            else
            {
                swal("",response.d,"error");
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function resetData() {
     
    $("#btnClear").removeClass('btn-warning');
    $("#btnClear").html('Reset');
    $("#ddlempid").val(0);
    $("#txtempname").val('');
    $("#txtEmailID").val('');
    $("#ddlcategory").val(0);

    $("#ddlempid").removeClass('border-warning');
    $("#txtempname").removeClass('border-warning');
    $("#txtEmailID").removeClass('border-warning');
    $("#ddlcategory").removeClass('border-warning');

    $("#btnUpdate").hide();
    $("#btnSave").show();
    $("#ddlempid").removeAttr('disabled');
}