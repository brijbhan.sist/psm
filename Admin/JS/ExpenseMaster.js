﻿$(document).ready(function () {
    $('.date').pickadate({
        min: new Date("01/01/2019"),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    Binddropdown();
    PaymentType();
    $('#txtExpenseAmount').bind('copy paste cut', function (e) {
        e.preventDefault();        
    });
    $('#txtamount').bind('copy paste cut', function (e) {
        e.preventDefault();
    });
    var d = new Date();
    var month = d.getMonth() + 1;
    if (month.toString().length == 1) {
        month = '0' + month;
    }
    var day = d.getDate();
    if (day.toString().length == 1) {
        day = '0' + day;
    }
    var year = d.getFullYear();
    $("#txtFromDate").val(month + '/' + day + '/' + year);
    $("#txtToDate").val(month + '/' + day + '/' + year);
    getExpenseList(1);
})
function setdatevalidation(val) {
    var fdate = $("#txtFromDate").val();
    var tdate = $("#txtToDate").val();
    if (val == 1) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtToDate").val(fdate);
        }
    }
    else if (val == 2) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtFromDate").val(tdate);
        }
    }
}
function Pagevalue(e) {
    getExpenseList(parseInt($(e).attr("page")));
};
function openExpense() {
    $("#ExpenseDetails").modal('show'); 
    $("#txtamount").val('0.00');
    $("#TotalexpenseAmount").html('0.00');  
    $("#btnSave").show();
    $("#btnUpdate").hide();
    var d = new Date();
    CurrentDate = (d.getMonth() + 1) + "/" + d.getDate() + "/" + d.getFullYear();
    $("#txtExpenseDate").val(CurrentDate);
    $('#tblExpensivCurrency tbody tr .TotalCount input').val('');
    $('#tblExpensivCurrency tbody tr .Amount').html('0.00');
    $("#txtExpenseAmount").val('0.00');  
}

function amount() {
    if ($("#txtamount").val() == '') {
        $("#txtamount").val('0.00');
    }
}
function amount1() {
    if ($("#txtamount").val() != '') {
        cal = parseFloat($("#txtamount").val())
        $("#txtamount").val(cal.toFixed(2));
    }
    
}

function Binddropdown() {

    $.ajax({
        type: "POST",
        url: "ExpenseMaster.aspx/Binddropdown",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: false,
        cache: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var getData = $.parseJSON(response.d);


                $("#ddlPaymentSource option:not(:first)").remove();
                $.each(getData[0].PaymentTypeMaster, function (index, item) {

                    $("#ddlPaymentSource").append("<option value='" + item.AutoId + "'>" + item.Type + "</option>");
                }); 
                $("#ddlPaymentSource").select2();
                $("#ddlPaymentSource").select2({ width: '100%' });   

                $("#ddlPaymentType option:not(:first)").remove();
                $.each(getData[0].PaymentMode, function (index, item) {

                    $("#ddlPaymentType").append("<option value='" + item.AutoID + "'>" + item.PaymentMode + "</option>");
                }); 
                $("#ddlPaymentType").select2();
                $("#ddlPaymentType").select2({ width: '100%' });   

                $("#ddlCategory option:not(:first)").remove();          
                $.each(getData[0].category, function (index, item) {     
                    $("#ddlCategory").append("<option value='" + item.AutoId + "'>" + item.CategoryName + "</option>");
                });  
                $("#ddlCategory").select2();
                $("#ddlCategory").select2({ width: '100%' });   
                $("#ddlSearchCategory option:not(:first)").remove();
                $.each(getData[0].category, function (index, item) {
                    $("#ddlSearchCategory").append("<option value='" + item.AutoId + "'>" + item.CategoryName + "</option>");
                });
                $("#ddlSearchCategory").select2();

            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function Save() {
    var XmlExpensivecurrency = '';
    if ($("#ddlPaymentType").val() == '1') {
        $("#tblExpensivCurrency tbody tr").each(function () {
            if ($(this).find('.TotalCount input').val() != '') {
                XmlExpensivecurrency += '<XmlExpensivecurrency>';
                XmlExpensivecurrency += '<AutoId><![CDATA[' + $(this).find('.AutoId').text() + ']]></AutoId>';
                XmlExpensivecurrency += '<Count><![CDATA[' + $(this).find('.TotalCount input').val() + ']]></Count>';
                XmlExpensivecurrency += '<TotalAmount><![CDATA[' + $(this).find('.Amount').text() + ']]></TotalAmount>';
                XmlExpensivecurrency += '</XmlExpensivecurrency>';
            }
        })
    }
    if (EcheckERequiredField()) {
        if ($("#txtExpenseAmount").val() == '0' || $("#txtExpenseAmount").val() == '0.00' || $("#txtExpenseAmount").val() == '0.0') {
            toastr.error('Please enter a valid expense amount.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            $("#txtExpenseAmount").addClass('border-warning');
        }
        else {
            var data = {
                ExpenseDate: $("#txtExpenseDate").val(),
                ExpenseAmount: $("#txtExpenseAmount").val(),
                PaymentTypeAutoId: $("#ddlPaymentType").val(),
                PaymentSource: $("#ddlPaymentSource").val() || 0,
                Category: $("#ddlCategory").val(),
                ExpenseDescription: $("#txtExpenseDescription").val(),
            }
            $.ajax({
                type: "POST",
                url: "/Admin/WebAPI/ExpenseMaster.asmx/AddExpense",
                data: JSON.stringify({ dataValue: JSON.stringify(data), XmlExpensiveCurrency: JSON.stringify(XmlExpensivecurrency) }),
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (result) {
                    debugger;
                    if (result.d == "SessionExpired") {
                        location.href = "/";
                    }
                    else if (result.d != 'false') {
                        swal("", "Expense saved Successfully.", "success");
                        $("#ExpenseDetails").modal('hide');
                        textClear();
                        getExpenseList(1);
                    } else {
                        swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                    }
                },
                error: function (result) {
                    swal("Error!", result.d, "error");
                },
                failure: function (result) {
                    swal("Error!", result.d, "error");
                }
            });
        }
    } else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
function Update() {
    var XmlExpensivecurrency = '';
    if ($("#ddlPaymentType").val() == '1') {
        $("#tblExpensivCurrency tbody tr").each(function () {
            if ($(this).find('.TotalCount input').val() != '') {
                XmlExpensivecurrency += '<XmlExpensivecurrency>';
                XmlExpensivecurrency += '<AutoId><![CDATA[' + $(this).find('.AutoId').text() + ']]></AutoId>';
                XmlExpensivecurrency += '<Count><![CDATA[' + $(this).find('.TotalCount input').val() + ']]></Count>';
                XmlExpensivecurrency += '<TotalAmount><![CDATA[' + $(this).find('.Amount').text() + ']]></TotalAmount>';
                XmlExpensivecurrency += '</XmlExpensivecurrency>';
            }
        })
    }
    if (EcheckERequiredField()) {
        if ($("#txtExpenseAmount").val() == '0' || $("#txtExpenseAmount").val() == '0.00' || $("#txtExpenseAmount").val() == '0.0') {
            toastr.error('Please enter a valid expense amount.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        }
        else {
            var data = {
                AutoId: $("#hfAutoid").val(),
                ExpenseDate: $("#txtExpenseDate").val(),
                ExpenseAmount: $("#txtExpenseAmount").val(),
                Category: $("#ddlCategory").val(),
                PaymentSource: $("#ddlPaymentSource").val() || 0,
                PaymentTypeAutoId: $("#ddlPaymentType").val(),
                ExpenseDescription: $("#txtExpenseDescription").val(),
            }
            $.ajax({
                type: "POST",
                url: "/Admin/WebAPI/ExpenseMaster.asmx/UpdateExpense",
                data: JSON.stringify({ dataValue: JSON.stringify(data), XmlExpensiveCurrency: JSON.stringify(XmlExpensivecurrency) }),
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (result) {
                    if (result.d == "SessionExpired") {
                        location.href = "/";
                    }
                    else if (result.d != 'false') {
                        swal("", "Expense updated successfully.", "success");
                        $("#btnSave").show();
                        $("#btnUpdate").hide();
                        $("#ExpenseDetails").modal('hide');
                        textClear();
                        getExpenseList(1);
                    } else {
                        swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                    }
                },
                error: function (result) {
                    swal("Error!", result.d, "error");
                },
                failure: function (result) {
                    swal("Error!", result.d, "error");
                }
            });
        }
    } else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}


function getExpenseList(PageIndex) {

    var data = {
        FromDate: $("#txtFromDate").val(),
        ToDate: $("#txtToDate").val(),
        SearchBy: $("#ddlSearchBy").val(),
        Category: $("#ddlSearchCategory").val(),
        ExpenseAmount: $("#txtamount").val(),
        PageIndex: PageIndex,
        PageSize: $("#ddlPageSize").val(),
    };
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/ExpenseMaster.asmx/GetExpense",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successgetExpenseList,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function successgetExpenseList(response) {
    debugger;
    if (response.d == "SessionExpired") {
        location.href = "/";
    }
    else {
        var xmldoc = $.parseXML(response.d);
        var ExpList = $(xmldoc).find("Table1");
        $("#tblExpenseList tbody tr").remove();
        var row = $("#tblExpenseList thead tr").clone(true);
        if (ExpList.length > 0) {
            $("#EmptyTable").hide();
            $.each(ExpList, function (index) {
                $(".Date", row).text($(this).find("ExpenseDate").text());
                $(".Amount", row).text($(this).find("ExpenseAmount").text());
                $(".PaymentMode", row).text($(this).find("PaymentMode").text());
                $(".PaymentSource", row).text($(this).find("PaymentSource").text());
                $(".Description", row).text($(this).find("ExpenseDescription").text());
                $(".Category", row).text($(this).find("Category").text());
                $(".Action", row).html("<a href='#'><span class='la la-edit' onclick='EditExpense(\"" + $(this).find("AutoId").text() + "\") '></span></a>&nbsp;<a href='#'><span class='la la-remove' onclick='deleteItemrecord(\"" + $(this).find("AutoId").text() + "\") '></span></a>");
                $("#tblExpenseList tbody").append(row);
                row = $("#tblExpenseList tbody tr:last").clone(true);
            });



        } else {
            $("#tblExpenseList").show();
            $("#EmptyTable").show();
        }
        var pager = $(xmldoc).find("Table");
        $(".Pager").ASPSnippets_Pager({
            ActiveCssClass: "current",
            PagerCssClass: "pager",
            PageIndex: parseInt(pager.find("PageIndex").text()),
            PageSize: parseInt(pager.find("PageSize").text()),
            RecordCount: parseInt(pager.find("RecordCount").text())
        });
        if ($("#ddlPageSize").val() == '0') {
            $(".Pager").hide();
        }
        else {
            $(".Pager").show();
        }

    }
}
function deleteItemrecord(AutoId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete Expense.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, Cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: false,
            },
            confirm: {
                text: "Yes, Delete it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            DeleteExpense(AutoId);

        } else {
            swal("", "Your expense is safe.", "error");
        }
    })
}
function DeleteExpense(AutoId) {

    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/ExpenseMaster.asmx/DeleteExpense",
        data: "{'AutoId':'" + AutoId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (result) {
            swal("", "Expense deleted successfully.", "success");
            getExpenseList(1);
        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function EditExpense(AutoId) {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/ExpenseMaster.asmx/EditExpense",
        data: "{'AutoId':'" + AutoId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var ExpList = $(xmldoc).find("Table");
            if (ExpList.length > 0) {
                $("#hfAutoid").val($(ExpList).find("AutoId").text())
                $("#txtExpenseDate").val($(ExpList).find("ExpenseDate").text());
                $("#txtExpenseAmount").val($(ExpList).find("ExpenseAmount").text());
                $("#txtExpenseDescription").val($(ExpList).find("ExpenseDescription").text());
                $("#ddlPaymentType").val($(ExpList).find("PaymentMode").text()).change();               
                $("#ddlPaymentSource").val($(ExpList).find("PaymentSource").text()).change();             
                $("#ddlCategory").val($(ExpList).find("Category").text()).change();
                $("#ExpenseDetails").modal('show');
                $("#TotalexpenseAmount").html($(ExpList).find("ExpenseAmount").text());
                $("#btnSave").hide();
                $("#btnUpdate").show();
            }
            var ExpensiveCurrency = $(xmldoc).find("Table1");

            $("#tblExpensivCurrency tbody tr").remove();
            var row = $("#tblExpensivCurrency thead tr").clone(true);
            if (ExpensiveCurrency.length > 0) {
                $.each(ExpensiveCurrency, function (index) {

                    $(".AutoId", row).html($(this).find("AutoId").text());
                    $(".CurrencyValue", row).html($(this).find("CurrencyValue").text());
                    $(".CurrencyName", row).html($(this).find("CurrencyName").text());
                    $(".TotalCount", row).html('<input type="text" class="text-center form-control input-sm border-primary" style="width:100%" maxlength="5" onchange="ExpensiveCal(this)" onkeypress="return isNumberKey(event)" value=' + $(this).find("TotalCount").text() + '></input>');
                    $(".Amount", row).html($(this).find("TotalAmount").text());
                    $("#tblExpensivCurrency tbody").append(row);
                    row = $("#tblExpensivCurrency tbody tr:last").clone(true);
                });
            }
        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function textClear() {
    $("#ExpenseDetails").modal('hide');
    $("#hfAutoid").val('');
    $("#txtExpenseDate").val('');
    $("#txtExpenseAmount").val('');
    $("#txtExpenseDescription").val('');
    $("#txtExpenseDate").removeClass("border-warning");
    $("#txtExpenseAmount").removeClass("border-warning");
    $("#txtExpenseDescription").removeClass("border-warning"); 
    $("#ddlCategory").val(0).change();
}
function EcheckERequiredField() {
    var boolcheck = true;
    $('.req').each(function () {
        if ($(this).val() == '' || parseInt($(this).val()) == 0) {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });

    $('.ddlreq').each(function () { 

        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == 'undefined' || $(this).val() == null || $(this).val() == 'Select') {
            boolcheck = false;
            $(this).closest('div').find('.select2-selection--single').attr('style', 'border:1px solid #FF9149  !important');
        } else {
            $(this).removeClass('border-warning');
            $(this).closest('div').find('.select2-selection--single').removeAttr('style');
        }
    });


    return boolcheck;
}

function ExpenseAmount() {
    if ($("#txtExpenseAmount").val() == '') {
        $("#txtExpenseAmount").val('0.00');
    }
}
function ExpenseAmount1() {
    if ($("#txtExpenseAmount").val() != '') {
        cal = parseFloat($("#txtExpenseAmount").val())
        $("#txtExpenseAmount").val(cal.toFixed(2));
    }

}

function PaymentType() {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/ExpenseMaster.asmx/bindPaymentType",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function (response) {
            debugger;
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                
                    var ExpensiveCurrency = $(xmldoc).find("Table");
                    $("#ddlProduct option:not(:first)").remove();
                    $("#tblExpensivCurrency tbody tr").remove();
                    var row = $("#tblExpensivCurrency thead tr").clone(true);
                    if (ExpensiveCurrency.length > 0) {
                        $("#EmptyTable").hide();
                        $.each(ExpensiveCurrency, function (index) {
                            $(".AutoId", row).html($(this).find("AutoId").text());
                            $(".CurrencyValue", row).html($(this).find("CurrencyValue").text());
                            $(".CurrencyName", row).html($(this).find("CurrencyName").text());
                            $(".TotalCount", row).html("<input type='text' class='text-center form-control input-sm border-primary' style='width:100%' maxlength='5' onchange='ExpensiveCal(this)' onkeypress='return isNumberKey(event)'/>");
                            $(".Amount", row).html('0.00');
                            $("#tblExpensivCurrency tbody").append(row);
                            row = $("#tblExpensivCurrency tbody tr:last").clone(true);
                        });
                    } 
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function ExpensiveCal(e) {

    var value = "0.00";
    var tr = $(e).closest('tr');
    if ($(e).val() == "") {
        value = 0 * (Number($(tr).find('.CurrencyValue').html()));
    } else {
        value = Number((parseFloat($(e).val()))) * (Number($(tr).find('.CurrencyValue').html()));
    }
    $(tr).find('.Amount').html(value.toFixed(2));
    var value = 0.00;
    $("#tblExpensivCurrency tbody tr").each(function () {
        value += parseFloat($(this).find('.Amount').html()) || 0;
    });
    $('#TotalexpenseAmount').html(parseFloat(value).toFixed(2));
    $("#txtExpenseAmount").val(parseFloat(value).toFixed(2));
}