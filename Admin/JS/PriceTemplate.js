﻿var getid = 0;
$(document).ready(function () {
    BindDropdowns();
    getid = getQueryString('PLAutoId');
    if (getid != null) {
        EditPriceLevel(getid);
        $("#HPriceLevelAutoId").val(getid);
        $("#btnCancel").show();
        $("#btnReset").hide();
    }

    BindProducts(1, getid);
    $("#btnSearch").click(function () {
        BindProducts(1, getid);
    });

});

var getQueryString = function (field, url) {
    var href = url ? url : window.location.href;
    var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
    var string = reg.exec(href);
    return string ? string[1] : null;
};

function Pagevalue(e) {
    if ($(e).closest(".Pager").attr('id') == 'ProductPager') {
        var getid = getQueryString('PLAutoId');
        BindProducts(parseInt($(e).attr("page")), getid);
    }
    if ($(e).closest(".Pager").attr('id') == 'LogPager') {
        ShowProductLogAll(parseInt($(e).attr("page")));
    }
};

function BindDropdowns() {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/PriceTemplate.asmx/BindDropdowns",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },

        async: false,
        success: function (response) {
            if (response.d != 'Session Expired') {
                var xmldoc = $.parseXML(response.d);
                var statusList = $(xmldoc).find("Table");
                var category = $(xmldoc).find("Table1");
                var CustomerType = $(xmldoc).find("Table2");

                $("#ddlStatus option").remove();
                $.each(statusList, function () {
                    $("#ddlStatus").append("<option value=" + $(this).find("AutoId").text() + ">" + $(this).find("StatusType").text() + "</option>");
                });

                $("#ddlSCategory option:not(:first)").remove();
                $.each(category, function () {
                    $("#ddlSCategory").append("<option value=" + $(this).find("AutoId").text() + ">" + $(this).find("CategoryName").text() + "</option>");
                });
                $("#ddlCustomerType option:not(:first)").remove();
                $.each(CustomerType, function () {
                    $("#ddlCustomerType").append("<option value=" + $(this).find("AutoId").text() + ">" + $(this).find("CustomerType").text() + "</option>");
                });

            } else {
                location.href = '/';
            }
        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function cancelUpdate() {
    location.href = '/Admin/PriceLevelList.aspx';
}
function BindSubCategory() {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/PriceTemplate.asmx/BindSubcategory",
        data: "{'CategoryAutoId':'" + parseInt($("#ddlSCategory").val()) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            if (response.d != 'Session Expired') {
                var xmldoc = $.parseXML(response.d);
                var subCat = $(xmldoc).find("Table");

                $("#ddlSSubcategory option:not(:first)").remove();
                $.each(subCat, function () {
                    $("#ddlSSubcategory").append("<option value=" + $(this).find("AutoId").text() + ">" + $(this).find("SubcategoryName").text() + "</option>");
                });

            } else {
                location.href = '/';
            }
        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
$("#ddlPageSize").change(function () {
    BindProducts(1, $("#HPriceLevelAutoId").val());
});
function BindProducts(pageIndex, priceLevelAutoId) {
    var data = {
        PriceLevelAutoId: parseInt(priceLevelAutoId),
        CustomerType: parseInt($('#ddlCustomerType').val()),
        CategoryAutoId: parseInt($("#ddlSCategory").val()),
        SubcategoryAutoId: parseInt($("#ddlSSubcategory").val()),
        ProductName: $("#txtSProductName").val().trim(),
        Productid: $("#txtSProductId").val().trim(),
        PageSize: $("#ddlPageSize").val(),
        PageIndex: pageIndex,
    };

    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/PriceTemplate.asmx/getProductList",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            if (response.d != 'Session Expired') {
                var xmldoc = $.parseXML(response.d);
                var productList = $(xmldoc).find("Table1");

                $("#tblProductList tbody tr").remove();
                var len = productList.length;
                var count = 0;
                var row = $("#tblProductList thead tr").clone(true);
                if (productList.length > 0) {
                    $("#EmptyTable").hide();
                    $.each(productList, function (index) {
                        if (priceLevelAutoId != null && priceLevelAutoId != "") {
                            if ($(this).find("CHK").text() == '1') {
                                $(".Select", row).find('.selectChkBox').attr({ "checked": true, "disabled": false });
                                $(".CustomPrice", row).html("<input type='text' class='form-control input-sm text-right' style='width:100px;display:inline;' onchange='SavingProductPrice(" + $(this).find("ProductAutoId").text() + "," + $(this).find("UnitAutoId").text() + ",this)' onkeypress='return isNumberDecimalKey(event,this)' value='" + $(this).find("CustomPrice").text() + "'/><br /><span class='msg'></span>");
                                count = count + 1;
                            } else {
                                $(".Select", row).find('.selectChkBox').attr({ "checked": false, "disabled": false });
                                $(".CustomPrice", row).html("<input type='text' class='form-control input-sm text-right' style='width:100px;display:inline;' onchange='SavingProductPrice(" + $(this).find("ProductAutoId").text() + "," + $(this).find("UnitAutoId").text() + ",this)' onkeypress='return isNumberDecimalKey(event,this)' /><br /><span class='msg'></span>");

                            }
                        } else {
                            $(".CustomPrice", row).html("<input type='text' class='form-control input-sm' style='width:100px;display:inline;' disabled='disabled' />");
                        }
                        $(".ProductId", row).html("<span productautoid=" + $(this).find("ProductAutoId").text() + ">" + $(this).find("ProductId").text() + "</span>");
                        $(".ProductName", row).text($(this).find("ProductName").text());
                        $(".UnitType", row).html("<span unitautoid=" + $(this).find("UnitAutoId").text() + ">" + $(this).find("UnitType").text() + "</span>");
                        $(".MinPrice", row).text($(this).find("MinPrice").text());
                        $(".BasePrice", row).text($(this).find("Price").text());
                       
                            $(".Action", row).html("<a title='Log' href='#' onclick='ShowProductLog(" + $(this).find("ProductAutoId").text() + ",this)'><span class='la la-history' ></span></a>");
                        
                        $("#tblProductList tbody").append(row);
                        row = $("#tblProductList tbody tr:last-child").clone(true);
                    });
                    if ($("#HPriceLevelAutoId").val() == '' || $("#HPriceLevelAutoId").val() == null) {
                        $(".Action").hide();
                    }
                } else {
                    $("#EmptyTable").show();
                }
                if (len == count) {
                    $("#allchk").prop('checked', true);
                }
                var pager = $(xmldoc).find("Table");
                $("#ProductPager").ASPSnippets_Pager({
                    ActiveCssClass: "current",
                    PagerCssClass: "pager",
                    PageIndex: parseInt(pager.find("PageIndex").text()),
                    PageSize: parseInt(pager.find("PageSize").text()),
                    RecordCount: parseInt(pager.find("RecordCount").text())
                });

                if (priceLevelAutoId != null) {
                    $("#tblProductList").find("input[type='checkbox']").attr("disabled", false);
                    checkMarkProduct(priceLevelAutoId);
                }
            } else {
                location.href = '/';
            }
        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}


function SavePriceLevel() {
    if (checkRequiredField()) {
        var data = {
            PriceLevelName: $("#txtPriceLevelName").val(),
            CustomerType: $("#ddlCustomerType").val(),
            Status: $("#ddlStatus").val()
        };

        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/PriceTemplate.asmx/SavePriceLevel",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d != 'Session Expired') {
                    if (response.d == 'Exists') {
                        swal("Error !", "Price Level already exist.", "error");
                    } else if (response.d == 'Error') {
                        swal("Error !", "Oops! Something went wrong.Please try later.", "error");
                    } else {
                        var xmldoc = $.parseXML(response.d);
                        var PriceLevelAutoId = $(xmldoc).find("Table").find("PriceLevelAutoId").text();
                        swal("", "Price Level details saved successfully.", "success"
                        ).then(function () {
                            window.location.href = window.location.href = "/Admin/PriceTemplate.aspx?PLAutoId=" + PriceLevelAutoId;
                        });
                    }

                } else {
                    location.href = '/';
                }
            },
            error: function (result) {
                swal("Error!", result.d, "error");
            },
            failure: function (result) {
                swal("Error!", result.d, "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
}

function EnablePrice(e) {
    if ($(e).is(":checked")) {
        $(e).closest("tr").find(".CustomPrice input").attr("disabled", false);
    } else {
        $(e).closest("tr").find(".CustomPrice input").attr("disabled", true);
    }
}

function EditPriceLevel(PriceLevelAutoId) {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/PriceTemplate.asmx/EditPriceLevel",
        data: "{'PriceLevelAutoId':'" + PriceLevelAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != 'Session Expired') {
                var xmldoc = $.parseXML(response.d);
                var template = $(xmldoc).find("Table");
                $("#txtPriceLevelName").val(template.find("PriceLevelName").text());
                $("#ddlCustomerType").val(template.find("CustomerType").text());
                $("#ddlStatus").val(template.find("Status").text());
                $("#ddlCustomerType").attr('disabled', true);
                $("#btnSave").hide();
                $("#btnUpdate").show();
            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}


function AddRemoveProduct(ProductAutoId, UnitAutoId, e) {
    if ($(e).is(":checked")) {
        AddProductInPriceLevel(ProductAutoId, UnitAutoId, e);
    } else {
        RemoveProductFromPriceLevel(ProductAutoId, UnitAutoId, e);
    }
}

function AddProductInPriceLevel(ProductAutoId, UnitAutoId, e) {
    var data = {
        PriceLevelAutoId: $("#HPriceLevelAutoId").val(),
        ProductAutoId: ProductAutoId,
        UnitAutoId: UnitAutoId
    };

    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/PriceTemplate.asmx/AddProduct",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != 'Session Expired') {
                EnablePrice(e);
            } else {
                location.href = '/';
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function RemoveProductFromPriceLevel(ProductAutoId, UnitAutoId, e) {
    var data = {
        PriceLevelAutoId: $("#HPriceLevelAutoId").val(),
        ProductAutoId: ProductAutoId,
        UnitAutoId: UnitAutoId
    };

    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/PriceTemplate.asmx/RemoveProduct",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != 'Session Expired') {
                EnablePrice(e);
            } else {
                location.href = '/';
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function checkMarkProduct(priceLevelAutoId) {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/PriceTemplate.asmx/checkMarkProduct",
        data: "{'priceLevelAutoId':'" + priceLevelAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != 'Session Expired') {
                var xmldoc = $.parseXML(response.d);
                var product = $(xmldoc).find("Table");
                var row = $("#tblProductList tbody tr");

                product.each(function () {
                    var PId = Number($(this).find("ProductAutoId").text());
                    var UId = Number($(this).find("UnitAutoId").text());
                    var customPrice = $(this).find("CustomPrice").text();
                    $.each(row, function () {
                        if ($(this).find(".ProductId span").attr("productautoid") == PId && $(this).find(".UnitType span").attr("unitautoid") == UId) {
                            $(this).find(".selectChkBox").prop("checked", true).end()
                                .find(".CustomPrice input").attr("disabled", false).val(customPrice);
                            return false;
                        }
                    });
                });
            } else {
                location.href = '/';
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function sortTable() {
    var table, rows, switching, i, x, y, shouldSwitch;
    table = document.getElementById("tblProductList");
    switching = true;

    while (switching) {
        switching = false;
        rows = table.getElementsByTagName("TR");
        for (i = 1; i < (rows.length - 1); i++) {
            shouldSwitch = false;
            x = rows[i].getElementsByTagName("TD")[0];
            y = rows[i + 1].getElementsByTagName("TD")[0];
            if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                shouldSwitch = true;
                break;
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
        }
    }
}

function SavingProductPrice(ProductAutoId, UnitAutoId, e) {
    var data = {
        PriceLevelAutoId: $("#HPriceLevelAutoId").val(),
        CustomerType: $("#ddlCustomerType").val(),
        ProductAutoId: ProductAutoId,
        UnitAutoId: UnitAutoId,
        CustomPrice: $(e).val()
    };
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/PriceTemplate.asmx/SavingProductPrice",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },

        success: function (response) {
            if (response.d == 'true') {
                swal("", "Custom price updated successfully.", "success");
            } else {
                swal("Error !", response.d, "error");
            }
            BindProducts(1, getid);
        },
        error: function (result) {
            swal("Error!", "Price cannot be below min price.", "error");

        },
        failure: function (result) {
            swal("Error!", result.d, "error");
        }
    });
}

function UpdatePriceLevel() {
    if (checkRequiredField()) {
        var data = {
            PriceLevelAutoId: $("#HPriceLevelAutoId").val(),
            CustomerType: $("#ddlCustomerType").val(),
            PriceLevelName: $("#txtPriceLevelName").val(),
            Status: $("#ddlStatus").val()
        };

        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/PriceTemplate.asmx/UpdatePriceLevel",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {

                if (response.d != 'Session Expired') {                    
                    if (response.d == 'Exists') {
                        swal("Error !", "Price Level already exist.", "error");
                    }
                    else if (response.d == 'Success') {
                        swal("", "Price Level details updated successfully.", "success");
                    }

                }
                else {
                    location.href = '/';
                }
            },
            error: function (result) {
                swal("Error!", result.d, "error");
            },
            failure: function (result) {
                swal("Error!", result.d, "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
}
function AllPriceSet() {
    var TypeAction = 0;
    if ($("#allchk").prop('checked') == true) {
        TypeAction = 1;
    } else {
        TypeAction = 0;
    }
    var data = {
        PriceLevelAutoId: $("#HPriceLevelAutoId").val(),
        ActionType: TypeAction
    }
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/PriceTemplate.asmx/AddAllProduct",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != 'Session Expired') {
                BindProducts(1, getid);
            } else {
                location.href = '/';
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}

function ShowProductLog(ProductAutoId, e) {
    debugger;
    $("#ProductAutoId").val(ProductAutoId);
    var ProductId = $(e).closest('tr').find('.ProductId').text(); 
    var ProductName = $(e).closest('tr').find('.ProductName').text(); 
    $("#pid").text("#"+ProductId);
    $("#pname").text(ProductName);
    ShowProductLogAll(1)
}

function ShowProductLogAll(pageIndex) {
    var data = {
        PriceLevelAutoId: $("#HPriceLevelAutoId").val(),
        ProductAutoId: $("#ProductAutoId").val(),
        PageSize: $("#ddlPageSizeLog").val(),
        PageIndex: pageIndex
    };

    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/PriceTemplate.asmx/ProductPriceLevelLog",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            console.log(response);
            var xmldoc = $.parseXML(response.d);
            var pricelevelLog = $(xmldoc).find("Table");
            $("#tblPriceLevelLog tbody tr").remove();
            var row = $("#tblPriceLevelLog thead tr").clone(true);
            if (pricelevelLog.length > 0) {
                $("#EmptyTable33").hide();
                $.each(pricelevelLog, function (index) {
                    $(".Unit", row).text($(this).find("Unit").text());
                    $(".CustomPrice", row).text($(this).find("CustomPrice").text());
                    $(".UpdateDate", row).text($(this).find("updateDate").text());
                    $(".UpdateBy", row).text($(this).find("UpdatedBy").text());
                    $("#tblPriceLevelLog tbody").append(row);
                    row = $("#tblPriceLevelLog tbody tr:last").clone(true);
                });
            }
            else {
                $('#EmptyTable33').show();
            }

            var pager = $(xmldoc).find("Table1");
            $("#LogPager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
            $("#ShowPODraftLog").modal('show');
        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}