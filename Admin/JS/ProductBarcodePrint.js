﻿$(document).ready(function () {
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    var getid = getQueryString('ProductId');
    if (getid != null) {
        GetProductBarcode(getid);
    }
});

/*-------------------------------------------------------------------------------------------------------------------------------*/
function GetProductBarcode(ProductAutoId) {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/ProductList.asmx/GetProductBarcode",
        data: "{'ProductAutoId':'" + ProductAutoId + "','check':'" + 1 + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('#fade').show();
        },
        async:false,
        complete: function () {
            $('#fade').hide();
        },
        success: function (response) {
            var html = ''; html1 = ''; var DefaultUnit = '';
            var xmldoc = $.parseXML(response.d);
            var Product = $(xmldoc).find("Table");
            var ProductBarcode = $(xmldoc).find("Table1");
            $(".ProductId").html('<h1 style="font-size: 55px;"><b>' + $(Product).find("ProductId").text() + '</b></h1>' + '<h2>' + $(Product).find("ProductName").text() + '<br/><br/>' + $(Product).find("Location").text() +'</h2>');
            DefaultUnit = $(Product).find("PackingAutoId").text();
            var i = 0;
            var value = "";
            var btype = "code128";
            var renderer = "css";

            var quietZone = false;

            var settings = {
                output: renderer,
            };

            var barcode = ProductBarcode.length;
            $.each(ProductBarcode, function () {
                var hr = ''; var heighttd = '';
                if (i > 0) {
                    hr = 'style="border-top: 1px solid;"';
                }
                if (barcode == 1)
                {

                }
                if (barcode == 1) {
                    heighttd = 'style="height: 350px;padding-top:10px;"';
                }
                else if (barcode == 2) {
                    heighttd = 'style="height: 175px;padding-top:10px;"';
                }
                else if (barcode == 3) {
                    heighttd = 'style="height: 116px;padding-top:10px;"';
                }
                value = { code: $(this).find("Barcode").text(), rect: true };
                $("#barcode").barcode(value, btype, settings);
                html1 += '<tr ' + hr + '><td class="text-center" ' + heighttd + '><div style="width:100%;display: table-cell;">' + $('#barcode').html() + '</div><div>' + $(this).find("Barcode").text() + '<br/><h2 class="header">' + $(this).find("UnitType").text() + ($(this).find("UnitAutoId").text() == DefaultUnit ? '*' : '') + '</h2></div></td></tr>';
                
                i++; 
            });
            $("#tblProduct1 tbody").append(html1);
            window.print();
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}