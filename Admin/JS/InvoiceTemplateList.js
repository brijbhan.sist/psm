﻿var row1 = "";
$(document).ready(function () {
    $('#txtSFromDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtSToDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    $("#txtSFromDate").val(month + '/' + day + '/' + year);
    $("#txtSToDate").val(month + '/' + day + '/' + year);
    getReport(1);
});

function Pagevalue(e) {
    getReport(parseInt($(e).attr("page")));
};

/*-------------------------------------------------------Get Page Tracker List----------------------------------------------------------*/
function getReport(PageIndex) {
    var data = {
        PageIndex: PageIndex,
        PageSize: $('#ddlPageSize').val(),
    };
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/InvoiceTemplateList.asmx/GetInvoiceTemplateList",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successgetReport,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function successgetReport(response) {

    if (response.d != "Session Expired") {
        if (response.d != "false") {
            var xmldoc = $.parseXML(response.d);
            var ReportData = $(xmldoc).find("Table2");
            $("#TableInvoiceTemplateList tbody tr").remove();
            var row = $("#TableInvoiceTemplateList thead tr").clone(true);
            if (ReportData.length > 0) {
                $("#EmptyTable").hide();
                var i = 0;
                $.each(ReportData, function (index) {
                    i++;
                    $(".sn", row).text(i);
                    $(".InvoiceTemplateName", row).text($(this).find("InvoiceTemplateName").text());
                    $(".Description", row).text($(this).find("InvoiceTemplateDescription").text());
                 
                    $("#TableInvoiceTemplateList tbody").append(row);
                    row = $("#TableInvoiceTemplateList tbody tr:last").clone(true);
                });
            }
            else {
                $("#EmptyTable").show();
            }


            var pager = $(xmldoc).find("Table");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
        }
    }
    else {
        location.href = "/";
    }
}



//function CreateTable(us) {

//    var image = $("#imgName").val();
//    var data = {
//        FromDate: $("#txtSFromDate").val(),
//        ToDate: $("#txtSToDate").val(),
//        User: $("#ddlUser").val(),
//        AppV: $("#ddlVersion").val(),
//        Method: $("#ddlMethods").val(),
//        PageIndex: 1,
//        PageSize: 0,
//    };
//    $.ajax({
//        type: "POST",
//        url: "/Admin/WebAPI/WApiRequestReport.asmx/GetApiLogData",
//        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
//        contentType: "application/json; charset=utf-8",
//        datatype: "json",
//        beforeSend: function () {
//            $.blockUI({
//                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
//                overlayCSS: {
//                    backgroundColor: '#FFF',
//                    opacity: 0.8,
//                    cursor: 'wait'
//                },
//                css: {
//                    border: 0,
//                    padding: 0,
//                    backgroundColor: 'transparent'
//                }
//            });
//        },
//        complete: function () {
//            $.unblockUI();
//        },
//        success: function (response) {
//            if (response.d != "Session Expired") {
//                if (response.d != "false") {
//                    var xmldoc = $.parseXML(response.d);
//                    var ReportData = $(xmldoc).find("Table2");
//                    $("#PrintTable tbody tr").remove();
//                    var row = $("#PrintTable thead tr:last-child").clone(true);
//                    if (ReportData.length > 0) {
//                        var i = 0;
//                        $.each(ReportData, function () {
//                            i++;
//                            $(".sn", row).text(i);
//                            $(".MethodName", row).text($(this).find("MethodName").text());
//                            $(".AccessToken", row).text($(this).find("AccessToken").text());
//                            $(".AppVersion", row).text($(this).find("AppVersion").text());
//                            $(".DeviceId", row).text($(this).find("DeviceId").text());
//                            $(".LatLong", row).text($(this).find("LatLong").text());
//                            $(".RouteName", row).text($(this).find("RouteName").text());
//                            $(".DeviceName", row).text($(this).find("DeviceName").text());
//                            $(".UserName", row).text($(this).find("UserName").text());
//                            $(".UTCTimeStamp", row).text($(this).find("UtcTimeStamp").text());
//                            $(".CreatedOn", row).html($(this).find("CreatedOn").text());
//                            $("#PrintTable tbody").append(row);
//                            row = $("#PrintTable tbody tr:last").clone(true);
//                        });
//                    }
//                    if (us == 1) {
                     
//                        var image = $("#imgName").val();
//                        var mywindow = window.open('', 'mywindow', 'height=400,width=600');
//                        mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
//                        $("#PrintDate").text("Print Date :" + (new Date()).format("MM/dd/yyyy hh:mm tt"));
//                        $("#PrintLogo").attr("src", "/Img/logo/" + image);
//                        if ($("#txtSFromDate").val() != "") {
//                            $("#DateRange").text("Date Range: " + $("#txtSFromDate").val() + " To " + $("#txtSToDate").val());
//                        }
//                        mywindow.document.write($(PrintTable1).clone().html());
//                        mywindow.document.write('</body></html>');
//                        setTimeout(function () {
//                            mywindow.print();
//                        }, 2000);

//                    }
//                    if (us == 2) {
//                        $("#PrintTable").table2excel({
//                            exclude: ".noExl",
//                            name: "Excel Document Name",
//                            filename: "APIRequestReport" + (new Date()).format("MM/dd/yyyy hh:mm tt"),
//                            fileext: ".xls",
//                            exclude_img: true,
//                            exclude_links: true,
//                            exclude_inputs: true
//                        });
//                    }
//                }
//            }
//            else {
//                location.href = "/";
//            }
//        }

//    });
//}

///*---------------Print Code------------------*/
//function PrintElem() {
//    if ($('#TableApiRequest tbody tr').length > 0) {
 
//        CreateTable(1);
//    }
//    else {
//        toastr.error('Please select atleast one Record in this list', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
//    }
//}
///*---------------Export To Excel---------------------------*/
//function Export() {
//    if ($('#TableApiRequest tbody tr').length > 0) {
//    CreateTable(2);
//    }
//    else {
//        toastr.error('Please select atleast one Record in this list', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
//    }
//}




