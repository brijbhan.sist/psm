﻿
$(document).ready(function () {
    bindDropdown();
    getAssignedModuleList();

});

function getAssignedModuleList() {
    debugger;
    var data = {
        searchLocation: $('#searchLocation').val(),
        //searchPage: $('#searchPage').val(),
        searchModule: $('#searchModule').val(),
        searchSubModule: $('#searchSubModule').val()
    };
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/AssignModule.asmx/getAssignedModuleList",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (result) {
            console.log(result);

            var xmldoc = $.parseXML(result.d);

            var modules = $(xmldoc).find('Table');
            $("#moduleList tbody tr").remove();
            var row = $("#moduleList thead tr").clone(true);

            if (modules.length > 0) {
                $.each(modules, function () {
                    var actionStr = $(this).find("PageAction").text().split("^").toString();

                    $(".action", row).html("<a href='#' title='Edit'><span class='ft-edit' onclick='editPage(this)' /></a>&nbsp;<a href='#' title='Delete'><span class='ft-x' onclick= 'removeAssignedModule(\"" + $(this).find("AssignedPageAutoId").text() + "\")' /></a>");
                    // hidden data
                    $(".AutoId", row).text($(this).find("PageUrl").text());
                    $(".HiddenStatus", row).text($(this).find("Status").text());
                    $(".HiddenModule", row).text($(this).find("ModuleAutoId").text());
                    $(".HiddenSubModule", row).text($(this).find("SubModuleAutoId").text());
                    $(".HiddenPage", row).text($(this).find("PageUrl").text());
                    $(".HiddenLocation", row).text($(this).find("AssignedLocation").text());

                    // end
                    $(".Module", row).text($(this).find("ModuleName").text());
                    $(".SubModule", row).text($(this).find("PageName").text());
                    $(".PageName", row).text($(this).find("PageUrl").text());
                    //$(".PageUrl", row).text($(this).find("PageUrl").text());
                    $(".Location", row).text($(this).find("Location").text());
                    if ($(this).find("Status").text() == '1') {
                        $(".Status", row).html("<span class='badge badge badge-pill badge-success'>Active</span>");
                        $(".HiddenStatus", row).text("1");

                    }
                    else {
                        $(".Status", row).html("<span class='badge badge badge-pill badge-danger'>Inactive</span>");
                        $(".HiddenStatus", row).text("0");

                    }

                    $("#moduleList tbody").append(row);
                    row = $("#moduleList tbody tr:last-child").clone(true);
                });
            }

        },
        error: function (result) {

        },

    });
}

function bindDropdown() {
    $.ajax({
        type: "POST",
        async: true,
        url: "/Admin/WebAPI/AssignModule.asmx/bindDropdown",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == "SessionExpire") {
                location.href = "/";
            }
            else {
                var xmldoc = $.parseXML(response.d);
                var Module = $(xmldoc).find("Table");
                var Location = $(xmldoc).find("Table1");
                //var Pages = $(xmldoc).find("Table2");
                var LocationHtml = '';

                $("#txtModule option:not(:first)").remove();
                $.each(Module, function () {
                    $("#txtModule").append($("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("ModuleName").text().replace(/&quot;/g, "\'") + "</option>"));
                });
                $("#txtModule").select2();

                

                LocationCheckBoxHtml('');

                //for search
                $("#searchLocation option:not(:first)").remove();
                $.each(Location, function () {
                    $("#searchLocation").append($("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("Location").text().replace(/&quot;/g, "\'") + "</option>"));
                });
                $("#searchLocation").select2();

                $("#searchModule option:not(:first)").remove();
                $.each(Module, function () {
                    $("#searchModule").append($("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("ModuleName").text().replace(/&quot;/g, "\'") + "</option>"));
                });
                $("#searchModule").select2();

              



            }
        },
        failure: function (result) {
            swal("Error!", 'Oops!,Something went wrong.Please try again.', "error");
        },
        error: function (result) {
            swal("Error!", 'Oops!,Something went wrong.Please try again.', "error");
        }
    });
}

function LocationCheckBoxHtml(checkedArray) {
    $.ajax({
        type: "POST",
        async: true,
        url: "/Admin/WebAPI/AssignModule.asmx/bindDropdown",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == "SessionExpire") {
                location.href = "/";
            }
            else {
                var xmldoc = $.parseXML(response.d);
                var Location = $(xmldoc).find("Table1");
                var LocationHtml = '';

                $.each(Location, function () {
                    if (jQuery.inArray($(this).find("AutoId").text(), checkedArray) !== -1) {
                        LocationHtml += "<input type='checkbox' class='req' name='Location' value='" + $(this).find("AutoId").text() + "' checked />" + $(this).find("Location").text();

                    } else {
                        LocationHtml += "<input type='checkbox' class='req' name='Location' value='" + $(this).find("AutoId").text() + "'/>" + $(this).find("Location").text();
                    }
                });
                $("#locationHtml").html(LocationHtml);
                //for search
                $("#searchLocation option:not(:first)").remove();
                $.each(Location, function () {
                    $("#searchLocation").append($("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("Location").text().replace(/&quot;/g, "\'") + "</option>"));
                });
                $("#searchLocation").select2();



            }
        },
        failure: function (result) {
            swal("Error!", 'Oops!,Something went wrong.Please try again.', "error");
        },
        error: function (result) {
            swal("Error!", 'Oops!,Something went wrong.Please try again.', "error");
        }
    });
}

function bindSubModule(ModuleId) {
    var data = {
        ModuleAutoId: ModuleId,
    };
    $.ajax({
        type: "POST",
        async: false,
        url: "/Admin/WebAPI/AssignModule.asmx/bindSubModule",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d == "SessionExpire") {
                location.href = "/";
            }
            else {
                var xmldoc = $.parseXML(response.d);
                var SubModule = $(xmldoc).find("Table");


                $("#txtSubModule option:not(:first)").remove();
                $.each(SubModule, function () {
                    $("#txtSubModule").append($("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("PageName").text().replace(/&quot;/g, "\'") + "</option>"));
                });
                $("#txtSubModule").select2();

                $("#searchSubModule option:not(:first)").remove();
                $.each(SubModule, function () {
                    $("#searchSubModule").append($("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("PageName").text().replace(/&quot;/g, "\'") + "</option>"));
                });
                $("#searchSubModule").select2();

            }
        },
        failure: function (result) {
            swal("Error!", 'Oops!,Something went wrong.Please try again.', "error");
        },
        error: function (result) {
            swal("Error!", 'Oops!,Something went wrong.Please try again.', "error");
        }
    });
}

function bindPageName(SubModuleAutoId) {
    var data = {
        SubModuleAutoId: SubModuleAutoId,
    };
    console.log(data);
    $.ajax({
        type: "POST",
        async: true,
        url: "/Admin/WebAPI/AssignModule.asmx/bindPageName",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d == "SessionExpire") {
                location.href = "/";
            }
            else {
                var xmldoc = $.parseXML(response.d);
                var Pages = $(xmldoc).find("Table");
                $("#textPageName option:not(:first)").remove();
                $.each(Pages, function () {
                    $("#textPageName").append($("<option value='" + $(this).find("PageUrl").text() + "'>" + $(this).find("PageUrl").text().replace(/&quot;/g, "\'") + "</option>"));
                });
                $("#textPageName").select2();

            }
        },
        failure: function (result) {
            swal("Error!", 'Oops!,Something went wrong.Please try again.', "error");
        },
        error: function (result) {
            swal("Error!", 'Oops!,Something went wrong.Please try again.', "error");
        }
    });
}


function save() {
    if ($("input[name=Location]:checked").length > 0) {
        var assignedLocation = $("input[name=Location]:checked").map(function () {
            return this.value;
        }).get().join(",");
    } else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return false;
    }

    if (checkRequiredField()) {

        var data = {
            Module: $("#txtModule").val(),
            Location: assignedLocation,
            SubModule: $("#txtSubModule").val(),
        }
 
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/AssignModule.asmx/assigedModule",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == 'Success') {
                    clearField();
                    swal("", "Module details saved successfully.", "success");
                    getAssignedModuleList();
                }
                else if (result.d == 'Unauthorized access.') {
                    location.href = "/";
                }
                else if (result.d = 'Page alread assigned') {
                    swal("Error!", "Page already assigned to this page.", "error");
                }
                else {
                    swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                }
            },
            error: function (result) {
                swal("Error!", result.d, "error");
            },
            failure: function (result) {
                swal("Error!", result.d, "error");
            }
        });
    } else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
};

function editPage(e) {
    var $row = $(e).closest("tr");
    var $AutoId = $row.find(".AutoId").text();
    var $HiddenStatus = $row.find(".HiddenStatus").text();
    var $HiddenModule = $row.find(".HiddenModule").text();
    var $HiddenSubModule = $row.find(".HiddenSubModule").text();
    var $HiddenPage = $row.find(".HiddenPage").text();
    var $HiddenLocation = $row.find(".HiddenLocation").text();
    $("#hiddenEditAutoId").val($AutoId);
    $("#txtModule").val($HiddenModule).change();
    $("#txtSubModule").val($HiddenSubModule).change();
    $("#textPageName").val($HiddenPage).change();
    var checkedArray = $HiddenLocation.split(',');
    LocationCheckBoxHtml(checkedArray);

    $("#saveBtn").hide();
    $("#btnReset").hide();
    $("#btnUpdate").show();
    $("#btnCancel").show();
}


function Update() {
    var assignedLocation = $("input[name=Location]:checked").map(function () {
        return this.value;
    }).get().join(",");
    if (checkRequiredField()) {
        var data = {
            Module: $("#txtModule").val(),
            Location: assignedLocation,
            SubModule: $("#txtSubModule").val(),
            AutoId: $("#hiddenEditAutoId").val(),

        }

        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/AssignModule.asmx/update",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == 'Success') {
                    swal("", "Module detail updated successfully.", "success");
                    clearField();
                    $("#saveBtn").show();
                    $("#btnReset").show();
                    $("#btnUpdate").hide();
                    $("#btnCancel").hide();
                    getAssignedModuleList();
                }
                else if (result.d == 'Unauthorized access.') {
                    location.href = "/";
                }
                else {
                    swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                }
            },
            error: function (result) {
                swal("Error!", result.d, "error");
            },
            failure: function (result) {
                swal("Error!", result.d, "error");
            }
        });
    } else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
};

function removeAssignedModule(AutoId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this module",
        icon: "warning",
        buttons: {
            cancel: {
                text: "No, cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: false,
            },
            confirm: {
                text: "Yes, delete it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm, e) {
        if (isConfirm) {
            var data = {
                AutoId: AutoId
            }
            $.ajax({
                type: "POST",
                url: "/Admin/WebAPI/AssignModule.asmx/delete",
                data: "{'dataValue':'" + JSON.stringify(data) + "'}",
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (result) {
                    if (result.d == 'Success') {
                        swal("", "Assigned module deleted successfully.", "success");
                        getAssignedModuleList();
                    } else {
                        swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                    }
                },
                error: function (result) {
                    swal("Error!", "Oops! Something went wrong.Please try later.", "error");
                },

            });

        } else {
            swal("", "Your data is safe.", "error");
        }
    })
}

function clearField() {
    $("#txtModule").val("");
    $("#textPageName").val("");
    $("#txtSubModule").val("");
    $("#hiddenEditAutoId").val("");
    $('input:checkbox').removeAttr('checked');
    return;
}
