﻿var c1 = 0;
$(document).ready(function () {
    if ($("#hiddenForPacker").val() == "") {
        $("#btnBulkUpload").show();
    }
    getProductList(1);
    bindCategory();
    BindBrand();
});
function Pagevalue(e) {
    debugger;
    var divid = $(e).parent().attr('id');
    
    if (divid =="ProductPager"){
        getProductList(parseInt($(e).attr("page")));
    }
    else if (divid == "LogPager") {
        ShowProductLStockogAll(parseInt($(e).attr("page")));
    }
};

/*---------------------------------Bind Customer Category-----------------------------------*/
function BindBrand() {
    $.ajax({
        type: "POST",
        url: "DropDownMaster.aspx/BindBrand",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: false,
        cache: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },

        success: function (response) {
            if (response.d != "Session Expired") {
                var getData = $.parseJSON(response.d);
                $("#ddlBrand option:not(:first)").remove();
                $.each(getData, function (index, item) {
                    $("#ddlBrand").append("<option value='" + getData[index].AutoId + "'>" + getData[index].BrandName + "</option>");
                });
                $("#ddlBrand").select2();
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}
/*---------------------------------------------------Bind Category-----------------------------------------------------------*/
function bindCategory() {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/productList.asmx/bindCategory",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var category = $(xmldoc).find("Table");

            $("#ddlSCategory option:not(:first)").remove();
            $.each(category, function () {
                $("#ddlSCategory").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("CategoryName").text() + "</option>");
            });
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

/*-------------------------------------------------------Bind Subcategory-----------------------------------------------------------*/
function bindSubcategory() {
    var categoryAutoId = $("#ddlSCategory").val();
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/productList.asmx/bindSubcategory",
        data: "{'categoryAutoId':'" + categoryAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var subcategory = $(xmldoc).find("Table");
            $("#ddlSSubcategory option:not(:first)").remove();
            $.each(subcategory, function () {
                $("#ddlSSubcategory").append("<option value=' " + $(this).find("AutoId").text() + "'>" + $(this).find("SubcategoryName").text() + "</option>");
            });
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
/*-------------------------------------------------------Get Product List----------------------------------------------------------*/
function getProductList(PageIndex) {
    var data = {
        CategoryAutoId: $("#ddlSCategory").val(),
        SubcategoryAutoId: $("#ddlSSubcategory").val(),
        Status: $("#ddlStatus").val(),
        ProductId: $("#txtSProductId").val().trim(),
        ProductName: $("#txtSProductName").val().trim(),
        Location: $("#txtSLocation").val().trim(),
        BarCode: $("#txtBarCode").val().trim(),
        SearchBy: $("#ddlSearch").val(),
        BrandAutoId: $("#ddlBrand").val(),
        Stock: $("#txtStock").val().trim(),
        PageSize: $("#ddlPageSize").val(),
        pageIndex: PageIndex
    };
    localStorage.setItem('SearchFilter', JSON.stringify(data));
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/ProductList.asmx/getProductList",
        data: JSON.stringify({ 'dataValue': JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successGetProductList,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function successGetProductList(response) {
    var xmldoc = $.parseXML(response.d);
    var productList = $(xmldoc).find("Table1");

    $("#tblProductList tbody tr").remove();

    var row = $("#tblProductList thead tr").clone(true);
    if (productList.length > 0) {
        $("#EmptyTable").hide();
        $.each(productList, function (index) {
            $(".ProductId", row).text($(this).find("ProductId").text());
            $(".Category", row).text($(this).find("Category").text());
            $(".Subcategory", row).text($(this).find("Subcategory").text());
            $(".ProductName", row).text($(this).find("ProductName").text().replace(/&quot;/g, "\'"));
            $(".Stock", row).text($(this).find("Stock").text());
            $(".BrandName", row).text($(this).find("BrandName").text());
            $(".ReOrderMark", row).html($(this).find("ReOrderMark").text()+' ('+$(this).find("UnitType").text()+')');
            if ($(this).find("Status").text() == 'Active') {
                $(".Status", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("Status").text() + "</span>");
            }
            else {
                $(".Status", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("Status").text() + "</span>");
            }

            $(".CommCode", row).html($(this).find("CommCode").text());
            $(".ImageUrl", row).html("<img src='" + $(this).find('ThumbnailImageUrl').text() + "' OnError='this.src =\"http://psmnj.a1whm.com/Attachments/default_pic.png\"' class='zoom img img-circle img-responsive' style='width: 50px;height: 50px;'  >");

            if ($('#HDDomain').val() == 'psmnj' || $('#HDDomain').val() == 'psm' || $('#HDDomain').val() == 'localhost') {//Delete option show to only PSMNJ
                $(".action", row).html("<a title='Print' href='#' onclick='PrintProductBarcode(" + $(this).find("AutoId").text() + ")'><span class='icon-printer' ></span></a>&nbsp;&nbsp;&nbsp<a title='Edit' href='/Admin/productMaster.aspx?ProductId=" + $(this).find("AutoId").text() + "'><span class='ft-edit' ></span></a>&nbsp;&nbsp<a title='Delete' href='javascript:;'><span class='ft-x' onclick='deleterecord(\"" + $(this).find("ProductId").text() + "\") '></span></a>&nbsp;&nbsp<a title='History' href='javascript:;'><span class='la la-history' onclick='ShowProductStockLog(\"" + $(this).find('ProductId').text() + "\",this)'></span></a>");
            }
            else {
                $(".action", row).html("<a title='Print' href='#' onclick='PrintProductBarcode(" + $(this).find("AutoId").text() + ")'><span class='icon-printer' ></span></a>&nbsp;&nbsp;&nbsp<a title='Edit' href='/Admin/productMaster.aspx?ProductId=" + $(this).find("AutoId").text() + "'><span class='ft-edit' ></span></a>&nbsp;&nbsp<a title='History' href='javascript:;'><span class='la la-history' onclick='ShowProductStockLog(\"" + $(this).find('ProductId').text() + "\",this)'></span></a>");
            }
            $("#tblProductList tbody").append(row);
            row = $("#tblProductList tbody tr:last").clone(true);
        });

    } else {
        $("#EmptyTable").show();
    }

    if ($("#hiddenForPacker").val() != "") {
        $("#linkAddNewProduct").hide();
        $(".glyphicon-remove").hide();
        $(".action").hide();
    }

    var pager = $(xmldoc).find("Table");
    $("#ProductPager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
}
function PrintProductBarcode(ProductAId) {
    window.open("/Admin/ProductBarcodePrint.html?ProductId=" + ProductAId, "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
}
function PrintProductAllBarcode() {

    window.open("/Admin/ProductBarcodeBulkPrint.html", "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
}

function deleterecord(ProductId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this product.",
        icon: "warning",
        showCancelButton: true,
        allowOutsideClick: false,
        closeOnClickOutside: false,
        buttons: {
            cancel: {
                text: "No, Cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, Delete it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(isConfirm => {
        if (isConfirm) {
            deleteProduct(ProductId);
        }
    })
}
function deleteProduct(ProductId) {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/ProductList.asmx/deleteProduct",
        data: "{'ProductId':'" + ProductId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (result) {
            if (result.d == 'Session Expired') {
                location.href = "/";
            }
            else if (result.d == 'true') {
                swal("", "Product details deleted successfully.", "success");
                getProductList(1);
            } else {
                swal("",result.d, "error");
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
            $("#alertCannotDelete").show();
            $("#cannotDeleteClose").click(function () {
                $("#alertCannotDelete").hide();
            })
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function BulkUpload() {
    var timeStamp = Date.parse(new Date());
    if ($("#fileProductBulk").val() != '') {
        var imgname = timeStamp;
    }
    else {
        var imgname = "default_pic.png";
    }
    if ($("#fileProductBulk").val() != "") {
        $("#errorMsg").hide().text("");
        $("#succMsg").hide().text("");

        var fileUpload = $("#fileProductBulk").get(0);
        var files = fileUpload.files;
        var test = new FormData();
        var fileExtension, flagExt = true;
        for (var i = 0; i < files.length; i++) {
            fileExtension = files[i].name.slice(files[i].name.lastIndexOf('.'));
            if (fileExtension == '.xls') {
                test.append(files[i].name, files[i]);
                flagExt = false;
            } else {
                swal("warning!", "Only .xls files are allowed", "warning");
            }
        }

        if (!flagExt) {
            $.ajax({
                type: "POST",
                url: "/FileUploadHandler.ashx?timestamp=" + timeStamp,
                data: test,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (result) {
                    $.ajax({
                        type: "POST",
                        url: "/Admin/WebAPI/ProductBulkUpload.asmx/BulkUpload",
                        data: "{'FileName':'" + JSON.parse(result)[0].URL + "'}",
                        contentType: "application/json; charset=utf-8",
                        datatype: "json",
                        beforeSend: function () {
                            $.blockUI({
                                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                                overlayCSS: {
                                    backgroundColor: '#FFF',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'transparent'
                                }
                            });
                        },
                        complete: function () {
                            $.unblockUI();
                        },
                        async: false,
                        success: function (response) {
                            if (response.d != 'Session Expired') {
                                $("#errorMsg").hide().text("");
                                var xmldoc = $.parseXML(response.d);
                                var productList = $(xmldoc).find("Table1");
                                $("#fileProductBulk").hide();
                                $("#btnUploadBulk").hide();
                                $("#btnFinalSave").show();

                                $("#tblTempProductList tbody tr").remove();
                                var row = $("#tblTempProductList thead tr").clone(true);
                                if (productList.length > 0) {
                                    $.each(productList, function () {
                                        $(".ProductId", row).text($(this).find("ProductId").text());
                                        if ($(this).find("chkCategory").text() == 'Ok') {
                                            $(".Category", row).text($(this).find("Category").text()).css("background-color", '#FFF');
                                        } else {
                                            $(".Category", row).text($(this).find("Category").text()).css("background-color", '#FF8A80');
                                        }
                                        if ($(this).find("chkSubcategory").text() == 'Ok') {
                                            $(".SubCategory", row).text($(this).find("Subcategory").text()).css("background-color", '#FFF');
                                        } else {
                                            $(".SubCategory", row).text($(this).find("Subcategory").text()).css("background-color", '#FF8A80');
                                        }

                                        $(".ProductName", row).text($(this).find("ProductName").text());
                                        $(".PreferVendor", row).text($(this).find("PreferVendor").text());
                                        $(".ReOrderMarkBox", row).text($(this).find("ReOrderMarkBox").text());
                                        $(".Unit1", row).text('Piece');
                                        $(".Qty1", row).text('1');
                                        $(".MinPrice1", row).text($(this).find("P_MinPrice").text());
                                        $(".wholesalePrice1", row).text($(this).find("P_WholeSale").text());
                                        $(".BasePrice1", row).text($(this).find("P_BasePrice").text());
                                        $(".CostPrice1", row).text($(this).find("P_CostPrice").text());
                                        $(".SRP1", row).text($(this).find("P_SRP").text());
                                        $(".Commission1", row).text($(this).find("P_CommCode").text());
                                        $(".Location1", row).text($(this).find("P_Location").text());
                                        $(".Barcode1", row).text($(this).find("P_Barcode").text());



                                        $(".Unit2", row).text('Box');
                                        if ($(this).find("chkBox").text() == 'Ok') {
                                            $(".Pieces2", row).text($(this).find("B_NoOfPieces").text()).css("background-color", '#fff');
                                            $(".BasePrice2", row).text($(this).find("B_BasePrice").text()).css("background-color", '#fff');
                                        } else {
                                            if ($(this).find("B_NoOfPieces").text() == "") {
                                                $(".Pieces2", row).text($(this).find("B_NoOfPieces").text()).css("background-color", '#FF8A80');
                                            } else {
                                                $(".Pieces2", row).text($(this).find("B_NoOfPieces").text());
                                            }
                                            if ($(this).find("B_BasePrice").text() == "") {
                                                $(".BasePrice2", row).text($(this).find("B_BasePrice").text()).css("background-color", '#FF8A80');
                                            } else {
                                                $(".BasePrice2", row).text($(this).find("B_BasePrice").text());
                                            }
                                        }

                                        $(".Qty2", row).text($(this).find("B_Qty").text());
                                        $(".MinPrice2", row).text($(this).find("B_MinPrice").text());
                                        $(".wholesalePrice2", row).text($(this).find("B_WholeSale").text());
                                        $(".BasePrice2", row).text($(this).find("B_BasePrice").text());
                                        $(".CostPrice2", row).text($(this).find("B_CostPrice").text());
                                        $(".SRP2", row).text($(this).find("B_SRP").text());
                                        $(".Commission2", row).text($(this).find("B_CommCode").text());
                                        $(".Location2", row).text($(this).find("B_Location").text());
                                        $(".Barcode2", row).text($(this).find("B_Barcode").text());
                                        $(".Unit3", row).text('Case');
                                        if ($(this).find("chkCase").text() == 'Ok') {
                                            $(".Pieces3", row).text($(this).find("C_NoOfPieces").text());
                                            $(".BasePrice3", row).text($(this).find("C_BasePrice").text());
                                        } else {
                                            if ($(this).find("B_NoOfPieces").text() == "") {
                                                $(".Pieces3", row).text($(this).find("C_NoOfPieces").text()).css("background-color", '#FF8A80');
                                                $(".BasePrice3", row).text($(this).find("C_BasePrice").text());
                                            } else {
                                                $(".Pieces3", row).text($(this).find("C_NoOfPieces").text());
                                                $(".BasePrice3", row).text($(this).find("C_BasePrice").text()).css("background-color", '#FF8A80');
                                            }
                                        }

                                        $(".Qty3", row).text($(this).find("C_Qty").text());
                                        $(".MinPrice3", row).text($(this).find("C_MinPrice").text());
                                        $(".wholesalePrice3", row).text($(this).find("C_WholeSale").text());
                                        $(".BasePrice3", row).text($(this).find("C_BasePrice").text());
                                        $(".CostPrice3", row).text($(this).find("C_CostPrice").text());
                                        $(".SRP3", row).text($(this).find("C_SRP").text());
                                        $(".Commission3", row).text($(this).find("C_CommCode").text());
                                        $(".Location3", row).text($(this).find("C_Location").text());
                                        $(".Barcode3", row).text($(this).find("C_Barcode").text());
                                        $(".D_Selling", row).text($(this).find("D_Selling").text());

                                        $("#tblTempProductList tbody").append(row);
                                        row = $("#tblTempProductList tbody tr:last").clone(true);
                                    });
                                    $("#tblTempProductList").show();
                                } else {
                                    $("#tblTempProductList tbody tr").remove();
                                    $("#tblTempProductList").hide();
                                }
                            } else {
                                location.href = '/';
                            }
                        },
                        error: function (result) {
                            console.log(result);
                            swal("Warning!", "Something not right with excel sheet", "warning");
                        },
                        failure: function (result) {
                            console.log(result);
                        }
                    });
                },
                error: function (result) {
                    swal("Warning!", "Something not right.", "warning");
                },
                failure: function (result) {
                    swal("Error!", 'Oops,some thing went wrong.Please try again !!', "error");
                }
            });
        }
    } else {
        swal("Warning!", "No file selected..", "warning");
    }
}

function FinalSave() {

    $("#finalError").hide().text('');
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/ProductBulkUpload.asmx/FinalUpload",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (result) {
            alert(result.d);
            if (result.d != 'Session Expired') {
                getProductList(1);
                $("#ModalBulUpload").modal('toggle');
            } else {
                location.href = '/';
            }
        },
        error: function (result) {
            console.log(result);
            swal("Error!", 'Oops,some thing went wrong.Please try again !!', "error");
        },
        failure: function (result) {
            swal("Error!", 'Oops,some thing went wrong.Please try again !!', "error");
        }
    });
}


function resetModalBulUpload() {
    $("#finalError").hide().text('');
    $("#errorMsg").hide().text('');
    $("#fileProductBulk").show().val("");
    $("#btnUploadBulk").show();
    $("#btnFinalSave").hide();
    $("#tblTempProductList").hide().find("tbody tr").remove();
}
function resetModalProductStock() {
    $("#btnFinalSave").hide();
    $("#tblTempProductList").hide().find("tbody tr").remove();
}
function ShowProductStockLog(ProductId, e) {
    $("#ProductStockId").val(ProductId);
    //var ProductId = $(e).closest('tr').find('.ProductId').text();
    //var ProductName = $(e).closest('tr').find('.ProductName').text();
    //$("#pid").text("#" + ProductId);
    //$("#pname").text(ProductName);
    ShowProductLStockogAll(1)
}

function ShowProductLStockogAll(pageIndex) {
    var data = {
        ProductId: $("#ProductStockId").val(),
        PageSize: $("#ddlPageSizeLog").val(),
        PageIndex: pageIndex
    };

    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/ProductList.asmx/getProductStochChangeLog",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var StockLog = $(xmldoc).find("Table1");
            $("#tblStockLog tbody tr").remove();
            var row = $("#tblStockLog thead tr").clone(true);

            if (StockLog.length > 0) {
                $("#EmptyTable33").hide();
                $.each(StockLog, function (index) {
                    $(".oldStcok", row).html($(this).find("oldStcok").text());
                    $(".NewStock", row).html($(this).find("NewStock").text());
                    $(".ReferenceId", row).html($(this).find("ReferenceId").text());
                    $(".remark", row).html($(this).find("remark").text());
                    $(".ProductName", row).html($(this).find("ProductName").text());                   
                    $(".AffectedStock", row).html($(this).find("AffectedStock").text());
                    $(".DefaultAffectedStock", row).html($(this).find("DefaultAffectedStock").text());
                    $(".ActionDate", row).html($(this).find("ActionDate").text());
                    $("#tblStockLog tbody").append(row);
                    row = $("#tblStockLog tbody tr:last").clone(true);
                });
            }
            else {
                $('#EmptyTable33').show();
            }

            var pager = $(xmldoc).find("Table");
            $("#LogPager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
            var Logheader = $(xmldoc).find("Table2");
            $("#pid").text("#" + Logheader.find("ProductId").text());
            $("#pname").text(Logheader.find("ProductName").text());
            $("#showStockModal").modal('show');

            if ($("#ddlPageSizeLog").val() == '0') {
                $("#LogPager").hide();
            }
            else {
                $("#LogPager").show();
            }
        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function closeHistoryModal() {
    $("#ddlPageSizeLog").val(10);
}