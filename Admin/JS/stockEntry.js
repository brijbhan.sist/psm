﻿$(document).ready(function () {
    bindVendor();
    bindProduct();
    $('#txtBillDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    $("#txtBillDate").val(month + '/' + day + '/' + year);
    $("#ddlUnitType").attr('disabled', true);
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    var getid = getQueryString('BillAutoId');
    if (getid != null) {
        editStockEntry(getid);
        //Product(getid);
    }
    else {
      //  bindProduct();
        $("#emptyTable").show();
    }
})

function bindVendor() {
    $.ajax({
        type: "POST",
        url: "/Admin/DropDownMaster.aspx/BindVendor",
        data: "{}",
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var getData = $.parseJSON(response.d);
            $("#ddlVendor option:not(:first)").remove();
            $.each(getData[0].VendorList, function (index, item) {
                $("#ddlVendor").append("<option value='" + item.AutoId + "'>" + item.VendorName + "</option>");
            });
            $("#ddlVendor").select2();
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function bindProduct() {
    $.ajax({
        type: "POST",
        url: "/Admin/DropDownMaster.aspx/BindProduct",
        data: "{}",
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var getData = $.parseJSON(response.d);
            $("#ddlProduct option:not(:first)").remove();
            $.each(getData[0].ProductList, function (index, item) {
                $("#ddlProduct").append("<option value='" + item.AutoId + "'>" + item.ProductName + "</option>");
            });
            $("#ddlProduct").select2();
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function bindUnitType() {
    if ($("#txtBarcode").val() != "") {
        return;
    }
    $("#btnAdd").attr("disabled", false);
    var productAutoId = $("#ddlProduct option:selected").val();
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/stockEntry.asmx/bindUnitType",
        data: "{'productAutoId':" + productAutoId + "}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == "SessionExpire") {
                location.href = "/";
            }
            else {
                var xmldoc = $.parseXML(response.d);
                var unitType = $(xmldoc).find("Table");
                var PackingType = $(xmldoc).find("Table1");
                $("#ddlUnitType option").remove();
                $.each(unitType, function () {
                    $("#ddlUnitType").append($("<option value='" + $(this).find("AutoId").text() + "' QtyPerUnit='" + $(this).find("Qty").text() + "'>" + $(this).find("UnitType").text() + " ( " + $(this).find("Qty").text() + " pcs" + " )</option>"));
                })
                $("#txtQuantity").val(1);


                $("#ddlUnitType").removeAttr('disabled');
                $("#ddlUnitType").val($(PackingType).find("PackingAutoId").text());
                $("#txtTotalPieces").val($("#ddlUnitType option:selected").attr('QtyPerUnit'));
            }
        },
        error: function (result) {
            swal("Error!", 'Oops!,Something went wrong.Please try again.', "error");
        },
        failure: function (result) {
            swal("Error!", 'Oops!,Something went wrong.Please try again.', "error");
        }
    });
}


$("#ddlUnitType").change(function () {
    $("#txtQuantity").val("0");
    $("#txtTotalPieces").val("0");

    /*----------------------------------To Check Barcode Count---------------------------------*/

    var data = {
        ProductAutoId: $("#ddlProduct").val(),
        UnitAutoId: $("#ddlUnitType").val()
    };

    $.ajax({
        type: "Post",
        url: "/Admin/WebAPI/stockEntry.asmx/countBarcode",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == "SessionExpire") {
                location.href = "/";
            }
            else {
                var xmldoc = $.parseXML(response.d);
                var barcode = $(xmldoc).find("Table");
                var ProductDetails = $(xmldoc).find("Table1");
                var BarcodeCount = $(barcode).find("BarcodeCount").text();
                Price = $(ProductDetails).find('Price').text();
                toastr.success('Barcode Count' + BarcodeCount, 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                $("#btnAdd").attr("disabled", false);
            }
        },
        error: function (result) {
            swal("Error!", 'Oops!,Something went wrong.Please try again.', "error");
        },
        failure: function (result) {
            swal("Error!", 'Oops!,Something went wrong.Please try again.', "error");
        }
    });
});

function calTotal() {

    var QtyperUnit = $("#ddlUnitType option:selected").attr("QtyPerUnit");
    var Qty = $("#txtQuantity").val() || 0;
    if (QtyperUnit == undefined) {
        $("#txtTotalPieces").val(Number(Qty) * 0);
    }
    else {
        $("#txtTotalPieces").val(Number(Qty) * Number(QtyperUnit));
    }

    var unitPrice = $("#txtUnitprice").val() || 0;
    $("#txtTotalAmount").val((Number(Qty) * Number(unitPrice)).toFixed(2));
}

$("#txtQuantity").keyup(function () {
    calTotal();
});

$("#txtUnitprice").keyup(function () {
    calTotal();
});
var CHK = 0;
/*----------------------------------Add Row to table---------------------------------*/
$("#btnAdd").click(function () {
    var Priceunit = $("#txtUnitprice").val();
    if (checkRequiredFieldItem()) {
        if (parseFloat(Priceunit) != parseFloat(Price) && CHK == 0) {
            //$('#Massage').show();
            //$('#ProductDetails').hide();
            GetProductDetails();
            $("#UpdateMinPrice").modal('show');
        } else {
            CHK = 0;
            $("#alertBarcodeCount").hide();
            $("#emptyTable").hide();
            var check = false,msg=0;
            var ProductAutoId = $("#ddlProduct option:selected").val();
            var UnitType = $("#ddlUnitType option:selected").val() 
            $("#tblProductDetail tbody tr").each(function () {
                if (Number(ProductAutoId) == Number($(this).find('.ProName span').attr('ProductAutoId')) && Number(UnitType) == Number($(this).find('.Unit span').attr('UnitAutoId'))) {
                    $(this).find('.Qty input').val(($(this).find('.Qty input').val() == '' ? '0' : (Number($(this).find('.Qty input').val()) + Number($("#txtQuantity").val()))));
                    var TotalPeices = 0;
                    TotalPeices = parseInt($(this).find('.Qty input').val() * $(this).find('.Unit span').attr('qtyperunit'));
                    $(this).find('.Pcs').html(TotalPeices);
                    var TotalCostPrice = parseFloat(parseInt($(this).find('.Qty input').val()) * parseFloat(($(this).find('.Price input').val().trim() == '' ? '0' : $(this).find('.Price input').val().trim()))).toFixed(2);
                    $(this).find('.TotalPrice').html(TotalCostPrice);
                    check = true;
                    var row = $(this);
                    $('#tblProductDetail tbody tr:first').before(row);
                }
                else if (Number(ProductAutoId) == Number($(this).find('.ProName span').attr('ProductAutoId')) && Number(UnitType) != Number($(this).find('.Unit span').attr('UnitAutoId'))) {
                    check = true;
                    msg = 1;
                }
            });
            if (msg == 1) {
                swal("", "You can't add different unit of added product.", "error");
            }
            else {
                if (check == false) {
                    var product = $("#ddlProduct option:selected").text().split("--");
                    var row = $("#tblProductDetail thead tr:last").clone(true);
                    var unitPrice = $("#txtUnitprice").val() || 0.00;
                    $(".ProId", row).text(product[0]);
                    $(".ProName", row).html("<span ProductAutoId='" + $("#ddlProduct option:selected").val() + "'>" + product[1] + "</span>");
                    $(".Unit", row).html("<span UnitAutoId='" + $("#ddlUnitType option:selected").val() + "' QtyPerUnit='" + $("#ddlUnitType option:selected").attr("QtyPerUnit") + "'>" + $("#ddlUnitType option:selected").text() + "</span>");
                    $(".Qty", row).html("<input type='text' class='form-control border-primary input-sm' style='width:100px;' id='Qty' onkeyup='calPcs(this)' value='" +
                        $("#txtQuantity").val() + "' onfocus='this.select()' onkeypress='return isNumberKey(event)'>");
                    $(".Pcs", row).text($("#txtTotalPieces").val());
                    $(".Price", row).html("<input type='text' class='form-control border-primary input-sm' style='text-align:right;width:100%' id='Price' onkeyup='calPcs(this)' value='" +
                        (Number(unitPrice).toFixed(2)) + "' onfocus='this.select()' onkeypress='return isNumberDecimalKey(event, this)' style='text - align: right'>");
                    $(".TotalPrice", row).text($("#txtTotalAmount").val());

                    $(".Action", row).html("<a title='Delete' href='#' id='deleterow' onclick='deleterecord(this)'><span class='ft-x'></span></a>");

                    if ($('#tblProductDetail tbody tr').length > 0) {
                        $('#tblProductDetail tbody tr:first').before(row);
                    }
                    else {
                        $('#tblProductDetail tbody').append(row);
                    }
                }
                calGTotal();
                $('#txtBarcode').focus();
                $("select#ddlUnitType").prop('selectedIndex', 0).change();
                $("#ddlProduct").prop('selectedIndex', 0).change();
                $('#txtQuantity').val('');
                $('#txtTotalPieces').val('');
                $('#txtTotalAmount').val('');
                $('#txtUnitprice').val('');
                $("#txtBarcode").val('');
                toastr.success('Item added successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            }
        }
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
});
/*----------------------------------Validator----------------------------------------*/
function checkRequiredFieldSave() {
    var boolcheck = true;
    $('.sreq').each(function () {
        if ($(this).val() == '') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });
    $('.sddlreq').each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == 'undefined' || $(this).val() == null) {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });

    $('table input[type=text]').each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == '0.00') {
            $(this).addClass('border-warning');
            boolcheck = false;
        }
        else {
            $(this).removeClass('border-warning');
        }
    });
    return boolcheck;

}

function checkRequiredFieldItem() {
    var boolcheck = true;
    $('.req').each(function () {
        if ($(this).val() == '') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });

    if ($("#txtQuantity").val() == '0' || $("#txtQuantity").val() == ' ') {
        boolcheck = false;
        $("#txtQuantity").addClass('border-warning');
    }
    else {
        $("#txtQuantity").removeClass('border-warning');
    }

    if ($("#txtUnitprice").val() == '0.00' || $("#txtUnitprice").val() == ' ') {
        boolcheck = false;
        $("#txtUnitprice").addClass('border-warning');
    }
    else {
        $("#txtUnitprice").removeClass('border-warning');
    }



    $('.ddlreq').each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == 'undefined' || $(this).val() == null) {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });

    $('.select2').each(function () {
        if ($(this).text() == '' || $(this).text() == '0' || $(this).text() == '-Select-') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });
    return boolcheck;
}

/*----------------------------------Add Stock----------------------------------------*/
$("#btnAddStock").click(function () {
    if (checkRequiredFieldSave()) {
        if ($('#tblProductDetail tbody tr').length > 0) {
            var Product = [];
            $("#tblProductDetail tbody tr").each(function () {
                Product.push({
                    ProductAutoId: $(this).find('.ProName span').attr('ProductAutoId'),
                    UnitAutoId: $(this).find('.Unit span').attr('UnitAutoId'),
                    QtyPerUnit: $(this).find('.Unit span').attr('QtyPerUnit'),
                    Quantity: $(this).find('.Qty input').val(),
                    TotalPieces: $(this).find('.Pcs').text(),
                    Price: $(this).find('.Price input').val()
                });
            });
            var billData = {
                VendorAutoId: $("#ddlVendor option:selected").val(),
                billNo: $("#txtBillNo").val(),
                billDate: $("#txtBillDate").val(),
                remarks: $("#txtRemarks").val()
            };

            $.ajax({
                type: "Post",
                url: "/Admin/WebAPI/stockEntry.asmx/insertStockEntry",
                data: "{'TableValues':'" + JSON.stringify(Product) + "','billData':'" + JSON.stringify(billData) + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (data) {
                    if (data.d == "SessionExpire") {
                        location.href = "/";
                    }
                    else if (data.d == "true") {
                        swal({
                            icon: "success",
                            title: "",
                            text: "Stock details saved successfully.",
                            type: "success"
                        }).then(function () {
                            reset();
                        });
                    }
                    else {
                        swal("Error!", data.d, "error");
                    }
                },
                error: function (result) {
                    swal("Warning!", JSON.parse(result.responseText).d, "error");
                },
                failure: function (result) {
                    swal("Error!", "Oops!,Something went wrong.Please try again.", "error");
                }
            });
        }
        else {
            swal("Error!", "Please add atleast one product.", "error");
        }
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
});


function editStockEntry(BillAutoId) {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/stockEntry.asmx/editStockEntry",
        data: "{'BillAutoId':" + BillAutoId + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == "SessionExpire") {
                location.href = "/";
            }
            else {
                var xmldoc = $.parseXML(response.d);
                var stockEntry = $(xmldoc).find("Table");
                var billitems = $(xmldoc).find("Table1");
                var product = $(xmldoc).find("Table2");
              
                $("#btnUpdate").show();
                $("#btnAddStock").hide();
                $("#btnReset").hide();
                //$("#pnlInputProduct").hide();

                $("#txtHBillAutoId").val($(stockEntry).find("AutoId").text())
                $("#txtBillNo").val($(stockEntry).find("BillNo").text());
                $("#txtBillDate").val($(stockEntry).find("BillDate").text());
                $("#txtRemarks").val($(stockEntry).find("Remarks").text());               
                $("#ddlVendor").val($(stockEntry).find("VendorAutoId").text()).change();
                $("#ddlVendor").attr('disabled',true);
                var row = $("#tblProductDetail thead tr").clone(true);

                if (billitems.length > 0) {
                    $("#emptyTable").hide();
                    $.each(billitems, function () {
                        $(".ProId", row).html("<span rowautoid='" + $(this).find("AutoId").text() + "'>" + $(this).find("ProductId").text() + "</span>");
                        $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "'>" + $(this).find("ProductName").text() + "</span>");
                        $(".Unit", row).html("<span UnitAutoId='" + $(this).find("UnitAutoId").text() + "' QtyPerUnit='" + $(this).find("QtyPerUnit").text() + "'>" + $(this).find("UnitType").text() + " ( " + $(this).find("QtyPerUnit").text() + " pcs" + " )</span>");
                        $(".Qty", row).html("<input type='text' class='form-control border-primary input-sm' id='Qty' onkeypress='return isNumberDecimalKey(event, this)' style='width:100px;text-align:center' onkeyup='calPcs(this)' value='" + $(this).find("Quantity").text() + "'>");
                        $(".Pcs", row).text($(this).find("TotalPieces").text());
                        $(".Price", row).html("<input type='text' class='form-control border-primary input-sm' style='text-align:right;width:100%' id='Price' onkeyup='calPcs(this)' value='" +
                            $(this).find("Price").text() + "' onfocus='this.select()' onkeypress='return isNumberDecimalKey(event, this)' style='text - align: right'>");
                        $(".TotalPrice", row).text($(this).find("TotalPrice").text());
                        $(".Action", row).html("<a href='#' id='deleterow' onclick='deleterecord(this)'><span class='ft-x'></span></a>");
                        $("#tblProductDetail tbody").append(row);
                        row = $("#tblProductDetail tbody tr:last").clone(true);
                    });
                    calGTotal();
                }
                else {
                    $("#emptyTable").show();
                }
                //  $("#ddlProduct option:not(:first)").remove();
                //$.each(product, function () {
                //    $("#ddlProduct").append($("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("ProductName").text().replace(/&quot;/g, "\'") + "</option>"));
                //});
                //$("#ddlProduct").select2();
            }
        },
        failure: function (result) {
            swal("Error!", 'Oops!,Something went wrong.Please try again.', "error");
        },
        error: function (result) {
            swal("Error!", 'Oops!,Something went wrong.Please try again.', "error");
        }
    });
}
function Product(BillAutoId) {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/stockEntry.asmx/editStockEntry",
        data: "{'BillAutoId':" + BillAutoId + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == "SessionExpire") {
                location.href = "/";
            }
            else {
                var xmldoc = $.parseXML(response.d);
                var product = $(xmldoc).find("Table2");
                $("#ddlProduct option:not(:first)").remove();
                $.each(product, function () {
                    $("#ddlProduct").append($("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("ProductName").text().replace(/&quot;/g, "\'") + "</option>"));
                });
                $("#ddlProduct").select2();
            }
        },
        failure: function (result) {
            swal("Error!", 'Oops!,Something went wrong.Please try again.', "error");
        },
        error: function (result) {
            swal("Error!", 'Oops!,Something went wrong.Please try again.', "error");
        }
    });
}
function deleterecord(e) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this item",
        icon: "warning",
        showCancelButton: true,
        allowOutsideClick: false,
        closeOnClickOutside: false,
        buttons: {
            cancel: {
                text: "No, cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, delete it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            deleterow(e);
        } 
    })
}

function deleterow(e) {
    $(e).closest('tr').remove();
    calGTotal();
    swal("", "Item deleted successfully.", "success");
    if ($("#tblProductDetail > tbody").children().length == 0) {
        $("#emptyTable").show();
    } else {
        $("#emptyTable").hide();
    }
}

function calPcs(e) {
    var row = $(e).closest("tr"), pcs;
    var Qty = row.find('.Qty input').val() || 0;
    var Price = row.find('.Price input').val() || 0;

    var pcs = Number(Qty) * Number(row.find(".Unit span").attr("QtyPerUnit"));
    row.find(".Pcs").text(pcs);

    var TotalPrice = (Number(Price) * Number(Qty)).toFixed(2);
    row.find(".TotalPrice").text(TotalPrice);

    calGTotal();
}
function calGTotal() {
    var totalAmount = 0.00;
    var rows = $("#tblProductDetail tbody .TotalPrice");
    $.each(rows, function () {
        totalAmount = Number(totalAmount) + Number($(this).text());

    });
    $("#totalAmount").text(Number(totalAmount).toFixed(2));
}

/*----------------------------------Update Stock Entry----------------------------------------*/
$("#btnUpdate").click(function () {
    if (checkRequiredFieldSave()) {

        var Product = [];
        $("#tblProductDetail tbody tr").each(function () {
            Product.push({
                ProductAutoId: $(this).find('.ProName span').attr('ProductAutoId'),
                UnitAutoId: $(this).find('.Unit span').attr('UnitAutoId'),
                QtyPerUnit: $(this).find('.Unit span').attr('QtyPerUnit'),
                Quantity: $(this).find('.Qty input').val(),
                TotalPieces: $(this).find('.Pcs').text(),
                Price: $(this).find('.Price input').val()
            });
        });
        var billData = {
            VendorAutoId: $("#ddlVendor option:selected").val(),
            billAutoId: $("#txtHBillAutoId").val(),
            billNo: $("#txtBillNo").val(),
            billDate: $("#txtBillDate").val(),
            remarks: $("#txtRemarks").val()
        };

        $.ajax({
            type: "Post",
            url: "/Admin/WebAPI/stockEntry.asmx/updateStockEntry",
            data: "{'TableValues':'" + JSON.stringify(Product) + "','billData':'" + JSON.stringify(billData) + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (data) {
                if (data.d == "SessionExpire") {
                    location.href = "/";
                }
                else if (data.d == "true") {
                    swal("", "Stock details updated successfully.", "success");
                    $('#btnclose').click(function () {
                        window.location.href = "/Admin/stockEntry.aspx";
                    });
                }
                else {
                    swal("Error!", data.d, "error");
                }

            },
            error: function (result) {
                swal("Error!", JSON.parse(result.responseText).d, "error");
            },
            failure: function (result) {
                swal("Error!", "Oops!,Something went wrong.Please try again.", "error");
            }
        });
    } else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

    }
});

$("#btnReset").click(function () {
    reset();
    //resetAfterAddStock();
});

function reset() {
    $("input[type=text], textarea").val("");
    $("select").val(0).change();
    $("#txtUnitprice").val("0.00");
    $("#txtTotalAmount").val("0.00");
    $("#tblProductDetail tbody tr").remove();
    $("#emptyTable").show();
    $("#totalAmount").text('0.00');
    $("#ddlVendor").removeClass('border-warning');
    $("#txtBillNo").removeClass('border-warning');
   // $("#txtBillDate").removeClass('border-warning');
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    $("#txtBillDate").val(month + '/' + day + '/' + year);
}

function resetProductPanel() {
    $("#panelProduct select").val(0);
    $("#ddlProduct").val(0).change();
    $("#txtQuantity, #txtTotalPieces").val("0");
    $("#txtUnitprice, #txtTotalAmount").val("0.00");
    $("#txtBarcode").val("");
    $("#alertBarcodeCount").hide();
    $("#alertBarcode").hide();
    $("#ddlProduct").attr('disabled', false);
    $("#ddlUnitType").attr('disabled', false);
}
function resetAfterAddStock() {
    $("#panelBill input[type='text']").val('');
    $("#panelBill textarea").val('');
    // $("#ddlVendor").val(0);
    bindProduct();
    $("#tblProductDetail tbody tr").remove();
    $("#ddlProduct").val(0).change();
    resetProductPanel();
    $("#emptyTable").show();
    $("#totalAmount").text("0.00")
    $("#btnUpdate").attr("disabled", true);
}
/*---------------------------------------------------------------------------------------------------------------------------------------*/
//                                                        Barcode Reading
/*---------------------------------------------------------------------------------------------------------------------------------------*/
var Price = 0.00;
function readBarcode() {
    var barcode = $("#txtBarcode").val();
    $("#alertBarcode").hide();
    $("#alertBarcode span").html("");
    if (barcode != "") {
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/stockEntry.asmx/getProductThruBarcode",
            data: "{'barcode':'" + barcode + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d == "SessionExpire") {
                    location.href = "/";
                }
                else {
                    var xmldoc = $.parseXML(response.d);
                    var product = $(xmldoc).find("Table");
                    var unitType = $(xmldoc).find("Table1");

                    if (product.length != 0) {
                        Price = $(unitType).find("Price").text();
                        $("#ddlProduct").attr('disabled', true);
                        $("#ddlUnitType").attr('disabled', true);

                        $("#ddlProduct").val($(product).find("ProductAutoId").text()).change();

                        $("#ddlUnitType option:not(:first)").remove();
                        $.each(unitType, function () {
                            $("#ddlUnitType").append($("<option value='" + $(this).find("AutoId").text() + "' QtyPerUnit='" + $(this).find("Qty").text() + "'>" + $(this).find("UnitType").text() + "(" + $(this).find("Qty").text() + " pcs" + ")</option>"));
                        })

                        $("#ddlUnitType").val($(product).find("UnitAutoId").text()).change();
                        $("#btnAdd").attr('disabled', false);
                    } else {
                        const wrapper = document.createElement('div');
                        wrapper.innerHTML = "Barcode does not exists in Record. Assign Product to this Barcode Or <a href='/Admin/productMaster.aspx'>Create a New Product</a>";
                        swal({
                            title: '',
                            text: '',
                            content: wrapper
                        });

                        $("#panelProduct select").val(0);
                        $("#txtQuantity, #txtTotalPieces").val("0");

                    }
                }
            },
            failure: function (result) {
                swal("Error!", 'Oops!,Something went wrong.Please try again.', "error");
            },
            error: function (result) {
                swal("Error!", 'Oops!,Something went wrong.Please try again.', "error");
            }
        });
    } else {
        $("select").val(0);
        $("#alertBarcodeCount").hide();
        $("#ddlProduct").attr('disabled', false);
        $("#ddlUnitType").attr('disabled', false);
    }
}
var ItemAutoId;
function GetProductDetails() {
    $("#Massage").hide();
    $("#ProductDetails").show();
    $('#Button1').hide();
    $('#Button3').hide();
    $('#Button2').show();
    $('#Button4').show();
    $("#txtBasePrice").attr('readonly', true);
    $("#txtWholesaleMinPrice").attr('readonly', true);
    $("#txtRetailMIN").attr('readonly', true);
    $("#txtCOSTPRICE").attr('readonly', true);
    $("#txtSRP").attr('readonly', true);

    var ProductDetails = {
        ProductAutoId: $("#ddlProduct").val(),
        UnitAutoId: $("#ddlUnitType").val()
    }
    $.ajax({
        type: "Post",
        url: "/Admin/WebAPI/stockEntry.asmx/GetProductDetails",
        data: "{'dataValues':'" + JSON.stringify(ProductDetails) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == "SessionExpire") {
                location.href = "/";
            }
            else {
                var xmldoc = $.parseXML(response.d);
                var product = $(xmldoc).find("Table");
                ItemAutoId = $(product).find('ItemAutoId').text();
                $("#txtproductId").val($(product).find('ProductId').text());
                $("#txtProductName").val($(product).find('ProductName').text());
                $("#txtUnitType").val($(product).find('UnitType').text());
                $("#txtBasePrice").val($(product).find('Price').text());
                $("#txtWholesaleMinPrice").val($(product).find('WHminPrice').text());
                $("#txtRetailMIN").val($(product).find('MinPrice').text());
                $("#txtCOSTPRICE").val($(product).find('CostPrice').text());
                $("#txtSRP").val($(product).find('SRP').text());
                $("#txtLocation").val($(product).find('Location').text());
                $("#txtCommissionCode").val($(product).find('CommCode').text());
                //$("#txtPreDefinedBarcode").val($(product).find('CommCode').text());
                $("#txtPQty").val($(product).find('Qty').text());
            }
        },
        error: function (result) {
            swal("Error!", 'Oops!,Something went wrong.Please try again.', "error");
        },
        failure: function (result) {
            swal("Error!", 'Oops!,Something went wrong.Please try again.', "error");
        }
    });
}
function updateBasePrice() {
    if (parseFloat($("#txtCOSTPRICE").val()) > parseFloat($('#txtRetailMIN').val())) {
        swal("warning!", "Cost Price should be less or equal to Retail Min Price.", "warning");
        return;
    }
    if (parseFloat($("#txtWholesaleMinPrice").val()) > parseFloat($('#txtRetailMIN').val())) {
        swal("warning!", "Whole Sale Min Price should be less or equal to  Retail Min Price.", "warning");
        return;
    }
    if ((parseFloat($("#txtWholesaleMinPrice").val()) > parseFloat($("#txtBasePrice").val())) || (parseFloat($('#txtRetailMIN').val()) > parseFloat($("#txtBasePrice").val()))) {
        swal("warning!", "Base Price should be greater  or equal to Retail Min Price and Whole Sale Min Price.", "warning");
        return;
    }
    var ProductDetails = {
        ItemAutoId: ItemAutoId,
        RetailMIN: $("#txtRetailMIN").val(),
        CostPrice: $("#txtCOSTPRICE").val(),
        SRP: $("#txtSRP").val(),
        BasePrice: $("#txtBasePrice").val(),
        WholesaleMinPrice: $("#txtWholesaleMinPrice").val(),
    }
    $.ajax({
        type: "Post",
        url: "/Admin/WebAPI/stockEntry.asmx/updateBasePrice",
        data: "{'dataValues':'" + JSON.stringify(ProductDetails) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == 'true') {
                Price = $("#txtBasePrice").val();
                swal("", 'Product price updated successfully.', "success");
            } else if (response.d == 'false') {
                swal("Error!", 'Oops!,Something went wrong.Please try again.', "error");
            }
            else if (response.d == 'SessionExpire') {
                location.href = "/";
            }
            else {

                var xmldoc = $.parseXML(response.d);
                var PricelevelList = $(xmldoc).find("Table");
                var html = "<div class='table-responsive'><table style='width:100%' class='table table-striped table-bordered'><thead class='bg-blue white'><tr><td>Product Id</td><td>Product Name</td><td>Unit</td><td>Price Level Name</td>";
                html += "<td>Custom Price</td></tr></thead><tbody>";
                $(PricelevelList).each(function () {
                    html += "<tr><td class='text-center'>" + $(this).find('ProductId').text() + "</td><td>" + $(this).find('ProductName').text() + "</td>";
                    html += "<td class='text-center'>" + $(this).find('UnitType').text() + "</td><td>" + $(this).find('PriceLevelName').text() + "</td>";
                    html += "<td style='text-align:right'>" + $(this).find('CustomPrice').text() + "</td>";
                    html += "</tr>";
                });
                html += "</tbody></table></div>";

                $("#StockEntryMsg").append(html);
                $("#StockEntryPopup").modal('show');
                // swal("warning!", html, "warning");

            }
        },
        error: function (result) {
            swal("Error!", "Product price not updated.", "error");
        },
        failure: function (result) {
            swal("Error!", 'Oops!,Something went wrong.Please try again.', "error");
        }
    });
}
function NOPrice() {
    CHK = 1;
    $("#UpdateMinPrice").modal('hide');
}
function EditProductDetails() {
    $("#txtBasePrice").attr('readonly', false);
    $("#txtWholesaleMinPrice").attr('readonly', false);
    $("#txtRetailMIN").attr('readonly', false);
    $("#txtCOSTPRICE").attr('readonly', false);
    $("#txtSRP").attr('readonly', false);
    $('#Button1').show();
    $('#Button3').show();
    $('#Button2').hide();
    $('#Button4').hide();
}
function CancelProduct() {
    GetProductDetails();
}