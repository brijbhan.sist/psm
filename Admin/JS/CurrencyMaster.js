﻿$(document).ready(function () {
    resetCurrency();
    GetCurrencyDetail();
    $("#btnUpdate").hide();
    $("#btnCancel").hide();
    
})
/*------------------------------------------------------Insert Currency------------------------------------------------------------*/
function Save() {
    if (checkRequiredField()) { 
        var Xmlcurrency;
        Xmlcurrency = '<Xmlcurrency>';
        Xmlcurrency += '<CurrencyName><![CDATA[' + $("#txtCurrencyName").val() + ']]></CurrencyName>';
        Xmlcurrency += '<CurrencyValue><![CDATA[' + $("#Currvalue").val() + ']]></CurrencyValue>';
        Xmlcurrency += '<Status><![CDATA[' + $("#ddlStatus").val() + ']]></Status>';
        Xmlcurrency += '</Xmlcurrency>';
        $.ajax({
            type: "POST",
            url: "/Admin/WebAPI/WCurrencyMaster.asmx/InsertCurrency",
            //data: "{'dataValue':'" + JSON.stringify(Xmlcurrency) +"'}",
            data: JSON.stringify({ dataValue: JSON.stringify(Xmlcurrency) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if(result.d=="Success")                {
                    swal("", "Currency details saved successfully.", "success");
                    var table = $('#tblCurrency').DataTable();
                    table.destroy();
                    resetCurrency();
                    GetCurrencyDetail();
                }
                else if(result.d=="Unauthorized access.")
                {
                    location.href="/";
                }
                else
                {
                    swal("Error!", result.d, "error");
                }
            },
            error: function (result) {
                swal("Error!", result.d, "error");               
            },
            failure: function (result) {
                swal("Error!", result.d, "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
};

/*----------------------------------------------------List Categroy Details--------------------------------------------------------*/
function GetCurrencyDetail() {
     
    var data = {
        CurrencyName: $('#txtSCurrName').val().trim(),
        Status: $('#ddlSStatus').val()
    };
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WCurrencyMaster.asmx/getCurrencyDetail",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: onSuccessOfCurrency,
        error: function (response) {
            console.log(JSON.parse(response.responseText).d);
        }
    });
}
function onSuccessOfCurrency(response) {
    var xmldoc = $.parseXML(response.d);
    var CurrencyDetails = $(xmldoc).find('Table');

    if (CurrencyDetails.length > 0) {
        $('#EmptyTable').hide();
        $('#tblCurrency tbody tr').remove();
        var row = $('#tblCurrency thead tr').clone(true);
        $.each(CurrencyDetails, function () {
            $(".CurrencyId", row).text($(this).find("AutoId").text());
            $(".CurrencyName", row).text($(this).find("CurrencyName").text());
            $(".Value", row).text($(this).find("CurrencyValue").text());
            if ($(this).find("Status").text() == 'Active') {
                $(".status", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("Status").text() + "</span>");
            } else {
                $(".status", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("Status").text() + "</span>");

            }
            $(".action", row).html("<a title='Edit' href='#'><span class='ft-edit' onclick='editCurrency(\"" + $(this).find("AutoId").text() + "\")' />&nbsp;&nbsp;</a><a title='Delete' href='#'><span class='ft-x' onclick= 'deleterecord(\"" + $(this).find("AutoId").text() + "\")' /></a>");
            $("#tblCurrency tbody").append(row);
            row = $("#tblCurrency tbody tr:last-child").clone(true);
        });
    }
    else {
        $('#tblCurrency tbody tr').remove();
        $('#EmptyTable').show();
    }

}
/*----------------------------------------------------Edit CurrencyDetails Detail---------------------------------------------------*/
function editCurrency(CurrencyId) {
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WCurrencyMaster.asmx/editCurrency",
        data: "{'CurrencyId':'" + CurrencyId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },  
        success: onSuccessOfEdit,
        error: function (response) {
            console.log(response.d)
        }
    })
}
function onSuccessOfEdit(response) {
    var xmldoc = $.parseXML(response.d);
    var CurrencyDetails = $(xmldoc).find('Table');
    $("#txtCurrId").val($(CurrencyDetails).find("AutoId").text());
    $("#txtCurrencyName").val($(CurrencyDetails).find("CurrencyName").text());
    $("#Currvalue").val($(CurrencyDetails).find("CurrencyValue").text());
    $("#ddlStatus").val($(CurrencyDetails).find("Status").text());
    $("#btnSave").hide();
    $("#btnReset").hide();
    $("#btnUpdate").show();
    $("#btnCancel").show();
}
/*----------------------------------------------------Update Currency Detail--------------------------------------------------------*/
function Update() {
    if (checkRequiredField()) {

        var Xmlcurrency;
        Xmlcurrency = '<Xmlcurrency>';      
        Xmlcurrency += '<CurrencyName><![CDATA[' + $("#txtCurrencyName").val() + ']]></CurrencyName>';
        Xmlcurrency += '<CurrencyValue><![CDATA[' + $("#Currvalue").val() + ']]></CurrencyValue>';
        Xmlcurrency += '<Status><![CDATA[' + $("#ddlStatus").val() + ']]></Status>';
        Xmlcurrency += '<Autoid><![CDATA[' + $("#txtCurrId").val() + ']]></Autoid>';
        Xmlcurrency += '</Xmlcurrency>';
    
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WCurrencyMaster.asmx/updateCurrency",
        //data: "{'dataValue':'" + JSON.stringify(Xmlcurrency) + "'}",
        data: JSON.stringify({ dataValue: JSON.stringify(Xmlcurrency) }),
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (result) {
            if(result.d=="Success")
            {
                swal("", "Currency details updated successfully.", "success");
                var table = $('#tblCurrency').DataTable();
                table.destroy();
                resetCurrency();
                GetCurrencyDetail();
            }
            else if(result.d=="Unauthorized access.")
            {
                location.href="/";
            }
            else
            {
                swal("Error!", result.d, "error");
            }
        },
        error: function (result) {
            swal("Error!", result.d, "error");
        },
        failure: function (result) {
            swal("Error!", result.d, "error");
        }
    });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
};

function resetCurrency() {
    $("#btnUpdate").hide();
    $("#btnCancel").hide();
    $("#btnSave").show();
    $("#btnReset").show();
    $("#txtCurrencyName").removeClass('border-warning');
    $("#Currvalue").removeClass('border-warning');
    $('input[type="text"]').val('');
    $('textarea').val('');
    $('option:selected').each(function () {
        $(this).attr('selected', false);
    });
    $('select option:first-child').each(function () {
        $(this).attr('selected', 'selected');
    });
    $("#ddlStatus").val('1');
}
function Cancel() {
    resetCurrency();
    $("#btnUpdate").hide();
    $("#btnCancel").hide();
    $("#btnSave").show();
    $("#btnReset").show();
};
/*--------------------------------------------------------Delete Currency Details ----------------------------------------------------------*/
function deleteCurrency(CurrencyId) {
   
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/WCurrencyMaster.asmx/deleteCurrency",
        data: "{'CurrencyId':'" + CurrencyId + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },   
        success: function (result) {
            if(result.d=="Success")
            {
                swal("", "Currency details deleted successfully.", "success");
                var table = $('#tblCurrency').DataTable();
                table.destroy();
                GetCurrencyDetail();
                resetCurrency();
            }
            else if(result.d=="Unauthorized access.")
            {
                location.href="/";
            }
            else 
            {
                swal("error", result.d, "error");
            }
        },
        error: function (result) {
            swal("error", result.d, "error");
                
        },
        failure: function (result) {
            swal("error", result.d, "error");
        }
    })   
}
function deleterecord(CurrencyId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this currency.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: false,
            },
            confirm: {
                text: "Yes, delete it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(isConfirm => {
        if (isConfirm) {
             deleteCurrency(CurrencyId);
            
} else {
             swal("", "Your currency is safe.", "error");
}
})
}