﻿
$(document).ready(function () {
    //managePrice();
    BindDropDown();
});
function readURL(input) {
    if ($(input).val() == '') {
        $("#imgProductImage").attr("src", "/images/default_pic.png");
    } else {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                document.getElementById('imgProductImage').src = e.target.result;
                $('#imgProductImage').attr('mainsrc', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
}
function succesddlBindCategory(response) {

}
function BindDropDown() {
    debugger;
    $.ajax({
        url: "/Admin/CreateNewProduct.aspx/ddlBind",
        type: 'post',
        contentType: 'application/json;charset=utf-8',
        dataType: 'json',
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            debugger;
            if (response.d != "Session Expired") {
                var DropDown = $.parseJSON(response.d);
                for (var i = 0; i < DropDown.length; i++) {
                    var AllDropDownList = DropDown[i];
                    var CategoryList = AllDropDownList.CategoryList;
                    var ddlCategoryList = $("#ddlCategory");
                    $("#ddlCategory option:not(:first)").remove();
                    for (var j = 0; j < CategoryList.length; j++) {
                        var CategoryDropList = CategoryList[j];
                        var option = $("<option />");
                        option.html(CategoryDropList.CN);
                        option.val(CategoryDropList.AutoId);
                        ddlCategoryList.append(option);
                    }
                    ddlCategoryList.select2();

                    var ddlVendor = $("#ddlVendor");
                    $("#ddlVendor option:not(:first)").remove();
                    var VenderList = AllDropDownList.VenderList;
                    for (var k = 0; k < VenderList.length; k++) {
                        var VenderDropList = VenderList[k];
                        var option = $("<option />");
                        option.html(VenderDropList.VN);
                        option.val(VenderDropList.AutoId);
                        ddlVendor.append(option);
                    }
                    ddlVendor.select2();

                    var ddlBrand = $("#ddlBrand");
                    $("#ddlBrand option:not(:first)").remove();
                    var BrandList = AllDropDownList.BrandList;
                    for (var l = 0; l < BrandList.length; l++) {
                        var BrandDropList = BrandList[l];
                        var option = $("<option />");
                        option.html(BrandDropList.BN);
                        option.val(BrandDropList.AutoId);
                        ddlBrand.append(option);
                    }
                    ddlBrand.select2();

                    var ddlCommCode = $("#ddlCommCode");
                    $("#ddlCommCode option:not(:first)").remove();
                    var CommissionList = AllDropDownList.CommissionList;
                    for (var m = 0; m < CommissionList.length; m++) {
                        var CommissionDropList = CommissionList[m];
                        var option = $("<option />");
                        option.html(CommissionDropList.CC);
                        option.val(CommissionDropList.AutoId);
                        ddlCommCode.append(option);
                    }
                    ddlCommCode.select2();
                }
            } else {
                location.href = '/';
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

//function managePrice() {
//    $('#Div1').hide();
//    $("input:radio").attr("checked", false);
//    ProductAutoId = $("#txtHProductAutoId").val();
//    $.ajax({
//        type: "POST",
//        url: "/Admin/WebAPI/RequestProductMaster.asmx/getManagePrice",
//        data: "{'ProductAutoId':'" + ProductAutoId + "'}",
//        contentType: "application/json; charset=utf-8",
//        datatype: "json",
//        beforeSend: function () {
//            $.blockUI({
//                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
//                overlayCSS: {
//                    backgroundColor: '#FFF',
//                    opacity: 0.8,
//                    cursor: 'wait'
//                },
//                css: {
//                    border: 0,
//                    padding: 0,
//                    backgroundColor: 'transparent'
//                }
//            });
//        },
//        complete: function () {
//            $.unblockUI();
//        },
//        success: function (response) {
//            if (response.d != "Session Expired") {
//                var xmldoc = $.parseXML(response.d);
//                var packingDetails = $(xmldoc).find("Table");
//                $("#Table1 tbody tr").remove();
//                var row = $("#Table1 thead tr:last-child").clone();
//                console.log(packingDetails);
//                $(packingDetails).each(function (index) {

//                    $(".Default", row).html('<input type="radio"  class="clsDefault" name="SetDefault">');
//                    $(".Free", row).html('<input type="checkbox" class="clsFree" name="SetFree">');

//                    if ($(this).find('UnitType').text() == '3') {
//                        $(".Action", row).html('<input type="checkbox" checked disabled class="clsAction" onclick="funchangeQty(this)" AutoId="' + index + '" id="tblPriceAction" name="tblAction">');
//                    } else {
//                        $(".Action", row).html('<input type="checkbox" class="clsAction" onclick="funchangeQty(this)" AutoId="' + index + '" id="tblPriceAction" name="tblAction">');
//                    }


//                    $(".UnitType", row).html($(this).find('UnitName').text() + "<span UnitType=" + $(this).find('UnitType').text() + "></span>");
//                    if ($(this).find('UnitType').text() == '3') {
//                        $(".NewQty", row).html("<input type='text' style='width:60px;text-align:center' class='form-control input-sm' onchange='funchangeprice(this)' onkeypress='return isNumberDecimalKey(event,this)' disabled  value='" + $(this).find('Qty').text() + "'/>");
//                    } else {
//                        $(".NewQty", row).html("<input type='text' style='width:60px;text-align:center' class='form-control input-sm' onchange='funchangeQty(this)' onkeypress='return isNumberDecimalKey(event,this)'  value='" + $(this).find('Qty').text() + "'/>");
//                    }

//                    if ($(this).find('UnitType').text() == '3') {
//                        $(".NewCosePrice", row).html("<input type='text' style='width:80px;text-align:right' onchange='funchangeprice(this)' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm UnitNewCost'  value='" + $(this).find('CostPrice').text() + "'/>");
//                    } else {
//                        $(".NewCosePrice", row).html("<input type='text' style='width:80px;text-align:right' onchange='funchangeprice(this)' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm'  value='" + $(this).find('CostPrice').text() + "'/>");
//                    }
//                    if ($(this).find('UnitType').text() == '3') {
//                        $(".NewBasePrice", row).html("<input type='text' style='width:80px;text-align:right' onchange='funchangeprice(this)' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm UnitBasePrice'  value='" + $(this).find('Price').text() + "'/>");
//                    } else {
//                        $(".NewBasePrice", row).html("<input type='text' style='width:80px;text-align:right' onchange='funchangeprice(this)' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm'  value='" + $(this).find('Price').text() + "'/>");
//                    }
//                    if ($(this).find('UnitType').text() == '3') {
//                        $(".NewRetailPrice", row).html("<input type='text' style='width:80px;text-align:right' onchange='funchangeprice(this)' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm UnitRetailPrice'  value='" + $(this).find('SRP').text() + "'/>");
//                    } else {
//                        $(".NewRetailPrice", row).html("<input type='text' style='width:80px;text-align:right' onchange='funchangeprice(this)' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm'  value='" + $(this).find('SRP').text() + "'/>");
//                    }
//                    if ($(this).find('UnitType').text() == '3') {
//                        $(".NWHPrice", row).html("<input type='text' style='width:80px;text-align:right'  onchange='funchangeprice(this)'  onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm UnitWHPrice'  value='" + $(this).find('WHminPrice').text() + "'/>");
//                    } else {
//                        $(".NWHPrice", row).html("<input type='text' style='width:80px;text-align:right'  onchange='funchangeprice(this)'  onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm'  value='" + $(this).find('WHminPrice').text() + "'/>");
//                    }
//                    $(".BarCode", row).html("<input type='text' class='Barcode form-control input-sm' name='Barcode' maxlength='16' value='' onblur='checkUniqueBarcode(this)'>");
//                    $(".Location", row).html("<input type='text' class='Location form-control input-sm' name='Location' >");

//                    $("#Table1 tbody").append(row);
//                    row = $("#Table1 tbody tr:last-child").clone();
//                });
//                $("#Table1 tbody tr").show();
//                $("#Table1 tbody tr").each(function () {
//                    if (parseFloat($(this).find('.CostPrice').text()) != "0.00" && parseFloat($(this).find('.BasePrice').text()) != "0.00") {

//                    }

//                    if ($(this).find('.Action input').prop('checked') == false) {
//                        $(this).find('.NewQty input').attr('disabled', true);
//                        $(this).find('.NewCosePrice input').attr('disabled', true);
//                        $(this).find('.NewBasePrice input').attr('disabled', true);
//                        $(this).find('.NewRetailPrice input').attr('disabled', true);
//                        $(this).find('.NWHPrice input').attr('disabled', true);
//                        $(this).find(".Default input").attr("disabled", true);
//                        $(this).find(".Free input").attr("disabled", true);
//                        $(this).find(".BarCode input").attr("disabled", true);
//                        $(this).find(".Location input").attr("disabled", true);
//                    }
//                });
//                if ($('#HDDomain').val() == 'psmnj' || $('#HDDomain').val() == 'psm' || $('#HDDomain').val() == 'localhost' || $('#HDDomain').val() == 'demo') {
//                    $(".Action").removeAttr("Style");
//                    $(".Default").removeAttr("Style");
//                    $(".Free").removeAttr("Style");
//                    $(".NewQty").removeAttr("Style");
//                }

//            } else {
//                location.href = '/';
//            }
//        },
//        error: function (result) {
//        },
//        failure: function (result) {
//        }
//    });
//    if ($('#Hd_Domain').val().toLowerCase() != 'psmnj' && $('#Hd_Domain').val().toLowerCase() != "psmnjm" && $('#Hd_Domain').val().toLowerCase() != "localhost" && $('#Hd_Domain').val().toLowerCase() != "demo" && $('#Hd_Domain').val().toLowerCase() != "psm") {
//        $('.QtyPop').hide();
//    }
//    $('#managePrice').modal('show');
//}