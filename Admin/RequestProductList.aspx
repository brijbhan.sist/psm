﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="RequestProductList.aspx.cs" Inherits="Admin_RequestProductList" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .zoom:hover {
            -ms-transform: scale(3); /* IE 9 */
            -webkit-transform: scale(3); /* Safari 3-8 */
            transform: scale(3);
        }

        .table tbody td {
            padding: 3px !important;
            vertical-align: middle !important;
        }

        .table th, .table td {
            padding: 3px !important;
            vertical-align: top;
            border-top: 1px solid #626E82;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Product Request List</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage Product</a></li>
                        <li class="breadcrumb-item active">Product Request List</li>
                        <input type="hidden" id="HDDomain" runat="server" />
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a class="dropdown-item" href="/Admin/RequestProductMaster.aspx" id="linkAddNewProduct" runat="server">Add New Product</a>
                    <a class="dropdown-item" href="#" id="btnBulkUpload" onclick="$('#ModalBulUpload').modal('show');" style="display: none">Bulk Upload</a>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlSCategory">
                                            <option value="0">All Category</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlSSubcategory">
                                            <option value="0">All Subcategory</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Product ID" id="txtSProductId" />
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Product Name" id="txtSProductName" />
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Barcode" id="txtBarCode" />
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlStatus">
                                            <option value="2">All Status</option>
                                            <option value="1">Pending</option>
                                            <option value="0">Created</option>
                                        </select>
                                    </div>

                                    <div class="col-md-1 form-group">
                                        <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnSearch">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-12 ">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblProductList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="action text-center">Action</td>
                                                        <td class="ProductId text-center">Product ID</td>
                                                        <td class="Category">Category</td>
                                                        <td class="Subcategory">Subcategory</td>
                                                        <td class="ProductName">Product Name</td>
                                                        <td class="BrandName">Brand</td>
                                                        <%--  <td class="Stock text-center">Stock</td>--%>
                                                        <td class="ReOrderMark text-center">Re Order Mark</td>
                                                        <td class="ImageUrl text-center">Image</td>
                                                        <td class="Location text-center">Location</td>
                                                        <td class="Status text-center">Status</td>
                                                        <td class="CommCode text-center">Comm. Code</td>
                                                        <td class="CreatedOn text-center">Created On</td>
                                                        <td class="CreatedBy text-center">Created By</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                    <div class="d-flex">
                                        <div class="form-group">
                                            <select class="form-control border-primary input-sm" id="ddlPageSize" onchange="getProductList(1)">
                                                <option value="10">10</option>
                                                <option value="50">50</option>
                                                <option value="100">100</option>
                                                <option value="500">500</option>
                                                <option value="1000">1000</option>
                                                <option value="0">All</option>
                                            </select>
                                        </div>
                                        <div class="ml-auto">
                                            <div class="Pager"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </div>
    <input type="hidden" id="hiddenForPacker" runat="server" />



    <!-- Modal -->
    <div id="ModalBulUpload" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 1172px;">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Upload File</h4>
                    <button type="button" class="close" data-dismiss="modal" onclick="resetModalBulUpload()">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4 form-group">
                            <input type="file" class="form-control" id="fileProductBulk" />
                            <span id="errorMsg" style="display: none; color: #C62828; font-weight: 700;"></span>
                            <span id="succMsg" style="display: none; color: #4CAF50; font-weight: 700;"></span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                        <table class="table table-bordered" id="tblTempProductList" style="display: none; white-space: nowrap;">
                            <thead class="bg-blue white">
                                <tr>
                                    <td class="ProductId">Product ID</td>
                                    <td class="Category">Category</td>
                                    <td class="SubCategory">Sub-Category</td>
                                    <td class="ProductName">Product Name</td>
                                    <td class="PreferVendor">Prefer Vendor</td>
                                    <td class="ReOrderMarkBox">ReOrder Mark Box</td>
                                    <td class="Unit1">Unit 1</td>
                                    <td class="Qty1">Qty</td>
                                    <td class="MinPrice1">Min Price</td>
                                    <td class="wholesalePrice1">wholesale Min Price </td>
                                    <td class="BasePrice1">Base Price</td>
                                    <td class="CostPrice1">Cost Price</td>
                                    <td class="SRP1">SRP</td>
                                    <td class="Commission1">Commission</td>
                                    <td class="Location1">Location</td>
                                    <td class="Barcode1">Barcode</td>
                                    <td class="Unit2">Unit 2</td>
                                    <td class="Qty2">Qty</td>
                                    <td class="MinPrice2">Min Price</td>
                                    <td class="wholesalePrice2">wholesale Min Price </td>
                                    <td class="BasePrice2">Base Price</td>
                                    <td class="CostPrice2">Cost Price</td>
                                    <td class="SRP2">SRP</td>
                                    <td class="Commission2">Commission</td>
                                    <td class="Location2">Location</td>
                                    <td class="Barcode2">Barcode</td>
                                    <td class="Unit3">Unit 3</td>
                                    <td class="Qty3">Qty</td>
                                    <td class="MinPrice3">Min Price</td>
                                    <td class="wholesalePrice3">wholesale Min Price </td>
                                    <td class="BasePrice3">Base Price</td>
                                    <td class="CostPrice3">Cost Price</td>
                                    <td class="SRP3">SRP</td>
                                    <td class="Commission3">Commission</td>
                                    <td class="Location3">Location</td>
                                    <td class="Barcode3">Barcode</td>
                                    <td class="D_Selling">Default Selling Unit</td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <span style="font-weight: 700; color: #FF8A80;" id="finalError"></span>
                    <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1 btn-sm" onclick="BulkUpload()" id="btnUploadBulk">Upload</button>
                    <button type="button" class="btn btn-primary btn-sm" id="btnFinalSave" style="display: none;" onclick="FinalSave()">Final Save</button>
                </div>
            </div>

        </div>
    </div>

    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="/Admin/JS/RequestProductList.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
</asp:Content>

