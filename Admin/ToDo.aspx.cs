﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DLLToDo;
public partial class Admin_ToDo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
            string text = File.ReadAllText(Server.MapPath("/Admin/JS/ToDo.js"));
            Page.Header.Controls.Add(
                new LiteralControl(
                    "<script id='checksdrivRequiredFields'>" + text + "</script>"
                    ));
        }
    }
    [WebMethod(EnableSession = true)]
    public static string SelectStatus()
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                PL_ToDo pobj = new PL_ToDo();
                BL_ToDo.SelectStatus(pobj);
                if(!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
                
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string insert(string dataValue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_ToDo pobj = new PL_ToDo();
                pobj.Title = jdv["Title"];
                pobj.Description = jdv["Description"];
                pobj.Starred = Convert.ToInt32(jdv["Starred"]);
                pobj.Important = Convert.ToInt32(jdv["Important"]);
                pobj.IsCompleted = Convert.ToInt32(jdv["IsCompleted"]);
                pobj.UserAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                pobj.Who = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                BL_ToDo.insert(pobj);
                if (!pobj.isException)
                {
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }


    [WebMethod(EnableSession = true)]
    public static string Select(string dataValue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_ToDo pobj = new PL_ToDo();
                if (jdv["FromDate"] != null && jdv["FromDate"] != "")
                {
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != null && jdv["ToDate"] != "")
                {
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                }
                pobj.Starred = Convert.ToInt32(jdv["Starred"]);
                pobj.Important = Convert.ToInt32(jdv["Important"]);
                pobj.IsCompleted = Convert.ToInt32(jdv["IsSCompleted"]);
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.UserAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                BL_ToDo.select(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }


    [WebMethod(EnableSession = true)]
    public static string edit(string AutoId)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                PL_ToDo pobj = new PL_ToDo();
                pobj.AutoId = Convert.ToInt32(AutoId);
                BL_ToDo.Edit(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }

    }

    [WebMethod(EnableSession = true)]
    public static string Delete(string AutoId)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                PL_ToDo pobj = new PL_ToDo();
                pobj.UserAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                pobj.AutoId = Convert.ToInt32(AutoId);
                BL_ToDo.MoveTrash(pobj);
                if (!pobj.isException)
                {
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string update(string dataValue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_ToDo pobj = new PL_ToDo();
                pobj.AutoId = Convert.ToInt32(jdv["AutoId"]);
                pobj.Title = jdv["Title"];
                pobj.Description = jdv["Description"];
                pobj.Starred = Convert.ToInt32(jdv["Starred"]);
                pobj.IsCompleted = Convert.ToInt32(jdv["IsCompleted"]);
                pobj.Important = Convert.ToInt32(jdv["Important"]);
                pobj.UserAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                pobj.Who = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                BL_ToDo.Update(pobj);
                if (!pobj.isException)
                {
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string CompletedTodo(string dataValue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_ToDo pobj = new PL_ToDo();
                pobj.UserAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                pobj.AutoId = Convert.ToInt32(jdv["AutoId"]);
                pobj.IsCompleted = Convert.ToInt32(jdv["IsCompleted"]);
                BL_ToDo.MarkAsCompleted(pobj);
                if (!pobj.isException)
                {
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}