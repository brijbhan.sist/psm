﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="ParameterSetting.aspx.cs" Inherits="Admin_ParameterSetting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .card .card {
            box-shadow: 0px 1px 15px 1px rgba(62, 57, 107, 0.07) !important;
        }
         .table th, .table td {
            padding: 3px !important;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Parameter Setting</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage Application</a></li>
                        <li class="breadcrumb-item active">Parameter Setting</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>


    <div class="content-body">
        <section id="basic-dropdowns">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <ul class="nav nav-tabs nav-topline">
                            <li class="nav-item">
                                <a class="nav-link active" id="base-tab21" data-toggle="tab" aria-controls="tab21" href="#tab21" aria-expanded="true">Manage State</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="base-tab22" data-toggle="tab" aria-controls="tab22" href="#tab22" aria-expanded="false">Manage City</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="base-tab23" data-toggle="tab" aria-controls="tab23" href="#tab23" aria-expanded="false">Manage Zip Code</a>
                            </li>

                        </ul>
                        <div class="tab-content px-1 pt-1 border-grey border-lighten-2 border-0-top">
                            <div role="tabpanel" class="tab-pane active" id="tab21" aria-expanded="true" aria-labelledby="base-tab21">

                                <div class="row">
                                    <div class="col-md-4 col-sm-4 panelLeft">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="card-title">State Details</h4>
                                            </div>
                                            <div class="card-content collapse show">
                                                <div class="card-body">
                                                    <div class="panel panel-default panel_dept">
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <label class="control-label">State ID</label>
                                                                </div>
                                                                <div class="col-md-7 form-group">
                                                                    <input type="text" id="txtStateCode" readonly="true" class="form-control input-sm border-primary" />
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <label class="control-label">State Name</label>
                                                                    <span class="required">*</span>
                                                                </div>
                                                                <div class="col-md-7 form-group">
                                                                    <input type="text" maxlength="200" class="form-control input-sm border-primary req text-capitalize" id="txtStateName_State" runat="server" />
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <label class="control-label">Abbreviation </label>
                                                                    <span class="required">*</span>
                                                                </div>
                                                                <div class="col-md-7 form-group">
                                                                    <input type="text" id="txtAbbreviation_State" maxlength="2" class="form-control input-sm border-primary req" runat="server" />
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <label class="control-label">Status</label>
                                                                </div>
                                                                <div class="col-md-7 form-group">
                                                                    <select id="ddlStatus_State" class="form-control input-sm border-primary">
                                                                        <option value="1" selected="selected">Active</option>
                                                                        <option value="0">Inactive</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12 text-right">
                                                                    <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1 btn-sm mr-1" id="btnSave">Save</button>
                                                                    <button type="button" class="btn btn-secondary buttonAnimation pull-right round box-shadow-1 btn-sm mr-1" id="btnReset">Reset</button>
                                                                    <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1 btn-sm mr-1" id="btnUpdate" style="display: none">Update</button>
                                                                    <button type="button" class="btn btn-warning buttonAnimation pull-right round box-shadow-1 btn-sm mr-1" id="btnCancel" style="display: none">Cancel</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-sm-8 panelRight">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="card-title">State List</h4>
                                            </div>
                                            <div class="card-content collapse show">
                                                <div class="card-body">
                                                    <div class="panel panel-default panel_dept">
                                                        <div class="panel-body">
                                                            <div class="panel panel-default">
                                                                <div class="panel-body">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="row">
                                                                                <div class="form-group col-md-5">
                                                                                    <input type="text" class="form-control input-sm border-primary" placeholder="State Name" id="txtSNameSearch_State" />
                                                                                </div>
                                                                                <div class="form-group col-md-5">
                                                                                    <select class="form-control input-sm border-primary" id="ddlSStatusSearch_State">
                                                                                        <option value="2" selected="selected">All</option>
                                                                                        <option value="1">Active</option>
                                                                                        <option value="0">Inactive</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1 btn-sm" id="btnSearch_State">Search</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <br />
                                                            <div class="table-responsive">
                                                                <table class="table table-striped table-bordered" id="tblState">
                                                                    <thead class="bg-blue white">
                                                                        <tr>
                                                                            <td class="Saction text-center width3per">Action</td>
                                                                            <td class="SStateName">State Name</td>
                                                                            <td class="SAbbreviation text-center width-5-per">Abbreviation</td>
                                                                            <td class="Sstatus text-center width3per">Status</td>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    </tbody>
                                                                </table>
                                                                <h5 class="well text-center" id="EmptyTableState" style="display: none">No data available.</h5>
                                                            </div>
                                                            <div class="Pager" id="PagerState"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab22" aria-labelledby="base-tab22">
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 panelLeft">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="card-title">City Details</h4>
                                            </div>
                                            <div class="card-content collapse show">
                                                <div class="card-body">
                                                    <div class="panel panel-default panel_dept">
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <label class="control-label">City ID</label>
                                                                </div>
                                                                <div class="col-md-7 form-group">
                                                                    <input type="text" id="txtCityCode_City" readonly="true" class="form-control input-sm border-primary" />
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <label class="control-label">State Name</label>
                                                                    <span class="required">*</span>
                                                                </div>
                                                                <div class="col-md-7 form-group">
                                                                    <select id="ddlState_City" class="form-control input-sm border-primary ddlreqc" runat="server" style="width: 100% !important">
                                                                        <option selected="selected" value="0">-Select-</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <label class="control-label">City</label>
                                                                    <span class="required">*</span>
                                                                </div>
                                                                <div class="col-md-7 form-group">
                                                                    <input type="text" id="txtCity_City" maxlength="100" class="form-control input-sm border-primary reqc" runat="server" />
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <label class="control-label">Status</label>
                                                                </div>
                                                                <div class="col-md-7 form-group">
                                                                    <select id="ddlStatus_City" class="form-control input-sm border-primary">
                                                                        <option value="1" selected="selected">Active</option>
                                                                        <option value="0">Inactive</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12 text-right">
                                                                    <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1 btn-sm mr-1" id="btnSave_City">Save</button>
                                                                    <button type="button" class="btn btn-secondary buttonAnimation pull-right round box-shadow-1 btn-sm mr-1" id="btnReset_City">Reset</button>
                                                                    <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1 btn-sm mr-1" id="btnUpdate_City" style="display: none">Update</button>
                                                                    <button type="button" class="btn btn-warning buttonAnimation pull-right round box-shadow-1 btn-sm mr-1" id="btnCancel_City" style="display: none">Cancel</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-sm-8 panelRight">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="card-title">City List</h4>
                                            </div>
                                            <div class="card-content collapse show">
                                                <div class="card-body">
                                                    <div class="panel panel-default panel_dept">
                                                        <div class="panel-body">
                                                            <div class="panel panel-default">
                                                                <div class="panel-body">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="row">

                                                                                <div class="form-group col-md-3">
                                                                                    <select class="form-control input-sm border-primary" id="ddlStateSearch_City" style="width: 100% !important">
                                                                                        <option selected="selected" value="0">All State</option>
                                                                                    </select>
                                                                                </div>

                                                                                <div class="form-group col-md-3">
                                                                                    <input type="text" class="form-control input-sm border-primary" placeholder="City Name" id="txtCitySearch_City" />
                                                                                </div>
                                                                                <div class="form-group col-md-3">
                                                                                    <select class="form-control input-sm border-primary" id="ddlStatusSearch_City">
                                                                                        <option value="2" selected="selected">All</option>
                                                                                        <option value="1">Active</option>
                                                                                        <option value="0">Inactive</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1 btn-sm" id="btnSearch_City">Search</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <br>
                                                            <div class="table-responsive">
                                                                <table class="table table-striped table-bordered" id="tblCity">
                                                                    <thead class="bg-blue white">
                                                                        <tr>
                                                                            <td class="Caction text-center width3per">Action</td>
                                                                            <td class="CId hidden text-center width4per ">City ID</td>
                                                                            <td class="CStateName">State Name</td>
                                                                            <td class="CCityName">City Name</td>
                                                                            <td class="Cstatus text-center width3per">Status</td>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    </tbody>
                                                                </table>
                                                                <h5 class="well text-center" id="EmptyTableCity" style="display: none">No data available.</h5>
                                                            </div>
                                                            <div class="Pager" id="PagerCity"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="tab-pane" id="tab23" aria-labelledby="base-tab23">
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 panelLeft">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="card-title">Zip Code Details</h4>
                                            </div>
                                            <div class="card-content collapse show">
                                                <div class="card-body">
                                                    <div class="panel panel-default panel_dept">
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <label class="control-label">ID</label>
                                                                </div>
                                                                <div class="col-md-7 form-group">
                                                                    <input type="text" id="txtZipId" class="form-control input-sm border-primary" readonly="true" />
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <label class="control-label">State Name</label>
                                                                    <span class="required">*</span>
                                                                </div>
                                                                <div class="col-md-7 form-group">
                                                                    <select id="ddlState_Zip" class="form-control input-sm border-primary ddlreqz" runat="server" onchange="bindSubcategory();" style="width: 100% !important">
                                                                        <option selected="selected" value="0">-Select-</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <label class="control-label">City Name</label>
                                                                    <span class="required">*</span>

                                                                </div>
                                                                <div class="col-md-7 form-group">
                                                                    <select id="ddlCity_Zip" class="form-control input-sm border-primary ddlreqz" runat="server" style="width: 100% !important">
                                                                        <option selected="selected" value="0">-Select-</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <label class="control-label">Zip Code </label>
                                                                    <span class="required">*</span>
                                                                </div>
                                                                <div class="col-md-7 form-group">
                                                                    <input type="text" id="txtZipCode_Zip" class="form-control input-sm border-primary reqz" maxlength="5" runat="server" onkeypress="return isNumberKey(event)" />
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <label class="control-label">Status</label>
                                                                </div>
                                                                <div class="col-md-7 form-group">
                                                                    <select id="ddlStatus_Zip" class="form-control input-sm border-primary">
                                                                        <option value="1" selected="selected">Active</option>
                                                                        <option value="0">Inactive</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12 text-right">
                                                                    <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1 btn-sm mr-1" id="btnSave_Zip">Save</button>
                                                                    <button type="button" class="btn btn-secondary buttonAnimation pull-right round box-shadow-1 btn-sm mr-1" id="btnReset_Zip">Reset</button>
                                                                    <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1 btn-sm mr-1" id="btnUpdate_Zip" style="display: none">Update</button>
                                                                    <button type="button" class="btn btn-warning buttonAnimation pull-right round box-shadow-1 btn-sm mr-1" id="btnCancel_Zip" style="display: none">Cancel</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-sm-8 panelRight">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="card-title">Zip Code List</h4>
                                            </div>
                                            <div class="card-content collapse show">
                                                <div class="card-body">
                                                    <div class="panel panel-default panel_dept">
                                                        <div class="panel-body">
                                                            <div class="panel panel-default">
                                                                <div class="panel-body">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="row">
                                                                                <div class="form-group col-md-3">
                                                                                    <select class="form-control input-sm border-primary" id="ddlStateSearch_Zip" runat="server" onchange="SbindSubcategory();" style="width: 100%">
                                                                                        <option selected="selected" value="0">All State</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="form-group col-md-3">
                                                                                    <select class="form-control input-sm border-primary" id="ddlCitySearch_Zip" runat="server" style="width: 100%">
                                                                                        <option selected="selected" value="0">All City</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="form-group col-md-2">
                                                                                    <input type="text" class="form-control input-sm border-primary" maxlength="5" placeholder="Zip Code" id="txtZipCodeSearch" onkeypress="return isNumberKey(event);" style="padding: 0; text-align: center;" />
                                                                                </div>
                                                                                <div class="form-group col-md-2">
                                                                                    <select class="form-control input-sm border-primary" id="ddlSStatus">
                                                                                        <option value="2" selected="selected">All</option>
                                                                                        <option value="1">Active</option>
                                                                                        <option value="0">Inactive</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1 btn-sm" id="btnSearch_Zip">Search</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <br />
                                                            <div class="table-responsive">
                                                                <table class="table table-striped table-bordered" id="tblZip">
                                                                    <thead class="bg-blue white">
                                                                        <tr>
                                                                            <td class="Caction text-center width3per">Action</td>
                                                                            <td class="CId hidden width3per">Zip ID</td>
                                                                            <td class="CStateName">State Name</td>
                                                                            <td class="CCityName">City Name</td>
                                                                            <td class="CZip text-center width3per">Zip Code</td>
                                                                            <td class="Cstatus text-center width3per">Status</td>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    </tbody>
                                                                </table>
                                                                <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                                            </div>
                                                            <div class="Pager" id="PagerZip"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="modal fade" id="ParameterSettingPopup" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Message</h4>
                </div>
                <div class="modal-body">
                    <div style="border-color: #ac2925; border: 1px solid; color: #ac2925; min-height: 45px; line-height: 20px; padding: 9px;">
                        <strong id="ParameterSettingMsg"></strong>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="JS/ParameterSetting.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>

</asp:Content>

