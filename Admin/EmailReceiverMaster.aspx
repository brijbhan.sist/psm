﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="EmailReceiverMaster.aspx.cs" Inherits="Admin_EmailReceiverMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Email Receiver</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                         <li class="breadcrumb-item"><a href="#">Manage Application</a>
                        </li>
                         <li class="breadcrumb-item">Email Receiver Master</li>
                    </ol>
                </div>
            </div>
        </div> 
    </div>

    <div class="content-body">
        <section id="drag-area1">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-header">
                                <h4 class="card-text">Email Receiver Details</h4>
                                <input type="hidden" id="hfautoID" />
                            </div>
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-2">
                                        <label class="control-label">Employee&nbsp;<span class="required">*</span></label>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <select id="ddlempid" onchange="bindAgentbyId()" class="form-control border-primary input-sm ddlreq">
                                              <option value="0">-Select-</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-2">
                                        <label class="control-label">Employee Name</label>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <input type="text" id="txtempname" readonly="true" class="form-control border-primary input-sm" />
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-2">
                                        <label class="control-label">Email ID</label>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <input type="text" id="txtEmailID" readonly="true" class="form-control border-primary input-sm" />
                                    </div>
                                    <div class="col-sm-12 col-md-2">
                                        <label class="control-label">Category&nbsp;<span class="required">*</span></label>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <select id="ddlcategory" class="form-control border-primary input-sm ddlreq">
                                            <option value="0">-Select-</option>
                                            <option>Client</option>
                                            <option>Developer</option>
                                            <option>Internal</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-12">
                                        <button type="button" class="btn btn-success buttonAnimation round box-shadow-1  btn-sm pull-right" style="margin:0 5px" id="btnSave">Save</button>                                                                             
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm pull-right" style="margin:0 5px;display:none" id="btnUpdate">Update</button>
                                        <button type="button" class="btn btn-secondary buttonAnimation round box-shadow-1  btn-sm pull-right" style="margin:0 5px" onclick="resetData()" id="btnClear">Reset</button> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </section>
    </div>
     <div class="content-body">
        <section id="drag-area2">
            <div class="row">
                              
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-header">
                                <h4 class="card-text">Email Receiver List</h4>
                            </div>
                            <div class="card-body">
                                 <div class="row form-group ">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblEmailRecieverList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="Action text-center">Action</td>      
                                                        <td class="EmpName">Employee Name</td>
                                                        <td class="EmailID">Email ID</td>
                                                        <td class="Category">Category</td>                                            
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="Pager"></div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </section>
    </div>
       <script type="text/javascript">
           document.write('<scr' + 'ipt type="text/javascript" src="/Admin/JS/EmailReciever.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
</asp:Content>

