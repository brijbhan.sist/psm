﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="WeightTaxMaster.aspx.cs" Inherits="Admin_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
       <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Manage Weight Tax</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage Tax</a></li>
                        <li class="breadcrumb-item">Manage Weight Tax</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
   <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Weight Tax Details</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Weight Tax ID</label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <input type="text" class="form-control border-primary input-sm" id="txtTaxId" readonly="readonly" />
                                            </div>

                                        </div>
                                    </div>
                                     <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">
                                                   Weight Tax Value <span class="required">*</span>
                                                </label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <input type="text" class="form-control border-primary input-sm req" id="TxtRate" runat="server" onkeypress="return isNumberDecimalKey(event,this)" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Print Label Text <span class="required">*</span> </label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <input type="text" class="form-control border-primary input-sm req" id="txtPrintLabel"/>
                                            </div>

                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" class="btn btn-secondary  buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnReset">Reset</button>
                                    </div>
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnEdit">Edit</button>
                                    </div>
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnUpdate" style="display: none">Update</button>
                                    </div>
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" class="btn btn-warning buttonAnimation pull-right round box-shadow-1 btn-sm" id="btnCancel" data-animation="pulse" style="display: none">Cancel</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
       <script type="text/javascript">
           document.write('<scr' + 'ipt type="text/javascript" src="/Admin/JS/WeightTaxMaster.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
</asp:Content>

