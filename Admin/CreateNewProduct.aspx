﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="CreateNewProduct.aspx.cs" Inherits="Admin_CreateNewProduct" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Create New Product</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage Product</a></li>
                        <li class="breadcrumb-item active">Create New Product</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a class="dropdown-item" href="/Admin/RequestProductMaster.aspx" id="addProduct" runat="server">Add New Product</a>
                    <a class="dropdown-item" href="RequestProductList.aspx" id="A3">Product Request List </a>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <div class="col-md-5 col-sm-5 col-xs-12">
                                                        <label class="control-label">Category</label>
                                                        <span class="required">*</span>
                                                    </div>
                                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                                        <select class="form-control border-primary input-sm ddlreq" id="ddlCategory" runat="server">
                                                            <option value="Choose Category"></option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                                        <label class="control-label">Subcategory</label>
                                                        <span class="required">*</span>
                                                    </div>
                                                    <div class="col-md-7 col-sm-7 col-xs-12 form-group">
                                                        <select class="form-control border-primary input-sm ddlreq" id="ddlSubcategory" runat="server" disabled="disabled">
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <div class="col-md-5 col-sm-5 col-xs-12">
                                                        <label class="control-label">Product ID</label>
                                                        <span class="required">*</span>
                                                    </div>
                                                    <div class="col-md-7 col-sm-7 col-xs-12 form-group">
                                                        <input type="text" class="form-control border-primary input-sm req" maxlength="8" id="txtProductId" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                                        <label class="control-label">Product Name</label>
                                                        <span class="required">*</span>
                                                    </div>
                                                    <div class="col-md-7 col-sm-7 col-xs-12 form-group">
                                                        <input type="text" class="form-control border-primary input-sm req" maxlength="50" id="txtProductName" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <div class="col-md-5 col-sm-5 col-xs-12">
                                                        <label class="control-label">Choose Vendor</label>
                                                    </div>
                                                    <div class="col-md-7 col-sm-7 col-xs-12 form-group">
                                                        <select class="form-control border-primary input-sm ddlreq" id="ddlVendor">
                                                            <option value="0">Choose an Vender</option>
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                                        <label class="control-label">Product Image</label>
                                                    </div>
                                                    <div class="col-md-7 col-sm-7 col-xs-12 form-group">
                                                        <input type="file" name="img" accept="img/*" id="fileImageUrl"  onchange='readURL(this)' class="form-control border-primary"  runat="server"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                             <div class="col-md-6">
                                                <div class="form-group row">
                                                    <div class="col-md-5 col-sm-5 col-xs-12">
                                                        <label class="control-label">
                                                            Brand <span class="required">*</span>  
                                                        </label>
                                                    </div>
                                                    <div class="col-md-7 col-sm-7 col-xs-12 form-group">
                                                        <select id="ddlBrand" class="form-control border-primary input-sm">
                                                            <option value="0">Choose an Brand</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <div class="col-md-4 col-sm-5 col-xs-12">
                                                        <label class="control-label">
                                                            Commission Code   
                                                        </label>
                                                    </div>
                                                    <div class="col-md-7 col-sm-7 col-xs-12 form-group">
                                                        <select id="ddlCommCode" onchange="IsNumeric()" class="form-control border-primary input-sm">
                                                            <option value="0">Choose an Commission Code</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <div class="col-md-5 col-sm-5 col-xs-12">
                                                        <label class="control-label">Is Apply ML Quantity</label>
                                                    </div>
                                                    <div class="col-md-7 col-sm-7 col-xs-12 form-group">
                                                        <input type="checkbox" id="cbMlQuantity" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                                        <label class="control-label">ML Quantity</label>
                                                    </div>
                                                    <div class="col-md-7 col-sm-7 col-xs-12 form-group">
                                                        <input type="text" class="form-control border-primary input-sm" maxlength="5" id="txtMLQty" value="0.00" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <div class="col-md-5 col-sm-5 col-xs-12">
                                                        <label class="control-label">Is Apply Weight (OZ)</label>
                                                    </div>
                                                    <div class="col-md-7 col-sm-7 col-xs-12 form-group">
                                                        <input type="checkbox" id="CbWeightQuantity" runat="server" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                                        <label class="control-label">Weight (Oz)</label>
                                                    </div>
                                                    <div class="col-md-7 col-sm-7 col-xs-12 form-group">
                                                        <input type="text" class="form-control border-primary input-sm" maxlength="5" id="txtWeightOz" value="0.00" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <div class="col-md-5 col-sm-5 col-xs-12">
                                                        <label class="control-label">Re Order Mark (Pieces)</label>
                                                    </div>
                                                    <div class="col-md-7 col-sm-7 col-xs-12 form-group">
                                                        <input type="text" class="form-control border-primary input-sm" maxlength="3" id="txtReOrderMark" runat="server" />
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                                        <label class="control-label">
                                                            SRP<span class="required">&nbsp*</span>
                                                        </label>

                                                    </div>
                                                    <div class="col-md-7 col-sm-7 col-xs-12 form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                                    <span>$</span>
                                                                </span>
                                                            </div>
                                                            <input type="text" class="form-control border-primary input-sm req" maxlength="5" id="Text1" style="text-align: right; white-space: nowrap" runat="server" placeholder="0.00" onfocus="this.select()" value="0.00" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-2">
                                        <img id="imgProductImage" src="/images/default_pic.png" class="imagePreview form-control border-primary input-sm" width="170" />
                                        <br />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="py-2 px-1">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered" id="Table1">
                                        <thead class="bg-blue white">
                                            <tr>
                                                <td class="Action text-center" rowspan="2" style="width: 5%;">Enable</td>
                                                <td class="Default text-center" rowspan="2" style="width: 5%;">Set Default</td>
                                                <td class="Free text-center" rowspan="2" style="width: 5%;">Free ?</td>
                                                <td class="UnitType" rowspan="2" style="width: 15%; text-align: center;">Unit Type</td>
                                                <td class="NewQty price" rowspan="2">Qty</td>
                                                <td colspan="4">Price</td>
                                                <td class="BarCode text-center" rowspan="2">Barcode</td>
                                                <td class="Location" rowspan="2">Location</td>
                                            </tr>
                                            <tr>
                                                <td class="NewCosePrice" style="width: 25%;">Cost</td>
                                                <td class="NWHPrice" style="width: 25%;">WH. Min</td>
                                                <td class="NewRetailPrice" style="width: 25%;">Retail Min</td>
                                                <td class="NewBasePrice" style="width: 25%;">Base</td>

                                            </tr>
                                            <tr style="display: none">
                                                <td class="Action text-center" style="width: 5%;">Enable</td>
                                                <td class="Default text-center" style="width: 5%;">Set Default</td>
                                                <td class="Free text-center" style="width: 5%;">Free ?</td>
                                                <td class="UnitType" style="width: 15%; text-align: center;">Unit Type</td>
                                                <td class="NewQty price">Qty</td>
                                                <td class="NewCosePrice" style="width: 25%;">Cost</td>
                                                <td class="NWHPrice" style="width: 25%;">WH. Min</td>
                                                <td class="NewRetailPrice" style="width: 25%;">M. Retail</td>
                                                <td class="NewBasePrice" style="width: 25%;">Base</td>
                                                <td class="BarCode text-center">Barcode</td>
                                                <td class="Location">Location</td>
                                            </tr>
                                        </thead>

                                       <tbody>
                                       </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="card-footer">
            <div class="row">
                <div class="col-md-12">
                    <div class="btn-group mr-1 pull-right">
                        <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnSave"<%-- onclick="Save()--%>">Save</button>
                    </div>
                    <div class="btn-group mr-1 pull-right">
                        <button type="button" class="btn btn-secondary  buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnReset1">Reset</button>
                    </div>

                    <div class="btn-group mr-1 pull-right">
                        <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnUpdate1" style="margin-top: 10px; display: none">Update</button>
                    </div>
                    <div class="btn-group mr-1 pull-right">
                        <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnCreateProduct" style="margin-top: 10px; display: none">Create Product</button>
                    </div>
                    <div class="btn-group mr-1 pull-right">
                        <button type="button" class="btn btn-warning buttonAnimation pull-right round box-shadow-1 btn-sm" id="btnCancel" data-animation="pulse" style="display: none">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="JS/CreateNewtProduct.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
   
</asp:Content>

