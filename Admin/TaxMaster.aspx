﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" ClientIDMode="Static" CodeFile="TaxMaster.aspx.cs" Inherits="Admin_TaxMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .table th, .table td {
            padding: 3px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  
        <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Manage Tax</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage Application</a></li>
                        <li class="breadcrumb-item active">Manage Tax</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Tax Details</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Tax ID</label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <input type="text" class="form-control border-primary input-sm" id="txtTaxId" readonly="readonly" />
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">
                                                    State <span class="required">*</span>
                                                </label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <select type="text" class="form-control border-primary input-sm ddlreq" id="ddlState" runat="server">
                                                    <option value="0">-Select-</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">
                                                    Tax Name <span class="required">*</span>
                                                </label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <input type="text" class="form-control border-primary input-sm req" id="txtTaxName" runat="server" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">
                                                    Tax <span class="required">*</span>
                                                </label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <div class="input-group ">
                                                    <input type="text" class="form-control border-primary input-sm req text-right" id="Txtvalue" runat="server" onkeypress="return isNumberDecimalKey(event,this)" />
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                                           %
                                                        </span>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Status</label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <select class="form-control border-primary input-sm" id="ddlStatus">
                                                    <option value="1" selected="selected">Active</option>
                                                    <option value="0">Inactive</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                     <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">
                                                    Print Label Text <span class="required">*</span>
                                                </label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <input type="text" class="form-control border-primary input-sm req" id="txtPrintLabel" runat="server"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnSave">Save</button>
                                    </div>
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" class="btn btn-secondary  buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnReset">Reset</button>
                                    </div>
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnUpdate" style="display: none">Update</button>
                                    </div>
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" class="btn btn-warning buttonAnimation pull-right round box-shadow-1 btn-sm" id="btnCancel" data-animation="pulse" style="display: none">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title mr-0">Tax List</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 form-group">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Tax ID" id="txtSTaxId" />
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Tax Name" id="txtSTaxName" />
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlSStatus">
                                            <option value="2">All</option>
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <button type="button" class="btn btn-info buttonAnimation pull-left round box-shadow-1  btn-sm" data-animation="pulse" id="btnSearch">Search</button>
                                    </div>

                                </div>
                                <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered" id="tblTax">
                                            <thead class="bg-blue white">
                                                <tr>
                                                    <td class="action text-center width3per">Action</td>
                                                    <td class="status text-center width3per">Status</td>
                                                    <td class="TaxId text-center width3per">Tax ID</td>
                                                    <td class="StateName">State Name</td>
                                                    <td class="TaxName">Tax Name</td>
                                                    <td class="Value price width3per">Tax</td>
                                                    <td class="PrintLabel text-center">Print Label Text</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                        <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="/Admin/JS/TaxMaster.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
</asp:Content>
