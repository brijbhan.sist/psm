﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Admin/MasterPage.master" CodeFile="ManageIP.aspx.cs" Inherits="Admin_ManageIP" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Manage IP</h3>
            <div class="row breadcrumbs-top">   
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Applications</a>
                        </li>
                        <li class="breadcrumb-item">Manage IP</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body">
        <section id="drag-area1">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-header">
                                <h4 class="card-text">IP Details</h4>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">IP Address <span class="required">*</span></label>
                                            </div>
                                            <div class="col-md-7 col-sm-7 col-xs-12 form-group">
                                                <input type="text" class="form-control border-primary input-sm req" id="txtipaddress"  runat="server"   />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <div class="col-md-2 col-sm-4 col-xs-12">
                                                <label class="control-label">Description</label>
                                            </div>
                                            <div class="col-md-7 col-sm-7 col-xs-12 form-group">
                                                <textarea id="txtDescription" maxlength="500" class="form-control border-primary input-sm" rows="5" runat="server"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" value="" id="hiddenAutoId">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Is Default IP</label>
                                            </div>
                                            <div class="col-md-7 col-sm-7 col-xs-12 form-group">
                                                <input type="checkbox" id="isDefault" runat="server" /><span style="color:red;margin-left:70px"><i>[ Note : Description upto 500 characters ]</i></span>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <div class="card-footer">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-12">
                                        <button type="button" class="btn btn-success buttonAnimation round box-shadow-1  btn-sm pull-right" style="margin: 0 5px" id="btnSave" onclick="svaeIpAddress()">Save</button>
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm pull-right" style="margin: 0 5px; display: none" id="btnUpdate" onclick="updateIpAddress()">Update</button>
                                        <button type="button" class="btn btn-secondary buttonAnimation round box-shadow-1  btn-sm pull-right" style="margin: 0 5px" onclick="resetData()" id="btnClear">Reset</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="content-body">
        <section id="drag-area2">
            <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-header">
                                <h4 class="card-text">IP List</h4>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 form-group">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="IP Address" id="txtSIpAddresss" />
                                    </div>
                                    <div class="col-md-3  form-group">
                                        <button type="button" class="btn btn-info buttonAnimation pull-left round box-shadow-1  btn-sm" id="btnSearch" onclick="getIPList(1)">Search</button>
                                    </div>
                                </div>
                                <div class="row form-group ">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblIpList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="Action text-center">Action</td>
                                                        <td class="IPAddress text-center">IP Address</td>
                                                        <td class="DefaultIp text-center">Is Defalt IP</td>
                                                        <td class="CreateDate text-center">Create Date</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row container-fluid">
                                    <div class="">
                                        <div class="form-group">
                                            <select class="form-control border-primary input-sm" id="ddlPageSize" onchange="getIPList(1)">

                                                <option value="10">10</option>
                                                <option value="20">20</option>
                                                <option value="50">50</option>
                                                <option value="100">100</option>
                                                <option value="0">All</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

</asp:Content>
