﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="StatusMaster.aspx.cs" Inherits="Admin_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <div class="content-header row">
         <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title"> Status List</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Manage Application</a>
                        </li>
                         <li class="breadcrumb-item">Manage Status</li>
                    </ol>
                </div>
            </div>
        </div>
        <%-- <div class="content-header-right col-md-6 col-12">
          <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
            <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
            id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false"> Action</button>
              <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                  <button type="button" class="dropdown-item" onclick=" location.href='/Admin/CarDetails.aspx'"id="btnAdd">Add New Car</button>
              </div>
          </div>
        </div>--%>
       
    </div>

     <%--<div class="content-body">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                 <div class="row form-group">
                                    <div class="col-md-3 form-group">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Car Name" id="txtScarName" />
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" id="btnSearch" data-animation="pulse" onclick="BindPriceLevel(1)">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>--%>

    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card"> 
                        <div class="card-content collapse show">
                            <div class="card-body">                               
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered" id="tblStatusList">
                                        <thead class="bg-blue white">
                                            <tr>
                                            <%--    <td class="Action text-center">Action</td>--%>
                                                <td class="StatusType text-center">Status Type</td>
                                                <td class="StatusCategory text-center">Status Category</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                    <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                </div>
                                <div class="Pager"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="/Admin/JS/StatusMaster.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
</asp:Content>

