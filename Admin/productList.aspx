﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="productList.aspx.cs" Inherits="Admin_productList" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .zoom:hover {
            -ms-transform: scale(3); /* IE 9 */
            -webkit-transform: scale(3); /* Safari 3-8 */
            transform: scale(3); 
        }
        .table th, .table td {
            padding: 4px !important;
        }
        .table tbody td {
    padding: 4px !important;
    vertical-align: middle !important;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Product List</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage Product</a></li>
                        <li class="breadcrumb-item active">Product List</li>
                        <input type="hidden" id="HDDomain" runat="server" />
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a class="dropdown-item" href="/Admin/productMaster.aspx" id="linkAddNewProduct" runat="server">Add New Product</a>
                    <a class="dropdown-item" href="#" id="btnBulkUpload" onclick="$('#ModalBulUpload').modal('show');">Bulk Upload</a>
                    <a class="dropdown-item" href="#" onclick="PrintProductAllBarcode()" id="A1" runat="server">Barcode Bulk Print</a>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlSCategory" onchange="bindSubcategory()">
                                            <option value="0">All Category</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlSSubcategory">
                                            <option value="0">All Subcategory</option>
                                        </select>
                                    </div>  <div class="col-md-3 form-group">
                                        <select class="form-control border-primary input-sm" style="width:100% !important" id="ddlBrand">
                                            <option value="0">All Brand</option>
                                        </select>
                                    </div>
                                       <div class="col-md-3 form-group">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Location" id="txtSLocation" />
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Product ID" id="txtSProductId" />
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Product Name" id="txtSProductName" />
                                    </div>
                                 
                                    <div class="col-md-3 form-group">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Barcode" id="txtBarCode" />
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlStatus">
                                            <option value="2">All Status</option>
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlSearch">
                                            <option value="0">All</option>
                                            <option value="=">=</option>
                                            <option value=">">></option>
                                            <option value="<"><</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Stock in Piece" id="txtStock" />
                                    </div>
                                    <div class="col-md-1 form-group">
                                        <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" onclick="getProductList(1);" id="btnSearch">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-12 ">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblProductList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <%
                                                            if (Session["EmpTypeNo"] != null)
                                                            {
                                                                if (Session["EmpTypeNo"].ToString() == "1" || Session["EmpTypeNo"].ToString() =="9")
                                                                {

                                                        %>

                                                        <td class="action text-center width2per">Action</td>
                                                        <%
                                                                }
                                                            }
                                                        %>
                                                       
                                                        <td class="ProductId text-center width3per">Product ID</td>
                                                        <td class="Category">Category</td>
                                                        <td class="Subcategory">Subcategory</td>
                                                        <td class="ProductName">Product Name</td>
                                                        <td class="BrandName">Brand</td>
                                                        <td class="Stock text-center width2per">Stock</td>
                                                        <td class="ReOrderMark text-center width3per">Re Order Mark</td>
                                                        <td class="ImageUrl text-center width2per">Image</td>
                                                        <td class="Status text-center width2per">Status</td>
                                                        <td class="CommCode text-center width3per">Comm.Code</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-1">
                                    <div class="col-md-2">
                                        <select class="form-control border-primary input-sm" id="ddlPageSize" onchange="getProductList(1)">
                                            <option value="10">10 </option>
                                            <option value="50">50</option>
                                            <option value="100">100 </option>
                                            <option value="500">500</option>
                                            <option value="1000">1000 </option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager text-right" id="ProductPager"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>
    <input type="hidden" id="hiddenForPacker" runat="server" />
    <input type="hidden" value="" id="ProductStockId" runat="server" />



    <!-- Modal -->
    <div id="ModalBulUpload" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 1172px;">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Upload File</h4>
                    <button type="button" class="close" data-dismiss="modal" onclick="resetModalBulUpload()">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4 form-group">
                            <input type="file" class="form-control" id="fileProductBulk" />
                            <span id="errorMsg" style="display: none; color: #C62828; font-weight: 700;"></span>
                            <span id="succMsg" style="display: none; color: #4CAF50; font-weight: 700;"></span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                        <table class="table table-bordered" id="tblTempProductList" style="display: none; white-space: nowrap;">
                            <thead class="bg-blue white">
                                <tr>
                                    <td class="ProductId">Product ID</td>
                                    <td class="Category">Category</td>
                                    <td class="SubCategory">Sub-Category</td>
                                    <td class="ProductName">Product Name</td>
                                    <td class="PreferVendor">Prefer Vendor</td>
                                    <td class="ReOrderMarkBox">ReOrder Mark Box</td>
                                    <td class="Unit1">Unit 1</td>
                                    <td class="Qty1">Qty</td>
                                    <td class="MinPrice1">Min Price</td>
                                    <td class="wholesalePrice1">wholesale Min Price</td>
                                    <td class="BasePrice1">Base Price</td>
                                    <td class="CostPrice1">Cost Price</td>
                                    <td class="SRP1">SRP</td>
                                    <td class="Commission1">Commission</td>
                                    <td class="Location1">Location</td>
                                    <td class="Barcode1">Barcode</td>
                                    <td class="Unit2">Unit 2</td>
                                    <td class="Qty2">Qty</td>
                                    <td class="MinPrice2">Min Price</td>
                                    <td class="wholesalePrice2">wholesale Min Price</td>
                                    <td class="BasePrice2">Base Price</td>
                                    <td class="CostPrice2">Cost Price</td>
                                    <td class="SRP2">SRP</td>
                                    <td class="Commission2">Commission</td>
                                    <td class="Location2">Location</td>
                                    <td class="Barcode2">Barcode</td>
                                    <td class="Unit3">Unit 3</td>
                                    <td class="Qty3">Qty</td>
                                    <td class="MinPrice3">Min Price</td>
                                    <td class="wholesalePrice3">wholesale Min Price</td>
                                    <td class="BasePrice3">Base Price</td>
                                    <td class="CostPrice3">Cost Price</td>
                                    <td class="SRP3">SRP</td>
                                    <td class="Commission3">Commission</td>
                                    <td class="Location3">Location</td>
                                    <td class="Barcode3">Barcode</td>
                                    <td class="D_Selling">Default Selling Unit</td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <%--<div class="PagerTemp"></div>--%>
                </div>
                <div class="modal-footer">
                    <span style="font-weight: 700; color: #FF8A80;" id="finalError"></span>
                    <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1 btn-sm" onclick="BulkUpload()" id="btnUploadBulk">Upload</button>
                    <button type="button" class="btn btn-primary btn-sm" id="btnFinalSave" style="display: none;" onclick="FinalSave()">Final Save</button>
                </div>
            </div>

        </div>
    </div>


    <div id="showStockModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-xl">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row" style="width: 100%;">
                        <div class="col-md-6">
                            <h4 class="modal-title">Product Stock Log (in piece) </h4>
                        </div>
                         <div class="col-md-6 text-right">
                            <h4 class="modal-title"><span id="pid" style="color: black;margin-right:5px"> - </span><span id="pname" style="color: black;"></span></h4>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="tblStockLog">
                            <thead class="bg-blue white">
                                <tr>
                                    <td class="ActionDate text-center">Action Date</td>
                                    <td class="oldStcok text-center">Before Stock</td>
                                    <td class="AffectedStock text-center">Affected Stock</td>
                                    <td class="DefaultAffectedStock text-center">Default Affected<br /> Stock</td>
                                    <td class="NewStock text-center">Current Stock</td>
                                    <td class="ReferenceId text-center">Reference Id</td>
                                    <td class="remark">Remark</td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <h5 class="well text-center" id="EmptyTable33" style="display: none">No data available.</h5>
                    </div>
                    <div class="row">
                        <div class="col-md-1">
                            <select class="form-control border-primary input-sm" onchange="ShowProductLStockogAll(1)" id="ddlPageSizeLog">
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                                <option value="0">All</option>
                            </select>
                        </div>
                        <div class="col-md-11">
                            <div class="Pager text-right" id="LogPager"></div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse" onclick="closeHistoryModal()" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

