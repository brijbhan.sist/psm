﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="pageTitleMaster.aspx.cs" Inherits="Admin_pageTitleMaster" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .disp {
            display: none;
        }

        #Table1 tr th {
            vertical-align: middle;
            text-align: center
        }

        #Table1 th, .table td {
            padding: 5px 5px !important;
        }

        #Table1 tbody td {
            padding: 5px 5px !important;
            vertical-align: middle !important;
        }

        .tiles {
            counter-reset: cool -1 good -1;
            padding: 12px 0;
            position: sticky;
        }

            .tiles > div {
                margin: 3px;
                padding: 2px;
            }

            .tiles > .col-xs-2 {
                width: 19%;
                box-shadow: 0px 1px 4px 0px;
                height: auto;
            }

            .tiles > .col-xs-4 {
                width: 40%;
            }

            .tiles > div:nth-of-type(3) > div {
                margin-bottom: -100%;
            }

            .tiles > div:nth-of-type(7) {
                margin-left: 40%;
            }

            .tiles > div:nth-of-type(8),
            .tiles > div:nth-of-type(22) {
                clear: both;
            }

            .tiles > div:nth-of-type(19) {
                clear: left;
            }

            .tiles > div > div {
                position: relative;
            }

                .tiles > div > div:before {
                    color: #fff;
                    font-size: 28px;
                    font-weight: bold;
                    left: 8px;
                    position: absolute;
                    top: 2px;
                }

            /*.tiles > div.col-xs-2 > div:before {
                content: counter(good);
                counter-increment: good;
            }

            .tiles > div.col-xs-4 > div:before {
                content: counter(cool);
                counter-increment: cool;
                font-size: 56px;
            }*/

            .tiles img {
                display: block;
                /*max-height: 116px;*/
                width: 100%;
                cursor: pointer;
            }



        .imageThumb {
            max-height: 75px;
            border: 0.5px solid blue;
            padding: 17px;
            cursor: pointer;
            width: 154px;
        }

        .pip {
            display: inline-block;
            margin: 10px 10px 0 0;
        }

        .remove {
            display: block;
            background: #444;
            border: 1px solid black;
            color: white;
            text-align: center;
            cursor: pointer;
        }

            .remove:hover {
                background: white;
                color: black;
            }

        .defaultImage {
            border: 1px solid red;
        }

        i.la.la-times {
            position: absolute;
            top: 17px;
            cursor: pointer;
            background: #333;
            color: #fff;
            font-size: 16px;
        }

        .note-editable.panel-body {
            height: 200px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Page Title Master</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin/mydashboard.aspx">Home</a></li>
                        <%--  <li class="breadcrumb-item"><a href="#">Manage Inventory</a></li>--%>
                        <li class="breadcrumb-item active">Page Title Master</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <%--   <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>--%>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a class="dropdown-item" href="/Admin/productMaster.aspx" id="addProduct" runat="server">Add New Product</a>
                    <a class="dropdown-item" href="productList.aspx" id="A3">Go to Product List</a>
                    <input type="hidden" id="HDDomain" runat="server" />
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Page Title Details</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <input type="hidden" id="CheckimpType" runat="server" />
                                <input type="hidden" id="hiddenText" runat="server" />
                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <div class="col-md-5 col-sm-5 col-xs-12">
                                                        <label class="control-label">Page ID</label>
                                                        <span class="required">*</span>
                                                    </div>
                                                    <input type="hidden" id="txtHPageTitleAutoId" />
                                                    <div class="col-md-7 col-sm-7 col-xs-12 form-group" style="position: relative; right: 16px;">
                                                        <input type="text"  class="form-control border-primary input-sm req" maxlength="6" id="txtPageId" runat="server" onkeypress='return isNumberKey(event)' />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                                        <label class="control-label">Select Location</label>
                                                        <span class="required">*</span>
                                                    </div>
                                                    <div class="col-md-7 col-sm-7 col-xs-12 form-group" id="AllLocation" style="position: relative; right: 16px;">
                                                        <select class="form-control border-primary input-sm " id="ddlLocation">
                                                            <option value="0">---- All Location ----</option>
                                                        </select>
                                                    </div>
                                                     <div class="col-md-7 col-sm-7 col-xs-12 form-group" style="display:none;position: relative; right: 16px;" id="IndiviLocation">
                                                        <select class="form-control border-primary input-sm " id="ddlIndividualLocation">
                                                             <option value="1">Individual</option>
                                                            <option value="0">---- All Location ----</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <div class="col-md-5 col-sm-5 col-xs-12">
                                                        <label class="control-label">Page Title</label>
                                                        <span class="required">*</span>
                                                    </div>
                                                    <div class="col-md-7 col-sm-7 col-xs-12 form-group" style="position: relative; right: 16px;">
                                                        <input type="text" class="form-control border-primary input-sm req" maxlength="149" id="txtPageTitle" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                                        <label class="control-label">Status</label>
                                                        <span class="required">*</span>
                                                    </div>
                                                    <div class="col-md-7 col-sm-7 col-xs-12 form-group" style="position: relative; right: 16px;">
                                                        <select class="form-control border-primary input-sm" id="ddlPageTitleStatus">
                                                            <option value="1">Active</option>
                                                            <option value="0">Inactive</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                           <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                                        <label class="control-label">Page Url</label>
                                                        <span class="required">*</span>
                                                    </div>
                                                    <div class="col-md-9 col-sm-9 col-xs-12 form-group" style="margin-left: -65px">
                                                        <input type="text" class="form-control border-primary input-sm req" maxlength="149" id="txtPageUrl" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <div class="col-md3 col-sm-3 col-xs-12">
                                                        <label class="control-label">User Description</label>
                                                        <span class="required">*</span>
                                                    </div>
                                                    <div class="col-md-9 col-sm-9 col-xs-12 form-group" style="margin-left: -65px">
                                                        <textarea id="txtUserDescription" class="form-control border-primary input-sm" style="height: 300px">

                                                        </textarea>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <div class="col-md3 col-sm-3 col-xs-12">
                                                        <label>Admin Description</label>
                                                        <span class="required">*</span>
                                                    </div>
                                                    <div class="col-md-9 col-sm-9 col-xs-12 form-group" style="margin-left: -65px">
                                                        <textarea id="txtAdminDescription" class="form-control border-primary input-sm"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                     
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnSave" onclick="savePageTitleDetail()">Save</button>
                                    </div>
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" class="btn btn-secondary  buttonAnimation pull-right round box-shadow-1  btn-sm" onclick="resetButton()" data-animation="pulse" id="btnReset1">Reset</button>
                                    </div>

                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnUpdate" onclick="updatePageTitleDetail()" style="margin-top: 10px; display: none">Update</button>
                                    </div>
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" class="btn btn-warning buttonAnimation pull-right round box-shadow-1 btn-sm" id="btnCancel" data-animation="pulse" style="display: none">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body" id="PackingDetails">
                <section>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-3 col-sm-6">
                                            <h4 class="card-title">Page Title Detail List</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">

                                    <div class="row form-group">
                                        <div class="col-md-3 form-group">
                                            <input type="text" class="form-control border-primary input-sm" maxlength="10" onkeypress='return isNumberKey(event)' id="sPageID" placeholder="Page Id" runat="server" />
                                        </div>
                                        <div class="col-md-3 form-group">
                                            <input type="text" class="form-control border-primary input-sm" maxlength="149" id="sPageUrl" placeholder="Page Url" runat="server" />
                                        </div>
                                        <div class="col-md-3 form-group">
                                            <input type="text" class="form-control border-primary input-sm" maxlength="149" id="sPageTitle" placeholder="Page Title" runat="server" />
                                        </div>
                                        </div>
                                     <div class="row form-group">
                                        <div class="col-md-3 form-group">
                                            <select class="form-control input-sm border-primary" id="ddlStatus">
                                                <option value="1">Active</option>
                                                <option value="0">Unactive</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3 form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="padding: 0rem 1rem;">
                                                        <span class="la la-calendar-o"></span>
                                                    </span>
                                                </div>
                                                <input type="text" class="form-control input-sm date border-primary" placeholder="From Date" id="txtSFromDate" onfocus="this.select()" />
                                            </div>
                                        </div>
                                        <div class="col-md-3 form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="padding: 0rem 1rem;">
                                                        <span class="la la-calendar-o"></span>
                                                    </span>
                                                </div>
                                                <input type="text" class="form-control input-sm date border-primary" placeholder="To Date" id="txtSToDate" onfocus="this.select()" />
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <button type="button" class="btn btn-info buttonAnimation round box-shadow-1 btn-sm" id="btnSearch" onclick=" getPageTitleDetailList(1);">Search</button>
                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="col-md-12">
                                            <%--<div class="card">
                                                <div class="card-content collapse show">
                                                    <div class="card-body">
                                                        <div class="row form-group">
                                                            <div class="col-md-12">--%>
                                                                <div class="table-responsive">
                                                                    <table class="table table-striped table-bordered" id="tblpageTitleList">
                                                                        <thead class="bg-blue white">
                                                                            <tr>
                                                                                <td class="Action width3per text-center" style="white-space: nowrap">Action</td>
                                                                                <td class="PageId">Page Id</td>
                                                                                <td class="PageUrl text-center">Page Url</td>
                                                                                <td class="PageTitle center left text-center wth9">Page Title</td>
                                                                                <td class="Status left text-center wth9">Status</td>
                                                                                <td class="UserDescription center left text-center wth9">User Description</td>
                                                                                <td class="AdminDescription center left text-center wth9">Admin Description</td>

                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        </tbody>
                                                                    </table>
                                                                    <h5 class="well text-center" id="emptyTable" style="display: none">No data available.</h5>
                                                                </div>
                                                           <%-- </div>
                                                        </div>--%>
                                                        <div class="row container-fluid">

                                                            <div>
                                                                <select class="form-control input-sm border-primary" id="ddlPageSize" onchange=" getReport(1);">
                                                                    <option selected="selected" value="10">10</option>
                                                                    <option value="50">50</option>
                                                                    <option value="100">100</option>
                                                                    <option value="500">500</option>
                                                                    <option value="1000">1000</option>
                                                                    <option value="0">All</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="Pager"></div>
                                                            </div>

                                                        </div>
                                                   <%-- </div>
                                                </div>
                                            </div>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

        </section>
    </div>







    <script src="../js/summernote.js"></script>
    <link href="../css/summernote.css" rel="stylesheet" />
    <script>
        $(document).ready(function () {
            $('#txtUserDescription').summernote();

        });
    </script>
    <script>
        $(document).ready(function () {
            $('#txtAdminDescription').summernote();

        });
    </script>
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script src="BarCode/jquery-barcode.js"></script>

</asp:Content>

