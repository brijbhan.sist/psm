﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DllEmployee;


public partial class Admin_Employee : System.Web.UI.Page
{



    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserName"] == null)
        {
            Response.Redirect("~/Default.aspx", false);
        }
        else
        {

            if (Session["EmpTypeNo"].ToString() == "1" || Session["EmpTypeNo"].ToString() == "12")
            {
                IpDiv.Visible = true;
            }
            else if (Session["EmpTypeNo"].ToString() == "13")
            {
                btnSave.Visible = false;
                btnReset.Visible = false;
                ddlEmpType.Disabled = true;
            }
            emptype.Value = Session["EmpTypeNo"].ToString();
        }
    }
    [WebMethod(EnableSession = true)]
    public static string getemployeeinfo(int AutoId)
    {
        PL_Employee pobj = new PL_Employee();
        pobj.AutoId = AutoId;
        BL_Employee.getemployeeinfo(pobj);
        if (!pobj.isException)
        {
            return pobj.Ds.GetXml();
        }
        else
        {
            return "false";
        }
    }
  
}