﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DLLMapTest;
public partial class Admin_Map : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod]
    public static string SelectwayPoint()
    {
        
            try
            {
                PL_MapTest pobj = new PL_MapTest();
                BL_MapTest.select(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                if(string.IsNullOrEmpty(json))
                {
                    json = "[]";
                }
                return json;

            }
            catch (Exception ex)
            {

                return "false";
            }
      
    }
}