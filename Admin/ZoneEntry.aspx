﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="ZoneEntry.aspx.cs" Inherits="Admin_ZoneEntry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Manage Zone</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage Application</a></li>
                        <li class="breadcrumb-item">Manage Zone</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">

            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a class="dropdown-item" href="/Admin/ZoneList.aspx" id="linkAddNewProduct" runat="server">Go To Zone List</a>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-header">
                            <h4 class="card-title">Zone Details</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Zone ID</label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <input type="text" class="form-control border-primary input-sm" id="txtZoneId" readonly="readonly" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">
                                                    Zone Name <span class="required">*</span>

                                                </label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <input type="text" class="form-control border-primary input-sm req" id="txtZoneName" runat="server" onkeyup="uppercase(this.id)" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Driver</label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <select class="form-control border-primary input-sm" id="ddlDriver" style="width:100%;">
                                                    <option value="0">-Select-</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Car</label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <select class="form-control border-primary input-sm" id="ddlCar" style="width:100%;">
                                                    <option value="0">-Select-</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Status</label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <select class="form-control border-primary input-sm" id="ddlStatus">
                                                    <option value="1" selected="selected">Active</option>
                                                    <option value="0">Inactive</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnSave">Save</button>
                                    </div>
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" class="btn btn-secondary  buttonAnimation pull-right round box-shadow-1  btn-sm" onclick="resttext()" data-animation="pulse" id="btnReset">Reset</button>
                                    </div>
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnUpdate" style="display: none">Update</button>
                                    </div>
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" class="btn btn-warning buttonAnimation pull-right round box-shadow-1 btn-sm" id="btnCancel" data-animation="pulse" style="display: none">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" style="display: none" id="divaddzipcode">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Add Zip Code</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row ">
                                    <div class="col-md-4 form-group">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Zip Code" id="txtSZipCode" />
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="City Name" id="txtCityname" />
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <button type="button" class="btn btn-info buttonAnimation pull-left round box-shadow-1  btn-sm" id="btnSearch">Search</button>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="" style="width:100%">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblZips" style="width:100%;">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="SelCheckbox text-center">Action </td>
                                                        <td class="Id" style="display: none">ID</td>
                                                        <td class="ZipCode text-center">Zip Code</td>
                                                        <td class="City">City</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="JS/ZoneZipMaster.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>

</asp:Content>

