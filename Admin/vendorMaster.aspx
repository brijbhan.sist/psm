﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" ClientIDMode="Static" CodeFile="vendorMaster.aspx.cs" Inherits="Admin_vendorMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Manage Vendor</h3>
             <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage Vendor</a></li>
                        <li class="breadcrumb-item">New Vendor</li>
                    </ol>
                </div>
            </div>
              </div>
            <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a href="/Admin/VendorList.aspx" class="dropdown-item">Vendor List</a>                    
                </div>
            </div>
        </div>
      
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Vendor Details</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-4 col-sm-4 col-xs-12">Vendor ID</label>

                                            <div class="col-md-8">
                                                <input type="text" class="form-control input-sm border-primary" readonly="readonly" id="txtVendorId" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="row form-group">
                                            <label class="col-md-4 col-sm-4 col-xs-12">
                                                Vendor Name <span class="required">&nbsp;*</span>                                                
                                            </label>
                                            <div class="col-md-8">
                                                <input type="text" onblur="checkSpecialCharacter(this)" maxlength="100" class="form-control input-sm border-primary req" id="txtVendorName"  runat="server" />

                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-4 col-sm-4 col-xs-12">Address <span class="required">&nbsp;*</span>  </label>
                                            <div class="col-md-8">
                                                <textarea class="form-control input-sm border-primary req" maxlength="200" rows="2" id="txtAddress"></textarea>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-4 col-sm-4 col-xs-12">
                                                Country <span class="required">&nbsp;*</span>
                                                
                                            </label>
                                            <div class="col-md-8">
                                                <select class="form-control input-sm border-primary ddlreq" id="ddlCountry" runat="server">
                                                    <option value="0">- Select -</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-4 col-sm-4 col-xs-12">Zip Code <span class="required">*</span></label> 
                                            <div class="col-md-8">
                                                <select class="form-control input-sm border-primary ddlreq" id="ddlZipcode" onchange="statecity()">
                                                    <option value="0">- Select -</option>
                                                </select>
                                            </div>
                                        </div>
                                        

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-4 col-sm-4 col-xs-12">
                                                State
                                            </label>
                                            <div class="col-md-8">
                                                 <input type="text" class="form-control input-sm border-primary" readonly id="txtState" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">                                        
                                        <div class="form-group row">
                                            <label class="col-md-4 col-sm-4 col-xs-12">
                                                City                                               
                                            </label>
                                            <div class="col-md-8 ">
                                                <input type="text" class="form-control input-sm border-primary" readonly id="txtCity" runat="server" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-4 col-sm-4 col-xs-12">
                                                Contact Person<span class="required">&nbsp;*</span>
                                               
                                            </label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control input-sm border-primary req" onchange="checkSpecialCharacter(this)" maxlength="50" id="txtContactPerson" runat="server"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-4 col-sm-4 col-xs-12">
                                                <span class="la la-mobile"></span>&nbsp;&nbsp;Cell<span class="required">&nbsp;*</span>
                                               
                                            </label>
                                            <div class="col-md-8">
                                                <input type="text" minlength="10" maxlength="10" onblur="MobileLength(this,1)" class="form-control input-sm border-primary req" id="txtCell" runat="server" onkeypress="return isNumberKey(event)"/>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-4 col-sm-4 col-xs-12"><span class="la la-phone"></span>&nbsp;&nbsp;Office 1 <span class="required">&nbsp;*</span></label>
                                            <div class="col-md-8 ">
                                                <input type="text" minlength="10" onblur="MobileLength(this,1)" maxlength="10" class="form-control input-sm border-primary req" id="txtOffice1" onkeypress="return isNumberKey(event)" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-4 col-sm-4 col-xs-12"><span class="la la-phone"></span>&nbsp;&nbsp;Office 2</label>
                                            <div class="col-md-8 ">
                                                <input type="text" minlength="10" onblur="MobileLength(this,2)" maxlength="10" class="form-control input-sm border-primary" id="txtOffice2"  onkeypress="return isNumberKey(event)"/>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-4 col-sm-4 col-xs-12">Email</label>
                                            <div class="col-md-8 ">
                                                <input type="text" maxlength="50" class="form-control input-sm border-primary" id="txtEmail" onchange="validateEmail(this)"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-4 col-sm-4 col-xs-12">Type</label>
                                            <div class="col-md-8 ">
                                                <select class="form-control border-primary input-sm" id="ddlType">
                                                    <option value="0" selected="selected">Normal</option>
                                                    <option value="1">Internal</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-4 col-sm-4 col-xs-12">Status</label>
                                            <div class="col-md-8 ">
                                                <select class="form-control border-primary input-sm" id="ddlStatus">
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="RowEnternal" style="display:none">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-4 col-sm-4 col-xs-12">Location<span class="required"> *</span></label>
                                            <div class="col-md-8 ">
                                                <select class="form-control border-primary input-sm" id="ddlInternal">
                                                    <option value="0" selected="selected">-Select-</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnSave">Save</button>
                                    </div>
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" class="btn btn-secondary  buttonAnimation pull-right round box-shadow-1  btn-sm" onclick="reset()" data-animation="pulse" id="btnReset">Reset</button>
                                    </div>
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnUpdate" style="display: none">Update</button>
                                    </div>
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" class="btn btn-warning buttonAnimation pull-right round box-shadow-1 btn-sm" id="btnCancel" onclick="reset()" data-animation="pulse" style="display: none">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="modal fade" id="mdMultipleCityState" style="display:none" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-md ">
            <!-- Modal content -->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Select State/City</h4>
                </div>
                <div class="modal-body">
                    <table id="tblStateCity" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <td style="text-align: center !important" class="CAction"><b>Action</b></td>
                                <td style="text-align: center !important" class="CZipcode"><b>Zipcode</b></td>
                                <td style="text-align: left !important" class="CState"><b>State</b></td>
                                <td style="text-align: left !important" class="CCity"><b>City</b></td>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                   <%--<div id="divStateCity"></div>--%>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success buttonAnimation round box-shadow-1  btn-sm" onclick="applyStateCity()" id="btnApply">Apply</button>
                     <button type="button" class="btn btn-danger pull-right round box-shadow-1 btn-sm" onclick="closeCitySateModal()" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
     <script type="text/javascript">
         document.write('<scr' + 'ipt type="text/javascript" src="/Admin/JS/VendorMaster.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
</asp:Content>

