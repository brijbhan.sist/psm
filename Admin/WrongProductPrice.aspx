﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/MasterPage.master" AutoEventWireup="true" CodeFile="WrongProductPrice.aspx.cs" Inherits="Admin_WrongProductPrice" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Wrong Product Price List</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item">Developer Help
                        </li>
                        <li class="breadcrumb-item">Wrong Product Price List
                        </li>
                        
                    </ol>
                </div>
            </div>          
        </div>
        <div class="content-header-right col-md-6 col-12" style="display:none">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1"> 
                    <input type="hidden" id="hiddenPackerAutoId" runat="server" />
                    <input type="hidden" id="hiddenEmpType" runat="server" />
                </div>
            </div>
        </div>
    </div>   

    <div class="content-body">
        <section id="drag-area2">
            <div class="row">      
                <div class="col-md-12" style="display:none;">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-3">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Product Name" id="txtProductName" />
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <div class="form-group">
                                            <select class="form-control input-sm" id="textProductUnit">
                                            <option value="">-- Select Unit --</option>
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" id="btnSearch">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="content-body">
        <section id="drag-area3">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-12">

                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblProductList">
                                                <thead class="bg-blue white">
                                                    <tr>           
                                                        <td class="ProductId text-center">Product Id</td>
                                                        <td class="ProductName">Product Name</td>
                                                        <td class="UnitType text-center">Unit</td>
                                                        <td class="Qty text-center">Qty</td>
                                                        <td class="MinPrice text-center">Min Price</td>
                                                        <td class="CostPrice text-center">Cost Price</td>
                                                        <td class="Price text-center">Cost</td>
                                                        <td class="SRP text-center">SRP</td>
                                                        <td class="WHminPrice text-center">WH Min Price</td>
                                                        <td class="LastDate text-center">Last Update Date</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row container-fluid">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <select id="ddlPageSize" onchange="getProductList(1)" class="form-control border-primary input-sm">
                                                <option selected="selected" value="10">10</option>
                                                <option value="50">50</option>
                                                <option value="100">100</option>
                                                <option value="500">500</option>
                                                <option value="1000">1000</option>
                                                <option value="0">All</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-10 col-sm-12">
                                        <div class="Pager"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="JS/WrongProductPrice.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
</asp:Content>

