﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="ToDo.aspx.cs" Inherits="Admin_ToDo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">To Do</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/Dashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage To Do</a></li>
                        <li class="breadcrumb-item active">To Do</li>

                    </ol>
                </div>
            </div>
        </div>

        <div class="content-header-left col-md-6 col-12 mb-2">
            <a href="#" class="btn btn-black buttonAnimation pull-right round box-shadow-1 btn-sm" onclick="openAddNewDo();">Add New To Do</a>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">From Date
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" id="txtFromDate" placeholder="From Date" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">To Date
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" id="txtToDate" placeholder="To Date" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <select class="form-control input-sm border-primary" id="ddlSToDoType">
                                            <option value="2" selected="selected">All Type</option>
                                            <option value="1">Important</option>
                                            <option value="0">Starred</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <select class="form-control input-sm border-primary" id="ddlSIsCompleted">
                                            <option value="0">All Status</option>                                           
                                        </select>
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <button type="button" class="btn btn-black buttonAnimation pull-left round box-shadow-1 btn-sm" id="btnSearch" onclick="getList(1)">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-content collapse show">
                            <div class="card-body">


                                <div class="row form-group ">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table tbl-3 table-striped table-bordered" id="TableToDo">
                                                <thead class="bg-black  white">
                                                    <tr>
                                                        <td class="action" style="width:40px">Action</td>
                                                        <th class="Id" style="display: none;">Id</th>
                                                        <td class="StatusType">Status</td>
                                                        <td class="Title">Title</td>
                                                        <td class="Description" style="white-space:normal;word-break:break-word">Description</td>
                                                        <td class="ToDoType ws-cntr text-center">Type</td>
                                                        <td class="CreateDate ws-cntr text-center">Create Date</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none; padding-top: 20px; padding-bottom: 20px;">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex">

                                    <div class=" form-group">
                                        <select id="ddlPageSize" class="form-control border-primary input-sm" onchange="getList(1)">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                        </select>
                                    </div>
                                    <div class="ml-auto text-right">
                                        <div class="Pager"></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div id="addnote" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <i class="icon-plus"></i><span>&nbsp;&nbsp;</span>Add To Do
                    </h5>
                    <button type="button" class="close mr-0" data-dismiss="modal" onclick="Cancel();" aria-label="Close">
                        <i class="icon-close"></i>
                    </button>
                </div>

                <div class="add-note-form">
                    <div class="modal-body">
                        <label class="control-label" id="lblAutoId" style="display: none"></label>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="inputfname" class="control-label col-form-label" style="display: none">Id</label>

                                </div>
                            </div>
                        </div>
                        <div class="row form-group">

                            <div class="col-md-3">
                                <label for="inputlname2" class="control-label col-form-label">Title</label>
                                <span class="required">*</span>
                            </div>
                            <div class="col-md-9">
                                <input type="text" class="form-control border-primary input-sm  req" id="TxtTitle" maxlength="100" placeholder="Write title here..." />
                            </div>
                        </div>
                        <div class="row form-group">

                            <div class="col-md-3">
                                <label for="inputlname2" class="control-label col-form-label">Status</label>
                                <span class="required">*</span>
                            </div>
                            <div class="col-md-9">
                                <select class="form-control border-primary input-sm  ddlreq " id="ddlStatus">
                                    <option value="0">Select Status</option>
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">

                            <div class="col-md-3">
                                <label for="inputlname2" class="control-label col-form-label">Description</label>

                            </div>
                            <div class="col-md-9">
                                <textarea id="txtDescription" maxlength="500" class="form-control border-primary input-sm" rows="13" placeholder="Write some description here..."></textarea>
                            </div>
                        </div>
                        <div class="row form-group">

                            <div class="col-md-3">
                                <label class="control-label">Starred</label>
                            </div>
                            <div class="col-md-1">
                                <input type="checkbox" class="checkbox border-primary" id="ChkStarred" />
                            </div>
                        </div>
                        <div class="row form-group">

                            <div class="col-md-3">
                                <label class="control-label">Important</label>
                            </div>
                            <div class="col-md-1">
                                <input type="checkbox" class="checkbox border-primary" id="ChkImportant" />
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="btn-group mr-1 pull-right">
                                    <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnSave" onclick="Save();">Save</button>
                                </div>
                                <div class="btn-group mr-1 pull-right">
                                    <button type="button" class="btn btn-secondary  buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" onclick="reset()" id="btnReset">Reset</button>
                                </div>
                                <div class="btn-group mr-1 pull-right">
                                    <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnUpdate" style="display: none" onclick="Update();">Update</button>
                                </div>
                                <div class="btn-group mr-1 pull-right">
                                    <button type="button" class="btn btn-warning buttonAnimation pull-right round box-shadow-1 btn-sm" id="btnCancel" data-animation="pulse" style="display: none" onclick="Cancel()">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

