﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RouteEntry : System.Web.UI.Page
{
   
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["EmpTypeNo"] != null)
        {
            if (Session["EmpTypeNo"].ToString() == "2")
            {
                hdEmpType.Value = Session["EmpAutoId"].ToString();
            }
            else
            {
                hdEmpType.Value = "0";
            }
        }
        else
        {
            Response.Redirect("/");
        }
    }
}