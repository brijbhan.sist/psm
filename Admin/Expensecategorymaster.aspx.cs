﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DLLExpensecategorymaster;
public partial class Admin_Expensecategorymaster : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Admin/JS/Expensecategorymaster.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredField'>" + text + "</script>"
                ));
        }
    }
    [WebMethod(EnableSession = true)]
    public static string insertCategory(string dataValue)
    {
        if (HttpContext.Current.Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_Expensecategorymaster pobj = new PL_Expensecategorymaster();
            try
            {
                pobj.CategoryName = jdv["CategoryName"];
                pobj.Description = jdv["Description"];
                pobj.Status = Convert.ToInt32(jdv["Status"]);
               
                BL_Expensecategorymaster.insert(pobj);
                if (!pobj.isException)
                {

                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string getCategoryDetail(string dataValue)
    {
        if (HttpContext.Current.Session["EmpTypeNo"] != null)
        {
            var jss = new System.Web.Script.Serialization.JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_Expensecategorymaster pobj = new PL_Expensecategorymaster();
            try
            {
                pobj.CategoryId = jdv["CategoryId"];
                pobj.CategoryName = jdv["CategoryName"];
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                BL_Expensecategorymaster.select(pobj);

                return pobj.Ds.GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string editCategory(string CategoryId)
    {
        if (HttpContext.Current.Session["EmpTypeNo"] != null)
        {
            PL_Expensecategorymaster pobj = new PL_Expensecategorymaster();
            pobj.CategoryId = CategoryId;
            BL_Expensecategorymaster.editCategory(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string updateCategory(string dataValue)
    {
        if (HttpContext.Current.Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_Expensecategorymaster pobj = new PL_Expensecategorymaster();
            try
            {
                pobj.CategoryId = jdv["CategoryId"];
                pobj.CategoryName = jdv["CategoryName"];
                pobj.Description = jdv["Description"];
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                
                BL_Expensecategorymaster.update(pobj);
                if (!pobj.isException)
                {
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string deleteCategory(string CategoryId)
    {
        if (HttpContext.Current.Session["EmpTypeNo"] != null)
        {
            PL_Expensecategorymaster pobj = new PL_Expensecategorymaster();
            try
            {
                pobj.CategoryId = CategoryId;
                BL_Expensecategorymaster.delete(pobj);
                if (!pobj.isException)
                {
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
}