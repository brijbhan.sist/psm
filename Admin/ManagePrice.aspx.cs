﻿using DLLManagePrice;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class Admin_ManagePrice : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Admin/JS/ManagePrice.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredField'>" + text + "</script>"
                ));
        }
        if (Session["DBLocation"] != null)
        {

            hfLocation.Value = Session["DBLocation"].ToString();
        }
        else
        {
            Response.Redirect("/");
        } 
    }

    [WebMethod(EnableSession = true)]
    public static string bindCategory()
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_ManagePrice pobj = new PL_ManagePrice();
            try
            {
                BL_Manageprice.bindCategory(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                return json;
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string bindSubcategory(string categoryAutoId)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_ManagePrice pobj = new PL_ManagePrice();
            try
            {
                pobj.CategoryAutoId =Convert.ToInt32(categoryAutoId);
                BL_Manageprice.bindSubcategory(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                return json;
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }


    [WebMethod(EnableSession = true)]
    public static string GetPriceDetails(string dataValue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_ManagePrice pobj = new PL_ManagePrice();
                if (jdv["ProductId"]== "")
                {
                    pobj.ProductId = 0;
                }
                else
                {
                    pobj.ProductId = Convert.ToInt32(jdv["ProductId"]);
                }
                pobj.CategoryAutoId = Convert.ToInt32(jdv["CategoryAutoId"]);
                pobj.SubcategoryAutoId = Convert.ToInt32(jdv["SubcategoryAutoId"]);
                pobj.BrandAutoId = Convert.ToInt32(jdv["BrandName"]);
                pobj.ProductName = jdv["ProductName"];
                pobj.CostPriceCompare = Convert.ToInt32(jdv["CostComparePrice"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                BL_Manageprice.getPrice(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string UpdatePrice(string dataValue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_ManagePrice pobj = new PL_ManagePrice();
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                pobj.ProductAutoid = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.Qty = Convert.ToInt32(jdv["Qty"]);
                pobj.Price = Convert.ToDecimal(jdv["Price"]);
                pobj.CostPrice = Convert.ToDecimal(jdv["CostPrice"]);
                pobj.MinPrice = Convert.ToDecimal(jdv["MinPrice"]);
                pobj.WMinPrice = Convert.ToDecimal(jdv["WMinPrice"]);
                pobj.SRP = Convert.ToDecimal(jdv["SRP"]);
                BL_Manageprice.UpdarePrice(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string updateReOrderMark(string dataValue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_ManagePrice pobj = new PL_ManagePrice();
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                pobj.ProductAutoid = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.Qty = Convert.ToInt32(jdv["Qty"]);
                pobj.ReOrderMark = Convert.ToInt32(jdv["ReOrderMark"]);
                BL_Manageprice.updateReOrderMark(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string UpdateChangedPrice(string dataValue, string ProductAutoIdValue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                DataTable ProductAutoIdData = new DataTable();
                ProductAutoIdData = JsonConvert.DeserializeObject<DataTable>(ProductAutoIdValue);

                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);

               
                PL_ManagePrice pobj = new PL_ManagePrice();
                if (ProductAutoIdData.Rows.Count > 0)
                {
                    pobj.TableValueMultiProductAutoId = ProductAutoIdData;
                }
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                //pobj.ReOrderMark = Convert.ToInt32(jdv["ReOrderMarkChange"]);
                pobj.Price = Convert.ToDecimal(jdv["BaseChangePrice"]);
                pobj.CostPrice = Convert.ToDecimal(jdv["CostChangePrice"]);
                pobj.MinPrice = Convert.ToDecimal(jdv["MinChangePrice"]);
                pobj.WMinPrice = Convert.ToDecimal(jdv["WMinChangePrice"]);
                pobj.SRP = Convert.ToDecimal(jdv["SRP"]);
                BL_Manageprice.updateChangedPrice(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    if (pobj.exceptionMessage == "SRP")
                    {
                        return "SRP";
                    }
                    else
                    {
                        return pobj.exceptionMessage;
                    }
                }
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string UpdateChangedReorderMark(string dataValue, string ProductAutoIdValue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                DataTable ProductAutoIdData = new DataTable();
                ProductAutoIdData = JsonConvert.DeserializeObject<DataTable>(ProductAutoIdValue);

                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);


                PL_ManagePrice pobj = new PL_ManagePrice();
                if (ProductAutoIdData.Rows.Count > 0)
                {
                    pobj.TableValueMultiProductAutoId = ProductAutoIdData;
                }
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                pobj.ReOrderMark = Convert.ToInt32(jdv["ReOrderMarkChange"]);
                BL_Manageprice.UpdateChangedReorderMark(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}