﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="BarCodeReport.aspx.cs" Inherits="Admin_BarCodeReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .tblwth {
            width: 14%;
        }

        .Pager {
            text-align: right;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Barcode</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Reports</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Product Report</a>
                        </li>
                        <li class="breadcrumb-item active">Barcode
                        </li>
                        <li class="breadcrumb-item active" title="Help"><a href="#" onclick="GetPageInformation(10038)"><i class="la la-question-circle"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem()">Print</button>
                    <button type="button" class="dropdown-item" id="btnExport" onclick="Export()">Export</button>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12 col-md-3">
                                        <select class="form-control border-primary input-sm" id="ddlSCategory" onchange="Category()">
                                            <option value="0">All Category</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <select class="form-control border-primary input-sm" id="ddlSSubcategory">
                                            <option value="0">All Subcategory</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-2">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Product ID" id="txtSProductId" />
                                    </div>
                                    <div class="col-sm-12 col-md-2">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Product Name" id="txtSProductName" />
                                    </div>
                                    <div class="col-sm-12 col-md-2">
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" id="btnSearch" onclick="getBarcodeReport(1);">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="MyTableHeader dataex-html5-export" id="tblProductList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="Category width-15-per text-center">Category</td>
                                                        <td class="SubCategory width-15-per text-center">Subcategory</td>
                                                        <td class="ProductId text-center width7per">Product ID</td>
                                                        <td class="ProductName left">Product Name</td>
                                                        <td class="Unit text-center width4per">Unit</td>
                                                        <td class="barcode text-center width8per">Barcode</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                            <table id="tblProductListExport" class="MyTableHeader" style="display: none">
                                                <thead class="bg-blue white">
                                                   <%-- <tr class="thead">
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr style="background-color: gray">
                                                        <td colspan="5" style="text-align: center">
                                                            <b>Report Name: </b>Product Bar Code Report.<br />
                                                            <b>Download Date:</b><span id="LblDate"></span><br />
                                                        </td>
                                                    </tr>--%>
                                                    <tr>
                                                        <td style="text-align: center">Category</td>
                                                        <td style="text-align: center">Sub-Category</td>
                                                        <td style="text-align: center">Product ID</td>
                                                        <td>Product Name</td>
                                                        <td style="text-align: center">Bar Code Number</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <select class="form-control input-sm border-primary pagesize" id="ddlPageSize" onchange="getBarcodeReport(1);">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager text-right"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div id="barcodeprint"></div>
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script src="/Admin/BarCode/Barcodeforproduct.js"></script>
    <script src="BarCode/jquery-barcode.js"></script>
    <script src="JS/BarCodeReport.js"></script>
    <script src="../js/code39.js"></script>
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>

