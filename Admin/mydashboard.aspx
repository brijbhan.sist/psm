﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="mydashboard.aspx.cs" Inherits="Admin_Dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%
        if (Session["EmpTypeNo"] != null)
        {
            if (Session["EmpTypeNo"].ToString() == "1" || Session["EmpTypeNo"].ToString() == "8" || Session["EmpTypeNo"].ToString() == "13")
            {
    %>
       <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">My Dashboard</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li> 
                        <li class="breadcrumb-item active" title="Help"><a href="#" onclick="GetPageInformation(10060)"><i class="la la-question-circle"></i></a></li>
                    </ol>
                </div>
            </div>
        </div> 
    </div>

    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <button type="button" onclick="LoadDashboard()" class="pull-right btn btn-sm btn-success" id="linktoOrderList"><b>Load Dashboard</b></button>
                            <input type="hidden" id="imgLogo" />
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="card" id="dashboard" style="display: none">
                <div class="card-content collapse show">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="row container-fluid">
                                    <table class="table table-striped table-bordered">
                                        <thead class="bg-blue white">
                                            <tr class="CustomColor">
                                                <td colspan="2" style="text-align: center"><b>Today's Overview</b></td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Total Orders</td>
                                                <td id="TotalOrders" style="text-align: right"><span></span>&nbsp;0</td>
                                            </tr>
                                            <tr>
                                                <td>Total Sales</td>
                                                <td id="TotalSales" style="text-align: right"><span></span>&nbsp;0.0</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-striped table-bordered" id="tableOrder">
                                        <thead class="bg-blue white">
                                            <tr class="CustomColor">
                                                <td colspan="2" style="text-align: center"><b>Order Status</b> </td>
                                            </tr>
                                            <tr style="display: none">
                                                <td class="StatusType"></td>
                                                <td class="TotalOrder text-center"></td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                    <table class="table table-striped table-bordered">
                                        <thead class="CustomColor">
                                            <tr>
                                                <td colspan="2" style="text-align: center"><b>Inventory Summary</b></td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Item Below Threshold Value</td>
                                                <td id="ThresholdValue"></td>
                                            </tr>
                                            <tr>
                                                <td>Product Out Of Stock</td>
                                                <td id="OutOfStock"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-7"> 
                                <div class="row">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered" id="tableSalesRevenue">
                                            <thead class="bg-blue white">
                                                <tr>
                                                    <td colspan="4" class="CustomColor"><b style="vertical-align: -5px">Sales Revenue By Sales Person</b>
                                                        <select id="ddlSalesRevenue" class="form-control input-sm" style="width: 100px; float: right" onchange="getSalesRevenue()">
                                                            <option value="0" selected>Today</option>
                                                            <option value="1">Yesterday</option>
                                                            <option value="2">This Month</option>
                                                            <option value="3">Last Month</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="SalesPerson">Sales Person Name	</td>
                                                    <td class="TotalOrder text-center">Total Order</td>
                                                    <td class="TotalSales" style="text-align: right">Total Sales</td>
                                                    <td class="AOV" style="text-align: right">AOV</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td>Total</td>
                                                    <td class="text-center"></td>
                                                    <td style="text-align: right"></td>
                                                    <td style="text-align: right"></td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered" id="tableCollectionDetails">
                                            <thead class="bg-blue white">
                                                <tr>
                                                    <td colspan="3" class="CustomColor"><b style="vertical-align: -5px">Collection Details</b>
                                                        <select id="ddlCollection" class="form-control input-sm" style="width: 100px; float: right" onchange="CollectionDetails()">
                                                            <option value="0">Today</option>
                                                            <option value="1">Yesterday</option>
                                                            <option value="5">Overall</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="StatusType">Status	</td>
                                                    <td class="totalCount text-center">Count</td>
                                                    <td class="ReceivedAmount">Amount</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered" id="tableCreditMaster">
                                            <thead class="bg-blue white">
                                                <tr>
                                                    <td colspan="3" class="CustomColor"><b style="vertical-align: -5px">Total Credit Memo</b>
                                                        <select id="ddlCreditMemo" style="width: 100px; float: right" class="form-control input-sm" onchange="CreditMemoDetails()">
                                                            <option value="0">Today</option>
                                                            <option value="1">Yesterday</option>
                                                            <option value="4">Overall</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="StatusType">Status	</td>
                                                    <td class="totalCount text-center">Count</td>
                                                    <td class="ReceivedAmount">Amount</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <%
            }
        }
    %>
    <%  if (Session["EmpTypeNo"].ToString() == "6")
        {
    %>
    <center>
        <div id="ShowPODraftLog" class="modal fade" role="dialog">
        <div class="modal-dialog modal-xl" style="width:1255px;">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row" style="width: 100%;text-align: left;">
                        <div class="col-md-10">
                            <h4 class="modal-title">Today's Deposit Checks</h4>
                        </div>
                        <div class="col-md-2">
                             <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="OpenTicektList_Popup()">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="tblPaymentLog">
                            <thead class="bg-blue white">
                                <tr>
                                    <td class="PaymentId">Payment Id</td>
                                    <td class="CustomerName">Customer Name</td>
                                    <td class="ReceivedDate text-center">Received Date</td>
                                    <td class="ReceivedBy">Received By</td>
                                    <td class="HoldBy">Hold By</td>
                                    <td class="DepositeDate text-center">Deposit Date</td>
                                    <td class="CheckNo text-center">Check No</td>
                                    <td class="Amount price">Amount</td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                          <h5 class="well text-center" id="EmptyTable33" style="display: none">No data available.</h5>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse" onclick="OpenTicektList_Popup()" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div> </center>
    <%}
    %>

    <center>
        <div id="ShowTicketList" class="modal fade" role="dialog">
        <div class="modal-dialog modal-xl" style="width:1200px;">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row" style="width: 100%;text-align: left;">
                        <div class="col-md-10">
                            <h4 class="modal-title">Ticket List</h4>
                        </div>
                        <div class="col-md-2">
                             <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="OpenDepositList_Popup()">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="tblTicketList">
                            <thead class="bg-blue white">
                                <tr>
                                     <td class="TicketId text-center">Ticket ID </td>
                                     <td class="TicketDate text-center">Date/Time </td>
                                     <td class="Fullname">By Employee</td>
                                     <td class="Priority text-center">Priority </td>
                                     <td class="Type">Type </td>
                                     <td class="ClentStatus text-center">Client Status </td>
                                     <td class="DeveloperStatus text-center">Dev.Status</td>
                                     <td class="TicketCloseDate text-center">Last Response Date </td>
                                     <td class="Subject">Subject </td>
                                     <td class="Description">Description </td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                          <h5 class="well text-center" id="EmptyTable2" style="display: none">No data available.</h5>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse" data-dismiss="modal" onclick="OpenDepositList_Popup()">Close</button>
                </div>
            </div>
        </div>
    </div> </center>
    <center>
        <div id="MsgDashborad_Mesg" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md" style="width:1200px;">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row" style="width: 100%;text-align: left;">
                        <div class="col-md-10">
                            <h4 class="modal-title">Message</h4>
                        </div>
                        <div class="col-md-2">
                             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <span id="Dash_NotificationMsg" style="float:left"></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div> </center>
    <%  if (Session["EmpTypeNo"].ToString() == "8")
        {
    %>
    <center>
        <div id="ShowWebOrderList" class="modal fade" role="dialog">
        <div class="modal-dialog modal-xl" style="width:900px;">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row" style="width: 100%;text-align: left;">
                        <div class="col-md-10">
                            <h4 class="modal-title">New Website Order (Pending)</h4>
                        </div>
                        <div class="col-md-2">
                             <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="closewebsiteorderlist()">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="tblWebOrderList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <%--<td class="status  text-center">Status</td>--%>
                                                        <td class="orderNo  text-center">Order No</td>
                                                        <td class="orderDt  text-center">Order Date</td>
                                                        <td class="SalesPerson text-center">Sales Person</td>
                                                        <td class="cust">Customer</td>
                                                        <td class="value price">Order Amount</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse" onclick="closewebsiteorderlist()" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
     </center>
    <%}
    %>
     <%  if (Session["EmpTypeNo"].ToString() == "8")
        {
    %>
    <div class="modal fade" id="CustomerlistModal" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content" style="width:600px;padding-left:5px;padding-right:5px;">
                <div class="modal-header">
                     <div class="row" style="width: 100%;text-align: left;">
                        <div class="col-md-10">
                            <h4>Draft Customer List </h4>
                        </div>
                        <div class="col-md-2">
                             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                   
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" id="tblCustomerlist">
                                <thead class="bg-blue white">
                                    <tr>                                        
                                        <td class="CustomerName">Customer Name</td>
                                        <td class="CustomerType text-center">Customer Type</td>                                       
                                        <td class="Date text-center">Created Date</td>                                       
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
     <%}
    %>

    <div class="modal fade" id="mdlInvalidPackingDetails" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" >
                <div class="modal-header">
                    <div class="row" style="width: 100%; text-align: left;">
                        <div class="col-md-10">
                            <h4>Missing Unit Type</h4>
                        </div>
                        <div class="col-md-2">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                         <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" id="tblInvalidPackingList">
                                <thead class="bg-blue white">
                                    <tr>
                                        <td class="ProductId text-center">Product Id</td>
                                        <td class="ProductName" style="white-space:normal">Product Name</td>
                                        <td class="ImageUrl text-center">Image</td>
                                        <td class="CreateBy">Created By</td>
                                        <td class="CreateDate text-center">Created On</td>
                                        <%--<td class="LastUpdateDate text-center">Update Date</td>--%>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                     </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="JS/DashBoardMaster.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
</asp:Content>

