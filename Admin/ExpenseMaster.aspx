﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="ExpenseMaster.aspx.cs" Inherits="Admin_ExpeneMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        #tblExpenseList tbody .Amount {
            text-align: right;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Manage Expense </h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item">Manage Expense</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="openExpense()" id="btnAdd">Add Expense</button>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="padding:0rem 0.5rem;">
                                                        <span class="la la-calendar-o pl-0"></span>
                                                    </span>
                                                </div>
                                                <input type="text" onchange="setdatevalidation(1)" class="form-control input-sm border-primary date" placeholder="From Date" id="txtFromDate" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="padding:0rem 0.5rem;">
                                                        <span class="la la-calendar-o pl-0"></span>
                                                    </span>
                                                </div>
                                                <input type="text" onchange="setdatevalidation(2)" class="form-control input-sm border-primary date" placeholder="To Date" id="txtToDate" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <select id="ddlSearchCategory" class="form-control input-sm border-primary">
                                                <option value="0">All Category</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <select id="ddlSearchBy" class="form-control input-sm border-primary select2">
                                                <option value="0">All Condition</option>
                                                <option value="<"><</option>
                                                <option value=">">></option>
                                                <option value="<="><=</option>
                                                <option value=">=">>=</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="input-group ">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 0.5rem;">
                                                    <span class="pl-0">$</span>
                                                </span>
                                            </div>
                                            <input type="text" id="txtamount" class="form-control input-sm border-primary text-right" onchange="amount1();" onkeyup="amount();" maxlength="10" value="0.00" onkeypress="return isNumberDecimalKey(event,this)" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" onclick="getExpenseList(1)" id="btnSearch">Search</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="content-body">
        <section id="drag-area3">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <%-- <div class="card-header">
                               <div class="row">
                                    <div class="col-md-10 col-sm-12">
                                        <h4 class="card-title">Expense List</h4>
                                    </div>
                                    <div class="col-md-2 col-sm-12">
                                    </div>
                                </div>
                            </div>--%>
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblExpenseList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="Action text-center" style="width:25px !important;">Action</td>
                                                        <td class="Category" style="width:30px !important;">Category</td>
                                                        <td class="Date  text-center" style="width:40px !important;">Date</td>
                                                        <td class="Amount" style="width:30px !important;">Amount</td>
                                                        <td class="PaymentMode text-center" style="width:50px !important;">Payment Mode</td>
                                                        <td class="PaymentSource text-center" style="width:50px !important;">Payment Source</td>
                                                        <td class="Description" style="max-width: 400px !important; white-space: normal !important;">Expense Description</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex">
                                    <div class="form-group">
                                        <select class="form-control input-sm border-primary" id="ddlPageSize" onchange="getExpenseList(1);">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="ml-auto">
                                        <div class="Pager"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <center>
    <div id="ExpenseDetails" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-xl" style="width:900px;text-align:left">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Expense Details</h4>
                    <input type="hidden" id="hfAutoid" />
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="row ">
                                <div class="col-md-3">
                                    <label class="control-label">Expense Date</label>
                                    <span class="required">*</span>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 0.5rem;">
                                                    <span class="la la-calendar-o pl-0"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control input-sm border-primary date req" id="txtExpenseDate" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label class="control-label">Payment Mode</label>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <select id="ddlPaymentType" disabled class="form-control border-primary input-sm ddlreq">
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label class="control-label" style="white-space:nowrap">Payment Source</label>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <select id="ddlPaymentSource" disabled class="form-control border-primary input-sm ddlreq">
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <label class="control-label">Category</label>
                                    <span class="required">*</span>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <select id="ddlCategory" class="form-control border-primary input-sm ddlreq">
                                            <option value="0" selected="selected">Select</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label class="control-label" style="white-space:nowrap">Expense Amount</label>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding:0rem 0.5rem;">
                                                    <span class="pl-0">$</span>
                                                </span>
                                            </div>
                                            <input type="text" style="text-align: right" class="form-control input-sm border-primary req" disabled="disabled" onkeyup="ExpenseAmount();" onchange="ExpenseAmount1();" maxlength='10' value="0.00" id="txtExpenseAmount" onkeypress='return isNumberDecimalKey(event,this)' />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                 <div class="col-md-3">
                                    
                                </div>
                        <div class="col-md-9">
                            <div class="form-group">                              
                                    <label class="control-label text-danger" style="font-weight: bold; font-size: 14px; margin-right: 5px;">Note: </label>
                                    This will credit same amount in petty cash.
                            </div>
                        </div>
                    </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label class="control-label">Description</label>
                                    <span class="required">*</span>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <textarea class="form-control input-sm border-primary req" rows="6" maxlength="250" id="txtExpenseDescription"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="row" id="TblPayment">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblExpensivCurrency">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="AutoId text-center hidden">AutoId</td>
                                                        <td class="CurrencyValue text-center hidden">Value</td>
                                                        <td class="CurrencyName text-center" style="width: 80px">Bill</td>
                                                        <td style="width: 100px" class="TotalCount text-center">Total Count</td>
                                                        <td class="Amount price" style="width: 100px">Amount</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                   <tr>
                                                       <td colspan="2" class="text-bold-600">Total Amount</td>
                                                       <td id="TotalexpenseAmount" class="text-right text-bold-600">0.00</td>
                                                   </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success buttonAnimation round box-shadow-1  btn-sm" id="btnSave" style="margin: 0 5px" onclick="Save()">Submit</button>
                    <button type="button" class="btn btn-primary buttonAnimation round box-shadow-1  btn-sm" id="btnUpdate" style="margin: 0 5px; display: none" onclick="Update()">Update</button>
                    <button type="button" class="btn btn-danger buttonAnimation round box-shadow-1  btn-sm" onclick="textClear()" style="margin: 0 5px">Close</button>
                </div>
            </div>
        </div>
    </div>
         </center>
</asp:Content>

