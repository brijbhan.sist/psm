﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_productList : System.Web.UI.Page
{

    protected void Page_load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Admin/JS/productList.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredFields'>" + text + "</script>"
                ));
        }
        try
        {
            if (Session["DBLocation"] != null)
            {
                HDDomain.Value = Session["DBLocation"].ToString().ToLower();
                try
                {
                    if (HDDomain.Value != "psmnj" && HDDomain.Value != "psmnjm" && HDDomain.Value != "localhost"
                        && HDDomain.Value != "psm" && HDDomain.Value != "demo")
                    {
                        linkAddNewProduct.Visible = false;
                    }
                }
                catch (Exception)
                {
                }
                if (Session["EmpTypeNo"].ToString() == "3" || Session["EmpTypeNo"].ToString() == "2" || Session["EmpTypeNo"].ToString() == "11")
                {
                    hiddenForPacker.Value = Session["EmpAutoId"].ToString();
                }
                else
                {
                    hiddenForPacker.Value = "";
                }
            }
            else
            {
                Response.Redirect("/");
            }
        }
        catch
        {
            Session.Abandon();
            Response.Redirect("~/Default.aspx", false);
        }

    }
}