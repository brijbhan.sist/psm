﻿using System;
using System.Web;
using DllProductImageList;
using System.Web.Services;
using System.Data;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Data.SqlClient;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Drawing;

public partial class Admin_ProductImageList : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod(EnableSession = true)]
    public static string getProductList(string dataValues)
    {
        string msg = "";
        DataSet ds = new DataSet();
        PL_ProductImgList pobj = new PL_ProductImgList();
         var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.CategoryAutoId = Convert.ToInt32(jdv["CategoryAutoId"]);
                pobj.SubcategoryAutoId = Convert.ToInt32(jdv["SubcategoryAutoId"]);
                pobj.ProductId = jdv["ProductId"];
                pobj.ImageType = Convert.ToInt32(jdv["ImageType"]);
                pobj.ProductName = jdv["ProductName"];
                pobj.ProductStatus = Convert.ToInt32(jdv["ProductStatus"]);
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                BL_ProductImgList.ProductList(pobj);
                if (!pobj.isException)
                {
                    //if (pobj.ImageType == 3 || pobj.ImageType == 2)
                    //{
                    //    foreach (DataRow dr in pobj.Ds.Tables[1].Rows)
                    //    {
                    //        string dirFullPath = "";
                    //        dirFullPath = HttpContext.Current.Server.MapPath("~" + dr["Image"].ToString());
                    //        if (File.Exists(dirFullPath) && File.Exists(HttpContext.Current.Server.MapPath(dr["ThumbnailImageUrl"].ToString())))
                    //        {
                    //            FileInfo file = new FileInfo(dirFullPath);
                    //            var sizeInBytes = file.Length;
                    //            Bitmap img = new Bitmap(dirFullPath);
                    //            var imageHeight = img.Height;
                    //            var imageWidth = img.Width;
                    //            if (imageHeight == imageWidth)
                    //            {
                    //                dr.Delete();
                    //            }
                    //        }
                    //        else if (pobj.ImageType == 3)
                    //        {
                    //            dr.Delete();
                    //        }
                    //    }
                    //    pobj.Ds.Tables[1].AcceptChanges();
                    //}
                    //if (pobj.ImageType == 1)
                    //{
                    //    foreach (DataRow dr in pobj.Ds.Tables[1].Rows)
                    //    {
                    //        if (dr["Image"].ToString() != "" && dr["ThumbnailImageUrl"].ToString() != "")
                    //        {
                    //            try
                    //            {
                    //                if (File.Exists(HttpContext.Current.Server.MapPath(dr["Image"].ToString())) 
                    //                    && File.Exists(HttpContext.Current.Server.MapPath(dr["ThumbnailImageUrl"].ToString())) 
                    //                    && File.Exists(HttpContext.Current.Server.MapPath(dr["ThirdImage"].ToString())))
                    //                {
                    //                    dr.Delete();
                    //                }
                    //            }
                    //            catch (Exception)
                    //            {
                                    
                    //            }
                    //        }

                    //    }
                    //    pobj.Ds.Tables[1].AcceptChanges();
                    //}
                    //var count = pobj.Ds.Tables[1].Rows.Count;
                    //pobj.Ds.Tables[0].Rows[0]["RecordCount"] = count.ToString();
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message + msg;// "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string UploadImage(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_ProductImgList pobj = new PL_ProductImgList();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductId"]);
                pobj.ImageUrl = "/productThumb1000/1000_1000_" + (jdv["ThumbnailImageUrl1000"]);
                pobj.ThumbnailImageUrl = "/productThumbnailImage/100_100_" + (jdv["ThumbnailImageUrl"]);
                pobj.FourImageUrl = "/productThumbnailImage/400_400_" + (jdv["ThumbnailImageUrl"]);
                pobj.OriginalImageUrl= "/Attachments/" + (jdv["ImageUrl"]);
                BL_ProductImgList.UploadImage(pobj);
                if (!pobj.isException)
                {

                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception)
        {
            return "false";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string getProductDetail(string dataValues)
    {
        PL_ProductImgList pobj = new PL_ProductImgList();
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                BL_ProductImgList.getProductDetail(pobj);
                if (!pobj.isException)
                {


                    return pobj.Ds.GetXml();

                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }


}