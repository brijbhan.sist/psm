﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="ErrorTicketResponseForm.aspx.cs" Inherits="Admin_ErrorTicketResponseForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title"> Ticket Details</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Ticket Details
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
          <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
            <button class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false" onclick=" location.href='/Admin/ErrorTicketLog.aspx'"> Back</button>
          </div>
        </div>
    </div>


    <div class="content-body">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-header">
                                <h4 class="card-title">Ticket Details</h4>
                            </div>
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-3"> 
                                        <label>Ticket No</label>                                  
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <input type="text" id="txtticketid" class="form-control border-primary input-sm" readonly />
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                         <label>Ticket Date</label> 
                                    </div>
                                   <div class="col-sm-12 col-md-3">
                                        <input type="text" id="txtTicketDate" class="form-control border-primary input-sm" readonly />
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-3">  
                                         <label>User Name</label>                                     
                                    </div>
                                   <div class="col-sm-12 col-md-3">
                                        <input type="text" id="txtbyname" class="form-control border-primary input-sm" readonly />
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Priority</label>  
                                    </div>
                                   <div class="col-sm-12 col-md-3">
                                        <input type="text" id="txtPriority" class="form-control border-primary input-sm" readonly />
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-3">
                                         <label>Type</label>  
                                    </div>
                                     <div class="col-sm-12 col-md-3">
                                           <input type="text" id="txttype" class="form-control border-primary input-sm" readonly />
                                    </div>
                                     <div class="col-sm-12 col-md-3">
                                           <label>Current Status</label>  
                                    </div>
                                     <div class="col-sm-12 col-md-3">
                                           <input type="text" id="txtcurrentstat" class="form-control border-primary input-sm" readonly />
                                    </div>
                                </div>
                                 <div class="row form-group">
                                    <div class="col-sm-12 col-md-3">
                                         <label>Subject</label>  
                                    </div>
                                     <div class="col-sm-12 col-md-3">
                                           <input type="text" id="txtsub" class="form-control border-primary input-sm" readonly />
                                    </div>
                                    
                                    <div class="col-sm-12 col-md-3">
                                        <label>Attachment</label> 
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <a href="#" target="_blank" id="download" class="btn btn-sm btn-info">View</a>
                                    </div>     

                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-3">
                                         <label>Client Status</label>  <span style="color: Red"> *</span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <select id="ddlClientStatus" style="padding:2px !important" class="form-control border-primary input-sm req">
                                            <option value="0">-Select-</option>
                                             <option value="Open">Open</option>
                                             <option value="Close">Close</option>
                                        </select> 
                                    </div>
                                      <div class="col-sm-12 col-md-3">
                                         <label>Page Url</label> 
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                          <input type="text" id="txtPageUrl" runat="server" readonly class="form-control border-primary input-sm"/>
                                    </div>

                                </div>
                                <div class="row form-group" >
                                    <div class="col-sm-12 col-md-6" visible="false" id="developerStatus" runat="server">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-6 hide">
                                                <label>Developer Status</label>
                                                <span style="color: Red">*</span>
                                            </div>
                                            <div class="col-sm-12 col-md-6 hide">
                                                <select id="ddldevStatus" style="padding:2px !important" class="form-control border-primary input-sm req">
                                                    <option value="0">-Select-</option>
                                                    <option value="Open">Open</option>
                                                    <option value="Under Process">Under Process</option>
                                                    <option value="Close">Close</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Description</label>
                                    </div>
                                    <div class="col-sm-12 col-md-3" id="descrip" runat="server">
                                        <textarea id="txtDescription" cols="1" rows="5" runat="server" readonly class="form-control border-primary input-sm" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-sm-12">
                                       <label>Response</label>  <span class="required">*</span>
                                    </div>
                                    <div class="col-md-9 col-sm-12">
                                          <textarea id="txtResponse" runat="server" cols="1" rows="5" class="form-control border-primary input-sm req"/>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" id="btnSaveTicket">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

   
     <div class="content-body">
        <section id="drag-area1">
            <div class="row">
                              
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-header">
                                <h4 class="card-text"> Log List</h4>
                            </div>
                            <div class="card-body">
                                 <div class="row form-group ">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblErrorTicketResponse">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="SN text-center">SN</td>
                                                        <%--<td class="ByEmployee">Employee ID</td>--%>
                                                        <td class="EmpName">Employee Name</td>
                                                        <td class="ActionDate text-center">Response Date</td>

                                                          <td class="status text-center">Client Status</td>
                                                        <td class="developerstatus  text-center">Developer Status</td>
                                                        <td class="Action">Response</td>

                                                       <%-- <td class="Action">Action</td>                    --%>                              
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="Pager"></div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </section>
    </div>
       <script type="text/javascript">
           document.write('<scr' + 'ipt type="text/javascript" src="/Admin/JS/ErrorTicketResponse.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>

    
    
</asp:Content>

