﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DllProduct;
public partial class Admin_productMaster : System.Web.UI.Page
{

    protected void Page_load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Admin/JS/ProductMaster.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredFields'>" + text + "</script>"
                ));
        }
        try
        {

            if (Session["DBLocation"] != null)
            {
                HDDomain.Value = Session["DBLocation"].ToString().ToLower();
                try
                {


                    if (HDDomain.Value != "psmnj" && HDDomain.Value != "psmnjm" && HDDomain.Value != "localhost"
                        && HDDomain.Value != "psm" && HDDomain.Value != "demo")
                    {
                        if (Request.QueryString["ProductId"] == null)
                        {
                            Response.Redirect("/Admin/productList.aspx");
                        }
                        addProduct.Visible = false;
                    }
                }
                catch (Exception)
                {
                }
            }
            else
            {
                Response.Redirect("/");

            }

            if (Session["EmpTypeNo"].ToString() == "1" || Session["EmpTypeNo"].ToString() == "9")
            {
                hiddenText.Value = "";
            }
            else
            {
                hiddenText.Value = Session["EmpAutoId"].ToString();
            }

            if (Session["EmpTypeNo"].ToString() == "1")
            {
                CheckimpType.Value = Session["EmpTypeNo"].ToString();
            }
        }
        catch
        {
            Session.Abandon();
            Response.Redirect("~/Default.aspx", false);
        }
    }
    [WebMethod(EnableSession = true)]
    public static string bindLocation(string AutoId)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_Product pobj = new PL_Product();
            try
            {
                if (AutoId != "")
                {
                    pobj.AutoId = Convert.ToInt32(AutoId);
                }
                BL_Product.bindLocation(pobj);

                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                return json;
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

}