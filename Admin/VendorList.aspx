﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="VendorList.aspx.cs" Inherits="Admin_VendorList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
        .table th, .table td {
            padding: 0.75rem;
            vertical-align: top;
            border-top: 1px solid #626E82;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Vendor List</h3>
             <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage Vendor</a></li>
                        <li class="breadcrumb-item">Vendor List</li>
                    </ol>
                </div>
            </div>
        </div>
           <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a href="/Admin/VendorMaster.aspx" class="dropdown-item">Add Vendor</a>                    
                </div>
            </div>
        </div>
    </div>

     <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">                    
                        <div class="card-content collapse show">
                            <div class="card-body">
                                  <div class="row">
                                    <div class="col-md-3">
                                        <input type="text" class="form-control input-sm border-primary" placeholder=" Vendor Name" id="txtSVendorName" />
                                    </div>
                                     <div class="col-md-3">
                                        <input type="text" class="form-control input-sm border-primary" placeholder=" Cell" id="txtSVendorCell" />
                                    </div>
                                    <div class="col-md-1">
                                        <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnSearch">Search</button>
                                    </div>
                                </div>                      
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">                    
                        <div class="card-content collapse show">
                            <div class="card-body">                               
                               
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <div class="table-responsive">

                                            <table class="table table-striped table-bordered" id="tblVendorList" style="white-space: nowrap">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="Action text-center" style="width:20px !important">Action</td>
                                                        <td class="Status text-center width2per">Status</td>
                                                        <td class="VendorId text-center width-5-per">Vendor ID</td>
                                                        <td class="VendorName">Vendor Name</td>
                                                        <td class="ContactPerson">Contact Person</td>
                                                        <td class="Cell text-center  width-5-per">Cell</td>
                                                        <td class="Office text-center  width-5-per">Office Contact</td>
                                                        <td class="Email width-8-per">Email ID</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <select id="ddlPageSize" onchange="getVendorRecord(1)" class="form-control input-sm border-primary pagesize">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager text-right"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </div>

     <script type="text/javascript">
         document.write('<scr' + 'ipt type="text/javascript" src="/Admin/JS/VendorMaster.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
</asp:Content>

