﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" ClientIDMode="static" CodeFile="PriceLevelList.aspx.cs" Inherits="Admin_PriceLevelList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .table th, .table td {
            padding: 3px 8px !important;
            vertical-align: top;
            border-top: 1px solid #626E82;
        }
       .table tbody td {
            padding: 3px 8px !important;
            vertical-align: top;
            border-top: 1px solid #626E82;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Price Level List</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage Customer</a></li>
                        <li class="breadcrumb-item">Manage Price Level</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" id="btnAddPriceLevel" onclick="location.href='/Admin/PriceTemplate.aspx'">Add New Price Level</button>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body">
        <section id="drag-area">
            <div class="row">

                <div class="col-md-12">
                    <div class="card">                    
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-3">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Price Level Name" id="txtSPriceLevelName" />
                                    </div>
                                    <div class="col-md-3">
                                        <select class="form-control border-primary input-sm" id="ddlSPLStatus">
                                            <option value="22">All</option>
                                        </select>
                                    </div>
                                     <div class="col-md-3">
                                        <select class="form-control border-primary input-sm" id="ddlCustomerType">
                                            <option value="0">All Customer Type</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <button type="button" class="btn btn-info buttonAnimation pull-left round box-shadow-1  btn-sm" id="btnSearch" onclick="BindPriceLevel(1)">Search</button>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblPriceLevelList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="Action width3per text-center" style="white-space: nowrap">Action</td>
                                                        <td class="PriceLevelId text-center">Price Level<br /> ID</td>
                                                        <td class="PriceLevelName">Price Level Name</td>
                                                        <td class="Customer">Customer</td>
                                                        <td class="CustomerCount text-center width3per">No Of<br /> Customer</td>
                                                         <td class="CreatedBy">Created By</td>
                                                        <td class="CreatedOn text-center width3per">Created On</td>
                                                        <td class="Status text-center width3per">Status</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex">                                   
                                        <div class="form-group">
                                            <select class="form-control border-primary input-sm" id="ddlPageSize" onchange="BindPriceLevel(1)">

                                                <option value="10">10</option>
                                                <option value="50">50</option>
                                                <option value="100">100</option>
                                                <option value="500">500</option>
                                                <option value="1000">1000</option>
                                                <option value="0">All</option>
                                            </select>
                                        </div>
                                    <div class="ml-auto">
                                        <div class="Pager"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div id="PopPrintTemplate" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">PL Template</h4>
                        <button type="button" class="close pull-right" data-dismiss="modal">×</button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-10">
                                            <input type="radio" name="Template" id="chkdefault" checked/>
                                            <label for="chkdefault">PL Template 1</label>
                                        </div>
                                    </div>
                                    <input type="hidden" id="txtPrintOrderPop" />
                                    <div class="clearfix"></div>
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-10">
                                            <input type="radio" name="Template" id="checktemplate1" />
                                            <label for="chkdueamount">PL Template 2</label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1  btn-sm" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" onclick="PrintOrder()">Print</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</asp:Content>

