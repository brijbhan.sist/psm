﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true"
    CodeFile="ManageModule.aspx.cs" Inherits="Admin_ManageModule" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        #tblBrand_wrapper {
            padding: 0;
        }

        .desc {
            margin-bottom: 7px;
        }

        .addActionBtn {
            cursor: pointer;
        }

        .hideAutoId {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Manage Module</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage Application</a></li>
                        <li class="breadcrumb-item">Manage Module</li>
                    </ol>
                </div>
            </div>
        </div>
        <%-- <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a href="/Admin/AsignPage.aspx"><button type="button" class="dropdown-item">Assign Module</button></a>
                </div>
            </div>
        </div>--%>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Module Details</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label">Module Id</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" class="form-control input-sm border-primary" id="txtModuleId" runat="server" readonly="true" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label">Module Name&nbsp;<span class="required">*</span></label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" class="form-control input-sm border-primary req" id="txtModule" runat="server" required="true" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label">Has Parent ?</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <select class="form-control input-sm border-primary" id="HasModule" runat="server" onchange="manageModuleBox(this.value)">
                                                    <option value="0">No</option>
                                                    <option value="1">Yes</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label">Status</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <select class="form-control input-sm border-primary" id="status" runat="server">
                                                    <option value="1">Active</option>
                                                    <option value="0">Inactive</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                <input type="hidden" id="HiddenAutoId" />
                                    <div class="col-md-6" style="display: none;" id="parentModule">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label">Parent Module</label>
                                                <span class="required">*</span>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <select class="form-control input-sm border-primary" id="parentModuleBox" runat="server">
                                                    <option value="">Select</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label">Icon Class</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" class="form-control input-sm border-primary" id="iconClass" runat="server" />
                                            </div>                                             
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label">Menu Type</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                 <select class="form-control input-sm border-primary" id="ddlMenuType" runat="server">
                                                    <option value="1">Internal</option>
                                                    <option value="0">External</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label">Sequence Number</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" maxlength="3" class="form-control input-sm border-primary" id="txtSequenceNumber" onkeypress="return isNumberKey(event)" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="btn-group mr-1 pull-right">
                                            <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="savePageBtn">Save</button>
                                        </div>
                                        <div class="btn-group mr-1 pull-right">
                                            <button type="button" class="btn btn-secondary  buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" onclick="clearField()" id="btnReset">Reset</button>
                                        </div>
                                        <div class="btn-group mr-1 pull-right">
                                            <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnUpdate" style="display: none">Update</button>
                                        </div>
                                        <div class="btn-group mr-1 pull-right">
                                            <button type="button" class="btn btn-warning buttonAnimation pull-right round box-shadow-1 btn-sm" id="btnCancel" data-animation="pulse" style="display: none">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Module List</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-3 form-group">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Module Name" id="SearchModuleName" />
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <select class="form-control input-sm" id="searchParentModule">
                                            <option value="0">Select</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <select class="form-control input-sm" id="searchStatus">
                                            <option value="2">All</option>
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <button type="button" class="btn btn-info buttonAnimation pull-left round box-shadow-1 btn-sm" onclick="getModuleList(1);">Search</button>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="moduleList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="action text-center">Action</td>
                                                        <td class="AutoId" style="display: none">AutoId</td>
                                                        <td class="HasParent" style="display: none">Has Parent</td>
                                                        <td class="HiddenIconClass" style="display: none">IconClass</td>
                                                        <td class="HiddenStatus" style="display: none">Status</td>
                                                        <td class="HiddenParentModule" style="display: none">Parent Module</td>
                                                        <td class="ModuleId text-center">Module Id</td>
                                                         <td class="Module">Module</td>
                                                        <td class="MenuType">Menu Type</td>
                                                        <td class="ParentModule">Parent Module</td>
                                                        <td class="SequenceNo text-center">Sequence No</td>
                                                        <td class="Status text-center">Status</td>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                 <div class="row container-fluid">
                                    <div class="">
                                        <select class="form-control border-primary input-sm" id="ddlPageSize" onchange="getModuleList(1)">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                     <div class="col-md-10">
                                    <div class="Pager col-md-8 form-group" id="ModulePager" style="width:100% !important"></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="/Admin/JS/ManageModule.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
</asp:Content>
