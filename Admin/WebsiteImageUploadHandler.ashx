﻿<%@ WebHandler Language="C#" Class="WebsiteImageUploadHandler" %>

using System;
using System.Web;
using System.IO;
using System.Data;
using Newtonsoft.Json;

using System.Drawing;
using System.Drawing.Drawing2D;

public class WebsiteImageUploadHandler : IHttpHandler {

    public void ProcessRequest(HttpContext context)
    {
        try
        {
            string timeStamp = context.Request.QueryString["timestamp"].ToString();
            string fname = "";

            DataTable dt = new DataTable();
            DataRow dtRow;
            dt.Columns.Add("URL");
			  dt.Columns.Add("Thumb100");
            dt.Columns.Add("Thumb400");
            if (context.Request.Files.Count > 0)
            {
                #region image
                HttpFileCollection files = context.Request.Files;
                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFile file = files[i];
                    if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE" || HttpContext.Current.Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                    {
                        string[] testfiles = file.FileName.Split(new char[] { '\\' });
                        fname = testfiles[testfiles.Length - 1];
                    }
                    else
                    {
                        fname = file.FileName;
                    }
                    fname = timeStamp + "_" + fname;

                   
                    string[] getextension = fname.Split('.');

                    //arr_images += ",{\"URL\": \"" + fname + "\"}";
                    fname = Path.Combine(context.Server.MapPath("~/Attachments"), fname);
                    file.SaveAs(fname);
					  CreateThumbnail(100, 100, fname, "productThumbnailImage", timeStamp);
                        CreateThumbnail(400, 400, fname, "productThumbnailImage", timeStamp);
						 DataRow row = dt.NewRow();
                    row["URL"] = getextension[0] + "." + getextension[getextension.Length - 1];
                    row["Thumb100"] = "100_100_Thumbnail_" + getextension[0] + "." + getextension[getextension.Length - 1];
                    row["Thumb400"] = "400_400_Thumbnail_" + getextension[0] + "." + getextension[getextension.Length - 1];

                    dt.Rows.Add(row);

                }
                #endregion
                //arr_images += "]";
            }
            context.Response.ContentType = "text/plain";
            //context.Response.Write(arr_images.Replace("{},", ""));
            context.Response.Write(JsonConvert.SerializeObject(dt));
            HttpContext.Current.Response.StatusCode = 200;
        }
        catch (Exception ex)
        {
            context.Response.ContentType = "text/plain";
            context.Response.Write(ex.Message);
            HttpContext.Current.Response.StatusCode = 400;
        }
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }
public string CreateThumbnail(int maxWidth, int maxHeight, string path, string folderName, string timeStamp)
    {
        var image = System.Drawing.Image.FromFile(path);
        var ratioX = (double)maxWidth / image.Width;
        var ratioY = (double)maxHeight / image.Height;
        var ratio = Math.Min(ratioX, ratioY);
        var newWidth = (int)(image.Width * ratio);
        var newHeight = (int)(image.Height * ratio);
        var newImage = new Bitmap(newWidth, newHeight);
        Graphics thumbGraph = Graphics.FromImage(newImage);

        thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
        thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
        thumbGraph.DrawImage(image, 0, 0, newWidth, newHeight);
        image.Dispose();

        string fileRelativePath = "~/" + folderName + "/" + maxHeight + "_" + maxHeight + "_Thumbnail_" + Path.GetFileName(path);
        newImage.Save(HttpContext.Current.Server.MapPath(fileRelativePath), newImage.RawFormat);

        return fileRelativePath;
    }
}