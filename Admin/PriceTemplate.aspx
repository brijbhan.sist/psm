﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="PriceTemplate.aspx.cs" Inherits="WPriceTemplate" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Manage Price Level</h3>
            <input type="hidden" id="HPriceLevelAutoId" />
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage Customer</a></li>
                        <li class="breadcrumb-item"><a href="/admin/PriceLevelList.aspx">Price Level List</a></li>
                        <li class="breadcrumb-item">Manage Price Level</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="location.href='/Admin/PriceLevelList.aspx'">Back to Price Level List</button>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">

                            <h4 class="card-title">Price Level Details</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <label class="col-md-2 control-label">
                                        Customer Type <span class="required">&nbsp;*</span>

                                    </label>
                                    <div class="col-md-3 form-group">
                                        <select id="ddlCustomerType" class="form-control border-primary input-sm ddlreq">
                                            <option value="0">-Select Customer Type-</option>
                                        </select>
                                    </div>
                                    <label class="col-md-2 control-label">
                                        Price Level Name<span class="required">&nbsp;*</span>

                                    </label>
                                    <div class="col-md-3 form-group">
                                        <input type="text" class="form-control border-primary input-sm req" id="txtPriceLevelName" runat="server" onblur="uppercase(this.id)" />
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-2 control-label">
                                        Status
                                    </label>
                                    <div class="col-md-3 form-group">
                                        <select class="form-control border-primary input-sm " id="ddlStatus">
                                        </select>
                                    </div>

                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="btn-group mr-1 pull-right">
                                            <button type="button" runat="server" class="btn btn-success buttonAnimation round box-shadow-1  btn-sm" data-animation="pulse" id="btnSave" onclick="SavePriceLevel()">Save</button>
                                        </div>
                                        <div class="btn-group mr-1 pull-right">
                                            <button type="button" runat="server" class="btn btn-secondary  buttonAnimation round box-shadow-1  btn-sm" data-animation="pulse" id="btnReset">Reset</button>
                                        </div>
                                        <div class="btn-group mr-1 pull-right">
                                            <button type="button" runat="server" class="btn btn-primary buttonAnimation round box-shadow-1  btn-sm" data-animation="pulse" id="btnUpdate" style="display: none" onclick="UpdatePriceLevel()">Update</button>
                                        </div>
                                        <div class="btn-group mr-1 pull-right">
                                            <button type="button" runat="server" class="btn btn-warning buttonAnimation round box-shadow-1 btn-sm" id="btnCancel" onclick="cancelUpdate()" data-animation="pulse" style="display: none">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Product Selection and Pricing</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-4">
                                        <select class="form-control border-primary input-sm" id="ddlSCategory" onchange="BindSubCategory()">
                                            <option value="0">All Category</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 ">
                                        <select class="form-control border-primary input-sm" id="ddlSSubcategory">
                                            <option value="0">All Subcategory</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-4 ">

                                        <input type="text" onkeypress='return isNumberKey(event)' class="form-control border-primary input-sm" placeholder="Product ID" id="txtSProductId" />
                                    </div>
                                    <div class="col-md-4 ">

                                        <input type="text" class="form-control border-primary input-sm" placeholder="Product Name" id="txtSProductName" />
                                    </div>

                                    <div class="col-md-3 ">
                                        <button type="button" class="btn btn-info buttonAnimation pull-left round box-shadow-1  btn-sm" id="btnSearch">Search</button>
                                    </div>

                                </div>
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped" style="margin: 0;" id="tblProductList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="Action text-center">Action</td>
                                                        <td class="ProductId text-center">Product ID</td>
                                                        <td class="ProductName">Product Name</td>
                                                        <td class="UnitType  text-center">Unit</td>
                                                        <td class="MinPrice price">Min. Price</td>
                                                        <td class="BasePrice price">Base Price</td>
                                                        <td class="CustomPrice text-center" style="width: 20%;">Custom Price</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>

                                <div class="row container-fluid">
                                    <div class="">
                                        <select class="form-control border-primary input-sm" id="ddlPageSize">
                                            <option value="10">10</option>
                                            <option value="20">20</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager" id="ProductPager"></div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>

    <div id="ShowPODraftLog" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="modal-title">Price Level Log  <span id="pid" style="color: black;"></span> <span id="pname" style="color: black;"></span></h4>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="tblPriceLevelLog">
                            <thead class="bg-blue white">
                                <tr>
                                    <td class="Unit text-center">Unit</td>
                                    <td class="CustomPrice price">Custom Price</td>
                                    <td class="UpdateDate text-center">Update Date</td>
                                    <td class="UpdateBy text-center">Update By</td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <h5 class="well text-center" id="EmptyTable33" style="display: none">No data available.</h5>
                    </div>
                    <div class="row container-fluid">
                        <div class="">
                            <select class="form-control border-primary input-sm" id="ddlPageSizeLog" onchange="ShowProductLogAll(1)">
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                                <option value="0">All</option>
                            </select>
                        </div>
                        <div class="col-md-10">
                            <div class="Pager" id="LogPager"></div>
                        </div>

                    </div>
                </div>
                <input type="hidden" id="ProductAutoId" />
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="/Admin/JS/PriceTemplate.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>

</asp:Content>

