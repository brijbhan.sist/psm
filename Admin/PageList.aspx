﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true"
    CodeFile="PageList.aspx.cs" Inherits="Admin_PageList" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        #tblBrand_wrapper {
            padding: 0;
        }

        .desc {
            margin-bottom: 7px;
        }

        .addActionBtn {
            cursor: pointer;
        }

        .hideAutoId {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Page List</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage Application</a></li>
                        <li class="breadcrumb-item">Page List</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a href="/Admin/ManagePage.aspx">
                        <button type="button" class="dropdown-item">Add New Page</button></a>
                </div>
            </div>
        </div>
    </div>
    <div class="row actionDiv">
        <div class="col-md-12">
          <%--  <div class="card">--%>
               
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Page List</h4>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-3 form-group">
                                                    <input type="text" class="form-control border-primary input-sm" placeholder="Page ID" id="txtSearchPageId" />
                                                </div>
                                                <div class="col-md-3 form-group">
                                                    <input type="text" class="form-control border-primary input-sm" placeholder="Page Name" id="txtSearchPageName" />
                                                </div>
                                                <div class="col-md-3 form-group">
                                                    <select class="form-control input-sm" id="searchStatus">
                                                        <option value="2">All</option>
                                                        <option value="1">Active</option>
                                                        <option value="0">Inactive</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3 form-group">
                                                    <button type="button" class="btn btn-info buttonAnimation pull-left round box-shadow-1 btn-sm" onclick="getPageList();">Search</button>
                                                </div>
                                            </div>

                                            <br />
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered" id="pageList">
                                                    <thead class="bg-blue white">
                                                        <tr>
                                                            <td class="action text-center">Action</td>
                                                            <td class="AutoId" style="display: none">AutoId</td>
                                                            <td class="Role" style="display: none">Role</td>
                                                            <td class="HiddenType" style="display: none">Type</td>
                                                            <td class="HiddenStatus" style="display: none">Status</td>
                                                            <td class="PageId text-center">Page ID</td>
                                                            <td class="PageName">Page Name</td>
                                                            <td class="PageUrl">Page Url</td>
                                                            <td class="Type">Type</td>
                                                            <td class="UserType">Role</td>
                                                            <td class="Description">Description</td>
                                                            <td class="Status text-center">Status</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                                <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
               
          <%--  </div>--%>

        </div>
    </div>


    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="/Admin/JS/ManagePage.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
</asp:Content>
