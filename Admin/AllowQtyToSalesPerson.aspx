﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="AllowQtyToSalesPerson.aspx.cs" Inherits="Packer_AllowQtyToSalesPerson" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .table tbody td {
            padding: 3px !important;
            vertical-align: middle !important;
        }

        .table th, .table td {
            padding: 3px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Product Allocation</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Products</a></li>
                        <li class="breadcrumb-item">Product Allocation</li>
                        <li class="breadcrumb-item active"><a href="#" onclick="GetPageInformation(10003)"><i class="la la-question-circle"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>

        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" onclick="return OpenPopup();" class="dropdown-item">Add New</button>

                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="alert alert-success alert-dismissable fade in" id="alertSuccessDelete" style="display: none;">
                                    <a aria-label="close" id="successDeleteClose" class="close" style="cursor: pointer;">&times;</a>
                                    <span></span>
                                </div>
                                <input type="hidden" id="txtHOrderAutoId" class="form-control input-sm" />

                                <input type="hidden" id="hiddenPackerAutoId" runat="server" />
                                <input type="hidden" id="hiddenEmpType" runat="server" />
                                <div class="row">
                                    <div class="col-md-3 form-group" id="SalesPerson">
                                        <select class="form-control input-sm border-primary" id="ddlSalesPerson" runat="server">
                                            <option value="0">All Sales Person</option>
                                        </select>

                                    </div>
                                    <div class="col-md-3 form-group">
                                        <select class="form-control border-primary input-sm" style="width: 100% !important;" id="ddlProduct">
                                            <option value="0">All Product</option>
                                        </select>
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                    <span class="input-group-text" style="padding: 0rem 0.5rem;">
                                                       From Date <span class="la la-calendar-o"></span>
                                                    </span>
                                                </div>
                                            <input type="text" class="form-control input-sm border-primary" onchange="setdatevalidation(1)" placeholder="From Date" id="txtSFromDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <div class="input-group">
                                               <div class="input-group-prepend">
                                                    <span class="input-group-text" style="padding: 0rem 0.5rem;">
                                                       To Date <span class="la la-calendar-o"></span>
                                                    </span>
                                                </div>
                                            <input type="text" class="form-control input-sm border-primary" onchange="setdatevalidation(2)" placeholder="To Date" id="txtSToDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-md-12 form-group text-right">
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" id="btnSearch" onclick=" getAllowQtyList(1);">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card" style="min-height: 350px">
                <div class="card-content collapse show">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered" id="tblOrderList">
                                        <thead class="bg-blue white">
                                            <tr>
                                                <td class="action text-center" style="width: 10%;">Action</td>
                                                <td class="AllowDate text-center" style="width: 10%;">Allow Date</td>
                                                <td class="SalesPerson" style="width: 10%;">Sales Person</td>
                                                <td class="ProductId text-center" style="width: 10%;">Product ID</td>
                                                <td class="Product">Product Name</td>
                                                <td class="DefautlUnit text-center" style="width: 10%;">Default Unit</td>
                                                <td class="AllowQty text-center" style="width: 10%;">Allow Qty In Piece</td>
                                                <td class="UseQty text-center" style="width: 10%;">Used Qty</td>
                                                <td class="RemainQty text-center" style="width: 10%;">Remaining Qty</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                    <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>

                                </div>
                            </div>
                        </div>
                        <div class="d-flex">
                            <div class="form-group">
                                <select id="ddlPageSize" class="form-control border-primary input-sm" onchange=" getAllowQtyList(1);">
                                    <option selected="selected" value="10">10</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                    <option value="500">500</option>
                                    <option value="1000">1000</option>
                                    <option value="0">All</option>
                                </select>
                            </div>

                            <div class="ml-auto">
                                <div class="Pager"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div id="ModalSaveAllowQty" class="modal fade text-left show" role="dialog">
        <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><span id="Pop_heading">Add New</span></h4>
                    <button type="button" class="close" onclick="return Cancel();">&times;</button>
                    <br />
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <label class="control-label">Date<span class="required">&nbsp;*</span></label>
                                        </div>
                                        <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="padding: 0rem 1rem;">
                                                        <span class="la la-calendar-o"></span>
                                                    </span>
                                                </div>
                                                <input type="text" class="form-control input-sm border-primary req" placeholder="Enter Date from to Allow Quantity" id="Pop_txtAllowDate" onfocus="this.select()" />
                                                <input type="hidden" id="Pop_AutoId" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <label class="control-label">Sales Person<span class="required">&nbsp;*</span></label>
                                        </div>
                                        <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                                            <select class="form-control input-sm border-primary ddlreq" style="width: 100% !important;" id="Pop_ddlSalesPerson">
                                                <option value="0">Select Sales Person</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <label class="control-label">Product<span class="required">&nbsp;*</span></label>
                                        </div>
                                        <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                                            <select class="form-control border-primary input-sm ddlreq" style="width: 100% !important;" onchange="return SetProductUnit();" id="Pop_ddlProduct">
                                                <option value="0">Select Product</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <label class="control-label" style="white-space: nowrap">Allow Qty In Pieces<span class="required">&nbsp;*</span></label>
                                        </div>
                                        <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                                            <input class="form-control border-primary input-sm req" maxlength="5" onkeypress="return isNumberDecimalKey(event,this)" id="Pop_TxtAllowPieces" />
                                            <input type="hidden" id="Pop_UsedQty" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="alert alert-danger alertSmall" id="alertStockQty" style="text-align: center; color: white !important;"></div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" onclick="return Cancel();" class="btn btn-warning buttonAnimation round box-shadow-1 btn-sm pull-right">Close</button>
                            <button type="button" id="Pop_save_btn" class="btn btn-success buttonAnimation round box-shadow-1 btn-sm pull-right" style="margin-right: 10px;" onclick="Popsave();">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="mdAllowQtyDetails" class="modal fade text-left show" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><span>Allocated Quantity Summary</span></h4>
                </div>
                <div class="modal-body" style="padding-bottom: 0">
                    <div class="row form-group">
                        <div class="col-md-2"><b>Allow Date</b></div>
                        <div class="col-md-8"><span id="spAllowDate"></span></div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-2"><b>Sales Person</b></div>
                        <div class="col-md-8"><span id="spSalesPerson"></span></div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-2"><b>Product Name</b></div>
                        <div class="col-md-8"><span id="spProductName"></span></div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered" id="tblAllocatedQtySummary">
                                    <thead class="bg-blue white">
                                        <tr>
                                            <td class="OrderNo text-center width3per" style="width: 10%;">Order No</td>
                                            <td class="OrderDate text-center width3per" style="width: 10%;">Order Date</td>
                                            <td class="CustomerName" style="width: 10%;">Customer Name</td>
                                            <%--<td class="PackedQty text-center width3per" style="width: 10%;">Packed Qty</td>--%>
                                            <td class="DefautlUnit text-center width3per" style="width: 10%;">Sold Unit</td>
                                            <td class="PackedPieces text-center width3per">Used Pieces</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr style="font-weight: bold; text-align: center">
                                            <td colspan="4">Total</td>
                                            <td id="tdPackedPieces">0</td>
                                        </tr>
                                    </tfoot>
                                </table>
                                <h5 class="well text-center" id="EmptyTable2" style="display: none">No data available.</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" data-dismiss="modal" class="btn btn-warning buttonAnimation round box-shadow-1 btn-sm pull-right">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
