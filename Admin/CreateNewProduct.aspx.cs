﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DLLCreateNewProduct;
using System.Data;
using System.Web.Services;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

public partial class Admin_CreateNewProduct : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            ddlBind();
        }
    }
    [WebMethod(EnableSession = true)]
    public static string ddlBind()
    {
        P_CreateNewProduct pobj = new P_CreateNewProduct();
        B_CreateNewProduct.ddlBind(pobj);
        if (!pobj.isException)
        {
            string json = "";
            foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
            {
                json += dr[0];
            }
            return json;
        }
        else
        {
            return pobj.exceptionMessage;
        }
    }
}