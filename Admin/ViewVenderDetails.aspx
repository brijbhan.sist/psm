﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="ViewVenderDetails.aspx.cs" Inherits="Admin_ViewVenderDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Vendor Details</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                            <li class="breadcrumb-item"><a href="/Admin/VendorList.aspx">Manage Vendor</a>
                        <li class="breadcrumb-item"><a href="/Admin/VendorList.aspx">Vendor List</a>
                        </li>
                        <li class="breadcrumb-item">Vendor Details
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a href="/Admin/VendorList.aspx" class="dropdown-item">Go to Vendor List</a>
                    <input type="hidden" id="hiddenEmpTypeVal" runat="server" />
                    <%
                        if (Session["EmpType"] != null)
                        {
                            if (Session["EmpType"].ToString() != "5" && Session["EmpType"].ToString() != "3" && Session["EmpType"].ToString() != "7")
                            {
                    %>
                    <a id="EditVendor" class="dropdown-item">Edit</a>
                    <a id="DeleteVendor" class="dropdown-item" onclick="deleterecord()">Delete</a>
                    <%
                            }
                        }%>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-3">
                                        <label class="control-label"><b>Vendor ID :</b></label>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <div class="form-group">
                                            <span id="spnVendorId"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label class="control-label">
                                            <b>Vendor Name : </b>
                                        </label>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <div class="form-group">
                                            <span id="spnVendorName" style="text-transform: capitalize;"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-3 col-sm-12">
                                        <label class="control-label">
                                            <b>Contact Person :  </b>
                                        </label>
                                    </div>
                                    <div class="col-md-3 col-sm-12">
                                        <div class="form-group">
                                            <span id="spanContactPerson" style="text-transform: capitalize;"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12">
                                        <label class="control-label">
                                            <b>Vendor Type :</b>
                                        </label>
                                    </div>
                                    <div class="col-md-3 col-sm-12">
                                        <div class="form-group">
                                            <span id="spanVendorType" style="text-transform: capitalize;"></span>
                                        </div>
                                    </div>
                                </div>                              
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="content-body">
        <section id="drag-area1">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body" id="navbar">
                                <div class="row">
                                    <div class="col-md-12 col-sm12">
                                        <ul class="nav nav-tabs nav-topline">
                                          <%--  <li class="nav-item">
                                                <a class="nav-link active" id="base-OrderList" data-toggle="tab" aria-controls="tab21" href="#OrderList" aria-expanded="true">PO List</a>
                                            </li>  --%>                                         
                                            <li class="nav-item">
                                                <a class="nav-link active" id="base-ReceivedBill" data-toggle="tab" aria-controls="tab23" href="#ReceivedBill" aria-expanded="false">Received Bill</a>
                                            </li>
                                             <li class="nav-item">
                                                <a class="nav-link" id="base-PaymentHistory" data-toggle="tab" aria-controls="tab22" href="#PaymentHistory" aria-expanded="false" onclick="getPayList(1)">Payment History</a>
                                            </li>
                                          <%--  <li class="nav-item">
                                                <a class="nav-link" id="base-DueOrderList" data-toggle="tab" aria-controls="tab21" href="#DueOrderList" aria-expanded="true">Due Payment</a>
                                            </li>--%>
                                          <%--  <li class="nav-item">
                                                <a class="nav-link" id="base-CollectionDetails" data-toggle="tab" aria-controls="tab23" href="#CollectionDetails" aria-expanded="false">Collection Details</a>
                                            </li>    --%>                                      
                                            <li class="nav-item">
                                                <a class="nav-link" id="base-CustomerLog" data-toggle="tab" aria-controls="tab23" href="#CustomerLog" aria-expanded="false" onclick="OrderLog(1)">Vendor Log</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="base-CustomerDocuments" data-toggle="tab" aria-controls="tab23" href="#CustomerDocuments" aria-expanded="false">Vendor Documents</a>
                                            </li>   
                                            <li class="nav-item">
                                                <a class="nav-link" id="base-BankDetails" data-toggle="tab" aria-controls="tab23" href="#BankDetails" aria-expanded="false">Bank Details</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tab-content px-1 pt-1 border-grey border-lighten-2 border-0-top">
                                    <div role="tabpanel" class="row tab-pane" id="OrderList" aria-expanded="true" aria-labelledby="base-OrderList">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                   <table class="table table-striped table-bordered" id="tblPOList">
                                                <thead class="bg-blue white">
                                                    <tr>                                                                                                          
                                                         <td class="BillNo text-center">PO No</td>
                                                         <td class="BillDate text-center">PO Date</td>
                                                         <td class="NoOfItems text-center">No. of Items</td> 
                                                            <td class="Status">Status</td>
                                                        <td class="VendorName">Requested By</td>
                                                        <td class="Remarks">Remark</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                    <%--   <tfoot>
                                                    <tr>
                                                        <td colspan="4" style="text-align: right">Total</td>
                                                        <td id="gTotal" style="text-align: right"></td>
                                                        <td></td>

                                                    </tr>
                                                </tfoot>--%>
                                            </table>
                                                </div>
                                            </div>
                                             <div class="row container-fluid">
                                        <div class="form-group"> 
                                             <div class="col-md-12 col-sm-12">                                      
                                            <select id="ddlPageSize" class="form-control border-primary input-sm">
                                                <option selected="selected" value="10">10</option>
                                                <option value="50">50</option>
                                                <option value="100">100</option>
                                                <option value="500">500</option>
                                                <option value="1000">1000</option>
                                                <option value="0">All</option>
                                            </select> 
                                                 </div>                                    
                                    </div>
                                    <div class="col-md-10 col-sm-12">
                                      <%--  <div class="Pager" id="POPage"></div>--%>
                                    </div>
                                </div>

                                        </div>
                                    </div>
                                    <div class="row tab-pane" id="PaymentHistory" aria-labelledby="base-PaymentHistory">
                                        <div class="row ml-0">

                                                            <div class="col-md-4 form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                                                            <span class="la la-calendar-o"></span>
                                                                        </span>
                                                                    </div>
                                                                    <input type="text" class="form-control border-primary input-sm" placeholder="Vendor Payment From Date" id="txtSFromDate" onfocus="this.select()" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                                                            <span class="la la-calendar-o"></span>
                                                                        </span>
                                                                    </div>
                                                                    <input type="text" class="form-control border-primary input-sm" placeholder="Vendor Payment To Date" id="txtSToDate" onfocus="this.select()" />
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2 form-group">
                                                                <button type="button" class="btn btn-info buttonAnimation pull-left round box-shadow-1  btn-sm" data-animation="pulse" id="btnSearch" onclick="getPayList(1)">Search</button>
                                                            </div>
                                                        </div>
                                        <div class="row">
                                        <div class="col-md-12">
                                            <div class="card">

                                                <div class="card-content collapse show">
                                                    <div class="card-body">
                                                      
                                                        

                                                        <div class="row">
                                                            <div class="table-responsive">
                                                                <table class="table table-striped table-bordered" id="tblPayDetails">
                                                                    <thead class="bg-blue white">
                                                                        <tr>
                                                                            <td class="action text-center">Action</td>
                                                                            <td class="PayID  text-center">Payment ID</td>
                                                                            <td class="PaymentDate  text-center">Payment Date</td>
                                                                            <td class="PaymentMode  text-center">Payment Mode</td>
                                                                            <td class="PaymentType  text-center">Payment Type</td>
                                                                            <td class="PaymentAmount price">Payment Amount</td>
                                                                            <td class="ReferenceId text-center">No. of Bill</td>
                                                                            <td class="Status  text-center">Status</td>
                                                                            <td class="VendorName">Paid By</td>
                                                                            <td class="Remark">Remark</td>

                                                                            <%--  <td class="ReferenceId">Reference ID</td>--%>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    </tbody>
                                                                </table>
                                                                <h5 class="well text-center" id="EmptyTable1" style="display: none">No data available.</h5>
                                                            </div>
                                                           
                                                        </div>
                                                        
                                                    </div> 
                                                    <div class="Pager" id="PayHis"></div>
                                                </div>

                                            </div>
                                        </div>
                                            </div>
                                    </div>

                                    <div class="row tab-pane active" id="ReceivedBill" aria-labelledby="base-ReceivedBill">
                                        <div class="row">
                                        <div class="col-md-12">
                                            <div class="card">

                                                <div class="card-content collapse show">
                                                    <div class="card-body">
                                                     
                                                        <div class="row">
                                                            <div class="table-responsive">
                                                                <table class="table table-striped table-bordered" id="tblReceivedBill">
                                                                    <thead class="bg-blue white">
                                                                        <tr>
                                                                            <td class="BillNo text-center">Bill No.</td>
                                                                            <td class="BillDate text-center">Bill Date</td>
                                                                            <td class="PONo  text-center">PO No.</td>
                                                                            <td class="NoofItems  text-center">No. of Items</td>
                                                                            <td class="TotalAmount price">Total Amount</td>
                                                                            <td class="PaidAmount price">Paid Amount</td>
                                                                            <td class="DueAmount price">Due Amount</td>
                                                                            <td class="ReceivedBy">Received By</td>
                                                                            <td class="ReceivedDate text-center">Received Date</td>

                                                                            <%--  <td class="ReferenceId">Reference ID</td>--%>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    </tbody>
                                                                    <tfoot>
                                                                        <tr style="font-weight:bold;">
                                                                            <td colspan="4" class="text-right">Total</td>
                                                                            <td id="TotalAmounts" style="text-align: right">0.00</td>
                                                                            <td id="PaidAmounts" style="text-align: right">0.00</td>
                                                                            <td id="DueAmounts" style="text-align: right">0.00</td>
                                                                            <td></td>
                                                                            <td></td>
                                                                        </tr>
                                                                    </tfoot>
                                                                </table>
                                                                <h5 class="well text-center" id="EmptyTable12" style="display: none">No data available.</h5>
                                                            </div>
                                                            
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="Pager" id="PagingReceived"></div>
                                                </div>

                                            </div>
                                        </div>
                                            </div>
                                    </div>
                                    <div class="row tab-pane" id="DueOrderList" aria-labelledby="base-DueOrderList">
                                        <div id="pay">
                                            <div class="row form-group">
                                                <div class="col-md-3 col-sm-3">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control border-primary input-sm" placeholder="Order No" id="txtSCustomerId" />
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-3">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                                    <span class="la la-calendar-o"></span>
                                                                </span>
                                                            </div>
                                                            <input type="text" class="form-control border-primary input-sm date" placeholder="Order From Date" id="txtFromDate" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-3">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                                    <span class="la la-calendar-o"></span>
                                                                </span>
                                                            </div>
                                                            <input type="text" class="form-control border-primary input-sm date" placeholder="Order To Date" id="txtToDate" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-3">
                                                    <div class="form-group">
                                                        <button type="button" class="btn btn-primary buttonAnimation round box-shadow-1  btn-sm" id="btnSearchs" onclick="CustomerDuePayment()">Search</button>
                                                        <button type="button" class="btn btn-success buttonAnimation round box-shadow-1  btn-sm" id="btnPayNow1" style="display: none" onclick="PayNow()">Pay Now</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="row form-group">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered" id="Table4">
                                                        <thead class="bg-blue white">
                                                            <tr>
                                                                <td class="OrderNo text-center">Order No</td>
                                                                <td class="OrderType text-center">Order Type</td>
                                                                <td class="OrderDate text-center">Order Date</td>
                                                                <td class="OrderAmount" style="text-align: right">Order Amount</td>
                                                                <td class="PaidAmount" style="text-align: right">Paid Amount</td>
                                                                <td class="DueAmount" style="text-align: right">Due Amount</td>
                                                                <td class="CheckBoc paynow text-center" style="display: none;">Select Check Box</td>
                                                                <td class="PayAmount paynow" style="display: none; text-align: right">Pay Amount</td>
                                                                <td class="payDueAmount paynow" style="display: none; text-align: right">Due Balance</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <td colspan="3">Total</td>
                                                                <td id="TotalOrderAmount" style="text-align: right;font-weight:bold">0.00</td>
                                                                <td id="TotalPaidAmount" style="text-align: right;font-weight:bold">0.00</td>
                                                                <td id="DueAmount" style="text-align: right;font-weight:bold">0.00</td>
                                                                <td id="Td3" class="paynow" style="display: none; text-align: right"></td>
                                                                <td id="TotalPayAmount" class="paynow" style="display: none; text-align: right;font-weight:bold">0.00</td>
                                                                <td id="TotalpayDueAmount" class="paynow" style="display: none; text-align: right;font-weight:bold">0.00</td>

                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row DuehideCurrency" style="display: none">
                                            <div class="col-md-6">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered" id="tblCurrencyList">
                                                        <thead class="bg-blue white">
                                                            <tr>
                                                                <td class="CurrencyName text-center">Bill</td>
                                                                <td class="NoofRupee text-center" style="width: 150px;">Total Count</td>
                                                                <td class="Total" style="text-align: right; width: 150px">Total Amount</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <td colspan="2"style="text-align: right">Total</td>
                                                                <td id="TotalAmount" style="text-align: right;font-weight:bold">0.00</td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12">
                                                        <label class="control-label">
                                                            <span style="white-space: nowrap">Short Amount :</span>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-4 col-sm-12">
                                                        <div class="form-group">
                                                            <input type="text" id="txtSortAmount" readonly="true"  onkeypress="return isNumberDecimalKey(event,this)" class="form-control border-primary input-sm" value="0.00" style="text-align: right" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-2">
                                                        <label class="control-label">
                                                            <span style="white-space: nowrap">Total Amount :</span>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-4 col-sm-4">
                                                        <div class="form-group">
                                                            <input type="text" id="txtTotalCurrencyAmount" readonly="true" onkeypress="return isNumberDecimalKey(event,this)" 
                                                                class="form-control border-primary input-sm" value="0.00" style="text-align: right;font-weight:bold;font-size: 17px;;" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-12 col-sm-12 full-right">
                                                <button type="button" class="btn btn-danger buttonAnimation round box-shadow-1  btn-sm pull-right" id="btnCancel" onclick="PayCancel()" style="display: none">Cancel</button>
                                                <button type="button" class="btn btn-warning buttonAnimation round box-shadow-1  btn-sm pull-right" id="btnpaymentCancel" onclick="paymentCancellation()" style="display: none; margin-right: 10px;">Cancel Payment</button>
                                                <button type="button" id="btnSubmit" onclick="SavePayment()" class="btn btn-success buttonAnimation round box-shadow-1  btn-sm pull-right paynow" style="display: none; margin-right: 10px;">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row tab-pane" id="CollectionDetails" aria-labelledby="base-CollectionDetails">
                                        <div class="row form-group">
                                            <div class="col-md-3 col-sm-3">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                <span class="la la-calendar-o"></span>
                                                            </span>
                                                        </div>
                                                        <input type="text" class="form-control border-primary input-sm date" placeholder="From Date" id="FromDate" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-3">
                                                <div class="form-group">
                                                    <select class="form-control border-primary input-sm" id="ddlStatus">
                                                        <option value="0">All</option>
                                                        <option value="1">New</option>
                                                        <option value="2">Settled</option>
                                                        <option value="3">Cancelled</option>
                                                    </select>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-3 col-sm-3">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                <span class="la la-calendar-o"></span>
                                                            </span>
                                                        </div>
                                                        <input type="text" class="form-control border-primary input-sm date" placeholder="To Date" id="ToDate" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8 col-sm-5">
                                                <div class="form-group">
                                                    <button type="button" class="btn btn-primary buttonAnimation round box-shadow-1  btn-sm" id="Button4" onclick="CollectionDetails()">Search</button>

                                                    <%
                                                        if (Session["EmpTypeNo"] != null)
                                                        {
                                                            if (Session["EmpTypeNo"].ToString() == "6")
                                                            {
                                                    %>

                                                    <button type="button" class="btn btn-warning buttonAnimation round box-shadow-1  btn-sm" id="btnProcess" onclick="ProcessPayment()">Process</button>
                                                    <button type="button" class="btn btn-success buttonAnimation round box-shadow-1  btn-sm" id="btnPaywithCredit" onclick="ProcessStoreCredit()">Pay with Store Credit</button>
                                                    <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" id="btnOnHold" style="display: none" onclick="OnHoldCheck()">On Hold</button>
                                                     <button type="button" class="btn btn-danger buttonAnimation round box-shadow-1  btn-sm" id="btnCancelCheck" style="display: none" onclick="CancelCheckSecurity()">Cancel</button>
                                                    <%
                                                            }
                                                        }
                                                    %>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered" id="tblCollectionDetails">
                                                        <thead class="bg-blue white">
                                                            <tr>
                                                                <td class="action text-center">Action</td>
                                                    <td class="PayID  text-center">Payment ID</td>
                                                    <td class="PaymentDate  text-center">Payment Date</td>
                                                    <td class="VendorName">Vendor  Name</td>
                                                    <td class="PaymentAmount price">Payment Amount</td>
                                                    <td class="PaymentMode  text-center">Payment Mode</td>
                                                    <td class="ReferenceId">Reference ID</td>
                                                    <td class="Remark">Remark</td>
                                                    <td class="Status  text-center">Status</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row tab-pane" id="CustomerLog" aria-labelledby="base-CustomerLog">
                                       <%-- <div class="row">
                                             <div class="col-md-3 col-sm-3">
                                                    <div class="form-group">
                                                        <button type="button" class="btn btn-primary buttonAnimation round box-shadow-1  btn-sm" id="btnSearc" onclick="getPayList1(1)">Search</button>
                                                     </div>
                                                </div>
                                        </div>--%>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered" id="tblOrderLog">
                                                        <thead class="bg-blue white">
                                                            <tr>
                                                                <td class="SrNo text-center">SN</td>
                                                                <td class="ActionBy text-center">Action By</td>
                                                                <td class="Date text-center">Date</td>
                                                                <td class="Action">Action</td>
                                                                <td class="Remark">Remark</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="Pager" id="OrdLog"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row tab-pane" id="CustomerDocuments" aria-labelledby="base-CustomerDocuments">
                                        <div class="row form-group">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered" id="tblCustomerDocuments">
                                                        <thead class="bg-blue white">
                                                            <tr>
                                                                <td class="SrNo" style="text-align:center">SN</td>
                                                                <td class="DocumentName">Document Name</td>
                                                                <td class="Document" style="text-align:center">Download/View</td>
                                                                <td class="ViewDownload" style="text-align:center">Action</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td class="SrNo">1</td>
                                                                <td class="DocumentName"></td>
                                                                <td class="Document"></td>
                                                                <td class="ViewDownload"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-12">
                                                <div class="form-group  pull-right">
                                                    <button type="button" id="btnbrowse" onclick="uploaddocument()" class="btn btn-sm btn-success">Upload Document</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="row tab-pane" id="StoreCreditlog" aria-labelledby="base-StoreCreditlog">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered" id="tblStoreCreditlog">
                                                        <thead class="bg-blue white">
                                                            <tr>
                                                                <td class="CreatedDate text-center">Receive Date</td>
                                                                <td class="ReferenceId text-center">Reference No</td>
                                                                <td class="Receivedby">Collect By</td>
                                                                <td class="Amount price">Amount</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                         <div class="row tab-pane" id="BankDetails" aria-labelledby="base-BankDetails">
                                             <div class="row form-group">
                                                 <div class="col-md-6">
                                                     <div class="form-group  pull-left">
                                                         <button type="button" id="btnSaveBankDetails" onclick="OpenPopup()" class="btn btn-sm btn-success">Add Bank Details</button>
                                                     </div>
                                                 </div>
                                                 <div class="col-md-6">
                                                     <div class="form-group  pull-left">
                                                         <button type="button" id="btnSaveCardDetails" onclick="OpenPopup1()" class="btn btn-sm btn-success">Add Card Details</button>
                                                     </div>
                                                 </div>
                                             </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered" id="tblBankDetails">
                                                        <thead class="bg-blue white">
                                                            <tr>
                                                                <td class="Action text-center">Action</td>
                                                                <td class="BankName">Bank Name</td>
                                                                 <td class="Routing text-center"">Routing No.</td>
                                                                <td class="BankACC text-center">Bank A/C</td>
                                                                <td class="BankRemarks">Remarks</td>
                                                               <%-- <td class="CardType text-center"">Card Type</td>
                                                                <td class="CardNo text-center"">Card No</td>
                                                                <td class="ExpiryDate text-center"">Expiry Date</td>
                                                                <td class="CVV text-center">CVV</td>--%>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                             <div class="col-md-6">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered" id="tblCardDetails">
                                                        <thead class="bg-blue white">
                                                            <tr>
                                                                <td class="Action text-center">Action</td>
                                                                <td class="CardType">Card Type</td>
                                                                <td class="CardNo text-center"">Card No</td>
                                                                <td class="ExpiryDate text-center">Expiry Date</td>
                                                                <td class="CVV text-center">CVV</td>
                                                                <td class="Zipcode text-center">Zipcode</td>
                                                                <td class="CardRemarks">Remarks</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
        <div id="PopCustomerDocuments" class="modal fade" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="padding-bottom:0 !important">
                    <h4>Upload Document</h4>
                    <input type="hidden" id="hdnuploadAutoId" />
                </div>
                <div class="modal-body" style="padding-bottom:0 !important">
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label class="control-label">
                                Document Name : <span class="required">*</span>
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm document1" placeholder="Document Name" id="txtDocumentName" runat="server" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">
                                Document : <span class="required">*</span>
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="file" class="form-control border-primary input-sm document1" style="height: 4rem" id="uploadedFile" runat="server" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                Allowed file type : <span class="required">gif,png,jpg,jpeg,doc,pdf</span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <a href="#" target="_blank" id="hrefViewDoc" class="btn btn-sm btn-success">View</a>

                            </div>
                        </div>
                    </div>                    
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-success pull-right  buttonAnimation round box-shadow-1  btn-sm" id="Button2" onclick="DocumentSave()" style="margin: 0 5px 0 5px">Save</button>
                            <button type="button" class="btn btn-warning pull-right  buttonAnimation round box-shadow-1  btn-sm" id="Button3" onclick="UpdateDocument()" style="display: none; margin: 0 5px 0 5px">Update</button>
                            <button type="button" class="btn btn-danger pull-right  buttonAnimation round box-shadow-1  btn-sm" onclick="clearBorder()" data-dismiss="modal" style="margin: 0 5px 0 5px">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
           <div id="PopBankDetails" class="modal fade" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="padding-bottom:0 !important">
                    <h4>Add Bank Details</h4>
                    <input type="hidden" id="hdnAutoId" />
                </div>
                <div class="modal-body" style="padding-bottom:0 !important">
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label class="control-label">
                                Bank Name  <span class="required">*</span>
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm document req" placeholder="Bank Name" id="txtBankName" runat="server" maxlength="50"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">
                                Routing No 
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm document" placeholder="Routing No" id="txtRoutingNo" onkeypress='return isNumberKey(event)' runat="server" maxlength="10"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">
                                Bank A/C  <span class="required">*</span>
                            </label>
                        </div>
                       <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm document req" placeholder="Bank A/C" id="txtACName" runat="server" onkeypress='return isNumberKey(event)' maxlength="20"/>
                            </div>
                        </div>
                          <div class="col-md-4">
                            <label class="control-label">
                                Remarks  
                            </label>
                        </div>
                       <div class="col-md-8">
                            <div class="form-group">
                                 <textarea class="form-control border-primary input-sm" id="txtBankRemarks"></textarea>
                            </div>
                        </div>
                        <br />

                    </div>                   
                </div>
                <div class="modal-footer">
                     <div class="row">
                        <div class="col-md-12">
                             <button type="button" class="btn btn-danger pull-right  buttonAnimation round box-shadow-1  btn-sm" onclick="clearBorder()" data-dismiss="modal" style="margin: 0 5px 0 5px">Close</button>
                            <button type="button" class="btn btn-success pull-right  buttonAnimation round box-shadow-1  btn-sm" id="btnSaves" onclick="SaveBankDetails()" style="margin: 0 5px 0 5px">Save</button>
                            <button type="button" class="btn btn-warning pull-right  buttonAnimation round box-shadow-1  btn-sm" id="btnUpdates" onclick="UpdateBankDetails()" style="display: none; margin: 0 5px 0 5px">Update</button>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

      <div id="PopCardDetails" class="modal fade" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="padding-bottom:0 !important">
                    <h4>Add Card Details</h4>
                    <input type="hidden" id="hdnCardAutoId" />
                </div>
                <div class="modal-body"style="padding-bottom:0 !important">
                    <div class="row form-group">
                     
                         <div class="col-md-4">
                            <label class="control-label">
                                Card Type  <span class="required">*</span>
                            </label>
                        </div>
                       <div class="col-md-8">
                            <div class="form-group">
                                <select id="ddlCardType" class="form-control border-primary input-sm ddlCardreq" runat="server">
                                    <option value="0">-Select Card Type-</option>
                                </select>
                            </div>
                        </div>
                         <div class="col-md-4">
                            <label class="control-label">
                                Card No.  <span class="required">*</span>
                            </label>
                        </div>
                       <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm document ddlCardreq" placeholder="Card No." id="txtCardNo" runat="server" onkeypress='return isNumberKey(event)' maxlength="16"/>
                          
                                </div>
                        </div>
                         <div class="col-md-4">
                            <label class="control-label">
                                Expiry Date  <span class="required">*</span>
                            </label>
                        </div>
                       <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm ddlCardreq" runat="server" id="txtExpiryDate" placeholder="MM/YY" onkeyup="dtval(this,event)" onchange="dtexp(this)" MaxLength="5"/>
                            </div>
                        </div>
                         <div class="col-md-4">
                            <label class="control-label">
                                CVV  <%--<span class="required">*</span>--%>
                            </label>
                        </div>
                       <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm document" placeholder="CVV" id="txtCVV" runat="server" onkeypress='return isNumberKey(event)' maxlength="4" onblur="checkCVVLength()"/>
                            </div>
                        </div>
                         <div class="col-md-4">
                            <label class="control-label">
                                Zipcode  
                            </label>
                        </div>
                       <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm" placeholder="Zipcode" id="txtZipcode" runat="server" onkeypress='return isNumberKey(event)' maxlength="5"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">
                                Remarks
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <textarea class="form-control border-primary input-sm" id="txtCardRemarks"></textarea>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-danger pull-right  buttonAnimation round box-shadow-1  btn-sm" onclick="clearBorder()" data-dismiss="modal" style="margin: 0 5px 0 5px">Close</button>
                            <button type="button" class="btn btn-success pull-right  buttonAnimation round box-shadow-1  btn-sm" id="btnSaves1" onclick="SaveBankDetails1()" style="margin: 0 5px 0 5px">Save</button>
                            <button type="button" class="btn btn-warning pull-right  buttonAnimation round box-shadow-1  btn-sm" id="btnUpdates1" onclick="UpdateBankDetails1()" style="display: none; margin: 0 5px 0 5px">Update</button>
                         
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <%--<div id="CustomerDueAmount" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-xl">
           
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Payment Details<span id="StatusName"></span></h4>
                    <input type="hidden" id="PaymentAutoId" value="0" />
                </div>
                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col-md-3 col-sm-3">
                            <label class="control-label">
                                Payment ID :
                            </label>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm" disabled placeholder="Payment Id" id="PaymentId" />
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label class="control-label">
                                Payment Date :
                            </label>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <span class="la la-calendar-o"></span>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control border-primary input-sm date" disabled placeholder="Payment Id" id="PaymentDate" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-3 col-sm-3">
                            <label class="control-label">
                                Payment Method : <span class="required">*</span>
                            </label>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <div class="form-group">
                                <select class="form-control border-primary input-sm Update" id="PaymentMethod" runat="server" disabled>
                                    <option value="0">-Select Payment Mode-</option>
                                    <option value="1">Cash</option>
                                    <option value="2">Check</option>
                                    <option value="3">Money Order</option>
                                    <option value="4">Credit Card</option>
                                    <option value="5">Store Credit</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label class="control-label">
                                Received  Amount : <span class="required">*</span>
                            </label>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm Update" disabled runat="server" placeholder="Received Amount" onfocus="EditReceivedPay()"
                                    onkeydown="EditReceivedPay()" onkeyup="EditReceivedPay()" id="ReceivedAmount" style="text-align: right" />
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-3 col-sm-3">
                            <label class="control-label">
                                Reference No :
                            </label>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm" disabled placeholder="Reference No" id="ReferenceNo" />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">
                                Store Credit:
                            </label>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" id="txtCreditStore" class="form-control border-primary input-sm" readonly="true" value="0.00" style="text-align: right" />
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-3">
                            <label class="control-label">
                                Received Payment By  : <span class="required">*</span>
                            </label>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <select id="EmpName1" class="form-control border-primary input-sm Update" runat="server">
                                    <option value="0">Select Employee</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">
                                Processed By : 
                            </label>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm" disabled placeholder="Received  By " id="EmpName" />
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-3">
                            <label class="control-label">
                                Account Remark:
                            </label>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <textarea id="EmployeeRemarks" disabled class="form-control border-primary input-sm"></textarea>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label class="control-label">
                                Payment Remark :
                            </label>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <div class="form-group">
                                <textarea id="PaymentRemarks" disabled class="form-control border-primary input-sm"></textarea>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <label class="control-label">
                                Total Store Credit:
                            </label>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" id="txtStoreCreditTotal" class="form-control border-primary input-sm" readonly="true" value="0.00" style="text-align: right" />
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered" id="tblduePayment">
                                    <thead class="bg-blue white">
                                        <tr>
                                            <td class="SrNo text-center">SN</td>
                                            <td class="OrderType text-center">Order Type</td>
                                            <td class="OrderDate text-center">Order Date</td>
                                            <td class="OrderNo text-center">Order No</td>
                                            <td class="Amount" style="text-align: right">Paid Amount</td>
                                            <td class="OrderAmount" style="display: none; text-align: right">Order Amount</td>
                                            <td class="PaidAmount" style="text-align: right; display: none">Paid Amount</td>
                                            <td class="DueAmount" style="text-align: right; display: none">Due Amount</td>
                                            <td class="CheckBoc paynow text-center" style="display: none">Select Check Box</td>
                                            <td class="PayAmount paynow" style="display: none; text-align: right">Pay Amount</td>
                                            <td class="payDueAmount paynow" style="display: none; text-align: right">Due Balance</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="4">Total</td>
                                            <td id="Td1" style="text-align: right">0.00</td>
                                            <td id="Td2" style="display: none; text-align: right;">0.00</td>
                                            <td id="Td4" style="display: none; text-align: right;">0.00</td>
                                            <td id="Td5" style="display: none; text-align: right;">0.00</td>
                                            <td id="Td8" style="display: none; text-align: right;">0.00</td>
                                            <td id="Td6" style="display: none; text-align: right;">0.00</td>
                                            <td id="Td7" style="display: none; text-align: right;">0.00</td>

                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="form-group hideCurrency" style="display: none">
                        <div class="col-md-8">
                            <h4 class="card-title">Cash Details</h4>
                        </div>
                    </div>
                    <div class="row hideCurrency" style="display: none; margin: 0">
                        <div class="col-md-8">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered" id="tblPaymentHistoryCurrencyList">
                                    <thead class="bg-blue white">
                                        <tr>
                                            <td class="CurrencyName text-center">Bill</td>
                                            <td class="NoofRupee" style="width: 150px;">Total Count</td>
                                            <td class="Total" style="text-align: right; width: 150px">Total Amount</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="2" style="text-align: right">Total</td>
                                            <td id="PaymentHistpryTotalAmount" style="text-align: right">0.00</td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">
                                    <b style="white-space: nowrap">Short Amount :</b>
                                </label>
                                <input type="text" id="txtPaymentHistorySortAmount" onkeyup="PaymentHistorySortAmountCal()" readonly="true" onkeypress="return isNumberDecimalKey(event,this)" class="form-control border-primary input-sm" value="0.00" style="text-align: right" />
                            </div>
                            <div class="form-group">
                                <label class="control-label">
                                    <b style="white-space: nowrap">Total Amount :</b>
                                </label>
                                <input type="text" id="txtPaymentHistoryTotalCurrencyAmount" readonly="true" onkeypress="return isNumberDecimalKey(event,this)" class="form-control border-primary input-sm" value="0.00" style="text-align: right;font-size: 19px;font-weight: bold;" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row form-group" style="margin: 0">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-danger buttonAnimation round box-shadow-1  btn-sm pull-right" style="margin: 0 5px 0 5px" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm pull-right" style="margin: 0 5px 0 5px" id="Print" onclick="printPaymentSlip()">Print</button>
                            <button type="button" class="btn btn-success buttonAnimation round box-shadow-1  btn-sm pull-right" style="margin: 0 5px 0 5px" id="editPayment" onclick="Paymentedit()">Edit</button>
                            <button type="button" class="btn btn-primary buttonAnimation round box-shadow-1  btn-sm pull-right" style="margin: 0 5px 0 5px; display: none" id="UpdatePayment" onclick="PaymentUpdate()">Update</button>
                            <button type="button" class="btn btn-warning buttonAnimation round box-shadow-1  btn-sm pull-right" id="btnCancelpayment" onclick="CancelPayment()" style="display: none">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="PopCustomerDocuments" class="modal fade" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
           
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="card-title">Upload Document</h4>
                    <input type="hidden" id="hdnuploadAutoId" />
                </div>
                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label class="control-label">
                                Document Name : <span class="required">*</span>
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm document" placeholder="Document Name" id="txtDocumentName" runat="server" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">
                                Document : <span class="required">*</span>
                            </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="file" class="form-control border-primary input-sm document" style="height: 4rem" id="uploadedFile" runat="server" />
                                Allowed file type : <span class="required">gif,png,jpg,jpeg,doc</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-success pull-right  buttonAnimation round box-shadow-1  btn-sm" id="Button2" onclick="DocumentSave()" style="margin: 0 5px 0 5px">Save</button>
                            <button type="button" class="btn btn-warning pull-right  buttonAnimation round box-shadow-1  btn-sm" id="Button3" onclick="UpdateDocument()" style="display: none; margin: 0 5px 0 5px">Update</button>
                            <button type="button" class="btn btn-danger pull-right  buttonAnimation round box-shadow-1  btn-sm" onclick="clearBorder()" data-dismiss="modal" style="margin: 0 5px 0 5px">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="modelClearance" class="modal fade" role="dialog">
        <div class="modal-dialog">
           
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Check Details</h4>
                </div>
                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col-sm-4 col-md-4">
                            <label>Check No <span class="required">*</span></label>
                        </div>
                        <div class="col-sm-6 col-md-8">
                            <input type="text" class="form-control border-primary input-sm SaveCheck" id="txtCheckNo" runat="server" />
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-4 col-md-4">
                            <label>Check Date <span class="required">*</span></label>
                        </div>
                        <div class="col-sm-6 col-md-8">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <span class="la la-calendar-o"></span>
                                    </span>
                                </div>
                                <input type="text" class="form-control border-primary input-sm date SaveCheck" id="txtCheckDate" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-4 col-md-4">
                            <label>Remark</label>
                        </div>
                        <div class="col-sm-6 col-md-8">
                            <textarea class="form-control border-primary input-sm" id="txtRemarks"></textarea>
                        </div>
                    </div>
                </div>
                <div class="model-footer">
                    <div class="row form-group">
                        <div class="col-sm-12 col-md-12">
                           
                            <button type="button" class="btn btn-success buttonAnimation round box-shadow-1 pull-right  btn-sm" style="margin: 0 5px" onclick="SaveCheckDetails()">Save</button>
                             <button type="button" class="btn btn-danger buttonAnimation round box-shadow-1 pull-right  btn-sm" onclick="clearBordertwo()" style="margin: 0 5px" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="modalPopDuePayment" class="modal fade" role="dialog">
        <div class="modal-dialog">
           
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" style="color:red"> Cancel Check Details</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <label>Check No</label>
                        </div>
                        <div class="col-sm-6 form-group">
                            <input type="text" class="form-control border-primary input-sm" id="txtChequeNo" readonly />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <label>Check Date</label>
                        </div>
                        <div class="col-sm-6 form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <span class="la la-calendar-o"></span>
                                    </span>
                                </div>
                                <input type="text" class="form-control border-primary input-sm date" id="txtChequeDate" readonly />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <label>Remark <span class="required">*</span></label>
                        </div>
                        <div class="col-sm-6 form-group">
                            <textarea class="form-control border-primary input-sm CheckSave" id="txtPopRemarks" runat="server"></textarea>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-6">
                            <label>Bank Fee Charge for Customer <span class="required">*</span></label>
                        </div>
                        <div class="col-sm-6">
                             <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                       $
                                    </span>
                                </div>
                            <input type="text" class="form-control border-primary input-sm CheckSave text-right" id="txtBankFeeCharge" maxlength="10" runat="server" onkeypress="return isNumberDecimalKey(event,this)" />

                        </div> </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-12 form-group">
                            <button type="button" class="btn btn-warning buttonAnimation pull-right round box-shadow-1  btn-sm" style="margin: 0 5px" onclick="CheckCancelDetails()">Cancel Payment</button>
                            <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1  btn-sm" onclick="clearBorder3()" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="PopPaymentDetails" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
           
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Payment Details</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-3">
                                <label class="control-label">
                                    Payment ID :
                                </label>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input type="text" class="form-control border-primary input-sm" disabled placeholder="Payment Id" id="PPaymentId" />
                                </div>
                            </div>

                            <div class="col-md-3">
                                <label class="control-label">
                                    Payment Date :
                                </label>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <span class="la la-calendar-o"></span>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control border-primary date input-sm" disabled placeholder="Payment Id" id="PPaymentDate" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <div class="col-md-3">
                                <label class="control-label">
                                    <span style="white-space: nowrap">Payment Method : </span>
                                </label>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select class="form-control border-primary input-sm" id="PPaymentMethod" runat="server" disabled>
                                        <option value="0">-Select Payment Mode-</option>
                                        <option value="1">Cash</option>
                                        <option value="2">Check</option>
                                        <option value="3">Money Order</option>
                                        <option value="4">Credit Card</option>
                                        <option value="5">Store Credit</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label class="control-label">
                                    <span style="white-space: nowrap">Received Payment Amount : </span>
                                </label>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input type="text" class="form-control border-primary input-sm" disabled runat="server" placeholder="Received Amount"
                                        onfocus="EditReceivedPay()" onkeydown="EditReceivedPay()" onkeyup="EditReceivedPay()" id="PReceivedAmount" style="text-align: right" />
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <div class="col-md-3">
                                <label class="control-label">
                                    <sapn style="white-space: nowrap">Reference No :</sapn>
                                </label>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input type="text" class="form-control border-primary input-sm" disabled placeholder="Reference No" id="PReferenceNo" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label class="control-label">
                                    <span style="white-space: nowrap">Payment Remark :</span>
                                </label>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <textarea id="PPaymentRemarks" disabled class="form-control border-primary input-sm"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <div class="col-md-3">
                                <label class="control-label">
                                    <span style="white-space: nowrap">Account Remark:</span>
                                </label>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <textarea id="PEmployeeRemarks" disabled class="form-control border-primary input-sm"></textarea>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="tblPduePayment">
                            <thead class="bg-blue white">
                                <tr>
                                    <td class="OrderType">Order Type</td>
                                    <td class="OrderDate">Order Date</td>
                                    <td class="OrderNo">Order No</td>
                                    <td class="OrderAmount" style="text-align: right;">Order Amount</td>
                                    <td class="PaidAmount" style="text-align: right">Paid Amount</td>
                                    <td class="DueAmount" style="text-align: right;">Due Amount</td>
                                    <td class="CheckBoc">Select Check Box</td>
                                    <td class="PayAmount paynow" style="text-align: right">Pay Amount</td>
                                    <td class="payDueAmount paynow" style="text-align: right">Due Balance</td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="3">Total</td>
                                    <td id="PT1">0.00</td>
                                    <td id="PT2">0.00</td>
                                    <td id="PT3">0.00</td>
                                    <td id="PT5"></td>
                                    <td id="PT4">0.00</td>
                                    <td id="PT6">0.00</td>

                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-5">
                            <button type="button" class="btn btn-success buttonAnimation round box-shadow-1  btn-sm" id="PeditPayment" onclick="Paymentedit()">Edit</button>
                            <button type="button" class="btn btn-success buttonAnimation round box-shadow-1  btn-sm" id="PUpdatePayment" onclick="PaymentUpdate()" style="display: none; margin: 0 5px">Update</button>
                            <button type="button" class="btn btn-danger buttonAnimation round box-shadow-1  btn-sm" id="PbtnCancelpayment" onclick="CancelPayment()" style="display: none; margin: 0 5px">Cancel</button>
                            <button type="button" class="btn btn-default buttonAnimation round box-shadow-1  btn-sm" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="CreditOrderPop" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
           
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Settle Credit Memo</h4>
                </div>
                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col-md-3">
                            Order No<span class="required"> *</span>
                        </div>
                        <div class="col-md-4">
                            <select id="ddlOrder" class="form-control border-primary input-sm" style="width: 100% !important" onchange="getamount()">
                                <option value="0">-Select Order-</option>
                            </select>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <label class="control-label">
                                <span style="white-space: nowrap">Total Order Amount : </span>
                            </label>
                        </div>
                        <div class="col-md-4 col-sm-3">
                            <div class="form-group">
                                <input type="text" class="form-control border-primary input-sm" disabled placeholder="0.00"
                                    id="txtCreditMemoAmount" style="text-align: right" />
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered" id="tblMemoOrderList">
                                    <thead class="bg-blue white">
                                        <tr>
                                            <td class="SrNo">SN</td>
                                            <td class="action"></td>
                                            <td class="CreditDate">Credit Date</td>
                                            <td class="CreditNo">Credit No</td>
                                            <td class="TotalAmount" style="text-align: right">Total Amount</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="4">Total</td>
                                            <td id="PTd1" style="text-align: right">0.00</td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-2">
                            <label class="control-label">
                                <b style="white-space: nowrap">Remark : </b>
                            </label>
                        </div>
                        <div class="col-md-8 col-sm-8">
                            <div class="form-group">
                                <textarea class="form-control border-primary input-sm" placeholder="Enter Remark"
                                    id="txtCreditRemarks"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation round box-shadow-1  btn-sm" style="margin: 0 5px" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-success buttonAnimation round box-shadow-1  btn-sm" style="margin: 0 5px" onclick="SettlledCredit()">Submit</button>
                </div>
            </div>
        </div>
    </div>--%>

     <script type="text/javascript">
         document.write('<scr' + 'ipt type="text/javascript" src="/Admin/JS/ViewVendorDetails.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
</asp:Content>

