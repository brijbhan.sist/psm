﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Map.aspx.cs" Inherits="Admin_Map" %>

<!DOCTYPE html>
<html>
<head>
    <title>Directions Service (Complex)</title>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <script type="text/javascript" src="/js/jquery.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8Y7FRVyPLlIEvF11qdFiD-ZWsf5OVIjs&callback=initMap&libraries=&v=weekly"
        defer></script>
    <style type="text/css">
        #right-panel {
            font-family: "Roboto", "sans-serif";
            line-height: 30px;
            padding-left: 10px;
        }

            #right-panel select,
            #right-panel input {
                font-size: 15px;
            }

            #right-panel select {
                width: 100%;
            }

            #right-panel i {
                font-size: 12px;
            }

        html,
        body {
            height: 100%;
            margin: 0;
            padding: 0;
        }

        #map {
            height: 100%;
            float: left;
            width: 70%;
            height: 100%;
        }

        #right-panel {
            margin: 20px;
            border-width: 2px;
            width: 20%;
            float: left;
            text-align: left;
            padding-top: 0;
        }

        #directions-panel {
            margin-top: 10px;
            background-color: #ffee77;
            padding: 10px;
            overflow: scroll;
            height: 174px;
        }
    </style>
    <script>
        const setData = [];
        var waypts = [];
        $(document).ready(function () {
            wayPoint();
        });

        function wayPoint() {
            $.ajax({
                type: "POST",
                url: "/Admin/Map.aspx/SelectwayPoint",
                data: "{}",
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                asyn: false,
                casshe: false,
                beforeSend: function () {

                },
                complete: function () {

                },
                success: function (response) {
                    getData = $.parseJSON(response.d);
                    setData.push(getData);
                    for (let i = 0; i < getData.length; i++) {
                        waypts.push({
                            location: new google.maps.LatLng(Number(getData[i].Lat), Number(getData[i].Long)),
                            stopover: true
                        })
                    }
                },
                error: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                },
                failure: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                }
            });

        }
       
        function initMap() {
            const directionsService = new google.maps.DirectionsService();
            const directionsRenderer = new google.maps.DirectionsRenderer({ suppressMarkers: false });
            const map = new google.maps.Map(document.getElementById("map"), {
                zoom: 6,
                center: { lat: 41.85, lng: -87.65 },
            });
            directionsRenderer.setMap(map);
            calculateAndDisplayRoute(directionsService, directionsRenderer);
            for (let i = 0; i < waypts.length; i++) {
                const marker = new google.maps.Marker({
                    position: new google.maps.LatLng(waypts[i].Lat, waypts[i].Long), 
                    map: map,
                });
            }
        }

        function calculateAndDisplayRoute(directionsService, directionsRenderer) {

            directionsService.route(
                {
                    origin: "40.560770,-74.420490",
                    destination: "40.5757563,-74.3548452",
                    waypoints: waypts,
                    optimizeWaypoints: true,
                    travelMode: google.maps.TravelMode.DRIVING,
                },
                (response, status) => {
                    if (status === "OK") {
                        directionsRenderer.setDirections(response);
                        console.log(response)
                        var leg = response.routes[0].legs[0];
                        //makeMarker(leg.start_location, "title");
                    } else {
                        window.alert("Directions request failed due to " + status);
                    }
                }
            );
        }

         




    </script>

</head>
<body>
    <div id="map" style="width: 100%"></div>
     
    <div id="directions-panel"></div>
</body>
</html>
