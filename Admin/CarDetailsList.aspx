﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="CarDetailsList.aspx.cs" Inherits="Admin_CarDetailsList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .table th, .table td {
        padding:0.75rem !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content-header row">
         <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title"> Car List</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Manage Application</a>
                        </li>
                         <li class="breadcrumb-item">Manage Car</li>
                    </ol>
                </div>
            </div>
        </div>
         <div class="content-header-right col-md-6 col-12">
          <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
            <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
            id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false"> Action</button>
              <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                  <button type="button" class="dropdown-item" onclick=" location.href='/Admin/CarDetails.aspx'"id="btnAdd">Add New Car</button>
              </div>
          </div>
        </div>
       
    </div>

     <div class="content-body">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                 <div class="row form-group">
                                    <div class="col-md-3 form-group">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Car Nick Name" id="txtScarName" />
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" id="btnSearch" data-animation="pulse" onclick="getCarList(1);">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card"> 
                        <div class="card-content collapse show">
                            <div class="card-body">                               
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered" id="tblCarList">
                                        <thead class="bg-blue white">
                                            <tr>
                                                <td class="Action text-center width3per">Action</td>
                                                <td class="CarId text-center width5per">Car ID</td>
                                                <td class="CarName">Car Nick Name</td>
                                                <td class="YearName text-center width3per">Year</td>
                                                <td class="Make">Make</td>
                                                <td class="Model">Model</td>
                                                <td class="VINNo ">VIN No</td>
                                                <td class="TagNo ">Licence Plate</td>
                                                <td class="InspectionExpDate text-center" style="width:130px;">Inspection Exp. Date</td>
                                                <td class="StartMilage text-center">Start Milage</td>

                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                    <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                </div>
                                <div class="Pager"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</asp:Content>

