﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="ApiLogReport.aspx.cs" Inherits="Admin_ApiLogReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .tblwth{
            width:11%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Api Request Logs Report</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Support</a></li>
                        <li class="breadcrumb-item"><a href="#">Api Request Logs</a></li>
                        <li class="breadcrumb-item active" title="Help"><a href="#" onclick="GetPageInformation(10052)"><i class="la la-question-circle"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem()">Print</button>
                    <button type="button" class="dropdown-item" id="btnExport" onclick="Export();">Export</button>
                </div>
            </div>
        </div>
    </div>

    <div>
        <div class="alert alert-success alert-dismissable fade in" id="alertSuccessDelete" style="display: none;">
            <a aria-label="close" id="successDeleteClose" class="close" style="cursor: pointer;">&times;</a>
            <span></span>
        </div>
        <div>
            <input type="hidden" id="hiddenPackerAutoId" runat="server" />
            <input type="hidden" id="hiddenEmpType" runat="server" />
            <div class="content-body">
                <section id="drag-area">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <div class="row form-group">
                                            <div class="col-md-3 form-group">
                                                <select class="form-control input-sm border-primary" id="ddlUser">
                                                    <option value="0">All Users</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3 form-group">
                                                <select class="form-control input-sm border-primary" id="ddlMethods">
                                                    <option value="0">All Methods</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3 form-group">
                                                <select class="form-control input-sm border-primary" id="ddlVersion">
                                                    <option value="0">All Version</option>
                                                </select>
                                            </div>
                                              <div class="col-md-3  form-group">
                                                <input type="text" class="form-control input-sm border-primary req" id="txtDeviceId" runat="server" placeholder="Device Name" />
                                            </div>
                                          
                                        </div>
                                        <div class="row form-group">
                                              <div class="col-md-3 form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                                            From Date <span class="la la-calendar-o"></span>
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control input-sm date border-primary" placeholder="From Date" id="txtSFromDate" onfocus="this.select()" />
                                                </div>
                                            </div>
                                            <div class="col-md-3 form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                                            To Date <span class="la la-calendar-o"></span>
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control input-sm date border-primary" placeholder="To Date" id="txtSToDate" onfocus="this.select()" />
                                                </div>
                                            </div>
                                          
                                             <div class="col-md-3">
                                                <button type="button" class="btn btn-info buttonAnimation round box-shadow-1 btn-sm" id="btnSearch" onclick=" getReport(1);">Search</button>
                                            </div>
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <div class="row form-group">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="MyTableHeader" id="TableApiRequest">
                                                        <thead class="bg-blue white">
                                                            <tr>
                                                                <td class="sn text-center" style="width:4%;">SN</td>
                                                                <td class="MethodName left wth9">Method Name</td>
                                                                <td class="UTCTimeStamp left wth9">UTC Time Stamp</td>
                                                                <td class="CreatedOn left wth9">Created On</td>
                                                                <td class="AppVersion text-center wth5">App Version</td>
                                                                <td class="UserName left wth9">User Name</td>
                                 <td class="DeviceName left text-center wth9">Device Name</td>
                                                                <td class="DeviceId left wth9">Device ID</td>
                                                                <td class="LatLong text-center wth6">Lat-Long</td>
                                                                <td class="RouteName text-center wth5">Route Name</td>
                                                                <td class="AccessToken wth10" >Access Token</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>

                                                    </table>
                                                    <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row container-fluid">

                                            <div>
                                                <select class="form-control input-sm border-primary" id="ddlPageSize" onchange=" getReport(1);">
                                                    <option selected="selected" value="10">10</option>
                                                    <option value="50">50</option>
                                                    <option value="100">100</option>
                                                    <option value="500">500</option>
                                                    <option value="1000">1000</option>
                                                    <option value="0">All</option>
                                                </select>
                                            </div>
                                            <div class="col-md-10">
                                                <div class="Pager"></div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <div class="table-responsive" style="display: none;"  id="PrintTable1">
                   <div class="row" style="margin-bottom: 5px;">
                <div class="col-md-4 col-lg-4 col-sm-4 text-left">
                    <img src="" id="PrintLogo" height="40" />
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-center" style="margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;">
             Api Request Logs Report
                    <br />
                    <span class="text-center DateRangeCSS" style="font-size:9px; color: black;" id="DateRange"></span>

                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-right" id="PrintDate" style="line-height: 3; font-size: 12px; color: black; font-weight: bold;">Print Date</div>
            </div>
                    <table class="MyTableHeader PrintMyTableHeader" id="PrintTable">
                        <thead class="bg-blue white">
                           <%-- <tr style="background: #fff; font-weight: bold; border: none;">
                                <td colspan="5" id="PrintDate" style="background: #fff; font-weight: bold; border: none;"></td>
                                <td colspan="5" id="Date" style="background: #fff; font-weight: bold; text-align: right; border: none;"></td>
                            </tr>--%>
                            <tr>
                                <td class="sn text-center wth4">SN</td>
                                <td class="MethodName left text-center wth9">Method Name</td>
                                <td class="UTCTimeStamp text-center wth9">UTC Time Stamp</td>
                                <td class="CreatedOn text-center wth9">Created On</td>
                                <td class="AppVersion text-center wth5">App Version</td>
                                <td class="UserName left text-center wth9">User Name</td>
                                <td class="RouteName  text-center wth9">Route Name</td>
                                 <td class="DeviceName left text-center wth9">Device Name</td>
                                <td class="DeviceId left text-center wth6">Device ID</td>
                                <td class="LatLong text-center wth5">Lat-Long</td>
                                <td class="AccessToken left text-center wth10">Access Token</td>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');       
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>


</asp:Content>

