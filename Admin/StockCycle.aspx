﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="StockCycle.aspx.cs" Inherits="Admin_StockCycle" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Stock Cycle</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Inventory</a>
                        </li>
                        <li class="breadcrumb-item">Stock Cycle
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body" style="min-height: 400px">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th></th>
                                                        <th>User Type</th>
                                                        <th>Action Type</th>
                                                        <th>Action Process</th>
                                                        <th>Status</th>
                                                        <th>Stock Action</th>
                                                        <th>Stock Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td rowspan="11">Order Cycle
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>CASE 1</td>
                                                        <td>Packer</td>
                                                        <td>Process - > Packed Add on - > Packed
                                                        </td>
                                                        <td>Packed Item</td>
                                                        <td>Sold Stock</td>
                                                        <td>Deduct</td>
                                                        <td>Deduct from Stock [Only packed Items and Qty]</td>
                                                    </tr>
                                                    
                                                    <tr>
                                                        <td rowspan="5">CASE 2</td>
                                                        <td rowspan="5">Sales Manager</td>
                                                        <td  rowspan="5">Remove Product/Add Products
                                                        </td>
                                                        <td>Remove Product</td>
                                                        <td>Revert Sold Stock</td>
                                                        <td>Add</td>
                                                        <td>Add To Stock for Remove Items</td>
                                                    </tr>
                                                   
                                                    <tr>                                                        
                                                        <td>Packed-Set As Process</td>
                                                        <td>Revert Sold Stock</td>
                                                        <td>Add</td>
                                                        <td>Add To Stock for All Item and Qty</td>
                                                    </tr>
                                                     <tr>                                                        
                                                        <td>Add-on-Set As Process</td>
                                                        <td>Revert Sold Stock Packed Item</td>
                                                        <td>Add</td>
                                                        <td>Add To Stock for All Item and Qty</td>
                                                    </tr>
                                                    <tr>                                                        
                                                        <td>Add-On-Packed-Set As Process</td>
                                                        <td>Revert Sold Stock Packed Item</td>
                                                        <td>Add</td>
                                                        <td>Add To Stock for All Item and Qty</td>
                                                    </tr>
                                                    <tr>                                                       
                                                        <td>Cancel Order</td>
                                                        <td>Revert Sold Stock</td>
                                                        <td>Add</td>
                                                        <td>Add To Stock for All Item and Qty</td>
                                                    </tr>
                                                  
                                                    <tr>
                                                        <td>CASE 3</td>
                                                        <td>POS</td>
                                                        <td>New Order</td>
                                                        <td>New Order</td>
                                                        <td>Sold Stock</td>
                                                        <td>Deduct</td>
                                                        <td></td>
                                                    </tr>
                                                   
                                                    <tr>
                                                        <td rowspan="3">CASE 4
                                                        </td>
                                                        <td rowspan="3">Account</td>
                                                        <td>Closing Order Void Orders</td>
                                                        <td>Fresh Return</td>
                                                        <td>Revert Sold Stock</td>
                                                        <td>Add</td>
                                                        <td>Add To Stock for Fresh Return Item and Qty</td>
                                                    </tr>
                                                   
                                                    <tr>
                                                        <td>Closing Order Void Orders
                                                        </td>
                                                        <td>Void Order</td>
                                                        <td>Revert Sold Stock</td>
                                                        <td>Add</td>
                                                        <td>Add To Stock for All Item and Qty</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Internal Order Status : Delivered</td>
                                                        <td>Add New Item</td>
                                                        <td>Sold Stock</td>
                                                        <td>Deduct</td>
                                                        <td>Deduct from Stock</td>
                                                    </tr>
                                                    
                                                    <tr>
                                                        <td rowspan="2">Credit Memo</td>
                                                        <td rowspan="2">CASE 5</td>
                                                        <td>Credit Memo</td>
                                                        <td>Credit Memo New To Approve
                                                        </td>
                                                        <td>Fresh Return</td>
                                                        <td>Credit Memo Purchase Stock</td>
                                                        <td>Add</td>
                                                        <td></td>
                                                    </tr>
                                                   
                                                    <tr>
                                                        <td>Credit Memo</td>
                                                        <td>Credit Memo  Approve To Cancel
                                                        </td>
                                                        <td>Fresh Return</td>
                                                        <td>Credit Memo Revert Purchase Stock</td>
                                                        <td>Deduct</td>
                                                        <td></td>
                                                    </tr>
                                                    
                                                    <tr>
                                                        <td rowspan="2">Purchase Order</td>
                                                        <td rowspan="2">CASE 6</td>
                                                        <td>Purchase Order</td>
                                                        <td>Internal PO Pending-> Close</td>
                                                        <td>Received PO</td>
                                                        <td>Internal PO Purchase Stock</td>
                                                        <td>Add</td>
                                                        <td></td>
                                                    </tr>
                                                   
                                                    <tr>
                                                        <td>Purchase Order</td>
                                                        <td>Vendor PO Pending-> Close</td>
                                                        <td>Received PO</td>
                                                        <td>Internal PO Purchase Stock</td>
                                                        <td>Add</td>
                                                        <td></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</asp:Content>

