﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" ClientIDMode="Static" CodeFile="stockEntryList.aspx.cs" Inherits="admin_stockEntryList" %>

<script runat="server">

</script>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Received Stock List</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Inventory Management</a></li>
                        <li class="breadcrumb-item active">Received Stock List</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" onclick=" location.href='/Admin/stockEntry.aspx'" class="dropdown-item" animation="pulse" id="Button1" runat="server">
                        Add Receive Stock</button>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body" style="min-height: 400px">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 form-group">
                                        <input type="text" class="form-control input-sm border-primary" placeholder="Bill No" id="txtSBillNo" onfocus="this.select()" />
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem .5rem;">From Date <span class="la la-calendar-o pl-0"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control input-sm border-primary" onchange="setdatevalidation(1)" placeholder="From Date" id="txtSFromBillDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem .5rem;">To Date <span class="la la-calendar-o pl-0"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control input-sm border-primary" onchange="setdatevalidation(2)" placeholder="To Date" id="txtSToBillDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <select class="form-control input-sm border-primary" id="ddlSVendor" runat="server" style="width: 100%">
                                            <option value="0">All Vendors</option>
                                        </select>
                                    </div>


                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1  btn-sm" id="btnSearch">Search</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <style>
                                    tbody .total {
                                        text-align: right;
                                    }
                                </style>
                                <div class="row form-group">
                                    <div class="col-md-12">


                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblStockEntryList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="Action text-center">Action</td>
                                                        <td class="BillNo text-center">Bill No</td>
                                                        <td class="BillDate">Bill Date</td>
                                                        <td class="VendorName">Vendor</td>
                                                        <td class="NoOfItems text-center">No. of Items</td>
                                                        <td class="TotalAmount total">Total Amount</td>
                                                        <td class="Remarks">Remark</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td colspan="5" style="text-align: right; font-weight: 600;">Total</td>
                                                        <td id="gTotal" style="text-align: right; font-weight: 600;"></td>
                                                        <td></td>

                                                    </tr>
                                                </tfoot>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex">
                                    <div class="form-group">
                                        <select class="form-control input-sm border-primary" id="ddlPaging" runat="server" style="width: 100%" onchange="pagingSize();">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="ml-auto">
                                        <div class="Pager"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="/Admin/JS/stockEntryList.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
</asp:Content>

