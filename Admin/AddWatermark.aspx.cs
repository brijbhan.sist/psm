﻿using DllProductImageList;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AddWatermark : System.Web.UI.Page

{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lblMsg.Text = getProductList();
        }
    }
    public string getProductList()
    {
        DataSet ds = new DataSet();
        PL_ProductImgList pobj = new PL_ProductImgList();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                int i = 0, j = 0, k = 0;
                BL_ProductImgList.AddWatermark(pobj);
                if (!pobj.isException)
                {
                    System.Drawing.Image logoImage = System.Drawing.Image.FromFile(Server.MapPath("/img/logo/watermark.png"));
                    foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                    {
                        string dirFullPath = "", thumb1 = "", thumb2 = "";
                        dirFullPath = Server.MapPath("~" + dr["ImageUrl"].ToString());
                        thumb1 = Server.MapPath("~" + dr["ThumbnailImageUrl"].ToString());
                        thumb2 = Server.MapPath("~" + dr["ThirdImage"].ToString());
                        if (File.Exists(dirFullPath))
                        {
                            System.Drawing.Image imgUrl = System.Drawing.Image.FromFile(Server.MapPath(dr["ImageUrl"].ToString()));
                            var width = (imgUrl.Width - logoImage.Width) / 2;
                            var height = (imgUrl.Height - logoImage.Height) / 2;
                            using (Graphics g = Graphics.FromImage(imgUrl))
                            {
                                g.DrawImage(logoImage, new Point(width, height));
                                File.Delete(dirFullPath);
                                imgUrl.Save(dirFullPath);
                            }
                            i = i + 1;
                        }
                        if (File.Exists(thumb1))
                        {
                            System.Drawing.Image imgUrl = System.Drawing.Image.FromFile(Server.MapPath(dr["ThumbnailImageUrl"].ToString()));
                            var width = (imgUrl.Width - logoImage.Width) / 2;
                            var height = (imgUrl.Height - logoImage.Height) / 2;
                            using (Graphics g = Graphics.FromImage(imgUrl))
                            {
                                g.DrawImage(logoImage, new Point(width, height));
                                imgUrl.Save(Path.Combine(Server.MapPath("~/productThumbnailImage"), dr["thumb1"].ToString()));
                            }
                            j = j + 1;
                        }
                        if (File.Exists(thumb2))
                        {
                            System.Drawing.Image imgUrl = System.Drawing.Image.FromFile(Server.MapPath(dr["ThirdImage"].ToString()));
                            var width = (imgUrl.Width - logoImage.Width) / 2;
                            var height = (imgUrl.Height - logoImage.Height) / 2;
                            using (Graphics g = Graphics.FromImage(imgUrl))
                            {
                                g.DrawImage(logoImage, new Point(width, height));
                                imgUrl.Save(Path.Combine(Server.MapPath("~/productThumbnailImage"), dr["thumb2"].ToString()));
                            }
                            k = k + 1;
                        }
                    }
                }
                lblMsg.Text = i.ToString() + " " + j.ToString() + " " + k.ToString();
                return "true";
            }
            else
            {
                return "false";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;// "Oops! Something went wrong.Please try later.";
        }

    }
    //[WebMethod(EnableSession = true)]
    //protected void btnSave_Click(object sender, EventArgs e)
    //{
    //    string fileName = Guid.NewGuid() + Path.GetExtension(FU1.FileName);
    //    System.Drawing.Image upImage = System.Drawing.Image.FromStream(FU1.PostedFile.InputStream);
    //    System.Drawing.Image logoImage = System.Drawing.Image.FromFile(Server.MapPath("img/logo/watermark.png"));
    //    //System.Drawing.Image logoImage = System.Drawing.Image.FromStream(FULogo.PostedFile.InputStream);
    //    var width = (upImage.Width - logoImage.Width) / 2;
    //    var height = (upImage.Height - logoImage.Height) / 2;

    //    using (Graphics g = Graphics.FromImage(upImage))
    //    {
    //        g.DrawImage(logoImage, new Point(width, height));
    //        upImage.Save(Path.Combine(Server.MapPath("~/Attachments"), fileName));
    //        Image1.ImageUrl = "~/Attachments" + "//" + fileName;
    //    }
    //}
}