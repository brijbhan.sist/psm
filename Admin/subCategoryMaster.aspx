﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" ClientIDMode="Static" CodeFile="subCategoryMaster.aspx.cs" Inherits="Admin_subCategoryMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        #tblSubcategory_wrapper {
            padding: 0
        }
          input.form-control.input-sm {
            padding: 5px !important;
        }
        select.form-control:not([size]):not([multiple]).input-sm {
            padding: 2px !important;
        }
        textarea {
            padding: 5px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Manage Subcategory</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Product</a></li>
                        <li class="breadcrumb-item">Manage Subcategory</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Subcategory Details</h4>

                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">
                                                    Category Name<span class="required">&nbsp;*</span>

                                                </label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                                                <select class="form-control border-primary input-sm ddlreq" id="ddlCategoryName" runat="server"></select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Subcategory ID</label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                                                <input type="text" class="form-control border-primary input-sm" id="txtSubcategoryId" readonly="readonly" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">
                                                    Subcategory<span class="required">&nbsp;*</span>

                                                </label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                                                <input type="text" class="form-control border-primary input-sm req" placeholder="Max 45 Characters." maxlength="45" id="txtSubcategoryName" runat="server" />

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Show On Website</label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                                                <input type="checkbox" id="cbShowOnWebsite" runat="server" />
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">
                                                    Status
                                                </label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                                                <select name="Status" class="form-control border-primary input-sm" id="ddlStatus">
                                                    <option value="1">Active</option>
                                                    <option value="0">Inactive</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Sequence No</label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                                                <input type="text" class="form-control border-primary input-sm" onkeypress="return isNumberKey(event)" placeholder="Sequence No" id="txtSequenceNo" maxlength="3" runat="server" />
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Description</label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                                                <textarea id="txtDescription" class="form-control border-primary input-sm" placeholder="Max 200 Characters." maxlength="200"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <%{
                                            if (Session["EmpTypeNo"].ToString() == "1")
                                            { %>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">Subcategory Tax</label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                                                <select class="form-control border-primary input-sm" id="dllSubcategoryTax">
                                                    <option value="0">Select Tax</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <% }
                                        }%>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnSave">Save</button>
                                    </div>
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" class="btn btn-secondary  buttonAnimation pull-right round box-shadow-1  btn-sm" onclick="resetSubcategory()" data-animation="pulse" id="btnReset">Reset</button>
                                    </div>
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnUpdate" style="display: none">Update</button>
                                    </div>
                                    <div class="btn-group mr-1 pull-right">
                                        <button type="button" class="btn btn-warning buttonAnimation pull-right round box-shadow-1 btn-sm" id="btnCancel" data-animation="pulse" style="display: none">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Subcategory List</h4>
                        </div>
                        <div class="card-body">
                            <div class="card-content collapse show">
                                <div class="row">
                                    <div class="col-md-3 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlSCategory">
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Subcategory ID" id="txtSSubcategoryId" />
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Subcategory Name" id="txtSSubcategoryName" />
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <select class="form-control border-primary input-sm" id="dllSSubcategoryTax">
                                            <option value="0">Select Tax</option>
                                        </select>
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <select id="ddlSStatus" runat="server" class="form-control border-primary input-sm">
                                            <option value="2">All Status</option>
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                    </div>
                                    <div class="col-md-9">
                                        <button type="button" class="btn btn-info buttonAnimation pull-left round box-shadow-1 btn-sm" id="btnSearch">Search</button>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblSubcategory">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="Action text-center">Action</td>
                                                        <td class="Category">Category Name</td>
                                                        <td class="SubCatId text-center">Subcategory ID</td>
                                                        <td class="SubcategoryName">Subcategory Name</td>
                                                        <td class="TaxName">Subcategory Tax</td>
                                                        <td class="SeqNo text-center">Sequence No</td>
                                                        <td class="IsShow text-center">Show On Website</td>
                                                        <td class="Status text-center">Status</td>
                                                        <td class="Description">Description</td>
                                                    </tr>
                                                </thead>
                                                <tbody></tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <select class="form-control border-primary input-sm" id="ddlPageSize" onchange="getSubcategory(1)">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager float-right" id="ProductPager"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="/Admin/JS/subcategoryMaster.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>

</asp:Content>

