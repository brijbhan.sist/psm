﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="map2.aspx.cs" Inherits="Admin_map2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Google Map</title>


</head>
<body>
    <form id="form1" runat="server">
        <div id="map" style="width: 900px; height: 600px;"></div>
        <script type="text/javascript">
            function initMap() {
                var service = new google.maps.DirectionsService;
                var map = new google.maps.Map(document.getElementById('map'));
                window.gMap = map;

                // list of points
                var stations = [
                    { lat: 48.9812840, lng: 21.2171920, name: '1' },
                    { lat: 48.9832841, lng: 21.2176398, name: '2' },
                    { lat: 48.9856443, lng: 21.2209088, name: '3' },
                    { lat: 48.9861461, lng: 21.2261563, name: '4' },
                    { lat: 48.9874682, lng: 21.2294855, name: '5' },
                    { lat: 48.9909244, lng: 21.2295512, name: '6' },
                    { lat: 48.9928871, lng: 21.2292352, name: '7' },
                    { lat: 48.9921334, lng: 21.2246742, name: '8' },
                    { lat: 48.9943196, lng: 21.2234792, name: '9' },
                    { lat: 48.9966345, lng: 21.2221262, name: '10' },
                    { lat: 48.9981191, lng: 21.2271386, name: '11' },
                    { lat: 49.0009168, lng: 21.2359527, name: '12' },
                    { lat: 49.0017950, lng: 21.2392890, name: '13' },
                    { lat: 48.9991912, lng: 21.2398272, name: '14' },
                    { lat: 48.9959850, lng: 21.2418410, name: '15' },
                    { lat: 48.9931772, lng: 21.2453901, name: '16' },
                    { lat: 48.9963512, lng: 21.2525850, name: '17' },
                    { lat: 48.9985134, lng: 21.2508423, name: '18' },
                    { lat: 49.0085000, lng: 21.2508000, name: '19' },
                    { lat: 49.0093000, lng: 21.2528000, name: '20' },
                    { lat: 49.0103000, lng: 21.2560000, name: '21' },
                    { lat: 49.0112000, lng: 21.2590000, name: '22' },
                    { lat: 49.0124000, lng: 21.2620000, name: '23' },
                    { lat: 49.0135000, lng: 21.2650000, name: '24' },
                    { lat: 49.0149000, lng: 21.2680000, name: '25' },
                    { lat: 49.0171000, lng: 21.2710000, name: '26' },
                    { lat: 49.0198000, lng: 21.2740000, name: '27' },
                    { lat: 49.0305000, lng: 21.3000000, name: '28' },
                    { lat: 49.0405000, lng: 21.2700000, name: '29' },
                    { lat: 49.0505000, lng: 21.27800000, name: '<b>30</b><br>This is test customer' },
                    // ... as many other stations as you need
                ];

                // Zoom and center map automatically by stations (each station will be in visible map area)
                var lngs = stations.map(function (station) { return station.lng; });
                var lats = stations.map(function (station) { return station.lat; });
                map.fitBounds({
                    west: Math.min.apply(null, lngs),
                    east: Math.max.apply(null, lngs),
                    north: Math.min.apply(null, lats),
                    south: Math.max.apply(null, lats),
                });
                var marker;
                // Show stations on the map as markers
                var infowindow;
                for (var i = 0; i < stations.length; i++) {
                    marker=   new google.maps.Marker({
                        position: stations[i],
                        map: map,
                        label: ''+i,
                        title: stations[i].name
                    });
                    infowindow = new google.maps.InfoWindow({});
                    google.maps.event.addListener(marker, 'click', (function (marker, i) {
                        return function () {
                            var content = stations[i].name;
                            infowindow.setContent(content);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                    //marker.addListener("click", (e) => {
                    //    var content = "test"+i;
                    //    infowindow.setContent(content);
                    //    infowindow.open(map, marker);
                    //    console.log(this)
                    //    console.log(e)
                    //});
                }
                
                
                // Divide route to several parts because max stations limit is 25 (23 waypoints + 1 origin + 1 destination)
                for (var i = 0, parts = [], max = 8 - 1; i < stations.length; i = i + max)
                    parts.push(stations.slice(i, i + max + 1));

                // Service callback to process service results
                var service_callback = function (response, status) {
                    if (status != 'OK') {
                        console.log('Directions request failed due to ' + status);
                        return;
                    }
                    var renderer = new google.maps.DirectionsRenderer;
                    if (!window.gRenderers)
                        window.gRenderers = [];
                    window.gRenderers.push(renderer);
                    renderer.setMap(map);
                    renderer.setOptions({ suppressMarkers: true, preserveViewport: true });
                    renderer.setDirections(response);
                };

                // Send requests to service to get route (for stations count <= 25 only one request will be sent)
                for (var i = 0; i < parts.length; i++) {
                    // Waypoints does not include first station (origin) and last station (destination)
                    var waypoints = [];
                    for (var j = 1; j < parts[i].length - 1; j++)
                        waypoints.push({ location: parts[i][j], stopover: false });
                    // Service options
                    var service_options = {
                        origin: parts[i][0],
                        destination: parts[i][parts[i].length - 1],
                        waypoints: waypoints,
                        travelMode: 'WALKING'
                    };
                    // Send request
                    service.route(service_options, service_callback);
                }
            }
        </script>
        <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyA8Y7FRVyPLlIEvF11qdFiD-ZWsf5OVIjs&callback=initMap"></script>
        >
    </form>
</body>
</html>
