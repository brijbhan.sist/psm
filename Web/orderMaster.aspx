﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Admin/MasterPage.master" CodeFile="orderMaster.aspx.cs" Inherits="Web_orderMaster" ClientIDMode="static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        #tblProductDetail tbody .ProName {
            text-align: left;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input id="DraftAutoId" type="hidden" />
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Manage Order</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage Order</a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">

                    <a class="dropdown-item" onclick="location.href='/Web/WebOrderList.aspx'" id="linktoOrderList" runat="server">Order List</a>

                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <input type="hidden" id="hiddenEmpTypeVal" runat="server" />
                        <input type="hidden" id="hiddenStatusCode" runat="server" />
                        <input type="hidden" id="hfMLTax" />
                        <div class="card-content collapse show">
                            <div class="card-body" id="panelBill">
                                <div class="row">
                                    <input type="hidden" id="ddlCustomer" />
                                    <div class="col-md-3 col-sm-3">
                                        <label class="control-label">Order No</label>
                                        <input type="hidden" id="txtHOrderAutoId" class="form-control input-sm" />
                                        <div class="form-group">
                                            <input type="text" id="txtOrderId" class="form-control input-sm border-primary" readonly="readonly" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label class="control-label">Order Date</label>
                                        <div class="form-group">
                                            <input type="text" id="txtOrderDate" class="form-control input-sm border-primary" readonly="readonly" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label class="control-label">Delivery Date</label>
                                        <div class="form-group input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" id="txtDeliveryDate" class="form-control input-sm border-primary" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label class="control-label">Order Status</label>
                                        <input type="hidden" id="hiddenOrderStatus" />
                                        <div class="form-group">
                                            <input type="text" id="txtOrderStatus" class="form-control input-sm border-primary" readonly="readonly" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3  col-sm-3">
                                        <label class="control-label">Customer</label>
                                        <div class="form-group">
                                            <input type="text" id="txtCustomer" class="form-control input-sm border-primary" readonly="readonly" />
                                        </div>
                                    </div>
                                    <div class="col-md-3  col-sm-3">
                                        <label class="control-label">Customer Type</label>
                                        <div class="form-group">
                                            <input type="text" id="txtCustomerType" class="form-control input-sm border-primary" readonly="readonly" />
                                        </div>
                                    </div>
                                    <div class="col-md-3  col-sm-3">
                                        <label class="control-label">Terms</label>
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm border-primary" id="txtTerms" runat="server" disabled="disabled" />
                                        </div>
                                    </div>
                                    <div class="col-md-3  col-sm-3">
                                        <label class="control-label">
                                            Shipping Type <span class="required">*</span>

                                        </label>
                                        <div class="form-group">
                                            <select class="form-control input-sm border-primary ddlreq" id="ddlShippingType" runat="server">
                                                <option value="0">- Select Shipping Type -</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="drag-area1">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">


                        <div class="card-header">
                            <h4 class="card-title">Customer Addresses</h4>


                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-plus"></i></a></li>
                                </ul>
                            </div>

                        </div>
                        <div class="card-content collapse">
                            <div class="card-body" id="panelBill1">

                                <div class="row">
                                    <div class="col-md-6  col-sm-6">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <label class="col-md-6 col-sm-6 control-label">
                                                        Billing Address
                                        <small class="pull-right" addrtype="11" id="popBillAddr"><a href="#" data-toggle="modal" data-target="#modalChangeAddress" onclick="getAddressList(this)">[Change Address]</a></small>
                                                    </label>
                                                    <div class="col-md-6 col-sm-6  form-group">
                                                        <textarea class="form-control input-sm border-primary" id="txtBillAddress" readonly="readonly"></textarea>
                                                        <input type="hidden" id="hiddenBillAddrAutoId" />
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <label class="col-md-6  col-sm-6  control-label">
                                                        Shipping Address
                                        <small class="pull-right" addrtype="22" id="popShipAddr"><a href="#" data-toggle="modal" data-target="#modalChangeAddress" onclick="getAddressList(this)">[Change Address]</a></small>
                                                    </label>
                                                    <div class="col-md-6  col-sm-6  form-group">
                                                        <textarea class="form-control input-sm border-primary" id="txtShipAddress" readonly="readonly"></textarea>
                                                        <input type="hidden" id="hiddenShipAddrAutoId" />
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="panelProduct">
            <div class="row" id="panelOrderContent">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-header">
                            <h4 class="card-title">Add Product</h4>


                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-plus" onclick="LoadProducts()" id="LoadProducts"></i></a></li>
                                </ul>
                            </div>

                        </div>

                        <div class="card-content collapse">
                            <div class="card-body" id="panelBill5">
                                <div class="row">
                                    <label class="col-md-1 col-sm-1 control-label">Barcode</label>
                                    <div class="col-md-3  col-sm-3  form-group">
                                        <input type="text" class="form-control input-sm border-primary" id="txtBarcode" runat="server" onfocus="this.select()" disabled="disabled" placeholder="Enter Barcode here" onchange="readBarcode()" />
                                    </div>

                                    <div class="col-md-3  col-sm-3 ">
                                        <div class="alert alert-default alertSmall pull-right " id="alertBarcodeCount" style="display: none; color: #c62828;">
                                            Barcode Count : &nbsp;<span>sd</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="row">
                                    <div class="col-md-5  col-sm-3 ">
                                        <label class="control-label">
                                            Product <span class="required">*</span>

                                        </label>
                                        <div class="form-group">
                                            <select class="form-control input-sm border-primary selectvalidate" id="ddlProduct" runat="server" style="width: 100% !important" onchange="BindUnittype()">
                                                <option value="0">-Select-</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2  col-sm-2 ">
                                        <label class="control-label">
                                            Unit Type <span class="required">*</span>

                                        </label>
                                        <div class="form-group">
                                            <select class="form-control input-sm border-primary ddlreq" onchange="BindunitDetails()" id="ddlUnitType" runat="server">
                                                <option value="0">-Select-</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-1  col-sm-2">
                                        <label class="control-label" style="word-wrap: normal">
                                            Qty <span class="required">*</span>

                                        </label>
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm border-primary ddlreq" id="txtReqQty" runat="server" value="1" maxlength="4" onkeypress="return isNumberKey(event)" />
                                        </div>
                                    </div>
                                    <div class="col-md-1  col-sm-1 ">
                                        <label class="control-label" style="white-space: nowrap">
                                            Is Exchange 
                                        </label>
                                        <div class="form-group">
                                            <input type="checkbox" id="chkExchange" onchange="changetype(this,'chkIsTaxable')" />
                                        </div>
                                    </div>
                                    <div class="col-md-1  col-sm-1 ">
                                        <label class="control-label" style="white-space: nowrap">
                                            Is Taxable 
                                        </label>
                                        <div class="form-group">
                                            <input type="checkbox" id="chkIsTaxable" onchange="changetype(this,'chkExchange')" />
                                        </div>
                                    </div>
                                    <div class="col-md-1  col-sm-1 " id="freeItem">
                                        <label class="control-label" style="white-space: nowrap">
                                            Is Free Item 
                                        </label>
                                        <div class="form-group">
                                            <input type="checkbox" id="chkFreeItem" />
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="pull-left">
                                            <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1 btn-sm pulse" id="btnAdd" disabled style="margin-top: 22px;">&nbsp;&nbsp;&nbsp;Add&nbsp;&nbsp;&nbsp;</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 form-group">
                                        <div class="alert alert-danger alertSmall" id="alertStockQty" style="display: none; text-align: center; color: white !important">
                                        </div>
                                    </div>

                                </div>
                                <div class="clearfix"></div>
                                <div id="divBarcode" style="display: none;">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-2 col-ms-2">
                                                    <label class="control-label">Quantity</label>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm border-primary" id="txtQty" value="1" maxlength="4" />
                                                    </div>
                                                </div>
                                                <div class="col-md-1  col-sm-1 ">
                                                    <label class="control-label">
                                                        Is Exchange 
                                                    </label>
                                                    <div class="form-group">
                                                        <input type="checkbox" id="IsExchange" onchange="changetype(this,'IsFreeItem')" />
                                                    </div>
                                                </div>
                                                <div class="col-md-1  col-sm-1 ">
                                                    <label class="control-label">
                                                        Is Free Item 
                                                    </label>
                                                    <div class="form-group">
                                                        <input type="checkbox" id="IsFreeItem" onchange="changetype(this,'IsExchange')" />
                                                    </div>
                                                </div>
                                                <script>
                                                    function changetype(e, idhtml) {
                                                        $('#' + idhtml).prop('checked', false);
                                                    }
                                                </script>
                                                <div class="col-md-3  col-ms-3">
                                                    <label class="control-label">Enter Barcode</label>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm border-primary" id="txtScanBarcode" onchange="checkBarcode()" />
                                                    </div>
                                                </div>
                                                <div class="col-md-3  col-ms-3">
                                                    <label class="control-label">
                                                        Product<span class="required">*</span>
                                                        <asp:RequiredFieldValidator ID="rfvitemproduct" runat="server" ErrorMessage="Please Select Product" SetFocusOnError="true"
                                                            ValidationGroup="AddProductQty" ControlToValidate="ddlitemproduct" Display="None" InitialValue="0" ForeColor="Transparent">*</asp:RequiredFieldValidator>
                                                        <asp:ValidatorCalloutExtender runat="server" CssClass="CustomValidatorCalloutStyle"
                                                            ID="ValidatorCalloutExtender1" TargetControlID="rfvitemproduct" PopupPosition="BottomLeft">
                                                        </asp:ValidatorCalloutExtender>
                                                    </label>
                                                    <div class="form-group">
                                                        <select class="form-control input-sm border-primary" id="ddlitemproduct" runat="server" onchange="itemProduct()" style="width: 100% !important">
                                                            <option value="0" style="display: none">-Select-</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2  col-ms-2">
                                                    <div class="pull-left">
                                                        <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1 btn-sm pulse" onclick="AddProductQty()" style="margin-top: 22px;">&nbsp;&nbsp;&nbsp;Add&nbsp;&nbsp;&nbsp;</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        <section>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-content collapse show">
                            <div class="card-body" id="panelBill6">
                                <div class="table-responsive">
                                    <table id="tblProductDetail" class="table table-striped table-bordered">
                                        <thead class="bg-blue white">
                                            <tr>
                                                <td class="Action text-center">Action</td>
                                                <td class="ProId text-center">ID</td>
                                                <td class="ProName">Product Name</td>
                                                <td class="UnitType text-center">Unit</td>
                                                <td class="ReqQty text-center" style="word-wrap: break-word; width: 5%">Qty</td>
                                                <td class="Barcode text-center" style="display: none;">Barcode</td>
                                                <td class="OM_MinPrice text-center" style="display: none;">OM_MinPrice</td>
                                                <td class="OM_CostPrice text-center" style="display: none;">OM_CostPrice</td>
                                                <td class="ItemType text-center" style="display: none;">Item Type</td>
                                                <td class="AutoId text-center" style="display: none;">AutoId</td>
                                                <td class="QtyRemain text-center" style="display: none;">Qty Remaining
                                                </td>
                                                <td class="TtlPcs text-center">Total<br />
                                                    Pieces</td>
                                                <td class="UnitPrice" style="text-align: center;">Unit<br />
                                                    Price</td>
                                                <%if (Session["empTypeno"] != null)
                                                    { %>
                                                <%if (Session["empTypeno"].ToString() == "5" || Session["empTypeno"].ToString() == "6")
                                                    { %>
                                                <td class="PerPrice" style="text-align: right;">Per Piece<br />
                                                    Price</td>
                                                <% } %>
                                                <% } %>

                                                <td class="SRP price">SRP</td>
                                                <td class="GP price">GP (%)</td>
                                                <td class="TaxRate text-center" style="display: none">Tax</td>

                                                <td class="IsExchange text-center" style="display: none">Exchange</td>
                                                <%--  <td class="IsTaxable text-center" style="display: none">IsTaxable</td>--%>
                                                <td class="NetPrice price">Net Price</td>

                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                    <h5 class="well text-center" id="emptyTable" style="display: none">No Product Selected.</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>

        <div class="row">
            <div class="col-md-7 col-sm-7">

                <section id="Div1">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Order Remark</h4>
                                </div>

                                <div class="card-content collapse show">
                                    <div class="card-body" id="panelBill7">
                                        <div class="row">
                                            <div class="col-md-12 form-group">
                                                <label>Remark</label><textarea id="txtOrderRemarks" maxlength="500" class="form-control border-primary"></textarea>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </section>

                <section id="Div2" style="display: none">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Order Remark Details</h4>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body" id="panelBill10">
                                        <div class="row">
                                            <div class="col-md-12 form-group">
                                                <div class="table-responsive">
                                                    <table id="Table2" class="table table-striped table-bordered">
                                                        <thead class="bg-blue white">
                                                            <tr>
                                                                <td class="SRNO text-center">SN</td>
                                                                <td class="EmployeeName">Employee Name</td>
                                                                <td class="EmployeeType">Employee Type</td>
                                                                <td class="Remarks">Remark</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>

                </section>


                <section id="ManagerRemarks" style="display: none">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Remark Details</h4>
                                </div>

                                <div class="card-content collapse show">
                                    <div class="card-body" id="panelBill11">
                                        <div class="row">
                                            <div class="col-md-12 form-group">
                                                <label>Remark</label><textarea id="txtManagerRemarks" class="form-control border-primary"></textarea>
                                            </div>
                                        </div>


                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </section>

                <div class="row">
                    <div class="col-md-12">

                        <section id="DrvDeliveryInfo" style="display: none">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">

                                        <div class="card-content collapse show">
                                            <div class="card-body" id="panelBill13">
                                                <div class="panel panel-default">
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <label class="control-label col-md-2">Delivered</label>
                                                            <div class="col-md-4 form-group">
                                                                <div class="form-control input-sm">
                                                                    <input type="radio" class="radio-inline" name="rblDeliver" value="yes" />&nbsp;Yes
                                            <input type="radio" class="radio-inline" name="rblDeliver" value="no" />&nbsp;No 
                                                                </div>
                                                            </div>
                                                            <label class="control-label col-md-2">Delivery Remark</label>
                                                            <div class="col-md-4 form-group">
                                                                <textarea class="form-control input-sm border-primary" rows="2" placeholder="Enter Payment details here" id="txtRemarks" disabled="disabled"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <label class="control-label col-md-2">Comment Type </label>
                                                            <div class="col-md-4 form-group">
                                                                <select id="ddlCommentType" class="form-control input-sm">
                                                                    <option value="0">-Select-</option>
                                                                    <option value="1">Fully Paid</option>
                                                                    <option value="2">Partially Paid</option>
                                                                    <option value="3">Paid for Other Invoice</option>
                                                                    <option value="4">Item Missing</option>
                                                                    <option value="5">Item Damage</option>
                                                                    <option value="6">Wrong Amount for Item</option>
                                                                    <option value="7">Item Return</option>
                                                                    <option value="8">Order Return</option>
                                                                    <option value="9">Other</option>
                                                                </select>
                                                            </div>
                                                            <label class="control-label col-md-2">Comment </label>
                                                            <div class="col-md-4 form-group">
                                                                <textarea class="form-control input-sm border-primary" rows="2" placeholder="Enter Comment here..." id="txtComment"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12 form-group">
                                                                <button type="button" id="btnSaveDelStatus" class="btn btn-primary buttonAnimation pull-right round box-shadow-1 btn-sm pulse" onclick="saveDelStatus()" style="display: none;"><b>Save</b></button>
                                                                <button type="button" id="btnUpdate" class="btn btn-success buttonAnimation pull-right round box-shadow-1 btn-sm pulse" onclick="saveDelStatus()" style="display: none;"><b>Update</b></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>

            </div>

            <div class="col-md-5 col-sm-5">
                <section id="orderSummary">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">

                                <div class="card-content collapse show">
                                    <div class="card-body" id="panelBill8">
                                        <div style="text-align: right">
                                            <div class="row">
                                                <label class="col-sm-5 form-group">Sub Total</label>
                                                <div class="col-sm-7 form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;">$</span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtTotalAmount" value="0.00" readonly="readonly" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <label class="col-sm-5 form-group">Overall Discount</label>
                                                <div class="col-sm-7 form-group">
                                                    <div style="display: flex;">

                                                        <div class="input-group">
                                                            <input type="text" class="form-control input-sm border-primary" maxlength="10" style="text-align: right;" onkeypress="return isNumberDecimalKey(event,this)" id="txtOverallDisc" value="0.00"
                                                                onkeyup="calOverallDisc()" />
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text border-radius" style="padding: 0rem 1rem;">%</span>
                                                            </div>
                                                        </div>
                                                        &nbsp;
                                 
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">$</span>
                                            </div>
                                            <input type="text" class="form-control input-sm border-primary" style="text-align: right;" onkeyup="calOverallDisc1()"
                                                onkeypress="return isNumberDecimalKey(event,this)" maxlength="10" id="txtDiscAmt" value="0.00" />
                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="row">
                                                <label class="col-sm-5 form-group">Shipping Charges</label>
                                                <div class="col-sm-7 form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;">$</span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtShipping" value="0.00" onkeyup="calGrandTotal()"
                                                            onfocus="this.select()" onkeypress="return isNumberDecimalKey(event,this)" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="row">
                                                <label class="col-sm-5 form-group">
                                                    Tax Type<span class="required"></span>

                                                </label>
                                                <div class="col-sm-7 form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;">%</span>
                                                        </div>
                                                        <select class="form-control input-sm border-primary" id="ddlTaxType" onchange="calTotalTax()" runat="server">
                                                            <option value="0" taxvalue="0.0">-Select Tax Type-</option>
                                                        </select>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-sm-5 form-group">Total Tax</label>
                                                <div class="col-sm-7 form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;">$</span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtTotalTax" value="0.00" readonly="readonly" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-sm-5 form-group">ML Quantity</label>
                                                <div class="col-sm-7 form-group">
                                                    <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtMLQty" value="0.00" readonly="readonly" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-sm-5 form-group">ML Tax</label>
                                                <div class="col-sm-7 form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;">$</span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtMLTax" value="0.00" readonly="readonly" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-sm-5 form-group">Weight Quantity</label>
                                                <div class="col-sm-7 form-group">
                                                    <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtWeightQuantity" value="0.00" readonly="readonly" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-sm-5 form-group">Weight Tax</label>
                                                <div class="col-sm-7 form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;">$</span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtWeightTax" value="0.00" readonly="readonly" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-sm-5 form-group">Adjustment</label>
                                                <div class="col-sm-7 form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;">$</span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtAdjustment" value="0.00" readonly="readonly" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-sm-5 form-group"><b>Order Total</b></label>
                                                <div class="col-sm-7 form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;">$</span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right; font-size: 14px; font-weight: 700;" readonly="readonly" id="txtGrandTotal" value="0.00" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row SMANAGER" style="display: none">
                                                <label class="col-sm-5 form-group">Credit Memo Amount</label>
                                                <div class="col-sm-7 form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">

                                                            <span class="input-group-text" style="padding: 0rem 1rem;">$</span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtDeductionAmount" value="0.00" readonly="readonly" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="row SMANAGER" style="display: none">
                                                <label class="col-sm-5 form-group">Store Credit Amount <span id="creditShow">(max $<span id="CreditMemoAmount">0.00</span> Available)</span></label>
                                                <div class="col-sm-7 form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">

                                                            <span class="input-group-text" style="padding: 0rem 1rem;">$</span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" disabled style="text-align: right" id="txtStoreCreditAmount" onkeyup="checkcreditAmount()" onfocus="this.select()" value="0.00" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="row SMANAGER" style="display: none">
                                                <label class="col-sm-5 form-group">Payable Amount</label>
                                                <div class="col-sm-7 form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">

                                                            <span class="input-group-text" style="padding: 0rem 1rem;">$</span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtPaybleAmount" value="0.00" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <%--   <div class="row" style="display: none;" id="hidenopackedbox">
                                                <label class="col-sm-5 form-group">
                                                    No. of Packed Boxes
                                                </label>
                                                <div class="col-sm-7 form-group">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control input-sm border-primary" id="txtPackedBoxes" runat="server" onkeypress="return isNumberKey(event)" />
                                                    </div>
                                                </div>
                                            </div>--%>
                                            <div class="row" style="display: none;" id="rowAmountPaid">
                                                <label class="col-sm-5 form-group"><b>Total Paid Amount</b></label>
                                                <div class="col-sm-7 form-group">
                                                    <div class="input-group">
                                                        <span class="input-group-text" style="padding: 0rem 1rem;">$</span>
                                                        <input type="text" class="form-control input-sm inputBold border-primary" readonly="readonly" id="txtAmtPaid" value="0.00" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="display: none;" id="rowAmountDue">
                                                <label class="col-sm-5 form-group"><b>Due Amount</b></label>
                                                <div class="col-sm-7 form-group">
                                                    <div class="input-group">
                                                        <span class="input-group-text" style="padding: 0rem 1rem;">$</span>
                                                        <input type="text" class="form-control input-sm inputBold border-primary" readonly="readonly" id="txtAmtDue" value="0.00" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>


        </div>


        <section>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body" id="panelBill9">
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <%--<button type="button" id="btnReset" class="btn btn-secondary buttonAnimation round box-shadow-1 btn-sm pulse">Reset</button>--%>
                                        <button type="button" id="btnApproveOrder" class="btn btn-warning buttonAnimation round box-shadow-1 btn-sm pulse" onclick="approveOrderSecurity()" style="display: none;">Approve</button>
                                        <button type="button" id="btnUpdateOrder" class="btn btn-primary buttonAnimation round box-shadow-1 btn-sm pulse" onclick="UpdateOrder(21)" style="display: none;">Update Order </button>
                                        <button type="button" id="btnCancelOrder" class="btn btn-warning buttonAnimation round box-shadow-1 btn-sm pulse" style="display: none;" onclick="confirm_Cancellation_Of_Order()">Cancel Order </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- Modal - Change Address -->
    <div class="modal fade" id="modalChangeAddress" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="display: block;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="modalTitle"></h4>
                </div>

                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" id="tblChangeAdd">
                            <thead class="bg-blue white">
                                <tr>
                                    <td class="text-center">Set Default</td>
                                    <td class="text-center">Action</td>
                                    <td>Customer Name</td>
                                    <td>Address</td>
                                    <td>State</td>
                                    <td>City</td>
                                    <td class="text-center">Zipcode</td>

                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                    <div class="col-md-12">
                        <div class="pull-right">
                            <small><a href="javascript:;" id="NewAddr" onclick="addNewAddr()">Add new address</a></small>
                        </div>
                    </div>
                    <br />


                    <div class="panel-body">
                        <section style="display: none;" id="panelAddNewAddr">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">

                                        <div class="card-content collapse show">
                                            <div class="card-header">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <b>Add new address details</b>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="card-body" id="panelBill16">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <label class="control-label">Address <span class="required">*</span></label>
                                                    </div>
                                                    <div class="col-md-4 form-group">
                                                        <textarea rows="2" class="form-control input-sm border-primary" id="txtAddr" runat="server"></textarea>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="control-label">Zipcode <span class="required">*</span></label>
                                                    </div>
                                                    <div class="col-md-4 form-group">
                                                        <select id="ddlZipCode" runat="server" class="form-control border-primary input-sm" style="width: 220px !important" onchange="getCityState(1,this)">
                                                            <option value="0">-Select-</option>
                                                        </select>
                                                    </div>

                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <label class="control-label">City</label>
                                                    </div>
                                                    <div class="col-md-4 form-group">
                                                        <input type="text" class="form-control input-sm border-primary" id="txtCity1" readonly="true" runat="server" onfocus="this.select()" />
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="control-label">State</label>
                                                    </div>
                                                    <div class="col-md-4 form-group">
                                                        <input type="text" class="form-control input-sm border-primary" id="txtState1" readonly="true" runat="server" onfocus="this.select()" />

                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 form-group">
                                                        <div class="pull-right">
                                                            <button type="button" id="btnSaveAddr" class="btn btn-success buttonAnimation pull-right round box-shadow-1 btn-sm" onclick="saveAddr()">Save</button>
                                                            <button type="button" id="btnUpdateAddr" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm" style="display: none">Update</button>
                                                            <button type="button" id="btnCancelUpdate" class="btn btn-warning buttonAnimation pull-right round box-shadow-1 btn-sm" style="display: none;">Cancel</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </section>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm" data-dismiss="modal" onclick="modalClose()">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="OrderBarcode" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content">

                <div class="modal-header">
                    Order Cancellation Form
                </div>
                <div class="modal-body">
                    <div id="form-group">
                        <label class="form-group">Cancellation Remark</label>
                    </div>
                    <div id="Div8">
                        <textarea id="txtCancellationRemarks" placeholder="Please Enter Cancellation Remark" class="form-control border-primary" row="4"></textarea>
                    </div>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1 btn-sm pulse" id="Button3" onclick="cancelOrders()">Cancel Order</button>
                    <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1 btn-sm pulse" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div id="msgPop" class="modal fade" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">
            <!-- Modal content-->
            <div class="modal-content">
                <br />
                <br />
                <br />
                <center><div id="packermsg"></div></center>

                <div class="modal-body">
                    <center>
                       
                       <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse"  data-dismiss="modal" >Close</button>          
                    </center>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" runat="server" id="HDDomain" />

    <div class="modal fade" id="SecurityEnabled" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="display: block">
                    <h4>Check Security </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">Security </div>
                        <div class="col-md-9">
                            <input type="password" id="txtSecurity" class="form-control input-sm border-primary" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <span id="errormsg"></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning buttonAnimation pull-right round box-shadow-1 btn-sm pulse" onclick="clickonSecurity()">OK</button>
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="SecurityEnvalid" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Message</h4>
                </div>
                <div class="modal-body">
                    <div style="border-color: #ac2925; border: 1px solid; color: #ac2925; min-height: 45px; line-height: 20px; padding: 9px;">
                        <strong id="Sbarcodemsg"></strong>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1 btn-sm pulse" onclick="ClosePop()">Close</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function funchek(e) {
            var check = 0;
            if ($(e).prop('checked')) {
                check = 1;
            }
            $('#chkExchangePop').prop('checked', false);
            $('#chkFreePop').prop('checked', false);
            $('#chkTaxablePop').prop('checked', false);
            if (check == 1) {
                $(e).prop('checked', true);
            }
        }
    </script>
    <input type="hidden" id="ItemAutoId" />
    <div class="modal fade" id="PopChangeItemType" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-md">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Change Item Type</h4>
                </div>
                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col-md-1"></div>
                        <div class="col-md-3">
                            <label>Product Id</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" id="txtProductId" class="form-control input-sm border-primary" />
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-1"></div>
                        <div class="col-md-3">
                            <label style="white-space: nowrap">Product Name</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" id="txtProductName" class="form-control input-sm border-primary" />
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-1"></div>
                        <div class="col-md-3">
                            <label>Exchange</label>
                        </div>
                        <div class="col-md-8">
                            <input type="checkbox" id="chkExchangePop" onclick="funchek(this)" />
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-1"></div>
                        <div class="col-md-3">
                            <label>Free</label>
                        </div>
                        <div class="col-md-8">
                            <input type="checkbox" id="chkFreePop" onclick="funchek(this)" />
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-1"></div>
                        <div class="col-md-3">
                            <label>Taxable</label>
                        </div>
                        <div class="col-md-8">
                            <input type="checkbox" id="chkTaxablePop" onclick="funchek(this)" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1 btn-sm pulse" onclick="updateIteminList()">Change</button>
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse" onclick="CloseCheckPop()">Close</button>
                </div>
            </div>
        </div>
    </div>
    <audio class="audios" id="yes_audio" controls preload="none" style="display: none">
        <source src="/Audio/NOExists.mp3" type="audio/mpeg" />
    </audio>
    <audio class="audios" id="No_audio" controls preload="none" style="display: none">
        <source src="/Audio/WrongProduct.mp3" type="audio/mpeg" />
    </audio>

    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="/Web/JS/OrderMaster.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
</asp:Content>


