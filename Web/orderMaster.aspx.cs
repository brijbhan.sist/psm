﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DllWebOrderMaster;
using Newtonsoft.Json;

public partial class Web_orderMaster : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod(EnableSession = true)]
    public static string bindAllDropdown()
    {
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.SalesPersonAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                pobj.LoginEmpType = Convert.ToInt32(HttpContext.Current.Session["EmpTypeNo"]);
                BL_OrderMaster.bindAllDropdown(pobj);
                if (!pobj.isException)
                {
                    string json = "";
                    foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                    {
                        json += dr[0];
                    }
                    return json;
                }
                else
                {
                    return pobj.exceptionMessage;
                }

            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

   
    [WebMethod(EnableSession = true)]
    public static string bindProductDropdown()
    {
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                BL_OrderMaster.bindProductDropdown(pobj);
                if (!pobj.isException)
                {
                    string json = "";
                    foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                    {
                        json += dr[0];
                    }

                    return json;
                }
                else
                {
                    return pobj.exceptionMessage;
                }

            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string editOrder(string orderNo)
    {
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.OrderNo = orderNo;
                pobj.LoginEmpType = Convert.ToInt32(HttpContext.Current.Session["EmpTypeNo"]);
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                BL_OrderMaster.editOrderDetails(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }


  
    [WebMethod(EnableSession = true)]
    public static string bindUnitType(int productAutoId)
    {
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.ProductAutoId = productAutoId;
                BL_OrderMaster.bindUnitType(pobj);
                return pobj.Ds.GetXml();
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string selectQtyPrice(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustAutoId"]);
                pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]);
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                BL_OrderMaster.selectQtyPrice(pobj);
                return pobj.Ds.GetXml();
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string SaveDraftOrder(string dataValues)
    {
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValues);
                if (jdv["DraftAutoId"] != "")
                    pobj.DraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.ShippingType = Convert.ToInt32(jdv["ShippingType"]);
                if (jdv["DeliveryDate"] != "")
                    pobj.DeliveryDate = Convert.ToDateTime(jdv["DeliveryDate"]);
                pobj.ProductAutoId = Convert.ToInt32(jdv["productAutoId"]);
                pobj.UnitAutoId = Convert.ToInt32(jdv["unitAutoId"]);
                pobj.ReqQty = Convert.ToInt32(jdv["ReqQty"]);
                pobj.QtyPerUnit = Convert.ToInt32(jdv["QtyPerUnit"]);
                pobj.UnitPrice = Convert.ToDecimal(jdv["UnitPrice"]);
                if (jdv["minprice"] != "")
                {
                    pobj.minprice = Convert.ToDecimal(jdv["minprice"]);
                }
                else
                {
                    pobj.minprice = 0;
                }
                pobj.SRP = Convert.ToDecimal(jdv["SRP"]);
                pobj.GP = Convert.ToDecimal(jdv["GP"]);
                pobj.MLQty = Convert.ToDecimal(jdv["MLQty"]);
                pobj.IsTaxable = Convert.ToInt32(jdv["IsTaxable"]);
                pobj.IsExchange = Convert.ToInt32(jdv["IsExchange"]);
                pobj.IsFreeItem = Convert.ToInt32(jdv["IsFreeItem"]);
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                pobj.Remarks = jdv["Remarks"];
                BL_OrderMaster.SaveDraftOrder(pobj);
                if (!pobj.isException)
                {
                    return pobj.DraftAutoId.ToString();
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string UpdateDraftReq(string dataValues)
    {
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValues);
                pobj.DraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]);
                pobj.IsExchange = Convert.ToInt32(jdv["IsExchange"]);
                pobj.IsFreeItem = Convert.ToInt32(jdv["IsFreeItem"]);
                if (jdv["ReqQty"] != "")
                    pobj.ReqQty = Convert.ToInt32(jdv["ReqQty"]);
                if (jdv["UnitPrice"] != "")
                    pobj.UnitPrice = Convert.ToDecimal(jdv["UnitPrice"]);

                BL_OrderMaster.UpdateDraftReq(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string DeleteDraftItem(string DataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(DataValue);
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {

                pobj.DraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]);
                pobj.IsFreeItem = Convert.ToInt32(jdv["isfreeitem"]);
                pobj.IsExchange = Convert.ToInt32(jdv["IsExchange"]);
                BL_OrderMaster.DeleteDraftItem(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    
    [WebMethod(EnableSession = true)]
    public static string ItemTypeUpdate(string dataValue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {

            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_OrderMaster pobj = new PL_OrderMaster();
                pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
                pobj.DraftAutoId = Convert.ToInt32(jdv["ItemAutoId"]);

                pobj.IsExchange = Convert.ToInt32(jdv["IsExchange"]);
                pobj.IsFreeItem = Convert.ToInt32(jdv["IsFreeItem"]);
                pobj.IsTaxable = Convert.ToInt32(jdv["IsTaxable"]);
                BL_OrderMaster.ItemTypeUpdate(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";

                }
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    

    [WebMethod(EnableSession = true)]
    public static string AddProductQty(string ProductAutoId, string UnitAutoId)
    {
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.ProductAutoId = Convert.ToInt32(ProductAutoId);
                pobj.UnitAutoId = Convert.ToInt32(UnitAutoId);
                BL_OrderMaster.AddProductQty(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
   

    [WebMethod(EnableSession = true)]
    public static string updateOrder(string TableValues, string orderData)
    {
        DataTable dtOrder = new DataTable();
        dtOrder = JsonConvert.DeserializeObject<DataTable>(TableValues);
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(orderData);
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                if (dtOrder.Rows.Count > 0)
                {
                    pobj.TableValue = dtOrder;
                }
                if (jdv["DeliveryDate"] != null && jdv["DeliveryDate"] != "")
                {
                    pobj.DeliveryDate = Convert.ToDateTime(jdv["DeliveryDate"]);
                }
                pobj.LoginEmpType = Convert.ToInt32(HttpContext.Current.Session["EmpTypeNo"]);
                pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
                pobj.ApproveStatus = Convert.ToInt32(jdv["ApproveStatus"]);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.Remarks = jdv["Remarks"];
                pobj.BillAddrAutoId = Convert.ToInt32(jdv["BillAddrAutoId"]);
                pobj.OrderStatus = Convert.ToInt32(jdv["Status"]);
                pobj.ShipAddrAutoId = Convert.ToInt32(jdv["ShipAddrAutoId"]);
                pobj.SalesPersonAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                pobj.TotalAmount = Convert.ToDecimal(jdv["TotalAmount"]);
                pobj.OverallDisc = Convert.ToDecimal(jdv["OverallDisc"]);
                pobj.OverallDiscAmt = Convert.ToDecimal(jdv["OverallDiscAmt"]);
                pobj.ShippingCharges = Convert.ToDecimal(jdv["ShippingCharges"]);
                pobj.TotalTax = Convert.ToDecimal(jdv["TotalTax"]);
                pobj.GrandTotal = Convert.ToDecimal(jdv["GrandTotal"]);
                pobj.ShippingType = jdv["ShippingType"];
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                pobj.MLQty = Convert.ToDecimal(jdv["MLQty"]);
                pobj.TaxType = Convert.ToInt32(jdv["TaxType"]);
                BL_OrderMaster.updateOrder(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;// "Oops! Something went wrong.Please try later.";
        }
    }
 

    [WebMethod(EnableSession = true)]
    public static string cancelOrder(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                pobj.Remarks = jdv["Remarks"];
                BL_OrderMaster.cancelOrder(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
   
    [WebMethod(EnableSession = true)]
    public static string GetBarDetails(string dataValues)
    {
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValues);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                if (jdv["DraftAutoId"] != "")
                    pobj.DraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
                pobj.ShippingType = Convert.ToInt32(jdv["ShippingType"]);
                if (jdv["DeliveryDate"] != "")
                    pobj.DeliveryDate = Convert.ToDateTime(jdv["DeliveryDate"]);
                if (jdv["ReqQty"] != "")
                    pobj.ReqQty = Convert.ToInt32(jdv["ReqQty"]);
                else
                    pobj.ReqQty = 1;
                pobj.Barcode = jdv["Barcode"];
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                pobj.IsTaxable = Convert.ToInt32(jdv["IsTaxable"]);
                pobj.IsExchange = Convert.ToInt32(jdv["IsExchange"]);
                pobj.IsFreeItem = Convert.ToInt32(jdv["IsFreeItem"]);
                BL_OrderMaster.GetBarDetails(pobj);
                if(!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
                
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    
    [WebMethod(EnableSession = true)]
    public static string clickonSecurity(string CheckSecurity)
    {
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {

                pobj.CheckSecurity = CheckSecurity;
                BL_OrderMaster.clickonSecurity(pobj);
                if (!pobj.isException)
                {
                    if (pobj.Ds.Tables.Count > 0 && pobj.Ds.Tables[0].Rows.Count > 0)
                    {
                        return "true";
                    }
                    else
                    {
                        return "false";
                    }
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string approveOrder(string dataValue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {

            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_OrderMaster pobj = new PL_OrderMaster();
                pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
                BL_OrderMaster.approveOrder(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return "false";

                }
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}