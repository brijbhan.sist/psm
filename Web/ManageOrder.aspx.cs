﻿using DllOrderMasterWeb;
using Newtonsoft.Json;
using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;

public partial class Web_ManageOrder : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Web/JS/ManageOrder.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredField'>" + text + "</script>"
                ));
        }
        try
        {
            if (Request.QueryString["OrderNo"] == null && (HttpContext.Current.Session["EmpTypeNo"].ToString() != "2"))
            {
                Session.Abandon();
                Response.Redirect("~/Default.aspx", false);
            }
            hiddenEmpTypeVal.Value = HttpContext.Current.Session["EmpTypeNo"].ToString();
        }
        catch
        {
            Session.Abandon();
            Response.Redirect("~/Default.aspx", false);
        }
        string[] GtUrl = Request.Url.Host.ToString().Split('.');
        HDDomain.Value = GtUrl[0].ToLower();
    }

    [WebMethod(EnableSession = true)]
    public static string editOrder(string orderNo, string loginEmpType)
    {
        PL_OrderMasterWeb pobj = new PL_OrderMasterWeb();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.OrderNo = orderNo;
                pobj.LoginEmpType = Convert.ToInt32(loginEmpType);
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                BL_OrderMasterWeb.GetOrderdetails(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string bindDropdown()
    {
        PL_OrderMasterWeb pobj = new PL_OrderMasterWeb();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                BL_OrderMasterWeb.bindDropdown(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0].ToString();
                }
                return json;
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string bindUnitType(int productAutoId)
    {
        PL_OrderMasterWeb pobj = new PL_OrderMasterWeb();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.ProductAutoId = productAutoId;
                BL_OrderMasterWeb.bindUnitType(pobj);
                return pobj.Ds.GetXml();
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string GetBarDetails(string dataValues)
    {
        PL_OrderMasterWeb pobj = new PL_OrderMasterWeb();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValues);
                pobj.ReqQty = 1;
                pobj.Barcode = jdv["Barcode"];
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                BL_OrderMasterWeb.GetBarDetails(pobj);
                return pobj.Ds.GetXml();
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string updateOrder(string TableValues, string orderData)
    {
        DataTable dtOrder = JsonConvert.DeserializeObject<DataTable>(TableValues);
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(orderData);
        PL_OrderMasterWeb pobj = new PL_OrderMasterWeb();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                if (dtOrder.Rows.Count > 0)
                {
                    pobj.TableValue = dtOrder;
                }
                pobj.LoginEmpType = Convert.ToInt32(HttpContext.Current.Session["EmpTypeNo"]);
                pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                BL_OrderMasterWeb.updateOrder(pobj);
                if (!pobj.isException)
                {
                    return pobj.exceptionMessage;
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops ! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string assignPacker(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_OrderMasterWeb pobj = new PL_OrderMasterWeb();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
                pobj.PackerAutoId = Convert.ToInt32(jdv["PackerAutoId"]);
                if (jdv["Times"] != "")
                {
                    pobj.Times = Convert.ToInt32(jdv["Times"]);
                }
                else
                {
                    pobj.Times = 0;
                }
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                pobj.Remarks = jdv["Remarks"];
                BL_OrderMasterWeb.assignPacker(pobj);
                if (!pobj.isException)
                {

                    return "true";
                }
                else
                {

                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string selectQtyPrice(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_OrderMasterWeb pobj = new PL_OrderMasterWeb();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustAutoId"]);
                pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]);
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                BL_OrderMasterWeb.selectQtyPrice(pobj);
                return pobj.Ds.GetXml();
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string viewOrderLog(int OrderAutoId)
    {
        PL_OrderMasterWeb pobj = new PL_OrderMasterWeb();
        pobj.OrderAutoId = OrderAutoId;

        BL_OrderMasterWeb.getOrderLog(pobj);
        if (!pobj.isException)
        {
            return pobj.Ds.GetXml();
        }
        else
        {
            return pobj.exceptionMessage;
        }
    }

    [WebMethod(EnableSession = true)]
    public static string getPackerList(int OrderAutoId)
    {
        PL_OrderMasterWeb pobj = new PL_OrderMasterWeb();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.OrderAutoId = OrderAutoId;
                BL_OrderMasterWeb.getPackerList(pobj);
                return pobj.Ds.GetXml();
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string clickonSecurity(string CheckSecurity)
    {
        PL_OrderMasterWeb pobj = new PL_OrderMasterWeb();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {

                pobj.CheckSecurity = CheckSecurity;
                BL_OrderMasterWeb.clickonSecurity(pobj);
                if (!pobj.isException)
                {
                    if (pobj.Ds.Tables.Count > 0 && pobj.Ds.Tables[0].Rows.Count > 0)
                    {
                        return "true";
                    }
                    else
                    {
                        return "false";
                    }
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

}