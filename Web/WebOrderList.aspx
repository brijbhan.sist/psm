﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Admin/MasterPage.master" CodeFile="WebOrderList.aspx.cs" Inherits="Web_WebOrderList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .StatusRTS {
            BACKGROUND-COLOR: #1166b1;
            COLOR: WHITE;
            PADDING: 3PX;
            BORDER-RADIUS: 5PX;
        }

        .addon {
            background-color: green;
            color: white;
            padding: 5px;
            border-radius: 5px;
        }

        .input-group-text {
            padding: 0 1rem
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Order List</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Manage Order</a>
                        </li>
                        <li class="breadcrumb-item">Order List
                        </li>
                    </ol>
                </div>
            </div>
        </div>

    </div>

    <div class="content-body" style="min-height:400px">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-4">
                                        <select class="form-control border-primary input-sm" id="ddlSalesPerson" runat="server" onchange="BindCustomer();">
                                            <option value="0">All Sales Person</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <select class="form-control border-primary input-sm" id="ddlCustomer" runat="server">
                                            <option value="0">All Customer</option>
                                        </select>
                                    </div>

                                    <div class="col-sm-12 col-md-4">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Order No" id="txtSOrderNo" onfocus="this.select()" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-md-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                   From Order Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" placeholder="Order From Date" id="txtSFromDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                   To Order Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" placeholder="Order To Date" id="txtSToDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" id="btnSearch">Search</button>&nbsp;&nbsp;&nbsp;
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblOrderList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="action text-center">Action</td>
                                                        <td class="status  text-center">Status</td>
                                                        <td class="orderNo  text-center">Order No</td>
                                                        <td class="orderDt  text-center">Order Date</td>
                                                        <td class="SalesPerson">Sales Person</td>
                                                        <td class="cust">Customer</td>
                                                        <td class="value price">Order Amount</td>
                                                        <td class="product  text-center">Products</td>
                                                        <td class="Shipping">Shipping Type</td>
                                                        <td class="CreditMemo" style="display: none">Credit Memo</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row container-fluid">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <select id="ddlPageSize" class="form-control border-primary input-sm">
                                                <option selected="selected" value="10">10</option>
                                                <option value="50">50</option>
                                                <option value="100">100</option>
                                                <option value="500">500</option>
                                                <option value="1000">1000</option>
                                                <option value="0">All</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-10 col-sm-12">
                                        <div class="Pager"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
          <div class="modal fade" id="SecurityEnabledSearch" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <h4>Check Security </h4>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">Security </div>
                        <div class="col-md-9">
                            <input type="password" id="txtSecurity" class="form-control border-primary input-sm" oncopy="return false;" onpaste="return false;" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success buttonAnimation round box-shadow-1  btn-sm" onclick="clickonSecurity()">OK</button>
                    <button type="button" class="btn btn-danger buttonAnimation round box-shadow-1  btn-sm" onclick="clickonSecurityclose()" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
      
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="/Web/JS/WebOrderList.js?v=' + new Date() + '"></sc' + 'ript>');
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>


