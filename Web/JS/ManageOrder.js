﻿
var getid = "", WeightTax = 0.00;
var PackerSecurityEnable = false;
$(document).ready(function () {
    $('#bodycaption').addClass('menu-collapsed');
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    getid = getQueryString('OrderNo');
    if (getid != null) {
        editOrder(getid);
    }
});

function LoadProducts() {
    if ($("#LoadProducts").find("i").attr('class') == "ft-plus") {
        bindDropdown();
    }
};
function bindDropdown() {
    $.ajax({
        type: "POST",
        url: "ManageOrder.aspx/bindDropdown",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var product = $.parseJSON(response.d);

                $("#ddlProduct option:not(:first)").remove();
                $.each(product, function (index, item) {
                    $("#ddlProduct").append("<option WeightOz='" + item.WeightOz + "' MLQty='" + item.MLQty + "' value='" + item.AutoId + "'>" + item.ProductName + "</option>");
                });
                $("#ddlProduct").select2();
            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function BindUnittype() {
    $("#alertStockQty").hide();
    var productAutoId = $("#ddlProduct option:selected").val();
    $.ajax({
        type: "POST",
        url: "ManageOrder.aspx/bindUnitType",
        data: "{'productAutoId':" + productAutoId + "}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var unitType = $(xmldoc).find("Table");
                var unitDefault = $(xmldoc).find("Table1");
                var count = 0;

                $("#ddlUnitType option:not(:first)").remove();
                $.each(unitType, function () {
                    $("#ddlUnitType").append("<option value='" + $(this).find("AutoId").text() + "' EligibleforFree='" + $(this).find("EligibleforFree").text() + "' QtyPerUnit='" + $(this).find("Qty").text() + "' >" + $(this).find("UnitType").text() + " (" + $(this).find("Qty").text() + " pcs" + ")</option>");
                });
                if (unitDefault.length > 0) {
                    if (BUnitAutoId == 0) {
                        $("#ddlUnitType").val($(unitDefault).find('AutoId').text()).change();
                    } else {
                        $("#ddlUnitType").val(BUnitAutoId).change();
                        BUnitAutoId = 0;
                    }
                } else {
                    $("#ddlUnitType").val(0);
                }
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
};


var MinPrice, CostPrice;
function BindunitDetails() {
    debugger
    if ($("#ddlUnitType option:selected").attr('eligibleforfree') == 1) {
        $('#chkFreeItem').attr('disabled', false);
    } else {
        $('#chkFreeItem').attr('disabled', true);
    }
    $('#chkFreeItem').prop('checked', false);
    var data = {
        ProductAutoId: $("#ddlProduct").val(),
        UnitAutoId: $("#ddlUnitType").val(),
        CustAutoId: $("#hiddenCustomerId").val()
    };

    if (data.UnitAutoId != 0) {
        $.ajax({
            type: "POST",
            url: "ManageOrder.aspx/selectQtyPrice",
            data: "{'dataValues':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d != "Session Expired") {
                    var xmldoc = $.parseXML(response.d);
                    var stockAndPrice = $(xmldoc).find("Table");
                    if ($("#ddlUnitType").val() != 0) {
                        $("#alertStockQty").text('');
                        MinPrice = $(stockAndPrice).find("MinPrice").text();
                        CostPrice = $(stockAndPrice).find("CostPrice").text();
                        if (Number($("#ddlUnitType").val()) == 1) {
                            $("#alertStockQty").append(" [ Stock : " + $(stockAndPrice).find("Stock").text() + " case ] ");
                        } else if (Number($("#ddlUnitType").val()) == 2) {
                            $("#alertStockQty").append(" [ Stock : " + $(stockAndPrice).find("Stock").text() + " box ] ");
                        } else {
                            $("#alertStockQty").append(" [ Stock : " + $(stockAndPrice).find("Stock").text() + " pcs ] ");
                        }
                        $("#alertStockQty").show();
                    } else {
                        $("#alertStockQty").hide();
                    }
                } else {
                    location.href = '/';
                }

            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
};
var BUnitAutoId = 0;
function editOrder(orderNo) {
    var loginEmpType = $("#hiddenEmpTypeVal").val();
    $.ajax({
        type: "POST",
        url: "ManageOrder.aspx/editOrder",
        data: "{'orderNo':'" + orderNo + "','loginEmpType':'" + loginEmpType + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var RemarksDetails = $(xmldoc).find("Table");
                var order = $(xmldoc).find("Table1");
                var items = $(xmldoc).find("Table2");
                var unitType = $(xmldoc).find("Table3");
                $("#Table2 tbody tr").remove();
                if ($(RemarksDetails).length > 0) {
                    $('#Div2').show();
                    var rowtest = $("#Table2 thead tr").clone();
                    $.each(RemarksDetails, function (index) {
                        $(".SRNO", rowtest).text((Number(index) + 1));
                        $(".EmployeeName", rowtest).text($(this).find("EmpName").text());
                        $(".EmployeeType", rowtest).text($(this).find("EmpType").text());
                        $(".Remarks", rowtest).text($(this).find("Remarks").text());
                        $("#Table2 tbody").append(rowtest);
                        rowtest = $("#Table2 tbody tr:last").clone(true);
                    })
                } else {
                    $('#Div2').hide();
                }

                var LoginPerson = Number($("#hiddenEmpTypeVal").val());
                var orderStatus = Number($(order).find("StatusCode").text());

                if ($(order).find("PackerAutoId").text() != '') {
                    $('#btnAssignPacker').html('<b>Change Packer</b>');
                } else {
                    $('#btnAssignPacker').html('<b>Assign Packer</b>');
                }
                if (orderStatus >= 3) {
                    $("#btnEditOrder").hide();
                    $("#btnAssignPacker").hide();
                }
                $("#hiddenStatusCode").val($(order).find("StatusCode").text());
                $("#txtHOrderAutoId").val($(order).find("AutoId").text());
                $("#lblOrderId").val($(order).find("OrderNo").text());
                $("#lblsOrderDate").val($(order).find("OrderDate").text());
                $("#lblCustomer").val($(order).find("CustomerName").text());
                $("#hiddenCustomerId").val($(order).find("CustomerAutoId").text());
                $("#lblDeliveryDate").val($(order).find("DeliveryDate").text());
                $("#lblOrderStatus").val($(order).find("Status").text());
                $("#lblTerms").val($(order).find("TermsDesc").text());
                $("#lblShippingType").val($(order).find("ShippingTypeName").text());
                $("#lblBillAddress").val($(order).find("BillAddr").text());
                $("#lblBillState").val($(order).find("State1").text());
                $("#lblBillCity").val($(order).find("City1").text());
                $("#lblBillZip").val($(order).find("Zipcode1").text());
                $("#lblShipAddress").val($(order).find("ShipAddr").text());
                $("#lblShipState").val($(order).find("State2").text());
                $("#lblShipCity").val($(order).find("City2").text());
                $("#lblShipZip").val($(order).find("Zipcode2").text());
                $("#txtHTimes").val($(order).find("Times").text());
                $("#tblProductDetail tbody tr").remove();
                var row = $("#tblProductDetail thead tr").clone(true);
                $.each(items, function () {
                    var pid = $(this).find('ProductAutoId').text()
                    $(".ProId", row).html("<span isFreeItem='" + $(this).find("isFreeItem").text() + "' ItemAutoId='" + $(this).find("ItemAutoId").text() + "'></span><productid>" + $(this).find("ProductId").text() + "</productid>");
                    if ($(this).find("isFreeItem").text() == 1) {
                        $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'></span><productName>" + $(this).find("ProductName").text() + "</productName>  <item class='badge badge badge-pill badge-success'>Free</item>");
                    }
                    else if ($(this).find("IsExchange").text() == 1) {
                        $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'></span><productName>" + $(this).find("ProductName").text() + "</productName>  <item class='badge badge badge-pill badge-primary'>Exchange</item>");
                    }
                    else if ($(this).find("Tax").text() == 1) {
                        $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'></span><productName>" + $(this).find("ProductName").text() + "</productName>  <item class='badge badge badge-pill badge-danger'>Taxable</item>");
                    }
                    else {
                        $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'></span><productName>" + $(this).find("ProductName").text() + "</productName>  <item class='badge badge badge-pill'></item>");
                    }
                    $(".OM_MinPrice", row).html($(this).find("MinPrice").text());
                    $(".OM_CostPrice", row).html($(this).find("CostPrice").text());
                    var ddu = '<select class="form-control input-sm" onchange="rowCal(this)">';
                    $.each(unitType, function () {
                        var pid1 = $(this).find('ProductAutoId').text()
                        if (pid1 == pid) {
                            ddu += "<option value='" + $(this).find("AutoId").text() + "' EligibleforFree='" + $(this).find("EligibleforFree").text() + "' QtyPerUnit='" + $(this).find("Qty").text() + "' >" + $(this).find("UnitType").text() + " (" + $(this).find("Qty").text() + " pcs" + ")</option>"
                        }
                    })
                    ddu += '</select>';
                    $(".UnitType", row).html(ddu);
                    row.find(".UnitType > select").val($(this).find('UnitAutoId').text()).change();
                    if ($(this).find("IsExchange").text() == "1") {
                        $(".IsExchange", row).html("<span IsExchange='" + $(this).find("IsExchange").text() + "'> </span>" + '<IsExchange style="font-weight: bold" class="la la-check-circle success center"></IsExchange>');
                    } else {
                        $(".IsExchange", row).html("<span IsExchange='" + $(this).find("IsExchange").text() + "'> </span>");
                    }
                    $(".IsStatus", row).html("<span Status='0'> </span>");
                    $(".OldUnitType", row).html("<span UnitAutoId=" + $(this).find('UnitAutoId').text() + "></span>");
                    $(".ReqQty", row).html("<input type='text' class='form-control input-sm text-center' maxlength='4' onkeypress='return isNumberKey(event)' runat='server' onkeyup='rowCal(this)' value='" + $(this).find("RequiredQty").text() + "'/>");
                    $(".TtlPcs", row).text($(this).find("TotalPieces").text());
                    $(".Action", row).html("-");
                    $('#tblProductDetail tbody').append(row);
                    $("#tblProductDetail tbody").append(row);
                    row = $("#tblProductDetail tbody tr:last").clone(true);
                    $("#tblProductDetail").find(".Action").hide();
                    $("input[type='text']").attr("disabled", true);
                    $("textarea").attr("disabled", true);
                    $("select").attr("disabled", true);


                });
            } else {
                location.href = '/';
            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}

function deleteItemrecord(e) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this product.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, Cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: false,
            },
            confirm: {
                text: "Yes, Delete it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            if ($("#tblProductDetail tbody tr").length == 0) {
                $("#emptyTable").show();
            } else {
                $("#emptyTable").hide();
            }
            $(e).closest('tr').remove();
            swal("", "Item deleted successfully.", "success");

        } else {
            swal("", "Your product is safe.", "error");
        }
    })
}

function ClickEditBtn() {
    $("#btnback").show();
    $("#btnEditOrder").hide();
    $("#btnAssignPacker").hide();
    $("input[type='text']").attr("disabled", false);
    $("select").attr("disabled", false);
    $("#panelProduct").show();
    $("#btnUpdateOrder").show();

}
function viewOrderLog() {
    $.ajax({
        type: "POST",
        url: "ManageOrder.aspx/viewOrderLog",
        data: "{'OrderAutoId':" + $("#txtHOrderAutoId").val() + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var orderlog = $(xmldoc).find("Table");
            var order = $(xmldoc).find("Table1");

            $("#Span1").text(order.find("OrderNo").text());
            $("#lblOrderDate").text(order.find("OrderDate").text());

            $("#tblOrderLog tbody tr").remove();
            var row = $("#tblOrderLog thead tr").clone(true);
            if (orderlog.length > 0) {
                $("#EmptyTable").hide();
                $.each(orderlog, function (index) {
                    $(".SrNo", row).text(index + 1);
                    $(".ActionBy", row).text($(this).find("EmpName").text());
                    $(".Date", row).text($(this).find("ActionDate").text());
                    $(".Action", row).text($(this).find("Action").text());
                    $(".Remark", row).text($(this).find("Remarks").text());

                    $("#tblOrderLog tbody").append(row);
                    row = $("#tblOrderLog tbody tr:last").clone(true);
                });
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
    $("#modalOrderLog").modal('show');
}
function AssignedPacker() {
    $('#packerDiv').modal('show');
    getPackerList();
    $("#txtTimes").attr('disabled', false);
    $("#txtTimes").val($('#txtHTimes').val());
    $("#txtWarehouseRemark").attr('disabled', false);
    $("#txtWarehouseRemark").val($('#txtHWarehouseRemark').val());
}
function getPackerList() {

    $.ajax({
        type: "Post",
        url: "ManageOrder.aspx/getPackerList",
        data: "{'OrderAutoId':'" + $("#txtHOrderAutoId").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var Drivers = $(xmldoc).find("Table");
                var selectDrv = $(xmldoc).find("Table1");
                $("#packerList tbody tr").remove();
                var row = $("#packerList thead tr").clone(true);
                $.each(Drivers, function () {
                    $(".DrvName", row).html("<span pkdautoid =" + $(this).find("AutoId").text() + ">" + $(this).find("Name").text() + "</span>");
                    if ($(this).find("AutoId").text() == $(selectDrv).find("PackerAutoId").text()) {
                        $(".Select", row).html("<input type='radio' name='packer' class='radio' onchange = 'changePacker(this)' checked='checked'>");
                        $("#btnAsgn").attr("disabled", true);
                    } else {
                        $(".Select", row).html("<input type='radio' name='packer' class='radio' onchange = 'changePacker(this)' >");
                    }

                    $("#packerList tbody").append(row);

                    row = $("#packerList tbody tr:last").clone(true);
                });
                $("#txtWarehouseRemark").val($(selectDrv).find("WarehouseRemarks").text());
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function assignPacker() {
    var PackerAutoId, flag = true;
    $("#packerList tbody tr").each(function () {
        if ($(this).find("input[name='packer']").is(":checked")) {
            PackerAutoId = $(this).find(".DrvName span").attr("pkdautoid");
            flag = false;
        }
    });

    var data = {
        PackerAutoId: PackerAutoId,
        OrderAutoId: $("#txtHOrderAutoId").val(),
        Times: $("#txtTimes").val(),
        Remarks: $("#txtWarehouseRemark").val().trim()
    };

    if (!flag) {
        $.ajax({
            type: "Post",
            url: "ManageOrder.aspx/assignPacker",
            data: JSON.stringify({ dataValues: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d != "Session Expired") {

                    if (response.d == 'true') {
                        swal("", "Packer assigned successfully.", "success").then(function () {
                            $('#packerDiv').modal('hide');
                            editOrder(getid);
                            $('#btnAssignPacker').html('<b>Change Packer</b>');
                        });
                    } else {
                        swal("Error!", response.d, "error")
                    }

                } else {
                    location.href = '/';
                }

            },
            error: function (result) {
                swal("Error!", "Oops! Something went wrong.Please try later.", "error")
            },
            failure: function (result) {
                swal("Error!", "Oops! Something went wrong.Please try later.", "error")
            }
        });
    } else {
        toastr.error('Error : No Packer selected.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
function changePacker(e) {
    var btnAsgn = $(e).closest(".modal-dialog").find("#btnAsgn");
    $(btnAsgn).attr("disabled", false);
}
function clickonSecurity() {

    $.ajax({
        type: "Post",
        url: "ManageOrder.aspx/clickonSecurity",
        data: "{'CheckSecurity':'" + $("#txtSecurity").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d == 'true') {
                    PackerSecurityEnable = true;
                    $("#SecurityEnabled").modal('hide');
                    $('#btnpkd').removeAttr('disabled');
                } else {
                    $('#SecurityEnvalid').modal('show');
                    $('#Sbarcodemsg').html('Invalid Security Key.')
                }
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });


}
function AddItems() {
    var flag1 = false;
    var validatecheck = dynamicInputTypeSelect2('selectvalidate');
    if (!validatecheck) {
        checkRequiredField();
    }
    else {
        validatecheck = checkRequiredField();
    }
    if (validatecheck) {

        var productAutoId = $("#ddlProduct option:selected").val();
        var unitAutoId = $("#ddlUnitType option:selected").val();
        var chkExchange = $('#chkExchange').prop('checked');
        var chkFreeItem = $('#chkFreeItem').prop('checked');
        var IsExchange = 0, IsTaxable = 0, IsFreeItem = 0, chkcount = 0;
        if (chkExchange == true) {
            IsExchange = 1;
            chkcount = chkcount + 1;
            MLQty = 0.00;
            WeightOz = 0.00;
        }
        if (chkFreeItem == true) {
            IsFreeItem = 1;
            chkcount = chkcount + 1;
            MLQty = 0.00;
            WeightOz = 0.00;
        }

        if (chkcount > 1) {
            toastr.error('Please check only one checkbox.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

            return;
        }
        if ($("#txtReqQty").val() == 0) {
            toastr.error('Required quantity can not be Zero.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            return;
        }
        $("#emptyTable").hide();
        $("#alertStockQty").hide();

        var tr = $("#tblProductDetail tbody tr");
        for (var i = 0; i <= tr.length - 1; i++) {
            if ($(tr[i]).find(".ProId > span").attr("IsFreeItem") == IsFreeItem && $(tr[i]).find(".ProName > span").attr("productautoid") == productAutoId &&
                $(tr[i]).find(".UnitType > select").val() == unitAutoId && $(tr[i]).find(".IsExchange > span").attr("IsExchange") == IsExchange) {
                var reqQty = Number($(tr[i]).find(".ReqQty input").val()) + Number($("#txtReqQty").val());
                $(tr[i]).find(".ReqQty input").val(reqQty);
                flag1 = true;
                rowCal($(tr[i]).find(".ReqQty > input"));
                $('#tblProductDetail tbody tr:first').before($(tr[i]));
            }

            if (chkFreeItem) {
                if (($(tr[i]).find(".ProName span").attr("productautoid") == productAutoId && $(tr[i]).find(".ProId > span").attr("IsFreeItem") == IsFreeItem &&
                    $(tr[i]).find(".IsExchange > span").attr("IsExchange") == '0') && $(tr[i]).find(".UnitType > select").val() != unitAutoId) {
                    flag1 = true;
                    toastr.error('Product already added in list.', 'Worning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    $("#ddlProduct").select2('val', '0');
                    $("#ddlUnitType").val(0);
                    $("#txtReqQty").val("1");
                    return;

                }
            }
            if (chkExchange) {
                if ($(tr[i]).find(".ProName span").attr("productautoid") == productAutoId && $(tr[i]).find(".UnitType > select").val() != unitAutoId && $(tr[i]).find(".ProId > span").attr("IsFreeItem") == '0' && $(tr[i]).find(".IsExchange > span").attr("IsExchange") == IsExchange) {
                    flag1 = true;
                    toastr.error('Product already added in list.', 'Worning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    $("#ddlProduct").select2('val', '0');
                    $("#ddlUnitType").val(0);
                    $("#txtReqQty").val("1");
                    return;
                }
            }

            if ($(tr[i]).find(".ProName span").attr("productautoid") == productAutoId &&
                $(tr[i]).find(".IsExchange > span").attr("IsExchange") == IsExchange
                && $(tr[i]).find(".ProId > span").attr("IsFreeItem") == IsFreeItem) {
                flag1 = true;
                toastr.error('Product already added in list.', 'Worning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                $("#ddlProduct").select2('val', '0');
                $("#ddlUnitType").val(0);
                $("#txtReqQty").val("1");
                return;
            }
        }

        if (!flag1) {
            var product = $("#ddlProduct option:selected").text().split("-");
            var row = $("#tblProductDetail thead tr").clone(true);
            $(".ProId", row).html("<span IsFreeItem='" + IsFreeItem + "'></span>" + product[0]);
            if (IsFreeItem == 1) {
                $(".ProName", row).html("<span ProductAutoId='" + $("#ddlProduct option:selected").val() + "'>" + product[1] + "</span>" + " <product class='badge badge badge-pill badge-success'>Free</product>");
            } else if (IsExchange == 1) {
                $(".ProName", row).html("<span ProductAutoId='" + $("#ddlProduct option:selected").val() + "'>" + product[1] + "</span>" + " <product class='badge badge badge-pill badge-primary'>Exchange</product>");

            } else {
                $(".ProName", row).html("<span ProductAutoId='" + $("#ddlProduct option:selected").val() + "'>" + product[1] + "</span>");
            }
            $(".UnitType", row).html('<select class="form-control input-sm" onchange="rowCal(this)"></select>');
            $(".ReqQty", row).html("<input type='text' onkeypress='return isNumberKey(event)'  maxlength='4' class='form-control input-sm text-center' runat='server' onkeyup='rowCal(this)' value='" + $("#txtReqQty").val() + "'/>");
            $(".TtlPcs", row).text(parseInt($("#txtReqQty").val()) * parseInt($("#ddlUnitType option:selected").attr("QtyPerUnit")));
            if (IsExchange == 1) {
                $(".IsExchange", row).html("<span IsExchange='" + IsExchange + "'> </span>" + '<IsExchange style="font-weight: bold" class="la la-check-circle success center"></IsExchange>');
            } else {
                $(".IsExchange", row).html("<span IsExchange='" + IsExchange + "'> </span>");
            }
            $(".IsStatus", row).html("<span Status='1'> </span>");
            $(".OM_MinPrice", row).html(MinPrice);
            $(".OM_CostPrice", row).html(CostPrice);
            $(".OldUnitType", row).html("<span UnitAutoId='0'></span>");
            $(".Action", row).html("<a href='javascript:;' id='deleterow' onclick='deleteItemrecord(this)'><span class='ft-x'></span></a>");
            if ($('#tblProductDetail tbody tr').length > 0) {
                $('#tblProductDetail tbody tr:first').before(row);
            }
            else {
                $('#tblProductDetail tbody').append(row);
            }
            fillUnitTlb(row);
            rowCal(row.find(".ReqQty > input"));
            $("#tblProductDetail").find(".Action").show();
        }
    }
    else {
        toastr.error('All * fields are mandatory', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
function fillUnitTlb(e) {
    var row = $(e);
    var ProductId = row.find(".ProName > span").attr('productautoid');
    $.ajax({
        type: "POST",
        url: "ManageOrder.aspx/bindUnitType",
        data: "{'productAutoId':" + ProductId + "}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var unitType = $(xmldoc).find("Table");
                var unitDefault = $(xmldoc).find("Table1");
                row.find(".UnitType >select option:not(:first)").remove();
                $.each(unitType, function () {
                    row.find(".UnitType > select").append("<option value='" + $(this).find("AutoId").text() + "' EligibleforFree='" + $(this).find("EligibleforFree").text() + "' QtyPerUnit='" + $(this).find("Qty").text() + "' >" + $(this).find("UnitType").text() + " (" + $(this).find("Qty").text() + " pcs" + ")</option>");
                });
                if (unitDefault.length > 0) {
                    if (BUnitAutoId == 0) {
                        row.find(".UnitType > select").val($("#ddlUnitType option:selected").val()).change();
                    } else {
                        row.find(".UnitType > select").val(BUnitAutoId).change();
                        BUnitAutoId = 0;
                    }
                }
                $(".TtlPcs", row).text(parseInt($("#txtReqQty").val()) * parseInt($("#ddlUnitType option:selected").attr('QtyPerUnit')));

                toastr.success('Product added successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                $("#ddlUnitType").val(0);
                $("#ddlProduct").select2('val', '0');
                $("#txtBarcode").val('');
                $("#txtBarcode").focus();
                $("#txtReqQty").val("1");
                $('#chkExchange').prop('checked', false);
                $('#chkFreeItem').prop('checked', false);
                $('#chkFreeItem').prop('disabled', false);
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });


}
function readBarcode() {
    var Barcode = $("#txtBarcode").val();
    if (Barcode != "") {
        var chkExchange = $('#chkExchange').prop('checked');
        var chkFreeItem = $('#chkFreeItem').prop('checked');
        var IsExchange = 0, IsFreeItem = 0;
        if (chkExchange == true) {
            IsExchange = 1;
        }
        if (chkFreeItem == true) {
            IsFreeItem = 1;
        }
        debugger
        var data = {
            Barcode: Barcode,
            CustomerAutoId: $("#hiddenCustomerId").val()

        }
        $.ajax({
            type: "POST",
            url: "ManageOrder.aspx/GetBarDetails",
            data: "{'dataValues':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                var xmldoc = $.parseXML(response.d);
                var product = $(xmldoc).find("Table");
                var unitType = $(xmldoc).find("Table1");
                if (product.length > 0) {
                    var productAutoId = $(product).find('ProductAutoId').text();
                    var unitAutoId = $(product).find('UnitType').text();
                    var flag1 = false;
                    var tr = $("#tblProductDetail tbody tr");

                    for (var i = 0; i <= tr.length - 1; i++) {
                        if ($(tr[i]).find(".ProId > span").attr("IsFreeItem") == IsFreeItem && $(tr[i]).find(".ProName > span").attr("productautoid") == productAutoId &&
                            $(tr[i]).find(".UnitType > select").val() == unitAutoId && $(tr[i]).find(".IsExchange > span").attr("IsExchange") == IsExchange) {
                            var reqQty = Number($(tr[i]).find(".ReqQty input").val()) + 1;
                            $(tr[i]).find(".ReqQty input").val(reqQty);
                            flag1 = true;
                            rowCal($(tr[i]).find(".ReqQty > input"));
                            $('#tblProductDetail tbody tr:first').before($(tr[i]));
                            toastr.success('Product added successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                            clear();
                            return;
                        }
                        if (chkFreeItem) {
                            if (($(tr[i]).find(".ProName > span").attr("productautoid") == productAutoId && $(tr[i]).find(".ProId > span").attr("IsFreeItem") == IsFreeItem && $(tr[i]).find(".IsExchange > span").attr("IsExchange") == '0') && $(tr[i]).find(".UnitType > select").val() != unitAutoId) {
                                flag1 = true;
                                toastr.error('Product already added in list.', 'Worning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                                clear();
                                return;
                            }
                        }
                        if (chkExchange) {
                            if ($(tr[i]).find(".ProName > span").attr("productautoid") == productAutoId && $(tr[i]).find(".UnitType > select").val() != unitAutoId && $(tr[i]).find(".ProId > span").attr("IsFreeItem") == '0' && $(tr[i]).find(".IsExchange > span").attr("IsExchange") == IsExchange) {
                                flag1 = true;
                                toastr.error('Product already added in list.', 'Worning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                                clear();
                                return;
                            }
                        }
                        if ($(tr[i]).find(".ProName > span").attr("productautoid") == productAutoId &&
                            $(tr[i]).find(".IsExchange > span").attr("IsExchange") == IsExchange
                            && $(tr[i]).find(".ProId > span").attr("IsFreeItem") == IsFreeItem) {
                            flag1 = true;
                            toastr.error('Product already added in list.', 'Worning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                            clear();
                            return;
                        }
                    }

                    if (!flag1) {
                        var row = $("#tblProductDetail thead tr").clone(true);
                        $(".ProId", row).html($(product).find('ProductId').text() + "<span IsFreeItem='" + IsFreeItem + "'></span>");
                        if (IsFreeItem == 1) {
                            $(".ProName", row).html("<span ProductAutoId='" + productAutoId + "'>" + $(product).find('ProductName').text() + "</span>" + " <product class='badge badge badge-pill badge-success'>Free</product>");
                        }
                        else if (IsExchange == 1) {
                            $(".ProName", row).html("<span ProductAutoId='" + productAutoId + "'>" + $(product).find('ProductName').text() + "</span>" + " <product class='badge badge badge-pill badge-primary'>Exchange</product>");
                        }
                        else {
                            $(".ProName", row).html("<span ProductAutoId='" + productAutoId + "'>" + $(product).find('ProductName').text() + "</span>");
                        }

                        $(".OM_MinPrice", row).html($(product).find('MinPrice').text());
                        $(".OM_CostPrice", row).html($(product).find('CostPrice').text());

                        $(".UnitType", row).html("<span UnitAutoId='" + unitAutoId + "' QtyPerUnit='" + $(product).find('UnitQty').text() + "'>" + $(product).find('UnitName').text() + "</span>");
                        var ddu = '<select class="form-control input-sm border-primary ddlreq" onchange="rowCal(this)">';//
                        $.each(unitType, function () {
                            ddu += "<option value='" + $(this).find("AutoId").text() + "' EligibleforFree='" + $(this).find("EligibleforFree").text() + "' QtyPerUnit='" + $(this).find("Qty").text() + "' >" + $(this).find("UnitType").text() + " (" + $(this).find("Qty").text() + " pcs" + ")</option>"
                        })
                        ddu += '</select>';
                        $(".UnitType", row).html(ddu);
                        row.find(".UnitType > select").val(unitAutoId).change();
                        $(".ReqQty", row).html("<input type='text' onkeypress='return isNumberKey(event)' class='form-control input-sm border-primary text-center' runat='server' onkeyup='rowCal(this)' value='1'/>");
                        $(".TtlPcs", row).text(parseInt($("#txtReqQty").val()) * $(product).find('UnitQty').text());
                        if (IsExchange == 1) {
                            $(".IsExchange", row).html("<span IsExchange='" + IsExchange + "'> </span>" + '<IsExchange style="font-weight: bold" class="la la-check-circle success center"></IsExchange>');
                        } else {
                            $(".IsExchange", row).html("<span IsExchange='" + IsExchange + "'> </span>");
                        }
                        $(".IsStatus", row).html("<span Status='1'> </span>");
                        $(".Action", row).html("<a href='javascript:;' id='deleterow' onclick='deleteItemrecord(this)'><span class='ft-x'></span></a>");
                        if ($('#tblProductDetail tbody tr').length > 0) {
                            $('#tblProductDetail tbody tr:first').before(row);
                        }
                        else {
                            $('#tblProductDetail tbody').append(row);
                        }
                        rowCal(row.find(".ReqQty > input"));
                        toastr.success('Product added successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                        $("#txtBarcode").val('');
                        $("#txtBarcode").focus();
                    }
                    else {
                        toastr.success('Product added successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    }
                    $("#txtBarcode").val('');
                    $("#txtBarcode").focus();

                } else {
                    swal({
                        title: "Error!",
                        text: "Barcode does not exists.",
                        icon: "error",
                        closeOnClickOutside: false
                    }).then(function (isConfirm) {

                        if (isConfirm) {
                            $("#txtBarcode").val('');
                            $("#txtBarcode").focus();
                        }
                    });
                    $("#yes_audio")[0].play();
                    clear();
                }
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    } else {
        $("#ddlProduct").val(0).change();
        $("#alertBarcodeCount").hide();
        $("#ddlProduct").attr('disabled', false);
        $("#ddlUnitType").attr('disabled', false);
    }
}
function clear() {
    $("#panelProduct select").val(0);
    $("#txtQuantity, #txtTotalPieces").val("0");
    $("#alertBarcodeCount").hide();
    $("#txtBarcode").val('');
    $("#txtReqQty").focus();
    $("#txtBarcode").blur();
    $("#txtBarcode").val('');
    $("#txtBarcode").focus();
}
function rowCal(e) {
    var row = $(e).closest("tr");
    var totalPcs = (parseInt(row.find(".ReqQty > input").val()) * parseInt(row.find(".UnitType > select option:selected").attr("QtyPerUnit")));
    row.find(".TtlPcs").html(totalPcs);
}
function Back() {
    editOrder(getid);
    $("#tblProductDetail").find(".Action").hide();
    $("#btnback").hide();
    $("#btnEditOrder").show();
    $("#btnAssignPacker").show();
    $("input[type='text']").attr("disabled", true);
    $("select").attr("disabled", true);
    $("#panelProduct").hide();
    $("#btnUpdateOrder").hide();
}

function UpdateOrder() {
    var flag1 = false, flag2 = false;
    if ($("#tblProductDetail tbody tr").length > 0) {
        $("#tblProductDetail tbody tr").each(function () {
            if (parseInt(($(this).find(".ReqQty input").val())) == 0 || $(this).find(".ReqQty input").val() == "") {
                $(this).find(".ReqQty input").focus().css("border-color", "red");
                flag1 = true;
            } else {
                $(this).find(".ReqQty input").blur().css("border-color", "#ccc");
            }
        });
        if (!flag1 && !flag2) {
            var Product = [];
            $("#tblProductDetail tbody tr").each(function () {
                Product.push({
                    'ProductAutoId': $(this).find('.ProName').find('span').attr('ProductAutoId'),
                    'UnitAutoId': $(this).find('.UnitType select').val(),
                    'QtyPerUnit': $(this).find('.UnitType').find('option:selected').attr('qtyperunit'),
                    'RequiredQty': $(this).find('.ReqQty input').val(),
                    'IsExchange': $(this).find('.IsExchange span').attr('IsExchange'),
                    'IsFreeItem': $(this).find('.ProId span').attr('IsFreeItem'),
                    'Status': $(this).find('.IsStatus span').attr('Status'),
                    'OldUnitAutoId': $(this).find('.OldUnitType span').attr('UnitAutoId'),
                    'OM_MinPrice': parseFloat($(this).find('.OM_MinPrice').text()).toFixed(),
                    'OM_CostPrice': parseFloat($(this).find('.OM_CostPrice').text()).toFixed()

                });
            });
            var orderData = {
                OrderAutoId: $("#txtHOrderAutoId").val()
            };
            $.ajax({
                type: "Post",
                url: "ManageOrder.aspx/updateOrder",
                data: "{'TableValues':'" + JSON.stringify(Product) + "','orderData':'" + JSON.stringify(orderData) + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    console.log(response);
                    if (response.d != "Session Expired") {
                        if (response.d == 'Yes') {
                            swal("", "Order updated successfully.", "success").then(function () {
                                Back();
                            });
                        }
                        else if (response.d == 'No') {
                            swal("", "Order has been processed so you can not update.", "error").then(function () {
                                location.reload();
                            });
                        }
                        else {
                            swal("Error !", "Oops ! Something went wrong. Please try later.", "error");
                        }

                    } else {
                        location.href = '/';
                    }

                },
                error: function (result) {
                },
                failure: function (result) {
                }
            });
        } else {
            if (flag1) {
                toastr.error('Required Quantity cannot be left empty.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            }
        }
    } else {
        toastr.error('No Product Added in the List', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}